﻿using System;
using System.Web;
using System.Data.OleDb;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DevExpress.Xpo;
using DevExpress.Xpo.DB;
using DevExpress.ExpressApp;
using DevExpress.Persistent.Base;
using FullDrive.Module.BusinessObjects;
using DevExpress.Data.Filtering;
using System.Web.Hosting;
using System.Reflection;
using System.Data;
using Excel;
using DevExpress.ExpressApp.Security.Strategy;
using System.Net.Mail;
using System.Net;

namespace FullDrive.Module.CustomProcess
{
    class GlobalFunction
    {
        #region Numbering

        //Harus di rubah untuk multi company
        public string GetNumberingUnlockOptimisticRecord(IDataLayer _currIDataLayer, ObjectList _currObject)
        {
            string _result = null;
            try
            {
                if(_currIDataLayer != null)
                {
                    Session _generatorSession = new Session(_currIDataLayer);

                    if(_generatorSession != null)
                    {
                        ApplicationSetup _appSetup = _generatorSession.FindObject<ApplicationSetup>(new GroupOperator(GroupOperatorType.And,
                                                new BinaryOperator("Active", true), new BinaryOperator("DefaultSystem", true)));
                        if (_appSetup != null)
                        {
                            XPCollection<NumberingHeader> _numberingHeaders = new XPCollection<NumberingHeader>
                                                                (_generatorSession, new GroupOperator(GroupOperatorType.And,
                                                                 new BinaryOperator("NumberingType", NumberingType.Objects),
                                                                 new BinaryOperator("ApplicationSetup", _appSetup, BinaryOperatorType.Equal),
                                                                 new BinaryOperator("Active", true)));
                            if (_numberingHeaders != null && _numberingHeaders.Count() > 0)
                            {
                                foreach(NumberingHeader _numberingHeader in _numberingHeaders)
                                {
                                    if(_numberingHeader.Workplace == null)
                                    {
                                        NumberingLine _numberingLine = _generatorSession.FindObject<NumberingLine>
                                                            (new GroupOperator(GroupOperatorType.And,
                                                             new BinaryOperator("ObjectList", _currObject),
                                                             new BinaryOperator("Default", true),
                                                             new BinaryOperator("Active", true),
                                                             new BinaryOperator("NumberingHeader", _numberingHeader)));
                                        if (_numberingLine != null)
                                        {
                                            _numberingLine.LastValue = _numberingLine.LastValue + _numberingLine.IncrementValue;
                                            _numberingLine.Save();
                                            _result = _numberingLine.FormatedValue;
                                            _numberingLine.Session.CommitTransaction();
                                        }
                                    } 
                                } 
                            }
                        }
                    } 
                }
                
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Module = GlobalFunction " + ex.ToString());
            }
            return _result;
        }

        public string GetNumberingUnlockOptimisticRecordBasedOrganization(IDataLayer _currIDataLayer, ObjectList _currObject, Company _currCompany, Workplace _currWorkplace)
        {
            string _result = null;
            try
            {
                string _locCompanyCode = null;
                string _locWorkplaceCode = null;
                if (_currIDataLayer != null)
                {
                    Session _generatorSession = new Session(_currIDataLayer);

                    if (_generatorSession != null && _currCompany != null && _currWorkplace != null)
                    {
                        if(_currCompany.Code != null)
                        {
                            _locCompanyCode = _currCompany.Code;
                        }
                        if(_currWorkplace.Code != null)
                        {
                            _locWorkplaceCode = _currWorkplace.Code;
                        }
                        
                        ApplicationSetup _appSetup = _generatorSession.FindObject<ApplicationSetup>(new GroupOperator(GroupOperatorType.And,
                                                new BinaryOperator("Company.Code", _locCompanyCode),
                                                new BinaryOperator("Active", true)));
                        if (_appSetup != null)
                        {
                            NumberingHeader _numberingHeader = _generatorSession.FindObject<NumberingHeader>
                                                                (new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("Company.Code", _locCompanyCode),
                                                                new BinaryOperator("Workplace.Code", _locWorkplaceCode),
                                                                new BinaryOperator("NumberingType", NumberingType.Objects),
                                                                new BinaryOperator("ApplicationSetup", _appSetup, BinaryOperatorType.Equal),
                                                                new BinaryOperator("Active", true)));
                            if (_numberingHeader != null)
                            {
                                NumberingLine _numberingLine = _generatorSession.FindObject<NumberingLine>
                                                            (new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("Company.Code", _locCompanyCode),
                                                            new BinaryOperator("Workplace.Code", _locWorkplaceCode),
                                                            new BinaryOperator("ObjectList", _currObject),
                                                            new BinaryOperator("Default", true),
                                                            new BinaryOperator("Active", true),
                                                            new BinaryOperator("NumberingHeader", _numberingHeader)));
                                if (_numberingLine != null)
                                {
                                    _numberingLine.LastValue = _numberingLine.LastValue + _numberingLine.IncrementValue;
                                    _numberingLine.Save();
                                    _result = _numberingLine.FormatedValue;
                                    _numberingLine.Session.CommitTransaction();
                                }
                            }
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Module = GlobalFunction " + ex.ToString());
            }
            return _result;
        }

        public string GetNumberingRecord(Session _generatorSession, ObjectList _currObject)
        {
            string _result = null;
            try
            {
                if(_generatorSession != null)
                {
                    ApplicationSetup _appSetup = _generatorSession.FindObject<ApplicationSetup>(new GroupOperator(GroupOperatorType.And,
                                            new BinaryOperator("Active", true), new BinaryOperator("DefaultSystem", true)));
                    if (_appSetup != null)
                    {
                        NumberingHeader _numberingHeader = _generatorSession.FindObject<NumberingHeader>
                                                            (new GroupOperator(GroupOperatorType.And,
                                                             new BinaryOperator("NumberingType", NumberingType.Objects),
                                                             new BinaryOperator("ApplicationSetup", _appSetup, BinaryOperatorType.Equal),
                                                             new BinaryOperator("Active", true)));
                        if (_numberingHeader != null)
                        {
                            NumberingLine _numberingLine = _generatorSession.FindObject<NumberingLine>
                                                        (new GroupOperator(GroupOperatorType.And,
                                                         new BinaryOperator("ObjectList", _currObject),
                                                         new BinaryOperator("Default", true),
                                                         new BinaryOperator("Active", true),
                                                         new BinaryOperator("NumberingHeader", _numberingHeader)));
                            if (_numberingLine != null)
                            {
                                _numberingLine.LastValue = _numberingLine.LastValue + _numberingLine.IncrementValue;
                                _numberingLine.Save();
                                _result = _numberingLine.FormatedValue;
                                _numberingLine.Session.CommitTransaction();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Module = GlobalFunction " + ex.ToString());
            }
            return _result;
        }

        public string GetDocumentNumberingUnlockOptimisticRecord(IDataLayer _currIDataLayer, DocumentType _currDocType)
        {
            string _result = null;
            try
            {
                Session _generatorSession = new Session(_currIDataLayer);

                ApplicationSetup _appSetup = _generatorSession.FindObject<ApplicationSetup>(new GroupOperator(GroupOperatorType.And,
                                            new BinaryOperator("Active", true), new BinaryOperator("DefaultSystem", true)));
                if (_appSetup != null)
                {
                    NumberingHeader _numberingHeader = _generatorSession.FindObject<NumberingHeader>
                                                        (new GroupOperator(GroupOperatorType.And,
                                                         new BinaryOperator("NumberingType", NumberingType.Documents),
                                                         new BinaryOperator("ApplicationSetup", _appSetup, BinaryOperatorType.Equal),
                                                         new BinaryOperator("Active", true)));

                    if (_numberingHeader != null)
                    {
                        NumberingLine _numberingLine = _generatorSession.FindObject<NumberingLine>
                                                    (new GroupOperator(GroupOperatorType.And,
                                                     new BinaryOperator("DocumentType.Oid", _currDocType.Oid),
                                                     new BinaryOperator("Active", true),
                                                     new BinaryOperator("NumberingHeader", _numberingHeader)));
                        if (_numberingLine != null)
                        {
                            _numberingLine.LastValue = _numberingLine.LastValue + _numberingLine.IncrementValue;
                            _numberingLine.Save();
                            _result = _numberingLine.FormatedValue;
                            _numberingLine.Session.CommitTransaction();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Module = GlobalFunction " + ex.ToString());
            }
            return _result;
        }

        public string GetDocumentNumberingUnlockOptimisticRecordBasedOrganization(IDataLayer _currIDataLayer, DocumentType _currDocType, Company _currCompany, Workplace _currWorkplace)
        {
            string _result = null;
            try
            {
                string _locCompanyCode = null;
                string _locWorkplaceCode = null;

                Session _generatorSession = new Session(_currIDataLayer);

                if(_generatorSession != null && _currCompany != null && _currWorkplace != null)
                {
                    if (_currCompany.Code != null)
                    {
                        _locCompanyCode = _currCompany.Code;
                    }
                    if (_currWorkplace.Code != null)
                    {
                        _locWorkplaceCode = _currWorkplace.Code;
                    }
                    ApplicationSetup _appSetup = _generatorSession.FindObject<ApplicationSetup>(new GroupOperator(GroupOperatorType.And,
                                            new BinaryOperator("Company.Code", _locCompanyCode),
                                            new BinaryOperator("Active", true)));
                    if (_appSetup != null)
                    {
                        NumberingHeader _numberingHeader = _generatorSession.FindObject<NumberingHeader>
                                                            (new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("Company.Code", _locCompanyCode),
                                                            new BinaryOperator("Workplace.Code", _locWorkplaceCode),
                                                            new BinaryOperator("NumberingType", NumberingType.Documents),
                                                            new BinaryOperator("ApplicationSetup", _appSetup, BinaryOperatorType.Equal),
                                                            new BinaryOperator("Active", true)));

                        if (_numberingHeader != null)
                        {
                            NumberingLine _numberingLine = _generatorSession.FindObject<NumberingLine>
                                                        (new GroupOperator(GroupOperatorType.And,
                                                        new BinaryOperator("Company.Code", _locCompanyCode),
                                                        new BinaryOperator("Workplace.Code", _locWorkplaceCode),
                                                        new BinaryOperator("DocumentType.Oid", _currDocType.Oid),
                                                        new BinaryOperator("Active", true),
                                                        new BinaryOperator("NumberingHeader", _numberingHeader)));
                            if (_numberingLine != null)
                            {
                                _numberingLine.LastValue = _numberingLine.LastValue + _numberingLine.IncrementValue;
                                _numberingLine.Save();
                                _result = _numberingLine.FormatedValue;
                                _numberingLine.Session.CommitTransaction();
                            }
                        }
                    }
                }

                
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Module = GlobalFunction " + ex.ToString());
            }
            return _result;
        }

        public string GetNumberingSelectionUnlockOptimisticRecord(IDataLayer _currIDataLayer, ObjectList _currObject, NumberingLine _currNumberingLine)
        {
            string _result = null;
            try
            {
                Session _generatorSession = new Session(_currIDataLayer);

                ApplicationSetup _appSetup = _generatorSession.FindObject<ApplicationSetup>(new GroupOperator(GroupOperatorType.And,
                                            new BinaryOperator("Active", true), new BinaryOperator("DefaultSystem", true)));
                if (_appSetup != null)
                {
                    NumberingHeader _numberingHeader = _generatorSession.FindObject<NumberingHeader>
                                                        (new GroupOperator(GroupOperatorType.And,
                                                         new BinaryOperator("NumberingType", NumberingType.Objects),
                                                         new BinaryOperator("ApplicationSetup", _appSetup, BinaryOperatorType.Equal),
                                                         new BinaryOperator("Active", true)));
                    if (_numberingHeader != null)
                    {
                        NumberingLine _numberingLine = _generatorSession.FindObject<NumberingLine>
                                                    (new GroupOperator(GroupOperatorType.And,
                                                     new BinaryOperator("Oid", _currNumberingLine.Oid),
                                                     new BinaryOperator("ObjectList", _currObject),
                                                     new BinaryOperator("Selection", true),
                                                     new BinaryOperator("Active", true),
                                                     new BinaryOperator("NumberingHeader", _numberingHeader)));
                        if (_numberingLine != null)
                        {
                            _numberingLine.LastValue = _numberingLine.LastValue + _numberingLine.IncrementValue;
                            _numberingLine.Save();
                            _result = _numberingLine.FormatedValue;
                            _numberingLine.Session.CommitTransaction();
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Module = GlobalFunction " + ex.ToString());
            }
            return _result;
        }

        public string GetNumberingSelectionUnlockOptimisticRecordBasedOrganization(IDataLayer _currIDataLayer, ObjectList _currObject, NumberingLine _currNumberingLine, Company _currCompany, Workplace _currWorkplace)
        {
            string _result = null;
            try
            {
                string _locCompanyCode = null;
                string _locWorkplaceCode = null;

                Session _generatorSession = new Session(_currIDataLayer);

                if(_generatorSession != null && _currCompany != null && _currWorkplace != null)
                {
                    if (_currCompany.Code != null)
                    {
                        _locCompanyCode = _currCompany.Code;
                    }
                    if (_currWorkplace.Code != null)
                    {
                        _locWorkplaceCode = _currWorkplace.Code;
                    }

                    ApplicationSetup _appSetup = _generatorSession.FindObject<ApplicationSetup>(new GroupOperator(GroupOperatorType.And,
                                            new BinaryOperator("Company.Code", _locCompanyCode),
                                            new BinaryOperator("Active", true)));
                    if (_appSetup != null)
                    {
                        NumberingHeader _numberingHeader = _generatorSession.FindObject<NumberingHeader>
                                                            (new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("Company.Code", _locCompanyCode),
                                                            new BinaryOperator("Workplace.Code", _locWorkplaceCode),
                                                            new BinaryOperator("NumberingType", NumberingType.Objects),
                                                            new BinaryOperator("ApplicationSetup", _appSetup, BinaryOperatorType.Equal),
                                                            new BinaryOperator("Active", true)));
                        if (_numberingHeader != null)
                        {
                            NumberingLine _numberingLine = _generatorSession.FindObject<NumberingLine>
                                                        (new GroupOperator(GroupOperatorType.And,
                                                        new BinaryOperator("Company.Code", _locCompanyCode),
                                                        new BinaryOperator("Workplace.Code", _locWorkplaceCode),
                                                        new BinaryOperator("Oid", _currNumberingLine.Oid),
                                                        new BinaryOperator("ObjectList", _currObject),
                                                        new BinaryOperator("Selection", true),
                                                        new BinaryOperator("Active", true),
                                                        new BinaryOperator("NumberingHeader", _numberingHeader)));
                            if (_numberingLine != null)
                            {
                                _numberingLine.LastValue = _numberingLine.LastValue + _numberingLine.IncrementValue;
                                _numberingLine.Save();
                                _result = _numberingLine.FormatedValue;
                                _numberingLine.Session.CommitTransaction();
                            }

                        }
                    }
                }
                
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Module = GlobalFunction " + ex.ToString());
            }
            return _result;
        }

        public string GetNumberingSignUnlockOptimisticRecord(IDataLayer _currIDataLayer, ObjectList _currObject)
        {
            string _result = null;
            try
            {
                Session _generatorSession = new Session(_currIDataLayer);

                ApplicationSetup _appSetup = _generatorSession.FindObject<ApplicationSetup>(new GroupOperator(GroupOperatorType.And,
                                            new BinaryOperator("Active", true), new BinaryOperator("DefaultSystem", true)));
                if (_appSetup != null)
                {
                    NumberingHeader _numberingHeader = _generatorSession.FindObject<NumberingHeader>
                                                        (new GroupOperator(GroupOperatorType.And,
                                                         new BinaryOperator("NumberingType", NumberingType.Objects),
                                                         new BinaryOperator("ApplicationSetup", _appSetup, BinaryOperatorType.Equal),
                                                         new BinaryOperator("Active", true)));
                    if (_numberingHeader != null)
                    {
                        NumberingLine _numberingLine = _generatorSession.FindObject<NumberingLine>
                                                    (new GroupOperator(GroupOperatorType.And,
                                                     new BinaryOperator("ObjectList", _currObject),
                                                     new BinaryOperator("Sign", true),
                                                     new BinaryOperator("Active", true),
                                                     new BinaryOperator("NumberingHeader", _numberingHeader)));
                        if (_numberingLine != null)
                        {
                            _numberingLine.LastValue = _numberingLine.LastValue + _numberingLine.IncrementValue;
                            _numberingLine.Save();
                            _result = _numberingLine.FormatedValue;
                            _numberingLine.Session.CommitTransaction();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Module = GlobalFunction " + ex.ToString());
            }
            return _result;
        }

        public string GetNumberingSignUnlockOptimisticRecordBasedOrganization(IDataLayer _currIDataLayer, ObjectList _currObject, Company _currCompany, Workplace _currWorkplace)
        {
            string _result = null;
            try
            {
                string _locCompanyCode = null;
                string _locWorkplaceCode = null;

                Session _generatorSession = new Session(_currIDataLayer);

                if(_generatorSession != null && _currCompany != null && _currWorkplace != null)
                {
                    if (_currCompany.Code != null)
                    {
                        _locCompanyCode = _currCompany.Code;
                    }
                    if (_currWorkplace.Code != null)
                    {
                        _locWorkplaceCode = _currWorkplace.Code;
                    }

                    ApplicationSetup _appSetup = _generatorSession.FindObject<ApplicationSetup>(new GroupOperator(GroupOperatorType.And,
                                                new BinaryOperator("Company.Code", _locCompanyCode),
                                                new BinaryOperator("Active", true)));
                    if (_appSetup != null)
                    {
                        NumberingHeader _numberingHeader = _generatorSession.FindObject<NumberingHeader>
                                                            (new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("Company.Code", _locCompanyCode),
                                                            new BinaryOperator("Workplace.Code", _locWorkplaceCode),
                                                            new BinaryOperator("NumberingType", NumberingType.Objects),
                                                            new BinaryOperator("ApplicationSetup", _appSetup, BinaryOperatorType.Equal),
                                                            new BinaryOperator("Active", true)));
                        if (_numberingHeader != null)
                        {
                            NumberingLine _numberingLine = _generatorSession.FindObject<NumberingLine>
                                                        (new GroupOperator(GroupOperatorType.And,
                                                        new BinaryOperator("Company.Code", _locCompanyCode),
                                                        new BinaryOperator("Workplace.Code", _locWorkplaceCode),
                                                        new BinaryOperator("ObjectList", _currObject),
                                                        new BinaryOperator("Sign", true),
                                                        new BinaryOperator("Active", true),
                                                        new BinaryOperator("NumberingHeader", _numberingHeader)));
                            if (_numberingLine != null)
                            {
                                _numberingLine.LastValue = _numberingLine.LastValue + _numberingLine.IncrementValue;
                                _numberingLine.Save();
                                _result = _numberingLine.FormatedValue;
                                _numberingLine.Session.CommitTransaction();
                            }
                        }
                    }
                }

                
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Module = GlobalFunction " + ex.ToString());
            }
            return _result;
        }

        public string GetNumberingPICUnlockOptimisticRecord(IDataLayer _currIDataLayer, ObjectList _currObject, Employee _locPIC, PriceGroup _locPriceGroup)
        {
            string _result = null;
            
            try
            {

                Session _generatorSession = new Session(_currIDataLayer);

                if (_generatorSession != null)
                {

                    ApplicationSetup _appSetup = _generatorSession.FindObject<ApplicationSetup>(new GroupOperator(GroupOperatorType.And,
                                            new BinaryOperator("Active", true), new BinaryOperator("DefaultSystem", true)));

                    if (_appSetup != null)
                    {

                        NumberingHeader _numberingHeader = _generatorSession.FindObject<NumberingHeader>
                                                            (new GroupOperator(GroupOperatorType.And,
                                                             new BinaryOperator("NumberingType", NumberingType.Employee),
                                                             new BinaryOperator("ApplicationSetup", _appSetup, BinaryOperatorType.Equal),
                                                             new BinaryOperator("Active", true)));
                        if (_numberingHeader != null)
                        {
                            if(_locPIC != null && _locPriceGroup != null)
                            {
                                NumberingLine _numberingLine = _generatorSession.FindObject<NumberingLine>
                                                        (new GroupOperator(GroupOperatorType.And,
                                                         new BinaryOperator("ObjectList", _currObject),
                                                         new BinaryOperator("PIC", _locPIC),
                                                         new BinaryOperator("PriceGroup", _locPriceGroup),
                                                         new BinaryOperator("Active", true),
                                                         new BinaryOperator("NumberingHeader", _numberingHeader)));
                                if (_numberingLine != null)
                                {
                                    _numberingLine.LastValue = _numberingLine.LastValue + _numberingLine.IncrementValue;
                                    _numberingLine.Save();
                                    _result = _numberingLine.FormatedValue;
                                    _numberingLine.Session.CommitTransaction();
                                }
                            }
                            
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Module = GlobalFunction " + ex.ToString());
            }
            return _result;
        }

        public string GetNumberingPICUnlockOptimisticRecord(IDataLayer _currIDataLayer, ObjectList _currObject, Employee _locPIC, PriceGroup _locPriceGroup, Company _currCompany, Workplace _currWorkplace)
        {
            string _result = null;

            try
            {
                string _locCompanyCode = null;
                string _locWorkplaceCode = null;

                Session _generatorSession = new Session(_currIDataLayer);

                if(_generatorSession != null && _currCompany != null && _currWorkplace != null)
                {
                    if (_currCompany.Code != null)
                    {
                        _locCompanyCode = _currCompany.Code;
                    }
                    if (_currWorkplace.Code != null)
                    {
                        _locWorkplaceCode = _currWorkplace.Code;
                    }

                    if (_generatorSession != null)
                    {

                        ApplicationSetup _appSetup = _generatorSession.FindObject<ApplicationSetup>(new GroupOperator(GroupOperatorType.And,
                                                    new BinaryOperator("Company.Code", _locCompanyCode),
                                                    new BinaryOperator("Active", true)));

                        if (_appSetup != null)
                        {

                            NumberingHeader _numberingHeader = _generatorSession.FindObject<NumberingHeader>
                                                                (new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("Company.Code", _locCompanyCode),
                                                                new BinaryOperator("Workplace.Code", _locWorkplaceCode),
                                                                new BinaryOperator("NumberingType", NumberingType.Employee),
                                                                new BinaryOperator("ApplicationSetup", _appSetup, BinaryOperatorType.Equal),
                                                                new BinaryOperator("Active", true)));
                            if (_numberingHeader != null)
                            {
                                if (_locPIC != null && _locPriceGroup != null)
                                {
                                    NumberingLine _numberingLine = _generatorSession.FindObject<NumberingLine>
                                                            (new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("Company.Code", _locCompanyCode),
                                                            new BinaryOperator("Workplace.Code", _locWorkplaceCode),
                                                            new BinaryOperator("ObjectList", _currObject),
                                                            new BinaryOperator("PIC", _locPIC),
                                                            new BinaryOperator("PriceGroup", _locPriceGroup),
                                                            new BinaryOperator("Active", true),
                                                            new BinaryOperator("NumberingHeader", _numberingHeader)));
                                    if (_numberingLine != null)
                                    {
                                        _numberingLine.LastValue = _numberingLine.LastValue + _numberingLine.IncrementValue;
                                        _numberingLine.Save();
                                        _result = _numberingLine.FormatedValue;
                                        _numberingLine.Session.CommitTransaction();
                                    }
                                }

                            }
                        }
                    }
                }
                
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Module = GlobalFunction " + ex.ToString());
            }
            return _result;
        }

        #endregion Numbering

        #region GlobalFunction
        public static string Right(string value, int lenght)
        {
            return value.Substring(value.Length - lenght);
        }

        public static string Mid(string value, int val1, int val2)
        {
            return value.Substring(val1, val2);
        }

        public static bool IsDate(Object obj)
        {
            string strDate = obj.ToString();
            try
            {
                DateTime dt = DateTime.Parse(strDate);
                if ((dt.Month != System.DateTime.Now.Month) || (dt.Day < 1 && dt.Day > 31) || dt.Year != System.DateTime.Now.Year)
                    return false;
                else
                    return true;
            }
            catch
            {
                return false;
            }
        }

        protected bool CheckDate(String date)
        {
            try
            {
                DateTime dt = DateTime.Parse(date);
                return true;
            }
            catch
            {
                return false;
            }
        }
        #endregion GlobalFunction

        #region Import Data

        #region StandardImport
        public void ImportDataExcel(Session _currSession, string _fullPath, string _ext, ObjectList _objList)
        {
            PropertyInfo[] _propInfo = null;
            object _newObject = null;
            IExcelDataReader excelReader;
            DataSet ds = new DataSet();
            Boolean _dataBool;
            try
            {
                FileStream stream = File.Open(_fullPath, FileMode.Open, FileAccess.Read);
                if (_ext == ".xlsx")
                {
                    //1. Reading from a binary Excel file ('97-2003 format; *.xls)
                    excelReader = ExcelReaderFactory.CreateOpenXmlReader(stream);
                }
                else
                {
                    //2. Reading from a OpenXml Excel file (2007 format; *.xlsx)
                    excelReader = ExcelReaderFactory.CreateBinaryReader(stream);
                }

                excelReader.IsFirstRowAsColumnNames = true;
                ds = excelReader.AsDataSet();
                DataTable dt = ds.Tables[0];
                _propInfo = GetPropInfo(_objList);
                excelReader.Close();

                if (_propInfo != null)
                {
                    foreach (DataRow dRow in dt.Rows)
                    {
                        _newObject = GetBusinessObject(_currSession, _objList);
                        for (int i = 0; i <= dRow.ItemArray.Length - 1; i++)
                        {
                            for (int j = 0; j <= _propInfo.Length - 1; j++)
                            {
                                if (_propInfo[j].Name.ToLower().Replace(" ", "") == dt.Columns[i].ToString().Trim().ToLower().Replace(" ", ""))
                                {
                                    #region DefaultProperty

                                    if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(string)))
                                    {
                                        _propInfo[j].SetValue(_newObject, dRow[i].ToString(), null);
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(bool)))
                                    {
                                        if (dRow[i].ToString().Trim().ToLower() == "Checked".ToLower())
                                        {
                                            _dataBool = true;
                                        }
                                        else if (dRow[i].ToString().Trim().ToLower() == "TRUE".ToLower())
                                        {
                                            _dataBool = true;
                                        }
                                        else
                                        {
                                            _dataBool = false;
                                        }

                                        _propInfo[j].SetValue(_newObject, _dataBool, null);
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(decimal)))
                                    {
                                        _propInfo[j].SetValue(_newObject, Convert.ToDecimal(dRow[i].ToString()), null);
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(int)))
                                    {
                                        _propInfo[j].SetValue(_newObject, Convert.ToInt32(dRow[i].ToString()), null);
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(double)))
                                    {
                                        _propInfo[j].SetValue(_newObject, Convert.ToDouble(dRow[i].ToString()), null);
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(System.DateTime)))
                                    {
                                        if (CheckDate(dRow[i].ToString()))
                                        {
                                            _propInfo[j].SetValue(_newObject, Convert.ToDateTime(dRow[i].ToString()), null);
                                        }
                                    }

                                    #endregion DefaultProperty

                                    #region Import Excel For Enum
                                    //Import Excel For Enum

                                    #region A

                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(AccountCharge)))
                                    {
                                        if (dRow[i].ToString() != String.Empty)
                                        {
                                            _propInfo[j].SetValue(_newObject, GetAccountCharge(dRow[i].ToString()), null);
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(AccountMode)))
                                    {
                                        if (dRow[i].ToString() != String.Empty)
                                        {
                                            _propInfo[j].SetValue(_newObject, GetAccountMode(dRow[i].ToString()), null);
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(AccountType)))
                                    {
                                        if (dRow[i].ToString() != String.Empty)
                                        {
                                            _propInfo[j].SetValue(_newObject, GetAccountType(dRow[i].ToString()), null);
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(ApprovalFilter)))
                                    {
                                        if (dRow[i].ToString() != String.Empty)
                                        {
                                            _propInfo[j].SetValue(_newObject, GetApprovalFilter(dRow[i].ToString()), null);
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(ApprovalLevel)))
                                    {
                                        if (dRow[i].ToString() != String.Empty)
                                        {
                                            _propInfo[j].SetValue(_newObject, GetApprovalLevel(dRow[i].ToString()), null);
                                        }
                                    }

                                    #endregion A
                                    #region B

                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(BalanceType)))
                                    {
                                        if (dRow[i].ToString() != String.Empty)
                                        {
                                            _propInfo[j].SetValue(_newObject, GetBalanceType(dRow[i].ToString()), null);
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(BusinessPartnerType)))
                                    {
                                        if (dRow[i].ToString() != String.Empty)
                                        {
                                            _propInfo[j].SetValue(_newObject, GetBusinessPartnerType(dRow[i].ToString()), null);
                                        }
                                    }

                                    #endregion
                                    #region D

                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(DeliveryStatus)))
                                    {
                                        if (dRow[i].ToString() != String.Empty)
                                        {
                                            _propInfo[j].SetValue(_newObject, GetDeliveryStatus(dRow[i].ToString()), null);
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(DirectionType)))
                                    {
                                        if (dRow[i].ToString() != String.Empty)
                                        {
                                            _propInfo[j].SetValue(_newObject, GetDirectionType(dRow[i].ToString()), null);
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(DiscountMode)))
                                    {
                                        if (dRow[i].ToString() != String.Empty)
                                        {
                                            _propInfo[j].SetValue(_newObject, GetDiscountMode(dRow[i].ToString()), null);
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(DiscountType)))
                                    {
                                        if (dRow[i].ToString() != String.Empty)
                                        {
                                            _propInfo[j].SetValue(_newObject, GetDiscountType(dRow[i].ToString()), null);
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(DocumentRule)))
                                    {
                                        if (dRow[i].ToString() != String.Empty)
                                        {
                                            _propInfo[j].SetValue(_newObject, GetDocumentRule(dRow[i].ToString()), null);
                                        }
                                    }

                                    #endregion D
                                    #region F

                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(FieldName)))
                                    {
                                        if (dRow[i].ToString() != String.Empty)
                                        {
                                            _propInfo[j].SetValue(_newObject, GetFieldName(dRow[i].ToString()), null);
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(FinancialStatment)))
                                    {
                                        if (dRow[i].ToString() != String.Empty)
                                        {
                                            _propInfo[j].SetValue(_newObject, GetFinancialStatment(dRow[i].ToString()), null);
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(FunctionList)))
                                    {
                                        if (dRow[i].ToString() != String.Empty)
                                        {
                                            _propInfo[j].SetValue(_newObject, GetFunctionList(dRow[i].ToString()), null);
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(FreeItemType1)))
                                    {
                                        if (dRow[i].ToString() != String.Empty)
                                        {
                                            _propInfo[j].SetValue(_newObject, GetFreeItemType1(dRow[i].ToString()), null);
                                        }
                                    }
                                    
                                    #endregion F
                                    #region I

                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(InventoryMovingType)))
                                    {
                                        if (dRow[i].ToString() != String.Empty)
                                        {
                                            _propInfo[j].SetValue(_newObject, GetInventoryMovingType(dRow[i].ToString()), null);
                                        }
                                    }

                                    #endregion I
                                    #region L

                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(LocationType)))
                                    {
                                        if (dRow[i].ToString() != String.Empty)
                                        {
                                            _propInfo[j].SetValue(_newObject, GetLocationType(dRow[i].ToString()), null);
                                        }
                                    }

                                    #endregion L
                                    #region N

                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(NumberingType)))
                                    {
                                        if (dRow[i].ToString() != String.Empty)
                                        {
                                            _propInfo[j].SetValue(_newObject, GetNumberingType(dRow[i].ToString()), null);
                                        }
                                    }

                                    #endregion N
                                    #region O

                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(ObjectList)))
                                    {
                                        if (dRow[i].ToString() != String.Empty)
                                        {
                                            _propInfo[j].SetValue(_newObject, GetObjectList(dRow[i].ToString()), null);
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(OrderType)))
                                    {
                                        if (dRow[i].ToString() != String.Empty)
                                        {
                                            _propInfo[j].SetValue(_newObject, GetOrderType(dRow[i].ToString()), null);
                                        }
                                    }

                                    #endregion O
                                    #region P

                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(PageType)))
                                    {
                                        if (dRow[i].ToString() != String.Empty)
                                        {
                                            _propInfo[j].SetValue(_newObject, GetPageType(dRow[i].ToString()), null);
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(PaymentMethodType)))
                                    {
                                        if (dRow[i].ToString() != String.Empty)
                                        {
                                            _propInfo[j].SetValue(_newObject, GetPaymentMethodType(dRow[i].ToString()), null);
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(PaymentType)))
                                    {
                                        if (dRow[i].ToString() != String.Empty)
                                        {
                                            _propInfo[j].SetValue(_newObject, GetPaymentType(dRow[i].ToString()), null);
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(PostingMethod)))
                                    {
                                        if (dRow[i].ToString() != String.Empty)
                                        {
                                            _propInfo[j].SetValue(_newObject, GetPostingMethod(dRow[i].ToString()), null);
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(PostingMethodType)))
                                    {
                                        if (dRow[i].ToString() != String.Empty)
                                        {
                                            _propInfo[j].SetValue(_newObject, GetPostingMethodType(dRow[i].ToString()), null);
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(PostingType)))
                                    {
                                        if (dRow[i].ToString() != String.Empty)
                                        {
                                            _propInfo[j].SetValue(_newObject, GetPostingType(dRow[i].ToString()), null);
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(PriceType)))
                                    {
                                        if (dRow[i].ToString() != String.Empty)
                                        {
                                            _propInfo[j].SetValue(_newObject, GetPriceType(dRow[i].ToString()), null);
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(PriorityType)))
                                    {
                                        if (dRow[i].ToString() != String.Empty)
                                        {
                                            _propInfo[j].SetValue(_newObject, GetPriorityType(dRow[i].ToString()), null);
                                        }
                                    }

                                    #endregion P
                                    #region R

                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(RuleType)))
                                    {
                                        if (dRow[i].ToString() != String.Empty)
                                        {
                                            _propInfo[j].SetValue(_newObject, GetRuleType(dRow[i].ToString()), null);
                                        }
                                    }

                                    #endregion R
                                    #region S

                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(SalesRole)))
                                    {
                                        if (dRow[i].ToString() != String.Empty)
                                        {
                                            _propInfo[j].SetValue(_newObject, GetSalesRole(dRow[i].ToString()), null);
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(SetupList)))
                                    {
                                        if (dRow[i].ToString() != String.Empty)
                                        {
                                            _propInfo[j].SetValue(_newObject, GetSetupList(dRow[i].ToString()), null);
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(Status)))
                                    {
                                        if (dRow[i].ToString() != String.Empty)
                                        {
                                            _propInfo[j].SetValue(_newObject, GetStatus(dRow[i].ToString()), null);
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(StockGroup)))
                                    {
                                        if (dRow[i].ToString() != String.Empty)
                                        {
                                            _propInfo[j].SetValue(_newObject, GetStockGroup(dRow[i].ToString()), null);
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(StockType)))
                                    {
                                        if (dRow[i].ToString() != String.Empty)
                                        {
                                            _propInfo[j].SetValue(_newObject, GetStockType(dRow[i].ToString()), null);
                                        }
                                    }

                                    #endregion S
                                    #region T

                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(TaxCalculationMethod)))
                                    {
                                        if (dRow[i].ToString() != String.Empty)
                                        {
                                            _propInfo[j].SetValue(_newObject, GetTaxCalculationMethod(dRow[i].ToString()), null);
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(TaxMode)))
                                    {
                                        if (dRow[i].ToString() != String.Empty)
                                        {
                                            _propInfo[j].SetValue(_newObject, GetTaxMode(dRow[i].ToString()), null);
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(TaxNature)))
                                    {
                                        if (dRow[i].ToString() != String.Empty)
                                        {
                                            _propInfo[j].SetValue(_newObject, GetTaxNature(dRow[i].ToString()), null);
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(TaxRule)))
                                    {
                                        if (dRow[i].ToString() != String.Empty)
                                        {
                                            _propInfo[j].SetValue(_newObject, GetTaxRule(dRow[i].ToString()), null);
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(TransactionTerm)))
                                    {
                                        if (dRow[i].ToString() != String.Empty)
                                        {
                                            _propInfo[j].SetValue(_newObject, GetTransactionTerm(dRow[i].ToString()), null);
                                        }
                                    }

                                    #endregion T
                                    #region V

                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(VehicleMode)))
                                    {
                                        if (dRow[i].ToString() != String.Empty)
                                        {
                                            _propInfo[j].SetValue(_newObject, GetVehicleMode(dRow[i].ToString()), null);
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(VehicleType)))
                                    {
                                        if (dRow[i].ToString() != String.Empty)
                                        {
                                            _propInfo[j].SetValue(_newObject, GetVehicleType(dRow[i].ToString()), null);
                                        }
                                    }

                                    #endregion V
                                    
                                    #endregion Import Excel For Enum

                                    #region Import Excel For Object

                                    #region A

                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(AccountGroup)))
                                    {
                                        if (dRow[i].ToString() != null)
                                        {
                                            AccountGroup _lineData = _currSession.FindObject<AccountGroup>
                                                (new BinaryOperator("Name", dRow[i].ToString()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(AccountMap)))
                                    {
                                        if (dRow[i].ToString() != null)
                                        {
                                            AccountMap _lineData = _currSession.FindObject<AccountMap>
                                                (new BinaryOperator("Name", dRow[i].ToString()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(AccountMapLine)))
                                    {
                                        if (dRow[i].ToString() != null)
                                        {
                                            AccountMapLine _lineData = _currSession.FindObject<AccountMapLine>
                                                (new BinaryOperator("Name", dRow[i].ToString()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(Address)))
                                    {
                                        if (dRow[i].ToString() != null)
                                        {
                                            Address _lineData = _currSession.FindObject<Address>
                                                (new BinaryOperator("Street", dRow[i].ToString()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(AdvanceAccountGroup)))
                                    {
                                        if (dRow[i].ToString() != null)
                                        {
                                            AdvanceAccountGroup _lineData = _currSession.FindObject<AdvanceAccountGroup>
                                                (new BinaryOperator("Name", dRow[i].ToString()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(ApplicationSetup)))
                                    {
                                        if (dRow[i].ToString() != null)
                                        {
                                            ApplicationSetup _lineData = _currSession.FindObject<ApplicationSetup>
                                                (new BinaryOperator("SetupName", dRow[i].ToString()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(ApplicationSetupDetail)))
                                    {
                                        if (dRow[i].ToString() != null)
                                        {
                                            ApplicationSetupDetail _lineData = _currSession.FindObject<ApplicationSetupDetail>
                                                (new BinaryOperator("Name", dRow[i].ToString()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(Area)))
                                    {
                                        if (dRow[i].ToString() != null)
                                        {
                                            Area _lineData = _currSession.FindObject<Area>
                                                (new BinaryOperator("Name", dRow[i].ToString()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }

                                    #endregion A
                                    #region B

                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(BankAccount)))
                                    {
                                        if (dRow[i].ToString() != null)
                                        {
                                            BankAccount _lineData = _currSession.FindObject<BankAccount>
                                                (new BinaryOperator("FullName", dRow[i].ToString()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(BankAccountGroup)))
                                    {
                                        if (dRow[i].ToString() != null)
                                        {
                                            BankAccountGroup _lineData = _currSession.FindObject<BankAccountGroup>
                                                (new BinaryOperator("Name", dRow[i].ToString()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(BeginningInventory)))
                                    {
                                        if (dRow[i].ToString() != null || !string.IsNullOrEmpty(dRow[i].ToString()))
                                        {
                                            BeginningInventory _lineData = _currSession.FindObject<BeginningInventory>
                                                (new BinaryOperator("Code", dRow[i].ToString().Trim()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(BeginningInventoryLine)))
                                    {
                                        if (dRow[i].ToString() != null || !string.IsNullOrEmpty(dRow[i].ToString()))
                                        {
                                            BeginningInventoryLine _lineData = _currSession.FindObject<BeginningInventoryLine>
                                                (new BinaryOperator("Code", dRow[i].ToString().Trim()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(BinLocation)))
                                    {
                                        if (dRow[i].ToString() != null)
                                        {
                                            BinLocation _lineData = _currSession.FindObject<BinLocation>
                                                (new BinaryOperator("Name", dRow[i].ToString()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(Brand)))
                                    {
                                        if (dRow[i].ToString() != null)
                                        {
                                            Brand _lineData = _currSession.FindObject<Brand>
                                                (new BinaryOperator("Name", dRow[i].ToString()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(BusinessPartner)))
                                    {
                                        if (dRow[i].ToString() != null)
                                        {
                                            BusinessPartner _lineData = _currSession.FindObject<BusinessPartner>
                                                (new BinaryOperator("Name", dRow[i].ToString()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(BusinessPartnerAccountGroup)))
                                    {
                                        if (dRow[i].ToString() != null)
                                        {
                                            BusinessPartnerAccountGroup _lineData = _currSession.FindObject<BusinessPartnerAccountGroup>
                                                (new BinaryOperator("Name", dRow[i].ToString()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(BusinessPartnerGroup)))
                                    {
                                        if (dRow[i].ToString() != null)
                                        {
                                            BusinessPartnerGroup _lineData = _currSession.FindObject<BusinessPartnerGroup>
                                                (new BinaryOperator("Name", dRow[i].ToString()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }

                                    #endregion B
                                    #region C

                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(CashAdvanceType)))
                                    {
                                        if (dRow[i].ToString() != null)
                                        {
                                            CashAdvanceType _lineData = _currSession.FindObject<CashAdvanceType>
                                                (new BinaryOperator("Name", dRow[i].ToString()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(ChartOfAccount)))
                                    {
                                        if (dRow[i].ToString() != null)
                                        {
                                            ChartOfAccount _lineData = _currSession.FindObject<ChartOfAccount>
                                                (new BinaryOperator("Name", dRow[i].ToString()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(City)))
                                    {
                                        if (dRow[i].ToString() != null)
                                        {
                                            City _lineData = _currSession.FindObject<City>
                                                (new BinaryOperator("Name", dRow[i].ToString()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(Company)))
                                    {
                                        if (dRow[i].ToString() != null)
                                        {
                                            Company _lineData = _currSession.FindObject<Company>
                                                (new BinaryOperator("Name", dRow[i].ToString()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(CompanyAccountGroup)))
                                    {
                                        if (dRow[i].ToString() != null)
                                        {
                                            CompanyAccountGroup _lineData = _currSession.FindObject<CompanyAccountGroup>
                                                (new BinaryOperator("Name", dRow[i].ToString()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(Country)))
                                    {
                                        if (dRow[i].ToString() != null)
                                        {
                                            Country _lineData = _currSession.FindObject<Country>
                                                (new BinaryOperator("Name", dRow[i].ToString()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(Currency)))
                                    {
                                        if (dRow[i].ToString() != null)
                                        {
                                            Currency _lineData = _currSession.FindObject<Currency>
                                                (new BinaryOperator("ShortName", dRow[i].ToString()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(CurrencyRate)))
                                    {
                                        if (dRow[i].ToString() != null)
                                        {
                                            CurrencyRate _lineData = _currSession.FindObject<CurrencyRate>
                                                (new BinaryOperator("Code", dRow[i].ToString()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(CustomerMap)))
                                    {
                                        if (dRow[i].ToString() != null)
                                        {
                                            CustomerMap _lineData = _currSession.FindObject<CustomerMap>
                                                (new BinaryOperator("Name", dRow[i].ToString()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }

                                    #endregion C
                                    #region D

                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(Department)))
                                    {
                                        if (dRow[i].ToString() != null)
                                        {
                                            Department _lineData = _currSession.FindObject<Department>
                                                (new BinaryOperator("Name", dRow[i].ToString()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(Discount)))
                                    {
                                        if (dRow[i].ToString() != null || !string.IsNullOrEmpty(dRow[i].ToString()))
                                        {
                                            Discount _lineData = _currSession.FindObject<Discount>
                                                (new BinaryOperator("Name", dRow[i].ToString().Trim()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(DiscountAccountGroup)))
                                    {
                                        if (dRow[i].ToString() != null || !string.IsNullOrEmpty(dRow[i].ToString()))
                                        {
                                            DiscountAccountGroup _lineData = _currSession.FindObject<DiscountAccountGroup>
                                                (new BinaryOperator("Name", dRow[i].ToString().Trim()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(DiscountRule)))
                                    {
                                        if (dRow[i].ToString() != null || !string.IsNullOrEmpty(dRow[i].ToString()))
                                        {
                                            DiscountRule _lineData = _currSession.FindObject<DiscountRule>
                                                (new BinaryOperator("Code", dRow[i].ToString().Trim()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(DiscountRuleType)))
                                    {
                                        if (dRow[i].ToString() != null || !string.IsNullOrEmpty(dRow[i].ToString()))
                                        {
                                            DiscountRuleType _lineData = _currSession.FindObject<DiscountRuleType>
                                                (new BinaryOperator("Code", dRow[i].ToString().Trim()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(Division)))
                                    {
                                        if (dRow[i].ToString() != null)
                                        {
                                            Division _lineData = _currSession.FindObject<Division>
                                                (new BinaryOperator("Name", dRow[i].ToString()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(DocumentType)))
                                    {
                                        if (dRow[i].ToString() != null)
                                        {
                                            DocumentType _lineData = _currSession.FindObject<DocumentType>
                                                (new BinaryOperator("FullName", dRow[i].ToString()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }

                                    #endregion D
                                    #region E

                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(Employee)))
                                    {
                                        if (dRow[i].ToString() != null)
                                        {
                                            Employee _lineData = _currSession.FindObject<Employee>
                                                (new BinaryOperator("Name", dRow[i].ToString()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }

                                    #endregion E
                                    #region F

                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(FreeItemRule)))
                                    {
                                        if (dRow[i].ToString() != null)
                                        {
                                            FreeItemRule _lineData = _currSession.FindObject<FreeItemRule>
                                                (new BinaryOperator("Code", dRow[i].ToString()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(FreeItemRuleType)))
                                    {
                                        if (dRow[i].ToString() != null)
                                        {
                                            FreeItemRuleType _lineData = _currSession.FindObject<FreeItemRuleType>
                                                (new BinaryOperator("Code", dRow[i].ToString()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }

                                    #endregion F
                                    #region I

                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(IndustrialType)))
                                    {
                                        if (dRow[i].ToString() != null)
                                        {
                                            IndustrialType _lineData = _currSession.FindObject<IndustrialType>
                                                (new BinaryOperator("Name", dRow[i].ToString()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(Item)))
                                    {
                                        if (dRow[i].ToString() != null)
                                        {
                                            Item _lineData = _currSession.FindObject<Item>
                                                (new BinaryOperator("Name", dRow[i].ToString()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(ItemAccountGroup)))
                                    {
                                        if (dRow[i].ToString() != null)
                                        {
                                            ItemAccountGroup _lineData = _currSession.FindObject<ItemAccountGroup>
                                                (new BinaryOperator("Name", dRow[i].ToString()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(ItemGroup)))
                                    {
                                        if (dRow[i].ToString() != null)
                                        {
                                            ItemGroup _lineData = _currSession.FindObject<ItemGroup>
                                                (new BinaryOperator("Name", dRow[i].ToString()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(ItemUnitOfMeasure)))
                                    {
                                        if (dRow[i].ToString() != null)
                                        {
                                            ItemUnitOfMeasure _lineData = _currSession.FindObject<ItemUnitOfMeasure>
                                                (new BinaryOperator("Code", dRow[i].ToString()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }

                                    #endregion I
                                    #region J

                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(JournalMap)))
                                    {
                                        if (dRow[i].ToString() != null)
                                        {
                                            JournalMap _lineData = _currSession.FindObject<JournalMap>
                                                (new BinaryOperator("Name", dRow[i].ToString()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }

                                    #endregion J
                                    #region L

                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(Location)))
                                    {
                                        if (dRow[i].ToString() != null)
                                        {
                                            Location _lineData = _currSession.FindObject<Location>
                                                (new BinaryOperator("Name", dRow[i].ToString()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }

                                    #endregion L
                                    #region M

                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(MailSetupDetail)))
                                    {
                                        if (dRow[i].ToString() != null)
                                        {
                                            MailSetupDetail _lineData = _currSession.FindObject<MailSetupDetail>
                                                (new BinaryOperator("Code", dRow[i].ToString()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }

                                    #endregion M
                                    #region N

                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(NumberingHeader)))
                                    {
                                        if (dRow[i].ToString() != null)
                                        {
                                            NumberingHeader _lineData = _currSession.FindObject<NumberingHeader>
                                                (new BinaryOperator("Name", dRow[i].ToString()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }

                                    #endregion N
                                    #region O

                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(OrganizationAccountGroup)))
                                    {
                                        if (dRow[i].ToString() != null)
                                        {
                                            OrganizationAccountGroup _lineData = _currSession.FindObject<OrganizationAccountGroup>
                                                (new BinaryOperator("Code", dRow[i].ToString()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(OrganizationSetupDetail)))
                                    {
                                        if (dRow[i].ToString() != null)
                                        {
                                            OrganizationSetupDetail _lineData = _currSession.FindObject<OrganizationSetupDetail>
                                                (new BinaryOperator("Code", dRow[i].ToString()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }

                                    #endregion O
                                    #region P

                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(PaymentMethod)))
                                    {
                                        if (dRow[i].ToString() != null)
                                        {
                                            PaymentMethod _lineData = _currSession.FindObject<PaymentMethod>
                                                (new BinaryOperator("Name", dRow[i].ToString()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(PhoneNumber)))
                                    {
                                        if (dRow[i].ToString() != null)
                                        {
                                            PhoneNumber _lineData = _currSession.FindObject<PhoneNumber>
                                                (new BinaryOperator("Number", dRow[i].ToString()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(PhoneType)))
                                    {
                                        if (dRow[i].ToString() != null)
                                        {
                                            PhoneType _lineData = _currSession.FindObject<PhoneType>
                                                (new BinaryOperator("Name", dRow[i].ToString()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(Position)))
                                    {
                                        if (dRow[i].ToString() != null)
                                        {
                                            Position _lineData = _currSession.FindObject<Position>
                                                (new BinaryOperator("Name", dRow[i].ToString()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(Price)))
                                    {
                                        if (dRow[i].ToString() != null)
                                        {
                                            Price _lineData = _currSession.FindObject<Price>
                                                (new BinaryOperator("Name", dRow[i].ToString()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(PriceGroup)))
                                    {
                                        if (dRow[i].ToString() != null || !string.IsNullOrEmpty(dRow[i].ToString()))
                                        {
                                            PriceGroup _lineData = _currSession.FindObject<PriceGroup>
                                                (new BinaryOperator("Name", dRow[i].ToString().Trim()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(PriceLine)))
                                    {
                                        if (dRow[i].ToString() != null)
                                        {
                                            PriceLine _lineData = _currSession.FindObject<PriceLine>
                                                (new BinaryOperator("Name", dRow[i].ToString()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(PurchaseSetupDetail)))
                                    {
                                        if (dRow[i].ToString() != null || !string.IsNullOrEmpty(dRow[i].ToString()))
                                        {
                                            PurchaseSetupDetail _lineData = _currSession.FindObject<PurchaseSetupDetail>
                                                (new BinaryOperator("Code", dRow[i].ToString().Trim()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(RoundingSetupDetail)))
                                    {
                                        if (dRow[i].ToString() != null)
                                        {
                                            RoundingSetupDetail _lineData = _currSession.FindObject<RoundingSetupDetail>
                                                (new BinaryOperator("Code", dRow[i].ToString()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }

                                    #endregion P
                                    #region R

                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(Route)))
                                    {
                                        if (dRow[i].ToString() != null)
                                        {
                                            Route _lineData = _currSession.FindObject<Route>
                                                (new BinaryOperator("Subject", dRow[i].ToString()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(RoutePlan)))
                                    {
                                        if (dRow[i].ToString() != null)
                                        {
                                            RoutePlan _lineData = _currSession.FindObject<RoutePlan>
                                                (new BinaryOperator("Code", dRow[i].ToString()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }

                                    #endregion R
                                    #region S

                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(SalesArea)))
                                    {
                                        if (dRow[i].ToString() != null)
                                        {
                                            SalesArea _lineData = _currSession.FindObject<SalesArea>
                                                (new BinaryOperator("Name", dRow[i].ToString()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(SalesArea1)))
                                    {
                                        if (dRow[i].ToString() != null)
                                        {
                                            SalesArea1 _lineData = _currSession.FindObject<SalesArea1>
                                                (new BinaryOperator("Name", dRow[i].ToString()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(SalesArea2)))
                                    {
                                        if (dRow[i].ToString() != null)
                                        {
                                            SalesArea2 _lineData = _currSession.FindObject<SalesArea2>
                                                (new BinaryOperator("Name", dRow[i].ToString()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(SalesArea3)))
                                    {
                                        if (dRow[i].ToString() != null)
                                        {
                                            SalesArea3 _lineData = _currSession.FindObject<SalesArea3>
                                                (new BinaryOperator("Name", dRow[i].ToString()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(SalesArea4)))
                                    {
                                        if (dRow[i].ToString() != null)
                                        {
                                            SalesArea4 _lineData = _currSession.FindObject<SalesArea4>
                                                (new BinaryOperator("Name", dRow[i].ToString()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(SalesAreaLine)))
                                    {
                                        if (dRow[i].ToString() != null)
                                        {
                                            SalesAreaLine _lineData = _currSession.FindObject<SalesAreaLine>
                                                (new BinaryOperator("Name", dRow[i].ToString()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(SalesmanAreaSetup)))
                                    {
                                        if (dRow[i].ToString() != null)
                                        {
                                            SalesmanAreaSetup _lineData = _currSession.FindObject<SalesmanAreaSetup>
                                                (new BinaryOperator("Name", dRow[i].ToString()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(SalesmanCustomerSetup)))
                                    {
                                        if (dRow[i].ToString() != null)
                                        {
                                            SalesmanCustomerSetup _lineData = _currSession.FindObject<SalesmanCustomerSetup>
                                                (new BinaryOperator("Name", dRow[i].ToString()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(SalesmanVehicleSetup)))
                                    {
                                        if (dRow[i].ToString() != null)
                                        {
                                            SalesmanVehicleSetup _lineData = _currSession.FindObject<SalesmanVehicleSetup>
                                                (new BinaryOperator("Name", dRow[i].ToString()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(SalesPromotionRule)))
                                    {
                                        if (dRow[i].ToString() != null)
                                        {
                                            SalesPromotionRule _lineData = _currSession.FindObject<SalesPromotionRule>
                                                (new BinaryOperator("Code", dRow[i].ToString()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(ScheduleType)))
                                    {
                                        if (dRow[i].ToString() != null)
                                        {
                                            ScheduleType _lineData = _currSession.FindObject<ScheduleType>
                                                (new BinaryOperator("Code", dRow[i].ToString()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(Section)))
                                    {
                                        if (dRow[i].ToString() != null)
                                        {
                                            Section _lineData = _currSession.FindObject<Section>
                                                (new BinaryOperator("Name", dRow[i].ToString()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }

                                    #endregion S
                                    #region T

                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(Tax)))
                                    {
                                        if (dRow[i].ToString() != null)
                                        {
                                            Tax _lineData = _currSession.FindObject<Tax>
                                                (new BinaryOperator("Code", dRow[i].ToString()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(TaxAccountGroup)))
                                    {
                                        if (dRow[i].ToString() != null)
                                        {
                                            TaxAccountGroup _lineData = _currSession.FindObject<TaxAccountGroup>
                                                (new BinaryOperator("Name", dRow[i].ToString()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(TaxGroup)))
                                    {
                                        if (dRow[i].ToString() != null)
                                        {
                                            TaxGroup _lineData = _currSession.FindObject<TaxGroup>
                                                (new BinaryOperator("Name", dRow[i].ToString()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(TaxType)))
                                    {
                                        if (dRow[i].ToString() != null)
                                        {
                                            TaxType _lineData = _currSession.FindObject<TaxType>
                                                (new BinaryOperator("Code", dRow[i].ToString()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(TermOfPayment)))
                                    {
                                        if (dRow[i].ToString() != null)
                                        {
                                            TermOfPayment _lineData = _currSession.FindObject<TermOfPayment>
                                                (new BinaryOperator("Name", dRow[i].ToString()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(TerritoryLevel1)))
                                    {
                                        if (dRow[i].ToString() != null)
                                        {
                                            TerritoryLevel1 _lineData = _currSession.FindObject<TerritoryLevel1>
                                                (new BinaryOperator("Name", dRow[i].ToString()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(TerritoryLevel2)))
                                    {
                                        if (dRow[i].ToString() != null)
                                        {
                                            TerritoryLevel2 _lineData = _currSession.FindObject<TerritoryLevel2>
                                                (new BinaryOperator("Name", dRow[i].ToString()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(TerritoryLevel3)))
                                    {
                                        if (dRow[i].ToString() != null)
                                        {
                                            TerritoryLevel3 _lineData = _currSession.FindObject<TerritoryLevel3>
                                                (new BinaryOperator("Name", dRow[i].ToString()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(TerritoryLevel4)))
                                    {
                                        if (dRow[i].ToString() != null)
                                        {
                                            TerritoryLevel4 _lineData = _currSession.FindObject<TerritoryLevel4>
                                                (new BinaryOperator("Name", dRow[i].ToString()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }

                                    #endregion T
                                    #region U

                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(UnitOfMeasure)))
                                    {
                                        if (dRow[i].ToString() != null)
                                        {
                                            UnitOfMeasure _lineData = _currSession.FindObject<UnitOfMeasure>
                                                (new BinaryOperator("FullName", dRow[i].ToString()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(UnitOfMeasureType)))
                                    {
                                        if (dRow[i].ToString() != null)
                                        {
                                            UnitOfMeasureType _lineData = _currSession.FindObject<UnitOfMeasureType>
                                                (new BinaryOperator("Name", dRow[i].ToString()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(UserAccess)))
                                    {
                                        if (dRow[i].ToString() != null || !string.IsNullOrEmpty(dRow[i].ToString()))
                                        {
                                            UserAccess _lineData = _currSession.FindObject<UserAccess>
                                                (new BinaryOperator("UserName", dRow[i].ToString().Trim()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }

                                    #endregion U
                                    #region V

                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(Vehicle)))
                                    {
                                        if (dRow[i].ToString() != null || !string.IsNullOrEmpty(dRow[i].ToString()))
                                        {
                                            Vehicle _lineData = _currSession.FindObject<Vehicle>
                                                (new BinaryOperator("Name", dRow[i].ToString().Trim()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(VehicleList)))
                                    {
                                        if (dRow[i].ToString() != null || !string.IsNullOrEmpty(dRow[i].ToString()))
                                        {
                                            VehicleList _lineData = _currSession.FindObject<VehicleList>
                                                (new BinaryOperator("Name", dRow[i].ToString().Trim()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }

                                    #endregion V
                                    #region W

                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(WarehouseSetupDetail)))
                                    {
                                        if (dRow[i].ToString() != null)
                                        {
                                            WarehouseSetupDetail _lineData = _currSession.FindObject<WarehouseSetupDetail>
                                                (new BinaryOperator("Code", dRow[i].ToString()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(Workplace)))
                                    {
                                        if (dRow[i].ToString() != null)
                                        {
                                            Workplace _lineData = _currSession.FindObject<Workplace>
                                                (new BinaryOperator("Name", dRow[i].ToString()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }

                                    #endregion W

                                    #endregion Import Excel For Object
                                }
                            }
                        }
                        _currSession.Save(_newObject);
                        _currSession.CommitTransaction();
                    }
                    //_currSession.CommitTransaction();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Module = GlobalFunction " + ex.ToString());
            }
        }

        private PropertyInfo[] GetPropInfo(ObjectList _objList)
        {
            PropertyInfo[] _result = null;
            try
            {
                #region PropertyInfo A
                if (_objList == ObjectList.AccountGroup)
                {
                    _result = typeof(AccountGroup).GetProperties();
                }
                else if (_objList == ObjectList.AccountingPeriodic)
                {
                    _result = typeof(AccountingPeriodic).GetProperties();
                }
                else if (_objList == ObjectList.AccountingPeriodicLine)
                {
                    _result = typeof(AccountingPeriodicLine).GetProperties();
                }
                else if (_objList == ObjectList.AccountMap)
                {
                    _result = typeof(AccountMap).GetProperties();
                }
                else if (_objList == ObjectList.AccountMapLine)
                {
                    _result = typeof(AccountMapLine).GetProperties();
                }
                else if (_objList == ObjectList.AccountReclassJournal)
                {
                    _result = typeof(AccountReclassJournal).GetProperties();
                }
                else if (_objList == ObjectList.AccountReclassJournalLine)
                {
                    _result = typeof(AccountReclassJournalLine).GetProperties();
                }
                else if (_objList == ObjectList.AccountReclassJournalMonitoring)
                {
                    _result = typeof(AccountReclassJournalMonitoring).GetProperties();
                }
                else if (_objList == ObjectList.Address)
                {
                    _result = typeof(Address).GetProperties();
                }
                else if (_objList == ObjectList.AdvanceAccountGroup)
                {
                    _result = typeof(AdvanceAccountGroup).GetProperties();
                }
                else if (_objList == ObjectList.AmendDelivery)
                {
                    _result = typeof(AmendDelivery).GetProperties();
                }
                else if (_objList == ObjectList.AmendDeliveryLine)
                {
                    _result = typeof(AmendDeliveryLine).GetProperties();
                }
                else if (_objList == ObjectList.AmendDeliveryMonitoring)
                {
                    _result = typeof(AmendDeliveryMonitoring).GetProperties();
                }
                else if (_objList == ObjectList.ApplicationImport)
                {
                    _result = typeof(ApplicationImport).GetProperties();
                }
                else if (_objList == ObjectList.ApplicationSetup)
                {
                    _result = typeof(ApplicationSetup).GetProperties();
                }
                else if (_objList == ObjectList.ApplicationSetupDetail)
                {
                    _result = typeof(ApplicationSetupDetail).GetProperties();
                }
                else if (_objList == ObjectList.ApprovalLine)
                {
                    _result = typeof(ApprovalLine).GetProperties();
                }
                else if (_objList == ObjectList.Area)
                {
                    _result = typeof(Area).GetProperties();
                }
                #endregion PropertyInfo A
                #region PropertyInfo B
                else if (_objList == ObjectList.BankAccount)
                {
                    _result = typeof(BankAccount).GetProperties();
                }
                else if (_objList == ObjectList.BankAccountGroup)
                {
                    _result = typeof(BankAccountGroup).GetProperties();
                }
                else if (_objList == ObjectList.BeginningInventory)
                {
                    _result = typeof(BeginningInventory).GetProperties();
                }
                else if (_objList == ObjectList.BeginningInventoryLine)
                {
                    _result = typeof(BeginningInventoryLine).GetProperties();
                }
                else if (_objList == ObjectList.BinLocation)
                {
                    _result = typeof(BinLocation).GetProperties();
                }
                else if (_objList == ObjectList.BinLocationSetupDetail)
                {
                    _result = typeof(BinLocationSetupDetail).GetProperties();
                }
                else if (_objList == ObjectList.Brand)
                {
                    _result = typeof(Brand).GetProperties();
                }
                else if (_objList == ObjectList.BusinessPartner)
                {
                    _result = typeof(BusinessPartner).GetProperties();
                }
                else if (_objList == ObjectList.BusinessPartnerAccountGroup)
                {
                    _result = typeof(BusinessPartnerAccountGroup).GetProperties();
                }
                else if (_objList == ObjectList.BusinessPartnerGroup)
                {
                    _result = typeof(BusinessPartnerGroup).GetProperties();
                }
                else if (_objList == ObjectList.BusinessPartnerPriceGroupSetup)
                {
                    _result = typeof(BusinessPartnerPriceGroupSetup).GetProperties();
                }
                #endregion PropertyInfo B
                #region PropertyInfo C
                else if (_objList == ObjectList.CanvasPaymentIn)
                {
                    _result = typeof(CanvasPaymentIn).GetProperties();
                }
                else if (_objList == ObjectList.CanvassingCollection)
                {
                    _result = typeof(CanvassingCollection).GetProperties();
                }
                else if (_objList == ObjectList.CanvassingCollectionFreeItem)
                {
                    _result = typeof(CanvassingCollectionFreeItem).GetProperties();
                }
                else if (_objList == ObjectList.CanvassingCollectionLine)
                {
                    _result = typeof(CanvassingCollectionLine).GetProperties();
                }
                else if (_objList == ObjectList.CanvassingCollectionMonitoring)
                {
                    _result = typeof(CanvassingCollectionMonitoring).GetProperties();
                }
                else if (_objList == ObjectList.CashAdvance)
                {
                    _result = typeof(CashAdvance).GetProperties();
                }
                else if (_objList == ObjectList.CashAdvanceCollection)
                {
                    _result = typeof(CashAdvanceCollection).GetProperties();
                }
                else if (_objList == ObjectList.CashAdvanceLine)
                {
                    _result = typeof(CashAdvanceLine).GetProperties();
                }
                else if (_objList == ObjectList.CashAdvanceMonitoring)
                {
                    _result = typeof(CashAdvanceMonitoring).GetProperties();
                }
                else if (_objList == ObjectList.CashAdvancePaymentPlan)
                {
                    _result = typeof(CashAdvancePaymentPlan).GetProperties();
                }
                else if (_objList == ObjectList.CashAdvanceType)
                {
                    _result = typeof(CashAdvanceType).GetProperties();
                }
                else if (_objList == ObjectList.ChartOfAccount)
                {
                    _result = typeof(ChartOfAccount).GetProperties();
                }
                else if (_objList == ObjectList.City)
                {
                    _result = typeof(City).GetProperties();
                }
                else if (_objList == ObjectList.Company)
                {
                    _result = typeof(Company).GetProperties();
                }
                else if (_objList == ObjectList.CompanyAccount)
                {
                    _result = typeof(CompanyAccount).GetProperties();
                }
                else if (_objList == ObjectList.CompanyAccountGroup)
                {
                    _result = typeof(CompanyAccountGroup).GetProperties();
                }
                else if (_objList == ObjectList.Country)
                {
                    _result = typeof(Country).GetProperties();
                }
                else if (_objList == ObjectList.CreditLimitList)
                {
                    _result = typeof(CreditLimitList).GetProperties();
                }
                else if (_objList == ObjectList.Currency)
                {
                    _result = typeof(Currency).GetProperties();
                }
                else if (_objList == ObjectList.CurrencyRate)
                {
                    _result = typeof(CurrencyRate).GetProperties();
                }
                else if (_objList == ObjectList.CustomerInvoice)
                {
                    _result = typeof(CustomerInvoice).GetProperties();
                }
                else if (_objList == ObjectList.CustomerInvoiceFreeItem)
                {
                    _result = typeof(CustomerInvoiceFreeItem).GetProperties();
                }
                else if (_objList == ObjectList.CustomerInvoiceLine)
                {
                    _result = typeof(CustomerInvoiceLine).GetProperties();
                }
                else if (_objList == ObjectList.CustomerMap)
                {
                    _result = typeof(CustomerMap).GetProperties();
                }
                else if (_objList == ObjectList.CustomerOrder)
                {
                    _result = typeof(CustomerOrder).GetProperties();
                }
                else if (_objList == ObjectList.CustomerOrderFreeItem)
                {
                    _result = typeof(CustomerOrderFreeItem).GetProperties();
                }
                else if (_objList == ObjectList.CustomerOrderLine)
                {
                    _result = typeof(CustomerOrderLine).GetProperties();
                }
                #endregion PropertyInfo C
                #region PropertyInfo D
                else if (_objList == ObjectList.DeliveryOrder)
                {
                    _result = typeof(DeliveryOrder).GetProperties();
                }
                else if (_objList == ObjectList.DeliveryOrderLine)
                {
                    _result = typeof(DeliveryOrderLine).GetProperties();
                }
                else if (_objList == ObjectList.DeliveryOrderMonitoring)
                {
                    _result = typeof(DeliveryOrderMonitoring).GetProperties();
                }
                else if (_objList == ObjectList.Department)
                {
                    _result = typeof(Department).GetProperties();
                }
                else if (_objList == ObjectList.Discount)
                {
                    _result = typeof(Discount).GetProperties();
                }
                else if (_objList == ObjectList.DiscountAccountGroup)
                {
                    _result = typeof(DiscountAccountGroup).GetProperties();
                }
                else if (_objList == ObjectList.DiscountRule)
                {
                    _result = typeof(DiscountRule).GetProperties();
                }
                else if (_objList == ObjectList.DiscountRuleType)
                {
                    _result = typeof(DiscountRuleType).GetProperties();
                }
                else if (_objList == ObjectList.Division)
                {
                    _result = typeof(Division).GetProperties();
                }
                else if (_objList == ObjectList.DocumentType)
                {
                    _result = typeof(DocumentType).GetProperties();
                }
                #endregion PropertyInfo D
                #region PropertyInfo E
                else if (_objList == ObjectList.Employee)
                {
                    _result = typeof(Employee).GetProperties();
                }
                else if (_objList == ObjectList.EmployeeDocumentTypeSetup)
                {
                    _result = typeof(EmployeeDocumentTypeSetup).GetProperties();
                }
                else if (_objList == ObjectList.ExchangeRate)
                {
                    _result = typeof(ExchangeRate).GetProperties();
                }
                #endregion PropertyInfo E
                #region PropertyInfo F
                else if (_objList == ObjectList.FreeItemRule)
                {
                    _result = typeof(FreeItemRule).GetProperties();
                }
                else if (_objList == ObjectList.FreeItemRuleType)
                {
                    _result = typeof(FreeItemRuleType).GetProperties();
                }
                #endregion PropertyInfo F
                #region PropertyInfo G
                else if (_objList == ObjectList.GeneralJournal)
                {
                    _result = typeof(GeneralJournal).GetProperties();
                }
                #endregion PropertyInfo G
                #region PropertyInfo I
                else if (_objList == ObjectList.IndustrialType)
                {
                    _result = typeof(IndustrialType).GetProperties();
                }
                else if (_objList == ObjectList.InventoryJournal)
                {
                    _result = typeof(InventoryJournal).GetProperties();
                }
                else if (_objList == ObjectList.InventoryPurchaseCollection)
                {
                    _result = typeof(InventoryPurchaseCollection).GetProperties();
                }
                else if (_objList == ObjectList.InventoryTransferIn)
                {
                    _result = typeof(InventoryTransferIn).GetProperties();
                }
                else if (_objList == ObjectList.InventoryTransferInLine)
                {
                    _result = typeof(InventoryTransferInLine).GetProperties();
                }
                else if (_objList == ObjectList.InventoryTransferInLot)
                {
                    _result = typeof(InventoryTransferInLot).GetProperties();
                }
                else if (_objList == ObjectList.InventoryTransferInMonitoring)
                {
                    _result = typeof(InventoryTransferInMonitoring).GetProperties();
                }
                else if (_objList == ObjectList.InventoryTransferOut)
                {
                    _result = typeof(InventoryTransferOut).GetProperties();
                }
                else if (_objList == ObjectList.InventoryTransferOutLine)
                {
                    _result = typeof(InventoryTransferOutLine).GetProperties();
                }
                else if (_objList == ObjectList.InventoryTransferOutLot)
                {
                    _result = typeof(InventoryTransferOutLot).GetProperties();
                }
                else if (_objList == ObjectList.InvoiceCanvassing)
                {
                    _result = typeof(InvoiceCanvassing).GetProperties();
                }
                else if (_objList == ObjectList.InvoiceCanvassingCollection)
                {
                    _result = typeof(InvoiceCanvassingCollection).GetProperties();
                }
                else if (_objList == ObjectList.InvoiceCanvassingFreeItem)
                {
                    _result = typeof(InvoiceCanvassingFreeItem).GetProperties();
                }
                else if (_objList == ObjectList.InvoiceCanvassingLine)
                {
                    _result = typeof(InvoiceCanvassingLine).GetProperties();
                }
                else if (_objList == ObjectList.InvoiceCanvassingMonitoring)
                {
                    _result = typeof(InvoiceCanvassingMonitoring).GetProperties();
                }
                else if (_objList == ObjectList.InvoiceInMonitoring)
                {
                    _result = typeof(InvoiceInMonitoring).GetProperties();
                }
                else if (_objList == ObjectList.InvoiceInMonitoringLine)
                {
                    _result = typeof(InvoiceInMonitoringLine).GetProperties();
                }
                else if (_objList == ObjectList.InvoiceOutMonitoring)
                {
                    _result = typeof(InvoiceOutMonitoring).GetProperties();
                }
                else if (_objList == ObjectList.InvoiceOutMonitoringLine)
                {
                    _result = typeof(InvoiceOutMonitoringLine).GetProperties();
                }

                else if (_objList == ObjectList.InvoicePurchaseCollection)
                {
                    _result = typeof(InvoicePurchaseCollection).GetProperties();
                }
                else if (_objList == ObjectList.Item)
                {
                    _result = typeof(Item).GetProperties();
                }
                else if (_objList == ObjectList.ItemAccount)
                {
                    _result = typeof(ItemAccount).GetProperties();
                }
                else if (_objList == ObjectList.ItemAccountGroup)
                {
                    _result = typeof(ItemAccountGroup).GetProperties();
                }
                else if (_objList == ObjectList.ItemComponent)
                {
                    _result = typeof(ItemComponent).GetProperties();
                }
                else if (_objList == ObjectList.ItemGroup)
                {
                    _result = typeof(ItemGroup).GetProperties();
                }
                else if (_objList == ObjectList.ItemReclassJournal)
                {
                    _result = typeof(ItemReclassJournal).GetProperties();
                }
                else if (_objList == ObjectList.ItemReclassJournalLine)
                {
                    _result = typeof(ItemReclassJournalLine).GetProperties();
                }
                else if (_objList == ObjectList.ItemReclassJournalMonitoring)
                {
                    _result = typeof(ItemReclassJournalMonitoring).GetProperties();
                }
                else if (_objList == ObjectList.ItemUnitOfMeasure)
                {
                    _result = typeof(ItemUnitOfMeasure).GetProperties();
                }
                #endregion PropertyInfo I
                #region PropertyInfo J
                else if (_objList == ObjectList.JournalMap)
                {
                    _result = typeof(JournalMap).GetProperties();
                }
                else if (_objList == ObjectList.JournalMapLine)
                {
                    _result = typeof(JournalMapLine).GetProperties();
                }
                else if (_objList == ObjectList.JournalSetupDetail)
                {
                    _result = typeof(JournalSetupDetail).GetProperties();
                }
                #endregion PropertyInfo J
                #region PropertyInfo L
                else if (_objList == ObjectList.ListImport)
                {
                    _result = typeof(ListImport).GetProperties();
                }
                else if (_objList == ObjectList.Location)
                {
                    _result = typeof(Location).GetProperties();
                }
                #endregion PropertyInfo L
                #region PropertyInfo M
                else if (_objList == ObjectList.MailSetupDetail)
                {
                    _result = typeof(MailSetupDetail).GetProperties();
                }
                #endregion PropertyInfo M
                #region PropertyInfo N
                else if (_objList == ObjectList.NumberingHeader)
                {
                    _result = typeof(NumberingHeader).GetProperties();
                }
                else if (_objList == ObjectList.NumberingLine)
                {
                    _result = typeof(NumberingLine).GetProperties();
                }
                #endregion  PropertyInfo N
                #region PropertyInfo O
                else if (_objList == ObjectList.OrderCollection)
                {
                    _result = typeof(OrderCollection).GetProperties();
                }
                else if (_objList == ObjectList.OrderCollectionLine)
                {
                    _result = typeof(OrderCollectionLine).GetProperties();
                }
                else if (_objList == ObjectList.OrderCollectionMonitoring)
                {
                    _result = typeof(OrderCollectionMonitoring).GetProperties();
                }
                else if (_objList == ObjectList.OrganizationAccountGroup)
                {
                    _result = typeof(OrganizationAccountGroup).GetProperties();
                }
                else if (_objList == ObjectList.OrganizationSetupDetail)
                {
                    _result = typeof(OrganizationSetupDetail).GetProperties();
                }
                #endregion PropertyInfo O
                #region PropertyInfo P
                else if (_objList == ObjectList.PayablePurchaseCollection)
                {
                    _result = typeof(PayablePurchaseCollection).GetProperties();
                }
                else if (_objList == ObjectList.PayableTransaction)
                {
                    _result = typeof(PayableTransaction).GetProperties();
                }
                else if (_objList == ObjectList.PayableTransactionLine)
                {
                    _result = typeof(PayableTransactionLine).GetProperties();
                }
                else if (_objList == ObjectList.PayableTransactionMonitoring)
                {
                    _result = typeof(PayableTransactionMonitoring).GetProperties();
                }
                else if (_objList == ObjectList.PaymentInPlan)
                {
                    _result = typeof(PaymentInPlan).GetProperties();
                }
                else if (_objList == ObjectList.PaymentMethod)
                {
                    _result = typeof(PaymentMethod).GetProperties();
                }
                else if (_objList == ObjectList.PaymentOutPlan)
                {
                    _result = typeof(PaymentOutPlan).GetProperties();
                }
                else if (_objList == ObjectList.PaymentRealization)
                {
                    _result = typeof(PaymentRealization).GetProperties();
                }
                else if (_objList == ObjectList.PaymentRealizationLine)
                {
                    _result = typeof(PaymentRealizationLine).GetProperties();
                }
                else if (_objList == ObjectList.PaymentRealizationMonitoring)
                {
                    _result = typeof(PaymentRealizationMonitoring).GetProperties();
                }
                else if (_objList == ObjectList.PhoneNumber)
                {
                    _result = typeof(PhoneNumber).GetProperties();
                }
                else if (_objList == ObjectList.PhoneType)
                {
                    _result = typeof(PhoneType).GetProperties();
                }
                else if (_objList == ObjectList.Position)
                {
                    _result = typeof(Position).GetProperties();
                }
                else if (_objList == ObjectList.Price)
                {
                    _result = typeof(Price).GetProperties();
                }
                else if (_objList == ObjectList.PriceCostMethod)
                {
                    _result = typeof(PriceCostMethod).GetProperties();
                }
                else if (_objList == ObjectList.PriceGroup)
                {
                    _result = typeof(PriceGroup).GetProperties();
                }
                else if (_objList == ObjectList.PriceLine)
                {
                    _result = typeof(PriceLine).GetProperties();
                }
                else if (_objList == ObjectList.PurchaseInvoice)
                {
                    _result = typeof(PurchaseInvoice).GetProperties();
                }
                else if (_objList == ObjectList.PurchaseInvoiceLine)
                {
                    _result = typeof(PurchaseInvoiceLine).GetProperties();
                }
                else if (_objList == ObjectList.PurchaseInvoiceMonitoring)
                {
                    _result = typeof(PurchaseInvoiceMonitoring).GetProperties();
                }
                else if (_objList == ObjectList.PurchaseOrder)
                {
                    _result = typeof(PurchaseOrder).GetProperties();
                }
                else if (_objList == ObjectList.PurchaseOrderLine)
                {
                    _result = typeof(PurchaseOrderLine).GetProperties();
                }
                else if (_objList == ObjectList.PurchaseOrderMonitoring)
                {
                    _result = typeof(PurchaseOrderMonitoring).GetProperties();
                }
                else if (_objList == ObjectList.PurchasePrePaymentInvoice)
                {
                    _result = typeof(PurchasePrePaymentInvoice).GetProperties();
                }
                else if (_objList == ObjectList.PurchasePrePaymentInvoiceMonitoring)
                {
                    _result = typeof(PurchasePrePaymentInvoiceMonitoring).GetProperties();
                }
                else if (_objList == ObjectList.PurchaseRequisition)
                {
                    _result = typeof(PurchaseRequisition).GetProperties();
                }
                else if (_objList == ObjectList.PurchaseRequisitionCollection)
                {
                    _result = typeof(PurchaseRequisitionCollection).GetProperties();
                }
                else if (_objList == ObjectList.PurchaseRequisitionLine)
                {
                    _result = typeof(PurchaseRequisitionLine).GetProperties();
                }
                else if (_objList == ObjectList.PurchaseRequisitionMonitoring)
                {
                    _result = typeof(PurchaseRequisitionMonitoring).GetProperties();
                }
                else if (_objList == ObjectList.PurchaseSetupDetail)
                {
                    _result = typeof(PurchaseSetupDetail).GetProperties();
                }
                #endregion PropertyInfo P
                #region PropertyInfo R
                else if (_objList == ObjectList.ReceivableOrderCollection)
                {
                    _result = typeof(ReceivableOrderCollection).GetProperties();
                }
                else if (_objList == ObjectList.ReceivableTransaction)
                {
                    _result = typeof(ReceivableTransaction).GetProperties();
                }
                else if (_objList == ObjectList.ReceivableTransactionLine)
                {
                    _result = typeof(ReceivableTransactionLine).GetProperties();
                }
                else if (_objList == ObjectList.ReceivableTransactionMonitoring)
                {
                    _result = typeof(ReceivableTransactionMonitoring).GetProperties();
                }
                else if (_objList == ObjectList.RoundingSetupDetail)
                {
                    _result = typeof(RoundingSetupDetail).GetProperties();
                }
                else if (_objList == ObjectList.Route)
                {
                    _result = typeof(Route).GetProperties();
                }
                else if (_objList == ObjectList.RouteInvoice)
                {
                    _result = typeof(RouteInvoice).GetProperties();
                }
                else if (_objList == ObjectList.RoutePlan)
                {
                    _result = typeof(RoutePlan).GetProperties();
                }
                #endregion PropertyInfo R
                #region PropertyInfo S
                else if (_objList == ObjectList.SalesArea)
                {
                    _result = typeof(SalesArea).GetProperties();
                }
                else if (_objList == ObjectList.SalesArea1)
                {
                    _result = typeof(SalesArea1).GetProperties();
                }
                else if (_objList == ObjectList.SalesArea2)
                {
                    _result = typeof(SalesArea2).GetProperties();
                }
                else if (_objList == ObjectList.SalesArea3)
                {
                    _result = typeof(SalesArea3).GetProperties();
                }
                else if (_objList == ObjectList.SalesArea4)
                {
                    _result = typeof(SalesArea4).GetProperties();
                }
                else if (_objList == ObjectList.SalesAreaLine)
                {
                    _result = typeof(SalesAreaLine).GetProperties();
                }
                else if (_objList == ObjectList.SalesInvoice)
                {
                    _result = typeof(SalesInvoice).GetProperties();
                }
                else if (_objList == ObjectList.SalesInvoiceCmp)
                {
                    _result = typeof(SalesInvoiceCmp).GetProperties();
                }
                else if (_objList == ObjectList.SalesInvoiceCollection)
                {
                    _result = typeof(SalesInvoiceCollection).GetProperties();
                }
                else if (_objList == ObjectList.SalesInvoiceFreeItem)
                {
                    _result = typeof(SalesInvoiceFreeItem).GetProperties();
                }
                else if (_objList == ObjectList.SalesInvoiceFreeItemCmp)
                {
                    _result = typeof(SalesInvoiceFreeItemCmp).GetProperties();
                }
                else if (_objList == ObjectList.SalesInvoiceLine)
                {
                    _result = typeof(SalesInvoiceLine).GetProperties();
                }
                else if (_objList == ObjectList.SalesInvoiceLineCmp)
                {
                    _result = typeof(SalesInvoiceLineCmp).GetProperties();
                }
                else if (_objList == ObjectList.SalesInvoiceMonitoring)
                {
                    _result = typeof(SalesInvoiceMonitoring).GetProperties();
                }
                else if (_objList == ObjectList.SalesmanAreaSetup)
                {
                    _result = typeof(SalesmanAreaSetup).GetProperties();
                }
                else if (_objList == ObjectList.SalesmanCustomerSetup)
                {
                    _result = typeof(SalesmanCustomerSetup).GetProperties();
                }
                else if (_objList == ObjectList.SalesmanLocationSetup)
                {
                    _result = typeof(SalesmanLocationSetup).GetProperties();
                }
                else if (_objList == ObjectList.SalesmanVehicleSetup)
                {
                    _result = typeof(SalesmanVehicleSetup).GetProperties();
                }
                else if (_objList == ObjectList.SalesOrder)
                {
                    _result = typeof(SalesOrder).GetProperties();
                }
                else if (_objList == ObjectList.SalesOrderCollection)
                {
                    _result = typeof(SalesOrderCollection).GetProperties();
                }
                else if (_objList == ObjectList.SalesOrderFreeItem)
                {
                    _result = typeof(SalesOrderFreeItem).GetProperties();
                }
                else if (_objList == ObjectList.SalesOrderLine)
                {
                    _result = typeof(SalesOrderLine).GetProperties();
                }
                else if (_objList == ObjectList.SalesOrderMonitoring)
                {
                    _result = typeof(SalesOrderMonitoring).GetProperties();
                }
                else if (_objList == ObjectList.SalesPromotionRule)
                {
                    _result = typeof(SalesPromotionRule).GetProperties();
                }
                else if (_objList == ObjectList.ScheduleType)
                {
                    _result = typeof(ScheduleType).GetProperties();
                }
                else if (_objList == ObjectList.Section)
                {
                    _result = typeof(Section).GetProperties();
                }
                #endregion PropertyInfo S
                #region PropertyInfo T
                else if (_objList == ObjectList.TaskMode)
                {
                    _result = typeof(TaskMode).GetProperties();
                }
                else if (_objList == ObjectList.Tax)
                {
                    _result = typeof(Tax).GetProperties();
                }
                else if (_objList == ObjectList.TaxAccount)
                {
                    _result = typeof(TaxAccount).GetProperties();
                }
                else if (_objList == ObjectList.TaxAccountGroup)
                {
                    _result = typeof(TaxAccountGroup).GetProperties();
                }
                else if (_objList == ObjectList.TaxGroup)
                {
                    _result = typeof(TaxGroup).GetProperties();
                }
                else if (_objList == ObjectList.TaxLine)
                {
                    _result = typeof(TaxLine).GetProperties();
                }
                else if (_objList == ObjectList.TaxType)
                {
                    _result = typeof(TaxType).GetProperties();
                }
                else if (_objList == ObjectList.TermOfPayment)
                {
                    _result = typeof(TermOfPayment).GetProperties();
                }
                else if (_objList == ObjectList.TerritoryLevel1)
                {
                    _result = typeof(TerritoryLevel1).GetProperties();
                }
                else if (_objList == ObjectList.TerritoryLevel2)
                {
                    _result = typeof(TerritoryLevel2).GetProperties();
                }
                else if (_objList == ObjectList.TerritoryLevel3)
                {
                    _result = typeof(TerritoryLevel3).GetProperties();
                }
                else if (_objList == ObjectList.TerritoryLevel4)
                {
                    _result = typeof(TerritoryLevel4).GetProperties();
                }
                else if (_objList == ObjectList.Transfer)
                {
                    _result = typeof(Transfer).GetProperties();
                }
                else if (_objList == ObjectList.TransferIn)
                {
                    _result = typeof(TransferIn).GetProperties();
                }
                else if (_objList == ObjectList.TransferInLine)
                {
                    _result = typeof(TransferInLine).GetProperties();
                }
                else if (_objList == ObjectList.TransferInLot)
                {
                    _result = typeof(TransferInLot).GetProperties();
                }
                else if (_objList == ObjectList.TransferInMonitoring)
                {
                    _result = typeof(TransferInMonitoring).GetProperties();
                }
                else if (_objList == ObjectList.TransferLine)
                {
                    _result = typeof(TransferLine).GetProperties();
                }
                else if (_objList == ObjectList.TransferMonitoring)
                {
                    _result = typeof(TransferMonitoring).GetProperties();
                }
                else if (_objList == ObjectList.TransferOut)
                {
                    _result = typeof(TransferOut).GetProperties();
                }
                else if (_objList == ObjectList.TransferOutCollection)
                {
                    _result = typeof(TransferOutCollection).GetProperties();
                }
                else if (_objList == ObjectList.TransferOutLine)
                {
                    _result = typeof(TransferOutLine).GetProperties();
                }
                else if (_objList == ObjectList.TransferOutLot)
                {
                    _result = typeof(TransferOutLot).GetProperties();
                }
                else if (_objList == ObjectList.TransferOutMonitoring)
                {
                    _result = typeof(TransferOutMonitoring).GetProperties();
                }
                #endregion PropertyInfo T
                #region PropertyInfo U
                else if (_objList == ObjectList.UnitOfMeasure)
                {
                    _result = typeof(UnitOfMeasure).GetProperties();
                }
                else if (_objList == ObjectList.UnitOfMeasureType)
                {
                    _result = typeof(UnitOfMeasureType).GetProperties();
                }
                else if (_objList == ObjectList.UserAccess)
                {
                    _result = typeof(UserAccess).GetProperties();
                }
                else if (_objList == ObjectList.UserAccessRole)
                {
                    _result = typeof(UserAccessRole).GetProperties();
                }
                #endregion PropertyInfo U
                #region PropertyInfo V
                else if (_objList == ObjectList.Vehicle)
                {
                    _result = typeof(Vehicle).GetProperties();
                }
                else if (_objList == ObjectList.VehicleList)
                {
                    _result = typeof(VehicleList).GetProperties();
                }
                #endregion PropertyInfo V
                #region PropertyInfo W
                else if (_objList == ObjectList.WarehouseSetupDetail)
                {
                    _result = typeof(WarehouseSetupDetail).GetProperties();
                }
                else if (_objList == ObjectList.Workplace)
                {
                    _result = typeof(Workplace).GetProperties();
                }
                #endregion PropertyInfo W

            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Module = GlobalFunction " + ex.ToString());
            }
            return _result;
        }

        private object GetBusinessObject(Session _currSession, ObjectList _objList)
        {
            object _result = null;
            try
            {
                #region GetBusinessObject A
                if (_objList == ObjectList.AccountGroup)
                {
                    _result = (AccountGroup)Activator.CreateInstance(typeof(AccountGroup), _currSession);
                }
                else if (_objList == ObjectList.AccountingPeriodic)
                {
                    _result = (AccountingPeriodic)Activator.CreateInstance(typeof(AccountingPeriodic), _currSession);
                }
                else if (_objList == ObjectList.AccountingPeriodicLine)
                {
                    _result = (AccountingPeriodicLine)Activator.CreateInstance(typeof(AccountingPeriodicLine), _currSession);
                }
                else if (_objList == ObjectList.AccountMap)
                {
                    _result = (AccountMap)Activator.CreateInstance(typeof(AccountMap), _currSession);
                }
                else if (_objList == ObjectList.AccountMapLine)
                {
                    _result = (AccountMapLine)Activator.CreateInstance(typeof(AccountMapLine), _currSession);
                }
                else if (_objList == ObjectList.AccountReclassJournal)
                {
                    _result = (AccountReclassJournal)Activator.CreateInstance(typeof(AccountReclassJournal), _currSession);
                }
                else if (_objList == ObjectList.AccountReclassJournalLine)
                {
                    _result = (AccountReclassJournalLine)Activator.CreateInstance(typeof(AccountReclassJournalLine), _currSession);
                }
                else if (_objList == ObjectList.AccountReclassJournalMonitoring)
                {
                    _result = (AccountReclassJournalMonitoring)Activator.CreateInstance(typeof(AccountReclassJournalMonitoring), _currSession);
                }
                else if (_objList == ObjectList.AdditionalAnalyze)
                {
                    _result = (AdditionalAnalyze)Activator.CreateInstance(typeof(AdditionalAnalyze), _currSession);
                }
                else if (_objList == ObjectList.AdditionalDashboard)
                {
                    _result = (AdditionalDashboard)Activator.CreateInstance(typeof(AdditionalDashboard), _currSession);
                }
                else if (_objList == ObjectList.AdditionalReport)
                {
                    _result = (AdditionalReport)Activator.CreateInstance(typeof(AdditionalReport), _currSession);
                }
                else if (_objList == ObjectList.Address)
                {
                    _result = (Address)Activator.CreateInstance(typeof(Address), _currSession);
                }
                else if (_objList == ObjectList.AdvanceAccountGroup)
                {
                    _result = (AdvanceAccountGroup)Activator.CreateInstance(typeof(AdvanceAccountGroup), _currSession);
                }
                else if (_objList == ObjectList.AmendDelivery)
                {
                    _result = (AmendDelivery)Activator.CreateInstance(typeof(AmendDelivery), _currSession);
                }
                else if (_objList == ObjectList.AmendDeliveryLine)
                {
                    _result = (AmendDeliveryLine)Activator.CreateInstance(typeof(AmendDeliveryLine), _currSession);
                }
                else if (_objList == ObjectList.AmendDeliveryMonitoring)
                {
                    _result = (AmendDeliveryMonitoring)Activator.CreateInstance(typeof(AmendDeliveryMonitoring), _currSession);
                }
                else if (_objList == ObjectList.ApplicationImport)
                {
                    _result = (ApplicationImport)Activator.CreateInstance(typeof(ApplicationImport), _currSession);
                }
                else if (_objList == ObjectList.ApplicationSetup)
                {
                    _result = (ApplicationSetup)Activator.CreateInstance(typeof(ApplicationSetup), _currSession);
                }
                else if (_objList == ObjectList.ApplicationSetupDetail)
                {
                    _result = (ApplicationSetupDetail)Activator.CreateInstance(typeof(ApplicationSetupDetail), _currSession);
                }
                else if (_objList == ObjectList.ApprovalLine)
                {
                    _result = (ApprovalLine)Activator.CreateInstance(typeof(ApprovalLine), _currSession);
                }
                else if (_objList == ObjectList.Area)
                {
                    _result = (Area)Activator.CreateInstance(typeof(Area), _currSession);
                }
                #endregion GetBusinessObject A
                #region GetBusinessObject B
                else if (_objList == ObjectList.BankAccount)
                {
                    _result = (BankAccount)Activator.CreateInstance(typeof(BankAccount), _currSession);
                }
                else if (_objList == ObjectList.BankAccountGroup)
                {
                    _result = (BankAccountGroup)Activator.CreateInstance(typeof(BankAccountGroup), _currSession);
                }
                else if (_objList == ObjectList.BeginningInventory)
                {
                    _result = (BeginningInventory)Activator.CreateInstance(typeof(BeginningInventory), _currSession);
                }
                else if (_objList == ObjectList.BeginningInventoryLine)
                {
                    _result = (BeginningInventoryLine)Activator.CreateInstance(typeof(BeginningInventoryLine), _currSession);
                }
                else if (_objList == ObjectList.BinLocation)
                {
                    _result = (BinLocation)Activator.CreateInstance(typeof(BinLocation), _currSession);
                }
                else if (_objList == ObjectList.BinLocationSetupDetail)
                {
                    _result = (BinLocationSetupDetail)Activator.CreateInstance(typeof(BinLocationSetupDetail), _currSession);
                }
                else if (_objList == ObjectList.Brand)
                {
                    _result = (Brand)Activator.CreateInstance(typeof(Brand), _currSession);
                }
                else if (_objList == ObjectList.BusinessPartner)
                {
                    _result = (BusinessPartner)Activator.CreateInstance(typeof(BusinessPartner), _currSession);
                }
                else if (_objList == ObjectList.BusinessPartnerAccountGroup)
                {
                    _result = (BusinessPartnerAccountGroup)Activator.CreateInstance(typeof(BusinessPartnerAccountGroup), _currSession);
                }
                else if (_objList == ObjectList.BusinessPartnerGroup)
                {
                    _result = (BusinessPartnerGroup)Activator.CreateInstance(typeof(BusinessPartnerGroup), _currSession);
                }
                else if (_objList == ObjectList.BusinessPartnerPriceGroupSetup)
                {
                    _result = (BusinessPartnerPriceGroupSetup)Activator.CreateInstance(typeof(BusinessPartnerPriceGroupSetup), _currSession);
                }
                #endregion GetBusinessObject B
                #region GetBusinessObject C
                else if (_objList == ObjectList.CanvasPaymentIn)
                {
                    _result = (CanvasPaymentIn)Activator.CreateInstance(typeof(CanvasPaymentIn), _currSession);
                }
                else if (_objList == ObjectList.CanvassingCollection)
                {
                    _result = (CanvassingCollection)Activator.CreateInstance(typeof(CanvassingCollection), _currSession);
                }
                else if (_objList == ObjectList.CanvassingCollectionFreeItem)
                {
                    _result = (CanvassingCollectionFreeItem)Activator.CreateInstance(typeof(CanvassingCollectionFreeItem), _currSession);
                }
                else if (_objList == ObjectList.CanvassingCollectionLine)
                {
                    _result = (CanvassingCollectionLine)Activator.CreateInstance(typeof(CanvassingCollectionLine), _currSession);
                }
                else if (_objList == ObjectList.CanvassingCollectionMonitoring)
                {
                    _result = (CanvassingCollectionMonitoring)Activator.CreateInstance(typeof(CanvassingCollectionMonitoring), _currSession);
                }
                else if (_objList == ObjectList.CashAdvance)
                {
                    _result = (CashAdvance)Activator.CreateInstance(typeof(CashAdvance), _currSession);
                }
                else if (_objList == ObjectList.CashAdvanceCollection)
                {
                    _result = (CashAdvanceCollection)Activator.CreateInstance(typeof(CashAdvanceCollection), _currSession);
                }
                else if (_objList == ObjectList.CashAdvanceLine)
                {
                    _result = (CashAdvanceLine)Activator.CreateInstance(typeof(CashAdvanceLine), _currSession);
                }
                else if (_objList == ObjectList.CashAdvanceMonitoring)
                {
                    _result = (CashAdvanceMonitoring)Activator.CreateInstance(typeof(CashAdvanceMonitoring), _currSession);
                }
                else if (_objList == ObjectList.CashAdvancePaymentPlan)
                {
                    _result = (CashAdvancePaymentPlan)Activator.CreateInstance(typeof(CashAdvancePaymentPlan), _currSession);
                }
                else if (_objList == ObjectList.CashAdvanceType)
                {
                    _result = (CashAdvanceType)Activator.CreateInstance(typeof(CashAdvanceType), _currSession);
                }
                else if (_objList == ObjectList.ChartOfAccount)
                {
                    _result = (ChartOfAccount)Activator.CreateInstance(typeof(ChartOfAccount), _currSession);
                }
                else if (_objList == ObjectList.City)
                {
                    _result = (City)Activator.CreateInstance(typeof(City), _currSession);
                }
                else if (_objList == ObjectList.Company)
                {
                    _result = (Company)Activator.CreateInstance(typeof(Company), _currSession);
                }
                else if (_objList == ObjectList.CompanyAccount)
                {
                    _result = (CompanyAccount)Activator.CreateInstance(typeof(CompanyAccount), _currSession);
                }
                else if (_objList == ObjectList.CompanyAccountGroup)
                {
                    _result = (CompanyAccountGroup)Activator.CreateInstance(typeof(CompanyAccountGroup), _currSession);
                }
                else if (_objList == ObjectList.Country)
                {
                    _result = (Country)Activator.CreateInstance(typeof(Country), _currSession);
                }
                else if (_objList == ObjectList.CreditLimitList)
                {
                    _result = (CreditLimitList)Activator.CreateInstance(typeof(CreditLimitList), _currSession);
                }
                else if (_objList == ObjectList.Currency)
                {
                    _result = (Currency)Activator.CreateInstance(typeof(Currency), _currSession);
                }
                else if (_objList == ObjectList.CurrencyRate)
                {
                    _result = (CurrencyRate)Activator.CreateInstance(typeof(CurrencyRate), _currSession);
                }
                else if (_objList == ObjectList.CustomerInvoice)
                {
                    _result = (CustomerInvoice)Activator.CreateInstance(typeof(CustomerInvoice), _currSession);
                }
                else if (_objList == ObjectList.CustomerInvoiceFreeItem)
                {
                    _result = (CustomerInvoiceFreeItem)Activator.CreateInstance(typeof(CustomerInvoiceFreeItem), _currSession);
                }
                else if (_objList == ObjectList.CustomerInvoiceLine)
                {
                    _result = (CustomerInvoiceLine)Activator.CreateInstance(typeof(CustomerInvoiceLine), _currSession);
                }
                else if (_objList == ObjectList.CustomerMap)
                {
                    _result = (CustomerMap)Activator.CreateInstance(typeof(CustomerMap), _currSession);
                }
                else if (_objList == ObjectList.CustomerOrder)
                {
                    _result = (CustomerOrder)Activator.CreateInstance(typeof(CustomerOrder), _currSession);
                }
                else if (_objList == ObjectList.CustomerOrderFreeItem)
                {
                    _result = (CustomerOrderFreeItem)Activator.CreateInstance(typeof(CustomerOrderFreeItem), _currSession);
                }
                else if (_objList == ObjectList.CustomerOrder)
                {
                    _result = (CustomerOrderLine)Activator.CreateInstance(typeof(CustomerOrderLine), _currSession);
                }
                #endregion GetBusinessObject C
                #region GetBusinessObject D
                else if (_objList == ObjectList.DeliveryOrder)
                {
                    _result = (DeliveryOrder)Activator.CreateInstance(typeof(DeliveryOrder), _currSession);
                }
                else if (_objList == ObjectList.DeliveryOrderLine)
                {
                    _result = (DeliveryOrderLine)Activator.CreateInstance(typeof(DeliveryOrderLine), _currSession);
                }
                else if (_objList == ObjectList.DeliveryOrderMonitoring)
                {
                    _result = (DeliveryOrderMonitoring)Activator.CreateInstance(typeof(DeliveryOrderMonitoring), _currSession);
                }
                else if (_objList == ObjectList.Department)
                {
                    _result = (Department)Activator.CreateInstance(typeof(Department), _currSession);
                }
                else if (_objList == ObjectList.Discount)
                {
                    _result = (Discount)Activator.CreateInstance(typeof(Discount), _currSession);
                }
                else if (_objList == ObjectList.DiscountAccountGroup)
                {
                    _result = (DiscountAccountGroup)Activator.CreateInstance(typeof(DiscountAccountGroup), _currSession);
                }
                else if (_objList == ObjectList.DiscountRule)
                {
                    _result = (DiscountRule)Activator.CreateInstance(typeof(DiscountRule), _currSession);
                }
                else if (_objList == ObjectList.DiscountRuleType)
                {
                    _result = (DiscountRuleType)Activator.CreateInstance(typeof(DiscountRuleType), _currSession);
                }
                else if (_objList == ObjectList.Division)
                {
                    _result = (Division)Activator.CreateInstance(typeof(Division), _currSession);
                }
                else if (_objList == ObjectList.DocumentType)
                {
                    _result = (DocumentType)Activator.CreateInstance(typeof(DocumentType), _currSession);
                }
                #endregion GetBusinessObject D
                #region GetBusinessObject E
                else if (_objList == ObjectList.Employee)
                {
                    _result = (Employee)Activator.CreateInstance(typeof(Employee), _currSession);
                }
                else if (_objList == ObjectList.EmployeeDocumentTypeSetup)
                {
                    _result = (EmployeeDocumentTypeSetup)Activator.CreateInstance(typeof(EmployeeDocumentTypeSetup), _currSession);
                }
                else if (_objList == ObjectList.ExchangeRate)
                {
                    _result = (ExchangeRate)Activator.CreateInstance(typeof(ExchangeRate), _currSession);
                }
                #endregion GetBusinessObject E
                #region GetBusinessObject G
                else if (_objList == ObjectList.FreeItemRule)
                {
                    _result = (FreeItemRule)Activator.CreateInstance(typeof(FreeItemRule), _currSession);
                }
                else if (_objList == ObjectList.FreeItemRuleType)
                {
                    _result = (FreeItemRuleType)Activator.CreateInstance(typeof(FreeItemRuleType), _currSession);
                }
                #endregion GetBusinessObject G
                #region GetBusinessObject G
                else if (_objList == ObjectList.GeneralJournal)
                {
                    _result = (GeneralJournal)Activator.CreateInstance(typeof(GeneralJournal), _currSession);
                }
                #endregion GetBusinessObject G
                #region GetBusinessObject I
                else if (_objList == ObjectList.IndustrialType)
                {
                    _result = (IndustrialType)Activator.CreateInstance(typeof(IndustrialType), _currSession);
                }
                else if (_objList == ObjectList.InventoryJournal)
                {
                    _result = (InventoryJournal)Activator.CreateInstance(typeof(InventoryJournal), _currSession);
                }
                else if (_objList == ObjectList.InventoryPurchaseCollection)
                {
                    _result = (InventoryPurchaseCollection)Activator.CreateInstance(typeof(InventoryPurchaseCollection), _currSession);
                }
                else if (_objList == ObjectList.InventoryTransferIn)
                {
                    _result = (InventoryTransferIn)Activator.CreateInstance(typeof(InventoryTransferIn), _currSession);
                }
                else if (_objList == ObjectList.InventoryTransferInLine)
                {
                    _result = (InventoryTransferInLine)Activator.CreateInstance(typeof(InventoryTransferInLine), _currSession);
                }
                else if (_objList == ObjectList.InventoryTransferInLot)
                {
                    _result = (InventoryTransferInLot)Activator.CreateInstance(typeof(InventoryTransferInLot), _currSession);
                }
                else if (_objList == ObjectList.InventoryTransferInMonitoring)
                {
                    _result = (InventoryTransferInMonitoring)Activator.CreateInstance(typeof(InventoryTransferInMonitoring), _currSession);
                }

                else if (_objList == ObjectList.InventoryTransferOut)
                {
                    _result = (InventoryTransferOut)Activator.CreateInstance(typeof(InventoryTransferOut), _currSession);
                }
                else if (_objList == ObjectList.InventoryTransferOutLine)
                {
                    _result = (InventoryTransferOutLine)Activator.CreateInstance(typeof(InventoryTransferOutLine), _currSession);
                }
                else if (_objList == ObjectList.InventoryTransferOutLot)
                {
                    _result = (InventoryTransferOutLot)Activator.CreateInstance(typeof(InventoryTransferOutLot), _currSession);
                }
                else if (_objList == ObjectList.InvoiceCanvassing)
                {
                    _result = (InvoiceCanvassing)Activator.CreateInstance(typeof(InvoiceCanvassing), _currSession);
                }
                else if (_objList == ObjectList.InvoiceCanvassingCollection)
                {
                    _result = (InvoiceCanvassingCollection)Activator.CreateInstance(typeof(InvoiceCanvassingCollection), _currSession);
                }
                else if (_objList == ObjectList.InvoiceCanvassingFreeItem)
                {
                    _result = (InvoiceCanvassingFreeItem)Activator.CreateInstance(typeof(InvoiceCanvassingFreeItem), _currSession);
                }
                else if (_objList == ObjectList.InvoiceCanvassingLine)
                {
                    _result = (InvoiceCanvassingLine)Activator.CreateInstance(typeof(InvoiceCanvassingLine), _currSession);
                }
                else if (_objList == ObjectList.InvoiceCanvassingMonitoring)
                {
                    _result = (InvoiceCanvassingMonitoring)Activator.CreateInstance(typeof(InvoiceCanvassingMonitoring), _currSession);
                }
                else if (_objList == ObjectList.InvoiceInMonitoring)
                {
                    _result = (InvoiceInMonitoring)Activator.CreateInstance(typeof(InvoiceInMonitoring), _currSession);
                }
                else if (_objList == ObjectList.InvoiceInMonitoringLine)
                {
                    _result = (InvoiceInMonitoringLine)Activator.CreateInstance(typeof(InvoiceInMonitoringLine), _currSession);
                }
                else if (_objList == ObjectList.InvoiceOutMonitoring)
                {
                    _result = (InvoiceOutMonitoring)Activator.CreateInstance(typeof(InvoiceOutMonitoring), _currSession);
                }
                else if (_objList == ObjectList.InvoiceOutMonitoringLine)
                {
                    _result = (InvoiceOutMonitoringLine)Activator.CreateInstance(typeof(InvoiceOutMonitoringLine), _currSession);
                }

                else if (_objList == ObjectList.InvoicePurchaseCollection)
                {
                    _result = (InvoicePurchaseCollection)Activator.CreateInstance(typeof(InvoicePurchaseCollection), _currSession);
                }
                else if (_objList == ObjectList.Item)
                {
                    _result = (Item)Activator.CreateInstance(typeof(Item), _currSession);
                }
                else if (_objList == ObjectList.ItemAccount)
                {
                    _result = (ItemAccount)Activator.CreateInstance(typeof(ItemAccount), _currSession);
                }
                else if (_objList == ObjectList.ItemAccountGroup)
                {
                    _result = (ItemAccountGroup)Activator.CreateInstance(typeof(ItemAccountGroup), _currSession);
                }
                else if (_objList == ObjectList.ItemComponent)
                {
                    _result = (ItemComponent)Activator.CreateInstance(typeof(ItemComponent), _currSession);
                }
                else if (_objList == ObjectList.ItemGroup)
                {
                    _result = (ItemGroup)Activator.CreateInstance(typeof(ItemGroup), _currSession);
                }
                else if (_objList == ObjectList.ItemReclassJournal)
                {
                    _result = (ItemReclassJournal)Activator.CreateInstance(typeof(ItemReclassJournal), _currSession);
                }
                else if (_objList == ObjectList.ItemReclassJournalLine)
                {
                    _result = (ItemReclassJournalLine)Activator.CreateInstance(typeof(ItemReclassJournalLine), _currSession);
                }
                else if (_objList == ObjectList.ItemReclassJournalMonitoring)
                {
                    _result = (ItemReclassJournalMonitoring)Activator.CreateInstance(typeof(ItemReclassJournalMonitoring), _currSession);
                }
                else if (_objList == ObjectList.ItemUnitOfMeasure)
                {
                    _result = (ItemUnitOfMeasure)Activator.CreateInstance(typeof(ItemUnitOfMeasure), _currSession);
                }
                #endregion GetBusinessObject I
                #region GetBusinessObject J
                else if (_objList == ObjectList.JournalMap)
                {
                    _result = (JournalMap)Activator.CreateInstance(typeof(JournalMap), _currSession);
                }
                else if (_objList == ObjectList.JournalMapLine)
                {
                    _result = (JournalMapLine)Activator.CreateInstance(typeof(JournalMapLine), _currSession);
                }
                else if (_objList == ObjectList.JournalSetupDetail)
                {
                    _result = (JournalSetupDetail)Activator.CreateInstance(typeof(JournalSetupDetail), _currSession);
                }
                #endregion GetBusinessObject J
                #region GetBusinessObject L
                else if (_objList == ObjectList.ListImport)
                {
                    _result = (ListImport)Activator.CreateInstance(typeof(ListImport), _currSession);
                }
                else if (_objList == ObjectList.Location)
                {
                    _result = (Location)Activator.CreateInstance(typeof(Location), _currSession);
                }
                #endregion GetBusinessObject L
                #region GetBusinessObject M
                else if (_objList == ObjectList.MailSetupDetail)
                {
                    _result = (MailSetupDetail)Activator.CreateInstance(typeof(MailSetupDetail), _currSession);
                }
                #endregion GetBusinessObject M
                #region GetBusinessObject N
                else if (_objList == ObjectList.NumberingHeader)
                {
                    _result = (NumberingHeader)Activator.CreateInstance(typeof(NumberingHeader), _currSession);
                }
                else if (_objList == ObjectList.NumberingLine)
                {
                    _result = (NumberingLine)Activator.CreateInstance(typeof(NumberingLine), _currSession);
                }
                #endregion GetBusinessObject N
                #region GetBusinessObject O
                else if (_objList == ObjectList.OrderCollection)
                {
                    _result = (OrderCollection)Activator.CreateInstance(typeof(OrderCollection), _currSession);
                }
                else if (_objList == ObjectList.OrderCollectionLine)
                {
                    _result = (OrderCollectionLine)Activator.CreateInstance(typeof(OrderCollectionLine), _currSession);
                }
                else if (_objList == ObjectList.OrderCollectionMonitoring)
                {
                    _result = (OrderCollectionMonitoring)Activator.CreateInstance(typeof(OrderCollectionMonitoring), _currSession);
                }
                else if (_objList == ObjectList.OrganizationAccountGroup)
                {
                    _result = (OrganizationAccountGroup)Activator.CreateInstance(typeof(OrganizationAccountGroup), _currSession);
                }
                else if (_objList == ObjectList.OrganizationSetupDetail)
                {
                    _result = (OrganizationSetupDetail)Activator.CreateInstance(typeof(OrganizationSetupDetail), _currSession);
                }
                #endregion GetBusinessObject O
                #region GetBusinessObject P
                else if (_objList == ObjectList.PayablePurchaseCollection)
                {
                    _result = (PayablePurchaseCollection)Activator.CreateInstance(typeof(PayablePurchaseCollection), _currSession);
                }
                else if (_objList == ObjectList.PayableTransaction)
                {
                    _result = (PayableTransaction)Activator.CreateInstance(typeof(PayableTransaction), _currSession);
                }
                else if (_objList == ObjectList.PayableTransactionLine)
                {
                    _result = (PayableTransactionLine)Activator.CreateInstance(typeof(PayableTransactionLine), _currSession);
                }
                else if (_objList == ObjectList.PayableTransactionMonitoring)
                {
                    _result = (PayableTransactionMonitoring)Activator.CreateInstance(typeof(PayableTransactionMonitoring), _currSession);
                }
                else if (_objList == ObjectList.PaymentInPlan)
                {
                    _result = (PaymentInPlan)Activator.CreateInstance(typeof(PaymentInPlan), _currSession);
                }
                else if (_objList == ObjectList.PaymentMethod)
                {
                    _result = (PaymentMethod)Activator.CreateInstance(typeof(PaymentMethod), _currSession);
                }
                else if (_objList == ObjectList.PaymentOutPlan)
                {
                    _result = (PaymentOutPlan)Activator.CreateInstance(typeof(PaymentOutPlan), _currSession);
                }
                else if (_objList == ObjectList.PaymentRealization)
                {
                    _result = (PaymentRealization)Activator.CreateInstance(typeof(PaymentRealization), _currSession);
                }
                else if (_objList == ObjectList.PaymentRealizationLine)
                {
                    _result = (PaymentRealizationLine)Activator.CreateInstance(typeof(PaymentRealizationLine), _currSession);
                }
                else if (_objList == ObjectList.PaymentRealizationMonitoring)
                {
                    _result = (PaymentRealizationMonitoring)Activator.CreateInstance(typeof(PaymentRealizationMonitoring), _currSession);
                }
                else if (_objList == ObjectList.PhoneNumber)
                {
                    _result = (PhoneNumber)Activator.CreateInstance(typeof(PhoneNumber), _currSession);
                }
                else if (_objList == ObjectList.PhoneType)
                {
                    _result = (PhoneType)Activator.CreateInstance(typeof(PhoneType), _currSession);
                }
                else if (_objList == ObjectList.Position)
                {
                    _result = (Position)Activator.CreateInstance(typeof(Position), _currSession);
                }
                else if (_objList == ObjectList.Price)
                {
                    _result = (Price)Activator.CreateInstance(typeof(Price), _currSession);
                }
                else if (_objList == ObjectList.PriceCostMethod)
                {
                    _result = (PriceCostMethod)Activator.CreateInstance(typeof(PriceCostMethod), _currSession);
                }
                else if (_objList == ObjectList.PriceGroup)
                {
                    _result = (PriceGroup)Activator.CreateInstance(typeof(PriceGroup), _currSession);
                }
                else if (_objList == ObjectList.PriceLine)
                {
                    _result = (PriceLine)Activator.CreateInstance(typeof(PriceLine), _currSession);
                }
                else if (_objList == ObjectList.PurchaseInvoice)
                {
                    _result = (PurchaseInvoice)Activator.CreateInstance(typeof(PurchaseInvoice), _currSession);
                }
                else if (_objList == ObjectList.PurchaseInvoiceLine)
                {
                    _result = (PurchaseInvoiceLine)Activator.CreateInstance(typeof(PurchaseInvoiceLine), _currSession);
                }
                else if (_objList == ObjectList.PurchaseInvoiceMonitoring)
                {
                    _result = (PurchaseInvoiceMonitoring)Activator.CreateInstance(typeof(PurchaseInvoiceMonitoring), _currSession);
                }
                else if (_objList == ObjectList.PurchaseOrder)
                {
                    _result = (PurchaseOrder)Activator.CreateInstance(typeof(PurchaseOrder), _currSession);
                }
                else if (_objList == ObjectList.PurchaseOrderLine)
                {
                    _result = (PurchaseOrderLine)Activator.CreateInstance(typeof(PurchaseOrderLine), _currSession);
                }
                else if (_objList == ObjectList.PurchaseOrderMonitoring)
                {
                    _result = (PurchaseOrderMonitoring)Activator.CreateInstance(typeof(PurchaseOrderMonitoring), _currSession);
                }
                else if (_objList == ObjectList.PurchasePrePaymentInvoice)
                {
                    _result = (PurchasePrePaymentInvoice)Activator.CreateInstance(typeof(PurchasePrePaymentInvoice), _currSession);
                }
                else if (_objList == ObjectList.PurchasePrePaymentInvoiceMonitoring)
                {
                    _result = (PurchasePrePaymentInvoiceMonitoring)Activator.CreateInstance(typeof(PurchasePrePaymentInvoiceMonitoring), _currSession);
                }
                else if (_objList == ObjectList.PurchaseRequisition)
                {
                    _result = (PurchaseRequisition)Activator.CreateInstance(typeof(PurchaseRequisition), _currSession);
                }
                else if (_objList == ObjectList.PurchaseRequisitionCollection)
                {
                    _result = (PurchaseRequisitionCollection)Activator.CreateInstance(typeof(PurchaseRequisitionCollection), _currSession);
                }
                else if (_objList == ObjectList.PurchaseRequisitionLine)
                {
                    _result = (PurchaseRequisitionLine)Activator.CreateInstance(typeof(PurchaseRequisitionLine), _currSession);
                }
                else if (_objList == ObjectList.PurchaseRequisitionMonitoring)
                {
                    _result = (PurchaseRequisitionMonitoring)Activator.CreateInstance(typeof(PurchaseRequisitionMonitoring), _currSession);
                }
                else if (_objList == ObjectList.PurchaseSetupDetail)
                {
                    _result = (PurchaseSetupDetail)Activator.CreateInstance(typeof(PurchaseSetupDetail), _currSession);
                }
                #endregion GetBusinessObject P
                #region GetBusinessObject R
                else if (_objList == ObjectList.ReceivableOrderCollection)
                {
                    _result = (ReceivableOrderCollection)Activator.CreateInstance(typeof(ReceivableOrderCollection), _currSession);
                }
                else if (_objList == ObjectList.ReceivableTransaction)
                {
                    _result = (ReceivableTransaction)Activator.CreateInstance(typeof(ReceivableTransaction), _currSession);
                }
                else if (_objList == ObjectList.ReceivableTransactionLine)
                {
                    _result = (ReceivableTransactionLine)Activator.CreateInstance(typeof(ReceivableTransactionLine), _currSession);
                }
                else if (_objList == ObjectList.ReceivableTransactionMonitoring)
                {
                    _result = (ReceivableTransactionMonitoring)Activator.CreateInstance(typeof(ReceivableTransactionMonitoring), _currSession);
                }
                else if (_objList == ObjectList.RoundingSetupDetail)
                {
                    _result = (RoundingSetupDetail)Activator.CreateInstance(typeof(RoundingSetupDetail), _currSession);
                }
                else if (_objList == ObjectList.Route)
                {
                    _result = (Route)Activator.CreateInstance(typeof(Route), _currSession);
                }
                else if (_objList == ObjectList.RouteInvoice)
                {
                    _result = (RouteInvoice)Activator.CreateInstance(typeof(RouteInvoice), _currSession);
                }
                else if (_objList == ObjectList.RoutePlan)
                {
                    _result = (RoutePlan)Activator.CreateInstance(typeof(RoutePlan), _currSession);
                }
                #endregion GetBusinessObject R
                #region GetBusinessObject S
                else if (_objList == ObjectList.SalesArea)
                {
                    _result = (SalesArea)Activator.CreateInstance(typeof(SalesArea), _currSession);
                }
                else if (_objList == ObjectList.SalesArea1)
                {
                    _result = (SalesArea1)Activator.CreateInstance(typeof(SalesArea1), _currSession);
                }
                else if (_objList == ObjectList.SalesArea2)
                {
                    _result = (SalesArea2)Activator.CreateInstance(typeof(SalesArea2), _currSession);
                }
                else if (_objList == ObjectList.SalesArea3)
                {
                    _result = (SalesArea3)Activator.CreateInstance(typeof(SalesArea3), _currSession);
                }
                else if (_objList == ObjectList.SalesArea4)
                {
                    _result = (SalesArea4)Activator.CreateInstance(typeof(SalesArea4), _currSession);
                }
                else if (_objList == ObjectList.SalesAreaLine)
                {
                    _result = (SalesAreaLine)Activator.CreateInstance(typeof(SalesAreaLine), _currSession);
                }
                else if (_objList == ObjectList.SalesInvoice)
                {
                    _result = (SalesInvoice)Activator.CreateInstance(typeof(SalesInvoice), _currSession);
                }
                else if (_objList == ObjectList.SalesInvoiceCmp)
                {
                    _result = (SalesInvoiceCmp)Activator.CreateInstance(typeof(SalesInvoiceCmp), _currSession);
                }
                else if (_objList == ObjectList.SalesInvoiceCollection)
                {
                    _result = (SalesInvoiceCollection)Activator.CreateInstance(typeof(SalesInvoiceCollection), _currSession);
                }
                else if (_objList == ObjectList.SalesInvoiceFreeItem)
                {
                    _result = (SalesInvoiceFreeItem)Activator.CreateInstance(typeof(SalesInvoiceFreeItem), _currSession);
                }
                else if (_objList == ObjectList.SalesInvoiceFreeItemCmp)
                {
                    _result = (SalesInvoiceFreeItemCmp)Activator.CreateInstance(typeof(SalesInvoiceFreeItemCmp), _currSession);
                }
                else if (_objList == ObjectList.SalesInvoiceLine)
                {
                    _result = (SalesInvoiceLine)Activator.CreateInstance(typeof(SalesInvoiceLine), _currSession);
                }
                else if (_objList == ObjectList.SalesInvoiceLineCmp)
                {
                    _result = (SalesInvoiceLineCmp)Activator.CreateInstance(typeof(SalesInvoiceLineCmp), _currSession);
                }
                else if (_objList == ObjectList.SalesInvoiceMonitoring)
                {
                    _result = (SalesInvoiceMonitoring)Activator.CreateInstance(typeof(SalesInvoiceMonitoring), _currSession);
                }
                else if (_objList == ObjectList.SalesmanAreaSetup)
                {
                    _result = (SalesmanAreaSetup)Activator.CreateInstance(typeof(SalesmanAreaSetup), _currSession);
                }
                else if (_objList == ObjectList.SalesmanCustomerSetup)
                {
                    _result = (SalesmanCustomerSetup)Activator.CreateInstance(typeof(SalesmanCustomerSetup), _currSession);
                }
                else if (_objList == ObjectList.SalesmanLocationSetup)
                {
                    _result = (SalesmanLocationSetup)Activator.CreateInstance(typeof(SalesmanLocationSetup), _currSession);
                }
                else if (_objList == ObjectList.SalesmanVehicleSetup)
                {
                    _result = (SalesmanVehicleSetup)Activator.CreateInstance(typeof(SalesmanVehicleSetup), _currSession);
                }
                else if (_objList == ObjectList.SalesOrder)
                {
                    _result = (SalesOrder)Activator.CreateInstance(typeof(SalesOrder), _currSession);
                }
                else if (_objList == ObjectList.SalesOrderCollection)
                {
                    _result = (SalesOrderCollection)Activator.CreateInstance(typeof(SalesOrderCollection), _currSession);
                }
                else if (_objList == ObjectList.SalesOrderFreeItem)
                {
                    _result = (SalesOrderFreeItem)Activator.CreateInstance(typeof(SalesOrderFreeItem), _currSession);
                }
                else if (_objList == ObjectList.SalesOrderLine)
                {
                    _result = (SalesOrderLine)Activator.CreateInstance(typeof(SalesOrderLine), _currSession);
                }
                else if (_objList == ObjectList.SalesOrderMonitoring)
                {
                    _result = (SalesOrderMonitoring)Activator.CreateInstance(typeof(SalesOrderMonitoring), _currSession);
                }
                else if (_objList == ObjectList.SalesPromotionRule)
                {
                    _result = (SalesPromotionRule)Activator.CreateInstance(typeof(SalesPromotionRule), _currSession);
                }
                else if (_objList == ObjectList.ScheduleType)
                {
                    _result = (ScheduleType)Activator.CreateInstance(typeof(ScheduleType), _currSession);
                }
                else if (_objList == ObjectList.Section)
                {
                    _result = (Section)Activator.CreateInstance(typeof(Section), _currSession);
                }
                #endregion GetBusinessObject S
                #region GetBusinessObject T
                else if (_objList == ObjectList.TaskMode)
                {
                    _result = (TaskMode)Activator.CreateInstance(typeof(TaskMode), _currSession);
                }
                else if (_objList == ObjectList.Tax)
                {
                    _result = (Tax)Activator.CreateInstance(typeof(Tax), _currSession);
                }
                else if (_objList == ObjectList.TaxAccount)
                {
                    _result = (TaxAccount)Activator.CreateInstance(typeof(TaxAccount), _currSession);
                }
                else if (_objList == ObjectList.TaxAccountGroup)
                {
                    _result = (TaxAccountGroup)Activator.CreateInstance(typeof(TaxAccountGroup), _currSession);
                }
                else if (_objList == ObjectList.TaxGroup)
                {
                    _result = (TaxGroup)Activator.CreateInstance(typeof(TaxGroup), _currSession);
                }
                else if (_objList == ObjectList.TaxLine)
                {
                    _result = (TaxLine)Activator.CreateInstance(typeof(TaxLine), _currSession);
                }
                else if (_objList == ObjectList.TaxType)
                {
                    _result = (TaxType)Activator.CreateInstance(typeof(TaxType), _currSession);
                }
                else if (_objList == ObjectList.TermOfPayment)
                {
                    _result = (TermOfPayment)Activator.CreateInstance(typeof(TermOfPayment), _currSession);
                }
                else if (_objList == ObjectList.TerritoryLevel1)
                {
                    _result = (TerritoryLevel1)Activator.CreateInstance(typeof(TerritoryLevel1), _currSession);
                }
                else if (_objList == ObjectList.TerritoryLevel2)
                {
                    _result = (TerritoryLevel2)Activator.CreateInstance(typeof(TerritoryLevel2), _currSession);
                }
                else if (_objList == ObjectList.TerritoryLevel3)
                {
                    _result = (TerritoryLevel3)Activator.CreateInstance(typeof(TerritoryLevel3), _currSession);
                }
                else if (_objList == ObjectList.TerritoryLevel4)
                {
                    _result = (TerritoryLevel4)Activator.CreateInstance(typeof(TerritoryLevel4), _currSession);
                }
                else if (_objList == ObjectList.Transfer)
                {
                    _result = (Transfer)Activator.CreateInstance(typeof(Transfer), _currSession);
                }
                else if (_objList == ObjectList.TransferIn)
                {
                    _result = (TransferIn)Activator.CreateInstance(typeof(TransferIn), _currSession);
                }
                else if (_objList == ObjectList.TransferInLine)
                {
                    _result = (TransferInLine)Activator.CreateInstance(typeof(TransferInLine), _currSession);
                }
                else if (_objList == ObjectList.TransferInLot)
                {
                    _result = (TransferInLot)Activator.CreateInstance(typeof(TransferInLot), _currSession);
                }
                else if (_objList == ObjectList.TransferInMonitoring)
                {
                    _result = (TransferInMonitoring)Activator.CreateInstance(typeof(TransferInMonitoring), _currSession);
                }
                else if (_objList == ObjectList.TransferLine)
                {
                    _result = (TransferLine)Activator.CreateInstance(typeof(TransferLine), _currSession);
                }
                else if (_objList == ObjectList.TransferMonitoring)
                {
                    _result = (TransferMonitoring)Activator.CreateInstance(typeof(TransferMonitoring), _currSession);
                }
                else if (_objList == ObjectList.TransferOut)
                {
                    _result = (TransferOut)Activator.CreateInstance(typeof(TransferOut), _currSession);
                }
                else if (_objList == ObjectList.TransferOut)
                {
                    _result = (TransferOut)Activator.CreateInstance(typeof(TransferOut), _currSession);
                }
                else if (_objList == ObjectList.TransferOutCollection)
                {
                    _result = (TransferOutCollection)Activator.CreateInstance(typeof(TransferOutCollection), _currSession);
                }
                else if (_objList == ObjectList.TransferOutLine)
                {
                    _result = (TransferOutLine)Activator.CreateInstance(typeof(TransferOutLine), _currSession);
                }
                else if (_objList == ObjectList.TransferOutLot)
                {
                    _result = (TransferOutLot)Activator.CreateInstance(typeof(TransferOutLot), _currSession);
                }
                else if (_objList == ObjectList.TransferOutMonitoring)
                {
                    _result = (TransferOutMonitoring)Activator.CreateInstance(typeof(TransferOutMonitoring), _currSession);
                }
                
                #endregion GetBusinessObject T
                #region GetBusinessObject U
                else if (_objList == ObjectList.UnitOfMeasure)
                {
                    _result = (UnitOfMeasure)Activator.CreateInstance(typeof(UnitOfMeasure), _currSession);
                }
                else if (_objList == ObjectList.UnitOfMeasureType)
                {
                    _result = (UnitOfMeasureType)Activator.CreateInstance(typeof(UnitOfMeasureType), _currSession);
                }
                else if (_objList == ObjectList.UserAccess)
                {
                    _result = (UserAccess)Activator.CreateInstance(typeof(UserAccess), _currSession);
                }
                else if (_objList == ObjectList.UserAccessRole)
                {
                    _result = (UserAccessRole)Activator.CreateInstance(typeof(UserAccessRole), _currSession);
                }
                #endregion GetBusinessObject U
                #region GetBusinessObject V
                else if (_objList == ObjectList.Vehicle)
                {
                    _result = (Vehicle)Activator.CreateInstance(typeof(Vehicle), _currSession);
                }
                else if (_objList == ObjectList.VehicleList)
                {
                    _result = (VehicleList)Activator.CreateInstance(typeof(VehicleList), _currSession);
                }
                #endregion GetBusinessObject V
                #region GetBusinessObject W
                else if (_objList == ObjectList.WarehouseSetupDetail)
                {
                    _result = (WarehouseSetupDetail)Activator.CreateInstance(typeof(WarehouseSetupDetail), _currSession);
                }
                else if (_objList == ObjectList.Workplace)
                {
                    _result = (Workplace)Activator.CreateInstance(typeof(Workplace), _currSession);
                }
                #endregion GetBusinessObject W

            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Module = GlobalFunction " + ex.ToString());
            }
            return _result;
        }

        #endregion StandardImport

        #region AccountingPeriodic

        public void ImportDataExcelForAccountingPeriodic(Session _currSession, string _fullPath, string _ext, ObjectList _objList)
        {
            PropertyInfo[] _propInfo = null;
            object _newObject = null;
            IExcelDataReader excelReader;
            DataSet ds = new DataSet();
            Boolean _dataBool;
            try
            {
                FileStream stream = File.Open(_fullPath, FileMode.Open, FileAccess.Read);
                if (_ext == ".xlsx")
                {
                    //1. Reading from a binary Excel file ('97-2003 format; *.xls)
                    excelReader = ExcelReaderFactory.CreateOpenXmlReader(stream);
                }
                else
                {
                    //2. Reading from a OpenXml Excel file (2007 format; *.xlsx)
                    excelReader = ExcelReaderFactory.CreateBinaryReader(stream);
                }

                excelReader.IsFirstRowAsColumnNames = true;
                ds = excelReader.AsDataSet();
                DataTable dt = ds.Tables[0];
                _propInfo = GetPropInfoForAccountingPeriodic(_objList);
                excelReader.Close();

                if (_propInfo != null)
                {
                    foreach (DataRow dRow in dt.Rows)
                    {
                        _newObject = GetBusinessObjectForAccountingPeriodic(_currSession, _objList);
                        for (int i = 0; i <= dRow.ItemArray.Length - 1; i++)
                        {
                            for (int j = 0; j <= _propInfo.Length - 1; j++)
                            {
                                if (_propInfo[j].Name.ToLower().Replace(" ", "") == dt.Columns[i].ToString().Trim().ToLower().Replace(" ", ""))
                                {
                                    #region DefaultProperty

                                    if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(string)))
                                    {
                                        _propInfo[j].SetValue(_newObject, dRow[i].ToString(), null);
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(bool)))
                                    {
                                        if (dRow[i].ToString().Trim().ToLower() == "Checked".ToLower())
                                        {
                                            _dataBool = true;
                                        }
                                        else if (dRow[i].ToString().Trim().ToLower() == "TRUE".ToLower())
                                        {
                                            _dataBool = true;
                                        }
                                        else
                                        {
                                            _dataBool = false;
                                        }

                                        _propInfo[j].SetValue(_newObject, _dataBool, null);
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(decimal)))
                                    {
                                        _propInfo[j].SetValue(_newObject, Convert.ToDecimal(dRow[i].ToString()), null);
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(int)))
                                    {
                                        _propInfo[j].SetValue(_newObject, Convert.ToInt32(dRow[i].ToString()), null);
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(double)))
                                    {
                                        _propInfo[j].SetValue(_newObject, Convert.ToDouble(dRow[i].ToString()), null);
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(System.DateTime)))
                                    {
                                        if (CheckDate(dRow[i].ToString()))
                                        {
                                            _propInfo[j].SetValue(_newObject, Convert.ToDateTime(dRow[i].ToString()), null);
                                        }
                                    }
                                    #endregion DefaultProperty
                                    #region Import Excel For Object

                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(Company)))
                                    {
                                        if (dRow[i].ToString() != null)
                                        {
                                            Company _lineData = _currSession.FindObject<Company>
                                                (new BinaryOperator("Name", dRow[i].ToString()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(Workplace)))
                                    {
                                        if (dRow[i].ToString() != null)
                                        {
                                            Workplace _lineData = _currSession.FindObject<Workplace>
                                                (new BinaryOperator("Name", dRow[i].ToString()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(Division)))
                                    {
                                        if (dRow[i].ToString() != null)
                                        {
                                            Division _lineData = _currSession.FindObject<Division>
                                                (new BinaryOperator("Name", dRow[i].ToString()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(Department)))
                                    {
                                        if (dRow[i].ToString() != null)
                                        {
                                            Department _lineData = _currSession.FindObject<Department>
                                                (new BinaryOperator("Name", dRow[i].ToString()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(Section)))
                                    {
                                        if (dRow[i].ToString() != null)
                                        {
                                            Section _lineData = _currSession.FindObject<Section>
                                                (new BinaryOperator("Name", dRow[i].ToString()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(Employee)))
                                    {
                                        if (dRow[i].ToString() != null)
                                        {
                                            Employee _lineData = _currSession.FindObject<Employee>
                                                (new BinaryOperator("Name", dRow[i].ToString()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(ChartOfAccount)))
                                    {
                                        if (dRow[i].ToString() != null)
                                        {
                                            ChartOfAccount _lineData = _currSession.FindObject<ChartOfAccount>
                                                (new BinaryOperator("Name", dRow[i].ToString()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(AccountingPeriodic)))
                                    {
                                        if (dRow[i].ToString() != null)
                                        {
                                            AccountingPeriodic _lineData = _currSession.FindObject<AccountingPeriodic>
                                                (new BinaryOperator("Code", dRow[i].ToString()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }
                                    #endregion Import Excel For Object
                                }
                            }
                        }
                        _currSession.Save(_newObject);
                        _currSession.CommitTransaction();
                    }
                    //_currSession.CommitTransaction();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Module = GlobalFunction " + ex.ToString());
            }
        }

        private PropertyInfo[] GetPropInfoForAccountingPeriodic(ObjectList _objList)
        {
            PropertyInfo[] _result = null;
            try
            {
                #region PropertyInfo A
                if (_objList == ObjectList.AccountingPeriodicLine)
                {
                    _result = typeof(AccountingPeriodicLine).GetProperties();
                }
                #endregion PropertyInfo A
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Module = GlobalFunction " + ex.ToString());
            }
            return _result;
        }

        private object GetBusinessObjectForAccountingPeriodic(Session _currSession, ObjectList _objList)
        {
            object _result = null;
            try
            {
                #region GetBusinessObject A
                if (_objList == ObjectList.AccountingPeriodicLine)
                {
                    _result = (AccountingPeriodicLine)Activator.CreateInstance(typeof(AccountingPeriodicLine), _currSession);
                }

                #endregion GetBusinessObject A


            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Module = GlobalFunction " + ex.ToString());
            }
            return _result;
        }

        #endregion AccountingPeriodic

        #region InvoiceOutMonitoring

        public void ImportDataExcelForInvoiceOutMonitoring(Session _currSession, string _fullPath, string _ext, ObjectList _objList, Company _locCompany, Workplace _locWorkplace)
        {
            PropertyInfo[] _propInfo = null;
            object _newObject = null;
            IExcelDataReader excelReader;
            DataSet ds = new DataSet();
            Boolean _dataBool;
            try
            {
                FileStream stream = File.Open(_fullPath, FileMode.Open, FileAccess.Read);
                if (_ext == ".xlsx")
                {
                    //1. Reading from a binary Excel file ('97-2003 format; *.xls)
                    excelReader = ExcelReaderFactory.CreateOpenXmlReader(stream);
                }
                else
                {
                    //2. Reading from a OpenXml Excel file (2007 format; *.xlsx)
                    excelReader = ExcelReaderFactory.CreateBinaryReader(stream);
                }

                excelReader.IsFirstRowAsColumnNames = true;
                ds = excelReader.AsDataSet();
                DataTable dt = ds.Tables[0];
                _propInfo = GetPropInfoForInvoiceOutMonitoring(_objList);
                excelReader.Close();

                if (_propInfo != null)
                {
                    foreach (DataRow dRow in dt.Rows)
                    {
                        _newObject = GetBusinessObjectForInvoiceOutMonitoring(_currSession, _objList);
                        for (int i = 0; i <= dRow.ItemArray.Length - 1; i++)
                        {
                            for (int j = 0; j <= _propInfo.Length - 1; j++)
                            {
                                if (_propInfo[j].Name.ToLower().Replace(" ", "") == dt.Columns[i].ToString().Trim().ToLower().Replace(" ", ""))
                                {
                                    #region DefaultProperty

                                    if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(string)))
                                    {
                                        _propInfo[j].SetValue(_newObject, dRow[i].ToString(), null);
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(bool)))
                                    {
                                        if (dRow[i].ToString().Trim().ToLower() == "Checked".ToLower())
                                        {
                                            _dataBool = true;
                                        }
                                        else if (dRow[i].ToString().Trim().ToLower() == "TRUE".ToLower())
                                        {
                                            _dataBool = true;
                                        }
                                        else
                                        {
                                            _dataBool = false;
                                        }

                                        _propInfo[j].SetValue(_newObject, _dataBool, null);
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(decimal)))
                                    {
                                        _propInfo[j].SetValue(_newObject, Convert.ToDecimal(dRow[i].ToString()), null);
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(int)))
                                    {
                                        _propInfo[j].SetValue(_newObject, Convert.ToInt32(dRow[i].ToString()), null);
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(double)))
                                    {
                                        _propInfo[j].SetValue(_newObject, Convert.ToDouble(dRow[i].ToString()), null);
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(System.DateTime)))
                                    {
                                        if (CheckDate(dRow[i].ToString()))
                                        {
                                            _propInfo[j].SetValue(_newObject, Convert.ToDateTime(dRow[i].ToString()), null);
                                        }
                                    }
                                    #endregion DefaultProperty
                                    #region Import Excel For Object

                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(Company)))
                                    {
                                        if (dRow[i].ToString() != null)
                                        {
                                            Company _lineData = _currSession.FindObject<Company>
                                                (new BinaryOperator("Name", dRow[i].ToString()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(Workplace)))
                                    {
                                        if (dRow[i].ToString() != null)
                                        {
                                            Workplace _lineData = _currSession.FindObject<Workplace>
                                                (new GroupOperator(GroupOperatorType.And,
                                                new BinaryOperator("Company", _locCompany),
                                                new BinaryOperator("Name", dRow[i].ToString())));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(SalesInvoice)))
                                    {
                                        if (dRow[i].ToString() != null)
                                        {
                                            SalesInvoice _lineData = _currSession.FindObject<SalesInvoice>
                                                (new GroupOperator(GroupOperatorType.And,
                                                new BinaryOperator("Company", _locCompany),
                                                new BinaryOperator("Workplace", _locWorkplace),
                                                new BinaryOperator("Code", dRow[i].ToString()))
                                                );
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }

                                    #endregion Import Excel For Object
                                }
                            }
                        }
                        _currSession.Save(_newObject);
                        _currSession.CommitTransaction();
                    }
                    //_currSession.CommitTransaction();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Module = GlobalFunction " + ex.ToString());
            }
        }

        private PropertyInfo[] GetPropInfoForInvoiceOutMonitoring(ObjectList _objList)
        {
            PropertyInfo[] _result = null;
            try
            {
                #region PropertyInfo S
                if (_objList == ObjectList.SalesInvoice)
                {
                    _result = typeof(SalesInvoice).GetProperties();
                }
                else if (_objList == ObjectList.CustomerInvoice)
                {
                    _result = typeof(CustomerInvoice).GetProperties();
                }
                #endregion PropertyInfo S
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Module = GlobalFunction " + ex.ToString());
            }
            return _result;
        }

        private object GetBusinessObjectForInvoiceOutMonitoring(Session _currSession, ObjectList _objList)
        {
            object _result = null;
            try
            {
                #region GetBusinessObject A
                if (_objList == ObjectList.SalesInvoice)
                {
                    _result = (SalesInvoice)Activator.CreateInstance(typeof(SalesInvoice), _currSession);
                }
                else if (_objList == ObjectList.CustomerInvoice)
                {
                    _result = (CustomerInvoice)Activator.CreateInstance(typeof(CustomerInvoice), _currSession);
                }
                #endregion GetBusinessObject A
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Module = GlobalFunction " + ex.ToString());
            }
            return _result;
        }

        #endregion InvoiceOutMonitoring

        #region InvoiceInMonitoring

        public void ImportDataExcelForInvoiceInMonitoring(Session _currSession, string _fullPath, string _ext, ObjectList _objList, Company _locCompany, Workplace _locWorkplace)
        {
            PropertyInfo[] _propInfo = null;
            object _newObject = null;
            IExcelDataReader excelReader;
            DataSet ds = new DataSet();
            Boolean _dataBool;
            try
            {
                FileStream stream = File.Open(_fullPath, FileMode.Open, FileAccess.Read);
                if (_ext == ".xlsx")
                {
                    //1. Reading from a binary Excel file ('97-2003 format; *.xls)
                    excelReader = ExcelReaderFactory.CreateOpenXmlReader(stream);
                }
                else
                {
                    //2. Reading from a OpenXml Excel file (2007 format; *.xlsx)
                    excelReader = ExcelReaderFactory.CreateBinaryReader(stream);
                }

                excelReader.IsFirstRowAsColumnNames = true;
                ds = excelReader.AsDataSet();
                DataTable dt = ds.Tables[0];
                _propInfo = GetPropInfoForInvoiceInMonitoring(_objList);
                excelReader.Close();

                if (_propInfo != null)
                {
                    foreach (DataRow dRow in dt.Rows)
                    {
                        _newObject = GetBusinessObjectForInvoiceInMonitoring(_currSession, _objList);
                        for (int i = 0; i <= dRow.ItemArray.Length - 1; i++)
                        {
                            for (int j = 0; j <= _propInfo.Length - 1; j++)
                            {
                                if (_propInfo[j].Name.ToLower().Replace(" ", "") == dt.Columns[i].ToString().Trim().ToLower().Replace(" ", ""))
                                {
                                    #region DefaultProperty

                                    if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(string)))
                                    {
                                        _propInfo[j].SetValue(_newObject, dRow[i].ToString(), null);
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(bool)))
                                    {
                                        if (dRow[i].ToString().Trim().ToLower() == "Checked".ToLower())
                                        {
                                            _dataBool = true;
                                        }
                                        else if (dRow[i].ToString().Trim().ToLower() == "TRUE".ToLower())
                                        {
                                            _dataBool = true;
                                        }
                                        else
                                        {
                                            _dataBool = false;
                                        }

                                        _propInfo[j].SetValue(_newObject, _dataBool, null);
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(decimal)))
                                    {
                                        _propInfo[j].SetValue(_newObject, Convert.ToDecimal(dRow[i].ToString()), null);
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(int)))
                                    {
                                        _propInfo[j].SetValue(_newObject, Convert.ToInt32(dRow[i].ToString()), null);
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(double)))
                                    {
                                        _propInfo[j].SetValue(_newObject, Convert.ToDouble(dRow[i].ToString()), null);
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(System.DateTime)))
                                    {
                                        if (CheckDate(dRow[i].ToString()))
                                        {
                                            _propInfo[j].SetValue(_newObject, Convert.ToDateTime(dRow[i].ToString()), null);
                                        }
                                    }
                                    #endregion DefaultProperty
                                    #region Import Excel For Object

                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(Company)))
                                    {
                                        if (dRow[i].ToString() != null)
                                        {
                                            Company _lineData = _currSession.FindObject<Company>
                                                (new BinaryOperator("Name", dRow[i].ToString()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(Workplace)))
                                    {
                                        if (dRow[i].ToString() != null)
                                        {
                                            Workplace _lineData = _currSession.FindObject<Workplace>
                                                (new GroupOperator(GroupOperatorType.And,
                                                new BinaryOperator("Company", _locCompany),
                                                new BinaryOperator("Name", dRow[i].ToString())));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(SalesInvoice)))
                                    {
                                        if (dRow[i].ToString() != null)
                                        {
                                            SalesInvoice _lineData = _currSession.FindObject<SalesInvoice>
                                                (new GroupOperator(GroupOperatorType.And,
                                                new BinaryOperator("Company", _locCompany),
                                                new BinaryOperator("Workplace", _locWorkplace),
                                                new BinaryOperator("Code", dRow[i].ToString()))
                                                );
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }

                                    #endregion Import Excel For Object
                                }
                            }
                        }
                        _currSession.Save(_newObject);
                        _currSession.CommitTransaction();
                    }
                    //_currSession.CommitTransaction();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Module = GlobalFunction " + ex.ToString());
            }
        }

        private PropertyInfo[] GetPropInfoForInvoiceInMonitoring(ObjectList _objList)
        {
            PropertyInfo[] _result = null;
            try
            {
                #region PropertyInfo S
                if (_objList == ObjectList.PurchaseOrder)
                {
                    _result = typeof(PurchaseOrder).GetProperties();
                }
                #endregion PropertyInfo S
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Module = GlobalFunction " + ex.ToString());
            }
            return _result;
        }

        private object GetBusinessObjectForInvoiceInMonitoring(Session _currSession, ObjectList _objList)
        {
            object _result = null;
            try
            {
                #region GetBusinessObject A
                if (_objList == ObjectList.PurchaseOrder)
                {
                    _result = (PurchaseOrder)Activator.CreateInstance(typeof(PurchaseOrder), _currSession);
                }
                #endregion GetBusinessObject A
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Module = GlobalFunction " + ex.ToString());
            }
            return _result;
        }

        #endregion InvoiceInMonitoring

        #region Function For Enum

        public int GetObjectList(string _objectName)
        {
            int _result = 0;
            string _locObjectName = null;
            try
            {
                if (_objectName != null)
                {
                    _locObjectName = _objectName.Trim().Replace(" ", "").ToLower();
                    #region A
                    if (_locObjectName == "AccountGroup".ToLower())
                    {
                        _result = 1;
                    }
                    else if (_locObjectName == "AccountingPeriodic".ToLower())
                    {
                        _result = 2;
                    }
                    else if (_locObjectName == "AccountingPeriodicLine".ToLower())
                    {
                        _result = 3;
                    }
                    else if (_locObjectName == "AccountMap".ToLower())
                    {
                        _result = 4;
                    }
                    else if (_locObjectName == "AccountMapLine".ToLower())
                    {
                        _result = 5;
                    }
                    else if (_locObjectName == "AccountReclassJournal".ToLower())
                    {
                        _result = 6;
                    }
                    else if (_locObjectName == "AccountReclassJournalLine".ToLower())
                    {
                        _result = 7;
                    }
                    else if (_locObjectName == "AccountReclassJournalMonitoring".ToLower())
                    {
                        _result = 8;
                    }
                    else if (_locObjectName == "AdditionalAnalyze".ToLower())
                    {
                        _result = 9;
                    }
                    else if (_locObjectName == "AdditionalDashboard".ToLower())
                    {
                        _result = 10;
                    }
                    else if (_locObjectName == "AdditionalReport".ToLower())
                    {
                        _result = 11;
                    }
                    else if (_locObjectName == "Address".ToLower())
                    {
                        _result = 12;
                    }
                    else if (_locObjectName == "AdvanceAccountGroup".ToLower())
                    {
                        _result = 13;
                    }
                    else if (_locObjectName == "AmendDelivery".ToLower())
                    {
                        _result = 14;
                    }
                    
                    else if (_locObjectName == "AmendDeliveryLine".ToLower())
                    {
                        _result = 15;
                    }
                    else if (_locObjectName == "AmendDeliveryMonitoring".ToLower())
                    {
                        _result = 16;
                    }
                    else if (_locObjectName == "ApplicationImport".ToLower())
                    {
                        _result = 17;
                    }
                    else if (_locObjectName == "ApplicationSetup".ToLower())
                    {
                        _result = 18;
                    }
                    else if (_locObjectName == "ApplicationSetupDetail".ToLower())
                    {
                        _result = 19;
                    }
                    else if (_locObjectName == "ApprovalLine".ToLower())
                    {
                        _result = 20;
                    }
                    else if (_locObjectName == "Area".ToLower())
                    {
                        _result = 21;
                    }
                    #endregion A
                    #region B
                    else if (_locObjectName == "BankAccount".ToLower())
                    {
                        _result = 22;
                    }
                    else if (_locObjectName == "BankAccountGroup".ToLower())
                    {
                        _result = 23;
                    }
                    else if (_locObjectName == "BeginningInventory".ToLower())
                    {
                        _result = 24;
                    }
                    else if (_locObjectName == "BeginningInventoryLine".ToLower())
                    {
                        _result = 25;
                    }
                    else if (_locObjectName == "BinLocation".ToLower())
                    {
                        _result = 26;
                    }
                    else if (_locObjectName == "BinLocationSetupDetail".ToLower())
                    {
                        _result = 27;
                    }
                    else if (_locObjectName == "Brand".ToLower())
                    {
                        _result = 28;
                    }
                    else if (_locObjectName == "BusinessPartner".ToLower())
                    {
                        _result = 29;
                    }
                    else if (_locObjectName == "BusinessPartnerAccountGroup".ToLower())
                    {
                        _result = 30;
                    }
                    else if (_locObjectName == "BusinessPartnerGroup".ToLower())
                    {
                        _result = 31;
                    }
                    else if (_locObjectName == "BusinessPartnerPriceGroupSetup".ToLower())
                    {
                        _result = 32;
                    }
                    #endregion B
                    #region C
                    else if (_locObjectName == "CanvasPaymentIn".ToLower())
                    {
                        _result = 33;
                    }
                    else if (_locObjectName == "CanvassingCollection".ToLower())
                    {
                        _result = 34;
                    }
                    
                    else if (_locObjectName == "CanvassingCollectionFreeItem".ToLower())
                    {
                        _result = 35;
                    }
                    else if (_locObjectName == "CanvassingCollectionLine".ToLower())
                    {
                        _result = 36;
                    }
                    else if (_locObjectName == "CanvassingCollectionMonitoring".ToLower())
                    {
                        _result = 37;
                    }
                    else if (_locObjectName == "CashAdvance".ToLower())
                    {
                        _result = 38;
                    }
                    else if (_locObjectName == "CashAdvanceCollection".ToLower())
                    {
                        _result = 39;
                    }
                    else if (_locObjectName == "CashAdvanceLine".ToLower())
                    {
                        _result = 40;
                    }
                    else if (_locObjectName == "CashAdvanceMonitoring".ToLower())
                    {
                        _result = 41;
                    }
                    else if (_locObjectName == "CashAdvancePaymentPlan".ToLower())
                    {
                        _result = 42;
                    }
                    else if (_locObjectName == "CashAdvanceType".ToLower())
                    {
                        _result = 43;
                    }
                    else if (_locObjectName == "ChartOfAccount".ToLower())
                    {
                        _result = 44;
                    }
                    else if (_locObjectName == "City".ToLower())
                    {
                        _result = 45;
                    }
                    else if (_locObjectName == "Company".ToLower())
                    {
                        _result = 46;
                    }
                    else if (_locObjectName == "CompanyAccount".ToLower())
                    {
                        _result = 47;
                    }
                    else if (_locObjectName == "CompanyAccountGroup".ToLower())
                    {
                        _result = 48;
                    }
                    else if (_locObjectName == "Country".ToLower())
                    {
                        _result = 49;
                    }
                    else if (_locObjectName == "CreditLimitList".ToLower())
                    {
                        _result = 50;
                    }
                    else if (_locObjectName == "Currency".ToLower())
                    {
                        _result = 51;
                    }
                    else if (_locObjectName == "CurrencyRate".ToLower())
                    {
                        _result = 52;
                    }
                    else if (_locObjectName == "CustomerInvoice".ToLower())
                    {
                        _result = 53;
                    }
                    else if (_locObjectName == "CustomerInvoiceFreeItem".ToLower())
                    {
                        _result = 54;
                    }
                    else if (_locObjectName == "CustomerInvoiceLine".ToLower())
                    {
                        _result = 55;
                    }
                    else if (_locObjectName == "CustomerMap".ToLower())
                    {
                        _result = 56;
                    }
                    else if (_locObjectName == "CustomerOrder".ToLower())
                    {
                        _result = 57;
                    }
                    else if (_locObjectName == "CustomerOrderFreeItem".ToLower())
                    {
                        _result = 58;
                    }
                    else if (_locObjectName == "CustomerOrderLine".ToLower())
                    {
                        _result = 59;
                    }
                    #endregion C
                    #region D
                    else if (_locObjectName == "DeliveryOrder".ToLower())
                    {
                        _result = 60;
                    }
                    else if (_locObjectName == "DeliveryOrderLine".ToLower())
                    {
                        _result = 61;
                    }
                    else if (_locObjectName == "DeliveryOrderMonitoring".ToLower())
                    {
                        _result = 62;
                    }
                    else if (_locObjectName == "Department".ToLower())
                    {
                        _result = 63;
                    }
                    else if (_locObjectName == "Discount".ToLower())
                    {
                        _result = 64;
                    }
                    else if (_locObjectName == "DiscountAccountGroup".ToLower())
                    {
                        _result = 65;
                    }
                    else if (_locObjectName == "DiscountRule".ToLower())
                    {
                        _result = 66;
                    }
                    else if (_locObjectName == "DiscountRuleType".ToLower())
                    {
                        _result = 67;
                    }
                    else if (_locObjectName == "Division".ToLower())
                    {
                        _result = 68;
                    }
                    else if (_locObjectName == "DocumentType".ToLower())
                    {
                        _result = 69;
                    }
                    #endregion D
                    #region E
                    else if (_locObjectName == "Employee".ToLower())
                    {
                        _result = 70;
                    }
                    else if (_locObjectName == "EmployeeDocumentTypeSetup".ToLower())
                    {
                        _result = 71;
                    }
                    else if (_locObjectName == "ExchangeRate".ToLower())
                    {
                        _result = 72;
                    }
                    #endregion E
                    #region F
                    else if (_locObjectName == "FreeItemRule".ToLower())
                    {
                        _result = 73;
                    }
                    else if (_locObjectName == "FreeItemRuleType".ToLower())
                    {
                        _result = 74;
                    }
                    #endregion F
                    #region G
                    else if (_locObjectName == "GeneralJournal".ToLower())
                    {
                        _result = 75;
                    }
                    #endregion G
                    #region I
                    else if (_locObjectName == "IndustrialType".ToLower())
                    {
                        _result = 76;
                    }
                    else if (_locObjectName == "InventoryJournal".ToLower())
                    {
                        _result = 77;
                    }
                    else if (_locObjectName == "InventoryPurchaseCollection".ToLower())
                    {
                        _result = 78;
                    }
                    else if (_locObjectName == "InventoryTransferIn".ToLower())
                    {
                        _result = 79;
                    }
                    else if (_locObjectName == "InventoryTransferInLine".ToLower())
                    {
                        _result = 80;
                    }
                    else if (_locObjectName == "InventoryTransferInLot".ToLower())
                    {
                        _result = 81;
                    }
                    else if (_locObjectName == "InventoryTransferInMonitoring".ToLower())
                    {
                        _result = 82;
                    }
                    else if (_locObjectName == "InventoryTransferOut".ToLower())
                    {
                        _result = 83;
                    }
                    else if (_locObjectName == "InventoryTransferOutLine".ToLower())
                    {
                        _result = 84;
                    }
                    else if (_locObjectName == "InventoryTransferOutLot".ToLower())
                    {
                        _result = 85;
                    }
                    else if (_locObjectName == "InvoiceCanvassing".ToLower())
                    {
                        _result = 86;
                    }
                    else if (_locObjectName == "InvoiceCanvassingCollection".ToLower())
                    {
                        _result = 87;
                    }
                    else if (_locObjectName == "InvoiceCanvassingFreeItem".ToLower())
                    {
                        _result = 88;
                    }
                    else if (_locObjectName == "InvoiceCanvassingLine".ToLower())
                    {
                        _result = 89;
                    }
                    else if (_locObjectName == "InvoiceCanvassingMonitoring".ToLower())
                    {
                        _result = 90;
                    }
                    else if (_locObjectName == "InvoiceInMonitoring".ToLower())
                    {
                        _result = 91;
                    }
                    else if (_locObjectName == "InvoiceInMonitoringLine".ToLower())
                    {
                        _result = 92;
                    }
                    else if (_locObjectName == "InvoiceOutMonitoring".ToLower())
                    {
                        _result = 93;
                    }
                    else if (_locObjectName == "InvoiceOutMonitoringLine".ToLower())
                    {
                        _result = 94;
                    }
                    else if (_locObjectName == "InvoicePurchaseCollection".ToLower())
                    {
                        _result = 95;
                    }
                    else if (_locObjectName == "Item".ToLower())
                    {
                        _result = 96;
                    }
                    else if (_locObjectName == "ItemAccount".ToLower())
                    {
                        _result = 97;
                    }
                    else if (_locObjectName == "ItemAccountGroup".ToLower())
                    {
                        _result = 98;
                    }
                    else if (_locObjectName == "ItemComponent".ToLower())
                    {
                        _result = 99;
                    }
                    else if (_locObjectName == "ItemGroup".ToLower())
                    {
                        _result = 100;
                    }
                    else if (_locObjectName == "ItemReclassJournal".ToLower())
                    {
                        _result = 101;
                    }
                    else if (_locObjectName == "ItemReclassJournalLine".ToLower())
                    {
                        _result = 102;
                    }
                    else if (_locObjectName == "ItemReclassJournalMonitoring".ToLower())
                    {
                        _result = 103;
                    }
                    else if (_locObjectName == "ItemUnitOfMeasure".ToLower())
                    {
                        _result = 104;
                    }
                    #endregion I
                    #region J
                    else if (_locObjectName == "JournalMap".ToLower())
                    {
                        _result = 105;
                    }
                    else if (_locObjectName == "JournalMapLine".ToLower())
                    {
                        _result = 106;
                    }
                    else if (_locObjectName == "JournalSetupDetail".ToLower())
                    {
                        _result = 107;
                    }
                    #endregion J
                    #region L
                    else if (_locObjectName == "ListImport".ToLower())
                    {
                        _result = 108;
                    }
                    else if (_locObjectName == "Location".ToLower())
                    {
                        _result = 109;
                    }
                    #endregion L
                    #region M
                    else if (_locObjectName == "MailSetupDetail".ToLower())
                    {
                        _result = 110;
                    }
                    #endregion M
                    #region N
                    else if (_locObjectName == "NumberingHeader".ToLower())
                    {
                        _result = 111;
                    }
                    else if (_locObjectName == "NumberingLine".ToLower())
                    {
                        _result = 112;
                    }
                    #endregion N
                    #region O
                    else if (_locObjectName == "OrderCollection".ToLower())
                    {
                        _result = 113;
                    }
                    else if (_locObjectName == "OrderCollectionLine".ToLower())
                    {
                        _result = 114;
                    }
                    else if (_locObjectName == "OrderCollectionMonitoring".ToLower())
                    {
                        _result = 115;
                    }
                    else if (_locObjectName == "OrganizationAccountGroup".ToLower())
                    {
                        _result = 116;
                    }
                    else if (_locObjectName == "OrganizationSetupDetail".ToLower())
                    {
                        _result = 117;
                    }
                    #endregion O
                    #region P
                    else if (_locObjectName == "PayablePurchaseCollection".ToLower())
                    {
                        _result = 118;
                    }
                    else if (_locObjectName == "PayableTransaction".ToLower())
                    {
                        _result = 119;
                    }
                    else if (_locObjectName == "PayableTransactionLine".ToLower())
                    {
                        _result = 120;
                    }
                    else if (_locObjectName == "PayableTransactionMonitoring".ToLower())
                    {
                        _result = 121;
                    }
                    else if (_locObjectName == "PaymentInPlan".ToLower())
                    {
                        _result = 122;
                    }
                    else if (_locObjectName == "PaymentMethod".ToLower())
                    {
                        _result = 123;
                    }
                    else if (_locObjectName == "PaymentOutPlan".ToLower())
                    {
                        _result = 124;
                    }
                    else if (_locObjectName == "PaymentRealization".ToLower())
                    {
                        _result = 125;
                    }
                    else if (_locObjectName == "PaymentRealizationLine".ToLower())
                    {
                        _result = 126;
                    }
                    else if (_locObjectName == "PaymentRealizationMonitoring".ToLower())
                    {
                        _result = 127;
                    }
                    else if (_locObjectName == "PhoneNumber".ToLower())
                    {
                        _result = 128;
                    }
                    else if (_locObjectName == "PhoneType".ToLower())
                    {
                        _result = 129;
                    }
                    else if (_locObjectName == "Position".ToLower())
                    {
                        _result = 130;
                    }
                    else if (_locObjectName == "Price".ToLower())
                    {
                        _result = 131;
                    }
                    else if (_locObjectName == "PriceCostMethod".ToLower())
                    {
                        _result = 132;
                    }
                    else if (_locObjectName == "PriceGroup".ToLower())
                    {
                        _result = 133;
                    }
                    else if (_locObjectName == "PriceLine".ToLower())
                    {
                        _result = 134;
                    }
                    else if (_locObjectName == "PurchaseInvoice".ToLower())
                    {
                        _result = 135;
                    }
                    else if (_locObjectName == "PurchaseInvoiceLine".ToLower())
                    {
                        _result = 136;
                    }
                    else if (_locObjectName == "PurchaseInvoiceMonitoring".ToLower())
                    {
                        _result = 137;
                    }
                    else if (_locObjectName == "PurchaseOrder".ToLower())
                    {
                        _result = 138;
                    }
                    else if (_locObjectName == "PurchaseOrderLine".ToLower())
                    {
                        _result = 139;
                    }
                    else if (_locObjectName == "PurchaseOrderMonitoring".ToLower())
                    {
                        _result = 140;
                    }
                    else if (_locObjectName == "PurchasePrePaymentInvoice".ToLower())
                    {
                        _result = 141;
                    }
                    else if (_locObjectName == "PurchasePrePaymentInvoiceMonitoring".ToLower())
                    {
                        _result = 142;
                    }
                    else if (_locObjectName == "PurchaseRequisition".ToLower())
                    {
                        _result = 143;
                    }
                    else if (_locObjectName == "PurchaseRequisitionCollection".ToLower())
                    {
                        _result = 144;
                    }
                    else if (_locObjectName == "PurchaseRequisitionLine".ToLower())
                    {
                        _result = 145;
                    }
                    else if (_locObjectName == "PurchaseRequisitionMonitoring".ToLower())
                    {
                        _result = 146;
                    }
                    else if (_locObjectName == "PurchaseSetupDetail".ToLower())
                    {
                        _result = 147;
                    }
                    #endregion P
                    #region R
                    else if (_locObjectName == "ReceivableOrderCollection".ToLower())
                    {
                        _result = 148;
                    }
                    else if (_locObjectName == "ReceivableTransaction".ToLower())
                    {
                        _result = 149;
                    }
                    else if (_locObjectName == "ReceivableTransactionLine".ToLower())
                    {
                        _result = 150;
                    }
                    else if (_locObjectName == "ReceivableTransactionMonitoring".ToLower())
                    {
                        _result = 151;
                    }
                    else if (_locObjectName == "RoundingSetupDetail".ToLower())
                    {
                        _result = 152;
                    }
                    else if (_locObjectName == "Route".ToLower())
                    {
                        _result = 153;
                    }
                    else if (_locObjectName == "RouteInvoice".ToLower())
                    {
                        _result = 154;
                    }
                    else if (_locObjectName == "RoutePlan".ToLower())
                    {
                        _result = 155;
                    }
                    #endregion R
                    #region S
                    else if (_locObjectName == "SalesArea".ToLower())
                    {
                        _result = 156;
                    }
                    else if (_locObjectName == "SalesArea1".ToLower())
                    {
                        _result = 157;
                    }
                    else if (_locObjectName == "SalesArea2".ToLower())
                    {
                        _result = 158;
                    }
                    else if (_locObjectName == "SalesArea3".ToLower())
                    {
                        _result = 159;
                    }
                    else if (_locObjectName == "SalesArea4".ToLower())
                    {
                        _result = 160;
                    }
                    else if (_locObjectName == "SalesAreaLine".ToLower())
                    {
                        _result = 161;
                    }
                    else if (_locObjectName == "SalesInvoice".ToLower())
                    {
                        _result = 162;
                    }
                    else if (_locObjectName == "SalesInvoiceCmp".ToLower())
                    {
                        _result = 163;
                    }
                    else if (_locObjectName == "SalesInvoiceCollection".ToLower())
                    {
                        _result = 164;
                    }
                    else if (_locObjectName == "SalesInvoiceFreeItem".ToLower())
                    {
                        _result = 165;
                    }
                    else if (_locObjectName == "SalesInvoiceFreeItemCmp".ToLower())
                    {
                        _result = 166;
                    }
                    else if (_locObjectName == "SalesInvoiceLine".ToLower())
                    {
                        _result = 167;
                    }
                    else if (_locObjectName == "SalesInvoiceLineCmp".ToLower())
                    {
                        _result = 168;
                    }
                    else if (_locObjectName == "SalesInvoiceMonitoring".ToLower())
                    {
                        _result = 169;
                    }
                    else if (_locObjectName == "SalesmanAreaSetup".ToLower())
                    {
                        _result = 170;
                    }
                    else if (_locObjectName == "SalesmanCustomerSetup".ToLower())
                    {
                        _result = 171;
                    }
                    else if (_locObjectName == "SalesmanLocationSetup".ToLower())
                    {
                        _result = 172;
                    }
                    else if (_locObjectName == "SalesmanVehicleSetup".ToLower())
                    {
                        _result = 173;
                    }
                    else if (_locObjectName == "SalesOrder".ToLower())
                    {
                        _result = 174;
                    }
                    else if (_locObjectName == "SalesOrderCollection".ToLower())
                    {
                        _result = 175;
                    }
                    else if (_locObjectName == "SalesOrderFreeItem".ToLower())
                    {
                        _result = 176;
                    }
                    else if (_locObjectName == "SalesOrderLine".ToLower())
                    {
                        _result = 177;
                    }
                    else if (_locObjectName == "SalesOrderMonitoring".ToLower())
                    {
                        _result = 178;
                    }
                    else if (_locObjectName == "SalesPromotionRule".ToLower())
                    {
                        _result = 179;
                    }
                    else if (_locObjectName == "ScheduleType".ToLower())
                    {
                        _result = 180;
                    }
                    else if (_locObjectName == "Section".ToLower())
                    {
                        _result = 181;
                    }
                    #endregion S
                    #region T
                    else if (_locObjectName == "TaskMode".ToLower())
                    {
                        _result = 182;
                    }
                    else if (_locObjectName == "Tax".ToLower())
                    {
                        _result = 183;
                    }
                    else if (_locObjectName == "TaxAccount".ToLower())
                    {
                        _result = 184;
                    }
                    else if (_locObjectName == "TaxAccountGroup".ToLower())
                    {
                        _result = 185;
                    }
                    else if (_locObjectName == "TaxGroup".ToLower())
                    {
                        _result = 186;
                    }
                    else if (_locObjectName == "TaxLine".ToLower())
                    {
                        _result = 187;
                    }
                    else if (_locObjectName == "TaxType".ToLower())
                    {
                        _result = 188;
                    }
                    else if (_locObjectName == "TermOfPayment".ToLower())
                    {
                        _result = 189;
                    }
                    else if (_locObjectName == "TerritoryLevel1".ToLower())
                    {
                        _result = 190;
                    }
                    else if (_locObjectName == "TerritoryLevel2".ToLower())
                    {
                        _result = 191;
                    }
                    else if (_locObjectName == "TerritoryLevel3".ToLower())
                    {
                        _result = 192;
                    }
                    else if (_locObjectName == "TerritoryLevel4".ToLower())
                    {
                        _result = 193;
                    }
                    else if (_locObjectName == "Transfer".ToLower())
                    {
                        _result = 194;
                    }
                    else if (_locObjectName == "TransferIn".ToLower())
                    {
                        _result = 195;
                    }
                    else if (_locObjectName == "TransferInLine".ToLower())
                    {
                        _result = 196;
                    }
                    else if (_locObjectName == "TransferInLot".ToLower())
                    {
                        _result = 197;
                    }
                    else if (_locObjectName == "TransferInMonitoring".ToLower())
                    {
                        _result = 198;
                    }
                    else if (_locObjectName == "TransferLine".ToLower())
                    {
                        _result = 199;
                    }
                    else if (_locObjectName == "TransferMonitoring".ToLower())
                    {
                        _result = 200;
                    }
                    else if (_locObjectName == "TransferOut".ToLower())
                    {
                        _result = 201;
                    }
                    else if (_locObjectName == "TransferOutCollection".ToLower())
                    {
                        _result = 202;
                    }
                    else if (_locObjectName == "TransferOutLine".ToLower())
                    {
                        _result = 203;
                    }
                    else if (_locObjectName == "TransferOutLot".ToLower())
                    {
                        _result = 204;
                    }
                    else if (_locObjectName == "TransferOutMonitoring".ToLower())
                    {
                        _result = 205;
                    }
                    #endregion T
                    #region U
                    else if (_locObjectName == "UnitOfMeasure".ToLower())
                    {
                        _result = 206;
                    }
                    else if (_locObjectName == "UnitOfMeasureType".ToLower())
                    {
                        _result = 207;
                    }
                    else if (_locObjectName == "UserAccess".ToLower())
                    {
                        _result = 208;
                    }
                    else if (_locObjectName == "UserAccessRole".ToLower())
                    {
                        _result = 209;
                    }
                    #endregion U
                    #region V
                    else if (_locObjectName == "Vehicle".ToLower())
                    {
                        _result = 210;
                    }
                    else if (_locObjectName == "VehicleList".ToLower())
                    {
                        _result = 211;
                    }
                    #endregion V
                    #region W
                    else if (_locObjectName == "WarehouseSetupDetail".ToLower())
                    {
                        _result = 212;
                    }
                    else if (_locObjectName == "Workplace".ToLower())
                    {
                        _result = 213;
                    }
                    #endregion W 
                    #region Additional
                    else if (_locObjectName == "Picking".ToLower())
                    {
                        _result = 214;
                    }
                    else if (_locObjectName == "PickingLine".ToLower())
                    {
                        _result = 215;
                    }
                    else if (_locObjectName == "PickingMonitoring".ToLower())
                    {
                        _result = 216;
                    }
                    else if (_locObjectName == "TransferOrder".ToLower())
                    {
                        _result = 217;
                    }
                    else if (_locObjectName == "TransferOrderLine".ToLower())
                    {
                        _result = 218;
                    }
                    else if (_locObjectName == "SalesInvoiceCollection2".ToLower())
                    {
                        _result = 219;
                    }
                    #endregion Additional
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Module = GlobalFunction " + ex.ToString());
            }
            return _result;
        }

        public int GetAccountCharge(string _objectName)
        {
            int _result = 0;
            string _locObjectName = null;
            try
            {
                if (_objectName != null)
                {
                    _locObjectName = _objectName.Trim().Replace(" ", "").ToLower();
                    if (_locObjectName == "Debit".ToLower())
                    {
                        _result = 1;
                    }
                    else if (_locObjectName == "Credit".ToLower())
                    {
                        _result = 2;
                    }
                    else if (_locObjectName == "Both".ToLower())
                    {
                        _result = 3;
                    }
                    else
                    {
                        _result = 0;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Module = GlobalFunction " + ex.ToString());
            }
            return _result;
        }

        public int GetAccountMode(string _objectName)
        {
            int _result = 0;
            string _locObjectName = null;
            try
            {
                if (_objectName != null)
                {
                    _locObjectName = _objectName.Trim().Replace(" ", "").ToLower();
                    if (_locObjectName == "Customer".ToLower())
                    {
                        _result = 1;
                    }
                    else if (_locObjectName == "Vendor".ToLower())
                    {
                        _result = 2;
                    }
                    else if (_locObjectName == "BankAccount".ToLower())
                    {
                        _result = 3;
                    }
                    else if (_locObjectName == "Others".ToLower())
                    {
                        _result = 4;
                    }
                    else
                    {
                        _result = 0;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Module = GlobalFunction " + ex.ToString());
            }
            return _result;
        }

        public int GetAccountType(string _objectName)
        {
            int _result = 0;
            string _locObjectName = null;
            try
            {
                if (_objectName != null)
                {
                    _locObjectName = _objectName.Trim().Replace(" ", "").ToLower();
                    if (_locObjectName == "Posting".ToLower())
                    {
                        _result = 1;
                    }
                    else if (_locObjectName == "Heading".ToLower())
                    {
                        _result = 2;
                    }
                    else if (_locObjectName == "Total".ToLower())
                    {
                        _result = 3;
                    }
                    else if (_locObjectName == "BeginTotal".ToLower())
                    {
                        _result = 4;
                    }
                    else if (_locObjectName == "EndTotal".ToLower())
                    {
                        _result = 5;
                    }
                    else
                    {
                        _result = 0;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Module = GlobalFunction " + ex.ToString());
            }
            return _result;
        }

        public int GetApprovalFilter(string _objectName)
        {
            int _result = 0;
            string _locObjectName = null;
            try
            {
                if (_objectName != null)
                {
                    _locObjectName = _objectName.Trim().Replace(" ", "").ToLower();
                    if (_locObjectName == "Approval".ToLower())
                    {
                        _result = 1;
                    }
                    else if (_locObjectName == "EndApproval".ToLower())
                    {
                        _result = 2;
                    }
                    else
                    {
                        _result = 0;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Module = GlobalFunction " + ex.ToString());
            }
            return _result;
        }

        public int GetApprovalLevel(string _objectName)
        {
            int _result = 0;
            string _locObjectName = null;
            try
            {
                if (_objectName != null)
                {
                    _locObjectName = _objectName.Trim().Replace(" ", "").ToLower();
                    if (_locObjectName == "Level1".ToLower())
                    {
                        _result = 1;
                    }
                    else if (_locObjectName == "Level2".ToLower())
                    {
                        _result = 2;
                    }
                    else if (_locObjectName == "Level3".ToLower())
                    {
                        _result = 3;
                    }
                    else if (_locObjectName == "Level4".ToLower())
                    {
                        _result = 4;
                    }
                    else if (_locObjectName == "Level5".ToLower())
                    {
                        _result = 5;
                    }
                    else if (_locObjectName == "Level6".ToLower())
                    {
                        _result = 6;
                    }
                    else
                    {
                        _result = 0;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Module = GlobalFunction " + ex.ToString());
            }
            return _result;
        }

        public int GetBalanceType(string _objectName)
        {
            int _result = 0;
            string _locObjectName = null;
            try
            {
                if (_objectName != null)
                {
                    _locObjectName = _objectName.Trim().Replace(" ", "").ToLower();
                    if (_locObjectName == "Change".ToLower())
                    {
                        _result = 1;
                    }
                    else if (_locObjectName == "Fix".ToLower())
                    {
                        _result = 2;
                    }
                    else if (_locObjectName == "Other".ToLower())
                    {
                        _result = 3;
                    }
                    else
                    {
                        _result = 0;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Module = GlobalFunction " + ex.ToString());
            }
            return _result;
        }

        public int GetBusinessPartnerType(string _objectName)
        {
            int _result = 0;
            string _locObjectName = null;
            try
            {
                if (_objectName != null)
                {
                    _locObjectName = _objectName.Trim().Replace(" ", "").ToLower();
                    if (_locObjectName == "All".ToLower())
                    {
                        _result = 1;
                    }
                    else if (_locObjectName == "Customer".ToLower())
                    {
                        _result = 2;
                    }
                    else if (_locObjectName == "Vendor".ToLower())
                    {
                        _result = 3;
                    }
                    else
                    {
                        _result = 0;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Module = GlobalFunction " + ex.ToString());
            }
            return _result;
        }

        public int GetDeliveryStatus(string _objectName)
        {
            int _result = 0;
            string _locObjectName = null;
            try
            {
                if (_objectName != null)
                {
                    _locObjectName = _objectName.Trim().Replace(" ", "").ToLower();
                    if (_locObjectName == "Cancel".ToLower())
                    {
                        _result = 1;
                    }
                    else if (_locObjectName == "DeliveryAll".ToLower())
                    {
                        _result = 2;
                    }
                    else if (_locObjectName == "Loading".ToLower())
                    {
                        _result = 3;
                    }
                    else if (_locObjectName == "Loaded".ToLower())
                    {
                        _result = 4;
                    }
                    else if (_locObjectName == "Return".ToLower())
                    {
                        _result = 5;
                    }
                    else
                    {
                        _result = 0;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Module = GlobalFunction " + ex.ToString());
            }
            return _result;
        }

        public int GetDirectionRule(string _objectName)
        {
            int _result = 0;
            string _locObjectName = null;
            try
            {
                if (_objectName != null)
                {
                    _locObjectName = _objectName.Trim().Replace(" ", "").ToLower();
                    if (_locObjectName == "Direct".ToLower())
                    {
                        _result = 1;
                    }
                    else if (_locObjectName == "Indirect".ToLower())
                    {
                        _result = 2;
                    }
                    else
                    {
                        _result = 0;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Module = GlobalFunction " + ex.ToString());
            }
            return _result;
        }

        public int GetDirectionType(string _objectName)
        {
            int _result = 0;
            string _locObjectName = null;
            try
            {
                if (_objectName != null)
                {
                    _locObjectName = _objectName.Trim().Replace(" ", "").ToLower();
                    if (_locObjectName == "Internal".ToLower())
                    {
                        _result = 1;
                    }
                    else if (_locObjectName == "External".ToLower())
                    {
                        _result = 2;
                    }
                    else
                    {
                        _result = 0;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Module = GlobalFunction " + ex.ToString());
            }
            return _result;
        }

        public int GetDiscountMode(string _objectName)
        {
            int _result = 0;
            string _locObjectName = null;
            try
            {
                if (_objectName != null)
                {
                    _locObjectName = _objectName.Trim().Replace(" ", "").ToLower();
                    if (_locObjectName == "Purchase".ToLower())
                    {
                        _result = 1;
                    }
                    else if (_locObjectName == "Sales".ToLower())
                    {
                        _result = 2;
                    }
                    else if (_locObjectName == "Both".ToLower())
                    {
                        _result = 3;
                    }
                    else
                    {
                        _result = 0;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Module = GlobalFunction " + ex.ToString());
            }
            return _result;
        }

        public int GetDiscountType(string _objectName)
        {
            int _result = 0;
            string _locObjectName = null;
            try
            {
                if (_objectName != null)
                {
                    _locObjectName = _objectName.Trim().Replace(" ", "").ToLower();
                    if (_locObjectName == "Percentage".ToLower())
                    {
                        _result = 1;
                    }
                    else if (_locObjectName == "Value".ToLower())
                    {
                        _result = 2;
                    }
                    else
                    {
                        _result = 0;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Module = GlobalFunction " + ex.ToString());
            }
            return _result;
        }

        public int GetDocumentRule(string _objectName)
        {
            int _result = 0;
            string _locObjectName = null;
            try
            {
                if (_objectName != null)
                {
                    _locObjectName = _objectName.Trim().Replace(" ", "").ToLower();
                    if (_locObjectName == "Customer".ToLower())
                    {
                        _result = 1;
                    }
                    else if (_locObjectName == "Vendor".ToLower())
                    {
                        _result = 2;
                    }
                    else
                    {
                        _result = 0;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Module = GlobalFunction " + ex.ToString());
            }
            return _result;
        }

        public int GetFieldName(string _objectName)
        {
            int _result = 0;
            string _locObjectName = null;
            try
            {
                if (_objectName != null)
                {
                    _locObjectName = _objectName.Trim().Replace(" ", "").ToLower();
                    if (_locObjectName == "MxUAmount".ToLower())
                    {
                        _result = 1;
                    }
                    else if (_locObjectName == "MxTUAmount".ToLower())
                    {
                        _result = 2;
                    }
                    else if (_locObjectName == "UAmount".ToLower())
                    {
                        _result = 3;
                    }
                    else if (_locObjectName == "TUAmount".ToLower())
                    {
                        _result = 4;
                    }
                    else if (_locObjectName == "TxAmount".ToLower())
                    {
                        _result = 5;
                    }
                    else if (_locObjectName == "DiscAmount".ToLower())
                    {
                        _result = 6;
                    }
                    else if (_locObjectName == "TAmount".ToLower())
                    {
                        _result = 7;
                    }
                    else if (_locObjectName == "TotalUnitAmount".ToLower())
                    {
                        _result = 8;
                    }
                    else if (_locObjectName == "UnitAmount".ToLower())
                    {
                        _result = 9;
                    }
                    else if (_locObjectName == "Amount1a".ToLower())
                    {
                        _result = 10;
                    }
                    else if (_locObjectName == "Amount1b".ToLower())
                    {
                        _result = 11;
                    }
                    else if (_locObjectName == "Amount2a".ToLower())
                    {
                        _result = 12;
                    }
                    else if (_locObjectName == "Amount2b".ToLower())
                    {
                        _result = 13;
                    }
                    else
                    {
                        _result = 0;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Module = GlobalFunction " + ex.ToString());
            }
            return _result;
        }

        public int GetFinancialStatment(string _objectName)
        {
            int _result = 0;
            string _locObjectName = null;
            try
            {
                if (_objectName != null)
                {
                    _locObjectName = _objectName.Trim().Replace(" ", "").ToLower();
                    if (_locObjectName == "BalanceSheet".ToLower())
                    {
                        _result = 1;
                    }
                    else if (_locObjectName == "IncomeStatment".ToLower())
                    {
                        _result = 2;
                    }
                    else
                    {
                        _result = 0;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Module = GlobalFunction " + ex.ToString());
            }
            return _result;
        }

        public int GetFunctionList(string _objectName)
        {
            int _result = 0;
            string _locObjectName = null;
            try
            {
                if (_objectName != null)
                {
                    _locObjectName = _objectName.Trim().Replace(" ", "").ToLower();
                    if (_locObjectName == "Approval".ToLower())
                    {
                        _result = 1;
                    }
                    else if (_locObjectName == "Posting".ToLower())
                    {
                        _result = 2;
                    }
                    else if (_locObjectName == "Progress".ToLower())
                    {
                        _result = 3;
                    }
                    else
                    {
                        _result = 0;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Module = GlobalFunction " + ex.ToString());
            }
            return _result;
        }

        public int GetFreeItemType1(string _objectName)
        {
            int _result = 0;
            string _locObjectName = null;
            try
            {
                if (_objectName != null)
                {
                    _locObjectName = _objectName.Trim().Replace(" ", "").ToLower();
                    if (_locObjectName == "Header".ToLower())
                    {
                        _result = 1;
                    }
                    else if (_locObjectName == "Line".ToLower())
                    {
                        _result = 2;
                    }
                    else
                    {
                        _result = 0;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Module = GlobalFunction " + ex.ToString());
            }
            return _result;
        }

        public int GetInventoryMovingType(string _objectName)
        {
            int _result = 0;
            string _locObjectName = null;
            try
            {
                if (_objectName != null)
                {
                    _locObjectName = _objectName.Trim().Replace(" ", "").ToLower();
                    if (_locObjectName == "Receive".ToLower())
                    {
                        _result = 1;
                    }
                    else if (_locObjectName == "Deliver".ToLower())
                    {
                        _result = 2;
                    }
                    else if (_locObjectName == "TransferIn".ToLower())
                    {
                        _result = 3;
                    }
                    else if (_locObjectName == "TransferOut".ToLower())
                    {
                        _result = 4;
                    }
                    else if (_locObjectName == "Adjust".ToLower())
                    {
                        _result = 5;
                    }
                    else if (_locObjectName == "Return".ToLower())
                    {
                        _result = 6;
                    }
                    else if (_locObjectName == "Consumpt".ToLower())
                    {
                        _result = 7;
                    }
                    else if (_locObjectName == "Deformed".ToLower())
                    {
                        _result = 8;
                    }
                    else if (_locObjectName == "Cutting".ToLower())
                    {
                        _result = 9;
                    }
                    else if (_locObjectName == "Transfer".ToLower())
                    {
                        _result = 10;
                    }
                    else
                    {
                        _result = 0;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Module = GlobalFunction " + ex.ToString());
            }
            return _result;
        }

        public int GetLocationType(string _objectName)
        {
            int _result = 0;
            string _locObjectName = null;
            try
            {
                if (_objectName != null)
                {
                    _locObjectName = _objectName.Trim().Replace(" ", "").ToLower();
                    if (_locObjectName == "Main".ToLower())
                    {
                        _result = 1;
                    }
                    else if (_locObjectName == "Transit".ToLower())
                    {
                        _result = 2;
                    }
                    else if (_locObjectName == "Site".ToLower())
                    {
                        _result = 3;
                    }
                    else if (_locObjectName == "Vehicle".ToLower())
                    {
                        _result = 4;
                    }
                    else
                    {
                        _result = 0;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Module = GlobalFunction " + ex.ToString());
            }
            return _result;
        }

        public int GetNumberingType(string _objectName)
        {
            int _result = 0;
            string _locObjectName = null;
            try
            {
                if (_objectName != null)
                {
                    _locObjectName = _objectName.Trim().Replace(" ", "").ToLower();
                    if (_locObjectName == "Documents".ToLower())
                    {
                        _result = 0;
                    }
                    else if (_locObjectName == "Objects".ToLower())
                    {
                        _result = 1;
                    }
                    else if (_locObjectName == "Employee".ToLower())
                    {
                        _result = 2;
                    }
                    else
                    {
                        _result = 3;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Module = GlobalFunction " + ex.ToString());
            }
            return _result;
        }

        public int GetOrderType(string _objectName)
        {
            int _result = 0;
            string _locObjectName = null;
            try
            {
                if (_objectName != null)
                {
                    _locObjectName = _objectName.Trim().Replace(" ", "").ToLower();
                    if (_locObjectName == "Item".ToLower())
                    {
                        _result = 1;
                    }
                    else if (_locObjectName == "FixedAsset".ToLower())
                    {
                        _result = 2;
                    }
                    else if (_locObjectName == "Account".ToLower())
                    {
                        _result = 3;
                    }
                    else if (_locObjectName == "Service".ToLower())
                    {
                        _result = 4;
                    }
                    else
                    {
                        _result = 0;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Module = GlobalFunction " + ex.ToString());
            }
            return _result;
        }

        public int GetPageType(string _objectName)
        {
            int _result = 0;
            string _locObjectName = null;
            try
            {
                if (_objectName != null)
                {
                    _locObjectName = _objectName.Trim().Replace(" ", "").ToLower();
                    if (_locObjectName == "DetailView".ToLower())
                    {
                        _result = 1;
                    }
                    else if (_locObjectName == "ListView".ToLower())
                    {
                        _result = 2;
                    }
                    else
                    {
                        _result = 0;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Module = GlobalFunction " + ex.ToString());
            }
            return _result;
        }

        public int GetPaymentMethodType(string _objectName)
        {
            int _result = 0;
            string _locObjectName = null;
            try
            {
                if (_objectName != null)
                {
                    _locObjectName = _objectName.Trim().Replace(" ", "").ToLower();
                    if (_locObjectName == "CashBeforeDelivery".ToLower())
                    {
                        _result = 1;
                    }
                    else if (_locObjectName == "DownPayment".ToLower())
                    {
                        _result = 2;
                    }
                    else if (_locObjectName == "Normal".ToLower())
                    {
                        _result = 3;
                    }
                    else
                    {
                        _result = 0;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Module = GlobalFunction " + ex.ToString());
            }
            return _result;
        }

        public int GetPaymentType(string _objectName)
        {
            int _result = 0;
            string _locObjectName = null;
            try
            {
                if (_objectName != null)
                {
                    _locObjectName = _objectName.Trim().Replace(" ", "").ToLower();
                    if (_locObjectName == "Cash".ToLower())
                    {
                        _result = 1;
                    }
                    else if (_locObjectName == "Cheque".ToLower())
                    {
                        _result = 2;
                    }
                    else if (_locObjectName == "Transfer".ToLower())
                    {
                        _result = 3;
                    }
                    else if (_locObjectName == "Giro".ToLower())
                    {
                        _result = 4;
                    }
                    else
                    {
                        _result = 0;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Module = GlobalFunction " + ex.ToString());
            }
            return _result;
        }

        public int GetPostingMethod(string _objectName)
        {
            int _result = 0;
            string _locObjectName = null;
            try
            {
                if (_objectName != null)
                {
                    _locObjectName = _objectName.Trim().Replace(" ", "").ToLower();
                    if (_locObjectName == "Receipt".ToLower())
                    {
                        _result = 1;
                    }
                    else if (_locObjectName == "Deliver".ToLower())
                    {
                        _result = 2;
                    }
                    else if (_locObjectName == "InvoiceAR".ToLower())
                    {
                        _result = 3;
                    }
                    else if (_locObjectName == "InvoiceAP".ToLower())
                    {
                        _result = 4;
                    }
                    else if (_locObjectName == "Payment".ToLower())
                    {
                        _result = 5;
                    }
                    else if (_locObjectName == "Bill".ToLower())
                    {
                        _result = 6;
                    }
                    else if (_locObjectName == "Return".ToLower())
                    {
                        _result = 7;
                    }
                    else if (_locObjectName == "CreditMemo".ToLower())
                    {
                        _result = 8;
                    }
                    else if (_locObjectName == "DebitMemo".ToLower())
                    {
                        _result = 9;
                    }
                    else if (_locObjectName == "ReclassJournal".ToLower())
                    {
                        _result = 10;
                    }
                    else if (_locObjectName == "PrePayment".ToLower())
                    {
                        _result = 11;
                    }
                    else if (_locObjectName == "CashAdvance".ToLower())
                    {
                        _result = 12;
                    }
                    else if (_locObjectName == "CashRealization".ToLower())
                    {
                        _result = 13;
                    }
                    else
                    {
                        _result = 0;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Module = GlobalFunction " + ex.ToString());
            }
            return _result;
        }

        public int GetPostingMethodType(string _objectName)
        {
            int _result = 0;
            string _locObjectName = null;
            try
            {
                if (_objectName != null)
                {
                    _locObjectName = _objectName.Trim().Replace(" ", "").ToLower();
                    if (_locObjectName == "Normal".ToLower())
                    {
                        _result = 1;
                    }
                    else if (_locObjectName == "Tax".ToLower())
                    {
                        _result = 2;
                    }
                    else if (_locObjectName == "Discount".ToLower())
                    {
                        _result = 3;
                    }
                    else if (_locObjectName == "Direct".ToLower())
                    {
                        _result = 4;
                    }
                    else if (_locObjectName == "Domestic".ToLower())
                    {
                        _result = 5;
                    }
                    else if (_locObjectName == "Advances".ToLower())
                    {
                        _result = 6;
                    }
                    else if (_locObjectName == "DownPayment".ToLower())
                    {
                        _result = 7;
                    }
                    else
                    {
                        _result = 0;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Module = GlobalFunction " + ex.ToString());
            }
            return _result;
        }

        public int GetPostingType(string _objectName)
        {
            int _result = 0;
            string _locObjectName = null;
            try
            {
                if (_objectName != null)
                {
                    _locObjectName = _objectName.Trim().Replace(" ", "").ToLower();
                    if (_locObjectName == "Purchase".ToLower())
                    {
                        _result = 1;
                    }
                    else if (_locObjectName == "Sales".ToLower())
                    {
                        _result = 2;
                    }
                    else if (_locObjectName == "Routing".ToLower())
                    {
                        _result = 3;
                    }
                    else if (_locObjectName == "Packing".ToLower())
                    {
                        _result = 4;
                    }
                    else if (_locObjectName == "Operation".ToLower())
                    {
                        _result = 5;
                    }
                    else if (_locObjectName == "Production".ToLower())
                    {
                        _result = 6;
                    }
                    else if (_locObjectName == "Account".ToLower())
                    {
                        _result = 7;
                    }
                    else if (_locObjectName == "Advances".ToLower())
                    {
                        _result = 8;
                    }
                    else if (_locObjectName == "Others".ToLower())
                    {
                        _result = 9;
                    }
                    else
                    {
                        _result = 0;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Module = GlobalFunction " + ex.ToString());
            }
            return _result;
        }

        public int GetPriceType(string _objectName)
        {
            int _result = 0;
            string _locObjectName = null;
            try
            {
                if (_objectName != null)
                {
                    _locObjectName = _objectName.Trim().Replace(" ", "").ToLower();
                    if (_locObjectName == "Normal".ToLower())
                    {
                        _result = 1;
                    }
                    else if (_locObjectName == "Weight".ToLower())
                    {
                        _result = 2;
                    }
                    else if (_locObjectName == "Distance".ToLower())
                    {
                        _result = 3;
                    }
                    else
                    {
                        _result = 0;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Module = GlobalFunction " + ex.ToString());
            }
            return _result;
        }

        public int GetPriorityType(string _objectName)
        {
            int _result = 0;
            string _locObjectName = null;
            try
            {
                if (_objectName != null)
                {
                    _locObjectName = _objectName.Trim().Replace(" ", "").ToLower();
                    if (_locObjectName == "Low".ToLower())
                    {
                        _result = 1;
                    }
                    else if (_locObjectName == "Middle".ToLower())
                    {
                        _result = 2;
                    }
                    else if (_locObjectName == "High".ToLower())
                    {
                        _result = 3;
                    }
                    else
                    {
                        _result = 0;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Module = GlobalFunction " + ex.ToString());
            }
            return _result;
        }

        public int GetRuleType(string _objectName)
        {
            int _result = 0;
            string _locObjectName = null;
            try
            {
                if (_objectName != null)
                {
                    _locObjectName = _objectName.Trim().Replace(" ", "").ToLower();
                    if (_locObjectName == "Rule1".ToLower())
                    {
                        _result = 1;
                    }
                    else if (_locObjectName == "Rule2".ToLower())
                    {
                        _result = 2;
                    }
                    else if (_locObjectName == "Rule3".ToLower())
                    {
                        _result = 3;
                    }
                    else if (_locObjectName == "Rule4".ToLower())
                    {
                        _result = 4;
                    }
                    else
                    {
                        _result = 0;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Module = GlobalFunction " + ex.ToString());
            }
            return _result;
        }

        public int GetSalesRole(string _objectName)
        {
            int _result = 0;
            string _locObjectName = null;
            try
            {
                if (_objectName != null)
                {
                    _locObjectName = _objectName.Trim().Replace(" ", "").ToLower();
                    if (_locObjectName == "Canvassing".ToLower())
                    {
                        _result = 1;
                    }
                    else if (_locObjectName == "TakingOrder".ToLower())
                    {
                        _result = 2;
                    }
                    else if (_locObjectName == "Direct".ToLower())
                    {
                        _result = 3;
                    }
                    else if (_locObjectName == "All".ToLower())
                    {
                        _result = 4;
                    }
                    else
                    {
                        _result = 0;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Module = GlobalFunction " + ex.ToString());
            }
            return _result;
        }

        public int GetSetupList(string _objectName)
        {
            int _result = 0;
            string _locObjectName = null;
            try
            {
                if (_objectName != null)
                {
                    _locObjectName = _objectName.Trim().Replace(" ", "").ToLower();
                    if (_locObjectName == "Currency".ToLower())
                    {
                        _result = 1;
                    }
                    else if (_locObjectName == "CurrencyRate".ToLower())
                    {
                        _result = 2;
                    }
                    else if (_locObjectName == "Numbering".ToLower())
                    {
                        _result = 3;
                    }
                    else if (_locObjectName == "Period".ToLower())
                    {
                        _result = 4;
                    }
                    else
                    {
                        _result = 0;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Module = GlobalFunction " + ex.ToString());
            }
            return _result;
        }

        public int GetStatus(string _objectName)
        {
            int _result = 0;
            string _locObjectName = null;
            try
            {
                if (_objectName != null)
                {
                    _locObjectName = _objectName.Trim().Replace(" ", "").ToLower();
                    if (_locObjectName == "Open".ToLower())
                    {
                        _result = 1;
                    }
                    else if (_locObjectName == "Progress".ToLower())
                    {
                        _result = 2;
                    }
                    else if (_locObjectName == "Posted".ToLower())
                    {
                        _result = 3;
                    }
                    else if (_locObjectName == "Close".ToLower())
                    {
                        _result = 4;
                    }
                    else if (_locObjectName == "Lock".ToLower())
                    {
                        _result = 5;
                    }
                    else if (_locObjectName == "Approved".ToLower())
                    {
                        _result = 6;
                    }
                    else
                    {
                        _result = 0;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Module = GlobalFunction " + ex.ToString());
            }
            return _result;
        }

        public int GetStockGroup(string _objectName)
        {
            int _result = 0;
            string _locObjectName = null;
            try
            {
                if (_objectName != null)
                {
                    _locObjectName = _objectName.Trim().Replace(" ", "").ToLower();
                    if (_locObjectName == "Normal".ToLower())
                    {
                        _result = 1;
                    }
                    else if (_locObjectName == "FreeItem".ToLower())
                    {
                        _result = 2;
                    }
                    else
                    {
                        _result = 0;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Module = GlobalFunction " + ex.ToString());
            }
            return _result;
        }

        public int GetStockType(string _objectName)
        {
            int _result = 0;
            string _locObjectName = null;
            try
            {
                if (_objectName != null)
                {
                    _locObjectName = _objectName.Trim().Replace(" ", "").ToLower();
                    if (_locObjectName == "Good".ToLower())
                    {
                        _result = 1;
                    }
                    else if (_locObjectName == "Bad".ToLower())
                    {
                        _result = 2;
                    }
                    else
                    {
                        _result = 0;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Module = GlobalFunction " + ex.ToString());
            }
            return _result;
        }

        public int GetTaxCalculationMethod(string _objectName)
        {
            int _result = 0;
            string _locObjectName = null;
            try
            {
                if (_objectName != null)
                {
                    _locObjectName = _objectName.Trim().Replace(" ", "").ToLower();
                    if (_locObjectName == "Net".ToLower())
                    {
                        _result = 1;
                    }
                    else if (_locObjectName == "Gross".ToLower())
                    {
                        _result = 2;
                    }
                    else if (_locObjectName == "Mixed".ToLower())
                    {
                        _result = 3;
                    }
                    else
                    {
                        _result = 0;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Module = GlobalFunction " + ex.ToString());
            }
            return _result;
        }

        public int GetTaxMode(string _objectName)
        {
            int _result = 0;
            string _locObjectName = null;
            try
            {
                if (_objectName != null)
                {
                    _locObjectName = _objectName.Trim().Replace(" ", "").ToLower();
                    if (_locObjectName == "Purchase".ToLower())
                    {
                        _result = 1;
                    }
                    else if (_locObjectName == "Sales".ToLower())
                    {
                        _result = 2;
                    }
                    else if (_locObjectName == "Both".ToLower())
                    {
                        _result = 3;
                    }
                    else
                    {
                        _result = 0;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Module = GlobalFunction " + ex.ToString());
            }
            return _result;
        }

        public int GetTaxNature(string _objectName)
        {
            int _result = 0;
            string _locObjectName = null;
            try
            {
                if (_objectName != null)
                {
                    _locObjectName = _objectName.Trim().Replace(" ", "").ToLower();
                    if (_locObjectName == "Increase".ToLower())
                    {
                        _result = 1;
                    }
                    else if (_locObjectName == "Decrease".ToLower())
                    {
                        _result = 2;
                    }
                    else
                    {
                        _result = 0;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Module = GlobalFunction " + ex.ToString());
            }
            return _result;
        }

        public int GetTaxRule(string _objectName)
        {
            int _result = 0;
            string _locObjectName = null;
            try
            {
                if (_objectName != null)
                {
                    _locObjectName = _objectName.Trim().Replace(" ", "").ToLower();
                    if (_locObjectName == "BeforeDiscount".ToLower())
                    {
                        _result = 1;
                    }
                    else if (_locObjectName == "AfterDiscount".ToLower())
                    {
                        _result = 2;
                    }
                    else
                    {
                        _result = 0;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Module = GlobalFunction " + ex.ToString());
            }
            return _result;
        }

        public int GetTransactionTerm(string _objectName)
        {
            int _result = 0;
            string _locObjectName = null;
            try
            {
                if (_objectName != null)
                {
                    _locObjectName = _objectName.Trim().Replace(" ", "").ToLower();
                    if (_locObjectName == "Daily".ToLower())
                    {
                        _result = 1;
                    }
                    else if (_locObjectName == "Monthly".ToLower())
                    {
                        _result = 2;
                    }
                    else
                    {
                        _result = 0;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Module = GlobalFunction " + ex.ToString());
            }
            return _result;
        }

        public int GetVehicleMode(string _objectName)
        {
            int _result = 0;
            string _locObjectName = null;
            try
            {
                if (_objectName != null)
                {
                    _locObjectName = _objectName.Trim().Replace(" ", "").ToLower();
                    if (_locObjectName == "Airplane".ToLower())
                    {
                        _result = 1;
                    }
                    else if (_locObjectName == "Truck".ToLower())
                    {
                        _result = 2;
                    }
                    else if (_locObjectName == "Vessel".ToLower())
                    {
                        _result = 3;
                    }
                    else if (_locObjectName == "Train".ToLower())
                    {
                        _result = 4;
                    }
                    else
                    {
                        _result = 0;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Module = GlobalFunction " + ex.ToString());
            }
            return _result;
        }

        public int GetVehicleType(string _objectName)
        {
            int _result = 0;
            string _locObjectName = null;
            try
            {
                if (_objectName != null)
                {
                    _locObjectName = _objectName.Trim().Replace(" ", "").ToLower();
                    if (_locObjectName == "Real".ToLower())
                    {
                        _result = 1;
                    }
                    else if (_locObjectName == "Virtual".ToLower())
                    {
                        _result = 2;
                    }
                    else
                    {
                        _result = 0;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Module = GlobalFunction " + ex.ToString());
            }
            return _result;
        }

        #endregion Function For Enum

        #endregion Import Data

        #region Normal Function

        public bool GetAdministrationAccessing(Session _locSession, string _locUserName)
        {
            bool _result = false;
            try
            {

                UserAccess _numLineUserAccess = _locSession.FindObject<UserAccess>
                                                (new BinaryOperator("UserName", _locUserName));
                if (_numLineUserAccess != null)
                {
                    XPCollection<UserAccessRole> _locUserAccessRoles = new XPCollection<UserAccessRole>(_locSession, new ContainsOperator("UserAccesses", new BinaryOperator("This", _numLineUserAccess)));
                    if (_locUserAccessRoles != null)
                    {
                        foreach (UserAccessRole _locUserAccessRole in _locUserAccessRoles)
                        {
                            if (_locUserAccessRole.IsAdministrative == true)
                            {
                                _result = true;
                                break;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = GlobalFunction " + ex.ToString());
            }
            return _result;
        }

        public Country GetDefaultCountry(Session _currSession)
        {
            Country _result = null;
            try
            {
                _result = _currSession.FindObject<Country>(new GroupOperator(GroupOperatorType.And,
                                                           new BinaryOperator("Default", true),
                                                           new BinaryOperator("Active", true)));
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Module = GlobalFunction " + ex.ToString());
            }
            return _result;
        }

        public Currency GetDefaultCurrency(Session _currSession)
        {
            Currency _result = null;
            try
            {
                _result = _currSession.FindObject<Currency>(new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("Default", true),
                                                            new BinaryOperator("Active", true)));
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Module = GlobalFunction " + ex.ToString());
            }
            return _result;
        }

        public Discount GetDefaultDiscount(Session _currSession, DiscountMode _locDiscountMode)
        {
            Discount _result = null;
            try
            {
                if (_locDiscountMode != DiscountMode.None)
                {
                    Discount _result2 = _currSession.FindObject<Discount>(new GroupOperator
                                    (GroupOperatorType.And,
                                    new BinaryOperator("Default", true),
                                    new BinaryOperator("Active", true),
                                    new BinaryOperator("DiscountMode", _locDiscountMode)));
                    if (_result2 != null)
                    {
                        _result = _result2;
                    }
                    else
                    {
                        Discount _result3 = _currSession.FindObject<Discount>(new GroupOperator
                                        (GroupOperatorType.And,
                                        new BinaryOperator("Default", true),
                                        new BinaryOperator("Active", true),
                                        new BinaryOperator("DiscountMode", DiscountMode.Both)));
                        if (_result3 != null)
                        {
                            _result = _result3;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Module = GlobalFunction " + ex.ToString());
            }
            return _result;
        }      

        public FreeItemRuleType GetDefaultFreeItemRuleType(Session _currSession)
        {
            FreeItemRuleType _result = null;
            try
            {
                _result = _currSession.FindObject<FreeItemRuleType>(new GroupOperator(GroupOperatorType.And,
                                                           new BinaryOperator("Default", true),
                                                           new BinaryOperator("Active", true)));
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Module = GlobalFunction " + ex.ToString());
            }
            return _result;
        }

        public PriceGroup GetPriceGroupPOS(Session _currSession)
        {
            PriceGroup _result = null;
            try
            {
                _result = _currSession.FindObject<PriceGroup>(new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("PriceGroupType", PriceGroupType.Direct),
                                                            new BinaryOperator("Active", true)));
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Module = GlobalFunction " + ex.ToString());
            }
            return _result;
        }

        public Tax GetDefaultTax(Session _currSession, TaxMode _locTaxMode)
        {
            Tax _result = null;
            try
            {
                if (_locTaxMode != TaxMode.None)
                {
                    Tax _result2 = _currSession.FindObject<Tax>(new GroupOperator
                                (GroupOperatorType.And,
                                new BinaryOperator("Default", true),
                                new BinaryOperator("Active", true),
                                new BinaryOperator("TaxMode", _locTaxMode)));
                    if (_result2 != null)
                    {
                        _result = _result2;
                    }
                    else
                    {
                        Tax _result3 = _currSession.FindObject<Tax>(new GroupOperator
                                    (GroupOperatorType.And,
                                    new BinaryOperator("Default", true),
                                    new BinaryOperator("Active", true),
                                    new BinaryOperator("TaxMode", TaxMode.Both)));
                        if (_result3 != null)
                        {
                            _result = _result3;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Module = GlobalFunction " + ex.ToString());
            }
            return _result;
        }

        public TermOfPayment GetDefaultTermOfPayment(Session _currSession)
        {
            TermOfPayment _result = null;
            try
            {
                _result = _currSession.FindObject<TermOfPayment>(new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("Default", true),
                                                            new BinaryOperator("Active", true)));
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Module = GlobalFunction " + ex.ToString());
            }
            return _result;
        }

        public Workplace GetDefaultWorkplace(Session _currSession, UserAccess _locUserAccess)
        {
            Workplace _result = null;
            try
            {
                if(_locUserAccess != null)
                {
                    OrganizationSetupDetail _locOrganizationSetupDetail = _currSession.FindObject<OrganizationSetupDetail>
                                                                        (new GroupOperator(GroupOperatorType.And,
                                                                        new BinaryOperator("UserAccess", _locUserAccess),
                                                                        new BinaryOperator("OpenWorkplace", true),
                                                                        new BinaryOperator("Active", true),
                                                                        new BinaryOperator("Default", true)));
                    if(_locOrganizationSetupDetail != null)
                    {
                        _result = _locOrganizationSetupDetail.Workplace;
                    }
                } 
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Module = GlobalFunction " + ex.ToString());
            }
            return _result;
        }

        #endregion Normal Function

        #region Rounding

        public bool GetRoundingList(Session _currSession, ObjectList _currObjectList, FieldName _currFieldName)
        {

            bool _result = false;
            RoundingSetupDetail _roundingSetupDetail = null;
            RoundingSetupDetail _roundingSetupDetail2 = null;

            try
            {

                ApplicationSetup _appSetup = _currSession.FindObject<ApplicationSetup>(new GroupOperator(GroupOperatorType.And,
                                            new BinaryOperator("Active", true), new BinaryOperator("DefaultSystem", true),
                                            new BinaryOperator("Code", "APS0001")));
                if (_appSetup != null)
                {
                    if (_currFieldName != FieldName.None)
                    {
                        _roundingSetupDetail = _currSession.FindObject<RoundingSetupDetail>
                                                            (new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("ObjectList", _currObjectList),
                                                            new BinaryOperator("FieldName", _currFieldName),
                                                            new BinaryOperator("Active", true)));
                        if (_roundingSetupDetail != null)
                        {
                            _result = true;
                        }
                        else
                        {
                            _roundingSetupDetail2 = _currSession.FindObject<RoundingSetupDetail>
                                                            (new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("All", true),
                                                            new BinaryOperator("Active", true)));

                            if (_roundingSetupDetail2 != null)
                            {
                                _result = true;
                            }
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Module = GlobalFunction " + ex.ToString());
            }
            return _result;
        }

        public Double GetRoundUp(Session _currSession, Double _currValue, ObjectList _currObjectList, FieldName _currFieldName)
        {
            Double result = 0;

            try
            {
                int _roundUpNumber = 0;
                RoundingSetupDetail _roundingSetupDetail = null;
                RoundingSetupDetail _roundingSetupDetail2 = null;

                ApplicationSetup _appSetup = _currSession.FindObject<ApplicationSetup>(new GroupOperator(GroupOperatorType.And,
                                            new BinaryOperator("Active", true),
                                            new BinaryOperator("DefaultSystem", true),
                                            new BinaryOperator("SetupCode", "APS0001")));
                if (_appSetup != null)
                {
                    if (_currFieldName != FieldName.None)
                    {
                        _roundingSetupDetail = _currSession.FindObject<RoundingSetupDetail>
                                                            (new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("ObjectList", _currObjectList),
                                                            new BinaryOperator("FieldName", _currFieldName),
                                                            new BinaryOperator("Active", true),
                                                            new BinaryOperator("ApplicationSetup", _appSetup)));
                        if (_roundingSetupDetail != null)
                        {
                            _roundUpNumber = _roundingSetupDetail.RoundValue;
                        }
                        else
                        {
                            _roundingSetupDetail2 = _currSession.FindObject<RoundingSetupDetail>
                                                            (new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("All", true),
                                                            new BinaryOperator("Active", true),
                                                            new BinaryOperator("ApplicationSetup", _appSetup)));
                            if (_roundingSetupDetail2 != null)
                            {
                                _roundUpNumber = _roundingSetupDetail2.RoundValue;
                            }
                        }
                    }
                    else
                    {
                        _roundingSetupDetail = _currSession.FindObject<RoundingSetupDetail>
                                                            (new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("All", true),
                                                            new BinaryOperator("Active", true),
                                                            new BinaryOperator("ApplicationSetup", _appSetup)));
                        if (_roundingSetupDetail != null)
                        {
                            _roundUpNumber = _roundingSetupDetail.RoundValue;
                        }
                    }

                }

                //result = System.Math.Round(_currValue, _roundUpNumber, MidpointRounding.ToEven);
                result = System.Math.Round(_currValue, _roundUpNumber);

            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Module = GlobalFunction " + ex.ToString());
            }
            return result;
        }

        #endregion Rounding

        #region Approval

        public ApplicationSetupDetail GetApplicationSetupDetailByApprovalLevel(Session currentSession, UserAccess _locUserAccess, ApprovalLevel _locApprovalLevel, ObjectList _locObjectList)
        {
            ApplicationSetupDetail _result = null;
            try
            {
                ApplicationSetupDetail _locAppSetupDetail = currentSession.FindObject<ApplicationSetupDetail>
                                                                                 (new GroupOperator(GroupOperatorType.And,
                                                                                  new BinaryOperator("UserAccess", _locUserAccess),
                                                                                  new BinaryOperator("ObjectList", _locObjectList),
                                                                                  new BinaryOperator("FunctionList", CustomProcess.FunctionList.Approval),
                                                                                  new BinaryOperator("ApprovalLevel", _locApprovalLevel),
                                                                                  new BinaryOperator("Active", true)));
                if (_locAppSetupDetail != null)
                {
                    _result = _locAppSetupDetail;
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject =  GlobalFunction " + ex.ToString());
            }
            return _result;
        }

        #endregion Approval      

        #region Email

        public void SetAndSendMail(string _locSmtpHost, int _locSmtpPort, string _locMailFrom, string _locMailFromPassword, string _locMailTo, string _locSubjectMail, string _locMessageBody)
        {
            try
            {
                if (_locMailTo != null)
                {
                    foreach (string _locRowMailTo in _locMailTo.Replace(" ", "").Split(';'))
                    {
                        if (!string.IsNullOrEmpty(_locRowMailTo))
                        {
                            SmtpClient client = new SmtpClient(_locSmtpHost, _locSmtpPort);
                            client.TargetName = "None";
                            client.UseDefaultCredentials = false;
                            client.Credentials = new System.Net.NetworkCredential(_locMailFrom, _locMailFromPassword);
                            MailAddress from = new MailAddress(_locMailFrom, String.Empty, System.Text.Encoding.UTF8);
                            MailAddress to = new MailAddress(_locRowMailTo);
                            MailMessage message = new MailMessage(from, to);
                            message.Body = _locMessageBody;
                            message.IsBodyHtml = true;
                            message.BodyEncoding = System.Text.Encoding.UTF8;
                            message.Subject = _locSubjectMail;
                            message.SubjectEncoding = System.Text.Encoding.UTF8;
                            client.Send(message);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = GlobalFunction " + ex.ToString());
            }
        }

        #endregion Email

    }
}