﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FullDrive.Module.CustomProcess
{
    class FullDriveSysBaseClass
    {
    }

    public enum NumberingType
    {
        Documents = 0,
        Objects = 1,
        Employee = 2,
        Other = 3
    }

    public enum ObjectList
    {
        None = 0,
        #region A

        AccountGroup = 1,
        AccountingPeriodic = 2,
        AccountingPeriodicLine = 3,
        AccountMap = 4,
        AccountMapLine = 5,
        AccountReclassJournal = 6,
        AccountReclassJournalLine = 7,
        AccountReclassJournalMonitoring = 8,
        AdditionalAnalyze = 9,
        AdditionalDashboard = 10,
        AdditionalReport = 11,
        Address = 12,
        AdvanceAccountGroup = 13,
        AmendDelivery = 14,
        AmendDeliveryLine = 15,
        AmendDeliveryMonitoring = 16,
        ApplicationImport = 17,
        ApplicationSetup = 18,
        ApplicationSetupDetail = 19,
        ApprovalLine = 20,
        Area = 21,
        #endregion A

        #region B

        BankAccount = 22,
        BankAccountGroup = 23,
        BeginningInventory = 24,
        BeginningInventoryLine = 25,
        BinLocation = 26,
        BinLocationSetupDetail = 27,
        Brand = 28,
        BusinessPartner = 29,
        BusinessPartnerAccountGroup = 30,
        BusinessPartnerGroup = 31,
        BusinessPartnerPriceGroupSetup = 32,
        #endregion B

        #region C

        CanvasPaymentIn = 33,
        CanvassingCollection = 34,
        CanvassingCollectionFreeItem = 35,
        CanvassingCollectionLine = 36,
        CanvassingCollectionMonitoring = 37,
        CashAdvance = 38,
        CashAdvanceCollection = 39,
        CashAdvanceLine = 40,
        CashAdvanceMonitoring = 41,
        CashAdvancePaymentPlan = 42,
        CashAdvanceType = 43,
        ChartOfAccount = 44,
        City = 45,
        Company = 46,
        CompanyAccount = 47,
        CompanyAccountGroup = 48,
        Country = 49,
        CreditLimitList = 50,
        Currency = 51,
        CurrencyRate = 52,
        CustomerInvoice = 53,
        CustomerInvoiceFreeItem = 54,
        CustomerInvoiceLine = 55,
        CustomerMap = 56,
        CustomerOrder = 57,
        CustomerOrderFreeItem = 58,
        CustomerOrderLine = 59,
        #endregion C

        #region D

        DeliveryOrder = 60,
        DeliveryOrderLine = 61,
        DeliveryOrderMonitoring = 62,
        Department = 63,
        Discount = 64,
        DiscountAccountGroup = 65,
        DiscountRule = 66,
        DiscountRuleType = 67,
        Division = 68,
        DocumentType = 69,
        #endregion D

        #region E

        Employee = 70,
        EmployeeDocumentTypeSetup = 71,
        ExchangeRate = 72,
        #endregion E

        #region F

        FreeItemRule = 73,
        FreeItemRuleType = 74,
        #endregion F

        #region G

        GeneralJournal = 75,
        #endregion G

        #region I

        IndustrialType = 76,
        InventoryJournal = 77,
        InventoryPurchaseCollection = 78,
        InventoryTransferIn = 79,
        InventoryTransferInLine = 80,
        InventoryTransferInLot = 81,
        InventoryTransferInMonitoring = 82,
        InventoryTransferOut = 83,
        InventoryTransferOutLine = 84,
        InventoryTransferOutLot = 85,
        InvoiceCanvassing = 86,
        InvoiceCanvassingCollection = 87,
        InvoiceCanvassingFreeItem = 88,
        InvoiceCanvassingLine = 89,
        InvoiceCanvassingMonitoring = 90,
        InvoiceInMonitoring = 91,
        InvoiceInMonitoringLine = 92,
        InvoiceOutMonitoring = 93,
        InvoiceOutMonitoringLine = 94,
        InvoicePurchaseCollection = 95,
        Item = 96,
        ItemAccount = 97,
        ItemAccountGroup = 98,
        ItemComponent = 99,
        ItemGroup = 100,
        ItemReclassJournal = 101,
        ItemReclassJournalLine = 102,
        ItemReclassJournalMonitoring = 103,
        ItemUnitOfMeasure = 104,
        #endregion I

        #region J

        JournalMap = 105,
        JournalMapLine = 106,
        JournalSetupDetail = 107,
        #endregion J

        #region L

        ListImport = 108,
        Location = 109,
        #endregion L

        #region M

        MailSetupDetail = 110,
        #endregion M

        #region N

        NumberingHeader = 111,
        NumberingLine = 112,
        #endregion N

        #region O

        
        OrderCollection = 113,
        OrderCollectionLine = 114,
        OrderCollectionMonitoring = 115,
        OrganizationAccountGroup = 116,
        OrganizationSetupDetail = 117,
        #endregion O

        #region P

        PayablePurchaseCollection = 118,
        PayableTransaction = 119,
        PayableTransactionLine = 120,
        PayableTransactionMonitoring = 121,
        PaymentInPlan = 122,
        PaymentMethod = 123,
        PaymentOutPlan = 124,
        PaymentRealization = 125,
        PaymentRealizationLine = 126,
        PaymentRealizationMonitoring = 127,
        PhoneNumber = 128,
        PhoneType = 129,
        Position = 130,
        Price = 131,
        PriceCostMethod = 132,
        PriceGroup = 133,
        PriceLine = 134,
        PurchaseInvoice = 135,
        PurchaseInvoiceLine = 136,
        PurchaseInvoiceMonitoring = 137,
        PurchaseOrder = 138,
        PurchaseOrderLine = 139,
        PurchaseOrderMonitoring = 140,
        PurchasePrePaymentInvoice = 141,
        PurchasePrePaymentInvoiceMonitoring = 142,
        PurchaseRequisition = 143,
        PurchaseRequisitionCollection = 144,
        PurchaseRequisitionLine = 145,
        PurchaseRequisitionMonitoring = 146,
        PurchaseSetupDetail = 147,
        #endregion P

        #region R

        ReceivableOrderCollection = 148,
        ReceivableTransaction = 149,
        ReceivableTransactionLine = 150,
        ReceivableTransactionMonitoring = 151,
        RoundingSetupDetail = 152,
        Route = 153,
        RouteInvoice = 154,
        RoutePlan = 155,
        #endregion R

        #region S

        SalesArea = 156,
        SalesArea1 = 157,
        SalesArea2 = 158,
        SalesArea3 = 159,
        SalesArea4 = 160,
        SalesAreaLine = 161,
        SalesInvoice = 162,
        SalesInvoiceCmp = 163,
        SalesInvoiceCollection = 164,
        SalesInvoiceFreeItem = 165,
        SalesInvoiceFreeItemCmp = 166,
        SalesInvoiceLine = 167,
        SalesInvoiceLineCmp = 168,
        SalesInvoiceMonitoring = 169,
        SalesmanAreaSetup = 170,
        SalesmanCustomerSetup = 171,
        SalesmanLocationSetup = 172,
        SalesmanVehicleSetup = 173,
        SalesOrder = 174,
        SalesOrderCollection = 175,
        SalesOrderFreeItem = 176,
        SalesOrderLine = 177,
        SalesOrderMonitoring = 178,
        SalesPromotionRule = 179,
        ScheduleType = 180,
        Section = 181,
        #endregion S

        #region T

        TaskMode = 182,
        Tax = 183,
        TaxAccount = 184,
        TaxAccountGroup = 185,
        TaxGroup = 186,
        TaxLine = 187,
        TaxType = 188,
        TermOfPayment = 189,
        TerritoryLevel1 = 190,
        TerritoryLevel2 = 191,
        TerritoryLevel3 = 192,
        TerritoryLevel4 = 193,
        Transfer = 194,
        TransferIn = 195,
        TransferInLine = 196,
        TransferInLot = 197,
        TransferInMonitoring = 198,
        TransferLine = 199,
        TransferMonitoring = 200,
        TransferOut = 201,
        TransferOutCollection = 202,
        TransferOutLine = 203,
        TransferOutLot = 204,
        TransferOutMonitoring = 205,
        #endregion T

        #region U

        UnitOfMeasure = 206,
        UnitOfMeasureType = 207,
        UserAccess = 208,
        UserAccessRole = 209,
        #endregion U

        #region V

        Vehicle = 210,
        VehicleList = 211,
        #endregion V

        #region W

        WarehouseSetupDetail = 212,
        Workplace = 213,
        #endregion W

        #region Additional

        Picking = 214,
        PickingLine = 215,
        PickingMonitoring = 216,
        TransferOrder = 217,
        TransferOrderLine = 218,
        TransferInventoryMonitoring = 219,
        SalesInvoiceCollection2 = 220,
        ReceivableTransaction2 = 221,
        ReceivableTransactionLine2 = 222,
        ReceivableTransactionBank = 223,
        BusinessPartnerAccount = 224,
        BankAccountMap = 225,
        PayableTransactionBank = 226,
        OrderCollectionScanner = 227,
        PickingScanner = 228,

        #endregion Additional

    }

    public enum AccountCharge
    {
        None = 0,
        Debit = 1,
        Credit = 2,
        Both = 3
    }

    public enum AccountMode
    {
        None = 0,
        Customer = 1,
        Vendor = 2,
        BankAccount = 3,
        Others = 4
    }

    public enum AccountType
    {
        None = 0,
        Posting = 1,
        Heading = 2,
        Total = 3,
        BeginTotal = 4,
        EndTotal = 5
    }

    public enum ApprovalFilter
    {
        AllData = 0,
        Approval = 1,
        EndApproval = 2,
    }

    public enum ApprovalLevel
    {
        None = 0,
        Level1 = 1,
        Level2 = 2,
        Level3 = 3,
        Level4 = 4,
        Level5 = 5,
        Level6 = 6,
    }

    public enum BalanceType
    {
        None = 0,
        Change = 1,
        Fix = 2,
        Other = 3,
    }

    public enum BusinessPartnerType
    {
        None = 0,
        All = 1,
        Customer = 2,
        Vendor = 3
    }

    public enum CollectStatus
    {
        None = 0,
        Ready = 1,
        NotReady = 2,
        Collect = 3,
        Finish = 4,
        Cancel = 5,
    }

    public enum CreditLimitStatus
    {
        None = 0,
        Released = 1,
        Hold = 2,
    }

    public enum DeliveryStatus
    {
        None = 0,
        CancelAll = 1,
        DeliveryAll = 2,
        Loading = 3,
        Loaded = 4,
        PendingAll = 5,
        CancelSome = 6,
        PendingSome = 7,
    }

    public enum DirectionType
    {
        None = 0,
        Internal = 1,
        External = 2,
    }

    public enum DiscountMode
    {
        None = 0,
        Purchase = 1,
        Sales = 2,
        Both = 3,
    }

    public enum DiscountType
    {
        None = 0,
        Percentage = 1,
        Value = 2,
    }

    public enum DocumentRule
    {
        None = 0,
        Customer = 1,
        Vendor = 2,
        Salesman = 3,
    }  

    public enum FieldName
    {
        None = 0,
        MxUAmount = 1,
        MxTUAmount = 2,
        UAmount = 3,
        TUAmount = 4,
        TxAmount = 5,
        DiscAmount = 6,
        TAmount = 7,
        TotalUnitAmount = 8,
        UnitAmount = 9,
        Amount1a = 10,
        Amount1b = 11,
        Amount2a = 12,
        Amount2b = 13,
        PTUAmount = 14,
        PTxAmount = 15,
        PTAmount = 16,
    }

    public enum FinancialStatment
    {
        None = 0,
        BalanceSheet = 1,
        IncomeStatment = 2
    }

    public enum FreeItemType1
    {
        None = 0,
        Header = 1,
        Line = 2,
    }

    public enum FunctionList
    {
        None = 0,
        Approval = 1,
        Posting = 2,
        Progress = 3,
        QcPassed = 4,
    }

    public enum InventoryMovingType
    {
        None = 0,
        Receive = 1,
        Deliver = 2,
        TransferIn = 3,
        TransferOut = 4,
        Adjust = 5,
        Return = 6,
        Consumpt = 7,
        Deformed = 8,
        Cutting = 9,
        Transfer = 10,
        Other = 11,
    }

    public enum JournalProcess
    {
        None = 0,
        JournalGoodReceipt = 1,
        JournalInvoiceAP = 2,
        JournalPayable = 3,
        JournalGoodDeliver = 4,
        JournalInvoiceAR = 5,
        JournalReceivable = 6,
        JournalReturnSales = 7,
        JournalReturnPurchase = 8,
        JournalCashAdvances = 9,
        JournalPaymentRealization = 10,
        JournalPrePayment = 11,
    }

    public enum LocationType
    {
        None = 0,
        Main = 1,
        Transit = 2,
        Site = 3,
        Vehicle = 4,
    }

    public enum LocationType2
    {
        None = 0,
        RM = 1,
        QC = 2,
        FG = 3,
        Prod = 4,
    }

    public enum OrderType
    {
        None = 0,
        Item = 1,
        FixedAsset = 2,
        Account = 3,
        Service = 4,
    }

    public enum PageType
    {
        None = 0,
        DetailView = 1,
        ListView = 2,
    }

    public enum PaymentMethodType
    {
        None = 0,
        CashBeforeDelivery = 1,
        DownPayment = 2,
        Normal = 3,
    }

    public enum PaymentStatus
    {
        None = 0,
        Settled = 1,
        Debt = 2,
    }

    public enum PaymentType
    {
        None = 0,
        Cash = 1,
        Cheque = 2,
        Transfer = 3,
        Giro = 4,
    }

    public enum PostingMethod
    {
        None = 0,
        Receipt = 1,
        Deliver = 2,
        InvoiceAR = 3,
        InvoiceAP = 4,
        Payment = 5,
        Bill = 6,
        Return = 7,
        CreditMemo = 8,
        DebitMemo = 9,
        ReclassJournal = 10,
        PrePayment = 11,
        CashAdvance = 12,
        CashRealization = 13,
        AdvancePayment = 14,
    }

    public enum PostingMethodType
    {
        None = 0,
        Normal = 1,
        Tax = 2,
        Discount = 3,
        Direct = 4,
        Domestic = 5,
        Advances = 6,
        DownPayment = 7,
        Return = 8,
        FreeItem = 9,
        AmountDN = 10,
        AmountCN = 11,
        OverPayment = 12,
        LowerPayment = 13,
    }

    public enum PostingType
    {
        None = 0,
        Purchase = 1,
        Sales = 2,
        Routing = 3,
        Packing = 4,
        Operation = 5,
        Production = 6,
        Account = 7,
        Advances = 8,
        Others = 9,
    }

    public enum PriceGroupType
    {
        None = 0,
        Direct = 1,
        Indirect = 2,
    }

    public enum PriceType
    {
        None = 0,
        Normal = 1,
        Weight = 2,
        Distance = 3
    }

    public enum PriorityType
    {
        None = 0,
        Low = 1,
        Middle = 2,
        High = 3
    }

    public enum Process
    {
        None = 0,
        SalesOrder = 1,
        SalesInvoice = 2,
        TransferOut = 3,
        TransferIn = 4,
        DeliveryOrder = 5,
        AmendDelivery = 6,
        OrderCollection = 7,
        ReceivableTransaction = 8,
        PurchaseRequisition = 9,
        PurchaseOrder = 10,
        InventoryTransferIn = 11,
        PurchaseInvoice = 12,
        PayableTransaction = 13,
        CashAdvance = 14,
        PaymentRealization = 15,
        InvoiceCanvassing = 16,
        CanvassingCollection = 17,
        ItemReclassJournal = 18,
        AccountReclassJournal = 19,
        SalesInvoiceCmp = 20,
        CustomerOrder = 21,
        CustomerInvoice = 22,
        InventoryTransferOut = 23,
        Transfer = 24,
        TransferLine = 25,
        AccountMap = 26,
        AllApplicationSetup = 27,
        AllAccountGroup = 28,
        CustomerMap = 29,
        AllDiscountRule = 30,
        GeneralJournal = 31,
        Location = 32,
        Price = 33,
        BeginningInventory = 34,
        InvoiceOutMonitoring = 35,
        InvoiceInMonitoring = 36,
        AdditionalReport = 37,
        Vehicle = 38,
        BusinessPartner = 39,
        AllSalesmanSetup = 40,
        Picking = 41,
        TransferOrder = 42,
    }

    public enum RuleType
    {
        None = 0,
        Rule1 = 1,
        Rule2 = 2,
        Rule3 = 3,
        Rule4 = 4,
    }

    public enum SalesRole
    {
        None = 0,
        Canvassing = 1,
        TakingOrder = 2,
        Direct = 3,
        All = 4
    }

    public enum SetupList
    {
        ApplicationSetup = 0,
        Currency = 1,
        CurrencyRate = 2,
        Numbering = 3,
        Period = 4,
    }

    public enum Status
    {
        None = 0,
        Open = 1,
        Progress = 2,
        Posted = 3,
        Close = 4,
        Lock = 5,
        Approved = 6,
        Cancel = 7,
    }

    public enum StockGroup
    {
        None = 0,
        Normal = 1,
        FreeItem = 2,
    }

    public enum StockType
    {
        None = 0,
        Good = 1,
        Bad = 2
    }

    public enum TaxCalculationMethod
    {
        None = 0,
        Net = 1,
        Gross = 2,
        Mixed = 3,
    }

    public enum TaxGroupType
    {
        None = 0,
        PPN = 1,
        PPH = 2,
        NPWP = 3,
        NonNPWP = 4,
        Materai = 5,
    }

    public enum TaxMode
    {
        None = 0,
        Purchase = 1,
        Sales = 2,
        Both = 3,
    }

    public enum TaxNature
    {
        None = 0,
        Increase = 1,
        Decrease = 2,
    }

    public enum TaxRule
    {
        None = 0,
        BeforeDiscount = 1,
        AfterDiscount = 2,
    }    

    public enum TransactionTerm
    {
        None = 0,
        Daily = 1,
        Monthly = 2,
    }

    public enum VehicleMode
    {
        None = 0,
        Airplane = 1,
        Truck = 2,
        Vessel = 3,
        Train = 4,
        Motorcycle = 5,
    }

    public enum VehicleType
    {
        None = 0,
        Real = 1,
        Virtual = 2,
    }

    public enum JournalSetupType
    {
        None = 0,
        Costing = 1,
        Journal = 2,
    }

    public enum CostingMethod
    {
        None = 0,
        FirstInFirstOut = 1,
        LastInFirstOut = 2,
        Average = 3,
        Standard = 4,
    }

    public enum TaskObject
    {
        None = 0,
        DiscountRule = 1,
        FreeItemRule = 2,
        PriceLine = 3,
        NumberingLine = 4,
    }

    public enum ObjectImport
    {
        None = 0,
        CustomerInvoice = 1,
        SalesInvoice = 2,
        PurchaseOrder = 3,
    }
}