﻿using System;
using System.Text;
using System.Linq;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using System.Collections.Generic;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.BaseImpl.PermissionPolicy;
using DevExpress.ExpressApp.Model;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Updating;
using DevExpress.ExpressApp.Model.Core;
using DevExpress.ExpressApp.Model.DomainLogics;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.Xpo;

using DevExpress.ExpressApp.ReportsV2;
using FullDrive.Module.BusinessObjects;
using FullDrive.Module.Reports;

namespace FullDrive.Module {
    // For more typical usage scenarios, be sure to check out https://docs.devexpress.com/eXpressAppFramework/DevExpress.ExpressApp.ModuleBase.
    public sealed partial class FullDriveModule : ModuleBase {
        public FullDriveModule() {
            InitializeComponent();
			BaseObject.OidInitializationMode = OidInitializationMode.AfterConstruction;
        }

        public override IEnumerable<ModuleUpdater> GetModuleUpdaters(IObjectSpace objectSpace, Version versionFromDB) {
            ModuleUpdater updater = new DatabaseUpdate.Updater(objectSpace, versionFromDB);
            PredefinedReportsUpdater predefinedReportsUpdater = new PredefinedReportsUpdater(Application, objectSpace, versionFromDB);
            predefinedReportsUpdater.AddPredefinedReport<ReportPurchaseRequisition>("Report Purchase Requisition", typeof(PurchaseRequisition), isInplaceReport: true);
            predefinedReportsUpdater.AddPredefinedReport<ReportPurchaseOrder>("Report Purchase Order", typeof(PurchaseOrder), isInplaceReport: true);
            predefinedReportsUpdater.AddPredefinedReport<ReportInventoryTransferIn>("Report Inventory Transfer In", typeof(InventoryTransferIn), isInplaceReport: true);
            predefinedReportsUpdater.AddPredefinedReport<ReportPurchaseInvoice>("Report Purchase Invoice", typeof(PurchaseInvoice), isInplaceReport: true);
            predefinedReportsUpdater.AddPredefinedReport<ReportPayableTransaction>("Report Payable Transaction", typeof(PayableTransaction), isInplaceReport: true);
            predefinedReportsUpdater.AddPredefinedReport<ReportSalesOrder>("Report Sales Order", typeof(SalesOrder), isInplaceReport: true);
            predefinedReportsUpdater.AddPredefinedReport<ReportSalesInvoice>("Report Sales Invoice", typeof(SalesInvoice), isInplaceReport: true);
            predefinedReportsUpdater.AddPredefinedReport<ReportPicking>("Report Picking", typeof(Picking), isInplaceReport: true);
            predefinedReportsUpdater.AddPredefinedReport<ReportTransferOrder>("Report Transfer Order", typeof(TransferOrder), isInplaceReport: true);
            predefinedReportsUpdater.AddPredefinedReport<ReportTransferOut>("Report Transfer Out", typeof(TransferOut), isInplaceReport: true);
            predefinedReportsUpdater.AddPredefinedReport<ReportTransferIn>("Report Transfer In", typeof(TransferIn), isInplaceReport: true);
            predefinedReportsUpdater.AddPredefinedReport<ReportDeliveryOrder>("Report Delivery Order", typeof(DeliveryOrder), isInplaceReport: true);
            predefinedReportsUpdater.AddPredefinedReport<ReportOrderCollection>("Report Order Collection", typeof(OrderCollection), isInplaceReport: true);
            predefinedReportsUpdater.AddPredefinedReport<ReportReceivableTransaction>("Report Receivable Transaction", typeof(ReceivableTransaction), isInplaceReport: true);
            predefinedReportsUpdater.AddPredefinedReport<ReportReceivableTransaction2>("Report Receivable Transaction 2", typeof(ReceivableTransaction2), isInplaceReport: true);
            predefinedReportsUpdater.AddPredefinedReport<ReportCustomerOrder>("Report Customer Order", typeof(CustomerOrder), isInplaceReport: true);
            predefinedReportsUpdater.AddPredefinedReport<ReportCustomerInvoice>("Report Customer Invoice", typeof(CustomerInvoice), isInplaceReport: true);
            predefinedReportsUpdater.AddPredefinedReport<ReportInventoryTransferOut>("Report Inventory Transfer Out", typeof(InventoryTransferOut), isInplaceReport: true);
            return new ModuleUpdater[] { updater, predefinedReportsUpdater };
        }

        public override void Setup(XafApplication application) {
            base.Setup(application);
            // Manage various aspects of the application UI and behavior at the module level.
        }

        public override void CustomizeTypesInfo(ITypesInfo typesInfo) {
            base.CustomizeTypesInfo(typesInfo);
            CalculatedPersistentAliasHelper.CustomizeTypesInfo(typesInfo);
        }
    }
}
