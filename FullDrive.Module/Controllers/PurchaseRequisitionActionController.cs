﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;

using FullDrive.Module.BusinessObjects;
using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.Xpo;
using System.Collections;
using DevExpress.Xpo;

using System.Web;
using System.IO;

namespace FullDrive.Module.Controllers
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class PurchaseRequisitionActionController : ViewController
    {
        private ChoiceActionItem _selectionListviewFilter;
        private ChoiceActionItem _selectionApprovalListviewFilter;
        private ChoiceActionItem _setApprovalLevel;

        public PurchaseRequisitionActionController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.
            #region FilterStatus
            PurchaseRequisitionListviewFilterSelectionAction.Items.Clear();
            foreach (object _currApproval in Enum.GetValues(typeof(CustomProcess.Status)))
            {
                EnumDescriptor _ed = new EnumDescriptor(typeof(CustomProcess.Status));
                if (_ed.GetCaption(_currApproval) != "Approved")
                {
                    _selectionListviewFilter = new ChoiceActionItem(_ed.GetCaption(_currApproval), _currApproval);
                }
                PurchaseRequisitionListviewFilterSelectionAction.Items.Add(_selectionListviewFilter);
            }
            #endregion FilterStatus

            #region FilterApproval
            PurchaseRequisitionListviewFilterApprovalSelectionAction.Items.Clear();
            foreach (object _currApprovalFilter in Enum.GetValues(typeof(CustomProcess.ApprovalFilter)))
            {
                EnumDescriptor _ed = new EnumDescriptor(typeof(CustomProcess.ApprovalFilter));
                _selectionApprovalListviewFilter = new ChoiceActionItem(_ed.GetCaption(_currApprovalFilter), _currApprovalFilter);
                PurchaseRequisitionListviewFilterApprovalSelectionAction.Items.Add(_selectionApprovalListviewFilter);
            }
            #endregion FilterApproval
        }

        protected override void OnActivated()
        {
            TypeOfView = typeof(ObjectView);
            base.OnActivated();
            // Perform various tasks depending on the target View.
            #region Approval
            var _locUser = SecuritySystem.CurrentUserName;

            if (this.ObjectSpace != null)
            {
                UserAccess _locUserAccess = this.ObjectSpace.FindObject<UserAccess>(new BinaryOperator("UserName", _locUser));

                if (_locUserAccess != null)
                {
                    PurchaseRequisitionApprovalAction.Items.Clear();

                    XPCollection<ApplicationSetupDetail> _locAppSetupDetails = new XPCollection<ApplicationSetupDetail>
                                                                                 (_locUserAccess.Session, new GroupOperator(GroupOperatorType.And,
                                                                                  new BinaryOperator("UserAccess", _locUserAccess),
                                                                                  new BinaryOperator("ObjectList", CustomProcess.ObjectList.PurchaseRequisition),
                                                                                  new BinaryOperator("FunctionList", CustomProcess.FunctionList.Approval),
                                                                                  new BinaryOperator("Active", true)));

                    if (_locAppSetupDetails != null && _locAppSetupDetails.Count > 0)
                    {

                        foreach (ApplicationSetupDetail _locAppSetupDetail in _locAppSetupDetails)
                        {
                            foreach (object _currApproval in Enum.GetValues(typeof(CustomProcess.ApprovalLevel)))
                            {
                                if (_locAppSetupDetail.ApprovalLevel != CustomProcess.ApprovalLevel.None)
                                {
                                    EnumDescriptor _ed = new EnumDescriptor(typeof(CustomProcess.ApprovalLevel));
                                    _setApprovalLevel = new ChoiceActionItem(_ed.GetCaption(_locAppSetupDetail.ApprovalLevel), _locAppSetupDetail.ApprovalLevel);
                                }
                            }
                            PurchaseRequisitionApprovalAction.Items.Add(_setApprovalLevel);
                        }
                    }
                }
            }
            #endregion Approval
        }

        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }

        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        private void PurchaseRequisitionApprovalAction_Execute(object sender, SingleChoiceActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                ApplicationSetupDetail _locAppSetDetail = null;
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                var User = SecuritySystem.CurrentUserName;
                Session _currentSession = null;
                string _currObjectId = null;

                foreach (Object obj in _objectsToProcess)
                {
                    PurchaseRequisition _objInNewObjectSpace = (PurchaseRequisition)_objectSpace.GetObject(obj);

                    if (_objInNewObjectSpace != null)
                    {
                        if (_objInNewObjectSpace.Code != null)
                        {
                            _currObjectId = _objInNewObjectSpace.Code;
                        }
                        if (_objInNewObjectSpace.Session != null)
                        {
                            _currentSession = _objInNewObjectSpace.Session;
                        }
                    }

                    if (_currObjectId != null && _currentSession != null)
                    {
                        UserAccess _locUserAccess = _currentSession.FindObject<UserAccess>(new BinaryOperator("UserName", User));

                        PurchaseRequisition _locPurchaseRequisitionXPO = _currentSession.FindObject<PurchaseRequisition>
                                                                        (new GroupOperator(GroupOperatorType.And,
                                                                         new BinaryOperator("Code", _currObjectId),
                                                                         new BinaryOperator("Status", Status.Open)));

                        if (_locPurchaseRequisitionXPO != null)
                        {
                            ApprovalLine _locApprovalLine = _currentSession.FindObject<ApprovalLine>
                                                                            (new GroupOperator(GroupOperatorType.And,
                                                                             new BinaryOperator("PurchaseRequisition", _locPurchaseRequisitionXPO),
                                                                             new BinaryOperator("EndApproval", true)));
                            if (_locApprovalLine == null)
                            {
                                #region Approval Level 1
                                if ((ApprovalLevel)e.SelectedChoiceActionItem.Data == ApprovalLevel.Level1)
                                {
                                    _locAppSetDetail = _globFunc.GetApplicationSetupDetailByApprovalLevel(_currentSession, _locUserAccess, ApprovalLevel.Level1, ObjectList.PurchaseRequisition);
                                    if (_locAppSetDetail != null)
                                    {
                                        //Buat bs input langsung ke approvalline
                                        ApprovalLine _locApprovalLineXPO = _currentSession.FindObject<ApprovalLine>
                                                                            (new GroupOperator(GroupOperatorType.And,
                                                                             new BinaryOperator("PurchaseRequisition", _locPurchaseRequisitionXPO),
                                                                             new BinaryOperator("ApprovalLevel", ApprovalLevel.Level1)));
                                        if (_locApprovalLineXPO == null)
                                        {

                                            if (_locAppSetDetail.EndApproval == true)
                                            {
                                                ApprovalLine _saveDataAL = new ApprovalLine(_currentSession)
                                                {
                                                    ApprovalDate = now,
                                                    ApprovalStatus = Status.Approved,
                                                    ApprovalLevel = ApprovalLevel.Level1,
                                                    EndApproval = true,
                                                    PurchaseRequisition = _locPurchaseRequisitionXPO,
                                                };
                                                _saveDataAL.Save();
                                                _saveDataAL.Session.CommitTransaction();

                                                _locPurchaseRequisitionXPO.ActivationPosting = true;
                                                _locPurchaseRequisitionXPO.ActiveApproved1 = false;
                                                _locPurchaseRequisitionXPO.ActiveApproved2 = false;
                                                _locPurchaseRequisitionXPO.ActiveApproved3 = true;
                                                _locPurchaseRequisitionXPO.Save();
                                                _locPurchaseRequisitionXPO.Session.CommitTransaction();
                                            }
                                            else
                                            {
                                                ApprovalLine _saveDataAL = new ApprovalLine(_currentSession)
                                                {
                                                    ApprovalDate = now,
                                                    ApprovalStatus = Status.Approved,
                                                    ApprovalLevel = ApprovalLevel.Level1,
                                                    PurchaseRequisition = _locPurchaseRequisitionXPO,
                                                };
                                                _saveDataAL.Save();
                                                _saveDataAL.Session.CommitTransaction();

                                                _locPurchaseRequisitionXPO.ActiveApproved1 = true;
                                                _locPurchaseRequisitionXPO.ActiveApproved2 = false;
                                                _locPurchaseRequisitionXPO.ActiveApproved3 = false;
                                                _locPurchaseRequisitionXPO.Save();
                                                _locPurchaseRequisitionXPO.Session.CommitTransaction();
                                            }

                                            //Send Email
                                            ApprovalLine _locApprovalLineXPO2 = _currentSession.FindObject<ApprovalLine>
                                                                                (new GroupOperator(GroupOperatorType.And,
                                                                                 new BinaryOperator("PurchaseRequisition", _locPurchaseRequisitionXPO)));
                                            if (_locApprovalLineXPO2 != null)
                                            {
                                                SendEmail(_currentSession, _locPurchaseRequisitionXPO, Status.Progress, _locApprovalLineXPO2);
                                            }

                                            SuccessMessageShow("Purchase Requisition has successfully Approve");
                                        }
                                    }
                                }
                                #endregion Approval Level 1

                                #region Approval Level 2
                                if ((ApprovalLevel)e.SelectedChoiceActionItem.Data == ApprovalLevel.Level2)
                                {
                                    _locAppSetDetail = _globFunc.GetApplicationSetupDetailByApprovalLevel(_currentSession, _locUserAccess, ApprovalLevel.Level2, ObjectList.PurchaseRequisition);

                                    if (_locAppSetDetail != null)
                                    {
                                        ApprovalLine _locApprovalLineXPO = _currentSession.FindObject<ApprovalLine>
                                                                            (new GroupOperator(GroupOperatorType.And,
                                                                             new BinaryOperator("PurchaseRequisition", _locPurchaseRequisitionXPO),
                                                                             new BinaryOperator("ApprovalLevel", ApprovalLevel.Level2)));

                                        if (_locApprovalLineXPO == null)
                                        {
                                            if (_locAppSetDetail.EndApproval == true)
                                            {
                                                ApprovalLine _saveDataAL = new ApprovalLine(_currentSession)
                                                {
                                                    ApprovalDate = now,
                                                    ApprovalStatus = Status.Approved,
                                                    ApprovalLevel = ApprovalLevel.Level2,
                                                    EndApproval = true,
                                                    PurchaseRequisition = _locPurchaseRequisitionXPO,
                                                };
                                                _saveDataAL.Save();
                                                _saveDataAL.Session.CommitTransaction();

                                                _locPurchaseRequisitionXPO.ActivationPosting = true;
                                                _locPurchaseRequisitionXPO.ActiveApproved1 = false;
                                                _locPurchaseRequisitionXPO.ActiveApproved2 = false;
                                                _locPurchaseRequisitionXPO.ActiveApproved3 = true;
                                                _locPurchaseRequisitionXPO.Save();
                                                _locPurchaseRequisitionXPO.Session.CommitTransaction();
                                            }
                                            else
                                            {
                                                ApprovalLine _saveDataAL = new ApprovalLine(_currentSession)
                                                {
                                                    ApprovalDate = now,
                                                    ApprovalStatus = Status.Approved,
                                                    ApprovalLevel = ApprovalLevel.Level2,
                                                    PurchaseRequisition = _locPurchaseRequisitionXPO,
                                                };
                                                _saveDataAL.Save();
                                                _saveDataAL.Session.CommitTransaction();

                                                SetApprovalLine(_currentSession, _locPurchaseRequisitionXPO, ApprovalLevel.Level1);

                                                _locPurchaseRequisitionXPO.ActiveApproved1 = false;
                                                _locPurchaseRequisitionXPO.ActiveApproved2 = true;
                                                _locPurchaseRequisitionXPO.ActiveApproved3 = false;
                                                _locPurchaseRequisitionXPO.Save();
                                                _locPurchaseRequisitionXPO.Session.CommitTransaction();
                                            }

                                            //Send Email
                                            ApprovalLine _locApprovalLineXPO2 = _currentSession.FindObject<ApprovalLine>
                                                                                (new GroupOperator(GroupOperatorType.And,
                                                                                 new BinaryOperator("PurchaseRequisition", _locPurchaseRequisitionXPO)));
                                            if (_locApprovalLineXPO2 != null)
                                            {
                                                SendEmail(_currentSession, _locPurchaseRequisitionXPO, Status.Progress, _locApprovalLineXPO2);
                                            }

                                            SuccessMessageShow("Purchase Requisition has successfully Approve");
                                        }
                                    }
                                }
                                #endregion Approval Level 2

                                #region Approval Level 3
                                if ((ApprovalLevel)e.SelectedChoiceActionItem.Data == ApprovalLevel.Level3)
                                {
                                    _locAppSetDetail = _globFunc.GetApplicationSetupDetailByApprovalLevel(_currentSession, _locUserAccess, ApprovalLevel.Level3, ObjectList.PurchaseRequisition);

                                    if (_locAppSetDetail != null)
                                    {
                                        ApprovalLine _locApprovalLineXPO = _currentSession.FindObject<ApprovalLine>
                                                                            (new GroupOperator(GroupOperatorType.And,
                                                                             new BinaryOperator("PurchaseRequisition", _locPurchaseRequisitionXPO),
                                                                             new BinaryOperator("ApprovalLevel", ApprovalLevel.Level3)));

                                        if (_locApprovalLineXPO == null)
                                        {
                                            if (_locAppSetDetail.EndApproval == true)
                                            {
                                                ApprovalLine _saveDataAL = new ApprovalLine(_currentSession)
                                                {
                                                    ApprovalDate = now,
                                                    ApprovalStatus = Status.Approved,
                                                    ApprovalLevel = ApprovalLevel.Level3,
                                                    EndApproval = true,
                                                    PurchaseRequisition = _locPurchaseRequisitionXPO,
                                                };
                                                _saveDataAL.Save();
                                                _saveDataAL.Session.CommitTransaction();

                                                _locPurchaseRequisitionXPO.ActivationPosting = true;
                                                _locPurchaseRequisitionXPO.ActiveApproved1 = false;
                                                _locPurchaseRequisitionXPO.ActiveApproved2 = false;
                                                _locPurchaseRequisitionXPO.ActiveApproved3 = true;
                                                _locPurchaseRequisitionXPO.Save();
                                                _locPurchaseRequisitionXPO.Session.CommitTransaction();
                                            }
                                            else
                                            {
                                                ApprovalLine _saveDataAL = new ApprovalLine(_currentSession)
                                                {
                                                    ApprovalDate = now,
                                                    ApprovalStatus = Status.Approved,
                                                    ApprovalLevel = ApprovalLevel.Level3,
                                                    PurchaseRequisition = _locPurchaseRequisitionXPO,
                                                };
                                                _saveDataAL.Save();
                                                _saveDataAL.Session.CommitTransaction();

                                                SetApprovalLine(_currentSession, _locPurchaseRequisitionXPO, ApprovalLevel.Level2);
                                                SetApprovalLine(_currentSession, _locPurchaseRequisitionXPO, ApprovalLevel.Level1);

                                                _locPurchaseRequisitionXPO.ActiveApproved1 = false;
                                                _locPurchaseRequisitionXPO.ActiveApproved2 = false;
                                                _locPurchaseRequisitionXPO.ActiveApproved3 = true;
                                                _locPurchaseRequisitionXPO.Save();
                                                _locPurchaseRequisitionXPO.Session.CommitTransaction();
                                            }

                                            //Send Email
                                            ApprovalLine _locApprovalLineXPO2 = _currentSession.FindObject<ApprovalLine>
                                                                                (new GroupOperator(GroupOperatorType.And,
                                                                                 new BinaryOperator("PurchaseRequisition", _locPurchaseRequisitionXPO)));
                                            if (_locApprovalLineXPO2 != null)
                                            {
                                                SendEmail(_currentSession, _locPurchaseRequisitionXPO, Status.Progress, _locApprovalLineXPO2);
                                            }

                                            SuccessMessageShow("Purchase Requisition has successfully Approve");
                                        }
                                    }
                                }
                                #endregion Approval Level 3
                            }
                        }
                        else
                        {
                            ErrorMessageShow("Purchase Requisition Not Available");
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PurchaseRequisition " + ex.ToString());
            }
        }

        private void PurchaseRequisitionProgressAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;
                Status _locStatus = Status.Open;

                if (_objectsToProcess != null)
                {
                    foreach (Object obj in _objectsToProcess)
                    {
                        PurchaseRequisition _locPurchaseRequisitionOS = (PurchaseRequisition)_objectSpace.GetObject(obj);

                        if (_locPurchaseRequisitionOS != null)
                        {
                            if(_locPurchaseRequisitionOS.Session != null)
                            {
                                _currSession = _locPurchaseRequisitionOS.Session;
                            }

                            if (_locPurchaseRequisitionOS.Code != null && _currSession != null)
                            {
                                _currObjectId = _locPurchaseRequisitionOS.Code;

                                PurchaseRequisition _locPurchaseRequisitionXPO = _currSession.FindObject<PurchaseRequisition>
                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                     new BinaryOperator("Code", _currObjectId),
                                                                     new GroupOperator(GroupOperatorType.Or,
                                                                     new BinaryOperator("Status", Status.Open),
                                                                     new BinaryOperator("Status", Status.Progress))));

                                if (_locPurchaseRequisitionXPO != null)
                                {
                                    ApprovalLine _locApprovalLine = _currSession.FindObject<ApprovalLine>
                                                                            (new GroupOperator(GroupOperatorType.And,
                                                                             new BinaryOperator("PurchaseRequisition", _locPurchaseRequisitionXPO),
                                                                             new BinaryOperator("EndApproval", true)));

                                    if(_locApprovalLine != null)
                                    {
                                        if (_locPurchaseRequisitionXPO.Status == Status.Open)
                                        {
                                            _locStatus = Status.Progress;
                                        }
                                        else
                                        {
                                            _locStatus = _locPurchaseRequisitionXPO.Status;
                                        }

                                        _locPurchaseRequisitionXPO.Status = _locStatus;
                                        _locPurchaseRequisitionXPO.StatusDate = now;
                                        _locPurchaseRequisitionXPO.Save();
                                        _locPurchaseRequisitionXPO.Session.CommitTransaction();

                                        #region StatusPurchaseRequisitionLine
                                        XPCollection<PurchaseRequisitionLine> _locPurchaseRequisitionLines = new XPCollection<PurchaseRequisitionLine>
                                                           (_currSession, new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("PurchaseRequisition", _locPurchaseRequisitionXPO),
                                                            new GroupOperator(GroupOperatorType.Or,
                                                            new BinaryOperator("Status", Status.Open),
                                                            new BinaryOperator("Status", Status.Progress))));

                                        if (_locPurchaseRequisitionLines != null && _locPurchaseRequisitionLines.Count > 0)
                                        {
                                            foreach (PurchaseRequisitionLine _locPurchaseRequisitionLine in _locPurchaseRequisitionLines)
                                            {
                                                _locPurchaseRequisitionLine.Status = Status.Progress;
                                                _locPurchaseRequisitionLine.StatusDate = now;
                                                _locPurchaseRequisitionLine.Save();
                                                _locPurchaseRequisitionLine.Session.CommitTransaction();
                                            }
                                        }
                                        #endregion StatusPurchaseRequisitionLine

                                        #region PurchaseRequisitionMonitoring
                                        XPCollection<PurchaseRequisitionMonitoring> _locPurchaseRequisitionMonitorings = new XPCollection<PurchaseRequisitionMonitoring>
                                                                                                    (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                    new BinaryOperator("Status", Status.Open)));
                                        if (_locPurchaseRequisitionMonitorings != null && _locPurchaseRequisitionMonitorings.Count() > 0)
                                        {
                                            foreach (PurchaseRequisitionMonitoring _locPurchaseRequisitionMonitoring in _locPurchaseRequisitionMonitorings)
                                            {
                                                _locPurchaseRequisitionMonitoring.Status = Status.Open;
                                                _locPurchaseRequisitionMonitoring.StatusDate = now;
                                                _locPurchaseRequisitionMonitoring.Save();
                                                _locPurchaseRequisitionMonitoring.Session.CommitTransaction();
                                            }
                                        }
                                        #endregion PurchaseRequisitionMonitoring
                                    }

                                    SuccessMessageShow("Purchase Requisition has successfully updated to progress");
                                }
                                else
                                {
                                    ErrorMessageShow("Data Purchase Requisition Not Available");
                                }
                            }
                            else
                            {
                                ErrorMessageShow("Data Purchase Requisition Not Available");
                            }
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PurchaseRequisition " + ex.ToString());
            }
        }

        private void PurchaseRequisitionPostingAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;

                if (_objectsToProcess != null)
                {
                    foreach (Object obj in _objectsToProcess)
                    {
                        PurchaseRequisition _locPurchaseRequisitionOS = (PurchaseRequisition)_objectSpace.GetObject(obj);

                        if (_locPurchaseRequisitionOS != null)
                        {
                            if (_locPurchaseRequisitionOS.Session != null)
                            {
                                _currSession = _locPurchaseRequisitionOS.Session;
                            }

                            if(_locPurchaseRequisitionOS.Code != null && _currSession != null)
                            {
                                _currObjectId = _locPurchaseRequisitionOS.Code;

                                PurchaseRequisition _locPurchaseRequisitionXPO = _currSession.FindObject<PurchaseRequisition>
                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                     new BinaryOperator("Code", _currObjectId),
                                                                     new GroupOperator(GroupOperatorType.Or,
                                                                     new BinaryOperator("Status", Status.Progress),
                                                                     new BinaryOperator("Status", Status.Posted))));

                                if (_locPurchaseRequisitionXPO != null)
                                {
                                    SetPurchaseRequisitionMonitoring(_currSession, _locPurchaseRequisitionXPO);
                                    SetStatusPurchaseRequisitionLine(_currSession, _locPurchaseRequisitionXPO);
                                    SetNormal(_currSession, _locPurchaseRequisitionXPO);
                                    SetFinalStatusPurchaseRequisition(_currSession, _locPurchaseRequisitionXPO);
                                    SuccessMessageShow("Purchase Requisition has successfully posted into Purchase Requisition Monitoring");
                                }
                                else
                                {
                                    ErrorMessageShow("Data Purchase Requisition Not Available");
                                }
                            }
                            else
                            {
                                ErrorMessageShow("Data Purchase Requisition Not Available");
                            }
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PurchaseRequisition " + ex.ToString());
            }
        }     

        private void PurchaseRequisitionListviewFilterApprovalSelectionAction_Execute(object sender, SingleChoiceActionExecuteEventArgs e)
        {
            try
            {
                if ((View is ListView) & (View.ObjectTypeInfo.Type == typeof(PurchaseRequisition)))
                {
                    if ((ApprovalFilter)e.SelectedChoiceActionItem.Data == ApprovalFilter.Approval)
                    {
                        ((ListView)View).CollectionSource.Criteria["FilterApproval1"] = new GroupOperator(GroupOperatorType.And,
                                                                                        new BinaryOperator("ActiveApproved3", false, BinaryOperatorType.Equal),
                                                                                        new GroupOperator(GroupOperatorType.Or,
                                                                                        new BinaryOperator("ActiveApproved1", true, BinaryOperatorType.Equal),
                                                                                        new BinaryOperator("ActiveApproved2", true, BinaryOperatorType.Equal)));
                    }
                    else if ((ApprovalFilter)e.SelectedChoiceActionItem.Data == ApprovalFilter.EndApproval)
                    {
                        ((ListView)View).CollectionSource.Criteria["FilterApproval1"] = new BinaryOperator("ActiveApproved3", true, BinaryOperatorType.Equal);
                    }
                    else if ((ApprovalFilter)e.SelectedChoiceActionItem.Data == ApprovalFilter.AllData)
                    {
                        ((ListView)View).CollectionSource.Criteria["FilterApproval1"] = new GroupOperator(GroupOperatorType.Or,
                                                                                        new BinaryOperator("ActiveApproved1", true, BinaryOperatorType.Equal),
                                                                                        new BinaryOperator("ActiveApproved2", true, BinaryOperatorType.Equal),
                                                                                        new BinaryOperator("ActiveApproved3", true, BinaryOperatorType.Equal),
                                                                                        new BinaryOperator("ActiveApproved1", false, BinaryOperatorType.Equal),
                                                                                        new BinaryOperator("ActiveApproved2", false, BinaryOperatorType.Equal),
                                                                                        new BinaryOperator("ActiveApproved3", false, BinaryOperatorType.Equal));
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PurchaseRequisition " + ex.ToString());
            }
        }

        private void PurchaseRequisitionListviewFilterSelectionAction_Execute(object sender, SingleChoiceActionExecuteEventArgs e)
        {
            try
            {
                if ((View is ListView) & (View.ObjectTypeInfo.Type == typeof(PurchaseRequisition)))
                {
                    if ((Status)e.SelectedChoiceActionItem.Data == Status.None)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.None, BinaryOperatorType.NotEqual);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Open)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Open, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Progress)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Progress, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Posted)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Posted, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Lock)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Lock, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Close)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Close, BinaryOperatorType.Equal);
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PurchaseRequisition " + ex.ToString());
            }
        }

        //============================================= Code In Here =============================================

        #region Posting

        private void SetPurchaseRequisitionMonitoring(Session _currSession, PurchaseRequisition _locPurchaseRequisitionXPO)
        {
            try
            {
                DateTime now = DateTime.Now;
                if (_locPurchaseRequisitionXPO != null)
                {
                    XPCollection<PurchaseRequisitionLine> _locPurchaseRequisitionLines = new XPCollection<PurchaseRequisitionLine>(_currSession,
                                                            new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("PurchaseRequisition", _locPurchaseRequisitionXPO),
                                                            new BinaryOperator("Select", true),
                                                            new GroupOperator(GroupOperatorType.Or,
                                                            new BinaryOperator("Status", Status.Progress),
                                                            new BinaryOperator("Status", Status.Posted))));

                    if (_locPurchaseRequisitionLines != null && _locPurchaseRequisitionLines.Count() > 0)
                    {
                        foreach (PurchaseRequisitionLine _locPurchaseRequisitionLine in _locPurchaseRequisitionLines)
                        {
                            PurchaseRequisitionMonitoring _saveDataPurchaseRequisitionMonitoring = new PurchaseRequisitionMonitoring(_currSession)
                            {
                                PurchaseRequisition = _locPurchaseRequisitionXPO,
                                PurchaseRequisitionLine = _locPurchaseRequisitionLine,
                                PurchaseType = _locPurchaseRequisitionLine.PurchaseType,
                                Item = _locPurchaseRequisitionLine.Item,
                                Brand = _locPurchaseRequisitionLine.Brand,
                                Description = _locPurchaseRequisitionLine.Description,
                                MxDQty = _locPurchaseRequisitionLine.DQty,
                                MxDUOM = _locPurchaseRequisitionLine.DUOM,
                                MxQty = _locPurchaseRequisitionLine.Qty,
                                MxUOM = _locPurchaseRequisitionLine.UOM,
                                MxTQty = _locPurchaseRequisitionLine.TQty,
                                DQty = _locPurchaseRequisitionLine.DQty,
                                DUOM = _locPurchaseRequisitionLine.DUOM,
                                Qty = _locPurchaseRequisitionLine.Qty,
                                UOM = _locPurchaseRequisitionLine.UOM,
                                TQty = _locPurchaseRequisitionLine.TQty,
                                Company = _locPurchaseRequisitionLine.Company,
                                Workplace = _locPurchaseRequisitionLine.Workplace,
                                Division = _locPurchaseRequisitionLine.Division,
                                Department = _locPurchaseRequisitionLine.Department,
                                Section = _locPurchaseRequisitionLine.Section,
                                Employee = _locPurchaseRequisitionLine.Employee,
                                Div = _locPurchaseRequisitionLine.Div,
                                Dept = _locPurchaseRequisitionLine.Dept,
                                Sect = _locPurchaseRequisitionLine.Sect,
                            };
                            _saveDataPurchaseRequisitionMonitoring.Save();
                            _saveDataPurchaseRequisitionMonitoring.Session.CommitTransaction();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PurchaseRequisition " + ex.ToString());
            }
        }

        private void SetStatusPurchaseRequisitionLine(Session _currSession, PurchaseRequisition _locPurchaseRequisitionXPO)
        {
            try
            {
                DateTime now = DateTime.Now;
                if (_locPurchaseRequisitionXPO != null)
                {
                    XPCollection<PurchaseRequisitionLine> _locPurchaseRequisitionLines = new XPCollection<PurchaseRequisitionLine>(_currSession,
                                                            new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("PurchaseRequisition", _locPurchaseRequisitionXPO),
                                                            new BinaryOperator("Select", true),
                                                            new GroupOperator(GroupOperatorType.Or,
                                                            new BinaryOperator("Status", Status.Progress),
                                                            new BinaryOperator("Status", Status.Posted))));

                    if (_locPurchaseRequisitionLines != null && _locPurchaseRequisitionLines.Count > 0)
                    {
                        foreach (PurchaseRequisitionLine _locPurchaseRequisitionLine in _locPurchaseRequisitionLines)
                        {
                            _locPurchaseRequisitionLine.Status = Status.Close;
                            _locPurchaseRequisitionLine.ActivationPosting = true;
                            _locPurchaseRequisitionLine.StatusDate = now;
                            _locPurchaseRequisitionLine.Save();
                            _locPurchaseRequisitionLine.Session.CommitTransaction();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PurchaseRequisition " + ex.ToString());
            }
        }

        private void SetNormal(Session _currSession, PurchaseRequisition _locPurchaseRequisitionXPO)
        {
            try
            {
                DateTime now = DateTime.Now;

                if (_locPurchaseRequisitionXPO != null)
                {
                    XPCollection<PurchaseRequisitionLine> _locPurchaseRequisitionLines = new XPCollection<PurchaseRequisitionLine>(_currSession,
                                                                                     new GroupOperator(GroupOperatorType.And,
                                                                                     new BinaryOperator("PurchaseRequisition", _locPurchaseRequisitionXPO),
                                                                                     new BinaryOperator("Select", true),
                                                                                     new GroupOperator(GroupOperatorType.Or,
                                                                                     new BinaryOperator("Status", Status.Progress),
                                                                                     new BinaryOperator("Status", Status.Posted),
                                                                                     new BinaryOperator("Status", Status.Close))));

                    if (_locPurchaseRequisitionLines != null && _locPurchaseRequisitionLines.Count > 0)
                    {
                        foreach (PurchaseRequisitionLine _locPurchaseRequisitionLine in _locPurchaseRequisitionLines)
                        {
                            _locPurchaseRequisitionLine.Select = false;
                            _locPurchaseRequisitionLine.Save();
                            _locPurchaseRequisitionLine.Session.CommitTransaction();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PurchaseRequisition " + ex.ToString());
            }
        }

        private void SetFinalStatusPurchaseRequisition(Session _currSession, PurchaseRequisition _locPurchaseRequisitionXPO)
        {
            try
            {
                DateTime now = DateTime.Now;

                if (_locPurchaseRequisitionXPO != null)
                {
                    XPCollection<PurchaseRequisitionLine> _locPurchaseRequisitionLines = new XPCollection<PurchaseRequisitionLine>(_currSession,
                                                            new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("PurchaseRequisition", _locPurchaseRequisitionXPO),
                                                            new GroupOperator(GroupOperatorType.Or,
                                                            new BinaryOperator("Status", Status.Close))));

                    if (_locPurchaseRequisitionLines != null && _locPurchaseRequisitionLines.Count() > 0)
                    {
                        foreach (PurchaseRequisitionLine _locPurchaseRequisitionLine in _locPurchaseRequisitionLines)
                        {
                            _locPurchaseRequisitionXPO.ActivationPosting = true;
                            _locPurchaseRequisitionXPO.Status = Status.Close;
                            _locPurchaseRequisitionXPO.StatusDate = now;
                            _locPurchaseRequisitionXPO.Save();
                            _locPurchaseRequisitionXPO.Session.CommitTransaction();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PurchaseRequisition " + ex.ToString());
            }
        }

        #endregion Posting

        #region Approval Process

        private void SetApprovalLine(Session _currentSession, PurchaseRequisition _locPurchaseRequisitionXPO, ApprovalLevel _locApprovalLevel)
        {
            try
            {
                DateTime now = DateTime.Now;

                ApprovalLine _locApprovalLineXPO = _currentSession.FindObject<ApprovalLine>
                                                                        (new GroupOperator(GroupOperatorType.And,
                                                                         new BinaryOperator("PurchaseRequisition", _locPurchaseRequisitionXPO),
                                                                         new BinaryOperator("ApprovalLevel", _locApprovalLevel)));

                if (_locApprovalLineXPO == null)
                {
                    ApprovalLine _saveDataAL2 = new ApprovalLine(_currentSession)
                    {
                        ApprovalDate = now,
                        ApprovalStatus = Status.Approved,
                        ApprovalLevel = _locApprovalLevel,
                        PurchaseRequisition = _locPurchaseRequisitionXPO,
                    };
                    _saveDataAL2.Save();
                    _saveDataAL2.Session.CommitTransaction();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PurchaseRequisition " + ex.ToString());
            }
        }

        #endregion Approval Process

        #region Email

        private string BackgroundBody(Session _currentSession, PurchaseRequisition _locPurchaseRequisitionXPO, MailSetupDetail _locMailSetDetail)
        {
            string body = string.Empty;

            PurchaseRequisition _locPurchaseRequisitions = _currentSession.FindObject<PurchaseRequisition>
                                           (new GroupOperator(GroupOperatorType.And,
                                            new BinaryOperator("Code", _locPurchaseRequisitionXPO.Code)));

            ApprovalLine _locApprovalLineXPO = _currentSession.FindObject<ApprovalLine>
                                               (new GroupOperator(GroupOperatorType.And,
                                                new BinaryOperator("PurchaseRequisition", _locPurchaseRequisitions)));

            string Status = _locApprovalLineXPO.ApprovalStatus.ToString();

            using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/Email/EmailTemplate.html")))
            {
                body = reader.ReadToEnd();
            }
            body = body.Replace("{Code}", _locPurchaseRequisitionXPO.Code);

            #region Level
            if (_locPurchaseRequisitions.ActiveApproved1 == true)
            {
                string Active = "Level 1";
                body = body.Replace("{Level}", Active);
            }
            else if (_locPurchaseRequisitions.ActiveApproved2 == true)
            {
                string Active = "Level 2";
                body = body.Replace("{Level}", Active);
            }
            else if (_locPurchaseRequisitions.ActiveApproved3 == true)
            {
                string Active = "Level 3";
                body = body.Replace("{Level}", Active);
            }
            else
            {
                string Active = "Not Available";
                body = body.Replace("{Level}", Active);
            }
            #endregion Level

            body = body.Replace("{Status}", Status);
            body = body.Replace("{Message}", _locMailSetDetail.MessageBody);
            return body;
        }

        protected void SendEmail(Session _currentSession, PurchaseRequisition _locPurchaseRequisitionXPO, Status _locStatus, ApprovalLine _locApprovalLineXPO2)
        {
            DateTime now = DateTime.Now;
            var User = SecuritySystem.CurrentUserName;
            GlobalFunction _globFunc = new GlobalFunction();
            UserAccess _numLineUserAccess = _currentSession.FindObject<UserAccess>(new BinaryOperator("UserName", User));
            string _locMessageBody = null;

            try
            {
                if (_locPurchaseRequisitionXPO != null)
                {
                    XPCollection<MailSetupDetail> _locMailSetDetails = new XPCollection<MailSetupDetail>
                                                                       (_currentSession, new GroupOperator(GroupOperatorType.And,
                                                                        new BinaryOperator("ObjectList", ObjectList.PurchaseRequisition),
                                                                        new BinaryOperator("UserAccess", _numLineUserAccess),
                                                                        new BinaryOperator("Active", true)));

                    if (_locMailSetDetails != null && _locMailSetDetails.Count > 0)
                    {
                        foreach (MailSetupDetail _locMailSetDetail in _locMailSetDetails)
                        {
                            if (_locMailSetDetail.MessageBody != null)
                            {
                                if (_locApprovalLineXPO2.EndApproval == true)
                                {
                                    _locMessageBody = this.BackgroundBody(_currentSession, _locPurchaseRequisitionXPO, _locMailSetDetail);
                                }
                                else
                                {
                                    _locMessageBody = this.BackgroundBody(_currentSession, _locPurchaseRequisitionXPO, _locMailSetDetail);
                                }
                            }

                            _globFunc.SetAndSendMail(_locMailSetDetail.SmtpHost, _locMailSetDetail.SmtpPort,
                            _locMailSetDetail.MailFrom, _locMailSetDetail.MailFromPassword, _locMailSetDetail.MailTo, _locMailSetDetail.MailSubject, _locMessageBody);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PurchaseRequisition " + ex.ToString());
            }
        }

        #endregion Email

        #region Global Method

        private void SuccessMessageShow(string _locActionName)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 2000;
            options.Message = string.Format("{0} !", _locActionName);
            options.Type = InformationType.Success;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Success";
            options.Win.Type = WinMessageType.Flyout;
            Application.ShowViewStrategy.ShowMessage(options);
        }

        private void ErrorMessageShow(string _locMessage)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 2000;
            options.Message = string.Format(_locMessage);
            options.Type = InformationType.Warning;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Error";
            options.Win.Type = WinMessageType.Alert;
            Application.ShowViewStrategy.ShowMessage(options);
        }

        #endregion Global Method

    }
}
