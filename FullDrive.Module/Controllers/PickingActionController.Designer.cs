﻿namespace FullDrive.Module.Controllers
{
    partial class PickingActionController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.PickingProgressAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.PickingPostingAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            // 
            // PickingProgressAction
            // 
            this.PickingProgressAction.Caption = "Progress";
            this.PickingProgressAction.ConfirmationMessage = null;
            this.PickingProgressAction.Id = "PickingProgressActionId";
            this.PickingProgressAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.Picking);
            this.PickingProgressAction.ToolTip = null;
            this.PickingProgressAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.PickingProgressAction_Execute);
            // 
            // PickingPostingAction
            // 
            this.PickingPostingAction.Caption = "Posting";
            this.PickingPostingAction.ConfirmationMessage = null;
            this.PickingPostingAction.Id = "PickingPostingActionId";
            this.PickingPostingAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.Picking);
            this.PickingPostingAction.ToolTip = null;
            this.PickingPostingAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.PickingPostingAction_Execute);
            // 
            // PickingActionController
            // 
            this.Actions.Add(this.PickingProgressAction);
            this.Actions.Add(this.PickingPostingAction);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SimpleAction PickingProgressAction;
        private DevExpress.ExpressApp.Actions.SimpleAction PickingPostingAction;
    }
}
