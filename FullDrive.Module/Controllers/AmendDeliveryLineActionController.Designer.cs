﻿namespace FullDrive.Module.Controllers
{
    partial class AmendDeliveryLineActionController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.AmendDeliveryLineSelectAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.AmendDeliveryLineUnselectAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.AmendDeliveryLineShowSIAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.AmendDeliveryLineShowSICAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            // 
            // AmendDeliveryLineSelectAction
            // 
            this.AmendDeliveryLineSelectAction.Caption = "Select";
            this.AmendDeliveryLineSelectAction.ConfirmationMessage = null;
            this.AmendDeliveryLineSelectAction.Id = "AmendDeliveryLineSelectActionId";
            this.AmendDeliveryLineSelectAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.AmendDeliveryLine);
            this.AmendDeliveryLineSelectAction.ToolTip = null;
            this.AmendDeliveryLineSelectAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.AmendDeliveryLineSelectAction_Execute);
            // 
            // AmendDeliveryLineUnselectAction
            // 
            this.AmendDeliveryLineUnselectAction.Caption = "Unselect";
            this.AmendDeliveryLineUnselectAction.ConfirmationMessage = null;
            this.AmendDeliveryLineUnselectAction.Id = "AmendDeliveryLineUnselectActionId";
            this.AmendDeliveryLineUnselectAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.AmendDeliveryLine);
            this.AmendDeliveryLineUnselectAction.ToolTip = null;
            this.AmendDeliveryLineUnselectAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.AmendDeliveryLineUnselectAction_Execute);
            // 
            // AmendDeliveryLineShowSIAction
            // 
            this.AmendDeliveryLineShowSIAction.Caption = "SI";
            this.AmendDeliveryLineShowSIAction.Category = "RecordEdit";
            this.AmendDeliveryLineShowSIAction.ConfirmationMessage = null;
            this.AmendDeliveryLineShowSIAction.Id = "AmendDeliveryLineShowSIActionId";
            this.AmendDeliveryLineShowSIAction.SelectionDependencyType = DevExpress.ExpressApp.Actions.SelectionDependencyType.RequireSingleObject;
            this.AmendDeliveryLineShowSIAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.AmendDeliveryLine);
            this.AmendDeliveryLineShowSIAction.ToolTip = null;
            this.AmendDeliveryLineShowSIAction.TypeOfView = typeof(DevExpress.ExpressApp.View);
            this.AmendDeliveryLineShowSIAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.AmendDeliveryLineShowSIAction_Execute);
            // 
            // AmendDeliveryLineShowSICAction
            // 
            this.AmendDeliveryLineShowSICAction.Caption = "SIC";
            this.AmendDeliveryLineShowSICAction.Category = "RecordEdit";
            this.AmendDeliveryLineShowSICAction.ConfirmationMessage = null;
            this.AmendDeliveryLineShowSICAction.Id = "AmendDeliveryLineShowSICActionId";
            this.AmendDeliveryLineShowSICAction.SelectionDependencyType = DevExpress.ExpressApp.Actions.SelectionDependencyType.RequireSingleObject;
            this.AmendDeliveryLineShowSICAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.AmendDeliveryLine);
            this.AmendDeliveryLineShowSICAction.ToolTip = null;
            this.AmendDeliveryLineShowSICAction.TypeOfView = typeof(DevExpress.ExpressApp.View);
            this.AmendDeliveryLineShowSICAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.AmendDeliveryLineShowSICAction_Execute);
            // 
            // AmendDeliveryLineActionController
            // 
            this.Actions.Add(this.AmendDeliveryLineSelectAction);
            this.Actions.Add(this.AmendDeliveryLineUnselectAction);
            this.Actions.Add(this.AmendDeliveryLineShowSIAction);
            this.Actions.Add(this.AmendDeliveryLineShowSICAction);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SimpleAction AmendDeliveryLineSelectAction;
        private DevExpress.ExpressApp.Actions.SimpleAction AmendDeliveryLineUnselectAction;
        private DevExpress.ExpressApp.Actions.SimpleAction AmendDeliveryLineShowSIAction;
        private DevExpress.ExpressApp.Actions.SimpleAction AmendDeliveryLineShowSICAction;
    }
}
