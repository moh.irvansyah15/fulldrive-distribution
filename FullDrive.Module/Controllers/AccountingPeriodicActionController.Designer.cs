﻿namespace FullDrive.Module.Controllers
{
    partial class AccountingPeriodicActionController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.AccountingPeriodicImportAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            // 
            // AccountingPeriodicImportAction
            // 
            this.AccountingPeriodicImportAction.Caption = "Import";
            this.AccountingPeriodicImportAction.ConfirmationMessage = null;
            this.AccountingPeriodicImportAction.Id = "AccountingPeriodicImportActionId";
            this.AccountingPeriodicImportAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.AccountingPeriodic);
            this.AccountingPeriodicImportAction.ToolTip = null;
            this.AccountingPeriodicImportAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.AccountingPeriodicImportAction_Execute);
            // 
            // AccountingPeriodicActionController
            // 
            this.Actions.Add(this.AccountingPeriodicImportAction);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SimpleAction AccountingPeriodicImportAction;
    }
}
