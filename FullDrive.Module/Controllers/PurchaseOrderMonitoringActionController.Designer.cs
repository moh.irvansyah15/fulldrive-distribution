﻿namespace FullDrive.Module.Controllers
{
    partial class PurchaseOrderMonitoringActionController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.PurchaseOrderMonitoringSelectAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.PurchaseOrderMonitoringUnselectAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.PurchaseOrderMonitoringCancelITIAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            // 
            // PurchaseOrderMonitoringSelectAction
            // 
            this.PurchaseOrderMonitoringSelectAction.Caption = "Select";
            this.PurchaseOrderMonitoringSelectAction.ConfirmationMessage = null;
            this.PurchaseOrderMonitoringSelectAction.Id = "PurchaseOrderMonitoringSelectActionId";
            this.PurchaseOrderMonitoringSelectAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.PurchaseOrderMonitoring);
            this.PurchaseOrderMonitoringSelectAction.ToolTip = null;
            this.PurchaseOrderMonitoringSelectAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.PurchaseOrderMonitoringSelectAction_Execute);
            // 
            // PurchaseOrderMonitoringUnselectAction
            // 
            this.PurchaseOrderMonitoringUnselectAction.Caption = "Unselect";
            this.PurchaseOrderMonitoringUnselectAction.ConfirmationMessage = null;
            this.PurchaseOrderMonitoringUnselectAction.Id = "PurchaseOrderMonitoringUnselectActionId";
            this.PurchaseOrderMonitoringUnselectAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.PurchaseOrderMonitoring);
            this.PurchaseOrderMonitoringUnselectAction.ToolTip = null;
            this.PurchaseOrderMonitoringUnselectAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.PurchaseOrderMonitoringUnselectAction_Execute);
            // 
            // PurchaseOrderMonitoringCancelITIAction
            // 
            this.PurchaseOrderMonitoringCancelITIAction.Caption = "Cancel ITI";
            this.PurchaseOrderMonitoringCancelITIAction.ConfirmationMessage = null;
            this.PurchaseOrderMonitoringCancelITIAction.Id = "PurchaseOrderMonitoringCancelITIActionId";
            this.PurchaseOrderMonitoringCancelITIAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.PurchaseOrderMonitoring);
            this.PurchaseOrderMonitoringCancelITIAction.ToolTip = null;
            this.PurchaseOrderMonitoringCancelITIAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.PurchaseOrderMonitoringCancelITIAction_Execute);
            // 
            // PurchaseOrderMonitoringActionController
            // 
            this.Actions.Add(this.PurchaseOrderMonitoringSelectAction);
            this.Actions.Add(this.PurchaseOrderMonitoringUnselectAction);
            this.Actions.Add(this.PurchaseOrderMonitoringCancelITIAction);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SimpleAction PurchaseOrderMonitoringSelectAction;
        private DevExpress.ExpressApp.Actions.SimpleAction PurchaseOrderMonitoringUnselectAction;
        private DevExpress.ExpressApp.Actions.SimpleAction PurchaseOrderMonitoringCancelITIAction;
    }
}
