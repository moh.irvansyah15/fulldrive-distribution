﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;

using FullDrive.Module.BusinessObjects;
using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.Xpo;
using System.Collections;
using DevExpress.Xpo;
using System.IO;
using System.Web;

namespace FullDrive.Module.Controllers
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class DeliveryOrderActionController : ViewController
    {

        private ChoiceActionItem _selectionListviewFilter;

        public DeliveryOrderActionController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.
            #region Filter Status
            DeliveryOrderListviewFilterSelectionAction.Items.Clear();
            foreach (object _currApproval in Enum.GetValues(typeof(CustomProcess.Status)))
            {
                EnumDescriptor _ed = new EnumDescriptor(typeof(CustomProcess.Status));
                _selectionListviewFilter = new ChoiceActionItem(_ed.GetCaption(_currApproval), _currApproval);
                DeliveryOrderListviewFilterSelectionAction.Items.Add(_selectionListviewFilter);
            }
            #endregion Filter Status
        }

        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
        }

        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }

        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        private void DeliveryOrderListviewFilterSelectionAction_Execute(object sender, SingleChoiceActionExecuteEventArgs e)
        {
            try
            {
                if ((View is ListView) & (View.ObjectTypeInfo.Type == typeof(DeliveryOrder)))
                {
                    if ((Status)e.SelectedChoiceActionItem.Data == Status.None)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.None, BinaryOperatorType.NotEqual);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Open)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Open, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Progress)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Progress, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Posted)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Posted, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Close)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Close, BinaryOperatorType.Equal);
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = DeliveryOrder " + ex.ToString());
            }
        }

        private void DeliveryOrderGetSIAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;

                if (_objectsToProcess != null)
                {
                    foreach (Object obj in _objectsToProcess)
                    {
                        DeliveryOrder _locDeliveryOrderOS = (DeliveryOrder)_objectSpace.GetObject(obj);

                        if (_locDeliveryOrderOS != null)
                        {
                            if (_locDeliveryOrderOS.Session != null)
                            {
                                _currSession = _locDeliveryOrderOS.Session;
                            }

                            if (_locDeliveryOrderOS.Code != null && _currSession != null)
                            {
                                _currObjectId = _locDeliveryOrderOS.Code;

                                DeliveryOrder _locDeliveryOrderXPO = _currSession.FindObject<DeliveryOrder>
                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                     new BinaryOperator("Code", _currObjectId),
                                                                     new GroupOperator(GroupOperatorType.Or,
                                                                     new BinaryOperator("Status", Status.Open),
                                                                     new BinaryOperator("Status", Status.Progress))));

                                if (_locDeliveryOrderXPO != null)
                                {
                                    GetTransferInventoryMonitoring(_currSession, _locDeliveryOrderXPO);
                                    SuccessMessageShow("SalesInvoice Has Been Successfully Getting into DeliveryOrder");
                                }
                                else
                                {
                                    ErrorMessageShow("Data SalesInvoice Not Available");
                                }
                            }
                            else
                            {
                                ErrorMessageShow("Data SalesInvoice Not Available");
                            }
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = DeliveryOrder " + ex.ToString());
            }
        }

        private void DeliveryOrderProgressAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;
                Status _locStatus = Status.Open;

                if (_objectsToProcess != null)
                {
                    foreach (Object obj in _objectsToProcess)
                    {
                        DeliveryOrder _locDeliveryOrderOS = (DeliveryOrder)_objectSpace.GetObject(obj);

                        if (_locDeliveryOrderOS != null)
                        {
                            if (_locDeliveryOrderOS.Session != null)
                            {
                                _currSession = _locDeliveryOrderOS.Session;
                            }

                            if (_locDeliveryOrderOS.Code != null && _currSession != null)
                            {
                                _currObjectId = _locDeliveryOrderOS.Code;

                                DeliveryOrder _locDeliveryOrderXPO = _currSession.FindObject<DeliveryOrder>
                                                                   (new GroupOperator(GroupOperatorType.And,
                                                                    new BinaryOperator("Code", _currObjectId),
                                                                    new GroupOperator(GroupOperatorType.Or,
                                                                    new BinaryOperator("Status", Status.Open),
                                                                    new BinaryOperator("Status", Status.Progress),
                                                                    new BinaryOperator("Status", Status.Posted))));

                                if (_locDeliveryOrderXPO != null)
                                {
                                    if (_locDeliveryOrderXPO.Status == Status.Open)
                                    {
                                        _locStatus = Status.Progress;

                                    }
                                    else
                                    {
                                        _locStatus = _locDeliveryOrderXPO.Status;
                                    }

                                    _locDeliveryOrderXPO.Status = _locStatus;
                                    _locDeliveryOrderXPO.StatusDate = now;
                                    _locDeliveryOrderXPO.Save();
                                    _locDeliveryOrderXPO.Session.CommitTransaction();

                                    #region DeliveryOrderLine
                                    XPCollection<DeliveryOrderLine> _locDeliveryOrderLines = new XPCollection<DeliveryOrderLine>
                                                                                           (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                            new BinaryOperator("DeliveryOrder", _locDeliveryOrderXPO)));

                                    if (_locDeliveryOrderLines != null && _locDeliveryOrderLines.Count > 0)
                                    {
                                        foreach (DeliveryOrderLine _locDeliveryOrderLine in _locDeliveryOrderLines)
                                        {
                                            if (_locDeliveryOrderLine.Status == Status.Open)
                                            {
                                                _locDeliveryOrderLine.Status = Status.Progress;
                                                _locDeliveryOrderLine.StatusDate = now;
                                                _locDeliveryOrderLine.Save();
                                                _locDeliveryOrderLine.Session.CommitTransaction();
                                            }
                                        }
                                    }
                                    #endregion DeliveryOrderLine

                                    SetBegInvLine(_currSession, _locDeliveryOrderXPO);

                                }
                                else
                                {
                                    ErrorMessageShow("Data DeliveryOrder Not Available");
                                }
                            }
                            else
                            {
                                ErrorMessageShow("Data DeliveryOrder Not Available");
                            }
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = DeliveryOrder " + ex.ToString());
            }
        }

        private void DeliveryOrderPostingAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;

                if (_objectsToProcess != null)
                {
                    foreach (Object obj in _objectsToProcess)
                    {
                        DeliveryOrder _locDeliveryOrderOS = (DeliveryOrder)_objectSpace.GetObject(obj);

                        if (_locDeliveryOrderOS != null)
                        {
                            if (_locDeliveryOrderOS.Session != null)
                            {
                                _currSession = _locDeliveryOrderOS.Session;
                            }

                            if (_locDeliveryOrderOS.Code != null && _currSession != null)
                            {
                                _currObjectId = _locDeliveryOrderOS.Code;

                                DeliveryOrder _locDeliveryOrderXPO = _currSession.FindObject<DeliveryOrder>
                                                                   (new GroupOperator(GroupOperatorType.And,
                                                                    new BinaryOperator("Code", _currObjectId),
                                                                    new GroupOperator(GroupOperatorType.Or,
                                                                    new BinaryOperator("Status", Status.Progress),
                                                                    new BinaryOperator("Status", Status.Posted))));

                                if (_locDeliveryOrderXPO != null)
                                {
                                    if(CheckStock(_currSession, _locDeliveryOrderXPO) == 0)
                                    {
                                        SetDeliveryOrderMonitoring(_currSession, _locDeliveryOrderXPO);
                                        SetDeliverBeginingInventory(_currSession, _locDeliveryOrderXPO);
                                        SetDeliverInventoryJournal(_currSession, _locDeliveryOrderXPO);
                                        if (CheckJournalSetup(_currSession, _locDeliveryOrderXPO) == true && CheckGoodsDeliveryJournal(_currSession, _locDeliveryOrderXPO) == false)
                                        {
                                            SetGoodsDeliveryJournal(_currSession, _locDeliveryOrderXPO);
                                        }
                                        SetRemainQty(_currSession, _locDeliveryOrderXPO);
                                        SetPostingQty(_currSession, _locDeliveryOrderXPO);
                                        SetPostedCount(_currSession, _locDeliveryOrderXPO);
                                        SetStatusDeliveryOrderLineAndSalesInvoiceMonitoringAndDeliveryOrderMonitoring(_currSession, _locDeliveryOrderXPO);
                                        SetNormalQuantity(_currSession, _locDeliveryOrderXPO);
                                        SetFinalDeliveryOrder(_currSession, _locDeliveryOrderXPO);
                                        SetCollectStatus(_currSession, _locDeliveryOrderXPO);
                                        SetPickingStatus(_currSession, _locDeliveryOrderXPO);
                                        SuccessMessageShow(_locDeliveryOrderXPO.Code + " has been change successfully to Posted");
                                    }
                                    else
                                    {
                                        ErrorMessageShow("Data DeliveryOrder Not Available");
                                    }
                                }
                                else
                                {
                                    ErrorMessageShow("Data DeliveryOrder Not Available");
                                }
                            }
                            else
                            {
                                ErrorMessageShow("Data DeliveryOrder Not Available");
                            }
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = DeliveryOrder " + ex.ToString());
            }
        }

        //=============================================== Code In Here =============================================

        #region GetSIBasedOnTIM

        private void GetTransferInventoryMonitoring(Session _currSession, DeliveryOrder _locDeliveryOrderXPO)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                DateTime now = DateTime.Now;
                
                if (_locDeliveryOrderXPO != null)
                {
                    if(_locDeliveryOrderXPO.SalesInvoice != null && _locDeliveryOrderXPO.Vehicle != null)
                    {
                        XPCollection<TransferInventoryMonitoring> _locTransferInventoryMonitorings = null;

                        if(_locDeliveryOrderXPO.Picking != null)
                        {
                            _locTransferInventoryMonitorings = new XPCollection<TransferInventoryMonitoring>
                                                            (_currSession, new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("SalesInvoice", _locDeliveryOrderXPO.SalesInvoice),
                                                            new BinaryOperator("Vehicle", _locDeliveryOrderXPO.Vehicle),
                                                            new BinaryOperator("Picking", _locDeliveryOrderXPO.Picking),
                                                            new BinaryOperator("Select", true),
                                                            new GroupOperator(GroupOperatorType.Or,
                                                            new BinaryOperator("Status", Status.Open),
                                                            new BinaryOperator("Status", Status.Posted))));
                        }
                        else
                        {
                            _locTransferInventoryMonitorings = new XPCollection<TransferInventoryMonitoring>
                                                            (_currSession, new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("SalesInvoice", _locDeliveryOrderXPO.SalesInvoice),
                                                            new BinaryOperator("Vehicle", _locDeliveryOrderXPO.Vehicle),
                                                            new BinaryOperator("Select", true),
                                                            new GroupOperator(GroupOperatorType.Or,
                                                            new BinaryOperator("Status", Status.Open),
                                                            new BinaryOperator("Status", Status.Posted))));
                        }
                        
                        if (_locTransferInventoryMonitorings != null && _locTransferInventoryMonitorings.Count() > 0)
                        {
                            foreach (TransferInventoryMonitoring _locTransferInventoryMonitoring in _locTransferInventoryMonitorings)
                            {
                                #region SaveDeliveryOrderLine
                                DeliveryOrderLine _saveDataDeliveryOrderLine = new DeliveryOrderLine(_currSession)
                                {
                                    DeliveryOrder = _locDeliveryOrderXPO,
                                    Company = _locDeliveryOrderXPO.Company,
                                    Workplace = _locDeliveryOrderXPO.Workplace,
                                    Division = _locDeliveryOrderXPO.Division,
                                    Department = _locDeliveryOrderXPO.Department,
                                    Section = _locDeliveryOrderXPO.Section,
                                    Employee = _locDeliveryOrderXPO.Employee,
                                    Salesman = _locDeliveryOrderXPO.Salesman,
                                    Vehicle = _locDeliveryOrderXPO.Vehicle,
                                    BeginningInventory = _locDeliveryOrderXPO.BeginningInventory,
                                    SelectItem = _locTransferInventoryMonitoring.BegInvLineTo,
                                    Item = _locTransferInventoryMonitoring.Item,
                                    BegInvLine = _locTransferInventoryMonitoring.BegInvLineTo,
                                    MxDQty = _locTransferInventoryMonitoring.DQty,
                                    MxDUOM = _locTransferInventoryMonitoring.DUOM,
                                    MxQty = _locTransferInventoryMonitoring.Qty,
                                    MxUOM = _locTransferInventoryMonitoring.UOM,
                                    MxTQty = _locTransferInventoryMonitoring.TQty,
                                    DQty = _locTransferInventoryMonitoring.DQty,
                                    DUOM = _locTransferInventoryMonitoring.DUOM,
                                    Qty = _locTransferInventoryMonitoring.Qty,
                                    UOM = _locTransferInventoryMonitoring.UOM,
                                    TQty = _locTransferInventoryMonitoring.TQty,
                                    StockType = _locTransferInventoryMonitoring.StockTypeTo,
                                    StockGroup = _locTransferInventoryMonitoring.StockGroupTo,
                                    DeliveryStatus = DeliveryStatus.Loaded,
                                    DeliveryStatusDate = now,
                                    ETA = _locTransferInventoryMonitoring.ETA,
                                    ETD = _locTransferInventoryMonitoring.ETD,
                                    DocDate = _locTransferInventoryMonitoring.DocDate,
                                    JournalMonth = _locTransferInventoryMonitoring.JournalMonth,
                                    SalesInvoiceMonitoring = _locTransferInventoryMonitoring.SalesInvoiceMonitoring,
                                };
                                _saveDataDeliveryOrderLine.Save();
                                _saveDataDeliveryOrderLine.Session.CommitTransaction();

                                #endregion SaveDeliveryOrderLine
                                #region CountTransferInMonitoring
                                SetRemainQty(_currSession, _locTransferInventoryMonitoring);
                                SetPostingQty(_currSession, _locTransferInventoryMonitoring);
                                SetProcessCount(_currSession, _locTransferInventoryMonitoring);
                                SetStatusTransferInMonitoring(_currSession, _locTransferInventoryMonitoring);
                                SetNormalQuantity(_currSession, _locTransferInventoryMonitoring);
                                #endregion CountTransferInMonitoring
                            }
                        }
                    }
                    
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = DeliveryOrder ", ex.ToString());
            }
        }

        private void SetRemainQty(Session _currSession, TransferInventoryMonitoring _locTransferInventoryMonitoringXPO)
        {
            try
            {
                if (_locTransferInventoryMonitoringXPO != null)
                {
                    double _locRmDQty = 0;
                    double _locRmQty = 0;
                    double _locInvLineTotal = 0;
                    ItemUnitOfMeasure _locItemUOM = null;

                    #region ProcessCount=0
                    if (_locTransferInventoryMonitoringXPO.PostedCount == 0)
                    {
                        #region MaxQuantity
                        if (_locTransferInventoryMonitoringXPO.MxDQty > 0)
                        {
                            if (_locTransferInventoryMonitoringXPO.DQty > 0 && _locTransferInventoryMonitoringXPO.DQty <= _locTransferInventoryMonitoringXPO.MxDQty)
                            {
                                _locRmDQty = _locTransferInventoryMonitoringXPO.MxDQty - _locTransferInventoryMonitoringXPO.DQty;
                            }

                            if (_locTransferInventoryMonitoringXPO.Qty > 0 && _locTransferInventoryMonitoringXPO.Qty <= _locTransferInventoryMonitoringXPO.MxQty)
                            {
                                _locRmQty = _locTransferInventoryMonitoringXPO.MxQty - _locTransferInventoryMonitoringXPO.Qty;
                            }

                            _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                       (new GroupOperator(GroupOperatorType.And,
                                                        new BinaryOperator("Item", _locTransferInventoryMonitoringXPO.Item),
                                                        new BinaryOperator("UOM", _locTransferInventoryMonitoringXPO.MxUOM),
                                                        new BinaryOperator("DefaultUOM", _locTransferInventoryMonitoringXPO.MxDUOM),
                                                        new BinaryOperator("Active", true)));
                            if (_locItemUOM != null)
                            {
                                if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                {
                                    _locInvLineTotal = _locRmQty * _locItemUOM.DefaultConversion + _locRmDQty;
                                }
                                else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                {
                                    _locInvLineTotal = _locRmQty / _locItemUOM.Conversion + _locRmDQty;
                                }
                                else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                {
                                    _locInvLineTotal = _locRmQty + _locRmDQty;
                                }
                            }
                            else
                            {
                                _locInvLineTotal = _locRmQty + _locRmDQty;
                            }
                        }
                        #endregion MaxQuantity

                        #region NonMaxQuantity
                        else
                        {
                            if (_locTransferInventoryMonitoringXPO.DQty > 0)
                            {
                                _locRmDQty = 0;
                            }

                            if (_locTransferInventoryMonitoringXPO.Qty > 0)
                            {
                                _locRmQty = 0;
                            }

                            _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                       (new GroupOperator(GroupOperatorType.And,
                                                        new BinaryOperator("Item", _locTransferInventoryMonitoringXPO.Item),
                                                        new BinaryOperator("UOM", _locTransferInventoryMonitoringXPO.UOM),
                                                        new BinaryOperator("DefaultUOM", _locTransferInventoryMonitoringXPO.DUOM),
                                                        new BinaryOperator("Active", true)));
                            if (_locItemUOM != null)
                            {
                                if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                {
                                    _locInvLineTotal = _locRmQty * _locItemUOM.DefaultConversion + _locRmDQty;
                                }
                                else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                {
                                    _locInvLineTotal = _locRmQty / _locItemUOM.Conversion + _locRmDQty;
                                }
                                else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                {
                                    _locInvLineTotal = _locRmQty + _locRmDQty;
                                }
                            }
                            else
                            {
                                _locInvLineTotal = _locRmQty + _locRmDQty;
                            }
                        }
                        #endregion NonMaxQuantity
                    }
                    #endregion ProcessCount=0

                    #region ProcessCount>0
                    if (_locTransferInventoryMonitoringXPO.PostedCount > 0)
                    {
                        if (_locTransferInventoryMonitoringXPO.RmDQty > 0)
                        {
                            _locRmDQty = _locTransferInventoryMonitoringXPO.RmDQty - _locTransferInventoryMonitoringXPO.DQty;
                        }

                        if (_locTransferInventoryMonitoringXPO.RmQty > 0)
                        {
                            _locRmQty = _locTransferInventoryMonitoringXPO.RmQty - _locTransferInventoryMonitoringXPO.Qty;
                        }

                        if (_locTransferInventoryMonitoringXPO.MxDQty > 0 || _locTransferInventoryMonitoringXPO.MxQty > 0)
                        {
                            _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                       (new GroupOperator(GroupOperatorType.And,
                                                        new BinaryOperator("Item", _locTransferInventoryMonitoringXPO.Item),
                                                        new BinaryOperator("UOM", _locTransferInventoryMonitoringXPO.MxUOM),
                                                        new BinaryOperator("DefaultUOM", _locTransferInventoryMonitoringXPO.MxDUOM),
                                                        new BinaryOperator("Active", true)));
                        }
                        else
                        {
                            _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                       (new GroupOperator(GroupOperatorType.And,
                                                        new BinaryOperator("Item", _locTransferInventoryMonitoringXPO.Item),
                                                        new BinaryOperator("UOM", _locTransferInventoryMonitoringXPO.UOM),
                                                        new BinaryOperator("DefaultUOM", _locTransferInventoryMonitoringXPO.DUOM),
                                                        new BinaryOperator("Active", true)));
                        }


                        if (_locItemUOM != null)
                        {
                            if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                            {
                                _locInvLineTotal = _locRmQty * _locItemUOM.DefaultConversion + _locRmDQty;
                            }
                            else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                            {
                                _locInvLineTotal = _locRmQty / _locItemUOM.Conversion + _locRmDQty;
                            }
                            else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                            {
                                _locInvLineTotal = _locRmQty + _locRmDQty;
                            }
                        }
                        else
                        {
                            _locInvLineTotal = _locRmQty + _locRmDQty;
                        }

                    }
                    #endregion ProcessCount>0

                    _locTransferInventoryMonitoringXPO.RmDQty = _locRmDQty;
                    _locTransferInventoryMonitoringXPO.RmQty = _locRmQty;
                    _locTransferInventoryMonitoringXPO.RmTQty = _locInvLineTotal;
                    _locTransferInventoryMonitoringXPO.Save();
                    _locTransferInventoryMonitoringXPO.Session.CommitTransaction();

                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = DeliveryOrder " + ex.ToString());
            }
        }

        private void SetPostingQty(Session _currSession, TransferInventoryMonitoring _locTransferInventoryMonitoringXPO)
        {
            try
            {
                if (_locTransferInventoryMonitoringXPO != null)
                {
                    double _locPDQty = 0;
                    double _locPQty = 0;
                    double _locInvLineTotal = 0;
                    ItemUnitOfMeasure _locItemUOM = null;

                    #region ProcessCount=0
                    if (_locTransferInventoryMonitoringXPO.PostedCount == 0)
                    {
                        #region MaxQuantity
                        if (_locTransferInventoryMonitoringXPO.MxDQty > 0)
                        {
                            if (_locTransferInventoryMonitoringXPO.DQty > 0 && _locTransferInventoryMonitoringXPO.DQty <= _locTransferInventoryMonitoringXPO.MxDQty)
                            {
                                _locPDQty = _locTransferInventoryMonitoringXPO.DQty;
                            }

                            if (_locTransferInventoryMonitoringXPO.Qty > 0 && _locTransferInventoryMonitoringXPO.Qty <= _locTransferInventoryMonitoringXPO.MxQty)
                            {
                                _locPQty = _locTransferInventoryMonitoringXPO.Qty;
                            }

                            _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                       (new GroupOperator(GroupOperatorType.And,
                                                        new BinaryOperator("Item", _locTransferInventoryMonitoringXPO.Item),
                                                        new BinaryOperator("UOM", _locTransferInventoryMonitoringXPO.MxUOM),
                                                        new BinaryOperator("DefaultUOM", _locTransferInventoryMonitoringXPO.MxDUOM),
                                                        new BinaryOperator("Active", true)));
                            if (_locItemUOM != null)
                            {
                                if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                {
                                    _locInvLineTotal = _locPQty * _locItemUOM.DefaultConversion + _locPDQty;
                                }
                                else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                {
                                    _locInvLineTotal = _locPQty / _locItemUOM.Conversion + _locPDQty;
                                }
                                else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                {
                                    _locInvLineTotal = _locPQty + _locPDQty;
                                }
                            }
                            else
                            {
                                _locInvLineTotal = _locPQty + _locPDQty;
                            }
                        }
                        #endregion MaxQuantity

                        #region NonMaxQuantity
                        else
                        {
                            if (_locTransferInventoryMonitoringXPO.DQty > 0)
                            {
                                _locPDQty = _locTransferInventoryMonitoringXPO.DQty;
                            }

                            if (_locTransferInventoryMonitoringXPO.Qty > 0)
                            {
                                _locPQty = _locTransferInventoryMonitoringXPO.Qty;
                            }

                            _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                       (new GroupOperator(GroupOperatorType.And,
                                                        new BinaryOperator("Item", _locTransferInventoryMonitoringXPO.Item),
                                                        new BinaryOperator("UOM", _locTransferInventoryMonitoringXPO.UOM),
                                                        new BinaryOperator("DefaultUOM", _locTransferInventoryMonitoringXPO.DUOM),
                                                        new BinaryOperator("Active", true)));
                            if (_locItemUOM != null)
                            {
                                if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                {
                                    _locInvLineTotal = _locPQty * _locItemUOM.DefaultConversion + _locPDQty;
                                }
                                else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                {
                                    _locInvLineTotal = _locPQty / _locItemUOM.Conversion + _locPDQty;
                                }
                                else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                {
                                    _locInvLineTotal = _locPQty + _locPDQty;
                                }
                            }
                            else
                            {
                                _locInvLineTotal = _locPQty + _locPDQty;
                            }
                        }
                        #endregion NonMaxQuantity
                    }
                    #endregion ProcessCount=0

                    #region ProcessCount>0
                    if (_locTransferInventoryMonitoringXPO.PostedCount > 0)
                    {
                        if (_locTransferInventoryMonitoringXPO.PDQty > 0)
                        {
                            _locPDQty = _locTransferInventoryMonitoringXPO.PDQty + _locTransferInventoryMonitoringXPO.DQty;
                        }

                        if (_locTransferInventoryMonitoringXPO.PQty > 0)
                        {
                            _locPQty = _locTransferInventoryMonitoringXPO.PQty + _locTransferInventoryMonitoringXPO.Qty;
                        }

                        if (_locTransferInventoryMonitoringXPO.MxDQty > 0 || _locTransferInventoryMonitoringXPO.MxQty > 0)
                        {
                            _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                   (new GroupOperator(GroupOperatorType.And,
                                                    new BinaryOperator("Item", _locTransferInventoryMonitoringXPO.Item),
                                                    new BinaryOperator("UOM", _locTransferInventoryMonitoringXPO.MxUOM),
                                                    new BinaryOperator("DefaultUOM", _locTransferInventoryMonitoringXPO.MxDUOM),
                                                    new BinaryOperator("Active", true)));
                        }
                        else
                        {
                            _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                   (new GroupOperator(GroupOperatorType.And,
                                                    new BinaryOperator("Item", _locTransferInventoryMonitoringXPO.Item),
                                                    new BinaryOperator("UOM", _locTransferInventoryMonitoringXPO.UOM),
                                                    new BinaryOperator("DefaultUOM", _locTransferInventoryMonitoringXPO.DUOM),
                                                    new BinaryOperator("Active", true)));
                        }

                        if (_locItemUOM != null)
                        {
                            if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                            {
                                _locInvLineTotal = _locPQty * _locItemUOM.DefaultConversion + _locPDQty;
                            }
                            else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                            {
                                _locInvLineTotal = _locPQty / _locItemUOM.Conversion + _locPDQty;
                            }
                            else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                            {
                                _locInvLineTotal = _locPQty + _locPDQty;
                            }
                        }
                        else
                        {
                            _locInvLineTotal = _locPQty + _locPDQty;
                        }
                    }
                    #endregion ProcessCount>0

                    _locTransferInventoryMonitoringXPO.PDQty = _locPDQty;
                    _locTransferInventoryMonitoringXPO.PDUOM = _locTransferInventoryMonitoringXPO.DUOM;
                    _locTransferInventoryMonitoringXPO.PQty = _locPQty;
                    _locTransferInventoryMonitoringXPO.PUOM = _locTransferInventoryMonitoringXPO.UOM;
                    _locTransferInventoryMonitoringXPO.PTQty = _locInvLineTotal;
                    _locTransferInventoryMonitoringXPO.Save();
                    _locTransferInventoryMonitoringXPO.Session.CommitTransaction();

                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = DeliveryOrder " + ex.ToString());
            }
        }

        private void SetProcessCount(Session _currSession, TransferInventoryMonitoring _locTransferInventoryMonitoringXPO)
        {
            try
            {
                if (_locTransferInventoryMonitoringXPO != null)
                {
                    _locTransferInventoryMonitoringXPO.PostedCount = _locTransferInventoryMonitoringXPO.PostedCount + 1;
                    _locTransferInventoryMonitoringXPO.Save();
                    _locTransferInventoryMonitoringXPO.Session.CommitTransaction();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = DeliveryOrder " + ex.ToString());
            }
        }

        private void SetStatusTransferInMonitoring(Session _currSession, TransferInventoryMonitoring _locTransferInventoryMonitoringXPO)
        {
            try
            {
                DateTime now = DateTime.Now;
                if (_locTransferInventoryMonitoringXPO != null)
                {
                    if (_locTransferInventoryMonitoringXPO.RmDQty == 0 && _locTransferInventoryMonitoringXPO.RmQty == 0 && _locTransferInventoryMonitoringXPO.RmTQty == 0)
                    {
                        _locTransferInventoryMonitoringXPO.Status = Status.Close;
                        _locTransferInventoryMonitoringXPO.ActivationPosting = true;
                        _locTransferInventoryMonitoringXPO.StatusDate = now;
                    }
                    else
                    {
                        _locTransferInventoryMonitoringXPO.Status = Status.Posted;
                        _locTransferInventoryMonitoringXPO.StatusDate = now;
                    }
                    _locTransferInventoryMonitoringXPO.Save();
                    _locTransferInventoryMonitoringXPO.Session.CommitTransaction();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = DeliveryOrder " + ex.ToString());
            }
        }

        private void SetNormalQuantity(Session _currSession, TransferInventoryMonitoring _locTransferInventoryMonitoringXPO)
        {
            try
            {
                if (_locTransferInventoryMonitoringXPO != null)
                {
                    if (_locTransferInventoryMonitoringXPO.DQty > 0 || _locTransferInventoryMonitoringXPO.Qty > 0)
                    {
                        _locTransferInventoryMonitoringXPO.Select = false;
                        _locTransferInventoryMonitoringXPO.DQty = 0;
                        _locTransferInventoryMonitoringXPO.Qty = 0;
                        _locTransferInventoryMonitoringXPO.Save();
                        _locTransferInventoryMonitoringXPO.Session.CommitTransaction();
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = DeliveryOrder " + ex.ToString());
            }
        }

        #endregion GetSIBasedOnTIM

        #region Progress

        private void SetBegInvLine(Session _currSession, DeliveryOrder _locDeliveryOrderXPO)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                
                if (_locDeliveryOrderXPO != null)
                {
                    XPCollection<DeliveryOrderLine> _locDeliveryOrderLines = new XPCollection<DeliveryOrderLine>(_currSession,
                                                                        new GroupOperator(GroupOperatorType.And,
                                                                        new BinaryOperator("DeliveryOrder", _locDeliveryOrderXPO),
                                                                        new GroupOperator(GroupOperatorType.Or,
                                                                        new BinaryOperator("Status", Status.Open),
                                                                        new BinaryOperator("Status", Status.Progress))));
                    if (_locDeliveryOrderLines != null && _locDeliveryOrderLines.Count() > 0)
                    {
                        foreach (DeliveryOrderLine _locDeliveryOrderLine in _locDeliveryOrderLines)
                        {
                            if (_locDeliveryOrderLine.Company != null && _locDeliveryOrderLine.Workplace != null && _locDeliveryOrderLine.BeginningInventory != null )
                            {
                                if(_locDeliveryOrderLine.BegInvLine == null)
                                {
                                    if (_locDeliveryOrderLine.SelectItem != null)
                                    {
                                        _locDeliveryOrderLine.BegInvLine = _locDeliveryOrderLine.SelectItem;
                                        _locDeliveryOrderLine.Save();
                                        _locDeliveryOrderLine.Session.CommitTransaction();
                                    }else
                                    {
                                        if(_locDeliveryOrderLine.Item != null)
                                        {
                                            BeginningInventoryLine _locBegInvLine = _currSession.FindObject<BeginningInventoryLine>(
                                                                        new GroupOperator(GroupOperatorType.And,
                                                                        new BinaryOperator("Company", _locDeliveryOrderLine.Company),
                                                                        new BinaryOperator("Workplace", _locDeliveryOrderLine.Workplace),
                                                                        new BinaryOperator("BeginningInventory", _locDeliveryOrderLine.BeginningInventory),
                                                                        new BinaryOperator("Item", _locDeliveryOrderLine.Item),
                                                                        new BinaryOperator("LocationType", _locDeliveryOrderLine.BeginningInventory.LocationType),
                                                                        new BinaryOperator("StockType", _locDeliveryOrderLine.StockType),
                                                                        new BinaryOperator("StockGroup", _locDeliveryOrderLine.StockGroup),
                                                                        new BinaryOperator("Lock", false),
                                                                        new BinaryOperator("Active", true)));
                                            if(_locBegInvLine != null)
                                            {
                                                _locDeliveryOrderLine.BegInvLine = _locBegInvLine;
                                                _locDeliveryOrderLine.Save();
                                                _locDeliveryOrderLine.Session.CommitTransaction();
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Module = DeliveryOrder " + ex.ToString());
            }
        }

        #endregion Progress

        #region Posting Method

        private int CheckStock(Session _currSession, DeliveryOrder _locDeliveryOrderXPO)
        {
            int _result = 0;
            try
            {
                if (_locDeliveryOrderXPO != null)
                {
                    if (_locDeliveryOrderXPO.Company != null && _locDeliveryOrderXPO.Workplace != null)
                    {
                        XPCollection<DeliveryOrderLine> _locDeliveryOrderLines = new XPCollection<DeliveryOrderLine>(_currSession,
                                                                        new GroupOperator(GroupOperatorType.And,
                                                                        new BinaryOperator("DeliveryOrder", _locDeliveryOrderXPO),
                                                                        new BinaryOperator("Select", true),
                                                                        new GroupOperator(GroupOperatorType.Or,
                                                                        new BinaryOperator("Status", Status.Progress),
                                                                        new BinaryOperator("Status", Status.Posted),
                                                                        new BinaryOperator("DeliveryStatus", DeliveryStatus.PendingAll, BinaryOperatorType.NotEqual),
                                                                        new BinaryOperator("DeliveryStatus", DeliveryStatus.CancelAll, BinaryOperatorType.NotEqual))));

                        if (_locDeliveryOrderLines != null && _locDeliveryOrderLines.Count() > 0)
                        {
                            foreach (DeliveryOrderLine _locDeliveryOrderLine in _locDeliveryOrderLines)
                            {
                                if (_locDeliveryOrderLine.BeginningInventory != null && _locDeliveryOrderLine.Item != null)
                                {
                                    if (_locDeliveryOrderLine.BegInvLine != null)
                                    {
                                        if (_locDeliveryOrderLine.BegInvLine.QtyAvailable < _locDeliveryOrderLine.TQty)
                                        {
                                            _locDeliveryOrderLine.Message = "Stock Not Available";
                                            _locDeliveryOrderLine.Save();
                                            _locDeliveryOrderLine.Session.CommitTransaction();
                                            _result = _result + 1;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = DeliveryOrder ", ex.ToString());
            }

            return _result;
        }

        private void SetDeliveryOrderMonitoring(Session _currSession, DeliveryOrder _locDeliveryOrderXPO)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                DateTime now = DateTime.Now;

                if (_locDeliveryOrderXPO != null)
                {
                    if (_locDeliveryOrderXPO.Status == Status.Progress || _locDeliveryOrderXPO.Status == Status.Posted)
                    {
                        XPCollection<DeliveryOrderLine> _locDeliveryOrderLines = new XPCollection<DeliveryOrderLine>(_currSession,
                                                                        new GroupOperator(GroupOperatorType.And,
                                                                        new BinaryOperator("DeliveryOrder", _locDeliveryOrderXPO),
                                                                        new BinaryOperator("Select", true),
                                                                        new GroupOperator(GroupOperatorType.Or,
                                                                        new BinaryOperator("Status", Status.Progress),
                                                                        new BinaryOperator("Status", Status.Posted))));

                        if (_locDeliveryOrderLines != null && _locDeliveryOrderLines.Count() > 0)
                        {
                            foreach (DeliveryOrderLine _locDeliveryOrderLine in _locDeliveryOrderLines)
                            {
                                DeliveryOrderMonitoring _saveDataInvoiceCanvassingMonitoring = new DeliveryOrderMonitoring(_currSession)
                                {
                                    DeliveryOrder = _locDeliveryOrderXPO,
                                    DeliveryOrderLine = _locDeliveryOrderLine,
                                    Company = _locDeliveryOrderLine.Company,
                                    Workplace = _locDeliveryOrderLine.Workplace,
                                    Division = _locDeliveryOrderLine.Division,
                                    Department = _locDeliveryOrderLine.Department,
                                    Section = _locDeliveryOrderLine.Section,
                                    Employee = _locDeliveryOrderLine.Employee,
                                    Salesman = _locDeliveryOrderLine.Salesman,
                                    Vehicle = _locDeliveryOrderLine.Vehicle,
                                    BeginningInventory = _locDeliveryOrderLine.BeginningInventory,
                                    StockType = _locDeliveryOrderLine.StockType,
                                    StockGroup = _locDeliveryOrderLine.StockGroup,
                                    Item = _locDeliveryOrderLine.Item,
                                    BegInvLine = _locDeliveryOrderLine.BegInvLine,
                                    MxDQty = _locDeliveryOrderLine.MxDQty,
                                    MxDUOM = _locDeliveryOrderLine.MxDUOM,
                                    MxQty = _locDeliveryOrderLine.MxQty,
                                    MxUOM = _locDeliveryOrderLine.MxUOM,
                                    MxTQty = _locDeliveryOrderLine.MxTQty,
                                    DQty = _locDeliveryOrderLine.DQty,
                                    DUOM = _locDeliveryOrderLine.DUOM,
                                    Qty = _locDeliveryOrderLine.Qty,
                                    UOM = _locDeliveryOrderLine.UOM,
                                    TQty = _locDeliveryOrderLine.TQty,
                                    ETD = _locDeliveryOrderLine.ETD,
                                    ETA = _locDeliveryOrderLine.ETA,
                                    ATD = _locDeliveryOrderLine.ATD,
                                    ATA = _locDeliveryOrderLine.ATA,
                                    DocDate = _locDeliveryOrderLine.DocDate,
                                    JournalMonth = _locDeliveryOrderLine.JournalMonth,
                                    JournalYear = _locDeliveryOrderLine.JournalYear,
                                    SalesInvoiceMonitoring = _locDeliveryOrderLine.SalesInvoiceMonitoring,
                                };
                                _saveDataInvoiceCanvassingMonitoring.Save();
                                _saveDataInvoiceCanvassingMonitoring.Session.CommitTransaction();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = DeliveryOrder ", ex.ToString());
            }
        }

        private void SetDeliverBeginingInventory(Session _currSession, DeliveryOrder _locDeliveryOrderXPO)
        {
            try
            {
                if (_locDeliveryOrderXPO != null)
                {
                    if (_locDeliveryOrderXPO.Company != null && _locDeliveryOrderXPO.Workplace != null)
                    {
                        XPCollection<DeliveryOrderLine> _locDeliveryOrderLines = new XPCollection<DeliveryOrderLine>(_currSession,
                                                                        new GroupOperator(GroupOperatorType.And,
                                                                        new BinaryOperator("DeliveryOrder", _locDeliveryOrderXPO),
                                                                        new BinaryOperator("DeliveryStatus", DeliveryStatus.PendingAll, BinaryOperatorType.NotEqual),
                                                                        new BinaryOperator("DeliveryStatus", DeliveryStatus.CancelAll, BinaryOperatorType.NotEqual),
                                                                        new BinaryOperator("Select", true),
                                                                        new GroupOperator(GroupOperatorType.Or,
                                                                        new BinaryOperator("Status", Status.Progress),
                                                                        new BinaryOperator("Status", Status.Posted))));

                        if (_locDeliveryOrderLines != null && _locDeliveryOrderLines.Count() > 0)
                        {
                            foreach (DeliveryOrderLine _locDeliveryOrderLine in _locDeliveryOrderLines)
                            {
                                if (_locDeliveryOrderLine.BeginningInventory != null && _locDeliveryOrderLine.Item != null)
                                {
                                    if (_locDeliveryOrderLine.BegInvLine != null)
                                    {
                                        _locDeliveryOrderLine.BegInvLine.QtyAvailable = _locDeliveryOrderLine.BegInvLine.QtyAvailable - _locDeliveryOrderLine.TQty;
                                        _locDeliveryOrderLine.BegInvLine.QtySale = _locDeliveryOrderLine.BegInvLine.QtySale + _locDeliveryOrderLine.TQty;
                                        _locDeliveryOrderLine.BegInvLine.Save();
                                        _locDeliveryOrderLine.BegInvLine.Session.CommitTransaction();

                                        _locDeliveryOrderLine.BeginningInventory.QtyAvailable = _locDeliveryOrderLine.BeginningInventory.QtyAvailable - _locDeliveryOrderLine.TQty;
                                        _locDeliveryOrderLine.BeginningInventory.QtySale = _locDeliveryOrderLine.BeginningInventory.QtySale + _locDeliveryOrderLine.TQty;
                                        _locDeliveryOrderLine.BeginningInventory.Save();
                                        _locDeliveryOrderLine.BeginningInventory.Session.CommitTransaction();

                                        #region MainSourceLocation
                                        if (_locDeliveryOrderLine.SalesInvoiceMonitoring != null)
                                        {
                                            #region AddSalesQty
                                            if (_locDeliveryOrderLine.SalesInvoiceMonitoring.BegInv != null)
                                            {
                                                _locDeliveryOrderLine.SalesInvoiceMonitoring.BegInv.QtySale = _locDeliveryOrderLine.SalesInvoiceMonitoring.BegInv.QtySale + _locDeliveryOrderLine.TQty;
                                                _locDeliveryOrderLine.SalesInvoiceMonitoring.BegInv.Save();
                                                _locDeliveryOrderLine.SalesInvoiceMonitoring.BegInv.Session.CommitTransaction();
                                            }
                                            #endregion AddSalesQty
                                            #region AddSalesQtyAndReleaseQtyOnHand
                                            if (_locDeliveryOrderLine.SalesInvoiceMonitoring.BegInvLine != null)
                                            {
                                                _locDeliveryOrderLine.SalesInvoiceMonitoring.BegInvLine.QtySale = _locDeliveryOrderLine.SalesInvoiceMonitoring.BegInvLine.QtySale + _locDeliveryOrderLine.TQty;
                                                _locDeliveryOrderLine.SalesInvoiceMonitoring.BegInvLine.QtyOnHand = _locDeliveryOrderLine.SalesInvoiceMonitoring.BegInvLine.QtyOnHand - _locDeliveryOrderLine.TQty;
                                                _locDeliveryOrderLine.SalesInvoiceMonitoring.BegInvLine.Save();
                                                _locDeliveryOrderLine.SalesInvoiceMonitoring.BegInvLine.Session.CommitTransaction();
                                            }
                                            #endregion AddSalesQtyAndReleaseQtyOnHand
                                        }
                                        #endregion MainSourceLocation
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = DeliveryOrder ", ex.ToString());
            }
        }

        private void SetDeliverInventoryJournal(Session _currSession, DeliveryOrder _locDeliveryOrderXPO)
        {
            try
            {
                DateTime now = DateTime.Now;

                if (_locDeliveryOrderXPO != null)
                {
                    if (_locDeliveryOrderXPO.Company != null && _locDeliveryOrderXPO.Workplace != null
                        && _locDeliveryOrderXPO.Salesman != null && _locDeliveryOrderXPO.BeginningInventory != null)
                    {
                        #region NormalItem
                        XPCollection<DeliveryOrderLine> _locDeliveryOrderLines = new XPCollection<DeliveryOrderLine>(_currSession,
                                                                        new GroupOperator(GroupOperatorType.And,
                                                                        new BinaryOperator("DeliveryOrder", _locDeliveryOrderXPO),
                                                                        new BinaryOperator("Select", true),
                                                                        new BinaryOperator("DeliveryStatus", DeliveryStatus.PendingAll, BinaryOperatorType.NotEqual),
                                                                        new BinaryOperator("DeliveryStatus", DeliveryStatus.CancelAll, BinaryOperatorType.NotEqual),
                                                                        new GroupOperator(GroupOperatorType.Or,
                                                                        new BinaryOperator("Status", Status.Progress),
                                                                        new BinaryOperator("Status", Status.Posted))));

                        if (_locDeliveryOrderLines != null && _locDeliveryOrderLines.Count() > 0)
                        {
                            foreach (DeliveryOrderLine _locDeliveryOrderLine in _locDeliveryOrderLines)
                            {
                                if (_locDeliveryOrderLine.BegInvLine != null)
                                {
                                    InventoryJournal _locNegatifInventoryJournal = new InventoryJournal(_currSession)
                                    {
                                        Company = _locDeliveryOrderXPO.Company,
                                        Workplace = _locDeliveryOrderXPO.Workplace,
                                        Location = _locDeliveryOrderXPO.BeginningInventory.Location,
                                        LocationType = _locDeliveryOrderXPO.BeginningInventory.LocationType,
                                        BinLocation = _locDeliveryOrderLine.BegInvLine.BinLocation,
                                        Salesman = _locDeliveryOrderXPO.Salesman,
                                        StockType = _locDeliveryOrderLine.StockType,
                                        StockGroup = _locDeliveryOrderLine.StockGroup,
                                        Item = _locDeliveryOrderLine.Item,
                                        QtyPos = 0,
                                        QtyNeg = _locDeliveryOrderLine.TQty,
                                        DUOM = _locDeliveryOrderLine.DUOM,
                                        JournalDate = now,
                                        DeliveryOrder = _locDeliveryOrderXPO,
                                    };
                                    _locNegatifInventoryJournal.Save();
                                    _locNegatifInventoryJournal.Session.CommitTransaction();
                                }
                            }
                        }
                        #endregion NormalItem
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = DeliveryOrder ", ex.ToString());
            }
        }

        //======================================================================================================
        #region SetGoodsDeliverJournal

        private bool CheckJournalSetup(Session _currSession, DeliveryOrder _locDeliveryOrderXPO)
        {
            bool _result = false;
            try
            {
                if (_locDeliveryOrderXPO != null)
                {
                    if (_locDeliveryOrderXPO.Company != null && _locDeliveryOrderXPO.Workplace != null)
                    {
                        JournalSetupDetail _locJournalSetupDetail = _currSession.FindObject<JournalSetupDetail>
                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                    new BinaryOperator("Company", _locDeliveryOrderXPO.Company),
                                                                    new BinaryOperator("Workplace", _locDeliveryOrderXPO.Workplace),
                                                                    new BinaryOperator("JournalSetupType", JournalSetupType.Journal),
                                                                    new BinaryOperator("ObjectList", ObjectList.DeliveryOrder),
                                                                    new BinaryOperator("JournalProcess", JournalProcess.JournalGoodDeliver),
                                                                    new BinaryOperator("Active", true)));

                        if (_locJournalSetupDetail != null)
                        {
                            _result = true;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = DeliveryOrder ", ex.ToString());
            }

            return _result;
        }

        private bool CheckGoodsDeliveryJournal(Session _currSession, DeliveryOrder _locDeliveryOrderXPO)
        {
            bool _result = false;
            try
            {
                if (_locDeliveryOrderXPO != null)
                {
                    if (_locDeliveryOrderXPO.Company != null && _locDeliveryOrderXPO.Workplace != null)
                    {
                        XPCollection<GeneralJournal> _locGeneralJournals = new XPCollection<GeneralJournal>(_currSession,
                                                                        new GroupOperator(GroupOperatorType.And,
                                                                        new BinaryOperator("Company", _locDeliveryOrderXPO.Company),
                                                                        new BinaryOperator("Workplace", _locDeliveryOrderXPO.Workplace),
                                                                        new BinaryOperator("DeliveryOrder", _locDeliveryOrderXPO)));
                        if (_locGeneralJournals != null && _locGeneralJournals.Count() > 0)
                        {
                            _result = true;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = DeliveryOrder ", ex.ToString());
            }

            return _result;
        }

        private void SetGoodsDeliveryJournal(Session _currSession, DeliveryOrder _locDeliveryOrderXPO)
        {
            try
            {
                DateTime now = DateTime.Now;
                double _locTotalUnitAmount = 0;
                ItemAccountGroup _locItemAccountGroup = null;

                if (_locDeliveryOrderXPO != null)
                {
                    if (_locDeliveryOrderXPO.SalesInvoice != null)
                    {
                        XPCollection<DeliveryOrderLine> _locDeliveryOrderLines = new XPCollection<DeliveryOrderLine>(_currSession,
                                                                            new GroupOperator(GroupOperatorType.And,
                                                                            new BinaryOperator("DeliveryOrder", _locDeliveryOrderXPO),
                                                                            new BinaryOperator("StockGroup", StockGroup.Normal),
                                                                            new GroupOperator(GroupOperatorType.Or,
                                                                            new BinaryOperator("Status", Status.Progress),
                                                                            new BinaryOperator("Status", Status.Posted))));


                        if (_locDeliveryOrderLines != null && _locDeliveryOrderLines.Count() > 0)
                        {
                            foreach (DeliveryOrderLine _locDeliveryOrderLine in _locDeliveryOrderLines)
                            {
                                if (_locDeliveryOrderLine.Item != null && _locDeliveryOrderLine.Company != null && _locDeliveryOrderLine.Workplace != null)
                                {
                                    //Cari ItemAccountGroup
                                    ItemAccount _locItemAccount = _currSession.FindObject<ItemAccount>(new GroupOperator(GroupOperatorType.And,
                                                                    new BinaryOperator("Company", _locDeliveryOrderLine.Company),
                                                                    new BinaryOperator("Workplace", _locDeliveryOrderLine.Workplace),
                                                                    new BinaryOperator("Item", _locDeliveryOrderLine.Item),
                                                                    new BinaryOperator("Active", true)));

                                    if (_locDeliveryOrderLine.JournalMonth > 0 && _locItemAccount != null)
                                    {
                                        if (_locItemAccount.ItemAccountGroup != null)
                                        {
                                            _locItemAccountGroup = _locItemAccount.ItemAccountGroup;
                                        }

                                        if (_locDeliveryOrderLine.SalesInvoiceMonitoring.SalesInvoiceLine != null)
                                        {
                                            if (_locDeliveryOrderLine.SalesInvoiceMonitoring.SalesInvoiceLine.PriceLine != null)
                                            {
                                                _locTotalUnitAmount = _locDeliveryOrderLine.SalesInvoiceMonitoring.SalesInvoiceLine.PriceLine.Amount1a * _locDeliveryOrderLine.TQty;
                                            }
                                        }

                                        #region JournalMapByItems

                                        XPCollection<JournalMap> _locJournalMapItems = new XPCollection<JournalMap>
                                                                                       (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                        new BinaryOperator("Company", _locDeliveryOrderLine.Company),
                                                                                        new BinaryOperator("Workplace", _locDeliveryOrderLine.Workplace),
                                                                                        new BinaryOperator("ItemAccountGroup", _locItemAccountGroup),
                                                                                        new BinaryOperator("Active", true)));

                                        if (_locJournalMapItems != null && _locJournalMapItems.Count() > 0)
                                        {
                                            foreach (JournalMap _locJournalMapItem in _locJournalMapItems)
                                            {
                                                XPCollection<JournalMapLine> _locJournalMapLines = new XPCollection<JournalMapLine>
                                                                                                   (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                    new BinaryOperator("Company", _locDeliveryOrderLine.Company),
                                                                                                    new BinaryOperator("Workplace", _locDeliveryOrderLine.Workplace),
                                                                                                    new BinaryOperator("JournalMap", _locJournalMapItem),
                                                                                                    new BinaryOperator("Active", true)));

                                                if (_locJournalMapLines != null && _locJournalMapLines.Count() > 0)
                                                {
                                                    foreach (JournalMapLine _locJournalMapLine in _locJournalMapLines)
                                                    {
                                                        if (_locJournalMapLine.AccountMap != null)
                                                        {
                                                            if (_locJournalMapLine.AccountMap.PostingType == PostingType.Sales && _locJournalMapLine.AccountMap.OrderType == OrderType.Item
                                                                && _locJournalMapLine.AccountMap.PostingMethod == PostingMethod.Deliver && _locJournalMapLine.AccountMap.PostingMethodType == PostingMethodType.Normal)
                                                            {
                                                                XPCollection<AccountMapLine> _locAccountMapLines = new XPCollection<AccountMapLine>
                                                                                                               (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                                new BinaryOperator("Company", _locDeliveryOrderLine.Company),
                                                                                                                new BinaryOperator("Workplace", _locDeliveryOrderLine.Workplace),
                                                                                                                new BinaryOperator("AccountMap", _locJournalMapLine.AccountMap),
                                                                                                                new BinaryOperator("Active", true)));

                                                                if (_locAccountMapLines != null && _locAccountMapLines.Count() > 0)
                                                                {
                                                                    double _locTotalAmountDebit = 0;
                                                                    double _locTotalAmountCredit = 0;
                                                                    double _locTotalBalance = 0;

                                                                    foreach (AccountMapLine _locAccountMapLine in _locAccountMapLines)
                                                                    {
                                                                        if (_locAccountMapLine.AccountCharge == AccountCharge.Debit)
                                                                        {
                                                                            _locTotalAmountDebit = _locTotalUnitAmount;
                                                                        }
                                                                        if (_locAccountMapLine.AccountCharge == AccountCharge.Credit)
                                                                        {
                                                                            _locTotalAmountCredit = _locTotalUnitAmount;
                                                                        }

                                                                        GeneralJournal _saveGeneralJournal = new GeneralJournal(_currSession)
                                                                        {
                                                                            Company = _locDeliveryOrderLine.Company,
                                                                            Workplace = _locDeliveryOrderLine.Workplace,
                                                                            PostingDate = now,
                                                                            PostingType = PostingType.Sales,
                                                                            OrderType = OrderType.Item,
                                                                            PostingMethod = PostingMethod.Deliver,
                                                                            PostingMethodType = PostingMethodType.Normal,
                                                                            Account = _locAccountMapLine.Account,
                                                                            Debit = _locTotalAmountDebit,
                                                                            Credit = _locTotalAmountCredit,
                                                                            JournalMonth = _locDeliveryOrderLine.JournalMonth,
                                                                            JournalYear = _locDeliveryOrderLine.JournalYear,
                                                                            DeliveryOrder = _locDeliveryOrderXPO,
                                                                        };

                                                                        _saveGeneralJournal.Save();
                                                                        _saveGeneralJournal.Session.CommitTransaction();

                                                                        #region AccountingPeriodicLine

                                                                        if (_locAccountMapLine.Account.Code != null)
                                                                        {

                                                                            //Cari Account Periodic Line
                                                                            AccountingPeriodicLine _locAPL = _currSession.FindObject<AccountingPeriodicLine>
                                                                                                            (new GroupOperator(GroupOperatorType.And,
                                                                                                            new BinaryOperator("Company", _locDeliveryOrderLine.Company),
                                                                                                            new BinaryOperator("Workplace", _locDeliveryOrderLine.Workplace),
                                                                                                            new BinaryOperator("AccountNo", _locAccountMapLine.Account),
                                                                                                            new BinaryOperator("Month", _locDeliveryOrderLine.JournalMonth),
                                                                                                            new BinaryOperator("Year", _locDeliveryOrderLine.JournalYear)));
                                                                            if (_locAPL != null)
                                                                            {
                                                                                if (_locAPL.AccountNo != null)
                                                                                {
                                                                                    if (_locAPL.AccountNo.BalanceType == BalanceType.Change)
                                                                                    {
                                                                                        if (_locAPL.AccountNo.AccountCharge == AccountCharge.Debit)
                                                                                        {
                                                                                            if (_locTotalAmountDebit > 0)
                                                                                            {
                                                                                                _locTotalBalance = _locAPL.Balance + _locTotalAmountDebit;
                                                                                            }
                                                                                            if (_locTotalAmountCredit > 0)
                                                                                            {
                                                                                                _locTotalBalance = _locAPL.Balance - _locTotalAmountCredit;
                                                                                            }
                                                                                        }
                                                                                        if (_locAPL.AccountNo.AccountCharge == AccountCharge.Credit)
                                                                                        {
                                                                                            if (_locTotalAmountDebit > 0)
                                                                                            {
                                                                                                _locTotalBalance = _locAPL.Balance - _locTotalAmountDebit;
                                                                                            }
                                                                                            if (_locTotalAmountCredit > 0)
                                                                                            {
                                                                                                _locTotalBalance = _locAPL.Balance + _locTotalAmountCredit;
                                                                                            }
                                                                                        }
                                                                                    }

                                                                                    _locAPL.Balance = _locTotalBalance;
                                                                                    _locAPL.Debit = _locAPL.Debit + _locTotalAmountDebit;
                                                                                    _locAPL.Credit = _locAPL.Credit + _locTotalAmountCredit;
                                                                                    _locAPL.Save();
                                                                                    _locAPL.Session.CommitTransaction();

                                                                                }
                                                                            }
                                                                        }
                                                                        #endregion COA
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        #endregion JournalMapByItems
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = DeliveryOrder ", ex.ToString());
            }
        }

        #endregion SetGoodsDeliverJournal
        //======================================================================================================

        private void SetRemainQty(Session _currSession, DeliveryOrder _locDeliveryOrderXPO)
        {
            try
            {
                if (_locDeliveryOrderXPO != null)
                {
                    double _locRmDQty = 0;
                    double _locRmQty = 0;
                    double _locInvLineTotal = 0;
                    ItemUnitOfMeasure _locItemUOM = null;
                    XPCollection<DeliveryOrderLine> _locDeliveryOrderLines = new XPCollection<DeliveryOrderLine>(_currSession,
                                                                        new GroupOperator(GroupOperatorType.And,
                                                                        new BinaryOperator("DeliveryOrder", _locDeliveryOrderXPO),
                                                                        new BinaryOperator("Select", true),
                                                                        new BinaryOperator("DeliveryStatus", DeliveryStatus.PendingAll, BinaryOperatorType.NotEqual),
                                                                        new BinaryOperator("DeliveryStatus", DeliveryStatus.CancelAll, BinaryOperatorType.NotEqual),
                                                                        new GroupOperator(GroupOperatorType.Or,
                                                                        new BinaryOperator("Status", Status.Progress),
                                                                        new BinaryOperator("Status", Status.Posted))));

                    if (_locDeliveryOrderLines != null && _locDeliveryOrderLines.Count() > 0)
                    {
                        foreach (DeliveryOrderLine _locDeliveryOrderLine in _locDeliveryOrderLines)
                        {
                            #region DeliveryOrderLine

                            #region DeliveryOrderLineProcessCount=0
                            if (_locDeliveryOrderLine.PostedCount == 0)
                            {
                                #region MaxQuantity
                                if ((_locDeliveryOrderLine.MxDQty > 0 || _locDeliveryOrderLine.MxQty > 0) && _locDeliveryOrderLine.MxTQty > 0)
                                {
                                    if (_locDeliveryOrderLine.DQty > 0 && _locDeliveryOrderLine.DQty <= _locDeliveryOrderLine.MxDQty)
                                    {
                                        _locRmDQty = _locDeliveryOrderLine.MxDQty - _locDeliveryOrderLine.DQty;
                                    }

                                    if (_locDeliveryOrderLine.Qty > 0 && _locDeliveryOrderLine.Qty <= _locDeliveryOrderLine.MxQty)
                                    {
                                        _locRmQty = _locDeliveryOrderLine.MxQty - _locDeliveryOrderLine.Qty;
                                    }

                                    if (_locDeliveryOrderLine.Item != null && _locDeliveryOrderLine.MxUOM != null && _locDeliveryOrderLine.MxDUOM != null)
                                    {
                                        _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                               (new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("Item", _locDeliveryOrderLine.Item),
                                                                new BinaryOperator("UOM", _locDeliveryOrderLine.MxUOM),
                                                                new BinaryOperator("DefaultUOM", _locDeliveryOrderLine.MxDUOM),
                                                                new BinaryOperator("Active", true)));
                                    }

                                    if (_locItemUOM != null)
                                    {
                                        if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locRmQty * _locItemUOM.DefaultConversion + _locRmDQty;
                                        }
                                        else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locRmQty / _locItemUOM.Conversion + _locRmDQty;
                                        }
                                        else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locRmQty + _locRmDQty;
                                        }
                                    }
                                    else
                                    {
                                        _locInvLineTotal = _locRmQty + _locRmDQty;
                                    }

                                }
                                #endregion MaxQuantity
                                #region NonMaxQuantity
                                else
                                {
                                    if (_locDeliveryOrderLine.DQty > 0)
                                    {
                                        _locRmDQty = 0;
                                    }

                                    if (_locDeliveryOrderLine.Qty > 0)
                                    {
                                        _locRmQty = 0;
                                    }

                                    if (_locDeliveryOrderLine.Item != null && _locDeliveryOrderLine.UOM != null && _locDeliveryOrderLine.DUOM != null)
                                    {
                                        _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                               (new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("Item", _locDeliveryOrderLine.Item),
                                                                new BinaryOperator("UOM", _locDeliveryOrderLine.UOM),
                                                                new BinaryOperator("DefaultUOM", _locDeliveryOrderLine.DUOM),
                                                                new BinaryOperator("Active", true)));
                                    }

                                    if (_locItemUOM != null)
                                    {
                                        if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locRmQty * _locItemUOM.DefaultConversion + _locRmDQty;
                                        }
                                        else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locRmQty / _locItemUOM.Conversion + _locRmDQty;
                                        }
                                        else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locRmQty + _locRmDQty;
                                        }
                                    }
                                    else
                                    {
                                        _locInvLineTotal = _locRmQty + _locRmDQty;
                                    }
                                }
                                #endregion NonMaxQuantity
                            }
                            #endregion DeliveryOrderLineProcessCount=0

                            #region DeliveryOrderLineProcessCount>0
                            if (_locDeliveryOrderLine.PostedCount > 0)
                            {
                                if (_locDeliveryOrderLine.RmDQty > 0)
                                {
                                    _locRmDQty = _locDeliveryOrderLine.RmDQty - _locDeliveryOrderLine.DQty;
                                }

                                if (_locDeliveryOrderLine.RmQty > 0)
                                {
                                    _locRmQty = _locDeliveryOrderLine.RmQty - _locDeliveryOrderLine.Qty;
                                }

                                if (_locDeliveryOrderLine.MxDUOM != null && _locDeliveryOrderLine.MxUOM != null && _locDeliveryOrderLine.Item != null)
                                {
                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                               (new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("Item", _locDeliveryOrderLine.Item),
                                                                new BinaryOperator("UOM", _locDeliveryOrderLine.MxUOM),
                                                                new BinaryOperator("DefaultUOM", _locDeliveryOrderLine.MxDUOM),
                                                                new BinaryOperator("Active", true)));
                                }
                                else
                                {
                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                               (new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("Item", _locDeliveryOrderLine.Item),
                                                                new BinaryOperator("UOM", _locDeliveryOrderLine.UOM),
                                                                new BinaryOperator("DefaultUOM", _locDeliveryOrderLine.DUOM),
                                                                new BinaryOperator("Active", true)));
                                }


                                if (_locItemUOM != null)
                                {
                                    if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                    {
                                        _locInvLineTotal = _locRmQty * _locItemUOM.DefaultConversion + _locRmDQty;
                                    }
                                    else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                    {
                                        _locInvLineTotal = _locRmQty / _locItemUOM.Conversion + _locRmDQty;
                                    }
                                    else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                    {
                                        _locInvLineTotal = _locRmQty + _locRmDQty;
                                    }
                                }
                                else
                                {
                                    _locInvLineTotal = _locRmQty + _locRmDQty;
                                }

                            }
                            #endregion DeliveryOrderLineProcessCount>0

                            _locDeliveryOrderLine.RmDQty = _locRmDQty;
                            _locDeliveryOrderLine.RmQty = _locRmQty;
                            _locDeliveryOrderLine.RmTQty = _locInvLineTotal;
                            _locDeliveryOrderLine.Save();
                            _locDeliveryOrderLine.Session.CommitTransaction();

                            #endregion DeliveryOrderLine
                            #region SalesInvoiceMonitoring
                            if (_locDeliveryOrderLine.SalesInvoiceMonitoring != null)
                            {
                                double _locDlvRmDQty = 0;
                                double _locDlvRmQty = 0;
                                double _locDlvInvLineTotal = 0;
                                #region SalesInvoiceMonitoringProcessCount=0
                                if (_locDeliveryOrderLine.SalesInvoiceMonitoring.DlvPostedCount == 0)
                                {
                                    #region MaxQuantity
                                    if ((_locDeliveryOrderLine.SalesInvoiceMonitoring.MxDQty > 0 || _locDeliveryOrderLine.SalesInvoiceMonitoring.MxQty > 0) &&
                                        _locDeliveryOrderLine.SalesInvoiceMonitoring.MxTQty > 0)
                                    {
                                        if (_locDeliveryOrderLine.DQty > 0 && _locDeliveryOrderLine.DQty <= _locDeliveryOrderLine.MxDQty
                                            && _locDeliveryOrderLine.DQty <= _locDeliveryOrderLine.SalesInvoiceMonitoring.MxDQty)
                                        {
                                            _locDlvRmDQty = _locDeliveryOrderLine.SalesInvoiceMonitoring.MxDQty - _locDeliveryOrderLine.DQty;
                                        }

                                        if (_locDeliveryOrderLine.Qty > 0 && _locDeliveryOrderLine.Qty <= _locDeliveryOrderLine.MxQty
                                            && _locDeliveryOrderLine.Qty <= _locDeliveryOrderLine.SalesInvoiceMonitoring.MxQty)
                                        {
                                            _locDlvRmQty = _locDeliveryOrderLine.SalesInvoiceMonitoring.MxQty - _locDeliveryOrderLine.Qty;
                                        }

                                        if (_locDeliveryOrderLine.SalesInvoiceMonitoring.Item != null && _locDeliveryOrderLine.SalesInvoiceMonitoring.MxDUOM != null &&
                                            _locDeliveryOrderLine.SalesInvoiceMonitoring.MxUOM != null)
                                        {
                                            _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                                   (new GroupOperator(GroupOperatorType.And,
                                                                    new BinaryOperator("Item", _locDeliveryOrderLine.SalesInvoiceMonitoring.Item),
                                                                    new BinaryOperator("UOM", _locDeliveryOrderLine.SalesInvoiceMonitoring.MxUOM),
                                                                    new BinaryOperator("DefaultUOM", _locDeliveryOrderLine.SalesInvoiceMonitoring.MxDUOM),
                                                                    new BinaryOperator("Active", true)));
                                        }

                                        if (_locItemUOM != null)
                                        {
                                            if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                            {
                                                _locDlvInvLineTotal = _locDlvRmQty * _locItemUOM.DefaultConversion + _locDlvRmDQty;
                                            }
                                            else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                            {
                                                _locDlvInvLineTotal = _locDlvRmQty / _locItemUOM.Conversion + _locDlvRmDQty;
                                            }
                                            else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                            {
                                                _locDlvInvLineTotal = _locDlvRmQty + _locDlvRmDQty;
                                            }
                                        }
                                        else
                                        {
                                            _locDlvInvLineTotal = _locDlvRmQty + _locDlvRmDQty;
                                        }

                                    }
                                    #endregion MaxQuantity
                                }
                                #endregion SalesInvoiceMonitoringProcessCount=0

                                #region SalesInvoiceMonitoringProcessCount>0
                                if (_locDeliveryOrderLine.SalesInvoiceMonitoring.DlvPostedCount > 0)
                                {
                                    if (_locDeliveryOrderLine.SalesInvoiceMonitoring.DlvRmDQty > 0)
                                    {
                                        _locDlvRmDQty = _locDeliveryOrderLine.SalesInvoiceMonitoring.DlvRmDQty - _locDeliveryOrderLine.DQty;
                                    }

                                    if (_locDeliveryOrderLine.SalesInvoiceMonitoring.DlvRmQty > 0)
                                    {
                                        _locDlvRmQty = _locDeliveryOrderLine.SalesInvoiceMonitoring.DlvRmQty - _locDeliveryOrderLine.Qty;
                                    }

                                    if (_locDeliveryOrderLine.SalesInvoiceMonitoring.MxDUOM != null && _locDeliveryOrderLine.SalesInvoiceMonitoring.MxUOM != null
                                        && _locDeliveryOrderLine.SalesInvoiceMonitoring.Item != null)
                                    {
                                        _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                                   (new GroupOperator(GroupOperatorType.And,
                                                                    new BinaryOperator("Item", _locDeliveryOrderLine.SalesInvoiceMonitoring.Item),
                                                                    new BinaryOperator("UOM", _locDeliveryOrderLine.SalesInvoiceMonitoring.MxUOM),
                                                                    new BinaryOperator("DefaultUOM", _locDeliveryOrderLine.SalesInvoiceMonitoring.MxDUOM),
                                                                    new BinaryOperator("Active", true)));
                                    }
                                    else
                                    {
                                        _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                                   (new GroupOperator(GroupOperatorType.And,
                                                                    new BinaryOperator("Item", _locDeliveryOrderLine.Item),
                                                                    new BinaryOperator("UOM", _locDeliveryOrderLine.UOM),
                                                                    new BinaryOperator("DefaultUOM", _locDeliveryOrderLine.DUOM),
                                                                    new BinaryOperator("Active", true)));
                                    }


                                    if (_locItemUOM != null)
                                    {
                                        if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                        {
                                            _locDlvInvLineTotal = _locDlvRmQty * _locItemUOM.DefaultConversion + _locDlvRmDQty;
                                        }
                                        else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                        {
                                            _locDlvInvLineTotal = _locDlvRmQty / _locItemUOM.Conversion + _locDlvRmDQty;
                                        }
                                        else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                        {
                                            _locDlvInvLineTotal = _locDlvRmQty + _locDlvRmDQty;
                                        }
                                    }
                                    else
                                    {
                                        _locDlvInvLineTotal = _locDlvRmQty + _locDlvRmDQty;
                                    }

                                }
                                #endregion SalesInvoiceMonitoringProcessCount>0
                                _locDeliveryOrderLine.SalesInvoiceMonitoring.DlvRmDQty = _locDlvRmDQty;
                                _locDeliveryOrderLine.SalesInvoiceMonitoring.DlvRmDQty = _locDlvRmQty;
                                _locDeliveryOrderLine.SalesInvoiceMonitoring.DlvRmDQty = _locDlvInvLineTotal;
                                _locDeliveryOrderLine.SalesInvoiceMonitoring.Save();
                                _locDeliveryOrderLine.SalesInvoiceMonitoring.Session.CommitTransaction();
                            }
                            #endregion SalesInvoiceMonitoring
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = DeliveryOrder " + ex.ToString());
            }
        }

        private void SetPostingQty(Session _currSession, DeliveryOrder _locDeliveryOrderXPO)
        {
            try
            {
                if (_locDeliveryOrderXPO != null)
                {
                    double _locPDQty = 0;
                    double _locPQty = 0;
                    double _locInvLineTotal = 0;
                    ItemUnitOfMeasure _locItemUOM = null;

                    XPCollection<DeliveryOrderLine> _locDeliveryOrderLines = new XPCollection<DeliveryOrderLine>(_currSession,
                                                                        new GroupOperator(GroupOperatorType.And,
                                                                        new BinaryOperator("DeliveryOrder", _locDeliveryOrderXPO),
                                                                        new BinaryOperator("Select", true),
                                                                        new BinaryOperator("DeliveryStatus", DeliveryStatus.PendingAll, BinaryOperatorType.NotEqual),
                                                                        new BinaryOperator("DeliveryStatus", DeliveryStatus.CancelAll, BinaryOperatorType.NotEqual),
                                                                        new GroupOperator(GroupOperatorType.Or,
                                                                        new BinaryOperator("Status", Status.Progress),
                                                                        new BinaryOperator("Status", Status.Posted))));

                    if (_locDeliveryOrderLines != null && _locDeliveryOrderLines.Count() > 0)
                    {
                        foreach (DeliveryOrderLine _locDeliveryOrderLine in _locDeliveryOrderLines)
                        {
                            #region DeliveryOrderLine

                            #region DeliveryOrderLineProcessCount=0
                            if (_locDeliveryOrderLine.PostedCount == 0)
                            {
                                #region MaxQuantity
                                if ((_locDeliveryOrderLine.MxDQty > 0 || _locDeliveryOrderLine.MxQty > 0) && _locDeliveryOrderLine.MxTQty > 0)
                                {
                                    if (_locDeliveryOrderLine.DQty > 0 && _locDeliveryOrderLine.DQty <= _locDeliveryOrderLine.MxDQty)
                                    {
                                        _locPDQty = _locDeliveryOrderLine.DQty;
                                    }

                                    if (_locDeliveryOrderLine.Qty > 0 && _locDeliveryOrderLine.Qty <= _locDeliveryOrderLine.MxQty)
                                    {
                                        _locPQty = _locDeliveryOrderLine.Qty;
                                    }

                                    if (_locDeliveryOrderLine.Item != null && _locDeliveryOrderLine.MxUOM != null && _locDeliveryOrderLine.MxDUOM != null)
                                    {
                                        _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                               (new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("Item", _locDeliveryOrderLine.Item),
                                                                new BinaryOperator("UOM", _locDeliveryOrderLine.MxUOM),
                                                                new BinaryOperator("DefaultUOM", _locDeliveryOrderLine.MxDUOM),
                                                                new BinaryOperator("Active", true)));
                                    }

                                    if (_locItemUOM != null)
                                    {
                                        if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locPQty * _locItemUOM.DefaultConversion + _locPDQty;
                                        }
                                        else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locPQty / _locItemUOM.Conversion + _locPDQty;
                                        }
                                        else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locPQty + _locPDQty;
                                        }
                                    }
                                    else
                                    {
                                        _locInvLineTotal = _locPQty + _locPDQty;
                                    }
                                }
                                #endregion MaxQuantity
                                #region NonMaxQuantity
                                else
                                {
                                    if (_locDeliveryOrderLine.DQty > 0)
                                    {
                                        _locPDQty = _locDeliveryOrderLine.DQty;
                                    }

                                    if (_locDeliveryOrderLine.Qty > 0)
                                    {
                                        _locPQty = _locDeliveryOrderLine.Qty;
                                    }

                                    if (_locDeliveryOrderLine.Item != null && _locDeliveryOrderLine.UOM != null && _locDeliveryOrderLine.DUOM != null)
                                    {
                                        _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                               (new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("Item", _locDeliveryOrderLine.Item),
                                                                new BinaryOperator("UOM", _locDeliveryOrderLine.UOM),
                                                                new BinaryOperator("DefaultUOM", _locDeliveryOrderLine.DUOM),
                                                                new BinaryOperator("Active", true)));
                                    }

                                    if (_locItemUOM != null)
                                    {
                                        if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locPQty * _locItemUOM.DefaultConversion + _locPDQty;
                                        }
                                        else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locPQty / _locItemUOM.Conversion + _locPDQty;
                                        }
                                        else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locPQty + _locPDQty;
                                        }
                                    }
                                    else
                                    {
                                        _locInvLineTotal = _locPQty + _locPDQty;
                                    }
                                }
                                #endregion NonMaxQuantity
                            }
                            #endregion DeliveryOrderLineProcessCount=0

                            #region DeliveryOrderLineProcessCount>0
                            if (_locDeliveryOrderLine.PostedCount > 0)
                            {
                                if (_locDeliveryOrderLine.PDQty >= 0)
                                {
                                    _locPDQty = _locDeliveryOrderLine.PDQty + _locDeliveryOrderLine.DQty;
                                }

                                if (_locDeliveryOrderLine.PQty >= 0)
                                {
                                    _locPQty = _locDeliveryOrderLine.PQty + _locDeliveryOrderLine.Qty;
                                }

                                if (_locDeliveryOrderLine.MxUOM != null || _locDeliveryOrderLine.MxDUOM != null && _locDeliveryOrderLine.Item != null)
                                {
                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                           (new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("Item", _locDeliveryOrderLine.Item),
                                                            new BinaryOperator("UOM", _locDeliveryOrderLine.MxUOM),
                                                            new BinaryOperator("DefaultUOM", _locDeliveryOrderLine.MxDUOM),
                                                            new BinaryOperator("Active", true)));
                                }
                                else
                                {
                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                           (new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("Item", _locDeliveryOrderLine.Item),
                                                            new BinaryOperator("UOM", _locDeliveryOrderLine.UOM),
                                                            new BinaryOperator("DefaultUOM", _locDeliveryOrderLine.DUOM),
                                                            new BinaryOperator("Active", true)));
                                }

                                if (_locItemUOM != null)
                                {
                                    if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                    {
                                        _locInvLineTotal = _locPQty * _locItemUOM.DefaultConversion + _locPDQty;
                                    }
                                    else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                    {
                                        _locInvLineTotal = _locPQty / _locItemUOM.Conversion + _locPDQty;
                                    }
                                    else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                    {
                                        _locInvLineTotal = _locPQty + _locPDQty;
                                    }
                                }
                                else
                                {
                                    _locInvLineTotal = _locPQty + _locPDQty;
                                }

                            }
                            #endregion DeliveryOrderLineProcessCount>0

                            _locDeliveryOrderLine.PDQty = _locPDQty;
                            _locDeliveryOrderLine.PDUOM = _locDeliveryOrderLine.DUOM;
                            _locDeliveryOrderLine.PQty = _locPQty;
                            _locDeliveryOrderLine.PUOM = _locDeliveryOrderLine.UOM;
                            _locDeliveryOrderLine.PTQty = _locInvLineTotal;
                            _locDeliveryOrderLine.Save();
                            _locDeliveryOrderLine.Session.CommitTransaction();

                            #endregion DeliveryOrderLine

                            #region SalesInvoiceMonitoring
                            if (_locDeliveryOrderLine.SalesInvoiceMonitoring != null)
                            {
                                double _locDlvPDQty = 0;
                                double _locDlvPQty = 0;
                                double _locDlvInvLineTotal = 0;
                                #region SalesInvoiceMonitoringProcessCount=0
                                if (_locDeliveryOrderLine.SalesInvoiceMonitoring.DlvPostedCount == 0)
                                {
                                    #region MaxQuantity
                                    if ((_locDeliveryOrderLine.SalesInvoiceMonitoring.MxDQty > 0 || _locDeliveryOrderLine.SalesInvoiceMonitoring.MxQty > 0)
                                        && _locDeliveryOrderLine.SalesInvoiceMonitoring.MxTQty > 0)
                                    {
                                        if (_locDeliveryOrderLine.DQty > 0 && _locDeliveryOrderLine.DQty <= _locDeliveryOrderLine.MxDQty &&
                                            _locDeliveryOrderLine.DQty <= _locDeliveryOrderLine.SalesInvoiceMonitoring.MxDQty)
                                        {
                                            _locDlvPDQty = _locDeliveryOrderLine.DQty;
                                        }

                                        if (_locDeliveryOrderLine.Qty > 0 && _locDeliveryOrderLine.Qty <= _locDeliveryOrderLine.MxQty &&
                                            _locDeliveryOrderLine.Qty <= _locDeliveryOrderLine.SalesInvoiceMonitoring.MxQty)
                                        {
                                            _locDlvPQty = _locDeliveryOrderLine.Qty;
                                        }

                                        if (_locDeliveryOrderLine.SalesInvoiceMonitoring.Item != null && _locDeliveryOrderLine.SalesInvoiceMonitoring.MxDUOM != null
                                            && _locDeliveryOrderLine.SalesInvoiceMonitoring.MxUOM != null)
                                        {
                                            _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                                   (new GroupOperator(GroupOperatorType.And,
                                                                    new BinaryOperator("Item", _locDeliveryOrderLine.SalesInvoiceMonitoring.Item),
                                                                    new BinaryOperator("UOM", _locDeliveryOrderLine.SalesInvoiceMonitoring.MxUOM),
                                                                    new BinaryOperator("DefaultUOM", _locDeliveryOrderLine.SalesInvoiceMonitoring.MxDUOM),
                                                                    new BinaryOperator("Active", true)));
                                        }

                                        if (_locItemUOM != null)
                                        {
                                            if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                            {
                                                _locDlvInvLineTotal = _locDlvPQty * _locItemUOM.DefaultConversion + _locDlvPDQty;
                                            }
                                            else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                            {
                                                _locDlvInvLineTotal = _locDlvPQty / _locItemUOM.Conversion + _locDlvPDQty;
                                            }
                                            else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                            {
                                                _locDlvInvLineTotal = _locDlvPQty + _locDlvPDQty;
                                            }
                                        }
                                        else
                                        {
                                            _locDlvInvLineTotal = _locDlvPQty + _locDlvPDQty;
                                        }
                                    }
                                    #endregion MaxQuantity
                                }
                                #endregion SalesInvoiceMonitoringProcessCount=0

                                #region SalesInvoiceMonitoringProcessCount>0
                                if (_locDeliveryOrderLine.SalesInvoiceMonitoring.DlvPostedCount > 0)
                                {
                                    if (_locDeliveryOrderLine.SalesInvoiceMonitoring.DlvPDQty >= 0)
                                    {
                                        _locDlvPDQty = _locDeliveryOrderLine.SalesInvoiceMonitoring.DlvPDQty + _locDeliveryOrderLine.DQty;
                                    }

                                    if (_locDeliveryOrderLine.SalesInvoiceMonitoring.DlvPQty >= 0)
                                    {
                                        _locDlvPQty = _locDeliveryOrderLine.SalesInvoiceMonitoring.DlvPQty + _locDeliveryOrderLine.Qty;
                                    }

                                    if (_locDeliveryOrderLine.SalesInvoiceMonitoring.Item != null && _locDeliveryOrderLine.SalesInvoiceMonitoring.MxUOM != null &&
                                        _locDeliveryOrderLine.SalesInvoiceMonitoring.MxDUOM != null)
                                    {
                                        _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                               (new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("Item", _locDeliveryOrderLine.SalesInvoiceMonitoring.Item),
                                                                new BinaryOperator("UOM", _locDeliveryOrderLine.SalesInvoiceMonitoring.MxUOM),
                                                                new BinaryOperator("DefaultUOM", _locDeliveryOrderLine.SalesInvoiceMonitoring.MxDUOM),
                                                                new BinaryOperator("Active", true)));
                                    }
                                    else
                                    {
                                        _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                               (new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("Item", _locDeliveryOrderLine.Item),
                                                                new BinaryOperator("UOM", _locDeliveryOrderLine.UOM),
                                                                new BinaryOperator("DefaultUOM", _locDeliveryOrderLine.DUOM),
                                                                new BinaryOperator("Active", true)));
                                    }

                                    if (_locItemUOM != null)
                                    {
                                        if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                        {
                                            _locDlvInvLineTotal = _locDlvPQty * _locItemUOM.DefaultConversion + _locDlvPDQty;
                                        }
                                        else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                        {
                                            _locDlvInvLineTotal = _locDlvPQty / _locItemUOM.Conversion + _locDlvPDQty;
                                        }
                                        else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                        {
                                            _locDlvInvLineTotal = _locDlvPQty + _locDlvPDQty;
                                        }
                                    }
                                    else
                                    {
                                        _locDlvInvLineTotal = _locDlvPQty + _locDlvPDQty;
                                    }

                                }
                                #endregion SalesInvoiceMonitoringProcessCount>0

                                _locDeliveryOrderLine.SalesInvoiceMonitoring.DlvPDQty = _locDlvPDQty;
                                _locDeliveryOrderLine.SalesInvoiceMonitoring.DlvPDUOM = _locDeliveryOrderLine.DUOM;
                                _locDeliveryOrderLine.SalesInvoiceMonitoring.DlvPQty = _locDlvPQty;
                                _locDeliveryOrderLine.SalesInvoiceMonitoring.DlvPUOM = _locDeliveryOrderLine.UOM;
                                _locDeliveryOrderLine.SalesInvoiceMonitoring.DlvPTQty = _locDlvInvLineTotal;
                                _locDeliveryOrderLine.SalesInvoiceMonitoring.Save();
                                _locDeliveryOrderLine.SalesInvoiceMonitoring.Session.CommitTransaction();
                            }
                            #endregion SalesInvoiceMonitoring
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = DeliveryOrder " + ex.ToString());
            }
        }

        private void SetPostedCount(Session _currSession, DeliveryOrder _locDeliveryOrderXPO)
        {
            try
            {
                if (_locDeliveryOrderXPO != null)
                {
                    XPCollection<DeliveryOrderLine> _locDeliveryOrderLines = new XPCollection<DeliveryOrderLine>(_currSession,
                                                                        new GroupOperator(GroupOperatorType.And,
                                                                        new BinaryOperator("DeliveryOrder", _locDeliveryOrderXPO),
                                                                        new BinaryOperator("Select", true),
                                                                        new GroupOperator(GroupOperatorType.Or,
                                                                        new BinaryOperator("Status", Status.Progress),
                                                                        new BinaryOperator("Status", Status.Posted))));

                    if (_locDeliveryOrderLines != null && _locDeliveryOrderLines.Count() > 0)
                    {
                        foreach (DeliveryOrderLine _locDeliveryOrderLine in _locDeliveryOrderLines)
                        {
                            _locDeliveryOrderLine.PostedCount = _locDeliveryOrderLine.PostedCount + 1;
                            _locDeliveryOrderLine.Save();
                            _locDeliveryOrderLine.Session.CommitTransaction();

                            if (_locDeliveryOrderLine.SalesInvoiceMonitoring != null)
                            {
                                _locDeliveryOrderLine.SalesInvoiceMonitoring.DlvPostedCount = _locDeliveryOrderLine.SalesInvoiceMonitoring.DlvPostedCount + 1;
                                _locDeliveryOrderLine.SalesInvoiceMonitoring.Save();
                                _locDeliveryOrderLine.SalesInvoiceMonitoring.Session.CommitTransaction();
                            }
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = DeliveryOrder " + ex.ToString());
            }
        }

        private void SetStatusDeliveryOrderLineAndSalesInvoiceMonitoringAndDeliveryOrderMonitoring(Session _currSession, DeliveryOrder _locDeliveryOrderXPO)
        {
            try
            {
                DateTime now = DateTime.Now;
                if (_locDeliveryOrderXPO != null)
                {
                    XPCollection<DeliveryOrderLine> _locDeliveryOrderLines = new XPCollection<DeliveryOrderLine>(_currSession,
                                                                        new GroupOperator(GroupOperatorType.And,
                                                                        new BinaryOperator("DeliveryOrder", _locDeliveryOrderXPO),
                                                                        new BinaryOperator("Select", true),
                                                                        new GroupOperator(GroupOperatorType.Or,
                                                                        new BinaryOperator("Status", Status.Progress),
                                                                        new BinaryOperator("Status", Status.Posted))));

                    if (_locDeliveryOrderLines != null && _locDeliveryOrderLines.Count() > 0)
                    {

                        foreach (DeliveryOrderLine _locDeliveryOrderLine in _locDeliveryOrderLines)
                        {
                            DeliveryStatus _locDeliveryStatus = DeliveryStatus.None;
                            CollectStatus _locCollectStatus = CollectStatus.None;
                            if (_locDeliveryOrderLine.DeliveryStatus == DeliveryStatus.Loaded || _locDeliveryOrderLine.DeliveryStatus == DeliveryStatus.DeliveryAll)
                            {
                                _locDeliveryStatus = DeliveryStatus.DeliveryAll;
                                _locCollectStatus = CollectStatus.Ready;
                            }
                            else
                            {
                                _locDeliveryStatus = _locDeliveryOrderLine.DeliveryStatus;
                                _locCollectStatus = CollectStatus.NotReady;
                            }

                            #region DeliveryOrderLine

                            if (_locDeliveryOrderLine.RmDQty == 0 && _locDeliveryOrderLine.RmQty == 0 && _locDeliveryOrderLine.RmTQty == 0)
                            {

                                _locDeliveryOrderLine.ActivationPosting = true;
                                _locDeliveryOrderLine.Status = Status.Close;
                                _locDeliveryOrderLine.StatusDate = now;
                                _locDeliveryOrderLine.DeliveryStatus = _locDeliveryStatus;
                                _locDeliveryOrderLine.DeliveryStatusDate = now;

                            }
                            else
                            {
                                _locDeliveryOrderLine.ActivationPosting = true;
                                _locDeliveryOrderLine.Status = Status.Posted;
                                _locDeliveryOrderLine.StatusDate = now;
                                _locDeliveryOrderLine.DeliveryStatus = _locDeliveryStatus;
                                _locDeliveryOrderLine.DeliveryStatusDate = now;
                            }
                            _locDeliveryOrderLine.Save();
                            _locDeliveryOrderLine.Session.CommitTransaction();
                            #endregion DeliveryOrderLine
                            #region SalesInvoiceMonitoring
                            if (_locDeliveryOrderLine.SalesInvoiceMonitoring != null)
                            {
                                
                                _locDeliveryOrderLine.SalesInvoiceMonitoring.DeliveryStatus = _locDeliveryStatus;
                                _locDeliveryOrderLine.SalesInvoiceMonitoring.DeliveryStatusDate = now;
                                _locDeliveryOrderLine.SalesInvoiceMonitoring.CollectStatus = _locCollectStatus;
                                _locDeliveryOrderLine.SalesInvoiceMonitoring.CollectStatusDate = now;
                                _locDeliveryOrderLine.SalesInvoiceMonitoring.Save();
                                _locDeliveryOrderLine.SalesInvoiceMonitoring.Session.CommitTransaction();
                            }
                            #endregion SalesInvoiceMonitoring
                            #region DeliveryOrderMonitoring
                            DeliveryOrderMonitoring _locDeliveryOrderMonitoring = _currSession.FindObject<DeliveryOrderMonitoring>
                                                                                (new GroupOperator(GroupOperatorType.And,
                                                                                new BinaryOperator("DeliveryOrderLine", _locDeliveryOrderLine)));
                            if(_locDeliveryOrderMonitoring != null)
                            {
                                _locDeliveryOrderMonitoring.DeliveryStatus = _locDeliveryStatus;
                                _locDeliveryOrderMonitoring.DeliveryStatusDate = _locDeliveryOrderLine.DeliveryStatusDate;
                                _locDeliveryOrderMonitoring.Save();
                                _locDeliveryOrderMonitoring.Session.CommitTransaction();

                            }
                            #endregion DeliveryOrderMonitoring
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = DeliveryOrder " + ex.ToString());
            }
        }

        private void SetNormalQuantity(Session _currSession, DeliveryOrder _locDeliveryOrderXPO)
        {
            try
            {
                DateTime now = DateTime.Now;
                if (_locDeliveryOrderXPO != null)
                {
                    XPCollection<DeliveryOrderLine> _locDeliveryOrderLines = new XPCollection<DeliveryOrderLine>(_currSession,
                                                                        new GroupOperator(GroupOperatorType.And,
                                                                        new BinaryOperator("DeliveryOrder", _locDeliveryOrderXPO),
                                                                        new BinaryOperator("Select", true),
                                                                        new GroupOperator(GroupOperatorType.Or,
                                                                        new BinaryOperator("Status", Status.Progress),
                                                                        new BinaryOperator("Status", Status.Posted))));

                    if (_locDeliveryOrderLines != null && _locDeliveryOrderLines.Count() > 0)
                    {
                        foreach (DeliveryOrderLine _locDeliveryOrderLine in _locDeliveryOrderLines)
                        {
                            #region DeliveryOrderLine
                            if (_locDeliveryOrderLine.DeliveryStatus == DeliveryStatus.CancelAll || _locDeliveryOrderLine.DeliveryStatus == DeliveryStatus.PendingAll)
                            {
                                if (_locDeliveryOrderLine.DQty > 0 || _locDeliveryOrderLine.Qty > 0)
                                {
                                    _locDeliveryOrderLine.Select = false;
                                    _locDeliveryOrderLine.Save();
                                    _locDeliveryOrderLine.Session.CommitTransaction();
                                }
                            }
                            else
                            {
                                if (_locDeliveryOrderLine.DQty > 0 || _locDeliveryOrderLine.Qty > 0)
                                {
                                    _locDeliveryOrderLine.Select = false;
                                    _locDeliveryOrderLine.DQty = 0;
                                    _locDeliveryOrderLine.Qty = 0;
                                    _locDeliveryOrderLine.ATA = now;
                                    _locDeliveryOrderLine.Save();
                                    _locDeliveryOrderLine.Session.CommitTransaction();

                                    if(_locDeliveryOrderLine.SalesInvoiceMonitoring != null)
                                    {
                                        _locDeliveryOrderLine.SalesInvoiceMonitoring.ATA = now;
                                        _locDeliveryOrderLine.SalesInvoiceMonitoring.Save();
                                        _locDeliveryOrderLine.SalesInvoiceMonitoring.Session.CommitTransaction();
                                    }
                                }
                            }
                            #endregion DeliveryOrderLine
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = DeliveryOrder " + ex.ToString());
            }
        }

        private void SetFinalDeliveryOrder(Session _currSession, DeliveryOrder _locDeliveryOrderXPO)
        {
            try
            {
                DateTime now = DateTime.Now;
                int _locStatusCount = 0;
                int _locDlvOrderLineCount = 0;

                if (_locDeliveryOrderXPO != null)
                {

                    XPCollection<DeliveryOrderLine> _locDeliveryOrderLines = new XPCollection<DeliveryOrderLine>(_currSession,
                                                            new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("DeliveryOrder", _locDeliveryOrderXPO)));

                    if (_locDeliveryOrderLines != null && _locDeliveryOrderLines.Count() > 0)
                    {
                        _locDlvOrderLineCount = _locDeliveryOrderLines.Count();

                        foreach (DeliveryOrderLine _locDeliveryOrderLine in _locDeliveryOrderLines)
                        {
                            if (_locDeliveryOrderLine.Status == Status.Close)
                            {
                                _locStatusCount = _locStatusCount + 1;
                            }
                        }

                        if (_locStatusCount == _locDlvOrderLineCount)
                        {
                            _locDeliveryOrderXPO.ActivationPosting = true;
                            _locDeliveryOrderXPO.Status = Status.Close;
                            _locDeliveryOrderXPO.StatusDate = now;
                            _locDeliveryOrderXPO.Save();
                            _locDeliveryOrderXPO.Session.CommitTransaction();
                        }
                        else
                        {
                            _locDeliveryOrderXPO.Status = Status.Posted;
                            _locDeliveryOrderXPO.StatusDate = now;
                            _locDeliveryOrderXPO.Save();
                            _locDeliveryOrderXPO.Session.CommitTransaction();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = DeliveryOrder " + ex.ToString());
            }
        }

        private void SetCollectStatus(Session _currSession, DeliveryOrder _locDeliveryOrderXPO)
        {
            try
            {
                DateTime now = DateTime.Now;
                int _locCollectStatusCount1 = 0;
                int _locCollectStatusCount2 = 0;
                int _locSlsInvMonCount = 0;
                double _locTotMaxTQty = 0;
                bool _locPassPickingMonitoringStatus = true;

                if (_locDeliveryOrderXPO != null)
                {
                    
                    if(_locDeliveryOrderXPO.SalesInvoice != null )
                    {
                        XPCollection<SalesInvoiceMonitoring> _locSalesInvoiceMonitorings = new XPCollection<SalesInvoiceMonitoring>(_currSession,
                                                                                        new GroupOperator(GroupOperatorType.And,
                                                                                        new BinaryOperator("SalesInvoice", _locDeliveryOrderXPO.SalesInvoice)));
                        if(_locSalesInvoiceMonitorings != null && _locSalesInvoiceMonitorings.Count() > 0)
                        {
                            foreach(SalesInvoiceMonitoring _locSalesInvoiceMonitoring in _locSalesInvoiceMonitorings)
                            {
                                XPCollection<DeliveryOrderMonitoring> _locDeliveryOrderMonitorings = new XPCollection<DeliveryOrderMonitoring>(_currSession, 
                                                                                                    new GroupOperator(GroupOperatorType.And,
                                                                                                    new BinaryOperator("SalesInvoiceMonitoring", _locSalesInvoiceMonitoring)));
                                if(_locDeliveryOrderMonitorings != null && _locDeliveryOrderMonitorings.Count() > 0)
                                {
                                    foreach(DeliveryOrderMonitoring _locDeliveryOrderMonitoring in _locDeliveryOrderMonitorings)
                                    {
                                        _locTotMaxTQty = _locTotMaxTQty + _locDeliveryOrderMonitoring.MxTQty;
                                    }
                                    if(_locSalesInvoiceMonitoring.MxTQty != _locTotMaxTQty)
                                    {
                                        _locPassPickingMonitoringStatus = false;
                                    }
                                }
                                _locTotMaxTQty = 0;
                            }
                            
                        }

                        if (_locPassPickingMonitoringStatus == true)
                        {
                            XPCollection<SalesInvoiceMonitoring> _locSalesInvMonitorings = new XPCollection<SalesInvoiceMonitoring>(_currSession,
                                                                                        new GroupOperator(GroupOperatorType.And,
                                                                                        new BinaryOperator("SalesInvoice", _locDeliveryOrderXPO.SalesInvoice)));

                            if(_locSalesInvMonitorings != null && _locSalesInvMonitorings.Count() > 0)
                            {
                                _locSlsInvMonCount = _locSalesInvMonitorings.Count();

                                foreach (SalesInvoiceMonitoring _locSalesInvMonitoring in _locSalesInvMonitorings)
                                {
                                    #region GetCollectStatus=Ready
                                    if (_locSalesInvMonitoring.CollectStatus == CollectStatus.Ready)
                                    {
                                        _locCollectStatusCount1 = _locCollectStatusCount1 + 1;
                                    }
                                    #endregion GetCollectStatus=Ready
                                    #region GetCollectStatus=NotReady
                                    if (_locSalesInvMonitoring.CollectStatus == CollectStatus.NotReady
                                        && (_locSalesInvMonitoring.DeliveryStatus == DeliveryStatus.PendingAll ||
                                        _locSalesInvMonitoring.DeliveryStatus == DeliveryStatus.CancelAll))
                                    {
                                        _locCollectStatusCount2 = _locCollectStatusCount2 + 1;
                                    }
                                    #endregion GetCollectStatus=NotReady
                                }
                            }

                            if (_locDeliveryOrderXPO.SelectInvoice != null)
                            {
                                if (_locCollectStatusCount1 == _locSlsInvMonCount)
                                {
                                    _locDeliveryOrderXPO.SelectInvoice.AD = false;
                                    _locDeliveryOrderXPO.SelectInvoice.OC = true;
                                    _locDeliveryOrderXPO.SelectInvoice.Active = false;
                                    _locDeliveryOrderXPO.SelectInvoice.CollectStatus = CollectStatus.Ready;
                                    _locDeliveryOrderXPO.SelectInvoice.CollectStatusDate = now;
                                    _locDeliveryOrderXPO.SelectInvoice.Save();
                                    _locDeliveryOrderXPO.SelectInvoice.Session.CommitTransaction();

                                }
                                else if (_locCollectStatusCount2 == _locSlsInvMonCount)
                                {
                                    _locDeliveryOrderXPO.SelectInvoice.AD = false;
                                    _locDeliveryOrderXPO.SelectInvoice.OC = false;
                                    _locDeliveryOrderXPO.SelectInvoice.Active = false;
                                    _locDeliveryOrderXPO.SelectInvoice.CollectStatus = CollectStatus.Cancel;
                                    _locDeliveryOrderXPO.SelectInvoice.CollectStatusDate = now;
                                    _locDeliveryOrderXPO.SelectInvoice.Active = false;
                                    _locDeliveryOrderXPO.SelectInvoice.Save();
                                    _locDeliveryOrderXPO.SelectInvoice.Session.CommitTransaction();

                                    _locDeliveryOrderXPO.SalesInvoice.Status = Status.Cancel;
                                    _locDeliveryOrderXPO.SalesInvoice.StatusDate = now;
                                    _locDeliveryOrderXPO.SalesInvoice.Save();
                                    _locDeliveryOrderXPO.SalesInvoice.Session.CommitTransaction();

                                }
                                else
                                {
                                    //Untuk split pengiriman gak perlu masuk ke AmendDelivery
                                    _locDeliveryOrderXPO.SelectInvoice.AD = true;
                                    _locDeliveryOrderXPO.SelectInvoice.OC = false;
                                    _locDeliveryOrderXPO.SelectInvoice.Active = false;
                                    _locDeliveryOrderXPO.SelectInvoice.CollectStatus = CollectStatus.NotReady;
                                    _locDeliveryOrderXPO.SelectInvoice.CollectStatusDate = now;
                                    _locDeliveryOrderXPO.SelectInvoice.Save();
                                    _locDeliveryOrderXPO.SelectInvoice.Session.CommitTransaction();
                                }
                            }
                        }
                        _locSlsInvMonCount = 0;
                        _locPassPickingMonitoringStatus = true;
                    }  
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = DeliveryOrder " + ex.ToString());
            }
        }

        private void SetPickingStatus(Session _currSession, DeliveryOrder _locDeliveryOrderXPO)
        {
            try
            {
                DateTime now = DateTime.Now;
                bool _locBoolAD = false;
                bool _locBoolOC = false;
                bool _locBoolActive = false;
                if (_locDeliveryOrderXPO != null)
                {
                    if (_locDeliveryOrderXPO.Picking != null)
                    {
                        XPCollection<PickingMonitoring> _locPickingMonitorings = new XPCollection<PickingMonitoring>(_currSession,
                                                                    new GroupOperator(GroupOperatorType.And,
                                                                    new BinaryOperator("Picking", _locDeliveryOrderXPO.Picking)));
                        if (_locPickingMonitorings != null && _locPickingMonitorings.Count() > 0)
                        {
                            foreach (PickingMonitoring _locPickingMonitoring in _locPickingMonitorings)
                            {
                                if (_locPickingMonitoring.AD == true)
                                {
                                    _locBoolAD = true;
                                }
                                if (_locPickingMonitoring.OC == true)
                                {
                                    _locBoolOC = true;
                                }
                                if (_locPickingMonitoring.Active == true)
                                {
                                    _locBoolActive = true;
                                }
                            }

                            _locDeliveryOrderXPO.Picking.AD = _locBoolAD;
                            _locDeliveryOrderXPO.Picking.OC = _locBoolOC;
                            _locDeliveryOrderXPO.Picking.Active = _locBoolActive;
                            _locDeliveryOrderXPO.Picking.Save();
                            _locDeliveryOrderXPO.Picking.Session.CommitTransaction();
                        }

                        _locDeliveryOrderXPO.SelectInvoice.DeliveryOrder = _locDeliveryOrderXPO;
                        _locDeliveryOrderXPO.SelectInvoice.Save();
                        _locDeliveryOrderXPO.SelectInvoice.Session.CommitTransaction();
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = DeliveryOrder " + ex.ToString());
            }
        }

        #endregion Posting Method

        #region Global Method

        private void SuccessMessageShow(string _locMessage)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 4000;
            options.Message = string.Format("{0}", _locMessage);
            options.Type = InformationType.Success;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Success";
            options.Win.Type = WinMessageType.Flyout;
            Application.ShowViewStrategy.ShowMessage(options);
        }

        private void ErrorMessageShow(string _locMessage)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 2000;
            options.Message = string.Format(_locMessage);
            options.Type = InformationType.Warning;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Error";
            options.Win.Type = WinMessageType.Alert;
            Application.ShowViewStrategy.ShowMessage(options);
        }

        #endregion Global Method

        
    }
}
