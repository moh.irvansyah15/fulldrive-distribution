﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;

using FullDrive.Module.BusinessObjects;
using FullDrive.Module.CustomProcess;
using System.Collections;
using DevExpress.Xpo;
using DevExpress.ExpressApp.Xpo;
using System.IO;
using System.Web;

namespace FullDrive.Module.Controllers
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class InvoiceInMonitoringActionController : ViewController
    {
        public InvoiceInMonitoringActionController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.
        }
        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }
        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        private void InvoiceInMonitoringImportAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;

                if (_objectsToProcess != null)
                {
                    foreach (Object obj in _objectsToProcess)
                    {
                        InvoiceInMonitoring _locInvoiceInMonitoringOS = (InvoiceInMonitoring)_objectSpace.GetObject(obj);

                        if (_locInvoiceInMonitoringOS != null)
                        {
                            if (_locInvoiceInMonitoringOS.Session != null)
                            {
                                _currSession = _locInvoiceInMonitoringOS.Session;
                            }

                            if (_locInvoiceInMonitoringOS.Code != null && _currSession != null)
                            {
                                _currObjectId = _locInvoiceInMonitoringOS.Code;

                                InvoiceInMonitoring _locInvoiceInMonitoringXPO = _currSession.FindObject<InvoiceInMonitoring>
                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                     new BinaryOperator("Code", _currObjectId)));

                                if (_locInvoiceInMonitoringXPO != null)
                                {
                                    string targetpath = null;
                                    if (_locInvoiceInMonitoringXPO.DataImport != null && _locInvoiceInMonitoringXPO.Message != "File is already available in server")
                                    {
                                        targetpath = HttpContext.Current.Server.MapPath("~/UploadFile/" + _locInvoiceInMonitoringXPO.DataImport.FileName);
                                        string ext = System.IO.Path.GetExtension(_locInvoiceInMonitoringXPO.DataImport.FileName);
                                        FileStream fileStream = new FileStream(targetpath, FileMode.OpenOrCreate);
                                        _locInvoiceInMonitoringXPO.DataImport.SaveToStream(fileStream);
                                        fileStream.Close();
                                        if (File.Exists(targetpath))
                                        {
                                            _globFunc.ImportDataExcelForInvoiceInMonitoring(_currSession, targetpath, ext, _locInvoiceInMonitoringXPO.ObjectList,
                                                _locInvoiceInMonitoringXPO.Company, _locInvoiceInMonitoringXPO.Workplace);
                                        }
                                        _locInvoiceInMonitoringXPO.DataImport.Clear();
                                        SuccessMessageShow(_locInvoiceInMonitoringXPO.Code + " has been change successfully to Import Data");
                                    }

                                }
                                else
                                {
                                    ErrorMessageShow("Account Reclass Journal Transaction Not Available");
                                }

                            }
                            else
                            {
                                ErrorMessageShow("Account Reclass Journal Transaction Not Available");
                            }
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = InvoiceOutMonitoring " + ex.ToString());
            }
        }

        private void InvoiceInMonitoringGetDataAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;

                if (_objectsToProcess != null)
                {
                    foreach (Object obj in _objectsToProcess)
                    {
                        InvoiceInMonitoring _locInvoiceInMonitoringOS = (InvoiceInMonitoring)_objectSpace.GetObject(obj);

                        if (_locInvoiceInMonitoringOS != null)
                        {
                            if (_locInvoiceInMonitoringOS.Session != null)
                            {
                                _currSession = _locInvoiceInMonitoringOS.Session;
                            }

                            if (_locInvoiceInMonitoringOS.Code != null && _currSession != null)
                            {
                                _currObjectId = _locInvoiceInMonitoringOS.Code;

                                InvoiceInMonitoring _locInvoiceInMonitoringXPO = _currSession.FindObject<InvoiceInMonitoring>
                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                     new BinaryOperator("Code", _currObjectId)));

                                if (_locInvoiceInMonitoringXPO != null)
                                {
                                    SetHeader1(_currSession, _locInvoiceInMonitoringXPO); 
                                    if (_locInvoiceInMonitoringXPO.ObjectImport == ObjectImport.PurchaseOrder)
                                    {
                                        SetContain1(_currSession, _locInvoiceInMonitoringXPO);
                                    }
                                    SuccessMessageShow(_locInvoiceInMonitoringXPO.Code + " has been change successfully to Get Data");

                                }
                                else
                                {
                                    ErrorMessageShow("Account Reclass Journal Transaction Not Available");
                                }

                            }
                            else
                            {
                                ErrorMessageShow("Account Reclass Journal Transaction Not Available");
                            }
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = InvoiceInMonitoring " + ex.ToString());
            }
        }

        //============================================== Code In Here ==============================================

        private void SetHeader1(Session _currSession, InvoiceInMonitoring _locInvoiceInMonitoringXPO)
        {
            try
            {
                DateTime now = DateTime.Now;
                if (_locInvoiceInMonitoringXPO != null)
                {
                    if (_locInvoiceInMonitoringXPO.Company != null && _locInvoiceInMonitoringXPO.Workplace != null)
                    {
                        InvoiceInMonitoringLine _saveDataInvoiceInMonitoringLine = new InvoiceInMonitoringLine(_currSession)
                        {
                            Company = _locInvoiceInMonitoringXPO.Company,
                            Workplace = _locInvoiceInMonitoringXPO.Workplace,
                            StartDate = _locInvoiceInMonitoringXPO.StartDate,
                            EndDate = _locInvoiceInMonitoringXPO.EndDate,
                            Column1 = "FM",
                            Column2 = "KD_JENIS_TRANSAKSI",
                            Column3 = "FG_PENGGANTI",
                            Column4 = "NOMOR_FAKTUR",
                            Column5 = "MASA_PAJAK",
                            Column6 = "TAHUN_PAJAK",
                            Column7 = "TANGGAL_FAKTUR",
                            Column8 = "NPWP",
                            Column9 = "NAMA",
                            Column10 = "ALAMAT_LENGKAP",
                            Column11 = "JUMLAH_DPP",
                            Column12 = "JUMLAH_PPN",
                            Column13 = "JUMLAH_PPNBM",
                            Column14 = "IS_CREDITABLE",
                            InvoiceInMonitoring = _locInvoiceInMonitoringXPO,
                        };
                        _saveDataInvoiceInMonitoringLine.Save();
                        _saveDataInvoiceInMonitoringLine.Session.CommitTransaction();
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = InvoiceOutMonitoring " + ex.ToString());
            }
        }

        private void SetContain1(Session _currSession, InvoiceInMonitoring _locInvoiceInMonitoringXPO)
        {
            try
            {
                DateTime now = DateTime.Now;
                if (_locInvoiceInMonitoringXPO.Company != null && _locInvoiceInMonitoringXPO.Workplace != null)
                {
                    if (_locInvoiceInMonitoringXPO.Company != null)
                    {
                        XPCollection<PurchaseOrder> _locPurchaseOrders = new XPCollection<PurchaseOrder>(_currSession,
                                                                    new GroupOperator(GroupOperatorType.And,
                                                                    new BinaryOperator("DocDate", _locInvoiceInMonitoringXPO.StartDate.AddDays(-1), BinaryOperatorType.Greater),
                                                                    new BinaryOperator("DocDate", _locInvoiceInMonitoringXPO.EndDate.AddDays(1), BinaryOperatorType.Less)));
                        if (_locPurchaseOrders != null && _locPurchaseOrders.Count() > 0)
                        {
                            foreach (PurchaseOrder _locPurchaseOrder in _locPurchaseOrders)
                            {
                                #region Customer
                                if (_locPurchaseOrder.BuyFromVendor != null)
                                {
                                    InvoiceInMonitoringLine _saveDataInvoiceInMonitoringLine = new InvoiceInMonitoringLine(_currSession)
                                    {
                                        Company = _locInvoiceInMonitoringXPO.Company,
                                        Workplace = _locInvoiceInMonitoringXPO.Workplace,
                                        StartDate = _locInvoiceInMonitoringXPO.StartDate,
                                        EndDate = _locInvoiceInMonitoringXPO.EndDate,
                                        Column1 = "FM",
                                        Column2 = "1",
                                        Column3 = "0",
                                        Column4 = _locPurchaseOrder.DJP_No,
                                        Column5 = _locPurchaseOrder.DocDate.Month.ToString(),
                                        Column6 = _locPurchaseOrder.DocDate.Year.ToString(),
                                        Column7 = _locPurchaseOrder.DocDate.ToShortDateString(),
                                        Column8 = _locPurchaseOrder.BuyFromVendor.TaxNo,
                                        Column9 = _locPurchaseOrder.BuyFromVendor.TaxName,
                                        Column10 = _locPurchaseOrder.BuyFromAddress,
                                        Column11 = _locPurchaseOrder.Amount.ToString(),
                                        Column12 = _locPurchaseOrder.TotTaxAmount.ToString(),
                                        Column13 = "0",
                                        Column14 = "",
                                        InvoiceInMonitoring = _locInvoiceInMonitoringXPO,
                                    };
                                    _saveDataInvoiceInMonitoringLine.Save();
                                    _saveDataInvoiceInMonitoringLine.Session.CommitTransaction();
                                }
                                #endregion Customer
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = InvoiceOutMonitoring " + ex.ToString());
            }
        }

        #region Global Method

        private void SuccessMessageShow(string _locMessage)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 4000;
            options.Message = string.Format("{0}", _locMessage);
            options.Type = InformationType.Success;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Success";
            options.Win.Type = WinMessageType.Flyout;
            Application.ShowViewStrategy.ShowMessage(options);
        }

        private void ErrorMessageShow(string _locMessage)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 2000;
            options.Message = string.Format(_locMessage);
            options.Type = InformationType.Warning;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Error";
            options.Win.Type = WinMessageType.Alert;
            Application.ShowViewStrategy.ShowMessage(options);
        }

        #endregion Global Method

        
    }
}
