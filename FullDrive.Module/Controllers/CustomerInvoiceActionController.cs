﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;

using FullDrive.Module.BusinessObjects;
using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.Xpo;
using System.Collections;
using DevExpress.Xpo;
using System.IO;
using System.Web;

namespace FullDrive.Module.Controllers
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class CustomerInvoiceActionController : ViewController
    {
        private ChoiceActionItem _selectionListviewFilter;

        public CustomerInvoiceActionController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.
            #region FilterStatus
            CustomerInvoiceListviewFilterSelectionAction.Items.Clear();
            foreach (object _currApproval in Enum.GetValues(typeof(CustomProcess.Status)))
            {
                EnumDescriptor _ed = new EnumDescriptor(typeof(CustomProcess.Status));
                _selectionListviewFilter = new ChoiceActionItem(_ed.GetCaption(_currApproval), _currApproval);
                CustomerInvoiceListviewFilterSelectionAction.Items.Add(_selectionListviewFilter);
            }
            #endregion FilterStatus
        }

        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
        }

        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }

        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        private void CustomerInvoiceProgressAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;
                Status _locStatus = Status.Open;

                if (_objectsToProcess != null)
                {
                    foreach (Object obj in _objectsToProcess)
                    {
                        CustomerInvoice _locCustomerInvoiceOS = (CustomerInvoice)_objectSpace.GetObject(obj);

                        if (_locCustomerInvoiceOS != null)
                        {
                            if (_locCustomerInvoiceOS.Session != null)
                            {
                                _currSession = _locCustomerInvoiceOS.Session;
                            }

                            if (_locCustomerInvoiceOS.Code != null && _currSession != null)
                            {
                                _currObjectId = _locCustomerInvoiceOS.Code;

                                CustomerInvoice _locCustomerInvoiceXPO = _currSession.FindObject<CustomerInvoice>
                                                                   (new GroupOperator(GroupOperatorType.And,
                                                                    new BinaryOperator("Code", _currObjectId),
                                                                    new GroupOperator(GroupOperatorType.Or,
                                                                    new BinaryOperator("Status", Status.Open),
                                                                    new BinaryOperator("Status", Status.Progress))));

                                if (_locCustomerInvoiceXPO != null)
                                {
                                    SetTotalAmountAndTaxAmountAndDiscountAmount(_currSession, _locCustomerInvoiceXPO);

                                    if (_locCustomerInvoiceXPO.Status == Status.Open)
                                    {
                                        _locStatus = Status.Progress;
                                    }
                                    else
                                    {
                                        _locStatus = _locCustomerInvoiceXPO.Status;
                                    }

                                    _locCustomerInvoiceXPO.Status = _locStatus;
                                    _locCustomerInvoiceXPO.StatusDate = now;
                                    _locCustomerInvoiceXPO.Save();
                                    _locCustomerInvoiceXPO.Session.CommitTransaction();

                                    #region CustomerInvoiceLine
                                    XPCollection<CustomerInvoiceLine> _locCustomerInvoiceLines = new XPCollection<CustomerInvoiceLine>
                                                                                           (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                            new BinaryOperator("CustomerInvoice", _locCustomerInvoiceXPO)));

                                    if (_locCustomerInvoiceLines != null && _locCustomerInvoiceLines.Count > 0)
                                    {
                                        foreach (CustomerInvoiceLine _locCustomerInvoiceLine in _locCustomerInvoiceLines)
                                        {
                                            if (_locCustomerInvoiceLine.Status == Status.Open)
                                            {
                                                _locCustomerInvoiceLine.Status = Status.Progress;
                                                _locCustomerInvoiceLine.StatusDate = now;
                                                _locCustomerInvoiceLine.ActivationPosting = true;
                                                _locCustomerInvoiceLine.Save();
                                                _locCustomerInvoiceLine.Session.CommitTransaction();
                                            }
                                        }
                                    }
                                    #endregion CustomerInvoiceLine

                                    #region CustomerInvoiceFreeItem
                                    XPCollection<CustomerInvoiceFreeItem> _locCustomerInvoiceFreeItems = new XPCollection<CustomerInvoiceFreeItem>
                                                                                           (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                            new BinaryOperator("CustomerInvoice", _locCustomerInvoiceXPO)));

                                    if (_locCustomerInvoiceFreeItems != null && _locCustomerInvoiceFreeItems.Count > 0)
                                    {
                                        foreach (CustomerInvoiceFreeItem _locCustomerInvoiceFreeItem in _locCustomerInvoiceFreeItems)
                                        {
                                            if (_locCustomerInvoiceFreeItem.Status == Status.Open)
                                            {
                                                _locCustomerInvoiceFreeItem.Status = Status.Progress;
                                                _locCustomerInvoiceFreeItem.StatusDate = now;
                                                _locCustomerInvoiceFreeItem.ActivationPosting = true;
                                                _locCustomerInvoiceFreeItem.Save();
                                                _locCustomerInvoiceFreeItem.Session.CommitTransaction();
                                            }
                                        }
                                    }
                                    #endregion CustomerInvoiceFreeItem

                                    SuccessMessageShow(_locCustomerInvoiceXPO.Code + " has been change successfully to Progress ");
                                }
                                else
                                {
                                    ErrorMessageShow("Data Customer Invoice Not Available");
                                }
                            }
                            else
                            {
                                ErrorMessageShow("Data Customer Invoice Not Available");
                            }
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = CustomerInvoice " + ex.ToString());
            }
        }

        private void CustomerInvoiceSetFreeItemAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;

                if (_objectsToProcess != null)
                {
                    foreach (Object obj in _objectsToProcess)
                    {
                        CustomerInvoice _locCustomerInvoiceOS = (CustomerInvoice)_objectSpace.GetObject(obj);

                        if (_locCustomerInvoiceOS != null)
                        {
                            if (_locCustomerInvoiceOS.Session != null)
                            {
                                _currSession = _locCustomerInvoiceOS.Session;
                            }

                            if (_locCustomerInvoiceOS.Code != null && _currSession != null)
                            {
                                _currObjectId = _locCustomerInvoiceOS.Code;

                                CustomerInvoice _locCustomerInvoiceXPO = _currSession.FindObject<CustomerInvoice>
                                                                   (new GroupOperator(GroupOperatorType.And,
                                                                    new BinaryOperator("Code", _currObjectId),
                                                                    new BinaryOperator("Status", Status.Progress)));

                                if (_locCustomerInvoiceXPO != null)
                                {
                                    SetFreeItem(_currSession, _locCustomerInvoiceXPO);
                                    SetTotalFreeItemAmount(_currSession, _locCustomerInvoiceXPO);
                                    SuccessMessageShow(_locCustomerInvoiceXPO.Code + " has been successfully create Free Item  ");
                                }
                                else
                                {
                                    ErrorMessageShow("Data Customer Invoice Not Available");
                                }
                            }
                            else
                            {
                                ErrorMessageShow("Data Customer Invoice Not Available");
                            }
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = CustomerInvoice " + ex.ToString());
            }
        }

        private void CustomerInvoicePostingAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {

                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;

                if (_objectsToProcess != null)
                {
                    foreach (Object obj in _objectsToProcess)
                    {
                        CustomerInvoice _locCustomerInvoiceOS = (CustomerInvoice)_objectSpace.GetObject(obj);

                        if (_locCustomerInvoiceOS != null)
                        {
                            if (_locCustomerInvoiceOS.Session != null)
                            {
                                _currSession = _locCustomerInvoiceOS.Session;
                            }

                            if (_locCustomerInvoiceOS.Code != null && _currSession != null)
                            {
                                _currObjectId = _locCustomerInvoiceOS.Code;

                                CustomerInvoice _locCustomerInvoiceXPO = _currSession.FindObject<CustomerInvoice>
                                                                   (new GroupOperator(GroupOperatorType.And,
                                                                    new BinaryOperator("Code", _currObjectId),
                                                                    new GroupOperator(GroupOperatorType.Or,
                                                                    new BinaryOperator("Status", Status.Progress),
                                                                    new BinaryOperator("Status", Status.Posted))));

                                if (_locCustomerInvoiceXPO != null)
                                {
                                    if (_locCustomerInvoiceXPO.TotAmount > 0)
                                    { 
                                        if (_locCustomerInvoiceXPO.TotAmount == GetSalesInvoiceLineTotalAmount(_currSession, _locCustomerInvoiceXPO))
                                        {
                                            SetInventoryTransferOut(_currSession, _locCustomerInvoiceXPO);
                                            if (CheckJournalSetup(_currSession, _locCustomerInvoiceXPO) == true)
                                            {
                                                SetReceivableJournal(_currSession, _locCustomerInvoiceXPO);
                                            }
                                            SetStatusCustomerInvoiceLine(_currSession, _locCustomerInvoiceXPO);
                                            SetStatusCustomerInvoiceFreeItem(_currSession, _locCustomerInvoiceXPO);
                                            SetNormalQuantity(_currSession, _locCustomerInvoiceXPO);
                                            SetFinalStatusCustomerOrder(_currSession, _locCustomerInvoiceXPO);
                                            SuccessMessageShow(_locCustomerInvoiceXPO.Code + " has been change successfully to Posted");
                                        }
                                    }
                                }
                                else
                                {
                                    ErrorMessageShow("Data Customer Invoice Not Available");
                                }
                            }
                            else
                            {
                                ErrorMessageShow("Data Customer Invoice Not Available");
                            }
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = CustomerInvoice " + ex.ToString());
            }
        }

        private void CustomerInvoiceListviewFilterSelectionAction_Execute(object sender, SingleChoiceActionExecuteEventArgs e)
        {
            try
            {
                if ((View is ListView) & (View.ObjectTypeInfo.Type == typeof(CustomerInvoice)))
                {
                    if ((Status)e.SelectedChoiceActionItem.Data == Status.None)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.None, BinaryOperatorType.NotEqual);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Open)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Open, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Progress)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Progress, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Posted)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Posted, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Lock)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Lock, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Close)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Close, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Cancel)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Cancel, BinaryOperatorType.Equal);
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = CustomerInvoice " + ex.ToString());
            }
        }

        //=========================================== Code In Here ===========================================

        #region GetAmount

        private void SetTotalAmountAndTaxAmountAndDiscountAmount(Session _currSession, CustomerInvoice _locCustomerInvoiceXPO)
        {
            try
            {
                double _locTAmount = 0;
                double _locTUAmount = 0;
                double _locDiscAmountRule2 = 0;
                double _locTxAmount = 0;
                double _locDiscAmountRule1 = 0;
                if (_locCustomerInvoiceXPO != null)
                {
                    XPCollection<CustomerInvoiceLine> _locCustomerInvoiceLines = new XPCollection<CustomerInvoiceLine>
                                                                                (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                new BinaryOperator("CustomerInvoice", _locCustomerInvoiceXPO),
                                                                                new BinaryOperator("Select", true),
                                                                                new GroupOperator(GroupOperatorType.Or,
                                                                                new BinaryOperator("Status", Status.Open),
                                                                                new BinaryOperator("Status", Status.Progress))));
                    if (_locCustomerInvoiceLines != null && _locCustomerInvoiceLines.Count() > 0)
                    {
                        foreach (CustomerInvoiceLine _locCustomerInvoiceLine in _locCustomerInvoiceLines)
                        {
                            _locTAmount = _locTAmount + _locCustomerInvoiceLine.TAmount;
                            _locTUAmount = _locTUAmount + _locCustomerInvoiceLine.TUAmount;
                            _locDiscAmountRule2 = _locDiscAmountRule2 + _locCustomerInvoiceLine.DiscAmount;
                            if (_locCustomerInvoiceLine.Tax != null)
                            {
                                if (_locCustomerInvoiceLine.Tax.TaxNature == TaxNature.Increase)
                                {
                                    _locTxAmount = _locTxAmount + _locCustomerInvoiceLine.TxAmount;
                                }
                                if (_locCustomerInvoiceLine.Tax.TaxNature == TaxNature.Decrease)
                                {
                                    _locTxAmount = _locTxAmount - _locCustomerInvoiceLine.TxAmount;
                                }
                            }
                        }

                        _locDiscAmountRule1 = GetDiscountRule1(_currSession, _locCustomerInvoiceXPO);
                    }

                    _locCustomerInvoiceXPO.TotUnitAmount = _locTUAmount;
                    _locCustomerInvoiceXPO.TotAmount = _locTAmount;
                    _locCustomerInvoiceXPO.AmountDisc = _locDiscAmountRule1;
                    _locCustomerInvoiceXPO.TotDiscAmount = (_locDiscAmountRule2 + _locDiscAmountRule1);
                    _locCustomerInvoiceXPO.TotTaxAmount = _locTxAmount;
                    _locCustomerInvoiceXPO.Amount = _locTAmount - _locDiscAmountRule1;
                    _locCustomerInvoiceXPO.Save();
                    _locCustomerInvoiceXPO.Session.CommitTransaction();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = CustomerInvoice ", ex.ToString());
            }
        }

        #endregion GetAmount

        #region Discount

        private double GetDiscountRule1(Session _currSession, CustomerInvoice _locCustomerInvoiceXPO)
        {
            double _result = 0;
            try
            {
                if (_locCustomerInvoiceXPO != null)
                {
                    if (_locCustomerInvoiceXPO.Company != null && _locCustomerInvoiceXPO.Workplace != null)
                    {
                        DiscountRule _locDiscountRule1 = _currSession.FindObject<DiscountRule>
                                                            (new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("Company", _locCustomerInvoiceXPO.Company),
                                                            new BinaryOperator("Workplace", _locCustomerInvoiceXPO.Workplace),
                                                            new BinaryOperator("PriceGroup", _locCustomerInvoiceXPO.PriceGroup),
                                                            new BinaryOperator("RuleType", RuleType.Rule1),
                                                            new BinaryOperator("Active", true)));

                        #region Rule1

                        //Grand Total Amount Per Invoice
                        if (_locDiscountRule1 != null)
                        {
                            double _locTUAmount = 0;
                            double _locTAmount = 0;
                            double _locTUAmountDiscount = 0;
                            double _locTUAmountDiscount2 = 0;
                            AccountingPeriodicLine _locAcctPerLine = null;

                            #region Gross
                            if (_locDiscountRule1.GrossAmountRule == true)
                            {
                                XPCollection<CustomerInvoiceLine> _locMainCustomerInvoiceLines = new XPCollection<CustomerInvoiceLine>
                                                                                               (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                               new BinaryOperator("CustomerInvoice", _locCustomerInvoiceXPO),
                                                                                               new BinaryOperator("Select", true),
                                                                                               new GroupOperator(GroupOperatorType.Or,
                                                                                               new BinaryOperator("Status", Status.Open))));


                                if (_locMainCustomerInvoiceLines != null && _locMainCustomerInvoiceLines.Count() > 0)
                                {
                                    foreach (CustomerInvoiceLine _locMainCustomerInvoiceLine in _locMainCustomerInvoiceLines)
                                    {
                                        if (_locMainCustomerInvoiceLine.TUAmount > 0)
                                        {
                                            _locTUAmount = _locTUAmount + _locMainCustomerInvoiceLine.TUAmount;
                                            _locMainCustomerInvoiceLine.DiscountChecked = true;
                                            _locMainCustomerInvoiceLine.Save();
                                            _locMainCustomerInvoiceLine.Session.CommitTransaction();
                                        }
                                    }

                                    if (_locTUAmount > 0 && _locTUAmount >= _locDiscountRule1.GrossTotalUAmount)
                                    {
                                        if (_locDiscountRule1.DiscountType == DiscountType.Percentage)
                                        {
                                            _locTUAmountDiscount = _locTUAmount * _locDiscountRule1.Value / 100;
                                        }
                                        if (_locDiscountRule1.DiscountType == DiscountType.Value)
                                        {
                                            _locTUAmountDiscount = _locDiscountRule1.Value;
                                        }
                                    }
                                }
                            }
                            #endregion Gross
                            #region Net
                            if (_locDiscountRule1.NetAmountRule == true)
                            {
                                XPCollection<CustomerInvoiceLine> _locMainCustomerInvoiceLines = new XPCollection<CustomerInvoiceLine>
                                                                                               (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                               new BinaryOperator("CustomerInvoice", _locCustomerInvoiceXPO),
                                                                                               new BinaryOperator("Select", true),
                                                                                               new GroupOperator(GroupOperatorType.Or,
                                                                                               new BinaryOperator("Status", Status.Open))));

                                if (_locMainCustomerInvoiceLines != null && _locMainCustomerInvoiceLines.Count() > 0)
                                {
                                    foreach (CustomerInvoiceLine _locMainCustomerInvoiceLine in _locMainCustomerInvoiceLines)
                                    {
                                        if (_locMainCustomerInvoiceLine.TAmount > 0)
                                        {
                                            _locTAmount = _locTAmount + _locMainCustomerInvoiceLine.TAmount;
                                            _locMainCustomerInvoiceLine.DiscountChecked = true;
                                            _locMainCustomerInvoiceLine.Save();
                                            _locMainCustomerInvoiceLine.Session.CommitTransaction();
                                        }
                                    }

                                    if (_locTAmount > 0 && _locTAmount >= _locDiscountRule1.NetTotalUAmount)
                                    {
                                        if (_locDiscountRule1.DiscountType == DiscountType.Percentage)
                                        {
                                            _locTUAmountDiscount = _locTAmount * _locDiscountRule1.Value / 100;
                                        }
                                        if (_locDiscountRule1.DiscountType == DiscountType.Value)
                                        {
                                            _locTUAmountDiscount = _locDiscountRule1.Value;
                                        }
                                    }
                                }
                            }
                            #endregion Net
                            #region ValBasedAccount
                            if (_locDiscountRule1.ValBasedAccount == true)
                            {
                                if (_locAcctPerLine != null)
                                {
                                    if (_locAcctPerLine.BalHold < _locAcctPerLine.Balance)
                                    {
                                        _locTUAmountDiscount2 = _locAcctPerLine.Balance - _locAcctPerLine.BalHold;
                                        if (_locTUAmountDiscount2 < _locTUAmountDiscount)
                                        {
                                            _locTUAmountDiscount = _locTUAmountDiscount2;
                                        }
                                    }
                                    else
                                    {
                                        _locTUAmountDiscount = 0;
                                    }
                                    _locAcctPerLine.BalHold = _locAcctPerLine.BalHold + _locTUAmountDiscount;
                                    _locAcctPerLine.Save();
                                    _locAcctPerLine.Session.CommitTransaction();

                                    _locTUAmountDiscount = 0;
                                    _locTUAmountDiscount2 = 0;
                                }
                                else
                                {
                                    if (_locDiscountRule1.AccountNo != null)
                                    {
                                        AccountingPeriodic _locAccountPeriodic = _currSession.FindObject<AccountingPeriodic>
                                                                        (new GroupOperator(GroupOperatorType.And,
                                                                        new BinaryOperator("Company", _locCustomerInvoiceXPO.Company),
                                                                        new BinaryOperator("Workplace", _locCustomerInvoiceXPO.Workplace),
                                                                        new BinaryOperator("Active", true)));
                                        if (_locAccountPeriodic != null)
                                        {
                                            AccountingPeriodicLine _locAccountingPeriodicLine = _currSession.FindObject<AccountingPeriodicLine>
                                                                                                (new GroupOperator(GroupOperatorType.And,
                                                                                                new BinaryOperator("AccountingPeriodic", _locAccountPeriodic),
                                                                                                new BinaryOperator("AccountNo", _locDiscountRule1.AccountNo)));
                                            if (_locAccountingPeriodicLine != null)
                                            {
                                                _locAcctPerLine = _locAccountingPeriodicLine;
                                                if (_locAccountingPeriodicLine.BalHold < _locAccountingPeriodicLine.Balance)
                                                {
                                                    _locTUAmountDiscount2 = _locAccountingPeriodicLine.Balance - _locAccountingPeriodicLine.BalHold;
                                                    if (_locTUAmountDiscount2 < _locTUAmountDiscount)
                                                    {
                                                        _locTUAmountDiscount = _locTUAmountDiscount2;
                                                    }
                                                }
                                                else
                                                {
                                                    _locTUAmountDiscount = 0;
                                                }
                                                _locAccountingPeriodicLine.BalHold = _locAccountingPeriodicLine.BalHold + _locTUAmountDiscount;
                                                _locAccountingPeriodicLine.Save();
                                                _locAccountingPeriodicLine.Session.CommitTransaction();
                                            }
                                        }
                                    }
                                }
                            }
                            #endregion ValBasedAccount

                            _locCustomerInvoiceXPO.DiscountRule = _locDiscountRule1;
                            _locCustomerInvoiceXPO.AccountingPeriodicLine = _locAcctPerLine;
                            _locCustomerInvoiceXPO.Save();
                            _locCustomerInvoiceXPO.Session.CommitTransaction();

                            _result = _locTUAmountDiscount;

                        }
                        #endregion Rule1
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = CustomerInvoice ", ex.ToString());
            }
            return _result;
        }

        #endregion Discount

        #region FreeItem

        private void SetFreeItem(Session _currSession, CustomerInvoice _locCustomerInvoiceXPO)
        {
            try
            {
                if (_locCustomerInvoiceXPO != null)
                {
                    XPCollection<FreeItemRule> _locFreeItemRules = new XPCollection<FreeItemRule>
                                                            (_currSession, new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("Company", _locCustomerInvoiceXPO.Company),
                                                            new BinaryOperator("Workplace", _locCustomerInvoiceXPO.Workplace),
                                                            new BinaryOperator("Location", _locCustomerInvoiceXPO.Location),
                                                            new BinaryOperator("PriceGroup", _locCustomerInvoiceXPO.PriceGroup),
                                                            new BinaryOperator("Active", true)));
                    if (_locFreeItemRules != null && _locFreeItemRules.Count() > 0)
                    {
                        foreach (FreeItemRule _locFreeItemRule in _locFreeItemRules)
                        {
                            #region Header
                            if (_locFreeItemRule.FreeItemType1 == FreeItemType1.Header)
                            {
                                if (_locFreeItemRule.OpenAmount == true)
                                {
                                    //1+1+1+1+1
                                    #region Rule1
                                    if (_locFreeItemRule.RuleType == RuleType.Rule1)
                                    {
                                        double _locQtyFI = 0;
                                        double _locTotQtyFI = 0;
                                        double _locFpDqty = 0;
                                        double _locFpQty = 0;
                                        int _locMaxLoop = 0;
                                        int _locTotMaxLoopVal = 0;
                                        #region Amount
                                        if (_locFreeItemRule.NetAmountRule == true)
                                        {
                                            if (_locCustomerInvoiceXPO.TotAmount >= _locFreeItemRule.NetTotalUAmount)
                                            {
                                                _locQtyFI = System.Math.Floor(_locCustomerInvoiceXPO.TotAmount / _locFreeItemRule.NetTotalUAmount);
                                            }
                                        }
                                        if (_locFreeItemRule.GrossAmountRule == true)
                                        {
                                            if (_locCustomerInvoiceXPO.TotUnitAmount >= _locFreeItemRule.GrossTotalUAmount)
                                            {
                                                _locQtyFI = System.Math.Floor(_locCustomerInvoiceXPO.TotUnitAmount / _locFreeItemRule.GrossTotalUAmount);
                                            }
                                        }
                                        #endregion Amount

                                        if (_locFreeItemRule.MaxLoop == 0)
                                        {
                                            _locMaxLoop = (int)_locQtyFI;
                                        }
                                        else
                                        {
                                            if (_locFreeItemRule.MaxLoop > (int)_locQtyFI)
                                            {
                                                _locMaxLoop = (int)_locQtyFI;
                                            }
                                            else
                                            {
                                                _locMaxLoop = _locFreeItemRule.MaxLoop;
                                            }
                                        }

                                        for (int i = 1; i <= _locMaxLoop; i++)
                                        {
                                            _locTotMaxLoopVal = _locTotMaxLoopVal + 1;
                                        }

                                        _locTotQtyFI = _locTotMaxLoopVal * _locFreeItemRule.FpTQty;

                                        if(_locFreeItemRule.FpBegInvLine != null)
                                        {
                                            if ((_locFreeItemRule.FpBegInvLine.QtyAvailable - _locFreeItemRule.FpBegInvLine.QtyOnHand) > 0)
                                            {
                                                if ((_locFreeItemRule.FpBegInvLine.QtyAvailable - _locFreeItemRule.FpBegInvLine.QtyOnHand) < _locTotQtyFI)
                                                {
                                                    _locTotQtyFI = _locFreeItemRule.FpBegInvLine.QtyAvailable - _locFreeItemRule.FpBegInvLine.QtyOnHand;
                                                }

                                                if (_locFreeItemRule.FpItem != null && _locFreeItemRule.FpDUOM != null && _locFreeItemRule.FpUOM != null)
                                                {
                                                    ItemUnitOfMeasure _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                                            (new GroupOperator(GroupOperatorType.And,
                                                                             new BinaryOperator("Item", _locFreeItemRule.FpItem),
                                                                             new BinaryOperator("UOM", _locFreeItemRule.FpUOM),
                                                                             new BinaryOperator("DefaultUOM", _locFreeItemRule.FpDUOM),
                                                                             new BinaryOperator("Active", true)));
                                                    if (_locItemUOM != null)
                                                    {
                                                        if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                                        {
                                                            _locFpDqty = _locTotQtyFI % _locItemUOM.DefaultConversion;
                                                            _locFpQty = System.Math.Floor(_locTotQtyFI / _locItemUOM.DefaultConversion);
                                                        }
                                                        else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                                        {
                                                            _locFpDqty = System.Math.Floor(_locTotQtyFI / _locItemUOM.DefaultConversion);
                                                            _locFpQty = (_locTotQtyFI % _locItemUOM.DefaultConversion) * _locItemUOM.Conversion;
                                                        }
                                                        else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                                        {
                                                            _locFpDqty = _locTotQtyFI;
                                                        }
                                                    }
                                                }
                                                else
                                                {
                                                    _locFpDqty = _locTotQtyFI;
                                                }

                                                CustomerInvoiceFreeItem _saveDataCustomerInvoiceFreeItem = new CustomerInvoiceFreeItem(_currSession)
                                                {
                                                    //Organization
                                                    Company = _locCustomerInvoiceXPO.Company,
                                                    Workplace = _locCustomerInvoiceXPO.Workplace,
                                                    Division = _locCustomerInvoiceXPO.Division,
                                                    Department = _locCustomerInvoiceXPO.Department,
                                                    Section = _locCustomerInvoiceXPO.Section,
                                                    Employee = _locCustomerInvoiceXPO.Employee,
                                                    //Item
                                                    CustomerInvoice = _locCustomerInvoiceXPO,
                                                    Location = _locCustomerInvoiceXPO.Location,
                                                    BegInv = _locFreeItemRule.FpBegInv,
                                                    FpLocationType = _locFreeItemRule.LocationType,
                                                    FpStockType = _locFreeItemRule.StockType,
                                                    FpStockGroup = _locFreeItemRule.FpStockGroup,
                                                    FreeItemRule = _locFreeItemRule,
                                                    FpItem = _locFreeItemRule.FpItem,
                                                    FpDQty = _locFpDqty,
                                                    FpDUOM = _locFreeItemRule.FpDUOM,
                                                    FpQty = _locFpQty,
                                                    FpUOM = _locFreeItemRule.FpUOM,
                                                    FpTQty = _locTotQtyFI,
                                                    BegInvLine = _locFreeItemRule.FpBegInvLine,
                                                    //Amount
                                                    Currency = _locCustomerInvoiceXPO.Currency,
                                                    PriceGroup = _locCustomerInvoiceXPO.PriceGroup,
                                                    PriceLine = GetPriceLine(_currSession, _locCustomerInvoiceXPO, _locFreeItemRule.FpItem),
                                                    UAmount = GetPriceLine(_currSession, _locCustomerInvoiceXPO, _locFreeItemRule.FpItem).Amount2a,
                                                };
                                                _saveDataCustomerInvoiceFreeItem.Save();
                                                _saveDataCustomerInvoiceFreeItem.Session.CommitTransaction();

                                                if (_locFreeItemRule.FpBegInv != null && _locFreeItemRule.FpBegInvLine != null)
                                                {
                                                    _locFreeItemRule.FpBegInvLine.QtyOnHand = _locFreeItemRule.FpBegInvLine.QtyOnHand + _locTotQtyFI;
                                                    _locFreeItemRule.FpBegInvLine.Save();
                                                    _locFreeItemRule.FpBegInvLine.Session.CommitTransaction();
                                                }
                                            }
                                        }                         
                                    }

                                    #endregion Rule1

                                    //1, 2+1, 3+1, 4+1
                                    #region Rule2
                                    if (_locFreeItemRule.RuleType == RuleType.Rule2)
                                    {
                                        double _locQtyFI = 0;
                                        double _locTotQtyFI = 0;
                                        double _locFpDqty = 0;
                                        double _locFpQty = 0;
                                        int _locMaxLoop = 0;

                                        #region Amount
                                        if (_locFreeItemRule.NetAmountRule == true)
                                        {
                                            if (_locCustomerInvoiceXPO.TotAmount >= _locFreeItemRule.NetTotalUAmount)
                                            {
                                                _locQtyFI = System.Math.Floor(_locCustomerInvoiceXPO.TotAmount / _locFreeItemRule.NetTotalUAmount);
                                            }
                                        }
                                        if (_locFreeItemRule.GrossAmountRule == true)
                                        {
                                            if (_locCustomerInvoiceXPO.TotUnitAmount >= _locFreeItemRule.GrossTotalUAmount)
                                            {
                                                _locQtyFI = System.Math.Floor(_locCustomerInvoiceXPO.TotUnitAmount / _locFreeItemRule.GrossTotalUAmount);
                                            }
                                        }
                                        #endregion Amount

                                        if (_locFreeItemRule.MaxLoop == 0)
                                        {
                                            _locMaxLoop = (int)_locQtyFI;
                                        }
                                        else
                                        {
                                            if (_locFreeItemRule.MaxLoop > (int)_locQtyFI)
                                            {
                                                _locMaxLoop = (int)_locQtyFI;
                                            }
                                            else
                                            {
                                                _locMaxLoop = _locFreeItemRule.MaxLoop;
                                            }
                                        }

                                        for (int i = 1; i <= _locMaxLoop; i++)
                                        {
                                            if (i == 1)
                                            {
                                                _locTotQtyFI = _locFreeItemRule.FpTQty;
                                            }
                                            else
                                            {
                                                _locTotQtyFI = _locTotQtyFI + (i + _locFreeItemRule.FpTQty);
                                            }
                                        }

                                        if (_locFreeItemRule.FpBegInvLine != null)
                                        {
                                            if ((_locFreeItemRule.FpBegInvLine.QtyAvailable - _locFreeItemRule.FpBegInvLine.QtyOnHand) > 0)
                                            {
                                                if ((_locFreeItemRule.FpBegInvLine.QtyAvailable - _locFreeItemRule.FpBegInvLine.QtyOnHand) < _locTotQtyFI)
                                                {
                                                    _locTotQtyFI = _locFreeItemRule.FpBegInvLine.QtyAvailable - _locFreeItemRule.FpBegInvLine.QtyOnHand;
                                                }

                                                if (_locFreeItemRule.FpItem != null && _locFreeItemRule.FpDUOM != null && _locFreeItemRule.FpUOM != null)
                                                {
                                                    ItemUnitOfMeasure _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                                            (new GroupOperator(GroupOperatorType.And,
                                                                             new BinaryOperator("Item", _locFreeItemRule.FpItem),
                                                                             new BinaryOperator("UOM", _locFreeItemRule.FpUOM),
                                                                             new BinaryOperator("DefaultUOM", _locFreeItemRule.FpDUOM),
                                                                             new BinaryOperator("Active", true)));
                                                    if (_locItemUOM != null)
                                                    {
                                                        if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                                        {
                                                            _locFpDqty = _locTotQtyFI % _locItemUOM.DefaultConversion;
                                                            _locFpQty = System.Math.Floor(_locTotQtyFI / _locItemUOM.DefaultConversion);
                                                        }
                                                        else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                                        {
                                                            _locFpDqty = System.Math.Floor(_locTotQtyFI / _locItemUOM.DefaultConversion);
                                                            _locFpQty = (_locTotQtyFI % _locItemUOM.DefaultConversion) * _locItemUOM.Conversion;
                                                        }
                                                        else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                                        {
                                                            _locFpDqty = _locTotQtyFI;
                                                        }
                                                    }
                                                }
                                                else
                                                {
                                                    _locFpDqty = _locTotQtyFI;
                                                }

                                                CustomerInvoiceFreeItem _saveDataCustomerInvoiceFreeItem = new CustomerInvoiceFreeItem(_currSession)
                                                {
                                                    //Organization
                                                    Company = _locCustomerInvoiceXPO.Company,
                                                    Workplace = _locCustomerInvoiceXPO.Workplace,
                                                    Division = _locCustomerInvoiceXPO.Division,
                                                    Department = _locCustomerInvoiceXPO.Department,
                                                    Section = _locCustomerInvoiceXPO.Section,
                                                    Employee = _locCustomerInvoiceXPO.Employee,
                                                    //Item
                                                    CustomerInvoice = _locCustomerInvoiceXPO,
                                                    Location = _locCustomerInvoiceXPO.Location,
                                                    BegInv = _locFreeItemRule.FpBegInv,
                                                    FpLocationType = _locFreeItemRule.LocationType,
                                                    FpStockType = _locFreeItemRule.StockType,
                                                    FpStockGroup = _locFreeItemRule.FpStockGroup,
                                                    FreeItemRule = _locFreeItemRule,
                                                    FpItem = _locFreeItemRule.FpItem,
                                                    FpDQty = _locFpDqty,
                                                    FpDUOM = _locFreeItemRule.FpDUOM,
                                                    FpQty = _locFpQty,
                                                    FpUOM = _locFreeItemRule.FpUOM,
                                                    FpTQty = _locTotQtyFI,
                                                    BegInvLine = _locFreeItemRule.FpBegInvLine,
                                                    //Amount
                                                    Currency = _locCustomerInvoiceXPO.Currency,
                                                    PriceGroup = _locCustomerInvoiceXPO.PriceGroup,
                                                    PriceLine = GetPriceLine(_currSession, _locCustomerInvoiceXPO, _locFreeItemRule.FpItem),
                                                    UAmount = GetPriceLine(_currSession, _locCustomerInvoiceXPO, _locFreeItemRule.FpItem).Amount2a,
                                                };
                                                _saveDataCustomerInvoiceFreeItem.Save();
                                                _saveDataCustomerInvoiceFreeItem.Session.CommitTransaction();

                                                if (_locFreeItemRule.FpBegInv != null && _locFreeItemRule.FpBegInvLine != null)
                                                {
                                                    _locFreeItemRule.FpBegInvLine.QtyOnHand = _locFreeItemRule.FpBegInvLine.QtyOnHand + _locTotQtyFI;
                                                    _locFreeItemRule.FpBegInvLine.Save();
                                                    _locFreeItemRule.FpBegInvLine.Session.CommitTransaction();
                                                }
                                            }
                                        }           
                                    }
                                    #endregion Rule2

                                    //2 * 2 * 2 * 2
                                    #region Rule3
                                    if (_locFreeItemRule.RuleType == RuleType.Rule3)
                                    {
                                        double _locQtyFI = 0;
                                        double _locTotQtyFI = 0;
                                        double _locFpDqty = 0;
                                        double _locFpQty = 0;
                                        int _locMaxLoop = 0;

                                        #region Amount
                                        if (_locFreeItemRule.NetAmountRule == true)
                                        {
                                            if (_locCustomerInvoiceXPO.TotAmount >= _locFreeItemRule.NetTotalUAmount)
                                            {
                                                _locQtyFI = System.Math.Floor(_locCustomerInvoiceXPO.TotAmount / _locFreeItemRule.NetTotalUAmount);
                                            }
                                        }
                                        if (_locFreeItemRule.GrossAmountRule == true)
                                        {
                                            if (_locCustomerInvoiceXPO.TotUnitAmount >= _locFreeItemRule.GrossTotalUAmount)
                                            {
                                                _locQtyFI = System.Math.Floor(_locCustomerInvoiceXPO.TotUnitAmount / _locFreeItemRule.GrossTotalUAmount);
                                            }
                                        }
                                        #endregion Amount

                                        if (_locFreeItemRule.MaxLoop == 0)
                                        {
                                            _locMaxLoop = (int)_locQtyFI;
                                        }
                                        else
                                        {
                                            if (_locFreeItemRule.MaxLoop > (int)_locQtyFI)
                                            {
                                                _locMaxLoop = (int)_locQtyFI;
                                            }
                                            else
                                            {
                                                _locMaxLoop = _locFreeItemRule.MaxLoop;
                                            }
                                        }

                                        for (int i = 1; i <= _locMaxLoop; i++)
                                        {
                                            _locTotQtyFI = _locTotQtyFI * _locFreeItemRule.FpTQty;
                                        }

                                        if (_locFreeItemRule.FpBegInvLine != null)
                                        {
                                            if ((_locFreeItemRule.FpBegInvLine.QtyAvailable - _locFreeItemRule.FpBegInvLine.QtyOnHand) > 0)
                                            {
                                                if ((_locFreeItemRule.FpBegInvLine.QtyAvailable - _locFreeItemRule.FpBegInvLine.QtyOnHand) < _locTotQtyFI)
                                                {
                                                    _locTotQtyFI = _locFreeItemRule.FpBegInvLine.QtyAvailable - _locFreeItemRule.FpBegInvLine.QtyOnHand;
                                                }

                                                if (_locFreeItemRule.FpItem != null && _locFreeItemRule.FpDUOM != null && _locFreeItemRule.FpUOM != null)
                                                {
                                                    ItemUnitOfMeasure _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                                            (new GroupOperator(GroupOperatorType.And,
                                                                             new BinaryOperator("Item", _locFreeItemRule.FpItem),
                                                                             new BinaryOperator("UOM", _locFreeItemRule.FpUOM),
                                                                             new BinaryOperator("DefaultUOM", _locFreeItemRule.FpDUOM),
                                                                             new BinaryOperator("Active", true)));
                                                    if (_locItemUOM != null)
                                                    {
                                                        if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                                        {
                                                            _locFpDqty = _locTotQtyFI % _locItemUOM.DefaultConversion;
                                                            _locFpQty = System.Math.Floor(_locTotQtyFI / _locItemUOM.DefaultConversion);
                                                        }
                                                        else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                                        {
                                                            _locFpDqty = System.Math.Floor(_locTotQtyFI / _locItemUOM.DefaultConversion);
                                                            _locFpQty = (_locTotQtyFI % _locItemUOM.DefaultConversion) * _locItemUOM.Conversion;
                                                        }
                                                        else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                                        {
                                                            _locFpDqty = _locTotQtyFI;
                                                        }
                                                    }
                                                }
                                                else
                                                {
                                                    _locFpDqty = _locTotQtyFI;
                                                }

                                                CustomerInvoiceFreeItem _saveDataCustomerInvoiceFreeItem = new CustomerInvoiceFreeItem(_currSession)
                                                {
                                                    //Organization
                                                    Company = _locCustomerInvoiceXPO.Company,
                                                    Workplace = _locCustomerInvoiceXPO.Workplace,
                                                    Division = _locCustomerInvoiceXPO.Division,
                                                    Department = _locCustomerInvoiceXPO.Department,
                                                    Section = _locCustomerInvoiceXPO.Section,
                                                    Employee = _locCustomerInvoiceXPO.Employee,
                                                    //Item
                                                    CustomerInvoice = _locCustomerInvoiceXPO,
                                                    Location = _locCustomerInvoiceXPO.Location,
                                                    BegInv = _locFreeItemRule.FpBegInv,
                                                    FpLocationType = _locFreeItemRule.LocationType,
                                                    FpStockType = _locFreeItemRule.StockType,
                                                    FpStockGroup = _locFreeItemRule.FpStockGroup,
                                                    FreeItemRule = _locFreeItemRule,
                                                    FpItem = _locFreeItemRule.FpItem,
                                                    FpDQty = _locFpDqty,
                                                    FpDUOM = _locFreeItemRule.FpDUOM,
                                                    FpQty = _locFpQty,
                                                    FpUOM = _locFreeItemRule.FpUOM,
                                                    FpTQty = _locTotQtyFI,
                                                    BegInvLine = _locFreeItemRule.FpBegInvLine,
                                                    //Amount
                                                    Currency = _locCustomerInvoiceXPO.Currency,
                                                    PriceGroup = _locCustomerInvoiceXPO.PriceGroup,
                                                    PriceLine = GetPriceLine(_currSession, _locCustomerInvoiceXPO, _locFreeItemRule.FpItem),
                                                    UAmount = GetPriceLine(_currSession, _locCustomerInvoiceXPO, _locFreeItemRule.FpItem).Amount2a,
                                                };
                                                _saveDataCustomerInvoiceFreeItem.Save();
                                                _saveDataCustomerInvoiceFreeItem.Session.CommitTransaction();

                                                if (_locFreeItemRule.FpBegInv != null && _locFreeItemRule.FpBegInvLine != null)
                                                {
                                                    _locFreeItemRule.FpBegInvLine.QtyOnHand = _locFreeItemRule.FpBegInvLine.QtyOnHand + _locTotQtyFI;
                                                    _locFreeItemRule.FpBegInvLine.Save();
                                                    _locFreeItemRule.FpBegInvLine.Session.CommitTransaction();
                                                }
                                            }
                                        }                      
                                    }
                                    #endregion Rule3

                                    //2^(1-1) + 2^(2-1) + 2^(3-1) + 2^(4-1)
                                    #region Rule4
                                    if (_locFreeItemRule.RuleType == RuleType.Rule4)
                                    {
                                        double _locQtyFI = 0;
                                        double _locTotQtyFI = 0;
                                        double _locFpDqty = 0;
                                        double _locFpQty = 0;
                                        int _locMaxLoop = 0;
                                        #region Amount
                                        if (_locFreeItemRule.NetAmountRule == true)
                                        {
                                            if (_locCustomerInvoiceXPO.TotAmount >= _locFreeItemRule.NetTotalUAmount)
                                            {
                                                _locQtyFI = System.Math.Floor(_locCustomerInvoiceXPO.TotAmount / _locFreeItemRule.NetTotalUAmount);
                                            }
                                        }
                                        if (_locFreeItemRule.GrossAmountRule == true)
                                        {
                                            if (_locCustomerInvoiceXPO.TotUnitAmount >= _locFreeItemRule.GrossTotalUAmount)
                                            {
                                                _locQtyFI = System.Math.Floor(_locCustomerInvoiceXPO.TotUnitAmount / _locFreeItemRule.GrossTotalUAmount);
                                            }
                                        }
                                        #endregion Amount


                                        if (_locFreeItemRule.MaxLoop == 0)
                                        {
                                            _locMaxLoop = (int)_locQtyFI;
                                        }
                                        else
                                        {
                                            if (_locFreeItemRule.MaxLoop > (int)_locQtyFI)
                                            {
                                                _locMaxLoop = (int)_locQtyFI;
                                            }
                                            else
                                            {
                                                _locMaxLoop = _locFreeItemRule.MaxLoop;
                                            }
                                        }

                                        for (int i = 1; i <= _locMaxLoop; i++)
                                        {

                                            _locTotQtyFI = _locTotQtyFI + System.Math.Pow(_locFreeItemRule.FpTQty, i - 1);
                                        }

                                        if (_locFreeItemRule.FpBegInvLine != null)
                                        {
                                            if ((_locFreeItemRule.FpBegInvLine.QtyAvailable - _locFreeItemRule.FpBegInvLine.QtyOnHand) > 0)
                                            {
                                                if ((_locFreeItemRule.FpBegInvLine.QtyAvailable - _locFreeItemRule.FpBegInvLine.QtyOnHand) < _locTotQtyFI)
                                                {
                                                    _locTotQtyFI = _locFreeItemRule.FpBegInvLine.QtyAvailable - _locFreeItemRule.FpBegInvLine.QtyOnHand;
                                                }

                                                if (_locFreeItemRule.FpItem != null && _locFreeItemRule.FpDUOM != null && _locFreeItemRule.FpUOM != null)
                                                {
                                                    ItemUnitOfMeasure _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                                            (new GroupOperator(GroupOperatorType.And,
                                                                             new BinaryOperator("Item", _locFreeItemRule.FpItem),
                                                                             new BinaryOperator("UOM", _locFreeItemRule.FpUOM),
                                                                             new BinaryOperator("DefaultUOM", _locFreeItemRule.FpDUOM),
                                                                             new BinaryOperator("Active", true)));
                                                    if (_locItemUOM != null)
                                                    {
                                                        if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                                        {
                                                            _locFpDqty = _locTotQtyFI % _locItemUOM.DefaultConversion;
                                                            _locFpQty = System.Math.Floor(_locTotQtyFI / _locItemUOM.DefaultConversion);
                                                        }
                                                        else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                                        {
                                                            _locFpDqty = System.Math.Floor(_locTotQtyFI / _locItemUOM.DefaultConversion);
                                                            _locFpQty = (_locTotQtyFI % _locItemUOM.DefaultConversion) * _locItemUOM.Conversion;
                                                        }
                                                        else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                                        {
                                                            _locFpDqty = _locTotQtyFI;
                                                        }
                                                    }
                                                }
                                                else
                                                {
                                                    _locFpDqty = _locTotQtyFI;
                                                }

                                                CustomerInvoiceFreeItem _saveDataCustomerInvoiceFreeItem = new CustomerInvoiceFreeItem(_currSession)
                                                {
                                                    //Organization
                                                    Company = _locCustomerInvoiceXPO.Company,
                                                    Workplace = _locCustomerInvoiceXPO.Workplace,
                                                    Division = _locCustomerInvoiceXPO.Division,
                                                    Department = _locCustomerInvoiceXPO.Department,
                                                    Section = _locCustomerInvoiceXPO.Section,
                                                    Employee = _locCustomerInvoiceXPO.Employee,
                                                    //Item
                                                    CustomerInvoice = _locCustomerInvoiceXPO,
                                                    Location = _locCustomerInvoiceXPO.Location,
                                                    BegInv = _locFreeItemRule.FpBegInv,
                                                    FpLocationType = _locFreeItemRule.LocationType,
                                                    FpStockType = _locFreeItemRule.StockType,
                                                    FpStockGroup = _locFreeItemRule.FpStockGroup,
                                                    FreeItemRule = _locFreeItemRule,
                                                    FpItem = _locFreeItemRule.FpItem,
                                                    FpDQty = _locFpDqty,
                                                    FpDUOM = _locFreeItemRule.FpDUOM,
                                                    FpQty = _locFpQty,
                                                    FpUOM = _locFreeItemRule.FpUOM,
                                                    FpTQty = _locTotQtyFI,
                                                    BegInvLine = _locFreeItemRule.FpBegInvLine,
                                                    //Amount
                                                    Currency = _locCustomerInvoiceXPO.Currency,
                                                    PriceGroup = _locCustomerInvoiceXPO.PriceGroup,
                                                    PriceLine = GetPriceLine(_currSession, _locCustomerInvoiceXPO, _locFreeItemRule.FpItem),
                                                    UAmount = GetPriceLine(_currSession, _locCustomerInvoiceXPO, _locFreeItemRule.FpItem).Amount2a,
                                                };
                                                _saveDataCustomerInvoiceFreeItem.Save();
                                                _saveDataCustomerInvoiceFreeItem.Session.CommitTransaction();

                                                if (_locFreeItemRule.FpBegInv != null && _locFreeItemRule.FpBegInvLine != null)
                                                {
                                                    _locFreeItemRule.FpBegInvLine.QtyOnHand = _locFreeItemRule.FpBegInvLine.QtyOnHand + _locTotQtyFI;
                                                    _locFreeItemRule.FpBegInvLine.Save();
                                                    _locFreeItemRule.FpBegInvLine.Session.CommitTransaction();
                                                }
                                            }
                                        }                 
                                    }
                                    #endregion Rule4
                                }
                            }
                            #endregion Header
                            #region Line
                            if (_locFreeItemRule.FreeItemType1 == FreeItemType1.Line)
                            {
                                XPQuery<CustomerInvoiceLine> _customerInvoiceLinesQuery = new XPQuery<CustomerInvoiceLine>(_currSession);

                                var _customerInvoiceLines = from cil in _customerInvoiceLinesQuery
                                                         where (cil.CustomerInvoice == _locCustomerInvoiceXPO
                                                         && cil.Status == Status.Progress
                                                         && cil.Select == true
                                                         )
                                                         group cil by cil.Item into g
                                                         select new { Item = g.Key };

                                if (_customerInvoiceLines != null && _customerInvoiceLines.Count() > 0)
                                {
                                    foreach (var _customerInvoiceLine in _customerInvoiceLines)
                                    {
                                        #region MultiItem=false
                                        if (_locFreeItemRule.MultiItem == false)
                                        {
                                            if (_locFreeItemRule.MpItem == _customerInvoiceLine.Item)
                                            {
                                                //1+1+1+1+1
                                                #region Rule1
                                                if (_locFreeItemRule.RuleType == RuleType.Rule1)
                                                {
                                                    XPCollection<CustomerInvoiceLine> _locCustomerInvoiceLines = new XPCollection<CustomerInvoiceLine>
                                                                                                           (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                           new BinaryOperator("CustomerInvoice", _locCustomerInvoiceXPO),
                                                                                                           new BinaryOperator("Item", _customerInvoiceLine.Item),
                                                                                                           new BinaryOperator("Status", Status.Progress),
                                                                                                           new BinaryOperator("FreeItemChecked", false),
                                                                                                           new BinaryOperator("Select", true)));

                                                    if (_locCustomerInvoiceLines != null && _locCustomerInvoiceLines.Count() > 0)
                                                    {
                                                        #region Foreach
                                                        foreach (CustomerInvoiceLine _locCustomerInvoiceLine in _locCustomerInvoiceLines)
                                                        {
                                                            double _locQtyFI = 0;
                                                            double _locTotQtyFI = 0;
                                                            double _locFpDqty = 0;
                                                            double _locFpQty = 0;
                                                            int _locMaxLoop = 0;
                                                            int _locTotMaxLoopVal = 0;

                                                            if (_locCustomerInvoiceLine.TQty >= _locFreeItemRule.MpTQty)
                                                            {
                                                                #region Amount
                                                                if (_locFreeItemRule.OpenAmount == true)
                                                                {
                                                                    if (_locFreeItemRule.NetAmountRule == true)
                                                                    {
                                                                        if (_locCustomerInvoiceLine.TAmount >= _locFreeItemRule.NetTotalUAmount)
                                                                        {
                                                                            _locQtyFI = System.Math.Floor(_locCustomerInvoiceLine.TAmount / _locFreeItemRule.NetTotalUAmount);
                                                                        }
                                                                    }
                                                                    if (_locFreeItemRule.GrossAmountRule == true)
                                                                    {
                                                                        if (_locCustomerInvoiceLine.TUAmount >= _locFreeItemRule.GrossTotalUAmount)
                                                                        {
                                                                            _locQtyFI = System.Math.Floor(_locCustomerInvoiceLine.TUAmount / _locFreeItemRule.GrossTotalUAmount);
                                                                        }
                                                                    }
                                                                }
                                                                #endregion Amount
                                                                #region Item
                                                                else
                                                                {
                                                                    if (_locCustomerInvoiceLine.TQty >= _locFreeItemRule.MpTQty)
                                                                    {
                                                                        _locQtyFI = System.Math.Floor(_locCustomerInvoiceLine.TQty / _locFreeItemRule.MpTQty);
                                                                    }
                                                                }
                                                                #endregion Item

                                                                if (_locFreeItemRule.MaxLoop == 0)
                                                                {
                                                                    _locMaxLoop = (int)_locQtyFI;
                                                                }
                                                                else
                                                                {
                                                                    if (_locFreeItemRule.MaxLoop > (int)_locQtyFI)
                                                                    {
                                                                        _locMaxLoop = (int)_locQtyFI;
                                                                    }
                                                                    else
                                                                    {
                                                                        _locMaxLoop = _locFreeItemRule.MaxLoop;
                                                                    }
                                                                }

                                                                for (int i = 1; i <= _locMaxLoop; i++)
                                                                {
                                                                    _locTotMaxLoopVal = _locTotMaxLoopVal + 1;
                                                                }

                                                                _locTotQtyFI = _locTotMaxLoopVal * _locFreeItemRule.FpTQty;

                                                                if(_locFreeItemRule.FpBegInvLine != null)
                                                                {
                                                                    if ((_locFreeItemRule.FpBegInvLine.QtyAvailable - _locFreeItemRule.FpBegInvLine.QtyOnHand) > 0)
                                                                    {
                                                                        if ((_locFreeItemRule.FpBegInvLine.QtyAvailable - _locFreeItemRule.FpBegInvLine.QtyOnHand) < _locTotQtyFI)
                                                                        {
                                                                            _locTotQtyFI = _locFreeItemRule.FpBegInvLine.QtyAvailable - _locFreeItemRule.FpBegInvLine.QtyOnHand;
                                                                        }

                                                                        if (_locFreeItemRule.FpItem != null && _locFreeItemRule.FpDUOM != null && _locFreeItemRule.FpUOM != null)
                                                                        {
                                                                            ItemUnitOfMeasure _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                                                     new BinaryOperator("Item", _locFreeItemRule.FpItem),
                                                                                                     new BinaryOperator("UOM", _locFreeItemRule.FpUOM),
                                                                                                     new BinaryOperator("DefaultUOM", _locFreeItemRule.FpDUOM),
                                                                                                     new BinaryOperator("Active", true)));
                                                                            if (_locItemUOM != null)
                                                                            {
                                                                                if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                                                                {
                                                                                    _locFpDqty = _locTotQtyFI % _locItemUOM.DefaultConversion;
                                                                                    _locFpQty = System.Math.Floor(_locTotQtyFI / _locItemUOM.DefaultConversion);
                                                                                }
                                                                                else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                                                                {
                                                                                    _locFpDqty = System.Math.Floor(_locTotQtyFI / _locItemUOM.DefaultConversion);
                                                                                    _locFpQty = (_locTotQtyFI % _locItemUOM.DefaultConversion) * _locItemUOM.Conversion;
                                                                                }
                                                                                else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                                                                {
                                                                                    _locFpDqty = _locTotQtyFI;
                                                                                }
                                                                            }
                                                                        }
                                                                        else
                                                                        {
                                                                            _locFpDqty = _locTotQtyFI;
                                                                        }

                                                                        CustomerInvoiceFreeItem _saveDataCustomerInvoiceFreeItem = new CustomerInvoiceFreeItem(_currSession)
                                                                        {
                                                                            //Organization
                                                                            Company = _locCustomerInvoiceXPO.Company,
                                                                            Workplace = _locCustomerInvoiceXPO.Workplace,
                                                                            Division = _locCustomerInvoiceXPO.Division,
                                                                            Department = _locCustomerInvoiceXPO.Department,
                                                                            Section = _locCustomerInvoiceXPO.Section,
                                                                            Employee = _locCustomerInvoiceXPO.Employee,
                                                                            //Item
                                                                            CustomerInvoice = _locCustomerInvoiceXPO,
                                                                            CustomerInvoiceLine = _locCustomerInvoiceLine,
                                                                            Location = _locCustomerInvoiceXPO.Location,
                                                                            BegInv = _locFreeItemRule.FpBegInv,
                                                                            FpLocationType = _locFreeItemRule.LocationType,
                                                                            FpStockType = _locFreeItemRule.StockType,
                                                                            FpStockGroup = _locFreeItemRule.FpStockGroup,
                                                                            FreeItemRule = _locFreeItemRule,
                                                                            FpItem = _locFreeItemRule.FpItem,
                                                                            FpDQty = _locFpDqty,
                                                                            FpDUOM = _locFreeItemRule.FpDUOM,
                                                                            FpQty = _locFpQty,
                                                                            FpUOM = _locFreeItemRule.FpUOM,
                                                                            FpTQty = _locTotQtyFI,
                                                                            BegInvLine = _locFreeItemRule.FpBegInvLine,
                                                                            //Amount
                                                                            Currency = _locCustomerInvoiceXPO.Currency,
                                                                            PriceGroup = _locCustomerInvoiceXPO.PriceGroup,
                                                                            PriceLine = GetPriceLine(_currSession, _locCustomerInvoiceXPO, _locFreeItemRule.FpItem),
                                                                            UAmount = GetPriceLine(_currSession, _locCustomerInvoiceXPO, _locFreeItemRule.FpItem).Amount2a,
                                                                        };
                                                                        _saveDataCustomerInvoiceFreeItem.Save();
                                                                        _saveDataCustomerInvoiceFreeItem.Session.CommitTransaction();

                                                                        _locCustomerInvoiceLine.FreeItemChecked = true;
                                                                        _locCustomerInvoiceLine.Save();
                                                                        _locCustomerInvoiceLine.Session.CommitTransaction();

                                                                        if (_locFreeItemRule.FpBegInv != null && _locFreeItemRule.FpBegInvLine != null)
                                                                        {
                                                                            _locFreeItemRule.FpBegInvLine.QtyOnHand = _locFreeItemRule.FpBegInvLine.QtyOnHand + _locTotQtyFI;
                                                                            _locFreeItemRule.FpBegInvLine.Save();
                                                                            _locFreeItemRule.FpBegInvLine.Session.CommitTransaction();
                                                                        }
                                                                    }
                                                                }                                                                
                                                            }
                                                        }
                                                        #endregion Foreach
                                                    }
                                                }

                                                #endregion Rule1

                                                //1, 2+1, 3+1, 4+1
                                                #region Rule2
                                                if (_locFreeItemRule.RuleType == RuleType.Rule2)
                                                {
                                                    XPCollection<CustomerInvoiceLine> _locCustomerInvoiceLines = new XPCollection<CustomerInvoiceLine>
                                                                                                           (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                           new BinaryOperator("CustomerInvoice", _locCustomerInvoiceXPO),
                                                                                                           new BinaryOperator("Item", _customerInvoiceLine.Item),
                                                                                                           new BinaryOperator("Status", Status.Progress),
                                                                                                           new BinaryOperator("FreeItemChecked", false),
                                                                                                           new BinaryOperator("Select", true)));

                                                    if (_locCustomerInvoiceLines != null && _locCustomerInvoiceLines.Count() > 0)
                                                    {
                                                        foreach (CustomerInvoiceLine _locCustomerInvoiceLine in _locCustomerInvoiceLines)
                                                        {
                                                            double _locQtyFI = 0;
                                                            double _locTotQtyFI = 0;
                                                            double _locFpDqty = 0;
                                                            double _locFpQty = 0;
                                                            int _locMaxLoop = 0;

                                                            if (_locCustomerInvoiceLine.TQty >= _locFreeItemRule.MpTQty)
                                                            {
                                                                #region Amount
                                                                if (_locFreeItemRule.OpenAmount == true)
                                                                {
                                                                    if (_locFreeItemRule.NetAmountRule == true)
                                                                    {
                                                                        if (_locCustomerInvoiceLine.TAmount >= _locFreeItemRule.NetTotalUAmount)
                                                                        {
                                                                            _locQtyFI = System.Math.Floor(_locCustomerInvoiceLine.TAmount / _locFreeItemRule.NetTotalUAmount);
                                                                        }
                                                                    }
                                                                    if (_locFreeItemRule.GrossAmountRule == true)
                                                                    {
                                                                        if (_locCustomerInvoiceLine.TUAmount >= _locFreeItemRule.GrossTotalUAmount)
                                                                        {
                                                                            _locQtyFI = System.Math.Floor(_locCustomerInvoiceLine.TUAmount / _locFreeItemRule.GrossTotalUAmount);
                                                                        }
                                                                    }
                                                                }
                                                                #endregion Amount
                                                                #region Item
                                                                else
                                                                {
                                                                    if (_locCustomerInvoiceLine.TQty >= _locFreeItemRule.MpTQty)
                                                                    {
                                                                        _locQtyFI = System.Math.Floor(_locCustomerInvoiceLine.TQty / _locFreeItemRule.MpTQty);
                                                                    }
                                                                }
                                                                #endregion Item

                                                                if (_locFreeItemRule.MaxLoop == 0)
                                                                {
                                                                    _locMaxLoop = (int)_locQtyFI;
                                                                }
                                                                else
                                                                {
                                                                    if (_locFreeItemRule.MaxLoop > (int)_locQtyFI)
                                                                    {
                                                                        _locMaxLoop = (int)_locQtyFI;
                                                                    }
                                                                    else
                                                                    {
                                                                        _locMaxLoop = _locFreeItemRule.MaxLoop;
                                                                    }
                                                                }

                                                                for (int i = 1; i <= _locMaxLoop; i++)
                                                                {
                                                                    if (i == 1)
                                                                    {
                                                                        _locTotQtyFI = _locFreeItemRule.FpTQty;
                                                                    }
                                                                    else
                                                                    {
                                                                        _locTotQtyFI = _locTotQtyFI + (i + _locFreeItemRule.FpTQty);
                                                                    }
                                                                }

                                                                if (_locFreeItemRule.FpBegInvLine != null)
                                                                {
                                                                    if ((_locFreeItemRule.FpBegInvLine.QtyAvailable - _locFreeItemRule.FpBegInvLine.QtyOnHand) > 0)
                                                                    {
                                                                        if ((_locFreeItemRule.FpBegInvLine.QtyAvailable - _locFreeItemRule.FpBegInvLine.QtyOnHand) < _locTotQtyFI)
                                                                        {
                                                                            _locTotQtyFI = _locFreeItemRule.FpBegInvLine.QtyAvailable - _locFreeItemRule.FpBegInvLine.QtyOnHand;
                                                                        }

                                                                        if (_locFreeItemRule.FpItem != null && _locFreeItemRule.FpDUOM != null && _locFreeItemRule.FpUOM != null)
                                                                        {
                                                                            ItemUnitOfMeasure _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                                                     new BinaryOperator("Item", _locFreeItemRule.FpItem),
                                                                                                     new BinaryOperator("UOM", _locFreeItemRule.FpUOM),
                                                                                                     new BinaryOperator("DefaultUOM", _locFreeItemRule.FpDUOM),
                                                                                                     new BinaryOperator("Active", true)));
                                                                            if (_locItemUOM != null)
                                                                            {
                                                                                if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                                                                {
                                                                                    _locFpDqty = _locTotQtyFI % _locItemUOM.DefaultConversion;
                                                                                    _locFpQty = System.Math.Floor(_locTotQtyFI / _locItemUOM.DefaultConversion);
                                                                                }
                                                                                else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                                                                {
                                                                                    _locFpDqty = System.Math.Floor(_locTotQtyFI / _locItemUOM.DefaultConversion);
                                                                                    _locFpQty = (_locTotQtyFI % _locItemUOM.DefaultConversion) * _locItemUOM.Conversion;
                                                                                }
                                                                                else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                                                                {
                                                                                    _locFpDqty = _locTotQtyFI;
                                                                                }
                                                                            }
                                                                        }
                                                                        else
                                                                        {
                                                                            _locFpDqty = _locTotQtyFI;
                                                                        }

                                                                        CustomerInvoiceFreeItem _saveDataCustomerInvoiceFreeItem = new CustomerInvoiceFreeItem(_currSession)
                                                                        {
                                                                            //Organization
                                                                            Company = _locCustomerInvoiceXPO.Company,
                                                                            Workplace = _locCustomerInvoiceXPO.Workplace,
                                                                            Division = _locCustomerInvoiceXPO.Division,
                                                                            Department = _locCustomerInvoiceXPO.Department,
                                                                            Section = _locCustomerInvoiceXPO.Section,
                                                                            Employee = _locCustomerInvoiceXPO.Employee,
                                                                            //Item
                                                                            CustomerInvoice = _locCustomerInvoiceXPO,
                                                                            CustomerInvoiceLine = _locCustomerInvoiceLine,
                                                                            Location = _locCustomerInvoiceXPO.Location,
                                                                            BegInv = _locFreeItemRule.FpBegInv,
                                                                            FpLocationType = _locFreeItemRule.LocationType,
                                                                            FpStockType = _locFreeItemRule.StockType,
                                                                            FpStockGroup = _locFreeItemRule.FpStockGroup,
                                                                            FreeItemRule = _locFreeItemRule,
                                                                            FpItem = _locFreeItemRule.FpItem,
                                                                            FpDQty = _locFpDqty,
                                                                            FpDUOM = _locFreeItemRule.FpDUOM,
                                                                            FpQty = _locFpQty,
                                                                            FpUOM = _locFreeItemRule.FpUOM,
                                                                            FpTQty = _locTotQtyFI,
                                                                            BegInvLine = _locFreeItemRule.FpBegInvLine,
                                                                            //Amount
                                                                            Currency = _locCustomerInvoiceXPO.Currency,
                                                                            PriceGroup = _locCustomerInvoiceXPO.PriceGroup,
                                                                            PriceLine = GetPriceLine(_currSession, _locCustomerInvoiceXPO, _locFreeItemRule.FpItem),
                                                                            UAmount = GetPriceLine(_currSession, _locCustomerInvoiceXPO, _locFreeItemRule.FpItem).Amount2a,
                                                                        };
                                                                        _saveDataCustomerInvoiceFreeItem.Save();
                                                                        _saveDataCustomerInvoiceFreeItem.Session.CommitTransaction();

                                                                        _locCustomerInvoiceLine.FreeItemChecked = true;
                                                                        _locCustomerInvoiceLine.Save();
                                                                        _locCustomerInvoiceLine.Session.CommitTransaction();

                                                                        if (_locFreeItemRule.FpBegInv != null && _locFreeItemRule.FpBegInvLine != null)
                                                                        {
                                                                            _locFreeItemRule.FpBegInvLine.QtyOnHand = _locFreeItemRule.FpBegInvLine.QtyOnHand + _locTotQtyFI;
                                                                            _locFreeItemRule.FpBegInvLine.Save();
                                                                            _locFreeItemRule.FpBegInvLine.Session.CommitTransaction();
                                                                        }
                                                                    }
                                                                }                                    
                                                            }
                                                        }
                                                    }
                                                }
                                                #endregion Rule2

                                                //2 * 2 * 2 * 2
                                                #region Rule3
                                                if (_locFreeItemRule.RuleType == RuleType.Rule3)
                                                {
                                                    XPCollection<CustomerInvoiceLine> _locCustomerInvoiceLines = new XPCollection<CustomerInvoiceLine>
                                                                                                           (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                           new BinaryOperator("CustomerInvoice", _locCustomerInvoiceXPO),
                                                                                                           new BinaryOperator("Item", _customerInvoiceLine.Item),
                                                                                                           new BinaryOperator("Status", Status.Progress),
                                                                                                           new BinaryOperator("FreeItemChecked", false),
                                                                                                           new BinaryOperator("Select", true)));

                                                    if (_locCustomerInvoiceLines != null && _locCustomerInvoiceLines.Count() > 0)
                                                    {
                                                        foreach (CustomerInvoiceLine _locCustomerInvoiceLine in _locCustomerInvoiceLines)
                                                        {
                                                            double _locQtyFI = 0;
                                                            double _locTotQtyFI = 0;
                                                            double _locFpDqty = 0;
                                                            double _locFpQty = 0;
                                                            int _locMaxLoop = 0;

                                                            if (_locCustomerInvoiceLine.TQty >= _locFreeItemRule.MpTQty)
                                                            {
                                                                #region Amount
                                                                if (_locFreeItemRule.OpenAmount == true)
                                                                {
                                                                    if (_locFreeItemRule.NetAmountRule == true)
                                                                    {
                                                                        if (_locCustomerInvoiceLine.TAmount >= _locFreeItemRule.NetTotalUAmount)
                                                                        {
                                                                            _locQtyFI = System.Math.Floor(_locCustomerInvoiceLine.TAmount / _locFreeItemRule.NetTotalUAmount);
                                                                        }
                                                                    }
                                                                    if (_locFreeItemRule.GrossAmountRule == true)
                                                                    {
                                                                        if (_locCustomerInvoiceLine.TUAmount >= _locFreeItemRule.GrossTotalUAmount)
                                                                        {
                                                                            _locQtyFI = System.Math.Floor(_locCustomerInvoiceLine.TUAmount / _locFreeItemRule.GrossTotalUAmount);
                                                                        }
                                                                    }
                                                                }
                                                                #endregion Amount
                                                                #region Item
                                                                else
                                                                {
                                                                    if (_locCustomerInvoiceLine.TQty >= _locFreeItemRule.MpTQty)
                                                                    {
                                                                        _locQtyFI = System.Math.Floor(_locCustomerInvoiceLine.TQty / _locFreeItemRule.MpTQty);
                                                                    }
                                                                }
                                                                #endregion Item

                                                                if (_locFreeItemRule.MaxLoop == 0)
                                                                {
                                                                    _locMaxLoop = (int)_locQtyFI;
                                                                }
                                                                else
                                                                {
                                                                    if (_locFreeItemRule.MaxLoop > (int)_locQtyFI)
                                                                    {
                                                                        _locMaxLoop = (int)_locQtyFI;
                                                                    }
                                                                    else
                                                                    {
                                                                        _locMaxLoop = _locFreeItemRule.MaxLoop;
                                                                    }
                                                                }

                                                                for (int i = 1; i <= _locMaxLoop; i++)
                                                                {
                                                                    _locTotQtyFI = _locTotQtyFI * _locFreeItemRule.FpTQty;
                                                                }

                                                                if (_locFreeItemRule.FpBegInvLine != null)
                                                                {
                                                                    if ((_locFreeItemRule.FpBegInvLine.QtyAvailable - _locFreeItemRule.FpBegInvLine.QtyOnHand) > 0)
                                                                    {
                                                                        if ((_locFreeItemRule.FpBegInvLine.QtyAvailable - _locFreeItemRule.FpBegInvLine.QtyOnHand) < _locTotQtyFI)
                                                                        {
                                                                            _locTotQtyFI = _locFreeItemRule.FpBegInvLine.QtyAvailable - _locFreeItemRule.FpBegInvLine.QtyOnHand;
                                                                        }

                                                                        if (_locFreeItemRule.FpItem != null && _locFreeItemRule.FpDUOM != null && _locFreeItemRule.FpUOM != null)
                                                                        {
                                                                            ItemUnitOfMeasure _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                                                     new BinaryOperator("Item", _locFreeItemRule.FpItem),
                                                                                                     new BinaryOperator("UOM", _locFreeItemRule.FpUOM),
                                                                                                     new BinaryOperator("DefaultUOM", _locFreeItemRule.FpDUOM),
                                                                                                     new BinaryOperator("Active", true)));
                                                                            if (_locItemUOM != null)
                                                                            {
                                                                                if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                                                                {
                                                                                    _locFpDqty = _locTotQtyFI % _locItemUOM.DefaultConversion;
                                                                                    _locFpQty = System.Math.Floor(_locTotQtyFI / _locItemUOM.DefaultConversion);
                                                                                }
                                                                                else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                                                                {
                                                                                    _locFpDqty = System.Math.Floor(_locTotQtyFI / _locItemUOM.DefaultConversion);
                                                                                    _locFpQty = (_locTotQtyFI % _locItemUOM.DefaultConversion) * _locItemUOM.Conversion;
                                                                                }
                                                                                else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                                                                {
                                                                                    _locFpDqty = _locTotQtyFI;
                                                                                }
                                                                            }
                                                                        }
                                                                        else
                                                                        {
                                                                            _locFpDqty = _locTotQtyFI;
                                                                        }

                                                                        CustomerInvoiceFreeItem _saveDataCustomerInvoiceFreeItem = new CustomerInvoiceFreeItem(_currSession)
                                                                        {
                                                                            //Organization
                                                                            Company = _locCustomerInvoiceXPO.Company,
                                                                            Workplace = _locCustomerInvoiceXPO.Workplace,
                                                                            Division = _locCustomerInvoiceXPO.Division,
                                                                            Department = _locCustomerInvoiceXPO.Department,
                                                                            Section = _locCustomerInvoiceXPO.Section,
                                                                            Employee = _locCustomerInvoiceXPO.Employee,
                                                                            //Item
                                                                            CustomerInvoice = _locCustomerInvoiceXPO,
                                                                            CustomerInvoiceLine = _locCustomerInvoiceLine,
                                                                            Location = _locCustomerInvoiceXPO.Location,
                                                                            BegInv = _locFreeItemRule.FpBegInv,
                                                                            FpLocationType = _locFreeItemRule.LocationType,
                                                                            FpStockType = _locFreeItemRule.StockType,
                                                                            FpStockGroup = _locFreeItemRule.FpStockGroup,
                                                                            FreeItemRule = _locFreeItemRule,
                                                                            FpItem = _locFreeItemRule.FpItem,
                                                                            FpDQty = _locFpDqty,
                                                                            FpDUOM = _locFreeItemRule.FpDUOM,
                                                                            FpQty = _locFpQty,
                                                                            FpUOM = _locFreeItemRule.FpUOM,
                                                                            FpTQty = _locTotQtyFI,
                                                                            BegInvLine = _locFreeItemRule.FpBegInvLine,
                                                                            //Amount
                                                                            Currency = _locCustomerInvoiceXPO.Currency,
                                                                            PriceGroup = _locCustomerInvoiceXPO.PriceGroup,
                                                                            PriceLine = GetPriceLine(_currSession, _locCustomerInvoiceXPO, _locFreeItemRule.FpItem),
                                                                            UAmount = GetPriceLine(_currSession, _locCustomerInvoiceXPO, _locFreeItemRule.FpItem).Amount2a,
                                                                        };
                                                                        _saveDataCustomerInvoiceFreeItem.Save();
                                                                        _saveDataCustomerInvoiceFreeItem.Session.CommitTransaction();

                                                                        _locCustomerInvoiceLine.FreeItemChecked = true;
                                                                        _locCustomerInvoiceLine.Save();
                                                                        _locCustomerInvoiceLine.Session.CommitTransaction();

                                                                        if (_locFreeItemRule.FpBegInv != null && _locFreeItemRule.FpBegInvLine != null)
                                                                        {
                                                                            _locFreeItemRule.FpBegInvLine.QtyOnHand = _locFreeItemRule.FpBegInvLine.QtyOnHand + _locTotQtyFI;
                                                                            _locFreeItemRule.FpBegInvLine.Save();
                                                                            _locFreeItemRule.FpBegInvLine.Session.CommitTransaction();
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                                #endregion Rule3

                                                //2^(1-1) + 2^(2-1) + 2^(3-1) + 2^(4-1)
                                                #region Rule4
                                                if (_locFreeItemRule.RuleType == RuleType.Rule4)
                                                {
                                                    XPCollection<CustomerInvoiceLine> _locCustomerInvoiceLines = new XPCollection<CustomerInvoiceLine>
                                                                                                           (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                           new BinaryOperator("CustomerInvoice", _locCustomerInvoiceXPO),
                                                                                                           new BinaryOperator("Item", _customerInvoiceLine.Item),
                                                                                                           new BinaryOperator("Status", Status.Progress),
                                                                                                           new BinaryOperator("FreeItemChecked", false),
                                                                                                           new BinaryOperator("Select", true)));

                                                    if (_locCustomerInvoiceLines != null && _locCustomerInvoiceLines.Count() > 0)
                                                    {
                                                        foreach (CustomerInvoiceLine _locCustomerInvoiceLine in _locCustomerInvoiceLines)
                                                        {
                                                            double _locQtyFI = 0;
                                                            double _locTotQtyFI = 0;
                                                            double _locFpDqty = 0;
                                                            double _locFpQty = 0;
                                                            int _locMaxLoop = 0;

                                                            if (_locCustomerInvoiceLine.TQty >= _locFreeItemRule.MpTQty)
                                                            {
                                                                #region Amount
                                                                if (_locFreeItemRule.OpenAmount == true)
                                                                {
                                                                    if (_locFreeItemRule.NetAmountRule == true)
                                                                    {
                                                                        if (_locCustomerInvoiceLine.TAmount >= _locFreeItemRule.NetTotalUAmount)
                                                                        {
                                                                            _locQtyFI = System.Math.Floor(_locCustomerInvoiceLine.TAmount / _locFreeItemRule.NetTotalUAmount);
                                                                        }
                                                                    }
                                                                    if (_locFreeItemRule.GrossAmountRule == true)
                                                                    {
                                                                        if (_locCustomerInvoiceLine.TUAmount >= _locFreeItemRule.GrossTotalUAmount)
                                                                        {
                                                                            _locQtyFI = System.Math.Floor(_locCustomerInvoiceLine.TUAmount / _locFreeItemRule.GrossTotalUAmount);
                                                                        }
                                                                    }
                                                                }
                                                                #endregion Amount
                                                                #region Item
                                                                else
                                                                {
                                                                    if (_locCustomerInvoiceLine.TQty >= _locFreeItemRule.MpTQty)
                                                                    {
                                                                        _locQtyFI = System.Math.Floor(_locCustomerInvoiceLine.TQty / _locFreeItemRule.MpTQty);
                                                                    }
                                                                }
                                                                #endregion Item

                                                                if (_locFreeItemRule.MaxLoop == 0)
                                                                {
                                                                    _locMaxLoop = (int)_locQtyFI;
                                                                }
                                                                else
                                                                {
                                                                    if (_locFreeItemRule.MaxLoop > (int)_locQtyFI)
                                                                    {
                                                                        _locMaxLoop = (int)_locQtyFI;
                                                                    }
                                                                    else
                                                                    {
                                                                        _locMaxLoop = _locFreeItemRule.MaxLoop;
                                                                    }
                                                                }

                                                                for (int i = 1; i <= _locMaxLoop; i++)
                                                                {
                                                                    _locTotQtyFI = _locTotQtyFI + System.Math.Pow(_locFreeItemRule.FpTQty, i - 1);
                                                                }

                                                                if (_locFreeItemRule.FpBegInvLine != null)
                                                                {
                                                                    if ((_locFreeItemRule.FpBegInvLine.QtyAvailable - _locFreeItemRule.FpBegInvLine.QtyOnHand) > 0)
                                                                    {
                                                                        if ((_locFreeItemRule.FpBegInvLine.QtyAvailable - _locFreeItemRule.FpBegInvLine.QtyOnHand) < _locTotQtyFI)
                                                                        {
                                                                            _locTotQtyFI = _locFreeItemRule.FpBegInvLine.QtyAvailable - _locFreeItemRule.FpBegInvLine.QtyOnHand;
                                                                        }

                                                                        if (_locFreeItemRule.FpItem != null && _locFreeItemRule.FpDUOM != null && _locFreeItemRule.FpUOM != null)
                                                                        {
                                                                            ItemUnitOfMeasure _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                                                     new BinaryOperator("Item", _locFreeItemRule.FpItem),
                                                                                                     new BinaryOperator("UOM", _locFreeItemRule.FpUOM),
                                                                                                     new BinaryOperator("DefaultUOM", _locFreeItemRule.FpDUOM),
                                                                                                     new BinaryOperator("Active", true)));
                                                                            if (_locItemUOM != null)
                                                                            {
                                                                                if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                                                                {
                                                                                    _locFpDqty = _locTotQtyFI % _locItemUOM.DefaultConversion;
                                                                                    _locFpQty = System.Math.Floor(_locTotQtyFI / _locItemUOM.DefaultConversion);
                                                                                }
                                                                                else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                                                                {
                                                                                    _locFpDqty = System.Math.Floor(_locTotQtyFI / _locItemUOM.DefaultConversion);
                                                                                    _locFpQty = (_locTotQtyFI % _locItemUOM.DefaultConversion) * _locItemUOM.Conversion;
                                                                                }
                                                                                else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                                                                {
                                                                                    _locFpDqty = _locTotQtyFI;
                                                                                }
                                                                            }
                                                                        }
                                                                        else
                                                                        {
                                                                            _locFpDqty = _locTotQtyFI;
                                                                        }

                                                                        CustomerInvoiceFreeItem _saveDataCustomerInvoiceFreeItem = new CustomerInvoiceFreeItem(_currSession)
                                                                        {
                                                                            //Organization
                                                                            Company = _locCustomerInvoiceXPO.Company,
                                                                            Workplace = _locCustomerInvoiceXPO.Workplace,
                                                                            Division = _locCustomerInvoiceXPO.Division,
                                                                            Department = _locCustomerInvoiceXPO.Department,
                                                                            Section = _locCustomerInvoiceXPO.Section,
                                                                            Employee = _locCustomerInvoiceXPO.Employee,
                                                                            //Item
                                                                            CustomerInvoice = _locCustomerInvoiceXPO,
                                                                            CustomerInvoiceLine = _locCustomerInvoiceLine,
                                                                            Location = _locCustomerInvoiceXPO.Location,
                                                                            BegInv = _locFreeItemRule.FpBegInv,
                                                                            FpLocationType = _locFreeItemRule.LocationType,
                                                                            FpStockType = _locFreeItemRule.StockType,
                                                                            FpStockGroup = _locFreeItemRule.FpStockGroup,
                                                                            FreeItemRule = _locFreeItemRule,
                                                                            FpItem = _locFreeItemRule.FpItem,
                                                                            FpDQty = _locFpDqty,
                                                                            FpDUOM = _locFreeItemRule.FpDUOM,
                                                                            FpQty = _locFpQty,
                                                                            FpUOM = _locFreeItemRule.FpUOM,
                                                                            FpTQty = _locTotQtyFI,
                                                                            BegInvLine = _locFreeItemRule.FpBegInvLine,
                                                                            //Amount
                                                                            Currency = _locCustomerInvoiceXPO.Currency,
                                                                            PriceGroup = _locCustomerInvoiceXPO.PriceGroup,
                                                                            PriceLine = GetPriceLine(_currSession, _locCustomerInvoiceXPO, _locFreeItemRule.FpItem),
                                                                            UAmount = GetPriceLine(_currSession, _locCustomerInvoiceXPO, _locFreeItemRule.FpItem).Amount2a,
                                                                        };
                                                                        _saveDataCustomerInvoiceFreeItem.Save();
                                                                        _saveDataCustomerInvoiceFreeItem.Session.CommitTransaction();

                                                                        _locCustomerInvoiceLine.FreeItemChecked = true;
                                                                        _locCustomerInvoiceLine.Save();
                                                                        _locCustomerInvoiceLine.Session.CommitTransaction();

                                                                        if (_locFreeItemRule.FpBegInv != null && _locFreeItemRule.FpBegInvLine != null)
                                                                        {
                                                                            _locFreeItemRule.FpBegInvLine.QtyOnHand = _locFreeItemRule.FpBegInvLine.QtyOnHand + _locTotQtyFI;
                                                                            _locFreeItemRule.FpBegInvLine.Save();
                                                                            _locFreeItemRule.FpBegInvLine.Session.CommitTransaction();
                                                                        }
                                                                    }
                                                                }                                      
                                                            }
                                                        }
                                                    }
                                                }
                                                #endregion Rule4
                                            }
                                        }
                                        #endregion MultiItem=false
                                        #region MultiItem=true
                                        else
                                        {
                                            CustomerInvoiceLine _locComboCustomerInvoiceLine = _currSession.FindObject<CustomerInvoiceLine>
                                                                                                        (new GroupOperator(GroupOperatorType.And,
                                                                                                        new BinaryOperator("CustomerInvoice", _locCustomerInvoiceXPO),
                                                                                                        new BinaryOperator("Item", _locFreeItemRule.CpItem),
                                                                                                        new BinaryOperator("Status", Status.Progress),
                                                                                                        new BinaryOperator("FreeItemChecked", false),
                                                                                                        new BinaryOperator("Select", true)));

                                            CustomerInvoiceLine _locMainCustomerInvoiceLine = _currSession.FindObject<CustomerInvoiceLine>
                                                                                                       (new GroupOperator(GroupOperatorType.And,
                                                                                                       new BinaryOperator("CustomerInvoice", _locCustomerInvoiceXPO),
                                                                                                       new BinaryOperator("Item", _locFreeItemRule.MpItem),
                                                                                                       new BinaryOperator("Status", Status.Progress),
                                                                                                       new BinaryOperator("FreeItemChecked", false),
                                                                                                       new BinaryOperator("Select", true)));

                                            if (_locMainCustomerInvoiceLine != null && _locComboCustomerInvoiceLine != null)
                                            {
                                                double _locComboQtyCI = 0;
                                                double _locComboTotQtyFI = 0;
                                                double _locComboCpDqty = 0;
                                                double _locComboCpQty = 0;
                                                int _locComboMaxLoop = 0;
                                                int _locComboTotMaxLoopVal = 0;

                                                double _locMainQtyFI = 0;
                                                double _locTotQtyFI = 0;
                                                double _locFpDqty = 0;
                                                double _locFpQty = 0;
                                                int _locMaxLoop = 0;
                                                int _locTotMaxLoopVal = 0;

                                                if (_locMainCustomerInvoiceLine.TQty >= _locFreeItemRule.MpTQty && _locComboCustomerInvoiceLine.TQty >= _locFreeItemRule.CpTQty)
                                                {
                                                    _locMainQtyFI = System.Math.Floor(_locMainCustomerInvoiceLine.TQty / _locFreeItemRule.MpTQty);
                                                    _locComboQtyCI = System.Math.Floor(_locComboCustomerInvoiceLine.TQty / _locFreeItemRule.CpTQty);

                                                    if (_locComboQtyCI < _locMainQtyFI)
                                                    {
                                                        #region ComboPromoItem

                                                        //1+1+1+1+1
                                                        #region Rule1
                                                        if (_locFreeItemRule.RuleType == RuleType.Rule1)
                                                        {

                                                            if (_locFreeItemRule.MaxLoop == 0)
                                                            {
                                                                _locComboMaxLoop = (int)_locComboQtyCI;
                                                            }
                                                            else
                                                            {
                                                                if (_locFreeItemRule.MaxLoop > (int)_locComboQtyCI)
                                                                {
                                                                    _locComboMaxLoop = (int)_locComboQtyCI;
                                                                }
                                                                else
                                                                {
                                                                    _locComboMaxLoop = _locFreeItemRule.MaxLoop;
                                                                }
                                                            }

                                                            for (int i = 1; i <= _locComboMaxLoop; i++)
                                                            {
                                                                _locComboTotMaxLoopVal = _locComboTotMaxLoopVal + 1;
                                                            }

                                                            _locComboTotQtyFI = _locComboTotMaxLoopVal * _locFreeItemRule.FpTQty;

                                                            if(_locFreeItemRule.FpBegInvLine != null)
                                                            {
                                                                if ((_locFreeItemRule.FpBegInvLine.QtyAvailable - _locFreeItemRule.FpBegInvLine.QtyOnHand) > 0)
                                                                {
                                                                    if ((_locFreeItemRule.FpBegInvLine.QtyAvailable - _locFreeItemRule.FpBegInvLine.QtyOnHand) < _locComboTotQtyFI)
                                                                    {
                                                                        _locComboTotQtyFI = _locFreeItemRule.FpBegInvLine.QtyAvailable - _locFreeItemRule.FpBegInvLine.QtyOnHand;
                                                                    }

                                                                    if (_locFreeItemRule.FpItem != null && _locFreeItemRule.FpDUOM != null && _locFreeItemRule.FpUOM != null)
                                                                    {
                                                                        ItemUnitOfMeasure _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                                                                (new GroupOperator(GroupOperatorType.And,
                                                                                                 new BinaryOperator("Item", _locFreeItemRule.FpItem),
                                                                                                 new BinaryOperator("UOM", _locFreeItemRule.FpUOM),
                                                                                                 new BinaryOperator("DefaultUOM", _locFreeItemRule.FpDUOM),
                                                                                                 new BinaryOperator("Active", true)));
                                                                        if (_locItemUOM != null)
                                                                        {
                                                                            if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                                                            {
                                                                                _locComboCpDqty = _locComboTotQtyFI % _locItemUOM.DefaultConversion;
                                                                                _locComboCpQty = System.Math.Floor(_locComboTotQtyFI / _locItemUOM.DefaultConversion);
                                                                            }
                                                                            else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                                                            {
                                                                                _locComboCpDqty = System.Math.Floor(_locComboTotQtyFI / _locItemUOM.DefaultConversion);
                                                                                _locComboCpQty = (_locComboTotQtyFI % _locItemUOM.DefaultConversion) * _locItemUOM.Conversion;
                                                                            }
                                                                            else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                                                            {
                                                                                _locComboCpDqty = _locComboTotQtyFI;
                                                                            }
                                                                        }
                                                                    }
                                                                    else
                                                                    {
                                                                        _locComboCpDqty = _locComboTotQtyFI;
                                                                    }

                                                                    CustomerInvoiceFreeItem _saveDataCustomerInvoiceFreeItem = new CustomerInvoiceFreeItem(_currSession)
                                                                    {
                                                                        //Organization
                                                                        Company = _locCustomerInvoiceXPO.Company,
                                                                        Workplace = _locCustomerInvoiceXPO.Workplace,
                                                                        Division = _locCustomerInvoiceXPO.Division,
                                                                        Department = _locCustomerInvoiceXPO.Department,
                                                                        Section = _locCustomerInvoiceXPO.Section,
                                                                        Employee = _locCustomerInvoiceXPO.Employee,
                                                                        //Item
                                                                        CustomerInvoice = _locCustomerInvoiceXPO,
                                                                        Location = _locFreeItemRule.Location,
                                                                        FreeItemRule = _locFreeItemRule,
                                                                        BegInv = _locFreeItemRule.FpBegInv,
                                                                        FpLocationType = _locFreeItemRule.LocationType,
                                                                        FpStockType = _locFreeItemRule.StockType,
                                                                        FpItem = _locFreeItemRule.FpItem,
                                                                        FpDQty = _locComboCpDqty,
                                                                        FpDUOM = _locFreeItemRule.FpDUOM,
                                                                        FpQty = _locComboCpQty,
                                                                        FpUOM = _locFreeItemRule.FpUOM,
                                                                        FpTQty = _locComboTotQtyFI,
                                                                        FpStockGroup = _locFreeItemRule.FpStockGroup,
                                                                        BegInvLine = _locFreeItemRule.FpBegInvLine,
                                                                        //Amount
                                                                        Currency = _locCustomerInvoiceXPO.Currency,
                                                                        PriceGroup = _locCustomerInvoiceXPO.PriceGroup,
                                                                        PriceLine = GetPriceLine(_currSession, _locCustomerInvoiceXPO, _locFreeItemRule.FpItem),
                                                                        UAmount = GetPriceLine(_currSession, _locCustomerInvoiceXPO, _locFreeItemRule.FpItem).Amount2a,
                                                                    };
                                                                    _saveDataCustomerInvoiceFreeItem.Save();
                                                                    _saveDataCustomerInvoiceFreeItem.Session.CommitTransaction();

                                                                    _locComboCustomerInvoiceLine.FreeItemChecked = true;
                                                                    _locComboCustomerInvoiceLine.Save();
                                                                    _locComboCustomerInvoiceLine.Session.CommitTransaction();

                                                                    _locMainCustomerInvoiceLine.FreeItemChecked = true;
                                                                    _locMainCustomerInvoiceLine.Save();
                                                                    _locMainCustomerInvoiceLine.Session.CommitTransaction();

                                                                    if (_locFreeItemRule.FpBegInv != null && _locFreeItemRule.FpBegInvLine != null)
                                                                    {
                                                                        _locFreeItemRule.FpBegInvLine.QtyOnHand = _locFreeItemRule.FpBegInvLine.QtyOnHand + _locComboTotQtyFI;
                                                                        _locFreeItemRule.FpBegInvLine.Save();
                                                                        _locFreeItemRule.FpBegInvLine.Session.CommitTransaction();
                                                                    }
                                                                }
                                                            }                         
                                                        }

                                                        #endregion Rule1

                                                        //1, 2+1, 3+1, 4+1
                                                        #region Rule2

                                                        if (_locFreeItemRule.RuleType == RuleType.Rule2)
                                                        {
                                                            if (_locFreeItemRule.MaxLoop == 0)
                                                            {
                                                                _locComboMaxLoop = (int)_locComboQtyCI;
                                                            }
                                                            else
                                                            {
                                                                if (_locFreeItemRule.MaxLoop > (int)_locComboQtyCI)
                                                                {
                                                                    _locComboMaxLoop = (int)_locComboQtyCI;
                                                                }
                                                                else
                                                                {
                                                                    _locComboMaxLoop = _locFreeItemRule.MaxLoop;
                                                                }
                                                            }

                                                            for (int i = 1; i <= _locComboMaxLoop; i++)
                                                            {
                                                                if (i == 1)
                                                                {
                                                                    _locComboTotQtyFI = _locFreeItemRule.FpTQty;
                                                                }
                                                                else
                                                                {
                                                                    _locComboTotQtyFI = _locComboTotQtyFI + (i + _locFreeItemRule.FpTQty);
                                                                }
                                                            }

                                                            if (_locFreeItemRule.FpBegInvLine != null)
                                                            {
                                                                if ((_locFreeItemRule.FpBegInvLine.QtyAvailable - _locFreeItemRule.FpBegInvLine.QtyOnHand) > 0)
                                                                {
                                                                    if ((_locFreeItemRule.FpBegInvLine.QtyAvailable - _locFreeItemRule.FpBegInvLine.QtyOnHand) < _locComboTotQtyFI)
                                                                    {
                                                                        _locComboTotQtyFI = _locFreeItemRule.FpBegInvLine.QtyAvailable - _locFreeItemRule.FpBegInvLine.QtyOnHand;
                                                                    }

                                                                    if (_locFreeItemRule.FpItem != null && _locFreeItemRule.FpDUOM != null && _locFreeItemRule.FpUOM != null)
                                                                    {
                                                                        ItemUnitOfMeasure _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                                                                (new GroupOperator(GroupOperatorType.And,
                                                                                                 new BinaryOperator("Item", _locFreeItemRule.FpItem),
                                                                                                 new BinaryOperator("UOM", _locFreeItemRule.FpUOM),
                                                                                                 new BinaryOperator("DefaultUOM", _locFreeItemRule.FpDUOM),
                                                                                                 new BinaryOperator("Active", true)));
                                                                        if (_locItemUOM != null)
                                                                        {
                                                                            if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                                                            {
                                                                                _locComboCpDqty = _locComboTotQtyFI % _locItemUOM.DefaultConversion;
                                                                                _locComboCpQty = System.Math.Floor(_locComboTotQtyFI / _locItemUOM.DefaultConversion);
                                                                            }
                                                                            else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                                                            {
                                                                                _locComboCpDqty = System.Math.Floor(_locComboTotQtyFI / _locItemUOM.DefaultConversion);
                                                                                _locComboCpQty = (_locComboTotQtyFI % _locItemUOM.DefaultConversion) * _locItemUOM.Conversion;
                                                                            }
                                                                            else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                                                            {
                                                                                _locComboCpDqty = _locComboTotQtyFI;
                                                                            }
                                                                        }
                                                                    }
                                                                    else
                                                                    {
                                                                        _locComboCpDqty = _locComboTotQtyFI;
                                                                    }

                                                                    CustomerInvoiceFreeItem _saveDataCustomerInvoiceFreeItem = new CustomerInvoiceFreeItem(_currSession)
                                                                    {
                                                                        //Organization
                                                                        Company = _locCustomerInvoiceXPO.Company,
                                                                        Workplace = _locCustomerInvoiceXPO.Workplace,
                                                                        Division = _locCustomerInvoiceXPO.Division,
                                                                        Department = _locCustomerInvoiceXPO.Department,
                                                                        Section = _locCustomerInvoiceXPO.Section,
                                                                        Employee = _locCustomerInvoiceXPO.Employee,
                                                                        //Item
                                                                        CustomerInvoice = _locCustomerInvoiceXPO,
                                                                        Location = _locFreeItemRule.Location,
                                                                        FreeItemRule = _locFreeItemRule,
                                                                        BegInv = _locFreeItemRule.FpBegInv,
                                                                        FpLocationType = _locFreeItemRule.LocationType,
                                                                        FpStockType = _locFreeItemRule.StockType,
                                                                        FpItem = _locFreeItemRule.FpItem,
                                                                        FpDQty = _locComboCpDqty,
                                                                        FpDUOM = _locFreeItemRule.FpDUOM,
                                                                        FpQty = _locComboCpQty,
                                                                        FpUOM = _locFreeItemRule.FpUOM,
                                                                        FpTQty = _locComboTotQtyFI,
                                                                        FpStockGroup = _locFreeItemRule.FpStockGroup,
                                                                        BegInvLine = _locFreeItemRule.FpBegInvLine,
                                                                        //Amount
                                                                        Currency = _locCustomerInvoiceXPO.Currency,
                                                                        PriceGroup = _locCustomerInvoiceXPO.PriceGroup,
                                                                        PriceLine = GetPriceLine(_currSession, _locCustomerInvoiceXPO, _locFreeItemRule.FpItem),
                                                                        UAmount = GetPriceLine(_currSession, _locCustomerInvoiceXPO, _locFreeItemRule.FpItem).Amount2a,
                                                                    };
                                                                    _saveDataCustomerInvoiceFreeItem.Save();
                                                                    _saveDataCustomerInvoiceFreeItem.Session.CommitTransaction();

                                                                    _locComboCustomerInvoiceLine.FreeItemChecked = true;
                                                                    _locComboCustomerInvoiceLine.Save();
                                                                    _locComboCustomerInvoiceLine.Session.CommitTransaction();

                                                                    _locMainCustomerInvoiceLine.FreeItemChecked = true;
                                                                    _locMainCustomerInvoiceLine.Save();
                                                                    _locMainCustomerInvoiceLine.Session.CommitTransaction();

                                                                    if (_locFreeItemRule.FpBegInv != null && _locFreeItemRule.FpBegInvLine != null)
                                                                    {
                                                                        _locFreeItemRule.FpBegInvLine.QtyOnHand = _locFreeItemRule.FpBegInvLine.QtyOnHand + _locComboTotQtyFI;
                                                                        _locFreeItemRule.FpBegInvLine.Save();
                                                                        _locFreeItemRule.FpBegInvLine.Session.CommitTransaction();
                                                                    }
                                                                }
                                                            }
                                                        }
                                                        #endregion Rule2

                                                        //2 * 2 * 2 * 2
                                                        #region Rule3

                                                        if (_locFreeItemRule.RuleType == RuleType.Rule3)
                                                        {

                                                            if (_locFreeItemRule.MaxLoop == 0)
                                                            {
                                                                _locComboMaxLoop = (int)_locComboQtyCI;
                                                            }
                                                            else
                                                            {
                                                                if (_locFreeItemRule.MaxLoop > (int)_locComboQtyCI)
                                                                {
                                                                    _locComboMaxLoop = (int)_locComboQtyCI;
                                                                }
                                                                else
                                                                {
                                                                    _locComboMaxLoop = _locFreeItemRule.MaxLoop;
                                                                }
                                                            }

                                                            for (int i = 1; i <= _locComboMaxLoop; i++)
                                                            {
                                                                _locComboTotQtyFI = _locComboTotQtyFI * _locFreeItemRule.FpTQty;
                                                            }

                                                            if (_locFreeItemRule.FpBegInvLine != null)
                                                            {
                                                                if ((_locFreeItemRule.FpBegInvLine.QtyAvailable - _locFreeItemRule.FpBegInvLine.QtyOnHand) > 0)
                                                                {
                                                                    if ((_locFreeItemRule.FpBegInvLine.QtyAvailable - _locFreeItemRule.FpBegInvLine.QtyOnHand) < _locComboTotQtyFI)
                                                                    {
                                                                        _locComboTotQtyFI = _locFreeItemRule.FpBegInvLine.QtyAvailable - _locFreeItemRule.FpBegInvLine.QtyOnHand;
                                                                    }

                                                                    if (_locFreeItemRule.FpItem != null && _locFreeItemRule.FpDUOM != null && _locFreeItemRule.FpUOM != null)
                                                                    {
                                                                        ItemUnitOfMeasure _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                                                                (new GroupOperator(GroupOperatorType.And,
                                                                                                 new BinaryOperator("Item", _locFreeItemRule.FpItem),
                                                                                                 new BinaryOperator("UOM", _locFreeItemRule.FpUOM),
                                                                                                 new BinaryOperator("DefaultUOM", _locFreeItemRule.FpDUOM),
                                                                                                 new BinaryOperator("Active", true)));
                                                                        if (_locItemUOM != null)
                                                                        {
                                                                            if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                                                            {
                                                                                _locComboCpDqty = _locComboTotQtyFI % _locItemUOM.DefaultConversion;
                                                                                _locComboCpQty = System.Math.Floor(_locComboTotQtyFI / _locItemUOM.DefaultConversion);
                                                                            }
                                                                            else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                                                            {
                                                                                _locComboCpDqty = System.Math.Floor(_locComboTotQtyFI / _locItemUOM.DefaultConversion);
                                                                                _locComboCpQty = (_locComboTotQtyFI % _locItemUOM.DefaultConversion) * _locItemUOM.Conversion;
                                                                            }
                                                                            else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                                                            {
                                                                                _locComboCpDqty = _locComboTotQtyFI;
                                                                            }
                                                                        }
                                                                    }
                                                                    else
                                                                    {
                                                                        _locComboCpDqty = _locComboTotQtyFI;
                                                                    }

                                                                    CustomerInvoiceFreeItem _saveDataCustomerInvoiceFreeItem = new CustomerInvoiceFreeItem(_currSession)
                                                                    {
                                                                        //Organization
                                                                        Company = _locCustomerInvoiceXPO.Company,
                                                                        Workplace = _locCustomerInvoiceXPO.Workplace,
                                                                        Division = _locCustomerInvoiceXPO.Division,
                                                                        Department = _locCustomerInvoiceXPO.Department,
                                                                        Section = _locCustomerInvoiceXPO.Section,
                                                                        Employee = _locCustomerInvoiceXPO.Employee,
                                                                        //Item
                                                                        CustomerInvoice = _locCustomerInvoiceXPO,
                                                                        Location = _locFreeItemRule.Location,
                                                                        FreeItemRule = _locFreeItemRule,
                                                                        BegInv = _locFreeItemRule.FpBegInv,
                                                                        FpLocationType = _locFreeItemRule.LocationType,
                                                                        FpStockType = _locFreeItemRule.StockType,
                                                                        FpItem = _locFreeItemRule.FpItem,
                                                                        FpDQty = _locComboCpDqty,
                                                                        FpDUOM = _locFreeItemRule.FpDUOM,
                                                                        FpQty = _locComboCpQty,
                                                                        FpUOM = _locFreeItemRule.FpUOM,
                                                                        FpTQty = _locComboTotQtyFI,
                                                                        FpStockGroup = _locFreeItemRule.FpStockGroup,
                                                                        BegInvLine = _locFreeItemRule.FpBegInvLine,
                                                                        //Amount
                                                                        Currency = _locCustomerInvoiceXPO.Currency,
                                                                        PriceGroup = _locCustomerInvoiceXPO.PriceGroup,
                                                                        PriceLine = GetPriceLine(_currSession, _locCustomerInvoiceXPO, _locFreeItemRule.FpItem),
                                                                        UAmount = GetPriceLine(_currSession, _locCustomerInvoiceXPO, _locFreeItemRule.FpItem).Amount2a,
                                                                    };
                                                                    _saveDataCustomerInvoiceFreeItem.Save();
                                                                    _saveDataCustomerInvoiceFreeItem.Session.CommitTransaction();

                                                                    _locComboCustomerInvoiceLine.FreeItemChecked = true;
                                                                    _locComboCustomerInvoiceLine.Save();
                                                                    _locComboCustomerInvoiceLine.Session.CommitTransaction();

                                                                    _locMainCustomerInvoiceLine.FreeItemChecked = true;
                                                                    _locMainCustomerInvoiceLine.Save();
                                                                    _locMainCustomerInvoiceLine.Session.CommitTransaction();

                                                                    if (_locFreeItemRule.FpBegInv != null && _locFreeItemRule.FpBegInvLine != null)
                                                                    {
                                                                        _locFreeItemRule.FpBegInvLine.QtyOnHand = _locFreeItemRule.FpBegInvLine.QtyOnHand + _locComboTotQtyFI;
                                                                        _locFreeItemRule.FpBegInvLine.Save();
                                                                        _locFreeItemRule.FpBegInvLine.Session.CommitTransaction();
                                                                    }
                                                                }
                                                            }                                                           
                                                        }
                                                        #endregion Rule3

                                                        //2^(1-1) + 2^(2-1) + 2^(3-1) + 2^(4-1)
                                                        #region Rule4

                                                        if (_locFreeItemRule.RuleType == RuleType.Rule4)
                                                        {
                                                            if (_locFreeItemRule.MaxLoop == 0)
                                                            {
                                                                _locComboMaxLoop = (int)_locComboQtyCI;
                                                            }
                                                            else
                                                            {
                                                                if (_locFreeItemRule.MaxLoop > (int)_locComboQtyCI)
                                                                {
                                                                    _locComboMaxLoop = (int)_locComboQtyCI;
                                                                }
                                                                else
                                                                {
                                                                    _locComboMaxLoop = _locFreeItemRule.MaxLoop;
                                                                }
                                                            }

                                                            for (int i = 1; i <= _locComboMaxLoop; i++)
                                                            {
                                                                _locComboTotQtyFI = _locComboTotQtyFI + System.Math.Pow(_locFreeItemRule.FpTQty, i - 1);
                                                            }

                                                            if (_locFreeItemRule.FpBegInvLine != null)
                                                            {
                                                                if ((_locFreeItemRule.FpBegInvLine.QtyAvailable - _locFreeItemRule.FpBegInvLine.QtyOnHand) > 0)
                                                                {
                                                                    if ((_locFreeItemRule.FpBegInvLine.QtyAvailable - _locFreeItemRule.FpBegInvLine.QtyOnHand) < _locComboTotQtyFI)
                                                                    {
                                                                        _locComboTotQtyFI = _locFreeItemRule.FpBegInvLine.QtyAvailable - _locFreeItemRule.FpBegInvLine.QtyOnHand;
                                                                    }

                                                                    if (_locFreeItemRule.FpItem != null && _locFreeItemRule.FpDUOM != null && _locFreeItemRule.FpUOM != null)
                                                                    {
                                                                        ItemUnitOfMeasure _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                                                                (new GroupOperator(GroupOperatorType.And,
                                                                                                 new BinaryOperator("Item", _locFreeItemRule.FpItem),
                                                                                                 new BinaryOperator("UOM", _locFreeItemRule.FpUOM),
                                                                                                 new BinaryOperator("DefaultUOM", _locFreeItemRule.FpDUOM),
                                                                                                 new BinaryOperator("Active", true)));
                                                                        if (_locItemUOM != null)
                                                                        {
                                                                            if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                                                            {
                                                                                _locComboCpDqty = _locComboTotQtyFI % _locItemUOM.DefaultConversion;
                                                                                _locComboCpQty = System.Math.Floor(_locComboTotQtyFI / _locItemUOM.DefaultConversion);
                                                                            }
                                                                            else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                                                            {
                                                                                _locComboCpDqty = System.Math.Floor(_locComboTotQtyFI / _locItemUOM.DefaultConversion);
                                                                                _locComboCpQty = (_locComboTotQtyFI % _locItemUOM.DefaultConversion) * _locItemUOM.Conversion;
                                                                            }
                                                                            else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                                                            {
                                                                                _locComboCpDqty = _locComboTotQtyFI;
                                                                            }
                                                                        }
                                                                    }
                                                                    else
                                                                    {
                                                                        _locComboCpDqty = _locComboTotQtyFI;
                                                                    }

                                                                    CustomerInvoiceFreeItem _saveDataCustomerInvoiceFreeItem = new CustomerInvoiceFreeItem(_currSession)
                                                                    {
                                                                        //Organization
                                                                        Company = _locCustomerInvoiceXPO.Company,
                                                                        Workplace = _locCustomerInvoiceXPO.Workplace,
                                                                        Division = _locCustomerInvoiceXPO.Division,
                                                                        Department = _locCustomerInvoiceXPO.Department,
                                                                        Section = _locCustomerInvoiceXPO.Section,
                                                                        Employee = _locCustomerInvoiceXPO.Employee,
                                                                        //Item
                                                                        CustomerInvoice = _locCustomerInvoiceXPO,
                                                                        Location = _locFreeItemRule.Location,
                                                                        FreeItemRule = _locFreeItemRule,
                                                                        BegInv = _locFreeItemRule.FpBegInv,
                                                                        FpLocationType = _locFreeItemRule.LocationType,
                                                                        FpStockType = _locFreeItemRule.StockType,
                                                                        FpItem = _locFreeItemRule.FpItem,
                                                                        FpDQty = _locComboCpDqty,
                                                                        FpDUOM = _locFreeItemRule.FpDUOM,
                                                                        FpQty = _locComboCpQty,
                                                                        FpUOM = _locFreeItemRule.FpUOM,
                                                                        FpTQty = _locComboTotQtyFI,
                                                                        FpStockGroup = _locFreeItemRule.FpStockGroup,
                                                                        BegInvLine = _locFreeItemRule.FpBegInvLine,
                                                                        //Amount
                                                                        Currency = _locCustomerInvoiceXPO.Currency,
                                                                        PriceGroup = _locCustomerInvoiceXPO.PriceGroup,
                                                                        PriceLine = GetPriceLine(_currSession, _locCustomerInvoiceXPO, _locFreeItemRule.FpItem),
                                                                        UAmount = GetPriceLine(_currSession, _locCustomerInvoiceXPO, _locFreeItemRule.FpItem).Amount2a,
                                                                    };
                                                                    _saveDataCustomerInvoiceFreeItem.Save();
                                                                    _saveDataCustomerInvoiceFreeItem.Session.CommitTransaction();

                                                                    _locComboCustomerInvoiceLine.FreeItemChecked = true;
                                                                    _locComboCustomerInvoiceLine.Save();
                                                                    _locComboCustomerInvoiceLine.Session.CommitTransaction();

                                                                    _locMainCustomerInvoiceLine.FreeItemChecked = true;
                                                                    _locMainCustomerInvoiceLine.Save();
                                                                    _locMainCustomerInvoiceLine.Session.CommitTransaction();

                                                                    if (_locFreeItemRule.FpBegInv != null && _locFreeItemRule.FpBegInvLine != null)
                                                                    {
                                                                        _locFreeItemRule.FpBegInvLine.QtyOnHand = _locFreeItemRule.FpBegInvLine.QtyOnHand + _locComboTotQtyFI;
                                                                        _locFreeItemRule.FpBegInvLine.Save();
                                                                        _locFreeItemRule.FpBegInvLine.Session.CommitTransaction();
                                                                    }
                                                                }
                                                            }                                                          
                                                        }
                                                        #endregion Rule4

                                                        #endregion ComboPromoItem
                                                    }
                                                    else
                                                    {
                                                        #region MainPromoItem

                                                        #region Rule1

                                                        if (_locFreeItemRule.RuleType == RuleType.Rule1)
                                                        {

                                                            if (_locFreeItemRule.MaxLoop == 0)
                                                            {
                                                                _locMaxLoop = (int)_locMainQtyFI;
                                                            }
                                                            else
                                                            {
                                                                if (_locFreeItemRule.MaxLoop > (int)_locMainQtyFI)
                                                                {
                                                                    _locMaxLoop = (int)_locMainQtyFI;
                                                                }
                                                                else
                                                                {
                                                                    _locMaxLoop = _locFreeItemRule.MaxLoop;
                                                                }
                                                            }

                                                            for (int i = 1; i <= _locMaxLoop; i++)
                                                            {
                                                                _locTotMaxLoopVal = _locTotMaxLoopVal + 1;
                                                            }

                                                            _locTotQtyFI = _locTotMaxLoopVal * _locFreeItemRule.FpTQty;

                                                            if(_locFreeItemRule.FpBegInvLine != null)
                                                            {
                                                                if ((_locFreeItemRule.FpBegInvLine.QtyAvailable - _locFreeItemRule.FpBegInvLine.QtyOnHand) > 0)
                                                                {
                                                                    if ((_locFreeItemRule.FpBegInvLine.QtyAvailable - _locFreeItemRule.FpBegInvLine.QtyOnHand) < _locTotQtyFI)
                                                                    {
                                                                        _locTotQtyFI = _locFreeItemRule.FpBegInvLine.QtyAvailable - _locFreeItemRule.FpBegInvLine.QtyOnHand;
                                                                    }

                                                                    if (_locFreeItemRule.FpItem != null && _locFreeItemRule.FpDUOM != null && _locFreeItemRule.FpUOM != null)
                                                                    {
                                                                        ItemUnitOfMeasure _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                                                                (new GroupOperator(GroupOperatorType.And,
                                                                                                 new BinaryOperator("Item", _locFreeItemRule.FpItem),
                                                                                                 new BinaryOperator("UOM", _locFreeItemRule.FpUOM),
                                                                                                 new BinaryOperator("DefaultUOM", _locFreeItemRule.FpDUOM),
                                                                                                 new BinaryOperator("Active", true)));
                                                                        if (_locItemUOM != null)
                                                                        {
                                                                            if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                                                            {
                                                                                _locFpDqty = _locTotQtyFI % _locItemUOM.DefaultConversion;
                                                                                _locFpQty = System.Math.Floor(_locTotQtyFI / _locItemUOM.DefaultConversion);
                                                                            }
                                                                            else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                                                            {
                                                                                _locFpDqty = System.Math.Floor(_locTotQtyFI / _locItemUOM.DefaultConversion);
                                                                                _locFpQty = (_locTotQtyFI % _locItemUOM.DefaultConversion) * _locItemUOM.Conversion;
                                                                            }
                                                                            else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                                                            {
                                                                                _locFpDqty = _locTotQtyFI;
                                                                            }
                                                                        }
                                                                    }
                                                                    else
                                                                    {
                                                                        _locFpDqty = _locTotQtyFI;
                                                                    }

                                                                    CustomerInvoiceFreeItem _saveDataCustomerInvoiceFreeItem = new CustomerInvoiceFreeItem(_currSession)
                                                                    {
                                                                        //Organization
                                                                        Company = _locCustomerInvoiceXPO.Company,
                                                                        Workplace = _locCustomerInvoiceXPO.Workplace,
                                                                        Division = _locCustomerInvoiceXPO.Division,
                                                                        Department = _locCustomerInvoiceXPO.Department,
                                                                        Section = _locCustomerInvoiceXPO.Section,
                                                                        Employee = _locCustomerInvoiceXPO.Employee,
                                                                        //Item
                                                                        CustomerInvoice = _locCustomerInvoiceXPO,
                                                                        Location = _locFreeItemRule.Location,
                                                                        FreeItemRule = _locFreeItemRule,
                                                                        FpLocationType = _locFreeItemRule.LocationType,
                                                                        FpStockType = _locFreeItemRule.StockType,
                                                                        BegInv = _locFreeItemRule.FpBegInv,
                                                                        FpItem = _locFreeItemRule.FpItem,
                                                                        FpDQty = _locFpDqty,
                                                                        FpDUOM = _locFreeItemRule.FpDUOM,
                                                                        FpQty = _locFpQty,
                                                                        FpUOM = _locFreeItemRule.FpUOM,
                                                                        FpTQty = _locTotQtyFI,
                                                                        FpStockGroup = _locFreeItemRule.FpStockGroup,
                                                                        BegInvLine = _locFreeItemRule.FpBegInvLine,
                                                                        //Amout
                                                                        Currency = _locCustomerInvoiceXPO.Currency,
                                                                        PriceGroup = _locCustomerInvoiceXPO.PriceGroup,
                                                                        PriceLine = GetPriceLine(_currSession, _locCustomerInvoiceXPO, _locFreeItemRule.FpItem),
                                                                        UAmount = GetPriceLine(_currSession, _locCustomerInvoiceXPO, _locFreeItemRule.FpItem).Amount2a,
                                                                    };
                                                                    _saveDataCustomerInvoiceFreeItem.Save();
                                                                    _saveDataCustomerInvoiceFreeItem.Session.CommitTransaction();

                                                                    _locComboCustomerInvoiceLine.FreeItemChecked = true;
                                                                    _locComboCustomerInvoiceLine.Save();
                                                                    _locComboCustomerInvoiceLine.Session.CommitTransaction();

                                                                    _locMainCustomerInvoiceLine.FreeItemChecked = true;
                                                                    _locMainCustomerInvoiceLine.Save();
                                                                    _locMainCustomerInvoiceLine.Session.CommitTransaction();

                                                                    if (_locFreeItemRule.FpBegInv != null && _locFreeItemRule.FpBegInvLine != null)
                                                                    {
                                                                        _locFreeItemRule.FpBegInvLine.QtyOnHand = _locFreeItemRule.FpBegInvLine.QtyOnHand + _locTotQtyFI;
                                                                        _locFreeItemRule.FpBegInvLine.Save();
                                                                        _locFreeItemRule.FpBegInvLine.Session.CommitTransaction();
                                                                    }
                                                                }
                                                            }            
                                                        }

                                                        #endregion Rule1

                                                        //1, 2+1, 3+1, 4+1
                                                        #region Rule2
                                                        if (_locFreeItemRule.RuleType == RuleType.Rule2)
                                                        {

                                                            if (_locFreeItemRule.MaxLoop == 0)
                                                            {
                                                                _locMaxLoop = (int)_locMainQtyFI;
                                                            }
                                                            else
                                                            {
                                                                if (_locFreeItemRule.MaxLoop > (int)_locMainQtyFI)
                                                                {
                                                                    _locMaxLoop = (int)_locMainQtyFI;
                                                                }
                                                                else
                                                                {
                                                                    _locMaxLoop = _locFreeItemRule.MaxLoop;
                                                                }
                                                            }

                                                            for (int i = 1; i <= _locMaxLoop; i++)
                                                            {

                                                                if (i == 1)
                                                                {
                                                                    _locTotQtyFI = _locFreeItemRule.FpTQty;
                                                                }
                                                                else
                                                                {
                                                                    _locTotQtyFI = _locTotQtyFI + (i + _locFreeItemRule.FpTQty);
                                                                }
                                                            }

                                                            if (_locFreeItemRule.FpBegInvLine != null)
                                                            {
                                                                if ((_locFreeItemRule.FpBegInvLine.QtyAvailable - _locFreeItemRule.FpBegInvLine.QtyOnHand) > 0)
                                                                {
                                                                    if ((_locFreeItemRule.FpBegInvLine.QtyAvailable - _locFreeItemRule.FpBegInvLine.QtyOnHand) < _locTotQtyFI)
                                                                    {
                                                                        _locTotQtyFI = _locFreeItemRule.FpBegInvLine.QtyAvailable - _locFreeItemRule.FpBegInvLine.QtyOnHand;
                                                                    }

                                                                    if (_locFreeItemRule.FpItem != null && _locFreeItemRule.FpDUOM != null && _locFreeItemRule.FpUOM != null)
                                                                    {
                                                                        ItemUnitOfMeasure _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                                                                (new GroupOperator(GroupOperatorType.And,
                                                                                                 new BinaryOperator("Item", _locFreeItemRule.FpItem),
                                                                                                 new BinaryOperator("UOM", _locFreeItemRule.FpUOM),
                                                                                                 new BinaryOperator("DefaultUOM", _locFreeItemRule.FpDUOM),
                                                                                                 new BinaryOperator("Active", true)));
                                                                        if (_locItemUOM != null)
                                                                        {
                                                                            if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                                                            {
                                                                                _locFpDqty = _locTotQtyFI % _locItemUOM.DefaultConversion;
                                                                                _locFpQty = System.Math.Floor(_locTotQtyFI / _locItemUOM.DefaultConversion);
                                                                            }
                                                                            else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                                                            {
                                                                                _locFpDqty = System.Math.Floor(_locTotQtyFI / _locItemUOM.DefaultConversion);
                                                                                _locFpQty = (_locTotQtyFI % _locItemUOM.DefaultConversion) * _locItemUOM.Conversion;
                                                                            }
                                                                            else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                                                            {
                                                                                _locFpDqty = _locTotQtyFI;
                                                                            }
                                                                        }
                                                                    }
                                                                    else
                                                                    {
                                                                        _locFpDqty = _locTotQtyFI;
                                                                    }

                                                                    CustomerInvoiceFreeItem _saveDataCustomerInvoiceFreeItem = new CustomerInvoiceFreeItem(_currSession)
                                                                    {
                                                                        //Organization
                                                                        Company = _locCustomerInvoiceXPO.Company,
                                                                        Workplace = _locCustomerInvoiceXPO.Workplace,
                                                                        Division = _locCustomerInvoiceXPO.Division,
                                                                        Department = _locCustomerInvoiceXPO.Department,
                                                                        Section = _locCustomerInvoiceXPO.Section,
                                                                        Employee = _locCustomerInvoiceXPO.Employee,
                                                                        //Item
                                                                        CustomerInvoice = _locCustomerInvoiceXPO,
                                                                        Location = _locFreeItemRule.Location,
                                                                        FreeItemRule = _locFreeItemRule,
                                                                        FpLocationType = _locFreeItemRule.LocationType,
                                                                        FpStockType = _locFreeItemRule.StockType,
                                                                        BegInv = _locFreeItemRule.FpBegInv,
                                                                        FpItem = _locFreeItemRule.FpItem,
                                                                        FpDQty = _locFpDqty,
                                                                        FpDUOM = _locFreeItemRule.FpDUOM,
                                                                        FpQty = _locFpQty,
                                                                        FpUOM = _locFreeItemRule.FpUOM,
                                                                        FpTQty = _locTotQtyFI,
                                                                        FpStockGroup = _locFreeItemRule.FpStockGroup,
                                                                        BegInvLine = _locFreeItemRule.FpBegInvLine,
                                                                        //Amout
                                                                        Currency = _locCustomerInvoiceXPO.Currency,
                                                                        PriceGroup = _locCustomerInvoiceXPO.PriceGroup,
                                                                        PriceLine = GetPriceLine(_currSession, _locCustomerInvoiceXPO, _locFreeItemRule.FpItem),
                                                                        UAmount = GetPriceLine(_currSession, _locCustomerInvoiceXPO, _locFreeItemRule.FpItem).Amount2a,
                                                                    };
                                                                    _saveDataCustomerInvoiceFreeItem.Save();
                                                                    _saveDataCustomerInvoiceFreeItem.Session.CommitTransaction();

                                                                    _locComboCustomerInvoiceLine.FreeItemChecked = true;
                                                                    _locComboCustomerInvoiceLine.Save();
                                                                    _locComboCustomerInvoiceLine.Session.CommitTransaction();

                                                                    _locMainCustomerInvoiceLine.FreeItemChecked = true;
                                                                    _locMainCustomerInvoiceLine.Save();
                                                                    _locMainCustomerInvoiceLine.Session.CommitTransaction();

                                                                    if (_locFreeItemRule.FpBegInv != null && _locFreeItemRule.FpBegInvLine != null)
                                                                    {
                                                                        _locFreeItemRule.FpBegInvLine.QtyOnHand = _locFreeItemRule.FpBegInvLine.QtyOnHand + _locTotQtyFI;
                                                                        _locFreeItemRule.FpBegInvLine.Save();
                                                                        _locFreeItemRule.FpBegInvLine.Session.CommitTransaction();
                                                                    }
                                                                }
                                                            }                           
                                                        }
                                                        #endregion Rule2

                                                        //2 * 2 * 2 * 2
                                                        #region Rule3
                                                        if (_locFreeItemRule.RuleType == RuleType.Rule3)
                                                        {

                                                            if (_locFreeItemRule.MaxLoop == 0)
                                                            {
                                                                _locMaxLoop = (int)_locMainQtyFI;
                                                            }
                                                            else
                                                            {
                                                                if (_locFreeItemRule.MaxLoop > (int)_locMainQtyFI)
                                                                {
                                                                    _locMaxLoop = (int)_locMainQtyFI;
                                                                }
                                                                else
                                                                {
                                                                    _locMaxLoop = _locFreeItemRule.MaxLoop;
                                                                }
                                                            }

                                                            for (int i = 1; i <= _locMaxLoop; i++)
                                                            {
                                                                _locTotQtyFI = _locTotQtyFI * _locFreeItemRule.FpTQty;
                                                            }

                                                            if (_locFreeItemRule.FpBegInvLine != null)
                                                            {
                                                                if ((_locFreeItemRule.FpBegInvLine.QtyAvailable - _locFreeItemRule.FpBegInvLine.QtyOnHand) > 0)
                                                                {
                                                                    if ((_locFreeItemRule.FpBegInvLine.QtyAvailable - _locFreeItemRule.FpBegInvLine.QtyOnHand) < _locTotQtyFI)
                                                                    {
                                                                        _locTotQtyFI = _locFreeItemRule.FpBegInvLine.QtyAvailable - _locFreeItemRule.FpBegInvLine.QtyOnHand;
                                                                    }

                                                                    if (_locFreeItemRule.FpItem != null && _locFreeItemRule.FpDUOM != null && _locFreeItemRule.FpUOM != null)
                                                                    {
                                                                        ItemUnitOfMeasure _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                                                                (new GroupOperator(GroupOperatorType.And,
                                                                                                 new BinaryOperator("Item", _locFreeItemRule.FpItem),
                                                                                                 new BinaryOperator("UOM", _locFreeItemRule.FpUOM),
                                                                                                 new BinaryOperator("DefaultUOM", _locFreeItemRule.FpDUOM),
                                                                                                 new BinaryOperator("Active", true)));
                                                                        if (_locItemUOM != null)
                                                                        {
                                                                            if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                                                            {
                                                                                _locFpDqty = _locTotQtyFI % _locItemUOM.DefaultConversion;
                                                                                _locFpQty = System.Math.Floor(_locTotQtyFI / _locItemUOM.DefaultConversion);
                                                                            }
                                                                            else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                                                            {
                                                                                _locFpDqty = System.Math.Floor(_locTotQtyFI / _locItemUOM.DefaultConversion);
                                                                                _locFpQty = (_locTotQtyFI % _locItemUOM.DefaultConversion) * _locItemUOM.Conversion;
                                                                            }
                                                                            else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                                                            {
                                                                                _locFpDqty = _locTotQtyFI;
                                                                            }
                                                                        }
                                                                    }
                                                                    else
                                                                    {
                                                                        _locFpDqty = _locTotQtyFI;
                                                                    }

                                                                    CustomerInvoiceFreeItem _saveDataCustomerInvoiceFreeItem = new CustomerInvoiceFreeItem(_currSession)
                                                                    {
                                                                        //Organization
                                                                        Company = _locCustomerInvoiceXPO.Company,
                                                                        Workplace = _locCustomerInvoiceXPO.Workplace,
                                                                        Division = _locCustomerInvoiceXPO.Division,
                                                                        Department = _locCustomerInvoiceXPO.Department,
                                                                        Section = _locCustomerInvoiceXPO.Section,
                                                                        Employee = _locCustomerInvoiceXPO.Employee,
                                                                        //Item
                                                                        CustomerInvoice = _locCustomerInvoiceXPO,
                                                                        Location = _locFreeItemRule.Location,
                                                                        FreeItemRule = _locFreeItemRule,
                                                                        FpLocationType = _locFreeItemRule.LocationType,
                                                                        FpStockType = _locFreeItemRule.StockType,
                                                                        BegInv = _locFreeItemRule.FpBegInv,
                                                                        FpItem = _locFreeItemRule.FpItem,
                                                                        FpDQty = _locFpDqty,
                                                                        FpDUOM = _locFreeItemRule.FpDUOM,
                                                                        FpQty = _locFpQty,
                                                                        FpUOM = _locFreeItemRule.FpUOM,
                                                                        FpTQty = _locTotQtyFI,
                                                                        FpStockGroup = _locFreeItemRule.FpStockGroup,
                                                                        BegInvLine = _locFreeItemRule.FpBegInvLine,
                                                                        //Amout
                                                                        Currency = _locCustomerInvoiceXPO.Currency,
                                                                        PriceGroup = _locCustomerInvoiceXPO.PriceGroup,
                                                                        PriceLine = GetPriceLine(_currSession, _locCustomerInvoiceXPO, _locFreeItemRule.FpItem),
                                                                        UAmount = GetPriceLine(_currSession, _locCustomerInvoiceXPO, _locFreeItemRule.FpItem).Amount2a,
                                                                    };
                                                                    _saveDataCustomerInvoiceFreeItem.Save();
                                                                    _saveDataCustomerInvoiceFreeItem.Session.CommitTransaction();

                                                                    _locComboCustomerInvoiceLine.FreeItemChecked = true;
                                                                    _locComboCustomerInvoiceLine.Save();
                                                                    _locComboCustomerInvoiceLine.Session.CommitTransaction();

                                                                    _locMainCustomerInvoiceLine.FreeItemChecked = true;
                                                                    _locMainCustomerInvoiceLine.Save();
                                                                    _locMainCustomerInvoiceLine.Session.CommitTransaction();

                                                                    if (_locFreeItemRule.FpBegInv != null && _locFreeItemRule.FpBegInvLine != null)
                                                                    {
                                                                        _locFreeItemRule.FpBegInvLine.QtyOnHand = _locFreeItemRule.FpBegInvLine.QtyOnHand + _locTotQtyFI;
                                                                        _locFreeItemRule.FpBegInvLine.Save();
                                                                        _locFreeItemRule.FpBegInvLine.Session.CommitTransaction();
                                                                    }
                                                                }
                                                            }                    
                                                        }
                                                        #endregion Rule3

                                                        //2^(1-1) + 2^(2-1) + 2^(3-1) + 2^(4-1)
                                                        #region Rule4

                                                        if (_locFreeItemRule.RuleType == RuleType.Rule4)
                                                        {
                                                            if (_locFreeItemRule.MaxLoop == 0)
                                                            {
                                                                _locMaxLoop = (int)_locMainQtyFI;
                                                            }
                                                            else
                                                            {
                                                                if (_locFreeItemRule.MaxLoop > (int)_locMainQtyFI)
                                                                {
                                                                    _locMaxLoop = (int)_locMainQtyFI;
                                                                }
                                                                else
                                                                {
                                                                    _locMaxLoop = _locFreeItemRule.MaxLoop;
                                                                }
                                                            }

                                                            for (int i = 1; i <= _locMaxLoop; i++)
                                                            {
                                                                _locTotQtyFI = _locTotQtyFI + System.Math.Pow(_locFreeItemRule.FpTQty, i - 1);
                                                            }

                                                            if (_locFreeItemRule.FpBegInvLine != null)
                                                            {
                                                                if ((_locFreeItemRule.FpBegInvLine.QtyAvailable - _locFreeItemRule.FpBegInvLine.QtyOnHand) > 0)
                                                                {
                                                                    if ((_locFreeItemRule.FpBegInvLine.QtyAvailable - _locFreeItemRule.FpBegInvLine.QtyOnHand) < _locTotQtyFI)
                                                                    {
                                                                        _locTotQtyFI = _locFreeItemRule.FpBegInvLine.QtyAvailable - _locFreeItemRule.FpBegInvLine.QtyOnHand;
                                                                    }

                                                                    if (_locFreeItemRule.FpItem != null && _locFreeItemRule.FpDUOM != null && _locFreeItemRule.FpUOM != null)
                                                                    {
                                                                        ItemUnitOfMeasure _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                                                                (new GroupOperator(GroupOperatorType.And,
                                                                                                 new BinaryOperator("Item", _locFreeItemRule.FpItem),
                                                                                                 new BinaryOperator("UOM", _locFreeItemRule.FpUOM),
                                                                                                 new BinaryOperator("DefaultUOM", _locFreeItemRule.FpDUOM),
                                                                                                 new BinaryOperator("Active", true)));
                                                                        if (_locItemUOM != null)
                                                                        {
                                                                            if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                                                            {
                                                                                _locFpDqty = _locTotQtyFI % _locItemUOM.DefaultConversion;
                                                                                _locFpQty = System.Math.Floor(_locTotQtyFI / _locItemUOM.DefaultConversion);
                                                                            }
                                                                            else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                                                            {
                                                                                _locFpDqty = System.Math.Floor(_locTotQtyFI / _locItemUOM.DefaultConversion);
                                                                                _locFpQty = (_locTotQtyFI % _locItemUOM.DefaultConversion) * _locItemUOM.Conversion;
                                                                            }
                                                                            else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                                                            {
                                                                                _locFpDqty = _locTotQtyFI;
                                                                            }
                                                                        }
                                                                    }
                                                                    else
                                                                    {
                                                                        _locFpDqty = _locTotQtyFI;
                                                                    }

                                                                    CustomerInvoiceFreeItem _saveDataCustomerInvoiceFreeItem = new CustomerInvoiceFreeItem(_currSession)
                                                                    {
                                                                        //Organization
                                                                        Company = _locCustomerInvoiceXPO.Company,
                                                                        Workplace = _locCustomerInvoiceXPO.Workplace,
                                                                        Division = _locCustomerInvoiceXPO.Division,
                                                                        Department = _locCustomerInvoiceXPO.Department,
                                                                        Section = _locCustomerInvoiceXPO.Section,
                                                                        Employee = _locCustomerInvoiceXPO.Employee,
                                                                        //Item
                                                                        CustomerInvoice = _locCustomerInvoiceXPO,
                                                                        Location = _locFreeItemRule.Location,
                                                                        FreeItemRule = _locFreeItemRule,
                                                                        FpLocationType = _locFreeItemRule.LocationType,
                                                                        FpStockType = _locFreeItemRule.StockType,
                                                                        BegInv = _locFreeItemRule.FpBegInv,
                                                                        FpItem = _locFreeItemRule.FpItem,
                                                                        FpDQty = _locFpDqty,
                                                                        FpDUOM = _locFreeItemRule.FpDUOM,
                                                                        FpQty = _locFpQty,
                                                                        FpUOM = _locFreeItemRule.FpUOM,
                                                                        FpTQty = _locTotQtyFI,
                                                                        FpStockGroup = _locFreeItemRule.FpStockGroup,
                                                                        BegInvLine = _locFreeItemRule.FpBegInvLine,
                                                                        //Amout
                                                                        Currency = _locCustomerInvoiceXPO.Currency,
                                                                        PriceGroup = _locCustomerInvoiceXPO.PriceGroup,
                                                                        PriceLine = GetPriceLine(_currSession, _locCustomerInvoiceXPO, _locFreeItemRule.FpItem),
                                                                        UAmount = GetPriceLine(_currSession, _locCustomerInvoiceXPO, _locFreeItemRule.FpItem).Amount2a,
                                                                    };
                                                                    _saveDataCustomerInvoiceFreeItem.Save();
                                                                    _saveDataCustomerInvoiceFreeItem.Session.CommitTransaction();

                                                                    _locComboCustomerInvoiceLine.FreeItemChecked = true;
                                                                    _locComboCustomerInvoiceLine.Save();
                                                                    _locComboCustomerInvoiceLine.Session.CommitTransaction();

                                                                    _locMainCustomerInvoiceLine.FreeItemChecked = true;
                                                                    _locMainCustomerInvoiceLine.Save();
                                                                    _locMainCustomerInvoiceLine.Session.CommitTransaction();

                                                                    if (_locFreeItemRule.FpBegInv != null && _locFreeItemRule.FpBegInvLine != null)
                                                                    {
                                                                        _locFreeItemRule.FpBegInvLine.QtyOnHand = _locFreeItemRule.FpBegInvLine.QtyOnHand + _locTotQtyFI;
                                                                        _locFreeItemRule.FpBegInvLine.Save();
                                                                        _locFreeItemRule.FpBegInvLine.Session.CommitTransaction();
                                                                    }
                                                                }
                                                            }                                    
                                                        }
                                                        #endregion Rule4

                                                        #endregion MainPromoItem
                                                    }
                                                }
                                            }
                                        }
                                        #endregion MultiItem=true

                                    }
                                }
                            }
                            #endregion Line
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = CustomerInvoice ", ex.ToString());
            }
        }

        private void SetTotalFreeItemAmount(Session _currSession, CustomerInvoice _locCustomerInvoiceXPO)
        {
            try
            {
                double _locTUAmount = 0;
                double _locGetTUAmount = 0;
                if (_locCustomerInvoiceXPO != null)
                {
                    XPCollection<CustomerInvoiceFreeItem> _locCustomerInvoiceFreeItems = new XPCollection<CustomerInvoiceFreeItem>
                                                                                (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                new BinaryOperator("CustomerInvoice", _locCustomerInvoiceXPO),
                                                                                new GroupOperator(GroupOperatorType.Or,
                                                                                new BinaryOperator("Status", Status.Open),
                                                                                new BinaryOperator("Status", Status.Progress))));
                    if (_locCustomerInvoiceFreeItems != null && _locCustomerInvoiceFreeItems.Count() > 0)
                    {
                        foreach (CustomerInvoiceFreeItem _locCustomerInvoiceFreeItem in _locCustomerInvoiceFreeItems)
                        {
                            _locGetTUAmount = GetTotalUnitAmount(_currSession, _locCustomerInvoiceFreeItem.FpTQty, _locCustomerInvoiceFreeItem.UAmount);
                            _locTUAmount = _locTUAmount + _locGetTUAmount;

                            _locCustomerInvoiceFreeItem.TUAmount = _locGetTUAmount;
                            _locCustomerInvoiceFreeItem.Save();
                            _locCustomerInvoiceFreeItem.Session.CommitTransaction();
                        }
                        _locCustomerInvoiceXPO.TotFIAmount = _locTUAmount;
                        _locCustomerInvoiceXPO.Save();
                        _locCustomerInvoiceXPO.Session.CommitTransaction();
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = CustomerInvoice ", ex.ToString());
            }
        }

        private PriceLine GetPriceLine(Session _currSession, CustomerInvoice _locCustomerInvoiceXPO, Item _locFpItem)
        {
            PriceLine _result = null;
            try
            {
                if (_locCustomerInvoiceXPO.Company != null && _locCustomerInvoiceXPO.Workplace != null && _locFpItem != null && _locCustomerInvoiceXPO.PriceGroup != null)
                {
                    PriceLine _locPriceLine = _currSession.FindObject<PriceLine>(new GroupOperator(GroupOperatorType.And,
                                                                            new BinaryOperator("Company", _locCustomerInvoiceXPO.Company),
                                                                            new BinaryOperator("Workplace", _locCustomerInvoiceXPO.Workplace),
                                                                            new BinaryOperator("Item", _locFpItem),
                                                                            new BinaryOperator("PriceGroup", _locCustomerInvoiceXPO.PriceGroup),
                                                                            new BinaryOperator("Active", true)));
                    if (_locPriceLine != null)
                    {
                        _result = _locPriceLine;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = CustomerInvoice " + ex.ToString());
            }
            return _result;
        }

        private double GetTotalUnitAmount(Session _currSession, double _locFpTQty, double _locUAmount)
        {
            double _result = 0;
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();

                if (_locFpTQty >= 0 & _locUAmount >= 0)
                {
                    if (_globFunc.GetRoundingList(_currSession, ObjectList.CustomerInvoiceFreeItem, FieldName.TUAmount) == true)
                    {
                        _result = _globFunc.GetRoundUp(_currSession, (_locFpTQty * _locUAmount), ObjectList.CustomerInvoiceFreeItem, FieldName.TUAmount);
                    }
                    else
                    {
                        _result = _locFpTQty * _locUAmount;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = CustomerInvoice " + ex.ToString());
            }
            return _result;
        }

        #endregion FreeItem

        #region Posting

        private double GetSalesInvoiceLineTotalAmount(Session _currSession, CustomerInvoice _locCustomerInvoiceXPO)
        {
            double _return = 0;
            try
            {
                if (_locCustomerInvoiceXPO != null)
                {
                    double _locTAmount = 0;
                    XPCollection<CustomerInvoiceLine> _locCustomerInvoiceLines = new XPCollection<CustomerInvoiceLine>
                                                                        (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                        new BinaryOperator("CustomerInvoice", _locCustomerInvoiceXPO),
                                                                        new BinaryOperator("Select", true),
                                                                        new GroupOperator(GroupOperatorType.Or,
                                                                        new BinaryOperator("Status", Status.Progress),
                                                                        new BinaryOperator("Status", Status.Posted))));

                    if (_locCustomerInvoiceLines != null && _locCustomerInvoiceLines.Count > 0)
                    {
                        foreach (CustomerInvoiceLine _locCustomerInvoiceLine in _locCustomerInvoiceLines)
                        {
                            _locTAmount = _locTAmount + _locCustomerInvoiceLine.TAmount;
                        }
                        _return = _locTAmount;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("BusinessObject = CustomerInvoice" + ex.ToString());
            }
            return _return;
        }

        private void SetInventoryTransferOut(Session _currSession, CustomerInvoice _locCustomerInvoiceXPO)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                DateTime now = DateTime.Now;
                string _locDocCode = null;
                string _locSignCodeITOL = null;

                DocumentType _locDocumentTypeXPO = _currSession.FindObject<DocumentType>
                                                  (new GroupOperator(GroupOperatorType.And,
                                                   new BinaryOperator("TransferType", DirectionType.External),
                                                   new BinaryOperator("InventoryMovingType", InventoryMovingType.Deliver),
                                                   new BinaryOperator("ObjectList", ObjectList.InventoryTransferOut),
                                                   new BinaryOperator("DocumentRule", DocumentRule.Customer),
                                                   new BinaryOperator("Active", true),
                                                   new BinaryOperator("Default", true)));

                if (_locDocumentTypeXPO != null)
                {
                    _locDocCode = _globFunc.GetDocumentNumberingUnlockOptimisticRecordBasedOrganization(_currSession.DataLayer, _locDocumentTypeXPO, _locCustomerInvoiceXPO.Company, _locCustomerInvoiceXPO.Workplace);
                    //_locDocCode = _globFunc.GetDocumentNumberingUnlockOptimisticRecord(_currSession.DataLayer, _locDocumentTypeXPO);

                    if (_locDocCode != null)
                    {
                        #region SaveInventoryTransferOut
                        InventoryTransferOut _saveDataITO = new InventoryTransferOut(_currSession)
                        {
                            TransferType = _locDocumentTypeXPO.TransferType,
                            InventoryMovingType = _locDocumentTypeXPO.InventoryMovingType,
                            ObjectList = _locDocumentTypeXPO.ObjectList,
                            DocumentRule = _locDocumentTypeXPO.DocumentRule,
                            DocumentType = _locDocumentTypeXPO,
                            DocNo = _locDocCode,
                            SalesToCustomer = _locCustomerInvoiceXPO.SalesToCustomer,
                            SalesToContact = _locCustomerInvoiceXPO.SalesToContact,
                            SalesToCountry = _locCustomerInvoiceXPO.SalesToCountry,
                            SalesToCity = _locCustomerInvoiceXPO.SalesToCity,
                            SalesToAddress = _locCustomerInvoiceXPO.SalesToAddress,
                            EstimatedDate = _locCustomerInvoiceXPO.DocDate,
                            CustomerInvoice = _locCustomerInvoiceXPO,
                            Company = _locCustomerInvoiceXPO.Company,
                            Workplace = _locCustomerInvoiceXPO.Workplace,
                            BegInv = _locCustomerInvoiceXPO.BegInv,
                            Location = _locCustomerInvoiceXPO.Location,
                            DocDate = _locCustomerInvoiceXPO.DocDate,
                            JournalMonth = _locCustomerInvoiceXPO.JournalMonth,
                            JournalYear = _locCustomerInvoiceXPO.JournalYear,
                        };
                        _saveDataITO.Save();
                        _saveDataITO.Session.CommitTransaction();
                        #endregion SaveInventoryTransferOut

                        #region SaveInventoryTransferOutLineByCIL
                        InventoryTransferOut _locInventoryTransferOut = _currSession.FindObject<InventoryTransferOut>
                                                (new GroupOperator(GroupOperatorType.And,
                                                 new BinaryOperator("DocNo", _locDocCode),
                                                 new BinaryOperator("CustomerInvoice", _locCustomerInvoiceXPO)));

                        if (_locInventoryTransferOut != null)
                        {
                            XPCollection<CustomerInvoiceLine> _locCustomerInvoiceLines = new XPCollection<CustomerInvoiceLine>
                                       (_currSession, new GroupOperator(GroupOperatorType.And,
                                        new BinaryOperator("CustomerInvoice", _locCustomerInvoiceXPO),
                                        new BinaryOperator("Status", Status.Progress)));

                            if (_locCustomerInvoiceLines != null && _locCustomerInvoiceLines.Count > 0)
                            {
                                foreach (CustomerInvoiceLine _locCustomerInvoiceLine in _locCustomerInvoiceLines)
                                {
                                    _locSignCodeITOL = _globFunc.GetNumberingSignUnlockOptimisticRecordBasedOrganization(_currSession.DataLayer, ObjectList.InventoryTransferOutLine, _locCustomerInvoiceXPO.Company, _locCustomerInvoiceXPO.Workplace);
                                    //_locSignCodeITOL = _globFunc.GetNumberingSignUnlockOptimisticRecord(_currSession.DataLayer, ObjectList.InventoryTransferOutLine);

                                    if (_locSignCodeITOL != null)
                                    {
                                        InventoryTransferOutLine _saveDataInventoryTransferOutLine = new InventoryTransferOutLine(_currSession)
                                        {
                                            SignCode = _locSignCodeITOL,
                                            Select = true,
                                            SalesType = _locCustomerInvoiceLine.SalesType,
                                            Item = _locCustomerInvoiceLine.Item,
                                            Brand = _locCustomerInvoiceLine.Brand,
                                            Description = _locCustomerInvoiceLine.Description,
                                            DQty = _locCustomerInvoiceLine.DQty,
                                            DUOM = _locCustomerInvoiceLine.DUOM,
                                            Qty = _locCustomerInvoiceLine.Qty,
                                            UOM = _locCustomerInvoiceLine.UOM,
                                            TQty = _locCustomerInvoiceLine.TQty,
                                            StockType = _locCustomerInvoiceLine.StockType,
                                            StockGroup = StockGroup.Normal,
                                            EstimatedDate = _locInventoryTransferOut.EstimatedDate,
                                            Company = _locInventoryTransferOut.Company,
                                            Workplace = _locInventoryTransferOut.Workplace,
                                            InventoryTransferOut = _locInventoryTransferOut,
                                            CustomerInvoiceLine = _locCustomerInvoiceLine,
                                            BegInv = _locInventoryTransferOut.BegInv,
                                            BegInvLine = _locCustomerInvoiceLine.BegInvLine,
                                            Location = _locCustomerInvoiceLine.Location,
                                            DocDate = _locInventoryTransferOut.DocDate,
                                            JournalMonth = _locInventoryTransferOut.JournalMonth,
                                            JournalYear = _locInventoryTransferOut.JournalYear,
                                        };
                                        _saveDataInventoryTransferOutLine.Save();
                                        _saveDataInventoryTransferOutLine.Session.CommitTransaction();
                                        
                                    }
                                }
                            }
                        }
                        #endregion SaveInventoryTransferOutLineByCIL

                        #region SaveInventoryTransferOutLineByCIFI
                        InventoryTransferOut _locInventoryTransferOut2 = _currSession.FindObject<InventoryTransferOut>
                                                (new GroupOperator(GroupOperatorType.And,
                                                 new BinaryOperator("DocNo", _locDocCode),
                                                 new BinaryOperator("CustomerInvoice", _locCustomerInvoiceXPO)));

                        if (_locInventoryTransferOut2 != null)
                        {
                            XPCollection<CustomerInvoiceFreeItem> _locCustomerInvoiceFreeItems = new XPCollection<CustomerInvoiceFreeItem>
                                       (_currSession, new GroupOperator(GroupOperatorType.And,
                                        new BinaryOperator("CustomerInvoice", _locCustomerInvoiceXPO),
                                        new GroupOperator(GroupOperatorType.Or,
                                        new BinaryOperator("Status", Status.Open),
                                        new BinaryOperator("Status", Status.Progress))));

                            if (_locCustomerInvoiceFreeItems != null && _locCustomerInvoiceFreeItems.Count > 0)
                            {
                                foreach (CustomerInvoiceFreeItem _locCustomerInvoiceFreeItem in _locCustomerInvoiceFreeItems)
                                {
                                    _locSignCodeITOL = _globFunc.GetNumberingSignUnlockOptimisticRecordBasedOrganization(_currSession.DataLayer, ObjectList.InventoryTransferOutLine, _locCustomerInvoiceXPO.Company, _locCustomerInvoiceXPO.Workplace);
                                    //_locSignCodeITOL = _globFunc.GetNumberingSignUnlockOptimisticRecord(_currSession.DataLayer, ObjectList.InventoryTransferOutLine);

                                    if (_locSignCodeITOL != null)
                                    {
                                        InventoryTransferOutLine _saveDataInventoryTransferOutLine = new InventoryTransferOutLine(_currSession)
                                        {
                                            SignCode = _locSignCodeITOL,
                                            Select = true,
                                            SalesType = OrderType.Item,
                                            Item = _locCustomerInvoiceFreeItem.FpItem,
                                            DQty = _locCustomerInvoiceFreeItem.FpDQty,
                                            DUOM = _locCustomerInvoiceFreeItem.FpDUOM,
                                            Qty = _locCustomerInvoiceFreeItem.FpQty,
                                            UOM = _locCustomerInvoiceFreeItem.FpUOM,
                                            TQty = _locCustomerInvoiceFreeItem.FpTQty,
                                            StockType = _locCustomerInvoiceFreeItem.FpStockType,
                                            StockGroup = StockGroup.FreeItem,
                                            EstimatedDate = _locInventoryTransferOut.EstimatedDate,
                                            Company = _locInventoryTransferOut.Company,
                                            Workplace = _locInventoryTransferOut.Workplace,
                                            InventoryTransferOut = _locInventoryTransferOut,
                                            BegInv = _locInventoryTransferOut.BegInv,
                                            BegInvLine = _locCustomerInvoiceFreeItem.BegInvLine,
                                            Location = _locCustomerInvoiceFreeItem.Location,
                                            DocDate = _locInventoryTransferOut.DocDate,
                                            JournalMonth = _locInventoryTransferOut.JournalMonth,
                                            JournalYear = _locInventoryTransferOut.JournalYear,
                                        };
                                        _saveDataInventoryTransferOutLine.Save();
                                        _saveDataInventoryTransferOutLine.Session.CommitTransaction();

                                    }
                                }
                            }
                        }

                        #endregion SaveInventoryTransferOutLineByCIFI
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = CustomerInvoice " + ex.ToString());
            }
        }

        #region JournalReceivable

        private bool CheckJournalSetup(Session _currSession, CustomerInvoice _locCustomerInvoiceXPO)
        {
            bool _result = false;
            try
            {
                if (_locCustomerInvoiceXPO != null)
                {
                    if (_locCustomerInvoiceXPO.Company != null && _locCustomerInvoiceXPO.Workplace != null)
                    {
                        JournalSetupDetail _locJournalSetupDetail = _currSession.FindObject<JournalSetupDetail>
                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                    new BinaryOperator("Company", _locCustomerInvoiceXPO.Company),
                                                                    new BinaryOperator("Workplace", _locCustomerInvoiceXPO.Workplace),
                                                                    new BinaryOperator("JournalSetupType", JournalSetupType.Journal),
                                                                    new BinaryOperator("ObjectList", ObjectList.CustomerInvoice),
                                                                    new BinaryOperator("JournalProcess", JournalProcess.JournalReceivable),
                                                                    new BinaryOperator("Active", true)));

                        if (_locJournalSetupDetail != null)
                        {
                            _result = true;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = CustomerInvoice ", ex.ToString());
            }

            return _result;
        }

        private void SetReceivableJournal(Session _currSession, CustomerInvoice _locCustomerInvoiceXPO)
        {
            try
            {
                DateTime now = DateTime.Now;
                double _locTotalAmountBP = 0;
                double _locTotalAmountC = 0;
                BusinessPartnerAccountGroup _locBusinessPartnerAccountGroup = null;
                CompanyAccountGroup _locCompanyAccountGroup = null;

                if (_locCustomerInvoiceXPO != null)
                {
                    XPCollection<CustomerInvoiceLine> _locCustomerInvoiceLines = new XPCollection<CustomerInvoiceLine>(_currSession,
                                                                        new GroupOperator(GroupOperatorType.And,
                                                                        new BinaryOperator("CustomerInvoice", _locCustomerInvoiceXPO),
                                                                        new BinaryOperator("Select", true),
                                                                        new GroupOperator(GroupOperatorType.Or,
                                                                        new BinaryOperator("Status", Status.Progress),
                                                                        new BinaryOperator("Status", Status.Posted))));

                    if (_locCustomerInvoiceLines != null && _locCustomerInvoiceLines.Count() > 0)
                    {
                        foreach (CustomerInvoiceLine _locCustomerInvoiceLine in _locCustomerInvoiceLines)
                        {
                            #region JournalMapBusinessPartnerAccountGroup

                            if (_locCustomerInvoiceLine.CustomerInvoice.BillToCustomer != null)
                            {
                                BusinessPartnerAccount _locBusinessPartnerAccount = _currSession.FindObject<BusinessPartnerAccount>(new GroupOperator(GroupOperatorType.And,
                                                                            new BinaryOperator("Company", _locCustomerInvoiceLine.Company),
                                                                            new BinaryOperator("Workplace", _locCustomerInvoiceLine.Workplace),
                                                                            new BinaryOperator("BusinessPartner", _locCustomerInvoiceLine.CustomerInvoice.BillToCustomer),
                                                                            new BinaryOperator("Active", true)));
                                if(_locBusinessPartnerAccount != null)
                                {
                                    if (_locBusinessPartnerAccount.BusinessPartnerAccountGroup != null)
                                    {
                                        _locBusinessPartnerAccountGroup = _locBusinessPartnerAccount.BusinessPartnerAccountGroup;

                                        _locTotalAmountBP = _locCustomerInvoiceLine.TAmount;


                                        XPCollection<JournalMap> _locJournalMapItems = new XPCollection<JournalMap>
                                                                           (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                            new BinaryOperator("Company", _locCustomerInvoiceLine.Company),
                                                                            new BinaryOperator("Workplace", _locCustomerInvoiceLine.Workplace),
                                                                            new BinaryOperator("BusinessPartnerAccountGroup", _locBusinessPartnerAccountGroup),
                                                                            new BinaryOperator("Active", true)));

                                        if (_locJournalMapItems != null && _locJournalMapItems.Count() > 0)
                                        {
                                            foreach (JournalMap _locJournalMapItem in _locJournalMapItems)
                                            {
                                                XPCollection<JournalMapLine> _locJournalMapLines = new XPCollection<JournalMapLine>
                                                                                                   (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                    new BinaryOperator("Company", _locCustomerInvoiceLine.Company),
                                                                                                    new BinaryOperator("Workplace", _locCustomerInvoiceLine.Workplace),
                                                                                                    new BinaryOperator("JournalMap", _locJournalMapItem),
                                                                                                    new BinaryOperator("Active", true)));

                                                if (_locJournalMapLines != null && _locJournalMapLines.Count() > 0)
                                                {
                                                    foreach (JournalMapLine _locJournalMapLine in _locJournalMapLines)
                                                    {
                                                        if (_locJournalMapLine.AccountMap != null)
                                                        {
                                                            #region NormalAmount
                                                            if (_locJournalMapLine.AccountMap.PostingType == PostingType.Sales && _locJournalMapLine.AccountMap.OrderType == OrderType.Item
                                                                && _locJournalMapLine.AccountMap.PostingMethod == PostingMethod.Deliver && _locJournalMapLine.AccountMap.PostingMethodType == PostingMethodType.Normal)
                                                            {
                                                                XPCollection<AccountMapLine> _locAccountMapLines = new XPCollection<AccountMapLine>
                                                                                                               (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                                new BinaryOperator("Company", _locCustomerInvoiceLine.Company),
                                                                                                                new BinaryOperator("Workplace", _locCustomerInvoiceLine.Workplace),
                                                                                                                new BinaryOperator("AccountMap", _locJournalMapLine.AccountMap),
                                                                                                                new BinaryOperator("Active", true)));

                                                                if (_locAccountMapLines != null && _locAccountMapLines.Count() > 0)
                                                                {
                                                                    double _locTotalAmountDebit = 0;
                                                                    double _locTotalAmountCredit = 0;
                                                                    double _locTotalBalance = 0;

                                                                    foreach (AccountMapLine _locAccountMapLine in _locAccountMapLines)
                                                                    {
                                                                        if (_locAccountMapLine.AccountCharge == AccountCharge.Debit)
                                                                        {
                                                                            _locTotalAmountDebit = _locTotalAmountBP;
                                                                        }
                                                                        if (_locAccountMapLine.AccountCharge == AccountCharge.Credit)
                                                                        {
                                                                            _locTotalAmountCredit = _locTotalAmountBP;
                                                                        }

                                                                        GeneralJournal _saveGeneralJournal = new GeneralJournal(_currSession)
                                                                        {
                                                                            Company = _locCustomerInvoiceLine.Company,
                                                                            Workplace = _locCustomerInvoiceLine.Workplace,
                                                                            PostingDate = now,
                                                                            PostingType = PostingType.Sales,
                                                                            OrderType = OrderType.Item,
                                                                            PostingMethod = PostingMethod.Deliver,
                                                                            PostingMethodType = PostingMethodType.Normal,
                                                                            Account = _locAccountMapLine.Account,
                                                                            Debit = _locTotalAmountDebit,
                                                                            Credit = _locTotalAmountCredit,
                                                                            JournalMonth = _locCustomerInvoiceLine.JournalMonth,
                                                                            JournalYear = _locCustomerInvoiceLine.JournalYear,
                                                                            CustomerInvoice = _locCustomerInvoiceXPO,
                                                                        };

                                                                        _saveGeneralJournal.Save();
                                                                        _saveGeneralJournal.Session.CommitTransaction();

                                                                        #region AccountingPeriodicLine

                                                                        if (_locAccountMapLine.Account.Code != null)
                                                                        {

                                                                            //Cari Account Periodic Line
                                                                            AccountingPeriodicLine _locAPL = _currSession.FindObject<AccountingPeriodicLine>
                                                                                                            (new GroupOperator(GroupOperatorType.And,
                                                                                                            new BinaryOperator("Company", _locCustomerInvoiceLine.Company),
                                                                                                            new BinaryOperator("Workplace", _locCustomerInvoiceLine.Workplace),
                                                                                                            new BinaryOperator("AccountNo", _locAccountMapLine.Account),
                                                                                                            new BinaryOperator("Month", _locCustomerInvoiceLine.JournalMonth),
                                                                                                            new BinaryOperator("Year", _locCustomerInvoiceLine.JournalYear)));
                                                                            if (_locAPL != null)
                                                                            {
                                                                                if (_locAPL.AccountNo != null)
                                                                                {
                                                                                    if (_locAPL.AccountNo.BalanceType == BalanceType.Change)
                                                                                    {
                                                                                        if (_locAPL.AccountNo.AccountCharge == AccountCharge.Debit)
                                                                                        {
                                                                                            if (_locTotalAmountDebit > 0)
                                                                                            {
                                                                                                _locTotalBalance = _locAPL.Balance + _locTotalAmountDebit;
                                                                                            }
                                                                                            if (_locTotalAmountCredit > 0)
                                                                                            {
                                                                                                _locTotalBalance = _locAPL.Balance - _locTotalAmountCredit;
                                                                                            }
                                                                                        }
                                                                                        if (_locAPL.AccountNo.AccountCharge == AccountCharge.Credit)
                                                                                        {
                                                                                            if (_locTotalAmountDebit > 0)
                                                                                            {
                                                                                                _locTotalBalance = _locAPL.Balance - _locTotalAmountDebit;
                                                                                            }
                                                                                            if (_locTotalAmountCredit > 0)
                                                                                            {
                                                                                                _locTotalBalance = _locAPL.Balance + _locTotalAmountCredit;
                                                                                            }
                                                                                        }
                                                                                    }

                                                                                    _locAPL.Balance = _locTotalBalance;
                                                                                    _locAPL.Debit = _locAPL.Debit + _locTotalAmountDebit;
                                                                                    _locAPL.Credit = _locAPL.Credit + _locTotalAmountCredit;
                                                                                    _locAPL.Save();
                                                                                    _locAPL.Session.CommitTransaction();

                                                                                }
                                                                            }
                                                                        }
                                                                        #endregion AccountingPeriodicLine
                                                                    }
                                                                }
                                                            }
                                                            #endregion NormalAmount   
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                                
                            }
                            #endregion JournalMapBusinessPartnerAccountGroup

                            #region JournalMapCompanyAccountGroup
                            if (_locCustomerInvoiceLine.Company != null)
                            {
                                _locTotalAmountC = _locCustomerInvoiceLine.TAmount;

                                CompanyAccount _locCompanyAccount = _currSession.FindObject<CompanyAccount>(new GroupOperator(GroupOperatorType.And,
                                                        new BinaryOperator("Company", _locCustomerInvoiceLine.Company),
                                                        new BinaryOperator("Workplace", _locCustomerInvoiceLine.Workplace),
                                                        new BinaryOperator("Active", true)));

                                if (_locCompanyAccount != null)
                                {
                                    if (_locCompanyAccount.CompanyAccountGroup != null)
                                    {
                                        _locCompanyAccountGroup = _locCompanyAccount.CompanyAccountGroup;

                                        XPCollection<JournalMap> _locJournalMapItems = new XPCollection<JournalMap>
                                                                       (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                        new BinaryOperator("Company", _locCustomerInvoiceLine.Company),
                                                                        new BinaryOperator("Workplace", _locCustomerInvoiceLine.Workplace),
                                                                        new BinaryOperator("CompanyAccountGroup", _locCompanyAccountGroup),
                                                                        new BinaryOperator("Active", true)));

                                        if (_locJournalMapItems != null && _locJournalMapItems.Count() > 0)
                                        {
                                            foreach (JournalMap _locJournalMapItem in _locJournalMapItems)
                                            {
                                                XPCollection<JournalMapLine> _locJournalMapLines = new XPCollection<JournalMapLine>
                                                                                                   (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                    new BinaryOperator("Company", _locCustomerInvoiceLine.Company),
                                                                                                    new BinaryOperator("Workplace", _locCustomerInvoiceLine.Workplace),
                                                                                                    new BinaryOperator("JournalMap", _locJournalMapItem),
                                                                                                    new BinaryOperator("Active", true)));

                                                if (_locJournalMapLines != null && _locJournalMapLines.Count() > 0)
                                                {
                                                    foreach (JournalMapLine _locJournalMapLine in _locJournalMapLines)
                                                    {
                                                        if (_locJournalMapLine.AccountMap != null)
                                                        {
                                                            #region NormalAmount
                                                            if (_locJournalMapLine.AccountMap.PostingType == PostingType.Sales && _locJournalMapLine.AccountMap.OrderType == OrderType.Item
                                                                && _locJournalMapLine.AccountMap.PostingMethod == PostingMethod.Deliver && _locJournalMapLine.AccountMap.PostingMethodType == PostingMethodType.Normal)
                                                            {
                                                                XPCollection<AccountMapLine> _locAccountMapLines = new XPCollection<AccountMapLine>
                                                                                                               (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                                new BinaryOperator("Company", _locCustomerInvoiceLine.Company),
                                                                                                                new BinaryOperator("Workplace", _locCustomerInvoiceLine.Workplace),
                                                                                                                new BinaryOperator("AccountMap", _locJournalMapLine.AccountMap),
                                                                                                                new BinaryOperator("Active", true)));

                                                                if (_locAccountMapLines != null && _locAccountMapLines.Count() > 0)
                                                                {
                                                                    double _locTotalAmountDebit = 0;
                                                                    double _locTotalAmountCredit = 0;
                                                                    double _locTotalBalance = 0;

                                                                    foreach (AccountMapLine _locAccountMapLine in _locAccountMapLines)
                                                                    {
                                                                        if (_locAccountMapLine.AccountCharge == AccountCharge.Debit)
                                                                        {
                                                                            _locTotalAmountDebit = _locTotalAmountC;
                                                                        }
                                                                        if (_locAccountMapLine.AccountCharge == AccountCharge.Credit)
                                                                        {
                                                                            _locTotalAmountCredit = _locTotalAmountC;
                                                                        }

                                                                        GeneralJournal _saveGeneralJournal = new GeneralJournal(_currSession)
                                                                        {
                                                                            Company = _locCustomerInvoiceLine.Company,
                                                                            Workplace = _locCustomerInvoiceLine.Workplace,
                                                                            PostingDate = now,
                                                                            PostingType = PostingType.Sales,
                                                                            OrderType = OrderType.Item,
                                                                            PostingMethod = PostingMethod.Deliver,
                                                                            PostingMethodType = PostingMethodType.Normal,
                                                                            Account = _locAccountMapLine.Account,
                                                                            Debit = _locTotalAmountDebit,
                                                                            Credit = _locTotalAmountCredit,
                                                                            JournalMonth = _locCustomerInvoiceLine.JournalMonth,
                                                                            JournalYear = _locCustomerInvoiceLine.JournalYear,
                                                                            CustomerInvoice = _locCustomerInvoiceXPO,
                                                                        };

                                                                        _saveGeneralJournal.Save();
                                                                        _saveGeneralJournal.Session.CommitTransaction();

                                                                        #region AccountingPeriodicLine

                                                                        if (_locAccountMapLine.Account.Code != null)
                                                                        {

                                                                            //Cari Account Periodic Line
                                                                            AccountingPeriodicLine _locAPL = _currSession.FindObject<AccountingPeriodicLine>
                                                                                                            (new GroupOperator(GroupOperatorType.And,
                                                                                                            new BinaryOperator("Company", _locCustomerInvoiceLine.Company),
                                                                                                            new BinaryOperator("Workplace", _locCustomerInvoiceLine.Workplace),
                                                                                                            new BinaryOperator("AccountNo", _locAccountMapLine.Account),
                                                                                                            new BinaryOperator("Month", _locCustomerInvoiceLine.JournalMonth),
                                                                                                            new BinaryOperator("Year", _locCustomerInvoiceLine.JournalYear)));
                                                                            if (_locAPL != null)
                                                                            {
                                                                                if (_locAPL.AccountNo != null)
                                                                                {
                                                                                    if (_locAPL.AccountNo.BalanceType == BalanceType.Change)
                                                                                    {
                                                                                        if (_locAPL.AccountNo.AccountCharge == AccountCharge.Debit)
                                                                                        {
                                                                                            if (_locTotalAmountDebit > 0)
                                                                                            {
                                                                                                _locTotalBalance = _locAPL.Balance + _locTotalAmountDebit;
                                                                                            }
                                                                                            if (_locTotalAmountCredit > 0)
                                                                                            {
                                                                                                _locTotalBalance = _locAPL.Balance - _locTotalAmountCredit;
                                                                                            }
                                                                                        }
                                                                                        if (_locAPL.AccountNo.AccountCharge == AccountCharge.Credit)
                                                                                        {
                                                                                            if (_locTotalAmountDebit > 0)
                                                                                            {
                                                                                                _locTotalBalance = _locAPL.Balance - _locTotalAmountDebit;
                                                                                            }
                                                                                            if (_locTotalAmountCredit > 0)
                                                                                            {
                                                                                                _locTotalBalance = _locAPL.Balance + _locTotalAmountCredit;
                                                                                            }
                                                                                        }
                                                                                    }

                                                                                    _locAPL.Balance = _locTotalBalance;
                                                                                    _locAPL.Debit = _locAPL.Debit + _locTotalAmountDebit;
                                                                                    _locAPL.Credit = _locAPL.Credit + _locTotalAmountCredit;
                                                                                    _locAPL.Save();
                                                                                    _locAPL.Session.CommitTransaction();

                                                                                }
                                                                            }
                                                                        }
                                                                        #endregion AccountingPeriodicLine
                                                                    }
                                                                }
                                                            }
                                                            #endregion NormalAmount 
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            #endregion JournalMapCompanyAccountGroup
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = CustomerInvoice ", ex.ToString());
            }
        }

        #endregion JournalReceivable

        private void SetStatusCustomerInvoiceLine(Session _currSession, CustomerInvoice _locCustomerInvoiceXPO)
        {
            try
            {
                DateTime now = DateTime.Now;
                if (_locCustomerInvoiceXPO != null)
                {
                    XPCollection<CustomerInvoiceLine> _locCustomerInvoiceLines = new XPCollection<CustomerInvoiceLine>(_currSession,
                                                            new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("CustomerInvoice", _locCustomerInvoiceXPO),
                                                            new BinaryOperator("Select", true),
                                                            new GroupOperator(GroupOperatorType.Or,
                                                            new BinaryOperator("Status", Status.Progress),
                                                            new BinaryOperator("Status", Status.Posted))));

                    if (_locCustomerInvoiceLines != null && _locCustomerInvoiceLines.Count > 0)
                    {
                        foreach (CustomerInvoiceLine _locCustomerInvoiceLine in _locCustomerInvoiceLines)
                        {
                            _locCustomerInvoiceLine.Status = Status.Close;
                            _locCustomerInvoiceLine.ActivationPosting = true;
                            _locCustomerInvoiceLine.Select = false;
                            _locCustomerInvoiceLine.StatusDate = now;
                            _locCustomerInvoiceLine.Save();
                            _locCustomerInvoiceLine.Session.CommitTransaction();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = CustomerInvoice " + ex.ToString());
            }
        }

        private void SetStatusCustomerInvoiceFreeItem(Session _currSession, CustomerInvoice _locCustomerInvoiceXPO)
        {
            try
            {
                DateTime now = DateTime.Now;
                if (_locCustomerInvoiceXPO != null)
                {
                    XPCollection<CustomerInvoiceFreeItem> _locCustomerInvoiceFreeItems = new XPCollection<CustomerInvoiceFreeItem>(_currSession,
                                                            new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("CustomerInvoice", _locCustomerInvoiceXPO),
                                                            new GroupOperator(GroupOperatorType.Or,
                                                            new BinaryOperator("Status", Status.Open),
                                                            new BinaryOperator("Status", Status.Progress),
                                                            new BinaryOperator("Status", Status.Posted))));

                    if (_locCustomerInvoiceFreeItems != null && _locCustomerInvoiceFreeItems.Count > 0)
                    {
                        foreach (CustomerInvoiceFreeItem _locCustomerInvoiceFreeItem in _locCustomerInvoiceFreeItems)
                        {
                            _locCustomerInvoiceFreeItem.Status = Status.Close;
                            _locCustomerInvoiceFreeItem.StatusDate = now;
                            _locCustomerInvoiceFreeItem.Save();
                            _locCustomerInvoiceFreeItem.Session.CommitTransaction();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = CustomerInvoice " + ex.ToString());
            }
        }

        private void SetNormalQuantity(Session _currSession, CustomerInvoice _locCustomerInvoiceXPO)
        {
            try
            {
                if (_locCustomerInvoiceXPO != null)
                {
                    XPCollection<CustomerInvoiceLine> _locCustomerInvoiceLines = new XPCollection<CustomerInvoiceLine>(_currSession,
                                                                                     new GroupOperator(GroupOperatorType.And,
                                                                                     new BinaryOperator("CustomerInvoice", _locCustomerInvoiceXPO),
                                                                                     new BinaryOperator("Select", true),
                                                                                     new GroupOperator(GroupOperatorType.Or,
                                                                                     new BinaryOperator("Status", Status.Progress),
                                                                                     new BinaryOperator("Status", Status.Posted))));

                    if (_locCustomerInvoiceLines != null && _locCustomerInvoiceLines.Count > 0)
                    {
                        foreach (CustomerInvoiceLine _locCustomerInvoiceLine in _locCustomerInvoiceLines)
                        {
                            if (_locCustomerInvoiceLine.DQty > 0 || _locCustomerInvoiceLine.Qty > 0)
                            {
                                _locCustomerInvoiceLine.Select = false;
                                _locCustomerInvoiceLine.Save();
                                _locCustomerInvoiceLine.Session.CommitTransaction();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = CustomerInvoice " + ex.ToString());
            }
        }

        private void SetFinalStatusCustomerOrder(Session _currSession, CustomerInvoice _locCustomerInvoiceXPO)
        {
            try
            {
                DateTime now = DateTime.Now;
                int _locStatusCount = 0;
                int _locCustOrderLineCount = 0;

                if (_locCustomerInvoiceXPO != null)
                {

                    XPCollection<CustomerInvoiceLine> _locCustomerInvoiceLines = new XPCollection<CustomerInvoiceLine>(_currSession,
                                                            new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("CustomerInvoice", _locCustomerInvoiceXPO)));

                    if (_locCustomerInvoiceLines != null && _locCustomerInvoiceLines.Count() > 0)
                    {
                        _locCustOrderLineCount = _locCustomerInvoiceLines.Count();

                        foreach (CustomerInvoiceLine _locCustomerInvoiceLine in _locCustomerInvoiceLines)
                        {
                            if (_locCustomerInvoiceLine.Status == Status.Close)
                            {
                                _locStatusCount = _locStatusCount + 1;
                            }
                        }
                    }

                    if (_locStatusCount == _locCustOrderLineCount)
                    {
                        _locCustomerInvoiceXPO.ActivationPosting = true;
                        _locCustomerInvoiceXPO.Status = Status.Close;
                        _locCustomerInvoiceXPO.StatusDate = now;
                        _locCustomerInvoiceXPO.PostedCount = _locCustomerInvoiceXPO.PostedCount + 1;
                        _locCustomerInvoiceXPO.Save();
                        _locCustomerInvoiceXPO.Session.CommitTransaction();
                    }
                    else
                    {
                        _locCustomerInvoiceXPO.Status = Status.Posted;
                        _locCustomerInvoiceXPO.StatusDate = now;
                        _locCustomerInvoiceXPO.PostedCount = _locCustomerInvoiceXPO.PostedCount + 1;
                        _locCustomerInvoiceXPO.Save();
                        _locCustomerInvoiceXPO.Session.CommitTransaction();
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = CustomerInvoice " + ex.ToString());
            }
        }

        #endregion Posting

        #region Global Method

        private void SuccessMessageShow(string _locActionName)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 2000;
            options.Message = string.Format("{0} !", _locActionName);
            options.Type = InformationType.Success;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Success";
            options.Win.Type = WinMessageType.Flyout;
            Application.ShowViewStrategy.ShowMessage(options);
        }

        private void ErrorMessageShow(string _locMessage)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 2000;
            options.Message = string.Format(_locMessage);
            options.Type = InformationType.Warning;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Error";
            options.Win.Type = WinMessageType.Alert;
            Application.ShowViewStrategy.ShowMessage(options);
        }

        #endregion Global Method

    }
}
