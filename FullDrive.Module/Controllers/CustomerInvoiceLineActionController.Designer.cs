﻿namespace FullDrive.Module.Controllers
{
    partial class CustomerInvoiceLineActionController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.CustomerInvoiceLineSelectAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.CustomerInvoiceLineUnselectAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.CustomerInvoiceLineTaxAndDiscountAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.CustomerInvoiceLineListviewFilterSelectionAction = new DevExpress.ExpressApp.Actions.SingleChoiceAction(this.components);
            // 
            // CustomerInvoiceLineSelectAction
            // 
            this.CustomerInvoiceLineSelectAction.Caption = "Select";
            this.CustomerInvoiceLineSelectAction.ConfirmationMessage = null;
            this.CustomerInvoiceLineSelectAction.Id = "CustomerInvoiceLineSelectActionId";
            this.CustomerInvoiceLineSelectAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.CustomerInvoiceLine);
            this.CustomerInvoiceLineSelectAction.ToolTip = null;
            this.CustomerInvoiceLineSelectAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.CustomerInvoiceLineSelectAction_Execute);
            // 
            // CustomerInvoiceLineUnselectAction
            // 
            this.CustomerInvoiceLineUnselectAction.Caption = "Unselect";
            this.CustomerInvoiceLineUnselectAction.ConfirmationMessage = null;
            this.CustomerInvoiceLineUnselectAction.Id = "CustomerInvoiceLineUnselectActionId";
            this.CustomerInvoiceLineUnselectAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.CustomerInvoiceLine);
            this.CustomerInvoiceLineUnselectAction.ToolTip = null;
            this.CustomerInvoiceLineUnselectAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.CustomerInvoiceLineUnselectAction_Execute);
            // 
            // CustomerInvoiceLineTaxAndDiscountAction
            // 
            this.CustomerInvoiceLineTaxAndDiscountAction.Caption = "Tax And Discount";
            this.CustomerInvoiceLineTaxAndDiscountAction.ConfirmationMessage = null;
            this.CustomerInvoiceLineTaxAndDiscountAction.Id = "CustomerInvoiceLineTaxAndDiscountActionId";
            this.CustomerInvoiceLineTaxAndDiscountAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.CustomerInvoiceLine);
            this.CustomerInvoiceLineTaxAndDiscountAction.ToolTip = null;
            this.CustomerInvoiceLineTaxAndDiscountAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.CustomerInvoiceLineTaxAndDiscountAction_Execute);
            // 
            // CustomerInvoiceLineListviewFilterSelectionAction
            // 
            this.CustomerInvoiceLineListviewFilterSelectionAction.Caption = "Filter";
            this.CustomerInvoiceLineListviewFilterSelectionAction.ConfirmationMessage = null;
            this.CustomerInvoiceLineListviewFilterSelectionAction.Id = "CustomerInvoiceLineListviewFilterSelectionActionId";
            this.CustomerInvoiceLineListviewFilterSelectionAction.ItemType = DevExpress.ExpressApp.Actions.SingleChoiceActionItemType.ItemIsOperation;
            this.CustomerInvoiceLineListviewFilterSelectionAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.CustomerInvoiceLine);
            this.CustomerInvoiceLineListviewFilterSelectionAction.ToolTip = null;
            this.CustomerInvoiceLineListviewFilterSelectionAction.Execute += new DevExpress.ExpressApp.Actions.SingleChoiceActionExecuteEventHandler(this.CustomerInvoiceLineListviewFilterSelectionAction_Execute);
            // 
            // CustomerInvoiceLineActionController
            // 
            this.Actions.Add(this.CustomerInvoiceLineSelectAction);
            this.Actions.Add(this.CustomerInvoiceLineUnselectAction);
            this.Actions.Add(this.CustomerInvoiceLineTaxAndDiscountAction);
            this.Actions.Add(this.CustomerInvoiceLineListviewFilterSelectionAction);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SimpleAction CustomerInvoiceLineSelectAction;
        private DevExpress.ExpressApp.Actions.SimpleAction CustomerInvoiceLineUnselectAction;
        private DevExpress.ExpressApp.Actions.SimpleAction CustomerInvoiceLineTaxAndDiscountAction;
        private DevExpress.ExpressApp.Actions.SingleChoiceAction CustomerInvoiceLineListviewFilterSelectionAction;
    }
}
