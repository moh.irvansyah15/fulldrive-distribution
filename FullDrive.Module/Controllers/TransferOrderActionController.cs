﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;

using FullDrive.Module.BusinessObjects;
using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.Xpo;
using System.Collections;
using DevExpress.Xpo;

namespace FullDrive.Module.Controllers
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class TransferOrderActionController : ViewController
    {
        private ChoiceActionItem _selectionListviewFilter;

        public TransferOrderActionController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.
            #region Filter Status
            TransferOrderListviewFilterSelectionAction.Items.Clear();
            foreach (object _currApproval in Enum.GetValues(typeof(CustomProcess.Status)))
            {
                EnumDescriptor _ed = new EnumDescriptor(typeof(CustomProcess.Status));
                _selectionListviewFilter = new ChoiceActionItem(_ed.GetCaption(_currApproval), _currApproval);
                TransferOrderListviewFilterSelectionAction.Items.Add(_selectionListviewFilter);
            }
            #endregion Filter Status
        }
        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }
        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        private void TransferOrderGetStockAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;

                if (_objectsToProcess != null)
                {
                    foreach (Object obj in _objectsToProcess)
                    {
                        TransferOrder _locTransferOrderOS = (TransferOrder)_objectSpace.GetObject(obj);

                        if (_locTransferOrderOS != null)
                        {
                            if (_locTransferOrderOS.Session != null)
                            {
                                _currSession = _locTransferOrderOS.Session;
                            }

                            if (_locTransferOrderOS.Code != null && _currSession != null)
                            {
                                _currObjectId = _locTransferOrderOS.Code;

                                TransferOrder _locTransferOrderXPO = _currSession.FindObject<TransferOrder>
                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                     new BinaryOperator("Code", _currObjectId),
                                                                     new GroupOperator(GroupOperatorType.Or,
                                                                     new BinaryOperator("Status", Status.Open),
                                                                     new BinaryOperator("Status", Status.Progress))));

                                if (_locTransferOrderXPO != null)
                                {
                                    if (_locTransferOrderXPO.DocumentRule == DocumentRule.Customer)
                                    {
                                        
                                        XPCollection<SalesInvoiceCollection2> _locSalesInvoiceCollection2s = new XPCollection<SalesInvoiceCollection2>
                                                                                        (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                        new BinaryOperator("TransferOrder", _locTransferOrderXPO),
                                                                                        new GroupOperator(GroupOperatorType.Or,
                                                                                        new BinaryOperator("Status", Status.Open),
                                                                                        new BinaryOperator("Status", Status.Progress))));

                                        if (_locSalesInvoiceCollection2s != null && _locSalesInvoiceCollection2s.Count() > 0)
                                        {
                                            foreach (SalesInvoiceCollection2 _locSalesInvoiceCollection2 in _locSalesInvoiceCollection2s)
                                            {
                                                if (_locSalesInvoiceCollection2.SalesInvoice != null)
                                                {
                                                    GetSalesInvoiceMonitoring(_currSession, _locSalesInvoiceCollection2.SalesInvoice, _locTransferOrderXPO);
                                                }
                                            }
                                            SuccessMessageShow("Sales Invoice Has Been Successfully Getting into Transfer Order");
                                        }

                                    }
                                    if (_locTransferOrderXPO.DocumentRule == DocumentRule.Salesman)
                                    {
                                        GetBeginningInventory(_currSession, _locTransferOrderXPO);
                                        SuccessMessageShow("Stock Has Been Successfully Getting into Transfer Order");
                                    }

                                }
                                else
                                {
                                    ErrorMessageShow("Data Transfer Order Not Available");
                                }
                            }
                            else
                            {
                                ErrorMessageShow("Data Transfer Order Not Available");
                            }
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = TransferOrder " + ex.ToString());
            }
        }

        private void TransferOrderProgressAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;
                Status _locStatus = Status.None;
                DateTime _locNow;
                Status _locStatus2 = Status.None;
                DateTime _locNow2;

                if (_objectsToProcess != null)
                {
                    foreach (Object obj in _objectsToProcess)
                    {
                        TransferOrder _locTransferOrderOS = (TransferOrder)_objectSpace.GetObject(obj);

                        if (_locTransferOrderOS != null)
                        {
                            if (_locTransferOrderOS.Session != null)
                            {
                                _currSession = _locTransferOrderOS.Session;
                            }
                            if (_locTransferOrderOS.Code != null && _currSession != null)
                            {
                                _currObjectId = _locTransferOrderOS.Code;

                                TransferOrder _locTransferOrderXPO = _currSession.FindObject<TransferOrder>
                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                     new BinaryOperator("Code", _currObjectId),
                                                                     new GroupOperator(GroupOperatorType.Or,
                                                                     new BinaryOperator("Status", Status.Open),
                                                                     new BinaryOperator("Status", Status.Progress))));

                                if (_locTransferOrderXPO != null)
                                {
                                    if (_locTransferOrderXPO.Status == Status.Open)
                                    {
                                        _locStatus = Status.Progress;
                                        _locNow = now;
                                    }
                                    else
                                    {
                                        _locStatus = _locTransferOrderXPO.Status;
                                        _locNow = _locTransferOrderXPO.StatusDate;
                                    }
                                    _locTransferOrderXPO.Status = _locStatus;
                                    _locTransferOrderXPO.StatusDate = _locNow;
                                    _locTransferOrderXPO.Save();
                                    _locTransferOrderXPO.Session.CommitTransaction();

                                    #region TransferOrderLine
                                    XPCollection<TransferOrderLine> _locTransferOrderLines = new XPCollection<TransferOrderLine>
                                                           (_currSession, new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("TransferOrder", _locTransferOrderXPO),
                                                            new GroupOperator(GroupOperatorType.Or,
                                                            new BinaryOperator("Status", Status.Open),
                                                            new BinaryOperator("Status", Status.Progress))));

                                    if (_locTransferOrderLines != null && _locTransferOrderLines.Count > 0)
                                    {
                                        foreach (TransferOrderLine _locTransferOrderLine in _locTransferOrderLines)
                                        {
                                            if (_locTransferOrderLine.Status == Status.Open)
                                            {
                                                _locStatus2 = Status.Progress;
                                                _locNow2 = now;
                                            }
                                            else
                                            {
                                                _locStatus2 = _locTransferOrderLine.Status;
                                                _locNow2 = _locTransferOrderLine.StatusDate;
                                            }

                                            _locTransferOrderLine.Status = _locStatus2;
                                            _locTransferOrderLine.StatusDate = _locNow2;
                                            _locTransferOrderLine.Save();
                                            _locTransferOrderLine.Session.CommitTransaction();
                                        }
                                    }
                                    #endregion TransferOrderLine

                                    #region SalesInvoiceCollection
                                    XPCollection<SalesInvoiceCollection2> _locSalesInvoiceCollection2s = new XPCollection<SalesInvoiceCollection2>
                                                                                    (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                    new BinaryOperator("TransferOrder", _locTransferOrderXPO),
                                                                                    new BinaryOperator("Status", Status.Open)));
                                    if (_locSalesInvoiceCollection2s != null && _locSalesInvoiceCollection2s.Count() > 0)
                                    {
                                        foreach (SalesInvoiceCollection2 _locSalesInvoiceCollection2 in _locSalesInvoiceCollection2s)
                                        {
                                            _locSalesInvoiceCollection2.Status = Status.Progress;
                                            _locSalesInvoiceCollection2.StatusDate = now;
                                            _locSalesInvoiceCollection2.Save();
                                            _locSalesInvoiceCollection2.Session.CommitTransaction();
                                        }
                                    }
                                    #endregion TransferOutMonitoring

                                    SetBegInvLineTo(_currSession, _locTransferOrderXPO);

                                    SuccessMessageShow(_locTransferOrderXPO.Code + " has been change successfully to Progress");

                                }
                                else
                                {
                                    ErrorMessageShow("Data Transfer Order Not Available");
                                }
                            }
                            else
                            {
                                ErrorMessageShow("Data Transfer Order Not Available");
                            }
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = TransferOrder " + ex.ToString());
            }
        }

        private void TransferOrderPostingAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;

                if (_objectsToProcess != null)
                {
                    foreach (Object obj in _objectsToProcess)
                    {
                        TransferOrder _locTransferOrderOS = (TransferOrder)_objectSpace.GetObject(obj);

                        if (_locTransferOrderOS != null)
                        {
                            if (_locTransferOrderOS.Status == Status.Progress || _locTransferOrderOS.Status == Status.Posted)
                            {
                                if (_locTransferOrderOS.Session != null)
                                {
                                    _currSession = _locTransferOrderOS.Session;
                                }
                                if (_locTransferOrderOS.Code != null && _currSession != null)
                                {
                                    _currObjectId = _locTransferOrderOS.Code;

                                    TransferOrder _locTransferOrderXPO = _currSession.FindObject<TransferOrder>
                                                                        (new GroupOperator(GroupOperatorType.And,
                                                                         new BinaryOperator("Code", _currObjectId)));

                                    if (_locTransferOrderXPO != null)
                                    {
                                        if (CheckAvailableStock(_currSession, _locTransferOrderXPO) == true && CheckLockStock(_currSession, _locTransferOrderXPO) == false)
                                        {
                                            SetDeliverBeginingInventory(_currSession, _locTransferOrderXPO);
                                            SetDeliverInventoryJournal(_currSession, _locTransferOrderXPO);
                                            SetTransferInventoryMonitoring(_currSession, _locTransferOrderXPO);
                                            SetStatusDeliverTransferOrderLine(_currSession, _locTransferOrderXPO);
                                            SetNormalDeliverQuantity(_currSession, _locTransferOrderXPO);
                                            SetFinalStatusDeliverTransferOrder(_currSession, _locTransferOrderXPO);
                                            SuccessMessageShow(_locTransferOrderXPO.Code + " has been change successfully to Deliver");
                                        }
                                        else
                                        {
                                            ErrorMessageShow("Please check stock quantity");
                                        }
                                    }
                                    else
                                    {
                                        ErrorMessageShow("Transfer Order Data Not Available");
                                    }
                                }
                            }
                            else
                            {
                                ErrorMessageShow("Transfer Order Data Not Available");
                            }
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = TransferOrder " + ex.ToString());
            }
        }

        private void TransferOrderListviewFilterSelectionAction_Execute(object sender, SingleChoiceActionExecuteEventArgs e)
        {
            try
            {
                if ((View is ListView) & (View.ObjectTypeInfo.Type == typeof(TransferOrder)))
                {
                    if ((Status)e.SelectedChoiceActionItem.Data == Status.None)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.None, BinaryOperatorType.NotEqual);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Open)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Open, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Progress)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Progress, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Posted)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Posted, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Lock)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Lock, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Close)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Close, BinaryOperatorType.Equal);
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = TransferOrder " + ex.ToString());
            }
        }

        //============================================ Code In Here ============================================

        #region GetStock

        #region GetSIM

        private void GetSalesInvoiceMonitoring(Session _currSession, SalesInvoice _locSalesInvoiceXPO, TransferOrder _locTransferOrderXPO)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                DateTime now = DateTime.Now;
                string _currSignCode = null;

                if (_locSalesInvoiceXPO != null && _locTransferOrderXPO != null)
                {
                    #region PickingLine
                    if (_locTransferOrderXPO.Picking != null)
                    {
                        PickingLine _locPickingLine = _currSession.FindObject<PickingLine>(new GroupOperator(GroupOperatorType.And,
                                                    new BinaryOperator("Picking", _locTransferOrderXPO.Picking),
                                                    new BinaryOperator("SalesInvoice", _locSalesInvoiceXPO)));
                        if(_locPickingLine != null)
                        {
                            PickingMonitoring _locPickingMonitoring = _currSession.FindObject<PickingMonitoring>(new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("Picking", _locTransferOrderXPO.Picking),
                                                                new BinaryOperator("PickingLine", _locPickingLine)));
                            if(_locPickingMonitoring != null)
                            {
                                _locPickingMonitoring.TransferOrder = _locTransferOrderXPO;
                                _locPickingMonitoring.Save();
                                _locPickingMonitoring.Session.CommitTransaction();
                            }
                            #region SalesInvoiceMonitoring
                            //Hanya memindahkan SalesInvoiceMonitoring ke TransferOutLine
                            XPCollection<SalesInvoiceMonitoring> _locSalesInvoiceMonitorings = new XPCollection<SalesInvoiceMonitoring>
                                                                                                (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                new BinaryOperator("SalesInvoice", _locSalesInvoiceXPO),
                                                                                                new BinaryOperator("Picking", _locTransferOrderXPO.Picking),
                                                                                                new BinaryOperator("Select", true),
                                                                                                new BinaryOperator("Status", Status.Close, BinaryOperatorType.NotEqual)));

                            if (_locSalesInvoiceMonitorings != null && _locSalesInvoiceMonitorings.Count() > 0)
                            {
                                foreach (SalesInvoiceMonitoring _locSalesInvoiceMonitoring in _locSalesInvoiceMonitorings)
                                {

                                    if ((_locSalesInvoiceMonitoring.SalesInvoiceLine != null || _locSalesInvoiceMonitoring.SalesInvoiceFreeItem != null) 
                                        && _locSalesInvoiceMonitoring.TransferOutLine == null && _locSalesInvoiceMonitoring.Item != null)
                                    {
                                        _currSignCode = _globFunc.GetNumberingSignUnlockOptimisticRecordBasedOrganization(_currSession.DataLayer, ObjectList.TransferOrderLine, _locTransferOrderXPO.Company, _locTransferOrderXPO.Workplace);

                                        if (_currSignCode != null)
                                        {
                                            #region TransferOrderLine
                                            TransferOrderLine _saveDataTransferOrderLine = new TransferOrderLine(_currSession)
                                            {
                                                SignCode = _currSignCode,
                                                TransferOrder = _locTransferOrderXPO,
                                                Company = _locTransferOrderXPO.Company,
                                                Workplace = _locTransferOrderXPO.Workplace,
                                                Division = _locTransferOrderXPO.Division,
                                                Department = _locTransferOrderXPO.Department,
                                                Section = _locTransferOrderXPO.Section,
                                                Employee = _locTransferOrderXPO.Employee,
                                                LocationTypeFrom = _locTransferOrderXPO.LocationTypeFrom,
                                                WorkplaceFrom = _locSalesInvoiceMonitoring.Workplace,
                                                LocationFrom = _locSalesInvoiceMonitoring.Location,
                                                StockGroupFrom = _locSalesInvoiceMonitoring.StockGroup,
                                                StockTypeFrom = _locSalesInvoiceMonitoring.StockType,
                                                BegInvFrom = _locSalesInvoiceMonitoring.BegInv,
                                                BegInvLineFrom = _locSalesInvoiceMonitoring.BegInvLine,
                                                LocationTypeTo = _locTransferOrderXPO.LocationTypeTo,
                                                WorkplaceTo = _locTransferOrderXPO.WorkplaceTo,
                                                LocationTo = _locTransferOrderXPO.LocationTo,
                                                StockGroupTo = _locSalesInvoiceMonitoring.StockGroup,
                                                StockTypeTo = _locTransferOrderXPO.StockTypeTo,
                                                BegInvTo = _locTransferOrderXPO.BegInvTo,
                                                Item = _locSalesInvoiceMonitoring.Item,
                                                Description = _locSalesInvoiceMonitoring.Description,
                                                DQty = _locSalesInvoiceMonitoring.DQty,
                                                DUOM = _locSalesInvoiceMonitoring.DUOM,
                                                Qty = _locSalesInvoiceMonitoring.Qty,
                                                UOM = _locSalesInvoiceMonitoring.UOM,
                                                TQty = _locSalesInvoiceMonitoring.TQty,
                                                SalesInvoiceMonitoring = _locSalesInvoiceMonitoring,
                                                SalesInvoice = _locSalesInvoiceMonitoring.SalesInvoice,
                                                ETD = _locSalesInvoiceMonitoring.ETD,
                                                ETA = _locSalesInvoiceMonitoring.ETA,
                                                DocDate = _locSalesInvoiceMonitoring.DocDate,
                                                JournalMonth = _locSalesInvoiceMonitoring.JournalMonth,
                                                JournalYear = _locSalesInvoiceMonitoring.JournalYear,
                                            };
                                            _saveDataTransferOrderLine.Save();
                                            _saveDataTransferOrderLine.Session.CommitTransaction();
                                            #endregion TransferOrderLine

                                            TransferOrderLine _locTransOrderLine = _currSession.FindObject<TransferOrderLine>
                                                                        (new GroupOperator(GroupOperatorType.And,
                                                                        new BinaryOperator("Company", _locTransferOrderXPO),
                                                                        new BinaryOperator("TransferOrder", _locTransferOrderXPO),
                                                                        new BinaryOperator("SignCode", _currSignCode)));
                                            if (_locTransOrderLine != null)
                                            {
                                                SetRemainQty(_currSession, _locSalesInvoiceMonitoring);
                                                SetPostingQty(_currSession, _locSalesInvoiceMonitoring);
                                                SetProcessCount(_currSession, _locSalesInvoiceMonitoring);
                                                SetStatusSalesInvoiceMonitoring(_currSession, _locSalesInvoiceMonitoring);
                                                SetNormalQuantity(_currSession, _locSalesInvoiceMonitoring);
                                            }
                                        }

                                        _locSalesInvoiceMonitoring.PickingLine = _locPickingLine;
                                        _locSalesInvoiceMonitoring.Save();
                                        _locSalesInvoiceMonitoring.Session.CommitTransaction();
                                    }
                                }
                            }
                            #endregion SalesInvoiceMonitoring
                        }
                    }
                    #endregion PickingLine
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = TransferOrder ", ex.ToString());
            }
        }

        private void SetRemainQty(Session _currSession, SalesInvoiceMonitoring _locSalesInvoiceMonitoringXPO)
        {
            try
            {
                if (_locSalesInvoiceMonitoringXPO != null)
                {
                    double _locRmDQty = 0;
                    double _locRmQty = 0;
                    double _locInvLineTotal = 0;
                    ItemUnitOfMeasure _locItemUOM = null;

                    #region ProcessCount=0
                    if (_locSalesInvoiceMonitoringXPO.PostedCount == 0)
                    {
                        #region MaxQuantity
                        if (_locSalesInvoiceMonitoringXPO.MxDQty > 0)
                        {
                            if (_locSalesInvoiceMonitoringXPO.DQty > 0 && _locSalesInvoiceMonitoringXPO.DQty <= _locSalesInvoiceMonitoringXPO.MxDQty)
                            {
                                _locRmDQty = _locSalesInvoiceMonitoringXPO.MxDQty - _locSalesInvoiceMonitoringXPO.DQty;
                            }

                            if (_locSalesInvoiceMonitoringXPO.Qty > 0 && _locSalesInvoiceMonitoringXPO.Qty <= _locSalesInvoiceMonitoringXPO.MxQty)
                            {
                                _locRmQty = _locSalesInvoiceMonitoringXPO.MxQty - _locSalesInvoiceMonitoringXPO.Qty;
                            }

                            _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                       (new GroupOperator(GroupOperatorType.And,
                                                        new BinaryOperator("Item", _locSalesInvoiceMonitoringXPO.Item),
                                                        new BinaryOperator("UOM", _locSalesInvoiceMonitoringXPO.MxUOM),
                                                        new BinaryOperator("DefaultUOM", _locSalesInvoiceMonitoringXPO.MxDUOM),
                                                        new BinaryOperator("Active", true)));
                            if (_locItemUOM != null)
                            {
                                if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                {
                                    _locInvLineTotal = _locRmQty * _locItemUOM.DefaultConversion + _locRmDQty;
                                }
                                else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                {
                                    _locInvLineTotal = _locRmQty / _locItemUOM.Conversion + _locRmDQty;
                                }
                                else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                {
                                    _locInvLineTotal = _locRmQty + _locRmDQty;
                                }
                            }
                            else
                            {
                                _locInvLineTotal = _locRmQty + _locRmDQty;
                            }
                        }
                        #endregion MaxQuantity

                        #region NonMaxQuantity
                        else
                        {
                            if (_locSalesInvoiceMonitoringXPO.DQty > 0)
                            {
                                _locRmDQty = 0;
                            }

                            if (_locSalesInvoiceMonitoringXPO.Qty > 0)
                            {
                                _locRmQty = 0;
                            }

                            _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                       (new GroupOperator(GroupOperatorType.And,
                                                        new BinaryOperator("Item", _locSalesInvoiceMonitoringXPO.Item),
                                                        new BinaryOperator("UOM", _locSalesInvoiceMonitoringXPO.UOM),
                                                        new BinaryOperator("DefaultUOM", _locSalesInvoiceMonitoringXPO.DUOM),
                                                        new BinaryOperator("Active", true)));
                            if (_locItemUOM != null)
                            {
                                if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                {
                                    _locInvLineTotal = _locRmQty * _locItemUOM.DefaultConversion + _locRmDQty;
                                }
                                else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                {
                                    _locInvLineTotal = _locRmQty / _locItemUOM.Conversion + _locRmDQty;
                                }
                                else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                {
                                    _locInvLineTotal = _locRmQty + _locRmDQty;
                                }
                            }
                            else
                            {
                                _locInvLineTotal = _locRmQty + _locRmDQty;
                            }
                        }
                        #endregion NonMaxQuantity
                    }
                    #endregion ProcessCount=0

                    #region ProcessCount>0
                    if (_locSalesInvoiceMonitoringXPO.PostedCount > 0)
                    {
                        if (_locSalesInvoiceMonitoringXPO.RmDQty > 0)
                        {
                            _locRmDQty = _locSalesInvoiceMonitoringXPO.RmDQty - _locSalesInvoiceMonitoringXPO.DQty;
                        }

                        if (_locSalesInvoiceMonitoringXPO.RmQty > 0)
                        {
                            _locRmQty = _locSalesInvoiceMonitoringXPO.RmQty - _locSalesInvoiceMonitoringXPO.Qty;
                        }

                        if (_locSalesInvoiceMonitoringXPO.MxDQty > 0 || _locSalesInvoiceMonitoringXPO.MxQty > 0)
                        {
                            _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                       (new GroupOperator(GroupOperatorType.And,
                                                        new BinaryOperator("Item", _locSalesInvoiceMonitoringXPO.Item),
                                                        new BinaryOperator("UOM", _locSalesInvoiceMonitoringXPO.MxUOM),
                                                        new BinaryOperator("DefaultUOM", _locSalesInvoiceMonitoringXPO.MxDUOM),
                                                        new BinaryOperator("Active", true)));
                        }
                        else
                        {
                            _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                       (new GroupOperator(GroupOperatorType.And,
                                                        new BinaryOperator("Item", _locSalesInvoiceMonitoringXPO.Item),
                                                        new BinaryOperator("UOM", _locSalesInvoiceMonitoringXPO.UOM),
                                                        new BinaryOperator("DefaultUOM", _locSalesInvoiceMonitoringXPO.DUOM),
                                                        new BinaryOperator("Active", true)));
                        }


                        if (_locItemUOM != null)
                        {
                            if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                            {
                                _locInvLineTotal = _locRmQty * _locItemUOM.DefaultConversion + _locRmDQty;
                            }
                            else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                            {
                                _locInvLineTotal = _locRmQty / _locItemUOM.Conversion + _locRmDQty;
                            }
                            else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                            {
                                _locInvLineTotal = _locRmQty + _locRmDQty;
                            }
                        }
                        else
                        {
                            _locInvLineTotal = _locRmQty + _locRmDQty;
                        }

                    }
                    #endregion ProcessCount>0

                    _locSalesInvoiceMonitoringXPO.RmDQty = _locRmDQty;
                    _locSalesInvoiceMonitoringXPO.RmQty = _locRmQty;
                    _locSalesInvoiceMonitoringXPO.RmTQty = _locInvLineTotal;
                    _locSalesInvoiceMonitoringXPO.Save();
                    _locSalesInvoiceMonitoringXPO.Session.CommitTransaction();

                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = TransferOrder " + ex.ToString());
            }
        }

        private void SetPostingQty(Session _currSession, SalesInvoiceMonitoring _locSalesInvoiceMonitoringXPO)
        {
            try
            {
                if (_locSalesInvoiceMonitoringXPO != null)
                {
                    double _locPDQty = 0;
                    double _locPQty = 0;
                    double _locInvLineTotal = 0;
                    ItemUnitOfMeasure _locItemUOM = null;

                    #region ProcessCount=0
                    if (_locSalesInvoiceMonitoringXPO.PostedCount == 0)
                    {
                        #region MaxQuantity
                        if (_locSalesInvoiceMonitoringXPO.MxDQty > 0)
                        {
                            if (_locSalesInvoiceMonitoringXPO.DQty > 0 && _locSalesInvoiceMonitoringXPO.DQty <= _locSalesInvoiceMonitoringXPO.MxDQty)
                            {
                                _locPDQty = _locSalesInvoiceMonitoringXPO.DQty;
                            }

                            if (_locSalesInvoiceMonitoringXPO.Qty > 0 && _locSalesInvoiceMonitoringXPO.Qty <= _locSalesInvoiceMonitoringXPO.MxQty)
                            {
                                _locPQty = _locSalesInvoiceMonitoringXPO.Qty;
                            }

                            _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                       (new GroupOperator(GroupOperatorType.And,
                                                        new BinaryOperator("Item", _locSalesInvoiceMonitoringXPO.Item),
                                                        new BinaryOperator("UOM", _locSalesInvoiceMonitoringXPO.MxUOM),
                                                        new BinaryOperator("DefaultUOM", _locSalesInvoiceMonitoringXPO.MxDUOM),
                                                        new BinaryOperator("Active", true)));
                            if (_locItemUOM != null)
                            {
                                if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                {
                                    _locInvLineTotal = _locPQty * _locItemUOM.DefaultConversion + _locPDQty;
                                }
                                else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                {
                                    _locInvLineTotal = _locPQty / _locItemUOM.Conversion + _locPDQty;
                                }
                                else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                {
                                    _locInvLineTotal = _locPQty + _locPDQty;
                                }
                            }
                            else
                            {
                                _locInvLineTotal = _locPQty + _locPDQty;
                            }
                        }
                        #endregion MaxQuantity

                        #region NonMaxQuantity
                        else
                        {
                            if (_locSalesInvoiceMonitoringXPO.DQty > 0)
                            {
                                _locPDQty = _locSalesInvoiceMonitoringXPO.DQty;
                            }

                            if (_locSalesInvoiceMonitoringXPO.Qty > 0)
                            {
                                _locPQty = _locSalesInvoiceMonitoringXPO.Qty;
                            }

                            _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                       (new GroupOperator(GroupOperatorType.And,
                                                        new BinaryOperator("Item", _locSalesInvoiceMonitoringXPO.Item),
                                                        new BinaryOperator("UOM", _locSalesInvoiceMonitoringXPO.UOM),
                                                        new BinaryOperator("DefaultUOM", _locSalesInvoiceMonitoringXPO.DUOM),
                                                        new BinaryOperator("Active", true)));
                            if (_locItemUOM != null)
                            {
                                if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                {
                                    _locInvLineTotal = _locPQty * _locItemUOM.DefaultConversion + _locPDQty;
                                }
                                else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                {
                                    _locInvLineTotal = _locPQty / _locItemUOM.Conversion + _locPDQty;
                                }
                                else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                {
                                    _locInvLineTotal = _locPQty + _locPDQty;
                                }
                            }
                            else
                            {
                                _locInvLineTotal = _locPQty + _locPDQty;
                            }
                        }
                        #endregion NonMaxQuantity
                    }
                    #endregion ProcessCount=0

                    #region ProcessCount>0
                    if (_locSalesInvoiceMonitoringXPO.PostedCount > 0)
                    {
                        if (_locSalesInvoiceMonitoringXPO.PDQty > 0)
                        {
                            _locPDQty = _locSalesInvoiceMonitoringXPO.PDQty + _locSalesInvoiceMonitoringXPO.DQty;
                        }

                        if (_locSalesInvoiceMonitoringXPO.PQty > 0)
                        {
                            _locPQty = _locSalesInvoiceMonitoringXPO.PQty + _locSalesInvoiceMonitoringXPO.Qty;
                        }

                        if (_locSalesInvoiceMonitoringXPO.MxDQty > 0 || _locSalesInvoiceMonitoringXPO.MxQty > 0)
                        {
                            _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                   (new GroupOperator(GroupOperatorType.And,
                                                    new BinaryOperator("Item", _locSalesInvoiceMonitoringXPO.Item),
                                                    new BinaryOperator("UOM", _locSalesInvoiceMonitoringXPO.MxUOM),
                                                    new BinaryOperator("DefaultUOM", _locSalesInvoiceMonitoringXPO.MxDUOM),
                                                    new BinaryOperator("Active", true)));
                        }
                        else
                        {
                            _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                   (new GroupOperator(GroupOperatorType.And,
                                                    new BinaryOperator("Item", _locSalesInvoiceMonitoringXPO.Item),
                                                    new BinaryOperator("UOM", _locSalesInvoiceMonitoringXPO.UOM),
                                                    new BinaryOperator("DefaultUOM", _locSalesInvoiceMonitoringXPO.DUOM),
                                                    new BinaryOperator("Active", true)));
                        }

                        if (_locItemUOM != null)
                        {
                            if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                            {
                                _locInvLineTotal = _locPQty * _locItemUOM.DefaultConversion + _locPDQty;
                            }
                            else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                            {
                                _locInvLineTotal = _locPQty / _locItemUOM.Conversion + _locPDQty;
                            }
                            else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                            {
                                _locInvLineTotal = _locPQty + _locPDQty;
                            }
                        }
                        else
                        {
                            _locInvLineTotal = _locPQty + _locPDQty;
                        }
                    }
                    #endregion ProcessCount>0

                    _locSalesInvoiceMonitoringXPO.PDQty = _locPDQty;
                    _locSalesInvoiceMonitoringXPO.PDUOM = _locSalesInvoiceMonitoringXPO.DUOM;
                    _locSalesInvoiceMonitoringXPO.PQty = _locPQty;
                    _locSalesInvoiceMonitoringXPO.PUOM = _locSalesInvoiceMonitoringXPO.UOM;
                    _locSalesInvoiceMonitoringXPO.PTQty = _locInvLineTotal;
                    _locSalesInvoiceMonitoringXPO.Save();
                    _locSalesInvoiceMonitoringXPO.Session.CommitTransaction();

                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = TransferOrder " + ex.ToString());
            }
        }

        private void SetProcessCount(Session _currSession, SalesInvoiceMonitoring _locSalesInvoiceMonitoringXPO)
        {
            try
            {
                if (_locSalesInvoiceMonitoringXPO != null)
                {
                    _locSalesInvoiceMonitoringXPO.PostedCount = _locSalesInvoiceMonitoringXPO.PostedCount + 1;
                    _locSalesInvoiceMonitoringXPO.Save();
                    _locSalesInvoiceMonitoringXPO.Session.CommitTransaction();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = TransferOrder " + ex.ToString());
            }
        }

        private void SetStatusSalesInvoiceMonitoring(Session _currSession, SalesInvoiceMonitoring _locSalesInvoiceMonitoringXPO)
        {
            try
            {
                DateTime now = DateTime.Now;
                if (_locSalesInvoiceMonitoringXPO != null)
                {
                    if (_locSalesInvoiceMonitoringXPO.RmDQty == 0 && _locSalesInvoiceMonitoringXPO.RmQty == 0 && _locSalesInvoiceMonitoringXPO.RmTQty == 0)
                    {
                        _locSalesInvoiceMonitoringXPO.Status = Status.Close;
                        _locSalesInvoiceMonitoringXPO.ActivationPosting = true;
                        _locSalesInvoiceMonitoringXPO.StatusDate = now;
                    }
                    else
                    {
                        _locSalesInvoiceMonitoringXPO.Status = Status.Posted;
                        _locSalesInvoiceMonitoringXPO.StatusDate = now;
                    }
                    _locSalesInvoiceMonitoringXPO.Save();
                    _locSalesInvoiceMonitoringXPO.Session.CommitTransaction();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = TransferOrder " + ex.ToString());
            }
        }

        private void SetNormalQuantity(Session _currSession, SalesInvoiceMonitoring _locSalesInvoiceMonitoringXPO)
        {
            try
            {
                if (_locSalesInvoiceMonitoringXPO != null)
                {
                    if (_locSalesInvoiceMonitoringXPO.DQty > 0 || _locSalesInvoiceMonitoringXPO.Qty > 0)
                    {
                        _locSalesInvoiceMonitoringXPO.Select = false;
                        _locSalesInvoiceMonitoringXPO.DQty = 0;
                        _locSalesInvoiceMonitoringXPO.Qty = 0;
                        _locSalesInvoiceMonitoringXPO.Save();
                        _locSalesInvoiceMonitoringXPO.Session.CommitTransaction();
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = TransferOrder " + ex.ToString());
            }
        }

        #endregion GetSIM
        #region GetBegInv

        //Yg Login Salesman dgn begInv Vehicle
        private void GetBeginningInventory(Session _currSession, TransferOrder _locTransferOrderXPO)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                DateTime now = DateTime.Now;
                
                if (_locTransferOrderXPO != null)
                {
                    if (_locTransferOrderXPO != null)
                    {
                        if (_locTransferOrderXPO.BegInvFrom != null)
                        {

                            XPCollection<BeginningInventoryLine> _locBegInventoryLines = new XPCollection<BeginningInventoryLine>(_currSession,
                                                                                        new GroupOperator(GroupOperatorType.And,
                                                                                        new BinaryOperator("BeginningInventory", _locTransferOrderXPO.BegInvFrom),
                                                                                        new BinaryOperator("QtyAvailable", 0, BinaryOperatorType.Greater),
                                                                                        new BinaryOperator("Lock", false),
                                                                                        new BinaryOperator("Active", true)));


                            if (_locBegInventoryLines != null && _locBegInventoryLines.Count() > 0)
                            {
                                foreach (BeginningInventoryLine _locBegInventoryLine in _locBegInventoryLines)
                                {
                                    double _locCmbTQty = 0;
                                    double _locTQty = 0;
                                    double _locDQty = 0;
                                    double _locQty = 0;
                                    UnitOfMeasure _locDUOM = null;
                                    UnitOfMeasure _locUOM = null;
                                    if (_locTransferOrderXPO.Company != null && _locBegInventoryLine.Item != null)
                                    {
                                        _locCmbTQty = _locBegInventoryLine.QtyAvailable;

                                        ItemUnitOfMeasure _locIUOM = _currSession.FindObject<ItemUnitOfMeasure>(new GroupOperator(GroupOperatorType.And,
                                                                                                new BinaryOperator("Company", _locTransferOrderXPO.Company),
                                                                                                new BinaryOperator("Item", _locBegInventoryLine.Item),
                                                                                                new BinaryOperator("DefaultUOM", _locBegInventoryLine.DefaultUOM),
                                                                                                new BinaryOperator("Default", true),
                                                                                                new BinaryOperator("Active", true)));

                                        if (_locIUOM.Item != null && _locIUOM.DefaultUOM != null && _locIUOM.UOM != null)
                                        {
                                            _locDUOM = _locIUOM.DefaultUOM;
                                            _locUOM = _locIUOM.UOM;
                                            if (_locIUOM.Conversion < _locIUOM.DefaultConversion)
                                            {
                                                _locDQty = _locCmbTQty % _locIUOM.DefaultConversion;
                                                _locQty = System.Math.Floor(_locCmbTQty / _locIUOM.DefaultConversion);
                                                _locTQty = _locCmbTQty;
                                            }
                                            else if (_locIUOM.Conversion > _locIUOM.DefaultConversion)
                                            {
                                                _locDQty = System.Math.Floor(_locCmbTQty / _locIUOM.DefaultConversion);
                                                _locQty = (_locCmbTQty % _locIUOM.DefaultConversion) * _locIUOM.Conversion;
                                                _locTQty = _locCmbTQty;
                                            }
                                            else if (_locIUOM.Conversion == _locIUOM.DefaultConversion)
                                            {
                                                _locDQty = _locCmbTQty;
                                                _locTQty = _locCmbTQty;
                                            }

                                        }
                                        else
                                        {
                                            _locDUOM = _locBegInventoryLine.DefaultUOM;
                                            _locDQty = _locCmbTQty;
                                            _locTQty = _locCmbTQty;
                                        }

                                    }

                                    #region TransferOrderLine
                                    TransferOrderLine _saveDataTransferOrderLine = new TransferOrderLine(_currSession)
                                    {
                                        TransferOrder = _locTransferOrderXPO,
                                        Company = _locTransferOrderXPO.Company,
                                        Division = _locTransferOrderXPO.Division,
                                        Department = _locTransferOrderXPO.Department,
                                        Section = _locTransferOrderXPO.Section,
                                        Employee = _locTransferOrderXPO.Employee,
                                        LocationTypeFrom = _locTransferOrderXPO.LocationTypeFrom,
                                        StockTypeFrom = _locBegInventoryLine.StockType,
                                        WorkplaceFrom = _locTransferOrderXPO.WorkplaceFrom,
                                        LocationFrom = _locTransferOrderXPO.LocationFrom,
                                        StockGroupFrom = _locBegInventoryLine.StockGroup,
                                        BegInvFrom = _locTransferOrderXPO.BegInvFrom,
                                        LocationTypeTo = _locTransferOrderXPO.LocationTypeTo,
                                        StockTypeTo = _locTransferOrderXPO.StockTypeTo,
                                        WorkplaceTo = _locTransferOrderXPO.WorkplaceTo,
                                        LocationTo = _locTransferOrderXPO.LocationTo,
                                        StockGroupTo = _locBegInventoryLine.StockGroup,
                                        BegInvTo = _locTransferOrderXPO.BegInvTo,
                                        Item = _locBegInventoryLine.Item,
                                        Description = _locBegInventoryLine.Description,
                                        BegInvLineFrom = _locBegInventoryLine,
                                        DQty = _locDQty,
                                        DUOM = _locBegInventoryLine.DefaultUOM,
                                        Qty = _locQty,
                                        UOM = _locUOM,
                                        TQty = _locTQty,

                                    };
                                    _saveDataTransferOrderLine.Save();
                                    _saveDataTransferOrderLine.Session.CommitTransaction();
                                    #endregion TransferOrderLine
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = TransferOrder ", ex.ToString());
            }
        }

        #endregion GetBegInv

        #endregion GetStock

        #region Progress

        private void SetBegInvLineTo(Session _currSession, TransferOrder _locTransferOrderXPO)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                string _locSignCode = null;

                if (_locTransferOrderXPO != null)
                {
                    XPCollection<TransferOrderLine> _locTransferOrderLines = new XPCollection<TransferOrderLine>(_currSession,
                                                                        new GroupOperator(GroupOperatorType.And,
                                                                        new BinaryOperator("TransferOrder", _locTransferOrderXPO),
                                                                        new GroupOperator(GroupOperatorType.Or,
                                                                        new BinaryOperator("Status", Status.Open),
                                                                        new BinaryOperator("Status", Status.Progress))));

                    if (_locTransferOrderLines != null && _locTransferOrderLines.Count() > 0)
                    {
                        foreach (TransferOrderLine _locTransferOrderLine in _locTransferOrderLines)
                        {

                            if (_locTransferOrderLine.Company != null && _locTransferOrderLine.WorkplaceTo != null && _locTransferOrderLine.BegInvTo != null
                                && _locTransferOrderLine.Item != null && _locTransferOrderLine.BegInvLineTo == null)
                            {
                                BeginningInventoryLine _locBegInvLine = null;

                                if (_locTransferOrderLine.DUOM != null)
                                {
                                    #region Update
                                    if (_locTransferOrderLine.BinLocationTo != null)
                                    {
                                        //DefaultUOM
                                        _locBegInvLine = _currSession.FindObject<BeginningInventoryLine>(
                                                   new GroupOperator(GroupOperatorType.And,
                                                   new BinaryOperator("Company", _locTransferOrderLine.Company),
                                                   new BinaryOperator("Workplace", _locTransferOrderLine.WorkplaceTo),
                                                   new BinaryOperator("BeginningInventory", _locTransferOrderLine.BegInvTo),
                                                   new BinaryOperator("Item", _locTransferOrderLine.Item),
                                                   new BinaryOperator("DefaultUOM", _locTransferOrderLine.DUOM),
                                                   new BinaryOperator("BinLocation", _locTransferOrderLine.BinLocationTo),
                                                   new BinaryOperator("LocationType", _locTransferOrderLine.LocationTypeTo),
                                                   new BinaryOperator("StockType", _locTransferOrderLine.StockTypeTo),
                                                   new BinaryOperator("StockGroup", _locTransferOrderLine.StockGroupTo),
                                                   new BinaryOperator("Lock", false),
                                                   new BinaryOperator("Active", true)));
                                    }
                                    else
                                    {
                                        _locBegInvLine = _currSession.FindObject<BeginningInventoryLine>(
                                                   new GroupOperator(GroupOperatorType.And,
                                                   new BinaryOperator("Company", _locTransferOrderLine.Company),
                                                   new BinaryOperator("Workplace", _locTransferOrderLine.WorkplaceTo),
                                                   new BinaryOperator("BeginningInventory", _locTransferOrderLine.BegInvTo),
                                                   new BinaryOperator("Item", _locTransferOrderLine.Item),
                                                   new BinaryOperator("DefaultUOM", _locTransferOrderLine.DUOM),
                                                   new BinaryOperator("LocationType", _locTransferOrderLine.LocationTypeTo),
                                                   new BinaryOperator("StockType", _locTransferOrderLine.StockTypeTo),
                                                   new BinaryOperator("StockGroup", _locTransferOrderLine.StockGroupTo),
                                                   new BinaryOperator("Lock", false),
                                                   new BinaryOperator("Active", true)));
                                    }

                                    if (_locBegInvLine != null)
                                    {
                                        _locTransferOrderLine.BegInvLineTo = _locBegInvLine;
                                        _locTransferOrderLine.Save();
                                        _locTransferOrderLine.Session.CommitTransaction();
                                    }
                                    #endregion Update
                                    #region CreateNew
                                    else
                                    {
                                        _locSignCode = _globFunc.GetNumberingSignUnlockOptimisticRecordBasedOrganization(_currSession.DataLayer, ObjectList.BeginningInventoryLine, _locTransferOrderLine.Company, _locTransferOrderLine.WorkplaceTo);
                                        if (_locSignCode != null)
                                        {
                                            BeginningInventoryLine _locSaveDataBeginingInventoryLine = new BeginningInventoryLine(_currSession)
                                            {
                                                SignCode = _locSignCode,
                                                Company = _locTransferOrderXPO.Company,
                                                Workplace = _locTransferOrderXPO.WorkplaceTo,
                                                Item = _locTransferOrderLine.Item,
                                                Location = _locTransferOrderLine.LocationTo,
                                                Vehicle = _locTransferOrderXPO.Vehicle,
                                                BinLocation = _locTransferOrderLine.BinLocationTo,
                                                DefaultUOM = _locTransferOrderLine.DUOM,
                                                LocationType = _locTransferOrderLine.LocationTypeTo,
                                                StockType = _locTransferOrderLine.StockTypeTo,
                                                StockGroup = _locTransferOrderLine.StockGroupTo,
                                                Lock = false,
                                                Active = true,
                                                BeginningInventory = _locTransferOrderLine.BegInvTo,
                                            };

                                            _locSaveDataBeginingInventoryLine.Save();
                                            _locSaveDataBeginingInventoryLine.Session.CommitTransaction();

                                            BeginningInventoryLine _locBegInvLine2 = _currSession.FindObject<BeginningInventoryLine>(new GroupOperator
                                                                                    (GroupOperatorType.And,
                                                                                    new BinaryOperator("Company", _locTransferOrderLine.Company),
                                                                                    new BinaryOperator("BeginningInventory", _locTransferOrderLine.BegInvTo),
                                                                                    new BinaryOperator("SignCode", _locSignCode)));
                                            if (_locBegInvLine2 != null)
                                            {
                                                _locTransferOrderLine.BegInvLineTo = _locBegInvLine2;
                                                _locTransferOrderLine.Save();
                                                _locTransferOrderLine.Session.CommitTransaction();
                                            }
                                        }
                                    }
                                    #endregion CreateNew
                                }

                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Module = TransferOrder " + ex.ToString());
            }
        }

        #endregion Progress

        #region Posting

        private bool CheckAvailableStock(Session _currSession, TransferOrder _locTransferOrderXPO)
        {
            bool _result = true;
            try
            {
                if (_locTransferOrderXPO != null)
                {
                    XPCollection<TransferOrderLine> _locTransOrderLines = new XPCollection<TransferOrderLine>(_currSession,
                                                                        new GroupOperator(GroupOperatorType.And,
                                                                        new BinaryOperator("TransferOrder", _locTransferOrderXPO),
                                                                        new BinaryOperator("Select", true),
                                                                        new GroupOperator(GroupOperatorType.Or,
                                                                        new BinaryOperator("Status", Status.Progress),
                                                                        new BinaryOperator("Status", Status.Posted))));

                    if (_locTransOrderLines != null && _locTransOrderLines.Count > 0)
                    {

                        foreach (TransferOrderLine _locTransOrderLine in _locTransOrderLines)
                        {
                            if (_locTransOrderLine.BegInvFrom != null)
                            {
                                if (_locTransOrderLine.BegInvLineFrom != null)
                                {
                                    if (_locTransOrderLine.TQty > _locTransOrderLine.BegInvLineFrom.QtyAvailable)
                                    {
                                        _result = false;
                                    }
                                }
                            }
                            
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = TransferOrder ", ex.ToString());
            }
            return _result;
        }

        private bool CheckLockStock(Session _currSession, TransferOrder _locTransferOrderXPO)
        {
            bool _result = false;
            try
            {
                bool _locLock = false;

                if (_locTransferOrderXPO != null)
                {
                    XPCollection<TransferOrderLine> _locTransferOrderLines = new XPCollection<TransferOrderLine>(_currSession,
                                                                        new GroupOperator(GroupOperatorType.And,
                                                                        new BinaryOperator("TransferOrder", _locTransferOrderXPO),
                                                                        new GroupOperator(GroupOperatorType.Or,
                                                                        new BinaryOperator("Status", Status.Progress),
                                                                        new BinaryOperator("Status", Status.Posted))));
                    if (_locTransferOrderLines != null && _locTransferOrderLines.Count() > 0)
                    {
                        foreach (TransferOrderLine _locTransferOrderLine in _locTransferOrderLines)
                        {
                            if (_locTransferOrderLine.BegInvLineFrom != null)
                            {
                                if (_locTransferOrderLine.BegInvLineFrom.Lock == true)
                                {
                                    _locLock = true;
                                }
                            }
                        }

                        _result = _locLock;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Module = TransferOrder " + ex.ToString());
            }
            return _result;
        }

        private void SetDeliverBeginingInventory(Session _currSession, TransferOrder _locTransferOrderXPO)
        {
            try
            {
                XPCollection<TransferOrderLine> _locTransferOrderLines = new XPCollection<TransferOrderLine>(_currSession,
                                                                        new GroupOperator(GroupOperatorType.And,
                                                                        new BinaryOperator("TransferOrder", _locTransferOrderXPO),
                                                                        new BinaryOperator("Select", true),
                                                                        new GroupOperator(GroupOperatorType.Or,
                                                                        new BinaryOperator("Status", Status.Progress),
                                                                        new BinaryOperator("Status", Status.Posted))));

                if (_locTransferOrderLines != null && _locTransferOrderLines.Count() > 0)
                {
                    GlobalFunction _globFunc = new GlobalFunction();

                    foreach (TransferOrderLine _locTransferOrderLine in _locTransferOrderLines)
                    {
                        if (_locTransferOrderLine.DQty > 0 || _locTransferOrderLine.Qty > 0)
                        {
                            #region BeginningInventoryFrom
                            if (_locTransferOrderLine.BegInvFrom != null && _locTransferOrderLine.BegInvLineFrom != null)
                            {
                                _locTransferOrderLine.BegInvLineFrom.QtyAvailable = _locTransferOrderLine.BegInvLineFrom.QtyAvailable - _locTransferOrderLine.TQty;
                                _locTransferOrderLine.BegInvLineFrom.Save();
                                _locTransferOrderLine.BegInvLineFrom.Session.CommitTransaction();

                                _locTransferOrderLine.BegInvFrom.QtyAvailable = _locTransferOrderLine.BegInvFrom.QtyAvailable - _locTransferOrderLine.TQty;
                                _locTransferOrderLine.BegInvFrom.Save();
                                _locTransferOrderLine.BegInvFrom.Session.CommitTransaction();

                            }
                            #endregion BeginningInventoryFrom
                            #region BeginningInventoryTo
                            if (_locTransferOrderLine.BegInvTo != null && _locTransferOrderLine.BegInvLineTo != null)
                            {
                                _locTransferOrderLine.BegInvLineTo.QtyAvailable = _locTransferOrderLine.BegInvLineTo.QtyAvailable + _locTransferOrderLine.TQty;
                                _locTransferOrderLine.BegInvLineTo.Save();
                                _locTransferOrderLine.BegInvLineTo.Session.CommitTransaction();

                                _locTransferOrderLine.BegInvTo.QtyAvailable = _locTransferOrderLine.BegInvTo.QtyAvailable + _locTransferOrderLine.TQty;
                                _locTransferOrderLine.BegInvTo.Save();
                                _locTransferOrderLine.BegInvTo.Session.CommitTransaction();
                            }
                            #endregion BeginningInventoryTo
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = TransferOrder ", ex.ToString());
            }
        }

        private void SetDeliverInventoryJournal(Session _currSession, TransferOrder _locTransferOrderXPO)
        {
            try
            {
                XPCollection<TransferOrderLine> _locTransferOrderLines = new XPCollection<TransferOrderLine>(_currSession,
                                                                        new GroupOperator(GroupOperatorType.And,
                                                                        new BinaryOperator("TransferOrder", _locTransferOrderXPO),
                                                                        new BinaryOperator("Select", true),
                                                                        new GroupOperator(GroupOperatorType.Or,
                                                                        new BinaryOperator("Status", Status.Progress),
                                                                        new BinaryOperator("Status", Status.Posted))));

                if (_locTransferOrderLines != null && _locTransferOrderLines.Count > 0)
                {
                    double _locInvLineTotal = 0;
                    DateTime now = DateTime.Now;

                    foreach (TransferOrderLine _locTransferOrderLine in _locTransferOrderLines)
                    {
                        if (_locTransferOrderLine.DQty > 0 || _locTransferOrderLine.Qty > 0)
                        {
                            if (_locTransferOrderLine.Item != null && _locTransferOrderLine.UOM != null && _locTransferOrderLine.DUOM != null)
                            {
                                ItemUnitOfMeasure _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                    (new GroupOperator(GroupOperatorType.And,
                                                     new BinaryOperator("Item", _locTransferOrderLine.Item),
                                                     new BinaryOperator("UOM", _locTransferOrderLine.UOM),
                                                     new BinaryOperator("DefaultUOM", _locTransferOrderLine.DUOM),
                                                     new BinaryOperator("Active", true)));
                                if (_locItemUOM != null)
                                {
                                    #region Code
                                    if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                    {
                                        _locInvLineTotal = _locTransferOrderLine.Qty * _locItemUOM.DefaultConversion + _locTransferOrderLine.DQty;
                                    }
                                    else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                    {
                                        _locInvLineTotal = _locTransferOrderLine.Qty / _locItemUOM.Conversion + _locTransferOrderLine.DQty;
                                    }
                                    else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                    {
                                        _locInvLineTotal = _locTransferOrderLine.Qty + _locTransferOrderLine.DQty;
                                    }
                                    #endregion Code

                                    #region Negatif
                                    InventoryJournal _locNegatifInventoryJournal = new InventoryJournal(_currSession)
                                    {
                                        DocumentType = _locTransferOrderXPO.DocumentType,
                                        DocNo = _locTransferOrderXPO.DocNo,
                                        Location = _locTransferOrderLine.LocationFrom,
                                        BinLocation = _locTransferOrderLine.BinLocationFrom,
                                        LocationType = _locTransferOrderLine.LocationTypeFrom,
                                        StockType = _locTransferOrderLine.StockTypeFrom,
                                        StockGroup = _locTransferOrderLine.StockGroupFrom,
                                        Item = _locTransferOrderLine.Item,
                                        Company = _locTransferOrderLine.Company,
                                        Workplace = _locTransferOrderLine.WorkplaceFrom,
                                        QtyPos = 0,
                                        QtyNeg = _locInvLineTotal,
                                        DUOM = _locTransferOrderLine.DUOM,
                                        JournalDate = now,
                                        TransferOrder = _locTransferOrderXPO,
                                    };
                                    _locNegatifInventoryJournal.Save();
                                    _locNegatifInventoryJournal.Session.CommitTransaction();
                                    #endregion Negatif
                                    #region Positif
                                    InventoryJournal _locPositifInventoryJournal = new InventoryJournal(_currSession)
                                    {
                                        DocumentType = _locTransferOrderXPO.DocumentType,
                                        DocNo = _locTransferOrderXPO.DocNo,
                                        Location = _locTransferOrderLine.LocationTo,
                                        BinLocation = _locTransferOrderLine.BinLocationTo,
                                        LocationType = _locTransferOrderLine.LocationTypeTo,
                                        StockType = _locTransferOrderLine.StockTypeTo,
                                        StockGroup = _locTransferOrderLine.StockGroupTo,
                                        Item = _locTransferOrderLine.Item,
                                        Company = _locTransferOrderLine.Company,
                                        Workplace = _locTransferOrderLine.WorkplaceTo,
                                        QtyPos = _locInvLineTotal,
                                        QtyNeg = 0,
                                        DUOM = _locTransferOrderLine.DUOM,
                                        JournalDate = now,
                                        TransferOrder = _locTransferOrderXPO,
                                    };
                                    _locPositifInventoryJournal.Save();
                                    _locPositifInventoryJournal.Session.CommitTransaction();
                                    #endregion Positif
                                }
                            }
                            else
                            {
                                _locInvLineTotal = _locTransferOrderLine.Qty + _locTransferOrderLine.DQty;
                                #region Negatif
                                InventoryJournal _locNegatifInventoryJournal = new InventoryJournal(_currSession)
                                {
                                    DocumentType = _locTransferOrderXPO.DocumentType,
                                    DocNo = _locTransferOrderXPO.DocNo,
                                    Location = _locTransferOrderLine.LocationFrom,
                                    BinLocation = _locTransferOrderLine.BinLocationFrom,
                                    LocationType = _locTransferOrderLine.LocationTypeFrom,
                                    StockType = _locTransferOrderLine.StockTypeFrom,
                                    StockGroup = _locTransferOrderLine.StockGroupFrom,
                                    Item = _locTransferOrderLine.Item,
                                    Company = _locTransferOrderLine.Company,
                                    Workplace = _locTransferOrderLine.WorkplaceFrom,
                                    QtyPos = 0,
                                    QtyNeg = _locInvLineTotal,
                                    DUOM = _locTransferOrderLine.DUOM,
                                    JournalDate = now,
                                    TransferOrder = _locTransferOrderXPO,
                                };
                                _locNegatifInventoryJournal.Save();
                                _locNegatifInventoryJournal.Session.CommitTransaction();
                                #endregion Negatif
                                #region Positif
                                InventoryJournal _locPositifInventoryJournal = new InventoryJournal(_currSession)
                                {
                                    DocumentType = _locTransferOrderXPO.DocumentType,
                                    DocNo = _locTransferOrderXPO.DocNo,
                                    Location = _locTransferOrderLine.LocationTo,
                                    BinLocation = _locTransferOrderLine.BinLocationTo,
                                    LocationType = _locTransferOrderLine.LocationTypeTo,
                                    StockType = _locTransferOrderLine.StockTypeTo,
                                    StockGroup = _locTransferOrderLine.StockGroupTo,
                                    Item = _locTransferOrderLine.Item,
                                    Company = _locTransferOrderLine.Company,
                                    Workplace = _locTransferOrderLine.WorkplaceTo,
                                    QtyPos = _locInvLineTotal,
                                    QtyNeg = 0,
                                    DUOM = _locTransferOrderLine.DUOM,
                                    JournalDate = now,
                                    TransferOrder = _locTransferOrderXPO,
                                };
                                _locPositifInventoryJournal.Save();
                                _locPositifInventoryJournal.Session.CommitTransaction();
                                #endregion Positif
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = TransferOrder ", ex.ToString());
            }
        }

        private void SetTransferInventoryMonitoring(Session _currSession, TransferOrder _locTransferOrderXPO)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                DateTime now = DateTime.Now;
                PickingLine _locPickingLine = null;
                Employee _locPIC = null;

                if (_locTransferOrderXPO != null)
                {
                    XPCollection<TransferOrderLine> _locTransferOrderLines = new XPCollection<TransferOrderLine>
                                                            (_currSession, new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("TransferOrder", _locTransferOrderXPO),
                                                            new BinaryOperator("Select", true),
                                                            new GroupOperator(GroupOperatorType.Or,
                                                            new BinaryOperator("Status", Status.Progress),
                                                            new BinaryOperator("Status", Status.Posted))));

                    if (_locTransferOrderLines != null && _locTransferOrderLines.Count() > 0)
                    {
                        foreach (TransferOrderLine _locTransferOrderLine in _locTransferOrderLines)
                        {
                            if (_locTransferOrderLine.SalesInvoiceMonitoring != null)
                            {
                                if (_locTransferOrderLine.SalesInvoiceMonitoring.PickingLine != null)
                                {
                                    _locPickingLine = _locTransferOrderLine.SalesInvoiceMonitoring.PickingLine;
                                }
                            }

                            if (_locTransferOrderLine.SalesInvoice != null)
                            {
                                _locPIC = _locTransferOrderLine.SalesInvoice.PIC;
                            }

                            TransferInventoryMonitoring _saveDataTransferInventoryMonitoring = new TransferInventoryMonitoring(_currSession)
                            {
                                TransferOrder = _locTransferOrderXPO,
                                TransferOrderLine = _locTransferOrderLine,
                                #region Organization
                                Company = _locTransferOrderLine.Company,
                                Workplace = _locTransferOrderLine.Workplace,
                                Division = _locTransferOrderLine.Division,
                                Department = _locTransferOrderLine.Department,
                                Section = _locTransferOrderLine.Section,
                                Employee = _locTransferOrderLine.Employee,
                                #endregion Organization
                                #region From
                                LocationTypeFrom = _locTransferOrderLine.LocationTypeFrom,
                                StockTypeFrom = _locTransferOrderLine.StockTypeFrom,
                                WorkplaceFrom = _locTransferOrderLine.WorkplaceFrom,
                                LocationFrom = _locTransferOrderLine.LocationFrom,
                                BinLocationFrom = _locTransferOrderLine.BinLocationFrom,
                                StockGroupFrom = _locTransferOrderLine.StockGroupFrom,
                                BegInvFrom = _locTransferOrderLine.BegInvFrom,
                                BegInvLineFrom = _locTransferOrderLine.BegInvLineFrom,
                                #endregion From
                                #region To
                                LocationTypeTo = _locTransferOrderLine.LocationTypeTo,
                                StockTypeTo = _locTransferOrderLine.StockTypeTo,
                                WorkplaceTo = _locTransferOrderLine.WorkplaceTo,
                                LocationTo = _locTransferOrderLine.LocationTo,
                                BinLocationTo = _locTransferOrderLine.BinLocationTo,
                                StockGroupTo = _locTransferOrderLine.StockGroupTo,
                                BegInvTo = _locTransferOrderLine.BegInvTo,
                                BegInvLineTo = _locTransferOrderLine.BegInvLineTo,
                                #endregion To
                                PickingDate = _locTransferOrderXPO.PickingDate,
                                Picking = _locTransferOrderXPO.Picking,
                                PickingLine = _locPickingLine,
                                PIC = _locPIC,
                                Vehicle = _locTransferOrderXPO.Vehicle,
                                #region Item
                                Item = _locTransferOrderLine.Item,
                                Brand = _locTransferOrderLine.Brand,
                                Description = _locTransferOrderLine.Description,
                                MxDQty = _locTransferOrderLine.DQty,
                                MxDUOM = _locTransferOrderLine.DUOM,
                                MxQty = _locTransferOrderLine.Qty,
                                MxUOM = _locTransferOrderLine.UOM,
                                MxTQty = _locTransferOrderLine.TQty,
                                DQty = _locTransferOrderLine.DQty,
                                DUOM = _locTransferOrderLine.DUOM,
                                Qty = _locTransferOrderLine.Qty,
                                UOM = _locTransferOrderLine.UOM,
                                TQty = _locTransferOrderLine.TQty,
                                #endregion Item
                                ETD = _locTransferOrderLine.ETD,
                                ETA = _locTransferOrderLine.ETA,
                                DocDate = _locTransferOrderLine.DocDate,
                                JournalMonth = _locTransferOrderLine.JournalMonth,
                                JournalYear = _locTransferOrderLine.JournalYear,
                                SalesInvoiceMonitoring = _locTransferOrderLine.SalesInvoiceMonitoring,
                                SalesInvoice = _locTransferOrderLine.SalesInvoice,
                            };
                            _saveDataTransferInventoryMonitoring.Save();
                            _saveDataTransferInventoryMonitoring.Session.CommitTransaction();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = TransferOrder ", ex.ToString());
            }
        }

        private void SetStatusDeliverTransferOrderLine(Session _currSession, TransferOrder _locTransferOrderXPO)
        {
            try
            {
                DateTime now = DateTime.Now;
                if (_locTransferOrderXPO != null)
                {
                    XPCollection<TransferOrderLine> _locTransferOrderLines = new XPCollection<TransferOrderLine>(_currSession,
                                                            new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("TransferOrder", _locTransferOrderXPO),
                                                            new BinaryOperator("Select", true),
                                                            new GroupOperator(GroupOperatorType.Or,
                                                            new BinaryOperator("Status", Status.Progress),
                                                            new BinaryOperator("Status", Status.Posted))));

                    if (_locTransferOrderLines != null && _locTransferOrderLines.Count > 0)
                    {
                        foreach (TransferOrderLine _locTransferOrderLine in _locTransferOrderLines)
                        {
                            _locTransferOrderLine.Status = Status.Close;
                            _locTransferOrderLine.ActivationPosting = true;
                            _locTransferOrderLine.StatusDate = now;
                            _locTransferOrderLine.Save();
                            _locTransferOrderLine.Session.CommitTransaction();

                            if(_locTransferOrderXPO.DocumentRule == DocumentRule.Customer)
                            {
                                if (_locTransferOrderLine.SalesInvoiceMonitoring != null)
                                {
                                    _locTransferOrderLine.SalesInvoiceMonitoring.ATD = now;
                                    _locTransferOrderLine.SalesInvoiceMonitoring.Save();
                                    _locTransferOrderLine.SalesInvoiceMonitoring.Session.CommitTransaction();
                                }
                            } 
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = TransferOrder " + ex.ToString());
            }
        }

        private void SetNormalDeliverQuantity(Session _currSession, TransferOrder _locTransferOrderXPO)
        {
            try
            {
                if (_locTransferOrderXPO != null)
                {
                    XPCollection<TransferOrderLine> _locTransferOrderLines = new XPCollection<TransferOrderLine>(_currSession,
                                                                                     new GroupOperator(GroupOperatorType.And,
                                                                                     new BinaryOperator("TransferOrder", _locTransferOrderXPO),
                                                                                     new BinaryOperator("Select", true),
                                                                                     new GroupOperator(GroupOperatorType.Or,
                                                                                     new BinaryOperator("Status", Status.Progress),
                                                                                     new BinaryOperator("Status", Status.Posted),
                                                                                     new BinaryOperator("Status", Status.Close))));

                    if (_locTransferOrderLines != null && _locTransferOrderLines.Count > 0)
                    {
                        foreach (TransferOrderLine _locTransferOrderLine in _locTransferOrderLines)
                        {
                            if (_locTransferOrderLine.DQty > 0 || _locTransferOrderLine.Qty > 0)
                            {
                                _locTransferOrderLine.Select = false;
                                _locTransferOrderLine.Save();
                                _locTransferOrderLine.Session.CommitTransaction();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = TransferOrder " + ex.ToString());
            }
        }

        private void SetFinalStatusDeliverTransferOrder(Session _currSession, TransferOrder _locTransferOrderXPO)
        {
            try
            {
                DateTime now = DateTime.Now;
                int _locStatusCount = 0;
                int _locTransOutLineCount = 0;

                if (_locTransferOrderXPO != null)
                {
                    XPCollection<TransferOrderLine> _locTransferOrderLines = new XPCollection<TransferOrderLine>(_currSession,
                                                                                     new GroupOperator(GroupOperatorType.And,
                                                                                     new BinaryOperator("TransferOrder", _locTransferOrderXPO)));

                    if (_locTransferOrderLines != null && _locTransferOrderLines.Count() > 0)
                    {
                        _locTransOutLineCount = _locTransferOrderLines.Count();

                        foreach (TransferOrderLine _locTransferOrderLine in _locTransferOrderLines)
                        {

                            if (_locTransferOrderLine.Status == Status.Close)
                            {
                                _locStatusCount = _locStatusCount + 1;
                            }
                        }

                        if (_locTransOutLineCount == _locStatusCount)
                        {
                            _locTransferOrderXPO.ActivationPosting = true;
                            _locTransferOrderXPO.Status = Status.Close;
                            _locTransferOrderXPO.StatusDate = now;
                            _locTransferOrderXPO.PostedCount = _locTransferOrderXPO.PostedCount + 1;
                            _locTransferOrderXPO.Save();
                            _locTransferOrderXPO.Session.CommitTransaction();
                        }
                        else
                        {
                            _locTransferOrderXPO.Status = Status.Posted;
                            _locTransferOrderXPO.StatusDate = now;
                            _locTransferOrderXPO.PostedCount = _locTransferOrderXPO.PostedCount + 1;
                            _locTransferOrderXPO.Save();
                            _locTransferOrderXPO.Session.CommitTransaction();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = TransferOrder " + ex.ToString());
            }
        }

        #endregion Posting

        #region Global Method

        private void SuccessMessageShow(string _locMessage)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 4000;
            options.Message = string.Format("{0}", _locMessage);
            options.Type = InformationType.Success;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Success";
            options.Win.Type = WinMessageType.Flyout;
            Application.ShowViewStrategy.ShowMessage(options);
        }

        private void ErrorMessageShow(string _locMessage)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 2000;
            options.Message = string.Format(_locMessage);
            options.Type = InformationType.Warning;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Error";
            options.Win.Type = WinMessageType.Alert;
            Application.ShowViewStrategy.ShowMessage(options);
        }

        #endregion Global Method
    }
}
