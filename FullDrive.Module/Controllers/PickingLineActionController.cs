﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;

using FullDrive.Module.BusinessObjects;
using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.Xpo;
using System.Collections;
using DevExpress.Xpo;

namespace FullDrive.Module.Controllers
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class PickingLineActionController : ViewController
    {
        public PickingLineActionController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.
        }
        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }
        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        private void PickingLineSelectAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;

                if (_objectToProcess != null)
                {
                    foreach (Object obj in _objectToProcess)
                    {
                        PickingLine _locPickingLineOS = (PickingLine)_objectSpace.GetObject(obj);

                        if (_locPickingLineOS != null)
                        {
                            if (_locPickingLineOS.Session != null)
                            {
                                _currSession = _locPickingLineOS.Session;
                            }

                            if (_locPickingLineOS.Code != null && _currSession != null)
                            {
                                _currObjectId = _locPickingLineOS.Code;

                                XPCollection<PickingLine> _locPickingLines = new XPCollection<PickingLine>
                                                                                                     (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                      new BinaryOperator("Code", _currObjectId),
                                                                                                      new GroupOperator(GroupOperatorType.Or,
                                                                                                      new BinaryOperator("Status", Status.Posted),
                                                                                                      new BinaryOperator("Status", Status.Progress),
                                                                                                      new BinaryOperator("Status", Status.Open)
                                                                                                      )));

                                if (_locPickingLines != null && _locPickingLines.Count > 0)
                                {
                                    foreach (PickingLine _locPickingLine in _locPickingLines)
                                    {
                                        _locPickingLine.Select = true;
                                        _locPickingLine.Save();
                                        _locPickingLine.Session.CommitTransaction();
                                    }

                                    SuccessMessageShow("PickingLine has been Select");
                                }
                                else
                                {
                                    ErrorMessageShow("Data PickingLine Not Available");
                                }
                            }
                        }
                    }
                    if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                    {
                        _objectSpace.CommitChanges();
                        _objectSpace.Refresh();
                    }
                    if (View is ListView)
                    {
                        _objectSpace.CommitChanges();
                        View.ObjectSpace.Refresh();
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("BusinessObject = PickingLine" + ex.ToString());
            }
        }

        private void PickingLineUnselectAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;

                if (_objectToProcess != null)
                {
                    foreach (Object obj in _objectToProcess)
                    {
                        PickingLine _locPickingLineOS = (PickingLine)_objectSpace.GetObject(obj);

                        if (_locPickingLineOS != null)
                        {
                            if (_locPickingLineOS.Session != null)
                            {
                                _currSession = _locPickingLineOS.Session;
                            }

                            if (_locPickingLineOS.Code != null && _currSession != null)
                            {
                                _currObjectId = _locPickingLineOS.Code;

                                XPCollection<PickingLine> _locPickingLines = new XPCollection<PickingLine>
                                                                                                     (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                      new BinaryOperator("Code", _currObjectId),
                                                                                                      new GroupOperator(GroupOperatorType.Or,
                                                                                                      new BinaryOperator("Status", Status.Posted),
                                                                                                      new BinaryOperator("Status", Status.Progress),
                                                                                                      new BinaryOperator("Status", Status.Open)
                                                                                                      )));

                                if (_locPickingLines != null && _locPickingLines.Count > 0)
                                {
                                    foreach (PickingLine _locPickingLine in _locPickingLines)
                                    {
                                        _locPickingLine.Select = false;
                                        _locPickingLine.Save();
                                        _locPickingLine.Session.CommitTransaction();
                                    }

                                    SuccessMessageShow("PickingLine has been Unselect");
                                }
                                else
                                {
                                    ErrorMessageShow("Data PickingLine Not Available");
                                }
                            }
                        }
                    }
                    if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                    {
                        _objectSpace.CommitChanges();
                        _objectSpace.Refresh();
                    }
                    if (View is ListView)
                    {
                        _objectSpace.CommitChanges();
                        View.ObjectSpace.Refresh();
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("BusinessObject = PickingLine" + ex.ToString());
            }
        }

        private void PickingLineShowSIMAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
            var ListView = View as ListView;
            IObjectSpace os = Application.CreateObjectSpace();
            string listViewId = Application.FindListViewId(typeof(SalesInvoiceMonitoring));
            CollectionSourceBase cs = Application.CreateCollectionSource(os, typeof(SalesInvoiceMonitoring), listViewId);
            //Tinggal buat 
            ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
            string _stringSIM = null;
            if (_objectsToProcess != null)
            {
                foreach (Object obj in _objectsToProcess)
                {
                    PickingLine _locPickingLine = (PickingLine)_objectSpace.GetObject(obj);
                    if (_locPickingLine != null)
                    {
                        if (_locPickingLine.Picking != null)
                        {
                            if (_stringSIM == null)
                            {
                                if (_locPickingLine.SalesInvoice.Code != null)
                                {
                                    _stringSIM = "( [SalesInvoice.Code]=='" + _locPickingLine.SalesInvoice.Code + "' And [Status] != 4)";
                                }
                            }
                            else
                            {
                                if (_locPickingLine.SalesInvoice.Code != null)
                                {
                                    _stringSIM = _stringSIM + " OR ( [SalesInvoice.Code]=='" + _locPickingLine.SalesInvoice.Code + "' And [Status] != 4)";
                                }
                            }
                        }
                    }
                }
            }
            if (_stringSIM != null)
            {
                cs.Criteria["SalesInvoiceCollectionFilter"] = CriteriaOperator.Parse(_stringSIM);
            }
            e.ShowViewParameters.CreatedView = Application.CreateListView(
                listViewId,
                cs,
                true
            );
            e.ShowViewParameters.Context = TemplateContext.View;
            e.ShowViewParameters.TargetWindow = TargetWindow.NewModalWindow;
            DialogController dc = Application.CreateController<DialogController>();
            dc.Accepting += new EventHandler<DialogControllerAcceptingEventArgs>(dc_Accepting);
            e.ShowViewParameters.Controllers.Add(dc);
        }

        //======================================== Code In Here ========================================

        void dc_Accepting(object sender, DialogControllerAcceptingEventArgs e)
        {
            try
            {
                Picking _locPicking = null;
                View popupView = ((Controller)sender).Frame.View;
                ArrayList _objectsToProcess = new ArrayList(View.SelectedObjects);
                if (_objectsToProcess != null)
                {
                    foreach (Object _obj in _objectsToProcess)
                    {
                        PickingLine _locPickingLine = (PickingLine)popupView.ObjectSpace.GetObject(_obj);
                        if (_locPickingLine != null)
                        {
                            if (_locPickingLine.Picking != null && _locPickingLine.Select == true)
                            {
                                _locPicking = _locPickingLine.Picking;
                            }
                        }
                    }
                }
                if (_locPicking != null)
                {
                    ArrayList _objectsToProcess2 = new ArrayList(popupView.SelectedObjects);
                    if (_objectsToProcess2 != null)
                    {
                        foreach (Object _obj2 in _objectsToProcess2)
                        {
                            SalesInvoiceMonitoring _locSalesInvoiceMonitoring = (SalesInvoiceMonitoring)popupView.ObjectSpace.GetObject(_obj2);
                            if (_locSalesInvoiceMonitoring != null && _locSalesInvoiceMonitoring.Select == true && (_locSalesInvoiceMonitoring.Status == Status.Open || _locSalesInvoiceMonitoring.Status == Status.Posted))
                            {
                                _locSalesInvoiceMonitoring.Picking = _locPicking;
                            }
                        }
                    }
                }
                popupView.ObjectSpace.CommitChanges();
            }
            catch (ValidationException)
            {
            }
        }

        #region Global Method

        private void SuccessMessageShow(string _locActionName)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 2000;
            options.Message = string.Format("{0}", _locActionName);
            options.Type = InformationType.Success;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Success";
            options.Win.Type = WinMessageType.Flyout;
            Application.ShowViewStrategy.ShowMessage(options);
        }

        private void ErrorMessageShow(string _locMessage)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 2000;
            options.Message = string.Format(_locMessage);
            options.Type = InformationType.Warning;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Error";
            options.Win.Type = WinMessageType.Alert;
            Application.ShowViewStrategy.ShowMessage(options);
        }

        #endregion Global Method
    }
}
