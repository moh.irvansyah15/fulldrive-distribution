﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;

using FullDrive.Module.BusinessObjects;
using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.Xpo;
using System.Collections;
using DevExpress.Xpo;

namespace FullDrive.Module.Controllers
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class InventoryTransferOutActionController : ViewController
    {
        private ChoiceActionItem _selectionListviewFilter;

        public InventoryTransferOutActionController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.
            #region Filter Status
            InventoryTransferOutListviewFilterSelectionAction.Items.Clear();
            foreach (object _currApproval in Enum.GetValues(typeof(CustomProcess.Status)))
            {
                EnumDescriptor _ed = new EnumDescriptor(typeof(CustomProcess.Status));
                _selectionListviewFilter = new ChoiceActionItem(_ed.GetCaption(_currApproval), _currApproval);
                InventoryTransferOutListviewFilterSelectionAction.Items.Add(_selectionListviewFilter);
            }
            #endregion Filter Status
        }

        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
        }

        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }

        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        private void InventoryTransferOutProgressAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;
                Status _locStatus = Status.None;
                DateTime _locNow;
                Status _locStatus2 = Status.None;
                DateTime _locNow2;

                if (_objectsToProcess != null)
                {
                    foreach (Object obj in _objectsToProcess)
                    {
                        InventoryTransferOut _locInventoryTransferOutOS = (InventoryTransferOut)_objectSpace.GetObject(obj);

                        if (_locInventoryTransferOutOS != null)
                        {
                            if (_locInventoryTransferOutOS.Session != null)
                            {
                                _currSession = _locInventoryTransferOutOS.Session;
                            }
                            if (_locInventoryTransferOutOS.Code != null && _currSession != null)
                            {
                                _currObjectId = _locInventoryTransferOutOS.Code;

                                InventoryTransferOut _locInventoryTransferOutXPO = _currSession.FindObject<InventoryTransferOut>
                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                     new BinaryOperator("Code", _currObjectId)));

                                if (_locInventoryTransferOutXPO != null)
                                {
                                    if (_locInventoryTransferOutXPO.Status == Status.Open || _locInventoryTransferOutXPO.Status == Status.Progress)
                                    {
                                        if (_locInventoryTransferOutXPO.Status == Status.Open)
                                        {
                                            _locStatus = Status.Progress;
                                            _locNow = now;
                                        }
                                        else
                                        {
                                            _locStatus = _locInventoryTransferOutXPO.Status;
                                            _locNow = _locInventoryTransferOutXPO.StatusDate;
                                        }
                                        _locInventoryTransferOutXPO.Status = _locStatus;
                                        _locInventoryTransferOutXPO.StatusDate = _locNow;
                                        _locInventoryTransferOutXPO.Save();
                                        _locInventoryTransferOutXPO.Session.CommitTransaction();

                                        #region InventoryTransferOutLine and InventoryTransferOutLot
                                        XPCollection<InventoryTransferOutLine> _locInventoryTransferOutLines = new XPCollection<InventoryTransferOutLine>
                                                               (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("InventoryTransferOut", _locInventoryTransferOutXPO)));

                                        if (_locInventoryTransferOutLines != null && _locInventoryTransferOutLines.Count > 0)
                                        {
                                            foreach (InventoryTransferOutLine _locInventoryTransferOutLine in _locInventoryTransferOutLines)
                                            {
                                                if (_locInventoryTransferOutLine.Status == Status.Open || _locInventoryTransferOutLine.Status == Status.Progress)
                                                {
                                                    if (_locInventoryTransferOutLine.Status == Status.Open)
                                                    {
                                                        _locStatus2 = Status.Progress;
                                                        _locNow2 = now;
                                                    }
                                                    else
                                                    {
                                                        _locStatus2 = _locInventoryTransferOutLine.Status;
                                                        _locNow2 = _locInventoryTransferOutLine.StatusDate;
                                                    }

                                                    _locInventoryTransferOutLine.Status = _locStatus2;
                                                    _locInventoryTransferOutLine.StatusDate = _locNow2;
                                                    _locInventoryTransferOutLine.Save();
                                                    _locInventoryTransferOutLine.Session.CommitTransaction();
                                                }

                                                XPCollection<InventoryTransferOutLot> _locInventoryTransferOutLots = new XPCollection<InventoryTransferOutLot>
                                                                                                        (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                        new BinaryOperator("InventoryTransferOutLine", _locInventoryTransferOutLine),
                                                                                                        new BinaryOperator("Status", Status.Open)));

                                                if (_locInventoryTransferOutLots != null && _locInventoryTransferOutLots.Count() > 0)
                                                {
                                                    foreach (InventoryTransferOutLot _locInventoryTransferOutLot in _locInventoryTransferOutLots)
                                                    {
                                                        _locInventoryTransferOutLot.Status = Status.Progress;
                                                        _locInventoryTransferOutLot.StatusDate = now;
                                                        _locInventoryTransferOutLot.Save();
                                                        _locInventoryTransferOutLot.Session.CommitTransaction();
                                                    }
                                                }
                                            }
                                        }
                                        #endregion InventoryTransferOutLine and InventoryTransferOutLot

                                        SuccessMessageShow(_locInventoryTransferOutXPO.Code + " has been change successfully to Progress");
                                    }
                                }
                                else
                                {
                                    ErrorMessageShow("Data Inventory Transfer Out Not Available");
                                }
                            }
                            else
                            {
                                ErrorMessageShow("Data Inventory Transfer Out Not Available");
                            }
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = InventoryTransferOut " + ex.ToString());
            }
        }

        private void InventoryTransferOutPostingAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;

                if (_objectsToProcess != null)
                {
                    foreach (Object obj in _objectsToProcess)
                    {
                        InventoryTransferOut _locInventoryTransferOutOS = (InventoryTransferOut)_objectSpace.GetObject(obj);

                        if (_locInventoryTransferOutOS != null)
                        {
                            if (_locInventoryTransferOutOS.Status == Status.Progress || _locInventoryTransferOutOS.Status == Status.Posted)
                            {
                                if (_locInventoryTransferOutOS.Session != null)
                                {
                                    _currSession = _locInventoryTransferOutOS.Session;
                                }
                                if (_locInventoryTransferOutOS.Code != null && _currSession != null)
                                {
                                    _currObjectId = _locInventoryTransferOutOS.Code;

                                    InventoryTransferOut _locInventoryTransferOutXPO = _currSession.FindObject<InventoryTransferOut>
                                                                        (new GroupOperator(GroupOperatorType.And,
                                                                         new BinaryOperator("Code", _currObjectId)));

                                    if (_locInventoryTransferOutXPO != null)
                                    {
                                        if (CheckAvailableStock(_currSession, _locInventoryTransferOutXPO) == true)
                                        {
                                            SetDeliverBeginingInventory(_currSession, _locInventoryTransferOutXPO);
                                            SetDeliverInventoryJournal(_currSession, _locInventoryTransferOutXPO);
                                            if (CheckJournalSetup(_currSession, _locInventoryTransferOutXPO) == true && CheckGoodsDeliveryJournal(_currSession, _locInventoryTransferOutXPO) == false)
                                            {
                                                SetGoodsDeliveryJournal(_currSession, _locInventoryTransferOutXPO);
                                            }
                                            SetStatusDeliverInventoryTransferOutLine(_currSession, _locInventoryTransferOutXPO);
                                            SetNormalDeliverQuantity(_currSession, _locInventoryTransferOutXPO);
                                            SetFinalStatusDeliverInventoryTransferOut(_currSession, _locInventoryTransferOutXPO);
                                            SuccessMessageShow(_locInventoryTransferOutXPO.Code + " has been change successfully to Deliver");
                                        }
                                        else
                                        {
                                            ErrorMessageShow("Please check stock quantity");
                                        }
                                    }
                                    else
                                    {
                                        ErrorMessageShow("Inventory Transfer Out Data Not Available");
                                    }
                                }
                            }
                            else
                            {
                                ErrorMessageShow("Inventory Transfer Out Data Not Available");
                            }
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = InventoryTransferOut " + ex.ToString());
            }
        }

        private void InventoryTransferOutListviewFilterSelectionAction_Execute(object sender, SingleChoiceActionExecuteEventArgs e)
        {
            try
            {
                if ((View is ListView) & (View.ObjectTypeInfo.Type == typeof(InventoryTransferOut)))
                {
                    if ((Status)e.SelectedChoiceActionItem.Data == Status.None)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.None, BinaryOperatorType.NotEqual);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Open)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Open, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Progress)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Progress, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Posted)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Posted, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Lock)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Lock, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Close)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Close, BinaryOperatorType.Equal);
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = InventoryTransferOut " + ex.ToString());
            }
        }

        //============================================ Code In Here ============================================

        #region Posting

        private bool CheckAvailableStock(Session _currSession, InventoryTransferOut _locInventoryTransferOutXPO)
        {
            bool _result = true;
            try
            {
                if (_locInventoryTransferOutXPO != null)
                {
                    XPCollection<InventoryTransferOutLine> _locInventoryTransferOutLines = new XPCollection<InventoryTransferOutLine>(_currSession,
                                                                        new GroupOperator(GroupOperatorType.And,
                                                                        new BinaryOperator("InventoryTransferOut", _locInventoryTransferOutXPO),
                                                                        new BinaryOperator("Select", true),
                                                                        new GroupOperator(GroupOperatorType.Or,
                                                                        new BinaryOperator("Status", Status.Progress),
                                                                        new BinaryOperator("Status", Status.Posted))));

                    if (_locInventoryTransferOutLines != null && _locInventoryTransferOutLines.Count > 0)
                    {
                        foreach (InventoryTransferOutLine _locInventoryTransferOutLine in _locInventoryTransferOutLines)
                        {
                            XPCollection<InventoryTransferOutLot> _locInventoryTransferOutLots = new XPCollection<InventoryTransferOutLot>
                                                                                            (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                            new BinaryOperator("InventoryTransferOutLine", _locInventoryTransferOutLine),
                                                                                            new BinaryOperator("Select", true),
                                                                                            new GroupOperator(GroupOperatorType.Or,
                                                                                            new BinaryOperator("Status", Status.Progress),
                                                                                            new BinaryOperator("Status", Status.Posted))));

                            #region LotNumber
                            if (_locInventoryTransferOutLots != null && _locInventoryTransferOutLots.Count() > 0)
                            {
                                foreach (InventoryTransferOutLot _locInventoryTransferOutLot in _locInventoryTransferOutLots)
                                {
                                    BeginningInventory _locBegInventory = _currSession.FindObject<BeginningInventory>
                                                        (new GroupOperator(GroupOperatorType.And,
                                                        new BinaryOperator("Item", _locInventoryTransferOutLot.Item)));
                                    if (_locBegInventory != null)
                                    {
                                        if (_locInventoryTransferOutLot.BegInvLine != null)
                                        {
                                            if (_locInventoryTransferOutLot.BegInvLine.Code != null)
                                            {
                                                BeginningInventoryLine _locBegInvLine = _currSession.FindObject<BeginningInventoryLine>(
                                                                                            new GroupOperator(GroupOperatorType.And,
                                                                                            new BinaryOperator("Code", _locInventoryTransferOutLot.BegInvLine.Code)));
                                                if (_locBegInvLine != null)
                                                {
                                                    if (_locInventoryTransferOutLot.TQty > _locBegInvLine.QtyAvailable)
                                                    {
                                                        _result = false;
                                                    }
                                                }
                                                else
                                                {
                                                    _result = false;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            #endregion LotNumber
                            #region NonLotNumber
                            else
                            {
                                if (_locInventoryTransferOutLine.BegInv != null)
                                {
                                    if (_locInventoryTransferOutLine.BegInvLine != null)
                                    {
                                        if (_locInventoryTransferOutLine.TQty > _locInventoryTransferOutLine.BegInvLine.QtyAvailable)
                                        {
                                            _result = false;
                                        }
                                    }
                                }
                            }
                            #endregion NonLotNumber
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = InventoryTransferOut ", ex.ToString());
            }
            return _result;
        }

        private void SetDeliverBeginingInventory(Session _currSession, InventoryTransferOut _locInventoryTransferOutXPO)
        {
            try
            {
                XPCollection<InventoryTransferOutLine> _locInventoryTransferOutLines = new XPCollection<InventoryTransferOutLine>(_currSession,
                                                                        new GroupOperator(GroupOperatorType.And,
                                                                        new BinaryOperator("InventoryTransferOut", _locInventoryTransferOutXPO),
                                                                        new BinaryOperator("Select", true),
                                                                        new GroupOperator(GroupOperatorType.Or,
                                                                        new BinaryOperator("Status", Status.Progress),
                                                                        new BinaryOperator("Status", Status.Posted))));

                if (_locInventoryTransferOutLines != null && _locInventoryTransferOutLines.Count() > 0)
                {
                    GlobalFunction _globFunc = new GlobalFunction();
                    //double _locInvLineTotal = 0;

                    foreach (InventoryTransferOutLine _locInventoryTransferOutLine in _locInventoryTransferOutLines)
                    {
                        if (_locInventoryTransferOutLine.DQty > 0 || _locInventoryTransferOutLine.Qty > 0)
                        {
                            XPCollection<InventoryTransferOutLot> _locInventoryTransferOutLots = new XPCollection<InventoryTransferOutLot>
                                                                                    (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                    new BinaryOperator("InventoryTransferOutLine", _locInventoryTransferOutLine),
                                                                                    new BinaryOperator("Select", true),
                                                                                    new GroupOperator(GroupOperatorType.Or,
                                                                                    new BinaryOperator("Status", Status.Progress),
                                                                                    new BinaryOperator("Status", Status.Posted))));
                            #region LotNumber
                            if (_locInventoryTransferOutLots != null && _locInventoryTransferOutLots.Count() > 0)
                            {
                                foreach (InventoryTransferOutLot _locInventoryTransferOutLot in _locInventoryTransferOutLots)
                                {
                                    if (_locInventoryTransferOutLot.Select == true)
                                    {
                                        if (_locInventoryTransferOutLot.BegInvLine != null)
                                        {
                                            if (_locInventoryTransferOutLot.BegInvLine.Code != null)
                                            {
                                                BeginningInventoryLine _locBegInvLine = _currSession.FindObject<BeginningInventoryLine>
                                                                                (new GroupOperator(GroupOperatorType.And,
                                                                                 new BinaryOperator("Code", _locInventoryTransferOutLot.BegInvLine.Code)));
                                                if (_locBegInvLine != null)
                                                {
                                                    if (_locBegInvLine.QtyAvailable >= _locInventoryTransferOutLot.TQty)
                                                    {
                                                        _locBegInvLine.QtyAvailable = _locBegInvLine.QtyAvailable - _locInventoryTransferOutLot.TQty;
                                                        _locBegInvLine.Save();
                                                        _locBegInvLine.Session.CommitTransaction();
                                                    }

                                                    if (_locBegInvLine.BeginningInventory != null)
                                                    {
                                                        if (_locBegInvLine.BeginningInventory.Code != null)
                                                        {
                                                            BeginningInventory _locBegInv = _currSession.FindObject<BeginningInventory>
                                                                                (new GroupOperator(GroupOperatorType.And,
                                                                                 new BinaryOperator("Code", _locBegInvLine.BeginningInventory.Code)));
                                                            if (_locBegInv != null)
                                                            {
                                                                _locBegInv.QtyAvailable = _locBegInv.QtyAvailable - _locInventoryTransferOutLot.TQty;
                                                                _locBegInv.QtySale = _locBegInv.QtySale + _locInventoryTransferOutLot.TQty;
                                                                _locBegInv.Save();
                                                                _locBegInv.Session.CommitTransaction();
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            #endregion LotNumber

                            #region NonLotNumber
                            else
                            {
                                #region BeginningInventory
                                if (_locInventoryTransferOutLine.BegInv != null && _locInventoryTransferOutLine.BegInvLine != null)
                                {
                                    _locInventoryTransferOutLine.BegInvLine.QtyAvailable = _locInventoryTransferOutLine.BegInvLine.QtyAvailable - _locInventoryTransferOutLine.TQty;
                                    _locInventoryTransferOutLine.BegInvLine.QtySale = _locInventoryTransferOutLine.BegInvLine.QtySale + _locInventoryTransferOutLine.TQty;
                                    _locInventoryTransferOutLine.BegInvLine.QtyOnHand = _locInventoryTransferOutLine.BegInvLine.QtyOnHand - _locInventoryTransferOutLine.TQty;
                                    _locInventoryTransferOutLine.BegInvLine.Save();
                                    _locInventoryTransferOutLine.BegInvLine.Session.CommitTransaction();

                                    _locInventoryTransferOutLine.BegInv.QtyAvailable = _locInventoryTransferOutLine.BegInv.QtyAvailable - _locInventoryTransferOutLine.TQty;
                                    _locInventoryTransferOutLine.BegInv.QtySale = _locInventoryTransferOutLine.BegInv.QtySale + _locInventoryTransferOutLine.TQty;
                                    _locInventoryTransferOutLine.BegInv.Save();
                                    _locInventoryTransferOutLine.BegInv.Session.CommitTransaction();

                                }
                                #endregion BeginningInventory
                            }
                            #endregion NonLotNumber
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = InventoryTransferOut ", ex.ToString());
            }
        }

        private void SetDeliverInventoryJournal(Session _currSession, InventoryTransferOut _locInventoryTransferOutXPO)
        {
            try
            {
                XPCollection<InventoryTransferOutLine> _locInventoryTransferOutLines = new XPCollection<InventoryTransferOutLine>(_currSession,
                                                                        new GroupOperator(GroupOperatorType.And,
                                                                        new BinaryOperator("InventoryTransferOut", _locInventoryTransferOutXPO),
                                                                        new BinaryOperator("Select", true),
                                                                        new GroupOperator(GroupOperatorType.Or,
                                                                        new BinaryOperator("Status", Status.Progress),
                                                                        new BinaryOperator("Status", Status.Posted))));

                if (_locInventoryTransferOutLines != null && _locInventoryTransferOutLines.Count > 0)
                {
                    double _locInvLineTotal = 0;
                    DateTime now = DateTime.Now;

                    foreach (InventoryTransferOutLine _locInventoryTransferOutLine in _locInventoryTransferOutLines)
                    {
                        if (_locInventoryTransferOutLine.DQty > 0 || _locInventoryTransferOutLine.Qty > 0)
                        {
                            XPCollection<InventoryTransferOutLot> _locInventoryTransferOutLots = new XPCollection<InventoryTransferOutLot>
                                                                                    (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                    new BinaryOperator("InventoryTransferOutLine", _locInventoryTransferOutLine),
                                                                                    new BinaryOperator("Select", true),
                                                                                    new GroupOperator(GroupOperatorType.Or,
                                                                                    new BinaryOperator("Status", Status.Progress),
                                                                                    new BinaryOperator("Status", Status.Posted))));

                            #region LotNumber
                            if (_locInventoryTransferOutLots != null && _locInventoryTransferOutLots.Count() > 0)
                            {
                                foreach (InventoryTransferOutLot _locInventoryTransferOutLot in _locInventoryTransferOutLots)
                                {
                                    if (_locInventoryTransferOutLot.Select == true)
                                    {
                                        if (_locInventoryTransferOutLot.BegInvLine != null)
                                        {
                                            if (_locInventoryTransferOutLot.BegInvLine.Code != null)
                                            {
                                                BeginningInventoryLine _locBegInvLine = _currSession.FindObject<BeginningInventoryLine>
                                                                                (new GroupOperator(GroupOperatorType.And,
                                                                                 new BinaryOperator("Code", _locInventoryTransferOutLot.BegInvLine.Code)));
                                                if (_locBegInvLine != null)
                                                {
                                                    InventoryJournal _locNegatifInventoryJournal = new InventoryJournal(_currSession)
                                                    {
                                                        DocumentType = _locInventoryTransferOutXPO.DocumentType,
                                                        DocNo = _locInventoryTransferOutXPO.DocNo,
                                                        Location = _locInventoryTransferOutLot.Location,
                                                        BinLocation = _locInventoryTransferOutLot.BinLocation,
                                                        StockType = _locInventoryTransferOutLot.StockType,
                                                        Item = _locInventoryTransferOutLot.Item,
                                                        Company = _locInventoryTransferOutLot.Company,
                                                        QtyPos = 0,
                                                        QtyNeg = _locInventoryTransferOutLot.TQty,
                                                        DUOM = _locInventoryTransferOutLot.DUOM,
                                                        JournalDate = now,
                                                        LotNumber = _locInventoryTransferOutLot.LotNumber,
                                                        InventoryTransferOut = _locInventoryTransferOutXPO,
                                                    };
                                                    _locNegatifInventoryJournal.Save();
                                                    _locNegatifInventoryJournal.Session.CommitTransaction();
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            #endregion LotNumber
                            #region NonLotNumber
                            else
                            {
                                if (_locInventoryTransferOutLine.Item != null && _locInventoryTransferOutLine.UOM != null && _locInventoryTransferOutLine.DUOM != null)
                                {
                                    ItemUnitOfMeasure _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                        (new GroupOperator(GroupOperatorType.And,
                                                         new BinaryOperator("Item", _locInventoryTransferOutLine.Item),
                                                         new BinaryOperator("UOM", _locInventoryTransferOutLine.UOM),
                                                         new BinaryOperator("DefaultUOM", _locInventoryTransferOutLine.DUOM),
                                                         new BinaryOperator("Active", true)));
                                    if (_locItemUOM != null)
                                    {
                                        #region Code
                                        if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locInventoryTransferOutLine.Qty * _locItemUOM.DefaultConversion + _locInventoryTransferOutLine.DQty;
                                        }
                                        else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locInventoryTransferOutLine.Qty / _locItemUOM.Conversion + _locInventoryTransferOutLine.DQty;
                                        }
                                        else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locInventoryTransferOutLine.Qty + _locInventoryTransferOutLine.DQty;
                                        }
                                        #endregion Code

                                        InventoryJournal _locNegatifInventoryJournal = new InventoryJournal(_currSession)
                                        {
                                            DocumentType = _locInventoryTransferOutXPO.DocumentType,
                                            DocNo = _locInventoryTransferOutXPO.DocNo,
                                            Location = _locInventoryTransferOutLine.Location,
                                            BinLocation = _locInventoryTransferOutLine.BinLocation,
                                            LocationType = _locInventoryTransferOutLine.LocationType,
                                            StockType = _locInventoryTransferOutLine.StockType,
                                            StockGroup = _locInventoryTransferOutLine.StockGroup,
                                            Item = _locInventoryTransferOutLine.Item,
                                            Company = _locInventoryTransferOutLine.Company,
                                            Workplace = _locInventoryTransferOutLine.Workplace,
                                            QtyPos = 0,
                                            QtyNeg = _locInvLineTotal,
                                            DUOM = _locInventoryTransferOutLine.DUOM,
                                            JournalDate = now,
                                            InventoryTransferOut = _locInventoryTransferOutXPO,
                                        };
                                        _locNegatifInventoryJournal.Save();
                                        _locNegatifInventoryJournal.Session.CommitTransaction();

                                    }
                                }
                                else
                                {
                                    _locInvLineTotal = _locInventoryTransferOutLine.Qty + _locInventoryTransferOutLine.DQty;

                                    InventoryJournal _locNegatifInventoryJournal = new InventoryJournal(_currSession)
                                    {
                                        DocumentType = _locInventoryTransferOutXPO.DocumentType,
                                        DocNo = _locInventoryTransferOutXPO.DocNo,
                                        Location = _locInventoryTransferOutLine.Location,
                                        BinLocation = _locInventoryTransferOutLine.BinLocation,
                                        LocationType = _locInventoryTransferOutLine.LocationType,
                                        StockType = _locInventoryTransferOutLine.StockType,
                                        StockGroup = _locInventoryTransferOutLine.StockGroup,
                                        Item = _locInventoryTransferOutLine.Item,
                                        Company = _locInventoryTransferOutLine.Company,
                                        Workplace = _locInventoryTransferOutLine.Workplace,
                                        QtyPos = 0,
                                        QtyNeg = _locInvLineTotal,
                                        DUOM = _locInventoryTransferOutLine.DUOM,
                                        JournalDate = now,
                                        InventoryTransferOut = _locInventoryTransferOutXPO,
                                    };
                                    _locNegatifInventoryJournal.Save();
                                    _locNegatifInventoryJournal.Session.CommitTransaction();
                                }
                            }
                            #endregion NonLotNumber
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = InventoryTransferOut ", ex.ToString());
            }
        }

        #region SetGoodsDeliverJournal

        private bool CheckJournalSetup(Session _currSession, InventoryTransferOut _locInventoryTransferOutXPO)
        {
            bool _result = false;
            try
            {
                if (_locInventoryTransferOutXPO != null)
                {
                    if (_locInventoryTransferOutXPO.Company != null && _locInventoryTransferOutXPO.Workplace != null)
                    {
                        JournalSetupDetail _locJournalSetupDetail = _currSession.FindObject<JournalSetupDetail>
                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                    new BinaryOperator("Company", _locInventoryTransferOutXPO.Company),
                                                                    new BinaryOperator("Workplace", _locInventoryTransferOutXPO.Workplace),
                                                                    new BinaryOperator("JournalSetupType", JournalSetupType.Journal),
                                                                    new BinaryOperator("ObjectList", ObjectList.InventoryTransferOut),
                                                                    new BinaryOperator("JournalProcess", JournalProcess.JournalGoodDeliver),
                                                                    new BinaryOperator("Active", true)));

                        if (_locJournalSetupDetail != null)
                        {
                            _result = true;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = InventoryTransferOut ", ex.ToString());
            }

            return _result;
        }

        private bool CheckGoodsDeliveryJournal(Session _currSession, InventoryTransferOut _locInventoryTransferOutXPO)
        {
            bool _result = false;
            try
            {
                if (_locInventoryTransferOutXPO != null)
                {
                    if (_locInventoryTransferOutXPO.Company != null && _locInventoryTransferOutXPO.Workplace != null)
                    {
                        XPCollection<GeneralJournal> _locGeneralJournals = new XPCollection<GeneralJournal>(_currSession,
                                                                        new GroupOperator(GroupOperatorType.And,
                                                                        new BinaryOperator("Company", _locInventoryTransferOutXPO.Company),
                                                                        new BinaryOperator("Workplace", _locInventoryTransferOutXPO.Workplace),
                                                                        new BinaryOperator("InventoryTransferOut", _locInventoryTransferOutXPO)));
                        if (_locGeneralJournals != null && _locGeneralJournals.Count() > 0)
                        {
                            _result = true;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = InventoryTransferOut ", ex.ToString());
            }

            return _result;
        }

        private void SetGoodsDeliveryJournal(Session _currSession, InventoryTransferOut _locInventoryTransferOutXPO)
        {
            try
            {
                DateTime now = DateTime.Now;
                double _locTotalUnitAmount = 0;
                ItemAccountGroup _locItemAccountGroup = null;

                if (_locInventoryTransferOutXPO != null)
                {
                    if (_locInventoryTransferOutXPO.CustomerInvoice != null)
                    {
                        XPCollection<InventoryTransferOutLine> _locInventoryTransferOutLines = new XPCollection<InventoryTransferOutLine>(_currSession,
                                                                            new GroupOperator(GroupOperatorType.And,
                                                                            new BinaryOperator("InventoryTransferOut", _locInventoryTransferOutXPO),
                                                                            new BinaryOperator("StockGroup", StockGroup.Normal),
                                                                            new GroupOperator(GroupOperatorType.Or,
                                                                            new BinaryOperator("Status", Status.Progress),
                                                                            new BinaryOperator("Status", Status.Posted))));


                        if (_locInventoryTransferOutLines != null && _locInventoryTransferOutLines.Count() > 0)
                        {
                            foreach (InventoryTransferOutLine _locInventoryTransferOutLine in _locInventoryTransferOutLines)
                            {
                                if (_locInventoryTransferOutLine.Item != null && _locInventoryTransferOutLine.Company != null && _locInventoryTransferOutLine.Workplace != null)
                                {
                                    //Cari ItemAccountGroup
                                    ItemAccount _locItemAccount = _currSession.FindObject<ItemAccount>(new GroupOperator(GroupOperatorType.And,
                                                                    new BinaryOperator("Company", _locInventoryTransferOutLine.Company),
                                                                    new BinaryOperator("Workplace", _locInventoryTransferOutLine.Workplace),
                                                                    new BinaryOperator("Item", _locInventoryTransferOutLine.Item),
                                                                    new BinaryOperator("Active", true)));

                                    if (_locInventoryTransferOutLine.JournalMonth > 0 && _locItemAccount != null)
                                    {
                                        if (_locItemAccount.ItemAccountGroup != null)
                                        {
                                            _locItemAccountGroup = _locItemAccount.ItemAccountGroup;
                                        }

                                        if (_locInventoryTransferOutLine.CustomerInvoiceLine != null)
                                        {
                                            if (_locInventoryTransferOutLine.CustomerInvoiceLine.PriceLine != null)
                                            {
                                                _locTotalUnitAmount = _locInventoryTransferOutLine.CustomerInvoiceLine.PriceLine.Amount1a * _locInventoryTransferOutLine.TQty;
                                            }
                                        }

                                        #region JournalMapByItems

                                        XPCollection<JournalMap> _locJournalMapItems = new XPCollection<JournalMap>
                                                                                       (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                        new BinaryOperator("Company", _locInventoryTransferOutLine.Company),
                                                                                        new BinaryOperator("Workplace", _locInventoryTransferOutLine.Workplace),
                                                                                        new BinaryOperator("ItemAccountGroup", _locItemAccountGroup),
                                                                                        new BinaryOperator("Active", true)));

                                        if (_locJournalMapItems != null && _locJournalMapItems.Count() > 0)
                                        {
                                            foreach (JournalMap _locJournalMapItem in _locJournalMapItems)
                                            {
                                                XPCollection<JournalMapLine> _locJournalMapLines = new XPCollection<JournalMapLine>
                                                                                                   (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                    new BinaryOperator("Company", _locInventoryTransferOutLine.Company),
                                                                                                    new BinaryOperator("Workplace", _locInventoryTransferOutLine.Workplace),
                                                                                                    new BinaryOperator("JournalMap", _locJournalMapItem),
                                                                                                    new BinaryOperator("Active", true)));

                                                if (_locJournalMapLines != null && _locJournalMapLines.Count() > 0)
                                                {
                                                    foreach (JournalMapLine _locJournalMapLine in _locJournalMapLines)
                                                    {
                                                        if (_locJournalMapLine.AccountMap != null)
                                                        {
                                                            if (_locJournalMapLine.AccountMap.PostingType == PostingType.Sales && _locJournalMapLine.AccountMap.OrderType == OrderType.Item
                                                                && _locJournalMapLine.AccountMap.PostingMethod == PostingMethod.Deliver && _locJournalMapLine.AccountMap.PostingMethodType == PostingMethodType.Normal)
                                                            {
                                                                XPCollection<AccountMapLine> _locAccountMapLines = new XPCollection<AccountMapLine>
                                                                                                               (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                                new BinaryOperator("Company", _locInventoryTransferOutLine.Company),
                                                                                                                new BinaryOperator("Workplace", _locInventoryTransferOutLine.Workplace),
                                                                                                                new BinaryOperator("AccountMap", _locJournalMapLine.AccountMap),
                                                                                                                new BinaryOperator("Active", true)));

                                                                if (_locAccountMapLines != null && _locAccountMapLines.Count() > 0)
                                                                {
                                                                    double _locTotalAmountDebit = 0;
                                                                    double _locTotalAmountCredit = 0;
                                                                    double _locTotalBalance = 0;

                                                                    foreach (AccountMapLine _locAccountMapLine in _locAccountMapLines)
                                                                    {
                                                                        if (_locAccountMapLine.AccountCharge == AccountCharge.Debit)
                                                                        {
                                                                            _locTotalAmountDebit = _locTotalUnitAmount;
                                                                        }
                                                                        if (_locAccountMapLine.AccountCharge == AccountCharge.Credit)
                                                                        {
                                                                            _locTotalAmountCredit = _locTotalUnitAmount;
                                                                        }

                                                                        GeneralJournal _saveGeneralJournal = new GeneralJournal(_currSession)
                                                                        {
                                                                            Company = _locInventoryTransferOutLine.Company,
                                                                            Workplace = _locInventoryTransferOutLine.Workplace,
                                                                            PostingDate = now,
                                                                            PostingType = PostingType.Sales,
                                                                            OrderType = OrderType.Item,
                                                                            PostingMethod = PostingMethod.Deliver,
                                                                            PostingMethodType = PostingMethodType.Normal,
                                                                            Account = _locAccountMapLine.Account,
                                                                            Debit = _locTotalAmountDebit,
                                                                            Credit = _locTotalAmountCredit,
                                                                            JournalMonth = _locInventoryTransferOutLine.JournalMonth,
                                                                            JournalYear = _locInventoryTransferOutLine.JournalYear,
                                                                            InventoryTransferOut = _locInventoryTransferOutXPO,
                                                                        };

                                                                        _saveGeneralJournal.Save();
                                                                        _saveGeneralJournal.Session.CommitTransaction();

                                                                        #region AccountingPeriodicLine

                                                                        if (_locAccountMapLine.Account.Code != null)
                                                                        {

                                                                            //Cari Account Periodic Line
                                                                            AccountingPeriodicLine _locAPL = _currSession.FindObject<AccountingPeriodicLine>
                                                                                                            (new GroupOperator(GroupOperatorType.And,
                                                                                                            new BinaryOperator("Company", _locInventoryTransferOutLine.Company),
                                                                                                            new BinaryOperator("Workplace", _locInventoryTransferOutLine.Workplace),
                                                                                                            new BinaryOperator("AccountNo", _locAccountMapLine.Account),
                                                                                                            new BinaryOperator("Month", _locInventoryTransferOutLine.JournalMonth),
                                                                                                            new BinaryOperator("Year", _locInventoryTransferOutLine.JournalYear)));
                                                                            if (_locAPL != null)
                                                                            {
                                                                                if (_locAPL.AccountNo != null)
                                                                                {
                                                                                    if (_locAPL.AccountNo.BalanceType == BalanceType.Change)
                                                                                    {
                                                                                        if (_locAPL.AccountNo.AccountCharge == AccountCharge.Debit)
                                                                                        {
                                                                                            if (_locTotalAmountDebit > 0)
                                                                                            {
                                                                                                _locTotalBalance = _locAPL.Balance + _locTotalAmountDebit;
                                                                                            }
                                                                                            if (_locTotalAmountCredit > 0)
                                                                                            {
                                                                                                _locTotalBalance = _locAPL.Balance - _locTotalAmountCredit;
                                                                                            }
                                                                                        }
                                                                                        if (_locAPL.AccountNo.AccountCharge == AccountCharge.Credit)
                                                                                        {
                                                                                            if (_locTotalAmountDebit > 0)
                                                                                            {
                                                                                                _locTotalBalance = _locAPL.Balance - _locTotalAmountDebit;
                                                                                            }
                                                                                            if (_locTotalAmountCredit > 0)
                                                                                            {
                                                                                                _locTotalBalance = _locAPL.Balance + _locTotalAmountCredit;
                                                                                            }
                                                                                        }
                                                                                    }

                                                                                    _locAPL.Balance = _locTotalBalance;
                                                                                    _locAPL.Debit = _locAPL.Debit + _locTotalAmountDebit;
                                                                                    _locAPL.Credit = _locAPL.Credit + _locTotalAmountCredit;
                                                                                    _locAPL.Save();
                                                                                    _locAPL.Session.CommitTransaction();

                                                                                }
                                                                            }
                                                                        }
                                                                        #endregion AccountingPeriodicLine
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        #endregion JournalMapByItems
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = InventoryTransferOut ", ex.ToString());
            }
        }

        #endregion SetGoodsDeliverJournal

        private void SetStatusDeliverInventoryTransferOutLine(Session _currSession, InventoryTransferOut _locInventoryTransferOutXPO)
        {
            try
            {
                DateTime now = DateTime.Now;
                if (_locInventoryTransferOutXPO != null)
                {
                    XPCollection<InventoryTransferOutLine> _locInventoryTransferOutLines = new XPCollection<InventoryTransferOutLine>(_currSession,
                                                            new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("InventoryTransferOut", _locInventoryTransferOutXPO),
                                                            new BinaryOperator("Select", true),
                                                            new GroupOperator(GroupOperatorType.Or,
                                                            new BinaryOperator("Status", Status.Progress),
                                                            new BinaryOperator("Status", Status.Posted))));

                    if (_locInventoryTransferOutLines != null && _locInventoryTransferOutLines.Count > 0)
                    {
                        foreach (InventoryTransferOutLine _locInventoryTransferOutLine in _locInventoryTransferOutLines)
                        {
                            _locInventoryTransferOutLine.Status = Status.Close;
                            _locInventoryTransferOutLine.ActualDate = now;
                            _locInventoryTransferOutLine.ActivationPosting = true;
                            _locInventoryTransferOutLine.StatusDate = now;
                            _locInventoryTransferOutLine.Save();
                            _locInventoryTransferOutLine.Session.CommitTransaction();

                            XPCollection<InventoryTransferOutLot> _locInventoryTransferOutLots = new XPCollection<InventoryTransferOutLot>(_currSession,
                                                                                     new GroupOperator(GroupOperatorType.And,
                                                                                     new BinaryOperator("InventoryTransferOutLine", _locInventoryTransferOutLine),
                                                                                     new BinaryOperator("Select", true),
                                                                                     new GroupOperator(GroupOperatorType.Or,
                                                                                     new BinaryOperator("Status", Status.Progress),
                                                                                     new BinaryOperator("Status", Status.Posted))));

                            if (_locInventoryTransferOutLots != null && _locInventoryTransferOutLots.Count() > 0)
                            {
                                foreach (InventoryTransferOutLot _locInventoryTransferOutLot in _locInventoryTransferOutLots)
                                {
                                    if (_locInventoryTransferOutLot.Status == Status.Progress || _locInventoryTransferOutLot.Status == Status.Posted)
                                    {
                                        _locInventoryTransferOutLot.Status = Status.Close;
                                        _locInventoryTransferOutLot.ActivationPosting = true;
                                        _locInventoryTransferOutLot.StatusDate = now;
                                        _locInventoryTransferOutLot.Save();
                                        _locInventoryTransferOutLot.Session.CommitTransaction();
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = InventoryTransferOut " + ex.ToString());
            }
        }

        private void SetNormalDeliverQuantity(Session _currSession, InventoryTransferOut _locInventoryTransferOutXPO)
        {
            try
            {
                if (_locInventoryTransferOutXPO != null)
                {
                    XPCollection<InventoryTransferOutLine> _locInventoryTransferOutLines = new XPCollection<InventoryTransferOutLine>(_currSession,
                                                                                     new GroupOperator(GroupOperatorType.And,
                                                                                     new BinaryOperator("InventoryTransferOut", _locInventoryTransferOutXPO),
                                                                                     new BinaryOperator("Select", true),
                                                                                     new GroupOperator(GroupOperatorType.Or,
                                                                                     new BinaryOperator("Status", Status.Progress),
                                                                                     new BinaryOperator("Status", Status.Posted),
                                                                                     new BinaryOperator("Status", Status.Close))));

                    if (_locInventoryTransferOutLines != null && _locInventoryTransferOutLines.Count > 0)
                    {
                        foreach (InventoryTransferOutLine _locInventoryTransferOutLine in _locInventoryTransferOutLines)
                        {
                            if (_locInventoryTransferOutLine.DQty > 0 || _locInventoryTransferOutLine.Qty > 0)
                            {
                                _locInventoryTransferOutLine.Select = false;
                                _locInventoryTransferOutLine.Save();
                                _locInventoryTransferOutLine.Session.CommitTransaction();

                                XPCollection<InventoryTransferOutLot> _locInventoryTransferOutLots = new XPCollection<InventoryTransferOutLot>(_currSession,
                                                                                     new GroupOperator(GroupOperatorType.And,
                                                                                     new BinaryOperator("InventoryTransferOutLine", _locInventoryTransferOutLine),
                                                                                     new BinaryOperator("Select", true),
                                                                                     new GroupOperator(GroupOperatorType.Or,
                                                                                     new BinaryOperator("Status", Status.Progress),
                                                                                     new BinaryOperator("Status", Status.Posted))));

                                if (_locInventoryTransferOutLots != null && _locInventoryTransferOutLots.Count() > 0)
                                {
                                    foreach (InventoryTransferOutLot _locInventoryTransferOutLot in _locInventoryTransferOutLots)
                                    {
                                        if (_locInventoryTransferOutLot.Status == Status.Progress || _locInventoryTransferOutLot.Status == Status.Posted)
                                        {
                                            _locInventoryTransferOutLot.Select = false;
                                            _locInventoryTransferOutLot.Save();
                                            _locInventoryTransferOutLot.Session.CommitTransaction();
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = InventoryTransferOut " + ex.ToString());
            }
        }

        private void SetFinalStatusDeliverInventoryTransferOut(Session _currSession, InventoryTransferOut _locInventoryTransferOutXPO)
        {
            try
            {
                DateTime now = DateTime.Now;
                int _locStatusCount = 0;
                int _locInvTransOutLineCount = 0;

                if (_locInventoryTransferOutXPO != null)
                {
                    XPCollection<InventoryTransferOutLine> _locInventoryTransferOutLines = new XPCollection<InventoryTransferOutLine>(_currSession,
                                                                                     new GroupOperator(GroupOperatorType.And,
                                                                                     new BinaryOperator("InventoryTransferOut", _locInventoryTransferOutXPO)));

                    if (_locInventoryTransferOutLines != null && _locInventoryTransferOutLines.Count() > 0)
                    {
                        _locInvTransOutLineCount = _locInventoryTransferOutLines.Count();

                        foreach (InventoryTransferOutLine _locInventoryTransferOutLine in _locInventoryTransferOutLines)
                        {

                            if (_locInventoryTransferOutLine.Status == Status.Close)
                            {
                                _locStatusCount = _locStatusCount + 1;
                            }
                        }

                        if (_locInvTransOutLineCount == _locStatusCount)
                        {
                            _locInventoryTransferOutXPO.ActivationPosting = true;
                            _locInventoryTransferOutXPO.Status = Status.Close;
                            _locInventoryTransferOutXPO.StatusDate = now;
                            _locInventoryTransferOutXPO.PostedCount = _locInventoryTransferOutXPO.PostedCount + 1;
                            _locInventoryTransferOutXPO.Save();
                            _locInventoryTransferOutXPO.Session.CommitTransaction();
                        }
                        else
                        {
                            _locInventoryTransferOutXPO.Status = Status.Posted;
                            _locInventoryTransferOutXPO.StatusDate = now;
                            _locInventoryTransferOutXPO.PostedCount = _locInventoryTransferOutXPO.PostedCount + 1;
                            _locInventoryTransferOutXPO.Save();
                            _locInventoryTransferOutXPO.Session.CommitTransaction();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = InventoryTransferOut " + ex.ToString());
            }
        }

        #endregion Posting

        #region Global Method

        private void SuccessMessageShow(string _locMessage)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 4000;
            options.Message = string.Format("{0}", _locMessage);
            options.Type = InformationType.Success;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Success";
            options.Win.Type = WinMessageType.Flyout;
            Application.ShowViewStrategy.ShowMessage(options);
        }

        private void ErrorMessageShow(string _locMessage)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 2000;
            options.Message = string.Format(_locMessage);
            options.Type = InformationType.Warning;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Error";
            options.Win.Type = WinMessageType.Alert;
            Application.ShowViewStrategy.ShowMessage(options);
        }

        private int GetStockType(StockType objectName)
        {
            int _result = 0;
            try
            {
                if (objectName == StockType.Good)
                {
                    _result = 1;
                }
                else if (objectName == StockType.Bad)
                {
                    _result = 2;
                }
                else
                {
                    _result = 0;
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Module = InventoryTransferOut " + ex.ToString());
            }
            return _result;
        }

        private int GetStockGroup(StockGroup objectName)
        {
            int _result = 0;
            try
            {
                if (objectName == StockGroup.Normal)
                {
                    _result = 1;
                }
                else if (objectName == StockGroup.FreeItem)
                {
                    _result = 2;
                }
                else
                {
                    _result = 0;
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Module = InventoryTransferOut " + ex.ToString());
            }
            return _result;
        }

        private int GetActive(bool objectName)
        {
            int _result = 0;
            try
            {
                if (objectName == true)
                {
                    _result = 1;
                }
                else
                {
                    _result = 0;
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Module = InventoryTransferOut " + ex.ToString());
            }
            return _result;
        }

        #endregion Global Method

    }
}
