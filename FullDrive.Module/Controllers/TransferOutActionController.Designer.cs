﻿namespace FullDrive.Module.Controllers
{
    partial class TransferOutActionController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.TransferOutProgressAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.TransferOutPostingAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.TransferOutListviewFilterSelectionAction = new DevExpress.ExpressApp.Actions.SingleChoiceAction(this.components);
            this.TransferOutGetStockAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            // 
            // TransferOutProgressAction
            // 
            this.TransferOutProgressAction.Caption = "Progress";
            this.TransferOutProgressAction.ConfirmationMessage = null;
            this.TransferOutProgressAction.Id = "TransferOutProgressActionId";
            this.TransferOutProgressAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.TransferOut);
            this.TransferOutProgressAction.ToolTip = null;
            this.TransferOutProgressAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.TransferOutProgressAction_Execute);
            // 
            // TransferOutPostingAction
            // 
            this.TransferOutPostingAction.Caption = "Posting";
            this.TransferOutPostingAction.ConfirmationMessage = null;
            this.TransferOutPostingAction.Id = "TransferOutPostingActionId";
            this.TransferOutPostingAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.TransferOut);
            this.TransferOutPostingAction.ToolTip = null;
            this.TransferOutPostingAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.TransferOutPostingAction_Execute);
            // 
            // TransferOutListviewFilterSelectionAction
            // 
            this.TransferOutListviewFilterSelectionAction.Caption = "Filter";
            this.TransferOutListviewFilterSelectionAction.ConfirmationMessage = null;
            this.TransferOutListviewFilterSelectionAction.Id = "TransferOutListviewFilterSelectionActionId";
            this.TransferOutListviewFilterSelectionAction.ItemType = DevExpress.ExpressApp.Actions.SingleChoiceActionItemType.ItemIsOperation;
            this.TransferOutListviewFilterSelectionAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.TransferOut);
            this.TransferOutListviewFilterSelectionAction.ToolTip = null;
            this.TransferOutListviewFilterSelectionAction.Execute += new DevExpress.ExpressApp.Actions.SingleChoiceActionExecuteEventHandler(this.TransferOutListviewFilterSelectionAction_Execute);
            // 
            // TransferOutGetStockAction
            // 
            this.TransferOutGetStockAction.Caption = "Get Stock";
            this.TransferOutGetStockAction.ConfirmationMessage = null;
            this.TransferOutGetStockAction.Id = "TransferOutGetStockActionId";
            this.TransferOutGetStockAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.TransferOut);
            this.TransferOutGetStockAction.ToolTip = null;
            this.TransferOutGetStockAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.TransferOutGetStockAction_Execute);
            // 
            // TransferOutActionController
            // 
            this.Actions.Add(this.TransferOutProgressAction);
            this.Actions.Add(this.TransferOutPostingAction);
            this.Actions.Add(this.TransferOutListviewFilterSelectionAction);
            this.Actions.Add(this.TransferOutGetStockAction);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SimpleAction TransferOutProgressAction;
        private DevExpress.ExpressApp.Actions.SimpleAction TransferOutPostingAction;
        private DevExpress.ExpressApp.Actions.SingleChoiceAction TransferOutListviewFilterSelectionAction;
        private DevExpress.ExpressApp.Actions.SimpleAction TransferOutGetStockAction;
    }
}
