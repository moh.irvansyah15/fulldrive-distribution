﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;

using FullDrive.Module.BusinessObjects;
using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.Xpo;
using System.Collections;
using DevExpress.Xpo;
using System.IO;
using System.Web;

namespace FullDrive.Module.Controllers
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class ReceivableTransaction2ActionController : ViewController
    {
        private ChoiceActionItem _selectionListviewFilter;

        public ReceivableTransaction2ActionController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.
            #region Filter Status
            ReceivableTransaction2ListviewFilterSelectionAction.Items.Clear();
            foreach (object _currApproval in Enum.GetValues(typeof(CustomProcess.Status)))
            {
                EnumDescriptor _ed = new EnumDescriptor(typeof(CustomProcess.Status));
                _selectionListviewFilter = new ChoiceActionItem(_ed.GetCaption(_currApproval), _currApproval);
                ReceivableTransaction2ListviewFilterSelectionAction.Items.Add(_selectionListviewFilter);
            }
            #endregion Filter Status
        }

        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }
        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        private void ReceivableTransaction2ListviewFilterSelectionAction_Execute(object sender, SingleChoiceActionExecuteEventArgs e)
        {
            try
            {
                if ((View is ListView) & (View.ObjectTypeInfo.Type == typeof(ReceivableTransaction2)))
                {
                    if ((Status)e.SelectedChoiceActionItem.Data == Status.None)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.None, BinaryOperatorType.NotEqual);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Open)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Open, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Progress)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Progress, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Posted)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Posted, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Close)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Close, BinaryOperatorType.Equal);
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = ReceivableTransaction2 " + ex.ToString());
            }
        }

        private void ReceivableTransaction2GetOCMAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;

                if (_objectsToProcess != null)
                {
                    foreach (Object obj in _objectsToProcess)
                    {
                        ReceivableTransaction2 _locReceivableTransaction2OS = (ReceivableTransaction2)_objectSpace.GetObject(obj);

                        if (_locReceivableTransaction2OS != null)
                        {
                            if (_locReceivableTransaction2OS.Session != null)
                            {
                                _currSession = _locReceivableTransaction2OS.Session;
                            }

                            if (_locReceivableTransaction2OS.Code != null && _currSession != null)
                            {
                                _currObjectId = _locReceivableTransaction2OS.Code;

                                ReceivableTransaction2 _locReceivableTransaction2XPO = _currSession.FindObject<ReceivableTransaction2>
                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                     new BinaryOperator("Code", _currObjectId),
                                                                     new GroupOperator(GroupOperatorType.Or,
                                                                     new BinaryOperator("Status", Status.Open),
                                                                     new BinaryOperator("Status", Status.Progress))));

                                if (_locReceivableTransaction2XPO != null)
                                {
                                    if (_locReceivableTransaction2XPO.Picking != null)
                                    {
                                        GetOrderCollectionMonitoring(_currSession, _locReceivableTransaction2XPO.Picking, _locReceivableTransaction2XPO);
                                        SuccessMessageShow("Order Collection Has Been Successfully Getting into Receivable Transaction");
                                    }
                                }
                                else
                                {
                                    ErrorMessageShow("Data Receivable Transaction Not Available");
                                }
                            }
                            else
                            {
                                ErrorMessageShow("Data Receivable Transaction Not Available");
                            }
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = ReceivableTransaction " + ex.ToString());
            }
        }

        private void ReceivableTransaction2ProgressAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;
                double _locTotalAmount = 0;
                double _locTotalAmountCN = 0;
                double _locTotalAmountDN = 0;
                double _locTotalPlan = 0;

                if (_objectsToProcess != null)
                {
                    foreach (Object obj in _objectsToProcess)
                    {
                        ReceivableTransaction2 _locReceivableTransaction2OS = (ReceivableTransaction2)_objectSpace.GetObject(obj);

                        if (_locReceivableTransaction2OS != null)
                        {
                            if (_locReceivableTransaction2OS.Session != null)
                            {
                                _currSession = _locReceivableTransaction2OS.Session;
                            }

                            if (_locReceivableTransaction2OS.Code != null && _currSession != null)
                            {
                                _currObjectId = _locReceivableTransaction2OS.Code;

                                ReceivableTransaction2 _locReceivableTransaction2XPO = _currSession.FindObject<ReceivableTransaction2>
                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                     new BinaryOperator("Code", _currObjectId),
                                                                     new GroupOperator(GroupOperatorType.Or,
                                                                     new BinaryOperator("Status", Status.Open),
                                                                     new BinaryOperator("Status", Status.Progress))));

                                if (_locReceivableTransaction2XPO != null)
                                {
                                    if (_locReceivableTransaction2XPO.Status == Status.Open)
                                    {
                                        _locReceivableTransaction2XPO.Status = Status.Progress;
                                        _locReceivableTransaction2XPO.StatusDate = now;
                                        _locReceivableTransaction2XPO.Save();
                                        _locReceivableTransaction2XPO.Session.CommitTransaction();
                                    }

                                    XPCollection<ReceivableTransactionLine2> _locReceivableTransactionLine2s = new XPCollection<ReceivableTransactionLine2>
                                                           (_currSession, new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("ReceivableTransaction2", _locReceivableTransaction2XPO),
                                                            new BinaryOperator("Status", Status.Open)));

                                    if (_locReceivableTransactionLine2s != null && _locReceivableTransactionLine2s.Count > 0)
                                    {
                                        foreach (ReceivableTransactionLine2 _locReceivableTransactionLine2 in _locReceivableTransactionLine2s)
                                        {
                                            _locTotalAmount = _locTotalAmount + _locReceivableTransactionLine2.Amount;
                                            _locTotalAmountCN = _locTotalAmountCN + _locReceivableTransactionLine2.AmountCN;
                                            _locTotalAmountDN = _locTotalAmountDN + _locReceivableTransactionLine2.AmountDN;
                                            _locTotalPlan = _locTotalPlan + _locReceivableTransactionLine2.Plan;
                                            _locReceivableTransactionLine2.Status = Status.Progress;
                                            _locReceivableTransactionLine2.StatusDate = now;
                                            _locReceivableTransactionLine2.Save();
                                            _locReceivableTransactionLine2.Session.CommitTransaction();
                                        }
                                    }

                                    _locReceivableTransaction2XPO.GrandTotalAmount = _locTotalAmount;
                                    _locReceivableTransaction2XPO.GrandTotalAmountCN = _locTotalAmountCN;
                                    _locReceivableTransaction2XPO.GrandTotalAmountDN = _locTotalAmountDN;
                                    _locReceivableTransaction2XPO.GrandTotalPlan = _locTotalPlan;
                                    _locReceivableTransaction2XPO.Status = Status.Progress;
                                    _locReceivableTransaction2XPO.StatusDate = now;
                                    _locReceivableTransaction2XPO.Save();
                                    _locReceivableTransaction2XPO.Session.CommitTransaction();

                                    
                                    SuccessMessageShow(_locReceivableTransaction2XPO.Code + "has been successfully updated to progress");
                                }
                                else
                                {
                                    ErrorMessageShow("Data Receivable Transaction Not Available");
                                }
                            }
                            else
                            {
                                ErrorMessageShow("Data Receivable Transaction Not Available");
                            }
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = ReceivableTransaction2 " + ex.ToString());
            }
        }

        private void ReceivableTransaction2PostingAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;

                if (_objectsToProcess != null)
                {
                    foreach (Object obj in _objectsToProcess)
                    {
                        ReceivableTransaction2 _locReceivableTransaction2OS = (ReceivableTransaction2)_objectSpace.GetObject(obj);

                        if (_locReceivableTransaction2OS != null)
                        {
                            if (_locReceivableTransaction2OS.Session != null)
                            {
                                _currSession = _locReceivableTransaction2OS.Session;
                            }

                            if (_locReceivableTransaction2OS.Code != null && _currSession != null)
                            {
                                _currObjectId = _locReceivableTransaction2OS.Code;

                                ReceivableTransaction2 _locReceivableTransaction2XPO = _currSession.FindObject<ReceivableTransaction2>
                                                                   (new GroupOperator(GroupOperatorType.And,
                                                                    new BinaryOperator("Code", _currObjectId),
                                                                    new GroupOperator(GroupOperatorType.Or,
                                                                    new BinaryOperator("Status", Status.Progress),
                                                                    new BinaryOperator("Status", Status.Posted))));

                                if (_locReceivableTransaction2XPO != null)
                                {
                                    
                                    ReleaseCreditLimitAmount(_currSession, _locReceivableTransaction2XPO);
                                    SetOutstanding(_currSession, _locReceivableTransaction2XPO);
                                    SetActualAndOverPayment(_currSession, _locReceivableTransaction2XPO);
                                    SetPostedCount(_currSession, _locReceivableTransaction2XPO);
                                    SetReceivableTransactionMonitoring(_currSession, _locReceivableTransaction2XPO);
                                    if (CheckJournalSetup(_currSession, _locReceivableTransaction2XPO) == true)
                                    {
                                        SetReceivableJournal(_currSession, _locReceivableTransaction2XPO);
                                    }
                                    SetStatusReceivableTransactionLine(_currSession, _locReceivableTransaction2XPO);
                                    SetNormal(_currSession, _locReceivableTransaction2XPO);
                                    SetFinalStatusReceivableTransaction(_currSession, _locReceivableTransaction2XPO);
                                    SuccessMessageShow(_locReceivableTransaction2XPO.Code + " has been change successfully to Posted");
                                }
                                else
                                {
                                    ErrorMessageShow("Data Receivable Transaction Not Available");
                                }
                            }
                            else
                            {
                                ErrorMessageShow("Data Receivable Transaction Not Available");
                            }
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = ReceivableTransaction2 " + ex.ToString());
            }
        }

        //=============================================== Code In Here =============================================

        #region GetOrderCollection

        private void GetOrderCollectionMonitoring(Session _currSession, Picking _locPickingXPO, ReceivableTransaction2 _locReceivableTransaction2XPO)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                DateTime now = DateTime.Now;
                string _currSignCode = null;

                SalesInvoice _locSalesInvoice = null;
                PriceGroup _locPriceGroup = null;
                Currency _locCurrency = null;
                BusinessPartner _locSalesToCustomer = null;
                string _locSalesToAddress = null;
                string _locSalesToContact = null;
                City _locSalesToCity = null;
                Country _locSalesToCountry = null;
                BusinessPartner _locBillToCustomer = null;
                string _locBillToAddress = null;
                string _locBillToContact = null;
                City _locBillToCity = null;
                Country _locBillToCountry = null;

                BankAccount _locCompanyBankAccount = null;
                string _locCompanyAccountName = null;
                string _locCompanyAccountNo = null;
                BankAccount _locBankAccount = null;
                string _locAccountName = null;
                string _locAccountNo = null;

                if (_locPickingXPO != null && _locReceivableTransaction2XPO != null)
                {
                    #region OrderCollectionMonitoring
                    //Hanya memindahkan OrderCollectionMonitoring ke ReceivableTransactionLine
                    XPCollection<OrderCollectionMonitoring> _locOrderCollectionMonitorings = new XPCollection<OrderCollectionMonitoring>
                                                                                        (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                        new BinaryOperator("Picking", _locPickingXPO),
                                                                                        new BinaryOperator("Select", true),
                                                                                        new BinaryOperator("Status", Status.Close, BinaryOperatorType.NotEqual)));

                    if (_locOrderCollectionMonitorings != null && _locOrderCollectionMonitorings.Count() > 0)
                    {
                        foreach (OrderCollectionMonitoring _locOrderCollectionMonitoring in _locOrderCollectionMonitorings)
                        {
                            if (_locOrderCollectionMonitoring.ReceivableTransaction == null && _locOrderCollectionMonitoring.ReceivableTransaction2 == null && _locOrderCollectionMonitoring.OrderCollectionLine != null)
                            {
                                _currSignCode = _globFunc.GetNumberingSignUnlockOptimisticRecordBasedOrganization(_currSession.DataLayer, ObjectList.ReceivableTransactionLine2, _locReceivableTransaction2XPO.Company, _locReceivableTransaction2XPO.Workplace);
                                //_currSignCode = _globFunc.GetNumberingSignUnlockOptimisticRecord(_currSession.DataLayer, ObjectList.ReceivableTransactionLine);

                                if (_currSignCode != null)
                                {
                                    #region SalesInvoice
                                    if(_locOrderCollectionMonitoring.SalesInvoice != null)
                                    {
                                        _locSalesInvoice = _locOrderCollectionMonitoring.SalesInvoice;
                                        if(_locOrderCollectionMonitoring.SalesInvoice.PriceGroup != null)
                                        {
                                            _locPriceGroup = _locOrderCollectionMonitoring.SalesInvoice.PriceGroup;
                                        }
                                        if (_locOrderCollectionMonitoring.SalesInvoice.Currency != null)
                                        {
                                            _locCurrency = _locOrderCollectionMonitoring.SalesInvoice.Currency;
                                        }
                                        #region SalesInvoiceInfo
                                        if (_locOrderCollectionMonitoring.SalesInvoice.SalesToCustomer != null)
                                        {
                                            _locSalesToCustomer = _locOrderCollectionMonitoring.SalesInvoice.SalesToCustomer;
                                        }
                                        if (_locOrderCollectionMonitoring.SalesInvoice.SalesToAddress != null)
                                        {
                                            _locSalesToAddress = _locOrderCollectionMonitoring.SalesInvoice.SalesToAddress;
                                        }
                                        if (_locOrderCollectionMonitoring.SalesInvoice.SalesToContact != null)
                                        {
                                            _locSalesToContact = _locOrderCollectionMonitoring.SalesInvoice.SalesToContact;
                                        }
                                        if (_locOrderCollectionMonitoring.SalesInvoice.SalesToCity != null)
                                        {
                                            _locSalesToCity = _locOrderCollectionMonitoring.SalesInvoice.SalesToCity;
                                        }
                                        if (_locOrderCollectionMonitoring.SalesInvoice.SalesToCountry != null)
                                        {
                                            _locSalesToCountry = _locOrderCollectionMonitoring.SalesInvoice.SalesToCountry;
                                        }
                                        if (_locOrderCollectionMonitoring.SalesInvoice.BillToCustomer != null)
                                        {
                                            _locBillToCustomer = _locOrderCollectionMonitoring.SalesInvoice.BillToCustomer;
                                        }
                                        if (_locOrderCollectionMonitoring.SalesInvoice.BillToAddress != null)
                                        {
                                            _locBillToAddress = _locOrderCollectionMonitoring.SalesInvoice.BillToAddress;
                                        }
                                        if (_locOrderCollectionMonitoring.SalesInvoice.BillToContact != null)
                                        {
                                            _locBillToContact = _locOrderCollectionMonitoring.SalesInvoice.BillToContact;
                                        }
                                        if (_locOrderCollectionMonitoring.SalesInvoice.BillToCity != null)
                                        {
                                            _locBillToCity = _locOrderCollectionMonitoring.SalesInvoice.BillToCity;
                                        }
                                        if (_locOrderCollectionMonitoring.SalesInvoice.BillToCountry != null)
                                        {
                                            _locBillToCountry = _locOrderCollectionMonitoring.SalesInvoice.BillToCountry;
                                        }
                                        #endregion SalesInvoiceInfo
                                        #region Bank
                                        if (_locOrderCollectionMonitoring.SalesInvoice.CompanyBankAccount != null)
                                        {
                                            _locCompanyBankAccount = _locOrderCollectionMonitoring.SalesInvoice.CompanyBankAccount;
                                        }
                                        if (_locOrderCollectionMonitoring.SalesInvoice.CompanyAccountName != null)
                                        {
                                            _locCompanyAccountName = _locOrderCollectionMonitoring.SalesInvoice.CompanyAccountName;
                                        }
                                        if (_locOrderCollectionMonitoring.SalesInvoice.CompanyAccountNo != null)
                                        {
                                            _locCompanyAccountNo = _locOrderCollectionMonitoring.SalesInvoice.CompanyAccountNo;
                                        }
                                        if (_locOrderCollectionMonitoring.SalesInvoice.BankAccount != null)
                                        {
                                            _locBankAccount = _locOrderCollectionMonitoring.SalesInvoice.BankAccount;
                                        }
                                        if (_locOrderCollectionMonitoring.SalesInvoice.AccountName != null)
                                        {
                                            _locAccountName = _locOrderCollectionMonitoring.SalesInvoice.AccountName;
                                        }
                                        if (_locOrderCollectionMonitoring.SalesInvoice.AccountNo != null)
                                        {
                                            _locAccountNo = _locOrderCollectionMonitoring.SalesInvoice.AccountNo;
                                        }
                                        #endregion Bank
                                    }
                                    #endregion SalesInvoice
                                    #region ReceivableTransactionLine
                                    ReceivableTransactionLine2 _saveReceivableTransactionLine2 = new ReceivableTransactionLine2(_currSession)
                                    {
                                        SignCode = _currSignCode,
                                        ReceivableTransaction2 = _locReceivableTransaction2XPO,
                                        #region Organization
                                        Company = _locReceivableTransaction2XPO.Company,
                                        Workplace = _locReceivableTransaction2XPO.Workplace,
                                        Division = _locReceivableTransaction2XPO.Division,
                                        Department = _locReceivableTransaction2XPO.Department,
                                        Section = _locReceivableTransaction2XPO.Section,
                                        Employee = _locReceivableTransaction2XPO.Employee,
                                        Div = _locOrderCollectionMonitoring.Div,
                                        Dept = _locOrderCollectionMonitoring.Dept,
                                        Sect = _locOrderCollectionMonitoring.Sect,
                                        PIC = _locOrderCollectionMonitoring.PIC,
                                        Collector = _locOrderCollectionMonitoring.Collector,
                                        #endregion Organization
                                        #region SalesInvoiceInfo
                                        SalesInvoice = _locSalesInvoice,
                                        PriceGroup = _locPriceGroup,
                                        Currency = _locCurrency,
                                        SalesToCustomer = _locSalesToCustomer,
                                        SalesToAddress = _locSalesToAddress,
                                        SalesToContact = _locSalesToContact,
                                        SalesToCity = _locSalesToCity,
                                        SalesToCountry = _locSalesToCountry,
                                        BillToCustomer = _locBillToCustomer,
                                        BillToAddress = _locBillToAddress,
                                        BillToContact = _locBillToContact,
                                        BillToCity = _locBillToCity,
                                        BillToCountry = _locBillToCountry,
                                        PaymentMethodType = _locOrderCollectionMonitoring.PaymentMethodType,
                                        PaymentMethod = _locOrderCollectionMonitoring.PaymentMethod,
                                        PaymentType = _locOrderCollectionMonitoring.PaymentType,
                                        OpenDueDate = _locOrderCollectionMonitoring.OpenDueDate,
                                        ChequeNo = _locOrderCollectionMonitoring.ChequeNo,
                                        EstimatedDate = _locOrderCollectionMonitoring.EstimatedDate,
                                        ActualDate = _locOrderCollectionMonitoring.ActualDate,
                                        DueDate = _locOrderCollectionMonitoring.DueDate,
                                        #endregion SalesInvoiceInfo
                                        #region Bank
                                        CompanyBankAccount = _locCompanyBankAccount,
                                        CompanyAccountName = _locCompanyAccountName,
                                        CompanyAccountNo = _locCompanyAccountNo,
                                        BankAccount = _locBankAccount,
                                        AccountName = _locAccountName,
                                        AccountNo = _locAccountNo,
                                        #endregion Bank
                                        Amount = _locOrderCollectionMonitoring.AmountColl,
                                        AmountDN = _locOrderCollectionMonitoring.AmountDN,
                                        AmountCN = _locOrderCollectionMonitoring.AmountCN,
                                        Plan = _locOrderCollectionMonitoring.AmountColl,
                                        PickingDate = _locOrderCollectionMonitoring.PickingDate,
                                        Picking = _locOrderCollectionMonitoring.Picking,
                                        PickingMonitoring = _locOrderCollectionMonitoring.PickingMonitoring,
                                        OrderCollectionMonitoring = _locOrderCollectionMonitoring,
                                        DocDate = _locOrderCollectionMonitoring.SalesInvoice.DocDate,
                                        JournalMonth = _locOrderCollectionMonitoring.SalesInvoice.JournalMonth,
                                        JournalYear = _locOrderCollectionMonitoring.SalesInvoice.JournalYear,
                                        
                                };
                                    _saveReceivableTransactionLine2.Save();
                                    _saveReceivableTransactionLine2.Session.CommitTransaction();
                                    #endregion ReceivableTransactionLine

                                    ReceivableTransactionLine2 _locReceivableTransactionLine2 = _currSession.FindObject<ReceivableTransactionLine2>
                                                                (new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("Company", _locReceivableTransaction2XPO.Company),
                                                                new BinaryOperator("Workplace", _locReceivableTransaction2XPO.Workplace),
                                                                new BinaryOperator("SignCode", _currSignCode)));
                                    if (_locReceivableTransactionLine2 != null)
                                    {
                                        SetProcessCount(_currSession, _locOrderCollectionMonitoring);
                                        SetStatusOrderCollectionMonitoring(_currSession, _locOrderCollectionMonitoring);
                                        SetNormal(_currSession, _locOrderCollectionMonitoring, _locReceivableTransaction2XPO);
                                    }
                                }
                            }
                        }
                    }
                    #endregion OrderCollectionMonitoring
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = ReceivableTransaction2 ", ex.ToString());
            }
        }

        private void SetProcessCount(Session _currSession, OrderCollectionMonitoring _locOrderCollectionMonitoringXPO)
        {
            try
            {
                if (_locOrderCollectionMonitoringXPO != null)
                {
                    _locOrderCollectionMonitoringXPO.PostedCount = _locOrderCollectionMonitoringXPO.PostedCount + 1;
                    _locOrderCollectionMonitoringXPO.Save();
                    _locOrderCollectionMonitoringXPO.Session.CommitTransaction();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = ReceivableTransaction2 " + ex.ToString());
            }
        }

        private void SetStatusOrderCollectionMonitoring(Session _currSession, OrderCollectionMonitoring _locOrderCollectionMonitoringXPO)
        {
            try
            {
                DateTime now = DateTime.Now;
                if (_locOrderCollectionMonitoringXPO != null)
                {
                    _locOrderCollectionMonitoringXPO.Status = Status.Close;
                    _locOrderCollectionMonitoringXPO.ActivationPosting = true;
                    _locOrderCollectionMonitoringXPO.StatusDate = now;
                    _locOrderCollectionMonitoringXPO.Save();
                    _locOrderCollectionMonitoringXPO.Session.CommitTransaction();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = ReceivableTransaction2 " + ex.ToString());
            }
        }

        private void SetNormal(Session _currSession, OrderCollectionMonitoring _locOrderCollectionMonitoringXPO, ReceivableTransaction2 _locReceivableTransaction2XPO)
        {
            try
            {
                if (_locOrderCollectionMonitoringXPO != null && _locReceivableTransaction2XPO != null)
                {
                    _locOrderCollectionMonitoringXPO.ReceivableTransaction2 = _locReceivableTransaction2XPO;
                    _locOrderCollectionMonitoringXPO.Select = false;
                    _locOrderCollectionMonitoringXPO.Save();
                    _locOrderCollectionMonitoringXPO.Session.CommitTransaction();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = ReceivableTransaction2 " + ex.ToString());
            }
        }

        #endregion GetOrderCollection

        #region Posting

        private void SetOutstanding(Session _currSession, ReceivableTransaction2 _locReceivableTransaction2XPO)
        {
            try
            {
                if (_locReceivableTransaction2XPO != null)
                {
                    XPCollection<ReceivableTransactionLine2> _locReceivableTransactionLine2s = new XPCollection<ReceivableTransactionLine2>(_currSession,
                                                            new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("ReceivableTransaction2", _locReceivableTransaction2XPO),
                                                            new BinaryOperator("Select", true),
                                                            new GroupOperator(GroupOperatorType.Or,
                                                            new BinaryOperator("Status", Status.Progress),
                                                            new BinaryOperator("Status", Status.Posted))));

                    if (_locReceivableTransactionLine2s != null && _locReceivableTransactionLine2s.Count > 0)
                    {
                        double _locOutstanding = 0;

                        foreach (ReceivableTransactionLine2 _locReceivableTransactionLine2 in _locReceivableTransactionLine2s)
                        {
                            #region ProcessCount=0
                            if (_locReceivableTransactionLine2.PostedCount == 0)
                            {
                                if (_locReceivableTransactionLine2.Plan > 0)
                                {
                                    if (_locReceivableTransactionLine2.Amount > 0 && _locReceivableTransactionLine2.Amount <= _locReceivableTransactionLine2.Plan)
                                    {
                                        _locOutstanding = _locReceivableTransactionLine2.Plan - _locReceivableTransactionLine2.Amount;
                                    }
                                }
                                else
                                {
                                    if (_locReceivableTransactionLine2.Amount > 0)
                                    {
                                        _locOutstanding = 0;
                                    }
                                }
                            }
                            #endregion ProcessCount=0

                            #region ProcessCount>0
                            if (_locReceivableTransactionLine2.PostedCount > 0)
                            {
                                if (_locReceivableTransactionLine2.Outstanding > 0)
                                {
                                    _locOutstanding = _locReceivableTransactionLine2.Outstanding - _locReceivableTransactionLine2.Amount;
                                }
                            }
                            #endregion ProcessCount>0

                            _locReceivableTransactionLine2.Outstanding = _locOutstanding;
                            _locReceivableTransactionLine2.Save();
                            _locReceivableTransactionLine2.Session.CommitTransaction();
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = ReceivableTransaction2 " + ex.ToString());
            }
        }

        private void SetActualAndOverPayment(Session _currSession, ReceivableTransaction2 _locReceivableTransaction2XPO)
        {
            try
            {
                if (_locReceivableTransaction2XPO != null)
                {
                    XPCollection<ReceivableTransactionLine2> _locReceivableTransactionLine2s = new XPCollection<ReceivableTransactionLine2>(_currSession,
                                                            new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("ReceivableTransaction2", _locReceivableTransaction2XPO),
                                                            new BinaryOperator("Select", true),
                                                            new GroupOperator(GroupOperatorType.Or,
                                                            new BinaryOperator("Status", Status.Progress),
                                                            new BinaryOperator("Status", Status.Posted))));

                    if (_locReceivableTransactionLine2s != null && _locReceivableTransactionLine2s.Count > 0)
                    {
                        double _locActual = 0;
                        double _locOverPayment = 0;
                        foreach (ReceivableTransactionLine2 _locReceivableTransactionLine2 in _locReceivableTransactionLine2s)
                        {
                            #region ProcessCount=0
                            if (_locReceivableTransactionLine2.PostedCount == 0)
                            {
                                if (_locReceivableTransactionLine2.Plan > 0)
                                {
                                    if (_locReceivableTransactionLine2.Amount > 0)
                                    {
                                        if (_locReceivableTransactionLine2.Amount <= _locReceivableTransactionLine2.Plan)
                                        {
                                            _locActual = _locReceivableTransactionLine2.Amount;
                                        }
                                        else
                                        {
                                            _locActual = _locReceivableTransactionLine2.Plan;
                                            _locOverPayment = _locReceivableTransactionLine2.Amount - _locReceivableTransactionLine2.Plan;
                                        }
                                    }
                                }
                            }
                            #endregion ProcessCount=0

                            #region ProcessCount>0
                            if (_locReceivableTransactionLine2.PostedCount > 0)
                            {
                                if (_locReceivableTransactionLine2.Actual > 0)
                                {
                                    _locActual = _locReceivableTransactionLine2.Actual + _locReceivableTransactionLine2.Amount;
                                    if (_locActual > _locReceivableTransactionLine2.Plan)
                                    {
                                        _locOverPayment = _locActual - _locReceivableTransactionLine2.Plan;
                                        _locActual = _locReceivableTransactionLine2.Plan;
                                    }
                                }
                            }
                            #endregion ProcessCount>0

                            _locReceivableTransactionLine2.Actual = _locActual;
                            _locReceivableTransactionLine2.OverPayment = _locOverPayment;
                            _locReceivableTransactionLine2.Save();
                            _locReceivableTransactionLine2.Session.CommitTransaction();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = ReceivableTransaction2 " + ex.ToString());
            }
        }

        private void ReleaseCreditLimitAmount(Session _currSession, ReceivableTransaction2 _locReceivableTransaction2XPO)
        {
            try
            {
                if (_locReceivableTransaction2XPO != null)
                {
                    XPCollection<ReceivableTransactionLine2> _locReceivableTransactionLine2s = new XPCollection<ReceivableTransactionLine2>(_currSession,
                                                            new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("ReceivableTransaction2", _locReceivableTransaction2XPO),
                                                            new BinaryOperator("Select", true),
                                                            new GroupOperator(GroupOperatorType.Or,
                                                            new BinaryOperator("Status", Status.Progress),
                                                            new BinaryOperator("Status", Status.Posted))));

                    if (_locReceivableTransactionLine2s != null && _locReceivableTransactionLine2s.Count > 0)
                    {

                        foreach (ReceivableTransactionLine2 _locReceivableTransactionLine2 in _locReceivableTransactionLine2s)
                        {
                            if (_locReceivableTransactionLine2.SalesInvoice != null)
                            {
                                double _locCurrCredit = 0;
                                if (_locReceivableTransactionLine2.SalesInvoice.CreditLimitList != null)
                                {
                                    _locCurrCredit = _locReceivableTransactionLine2.SalesInvoice.CreditLimitList.CurrCredit - _locReceivableTransactionLine2.Amount;
                                    if (_locCurrCredit < 0)
                                    {
                                        _locCurrCredit = 0;
                                    }
                                    _locReceivableTransactionLine2.SalesInvoice.CreditLimitList.CurrCredit = _locCurrCredit;
                                    _locReceivableTransactionLine2.SalesInvoice.CreditLimitList.Save();
                                    _locReceivableTransactionLine2.SalesInvoice.CreditLimitList.Session.CommitTransaction();
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = ReceivableTransaction2 " + ex.ToString());
            }
        }

        private void SetPostedCount(Session _currSession, ReceivableTransaction2 _locReceivableTransaction2XPO)
        {
            try
            {
                if (_locReceivableTransaction2XPO != null)
                {
                    XPCollection<ReceivableTransactionLine2> _locReceivableTransactionLine2s = new XPCollection<ReceivableTransactionLine2>(_currSession,
                                                                        new GroupOperator(GroupOperatorType.And,
                                                                        new BinaryOperator("ReceivableTransaction2", _locReceivableTransaction2XPO),
                                                                        new BinaryOperator("Select", true),
                                                                        new GroupOperator(GroupOperatorType.Or,
                                                                        new BinaryOperator("Status", Status.Progress),
                                                                        new BinaryOperator("Status", Status.Posted))));

                    if (_locReceivableTransactionLine2s != null && _locReceivableTransactionLine2s.Count() > 0)
                    {
                        foreach (ReceivableTransactionLine2 _locReceivableTransactionLine2 in _locReceivableTransactionLine2s)
                        {
                            if (_locReceivableTransactionLine2.PostedCount == 0)
                            {
                                _locReceivableTransactionLine2.OpenCN = true;
                                _locReceivableTransactionLine2.OpenDN = true;
                            }
                            _locReceivableTransactionLine2.PostedCount = _locReceivableTransactionLine2.PostedCount + 1;
                            _locReceivableTransactionLine2.Save();
                            _locReceivableTransactionLine2.Session.CommitTransaction();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = ReceivableTransaction2 " + ex.ToString());
            }
        }

        private void SetReceivableTransactionMonitoring(Session _currSession, ReceivableTransaction2 _locReceivableTransaction2XPO)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                DateTime now = DateTime.Now;

                if (_locReceivableTransaction2XPO != null)
                {
                    XPCollection<ReceivableTransactionLine2> _locReceivableTransactionLine2s = new XPCollection<ReceivableTransactionLine2>(_currSession,
                                                                        new GroupOperator(GroupOperatorType.And,
                                                                        new BinaryOperator("ReceivableTransaction2", _locReceivableTransaction2XPO),
                                                                        new BinaryOperator("Select", true),
                                                                        new GroupOperator(GroupOperatorType.Or,
                                                                        new BinaryOperator("Status", Status.Progress),
                                                                        new BinaryOperator("Status", Status.Posted))));

                    if (_locReceivableTransactionLine2s != null && _locReceivableTransactionLine2s.Count() > 0)
                    {
                        foreach (ReceivableTransactionLine2 _locReceivableTransactionLine2 in _locReceivableTransactionLine2s)
                        {
                            ReceivableTransactionMonitoring _saveDataReceivableTransactionMonitoring = new ReceivableTransactionMonitoring(_currSession)
                            {
                                ReceivableTransaction2 = _locReceivableTransaction2XPO,
                                ReceivableTransactionLine2 = _locReceivableTransactionLine2,
                                #region Organization
                                Company = _locReceivableTransaction2XPO.Company,
                                Workplace = _locReceivableTransaction2XPO.Workplace,
                                Division = _locReceivableTransaction2XPO.Division,
                                Department = _locReceivableTransaction2XPO.Department,
                                Section = _locReceivableTransaction2XPO.Section,
                                Employee = _locReceivableTransaction2XPO.Employee,
                                Div = _locReceivableTransactionLine2.Div,
                                Dept = _locReceivableTransactionLine2.Dept,
                                Sect = _locReceivableTransactionLine2.Section,
                                PIC = _locReceivableTransactionLine2.PIC,
                                Collector = _locReceivableTransactionLine2.Collector,
                                #endregion Organization
                                Vehicle = _locReceivableTransactionLine2.Vehicle,
                                Customer = _locReceivableTransactionLine2.BillToCustomer,
                                #region SalesInvoiceInfo
                                SalesInvoice = _locReceivableTransactionLine2.SalesInvoice,
                                SalesToCustomer = _locReceivableTransactionLine2.SalesToCustomer,
                                SalesToAddress = _locReceivableTransactionLine2.SalesToAddress,
                                SalesToContact = _locReceivableTransactionLine2.SalesToContact,
                                SalesToCity = _locReceivableTransactionLine2.SalesToCity,
                                SalesToCountry = _locReceivableTransactionLine2.SalesToCountry,
                                BillToCustomer = _locReceivableTransactionLine2.BillToCustomer,
                                BillToAddress = _locReceivableTransactionLine2.BillToAddress,
                                BillToContact = _locReceivableTransactionLine2.BillToContact,
                                BillToCity = _locReceivableTransactionLine2.BillToCity,
                                BillToCountry = _locReceivableTransactionLine2.BillToCountry,
                                #endregion SalesInvoiceInfo
                                #region Bank
                                CompanyBankAccount = _locReceivableTransactionLine2.CompanyBankAccount,
                                CompanyAccountName = _locReceivableTransactionLine2.CompanyAccountName,
                                CompanyAccountNo = _locReceivableTransactionLine2.CompanyAccountNo,
                                BankAccount = _locReceivableTransactionLine2.BankAccount,
                                AccountName = _locReceivableTransactionLine2.AccountName,
                                AccountNo = _locReceivableTransactionLine2.AccountNo,
                                #endregion Bank
                                Currency = _locReceivableTransactionLine2.Currency,
                                PriceGroup = _locReceivableTransactionLine2.PriceGroup,
                                PostingMethod = _locReceivableTransactionLine2.PostingMethod,
                                PaymentMethod = _locReceivableTransactionLine2.PaymentMethod,
                                PaymentMethodType = _locReceivableTransactionLine2.PaymentMethodType,
                                PaymentType = _locReceivableTransactionLine2.PaymentType,
                                OpenDueDate = _locReceivableTransactionLine2.OpenDueDate,
                                ChequeNo = _locReceivableTransactionLine2.ChequeNo,
                                EstimatedDate = _locReceivableTransactionLine2.EstimatedDate,
                                ActualDate = _locReceivableTransactionLine2.ActualDate,
                                DueDate = _locReceivableTransactionLine2.DueDate,
                                Amount = _locReceivableTransactionLine2.Amount,
                                AmountCN = _locReceivableTransactionLine2.AmountCN,
                                AmountDN = _locReceivableTransactionLine2.AmountDN,
                                Plan = _locReceivableTransactionLine2.Plan,
                                Outstanding = _locReceivableTransactionLine2.Outstanding,
                                Actual = _locReceivableTransactionLine2.Actual,
                                OverPayment = _locReceivableTransactionLine2.OverPayment,
                                DocDate = _locReceivableTransactionLine2.DocDate,
                                JournalMonth = _locReceivableTransactionLine2.JournalMonth,
                                JournalYear = _locReceivableTransactionLine2.JournalYear,
                                Picking = _locReceivableTransactionLine2.Picking,
                                PickingDate = _locReceivableTransactionLine2.PickingDate,
                                PickingMonitoring = _locReceivableTransactionLine2.PickingMonitoring,
                                PickingLine = _locReceivableTransactionLine2.PickingMonitoring.PickingLine,
                            };
                            _saveDataReceivableTransactionMonitoring.Save();
                            _saveDataReceivableTransactionMonitoring.Session.CommitTransaction();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = ReceivableTransaction2 ", ex.ToString());
            }
        }

        #region JournalReceivable

        private bool CheckJournalSetup(Session _currSession, ReceivableTransaction2 _locReceivableTransaction2XPO)
        {
            bool _result = false;
            try
            {
                if (_locReceivableTransaction2XPO != null)
                {
                    if (_locReceivableTransaction2XPO.Company != null && _locReceivableTransaction2XPO.Workplace != null)
                    {
                        JournalSetupDetail _locJournalSetupDetail = _currSession.FindObject<JournalSetupDetail>
                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                    new BinaryOperator("Company", _locReceivableTransaction2XPO.Company),
                                                                    new BinaryOperator("Workplace", _locReceivableTransaction2XPO.Workplace),
                                                                    new BinaryOperator("JournalSetupType", JournalSetupType.Journal),
                                                                    new BinaryOperator("ObjectList", ObjectList.ReceivableTransaction2),
                                                                    new BinaryOperator("JournalProcess", JournalProcess.JournalReceivable),
                                                                    new BinaryOperator("Active", true)));

                        if (_locJournalSetupDetail != null)
                        {
                            _result = true;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = ReceivableTransaction2 ", ex.ToString());
            }

            return _result;
        }

        private void SetReceivableJournal(Session _currSession, ReceivableTransaction2 _locReceivableTransaction2XPO)
        {
            try
            {
                DateTime now = DateTime.Now;
                double _locTotalAmountCollBP = 0;
                double _locTotalAmountCollBPDN = 0;
                double _locTotalAmountCollBPCN = 0;
                double _locTotalAmountCollBPOP = 0;
                double _locTotalAmountCollBPLP = 0;
                double _locTotalAmountCollC = 0;
                double _locTotalAmountCollCDN = 0;
                double _locTotalAmountCollCCN = 0;
                double _locTotalAmountCollCOP = 0;
                double _locTotalAmountCollCLP = 0;
                BankAccountGroup _locBankAccountGroup = null;
                BankAccountGroup _locCompanyBankAccountGroup = null;
                BusinessPartnerAccountGroup _locBusinessPartnerAccountGroup = null;
                CompanyAccountGroup _locCompanyAccountGroup = null;

                if (_locReceivableTransaction2XPO != null)
                {
                    XPCollection<ReceivableTransactionLine2> _locReceivableTransactionLine2s = new XPCollection<ReceivableTransactionLine2>(_currSession,
                                                                        new GroupOperator(GroupOperatorType.And,
                                                                        new BinaryOperator("ReceivableTransaction2", _locReceivableTransaction2XPO),
                                                                        new BinaryOperator("Select", true),
                                                                        new GroupOperator(GroupOperatorType.Or,
                                                                        new BinaryOperator("Status", Status.Progress),
                                                                        new BinaryOperator("Status", Status.Posted))));

                    if (_locReceivableTransactionLine2s != null && _locReceivableTransactionLine2s.Count() > 0)
                    {
                        foreach (ReceivableTransactionLine2 _locReceivableTransactionLine2 in _locReceivableTransactionLine2s)
                        {

                            #region NonBankAccount

                            #region JournalMapBusinessPartnerAccountGroup

                            if (_locReceivableTransactionLine2.SalesInvoice != null)
                            {
                                if (_locReceivableTransactionLine2.SalesInvoice.BillToCustomer != null)
                                {
                                    BusinessPartnerAccount _locBusinessPartnerAccount = _currSession.FindObject<BusinessPartnerAccount>(new GroupOperator(GroupOperatorType.And,
                                                                    new BinaryOperator("Company", _locReceivableTransactionLine2.Company),
                                                                    new BinaryOperator("Workplace", _locReceivableTransactionLine2.Workplace),
                                                                    new BinaryOperator("BusinessPartner", _locReceivableTransactionLine2.SalesInvoice.BillToCustomer),
                                                                    new BinaryOperator("Active", true)));
                                    if(_locBusinessPartnerAccount != null)
                                    {
                                        if (_locBusinessPartnerAccount.BusinessPartnerAccountGroup != null)
                                        {
                                            _locBusinessPartnerAccountGroup = _locBusinessPartnerAccount.BusinessPartnerAccountGroup;

                                            _locTotalAmountCollBP = _locReceivableTransactionLine2.Amount;
                                            _locTotalAmountCollBPDN = _locReceivableTransactionLine2.AmountDN;
                                            _locTotalAmountCollBPCN = _locReceivableTransactionLine2.AmountCN;
                                            _locTotalAmountCollBPOP = _locReceivableTransactionLine2.OverPayment;
                                            _locTotalAmountCollBPLP = _locReceivableTransactionLine2.Outstanding;

                                            XPCollection<JournalMap> _locJournalMapItems = new XPCollection<JournalMap>
                                                                               (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                new BinaryOperator("Company", _locReceivableTransactionLine2.Company),
                                                                                new BinaryOperator("Workplace", _locReceivableTransactionLine2.Workplace),
                                                                                new BinaryOperator("BusinessPartnerAccountGroup", _locBusinessPartnerAccountGroup),
                                                                                new BinaryOperator("Active", true)));

                                            if (_locJournalMapItems != null && _locJournalMapItems.Count() > 0)
                                            {
                                                foreach (JournalMap _locJournalMapItem in _locJournalMapItems)
                                                {
                                                    XPCollection<JournalMapLine> _locJournalMapLines = new XPCollection<JournalMapLine>
                                                                                                       (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                        new BinaryOperator("Company", _locReceivableTransactionLine2.Company),
                                                                                                        new BinaryOperator("Workplace", _locReceivableTransactionLine2.Workplace),
                                                                                                        new BinaryOperator("JournalMap", _locJournalMapItem),
                                                                                                        new BinaryOperator("Active", true)));

                                                    if (_locJournalMapLines != null && _locJournalMapLines.Count() > 0)
                                                    {
                                                        foreach (JournalMapLine _locJournalMapLine in _locJournalMapLines)
                                                        {
                                                            if (_locJournalMapLine.AccountMap != null)
                                                            {
                                                                #region NormalAmount
                                                                if (_locJournalMapLine.AccountMap.PostingType == PostingType.Sales && _locJournalMapLine.AccountMap.OrderType == OrderType.Item
                                                                    && _locJournalMapLine.AccountMap.PostingMethod == PostingMethod.Bill && _locJournalMapLine.AccountMap.PostingMethodType == PostingMethodType.Normal)
                                                                {
                                                                    XPCollection<AccountMapLine> _locAccountMapLines = new XPCollection<AccountMapLine>
                                                                                                                   (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                                    new BinaryOperator("Company", _locReceivableTransactionLine2.Company),
                                                                                                                    new BinaryOperator("Workplace", _locReceivableTransactionLine2.Workplace),
                                                                                                                    new BinaryOperator("AccountMap", _locJournalMapLine.AccountMap),
                                                                                                                    new BinaryOperator("Active", true)));

                                                                    if (_locAccountMapLines != null && _locAccountMapLines.Count() > 0)
                                                                    {
                                                                        double _locTotalAmountDebit = 0;
                                                                        double _locTotalAmountCredit = 0;
                                                                        double _locTotalBalance = 0;

                                                                        foreach (AccountMapLine _locAccountMapLine in _locAccountMapLines)
                                                                        {
                                                                            if (_locAccountMapLine.AccountCharge == AccountCharge.Debit)
                                                                            {
                                                                                _locTotalAmountDebit = _locTotalAmountCollBP;
                                                                            }
                                                                            if (_locAccountMapLine.AccountCharge == AccountCharge.Credit)
                                                                            {
                                                                                _locTotalAmountCredit = _locTotalAmountCollBP;
                                                                            }

                                                                            GeneralJournal _saveGeneralJournal = new GeneralJournal(_currSession)
                                                                            {
                                                                                Company = _locReceivableTransactionLine2.Company,
                                                                                Workplace = _locReceivableTransactionLine2.Workplace,
                                                                                PostingDate = now,
                                                                                PostingType = PostingType.Sales,
                                                                                OrderType = OrderType.Item,
                                                                                PostingMethod = PostingMethod.Bill,
                                                                                PostingMethodType = PostingMethodType.Normal,
                                                                                Account = _locAccountMapLine.Account,
                                                                                Debit = _locTotalAmountDebit,
                                                                                Credit = _locTotalAmountCredit,
                                                                                JournalMonth = _locReceivableTransactionLine2.JournalMonth,
                                                                                JournalYear = _locReceivableTransactionLine2.JournalYear,
                                                                                ReceivableTransaction2 = _locReceivableTransaction2XPO,
                                                                            };

                                                                            _saveGeneralJournal.Save();
                                                                            _saveGeneralJournal.Session.CommitTransaction();

                                                                            #region AccountingPeriodicLine

                                                                            if (_locAccountMapLine.Account.Code != null)
                                                                            {

                                                                                //Cari Account Periodic Line
                                                                                AccountingPeriodicLine _locAPL = _currSession.FindObject<AccountingPeriodicLine>
                                                                                                                (new GroupOperator(GroupOperatorType.And,
                                                                                                                new BinaryOperator("Company", _locReceivableTransactionLine2.Company),
                                                                                                                new BinaryOperator("Workplace", _locReceivableTransactionLine2.Workplace),
                                                                                                                new BinaryOperator("AccountNo", _locAccountMapLine.Account),
                                                                                                                new BinaryOperator("Month", _locReceivableTransactionLine2.JournalMonth),
                                                                                                                new BinaryOperator("Year", _locReceivableTransactionLine2.JournalYear)));
                                                                                if (_locAPL != null)
                                                                                {
                                                                                    if (_locAPL.AccountNo != null)
                                                                                    {
                                                                                        if (_locAPL.AccountNo.BalanceType == BalanceType.Change)
                                                                                        {
                                                                                            if (_locAPL.AccountNo.AccountCharge == AccountCharge.Debit)
                                                                                            {
                                                                                                if (_locTotalAmountDebit > 0)
                                                                                                {
                                                                                                    _locTotalBalance = _locAPL.Balance + _locTotalAmountDebit;
                                                                                                }
                                                                                                if (_locTotalAmountCredit > 0)
                                                                                                {
                                                                                                    _locTotalBalance = _locAPL.Balance - _locTotalAmountCredit;
                                                                                                }
                                                                                            }
                                                                                            if (_locAPL.AccountNo.AccountCharge == AccountCharge.Credit)
                                                                                            {
                                                                                                if (_locTotalAmountDebit > 0)
                                                                                                {
                                                                                                    _locTotalBalance = _locAPL.Balance - _locTotalAmountDebit;
                                                                                                }
                                                                                                if (_locTotalAmountCredit > 0)
                                                                                                {
                                                                                                    _locTotalBalance = _locAPL.Balance + _locTotalAmountCredit;
                                                                                                }
                                                                                            }
                                                                                        }

                                                                                        _locAPL.Balance = _locTotalBalance;
                                                                                        _locAPL.Debit = _locAPL.Debit + _locTotalAmountDebit;
                                                                                        _locAPL.Credit = _locAPL.Credit + _locTotalAmountCredit;
                                                                                        _locAPL.Save();
                                                                                        _locAPL.Session.CommitTransaction();

                                                                                    }
                                                                                }
                                                                            }
                                                                            #endregion COA
                                                                        }
                                                                    }
                                                                }
                                                                #endregion NormalAmount   
                                                                #region AmountDN
                                                                if (_locTotalAmountCollBPDN > 0)
                                                                {
                                                                    if (_locJournalMapLine.AccountMap.PostingType == PostingType.Sales && _locJournalMapLine.AccountMap.OrderType == OrderType.Item
                                                                    && _locJournalMapLine.AccountMap.PostingMethod == PostingMethod.Bill && _locJournalMapLine.AccountMap.PostingMethodType == PostingMethodType.AmountDN)
                                                                    {
                                                                        XPCollection<AccountMapLine> _locAccountMapLines = new XPCollection<AccountMapLine>
                                                                                                                       (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                                        new BinaryOperator("Company", _locReceivableTransactionLine2.Company),
                                                                                                                        new BinaryOperator("Workplace", _locReceivableTransactionLine2.Workplace),
                                                                                                                        new BinaryOperator("AccountMap", _locJournalMapLine.AccountMap),
                                                                                                                        new BinaryOperator("Active", true)));

                                                                        if (_locAccountMapLines != null && _locAccountMapLines.Count() > 0)
                                                                        {
                                                                            double _locTotalAmountDebit = 0;
                                                                            double _locTotalAmountCredit = 0;
                                                                            double _locTotalBalance = 0;

                                                                            foreach (AccountMapLine _locAccountMapLine in _locAccountMapLines)
                                                                            {
                                                                                if (_locAccountMapLine.AccountCharge == AccountCharge.Debit)
                                                                                {
                                                                                    _locTotalAmountDebit = _locTotalAmountCollBPDN;
                                                                                }
                                                                                if (_locAccountMapLine.AccountCharge == AccountCharge.Credit)
                                                                                {
                                                                                    _locTotalAmountCredit = _locTotalAmountCollBPDN;
                                                                                }

                                                                                GeneralJournal _saveGeneralJournal = new GeneralJournal(_currSession)
                                                                                {
                                                                                    Company = _locReceivableTransactionLine2.Company,
                                                                                    Workplace = _locReceivableTransactionLine2.Workplace,
                                                                                    PostingDate = now,
                                                                                    PostingType = PostingType.Sales,
                                                                                    PostingMethod = PostingMethod.Bill,
                                                                                    PostingMethodType = PostingMethodType.AmountDN,
                                                                                    Account = _locAccountMapLine.Account,
                                                                                    Debit = _locTotalAmountDebit,
                                                                                    Credit = _locTotalAmountCredit,
                                                                                    JournalMonth = _locReceivableTransactionLine2.JournalMonth,
                                                                                    JournalYear = _locReceivableTransactionLine2.JournalYear,
                                                                                    ReceivableTransaction2 = _locReceivableTransaction2XPO,
                                                                                };

                                                                                _saveGeneralJournal.Save();
                                                                                _saveGeneralJournal.Session.CommitTransaction();

                                                                                #region AccountingPeriodicLine

                                                                                if (_locAccountMapLine.Account.Code != null)
                                                                                {

                                                                                    //Cari Account Periodic Line
                                                                                    AccountingPeriodicLine _locAPL = _currSession.FindObject<AccountingPeriodicLine>
                                                                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                                                                    new BinaryOperator("Company", _locReceivableTransactionLine2.Company),
                                                                                                                    new BinaryOperator("Workplace", _locReceivableTransactionLine2.Workplace),
                                                                                                                    new BinaryOperator("AccountNo", _locAccountMapLine.Account),
                                                                                                                    new BinaryOperator("Month", _locReceivableTransactionLine2.JournalMonth),
                                                                                                                    new BinaryOperator("Year", _locReceivableTransactionLine2.JournalYear)));
                                                                                    if (_locAPL != null)
                                                                                    {
                                                                                        if (_locAPL.AccountNo != null)
                                                                                        {
                                                                                            if (_locAPL.AccountNo.BalanceType == BalanceType.Change)
                                                                                            {
                                                                                                if (_locAPL.AccountNo.AccountCharge == AccountCharge.Debit)
                                                                                                {
                                                                                                    if (_locTotalAmountDebit > 0)
                                                                                                    {
                                                                                                        _locTotalBalance = _locAPL.Balance + _locTotalAmountDebit;
                                                                                                    }
                                                                                                    if (_locTotalAmountCredit > 0)
                                                                                                    {
                                                                                                        _locTotalBalance = _locAPL.Balance - _locTotalAmountCredit;
                                                                                                    }
                                                                                                }
                                                                                                if (_locAPL.AccountNo.AccountCharge == AccountCharge.Credit)
                                                                                                {
                                                                                                    if (_locTotalAmountDebit > 0)
                                                                                                    {
                                                                                                        _locTotalBalance = _locAPL.Balance - _locTotalAmountDebit;
                                                                                                    }
                                                                                                    if (_locTotalAmountCredit > 0)
                                                                                                    {
                                                                                                        _locTotalBalance = _locAPL.Balance + _locTotalAmountCredit;
                                                                                                    }
                                                                                                }
                                                                                            }

                                                                                            _locAPL.Balance = _locTotalBalance;
                                                                                            _locAPL.Debit = _locAPL.Debit + _locTotalAmountDebit;
                                                                                            _locAPL.Credit = _locAPL.Credit + _locTotalAmountCredit;
                                                                                            _locAPL.Save();
                                                                                            _locAPL.Session.CommitTransaction();

                                                                                        }
                                                                                    }
                                                                                }
                                                                                #endregion COA
                                                                            }
                                                                        }
                                                                    }
                                                                }

                                                                #endregion AmountDN
                                                                #region AmountCN
                                                                if (_locTotalAmountCollBPCN > 0)
                                                                {
                                                                    if (_locJournalMapLine.AccountMap.PostingType == PostingType.Sales && _locJournalMapLine.AccountMap.OrderType == OrderType.Item
                                                                    && _locJournalMapLine.AccountMap.PostingMethod == PostingMethod.Bill && _locJournalMapLine.AccountMap.PostingMethodType == PostingMethodType.AmountCN)
                                                                    {
                                                                        XPCollection<AccountMapLine> _locAccountMapLines = new XPCollection<AccountMapLine>
                                                                                                                       (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                                        new BinaryOperator("Company", _locReceivableTransactionLine2.Company),
                                                                                                                        new BinaryOperator("Workplace", _locReceivableTransactionLine2.Workplace),
                                                                                                                        new BinaryOperator("AccountMap", _locJournalMapLine.AccountMap),
                                                                                                                        new BinaryOperator("Active", true)));

                                                                        if (_locAccountMapLines != null && _locAccountMapLines.Count() > 0)
                                                                        {
                                                                            double _locTotalAmountDebit = 0;
                                                                            double _locTotalAmountCredit = 0;
                                                                            double _locTotalBalance = 0;

                                                                            foreach (AccountMapLine _locAccountMapLine in _locAccountMapLines)
                                                                            {
                                                                                if (_locAccountMapLine.AccountCharge == AccountCharge.Debit)
                                                                                {
                                                                                    _locTotalAmountDebit = _locTotalAmountCollBPCN;
                                                                                }
                                                                                if (_locAccountMapLine.AccountCharge == AccountCharge.Credit)
                                                                                {
                                                                                    _locTotalAmountCredit = _locTotalAmountCollBPCN;
                                                                                }

                                                                                GeneralJournal _saveGeneralJournal = new GeneralJournal(_currSession)
                                                                                {
                                                                                    Company = _locReceivableTransactionLine2.Company,
                                                                                    Workplace = _locReceivableTransactionLine2.Workplace,
                                                                                    PostingDate = now,
                                                                                    PostingType = PostingType.Sales,
                                                                                    PostingMethod = PostingMethod.Bill,
                                                                                    PostingMethodType = PostingMethodType.AmountCN,
                                                                                    Account = _locAccountMapLine.Account,
                                                                                    Debit = _locTotalAmountDebit,
                                                                                    Credit = _locTotalAmountCredit,
                                                                                    JournalMonth = _locReceivableTransactionLine2.JournalMonth,
                                                                                    JournalYear = _locReceivableTransactionLine2.JournalYear,
                                                                                    ReceivableTransaction2 = _locReceivableTransaction2XPO,
                                                                                };

                                                                                _saveGeneralJournal.Save();
                                                                                _saveGeneralJournal.Session.CommitTransaction();

                                                                                #region AccountingPeriodicLine

                                                                                if (_locAccountMapLine.Account.Code != null)
                                                                                {

                                                                                    //Cari Account Periodic Line
                                                                                    AccountingPeriodicLine _locAPL = _currSession.FindObject<AccountingPeriodicLine>
                                                                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                                                                    new BinaryOperator("Company", _locReceivableTransactionLine2.Company),
                                                                                                                    new BinaryOperator("Workplace", _locReceivableTransactionLine2.Workplace),
                                                                                                                    new BinaryOperator("AccountNo", _locAccountMapLine.Account),
                                                                                                                    new BinaryOperator("Month", _locReceivableTransactionLine2.JournalMonth),
                                                                                                                    new BinaryOperator("Year", _locReceivableTransactionLine2.JournalYear)));
                                                                                    if (_locAPL != null)
                                                                                    {
                                                                                        if (_locAPL.AccountNo != null)
                                                                                        {
                                                                                            if (_locAPL.AccountNo.BalanceType == BalanceType.Change)
                                                                                            {
                                                                                                if (_locAPL.AccountNo.AccountCharge == AccountCharge.Debit)
                                                                                                {
                                                                                                    if (_locTotalAmountDebit > 0)
                                                                                                    {
                                                                                                        _locTotalBalance = _locAPL.Balance + _locTotalAmountDebit;
                                                                                                    }
                                                                                                    if (_locTotalAmountCredit > 0)
                                                                                                    {
                                                                                                        _locTotalBalance = _locAPL.Balance - _locTotalAmountCredit;
                                                                                                    }
                                                                                                }
                                                                                                if (_locAPL.AccountNo.AccountCharge == AccountCharge.Credit)
                                                                                                {
                                                                                                    if (_locTotalAmountDebit > 0)
                                                                                                    {
                                                                                                        _locTotalBalance = _locAPL.Balance - _locTotalAmountDebit;
                                                                                                    }
                                                                                                    if (_locTotalAmountCredit > 0)
                                                                                                    {
                                                                                                        _locTotalBalance = _locAPL.Balance + _locTotalAmountCredit;
                                                                                                    }
                                                                                                }
                                                                                            }

                                                                                            _locAPL.Balance = _locTotalBalance;
                                                                                            _locAPL.Debit = _locAPL.Debit + _locTotalAmountDebit;
                                                                                            _locAPL.Credit = _locAPL.Credit + _locTotalAmountCredit;
                                                                                            _locAPL.Save();
                                                                                            _locAPL.Session.CommitTransaction();

                                                                                        }
                                                                                    }
                                                                                }
                                                                                #endregion COA
                                                                            }
                                                                        }
                                                                    }
                                                                }

                                                                #endregion AmountCN
                                                                #region OverPayment
                                                                if (_locTotalAmountCollBPOP > 0)
                                                                {
                                                                    if (_locJournalMapLine.AccountMap.PostingType == PostingType.Sales && _locJournalMapLine.AccountMap.OrderType == OrderType.Item
                                                                    && _locJournalMapLine.AccountMap.PostingMethod == PostingMethod.Bill && _locJournalMapLine.AccountMap.PostingMethodType == PostingMethodType.OverPayment)
                                                                    {
                                                                        XPCollection<AccountMapLine> _locAccountMapLines = new XPCollection<AccountMapLine>
                                                                                                                       (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                                        new BinaryOperator("Company", _locReceivableTransactionLine2.Company),
                                                                                                                        new BinaryOperator("Workplace", _locReceivableTransactionLine2.Workplace),
                                                                                                                        new BinaryOperator("AccountMap", _locJournalMapLine.AccountMap),
                                                                                                                        new BinaryOperator("Active", true)));

                                                                        if (_locAccountMapLines != null && _locAccountMapLines.Count() > 0)
                                                                        {
                                                                            double _locTotalAmountDebit = 0;
                                                                            double _locTotalAmountCredit = 0;
                                                                            double _locTotalBalance = 0;

                                                                            foreach (AccountMapLine _locAccountMapLine in _locAccountMapLines)
                                                                            {
                                                                                if (_locAccountMapLine.AccountCharge == AccountCharge.Debit)
                                                                                {
                                                                                    _locTotalAmountDebit = _locTotalAmountCollBPOP;
                                                                                }
                                                                                if (_locAccountMapLine.AccountCharge == AccountCharge.Credit)
                                                                                {
                                                                                    _locTotalAmountCredit = _locTotalAmountCollBPOP;
                                                                                }

                                                                                GeneralJournal _saveGeneralJournal = new GeneralJournal(_currSession)
                                                                                {
                                                                                    Company = _locReceivableTransactionLine2.Company,
                                                                                    Workplace = _locReceivableTransactionLine2.Workplace,
                                                                                    PostingDate = now,
                                                                                    PostingType = PostingType.Sales,
                                                                                    PostingMethod = PostingMethod.Bill,
                                                                                    PostingMethodType = PostingMethodType.OverPayment,
                                                                                    Account = _locAccountMapLine.Account,
                                                                                    Debit = _locTotalAmountDebit,
                                                                                    Credit = _locTotalAmountCredit,
                                                                                    JournalMonth = _locReceivableTransactionLine2.JournalMonth,
                                                                                    JournalYear = _locReceivableTransactionLine2.JournalYear,
                                                                                    ReceivableTransaction2 = _locReceivableTransaction2XPO,
                                                                                };

                                                                                _saveGeneralJournal.Save();
                                                                                _saveGeneralJournal.Session.CommitTransaction();

                                                                                #region AccountingPeriodicLine

                                                                                if (_locAccountMapLine.Account.Code != null)
                                                                                {

                                                                                    //Cari Account Periodic Line
                                                                                    AccountingPeriodicLine _locAPL = _currSession.FindObject<AccountingPeriodicLine>
                                                                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                                                                    new BinaryOperator("Company", _locReceivableTransactionLine2.Company),
                                                                                                                    new BinaryOperator("Workplace", _locReceivableTransactionLine2.Workplace),
                                                                                                                    new BinaryOperator("AccountNo", _locAccountMapLine.Account),
                                                                                                                    new BinaryOperator("Month", _locReceivableTransactionLine2.JournalMonth),
                                                                                                                    new BinaryOperator("Year", _locReceivableTransactionLine2.JournalYear)));
                                                                                    if (_locAPL != null)
                                                                                    {
                                                                                        if (_locAPL.AccountNo != null)
                                                                                        {
                                                                                            if (_locAPL.AccountNo.BalanceType == BalanceType.Change)
                                                                                            {
                                                                                                if (_locAPL.AccountNo.AccountCharge == AccountCharge.Debit)
                                                                                                {
                                                                                                    if (_locTotalAmountDebit > 0)
                                                                                                    {
                                                                                                        _locTotalBalance = _locAPL.Balance + _locTotalAmountDebit;
                                                                                                    }
                                                                                                    if (_locTotalAmountCredit > 0)
                                                                                                    {
                                                                                                        _locTotalBalance = _locAPL.Balance - _locTotalAmountCredit;
                                                                                                    }
                                                                                                }
                                                                                                if (_locAPL.AccountNo.AccountCharge == AccountCharge.Credit)
                                                                                                {
                                                                                                    if (_locTotalAmountDebit > 0)
                                                                                                    {
                                                                                                        _locTotalBalance = _locAPL.Balance - _locTotalAmountDebit;
                                                                                                    }
                                                                                                    if (_locTotalAmountCredit > 0)
                                                                                                    {
                                                                                                        _locTotalBalance = _locAPL.Balance + _locTotalAmountCredit;
                                                                                                    }
                                                                                                }
                                                                                            }

                                                                                            _locAPL.Balance = _locTotalBalance;
                                                                                            _locAPL.Debit = _locAPL.Debit + _locTotalAmountDebit;
                                                                                            _locAPL.Credit = _locAPL.Credit + _locTotalAmountCredit;
                                                                                            _locAPL.Save();
                                                                                            _locAPL.Session.CommitTransaction();

                                                                                        }
                                                                                    }
                                                                                }
                                                                                #endregion COA
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                                #endregion OverPayment
                                                                #region LowerPayment
                                                                if (_locTotalAmountCollBPLP > 0 && _locReceivableTransactionLine2.Settled == true)
                                                                {
                                                                    if (_locJournalMapLine.AccountMap.PostingType == PostingType.Sales && _locJournalMapLine.AccountMap.OrderType == OrderType.Item
                                                                    && _locJournalMapLine.AccountMap.PostingMethod == PostingMethod.Bill && _locJournalMapLine.AccountMap.PostingMethodType == PostingMethodType.LowerPayment)
                                                                    {
                                                                        XPCollection<AccountMapLine> _locAccountMapLines = new XPCollection<AccountMapLine>
                                                                                                                       (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                                        new BinaryOperator("Company", _locReceivableTransactionLine2.Company),
                                                                                                                        new BinaryOperator("Workplace", _locReceivableTransactionLine2.Workplace),
                                                                                                                        new BinaryOperator("AccountMap", _locJournalMapLine.AccountMap),
                                                                                                                        new BinaryOperator("Active", true)));

                                                                        if (_locAccountMapLines != null && _locAccountMapLines.Count() > 0)
                                                                        {
                                                                            double _locTotalAmountDebit = 0;
                                                                            double _locTotalAmountCredit = 0;
                                                                            double _locTotalBalance = 0;

                                                                            foreach (AccountMapLine _locAccountMapLine in _locAccountMapLines)
                                                                            {
                                                                                if (_locAccountMapLine.AccountCharge == AccountCharge.Debit)
                                                                                {
                                                                                    _locTotalAmountDebit = _locTotalAmountCollBPLP;
                                                                                }
                                                                                if (_locAccountMapLine.AccountCharge == AccountCharge.Credit)
                                                                                {
                                                                                    _locTotalAmountCredit = _locTotalAmountCollBPLP;
                                                                                }

                                                                                GeneralJournal _saveGeneralJournal = new GeneralJournal(_currSession)
                                                                                {
                                                                                    Company = _locReceivableTransactionLine2.Company,
                                                                                    Workplace = _locReceivableTransactionLine2.Workplace,
                                                                                    PostingDate = now,
                                                                                    PostingType = PostingType.Sales,
                                                                                    PostingMethod = PostingMethod.Bill,
                                                                                    PostingMethodType = PostingMethodType.LowerPayment,
                                                                                    Account = _locAccountMapLine.Account,
                                                                                    Debit = _locTotalAmountDebit,
                                                                                    Credit = _locTotalAmountCredit,
                                                                                    JournalMonth = _locReceivableTransactionLine2.JournalMonth,
                                                                                    JournalYear = _locReceivableTransactionLine2.JournalYear,
                                                                                    ReceivableTransaction2 = _locReceivableTransaction2XPO,
                                                                                };

                                                                                _saveGeneralJournal.Save();
                                                                                _saveGeneralJournal.Session.CommitTransaction();

                                                                                #region AccountingPeriodicLine

                                                                                if (_locAccountMapLine.Account.Code != null)
                                                                                {

                                                                                    //Cari Account Periodic Line
                                                                                    AccountingPeriodicLine _locAPL = _currSession.FindObject<AccountingPeriodicLine>
                                                                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                                                                    new BinaryOperator("Company", _locReceivableTransactionLine2.Company),
                                                                                                                    new BinaryOperator("Workplace", _locReceivableTransactionLine2.Workplace),
                                                                                                                    new BinaryOperator("AccountNo", _locAccountMapLine.Account),
                                                                                                                    new BinaryOperator("Month", _locReceivableTransactionLine2.JournalMonth),
                                                                                                                    new BinaryOperator("Year", _locReceivableTransactionLine2.JournalYear)));
                                                                                    if (_locAPL != null)
                                                                                    {
                                                                                        if (_locAPL.AccountNo != null)
                                                                                        {
                                                                                            if (_locAPL.AccountNo.BalanceType == BalanceType.Change)
                                                                                            {
                                                                                                if (_locAPL.AccountNo.AccountCharge == AccountCharge.Debit)
                                                                                                {
                                                                                                    if (_locTotalAmountDebit > 0)
                                                                                                    {
                                                                                                        _locTotalBalance = _locAPL.Balance + _locTotalAmountDebit;
                                                                                                    }
                                                                                                    if (_locTotalAmountCredit > 0)
                                                                                                    {
                                                                                                        _locTotalBalance = _locAPL.Balance - _locTotalAmountCredit;
                                                                                                    }
                                                                                                }
                                                                                                if (_locAPL.AccountNo.AccountCharge == AccountCharge.Credit)
                                                                                                {
                                                                                                    if (_locTotalAmountDebit > 0)
                                                                                                    {
                                                                                                        _locTotalBalance = _locAPL.Balance - _locTotalAmountDebit;
                                                                                                    }
                                                                                                    if (_locTotalAmountCredit > 0)
                                                                                                    {
                                                                                                        _locTotalBalance = _locAPL.Balance + _locTotalAmountCredit;
                                                                                                    }
                                                                                                }
                                                                                            }

                                                                                            _locAPL.Balance = _locTotalBalance;
                                                                                            _locAPL.Debit = _locAPL.Debit + _locTotalAmountDebit;
                                                                                            _locAPL.Credit = _locAPL.Credit + _locTotalAmountCredit;
                                                                                            _locAPL.Save();
                                                                                            _locAPL.Session.CommitTransaction();

                                                                                        }
                                                                                    }
                                                                                }
                                                                                #endregion COA
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                                #endregion LowerPayment
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            #endregion JournalMapBusinessPartnerAccountGroup

                            #region JournalMapCompanyAccountGroup
                            if (_locReceivableTransactionLine2.Company != null)
                            {
                                _locTotalAmountCollC = _locReceivableTransactionLine2.Amount;
                                _locTotalAmountCollCDN = _locReceivableTransactionLine2.AmountDN;
                                _locTotalAmountCollCCN = _locReceivableTransactionLine2.AmountCN;
                                _locTotalAmountCollCOP = _locReceivableTransactionLine2.OverPayment;
                                _locTotalAmountCollCLP = _locReceivableTransactionLine2.Outstanding;

                                CompanyAccount _locCompanyAccount = _currSession.FindObject<CompanyAccount>(new GroupOperator(GroupOperatorType.And,
                                                                    new BinaryOperator("Company", _locReceivableTransactionLine2.Company),
                                                                    new BinaryOperator("Workplace", _locReceivableTransactionLine2.Workplace),
                                                                    new BinaryOperator("Active", true)));

                                if (_locCompanyAccount != null)
                                {
                                    if (_locCompanyAccount.CompanyAccountGroup != null)
                                    {
                                        _locCompanyAccountGroup = _locCompanyAccount.CompanyAccountGroup;

                                        XPCollection<JournalMap> _locJournalMapItems = new XPCollection<JournalMap>
                                                                   (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                    new BinaryOperator("Company", _locReceivableTransactionLine2.Company),
                                                                    new BinaryOperator("Workplace", _locReceivableTransactionLine2.Workplace),
                                                                    new BinaryOperator("CompanyAccountGroup", _locCompanyAccountGroup),
                                                                    new BinaryOperator("Active", true)));

                                        if (_locJournalMapItems != null && _locJournalMapItems.Count() > 0)
                                        {
                                            foreach (JournalMap _locJournalMapItem in _locJournalMapItems)
                                            {
                                                XPCollection<JournalMapLine> _locJournalMapLines = new XPCollection<JournalMapLine>
                                                                                                   (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                    new BinaryOperator("Company", _locReceivableTransactionLine2.Company),
                                                                                                    new BinaryOperator("Workplace", _locReceivableTransactionLine2.Workplace),
                                                                                                    new BinaryOperator("JournalMap", _locJournalMapItem),
                                                                                                    new BinaryOperator("Active", true)));

                                                if (_locJournalMapLines != null && _locJournalMapLines.Count() > 0)
                                                {
                                                    foreach (JournalMapLine _locJournalMapLine in _locJournalMapLines)
                                                    {
                                                        if (_locJournalMapLine.AccountMap != null)
                                                        {
                                                            #region NormalAmount
                                                            if (_locJournalMapLine.AccountMap.PostingType == PostingType.Sales && _locJournalMapLine.AccountMap.OrderType == OrderType.Item
                                                                && _locJournalMapLine.AccountMap.PostingMethod == PostingMethod.Bill && _locJournalMapLine.AccountMap.PostingMethodType == PostingMethodType.Normal)
                                                            {
                                                                XPCollection<AccountMapLine> _locAccountMapLines = new XPCollection<AccountMapLine>
                                                                                                               (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                                new BinaryOperator("Company", _locReceivableTransactionLine2.Company),
                                                                                                                new BinaryOperator("Workplace", _locReceivableTransactionLine2.Workplace),
                                                                                                                new BinaryOperator("AccountMap", _locJournalMapLine.AccountMap),
                                                                                                                new BinaryOperator("Active", true)));

                                                                if (_locAccountMapLines != null && _locAccountMapLines.Count() > 0)
                                                                {
                                                                    double _locTotalAmountDebit = 0;
                                                                    double _locTotalAmountCredit = 0;
                                                                    double _locTotalBalance = 0;

                                                                    foreach (AccountMapLine _locAccountMapLine in _locAccountMapLines)
                                                                    {
                                                                        if (_locAccountMapLine.AccountCharge == AccountCharge.Debit)
                                                                        {
                                                                            _locTotalAmountDebit = _locTotalAmountCollC;
                                                                        }
                                                                        if (_locAccountMapLine.AccountCharge == AccountCharge.Credit)
                                                                        {
                                                                            _locTotalAmountCredit = _locTotalAmountCollC;
                                                                        }

                                                                        GeneralJournal _saveGeneralJournal = new GeneralJournal(_currSession)
                                                                        {
                                                                            Company = _locReceivableTransactionLine2.Company,
                                                                            Workplace = _locReceivableTransactionLine2.Workplace,
                                                                            PostingDate = now,
                                                                            PostingType = PostingType.Sales,
                                                                            OrderType = OrderType.Item,
                                                                            PostingMethod = PostingMethod.Bill,
                                                                            PostingMethodType = PostingMethodType.Normal,
                                                                            Account = _locAccountMapLine.Account,
                                                                            Debit = _locTotalAmountDebit,
                                                                            Credit = _locTotalAmountCredit,
                                                                            JournalMonth = _locReceivableTransactionLine2.JournalMonth,
                                                                            JournalYear = _locReceivableTransactionLine2.JournalYear,
                                                                            ReceivableTransaction2 = _locReceivableTransaction2XPO,
                                                                        };

                                                                        _saveGeneralJournal.Save();
                                                                        _saveGeneralJournal.Session.CommitTransaction();

                                                                        #region AccountingPeriodicLine

                                                                        if (_locAccountMapLine.Account.Code != null)
                                                                        {

                                                                            //Cari Account Periodic Line
                                                                            AccountingPeriodicLine _locAPL = _currSession.FindObject<AccountingPeriodicLine>
                                                                                                            (new GroupOperator(GroupOperatorType.And,
                                                                                                            new BinaryOperator("Company", _locReceivableTransactionLine2.Company),
                                                                                                            new BinaryOperator("Workplace", _locReceivableTransactionLine2.Workplace),
                                                                                                            new BinaryOperator("AccountNo", _locAccountMapLine.Account),
                                                                                                            new BinaryOperator("Month", _locReceivableTransactionLine2.JournalMonth),
                                                                                                            new BinaryOperator("Year", _locReceivableTransactionLine2.JournalYear)));
                                                                            if (_locAPL != null)
                                                                            {
                                                                                if (_locAPL.AccountNo != null)
                                                                                {
                                                                                    if (_locAPL.AccountNo.BalanceType == BalanceType.Change)
                                                                                    {
                                                                                        if (_locAPL.AccountNo.AccountCharge == AccountCharge.Debit)
                                                                                        {
                                                                                            if (_locTotalAmountDebit > 0)
                                                                                            {
                                                                                                _locTotalBalance = _locAPL.Balance + _locTotalAmountDebit;
                                                                                            }
                                                                                            if (_locTotalAmountCredit > 0)
                                                                                            {
                                                                                                _locTotalBalance = _locAPL.Balance - _locTotalAmountCredit;
                                                                                            }
                                                                                        }
                                                                                        if (_locAPL.AccountNo.AccountCharge == AccountCharge.Credit)
                                                                                        {
                                                                                            if (_locTotalAmountDebit > 0)
                                                                                            {
                                                                                                _locTotalBalance = _locAPL.Balance - _locTotalAmountDebit;
                                                                                            }
                                                                                            if (_locTotalAmountCredit > 0)
                                                                                            {
                                                                                                _locTotalBalance = _locAPL.Balance + _locTotalAmountCredit;
                                                                                            }
                                                                                        }
                                                                                    }

                                                                                    _locAPL.Balance = _locTotalBalance;
                                                                                    _locAPL.Debit = _locAPL.Debit + _locTotalAmountDebit;
                                                                                    _locAPL.Credit = _locAPL.Credit + _locTotalAmountCredit;
                                                                                    _locAPL.Save();
                                                                                    _locAPL.Session.CommitTransaction();

                                                                                }
                                                                            }
                                                                        }
                                                                        #endregion COA
                                                                    }
                                                                }
                                                            }
                                                            #endregion NormalAmount 
                                                            #region AmountDN
                                                            if (_locTotalAmountCollCDN > 0)
                                                            {
                                                                if (_locJournalMapLine.AccountMap.PostingType == PostingType.Sales && _locJournalMapLine.AccountMap.OrderType == OrderType.Item
                                                                && _locJournalMapLine.AccountMap.PostingMethod == PostingMethod.Bill && _locJournalMapLine.AccountMap.PostingMethodType == PostingMethodType.AmountDN)
                                                                {
                                                                    XPCollection<AccountMapLine> _locAccountMapLines = new XPCollection<AccountMapLine>
                                                                                                                   (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                                    new BinaryOperator("Company", _locReceivableTransactionLine2.Company),
                                                                                                                    new BinaryOperator("Workplace", _locReceivableTransactionLine2.Workplace),
                                                                                                                    new BinaryOperator("AccountMap", _locJournalMapLine.AccountMap),
                                                                                                                    new BinaryOperator("Active", true)));

                                                                    if (_locAccountMapLines != null && _locAccountMapLines.Count() > 0)
                                                                    {
                                                                        double _locTotalAmountDebit = 0;
                                                                        double _locTotalAmountCredit = 0;
                                                                        double _locTotalBalance = 0;

                                                                        foreach (AccountMapLine _locAccountMapLine in _locAccountMapLines)
                                                                        {
                                                                            if (_locAccountMapLine.AccountCharge == AccountCharge.Debit)
                                                                            {
                                                                                _locTotalAmountDebit = _locTotalAmountCollCDN;
                                                                            }
                                                                            if (_locAccountMapLine.AccountCharge == AccountCharge.Credit)
                                                                            {
                                                                                _locTotalAmountCredit = _locTotalAmountCollCDN;
                                                                            }

                                                                            GeneralJournal _saveGeneralJournal = new GeneralJournal(_currSession)
                                                                            {
                                                                                Company = _locReceivableTransactionLine2.Company,
                                                                                Workplace = _locReceivableTransactionLine2.Workplace,
                                                                                PostingDate = now,
                                                                                PostingType = PostingType.Sales,
                                                                                OrderType = OrderType.Item,
                                                                                PostingMethod = PostingMethod.Bill,
                                                                                PostingMethodType = PostingMethodType.AmountDN,
                                                                                Account = _locAccountMapLine.Account,
                                                                                Debit = _locTotalAmountDebit,
                                                                                Credit = _locTotalAmountCredit,
                                                                                JournalMonth = _locReceivableTransactionLine2.JournalMonth,
                                                                                JournalYear = _locReceivableTransactionLine2.JournalYear,
                                                                                ReceivableTransaction2 = _locReceivableTransaction2XPO,
                                                                            };

                                                                            _saveGeneralJournal.Save();
                                                                            _saveGeneralJournal.Session.CommitTransaction();

                                                                            #region AccountingPeriodicLine

                                                                            if (_locAccountMapLine.Account.Code != null)
                                                                            {

                                                                                //Cari Account Periodic Line
                                                                                AccountingPeriodicLine _locAPL = _currSession.FindObject<AccountingPeriodicLine>
                                                                                                                (new GroupOperator(GroupOperatorType.And,
                                                                                                                new BinaryOperator("Company", _locReceivableTransactionLine2.Company),
                                                                                                                new BinaryOperator("Workplace", _locReceivableTransactionLine2.Workplace),
                                                                                                                new BinaryOperator("AccountNo", _locAccountMapLine.Account),
                                                                                                                new BinaryOperator("Month", _locReceivableTransactionLine2.JournalMonth),
                                                                                                                new BinaryOperator("Year", _locReceivableTransactionLine2.JournalYear)));
                                                                                if (_locAPL != null)
                                                                                {
                                                                                    if (_locAPL.AccountNo != null)
                                                                                    {
                                                                                        if (_locAPL.AccountNo.BalanceType == BalanceType.Change)
                                                                                        {
                                                                                            if (_locAPL.AccountNo.AccountCharge == AccountCharge.Debit)
                                                                                            {
                                                                                                if (_locTotalAmountDebit > 0)
                                                                                                {
                                                                                                    _locTotalBalance = _locAPL.Balance + _locTotalAmountDebit;
                                                                                                }
                                                                                                if (_locTotalAmountCredit > 0)
                                                                                                {
                                                                                                    _locTotalBalance = _locAPL.Balance - _locTotalAmountCredit;
                                                                                                }
                                                                                            }
                                                                                            if (_locAPL.AccountNo.AccountCharge == AccountCharge.Credit)
                                                                                            {
                                                                                                if (_locTotalAmountDebit > 0)
                                                                                                {
                                                                                                    _locTotalBalance = _locAPL.Balance - _locTotalAmountDebit;
                                                                                                }
                                                                                                if (_locTotalAmountCredit > 0)
                                                                                                {
                                                                                                    _locTotalBalance = _locAPL.Balance + _locTotalAmountCredit;
                                                                                                }
                                                                                            }
                                                                                        }

                                                                                        _locAPL.Balance = _locTotalBalance;
                                                                                        _locAPL.Debit = _locAPL.Debit + _locTotalAmountDebit;
                                                                                        _locAPL.Credit = _locAPL.Credit + _locTotalAmountCredit;
                                                                                        _locAPL.Save();
                                                                                        _locAPL.Session.CommitTransaction();

                                                                                    }
                                                                                }
                                                                            }
                                                                            #endregion COA
                                                                        }
                                                                    }
                                                                }
                                                            }

                                                            #endregion AmountDN
                                                            #region AmountCN
                                                            if (_locTotalAmountCollCCN > 0)
                                                            {
                                                                if (_locJournalMapLine.AccountMap.PostingType == PostingType.Sales && _locJournalMapLine.AccountMap.OrderType == OrderType.Item
                                                                && _locJournalMapLine.AccountMap.PostingMethod == PostingMethod.Bill && _locJournalMapLine.AccountMap.PostingMethodType == PostingMethodType.AmountCN)
                                                                {
                                                                    XPCollection<AccountMapLine> _locAccountMapLines = new XPCollection<AccountMapLine>
                                                                                                                   (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                                    new BinaryOperator("Company", _locReceivableTransactionLine2.Company),
                                                                                                                    new BinaryOperator("Workplace", _locReceivableTransactionLine2.Workplace),
                                                                                                                    new BinaryOperator("AccountMap", _locJournalMapLine.AccountMap),
                                                                                                                    new BinaryOperator("Active", true)));

                                                                    if (_locAccountMapLines != null && _locAccountMapLines.Count() > 0)
                                                                    {
                                                                        double _locTotalAmountDebit = 0;
                                                                        double _locTotalAmountCredit = 0;
                                                                        double _locTotalBalance = 0;

                                                                        foreach (AccountMapLine _locAccountMapLine in _locAccountMapLines)
                                                                        {
                                                                            if (_locAccountMapLine.AccountCharge == AccountCharge.Debit)
                                                                            {
                                                                                _locTotalAmountDebit = _locTotalAmountCollCCN;
                                                                            }
                                                                            if (_locAccountMapLine.AccountCharge == AccountCharge.Credit)
                                                                            {
                                                                                _locTotalAmountCredit = _locTotalAmountCollCCN;
                                                                            }

                                                                            GeneralJournal _saveGeneralJournal = new GeneralJournal(_currSession)
                                                                            {
                                                                                Company = _locReceivableTransactionLine2.Company,
                                                                                Workplace = _locReceivableTransactionLine2.Workplace,
                                                                                PostingDate = now,
                                                                                PostingType = PostingType.Sales,
                                                                                OrderType = OrderType.Item,
                                                                                PostingMethod = PostingMethod.Bill,
                                                                                PostingMethodType = PostingMethodType.AmountCN,
                                                                                Account = _locAccountMapLine.Account,
                                                                                Debit = _locTotalAmountDebit,
                                                                                Credit = _locTotalAmountCredit,
                                                                                JournalMonth = _locReceivableTransactionLine2.JournalMonth,
                                                                                JournalYear = _locReceivableTransactionLine2.JournalYear,
                                                                                ReceivableTransaction2 = _locReceivableTransaction2XPO,
                                                                            };

                                                                            _saveGeneralJournal.Save();
                                                                            _saveGeneralJournal.Session.CommitTransaction();

                                                                            #region AccountingPeriodicLine

                                                                            if (_locAccountMapLine.Account.Code != null)
                                                                            {

                                                                                //Cari Account Periodic Line
                                                                                AccountingPeriodicLine _locAPL = _currSession.FindObject<AccountingPeriodicLine>
                                                                                                                (new GroupOperator(GroupOperatorType.And,
                                                                                                                new BinaryOperator("Company", _locReceivableTransactionLine2.Company),
                                                                                                                new BinaryOperator("Workplace", _locReceivableTransactionLine2.Workplace),
                                                                                                                new BinaryOperator("AccountNo", _locAccountMapLine.Account),
                                                                                                                new BinaryOperator("Month", _locReceivableTransactionLine2.JournalMonth),
                                                                                                                new BinaryOperator("Year", _locReceivableTransactionLine2.JournalYear)));
                                                                                if (_locAPL != null)
                                                                                {
                                                                                    if (_locAPL.AccountNo != null)
                                                                                    {
                                                                                        if (_locAPL.AccountNo.BalanceType == BalanceType.Change)
                                                                                        {
                                                                                            if (_locAPL.AccountNo.AccountCharge == AccountCharge.Debit)
                                                                                            {
                                                                                                if (_locTotalAmountDebit > 0)
                                                                                                {
                                                                                                    _locTotalBalance = _locAPL.Balance + _locTotalAmountDebit;
                                                                                                }
                                                                                                if (_locTotalAmountCredit > 0)
                                                                                                {
                                                                                                    _locTotalBalance = _locAPL.Balance - _locTotalAmountCredit;
                                                                                                }
                                                                                            }
                                                                                            if (_locAPL.AccountNo.AccountCharge == AccountCharge.Credit)
                                                                                            {
                                                                                                if (_locTotalAmountDebit > 0)
                                                                                                {
                                                                                                    _locTotalBalance = _locAPL.Balance - _locTotalAmountDebit;
                                                                                                }
                                                                                                if (_locTotalAmountCredit > 0)
                                                                                                {
                                                                                                    _locTotalBalance = _locAPL.Balance + _locTotalAmountCredit;
                                                                                                }
                                                                                            }
                                                                                        }

                                                                                        _locAPL.Balance = _locTotalBalance;
                                                                                        _locAPL.Debit = _locAPL.Debit + _locTotalAmountDebit;
                                                                                        _locAPL.Credit = _locAPL.Credit + _locTotalAmountCredit;
                                                                                        _locAPL.Save();
                                                                                        _locAPL.Session.CommitTransaction();

                                                                                    }
                                                                                }
                                                                            }
                                                                            #endregion COA
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                            #endregion AmountCN
                                                            #region OverPayment
                                                            if (_locTotalAmountCollCOP > 0)
                                                            {
                                                                if (_locJournalMapLine.AccountMap.PostingType == PostingType.Sales && _locJournalMapLine.AccountMap.OrderType == OrderType.Item
                                                                && _locJournalMapLine.AccountMap.PostingMethod == PostingMethod.Bill && _locJournalMapLine.AccountMap.PostingMethodType == PostingMethodType.OverPayment)
                                                                {
                                                                    XPCollection<AccountMapLine> _locAccountMapLines = new XPCollection<AccountMapLine>
                                                                                                                   (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                                    new BinaryOperator("Company", _locReceivableTransactionLine2.Company),
                                                                                                                    new BinaryOperator("Workplace", _locReceivableTransactionLine2.Workplace),
                                                                                                                    new BinaryOperator("AccountMap", _locJournalMapLine.AccountMap),
                                                                                                                    new BinaryOperator("Active", true)));

                                                                    if (_locAccountMapLines != null && _locAccountMapLines.Count() > 0)
                                                                    {
                                                                        double _locTotalAmountDebit = 0;
                                                                        double _locTotalAmountCredit = 0;
                                                                        double _locTotalBalance = 0;

                                                                        foreach (AccountMapLine _locAccountMapLine in _locAccountMapLines)
                                                                        {
                                                                            if (_locAccountMapLine.AccountCharge == AccountCharge.Debit)
                                                                            {
                                                                                _locTotalAmountDebit = _locTotalAmountCollCOP;
                                                                            }
                                                                            if (_locAccountMapLine.AccountCharge == AccountCharge.Credit)
                                                                            {
                                                                                _locTotalAmountCredit = _locTotalAmountCollCOP;
                                                                            }

                                                                            GeneralJournal _saveGeneralJournal = new GeneralJournal(_currSession)
                                                                            {
                                                                                Company = _locReceivableTransactionLine2.Company,
                                                                                Workplace = _locReceivableTransactionLine2.Workplace,
                                                                                PostingDate = now,
                                                                                PostingType = PostingType.Sales,
                                                                                OrderType = OrderType.Item,
                                                                                PostingMethod = PostingMethod.Bill,
                                                                                PostingMethodType = PostingMethodType.OverPayment,
                                                                                Account = _locAccountMapLine.Account,
                                                                                Debit = _locTotalAmountDebit,
                                                                                Credit = _locTotalAmountCredit,
                                                                                JournalMonth = _locReceivableTransactionLine2.JournalMonth,
                                                                                JournalYear = _locReceivableTransactionLine2.JournalYear,
                                                                                ReceivableTransaction2 = _locReceivableTransaction2XPO,
                                                                            };

                                                                            _saveGeneralJournal.Save();
                                                                            _saveGeneralJournal.Session.CommitTransaction();

                                                                            #region AccountingPeriodicLine

                                                                            if (_locAccountMapLine.Account.Code != null)
                                                                            {

                                                                                //Cari Account Periodic Line
                                                                                AccountingPeriodicLine _locAPL = _currSession.FindObject<AccountingPeriodicLine>
                                                                                                                (new GroupOperator(GroupOperatorType.And,
                                                                                                                new BinaryOperator("Company", _locReceivableTransactionLine2.Company),
                                                                                                                new BinaryOperator("Workplace", _locReceivableTransactionLine2.Workplace),
                                                                                                                new BinaryOperator("AccountNo", _locAccountMapLine.Account),
                                                                                                                new BinaryOperator("Month", _locReceivableTransactionLine2.JournalMonth),
                                                                                                                new BinaryOperator("Year", _locReceivableTransactionLine2.JournalYear)));
                                                                                if (_locAPL != null)
                                                                                {
                                                                                    if (_locAPL.AccountNo != null)
                                                                                    {
                                                                                        if (_locAPL.AccountNo.BalanceType == BalanceType.Change)
                                                                                        {
                                                                                            if (_locAPL.AccountNo.AccountCharge == AccountCharge.Debit)
                                                                                            {
                                                                                                if (_locTotalAmountDebit > 0)
                                                                                                {
                                                                                                    _locTotalBalance = _locAPL.Balance + _locTotalAmountDebit;
                                                                                                }
                                                                                                if (_locTotalAmountCredit > 0)
                                                                                                {
                                                                                                    _locTotalBalance = _locAPL.Balance - _locTotalAmountCredit;
                                                                                                }
                                                                                            }
                                                                                            if (_locAPL.AccountNo.AccountCharge == AccountCharge.Credit)
                                                                                            {
                                                                                                if (_locTotalAmountDebit > 0)
                                                                                                {
                                                                                                    _locTotalBalance = _locAPL.Balance - _locTotalAmountDebit;
                                                                                                }
                                                                                                if (_locTotalAmountCredit > 0)
                                                                                                {
                                                                                                    _locTotalBalance = _locAPL.Balance + _locTotalAmountCredit;
                                                                                                }
                                                                                            }
                                                                                        }

                                                                                        _locAPL.Balance = _locTotalBalance;
                                                                                        _locAPL.Debit = _locAPL.Debit + _locTotalAmountDebit;
                                                                                        _locAPL.Credit = _locAPL.Credit + _locTotalAmountCredit;
                                                                                        _locAPL.Save();
                                                                                        _locAPL.Session.CommitTransaction();

                                                                                    }
                                                                                }
                                                                            }
                                                                            #endregion COA
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                            #endregion OverPayment
                                                            #region LowerPayment
                                                            if (_locTotalAmountCollCLP > 0 && _locReceivableTransactionLine2.Settled == true)
                                                            {
                                                                if (_locJournalMapLine.AccountMap.PostingType == PostingType.Sales && _locJournalMapLine.AccountMap.OrderType == OrderType.Item
                                                                && _locJournalMapLine.AccountMap.PostingMethod == PostingMethod.Bill && _locJournalMapLine.AccountMap.PostingMethodType == PostingMethodType.LowerPayment)
                                                                {
                                                                    XPCollection<AccountMapLine> _locAccountMapLines = new XPCollection<AccountMapLine>
                                                                                                                   (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                                    new BinaryOperator("Company", _locReceivableTransactionLine2.Company),
                                                                                                                    new BinaryOperator("Workplace", _locReceivableTransactionLine2.Workplace),
                                                                                                                    new BinaryOperator("AccountMap", _locJournalMapLine.AccountMap),
                                                                                                                    new BinaryOperator("Active", true)));

                                                                    if (_locAccountMapLines != null && _locAccountMapLines.Count() > 0)
                                                                    {
                                                                        double _locTotalAmountDebit = 0;
                                                                        double _locTotalAmountCredit = 0;
                                                                        double _locTotalBalance = 0;

                                                                        foreach (AccountMapLine _locAccountMapLine in _locAccountMapLines)
                                                                        {
                                                                            if (_locAccountMapLine.AccountCharge == AccountCharge.Debit)
                                                                            {
                                                                                _locTotalAmountDebit = _locTotalAmountCollCOP;
                                                                            }
                                                                            if (_locAccountMapLine.AccountCharge == AccountCharge.Credit)
                                                                            {
                                                                                _locTotalAmountCredit = _locTotalAmountCollCOP;
                                                                            }

                                                                            GeneralJournal _saveGeneralJournal = new GeneralJournal(_currSession)
                                                                            {
                                                                                Company = _locReceivableTransactionLine2.Company,
                                                                                Workplace = _locReceivableTransactionLine2.Workplace,
                                                                                PostingDate = now,
                                                                                PostingType = PostingType.Sales,
                                                                                OrderType = OrderType.Item,
                                                                                PostingMethod = PostingMethod.Bill,
                                                                                PostingMethodType = PostingMethodType.LowerPayment,
                                                                                Account = _locAccountMapLine.Account,
                                                                                Debit = _locTotalAmountDebit,
                                                                                Credit = _locTotalAmountCredit,
                                                                                JournalMonth = _locReceivableTransactionLine2.JournalMonth,
                                                                                JournalYear = _locReceivableTransactionLine2.JournalYear,
                                                                                ReceivableTransaction2 = _locReceivableTransaction2XPO,
                                                                            };

                                                                            _saveGeneralJournal.Save();
                                                                            _saveGeneralJournal.Session.CommitTransaction();

                                                                            #region AccountingPeriodicLine

                                                                            if (_locAccountMapLine.Account.Code != null)
                                                                            {

                                                                                //Cari Account Periodic Line
                                                                                AccountingPeriodicLine _locAPL = _currSession.FindObject<AccountingPeriodicLine>
                                                                                                                (new GroupOperator(GroupOperatorType.And,
                                                                                                                new BinaryOperator("Company", _locReceivableTransactionLine2.Company),
                                                                                                                new BinaryOperator("Workplace", _locReceivableTransactionLine2.Workplace),
                                                                                                                new BinaryOperator("AccountNo", _locAccountMapLine.Account),
                                                                                                                new BinaryOperator("Month", _locReceivableTransactionLine2.JournalMonth),
                                                                                                                new BinaryOperator("Year", _locReceivableTransactionLine2.JournalYear)));
                                                                                if (_locAPL != null)
                                                                                {
                                                                                    if (_locAPL.AccountNo != null)
                                                                                    {
                                                                                        if (_locAPL.AccountNo.BalanceType == BalanceType.Change)
                                                                                        {
                                                                                            if (_locAPL.AccountNo.AccountCharge == AccountCharge.Debit)
                                                                                            {
                                                                                                if (_locTotalAmountDebit > 0)
                                                                                                {
                                                                                                    _locTotalBalance = _locAPL.Balance + _locTotalAmountDebit;
                                                                                                }
                                                                                                if (_locTotalAmountCredit > 0)
                                                                                                {
                                                                                                    _locTotalBalance = _locAPL.Balance - _locTotalAmountCredit;
                                                                                                }
                                                                                            }
                                                                                            if (_locAPL.AccountNo.AccountCharge == AccountCharge.Credit)
                                                                                            {
                                                                                                if (_locTotalAmountDebit > 0)
                                                                                                {
                                                                                                    _locTotalBalance = _locAPL.Balance - _locTotalAmountDebit;
                                                                                                }
                                                                                                if (_locTotalAmountCredit > 0)
                                                                                                {
                                                                                                    _locTotalBalance = _locAPL.Balance + _locTotalAmountCredit;
                                                                                                }
                                                                                            }
                                                                                        }

                                                                                        _locAPL.Balance = _locTotalBalance;
                                                                                        _locAPL.Debit = _locAPL.Debit + _locTotalAmountDebit;
                                                                                        _locAPL.Credit = _locAPL.Credit + _locTotalAmountCredit;
                                                                                        _locAPL.Save();
                                                                                        _locAPL.Session.CommitTransaction();

                                                                                    }
                                                                                }
                                                                            }
                                                                            #endregion COA
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                            #endregion LowerPayment
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            #endregion JournalMapCompanyAccountGroup

                            #endregion NonBankAccount

                            #region BankAccount
                            if (_locReceivableTransactionLine2.MultiBank == true)
                            {
                                XPCollection<ReceivableTransactionBank> _locReceivableTransactionBanks = new XPCollection<ReceivableTransactionBank>(_currSession,
                                                                                                         new GroupOperator(GroupOperatorType.And,
                                                                                                         new BinaryOperator("ReceivableTransactionLine2", _locReceivableTransactionLine2)));
                                if (_locReceivableTransactionBanks != null && _locReceivableTransactionBanks.Count() > 0)
                                {
                                    foreach (ReceivableTransactionBank _locReceivableTransactionBank in _locReceivableTransactionBanks)
                                    {
                                        #region JournalMapBusinessPartnerAccountGroup
                                        if (_locReceivableTransactionBank.BankAccount != null)
                                        {
                                            BankAccountMap _locBankAccountMap = _currSession.FindObject<BankAccountMap>(new GroupOperator(GroupOperatorType.And,
                                                                                            new BinaryOperator("Company", _locReceivableTransactionLine2.Company),
                                                                                            new BinaryOperator("Workplace", _locReceivableTransactionLine2.Workplace),
                                                                                            new BinaryOperator("BankAccount", _locReceivableTransactionBank.BankAccount),
                                                                                            new BinaryOperator("Active", true)));
                                            if(_locBankAccountMap != null)
                                            {
                                                if (_locBankAccountMap.BankAccountGroup != null)
                                                {
                                                    _locBankAccountGroup = _locReceivableTransactionBank.BankAccount.BankAccountGroup;

                                                    _locTotalAmountCollBP = _locReceivableTransactionBank.Amount;


                                                    XPCollection<JournalMap> _locJournalMapItems = new XPCollection<JournalMap>
                                                                                       (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                        new BinaryOperator("Company", _locReceivableTransactionBank.Company),
                                                                                        new BinaryOperator("Workplace", _locReceivableTransactionBank.Workplace),
                                                                                        new BinaryOperator("BankAccountGroup", _locBankAccountGroup),
                                                                                        new BinaryOperator("Active", true)));

                                                    if (_locJournalMapItems != null && _locJournalMapItems.Count() > 0)
                                                    {
                                                        foreach (JournalMap _locJournalMapItem in _locJournalMapItems)
                                                        {
                                                            XPCollection<JournalMapLine> _locJournalMapLines = new XPCollection<JournalMapLine>
                                                                                                               (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                                new BinaryOperator("Company", _locReceivableTransactionBank.Company),
                                                                                                                new BinaryOperator("Workplace", _locReceivableTransactionBank.Workplace),
                                                                                                                new BinaryOperator("JournalMap", _locJournalMapItem),
                                                                                                                new BinaryOperator("Active", true)));

                                                            if (_locJournalMapLines != null && _locJournalMapLines.Count() > 0)
                                                            {
                                                                foreach (JournalMapLine _locJournalMapLine in _locJournalMapLines)
                                                                {
                                                                    if (_locJournalMapLine.AccountMap != null)
                                                                    {
                                                                        #region NormalAmount
                                                                        if (_locJournalMapLine.AccountMap.PostingType == PostingType.Sales && _locJournalMapLine.AccountMap.OrderType == OrderType.Item
                                                                            && _locJournalMapLine.AccountMap.PostingMethod == PostingMethod.Bill && _locJournalMapLine.AccountMap.PostingMethodType == PostingMethodType.Normal)
                                                                        {
                                                                            XPCollection<AccountMapLine> _locAccountMapLines = new XPCollection<AccountMapLine>
                                                                                                                           (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                                            new BinaryOperator("Company", _locReceivableTransactionBank.Company),
                                                                                                                            new BinaryOperator("Workplace", _locReceivableTransactionBank.Workplace),
                                                                                                                            new BinaryOperator("AccountMap", _locJournalMapLine.AccountMap),
                                                                                                                            new BinaryOperator("Active", true)));

                                                                            if (_locAccountMapLines != null && _locAccountMapLines.Count() > 0)
                                                                            {
                                                                                double _locTotalAmountDebit = 0;
                                                                                double _locTotalAmountCredit = 0;
                                                                                double _locTotalBalance = 0;

                                                                                foreach (AccountMapLine _locAccountMapLine in _locAccountMapLines)
                                                                                {
                                                                                    if (_locAccountMapLine.AccountCharge == AccountCharge.Debit)
                                                                                    {
                                                                                        _locTotalAmountDebit = _locTotalAmountCollBP;
                                                                                    }
                                                                                    if (_locAccountMapLine.AccountCharge == AccountCharge.Credit)
                                                                                    {
                                                                                        _locTotalAmountCredit = _locTotalAmountCollBP;
                                                                                    }

                                                                                    GeneralJournal _saveGeneralJournal = new GeneralJournal(_currSession)
                                                                                    {
                                                                                        Company = _locReceivableTransactionBank.Company,
                                                                                        Workplace = _locReceivableTransactionBank.Workplace,
                                                                                        PostingDate = now,
                                                                                        PostingType = PostingType.Sales,
                                                                                        OrderType = OrderType.Item,
                                                                                        PostingMethod = PostingMethod.Bill,
                                                                                        PostingMethodType = PostingMethodType.Normal,
                                                                                        Account = _locAccountMapLine.Account,
                                                                                        Debit = _locTotalAmountDebit,
                                                                                        Credit = _locTotalAmountCredit,
                                                                                        JournalMonth = _locReceivableTransactionLine2.JournalMonth,
                                                                                        JournalYear = _locReceivableTransactionLine2.JournalYear,
                                                                                        ReceivableTransaction2 = _locReceivableTransaction2XPO,
                                                                                    };

                                                                                    _saveGeneralJournal.Save();
                                                                                    _saveGeneralJournal.Session.CommitTransaction();

                                                                                    #region AccountingPeriodicLine

                                                                                    if (_locAccountMapLine.Account.Code != null)
                                                                                    {

                                                                                        //Cari Account Periodic Line
                                                                                        AccountingPeriodicLine _locAPL = _currSession.FindObject<AccountingPeriodicLine>
                                                                                                                        (new GroupOperator(GroupOperatorType.And,
                                                                                                                        new BinaryOperator("Company", _locReceivableTransactionBank.Company),
                                                                                                                        new BinaryOperator("Workplace", _locReceivableTransactionBank.Workplace),
                                                                                                                        new BinaryOperator("AccountNo", _locAccountMapLine.Account),
                                                                                                                        new BinaryOperator("Month", _locReceivableTransactionLine2.JournalMonth),
                                                                                                                        new BinaryOperator("Year", _locReceivableTransactionLine2.JournalYear)));
                                                                                        if (_locAPL != null)
                                                                                        {
                                                                                            if (_locAPL.AccountNo != null)
                                                                                            {
                                                                                                if (_locAPL.AccountNo.BalanceType == BalanceType.Change)
                                                                                                {
                                                                                                    if (_locAPL.AccountNo.AccountCharge == AccountCharge.Debit)
                                                                                                    {
                                                                                                        if (_locTotalAmountDebit > 0)
                                                                                                        {
                                                                                                            _locTotalBalance = _locAPL.Balance + _locTotalAmountDebit;
                                                                                                        }
                                                                                                        if (_locTotalAmountCredit > 0)
                                                                                                        {
                                                                                                            _locTotalBalance = _locAPL.Balance - _locTotalAmountCredit;
                                                                                                        }
                                                                                                    }
                                                                                                    if (_locAPL.AccountNo.AccountCharge == AccountCharge.Credit)
                                                                                                    {
                                                                                                        if (_locTotalAmountDebit > 0)
                                                                                                        {
                                                                                                            _locTotalBalance = _locAPL.Balance - _locTotalAmountDebit;
                                                                                                        }
                                                                                                        if (_locTotalAmountCredit > 0)
                                                                                                        {
                                                                                                            _locTotalBalance = _locAPL.Balance + _locTotalAmountCredit;
                                                                                                        }
                                                                                                    }
                                                                                                }

                                                                                                _locAPL.Balance = _locTotalBalance;
                                                                                                _locAPL.Debit = _locAPL.Debit + _locTotalAmountDebit;
                                                                                                _locAPL.Credit = _locAPL.Credit + _locTotalAmountCredit;
                                                                                                _locAPL.Save();
                                                                                                _locAPL.Session.CommitTransaction();

                                                                                            }
                                                                                        }
                                                                                    }
                                                                                    #endregion COA
                                                                                }
                                                                            }
                                                                        }
                                                                        #endregion NormalAmount   
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        #endregion JournalMapBusinessPartnerAccountGroup

                                        #region JournalMapCompanyAccountGroup
                                        if (_locReceivableTransactionBank.CompanyBankAccount != null)
                                        {
                                            BankAccountMap _locBankAccountMap = _currSession.FindObject<BankAccountMap>(new GroupOperator(GroupOperatorType.And,
                                                                                            new BinaryOperator("Company", _locReceivableTransactionLine2.Company),
                                                                                            new BinaryOperator("Workplace", _locReceivableTransactionLine2.Workplace),
                                                                                            new BinaryOperator("BankAccount", _locReceivableTransactionBank.CompanyBankAccount),
                                                                                            new BinaryOperator("Active", true)));
                                            if(_locBankAccountMap != null)
                                            {
                                                if (_locBankAccountMap.BankAccountGroup != null)
                                                {
                                                    _locTotalAmountCollC = _locReceivableTransactionBank.Amount;
                                                    _locCompanyBankAccountGroup = _locBankAccountMap.BankAccountGroup;

                                                    XPCollection<JournalMap> _locJournalMapItems = new XPCollection<JournalMap>
                                                                                   (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                    new BinaryOperator("Company", _locReceivableTransactionBank.Company),
                                                                                    new BinaryOperator("Workplace", _locReceivableTransactionBank.Workplace),
                                                                                    new BinaryOperator("BankAccountGroup", _locCompanyBankAccountGroup),
                                                                                    new BinaryOperator("Active", true)));

                                                    if (_locJournalMapItems != null && _locJournalMapItems.Count() > 0)
                                                    {
                                                        foreach (JournalMap _locJournalMapItem in _locJournalMapItems)
                                                        {
                                                            XPCollection<JournalMapLine> _locJournalMapLines = new XPCollection<JournalMapLine>
                                                                                                               (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                                new BinaryOperator("Company", _locReceivableTransactionBank.Company),
                                                                                                                new BinaryOperator("Workplace", _locReceivableTransactionBank.Workplace),
                                                                                                                new BinaryOperator("JournalMap", _locJournalMapItem),
                                                                                                                new BinaryOperator("Active", true)));

                                                            if (_locJournalMapLines != null && _locJournalMapLines.Count() > 0)
                                                            {
                                                                foreach (JournalMapLine _locJournalMapLine in _locJournalMapLines)
                                                                {
                                                                    if (_locJournalMapLine.AccountMap != null)
                                                                    {
                                                                        #region NormalAmount
                                                                        if (_locJournalMapLine.AccountMap.PostingType == PostingType.Sales && _locJournalMapLine.AccountMap.OrderType == OrderType.Item
                                                                            && _locJournalMapLine.AccountMap.PostingMethod == PostingMethod.Bill && _locJournalMapLine.AccountMap.PostingMethodType == PostingMethodType.Normal)
                                                                        {
                                                                            XPCollection<AccountMapLine> _locAccountMapLines = new XPCollection<AccountMapLine>
                                                                                                                           (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                                            new BinaryOperator("Company", _locReceivableTransactionBank.Company),
                                                                                                                            new BinaryOperator("Workplace", _locReceivableTransactionBank.Workplace),
                                                                                                                            new BinaryOperator("AccountMap", _locJournalMapLine.AccountMap),
                                                                                                                            new BinaryOperator("Active", true)));

                                                                            if (_locAccountMapLines != null && _locAccountMapLines.Count() > 0)
                                                                            {
                                                                                double _locTotalAmountDebit = 0;
                                                                                double _locTotalAmountCredit = 0;
                                                                                double _locTotalBalance = 0;

                                                                                foreach (AccountMapLine _locAccountMapLine in _locAccountMapLines)
                                                                                {
                                                                                    if (_locAccountMapLine.AccountCharge == AccountCharge.Debit)
                                                                                    {
                                                                                        _locTotalAmountDebit = _locTotalAmountCollC;
                                                                                    }
                                                                                    if (_locAccountMapLine.AccountCharge == AccountCharge.Credit)
                                                                                    {
                                                                                        _locTotalAmountCredit = _locTotalAmountCollC;
                                                                                    }

                                                                                    GeneralJournal _saveGeneralJournal = new GeneralJournal(_currSession)
                                                                                    {
                                                                                        Company = _locReceivableTransactionBank.Company,
                                                                                        Workplace = _locReceivableTransactionBank.Workplace,
                                                                                        PostingDate = now,
                                                                                        PostingType = PostingType.Sales,
                                                                                        OrderType = OrderType.Item,
                                                                                        PostingMethod = PostingMethod.Bill,
                                                                                        PostingMethodType = PostingMethodType.Normal,
                                                                                        Account = _locAccountMapLine.Account,
                                                                                        Debit = _locTotalAmountDebit,
                                                                                        Credit = _locTotalAmountCredit,
                                                                                        JournalMonth = _locReceivableTransactionLine2.JournalMonth,
                                                                                        JournalYear = _locReceivableTransactionLine2.JournalYear,
                                                                                        ReceivableTransaction2 = _locReceivableTransaction2XPO,
                                                                                    };

                                                                                    _saveGeneralJournal.Save();
                                                                                    _saveGeneralJournal.Session.CommitTransaction();

                                                                                    #region AccountingPeriodicLine

                                                                                    if (_locAccountMapLine.Account.Code != null)
                                                                                    {

                                                                                        //Cari Account Periodic Line
                                                                                        AccountingPeriodicLine _locAPL = _currSession.FindObject<AccountingPeriodicLine>
                                                                                                                        (new GroupOperator(GroupOperatorType.And,
                                                                                                                        new BinaryOperator("Company", _locReceivableTransactionBank.Company),
                                                                                                                        new BinaryOperator("Workplace", _locReceivableTransactionBank.Workplace),
                                                                                                                        new BinaryOperator("AccountNo", _locAccountMapLine.Account),
                                                                                                                        new BinaryOperator("Month", _locReceivableTransactionLine2.JournalMonth),
                                                                                                                        new BinaryOperator("Year", _locReceivableTransactionLine2.JournalYear)));
                                                                                        if (_locAPL != null)
                                                                                        {
                                                                                            if (_locAPL.AccountNo != null)
                                                                                            {
                                                                                                if (_locAPL.AccountNo.BalanceType == BalanceType.Change)
                                                                                                {
                                                                                                    if (_locAPL.AccountNo.AccountCharge == AccountCharge.Debit)
                                                                                                    {
                                                                                                        if (_locTotalAmountDebit > 0)
                                                                                                        {
                                                                                                            _locTotalBalance = _locAPL.Balance + _locTotalAmountDebit;
                                                                                                        }
                                                                                                        if (_locTotalAmountCredit > 0)
                                                                                                        {
                                                                                                            _locTotalBalance = _locAPL.Balance - _locTotalAmountCredit;
                                                                                                        }
                                                                                                    }
                                                                                                    if (_locAPL.AccountNo.AccountCharge == AccountCharge.Credit)
                                                                                                    {
                                                                                                        if (_locTotalAmountDebit > 0)
                                                                                                        {
                                                                                                            _locTotalBalance = _locAPL.Balance - _locTotalAmountDebit;
                                                                                                        }
                                                                                                        if (_locTotalAmountCredit > 0)
                                                                                                        {
                                                                                                            _locTotalBalance = _locAPL.Balance + _locTotalAmountCredit;
                                                                                                        }
                                                                                                    }
                                                                                                }

                                                                                                _locAPL.Balance = _locTotalBalance;
                                                                                                _locAPL.Debit = _locAPL.Debit + _locTotalAmountDebit;
                                                                                                _locAPL.Credit = _locAPL.Credit + _locTotalAmountCredit;
                                                                                                _locAPL.Save();
                                                                                                _locAPL.Session.CommitTransaction();

                                                                                            }
                                                                                        }
                                                                                    }
                                                                                    #endregion COA
                                                                                }
                                                                            }
                                                                        }
                                                                        #endregion NormalAmount 
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                            
                                        }

                                        #endregion JournalMapCompanyAccountGroup
                                    }
                                }
                            }
                            else
                            {
                                #region JournalMapBusinessPartnerAccountGroup
                                if (_locReceivableTransactionLine2.BankAccount != null)
                                {
                                    BankAccountMap _locBankAccountMap = _currSession.FindObject<BankAccountMap>(new GroupOperator(GroupOperatorType.And,
                                                                                            new BinaryOperator("Company", _locReceivableTransactionLine2.Company),
                                                                                            new BinaryOperator("Workplace", _locReceivableTransactionLine2.Workplace),
                                                                                            new BinaryOperator("BankAccount", _locReceivableTransactionLine2.BankAccount),
                                                                                            new BinaryOperator("Active", true)));

                                    if(_locBankAccountMap != null)
                                    {
                                        if (_locBankAccountMap.BankAccountGroup != null)
                                        {
                                            _locBankAccountGroup = _locBankAccountMap.BankAccountGroup;

                                            _locTotalAmountCollBP = _locReceivableTransactionLine2.Amount;


                                            XPCollection<JournalMap> _locJournalMapItems = new XPCollection<JournalMap>
                                                                               (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                new BinaryOperator("Company", _locReceivableTransactionLine2.Company),
                                                                                new BinaryOperator("Workplace", _locReceivableTransactionLine2.Workplace),
                                                                                new BinaryOperator("BankAccountGroup", _locBankAccountGroup),
                                                                                new BinaryOperator("Active", true)));

                                            if (_locJournalMapItems != null && _locJournalMapItems.Count() > 0)
                                            {
                                                foreach (JournalMap _locJournalMapItem in _locJournalMapItems)
                                                {
                                                    XPCollection<JournalMapLine> _locJournalMapLines = new XPCollection<JournalMapLine>
                                                                                                       (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                        new BinaryOperator("Company", _locReceivableTransactionLine2.Company),
                                                                                                        new BinaryOperator("Workplace", _locReceivableTransactionLine2.Workplace),
                                                                                                        new BinaryOperator("JournalMap", _locJournalMapItem),
                                                                                                        new BinaryOperator("Active", true)));

                                                    if (_locJournalMapLines != null && _locJournalMapLines.Count() > 0)
                                                    {
                                                        foreach (JournalMapLine _locJournalMapLine in _locJournalMapLines)
                                                        {
                                                            if (_locJournalMapLine.AccountMap != null)
                                                            {
                                                                #region NormalAmount
                                                                if (_locJournalMapLine.AccountMap.PostingType == PostingType.Sales && _locJournalMapLine.AccountMap.OrderType == OrderType.Item
                                                                    && _locJournalMapLine.AccountMap.PostingMethod == PostingMethod.Bill && _locJournalMapLine.AccountMap.PostingMethodType == PostingMethodType.Normal)
                                                                {
                                                                    XPCollection<AccountMapLine> _locAccountMapLines = new XPCollection<AccountMapLine>
                                                                                                                   (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                                    new BinaryOperator("Company", _locReceivableTransactionLine2.Company),
                                                                                                                    new BinaryOperator("Workplace", _locReceivableTransactionLine2.Workplace),
                                                                                                                    new BinaryOperator("AccountMap", _locJournalMapLine.AccountMap),
                                                                                                                    new BinaryOperator("Active", true)));

                                                                    if (_locAccountMapLines != null && _locAccountMapLines.Count() > 0)
                                                                    {
                                                                        double _locTotalAmountDebit = 0;
                                                                        double _locTotalAmountCredit = 0;
                                                                        double _locTotalBalance = 0;

                                                                        foreach (AccountMapLine _locAccountMapLine in _locAccountMapLines)
                                                                        {
                                                                            if (_locAccountMapLine.AccountCharge == AccountCharge.Debit)
                                                                            {
                                                                                _locTotalAmountDebit = _locTotalAmountCollBP;
                                                                            }
                                                                            if (_locAccountMapLine.AccountCharge == AccountCharge.Credit)
                                                                            {
                                                                                _locTotalAmountCredit = _locTotalAmountCollBP;
                                                                            }

                                                                            GeneralJournal _saveGeneralJournal = new GeneralJournal(_currSession)
                                                                            {
                                                                                Company = _locReceivableTransactionLine2.Company,
                                                                                Workplace = _locReceivableTransactionLine2.Workplace,
                                                                                PostingDate = now,
                                                                                PostingType = PostingType.Sales,
                                                                                OrderType = OrderType.Item,
                                                                                PostingMethod = PostingMethod.Bill,
                                                                                PostingMethodType = PostingMethodType.Normal,
                                                                                Account = _locAccountMapLine.Account,
                                                                                Debit = _locTotalAmountDebit,
                                                                                Credit = _locTotalAmountCredit,
                                                                                JournalMonth = _locReceivableTransactionLine2.JournalMonth,
                                                                                JournalYear = _locReceivableTransactionLine2.JournalYear,
                                                                                ReceivableTransaction2 = _locReceivableTransaction2XPO,
                                                                            };

                                                                            _saveGeneralJournal.Save();
                                                                            _saveGeneralJournal.Session.CommitTransaction();

                                                                            #region AccountingPeriodicLine

                                                                            if (_locAccountMapLine.Account.Code != null)
                                                                            {

                                                                                //Cari Account Periodic Line
                                                                                AccountingPeriodicLine _locAPL = _currSession.FindObject<AccountingPeriodicLine>
                                                                                                                (new GroupOperator(GroupOperatorType.And,
                                                                                                                new BinaryOperator("Company", _locReceivableTransactionLine2.Company),
                                                                                                                new BinaryOperator("Workplace", _locReceivableTransactionLine2.Workplace),
                                                                                                                new BinaryOperator("AccountNo", _locAccountMapLine.Account),
                                                                                                                new BinaryOperator("Month", _locReceivableTransactionLine2.JournalMonth),
                                                                                                                new BinaryOperator("Year", _locReceivableTransactionLine2.JournalYear)));
                                                                                if (_locAPL != null)
                                                                                {
                                                                                    if (_locAPL.AccountNo != null)
                                                                                    {
                                                                                        if (_locAPL.AccountNo.BalanceType == BalanceType.Change)
                                                                                        {
                                                                                            if (_locAPL.AccountNo.AccountCharge == AccountCharge.Debit)
                                                                                            {
                                                                                                if (_locTotalAmountDebit > 0)
                                                                                                {
                                                                                                    _locTotalBalance = _locAPL.Balance + _locTotalAmountDebit;
                                                                                                }
                                                                                                if (_locTotalAmountCredit > 0)
                                                                                                {
                                                                                                    _locTotalBalance = _locAPL.Balance - _locTotalAmountCredit;
                                                                                                }
                                                                                            }
                                                                                            if (_locAPL.AccountNo.AccountCharge == AccountCharge.Credit)
                                                                                            {
                                                                                                if (_locTotalAmountDebit > 0)
                                                                                                {
                                                                                                    _locTotalBalance = _locAPL.Balance - _locTotalAmountDebit;
                                                                                                }
                                                                                                if (_locTotalAmountCredit > 0)
                                                                                                {
                                                                                                    _locTotalBalance = _locAPL.Balance + _locTotalAmountCredit;
                                                                                                }
                                                                                            }
                                                                                        }

                                                                                        _locAPL.Balance = _locTotalBalance;
                                                                                        _locAPL.Debit = _locAPL.Debit + _locTotalAmountDebit;
                                                                                        _locAPL.Credit = _locAPL.Credit + _locTotalAmountCredit;
                                                                                        _locAPL.Save();
                                                                                        _locAPL.Session.CommitTransaction();

                                                                                    }
                                                                                }
                                                                            }
                                                                            #endregion COA
                                                                        }
                                                                    }
                                                                }
                                                                #endregion NormalAmount   
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                                #endregion JournalMapBusinessPartnerAccountGroup

                                #region JournalMapCompanyAccountGroup
                                if (_locReceivableTransactionLine2.CompanyBankAccount != null)
                                {
                                    BankAccountMap _locBankAccountMap = _currSession.FindObject<BankAccountMap>(new GroupOperator(GroupOperatorType.And,
                                                                                            new BinaryOperator("Company", _locReceivableTransactionLine2.Company),
                                                                                            new BinaryOperator("Workplace", _locReceivableTransactionLine2.Workplace),
                                                                                            new BinaryOperator("BankAccount", _locReceivableTransactionLine2.CompanyBankAccount),
                                                                                            new BinaryOperator("Active", true)));
                                    if(_locBankAccountMap != null)
                                    {
                                        if (_locBankAccountMap.BankAccountGroup != null)
                                        {
                                            _locTotalAmountCollC = _locReceivableTransactionLine2.Amount;
                                            _locCompanyBankAccountGroup = _locBankAccountMap.BankAccountGroup;

                                            XPCollection<JournalMap> _locJournalMapItems = new XPCollection<JournalMap>
                                                                           (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                            new BinaryOperator("Company", _locReceivableTransactionLine2.Company),
                                                                            new BinaryOperator("Workplace", _locReceivableTransactionLine2.Workplace),
                                                                            new BinaryOperator("BankAccountGroup", _locCompanyBankAccountGroup),
                                                                            new BinaryOperator("Active", true)));

                                            if (_locJournalMapItems != null && _locJournalMapItems.Count() > 0)
                                            {
                                                foreach (JournalMap _locJournalMapItem in _locJournalMapItems)
                                                {
                                                    XPCollection<JournalMapLine> _locJournalMapLines = new XPCollection<JournalMapLine>
                                                                                                       (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                        new BinaryOperator("Company", _locReceivableTransactionLine2.Company),
                                                                                                        new BinaryOperator("Workplace", _locReceivableTransactionLine2.Workplace),
                                                                                                        new BinaryOperator("JournalMap", _locJournalMapItem),
                                                                                                        new BinaryOperator("Active", true)));

                                                    if (_locJournalMapLines != null && _locJournalMapLines.Count() > 0)
                                                    {
                                                        foreach (JournalMapLine _locJournalMapLine in _locJournalMapLines)
                                                        {
                                                            if (_locJournalMapLine.AccountMap != null)
                                                            {
                                                                #region NormalAmount
                                                                if (_locJournalMapLine.AccountMap.PostingType == PostingType.Sales && _locJournalMapLine.AccountMap.OrderType == OrderType.Item
                                                                    && _locJournalMapLine.AccountMap.PostingMethod == PostingMethod.Bill && _locJournalMapLine.AccountMap.PostingMethodType == PostingMethodType.Normal)
                                                                {
                                                                    XPCollection<AccountMapLine> _locAccountMapLines = new XPCollection<AccountMapLine>
                                                                                                                   (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                                    new BinaryOperator("Company", _locReceivableTransactionLine2.Company),
                                                                                                                    new BinaryOperator("Workplace", _locReceivableTransactionLine2.Workplace),
                                                                                                                    new BinaryOperator("AccountMap", _locJournalMapLine.AccountMap),
                                                                                                                    new BinaryOperator("Active", true)));

                                                                    if (_locAccountMapLines != null && _locAccountMapLines.Count() > 0)
                                                                    {
                                                                        double _locTotalAmountDebit = 0;
                                                                        double _locTotalAmountCredit = 0;
                                                                        double _locTotalBalance = 0;

                                                                        foreach (AccountMapLine _locAccountMapLine in _locAccountMapLines)
                                                                        {
                                                                            if (_locAccountMapLine.AccountCharge == AccountCharge.Debit)
                                                                            {
                                                                                _locTotalAmountDebit = _locTotalAmountCollC;
                                                                            }
                                                                            if (_locAccountMapLine.AccountCharge == AccountCharge.Credit)
                                                                            {
                                                                                _locTotalAmountCredit = _locTotalAmountCollC;
                                                                            }

                                                                            GeneralJournal _saveGeneralJournal = new GeneralJournal(_currSession)
                                                                            {
                                                                                Company = _locReceivableTransactionLine2.Company,
                                                                                Workplace = _locReceivableTransactionLine2.Workplace,
                                                                                PostingDate = now,
                                                                                PostingType = PostingType.Sales,
                                                                                OrderType = OrderType.Item,
                                                                                PostingMethod = PostingMethod.Bill,
                                                                                PostingMethodType = PostingMethodType.Normal,
                                                                                Account = _locAccountMapLine.Account,
                                                                                Debit = _locTotalAmountDebit,
                                                                                Credit = _locTotalAmountCredit,
                                                                                JournalMonth = _locReceivableTransactionLine2.JournalMonth,
                                                                                JournalYear = _locReceivableTransactionLine2.JournalYear,
                                                                                ReceivableTransaction2 = _locReceivableTransaction2XPO,
                                                                            };

                                                                            _saveGeneralJournal.Save();
                                                                            _saveGeneralJournal.Session.CommitTransaction();

                                                                            #region AccountingPeriodicLine

                                                                            if (_locAccountMapLine.Account.Code != null)
                                                                            {

                                                                                //Cari Account Periodic Line
                                                                                AccountingPeriodicLine _locAPL = _currSession.FindObject<AccountingPeriodicLine>
                                                                                                                (new GroupOperator(GroupOperatorType.And,
                                                                                                                new BinaryOperator("Company", _locReceivableTransactionLine2.Company),
                                                                                                                new BinaryOperator("Workplace", _locReceivableTransactionLine2.Workplace),
                                                                                                                new BinaryOperator("AccountNo", _locAccountMapLine.Account),
                                                                                                                new BinaryOperator("Month", _locReceivableTransactionLine2.JournalMonth),
                                                                                                                new BinaryOperator("Year", _locReceivableTransactionLine2.JournalYear)));
                                                                                if (_locAPL != null)
                                                                                {
                                                                                    if (_locAPL.AccountNo != null)
                                                                                    {
                                                                                        if (_locAPL.AccountNo.BalanceType == BalanceType.Change)
                                                                                        {
                                                                                            if (_locAPL.AccountNo.AccountCharge == AccountCharge.Debit)
                                                                                            {
                                                                                                if (_locTotalAmountDebit > 0)
                                                                                                {
                                                                                                    _locTotalBalance = _locAPL.Balance + _locTotalAmountDebit;
                                                                                                }
                                                                                                if (_locTotalAmountCredit > 0)
                                                                                                {
                                                                                                    _locTotalBalance = _locAPL.Balance - _locTotalAmountCredit;
                                                                                                }
                                                                                            }
                                                                                            if (_locAPL.AccountNo.AccountCharge == AccountCharge.Credit)
                                                                                            {
                                                                                                if (_locTotalAmountDebit > 0)
                                                                                                {
                                                                                                    _locTotalBalance = _locAPL.Balance - _locTotalAmountDebit;
                                                                                                }
                                                                                                if (_locTotalAmountCredit > 0)
                                                                                                {
                                                                                                    _locTotalBalance = _locAPL.Balance + _locTotalAmountCredit;
                                                                                                }
                                                                                            }
                                                                                        }

                                                                                        _locAPL.Balance = _locTotalBalance;
                                                                                        _locAPL.Debit = _locAPL.Debit + _locTotalAmountDebit;
                                                                                        _locAPL.Credit = _locAPL.Credit + _locTotalAmountCredit;
                                                                                        _locAPL.Save();
                                                                                        _locAPL.Session.CommitTransaction();

                                                                                    }
                                                                                }
                                                                            }
                                                                            #endregion COA
                                                                        }
                                                                    }
                                                                }
                                                                #endregion NormalAmount 
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    } 
                                }

                                #endregion JournalMapCompanyAccountGroup
                            }
                            #endregion BankAccount
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = ReceivableTransaction2 ", ex.ToString());
            }
        }

        #endregion JournalReceivable

        private void SetStatusReceivableTransactionLine(Session _currSession, ReceivableTransaction2 _locReceivableTransaction2XPO)
        {
            try
            {
                DateTime now = DateTime.Now;
                if (_locReceivableTransaction2XPO != null)
                {
                    XPCollection<ReceivableTransactionLine2> _locReceivableTransactionLine2s = new XPCollection<ReceivableTransactionLine2>(_currSession,
                                                                        new GroupOperator(GroupOperatorType.And,
                                                                        new BinaryOperator("ReceivableTransaction2", _locReceivableTransaction2XPO),
                                                                        new BinaryOperator("Select", true),
                                                                        new GroupOperator(GroupOperatorType.Or,
                                                                        new BinaryOperator("Status", Status.Progress),
                                                                        new BinaryOperator("Status", Status.Posted))));

                    if (_locReceivableTransactionLine2s != null && _locReceivableTransactionLine2s.Count() > 0)
                    {
                        foreach (ReceivableTransactionLine2 _locReceivableTransactionLine2 in _locReceivableTransactionLine2s)
                        {
                            if (_locReceivableTransactionLine2.Outstanding == 0 || _locReceivableTransactionLine2.Settled == true)
                            {
                                _locReceivableTransactionLine2.Status = Status.Close;
                                _locReceivableTransactionLine2.ActivationPosting = true;
                                _locReceivableTransactionLine2.StatusDate = now;

                                #region Invoice
                                if (_locReceivableTransactionLine2.SalesInvoice != null)
                                {
                                    _locReceivableTransactionLine2.SalesInvoice.Status = Status.Close;
                                    _locReceivableTransactionLine2.SalesInvoice.StatusDate = now;
                                    _locReceivableTransactionLine2.SalesInvoice.Save();
                                    _locReceivableTransactionLine2.SalesInvoice.Session.CommitTransaction();

                                    XPCollection<SalesOrderCollection> _locSalesOrderCollections = new XPCollection<SalesOrderCollection>
                                                                                                (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                new BinaryOperator("SalesInvoice", _locReceivableTransactionLine2.SalesInvoice)));
                                    if (_locSalesOrderCollections != null && _locSalesOrderCollections.Count() > 0)
                                    {
                                        foreach (SalesOrderCollection _locSalesOrderCollection in _locSalesOrderCollections)
                                        {
                                            if (_locSalesOrderCollection.SalesOrder != null)
                                            {
                                                _locSalesOrderCollection.SalesOrder.Status = Status.Close;
                                                _locSalesOrderCollection.SalesOrder.StatusDate = now;
                                                _locSalesOrderCollection.SalesOrder.Save();
                                                _locSalesOrderCollection.SalesOrder.Session.CommitTransaction();
                                            }
                                        }
                                    }
                                }

                                #endregion Invoice

                                #region PickingMonitoring
                                if (_locReceivableTransactionLine2.PickingMonitoring != null)
                                {
                                    _locReceivableTransactionLine2.PickingMonitoring.CollectStatus = CollectStatus.Finish;
                                    _locReceivableTransactionLine2.PickingMonitoring.CollectStatusDate = now;
                                    _locReceivableTransactionLine2.PickingMonitoring.Save();
                                    _locReceivableTransactionLine2.PickingMonitoring.Session.CommitTransaction();
                                }

                                #endregion PickingMonitoring

                                #region OrderCollection
                                if (_locReceivableTransactionLine2.OrderCollectionMonitoring != null)
                                {
                                    #region OrderCollectionMonitoring
                                    _locReceivableTransactionLine2.OrderCollectionMonitoring.CollectStatus = CollectStatus.Finish;
                                    _locReceivableTransactionLine2.OrderCollectionMonitoring.CollectStatusDate = now;
                                    _locReceivableTransactionLine2.OrderCollectionMonitoring.Status = Status.Close;
                                    _locReceivableTransactionLine2.OrderCollectionMonitoring.StatusDate = now;
                                    _locReceivableTransactionLine2.OrderCollectionMonitoring.Save();
                                    _locReceivableTransactionLine2.OrderCollectionMonitoring.Session.CommitTransaction();
                                    #endregion OrderCollectionMonitoring

                                    #region OrderCollectionLine
                                    if (_locReceivableTransactionLine2.OrderCollectionMonitoring.OrderCollectionLine != null)
                                    {
                                        _locReceivableTransactionLine2.OrderCollectionMonitoring.OrderCollectionLine.CollectStatus = CollectStatus.Finish;
                                        _locReceivableTransactionLine2.OrderCollectionMonitoring.OrderCollectionLine.CollectStatusDate = now;
                                        _locReceivableTransactionLine2.OrderCollectionMonitoring.OrderCollectionLine.Status = Status.Close;
                                        _locReceivableTransactionLine2.OrderCollectionMonitoring.OrderCollectionLine.StatusDate = now;
                                        _locReceivableTransactionLine2.OrderCollectionMonitoring.OrderCollectionLine.Save();
                                        _locReceivableTransactionLine2.OrderCollectionMonitoring.OrderCollectionLine.Session.CommitTransaction();
                                    }
                                    #endregion OrderCollectionLine
                                }

                                #endregion OrderCollection
                            }
                            else
                            {
                                _locReceivableTransactionLine2.Status = Status.Posted;
                                _locReceivableTransactionLine2.StatusDate = now;

                                #region PickingMonitoring
                                if (_locReceivableTransactionLine2.PickingMonitoring != null)
                                {
                                    _locReceivableTransactionLine2.PickingMonitoring.CollectStatus = CollectStatus.Collect;
                                    _locReceivableTransactionLine2.PickingMonitoring.CollectStatusDate = now;
                                    _locReceivableTransactionLine2.PickingMonitoring.Save();
                                    _locReceivableTransactionLine2.PickingMonitoring.Session.CommitTransaction();
                                }

                                #endregion PickingMonitoring

                                #region OrderCollection
                                if (_locReceivableTransactionLine2.OrderCollectionMonitoring != null)
                                {
                                    #region OrderCollectionMonitoring
                                    _locReceivableTransactionLine2.OrderCollectionMonitoring.CollectStatus = CollectStatus.Collect;
                                    _locReceivableTransactionLine2.OrderCollectionMonitoring.CollectStatusDate = now;
                                    _locReceivableTransactionLine2.OrderCollectionMonitoring.Status = Status.Posted;
                                    _locReceivableTransactionLine2.OrderCollectionMonitoring.StatusDate = now;
                                    _locReceivableTransactionLine2.OrderCollectionMonitoring.Save();
                                    _locReceivableTransactionLine2.OrderCollectionMonitoring.Session.CommitTransaction();
                                    #endregion OrderCollectionMonitoring

                                    #region OrderCollectionLine
                                    if (_locReceivableTransactionLine2.OrderCollectionMonitoring.OrderCollectionLine != null)
                                    {
                                        _locReceivableTransactionLine2.OrderCollectionMonitoring.OrderCollectionLine.CollectStatus = CollectStatus.Collect;
                                        _locReceivableTransactionLine2.OrderCollectionMonitoring.OrderCollectionLine.CollectStatusDate = now;
                                        _locReceivableTransactionLine2.OrderCollectionMonitoring.OrderCollectionLine.Status = Status.Posted;
                                        _locReceivableTransactionLine2.OrderCollectionMonitoring.OrderCollectionLine.StatusDate = now;
                                        _locReceivableTransactionLine2.OrderCollectionMonitoring.OrderCollectionLine.Save();
                                        _locReceivableTransactionLine2.OrderCollectionMonitoring.OrderCollectionLine.Session.CommitTransaction();
                                    }
                                    #endregion OrderCollectionLine
                                }

                                #endregion OrderCollection
                            }
                            _locReceivableTransactionLine2.Save();
                            _locReceivableTransactionLine2.Session.CommitTransaction();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = ReceivableTransaction2 " + ex.ToString());
            }
        }

        private void SetNormal(Session _currSession, ReceivableTransaction2 _locReceivableTransaction2XPO)
        {
            try
            {
                DateTime now = DateTime.Now;

                if (_locReceivableTransaction2XPO != null)
                {
                    XPCollection<ReceivableTransactionLine2> _locReceivableTransactionLine2s = new XPCollection<ReceivableTransactionLine2>(_currSession,
                                                                                     new GroupOperator(GroupOperatorType.And,
                                                                                     new BinaryOperator("ReceivableTransaction2", _locReceivableTransaction2XPO),
                                                                                     new BinaryOperator("Select", true)));

                    if (_locReceivableTransactionLine2s != null && _locReceivableTransactionLine2s.Count > 0)
                    {
                        foreach (ReceivableTransactionLine2 _locReceivableTransactionLine2 in _locReceivableTransactionLine2s)
                        {
                            if (_locReceivableTransactionLine2.Status == Status.Progress || _locReceivableTransactionLine2.Status == Status.Posted || _locReceivableTransactionLine2.Status == Status.Close)
                            {
                                if (_locReceivableTransactionLine2.Outstanding == 0 || _locReceivableTransactionLine2.Settled == true)
                                {
                                    _locReceivableTransactionLine2.Select = false;
                                }
                                else
                                {
                                    _locReceivableTransactionLine2.Select = true;
                                }
                                _locReceivableTransactionLine2.Amount = 0;
                                _locReceivableTransactionLine2.AmountCN = 0;
                                _locReceivableTransactionLine2.AmountDN = 0;
                                _locReceivableTransactionLine2.Save();
                                _locReceivableTransactionLine2.Session.CommitTransaction();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = ReceivableTransaction2 " + ex.ToString());
            }
        }

        private void SetFinalStatusReceivableTransaction(Session _currSession, ReceivableTransaction2 _locReceivableTransaction2XPO)
        {
            try
            {
                DateTime now = DateTime.Now;
                int _locStatusCount = 0;
                int _locReceiTransLineCount = 0;

                if (_locReceivableTransaction2XPO != null)
                {
                    XPCollection<ReceivableTransactionLine2> _locReceivableTransactionLine2s = new XPCollection<ReceivableTransactionLine2>(_currSession,
                                                                                     new GroupOperator(GroupOperatorType.And,
                                                                                     new BinaryOperator("ReceivableTransaction2", _locReceivableTransaction2XPO)));

                    if (_locReceivableTransactionLine2s != null && _locReceivableTransactionLine2s.Count > 0)
                    {
                        _locReceiTransLineCount = _locReceivableTransactionLine2s.Count();

                        foreach (ReceivableTransactionLine2 _locReceivableTransactionLine2 in _locReceivableTransactionLine2s)
                        {
                            if (_locReceivableTransactionLine2.Status == Status.Close)
                            {
                                _locStatusCount = _locStatusCount + 1;
                            }
                        }

                        if (_locStatusCount == _locReceiTransLineCount)
                        {
                            _locReceivableTransaction2XPO.ActivationPosting = true;
                            _locReceivableTransaction2XPO.Status = Status.Close;
                            _locReceivableTransaction2XPO.StatusDate = now;
                            _locReceivableTransaction2XPO.Save();
                            _locReceivableTransaction2XPO.Session.CommitTransaction();
                        }
                        else
                        {
                            _locReceivableTransaction2XPO.Status = Status.Posted;
                            _locReceivableTransaction2XPO.StatusDate = now;
                            _locReceivableTransaction2XPO.Save();
                            _locReceivableTransaction2XPO.Session.CommitTransaction();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = ReceivableTransaction2 " + ex.ToString());
            }
        }

        private void SetFinalStatusOrderCollection(Session _currSession, ReceivableTransaction2 _locReceivableTransaction2XPO)
        {
            try
            {
                DateTime now = DateTime.Now;
                int _locStatusCount = 0;
                int _locOrderCollLineCount = 0;

                if (_locReceivableTransaction2XPO != null)
                {
                    if(_locReceivableTransaction2XPO.Picking != null)
                    {
                        XPQuery<OrderCollectionMonitoring> _orderCollectionMonitoringsQuery = new XPQuery<OrderCollectionMonitoring>(_currSession);

                        var _orderCollectionMonitorings = from ocm in _orderCollectionMonitoringsQuery
                                                          where (ocm.Status != Status.Close
                                                          && ocm.CollectStatus == CollectStatus.Collect
                                                          && ocm.Company == _locReceivableTransaction2XPO.Company
                                                          && ocm.Workplace == _locReceivableTransaction2XPO.Workplace
                                                          && ocm.Picking == _locReceivableTransaction2XPO.Picking
                                                          && ocm.OrderCollection != null)
                                                          group ocm by ocm.OrderCollection into g
                                                          select new { OrderCollection = g.Key };

                        if (_orderCollectionMonitorings != null && _orderCollectionMonitorings.Count() > 0)
                        {
                            foreach (var _orderCollectionMonitoring in _orderCollectionMonitorings)
                            {
                                XPCollection<OrderCollectionLine> _locOrderCollectionLines = new XPCollection<OrderCollectionLine>(_currSession,
                                                                                                  new GroupOperator(GroupOperatorType.And,
                                                                                                  new BinaryOperator("OrderCollection", _orderCollectionMonitoring.OrderCollection)));
                                if (_locOrderCollectionLines != null && _locOrderCollectionLines.Count() > 0)
                                {
                                    _locOrderCollLineCount = _locOrderCollectionLines.Count();

                                    foreach (OrderCollectionLine _locOrderCollectionLine in _locOrderCollectionLines)
                                    {
                                        if (_locOrderCollectionLine.Status == Status.Close)
                                        {
                                            _locStatusCount = _locStatusCount + 1;
                                        }
                                    }

                                    if (_locStatusCount == _locOrderCollLineCount)
                                    {
                                        _orderCollectionMonitoring.OrderCollection.ActivationPosting = true;
                                        _orderCollectionMonitoring.OrderCollection.Status = Status.Close;
                                        _orderCollectionMonitoring.OrderCollection.StatusDate = now;
                                        _orderCollectionMonitoring.OrderCollection.Save();
                                        _orderCollectionMonitoring.OrderCollection.Session.CommitTransaction();
                                    }
                                    else
                                    {
                                        _orderCollectionMonitoring.OrderCollection.Status = Status.Posted;
                                        _orderCollectionMonitoring.OrderCollection.StatusDate = now;
                                        _orderCollectionMonitoring.OrderCollection.Save();
                                        _orderCollectionMonitoring.OrderCollection.Session.CommitTransaction();
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = ReceivableTransaction2 " + ex.ToString());
            }
        }

        #endregion Posting

        #region Global Method

        private void SuccessMessageShow(string _locMessage)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 4000;
            options.Message = string.Format("{0}", _locMessage);
            options.Type = InformationType.Success;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Success";
            options.Win.Type = WinMessageType.Flyout;
            Application.ShowViewStrategy.ShowMessage(options);
        }

        private void ErrorMessageShow(string _locMessage)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 2000;
            options.Message = string.Format(_locMessage);
            options.Type = InformationType.Warning;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Error";
            options.Win.Type = WinMessageType.Alert;
            Application.ShowViewStrategy.ShowMessage(options);
        }

        #endregion Global Method
    }
}
