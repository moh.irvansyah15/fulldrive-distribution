﻿namespace FullDrive.Module.Controllers
{
    partial class InventoryTransferOutLineActionController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.InventoryTransferOutLineSelectAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.InventoryTransferOutLineUnselectAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            // 
            // InventoryTransferOutLineSelectAction
            // 
            this.InventoryTransferOutLineSelectAction.Caption = "Select";
            this.InventoryTransferOutLineSelectAction.ConfirmationMessage = null;
            this.InventoryTransferOutLineSelectAction.Id = "InventoryTransferOutLineSelectActionId";
            this.InventoryTransferOutLineSelectAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.InventoryTransferOutLine);
            this.InventoryTransferOutLineSelectAction.ToolTip = null;
            this.InventoryTransferOutLineSelectAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.InventoryTransferOutLineSelectAction_Execute);
            // 
            // InventoryTransferOutLineUnselectAction
            // 
            this.InventoryTransferOutLineUnselectAction.Caption = "Unselect";
            this.InventoryTransferOutLineUnselectAction.ConfirmationMessage = null;
            this.InventoryTransferOutLineUnselectAction.Id = "InventoryTransferOutLineUnselectActionId";
            this.InventoryTransferOutLineUnselectAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.InventoryTransferOutLine);
            this.InventoryTransferOutLineUnselectAction.ToolTip = null;
            this.InventoryTransferOutLineUnselectAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.InventoryTransferOutLineUnselectAction_Execute);
            // 
            // InventoryTransferOutLineActionController
            // 
            this.Actions.Add(this.InventoryTransferOutLineSelectAction);
            this.Actions.Add(this.InventoryTransferOutLineUnselectAction);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SimpleAction InventoryTransferOutLineSelectAction;
        private DevExpress.ExpressApp.Actions.SimpleAction InventoryTransferOutLineUnselectAction;
    }
}
