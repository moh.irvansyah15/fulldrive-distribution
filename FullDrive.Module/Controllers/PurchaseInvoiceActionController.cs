﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;

using FullDrive.Module.BusinessObjects;
using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.Xpo;
using System.Collections;
using DevExpress.Xpo;
using System.IO;
using System.Web;

namespace FullDrive.Module.Controllers
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class PurchaseInvoiceActionController : ViewController
    {
        private ChoiceActionItem _selectionListviewFilter;
        private ChoiceActionItem _setApprovalLevel;

        public PurchaseInvoiceActionController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.
            #region Filter Status
            PurchaseInvoiceListviewFilterSelectionAction.Items.Clear();
            foreach (object _currApproval in Enum.GetValues(typeof(CustomProcess.Status)))
            {
                EnumDescriptor _ed = new EnumDescriptor(typeof(CustomProcess.Status));
                _selectionListviewFilter = new ChoiceActionItem(_ed.GetCaption(_currApproval), _currApproval);
                PurchaseInvoiceListviewFilterSelectionAction.Items.Add(_selectionListviewFilter);
            }
            #endregion Filter Status
             
        }

        protected override void OnActivated()
        {
            //TypeOfView = typeof(ObjectView);
            base.OnActivated();
            // Perform various tasks depending on the target View.

            #region Approval
            var _locUser = SecuritySystem.CurrentUserName;
            GlobalFunction _globalFunction = new GlobalFunction();
            //Session _currentSession = null;
            //if (this.ObjectSpace != null)
            //{
            //    _currentSession = ((XPObjectSpace)this.ObjectSpace).Session;
            //}

            if (this.ObjectSpace != null)
            {
                UserAccess _locUserAccess = this.ObjectSpace.FindObject<UserAccess>(new BinaryOperator("UserName", _locUser));

                if (_locUserAccess != null)
                {
                    PurchaseInvoiceApprovalAction.Items.Clear();

                    XPCollection<ApplicationSetupDetail> _locAppSetupDetails = new XPCollection<ApplicationSetupDetail>
                                                                                 (_locUserAccess.Session, new GroupOperator(GroupOperatorType.And,
                                                                                  new BinaryOperator("UserAccess", _locUserAccess),
                                                                                  new BinaryOperator("ObjectList", CustomProcess.ObjectList.PurchaseInvoice),
                                                                                  new BinaryOperator("FunctionList", CustomProcess.FunctionList.Approval),
                                                                                  new BinaryOperator("Active", true)));

                    if (_locAppSetupDetails != null && _locAppSetupDetails.Count > 0)
                    {

                        foreach (ApplicationSetupDetail _locAppSetupDetail in _locAppSetupDetails)
                        {
                            foreach (object _currApproval in Enum.GetValues(typeof(CustomProcess.ApprovalLevel)))
                            {
                                if (_locAppSetupDetail.ApprovalLevel != CustomProcess.ApprovalLevel.None)
                                {
                                    EnumDescriptor _ed = new EnumDescriptor(typeof(CustomProcess.ApprovalLevel));
                                    _setApprovalLevel = new ChoiceActionItem(_ed.GetCaption(_locAppSetupDetail.ApprovalLevel), _locAppSetupDetail.ApprovalLevel);
                                }
                            }
                            PurchaseInvoiceApprovalAction.Items.Add(_setApprovalLevel);
                        }
                    }
                }
            }
            #endregion Approval

        }

        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }

        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        private void PurchaseInvoiceGetITIAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;

                if (_objectsToProcess != null)
                {
                    foreach (Object obj in _objectsToProcess)
                    {
                        PurchaseInvoice _locPurchaseInvoiceOS = (PurchaseInvoice)_objectSpace.GetObject(obj);

                        if (_locPurchaseInvoiceOS != null)
                        {
                            if (_locPurchaseInvoiceOS.Session != null)
                            {
                                _currSession = _locPurchaseInvoiceOS.Session;
                            }

                            if (_locPurchaseInvoiceOS.Code != null && _currSession != null)
                            {
                                _currObjectId = _locPurchaseInvoiceOS.Code;

                                PurchaseInvoice _locPurchaseInvoiceXPO = _currSession.FindObject<PurchaseInvoice>
                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                     new BinaryOperator("Code", _currObjectId),
                                                                     new GroupOperator(GroupOperatorType.Or,
                                                                     new BinaryOperator("Status", Status.Open),
                                                                     new BinaryOperator("Status", Status.Progress))));

                                if (_locPurchaseInvoiceXPO != null)
                                {
                                    XPCollection<InvoicePurchaseCollection> _locInvoicePurchaseCollections = new XPCollection<InvoicePurchaseCollection>
                                                                                        (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                        new BinaryOperator("PurchaseInvoice", _locPurchaseInvoiceXPO),
                                                                                        new GroupOperator(GroupOperatorType.Or,
                                                                                        new BinaryOperator("Status", Status.Open),
                                                                                        new BinaryOperator("Status", Status.Progress))));

                                    if (_locInvoicePurchaseCollections != null && _locInvoicePurchaseCollections.Count() > 0)
                                    {
                                        foreach (InvoicePurchaseCollection _locInvoicePurchaseCollection in _locInvoicePurchaseCollections)
                                        {
                                            if (_locInvoicePurchaseCollection.PurchaseInvoice != null && _locInvoicePurchaseCollection.InventoryTransferIn != null)
                                            {
                                                GetInventoryTransferInMonitoring(_currSession, _locInvoicePurchaseCollection.InventoryTransferIn, _locPurchaseInvoiceXPO);
                                            }
                                        }
                                        SuccessMessageShow("Inventory Transfer In Has Been Successfully Getting into Purchase Invoice");
                                    }
                                }
                                else
                                {
                                    ErrorMessageShow("Inventory Transfer In Not Available");
                                }
                            }
                            else
                            {
                                ErrorMessageShow("Inventory Transfer In Not Available");
                            }
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }

            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PurchaseInvoice " + ex.ToString());
            }
        }

        private void PurchaseInvoiceApprovalAction_Execute(object sender, SingleChoiceActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                ApplicationSetupDetail _locAppSetDetail = null;
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                var User = SecuritySystem.CurrentUserName;
                Session _currentSession = null;
                string _currObjectId = null;

                foreach (Object obj in _objectsToProcess)
                {
                    PurchaseInvoice _objInNewObjectSpace = (PurchaseInvoice)_objectSpace.GetObject(obj);

                    if (_objInNewObjectSpace != null)
                    {
                        if (_objInNewObjectSpace.Code != null)
                        {
                            _currObjectId = _objInNewObjectSpace.Code;
                        }
                        if (_objInNewObjectSpace.Session != null)
                        {
                            _currentSession = _objInNewObjectSpace.Session;
                        }
                    }

                    if (_currObjectId != null && _currentSession != null)
                    {
                        UserAccess _locUserAccess = _currentSession.FindObject<UserAccess>(new BinaryOperator("UserName", User));

                        PurchaseInvoice _locPurchaseInvoiceXPO = _currentSession.FindObject<PurchaseInvoice>
                                                                        (new GroupOperator(GroupOperatorType.And,
                                                                         new BinaryOperator("Code", _currObjectId),
                                                                         new BinaryOperator("Status", Status.Open)));

                        if (_locPurchaseInvoiceXPO != null)
                        {
                            ApprovalLine _locApprovalLine = _currentSession.FindObject<ApprovalLine>
                                                                            (new GroupOperator(GroupOperatorType.And,
                                                                             new BinaryOperator("PurchaseInvoice", _locPurchaseInvoiceXPO),
                                                                             new BinaryOperator("EndApproval", true)));
                            if (_locApprovalLine == null)
                            {
                                #region Approval Level 1
                                if ((ApprovalLevel)e.SelectedChoiceActionItem.Data == ApprovalLevel.Level1)
                                {
                                    _locAppSetDetail = _globFunc.GetApplicationSetupDetailByApprovalLevel(_currentSession, _locUserAccess, ApprovalLevel.Level1, ObjectList.PurchaseInvoice);
                                    if (_locAppSetDetail != null)
                                    {
                                        //Buat bs input langsung ke approvalline
                                        ApprovalLine _locApprovalLineXPO = _currentSession.FindObject<ApprovalLine>
                                                                            (new GroupOperator(GroupOperatorType.And,
                                                                             new BinaryOperator("PurchaseInvoice", _locPurchaseInvoiceXPO),
                                                                             new BinaryOperator("ApprovalLevel", ApprovalLevel.Level1)));
                                        if (_locApprovalLineXPO == null)
                                        {

                                            if (_locAppSetDetail.EndApproval == true)
                                            {
                                                ApprovalLine _saveDataAL = new ApprovalLine(_currentSession)
                                                {
                                                    ApprovalDate = now,
                                                    ApprovalStatus = Status.Approved,
                                                    ApprovalLevel = ApprovalLevel.Level1,
                                                    EndApproval = true,
                                                    PurchaseInvoice = _locPurchaseInvoiceXPO,
                                                };
                                                _saveDataAL.Save();
                                                _saveDataAL.Session.CommitTransaction();

                                                _locPurchaseInvoiceXPO.ActivationPosting = true;
                                                _locPurchaseInvoiceXPO.ActiveApproved1 = false;
                                                _locPurchaseInvoiceXPO.ActiveApproved2 = false;
                                                _locPurchaseInvoiceXPO.ActiveApproved3 = true;
                                                _locPurchaseInvoiceXPO.Save();
                                                _locPurchaseInvoiceXPO.Session.CommitTransaction();
                                            }
                                            else
                                            {
                                                ApprovalLine _saveDataAL = new ApprovalLine(_currentSession)
                                                {
                                                    ApprovalDate = now,
                                                    ApprovalStatus = Status.Approved,
                                                    ApprovalLevel = ApprovalLevel.Level1,
                                                    PurchaseInvoice = _locPurchaseInvoiceXPO,
                                                };
                                                _saveDataAL.Save();
                                                _saveDataAL.Session.CommitTransaction();

                                                _locPurchaseInvoiceXPO.ActiveApproved1 = true;
                                                _locPurchaseInvoiceXPO.ActiveApproved2 = false;
                                                _locPurchaseInvoiceXPO.ActiveApproved3 = false;
                                                _locPurchaseInvoiceXPO.Save();
                                                _locPurchaseInvoiceXPO.Session.CommitTransaction();
                                            }

                                            //Send Email
                                            ApprovalLine _locApprovalLineXPO2 = _currentSession.FindObject<ApprovalLine>
                                                                                (new GroupOperator(GroupOperatorType.And,
                                                                                 new BinaryOperator("PurchaseInvoice", _locPurchaseInvoiceXPO)));

                                            if (_locApprovalLineXPO2 != null)
                                            {
                                                SendEmail(_currentSession, _locPurchaseInvoiceXPO, Status.Lock, _locApprovalLineXPO2);
                                            }

                                            SuccessMessageShow("Purchase Invoice has successfully Approve");
                                        }
                                    }
                                }
                                #endregion Approval Level 1

                                #region Approval Level 2
                                if ((ApprovalLevel)e.SelectedChoiceActionItem.Data == ApprovalLevel.Level2)
                                {
                                    _locAppSetDetail = _globFunc.GetApplicationSetupDetailByApprovalLevel(_currentSession, _locUserAccess, ApprovalLevel.Level2, ObjectList.PurchaseInvoice);

                                    if (_locAppSetDetail != null)
                                    {
                                        ApprovalLine _locApprovalLineXPO = _currentSession.FindObject<ApprovalLine>
                                                                            (new GroupOperator(GroupOperatorType.And,
                                                                             new BinaryOperator("PurchaseInvoice", _locPurchaseInvoiceXPO),
                                                                             new BinaryOperator("ApprovalLevel", ApprovalLevel.Level2)));

                                        if (_locApprovalLineXPO == null)
                                        {
                                            if (_locAppSetDetail.EndApproval == true)
                                            {
                                                ApprovalLine _saveDataAL = new ApprovalLine(_currentSession)
                                                {
                                                    ApprovalDate = now,
                                                    ApprovalStatus = Status.Approved,
                                                    ApprovalLevel = ApprovalLevel.Level2,
                                                    EndApproval = true,
                                                    PurchaseInvoice = _locPurchaseInvoiceXPO,
                                                };
                                                _saveDataAL.Save();
                                                _saveDataAL.Session.CommitTransaction();

                                                _locPurchaseInvoiceXPO.ActivationPosting = true;
                                                _locPurchaseInvoiceXPO.ActiveApproved1 = false;
                                                _locPurchaseInvoiceXPO.ActiveApproved2 = false;
                                                _locPurchaseInvoiceXPO.ActiveApproved3 = true;
                                                _locPurchaseInvoiceXPO.Save();
                                                _locPurchaseInvoiceXPO.Session.CommitTransaction();
                                            }
                                            else
                                            {
                                                ApprovalLine _saveDataAL = new ApprovalLine(_currentSession)
                                                {
                                                    ApprovalDate = now,
                                                    ApprovalStatus = Status.Approved,
                                                    ApprovalLevel = ApprovalLevel.Level2,
                                                    PurchaseInvoice = _locPurchaseInvoiceXPO,
                                                };
                                                _saveDataAL.Save();
                                                _saveDataAL.Session.CommitTransaction();

                                                SetApprovalLine(_currentSession, _locPurchaseInvoiceXPO, ApprovalLevel.Level1);

                                                _locPurchaseInvoiceXPO.ActiveApproved1 = false;
                                                _locPurchaseInvoiceXPO.ActiveApproved2 = true;
                                                _locPurchaseInvoiceXPO.ActiveApproved3 = false;
                                                _locPurchaseInvoiceXPO.Save();
                                                _locPurchaseInvoiceXPO.Session.CommitTransaction();
                                            }

                                            //Send Email
                                            ApprovalLine _locApprovalLineXPO2 = _currentSession.FindObject<ApprovalLine>
                                                                                (new GroupOperator(GroupOperatorType.And,
                                                                                 new BinaryOperator("PurchaseInvoice", _locPurchaseInvoiceXPO)));

                                            if (_locApprovalLineXPO2 != null)
                                            {
                                                SendEmail(_currentSession, _locPurchaseInvoiceXPO, Status.Lock, _locApprovalLineXPO2);
                                            }

                                            SuccessMessageShow("Purchase Invoice has successfully Approve");
                                        }
                                    }
                                }
                                #endregion Approval Level 2

                                #region Approval Level 3
                                if ((ApprovalLevel)e.SelectedChoiceActionItem.Data == ApprovalLevel.Level3)
                                {
                                    _locAppSetDetail = _globFunc.GetApplicationSetupDetailByApprovalLevel(_currentSession, _locUserAccess, ApprovalLevel.Level3, ObjectList.PurchaseInvoice);

                                    if (_locAppSetDetail != null)
                                    {
                                        ApprovalLine _locApprovalLineXPO = _currentSession.FindObject<ApprovalLine>
                                                                            (new GroupOperator(GroupOperatorType.And,
                                                                             new BinaryOperator("PurchaseInvoice", _locPurchaseInvoiceXPO),
                                                                             new BinaryOperator("ApprovalLevel", ApprovalLevel.Level3)));

                                        if (_locApprovalLineXPO == null)
                                        {
                                            if (_locAppSetDetail.EndApproval == true)
                                            {
                                                ApprovalLine _saveDataAL = new ApprovalLine(_currentSession)
                                                {
                                                    ApprovalDate = now,
                                                    ApprovalStatus = Status.Approved,
                                                    ApprovalLevel = ApprovalLevel.Level3,
                                                    EndApproval = true,
                                                    PurchaseInvoice = _locPurchaseInvoiceXPO,
                                                };
                                                _saveDataAL.Save();
                                                _saveDataAL.Session.CommitTransaction();

                                                _locPurchaseInvoiceXPO.ActivationPosting = true;
                                                _locPurchaseInvoiceXPO.ActiveApproved1 = false;
                                                _locPurchaseInvoiceXPO.ActiveApproved2 = false;
                                                _locPurchaseInvoiceXPO.ActiveApproved3 = true;
                                                _locPurchaseInvoiceXPO.Save();
                                                _locPurchaseInvoiceXPO.Session.CommitTransaction();
                                            }
                                            else
                                            {
                                                ApprovalLine _saveDataAL = new ApprovalLine(_currentSession)
                                                {
                                                    ApprovalDate = now,
                                                    ApprovalStatus = Status.Approved,
                                                    ApprovalLevel = ApprovalLevel.Level3,
                                                    PurchaseInvoice = _locPurchaseInvoiceXPO,
                                                };
                                                _saveDataAL.Save();
                                                _saveDataAL.Session.CommitTransaction();

                                                SetApprovalLine(_currentSession, _locPurchaseInvoiceXPO, ApprovalLevel.Level2);
                                                SetApprovalLine(_currentSession, _locPurchaseInvoiceXPO, ApprovalLevel.Level1);

                                                _locPurchaseInvoiceXPO.ActiveApproved1 = false;
                                                _locPurchaseInvoiceXPO.ActiveApproved2 = false;
                                                _locPurchaseInvoiceXPO.ActiveApproved3 = true;
                                                _locPurchaseInvoiceXPO.Save();
                                                _locPurchaseInvoiceXPO.Session.CommitTransaction();
                                            }

                                            //Send Email
                                            ApprovalLine _locApprovalLineXPO2 = _currentSession.FindObject<ApprovalLine>
                                                                                (new GroupOperator(GroupOperatorType.And,
                                                                                 new BinaryOperator("PurchaseInvoice", _locPurchaseInvoiceXPO)));

                                            if (_locApprovalLineXPO2 != null)
                                            {
                                                SendEmail(_currentSession, _locPurchaseInvoiceXPO, Status.Lock, _locApprovalLineXPO2);
                                            }

                                            SuccessMessageShow("Purchase Invoice has successfully Approve");
                                        }
                                    }
                                }
                                #endregion Approval Level 3
                            }
                        }
                        else
                        {
                            ErrorMessageShow("Purchase Invoice Not Available");
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PurchaseInvoice " + ex.ToString());
            }
        }

        private void PurchaseInvoiceProgressAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;
                Status _locStatus = Status.Open;

                if (_objectsToProcess != null)
                {
                    foreach (Object obj in _objectsToProcess)
                    {
                        PurchaseInvoice _locPurchaseInvoiceOS = (PurchaseInvoice)_objectSpace.GetObject(obj);

                        if (_locPurchaseInvoiceOS != null)
                        {
                            if (_locPurchaseInvoiceOS.Session != null)
                            {
                                _currSession = _locPurchaseInvoiceOS.Session;
                            }

                            if (_locPurchaseInvoiceOS.Code != null && _currSession != null)
                            {
                                _currObjectId = _locPurchaseInvoiceOS.Code;

                                PurchaseInvoice _locPurchaseInvoiceXPO = _currSession.FindObject<PurchaseInvoice>
                                                                   (new GroupOperator(GroupOperatorType.And,
                                                                    new BinaryOperator("Code", _currObjectId),
                                                                    new GroupOperator(GroupOperatorType.Or,
                                                                    new BinaryOperator("Status", Status.Open),
                                                                    new BinaryOperator("Status", Status.Progress))));

                                if (_locPurchaseInvoiceXPO != null)
                                {
                                    ApprovalLine _locApprovalLine = _currSession.FindObject<ApprovalLine>
                                                                            (new GroupOperator(GroupOperatorType.And,
                                                                             new BinaryOperator("PurchaseInvoice", _locPurchaseInvoiceXPO),
                                                                             new BinaryOperator("EndApproval", true)));
                                    if(_locApprovalLine != null)
                                    {
                                        if (_locPurchaseInvoiceXPO.Status == Status.Open)
                                        {
                                            _locStatus = Status.Progress;
                                        }
                                        else
                                        {
                                            _locStatus = _locPurchaseInvoiceXPO.Status;
                                        }

                                        _locPurchaseInvoiceXPO.Status = _locStatus;
                                        _locPurchaseInvoiceXPO.StatusDate = now;
                                        _locPurchaseInvoiceXPO.Save();
                                        _locPurchaseInvoiceXPO.Session.CommitTransaction();

                                        #region PurchaseInvoiceLine
                                        XPCollection<PurchaseInvoiceLine> _locPurchaseInvoiceLines = new XPCollection<PurchaseInvoiceLine>
                                                                                               (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                new BinaryOperator("PurchaseInvoice", _locPurchaseInvoiceXPO),
                                                                                                new BinaryOperator("Status", Status.Open)));

                                        if (_locPurchaseInvoiceLines != null && _locPurchaseInvoiceLines.Count > 0)
                                        {
                                            foreach (PurchaseInvoiceLine _locPurchaseInvoiceLine in _locPurchaseInvoiceLines)
                                            {
                                                _locPurchaseInvoiceLine.Status = Status.Progress;
                                                _locPurchaseInvoiceLine.StatusDate = now;
                                                _locPurchaseInvoiceLine.Save();
                                                _locPurchaseInvoiceLine.Session.CommitTransaction();
                                            }
                                        }
                                        #endregion PurchaseInvoiceLine

                                        #region InvoicePurchaseCollection
                                        {
                                            XPCollection<InvoicePurchaseCollection> _locInvoicePurchaseCollections = new XPCollection<BusinessObjects.InvoicePurchaseCollection>
                                                                                                            (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                            new BinaryOperator("PurchaseInvoice", _locPurchaseInvoiceXPO),
                                                                                                            new BinaryOperator("Status", Status.Open)));
                                            if (_locInvoicePurchaseCollections != null && _locInvoicePurchaseCollections.Count() > 0)
                                            {
                                                foreach (InvoicePurchaseCollection _locInvoicePurchaseCollection in _locInvoicePurchaseCollections)
                                                {
                                                    _locInvoicePurchaseCollection.Status = Status.Progress;
                                                    _locInvoicePurchaseCollection.StatusDate = now;
                                                    _locInvoicePurchaseCollection.Save();
                                                    _locInvoicePurchaseCollection.Session.CommitTransaction();
                                                }
                                            }
                                        }
                                        #endregion InvoicePurchaseCollection

                                        SetAmountAndTotalAmountInPI(_currSession, _locPurchaseInvoiceXPO);
                                        SuccessMessageShow(_locPurchaseInvoiceXPO.Code + " has been change successfully to Progress ");
                                    }                       
                                }
                                else
                                {
                                    ErrorMessageShow("Data Purchase Invoice Not Available");
                                }
                            }
                            else
                            {
                                ErrorMessageShow("Data Purchase Invoice Not Available");
                            }
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PurchaseInvoice " + ex.ToString());
            }
        }

        private void PurchaseInvoicePostingAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                //Mengirim informasi ke Payable Transaction --HutangDagang Dan --BankAccount Perusahaan yg aktif default
                //Meng-akumulasi semua hal perhitungan lokal Purchase Invoice termasuk close status

                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;

                if (_objectsToProcess != null)
                {
                    foreach (Object obj in _objectsToProcess)
                    {
                        PurchaseInvoice _locPurchaseInvoiceOS = (PurchaseInvoice)_objectSpace.GetObject(obj);

                        if (_locPurchaseInvoiceOS != null)
                        {
                            if (_locPurchaseInvoiceOS.Session != null)
                            {
                                _currSession = _locPurchaseInvoiceOS.Session;
                            }

                            if (_locPurchaseInvoiceOS.Code != null && _currSession != null)
                            {
                                _currObjectId = _locPurchaseInvoiceOS.Code;

                                PurchaseInvoice _locPurchaseInvoiceXPO = _currSession.FindObject<PurchaseInvoice>
                                                                   (new GroupOperator(GroupOperatorType.And,
                                                                    new BinaryOperator("Code", _currObjectId),
                                                                    new GroupOperator(GroupOperatorType.Or,
                                                                    new BinaryOperator("Status", Status.Progress),
                                                                    new BinaryOperator("Status", Status.Posted))));

                                if (_locPurchaseInvoiceXPO != null)
                                {
                                    ApprovalLine _locApprovalLineXPO = _currSession.FindObject<ApprovalLine>
                                                                           (new GroupOperator(GroupOperatorType.And,
                                                                            new BinaryOperator("EndApproval", true),
                                                                            new BinaryOperator("PurchaseInvoice", _locPurchaseInvoiceXPO)));

                                    if (_locApprovalLineXPO != null)
                                    {
                                        if (_locPurchaseInvoiceXPO.Amount > 0)
                                        {
                                            //Berdasarkan Outstanding dari PaymentOutPlan yg mana di hitung berdasarkan jumlah mak Outstanding 
                                            if (_locPurchaseInvoiceXPO.Amount == GetPurchaseInvoiceLineTotalAmount(_currSession, _locPurchaseInvoiceXPO))
                                            {
                                                SetPurchaseInvoiceMonitoring(_currSession, _locPurchaseInvoiceXPO);
                                                if (CheckJournalSetup(_currSession, _locPurchaseInvoiceXPO) == true)
                                                {
                                                    SetReverseGoodsReceiveJournal(_currSession, _locPurchaseInvoiceXPO);
                                                    SetInvoiceAPJournal(_currSession, _locPurchaseInvoiceXPO);
                                                }
                                                SetStatusPurchaseInvoiceLine(_currSession, _locPurchaseInvoiceXPO);
                                                SetNormalQuantityInPostPIM(_currSession, _locPurchaseInvoiceXPO);
                                                SetFinalPurchaseInvoice(_currSession, _locPurchaseInvoiceXPO);
                                                SuccessMessageShow(_locPurchaseInvoiceXPO.Code + " has been change successfully to Posted");
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    ErrorMessageShow("Data Purchase Invoice Not Available");
                                }
                            }
                            else
                            {
                                ErrorMessageShow("Data Purchase Invoice Not Available");
                            }
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PurchaseInvoice " + ex.ToString());
            }
        }      

        private void PurchaseInvoiceListviewFilterSelectionAction_Execute(object sender, SingleChoiceActionExecuteEventArgs e)
        {
            try
            {
                if ((View is ListView) & (View.ObjectTypeInfo.Type == typeof(PurchaseInvoice)))
                {
                    if ((Status)e.SelectedChoiceActionItem.Data == Status.None)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.None, BinaryOperatorType.NotEqual);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Open)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Open, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Progress)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Progress, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Posted)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Posted, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Lock)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Lock, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Close)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Close, BinaryOperatorType.Equal);
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PurchaseInvoice " + ex.ToString());
            }
        }

        //========================================== Code In Here ==========================================

        #region GetITI

        private void GetInventoryTransferInMonitoring(Session _currSession, InventoryTransferIn _locInventoryTransferInXPO, PurchaseInvoice _locPurchaseInvoiceXPO)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                DateTime now = DateTime.Now;
                string _currSignCode = null;
                double _locTxValue = 0;
                double _locTxAmount = 0;
                double _locDisc = 0;
                double _locDiscAmount = 0;
                double _locTAmount = 0;
                DateTime _locDocDate = now;
                int _locJournalMonth = 0;
                int _locJournalYear = 0;

                if (_locInventoryTransferInXPO != null && _locPurchaseInvoiceXPO != null)
                {
                    //Harus mampu menghitung otomatis buat MultiTax dan MultiDiscount
                    XPCollection<InventoryTransferInMonitoring> _locInventoryTransferInMonitorings = new XPCollection<InventoryTransferInMonitoring>
                                                                                        (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                        new BinaryOperator("InventoryTransferIn", _locInventoryTransferInXPO),
                                                                                        new BinaryOperator("PurchaseInvoice", _locPurchaseInvoiceXPO),
                                                                                        new BinaryOperator("Select", true),
                                                                                        new GroupOperator(GroupOperatorType.Or,
                                                                                        new BinaryOperator("Status", Status.Open),
                                                                                        new BinaryOperator("Status", Status.Posted))));

                    if (_locInventoryTransferInMonitorings != null && _locInventoryTransferInMonitorings.Count() > 0)
                    {
                        foreach (InventoryTransferInMonitoring _locInventoryTransferInMonitoring in _locInventoryTransferInMonitorings)
                        {
                            if (_locInventoryTransferInMonitoring.InventoryTransferInLine != null && _locInventoryTransferInMonitoring.PurchaseOrderMonitoring != null
                                && _locInventoryTransferInMonitoring.PurchaseOrderMonitoring.PurchaseOrderLine != null)
                            {
                                _currSignCode = _globFunc.GetNumberingSignUnlockOptimisticRecordBasedOrganization(_currSession.DataLayer, ObjectList.PurchaseInvoiceLine, _locPurchaseInvoiceXPO.Company, _locPurchaseInvoiceXPO.Workplace);
                                
                                if (_currSignCode != null)
                                {
                                    //Count Tax
                                    #region AfterDiscount
                                    if (_locInventoryTransferInMonitoring.PurchaseOrderMonitoring.MultiTax == true && _locInventoryTransferInMonitoring.PurchaseOrderMonitoring.TaxRule == TaxRule.AfterDiscount)
                                    {
                                        _locTxValue = GetSumTotalTaxValueAfterDiscount(_currSession, _locInventoryTransferInMonitoring.PurchaseOrderMonitoring.PurchaseOrderLine);
                                        _locTxAmount = GetSumTotalTaxAmountAfterDiscount(_currSession, _locInventoryTransferInMonitoring.PurchaseOrderMonitoring.PurchaseOrderLine, _locInventoryTransferInMonitoring);
                                    }
                                    if (_locInventoryTransferInMonitoring.PurchaseOrderMonitoring.MultiTax == false && _locInventoryTransferInMonitoring.PurchaseOrderMonitoring.TaxRule == TaxRule.AfterDiscount)
                                    {
                                        if (_locInventoryTransferInMonitoring.PurchaseOrderMonitoring.TxValue > 0)
                                        {
                                            _locTxValue = _locInventoryTransferInMonitoring.PurchaseOrderMonitoring.PurchaseOrderLine.TxValue;
                                            _locTxAmount = (_locTxValue / 100) * (_locInventoryTransferInMonitoring.PurchaseOrderMonitoring.TUAmount - _locInventoryTransferInMonitoring.PurchaseOrderMonitoring.DiscAmount);
                                        }
                                    }
                                    #endregion AfterDiscount

                                    #region BeforeDiscount
                                    if (_locInventoryTransferInMonitoring.PurchaseOrderMonitoring.MultiTax == true && _locInventoryTransferInMonitoring.PurchaseOrderMonitoring.TaxRule == TaxRule.BeforeDiscount
                                        || _locInventoryTransferInMonitoring.PurchaseOrderMonitoring.TaxRule == TaxRule.None)
                                    {
                                        _locTxValue = GetSumTotalTaxValueBeforeDiscount(_currSession, _locInventoryTransferInMonitoring.PurchaseOrderMonitoring.PurchaseOrderLine);
                                        _locTxAmount = GetSumTotalTaxAmountBeforeDiscount(_currSession, _locInventoryTransferInMonitoring.PurchaseOrderMonitoring.PurchaseOrderLine, _locInventoryTransferInMonitoring);
                                    }
                                    if (_locInventoryTransferInMonitoring.PurchaseOrderMonitoring.MultiTax == false && _locInventoryTransferInMonitoring.PurchaseOrderMonitoring.TaxRule == TaxRule.BeforeDiscount
                                        || _locInventoryTransferInMonitoring.PurchaseOrderMonitoring.TaxRule == TaxRule.None)
                                    {
                                        if (_locInventoryTransferInMonitoring.PurchaseOrderMonitoring.TxValue > 0)
                                        {
                                            _locTxValue = _locInventoryTransferInMonitoring.PurchaseOrderMonitoring.PurchaseOrderLine.TxValue;
                                            _locTxAmount = (_locTxValue / 100) * (_locInventoryTransferInMonitoring.TQty * _locInventoryTransferInMonitoring.PurchaseOrderMonitoring.UAmount);
                                        }
                                    }
                                    #endregion BeforeDiscount

                                    //Count Discount
                                    if (_locInventoryTransferInMonitoring.PurchaseOrderMonitoring.Discount != null && _locInventoryTransferInMonitoring.PurchaseOrderMonitoring.Disc > 0)
                                    {
                                        _locDisc = _locInventoryTransferInMonitoring.PurchaseOrderMonitoring.PurchaseOrderLine.Disc;
                                        _locDiscAmount = (_locDisc / 100) * (_locInventoryTransferInMonitoring.TQty * _locInventoryTransferInMonitoring.PurchaseOrderMonitoring.UAmount);
                                    }

                                    //Count TAmount
                                    _locTAmount = (_locInventoryTransferInMonitoring.PurchaseOrderMonitoring.UAmount * _locInventoryTransferInMonitoring.TQty) + _locTxAmount - _locDiscAmount;

                                    #region SavePurchaseInvoiceLine
                                    PurchaseInvoiceLine _saveDataPurchaseInvoiceLine = new PurchaseInvoiceLine(_currSession)
                                    {
                                        SignCode = _currSignCode,
                                        PurchaseType = _locInventoryTransferInMonitoring.PurchaseOrderMonitoring.PurchaseOrderLine.PurchaseType,
                                        Item = _locInventoryTransferInMonitoring.Item,
                                        Brand = _locInventoryTransferInMonitoring.InventoryTransferInLine.Brand,
                                        Description = _locInventoryTransferInMonitoring.InventoryTransferInLine.Description,
                                        DQty = _locInventoryTransferInMonitoring.DQty,
                                        DUOM = _locInventoryTransferInMonitoring.DUOM,
                                        Qty = _locInventoryTransferInMonitoring.Qty,
                                        UOM = _locInventoryTransferInMonitoring.UOM,
                                        TQty = _locInventoryTransferInMonitoring.TQty,
                                        Currency = _locInventoryTransferInMonitoring.PurchaseOrderMonitoring.Currency,
                                        UAmount = _locInventoryTransferInMonitoring.PurchaseOrderMonitoring.UAmount,
                                        TUAmount = _locInventoryTransferInMonitoring.TQty * _locInventoryTransferInMonitoring.PurchaseOrderMonitoring.UAmount,
                                        TaxRule = _locInventoryTransferInMonitoring.PurchaseOrderMonitoring.TaxRule,
                                        MultiTax = _locInventoryTransferInMonitoring.PurchaseOrderMonitoring.MultiTax,
                                        Tax = _locInventoryTransferInMonitoring.PurchaseOrderMonitoring.Tax,
                                        TxValue = _locTxValue,
                                        TxAmount = _locTxAmount,
                                        Discount = _locInventoryTransferInMonitoring.PurchaseOrderMonitoring.Discount,
                                        Disc = _locDisc,
                                        DiscAmount = _locDiscAmount,
                                        TAmount = _locTAmount,
                                        Company = _locInventoryTransferInMonitoring.Company,
                                        Workplace = _locInventoryTransferInMonitoring.Workplace,
                                        InventoryTransferInMonitoring = _locInventoryTransferInMonitoring,
                                        PurchaseOrderMonitoring = _locInventoryTransferInMonitoring.PurchaseOrderMonitoring,
                                        PurchaseInvoice = _locPurchaseInvoiceXPO,
                                        DocDate = _locInventoryTransferInMonitoring.DocDate,
                                        JournalMonth = _locInventoryTransferInMonitoring.JournalMonth,
                                        JournalYear = _locInventoryTransferInMonitoring.JournalYear,
                                    };
                                    _saveDataPurchaseInvoiceLine.Save();
                                    _saveDataPurchaseInvoiceLine.Session.CommitTransaction();
                                    #endregion SavePurchaseInvoiceLine

                                    _locDocDate = _locInventoryTransferInMonitoring.DocDate;
                                    _locJournalMonth = _locInventoryTransferInMonitoring.JournalMonth;
                                    _locJournalYear = _locInventoryTransferInMonitoring.JournalYear;

                                    PurchaseInvoiceLine _locPurchaseInvoiceLine = _currSession.FindObject<PurchaseInvoiceLine>
                                                                (new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("SignCode", _currSignCode)));
                                    if (_locPurchaseInvoiceLine != null)
                                    {
                                        #region TaxLineAfterDiscount
                                        if (_locInventoryTransferInMonitoring.PurchaseOrderMonitoring.PurchaseOrderLine.MultiTax == true && _locInventoryTransferInMonitoring.PurchaseOrderMonitoring.PurchaseOrderLine.TaxRule == TaxRule.AfterDiscount)
                                        {
                                            XPCollection<TaxLine> _locTaxLines = new XPCollection<TaxLine>(_currSession, new GroupOperator(
                                                                                GroupOperatorType.And,
                                                                                new BinaryOperator("PurchaseOrderLine", _locInventoryTransferInMonitoring.PurchaseOrderMonitoring.PurchaseOrderLine)));
                                            if (_locTaxLines != null && _locTaxLines.Count() > 0)
                                            {
                                                foreach (TaxLine _locTaxLine in _locTaxLines)
                                                {
                                                    TaxLine _saveDataTaxLine = new TaxLine(_currSession)
                                                    {
                                                        Tax = _locTaxLine.Tax,
                                                        Name = _locTaxLine.Name,
                                                        TaxType = _locTaxLine.TaxType,
                                                        TaxGroup = _locTaxLine.TaxGroup,
                                                        TaxGroupType = _locTaxLine.TaxGroupType,
                                                        TaxAccountGroup = _locTaxLine.TaxAccountGroup,
                                                        TaxNature = _locTaxLine.TaxNature,
                                                        TaxMode = _locTaxLine.TaxMode,
                                                        TxValue = _locTaxLine.TxValue,
                                                        Description = _locTaxLine.Description,
                                                        UAmount = _locPurchaseInvoiceLine.UAmount,
                                                        TUAmount = _locPurchaseInvoiceLine.TUAmount,
                                                        //TxAmount = _locTaxLine.TxAmount,
                                                        TxAmount = ((_locPurchaseInvoiceLine.TUAmount - _locPurchaseInvoiceLine.DiscAmount) * (_locTaxLine.TxValue / 100)),
                                                        PurchaseInvoiceLine = _locPurchaseInvoiceLine,
                                                    };
                                                    _saveDataTaxLine.Save();
                                                    _saveDataTaxLine.Session.CommitTransaction();
                                                }
                                            }
                                        }
                                        #endregion TaxLineAfterDiscount

                                        #region TaxLineBeforeDiscountOrNone
                                        if ((_locInventoryTransferInMonitoring.PurchaseOrderMonitoring.PurchaseOrderLine.MultiTax == true) && 
                                            (_locInventoryTransferInMonitoring.PurchaseOrderMonitoring.PurchaseOrderLine.TaxRule == TaxRule.BeforeDiscount ||
                                             _locInventoryTransferInMonitoring.PurchaseOrderMonitoring.PurchaseOrderLine.TaxRule == TaxRule.None))
                                        {
                                            XPCollection<TaxLine> _locTaxLines = new XPCollection<TaxLine>(_currSession, new GroupOperator(
                                                                                GroupOperatorType.And,
                                                                                new BinaryOperator("PurchaseOrderLine", _locInventoryTransferInMonitoring.PurchaseOrderMonitoring.PurchaseOrderLine)));
                                            if (_locTaxLines != null && _locTaxLines.Count() > 0)
                                            {
                                                foreach (TaxLine _locTaxLine in _locTaxLines)
                                                {
                                                    TaxLine _saveDataTaxLine = new TaxLine(_currSession)
                                                    {
                                                        Tax = _locTaxLine.Tax,
                                                        Name = _locTaxLine.Name,
                                                        TaxType = _locTaxLine.TaxType,
                                                        TaxGroup = _locTaxLine.TaxGroup,
                                                        TaxGroupType = _locTaxLine.TaxGroupType,
                                                        TaxAccountGroup = _locTaxLine.TaxAccountGroup,
                                                        TaxNature = _locTaxLine.TaxNature,
                                                        TaxMode = _locTaxLine.TaxMode,
                                                        TxValue = _locTaxLine.TxValue,
                                                        Description = _locTaxLine.Description,
                                                        UAmount = _locPurchaseInvoiceLine.UAmount,
                                                        TUAmount = _locPurchaseInvoiceLine.TUAmount,
                                                        //TxAmount = _locTaxLine.TxAmount,
                                                        TxAmount = ((_locPurchaseInvoiceLine.TUAmount - _locPurchaseInvoiceLine.DiscAmount) * (_locTaxLine.TxValue / 100)),
                                                        PurchaseInvoiceLine = _locPurchaseInvoiceLine,
                                                    };
                                                    _saveDataTaxLine.Save();
                                                    _saveDataTaxLine.Session.CommitTransaction();
                                                }
                                            }
                                        }
                                        #endregion TaxLineBeforeDiscountOrNone

                                        SetRemainQty(_currSession, _locInventoryTransferInMonitoring);
                                        SetPostingQty(_currSession, _locInventoryTransferInMonitoring);
                                        SetProcessCount(_currSession, _locInventoryTransferInMonitoring);
                                        SetStatusInventoryTransferInMonitoring(_currSession, _locInventoryTransferInMonitoring);
                                        SetNormalQuantity(_currSession, _locInventoryTransferInMonitoring);
                                    }
                                }
                            }
                        }
                        _locPurchaseInvoiceXPO.DocDate = _locDocDate;
                        _locPurchaseInvoiceXPO.JournalMonth = _locJournalMonth;
                        _locPurchaseInvoiceXPO.JournalYear = _locJournalYear;
                        _locPurchaseInvoiceXPO.Save();
                        _locPurchaseInvoiceXPO.Session.CommitTransaction();
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = PurchaseInvoice ", ex.ToString());
            }
        }

        #region GetTotalTax

        private double GetSumTotalTaxValueAfterDiscount(Session _currSession, PurchaseOrderLine _purchaseOrderLineXPO)
        {
            double _result = 0;
            try
            {
                double _locTxValue = 0;

                if (_purchaseOrderLineXPO != null)
                {
                    XPCollection<TaxLine> _locTaxLines = new XPCollection<TaxLine>(_currSession,
                                                     new BinaryOperator("PurchaseOrderLine", _purchaseOrderLineXPO));

                    if (_locTaxLines.Count() > 0)
                    {
                        foreach (TaxLine _locTaxLine in _locTaxLines)
                        {
                            if (_locTaxLine.TxValue > 0)
                            {
                                if (_locTaxLine.TaxNature == TaxNature.Increase)
                                {
                                    _locTxValue = _locTxValue + _locTaxLine.TxValue;
                                }
                                else if (_locTaxLine.TaxNature == TaxNature.Decrease)
                                {
                                    _locTxValue = _locTxValue - _locTaxLine.TxValue;
                                }
                            }
                        }
                        _result = _locTxValue;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("BusinessObject = PurchaseInvoice " + ex.ToString());
            }
            return _result;
        }

        private double GetSumTotalTaxAmountAfterDiscount(Session _currSession, PurchaseOrderLine _purchaseOrderLineXPO, InventoryTransferInMonitoring _inventoryTransferInMonitoringXPO)
        {
            double _result = 0;
            try
            {
                double _locTxAmount = 0;

                if (_purchaseOrderLineXPO != null)
                {
                    XPCollection<TaxLine> _locTaxLines = new XPCollection<TaxLine>(_currSession,
                                                     new BinaryOperator("PurchaseOrderLine", _purchaseOrderLineXPO));

                    if (_locTaxLines.Count() > 0)
                    {
                        foreach (TaxLine _locTaxLine in _locTaxLines)
                        {
                            if (_locTaxLine.TxValue > 0)
                            {
                                if (_locTaxLine.TaxNature == TaxNature.Increase)
                                {
                                    _locTxAmount = _locTxAmount + _locTaxLine.TxValue / 100 * (_purchaseOrderLineXPO.TUAmount - _purchaseOrderLineXPO.DiscAmount);
                                }
                                else if (_locTaxLine.TaxNature == TaxNature.Decrease)
                                {
                                    _locTxAmount = _locTxAmount - _locTaxLine.TxValue / 100 * (_purchaseOrderLineXPO.TUAmount - _purchaseOrderLineXPO.DiscAmount);
                                }
                            }
                        }
                        _result = _locTxAmount;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("BusinessObject = PurchaseInvoice" + ex.ToString());
            }
            return _result;
        }

        private double GetSumTotalTaxValueBeforeDiscount(Session _currSession, PurchaseOrderLine _purchaseOrderLineXPO)
        {
            double _result = 0;
            try
            {
                double _locTxValue = 0;

                if (_purchaseOrderLineXPO != null)
                {
                    XPCollection<TaxLine> _locTaxLines = new XPCollection<TaxLine>(_currSession,
                                                     new BinaryOperator("PurchaseOrderLine", _purchaseOrderLineXPO));

                    if (_locTaxLines.Count() > 0)
                    {
                        foreach (TaxLine _locTaxLine in _locTaxLines)
                        {
                            if (_locTaxLine.TxValue > 0 && _locTaxLine.TxAmount > 0)
                            {
                                if (_locTaxLine.TaxNature == TaxNature.Increase)
                                {
                                    _locTxValue = _locTxValue + _locTaxLine.TxValue;
                                }
                                else if (_locTaxLine.TaxNature == TaxNature.Decrease)
                                {
                                    _locTxValue = _locTxValue - _locTaxLine.TxValue;
                                }
                            }
                        }
                        _result = _locTxValue;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("BusinessObject = PurchaseInvoice " + ex.ToString());
            }
            return _result;
        }

        private double GetSumTotalTaxAmountBeforeDiscount(Session _currSession, PurchaseOrderLine _purchaseOrderLineXPO, InventoryTransferInMonitoring _inventoryTransferInMonitoringXPO)
        {
            double _result = 0;
            try
            {
                double _locTxAmount = 0;

                if (_purchaseOrderLineXPO != null)
                {
                    XPCollection<TaxLine> _locTaxLines = new XPCollection<TaxLine>(_currSession,
                                                     new BinaryOperator("PurchaseOrderLine", _purchaseOrderLineXPO));

                    if (_locTaxLines.Count() > 0)
                    {
                        foreach (TaxLine _locTaxLine in _locTaxLines)
                        {
                            if (_locTaxLine.TxValue > 0)
                            {
                                if (_locTaxLine.TaxNature == TaxNature.Increase)
                                {
                                    _locTxAmount = _locTxAmount + (_purchaseOrderLineXPO.UAmount * _inventoryTransferInMonitoringXPO.TQty * _locTaxLine.TxValue / 100);
                                }
                                else if (_locTaxLine.TaxNature == TaxNature.Decrease)
                                {
                                    _locTxAmount = _locTxAmount - (_purchaseOrderLineXPO.UAmount * _inventoryTransferInMonitoringXPO.TQty * _locTaxLine.TxValue / 100);
                                }
                            }
                        }
                        _result = _locTxAmount;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("BusinessObject = PurchaseInvoice" + ex.ToString());
            }
            return _result;
        }

        #endregion GetTotalTax

        #region Set
        private void SetRemainQty(Session _currSession, InventoryTransferInMonitoring _locInventoryTransferInMonitoringXPO)
        {
            try
            {
                if (_locInventoryTransferInMonitoringXPO != null)
                {
                    double _locRmDQty = 0;
                    double _locRmQty = 0;
                    double _locInvLineTotal = 0;
                    ItemUnitOfMeasure _locItemUOM = null;

                    #region ProcessCount=0
                    if (_locInventoryTransferInMonitoringXPO.PostedCount == 0)
                    {
                        #region MaxQuantity
                        if (_locInventoryTransferInMonitoringXPO.MxDQty > 0)
                        {
                            if (_locInventoryTransferInMonitoringXPO.DQty > 0 && _locInventoryTransferInMonitoringXPO.DQty <= _locInventoryTransferInMonitoringXPO.MxDQty)
                            {
                                _locRmDQty = _locInventoryTransferInMonitoringXPO.MxDQty - _locInventoryTransferInMonitoringXPO.DQty;
                            }

                            if (_locInventoryTransferInMonitoringXPO.Qty > 0 && _locInventoryTransferInMonitoringXPO.Qty <= _locInventoryTransferInMonitoringXPO.MxQty)
                            {
                                _locRmQty = _locInventoryTransferInMonitoringXPO.MxQty - _locInventoryTransferInMonitoringXPO.Qty;
                            }


                            _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                       (new GroupOperator(GroupOperatorType.And,
                                                        new BinaryOperator("Item", _locInventoryTransferInMonitoringXPO.Item),
                                                        new BinaryOperator("UOM", _locInventoryTransferInMonitoringXPO.MxUOM),
                                                        new BinaryOperator("DefaultUOM", _locInventoryTransferInMonitoringXPO.MxDUOM),
                                                        new BinaryOperator("Active", true)));
                            if (_locItemUOM != null)
                            {
                                if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                {
                                    _locInvLineTotal = _locRmQty * _locItemUOM.DefaultConversion + _locRmDQty;
                                }
                                else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                {
                                    _locInvLineTotal = _locRmQty / _locItemUOM.Conversion + _locRmDQty;
                                }
                                else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                {
                                    _locInvLineTotal = _locRmQty + _locRmDQty;
                                }
                            }
                            else
                            {
                                _locInvLineTotal = _locRmQty + _locRmDQty;
                            }

                        }
                        #endregion MaxQuantity
                        #region NonMaxQuantity
                        else
                        {
                            if (_locInventoryTransferInMonitoringXPO.DQty > 0)
                            {
                                _locRmDQty = 0;
                            }

                            if (_locInventoryTransferInMonitoringXPO.Qty > 0)
                            {
                                _locRmQty = 0;
                            }


                            _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                       (new GroupOperator(GroupOperatorType.And,
                                                        new BinaryOperator("Item", _locInventoryTransferInMonitoringXPO.Item),
                                                        new BinaryOperator("UOM", _locInventoryTransferInMonitoringXPO.UOM),
                                                        new BinaryOperator("DefaultUOM", _locInventoryTransferInMonitoringXPO.DUOM),
                                                        new BinaryOperator("Active", true)));
                            if (_locItemUOM != null)
                            {
                                if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                {
                                    _locInvLineTotal = _locRmQty * _locItemUOM.DefaultConversion + _locRmDQty;
                                }
                                else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                {
                                    _locInvLineTotal = _locRmQty / _locItemUOM.Conversion + _locRmDQty;
                                }
                                else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                {
                                    _locInvLineTotal = _locRmQty + _locRmDQty;
                                }
                            }
                            else
                            {
                                _locInvLineTotal = _locRmQty + _locRmDQty;
                            }
                        }
                        #endregion NonMaxQuantity
                    }
                    #endregion ProcessCount=0

                    #region ProcessCount>0
                    if (_locInventoryTransferInMonitoringXPO.PostedCount > 0)
                    {
                        if (_locInventoryTransferInMonitoringXPO.RmDQty > 0)
                        {
                            _locRmDQty = _locInventoryTransferInMonitoringXPO.RmDQty - _locInventoryTransferInMonitoringXPO.DQty;
                        }

                        if (_locInventoryTransferInMonitoringXPO.RmQty > 0)
                        {
                            _locRmQty = _locInventoryTransferInMonitoringXPO.RmQty - _locInventoryTransferInMonitoringXPO.Qty;
                        }

                        if (_locInventoryTransferInMonitoringXPO.MxDQty > 0 || _locInventoryTransferInMonitoringXPO.MxQty > 0)
                        {
                            _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                       (new GroupOperator(GroupOperatorType.And,
                                                        new BinaryOperator("Item", _locInventoryTransferInMonitoringXPO.Item),
                                                        new BinaryOperator("UOM", _locInventoryTransferInMonitoringXPO.MxUOM),
                                                        new BinaryOperator("DefaultUOM", _locInventoryTransferInMonitoringXPO.MxDUOM),
                                                        new BinaryOperator("Active", true)));
                        }
                        else
                        {
                            _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                       (new GroupOperator(GroupOperatorType.And,
                                                        new BinaryOperator("Item", _locInventoryTransferInMonitoringXPO.Item),
                                                        new BinaryOperator("UOM", _locInventoryTransferInMonitoringXPO.UOM),
                                                        new BinaryOperator("DefaultUOM", _locInventoryTransferInMonitoringXPO.DUOM),
                                                        new BinaryOperator("Active", true)));
                        }


                        if (_locItemUOM != null)
                        {
                            if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                            {
                                _locInvLineTotal = _locRmQty * _locItemUOM.DefaultConversion + _locRmDQty;
                            }
                            else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                            {
                                _locInvLineTotal = _locRmQty / _locItemUOM.Conversion + _locRmDQty;
                            }
                            else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                            {
                                _locInvLineTotal = _locRmQty + _locRmDQty;
                            }
                        }
                        else
                        {
                            _locInvLineTotal = _locRmQty + _locRmDQty;
                        }

                    }
                    #endregion ProcessCount>0

                    _locInventoryTransferInMonitoringXPO.RmDQty = _locRmDQty;
                    _locInventoryTransferInMonitoringXPO.RmQty = _locRmQty;
                    _locInventoryTransferInMonitoringXPO.RmTQty = _locInvLineTotal;
                    _locInventoryTransferInMonitoringXPO.Save();
                    _locInventoryTransferInMonitoringXPO.Session.CommitTransaction();

                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PurchaseInvoice " + ex.ToString());
            }
        }

        private void SetPostingQty(Session _currSession, InventoryTransferInMonitoring _locInventoryTransferInMonitoringXPO)
        {
            try
            {
                if (_locInventoryTransferInMonitoringXPO != null)
                {
                    double _locPDQty = 0;
                    double _locPQty = 0;
                    double _locInvLineTotal = 0;
                    ItemUnitOfMeasure _locItemUOM = null;

                    #region ProcessCount=0
                    if (_locInventoryTransferInMonitoringXPO.PostedCount == 0)
                    {
                        #region MaxQuantity
                        if (_locInventoryTransferInMonitoringXPO.MxDQty > 0)
                        {
                            if (_locInventoryTransferInMonitoringXPO.DQty > 0 && _locInventoryTransferInMonitoringXPO.DQty <= _locInventoryTransferInMonitoringXPO.MxDQty)
                            {
                                _locPDQty = _locInventoryTransferInMonitoringXPO.DQty;
                            }

                            if (_locInventoryTransferInMonitoringXPO.Qty > 0 && _locInventoryTransferInMonitoringXPO.Qty <= _locInventoryTransferInMonitoringXPO.MxQty)
                            {
                                _locPQty = _locInventoryTransferInMonitoringXPO.Qty;
                            }


                            _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                       (new GroupOperator(GroupOperatorType.And,
                                                        new BinaryOperator("Item", _locInventoryTransferInMonitoringXPO.Item),
                                                        new BinaryOperator("UOM", _locInventoryTransferInMonitoringXPO.MxUOM),
                                                        new BinaryOperator("DefaultUOM", _locInventoryTransferInMonitoringXPO.MxDUOM),
                                                        new BinaryOperator("Active", true)));
                            if (_locItemUOM != null)
                            {
                                if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                {
                                    _locInvLineTotal = _locPQty * _locItemUOM.DefaultConversion + _locPDQty;
                                }
                                else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                {
                                    _locInvLineTotal = _locPQty / _locItemUOM.Conversion + _locPDQty;
                                }
                                else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                {
                                    _locInvLineTotal = _locPQty + _locPDQty;
                                }
                            }
                            else
                            {
                                _locInvLineTotal = _locPQty + _locPDQty;
                            }

                        }
                        #endregion MaxQuantity
                        #region NonMaxQuantity
                        else
                        {
                            if (_locInventoryTransferInMonitoringXPO.DQty > 0)
                            {
                                _locPDQty = _locInventoryTransferInMonitoringXPO.DQty;
                            }

                            if (_locInventoryTransferInMonitoringXPO.Qty > 0)
                            {
                                _locPQty = _locInventoryTransferInMonitoringXPO.Qty;
                            }


                            _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                       (new GroupOperator(GroupOperatorType.And,
                                                        new BinaryOperator("Item", _locInventoryTransferInMonitoringXPO.Item),
                                                        new BinaryOperator("UOM", _locInventoryTransferInMonitoringXPO.UOM),
                                                        new BinaryOperator("DefaultUOM", _locInventoryTransferInMonitoringXPO.DUOM),
                                                        new BinaryOperator("Active", true)));
                            if (_locItemUOM != null)
                            {
                                if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                {
                                    _locInvLineTotal = _locPQty * _locItemUOM.DefaultConversion + _locPDQty;
                                }
                                else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                {
                                    _locInvLineTotal = _locPQty / _locItemUOM.Conversion + _locPDQty;
                                }
                                else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                {
                                    _locInvLineTotal = _locPQty + _locPDQty;
                                }
                            }
                            else
                            {
                                _locInvLineTotal = _locPQty + _locPDQty;
                            }
                        }
                        #endregion NonMaxQuantity
                    }
                    #endregion ProcessCount=0

                    #region ProcessCount>0
                    if (_locInventoryTransferInMonitoringXPO.PostedCount > 0)
                    {
                        if (_locInventoryTransferInMonitoringXPO.PDQty > 0)
                        {
                            _locPDQty = _locInventoryTransferInMonitoringXPO.PDQty + _locInventoryTransferInMonitoringXPO.DQty;
                        }

                        if (_locInventoryTransferInMonitoringXPO.PQty > 0)
                        {
                            _locPQty = _locInventoryTransferInMonitoringXPO.PQty + _locInventoryTransferInMonitoringXPO.Qty;
                        }

                        if (_locInventoryTransferInMonitoringXPO.MxDQty > 0 || _locInventoryTransferInMonitoringXPO.MxQty > 0)
                        {
                            _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                   (new GroupOperator(GroupOperatorType.And,
                                                    new BinaryOperator("Item", _locInventoryTransferInMonitoringXPO.Item),
                                                    new BinaryOperator("UOM", _locInventoryTransferInMonitoringXPO.MxUOM),
                                                    new BinaryOperator("DefaultUOM", _locInventoryTransferInMonitoringXPO.MxDUOM),
                                                    new BinaryOperator("Active", true)));
                        }
                        else
                        {
                            _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                   (new GroupOperator(GroupOperatorType.And,
                                                    new BinaryOperator("Item", _locInventoryTransferInMonitoringXPO.Item),
                                                    new BinaryOperator("UOM", _locInventoryTransferInMonitoringXPO.UOM),
                                                    new BinaryOperator("DefaultUOM", _locInventoryTransferInMonitoringXPO.DUOM),
                                                    new BinaryOperator("Active", true)));
                        }

                        if (_locItemUOM != null)
                        {
                            if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                            {
                                _locInvLineTotal = _locPQty * _locItemUOM.DefaultConversion + _locPDQty;
                            }
                            else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                            {
                                _locInvLineTotal = _locPQty / _locItemUOM.Conversion + _locPDQty;
                            }
                            else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                            {
                                _locInvLineTotal = _locPQty + _locPDQty;
                            }
                        }
                        else
                        {
                            _locInvLineTotal = _locPQty + _locPDQty;
                        }

                    }
                    #endregion ProcessCount>0

                    _locInventoryTransferInMonitoringXPO.PDQty = _locPDQty;
                    _locInventoryTransferInMonitoringXPO.PDUOM = _locInventoryTransferInMonitoringXPO.DUOM;
                    _locInventoryTransferInMonitoringXPO.PQty = _locPQty;
                    _locInventoryTransferInMonitoringXPO.PUOM = _locInventoryTransferInMonitoringXPO.UOM;
                    _locInventoryTransferInMonitoringXPO.PTQty = _locInvLineTotal;
                    _locInventoryTransferInMonitoringXPO.Save();
                    _locInventoryTransferInMonitoringXPO.Session.CommitTransaction();

                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PurchaseInvoice " + ex.ToString());
            }
        }

        private void SetProcessCount(Session _currSession, InventoryTransferInMonitoring _locInventoryTransferInMonitoringXPO)
        {
            try
            {
                if (_locInventoryTransferInMonitoringXPO != null)
                {
                    if (_locInventoryTransferInMonitoringXPO.Status == Status.Open || _locInventoryTransferInMonitoringXPO.Status == Status.Progress || _locInventoryTransferInMonitoringXPO.Status == Status.Posted)
                    {
                        _locInventoryTransferInMonitoringXPO.PostedCount = _locInventoryTransferInMonitoringXPO.PostedCount + 1;
                        _locInventoryTransferInMonitoringXPO.Save();
                        _locInventoryTransferInMonitoringXPO.Session.CommitTransaction();
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PurchaseInvoice " + ex.ToString());
            }
        }

        private void SetStatusInventoryTransferInMonitoring(Session _currSession, InventoryTransferInMonitoring _locInventoryTransferInMonitoringXPO)
        {
            try
            {
                DateTime now = DateTime.Now;
                if (_locInventoryTransferInMonitoringXPO != null)
                {
                    if (_locInventoryTransferInMonitoringXPO.Status == Status.Open || _locInventoryTransferInMonitoringXPO.Status == Status.Progress || _locInventoryTransferInMonitoringXPO.Status == Status.Posted)
                    {
                        if (_locInventoryTransferInMonitoringXPO.RmDQty == 0 && _locInventoryTransferInMonitoringXPO.RmQty == 0 && _locInventoryTransferInMonitoringXPO.RmTQty == 0)
                        {
                            _locInventoryTransferInMonitoringXPO.Status = Status.Close;
                            _locInventoryTransferInMonitoringXPO.ActivationPosting = true;
                            _locInventoryTransferInMonitoringXPO.StatusDate = now;
                        }
                        else
                        {
                            _locInventoryTransferInMonitoringXPO.Status = Status.Posted;
                            _locInventoryTransferInMonitoringXPO.StatusDate = now;
                        }
                        _locInventoryTransferInMonitoringXPO.Save();
                        _locInventoryTransferInMonitoringXPO.Session.CommitTransaction();
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PurchaseInvoice " + ex.ToString());
            }
        }

        private void SetNormalQuantity(Session _currSession, InventoryTransferInMonitoring _locInventoryTransferInMonitoringXPO)
        {
            try
            {
                if (_locInventoryTransferInMonitoringXPO != null)
                {
                    if (_locInventoryTransferInMonitoringXPO.Status == Status.Open || _locInventoryTransferInMonitoringXPO.Status == Status.Progress || _locInventoryTransferInMonitoringXPO.Status == Status.Posted || _locInventoryTransferInMonitoringXPO.Status == Status.Close)
                    {
                        if (_locInventoryTransferInMonitoringXPO.DQty > 0 || _locInventoryTransferInMonitoringXPO.Qty > 0)
                        {
                            _locInventoryTransferInMonitoringXPO.Select = false;
                            _locInventoryTransferInMonitoringXPO.DQty = 0;
                            _locInventoryTransferInMonitoringXPO.Qty = 0;
                            _locInventoryTransferInMonitoringXPO.PurchaseInvoice = null;
                            _locInventoryTransferInMonitoringXPO.Save();
                            _locInventoryTransferInMonitoringXPO.Session.CommitTransaction();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PurchaseInvoice " + ex.ToString());
            }
        }
        #endregion Set

        #endregion GetITI

        #region GetTotalAmount

        private void SetAmountAndTotalAmountInPI(Session _currSession, PurchaseInvoice _locPurchaseInvoiceXPO)
        {
            try
            {
                double _locTotAmount = 0;
                double _locAmount = 0;

                if (_locPurchaseInvoiceXPO != null)
                {
                    XPCollection<PurchaseInvoiceLine> _locPurchaseInvoiceLines = new XPCollection<BusinessObjects.PurchaseInvoiceLine>
                                                                                (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                new BinaryOperator("PurchaseInvoice", _locPurchaseInvoiceXPO),
                                                                                new GroupOperator(GroupOperatorType.Or,
                                                                                new BinaryOperator("Status", Status.Open),
                                                                                new BinaryOperator("Status", Status.Progress))));
                    if (_locPurchaseInvoiceLines != null && _locPurchaseInvoiceLines.Count() > 0)
                    {
                        foreach (PurchaseInvoiceLine _locPurchaseInvoiceLine in _locPurchaseInvoiceLines)
                        {
                            if (_locPurchaseInvoiceLine.Status == Status.Progress)
                            {
                                _locTotAmount = _locTotAmount + _locPurchaseInvoiceLine.TAmount;
                                _locAmount = _locAmount + _locPurchaseInvoiceLine.TAmount;
                            }
                        }
                    }

                    _locPurchaseInvoiceXPO.TAmount = _locTotAmount;
                    _locPurchaseInvoiceXPO.Amount = _locAmount;
                    _locPurchaseInvoiceXPO.Save();
                    _locPurchaseInvoiceXPO.Session.CommitTransaction();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = PurchaseInvoice ", ex.ToString());
            }
        }

        #endregion GetTotalAmount

        #region Posting Method

        private double GetPurchaseInvoiceLineTotalAmount(Session _currSession, PurchaseInvoice _locPurchaseInvoiceXPO)
        {
            double _result = 0;
            try
            {
                if (_locPurchaseInvoiceXPO != null)
                {
                    XPCollection<PurchaseInvoiceLine> _locPurchaseInvoiceLines = new XPCollection<PurchaseInvoiceLine>
                                                                                               (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                new BinaryOperator("PurchaseInvoice", _locPurchaseInvoiceXPO),
                                                                                                new BinaryOperator("Select", true),
                                                                                                new GroupOperator(GroupOperatorType.Or,
                                                                                                new BinaryOperator("Status", Status.Progress),
                                                                                                new BinaryOperator("Status", Status.Posted))));

                    if (_locPurchaseInvoiceLines != null && _locPurchaseInvoiceLines.Count > 0)
                    {
                        foreach (PurchaseInvoiceLine _locPurchaseInvoiceLine in _locPurchaseInvoiceLines)
                        {
                            _result = _result + _locPurchaseInvoiceLine.TAmount;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("BusinessObject = PurchaseInvoice" + ex.ToString());
            }
            return _result;
        }

        private void SetPurchaseInvoiceMonitoring(Session _currSession, PurchaseInvoice _locPurchaseInvoiceXPO)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                DateTime now = DateTime.Now;
                string _currSignCode = null;

                if (_locPurchaseInvoiceXPO != null)
                {
                    if (_locPurchaseInvoiceXPO.Status == Status.Progress || _locPurchaseInvoiceXPO.Status == Status.Posted)
                    {
                        XPCollection<PurchaseInvoiceLine> _locPurchaseInvoiceLines = new XPCollection<PurchaseInvoiceLine>
                                                            (_currSession, new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("PurchaseInvoice", _locPurchaseInvoiceXPO),
                                                            new BinaryOperator("Select", true),
                                                            new GroupOperator(GroupOperatorType.Or,
                                                            new BinaryOperator("Status", Status.Progress),
                                                            new BinaryOperator("Status", Status.Posted))));

                        if (_locPurchaseInvoiceLines != null && _locPurchaseInvoiceLines.Count() > 0)
                        {
                            foreach (PurchaseInvoiceLine _locPurchaseInvoiceLine in _locPurchaseInvoiceLines)
                            {
                                if (_locPurchaseInvoiceLine.TAmount > 0)
                                {
                                    _currSignCode = _globFunc.GetNumberingSignUnlockOptimisticRecordBasedOrganization(_currSession.DataLayer, ObjectList.PurchaseInvoiceMonitoring, _locPurchaseInvoiceXPO.Company, _locPurchaseInvoiceXPO.Workplace);
                                    
                                    if (_currSignCode != null)
                                    {
                                        #region SavePurchaseInvoiceMonitoring
                                        PurchaseInvoiceMonitoring _saveDataPurchaseInvoiceMonitoring = new PurchaseInvoiceMonitoring(_currSession)
                                        {
                                            SignCode = _currSignCode,
                                            PurchaseInvoice = _locPurchaseInvoiceXPO,
                                            PurchaseInvoiceLine = _locPurchaseInvoiceLine,
                                            Currency = _locPurchaseInvoiceLine.Currency,
                                            TAmount = _locPurchaseInvoiceXPO.TAmount,
                                            AmountDN = _locPurchaseInvoiceXPO.AmountDN,
                                            AmountCN = _locPurchaseInvoiceXPO.AmountCN,
                                            Amount = _locPurchaseInvoiceXPO.Amount,
                                            Company = _locPurchaseInvoiceLine.Company,
                                            Workplace = _locPurchaseInvoiceLine.Workplace,
                                            Division = _locPurchaseInvoiceLine.Division,
                                            Department = _locPurchaseInvoiceLine.Department,
                                            Section = _locPurchaseInvoiceLine.Section,
                                            Employee = _locPurchaseInvoiceLine.Employee,
                                            PurchaseOrderMonitoring = _locPurchaseInvoiceLine.PurchaseOrderMonitoring,
                                            InventoryTransferInMonitoring = _locPurchaseInvoiceLine.InventoryTransferInMonitoring,
                                            DocDate = _locPurchaseInvoiceLine.DocDate,
                                            JournalMonth = _locPurchaseInvoiceLine.JournalMonth,
                                            JournalYear = _locPurchaseInvoiceLine.JournalYear,
                                        };
                                        _saveDataPurchaseInvoiceMonitoring.Save();
                                        _saveDataPurchaseInvoiceMonitoring.Session.CommitTransaction();
                                        #endregion SavePurchaseInvoiceMonitoring

                                        PurchaseInvoiceMonitoring _locPurchaseInvoiceMonitoring = _currSession.FindObject<PurchaseInvoiceMonitoring>
                                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                                    new BinaryOperator("SignCode", _currSignCode)));
                                        if (_locPurchaseInvoiceMonitoring != null)
                                        {
                                            SetPaymentOutPlan(_currSession, _locPurchaseInvoiceXPO, _locPurchaseInvoiceMonitoring);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = PurchaseInvoice ", ex.ToString());
            }
        }

        private void SetPaymentOutPlan(Session _currSession, PurchaseInvoice _locPurchaseInvoiceXPO, PurchaseInvoiceMonitoring _locPurchaseInvoiceMonitoringXPO)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                DateTime now = DateTime.Now;

                if (_locPurchaseInvoiceXPO != null && _locPurchaseInvoiceMonitoringXPO != null)
                {
                    PaymentOutPlan _locPaymentOutPlan = _currSession.FindObject<PaymentOutPlan>
                                                    (new GroupOperator(GroupOperatorType.And,
                                                    new BinaryOperator("PurchaseInvoiceMonitoring", _locPurchaseInvoiceMonitoringXPO)));
                    if (_locPaymentOutPlan == null)
                    {
                        PaymentOutPlan _savePaymentOutPlan = new PaymentOutPlan(_currSession)
                        {
                            PaymentMethod = _locPurchaseInvoiceXPO.PaymentMethod,
                            PaymentMethodType = _locPurchaseInvoiceXPO.PaymentMethodType,
                            PaymentType = _locPurchaseInvoiceXPO.PaymentType,
                            EstimatedDate = _locPurchaseInvoiceXPO.EstimatedDate,
                            Plan = _locPurchaseInvoiceMonitoringXPO.TAmount,
                            Outstanding = _locPurchaseInvoiceMonitoringXPO.TAmount,
                            PurchaseOrder = _locPurchaseInvoiceMonitoringXPO.PurchaseOrderMonitoring.PurchaseOrder,
                            PurchaseInvoice = _locPurchaseInvoiceXPO,
                            PurchaseInvoiceMonitoring = _locPurchaseInvoiceMonitoringXPO,
                            Company = _locPurchaseInvoiceXPO.Company,
                            Workplace = _locPurchaseInvoiceXPO.Workplace,
                        };
                        _savePaymentOutPlan.Save();
                        _savePaymentOutPlan.Session.CommitTransaction();
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = PurchaseInvoice ", ex.ToString());
            }
        }

        #region JournalInvoiceAP

        private bool CheckJournalSetup(Session _currSession, PurchaseInvoice _locPurchaseInvoiceXPO)
        {
            bool _result = false;
            try
            {
                if (_locPurchaseInvoiceXPO != null)
                {
                    if (_locPurchaseInvoiceXPO.Company != null && _locPurchaseInvoiceXPO.Workplace != null)
                    {
                        JournalSetupDetail _locJournalSetupDetail = _currSession.FindObject<JournalSetupDetail>
                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                    new BinaryOperator("Company", _locPurchaseInvoiceXPO.Company),
                                                                    new BinaryOperator("Workplace", _locPurchaseInvoiceXPO.Workplace),
                                                                    new BinaryOperator("JournalSetupType", JournalSetupType.Journal),
                                                                    new BinaryOperator("ObjectList", ObjectList.PurchaseInvoice),
                                                                    new BinaryOperator("JournalProcess", JournalProcess.JournalInvoiceAP),
                                                                    new BinaryOperator("Active", true)));

                        if (_locJournalSetupDetail != null)
                        {
                            _result = true;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = PurchaseInvoice ", ex.ToString());
            }
            return _result;
        }

        private void SetReverseGoodsReceiveJournal(Session _currSession, PurchaseInvoice _locPurchaseInvoiceXPO)
        {
            try
            {
                DateTime now = DateTime.Now;
                double _locTotalUnitAmount = 0;
                ItemAccountGroup _locItemAccountGroup = null;

                if (_locPurchaseInvoiceXPO != null)
                {
                    XPCollection<PurchaseInvoiceLine> _locPurchaseInvoiceLines = new XPCollection<PurchaseInvoiceLine>(_currSession,
                                                                                new GroupOperator(GroupOperatorType.And,
                                                                                new BinaryOperator("PurchaseInvoice", _locPurchaseInvoiceXPO),
                                                                                new BinaryOperator("Select", true),
                                                                                new BinaryOperator("Status", Status.Progress)));

                    if (_locPurchaseInvoiceLines != null && _locPurchaseInvoiceLines.Count() > 0)
                    {
                        foreach (PurchaseInvoiceLine _locPurchaseInvoiceLine in _locPurchaseInvoiceLines)
                        {
                            _locTotalUnitAmount = _locPurchaseInvoiceLine.TUAmount;

                            if(_locPurchaseInvoiceLine.PurchaseOrderMonitoring != null && _locPurchaseInvoiceLine.InventoryTransferInMonitoring != null)
                            {
                                if(_locPurchaseInvoiceLine.PurchaseOrderMonitoring.PurchaseOrder != null && _locPurchaseInvoiceLine.InventoryTransferInMonitoring.InventoryTransferIn != null)
                                {
                                    ItemAccount _locItemAccount = _currSession.FindObject<ItemAccount>(new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("Company", _locPurchaseInvoiceLine.Company),
                                                                new BinaryOperator("Workplace", _locPurchaseInvoiceLine.Workplace),
                                                                new BinaryOperator("Item", _locPurchaseInvoiceLine.Item),
                                                                new BinaryOperator("Active", true)));

                                    if (_locItemAccount != null)
                                    {
                                        if (_locItemAccount.ItemAccountGroup != null)
                                        {
                                            _locItemAccountGroup = _locItemAccount.ItemAccountGroup;
                                        }
                                    }

                                    #region JournalMapByItems

                                    XPCollection<JournalMap> _locJournalMapItems = new XPCollection<JournalMap>
                                                                                   (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                    new BinaryOperator("Company", _locPurchaseInvoiceLine.Company),
                                                                                    new BinaryOperator("Workplace", _locPurchaseInvoiceLine.Workplace),
                                                                                    new BinaryOperator("ItemAccountGroup", _locItemAccountGroup),
                                                                                    new BinaryOperator("Active", true)));

                                    if (_locJournalMapItems != null && _locJournalMapItems.Count() > 0)
                                    {
                                        foreach (JournalMap _locJournalMapItem in _locJournalMapItems)
                                        {
                                            XPCollection<JournalMapLine> _locJournalMapLines = new XPCollection<JournalMapLine>
                                                                                               (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                new BinaryOperator("Company", _locPurchaseInvoiceLine.Company),
                                                                                                new BinaryOperator("Workplace", _locPurchaseInvoiceLine.Workplace),
                                                                                                new BinaryOperator("JournalMap", _locJournalMapItem),
                                                                                                new BinaryOperator("Active", true)));

                                            if (_locJournalMapLines != null && _locJournalMapLines.Count() > 0)
                                            {
                                                foreach (JournalMapLine _locJournalMapLine in _locJournalMapLines)
                                                {
                                                    if (_locJournalMapLine.AccountMap != null)
                                                    {
                                                        if (_locJournalMapLine.AccountMap.PostingType == PostingType.Purchase && _locJournalMapLine.AccountMap.OrderType == OrderType.Item
                                                            && _locJournalMapLine.AccountMap.PostingMethod == PostingMethod.Receipt && _locJournalMapLine.AccountMap.PostingMethodType == PostingMethodType.Normal)
                                                        {
                                                            XPCollection<AccountMapLine> _locAccountMapLines = new XPCollection<AccountMapLine>
                                                                                                           (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                            new BinaryOperator("Company", _locPurchaseInvoiceLine.Company),
                                                                                                            new BinaryOperator("Workplace", _locPurchaseInvoiceLine.Workplace),
                                                                                                            new BinaryOperator("AccountMap", _locJournalMapLine.AccountMap),
                                                                                                            new BinaryOperator("Active", true)));

                                                            if (_locAccountMapLines != null && _locAccountMapLines.Count() > 0)
                                                            {
                                                                double _locTotalAmountDebit = 0;
                                                                double _locTotalAmountCredit = 0;
                                                                double _locTotalBalance = 0;

                                                                foreach (AccountMapLine _locAccountMapLine in _locAccountMapLines)
                                                                {
                                                                    if (_locAccountMapLine.AccountCharge == AccountCharge.Debit)
                                                                    {
                                                                        _locTotalAmountCredit = _locTotalUnitAmount;
                                                                    }
                                                                    if (_locAccountMapLine.AccountCharge == AccountCharge.Credit)
                                                                    {
                                                                        _locTotalAmountDebit = _locTotalUnitAmount;
                                                                    }

                                                                    GeneralJournal _saveGeneralJournal = new GeneralJournal(_currSession)
                                                                    {
                                                                        Company = _locPurchaseInvoiceLine.Company,
                                                                        Workplace = _locPurchaseInvoiceLine.Workplace,
                                                                        PostingDate = now,
                                                                        PostingType = PostingType.Purchase,
                                                                        OrderType = OrderType.Item,
                                                                        PostingMethod = PostingMethod.InvoiceAP,
                                                                        PostingMethodType = PostingMethodType.Normal,
                                                                        Account = _locAccountMapLine.Account,
                                                                        Debit = _locTotalAmountDebit,
                                                                        Credit = _locTotalAmountCredit,
                                                                        JournalMonth = _locPurchaseInvoiceLine.JournalMonth,
                                                                        JournalYear = _locPurchaseInvoiceLine.JournalYear,
                                                                        InventoryTransferIn = _locPurchaseInvoiceLine.InventoryTransferInMonitoring.InventoryTransferIn,
                                                                        PurchaseInvoice = _locPurchaseInvoiceXPO,
                                                                    };

                                                                    _saveGeneralJournal.Save();
                                                                    _saveGeneralJournal.Session.CommitTransaction();

                                                                    #region AccountingPeriodicLine

                                                                    if (_locAccountMapLine.Account.Code != null)
                                                                    {
                                                                        //Cari Account Periodic Line
                                                                        AccountingPeriodicLine _locAPL = _currSession.FindObject<AccountingPeriodicLine>
                                                                                                        (new GroupOperator(GroupOperatorType.And,
                                                                                                        new BinaryOperator("Company", _locPurchaseInvoiceLine.Company),
                                                                                                        new BinaryOperator("Workplace", _locPurchaseInvoiceLine.Workplace),
                                                                                                        new BinaryOperator("AccountNo", _locAccountMapLine.Account),
                                                                                                        new BinaryOperator("Month", _locPurchaseInvoiceLine.JournalMonth),
                                                                                                        new BinaryOperator("Year", _locPurchaseInvoiceLine.JournalYear)));
                                                                        if (_locAPL != null)
                                                                        {
                                                                            if (_locAPL.AccountNo != null)
                                                                            {
                                                                                if (_locAPL.AccountNo.BalanceType == BalanceType.Change)
                                                                                {
                                                                                    if (_locAPL.AccountNo.AccountCharge == AccountCharge.Debit)
                                                                                    {
                                                                                        if (_locTotalAmountDebit > 0)
                                                                                        {
                                                                                            _locTotalBalance = _locAPL.Balance + _locTotalAmountDebit;
                                                                                        }
                                                                                        if (_locTotalAmountCredit > 0)
                                                                                        {
                                                                                            _locTotalBalance = _locAPL.Balance - _locTotalAmountCredit;
                                                                                        }
                                                                                    }
                                                                                    if (_locAPL.AccountNo.AccountCharge == AccountCharge.Credit)
                                                                                    {
                                                                                        if (_locTotalAmountDebit > 0)
                                                                                        {
                                                                                            _locTotalBalance = _locAPL.Balance - _locTotalAmountDebit;
                                                                                        }
                                                                                        if (_locTotalAmountCredit > 0)
                                                                                        {
                                                                                            _locTotalBalance = _locAPL.Balance + _locTotalAmountCredit;
                                                                                        }
                                                                                    }
                                                                                }

                                                                                _locAPL.Balance = _locTotalBalance;
                                                                                _locAPL.Debit = _locAPL.Debit + _locTotalAmountDebit;
                                                                                _locAPL.Credit = _locAPL.Credit + _locTotalAmountCredit;
                                                                                _locAPL.Save();
                                                                                _locAPL.Session.CommitTransaction();

                                                                            }
                                                                        }
                                                                    }
                                                                    #endregion AccountingPeriodicLine
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    #endregion JournalMapByItems
                                }
                            }   
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = PurchaseInvoice ", ex.ToString());
            }
        }

        private void SetInvoiceAPJournal(Session _currSession, PurchaseInvoice _locPurchaseInvoiceXPO)
        {
            try
            {
                DateTime now = DateTime.Now;
                double _locTotalUnitAmount = 0;
                double _locTotalTaxAmount = 0;
                double _locTotalDiscount = 0;
                double _locTotalAmountBP = 0;
                double _locTotalAmountCompany = 0;
                ItemAccountGroup _locItemAccountGroup = null;
                TaxAccountGroup _locTaxAccountGroup = null;
                DiscountAccountGroup _locDiscountAccountGroup = null;
                BusinessPartnerAccountGroup _locBusinessPartnerAccountGroup = null;
                CompanyAccountGroup _locCompanyAccountGroup = null;

                if (_locPurchaseInvoiceXPO != null)
                {
                    XPCollection<PurchaseInvoiceLine> _locPurchaseInvoiceLines = new XPCollection<PurchaseInvoiceLine>(_currSession,
                                                                              new GroupOperator(GroupOperatorType.And,
                                                                              new BinaryOperator("Company", _locPurchaseInvoiceXPO.Company),
                                                                              new BinaryOperator("Workplace", _locPurchaseInvoiceXPO.Workplace),
                                                                              new BinaryOperator("PurchaseInvoice", _locPurchaseInvoiceXPO),
                                                                              new BinaryOperator("Status", Status.Progress)));

                    if (_locPurchaseInvoiceLines != null && _locPurchaseInvoiceLines.Count() > 0)
                    {
                        foreach (PurchaseInvoiceLine _locPurchaseInvoiceLine in _locPurchaseInvoiceLines)
                        {
                            if(_locPurchaseInvoiceLine.PurchaseOrderMonitoring != null)
                            {
                                if(_locPurchaseInvoiceLine.PurchaseOrderMonitoring.PurchaseOrder != null)
                                {
                                    _locTotalUnitAmount = _locPurchaseInvoiceLine.TUAmount;

                                    if(_locPurchaseInvoiceLine.Item != null)
                                    {
                                        #region NormalJournal
                                        ItemAccount _locItemAccount = _currSession.FindObject<ItemAccount>(new GroupOperator(GroupOperatorType.And,
                                                        new BinaryOperator("Company", _locPurchaseInvoiceLine.Company),
                                                        new BinaryOperator("Workplace", _locPurchaseInvoiceLine.Workplace),
                                                        new BinaryOperator("Item", _locPurchaseInvoiceLine.Item),
                                                        new BinaryOperator("Active", true)));

                                        if (_locItemAccount != null)
                                        {
                                            if (_locItemAccount.ItemAccountGroup != null)
                                            {
                                                _locItemAccountGroup = _locItemAccount.ItemAccountGroup;

                                                XPCollection<JournalMap> _locJournalMapItems = new XPCollection<JournalMap>
                                                                               (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                new BinaryOperator("Company", _locPurchaseInvoiceLine.Company),
                                                                                new BinaryOperator("Workplace", _locPurchaseInvoiceLine.Workplace),
                                                                                new BinaryOperator("ItemAccountGroup", _locItemAccountGroup),
                                                                                new BinaryOperator("Active", true)));

                                                if (_locJournalMapItems != null && _locJournalMapItems.Count() > 0)
                                                {
                                                    foreach (JournalMap _locJournalMapItem in _locJournalMapItems)
                                                    {
                                                        XPCollection<JournalMapLine> _locJournalMapLines = new XPCollection<JournalMapLine>
                                                                                                           (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                            new BinaryOperator("Company", _locPurchaseInvoiceLine.Company),
                                                                                                            new BinaryOperator("Workplace", _locPurchaseInvoiceLine.Workplace),
                                                                                                            new BinaryOperator("JournalMap", _locJournalMapItem),
                                                                                                            new BinaryOperator("Active", true)));

                                                        if (_locJournalMapLines != null && _locJournalMapLines.Count() > 0)
                                                        {
                                                            foreach (JournalMapLine _locJournalMapLine in _locJournalMapLines)
                                                            {
                                                                if (_locJournalMapLine.AccountMap != null)
                                                                {
                                                                    if (_locJournalMapLine.AccountMap.PostingType == PostingType.Purchase && _locJournalMapLine.AccountMap.OrderType == OrderType.Item
                                                                        && _locJournalMapLine.AccountMap.PostingMethod == PostingMethod.InvoiceAP && _locJournalMapLine.AccountMap.PostingMethodType == PostingMethodType.Normal)
                                                                    {
                                                                        XPCollection<AccountMapLine> _locAccountMapLines = new XPCollection<AccountMapLine>
                                                                                                                       (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                                        new BinaryOperator("Company", _locPurchaseInvoiceLine.Company),
                                                                                                                        new BinaryOperator("Workplace", _locPurchaseInvoiceLine.Workplace),
                                                                                                                        new BinaryOperator("AccountMap", _locJournalMapLine.AccountMap),
                                                                                                                        new BinaryOperator("Active", true)));

                                                                        if (_locAccountMapLines != null && _locAccountMapLines.Count() > 0)
                                                                        {
                                                                            double _locTotalAmountDebit = 0;
                                                                            double _locTotalAmountCredit = 0;
                                                                            double _locTotalBalance = 0;

                                                                            foreach (AccountMapLine _locAccountMapLine in _locAccountMapLines)
                                                                            {
                                                                                if (_locAccountMapLine.AccountCharge == AccountCharge.Debit)
                                                                                {
                                                                                    _locTotalAmountDebit = _locTotalUnitAmount;
                                                                                }
                                                                                if (_locAccountMapLine.AccountCharge == AccountCharge.Credit)
                                                                                {
                                                                                    _locTotalAmountCredit = _locTotalUnitAmount;
                                                                                }

                                                                                GeneralJournal _saveGeneralJournal = new GeneralJournal(_currSession)
                                                                                {
                                                                                    Company = _locPurchaseInvoiceLine.Company,
                                                                                    Workplace = _locPurchaseInvoiceLine.Workplace,
                                                                                    PostingDate = now,
                                                                                    PostingType = PostingType.Purchase,
                                                                                    OrderType = OrderType.Item,
                                                                                    PostingMethod = PostingMethod.InvoiceAP,
                                                                                    PostingMethodType = PostingMethodType.Normal,
                                                                                    Account = _locAccountMapLine.Account,
                                                                                    Debit = _locTotalAmountDebit,
                                                                                    Credit = _locTotalAmountCredit,
                                                                                    JournalMonth = _locPurchaseInvoiceLine.JournalMonth,
                                                                                    JournalYear = _locPurchaseInvoiceLine.JournalYear,
                                                                                    PurchaseInvoice = _locPurchaseInvoiceXPO,
                                                                                };

                                                                                _saveGeneralJournal.Save();
                                                                                _saveGeneralJournal.Session.CommitTransaction();

                                                                                #region AccountingPeriodicLine

                                                                                if (_locAccountMapLine.Account.Code != null)
                                                                                {
                                                                                    //Cari Account Periodic Line
                                                                                    AccountingPeriodicLine _locAPL = _currSession.FindObject<AccountingPeriodicLine>
                                                                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                                                                    new BinaryOperator("Company", _locPurchaseInvoiceLine.Company),
                                                                                                                    new BinaryOperator("Workplace", _locPurchaseInvoiceLine.Workplace),
                                                                                                                    new BinaryOperator("AccountNo", _locAccountMapLine.Account),
                                                                                                                    new BinaryOperator("Month", _locPurchaseInvoiceLine.JournalMonth),
                                                                                                                    new BinaryOperator("Year", _locPurchaseInvoiceLine.JournalYear)));
                                                                                    if (_locAPL != null)
                                                                                    {
                                                                                        if (_locAPL.AccountNo != null)
                                                                                        {
                                                                                            if (_locAPL.AccountNo.BalanceType == BalanceType.Change)
                                                                                            {
                                                                                                if (_locAPL.AccountNo.AccountCharge == AccountCharge.Debit)
                                                                                                {
                                                                                                    if (_locTotalAmountDebit > 0)
                                                                                                    {
                                                                                                        _locTotalBalance = _locAPL.Balance + _locTotalAmountDebit;
                                                                                                    }
                                                                                                    if (_locTotalAmountCredit > 0)
                                                                                                    {
                                                                                                        _locTotalBalance = _locAPL.Balance - _locTotalAmountCredit;
                                                                                                    }
                                                                                                }
                                                                                                if (_locAPL.AccountNo.AccountCharge == AccountCharge.Credit)
                                                                                                {
                                                                                                    if (_locTotalAmountDebit > 0)
                                                                                                    {
                                                                                                        _locTotalBalance = _locAPL.Balance - _locTotalAmountDebit;
                                                                                                    }
                                                                                                    if (_locTotalAmountCredit > 0)
                                                                                                    {
                                                                                                        _locTotalBalance = _locAPL.Balance + _locTotalAmountCredit;
                                                                                                    }
                                                                                                }
                                                                                            }

                                                                                            _locAPL.Balance = _locTotalBalance;
                                                                                            _locAPL.Debit = _locAPL.Debit + _locTotalAmountDebit;
                                                                                            _locAPL.Credit = _locAPL.Credit + _locTotalAmountCredit;
                                                                                            _locAPL.Save();
                                                                                            _locAPL.Session.CommitTransaction();

                                                                                        }
                                                                                    }
                                                                                }
                                                                                #endregion AccountingPeriodicLine
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        #endregion NormalJournal
                                        #region TaxJournal
                                        #region MultiTax=trueTaxRule=AfterDiscount
                                        if (_locPurchaseInvoiceLine.MultiTax == true && _locPurchaseInvoiceLine.TaxRule == TaxRule.AfterDiscount)
                                        {
                                            XPCollection<TaxLine> _locTaxLines = new XPCollection<TaxLine>
                                                                                (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                 new BinaryOperator("PurchaseInvoiceLine", _locPurchaseInvoiceLine)));
                                            if (_locTaxLines != null && _locTaxLines.Count() > 0)
                                            {
                                                foreach (TaxLine _locTaxLine in _locTaxLines)
                                                {
                                                    if (_locTaxLine.Tax != null)
                                                    {
                                                        _locTotalTaxAmount = ((_locTaxLine.TxValue / 100) * (_locPurchaseInvoiceLine.TUAmount - _locPurchaseInvoiceLine.DiscAmount));
                                                        //_locTotalTaxAmount = GetSumTotalTaxAmountAfterDiscountJournal(_currSession, _locPurchaseInvoiceLine);

                                                        TaxAccount _locTaxAccount = _currSession.FindObject<TaxAccount>(new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("Company", _locPurchaseInvoiceLine.Company),
                                                                new BinaryOperator("Workplace", _locPurchaseInvoiceLine.Workplace),
                                                                new BinaryOperator("Tax", _locTaxLine.Tax),
                                                                new BinaryOperator("Active", true)));

                                                        if (_locTaxAccount != null)
                                                        {
                                                            if (_locTaxAccount.TaxAccountGroup != null)
                                                            {
                                                                _locTaxAccountGroup = _locTaxAccount.TaxAccountGroup;

                                                                XPCollection<JournalMap> _locJournalMapItems = new XPCollection<JournalMap>
                                                                                               (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                new BinaryOperator("Company", _locPurchaseInvoiceLine.Company),
                                                                                                new BinaryOperator("Workplace", _locPurchaseInvoiceLine.Workplace),
                                                                                                new BinaryOperator("TaxAccountGroup", _locTaxAccountGroup),
                                                                                                new BinaryOperator("Active", true)));

                                                                if (_locJournalMapItems != null && _locJournalMapItems.Count() > 0)
                                                                {
                                                                    foreach (JournalMap _locJournalMapItem in _locJournalMapItems)
                                                                    {
                                                                        XPCollection<JournalMapLine> _locJournalMapLines = new XPCollection<JournalMapLine>
                                                                                                                           (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                                            new BinaryOperator("Company", _locPurchaseInvoiceLine.Company),
                                                                                                                            new BinaryOperator("Workplace", _locPurchaseInvoiceLine.Workplace),
                                                                                                                            new BinaryOperator("JournalMap", _locJournalMapItem),
                                                                                                                            new BinaryOperator("Active", true)));

                                                                        if (_locJournalMapLines != null && _locJournalMapLines.Count() > 0)
                                                                        {
                                                                            foreach (JournalMapLine _locJournalMapLine in _locJournalMapLines)
                                                                            {
                                                                                if (_locJournalMapLine.AccountMap != null)
                                                                                {
                                                                                    if (_locJournalMapLine.AccountMap.PostingType == PostingType.Purchase && _locJournalMapLine.AccountMap.OrderType == OrderType.Item
                                                                                        && _locJournalMapLine.AccountMap.PostingMethod == PostingMethod.InvoiceAP && _locJournalMapLine.AccountMap.PostingMethodType == PostingMethodType.Tax)
                                                                                    {
                                                                                        XPCollection<AccountMapLine> _locAccountMapLines = new XPCollection<AccountMapLine>
                                                                                                                                       (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                                                        new BinaryOperator("Company", _locPurchaseInvoiceLine.Company),
                                                                                                                                        new BinaryOperator("Workplace", _locPurchaseInvoiceLine.Workplace),
                                                                                                                                        new BinaryOperator("AccountMap", _locJournalMapLine.AccountMap),
                                                                                                                                        new BinaryOperator("Active", true)));

                                                                                        if (_locAccountMapLines != null && _locAccountMapLines.Count() > 0)
                                                                                        {
                                                                                            double _locTotalAmountDebit = 0;
                                                                                            double _locTotalAmountCredit = 0;
                                                                                            double _locTotalBalance = 0;

                                                                                            foreach (AccountMapLine _locAccountMapLine in _locAccountMapLines)
                                                                                            {
                                                                                                if (_locAccountMapLine.AccountCharge == AccountCharge.Debit)
                                                                                                {
                                                                                                    _locTotalAmountDebit = _locTotalTaxAmount;
                                                                                                }
                                                                                                if (_locAccountMapLine.AccountCharge == AccountCharge.Credit)
                                                                                                {
                                                                                                    _locTotalAmountCredit = _locTotalTaxAmount;
                                                                                                }

                                                                                                GeneralJournal _saveGeneralJournal = new GeneralJournal(_currSession)
                                                                                                {
                                                                                                    Company = _locPurchaseInvoiceLine.Company,
                                                                                                    Workplace = _locPurchaseInvoiceLine.Workplace,
                                                                                                    PostingDate = now,
                                                                                                    PostingType = PostingType.Purchase,
                                                                                                    OrderType = OrderType.Item,
                                                                                                    PostingMethod = PostingMethod.InvoiceAP,
                                                                                                    PostingMethodType = PostingMethodType.Tax,
                                                                                                    Account = _locAccountMapLine.Account,
                                                                                                    Debit = _locTotalAmountDebit,
                                                                                                    Credit = _locTotalAmountCredit,
                                                                                                    JournalMonth = _locPurchaseInvoiceLine.JournalMonth,
                                                                                                    JournalYear = _locPurchaseInvoiceLine.JournalYear,
                                                                                                    PurchaseInvoice = _locPurchaseInvoiceXPO,
                                                                                                };
                                                                                                _saveGeneralJournal.Save();
                                                                                                _saveGeneralJournal.Session.CommitTransaction();

                                                                                                #region AccountingPeriodicLine

                                                                                                if (_locAccountMapLine.Account.Code != null)
                                                                                                {
                                                                                                    //Cari Account Periodic Line
                                                                                                    AccountingPeriodicLine _locAPL = _currSession.FindObject<AccountingPeriodicLine>
                                                                                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                                                                                    new BinaryOperator("Company", _locPurchaseInvoiceLine.Company),
                                                                                                                                    new BinaryOperator("Workplace", _locPurchaseInvoiceLine.Workplace),
                                                                                                                                    new BinaryOperator("AccountNo", _locAccountMapLine.Account),
                                                                                                                                    new BinaryOperator("Month", _locPurchaseInvoiceLine.JournalMonth),
                                                                                                                                    new BinaryOperator("Year", _locPurchaseInvoiceLine.JournalYear)));
                                                                                                    if (_locAPL != null)
                                                                                                    {
                                                                                                        if (_locAPL.AccountNo != null)
                                                                                                        {
                                                                                                            if (_locAPL.AccountNo.BalanceType == BalanceType.Change)
                                                                                                            {
                                                                                                                if (_locAPL.AccountNo.AccountCharge == AccountCharge.Debit)
                                                                                                                {
                                                                                                                    if (_locTotalAmountDebit > 0)
                                                                                                                    {
                                                                                                                        _locTotalBalance = _locAPL.Balance + _locTotalAmountDebit;
                                                                                                                    }
                                                                                                                    if (_locTotalAmountCredit > 0)
                                                                                                                    {
                                                                                                                        _locTotalBalance = _locAPL.Balance - _locTotalAmountCredit;
                                                                                                                    }
                                                                                                                }
                                                                                                                if (_locAPL.AccountNo.AccountCharge == AccountCharge.Credit)
                                                                                                                {
                                                                                                                    if (_locTotalAmountDebit > 0)
                                                                                                                    {
                                                                                                                        _locTotalBalance = _locAPL.Balance - _locTotalAmountDebit;
                                                                                                                    }
                                                                                                                    if (_locTotalAmountCredit > 0)
                                                                                                                    {
                                                                                                                        _locTotalBalance = _locAPL.Balance + _locTotalAmountCredit;
                                                                                                                    }
                                                                                                                }
                                                                                                            }

                                                                                                            _locAPL.Balance = _locTotalBalance;
                                                                                                            _locAPL.Debit = _locAPL.Debit + _locTotalAmountDebit;
                                                                                                            _locAPL.Credit = _locAPL.Credit + _locTotalAmountCredit;
                                                                                                            _locAPL.Save();
                                                                                                            _locAPL.Session.CommitTransaction();

                                                                                                        }
                                                                                                    }
                                                                                                }
                                                                                                #endregion AccountingPeriodicLine
                                                                                            }
                                                                                        }
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        #endregion MultiTax=trueTaxRule=AfterDiscount
                                        #region MultiTax=falseTaxRule=AfterDiscount
                                        if (_locPurchaseInvoiceLine.MultiTax == false && _locPurchaseInvoiceLine.TaxRule == TaxRule.AfterDiscount)
                                        {
                                            if (_locPurchaseInvoiceLine.Tax != null)
                                            {
                                                _locTotalTaxAmount = _locPurchaseInvoiceLine.TxAmount;

                                                TaxAccount _locTaxAccount = _currSession.FindObject<TaxAccount>(new GroupOperator(GroupOperatorType.And,
                                                                    new BinaryOperator("Company", _locPurchaseInvoiceLine.Company),
                                                                    new BinaryOperator("Workplace", _locPurchaseInvoiceLine.Workplace),
                                                                    new BinaryOperator("Tax", _locPurchaseInvoiceLine.Tax),
                                                                    new BinaryOperator("Active", true)));

                                                if (_locTaxAccount != null)
                                                {
                                                    if (_locTaxAccount.TaxAccountGroup != null)
                                                    {
                                                        _locTaxAccountGroup = _locTaxAccount.TaxAccountGroup;

                                                        XPCollection<JournalMap> _locJournalMapItems = new XPCollection<JournalMap>
                                                                                       (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                        new BinaryOperator("Company", _locPurchaseInvoiceLine.Company),
                                                                                        new BinaryOperator("Workplace", _locPurchaseInvoiceLine.Workplace),
                                                                                        new BinaryOperator("TaxAccountGroup", _locTaxAccountGroup),
                                                                                        new BinaryOperator("Active", true)));

                                                        if (_locJournalMapItems != null && _locJournalMapItems.Count() > 0)
                                                        {
                                                            foreach (JournalMap _locJournalMapItem in _locJournalMapItems)
                                                            {
                                                                XPCollection<JournalMapLine> _locJournalMapLines = new XPCollection<JournalMapLine>
                                                                                                                   (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                                    new BinaryOperator("Company", _locPurchaseInvoiceLine.Company),
                                                                                                                    new BinaryOperator("Workplace", _locPurchaseInvoiceLine.Workplace),
                                                                                                                    new BinaryOperator("JournalMap", _locJournalMapItem),
                                                                                                                    new BinaryOperator("Active", true)));

                                                                if (_locJournalMapLines != null && _locJournalMapLines.Count() > 0)
                                                                {
                                                                    foreach (JournalMapLine _locJournalMapLine in _locJournalMapLines)
                                                                    {
                                                                        if (_locJournalMapLine.AccountMap != null)
                                                                        {
                                                                            if (_locJournalMapLine.AccountMap.PostingType == PostingType.Purchase && _locJournalMapLine.AccountMap.OrderType == OrderType.Item
                                                                                && _locJournalMapLine.AccountMap.PostingMethod == PostingMethod.InvoiceAP && _locJournalMapLine.AccountMap.PostingMethodType == PostingMethodType.Tax)
                                                                            {
                                                                                XPCollection<AccountMapLine> _locAccountMapLines = new XPCollection<AccountMapLine>
                                                                                                                               (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                                                new BinaryOperator("Company", _locPurchaseInvoiceLine.Company),
                                                                                                                                new BinaryOperator("Workplace", _locPurchaseInvoiceLine.Workplace),
                                                                                                                                new BinaryOperator("AccountMap", _locJournalMapLine.AccountMap),
                                                                                                                                new BinaryOperator("Active", true)));

                                                                                if (_locAccountMapLines != null && _locAccountMapLines.Count() > 0)
                                                                                {
                                                                                    double _locTotalAmountDebit = 0;
                                                                                    double _locTotalAmountCredit = 0;
                                                                                    double _locTotalBalance = 0;

                                                                                    foreach (AccountMapLine _locAccountMapLine in _locAccountMapLines)
                                                                                    {
                                                                                        if (_locAccountMapLine.AccountCharge == AccountCharge.Debit)
                                                                                        {
                                                                                            _locTotalAmountDebit = _locTotalTaxAmount;
                                                                                        }
                                                                                        if (_locAccountMapLine.AccountCharge == AccountCharge.Credit)
                                                                                        {
                                                                                            _locTotalAmountCredit = _locTotalTaxAmount;
                                                                                        }

                                                                                        GeneralJournal _saveGeneralJournal = new GeneralJournal(_currSession)
                                                                                        {
                                                                                            Company = _locPurchaseInvoiceLine.Company,
                                                                                            Workplace = _locPurchaseInvoiceLine.Workplace,
                                                                                            PostingDate = now,
                                                                                            PostingType = PostingType.Purchase,
                                                                                            PostingMethod = PostingMethod.InvoiceAP,
                                                                                            PostingMethodType = PostingMethodType.Tax,
                                                                                            Account = _locAccountMapLine.Account,
                                                                                            Debit = _locTotalAmountDebit,
                                                                                            Credit = _locTotalAmountCredit,
                                                                                            JournalMonth = _locPurchaseInvoiceLine.JournalMonth,
                                                                                            JournalYear = _locPurchaseInvoiceLine.JournalYear,
                                                                                            PurchaseInvoice = _locPurchaseInvoiceXPO,
                                                                                        };

                                                                                        _saveGeneralJournal.Save();
                                                                                        _saveGeneralJournal.Session.CommitTransaction();

                                                                                        #region AccountingPeriodicLine

                                                                                        if (_locAccountMapLine.Account.Code != null)
                                                                                        {
                                                                                            //Cari Account Periodic Line
                                                                                            AccountingPeriodicLine _locAPL = _currSession.FindObject<AccountingPeriodicLine>
                                                                                                                            (new GroupOperator(GroupOperatorType.And,
                                                                                                                            new BinaryOperator("Company", _locPurchaseInvoiceLine.Company),
                                                                                                                            new BinaryOperator("Workplace", _locPurchaseInvoiceLine.Workplace),
                                                                                                                            new BinaryOperator("AccountNo", _locAccountMapLine.Account),
                                                                                                                            new BinaryOperator("Month", _locPurchaseInvoiceLine.JournalMonth),
                                                                                                                            new BinaryOperator("Year", _locPurchaseInvoiceLine.JournalYear)));
                                                                                            if (_locAPL != null)
                                                                                            {
                                                                                                if (_locAPL.AccountNo != null)
                                                                                                {
                                                                                                    if (_locAPL.AccountNo.BalanceType == BalanceType.Change)
                                                                                                    {
                                                                                                        if (_locAPL.AccountNo.AccountCharge == AccountCharge.Debit)
                                                                                                        {
                                                                                                            if (_locTotalAmountDebit > 0)
                                                                                                            {
                                                                                                                _locTotalBalance = _locAPL.Balance + _locTotalAmountDebit;
                                                                                                            }
                                                                                                            if (_locTotalAmountCredit > 0)
                                                                                                            {
                                                                                                                _locTotalBalance = _locAPL.Balance - _locTotalAmountCredit;
                                                                                                            }
                                                                                                        }
                                                                                                        if (_locAPL.AccountNo.AccountCharge == AccountCharge.Credit)
                                                                                                        {
                                                                                                            if (_locTotalAmountDebit > 0)
                                                                                                            {
                                                                                                                _locTotalBalance = _locAPL.Balance - _locTotalAmountDebit;
                                                                                                            }
                                                                                                            if (_locTotalAmountCredit > 0)
                                                                                                            {
                                                                                                                _locTotalBalance = _locAPL.Balance + _locTotalAmountCredit;
                                                                                                            }
                                                                                                        }
                                                                                                    }

                                                                                                    _locAPL.Balance = _locTotalBalance;
                                                                                                    _locAPL.Debit = _locAPL.Debit + _locTotalAmountDebit;
                                                                                                    _locAPL.Credit = _locAPL.Credit + _locTotalAmountCredit;
                                                                                                    _locAPL.Save();
                                                                                                    _locAPL.Session.CommitTransaction();

                                                                                                }
                                                                                            }
                                                                                        }
                                                                                        #endregion AccountingPeriodicLine
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        #endregion MultiTax=falseTaxRule=AfterDiscount
                                        #region MultiTax=trueTaxRule=BeforeDiscountOrNone
                                        if ((_locPurchaseInvoiceLine.MultiTax == true) && (_locPurchaseInvoiceLine.TaxRule == TaxRule.BeforeDiscount ||
                                            _locPurchaseInvoiceLine.TaxRule == TaxRule.None))
                                        {
                                            XPCollection<TaxLine> _locTaxLines = new XPCollection<TaxLine>
                                                                                (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                 new BinaryOperator("PurchaseInvoiceLine", _locPurchaseInvoiceLine)));
                                            if (_locTaxLines != null && _locTaxLines.Count() > 0)
                                            {
                                                foreach (TaxLine _locTaxLine in _locTaxLines)
                                                {
                                                    if (_locTaxLine.Tax != null)
                                                    {
                                                        _locTotalTaxAmount = ((_locTaxLine.TxValue / 100) * (_locPurchaseInvoiceLine.TUAmount));
                                                        //_locTotalTaxAmount = GetSumTotalTaxAmountBeforeDiscountJournal(_currSession, _locPurchaseInvoiceLine);

                                                        TaxAccount _locTaxAccount = _currSession.FindObject<TaxAccount>(new GroupOperator(GroupOperatorType.And,
                                                                            new BinaryOperator("Company", _locPurchaseInvoiceLine.Company),
                                                                            new BinaryOperator("Workplace", _locPurchaseInvoiceLine.Workplace),
                                                                            new BinaryOperator("Tax", _locPurchaseInvoiceLine.Tax),
                                                                            new BinaryOperator("Active", true)));

                                                        if (_locTaxAccount != null)
                                                        {
                                                            if (_locTaxAccount.TaxAccountGroup != null)
                                                            {
                                                                _locTaxAccountGroup = _locTaxAccount.TaxAccountGroup;

                                                                XPCollection<JournalMap> _locJournalMapItems = new XPCollection<JournalMap>
                                                                                               (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                new BinaryOperator("Company", _locPurchaseInvoiceLine.Company),
                                                                                                new BinaryOperator("Workplace", _locPurchaseInvoiceLine.Workplace),
                                                                                                new BinaryOperator("TaxAccountGroup", _locTaxAccountGroup),
                                                                                                new BinaryOperator("Active", true)));

                                                                if (_locJournalMapItems != null && _locJournalMapItems.Count() > 0)
                                                                {
                                                                    foreach (JournalMap _locJournalMapItem in _locJournalMapItems)
                                                                    {
                                                                        XPCollection<JournalMapLine> _locJournalMapLines = new XPCollection<JournalMapLine>
                                                                                                                           (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                                            new BinaryOperator("Company", _locPurchaseInvoiceLine.Company),
                                                                                                                            new BinaryOperator("Workplace", _locPurchaseInvoiceLine.Workplace),
                                                                                                                            new BinaryOperator("JournalMap", _locJournalMapItem),
                                                                                                                            new BinaryOperator("Active", true)));

                                                                        if (_locJournalMapLines != null && _locJournalMapLines.Count() > 0)
                                                                        {
                                                                            foreach (JournalMapLine _locJournalMapLine in _locJournalMapLines)
                                                                            {
                                                                                if (_locJournalMapLine.AccountMap != null)
                                                                                {
                                                                                    if (_locJournalMapLine.AccountMap.PostingType == PostingType.Purchase && _locJournalMapLine.AccountMap.OrderType == OrderType.Item
                                                                                        && _locJournalMapLine.AccountMap.PostingMethod == PostingMethod.InvoiceAP && _locJournalMapLine.AccountMap.PostingMethodType == PostingMethodType.Tax)
                                                                                    {
                                                                                        XPCollection<AccountMapLine> _locAccountMapLines = new XPCollection<AccountMapLine>
                                                                                                                                       (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                                                        new BinaryOperator("Company", _locPurchaseInvoiceLine.Company),
                                                                                                                                        new BinaryOperator("Workplace", _locPurchaseInvoiceLine.Workplace),
                                                                                                                                        new BinaryOperator("AccountMap", _locJournalMapLine.AccountMap),
                                                                                                                                        new BinaryOperator("Active", true)));

                                                                                        if (_locAccountMapLines != null && _locAccountMapLines.Count() > 0)
                                                                                        {
                                                                                            double _locTotalAmountDebit = 0;
                                                                                            double _locTotalAmountCredit = 0;
                                                                                            double _locTotalBalance = 0;

                                                                                            foreach (AccountMapLine _locAccountMapLine in _locAccountMapLines)
                                                                                            {
                                                                                                if (_locAccountMapLine.AccountCharge == AccountCharge.Debit)
                                                                                                {
                                                                                                    _locTotalAmountDebit = _locTotalTaxAmount;
                                                                                                }
                                                                                                if (_locAccountMapLine.AccountCharge == AccountCharge.Credit)
                                                                                                {
                                                                                                    _locTotalAmountCredit = _locTotalTaxAmount;
                                                                                                }

                                                                                                GeneralJournal _saveGeneralJournal = new GeneralJournal(_currSession)
                                                                                                {
                                                                                                    Company = _locPurchaseInvoiceLine.Company,
                                                                                                    Workplace = _locPurchaseInvoiceLine.Workplace,
                                                                                                    PostingDate = now,
                                                                                                    PostingType = PostingType.Purchase,
                                                                                                    PostingMethod = PostingMethod.InvoiceAP,
                                                                                                    PostingMethodType = PostingMethodType.Tax,
                                                                                                    Account = _locAccountMapLine.Account,
                                                                                                    Debit = _locTotalAmountDebit,
                                                                                                    Credit = _locTotalAmountCredit,
                                                                                                    JournalMonth = _locPurchaseInvoiceLine.JournalMonth,
                                                                                                    JournalYear = _locPurchaseInvoiceLine.JournalYear,
                                                                                                    PurchaseInvoice = _locPurchaseInvoiceXPO,
                                                                                                };

                                                                                                _saveGeneralJournal.Save();
                                                                                                _saveGeneralJournal.Session.CommitTransaction();

                                                                                                #region AccountingPeriodicLine

                                                                                                if (_locAccountMapLine.Account.Code != null)
                                                                                                {
                                                                                                    //Cari Account Periodic Line
                                                                                                    AccountingPeriodicLine _locAPL = _currSession.FindObject<AccountingPeriodicLine>
                                                                                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                                                                                    new BinaryOperator("Company", _locPurchaseInvoiceLine.Company),
                                                                                                                                    new BinaryOperator("Workplace", _locPurchaseInvoiceLine.Workplace),
                                                                                                                                    new BinaryOperator("AccountNo", _locAccountMapLine.Account),
                                                                                                                                    new BinaryOperator("Month", _locPurchaseInvoiceLine.JournalMonth),
                                                                                                                                    new BinaryOperator("Year", _locPurchaseInvoiceLine.JournalYear)));
                                                                                                    if (_locAPL != null)
                                                                                                    {
                                                                                                        if (_locAPL.AccountNo != null)
                                                                                                        {
                                                                                                            if (_locAPL.AccountNo.BalanceType == BalanceType.Change)
                                                                                                            {
                                                                                                                if (_locAPL.AccountNo.AccountCharge == AccountCharge.Debit)
                                                                                                                {
                                                                                                                    if (_locTotalAmountDebit > 0)
                                                                                                                    {
                                                                                                                        _locTotalBalance = _locAPL.Balance + _locTotalAmountDebit;
                                                                                                                    }
                                                                                                                    if (_locTotalAmountCredit > 0)
                                                                                                                    {
                                                                                                                        _locTotalBalance = _locAPL.Balance - _locTotalAmountCredit;
                                                                                                                    }
                                                                                                                }
                                                                                                                if (_locAPL.AccountNo.AccountCharge == AccountCharge.Credit)
                                                                                                                {
                                                                                                                    if (_locTotalAmountDebit > 0)
                                                                                                                    {
                                                                                                                        _locTotalBalance = _locAPL.Balance - _locTotalAmountDebit;
                                                                                                                    }
                                                                                                                    if (_locTotalAmountCredit > 0)
                                                                                                                    {
                                                                                                                        _locTotalBalance = _locAPL.Balance + _locTotalAmountCredit;
                                                                                                                    }
                                                                                                                }
                                                                                                            }

                                                                                                            _locAPL.Balance = _locTotalBalance;
                                                                                                            _locAPL.Debit = _locAPL.Debit + _locTotalAmountDebit;
                                                                                                            _locAPL.Credit = _locAPL.Credit + _locTotalAmountCredit;
                                                                                                            _locAPL.Save();
                                                                                                            _locAPL.Session.CommitTransaction();

                                                                                                        }
                                                                                                    }
                                                                                                }
                                                                                                #endregion AccountingPeriodicLine
                                                                                            }
                                                                                        }
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        #endregion MultiTax=trueTaxRule=BeforeDiscountOrNone
                                        #region MultiTax=falseTaxRule=BeforeDiscountOrNone
                                        if ((_locPurchaseInvoiceLine.MultiTax == false) && (_locPurchaseInvoiceLine.TaxRule == TaxRule.BeforeDiscount ||
                                            _locPurchaseInvoiceLine.TaxRule == TaxRule.None))
                                        {
                                            if (_locPurchaseInvoiceLine.Tax != null)
                                            {
                                                _locTotalTaxAmount = _locPurchaseInvoiceLine.TxAmount;

                                                TaxAccount _locTaxAccount = _currSession.FindObject<TaxAccount>(new GroupOperator(GroupOperatorType.And,
                                                                    new BinaryOperator("Company", _locPurchaseInvoiceLine.Company),
                                                                    new BinaryOperator("Workplace", _locPurchaseInvoiceLine.Workplace),
                                                                    new BinaryOperator("Tax", _locPurchaseInvoiceLine.Tax),
                                                                    new BinaryOperator("Active", true)));

                                                if (_locTaxAccount != null)
                                                {
                                                    if (_locTaxAccount.TaxAccountGroup != null)
                                                    {
                                                        _locTaxAccountGroup = _locTaxAccount.TaxAccountGroup;

                                                        XPCollection<JournalMap> _locJournalMapItems = new XPCollection<JournalMap>
                                                                                       (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                        new BinaryOperator("Company", _locPurchaseInvoiceLine.Company),
                                                                                        new BinaryOperator("Workplace", _locPurchaseInvoiceLine.Workplace),
                                                                                        new BinaryOperator("TaxAccountGroup", _locTaxAccountGroup),
                                                                                        new BinaryOperator("Active", true)));

                                                        if (_locJournalMapItems != null && _locJournalMapItems.Count() > 0)
                                                        {
                                                            foreach (JournalMap _locJournalMapItem in _locJournalMapItems)
                                                            {
                                                                XPCollection<JournalMapLine> _locJournalMapLines = new XPCollection<JournalMapLine>
                                                                                                                   (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                                    new BinaryOperator("Company", _locPurchaseInvoiceLine.Company),
                                                                                                                    new BinaryOperator("Workplace", _locPurchaseInvoiceLine.Workplace),
                                                                                                                    new BinaryOperator("JournalMap", _locJournalMapItem),
                                                                                                                    new BinaryOperator("Active", true)));

                                                                if (_locJournalMapLines != null && _locJournalMapLines.Count() > 0)
                                                                {
                                                                    foreach (JournalMapLine _locJournalMapLine in _locJournalMapLines)
                                                                    {
                                                                        if (_locJournalMapLine.AccountMap != null)
                                                                        {
                                                                            if (_locJournalMapLine.AccountMap.PostingType == PostingType.Purchase && _locJournalMapLine.AccountMap.OrderType == OrderType.Item
                                                                                && _locJournalMapLine.AccountMap.PostingMethod == PostingMethod.InvoiceAP && _locJournalMapLine.AccountMap.PostingMethodType == PostingMethodType.Tax)
                                                                            {
                                                                                XPCollection<AccountMapLine> _locAccountMapLines = new XPCollection<AccountMapLine>
                                                                                                                               (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                                                new BinaryOperator("Company", _locPurchaseInvoiceLine.Company),
                                                                                                                                new BinaryOperator("Workplace", _locPurchaseInvoiceLine.Workplace),
                                                                                                                                new BinaryOperator("AccountMap", _locJournalMapLine.AccountMap),
                                                                                                                                new BinaryOperator("Active", true)));

                                                                                if (_locAccountMapLines != null && _locAccountMapLines.Count() > 0)
                                                                                {
                                                                                    double _locTotalAmountDebit = 0;
                                                                                    double _locTotalAmountCredit = 0;
                                                                                    double _locTotalBalance = 0;

                                                                                    foreach (AccountMapLine _locAccountMapLine in _locAccountMapLines)
                                                                                    {
                                                                                        if (_locAccountMapLine.AccountCharge == AccountCharge.Debit)
                                                                                        {
                                                                                            _locTotalAmountDebit = _locTotalTaxAmount;
                                                                                        }
                                                                                        if (_locAccountMapLine.AccountCharge == AccountCharge.Credit)
                                                                                        {
                                                                                            _locTotalAmountCredit = _locTotalTaxAmount;
                                                                                        }

                                                                                        GeneralJournal _saveGeneralJournal = new GeneralJournal(_currSession)
                                                                                        {
                                                                                            Company = _locPurchaseInvoiceLine.Company,
                                                                                            Workplace = _locPurchaseInvoiceLine.Workplace,
                                                                                            PostingDate = now,
                                                                                            PostingType = PostingType.Purchase,
                                                                                            PostingMethod = PostingMethod.InvoiceAP,
                                                                                            PostingMethodType = PostingMethodType.Tax,
                                                                                            Account = _locAccountMapLine.Account,
                                                                                            Debit = _locTotalAmountDebit,
                                                                                            Credit = _locTotalAmountCredit,
                                                                                            JournalMonth = _locPurchaseInvoiceLine.JournalMonth,
                                                                                            JournalYear = _locPurchaseInvoiceLine.JournalYear,
                                                                                            PurchaseInvoice = _locPurchaseInvoiceXPO,
                                                                                        };

                                                                                        _saveGeneralJournal.Save();
                                                                                        _saveGeneralJournal.Session.CommitTransaction();

                                                                                        #region AccountingPeriodicLine

                                                                                        if (_locAccountMapLine.Account.Code != null)
                                                                                        {
                                                                                            //Cari Account Periodic Line
                                                                                            AccountingPeriodicLine _locAPL = _currSession.FindObject<AccountingPeriodicLine>
                                                                                                                            (new GroupOperator(GroupOperatorType.And,
                                                                                                                            new BinaryOperator("Company", _locPurchaseInvoiceLine.Company),
                                                                                                                            new BinaryOperator("Workplace", _locPurchaseInvoiceLine.Workplace),
                                                                                                                            new BinaryOperator("AccountNo", _locAccountMapLine.Account),
                                                                                                                            new BinaryOperator("Month", _locPurchaseInvoiceLine.JournalMonth),
                                                                                                                            new BinaryOperator("Year", _locPurchaseInvoiceLine.JournalYear)));
                                                                                            if (_locAPL != null)
                                                                                            {
                                                                                                if (_locAPL.AccountNo != null)
                                                                                                {
                                                                                                    if (_locAPL.AccountNo.BalanceType == BalanceType.Change)
                                                                                                    {
                                                                                                        if (_locAPL.AccountNo.AccountCharge == AccountCharge.Debit)
                                                                                                        {
                                                                                                            if (_locTotalAmountDebit > 0)
                                                                                                            {
                                                                                                                _locTotalBalance = _locAPL.Balance + _locTotalAmountDebit;
                                                                                                            }
                                                                                                            if (_locTotalAmountCredit > 0)
                                                                                                            {
                                                                                                                _locTotalBalance = _locAPL.Balance - _locTotalAmountCredit;
                                                                                                            }
                                                                                                        }
                                                                                                        if (_locAPL.AccountNo.AccountCharge == AccountCharge.Credit)
                                                                                                        {
                                                                                                            if (_locTotalAmountDebit > 0)
                                                                                                            {
                                                                                                                _locTotalBalance = _locAPL.Balance - _locTotalAmountDebit;
                                                                                                            }
                                                                                                            if (_locTotalAmountCredit > 0)
                                                                                                            {
                                                                                                                _locTotalBalance = _locAPL.Balance + _locTotalAmountCredit;
                                                                                                            }
                                                                                                        }
                                                                                                    }

                                                                                                    _locAPL.Balance = _locTotalBalance;
                                                                                                    _locAPL.Debit = _locAPL.Debit + _locTotalAmountDebit;
                                                                                                    _locAPL.Credit = _locAPL.Credit + _locTotalAmountCredit;
                                                                                                    _locAPL.Save();
                                                                                                    _locAPL.Session.CommitTransaction();

                                                                                                }
                                                                                            }
                                                                                        }
                                                                                        #endregion AccountingPeriodicLine
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        #endregion MultiTax=falseTaxRule=BeforeDiscountOrNone
                                        #endregion TaxJournal
                                        #region DiscountJournal
                                        if (_locPurchaseInvoiceLine.Discount != null)
                                        {
                                            _locTotalDiscount = _locPurchaseInvoiceLine.DiscAmount;

                                            if (_locPurchaseInvoiceLine.Discount.DiscountAccountGroup != null)
                                            {
                                                _locDiscountAccountGroup = _locPurchaseInvoiceLine.Discount.DiscountAccountGroup;

                                                XPCollection<JournalMap> _locJournalMapItems = new XPCollection<JournalMap>
                                                                                   (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                    new BinaryOperator("Company", _locPurchaseInvoiceLine.Company),
                                                                                    new BinaryOperator("Workplace", _locPurchaseInvoiceLine.Workplace),
                                                                                    new BinaryOperator("DiscountAccountGroup", _locDiscountAccountGroup),
                                                                                    new BinaryOperator("Active", true)));

                                                if (_locJournalMapItems != null && _locJournalMapItems.Count() > 0)
                                                {
                                                    foreach (JournalMap _locJournalMapItem in _locJournalMapItems)
                                                    {
                                                        XPCollection<JournalMapLine> _locJournalMapLines = new XPCollection<JournalMapLine>
                                                                                                           (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                            new BinaryOperator("Company", _locPurchaseInvoiceLine.Company),
                                                                                                            new BinaryOperator("Workplace", _locPurchaseInvoiceLine.Workplace),
                                                                                                            new BinaryOperator("JournalMap", _locJournalMapItem),
                                                                                                            new BinaryOperator("Active", true)));

                                                        if (_locJournalMapLines != null && _locJournalMapLines.Count() > 0)
                                                        {
                                                            foreach (JournalMapLine _locJournalMapLine in _locJournalMapLines)
                                                            {
                                                                if (_locJournalMapLine.AccountMap != null)
                                                                {
                                                                    if (_locJournalMapLine.AccountMap.PostingType == PostingType.Purchase && _locJournalMapLine.AccountMap.OrderType == OrderType.Item
                                                                        && _locJournalMapLine.AccountMap.PostingMethod == PostingMethod.InvoiceAP && _locJournalMapLine.AccountMap.PostingMethodType == PostingMethodType.Discount)
                                                                    {
                                                                        XPCollection<AccountMapLine> _locAccountMapLines = new XPCollection<AccountMapLine>
                                                                                                                       (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                                        new BinaryOperator("Company", _locPurchaseInvoiceLine.Company),
                                                                                                                        new BinaryOperator("Workplace", _locPurchaseInvoiceLine.Workplace),
                                                                                                                        new BinaryOperator("AccountMap", _locJournalMapLine.AccountMap),
                                                                                                                        new BinaryOperator("Active", true)));

                                                                        if (_locAccountMapLines != null && _locAccountMapLines.Count() > 0)
                                                                        {
                                                                            double _locTotalAmountDebit = 0;
                                                                            double _locTotalAmountCredit = 0;
                                                                            double _locTotalBalance = 0;

                                                                            foreach (AccountMapLine _locAccountMapLine in _locAccountMapLines)
                                                                            {
                                                                                if (_locAccountMapLine.AccountCharge == AccountCharge.Debit)
                                                                                {
                                                                                    _locTotalAmountDebit = _locTotalDiscount;
                                                                                }
                                                                                if (_locAccountMapLine.AccountCharge == AccountCharge.Credit)
                                                                                {
                                                                                    _locTotalAmountCredit = _locTotalDiscount;
                                                                                }

                                                                                GeneralJournal _saveGeneralJournal = new GeneralJournal(_currSession)
                                                                                {
                                                                                    Company = _locPurchaseInvoiceLine.Company,
                                                                                    Workplace = _locPurchaseInvoiceLine.Workplace,
                                                                                    PostingDate = now,
                                                                                    PostingType = PostingType.Purchase,
                                                                                    OrderType = OrderType.Item,
                                                                                    PostingMethod = PostingMethod.InvoiceAP,
                                                                                    PostingMethodType = PostingMethodType.Discount,
                                                                                    Account = _locAccountMapLine.Account,
                                                                                    Debit = _locTotalAmountDebit,
                                                                                    Credit = _locTotalAmountCredit,
                                                                                    JournalMonth = _locPurchaseInvoiceLine.JournalMonth,
                                                                                    JournalYear = _locPurchaseInvoiceLine.JournalYear,
                                                                                    PurchaseInvoice = _locPurchaseInvoiceXPO,
                                                                                };

                                                                                _saveGeneralJournal.Save();
                                                                                _saveGeneralJournal.Session.CommitTransaction();

                                                                                #region AccountingPeriodicLine

                                                                                if (_locAccountMapLine.Account.Code != null)
                                                                                {
                                                                                    //Cari Account Periodic Line
                                                                                    AccountingPeriodicLine _locAPL = _currSession.FindObject<AccountingPeriodicLine>
                                                                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                                                                    new BinaryOperator("Company", _locPurchaseInvoiceLine.Company),
                                                                                                                    new BinaryOperator("Workplace", _locPurchaseInvoiceLine.Workplace),
                                                                                                                    new BinaryOperator("AccountNo", _locAccountMapLine.Account),
                                                                                                                    new BinaryOperator("Month", _locPurchaseInvoiceLine.JournalMonth),
                                                                                                                    new BinaryOperator("Year", _locPurchaseInvoiceLine.JournalYear)));
                                                                                    if (_locAPL != null)
                                                                                    {
                                                                                        if (_locAPL.AccountNo != null)
                                                                                        {
                                                                                            if (_locAPL.AccountNo.BalanceType == BalanceType.Change)
                                                                                            {
                                                                                                if (_locAPL.AccountNo.AccountCharge == AccountCharge.Debit)
                                                                                                {
                                                                                                    if (_locTotalAmountDebit > 0)
                                                                                                    {
                                                                                                        _locTotalBalance = _locAPL.Balance + _locTotalAmountDebit;
                                                                                                    }
                                                                                                    if (_locTotalAmountCredit > 0)
                                                                                                    {
                                                                                                        _locTotalBalance = _locAPL.Balance - _locTotalAmountCredit;
                                                                                                    }
                                                                                                }
                                                                                                if (_locAPL.AccountNo.AccountCharge == AccountCharge.Credit)
                                                                                                {
                                                                                                    if (_locTotalAmountDebit > 0)
                                                                                                    {
                                                                                                        _locTotalBalance = _locAPL.Balance - _locTotalAmountDebit;
                                                                                                    }
                                                                                                    if (_locTotalAmountCredit > 0)
                                                                                                    {
                                                                                                        _locTotalBalance = _locAPL.Balance + _locTotalAmountCredit;
                                                                                                    }
                                                                                                }
                                                                                            }

                                                                                            _locAPL.Balance = _locTotalBalance;
                                                                                            _locAPL.Debit = _locAPL.Debit + _locTotalAmountDebit;
                                                                                            _locAPL.Credit = _locAPL.Credit + _locTotalAmountCredit;
                                                                                            _locAPL.Save();
                                                                                            _locAPL.Session.CommitTransaction();

                                                                                        }
                                                                                    }
                                                                                }
                                                                                #endregion AccountingPeriodicLine
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        #endregion DiscountJournal  
                                    }
                                }
                            }

                            #region JournalMapBusinessPartnerAccountGroup

                            if(_locPurchaseInvoiceLine.PurchaseInvoice.PayToVendor != null)
                            {
                                BusinessPartnerAccount _locBusinessPartnerAccount = _currSession.FindObject<BusinessPartnerAccount>(new GroupOperator(GroupOperatorType.And,
                                                                                        new BinaryOperator("Company", _locPurchaseInvoiceLine.Company),
                                                                                        new BinaryOperator("Workplace", _locPurchaseInvoiceLine.Workplace),
                                                                                        new BinaryOperator("BusinessPartner", _locPurchaseInvoiceLine.PurchaseInvoice.PayToVendor),
                                                                                        new BinaryOperator("Active", true)));
                                if(_locBusinessPartnerAccount != null)
                                {
                                    if(_locBusinessPartnerAccount.BusinessPartnerAccountGroup != null)
                                    {
                                        _locBusinessPartnerAccountGroup = _locBusinessPartnerAccount.BusinessPartnerAccountGroup;

                                        _locTotalAmountBP = _locPurchaseInvoiceLine.TUAmount;

                                        XPCollection<JournalMap> _locJournalMapItems = new XPCollection<JournalMap>
                                                                           (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                            new BinaryOperator("Company", _locPurchaseInvoiceLine.Company),
                                                                            new BinaryOperator("Workplace", _locPurchaseInvoiceLine.Workplace),
                                                                            new BinaryOperator("BusinessPartnerAccountGroup", _locBusinessPartnerAccountGroup),
                                                                            new BinaryOperator("Active", true)));

                                        if (_locJournalMapItems != null && _locJournalMapItems.Count() > 0)
                                        {
                                            foreach (JournalMap _locJournalMapItem in _locJournalMapItems)
                                            {
                                                XPCollection<JournalMapLine> _locJournalMapLines = new XPCollection<JournalMapLine>
                                                                                                   (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                    new BinaryOperator("Company", _locPurchaseInvoiceLine.Company),
                                                                                                    new BinaryOperator("Workplace", _locPurchaseInvoiceLine.Workplace),
                                                                                                    new BinaryOperator("JournalMap", _locJournalMapItem),
                                                                                                    new BinaryOperator("Active", true)));

                                                if (_locJournalMapLines != null && _locJournalMapLines.Count() > 0)
                                                {
                                                    foreach (JournalMapLine _locJournalMapLine in _locJournalMapLines)
                                                    {
                                                        if (_locJournalMapLine.AccountMap != null)
                                                        {
                                                            #region NormalAmount
                                                            if (_locJournalMapLine.AccountMap.PostingType == PostingType.Purchase && _locJournalMapLine.AccountMap.OrderType == OrderType.Item
                                                                && _locJournalMapLine.AccountMap.PostingMethod == PostingMethod.InvoiceAP && _locJournalMapLine.AccountMap.PostingMethodType == PostingMethodType.Normal)
                                                            {
                                                                XPCollection<AccountMapLine> _locAccountMapLines = new XPCollection<AccountMapLine>
                                                                                                               (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                                new BinaryOperator("Company", _locPurchaseInvoiceLine.Company),
                                                                                                                new BinaryOperator("Workplace", _locPurchaseInvoiceLine.Workplace),
                                                                                                                new BinaryOperator("AccountMap", _locJournalMapLine.AccountMap),
                                                                                                                new BinaryOperator("Active", true)));

                                                                if (_locAccountMapLines != null && _locAccountMapLines.Count() > 0)
                                                                {
                                                                    double _locTotalAmountDebit = 0;
                                                                    double _locTotalAmountCredit = 0;
                                                                    double _locTotalBalance = 0;

                                                                    foreach (AccountMapLine _locAccountMapLine in _locAccountMapLines)
                                                                    {
                                                                        if (_locAccountMapLine.AccountCharge == AccountCharge.Debit)
                                                                        {
                                                                            _locTotalAmountDebit = _locTotalAmountBP;
                                                                        }
                                                                        if (_locAccountMapLine.AccountCharge == AccountCharge.Credit)
                                                                        {
                                                                            _locTotalAmountCredit = _locTotalAmountBP;
                                                                        }

                                                                        GeneralJournal _saveGeneralJournal = new GeneralJournal(_currSession)
                                                                        {
                                                                            Company = _locPurchaseInvoiceLine.Company,
                                                                            Workplace = _locPurchaseInvoiceLine.Workplace,
                                                                            PostingDate = now,
                                                                            PostingType = PostingType.Purchase,
                                                                            OrderType = OrderType.Item,
                                                                            PostingMethod = PostingMethod.InvoiceAP,
                                                                            PostingMethodType = PostingMethodType.Normal,
                                                                            Account = _locAccountMapLine.Account,
                                                                            Debit = _locTotalAmountDebit,
                                                                            Credit = _locTotalAmountCredit,
                                                                            JournalMonth = _locPurchaseInvoiceLine.JournalMonth,
                                                                            JournalYear = _locPurchaseInvoiceLine.JournalYear,
                                                                            PurchaseInvoice = _locPurchaseInvoiceXPO,
                                                                        };

                                                                        _saveGeneralJournal.Save();
                                                                        _saveGeneralJournal.Session.CommitTransaction();

                                                                        #region AccountingPeriodicLine

                                                                        if (_locAccountMapLine.Account.Code != null)
                                                                        {
                                                                            //Cari Account Periodic Line
                                                                            AccountingPeriodicLine _locAPL = _currSession.FindObject<AccountingPeriodicLine>
                                                                                                            (new GroupOperator(GroupOperatorType.And,
                                                                                                            new BinaryOperator("Company", _locPurchaseInvoiceLine.Company),
                                                                                                            new BinaryOperator("Workplace", _locPurchaseInvoiceLine.Workplace),
                                                                                                            new BinaryOperator("AccountNo", _locAccountMapLine.Account),
                                                                                                            new BinaryOperator("Month", _locPurchaseInvoiceLine.JournalMonth),
                                                                                                            new BinaryOperator("Year", _locPurchaseInvoiceLine.JournalYear)));
                                                                            if (_locAPL != null)
                                                                            {
                                                                                if (_locAPL.AccountNo != null)
                                                                                {
                                                                                    if (_locAPL.AccountNo.BalanceType == BalanceType.Change)
                                                                                    {
                                                                                        if (_locAPL.AccountNo.AccountCharge == AccountCharge.Debit)
                                                                                        {
                                                                                            if (_locTotalAmountDebit > 0)
                                                                                            {
                                                                                                _locTotalBalance = _locAPL.Balance + _locTotalAmountDebit;
                                                                                            }
                                                                                            if (_locTotalAmountCredit > 0)
                                                                                            {
                                                                                                _locTotalBalance = _locAPL.Balance - _locTotalAmountCredit;
                                                                                            }
                                                                                        }
                                                                                        if (_locAPL.AccountNo.AccountCharge == AccountCharge.Credit)
                                                                                        {
                                                                                            if (_locTotalAmountDebit > 0)
                                                                                            {
                                                                                                _locTotalBalance = _locAPL.Balance - _locTotalAmountDebit;
                                                                                            }
                                                                                            if (_locTotalAmountCredit > 0)
                                                                                            {
                                                                                                _locTotalBalance = _locAPL.Balance + _locTotalAmountCredit;
                                                                                            }
                                                                                        }
                                                                                    }

                                                                                    _locAPL.Balance = _locTotalBalance;
                                                                                    _locAPL.Debit = _locAPL.Debit + _locTotalAmountDebit;
                                                                                    _locAPL.Credit = _locAPL.Credit + _locTotalAmountCredit;
                                                                                    _locAPL.Save();
                                                                                    _locAPL.Session.CommitTransaction();

                                                                                }
                                                                            }
                                                                        }
                                                                        #endregion AccountingPeriodicLine
                                                                    }
                                                                }
                                                            }
                                                            #endregion NormalAmount
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }

                            #endregion JournalMapBusinessPartnerAccountGroup

                            #region JournalMapCompanyAccountGroup
                            if (_locPurchaseInvoiceLine.Company != null)
                            {
                                _locTotalAmountCompany = _locPurchaseInvoiceLine.TUAmount;

                                CompanyAccount _locCompanyAccount = _currSession.FindObject<CompanyAccount>(new GroupOperator(GroupOperatorType.And,
                                                        new BinaryOperator("Company", _locPurchaseInvoiceLine.Company),
                                                        new BinaryOperator("Workplace", _locPurchaseInvoiceLine.Workplace),
                                                        new BinaryOperator("Active", true)));

                                if (_locCompanyAccount != null)
                                {
                                    if (_locCompanyAccount.CompanyAccountGroup != null)
                                    {
                                        _locCompanyAccountGroup = _locCompanyAccount.CompanyAccountGroup;

                                        XPCollection<JournalMap> _locJournalMapItems = new XPCollection<JournalMap>
                                                                       (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                        new BinaryOperator("Company", _locPurchaseInvoiceLine.Company),
                                                                        new BinaryOperator("Workplace", _locPurchaseInvoiceLine.Workplace),
                                                                        new BinaryOperator("CompanyAccountGroup", _locCompanyAccountGroup),
                                                                        new BinaryOperator("Active", true)));

                                        if (_locJournalMapItems != null && _locJournalMapItems.Count() > 0)
                                        {
                                            foreach (JournalMap _locJournalMapItem in _locJournalMapItems)
                                            {
                                                XPCollection<JournalMapLine> _locJournalMapLines = new XPCollection<JournalMapLine>
                                                                                                   (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                    new BinaryOperator("Company", _locPurchaseInvoiceLine.Company),
                                                                                                    new BinaryOperator("Workplace", _locPurchaseInvoiceLine.Workplace),
                                                                                                    new BinaryOperator("JournalMap", _locJournalMapItem),
                                                                                                    new BinaryOperator("Active", true)));

                                                if (_locJournalMapLines != null && _locJournalMapLines.Count() > 0)
                                                {
                                                    foreach (JournalMapLine _locJournalMapLine in _locJournalMapLines)
                                                    {
                                                        if (_locJournalMapLine.AccountMap != null)
                                                        {
                                                            #region NormalAmount
                                                            if (_locJournalMapLine.AccountMap.PostingType == PostingType.Purchase && _locJournalMapLine.AccountMap.OrderType == OrderType.Item
                                                                && _locJournalMapLine.AccountMap.PostingMethod == PostingMethod.InvoiceAP && _locJournalMapLine.AccountMap.PostingMethodType == PostingMethodType.Normal)
                                                            {
                                                                XPCollection<AccountMapLine> _locAccountMapLines = new XPCollection<AccountMapLine>
                                                                                                               (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                                new BinaryOperator("Company", _locPurchaseInvoiceLine.Company),
                                                                                                                new BinaryOperator("Workplace", _locPurchaseInvoiceLine.Workplace),
                                                                                                                new BinaryOperator("AccountMap", _locJournalMapLine.AccountMap),
                                                                                                                new BinaryOperator("Active", true)));

                                                                if (_locAccountMapLines != null && _locAccountMapLines.Count() > 0)
                                                                {
                                                                    double _locTotalAmountDebit = 0;
                                                                    double _locTotalAmountCredit = 0;
                                                                    double _locTotalBalance = 0;

                                                                    foreach (AccountMapLine _locAccountMapLine in _locAccountMapLines)
                                                                    {
                                                                        if (_locAccountMapLine.AccountCharge == AccountCharge.Debit)
                                                                        {
                                                                            _locTotalAmountDebit = _locTotalAmountCompany;
                                                                        }
                                                                        if (_locAccountMapLine.AccountCharge == AccountCharge.Credit)
                                                                        {
                                                                            _locTotalAmountCredit = _locTotalAmountCompany;
                                                                        }

                                                                        GeneralJournal _saveGeneralJournal = new GeneralJournal(_currSession)
                                                                        {
                                                                            Company = _locPurchaseInvoiceLine.Company,
                                                                            Workplace = _locPurchaseInvoiceLine.Workplace,
                                                                            PostingDate = now,
                                                                            PostingType = PostingType.Purchase,
                                                                            OrderType = OrderType.Item,
                                                                            PostingMethod = PostingMethod.Receipt,
                                                                            PostingMethodType = PostingMethodType.Normal,
                                                                            Account = _locAccountMapLine.Account,
                                                                            Debit = _locTotalAmountDebit,
                                                                            Credit = _locTotalAmountCredit,
                                                                            JournalMonth = _locPurchaseInvoiceLine.JournalMonth,
                                                                            JournalYear = _locPurchaseInvoiceLine.JournalYear,
                                                                            PurchaseInvoice = _locPurchaseInvoiceXPO,
                                                                        };

                                                                        _saveGeneralJournal.Save();
                                                                        _saveGeneralJournal.Session.CommitTransaction();

                                                                        #region AccountingPeriodicLine

                                                                        if (_locAccountMapLine.Account.Code != null)
                                                                        {
                                                                            //Cari Account Periodic Line
                                                                            AccountingPeriodicLine _locAPL = _currSession.FindObject<AccountingPeriodicLine>
                                                                                                            (new GroupOperator(GroupOperatorType.And,
                                                                                                            new BinaryOperator("Company", _locPurchaseInvoiceLine.Company),
                                                                                                            new BinaryOperator("Workplace", _locPurchaseInvoiceLine.Workplace),
                                                                                                            new BinaryOperator("AccountNo", _locAccountMapLine.Account),
                                                                                                            new BinaryOperator("Month", _locPurchaseInvoiceLine.JournalMonth),
                                                                                                            new BinaryOperator("Year", _locPurchaseInvoiceLine.JournalYear)));
                                                                            if (_locAPL != null)
                                                                            {
                                                                                if (_locAPL.AccountNo != null)
                                                                                {
                                                                                    if (_locAPL.AccountNo.BalanceType == BalanceType.Change)
                                                                                    {
                                                                                        if (_locAPL.AccountNo.AccountCharge == AccountCharge.Debit)
                                                                                        {
                                                                                            if (_locTotalAmountDebit > 0)
                                                                                            {
                                                                                                _locTotalBalance = _locAPL.Balance + _locTotalAmountDebit;
                                                                                            }
                                                                                            if (_locTotalAmountCredit > 0)
                                                                                            {
                                                                                                _locTotalBalance = _locAPL.Balance - _locTotalAmountCredit;
                                                                                            }
                                                                                        }
                                                                                        if (_locAPL.AccountNo.AccountCharge == AccountCharge.Credit)
                                                                                        {
                                                                                            if (_locTotalAmountDebit > 0)
                                                                                            {
                                                                                                _locTotalBalance = _locAPL.Balance - _locTotalAmountDebit;
                                                                                            }
                                                                                            if (_locTotalAmountCredit > 0)
                                                                                            {
                                                                                                _locTotalBalance = _locAPL.Balance + _locTotalAmountCredit;
                                                                                            }
                                                                                        }
                                                                                    }

                                                                                    _locAPL.Balance = _locTotalBalance;
                                                                                    _locAPL.Debit = _locAPL.Debit + _locTotalAmountDebit;
                                                                                    _locAPL.Credit = _locAPL.Credit + _locTotalAmountCredit;
                                                                                    _locAPL.Save();
                                                                                    _locAPL.Session.CommitTransaction();

                                                                                }
                                                                            }
                                                                        }
                                                                        #endregion AccountingPeriodicLine
                                                                    }
                                                                }
                                                            }
                                                            #endregion NormalAmount
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            #endregion JournalMapCompanyAccountGroup
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = PurchaseInvoice ", ex.ToString());
            }
        }

        #endregion JournalInvoiceAP

        #region GetTotalTaxJournal

        private double GetSumTotalTaxValueAfterDiscountJournal(Session _currSession, PurchaseInvoiceLine _locPurchaseInvoiceLineXPO)
        {
            double _result = 0;
            try
            {
                double _locTxValue = 0;

                if (_locPurchaseInvoiceLineXPO != null)
                {
                    XPCollection<TaxLine> _locTaxLines = new XPCollection<TaxLine>(_currSession,
                                                     new BinaryOperator("PurchaseInvoiceLine", _locPurchaseInvoiceLineXPO));

                    if (_locTaxLines.Count() > 0)
                    {
                        foreach (TaxLine _locTaxLine in _locTaxLines)
                        {
                            if (_locTaxLine.TxValue > 0)
                            {
                                if (_locTaxLine.TaxNature == TaxNature.Increase)
                                {
                                    _locTxValue = _locTxValue + _locTaxLine.TxValue;
                                }
                                else if (_locTaxLine.TaxNature == TaxNature.Decrease)
                                {
                                    _locTxValue = _locTxValue - _locTaxLine.TxValue;
                                }
                            }
                        }
                        _result = _locTxValue;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("BusinessObject = PurchaseInvoice " + ex.ToString());
            }
            return _result;
        }

        private double GetSumTotalTaxAmountAfterDiscountJournal(Session _currSession, PurchaseInvoiceLine _locPurchaseInvoiceLineXPO)
        {
            double _return = 0;
            try
            {
                double _locTxAmount = 0;

                if (_locPurchaseInvoiceLineXPO != null)
                {
                    XPCollection<TaxLine> _locTaxLines = new XPCollection<TaxLine>(_currSession,
                                                     new BinaryOperator("PurchaseInvoiceLine", _locPurchaseInvoiceLineXPO));

                    if (_locTaxLines.Count() > 0)
                    {
                        foreach (TaxLine _locTaxLine in _locTaxLines)
                        {
                            if (_locTaxLine.TxValue > 0)
                            {
                                if (_locTaxLine.TaxNature == TaxNature.Increase)
                                {
                                    _locTxAmount = _locTxAmount + _locTaxLine.TxValue / 100 * (_locPurchaseInvoiceLineXPO.TUAmount - _locPurchaseInvoiceLineXPO.DiscAmount);
                                }
                                else if (_locTaxLine.TaxNature == TaxNature.Decrease)
                                {
                                    _locTxAmount = _locTxAmount - _locTaxLine.TxValue / 100 * (_locPurchaseInvoiceLineXPO.TUAmount - _locPurchaseInvoiceLineXPO.DiscAmount);
                                }
                            }
                        }
                        _return = _locTxAmount;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("BusinessObject = PurchaseInvoice" + ex.ToString());
            }
            return _return;
        }

        private double GetSumTotalTaxValueBeforeDiscountJournal(Session _currSession, PurchaseInvoiceLine _locPurchaseInvoiceLineXPO)
        {
            double _result = 0;
            try
            {
                double _locTxValue = 0;

                if (_locPurchaseInvoiceLineXPO != null)
                {
                    XPCollection<TaxLine> _locTaxLines = new XPCollection<TaxLine>(_currSession,
                                                     new BinaryOperator("PurchaseInvoiceLine", _locPurchaseInvoiceLineXPO));

                    if (_locTaxLines.Count() > 0)
                    {
                        foreach (TaxLine _locTaxLine in _locTaxLines)
                        {
                            if (_locTaxLine.TxValue > 0 && _locTaxLine.TxAmount > 0)
                            {
                                if (_locTaxLine.TaxNature == TaxNature.Increase)
                                {
                                    _locTxValue = _locTxValue + _locTaxLine.TxValue;
                                }
                                else if (_locTaxLine.TaxNature == TaxNature.Decrease)
                                {
                                    _locTxValue = _locTxValue - _locTaxLine.TxValue;
                                }
                            }
                        }
                        _result = _locTxValue;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("BusinessObject = PurchaseInvoice " + ex.ToString());
            }
            return _result;
        }

        private double GetSumTotalTaxAmountBeforeDiscountJournal(Session _currSession, PurchaseInvoiceLine _locPurchaseInvoiceLineXPO)
        {
            double _return = 0;
            try
            {
                double _locTxAmount = 0;

                if (_locPurchaseInvoiceLineXPO != null)
                {
                    XPCollection<TaxLine> _locTaxLines = new XPCollection<TaxLine>(_currSession,
                                                     new BinaryOperator("PurchaseInvoiceLine", _locPurchaseInvoiceLineXPO));

                    if (_locTaxLines.Count() > 0)
                    {
                        foreach (TaxLine _locTaxLine in _locTaxLines)
                        {
                            if (_locTaxLine.TxValue > 0)
                            {
                                if (_locTaxLine.TaxNature == TaxNature.Increase)
                                {
                                    _locTxAmount = _locTxAmount + (_locPurchaseInvoiceLineXPO.UAmount * _locPurchaseInvoiceLineXPO.TQty * _locTaxLine.TxValue / 100);
                                }
                                else if (_locTaxLine.TaxNature == TaxNature.Decrease)
                                {
                                    _locTxAmount = _locTxAmount - (_locPurchaseInvoiceLineXPO.UAmount * _locPurchaseInvoiceLineXPO.TQty * _locTaxLine.TxValue / 100);
                                }
                            }
                        }
                        _return = _locTxAmount;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("BusinessObject = PurchaseInvoice" + ex.ToString());
            }
            return _return;
        }

        #endregion GetTotalTaxJournal

        private void SetStatusPurchaseInvoiceLine(Session _currSession, PurchaseInvoice _locPurchaseInvoiceXPO)
        {
            try
            {
                DateTime now = DateTime.Now;
                if (_locPurchaseInvoiceXPO != null)
                {
                    XPCollection<PurchaseInvoiceLine> _locPurchaseInvoiceLines = new XPCollection<PurchaseInvoiceLine>(_currSession,
                                                            new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("PurchaseInvoice", _locPurchaseInvoiceXPO),
                                                            new BinaryOperator("Select", true),
                                                            new GroupOperator(GroupOperatorType.Or,
                                                            new BinaryOperator("Status", Status.Progress),
                                                            new BinaryOperator("Status", Status.Posted))));

                    if (_locPurchaseInvoiceLines != null && _locPurchaseInvoiceLines.Count > 0)
                    {
                        foreach (PurchaseInvoiceLine _locPurchaseInvoiceLine in _locPurchaseInvoiceLines)
                        {
                            _locPurchaseInvoiceLine.Status = Status.Close;
                            _locPurchaseInvoiceLine.ActivationPosting = true;
                            _locPurchaseInvoiceLine.StatusDate = now;
                            _locPurchaseInvoiceLine.Save();
                            _locPurchaseInvoiceLine.Session.CommitTransaction();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PurchaseInvoice " + ex.ToString());
            }
        }

        private void SetNormalQuantityInPostPIM(Session _currSession, PurchaseInvoice _locPurchaseInvoiceXPO)
        {
            try
            {
                if (_locPurchaseInvoiceXPO != null)
                {
                    XPCollection<PurchaseInvoiceLine> _locPurchaseInvoiceLines = new XPCollection<PurchaseInvoiceLine>(_currSession,
                                                                                     new GroupOperator(GroupOperatorType.And,
                                                                                     new BinaryOperator("PurchaseInvoice", _locPurchaseInvoiceXPO),
                                                                                     new BinaryOperator("Select", true),
                                                                                     new GroupOperator(GroupOperatorType.Or,
                                                                                     new BinaryOperator("Status", Status.Progress),
                                                                                     new BinaryOperator("Status", Status.Posted),
                                                                                     new BinaryOperator("Status", Status.Close))));

                    if (_locPurchaseInvoiceLines != null && _locPurchaseInvoiceLines.Count > 0)
                    {
                        foreach (PurchaseInvoiceLine _locPurchaseInvoiceLine in _locPurchaseInvoiceLines)
                        {
                            if (_locPurchaseInvoiceLine.DQty > 0 || _locPurchaseInvoiceLine.Qty > 0)
                            {
                                _locPurchaseInvoiceLine.Select = false;
                                _locPurchaseInvoiceLine.Save();
                                _locPurchaseInvoiceLine.Session.CommitTransaction();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PurchaseInvoice " + ex.ToString());
            }
        }

        private void SetFinalPurchaseInvoice(Session _currSession, PurchaseInvoice _locPurchaseInvoiceXPO)
        {
            try
            {
                DateTime now = DateTime.Now;

                if (_locPurchaseInvoiceXPO != null)
                {
                    XPCollection<PurchaseInvoiceLine> _locPurchaseInvoiceLines = new XPCollection<PurchaseInvoiceLine>(_currSession,
                                                            new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("PurchaseInvoice", _locPurchaseInvoiceXPO)));

                    if (_locPurchaseInvoiceLines != null && _locPurchaseInvoiceLines.Count() > 0)
                    {
                        foreach (PurchaseInvoiceLine _locPurchaseInvoiceLine in _locPurchaseInvoiceLines)
                        {
                            if (_locPurchaseInvoiceLine.Status == Status.Close)
                            {
                                _locPurchaseInvoiceXPO.ActivationPosting = true;
                                _locPurchaseInvoiceXPO.Status = Status.Posted;
                                _locPurchaseInvoiceXPO.StatusDate = now;
                                _locPurchaseInvoiceXPO.Save();
                                _locPurchaseInvoiceXPO.Session.CommitTransaction();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PurchaseInvoice " + ex.ToString());
            }
        }

        #endregion Posting Method

        #region Approval

        private void SetApprovalLine(Session _currentSession, PurchaseInvoice _locPurchaseInvoiceXPO, ApprovalLevel _locApprovalLevel)
        {
            try
            {
                DateTime now = DateTime.Now;

                ApprovalLine _locApprovalLineXPO = _currentSession.FindObject<ApprovalLine>
                                                                        (new GroupOperator(GroupOperatorType.And,
                                                                         new BinaryOperator("PurchaseInvoice", _locPurchaseInvoiceXPO),
                                                                         new BinaryOperator("ApprovalLevel", _locApprovalLevel)));

                if (_locApprovalLineXPO == null)
                {
                    ApprovalLine _saveDataAL2 = new ApprovalLine(_currentSession)
                    {
                        ApprovalDate = now,
                        ApprovalStatus = Status.Approved,
                        ApprovalLevel = _locApprovalLevel,
                        PurchaseInvoice = _locPurchaseInvoiceXPO,
                    };
                    _saveDataAL2.Save();
                    _saveDataAL2.Session.CommitTransaction();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PurchaseInvoice " + ex.ToString());
            }
        }

        #endregion Approval

        #region Email

        private string BackgroundBody(Session _currentSession, PurchaseInvoice _locPurchaseInvoiceXPO, MailSetupDetail _locMailSetDetail)
        {
            string body = string.Empty;

            if (_locPurchaseInvoiceXPO != null)
            {

                ApprovalLine _locApprovalLineXPO = _currentSession.FindObject<ApprovalLine>
                                                   (new GroupOperator(GroupOperatorType.And,
                                                    new BinaryOperator("PurchaseInvoice", _locPurchaseInvoiceXPO)));

                string Status = _locApprovalLineXPO.ApprovalStatus.ToString();

                using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/Email/EmailTemplate.html")))
                {
                    body = reader.ReadToEnd();
                }
                body = body.Replace("{Code}", _locPurchaseInvoiceXPO.Code);

                #region Level
                if (_locPurchaseInvoiceXPO.ActiveApproved1 == true)
                {
                    string Active = "Level 1";
                    body = body.Replace("{Level}", Active);
                }
                else if (_locPurchaseInvoiceXPO.ActiveApproved2 == true)
                {
                    string Active = "Level 2";
                    body = body.Replace("{Level}", Active);
                }
                else if (_locPurchaseInvoiceXPO.ActiveApproved3 == true)
                {
                    string Active = "Level 3";
                    body = body.Replace("{Level}", Active);
                }
                else
                {
                    string Active = "Not Available";
                    body = body.Replace("{Level}", Active);
                }
                #endregion Level

                body = body.Replace("{Status}", Status);
                body = body.Replace("{Message}", _locMailSetDetail.MessageBody);

            }

            return body;
        }

        protected void SendEmail(Session _currentSession, PurchaseInvoice _locPurchaseInvoiceXPO, Status _locStatus, ApprovalLine _locApprovalLineXPO2)
        {
            DateTime now = DateTime.Now;
            var User = SecuritySystem.CurrentUserName;
            GlobalFunction _globFunc = new GlobalFunction();
            UserAccess _numLineUserAccess = _currentSession.FindObject<UserAccess>(new BinaryOperator("UserName", User));
            string _locMessageBody = null;

            try
            {
                if (_locPurchaseInvoiceXPO != null)
                {
                    XPCollection<MailSetupDetail> _locMailSetDetails = new XPCollection<MailSetupDetail>
                                                                       (_currentSession, new GroupOperator(GroupOperatorType.And,
                                                                        new BinaryOperator("ObjectList", ObjectList.PurchaseInvoice),
                                                                        new BinaryOperator("UserAccess", _numLineUserAccess),
                                                                        new BinaryOperator("Active", true)));

                    if (_locMailSetDetails != null && _locMailSetDetails.Count > 0)
                    {
                        foreach (MailSetupDetail _locMailSetDetail in _locMailSetDetails)
                        {
                            if (_locMailSetDetail.MessageBody != null)
                            {
                                if (_locApprovalLineXPO2.EndApproval == true)
                                {
                                    _locMessageBody = this.BackgroundBody(_currentSession, _locPurchaseInvoiceXPO, _locMailSetDetail);
                                }
                                else
                                {
                                    _locMessageBody = this.BackgroundBody(_currentSession, _locPurchaseInvoiceXPO, _locMailSetDetail);
                                }
                            }

                            _globFunc.SetAndSendMail(_locMailSetDetail.SmtpHost, _locMailSetDetail.SmtpPort,
                            _locMailSetDetail.MailFrom, _locMailSetDetail.MailFromPassword, _locMailSetDetail.MailTo, _locMailSetDetail.MailSubject, _locMessageBody);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PurchaseInvoice " + ex.ToString());
            }
        }

        #endregion Email

        #region Global Method

        private void SuccessMessageShow(string _locMessage)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 4000;
            options.Message = string.Format("{0}", _locMessage);
            options.Type = InformationType.Success;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Success";
            options.Win.Type = WinMessageType.Flyout;
            Application.ShowViewStrategy.ShowMessage(options);
        }

        private void ErrorMessageShow(string _locMessage)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 2000;
            options.Message = string.Format(_locMessage);
            options.Type = InformationType.Warning;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Error";
            options.Win.Type = WinMessageType.Alert;
            Application.ShowViewStrategy.ShowMessage(options);
        }

        #endregion Global Method

    }
}
