﻿namespace FullDrive.Module.Controllers
{
    partial class ApplicationImportActionController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.ApplicationImportImportDataAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            // 
            // ApplicationImportImportDataAction
            // 
            this.ApplicationImportImportDataAction.Caption = "Import Data";
            this.ApplicationImportImportDataAction.ConfirmationMessage = null;
            this.ApplicationImportImportDataAction.Id = "ApplicationImportImportDataActionId";
            this.ApplicationImportImportDataAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.ApplicationImport);
            this.ApplicationImportImportDataAction.ToolTip = null;
            this.ApplicationImportImportDataAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.ApplicationImportImportDataAction_Execute);
            // 
            // ApplicationImportActionController
            // 
            this.Actions.Add(this.ApplicationImportImportDataAction);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SimpleAction ApplicationImportImportDataAction;
    }
}
