﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;

using FullDrive.Module.BusinessObjects;
using FullDrive.Module.CustomProcess;
using System.Collections;
using DevExpress.Xpo;
using DevExpress.ExpressApp.Xpo;

namespace FullDrive.Module.Controllers
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class AccountReclassJournalActionController : ViewController
    {
        public AccountReclassJournalActionController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.
        }

        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }

        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        private void AccountReclassJournalProgressAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;

                if (_objectsToProcess != null)
                {
                    foreach (Object obj in _objectsToProcess)
                    {
                        AccountReclassJournal _locAccountReclassJournalOS = (AccountReclassJournal)_objectSpace.GetObject(obj);

                        if (_locAccountReclassJournalOS != null)
                        {
                            if (_locAccountReclassJournalOS.Session != null)
                            {
                                _currSession = _locAccountReclassJournalOS.Session;
                            }

                            if (_locAccountReclassJournalOS.Code != null && _currSession != null)
                            {
                                _currObjectId = _locAccountReclassJournalOS.Code;

                                AccountReclassJournal _locAccountReclassJournalXPO = _currSession.FindObject<AccountReclassJournal>
                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                     new BinaryOperator("Code", _currObjectId),
                                                                     new BinaryOperator("Status", Status.Open)));

                                if (_locAccountReclassJournalXPO != null)
                                {
                                    _locAccountReclassJournalXPO.Status = Status.Progress;
                                    _locAccountReclassJournalXPO.StatusDate = now;
                                    _locAccountReclassJournalXPO.Save();
                                    _locAccountReclassJournalXPO.Session.CommitTransaction();

                                    XPCollection<AccountReclassJournalLine> _locAccountReclassJournalLines = new XPCollection<AccountReclassJournalLine>
                                                           (_currSession, new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("AccountReclassJournal", _locAccountReclassJournalXPO),
                                                            new BinaryOperator("Status", Status.Open)));

                                    if (_locAccountReclassJournalLines != null && _locAccountReclassJournalLines.Count > 0)
                                    {
                                        foreach (AccountReclassJournalLine _locAccountReclassJournalLine in _locAccountReclassJournalLines)
                                        {
                                            _locAccountReclassJournalLine.Status = Status.Progress;
                                            _locAccountReclassJournalLine.StatusDate = now;
                                            _locAccountReclassJournalLine.Save();
                                            _locAccountReclassJournalLine.Session.CommitTransaction();

                                        }
                                    }
                                    SuccessMessageShow(_locAccountReclassJournalXPO.Code + " has been change successfully to Progress");
                                }
                                else
                                {
                                    ErrorMessageShow("Account Reclass Journal Transaction Not Available");
                                }

                            }
                            else
                            {
                                ErrorMessageShow("Account Reclass Journal Transaction Not Available");
                            }
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = AccountReclassJournal " + ex.ToString());
            }
        }

        private void AccountReclassJournalPostingAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;


                if (this.ObjectSpace != null)
                {
                    _currSession = ((XPObjectSpace)this.ObjectSpace).Session;
                }

                if (_objectsToProcess != null)
                {
                    foreach (Object obj in _objectsToProcess)
                    {
                        AccountReclassJournal _locAccountReclassJournalOS = (AccountReclassJournal)_objectSpace.GetObject(obj);

                        if (_locAccountReclassJournalOS != null)
                        {
                            if (_locAccountReclassJournalOS.Session != null)
                            {
                                _currSession = _locAccountReclassJournalOS.Session;
                            }

                            if (_locAccountReclassJournalOS.Code != null && _currSession != null)
                            {
                                _currObjectId = _locAccountReclassJournalOS.Code;

                                AccountReclassJournal _locAccountReclassJournalXPO = _currSession.FindObject<AccountReclassJournal>
                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                     new BinaryOperator("Code", _currObjectId),
                                                                     new GroupOperator(GroupOperatorType.Or,
                                                                     new BinaryOperator("Status", Status.Progress),
                                                                     new BinaryOperator("Status", Status.Posted))));

                                if (_locAccountReclassJournalXPO != null)
                                {
                                    SetAccountReclassJournalMonitoring(_currSession, _locAccountReclassJournalXPO);
                                    SetAccountReclassJournal(_currSession, _locAccountReclassJournalXPO);
                                    SetFinalStatusAccountReclassJournal(_currSession, _locAccountReclassJournalXPO);
                                    SuccessMessageShow(_locAccountReclassJournalXPO.Code + " has been change successfully to Posted");
                                }
                                else
                                {
                                    ErrorMessageShow("Data Account Reclass Journal Not Available");
                                }
                            }
                            else
                            {
                                ErrorMessageShow("Data Account Reclass Journal Not Available");
                            }
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = AccountReclassJournal " + ex.ToString());
            }
        }

        //============================================== Code In Here ==============================================

        #region PostingMethod

        private void SetAccountReclassJournalMonitoring(Session _currSession, AccountReclassJournal _locAccountReclassJournalXPO)
        {
            try
            {
                if (_locAccountReclassJournalXPO != null)
                {
                    XPCollection<AccountReclassJournalLine> _locAccountReclassJournalLines = new XPCollection<AccountReclassJournalLine>
                                                                                    (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                    new BinaryOperator("AccountReclassJournal", _locAccountReclassJournalXPO),
                                                                                    new GroupOperator(GroupOperatorType.Or,
                                                                                    new BinaryOperator("Status", Status.Progress),
                                                                                    new BinaryOperator("Status", Status.Posted))));
                    if (_locAccountReclassJournalLines != null && _locAccountReclassJournalLines.Count() > 0)
                    {
                        foreach (AccountReclassJournalLine _locAccountReclassJournalLine in _locAccountReclassJournalLines)
                        {
                            AccountReclassJournalMonitoring _saveDataAccountReclassJournalMonitoring = new AccountReclassJournalMonitoring(_currSession)
                            {
                                AccountReclassJournal = _locAccountReclassJournalXPO,
                                AccountReclassJournalLine = _locAccountReclassJournalLine,
                                Company = _locAccountReclassJournalLine.Company,
                                Workplace = _locAccountReclassJournalLine.Workplace,
                                Division = _locAccountReclassJournalLine.Division,
                                Department = _locAccountReclassJournalLine.Department,
                                Section = _locAccountReclassJournalLine.Section,
                                Employee = _locAccountReclassJournalLine.Employee,
                                PostingType = _locAccountReclassJournalLine.PostingType,
                                PostingMethod = _locAccountReclassJournalLine.PostingMethod,
                                PostingMethodType = _locAccountReclassJournalLine.PostingMethodType,
                                Description = _locAccountReclassJournalLine.Description,
                                AccountGroup = _locAccountReclassJournalLine.AccountGroup,
                                Account = _locAccountReclassJournalLine.Account,
                                Debit = _locAccountReclassJournalLine.Debit,
                                Credit = _locAccountReclassJournalLine.Credit,
                                DocDate = _locAccountReclassJournalLine.DocDate,
                                JournalMonth = _locAccountReclassJournalLine.JournalMonth,
                                JournalYear = _locAccountReclassJournalLine.JournalYear,
                            };
                            _saveDataAccountReclassJournalMonitoring.Save();
                            _saveDataAccountReclassJournalMonitoring.Session.CommitTransaction();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = AccountReclassJournal ", ex.ToString());
            }
        }

        private void SetAccountReclassJournal(Session _currSession, AccountReclassJournal _locAccountReclassJournalXPO)
        {
            try
            {
                DateTime now = DateTime.Now;
                double _locTotalAmountDebit = 0;
                double _locTotalAmountCredit = 0;
                double _locTotalBalance = 0;

                if (_locAccountReclassJournalXPO != null)
                {
                    XPCollection<AccountReclassJournalLine> _locAccountReclassJournalLines = new XPCollection<AccountReclassJournalLine>(_currSession,
                                                                                    new GroupOperator(GroupOperatorType.And,
                                                                                    new BinaryOperator("AccountReclassJournal", _locAccountReclassJournalXPO),
                                                                                    new GroupOperator(GroupOperatorType.Or,
                                                                                    new BinaryOperator("Status", Status.Progress),
                                                                                    new BinaryOperator("Status", Status.Posted))));

                    if (_locAccountReclassJournalLines != null && _locAccountReclassJournalLines.Count > 0)
                    {
                        foreach (AccountReclassJournalLine _locAccountReclassJournalLine in _locAccountReclassJournalLines)
                        {
                            if (_locAccountReclassJournalLine.OpenDebit == true)
                            {
                                _locTotalAmountDebit = _locAccountReclassJournalLine.Debit;
                            }

                            if (_locAccountReclassJournalLine.OpenCredit == true)
                            {
                                _locTotalAmountCredit = _locAccountReclassJournalLine.Credit;
                            }

                            GeneralJournal _saveGeneralJournal = new GeneralJournal(_currSession)
                            {
                                Company = _locAccountReclassJournalXPO.Company,
                                Workplace = _locAccountReclassJournalXPO.Workplace,
                                PostingDate = now,
                                PostingType = _locAccountReclassJournalLine.PostingType,
                                PostingMethod = _locAccountReclassJournalLine.PostingMethod,
                                Account = _locAccountReclassJournalLine.Account,
                                Debit = _locTotalAmountDebit,
                                Credit = _locTotalAmountCredit,
                                JournalMonth = _locAccountReclassJournalLine.JournalMonth,
                                JournalYear = _locAccountReclassJournalLine.JournalYear,
                                AccountReclassJournal = _locAccountReclassJournalXPO,
                            };
                            _saveGeneralJournal.Save();
                            _saveGeneralJournal.Session.CommitTransaction();

                            if (_locAccountReclassJournalLine.Account.Code != null)
                            {
                                AccountingPeriodicLine _locAPL = _currSession.FindObject<AccountingPeriodicLine>
                                                                (new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("Company", _locAccountReclassJournalLine.Company),
                                                                new BinaryOperator("Workplace", _locAccountReclassJournalLine.Workplace),
                                                                new BinaryOperator("AccountNo", _locAccountReclassJournalLine.Account),
                                                                new BinaryOperator("Month", _locAccountReclassJournalLine.JournalMonth),
                                                                new BinaryOperator("Year", _locAccountReclassJournalLine.JournalYear)));
                                if (_locAPL != null)
                                {
                                    if (_locAPL.AccountNo != null)
                                    {
                                        if (_locAPL.AccountNo.BalanceType == BalanceType.Change)
                                        {
                                            if (_locAPL.AccountNo.AccountCharge == AccountCharge.Debit)
                                            {
                                                if (_locTotalAmountDebit > 0)
                                                {
                                                    _locTotalBalance = _locAPL.Balance + _locTotalAmountDebit;
                                                }
                                                if (_locTotalAmountCredit > 0)
                                                {
                                                    _locTotalBalance = _locAPL.Balance - _locTotalAmountCredit;
                                                }
                                            }
                                            if (_locAPL.AccountNo.AccountCharge == AccountCharge.Credit)
                                            {
                                                if (_locTotalAmountDebit > 0)
                                                {
                                                    _locTotalBalance = _locAPL.Balance - _locTotalAmountDebit;
                                                }
                                                if (_locTotalAmountCredit > 0)
                                                {
                                                    _locTotalBalance = _locAPL.Balance + _locTotalAmountCredit;
                                                }
                                            }
                                        }

                                        _locAPL.Balance = _locTotalBalance;
                                        _locAPL.Debit = _locAPL.Debit + _locTotalAmountDebit;
                                        _locAPL.Credit = _locAPL.Credit + _locTotalAmountCredit;
                                        _locAPL.Save();
                                        _locAPL.Session.CommitTransaction();

                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = AccountReclassJournal ", ex.ToString());
            }
        }

        private void SetFinalStatusAccountReclassJournal(Session _currSession, AccountReclassJournal _locAccountReclassJournalXPO)
        {
            try
            {
                DateTime now = DateTime.Now;

                if (_locAccountReclassJournalXPO != null)
                {
                    XPCollection<AccountReclassJournalLine> _locAccountReclassJournalLines = new XPCollection<AccountReclassJournalLine>(_currSession,
                                                                                    new GroupOperator(GroupOperatorType.And,
                                                                                    new BinaryOperator("AccountReclassJournal", _locAccountReclassJournalXPO),
                                                                                    new GroupOperator(GroupOperatorType.Or,
                                                                                    new BinaryOperator("Status", Status.Progress),
                                                                                    new BinaryOperator("Status", Status.Posted))));

                    if (_locAccountReclassJournalLines != null && _locAccountReclassJournalLines.Count() > 0)
                    {

                        foreach (AccountReclassJournalLine _locAccountReclassJournalLine in _locAccountReclassJournalLines)
                        {
                            _locAccountReclassJournalLine.ActivationPosting = true;
                            _locAccountReclassJournalLine.Status = Status.Close;
                            _locAccountReclassJournalLine.StatusDate = now;
                            _locAccountReclassJournalLine.Save();
                            _locAccountReclassJournalLine.Session.CommitTransaction();
                        }

                        _locAccountReclassJournalXPO.ActivationPosting = true;
                        _locAccountReclassJournalXPO.Status = Status.Close;
                        _locAccountReclassJournalXPO.StatusDate = now;
                        _locAccountReclassJournalXPO.Save();
                        _locAccountReclassJournalXPO.Session.CommitTransaction();
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = AccountReclassJournal " + ex.ToString());
            }
        }

        #endregion PostingMethod

        #region Global Method

        private void SuccessMessageShow(string _locMessage)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 4000;
            options.Message = string.Format("{0}", _locMessage);
            options.Type = InformationType.Success;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Success";
            options.Win.Type = WinMessageType.Flyout;
            Application.ShowViewStrategy.ShowMessage(options);
        }

        private void ErrorMessageShow(string _locMessage)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 2000;
            options.Message = string.Format(_locMessage);
            options.Type = InformationType.Warning;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Error";
            options.Win.Type = WinMessageType.Alert;
            Application.ShowViewStrategy.ShowMessage(options);
        }

        #endregion Global Method

    }
}
