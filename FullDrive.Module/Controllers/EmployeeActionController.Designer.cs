﻿namespace FullDrive.Module.Controllers
{
    partial class EmployeeActionController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.EmployeeCreateRouteAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.EmployeeAddRouteAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            // 
            // EmployeeCreateRouteAction
            // 
            this.EmployeeCreateRouteAction.Caption = "Create Route";
            this.EmployeeCreateRouteAction.ConfirmationMessage = null;
            this.EmployeeCreateRouteAction.Id = "EmployeeCreateRouteActionId";
            this.EmployeeCreateRouteAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.Employee);
            this.EmployeeCreateRouteAction.ToolTip = null;
            this.EmployeeCreateRouteAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.EmployeeCreateRouteAction_Execute);
            // 
            // EmployeeAddRouteAction
            // 
            this.EmployeeAddRouteAction.Caption = "Add Route";
            this.EmployeeAddRouteAction.ConfirmationMessage = null;
            this.EmployeeAddRouteAction.Id = "EmployeeAddRouteActionId";
            this.EmployeeAddRouteAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.Employee);
            this.EmployeeAddRouteAction.ToolTip = null;
            this.EmployeeAddRouteAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.EmployeeAddRouteAction_Execute);
            // 
            // EmployeeActionController
            // 
            this.Actions.Add(this.EmployeeCreateRouteAction);
            this.Actions.Add(this.EmployeeAddRouteAction);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SimpleAction EmployeeCreateRouteAction;
        private DevExpress.ExpressApp.Actions.SimpleAction EmployeeAddRouteAction;
    }
}
