﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;

using FullDrive.Module.BusinessObjects;
using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.Xpo;
using System.Collections;
using DevExpress.Xpo;
using System.IO;
using System.Web;

namespace FullDrive.Module.Controllers
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class OrderCollectionActionController : ViewController
    {
        private ChoiceActionItem _selectionListviewFilter;

        public OrderCollectionActionController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.
            #region Filter Status
            OrderCollectionListviewFilterSelectionAction.Items.Clear();
            foreach (object _currApproval in Enum.GetValues(typeof(CustomProcess.Status)))
            {
                EnumDescriptor _ed = new EnumDescriptor(typeof(CustomProcess.Status));
                _selectionListviewFilter = new ChoiceActionItem(_ed.GetCaption(_currApproval), _currApproval);
                OrderCollectionListviewFilterSelectionAction.Items.Add(_selectionListviewFilter);
            }
            #endregion Filter Status
        }

        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
        }

        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }

        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        private void OrderCollectionGetSIAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;

                if (_objectsToProcess != null)
                {
                    foreach (Object obj in _objectsToProcess)
                    {
                        OrderCollection _locOrderCollectionOS = (OrderCollection)_objectSpace.GetObject(obj);

                        if (_locOrderCollectionOS != null)
                        {
                            if (_locOrderCollectionOS.Session != null)
                            {
                                _currSession = _locOrderCollectionOS.Session;
                            }

                            if (_locOrderCollectionOS.Code != null && _currSession != null)
                            {
                                _currObjectId = _locOrderCollectionOS.Code;

                                OrderCollection _locOrderCollectionXPO = _currSession.FindObject<OrderCollection>
                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                     new BinaryOperator("Code", _currObjectId),
                                                                     new GroupOperator(GroupOperatorType.Or,
                                                                     new BinaryOperator("Status", Status.Open),
                                                                     new BinaryOperator("Status", Status.Progress))));

                                if (_locOrderCollectionXPO != null)
                                {
                                    GetPickingMonitoring(_currSession, _locOrderCollectionXPO);
                                    SuccessMessageShow("SalesInvoice Has Been Successfully Getting into OrderCollection");
                                }
                                else
                                {
                                    ErrorMessageShow("Data Order Collection Not Available");
                                }
                            }
                            else
                            {
                                ErrorMessageShow("Data Order Collection Not Available");
                            }
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = OrderCollection " + ex.ToString());
            }
        }

        private void OrderCollectionProgressAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;
                Status _locStatus = Status.Open;
                double _locTotalAmount = 0;
                double _locTotalAmountCN = 0;
                double _locTotalAmountDN = 0;
                double _locTotalAmountColl = 0;

                if (_objectsToProcess != null)
                {
                    foreach (Object obj in _objectsToProcess)
                    {
                        OrderCollection _locOrderCollectionOS = (OrderCollection)_objectSpace.GetObject(obj);

                        if (_locOrderCollectionOS != null)
                        {
                            if (_locOrderCollectionOS.Session != null)
                            {
                                _currSession = _locOrderCollectionOS.Session;
                            }

                            if (_locOrderCollectionOS.Code != null && _currSession != null)
                            {
                                _currObjectId = _locOrderCollectionOS.Code;

                                OrderCollection _locOrderCollectionXPO = _currSession.FindObject<OrderCollection>
                                                                   (new GroupOperator(GroupOperatorType.And,
                                                                    new BinaryOperator("Code", _currObjectId),
                                                                    new GroupOperator(GroupOperatorType.Or,
                                                                    new BinaryOperator("Status", Status.Open),
                                                                    new BinaryOperator("Status", Status.Progress),
                                                                    new BinaryOperator("Status", Status.Posted))));

                                if (_locOrderCollectionXPO != null)
                                {
                                    if (_locOrderCollectionXPO.Status == Status.Open)
                                    {
                                        _locStatus = Status.Progress;

                                    }
                                    else
                                    {
                                        _locStatus = _locOrderCollectionXPO.Status;
                                    }

                                    #region OrderCollectionLine
                                    XPCollection<OrderCollectionLine> _locOrderCollectionLines = new XPCollection<OrderCollectionLine>
                                                                                           (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                            new BinaryOperator("OrderCollection", _locOrderCollectionXPO),
                                                                                            new BinaryOperator("Status", Status.Open)));

                                    if (_locOrderCollectionLines != null && _locOrderCollectionLines.Count > 0)
                                    {
                                        foreach (OrderCollectionLine _locOrderCollectionLine in _locOrderCollectionLines)
                                        {
                                            _locTotalAmount = _locTotalAmount + _locOrderCollectionLine.Amount;
                                            _locTotalAmountCN = _locTotalAmountCN + _locOrderCollectionLine.AmountCN;
                                            _locTotalAmountDN = _locTotalAmountDN + _locOrderCollectionLine.AmountDN;
                                            _locTotalAmountColl = _locTotalAmountColl + _locOrderCollectionLine.AmountColl;
                                            _locOrderCollectionLine.Status = Status.Progress;
                                            _locOrderCollectionLine.StatusDate = now;
                                            _locOrderCollectionLine.Save();
                                            _locOrderCollectionLine.Session.CommitTransaction();

                                        }
                                    }
                                    #endregion OrderCollectionLine
                                    _locOrderCollectionXPO.GrandTotalAmount = _locTotalAmount;
                                    _locOrderCollectionXPO.GrandTotalAmountCN = _locTotalAmountCN;
                                    _locOrderCollectionXPO.GrandTotalAmountDN = _locTotalAmountDN;
                                    _locOrderCollectionXPO.GrandTotalAmountColl = _locTotalAmountColl;
                                    _locOrderCollectionXPO.Status = _locStatus;
                                    _locOrderCollectionXPO.StatusDate = now;
                                    _locOrderCollectionXPO.Save();
                                    _locOrderCollectionXPO.Session.CommitTransaction();
                                }
                                else
                                {
                                    ErrorMessageShow("Data Order Collection Not Available");
                                }
                            }
                            else
                            {
                                ErrorMessageShow("Data Order Collection Not Available");
                            }
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = OrderCollection " + ex.ToString());
            }
        }

        private void OrderCollectionPostingAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;

                if (_objectsToProcess != null)
                {
                    foreach (Object obj in _objectsToProcess)
                    {
                        OrderCollection _locOrderCollectionOS = (OrderCollection)_objectSpace.GetObject(obj);

                        if (_locOrderCollectionOS != null)
                        {
                            if (_locOrderCollectionOS.Session != null)
                            {
                                _currSession = _locOrderCollectionOS.Session;
                            }

                            if (_locOrderCollectionOS.Code != null && _currSession != null)
                            {
                                _currObjectId = _locOrderCollectionOS.Code;

                                OrderCollection _locOrderCollectionXPO = _currSession.FindObject<OrderCollection>
                                                                   (new GroupOperator(GroupOperatorType.And,
                                                                    new BinaryOperator("Code", _currObjectId),
                                                                    new GroupOperator(GroupOperatorType.Or,
                                                                    new BinaryOperator("Status", Status.Progress),
                                                                    new BinaryOperator("Status", Status.Posted))));

                                if (_locOrderCollectionXPO != null)
                                {
                                    SetCollectStatusOrderCollectionLine(_currSession, _locOrderCollectionXPO);
                                    SetOrderCollectionMonitoring(_currSession, _locOrderCollectionXPO);
                                    if (CheckJournalSetup(_currSession, _locOrderCollectionXPO) == true )
                                    {
                                        SetReverseGoodsDeliverJournal(_currSession, _locOrderCollectionXPO);
                                        SetInvoiceARJournal(_currSession, _locOrderCollectionXPO);
                                    }
                                    SetStatusOrderCollectionLine(_currSession, _locOrderCollectionXPO);
                                    SetPostedCount(_currSession, _locOrderCollectionXPO);
                                    SetNormal(_currSession, _locOrderCollectionXPO);
                                    SetFinalStatusOrderCollection(_currSession, _locOrderCollectionXPO);
                                    SetFinalPickingStatus(_currSession, _locOrderCollectionXPO);
                                    SuccessMessageShow(_locOrderCollectionXPO.Code + " has been change successfully to Posted");
                                }
                                else
                                {
                                    ErrorMessageShow("Data Order Collection Not Available");
                                }
                            }
                            else
                            {
                                ErrorMessageShow("Data Order Collection Not Available");
                            }
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = OrderCollection " + ex.ToString());
            }
        }

        private void OrderCollectionListviewFilterSelectionAction_Execute(object sender, SingleChoiceActionExecuteEventArgs e)
        {
            try
            {
                if ((View is ListView) & (View.ObjectTypeInfo.Type == typeof(OrderCollection)))
                {
                    if ((Status)e.SelectedChoiceActionItem.Data == Status.None)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.None, BinaryOperatorType.NotEqual);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Open)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Open, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Progress)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Progress, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Posted)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Posted, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Close)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Close, BinaryOperatorType.Equal);
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = OrderCollection " + ex.ToString());
            }
        }

        //=============================================== Code In Here =============================================

        #region GetSIBasedOnRouteInvoice

        private void GetPickingMonitoring(Session _currSession, OrderCollection _locOrderCollectionXPO)  
        {
            try
            {
                DateTime now = DateTime.Now;

                if (_locOrderCollectionXPO != null)
                {
                    if (_locOrderCollectionXPO.Picking != null)
                    {
                        SalesInvoice _locSalesInvoice = null;
                        Division _locDiv = null;
                        Department _locDept = null;
                        Section _locSect = null;
                        Employee _locPIC = null;
                        Currency _locCurrency = null;
                        PriceGroup _locPriceGroup = null;
                        PaymentMethodType _locPaymentMethodType = PaymentMethodType.None;
                        PaymentMethod _locPaymentMethod = null;
                        PaymentType _locPaymentType = PaymentType.None;
                        TermOfPayment _locTOP = null;

                        //Hanya memindahkan Route Invoice ke OrderCollection Line                        
                        XPCollection<PickingMonitoring> _locPickingMonitorings = new XPCollection<PickingMonitoring>
                                                                    (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                    new BinaryOperator("Picking", _locOrderCollectionXPO.Picking),
                                                                    new BinaryOperator("CollectStatus", CollectStatus.Ready),
                                                                    new BinaryOperator("OC", true)));

                        if (_locPickingMonitorings != null && _locPickingMonitorings.Count() > 0)
                        {
                            foreach (PickingMonitoring _locPickingMonitoring in _locPickingMonitorings)
                            {
                                if (_locPickingMonitoring.OrderCollection == null)
                                {
                                    #region SalesInvoice
                                    if(_locPickingMonitoring.SalesInvoice != null)
                                    {
                                        _locSalesInvoice = _locPickingMonitoring.SalesInvoice;
                                        if(_locPickingMonitoring.SalesInvoice.Div != null)
                                        {
                                            _locDiv = _locPickingMonitoring.SalesInvoice.Div;
                                        }
                                        if (_locPickingMonitoring.SalesInvoice.Dept != null)
                                        {
                                            _locDept = _locPickingMonitoring.SalesInvoice.Dept;
                                        }
                                        if (_locPickingMonitoring.SalesInvoice.Sect != null)
                                        {
                                            _locSect = _locPickingMonitoring.SalesInvoice.Sect;
                                        }
                                        if (_locPickingMonitoring.SalesInvoice.PIC != null)
                                        {
                                            _locPIC = _locPickingMonitoring.SalesInvoice.PIC;
                                        }
                                        if (_locPickingMonitoring.SalesInvoice.Currency != null)
                                        {
                                            _locCurrency = _locPickingMonitoring.SalesInvoice.Currency;
                                        }
                                        if (_locPickingMonitoring.SalesInvoice.PriceGroup != null)
                                        {
                                            _locPriceGroup = _locPickingMonitoring.SalesInvoice.PriceGroup;
                                        }
                                        if (_locPickingMonitoring.SalesInvoice.TOP != null)
                                        {
                                            _locTOP = _locPickingMonitoring.SalesInvoice.TOP;
                                        }
                                        _locPaymentMethodType = _locPickingMonitoring.SalesInvoice.PaymentMethodType;
                                        if (_locPickingMonitoring.SalesInvoice.PaymentMethod != null)
                                        {
                                            _locPaymentMethod = _locPickingMonitoring.SalesInvoice.PaymentMethod;
                                        }
                                        _locPaymentType = _locPickingMonitoring.SalesInvoice.PaymentType;
                                    }
                                    #endregion SalesInvoice
                                    if (_locPickingMonitoring.AmendDelivery == null && _locPickingMonitoring.SalesInvoiceCmp == null)
                                    {
                                        #region SaveOrderCollectionLine
                                        OrderCollectionLine _saveDataOrderCollectionLine = new OrderCollectionLine(_currSession)
                                        {
                                            OrderCollection = _locOrderCollectionXPO,
                                            Company = _locOrderCollectionXPO.Company,
                                            Workplace = _locOrderCollectionXPO.Workplace,
                                            Division = _locOrderCollectionXPO.Division,
                                            Department = _locOrderCollectionXPO.Department,
                                            Section = _locOrderCollectionXPO.Section,
                                            Employee = _locOrderCollectionXPO.Employee,
                                            Div = _locDiv,
                                            Dept = _locDept,
                                            Sect = _locSect,
                                            PIC = _locPIC,
                                            Collector = _locOrderCollectionXPO.Collector,
                                            Vehicle = _locOrderCollectionXPO.Vehicle,
                                            SalesInvoice = _locSalesInvoice,
                                            Customer = _locPickingMonitoring.Customer,
                                            Currency = _locCurrency,
                                            PriceGroup = _locPriceGroup,
                                            PaymentMethodType = _locPaymentMethodType,
                                            PaymentMethod = _locPaymentMethod,
                                            PaymentType = _locPaymentType,
                                            EstimatedDate = _locPickingMonitoring.SalesInvoice.DueDate,
                                            DueDate = _locPickingMonitoring.SalesInvoice.DueDate,
                                            TotAmount = _locPickingMonitoring.SalesInvoice.TotAmount,
                                            TotTaxAmount = _locPickingMonitoring.SalesInvoice.TotTaxAmount,
                                            TotDiscAmount = _locPickingMonitoring.SalesInvoice.TotDiscAmount,
                                            Amount = _locPickingMonitoring.SalesInvoice.Amount,
                                            TOP = _locTOP,
                                            PickingDate = _locOrderCollectionXPO.PickingDate,
                                            Picking = _locOrderCollectionXPO.Picking,
                                            PickingMonitoring = _locPickingMonitoring,
                                            CollectStatus = _locPickingMonitoring.CollectStatus,
                                            CollectStatusDate = now,
                                            DocDate = _locPickingMonitoring.SalesInvoice.DocDate,
                                            JournalMonth = _locPickingMonitoring.SalesInvoice.JournalMonth,
                                            JournalYear = _locPickingMonitoring.SalesInvoice.JournalYear,
                                        };
                                        _saveDataOrderCollectionLine.Save();
                                        _saveDataOrderCollectionLine.Session.CommitTransaction();

                                        #endregion SaveDeliveryOrderLine
                                    } else
                                    {
                                        #region SaveOrderCollectionLine
                                        OrderCollectionLine _saveDataOrderCollectionLine = new OrderCollectionLine(_currSession)
                                        {
                                            OrderCollection = _locOrderCollectionXPO,
                                            Company = _locOrderCollectionXPO.Company,
                                            Workplace = _locOrderCollectionXPO.Workplace,
                                            Division = _locOrderCollectionXPO.Division,
                                            Department = _locOrderCollectionXPO.Department,
                                            Section = _locOrderCollectionXPO.Section,
                                            Employee = _locOrderCollectionXPO.Employee,
                                            Div = _locDiv,
                                            Dept = _locDept,
                                            Sect = _locSect,
                                            PIC = _locPIC,
                                            Collector = _locOrderCollectionXPO.Collector,
                                            Vehicle = _locOrderCollectionXPO.Vehicle,
                                            SalesInvoice = _locSalesInvoice,
                                            Customer = _locPickingMonitoring.Customer,
                                            Currency = _locCurrency,
                                            PriceGroup = _locPriceGroup,
                                            PaymentMethodType = _locPaymentMethodType,
                                            PaymentMethod = _locPaymentMethod,
                                            PaymentType = _locPaymentType,
                                            EstimatedDate = _locPickingMonitoring.SalesInvoice.DueDate,
                                            DueDate = _locPickingMonitoring.SalesInvoice.DueDate,
                                            TotAmount = _locPickingMonitoring.TotAmount,
                                            TotTaxAmount = _locPickingMonitoring.TotTaxAmount,
                                            TotDiscAmount = _locPickingMonitoring.TotDiscAmount,
                                            Amount = _locPickingMonitoring.Amount,
                                            AmountCN = _locPickingMonitoring.AmountCN,
                                            AmountDN = _locPickingMonitoring.AmountDN,
                                            AmountColl = _locPickingMonitoring.AmountColl,
                                            TOP = _locTOP,
                                            PickingDate = _locOrderCollectionXPO.PickingDate,
                                            Picking = _locOrderCollectionXPO.Picking,
                                            PickingMonitoring = _locPickingMonitoring,
                                            CollectStatus = _locPickingMonitoring.CollectStatus,
                                            CollectStatusDate = now,
                                            JournalMonth = _locPickingMonitoring.SalesInvoice.JournalMonth,
                                            JournalYear = _locPickingMonitoring.SalesInvoice.JournalYear,
                                        };
                                        _saveDataOrderCollectionLine.Save();
                                        _saveDataOrderCollectionLine.Session.CommitTransaction();

                                        #endregion SaveDeliveryOrderLine
                                    }

                                    #region SetOrderCollection
                                    _locPickingMonitoring.AD = false;
                                    _locPickingMonitoring.OC = false;
                                    _locPickingMonitoring.Active = false;
                                    _locPickingMonitoring.OrderCollection = _locOrderCollectionXPO;
                                    _locPickingMonitoring.Save();
                                    _locPickingMonitoring.Session.CommitTransaction();
                                    #endregion SetOrderCollection

                                    _locSalesInvoice = null;
                                    _locDiv = null;
                                    _locDept = null;
                                    _locSect = null;
                                    _locPIC = null;
                                    _locCurrency = null;
                                    _locPriceGroup = null;
                                    _locPaymentMethodType = PaymentMethodType.None;
                                    _locPaymentMethod = null;
                                    _locPaymentType = PaymentType.None;
                                    _locTOP = null;
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = OrderCollection ", ex.ToString());
            }
        }

        #endregion GetSIBasedOnRouteInvoice

        #region Posting

        private void SetCollectStatusOrderCollectionLine(Session _currSession, OrderCollection _locOrderCollectionXPO)
        {
            try
            {
                DateTime now = DateTime.Now;
                if (_locOrderCollectionXPO != null)
                {
                    XPCollection<OrderCollectionLine> _locOrderCollectionLines = new XPCollection<OrderCollectionLine>(_currSession,
                                                                        new GroupOperator(GroupOperatorType.And,
                                                                        new BinaryOperator("OrderCollection", _locOrderCollectionXPO),
                                                                        new BinaryOperator("Select", true),
                                                                        new GroupOperator(GroupOperatorType.Or,
                                                                        new BinaryOperator("Status", Status.Progress),
                                                                        new BinaryOperator("Status", Status.Posted))));

                    if (_locOrderCollectionLines != null && _locOrderCollectionLines.Count() > 0)
                    {
                        CollectStatus _locCollectStatus = CollectStatus.None;
                        foreach (OrderCollectionLine _locOrderCollectionLine in _locOrderCollectionLines)
                        {
                            if (_locOrderCollectionLine.CollectStatus == CollectStatus.Ready && _locOrderCollectionLine.PostedCount == 0)
                            {
                                _locCollectStatus = CollectStatus.Collect;
                            }
                            else
                            {
                                _locCollectStatus = _locOrderCollectionLine.CollectStatus;
                            }
                            _locOrderCollectionLine.CollectStatus = _locCollectStatus;
                            _locOrderCollectionLine.CollectStatusDate = now;
                            _locOrderCollectionLine.Save();
                            _locOrderCollectionLine.Session.CommitTransaction();

                            _locOrderCollectionLine.PickingMonitoring.CollectStatus = _locCollectStatus;
                            _locOrderCollectionLine.PickingMonitoring.CollectStatusDate = now;
                            _locOrderCollectionLine.PickingMonitoring.Save();
                            _locOrderCollectionLine.PickingMonitoring.Session.CommitTransaction();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = OrderCollection " + ex.ToString());
            }
        }

        private void SetOrderCollectionMonitoring(Session _currSession, OrderCollection _locOrderCollectionXPO)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                DateTime now = DateTime.Now;

                if (_locOrderCollectionXPO != null)
                {
                    if (_locOrderCollectionXPO.Status == Status.Progress || _locOrderCollectionXPO.Status == Status.Posted)
                    {
                        XPCollection<OrderCollectionLine> _locOrderCollectionLines = new XPCollection<OrderCollectionLine>(_currSession,
                                                                        new GroupOperator(GroupOperatorType.And,
                                                                        new BinaryOperator("OrderCollection", _locOrderCollectionXPO),
                                                                        new BinaryOperator("Select", true),
                                                                        new BinaryOperator("CollectStatus", CollectStatus.Collect),
                                                                        new GroupOperator(GroupOperatorType.Or,
                                                                        new BinaryOperator("Status", Status.Progress),
                                                                        new BinaryOperator("Status", Status.Posted))));

                        if (_locOrderCollectionLines != null && _locOrderCollectionLines.Count() > 0)
                        {
                            foreach (OrderCollectionLine _locOrderCollectionLine in _locOrderCollectionLines)
                            {
                                if (_locOrderCollectionLine.PostedCount == 0 && _locOrderCollectionLine.Status == Status.Progress)
                                {
                                    OrderCollectionMonitoring _saveDataOrderCollectionMonitoring = new OrderCollectionMonitoring(_currSession)
                                    {
                                        OrderCollection = _locOrderCollectionXPO,
                                        OrderCollectionLine = _locOrderCollectionLine,
                                        Company = _locOrderCollectionLine.Company,
                                        Workplace = _locOrderCollectionLine.Workplace,
                                        Division = _locOrderCollectionLine.Division,
                                        Department = _locOrderCollectionLine.Department,
                                        Section = _locOrderCollectionLine.Section,
                                        Employee = _locOrderCollectionLine.Employee,
                                        Div = _locOrderCollectionLine.Div,
                                        Dept = _locOrderCollectionLine.Dept,
                                        Sect = _locOrderCollectionLine.Sect,
                                        PIC = _locOrderCollectionLine.PIC,
                                        Collector = _locOrderCollectionLine.Collector,
                                        Vehicle = _locOrderCollectionLine.Vehicle,
                                        Customer = _locOrderCollectionLine.Customer,
                                        SalesInvoice = _locOrderCollectionLine.SalesInvoice,
                                        Currency = _locOrderCollectionLine.Currency,
                                        PriceGroup = _locOrderCollectionLine.PriceGroup,
                                        PaymentMethodType = _locOrderCollectionLine.PaymentMethodType,
                                        PaymentMethod = _locOrderCollectionLine.PaymentMethod,
                                        PaymentType = _locOrderCollectionLine.PaymentType,
                                        OpenDueDate = _locOrderCollectionLine.OpenDueDate,
                                        ChequeNo = _locOrderCollectionLine.ChequeNo,
                                        EstimatedDate = _locOrderCollectionLine.EstimatedDate,
                                        ActualDate = _locOrderCollectionLine.ActualDate,
                                        DueDate = _locOrderCollectionLine.DueDate,
                                        TotAmount = _locOrderCollectionLine.TotAmount,
                                        TotTaxAmount = _locOrderCollectionLine.TotTaxAmount,
                                        TotDiscAmount = _locOrderCollectionLine.TotDiscAmount,
                                        Amount = _locOrderCollectionLine.Amount,
                                        AmountCN = _locOrderCollectionLine.AmountCN,
                                        AmountDN = _locOrderCollectionLine.AmountDN,
                                        AmountColl = _locOrderCollectionLine.AmountColl,
                                        TOP = _locOrderCollectionLine.TOP,
                                        PickingDate = _locOrderCollectionLine.PickingDate,
                                        Picking = _locOrderCollectionLine.Picking,
                                        PickingMonitoring = _locOrderCollectionLine.PickingMonitoring,
                                        CollectStatus = _locOrderCollectionLine.CollectStatus,
                                        CollectStatusDate = _locOrderCollectionLine.CollectStatusDate,
                                    };
                                    _saveDataOrderCollectionMonitoring.Save();
                                    _saveDataOrderCollectionMonitoring.Session.CommitTransaction();
                                }
                                else
                                {
                                    OrderCollectionMonitoring _locOrderCollectionMonitoring = _currSession.FindObject<OrderCollectionMonitoring>
                                                                                        (new GroupOperator(GroupOperatorType.And,
                                                                                        new BinaryOperator("OrderCollection", _locOrderCollectionXPO),
                                                                                        new BinaryOperator("OrderCollectionLine", _locOrderCollectionLine)));
                                    if (_locOrderCollectionMonitoring != null)
                                    {
                                        _locOrderCollectionMonitoring.CollectStatus = _locOrderCollectionLine.CollectStatus;
                                        _locOrderCollectionMonitoring.CollectStatusDate = _locOrderCollectionLine.CollectStatusDate;
                                        _locOrderCollectionMonitoring.Save();
                                        _locOrderCollectionMonitoring.Session.CommitTransaction();
                                    }
                                }

                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = OrderCollection ", ex.ToString());
            }
        }

        #region JournalInvoiceAR

        private bool CheckJournalSetup(Session _currSession, OrderCollection _locOrderCollectionXPO)
        {
            bool _result = false;
            try
            {
                if (_locOrderCollectionXPO != null)
                {
                    if (_locOrderCollectionXPO.Company != null && _locOrderCollectionXPO.Workplace != null)
                    {
                        JournalSetupDetail _locJournalSetupDetail = _currSession.FindObject<JournalSetupDetail>
                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                    new BinaryOperator("Company", _locOrderCollectionXPO.Company),
                                                                    new BinaryOperator("Workplace", _locOrderCollectionXPO.Workplace),
                                                                    new BinaryOperator("JournalSetupType", JournalSetupType.Journal),
                                                                    new BinaryOperator("ObjectList", ObjectList.OrderCollection),
                                                                    new BinaryOperator("JournalProcess", JournalProcess.JournalInvoiceAR),
                                                                    new BinaryOperator("Active", true)));

                        if (_locJournalSetupDetail != null)
                        {
                            _result = true;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = OrderCollection ", ex.ToString());
            }

            return _result;
        }

        private void SetReverseGoodsDeliverJournal(Session _currSession, OrderCollection _locOrderCollectionXPO)
        {
            try
            {
                DateTime now = DateTime.Now;
                double _locTotalUnitAmount = 0;
                ItemAccountGroup _locItemAccountGroup = null;

                if (_locOrderCollectionXPO != null)
                {
                    XPCollection<OrderCollectionLine> _locOrderCollectionLines = new XPCollection<OrderCollectionLine>(_currSession,
                                                                                new GroupOperator(GroupOperatorType.And,
                                                                                new BinaryOperator("OrderCollection", _locOrderCollectionXPO),
                                                                                new BinaryOperator("Select", true),
                                                                                new BinaryOperator("Status", Status.Progress)));
                    if (_locOrderCollectionLines != null && _locOrderCollectionLines.Count() > 0)
                    {
                        foreach (OrderCollectionLine _locOrderCollectionLine in _locOrderCollectionLines)
                        {
                            if (_locOrderCollectionLine.SalesInvoice != null)
                            {
                                XPCollection<SalesInvoiceMonitoring> _locSalesInvoiceMonitorings = new XPCollection<SalesInvoiceMonitoring>(_currSession,
                                                                                                new GroupOperator(GroupOperatorType.And,
                                                                                                new BinaryOperator("SalesInvoice", _locOrderCollectionLine.SalesInvoice)));
                                if (_locSalesInvoiceMonitorings != null && _locSalesInvoiceMonitorings.Count() > 0)
                                {
                                    foreach (SalesInvoiceMonitoring _locSalesInvoiceMonitoring in _locSalesInvoiceMonitorings)
                                    {
                                        XPCollection<DeliveryOrderMonitoring> _locDeliveryOrderMonitorings = new XPCollection<DeliveryOrderMonitoring>(_currSession,
                                                                                                            new GroupOperator(GroupOperatorType.And,
                                                                                                            new BinaryOperator("SalesInvoiceMonitoring", _locSalesInvoiceMonitoring)));

                                        if (_locDeliveryOrderMonitorings != null && _locDeliveryOrderMonitorings.Count() > 0)
                                        {
                                            foreach (DeliveryOrderMonitoring _locDeliveryOrderMonitoring in _locDeliveryOrderMonitorings)
                                            {
                                                ItemAccount _locItemAccount = _currSession.FindObject<ItemAccount>(new GroupOperator(GroupOperatorType.And,
                                                                    new BinaryOperator("Company", _locOrderCollectionLine.Company),
                                                                    new BinaryOperator("Workplace", _locOrderCollectionLine.Workplace),
                                                                    new BinaryOperator("Item", _locDeliveryOrderMonitoring.Item),
                                                                    new BinaryOperator("Active", true)));

                                                if (_locItemAccount != null)
                                                {
                                                    if (_locItemAccount.ItemAccountGroup != null)
                                                    {
                                                        _locItemAccountGroup = _locItemAccount.ItemAccountGroup;
                                                    }
                                                }

                                                if (_locDeliveryOrderMonitoring.SalesInvoiceMonitoring.SalesInvoiceLine != null)
                                                {
                                                    if (_locDeliveryOrderMonitoring.SalesInvoiceMonitoring.SalesInvoiceLine.PriceLine != null)
                                                    {
                                                        _locTotalUnitAmount = _locDeliveryOrderMonitoring.SalesInvoiceMonitoring.SalesInvoiceLine.PriceLine.Amount1a * _locDeliveryOrderMonitoring.TQty;
                                                    }
                                                }

                                                #region JournalMapByItems

                                                XPCollection<JournalMap> _locJournalMapItems = new XPCollection<JournalMap>
                                                                                               (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                new BinaryOperator("Company", _locOrderCollectionLine.Company),
                                                                                                new BinaryOperator("Workplace", _locOrderCollectionLine.Workplace),
                                                                                                new BinaryOperator("ItemAccountGroup", _locItemAccountGroup),
                                                                                                new BinaryOperator("Active", true)));

                                                if (_locJournalMapItems != null && _locJournalMapItems.Count() > 0)
                                                {
                                                    foreach (JournalMap _locJournalMapItem in _locJournalMapItems)
                                                    {
                                                        XPCollection<JournalMapLine> _locJournalMapLines = new XPCollection<JournalMapLine>
                                                                                                           (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                            new BinaryOperator("Company", _locOrderCollectionLine.Company),
                                                                                                            new BinaryOperator("Workplace", _locOrderCollectionLine.Workplace),
                                                                                                            new BinaryOperator("JournalMap", _locJournalMapItem),
                                                                                                            new BinaryOperator("Active", true)));

                                                        if (_locJournalMapLines != null && _locJournalMapLines.Count() > 0)
                                                        {
                                                            foreach (JournalMapLine _locJournalMapLine in _locJournalMapLines)
                                                            {
                                                                if (_locJournalMapLine.AccountMap != null)
                                                                {
                                                                    if (_locJournalMapLine.AccountMap.PostingType == PostingType.Sales && _locJournalMapLine.AccountMap.OrderType == OrderType.Item
                                                                        && _locJournalMapLine.AccountMap.PostingMethod == PostingMethod.Deliver && _locJournalMapLine.AccountMap.PostingMethodType == PostingMethodType.Normal)
                                                                    {
                                                                        XPCollection<AccountMapLine> _locAccountMapLines = new XPCollection<AccountMapLine>
                                                                                                                       (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                                        new BinaryOperator("Company", _locOrderCollectionLine.Company),
                                                                                                                        new BinaryOperator("Workplace", _locOrderCollectionLine.Workplace),
                                                                                                                        new BinaryOperator("AccountMap", _locJournalMapLine.AccountMap),
                                                                                                                        new BinaryOperator("Active", true)));

                                                                        if (_locAccountMapLines != null && _locAccountMapLines.Count() > 0)
                                                                        {
                                                                            double _locTotalAmountDebit = 0;
                                                                            double _locTotalAmountCredit = 0;
                                                                            double _locTotalBalance = 0;

                                                                            foreach (AccountMapLine _locAccountMapLine in _locAccountMapLines)
                                                                            {
                                                                                if (_locAccountMapLine.AccountCharge == AccountCharge.Debit)
                                                                                {
                                                                                    _locTotalAmountCredit = _locTotalUnitAmount;
                                                                                }
                                                                                if (_locAccountMapLine.AccountCharge == AccountCharge.Credit)
                                                                                {
                                                                                    _locTotalAmountDebit = _locTotalUnitAmount;
                                                                                }

                                                                                GeneralJournal _saveGeneralJournal = new GeneralJournal(_currSession)
                                                                                {
                                                                                    Company = _locOrderCollectionLine.Company,
                                                                                    Workplace = _locOrderCollectionLine.Workplace,
                                                                                    PostingDate = now,
                                                                                    PostingType = PostingType.Sales,
                                                                                    OrderType = OrderType.Item,
                                                                                    PostingMethod = PostingMethod.InvoiceAR,
                                                                                    PostingMethodType = PostingMethodType.Normal,
                                                                                    Account = _locAccountMapLine.Account,
                                                                                    Debit = _locTotalAmountDebit,
                                                                                    Credit = _locTotalAmountCredit,
                                                                                    JournalMonth = _locOrderCollectionLine.JournalMonth,
                                                                                    JournalYear = _locOrderCollectionLine.JournalYear,
                                                                                    DeliveryOrder = _locDeliveryOrderMonitoring.DeliveryOrder,
                                                                                    OrderCollection = _locOrderCollectionXPO,
                                                                                };

                                                                                _saveGeneralJournal.Save();
                                                                                _saveGeneralJournal.Session.CommitTransaction();

                                                                                #region AccountingPeriodicLine

                                                                                if (_locAccountMapLine.Account.Code != null)
                                                                                {

                                                                                    //Cari Account Periodic Line
                                                                                    AccountingPeriodicLine _locAPL = _currSession.FindObject<AccountingPeriodicLine>
                                                                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                                                                    new BinaryOperator("Company", _locOrderCollectionLine.Company),
                                                                                                                    new BinaryOperator("Workplace", _locOrderCollectionLine.Workplace),
                                                                                                                    new BinaryOperator("AccountNo", _locAccountMapLine.Account),
                                                                                                                    new BinaryOperator("Month", _locOrderCollectionLine.JournalMonth),
                                                                                                                    new BinaryOperator("Year", _locOrderCollectionLine.JournalYear)));
                                                                                    if (_locAPL != null)
                                                                                    {
                                                                                        if (_locAPL.AccountNo != null)
                                                                                        {
                                                                                            if (_locAPL.AccountNo.BalanceType == BalanceType.Change)
                                                                                            {
                                                                                                if (_locAPL.AccountNo.AccountCharge == AccountCharge.Debit)
                                                                                                {
                                                                                                    if (_locTotalAmountDebit > 0)
                                                                                                    {
                                                                                                        _locTotalBalance = _locAPL.Balance + _locTotalAmountDebit;
                                                                                                    }
                                                                                                    if (_locTotalAmountCredit > 0)
                                                                                                    {
                                                                                                        _locTotalBalance = _locAPL.Balance - _locTotalAmountCredit;
                                                                                                    }
                                                                                                }
                                                                                                if (_locAPL.AccountNo.AccountCharge == AccountCharge.Credit)
                                                                                                {
                                                                                                    if (_locTotalAmountDebit > 0)
                                                                                                    {
                                                                                                        _locTotalBalance = _locAPL.Balance - _locTotalAmountDebit;
                                                                                                    }
                                                                                                    if (_locTotalAmountCredit > 0)
                                                                                                    {
                                                                                                        _locTotalBalance = _locAPL.Balance + _locTotalAmountCredit;
                                                                                                    }
                                                                                                }
                                                                                            }

                                                                                            _locAPL.Balance = _locTotalBalance;
                                                                                            _locAPL.Debit = _locAPL.Debit + _locTotalAmountDebit;
                                                                                            _locAPL.Credit = _locAPL.Credit + _locTotalAmountCredit;
                                                                                            _locAPL.Save();
                                                                                            _locAPL.Session.CommitTransaction();

                                                                                        }
                                                                                    }
                                                                                }
                                                                                #endregion COA
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                                #endregion JournalMapByItems

                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = SalesInvoice ", ex.ToString());
            }
        }

        private void SetInvoiceARJournal(Session _currSession, OrderCollection _locOrderCollectionXPO)
        {
            try
            {
                DateTime now = DateTime.Now;
                double _locTotalUnitAmount = 0;
                double _locTotalTaxAmount = 0;
                double _locTotalDiscount = 0;
                double _locTotalFreeItemAmount = 0;
                double _locTotalAmountCollBP = 0;
                double _locTotalAmountCollBPDN = 0;
                double _locTotalAmountCollBPCN = 0;
                double _locTotalAmountCollC = 0;
                double _locTotalAmountCollCDN = 0;
                double _locTotalAmountCollCCN = 0;
                double _locTotalHeaderDiscount = 0;
                ItemAccountGroup _locItemAccountGroup = null;
                TaxAccountGroup _locTaxAccountGroup = null;
                DiscountAccountGroup _locDiscountAccountGroup = null;
                DiscountAccountGroup _locFreeAccountGroup = null;
                BusinessPartnerAccountGroup _locBusinessPartnerAccountGroup = null;
                CompanyAccountGroup _locCompanyAccountGroup = null;
                DiscountAccountGroup _locHeaderDiscountAccountGroup = null;

                if (_locOrderCollectionXPO != null)
                {
                    //1.Based On SalesInvoice
                    XPCollection<OrderCollectionLine> _locOrderCollectionLines = new XPCollection<OrderCollectionLine>(_currSession,
                                                                              new GroupOperator(GroupOperatorType.And,
                                                                              new BinaryOperator("Company", _locOrderCollectionXPO.Company),
                                                                              new BinaryOperator("Workplace", _locOrderCollectionXPO.Workplace),
                                                                              new BinaryOperator("OrderCollection", _locOrderCollectionXPO),
                                                                              new BinaryOperator("Status", Status.Progress)));
                    if(_locOrderCollectionLines != null && _locOrderCollectionLines.Count() > 0)
                    {
                        foreach(OrderCollectionLine _locOrderCollectionLine in _locOrderCollectionLines)
                        {
                            if(_locOrderCollectionLine.PickingMonitoring != null && _locOrderCollectionLine.SalesInvoice != null)
                            {
                                #region BasedOnSalesInvoice
                                if(_locOrderCollectionLine.PickingMonitoring.AmendDelivery == null && _locOrderCollectionLine.PickingMonitoring.SalesInvoiceCmp == null)
                                {
                                    XPCollection<SalesInvoiceMonitoring> _locSalesInvoiceMonitorings = new XPCollection<SalesInvoiceMonitoring>(_currSession,
                                                                                                   new GroupOperator(GroupOperatorType.And,
                                                                                                   new BinaryOperator("SalesInvoice", _locOrderCollectionLine.SalesInvoice)));
                                    if(_locSalesInvoiceMonitorings != null && _locSalesInvoiceMonitorings.Count() > 0)
                                    {
                                        #region JournalMapByItems
                                        foreach (SalesInvoiceMonitoring _locSalesInvoiceMonitoring in _locSalesInvoiceMonitorings)
                                        {
                                            
                                            if (_locSalesInvoiceMonitoring.Item != null)
                                            {
                                                #region JournalMapByNormalItems
                                                if (_locSalesInvoiceMonitoring.SalesInvoiceLine != null)
                                                {
                                                    #region NormalJournal
                                                    if (_locSalesInvoiceMonitoring.SalesInvoiceLine.PriceLine != null)
                                                    {
                                                        _locTotalUnitAmount = _locSalesInvoiceMonitoring.SalesInvoiceLine.PriceLine.Amount1a * _locSalesInvoiceMonitoring.MxTQty;

                                                        ItemAccount _locItemAccount = _currSession.FindObject<ItemAccount>(new GroupOperator(GroupOperatorType.And,
                                                                                new BinaryOperator("Company", _locOrderCollectionLine.Company),
                                                                                new BinaryOperator("Workplace", _locOrderCollectionLine.Workplace),
                                                                                new BinaryOperator("Item", _locSalesInvoiceMonitoring.Item),
                                                                                new BinaryOperator("Active", true)));

                                                        if (_locItemAccount != null)
                                                        {
                                                            if (_locItemAccount.ItemAccountGroup != null)
                                                            {
                                                                _locItemAccountGroup = _locItemAccount.ItemAccountGroup;

                                                                XPCollection<JournalMap> _locJournalMapItems = new XPCollection<JournalMap>
                                                                                               (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                new BinaryOperator("Company", _locOrderCollectionLine.Company),
                                                                                                new BinaryOperator("Workplace", _locOrderCollectionLine.Workplace),
                                                                                                new BinaryOperator("ItemAccountGroup", _locItemAccountGroup),
                                                                                                new BinaryOperator("Active", true)));

                                                                if (_locJournalMapItems != null && _locJournalMapItems.Count() > 0)
                                                                {
                                                                    foreach (JournalMap _locJournalMapItem in _locJournalMapItems)
                                                                    {
                                                                        XPCollection<JournalMapLine> _locJournalMapLines = new XPCollection<JournalMapLine>
                                                                                                                           (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                                            new BinaryOperator("Company", _locOrderCollectionLine.Company),
                                                                                                                            new BinaryOperator("Workplace", _locOrderCollectionLine.Workplace),
                                                                                                                            new BinaryOperator("JournalMap", _locJournalMapItem),
                                                                                                                            new BinaryOperator("Active", true)));

                                                                        if (_locJournalMapLines != null && _locJournalMapLines.Count() > 0)
                                                                        {
                                                                            foreach (JournalMapLine _locJournalMapLine in _locJournalMapLines)
                                                                            {
                                                                                if (_locJournalMapLine.AccountMap != null)
                                                                                {
                                                                                    if (_locJournalMapLine.AccountMap.PostingType == PostingType.Sales && _locJournalMapLine.AccountMap.OrderType == OrderType.Item
                                                                                        && _locJournalMapLine.AccountMap.PostingMethod == PostingMethod.InvoiceAR && _locJournalMapLine.AccountMap.PostingMethodType == PostingMethodType.Normal)
                                                                                    {
                                                                                        XPCollection<AccountMapLine> _locAccountMapLines = new XPCollection<AccountMapLine>
                                                                                                                                       (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                                                        new BinaryOperator("Company", _locOrderCollectionLine.Company),
                                                                                                                                        new BinaryOperator("Workplace", _locOrderCollectionLine.Workplace),
                                                                                                                                        new BinaryOperator("AccountMap", _locJournalMapLine.AccountMap),
                                                                                                                                        new BinaryOperator("Active", true)));

                                                                                        if (_locAccountMapLines != null && _locAccountMapLines.Count() > 0)
                                                                                        {
                                                                                            double _locTotalAmountDebit = 0;
                                                                                            double _locTotalAmountCredit = 0;
                                                                                            double _locTotalBalance = 0;

                                                                                            foreach (AccountMapLine _locAccountMapLine in _locAccountMapLines)
                                                                                            {
                                                                                                if (_locAccountMapLine.AccountCharge == AccountCharge.Debit)
                                                                                                {
                                                                                                    _locTotalAmountDebit = _locTotalUnitAmount;
                                                                                                }
                                                                                                if (_locAccountMapLine.AccountCharge == AccountCharge.Credit)
                                                                                                {
                                                                                                    _locTotalAmountCredit = _locTotalUnitAmount;
                                                                                                }

                                                                                                GeneralJournal _saveGeneralJournal = new GeneralJournal(_currSession)
                                                                                                {
                                                                                                    Company = _locOrderCollectionLine.Company,
                                                                                                    Workplace = _locOrderCollectionLine.Workplace,
                                                                                                    PostingDate = now,
                                                                                                    PostingType = PostingType.Sales,
                                                                                                    OrderType = OrderType.Item,
                                                                                                    PostingMethod = PostingMethod.InvoiceAR,
                                                                                                    PostingMethodType = PostingMethodType.Normal,
                                                                                                    Account = _locAccountMapLine.Account,
                                                                                                    Debit = _locTotalAmountDebit,
                                                                                                    Credit = _locTotalAmountCredit,
                                                                                                    JournalMonth = _locOrderCollectionLine.JournalMonth,
                                                                                                    JournalYear = _locOrderCollectionLine.JournalYear,

                                                                                                    OrderCollection = _locOrderCollectionXPO,
                                                                                                };

                                                                                                _saveGeneralJournal.Save();
                                                                                                _saveGeneralJournal.Session.CommitTransaction();

                                                                                                #region AccountingPeriodicLine

                                                                                                if (_locAccountMapLine.Account.Code != null)
                                                                                                {

                                                                                                    //Cari Account Periodic Line
                                                                                                    AccountingPeriodicLine _locAPL = _currSession.FindObject<AccountingPeriodicLine>
                                                                                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                                                                                    new BinaryOperator("Company", _locOrderCollectionLine.Company),
                                                                                                                                    new BinaryOperator("Workplace", _locOrderCollectionLine.Workplace),
                                                                                                                                    new BinaryOperator("AccountNo", _locAccountMapLine.Account),
                                                                                                                                    new BinaryOperator("Month", _locOrderCollectionLine.JournalMonth),
                                                                                                                                    new BinaryOperator("Year", _locOrderCollectionLine.JournalYear)));
                                                                                                    if (_locAPL != null)
                                                                                                    {
                                                                                                        if (_locAPL.AccountNo != null)
                                                                                                        {
                                                                                                            if (_locAPL.AccountNo.BalanceType == BalanceType.Change)
                                                                                                            {
                                                                                                                if (_locAPL.AccountNo.AccountCharge == AccountCharge.Debit)
                                                                                                                {
                                                                                                                    if (_locTotalAmountDebit > 0)
                                                                                                                    {
                                                                                                                        _locTotalBalance = _locAPL.Balance + _locTotalAmountDebit;
                                                                                                                    }
                                                                                                                    if (_locTotalAmountCredit > 0)
                                                                                                                    {
                                                                                                                        _locTotalBalance = _locAPL.Balance - _locTotalAmountCredit;
                                                                                                                    }
                                                                                                                }
                                                                                                                if (_locAPL.AccountNo.AccountCharge == AccountCharge.Credit)
                                                                                                                {
                                                                                                                    if (_locTotalAmountDebit > 0)
                                                                                                                    {
                                                                                                                        _locTotalBalance = _locAPL.Balance - _locTotalAmountDebit;
                                                                                                                    }
                                                                                                                    if (_locTotalAmountCredit > 0)
                                                                                                                    {
                                                                                                                        _locTotalBalance = _locAPL.Balance + _locTotalAmountCredit;
                                                                                                                    }
                                                                                                                }
                                                                                                            }

                                                                                                            _locAPL.Balance = _locTotalBalance;
                                                                                                            _locAPL.Debit = _locAPL.Debit + _locTotalAmountDebit;
                                                                                                            _locAPL.Credit = _locAPL.Credit + _locTotalAmountCredit;
                                                                                                            _locAPL.Save();
                                                                                                            _locAPL.Session.CommitTransaction();

                                                                                                        }
                                                                                                    }
                                                                                                }
                                                                                                #endregion COA
                                                                                            }
                                                                                        }
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                    #endregion NormalJournal
                                                    #region TaxJournal
                                                    if (_locSalesInvoiceMonitoring.SalesInvoiceLine.Tax != null)
                                                    {
                                                        _locTotalTaxAmount = _locSalesInvoiceMonitoring.SalesInvoiceLine.TxAmount;


                                                        TaxAccount _locTaxAccount = _currSession.FindObject<TaxAccount>(new GroupOperator(GroupOperatorType.And,
                                                                            new BinaryOperator("Company", _locOrderCollectionLine.Company),
                                                                            new BinaryOperator("Workplace", _locOrderCollectionLine.Workplace),
                                                                            new BinaryOperator("Tax", _locSalesInvoiceMonitoring.SalesInvoiceLine.Tax),
                                                                            new BinaryOperator("Active", true)));

                                                        if (_locTaxAccount != null)
                                                        {
                                                            if (_locTaxAccount.TaxAccountGroup != null)
                                                            {
                                                                _locTaxAccountGroup = _locTaxAccount.TaxAccountGroup;

                                                                XPCollection<JournalMap> _locJournalMapItems = new XPCollection<JournalMap>
                                                                                               (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                new BinaryOperator("Company", _locOrderCollectionLine.Company),
                                                                                                new BinaryOperator("Workplace", _locOrderCollectionLine.Workplace),
                                                                                                new BinaryOperator("TaxAccountGroup", _locTaxAccountGroup),
                                                                                                new BinaryOperator("Active", true)));

                                                                if (_locJournalMapItems != null && _locJournalMapItems.Count() > 0)
                                                                {
                                                                    foreach (JournalMap _locJournalMapItem in _locJournalMapItems)
                                                                    {
                                                                        XPCollection<JournalMapLine> _locJournalMapLines = new XPCollection<JournalMapLine>
                                                                                                                           (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                                            new BinaryOperator("Company", _locOrderCollectionLine.Company),
                                                                                                                            new BinaryOperator("Workplace", _locOrderCollectionLine.Workplace),
                                                                                                                            new BinaryOperator("JournalMap", _locJournalMapItem),
                                                                                                                            new BinaryOperator("Active", true)));

                                                                        if (_locJournalMapLines != null && _locJournalMapLines.Count() > 0)
                                                                        {
                                                                            foreach (JournalMapLine _locJournalMapLine in _locJournalMapLines)
                                                                            {
                                                                                if (_locJournalMapLine.AccountMap != null)
                                                                                {
                                                                                    if (_locJournalMapLine.AccountMap.PostingType == PostingType.Sales && _locJournalMapLine.AccountMap.OrderType == OrderType.Item
                                                                                        && _locJournalMapLine.AccountMap.PostingMethod == PostingMethod.InvoiceAR && _locJournalMapLine.AccountMap.PostingMethodType == PostingMethodType.Tax)
                                                                                    {
                                                                                        XPCollection<AccountMapLine> _locAccountMapLines = new XPCollection<AccountMapLine>
                                                                                                                                       (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                                                        new BinaryOperator("Company", _locOrderCollectionLine.Company),
                                                                                                                                        new BinaryOperator("Workplace", _locOrderCollectionLine.Workplace),
                                                                                                                                        new BinaryOperator("AccountMap", _locJournalMapLine.AccountMap),
                                                                                                                                        new BinaryOperator("Active", true)));

                                                                                        if (_locAccountMapLines != null && _locAccountMapLines.Count() > 0)
                                                                                        {
                                                                                            double _locTotalAmountDebit = 0;
                                                                                            double _locTotalAmountCredit = 0;
                                                                                            double _locTotalBalance = 0;

                                                                                            foreach (AccountMapLine _locAccountMapLine in _locAccountMapLines)
                                                                                            {
                                                                                                if (_locAccountMapLine.AccountCharge == AccountCharge.Debit)
                                                                                                {
                                                                                                    _locTotalAmountDebit = _locTotalTaxAmount;
                                                                                                }
                                                                                                if (_locAccountMapLine.AccountCharge == AccountCharge.Credit)
                                                                                                {
                                                                                                    _locTotalAmountCredit = _locTotalTaxAmount;
                                                                                                }

                                                                                                GeneralJournal _saveGeneralJournal = new GeneralJournal(_currSession)
                                                                                                {
                                                                                                    Company = _locOrderCollectionLine.Company,
                                                                                                    Workplace = _locOrderCollectionLine.Workplace,
                                                                                                    PostingDate = now,
                                                                                                    PostingType = PostingType.Sales,
                                                                                                    OrderType = OrderType.Item,
                                                                                                    PostingMethod = PostingMethod.InvoiceAR,
                                                                                                    PostingMethodType = PostingMethodType.Tax,
                                                                                                    Account = _locAccountMapLine.Account,
                                                                                                    Debit = _locTotalAmountDebit,
                                                                                                    Credit = _locTotalAmountCredit,
                                                                                                    JournalMonth = _locOrderCollectionLine.JournalMonth,
                                                                                                    JournalYear = _locOrderCollectionLine.JournalYear,

                                                                                                    OrderCollection = _locOrderCollectionXPO,
                                                                                                };

                                                                                                _saveGeneralJournal.Save();
                                                                                                _saveGeneralJournal.Session.CommitTransaction();

                                                                                                #region AccountingPeriodicLine

                                                                                                if (_locAccountMapLine.Account.Code != null)
                                                                                                {

                                                                                                    //Cari Account Periodic Line
                                                                                                    AccountingPeriodicLine _locAPL = _currSession.FindObject<AccountingPeriodicLine>
                                                                                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                                                                                    new BinaryOperator("Company", _locOrderCollectionLine.Company),
                                                                                                                                    new BinaryOperator("Workplace", _locOrderCollectionLine.Workplace),
                                                                                                                                    new BinaryOperator("AccountNo", _locAccountMapLine.Account),
                                                                                                                                    new BinaryOperator("Month", _locOrderCollectionLine.JournalMonth),
                                                                                                                                    new BinaryOperator("Year", _locOrderCollectionLine.JournalYear)));
                                                                                                    if (_locAPL != null)
                                                                                                    {
                                                                                                        if (_locAPL.AccountNo != null)
                                                                                                        {
                                                                                                            if (_locAPL.AccountNo.BalanceType == BalanceType.Change)
                                                                                                            {
                                                                                                                if (_locAPL.AccountNo.AccountCharge == AccountCharge.Debit)
                                                                                                                {
                                                                                                                    if (_locTotalAmountDebit > 0)
                                                                                                                    {
                                                                                                                        _locTotalBalance = _locAPL.Balance + _locTotalAmountDebit;
                                                                                                                    }
                                                                                                                    if (_locTotalAmountCredit > 0)
                                                                                                                    {
                                                                                                                        _locTotalBalance = _locAPL.Balance - _locTotalAmountCredit;
                                                                                                                    }
                                                                                                                }
                                                                                                                if (_locAPL.AccountNo.AccountCharge == AccountCharge.Credit)
                                                                                                                {
                                                                                                                    if (_locTotalAmountDebit > 0)
                                                                                                                    {
                                                                                                                        _locTotalBalance = _locAPL.Balance - _locTotalAmountDebit;
                                                                                                                    }
                                                                                                                    if (_locTotalAmountCredit > 0)
                                                                                                                    {
                                                                                                                        _locTotalBalance = _locAPL.Balance + _locTotalAmountCredit;
                                                                                                                    }
                                                                                                                }
                                                                                                            }

                                                                                                            _locAPL.Balance = _locTotalBalance;
                                                                                                            _locAPL.Debit = _locAPL.Debit + _locTotalAmountDebit;
                                                                                                            _locAPL.Credit = _locAPL.Credit + _locTotalAmountCredit;
                                                                                                            _locAPL.Save();
                                                                                                            _locAPL.Session.CommitTransaction();

                                                                                                        }
                                                                                                    }
                                                                                                }
                                                                                                #endregion COA
                                                                                            }
                                                                                        }
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }  
                                                    }
                                                    #endregion TaxJournal
                                                    #region DiscountJournal
                                                    if (_locSalesInvoiceMonitoring.SalesInvoiceLine.DiscountRule != null)
                                                    {

                                                        _locTotalDiscount = _locSalesInvoiceMonitoring.SalesInvoiceLine.DiscAmount;
                                                        #region DiscountAccountGroup
                                                        if (_locSalesInvoiceMonitoring.SalesInvoiceLine.DiscountRule.DiscountAccountGroup != null)
                                                        {
                                                            _locDiscountAccountGroup = _locSalesInvoiceMonitoring.SalesInvoiceLine.DiscountRule.DiscountAccountGroup;

                                                            XPCollection<JournalMap> _locJournalMapItems = new XPCollection<JournalMap>
                                                                                               (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                new BinaryOperator("Company", _locOrderCollectionLine.Company),
                                                                                                new BinaryOperator("Workplace", _locOrderCollectionLine.Workplace),
                                                                                                new BinaryOperator("DiscountAccountGroup", _locDiscountAccountGroup),
                                                                                                new BinaryOperator("Active", true)));

                                                            if (_locJournalMapItems != null && _locJournalMapItems.Count() > 0)
                                                            {
                                                                foreach (JournalMap _locJournalMapItem in _locJournalMapItems)
                                                                {
                                                                    XPCollection<JournalMapLine> _locJournalMapLines = new XPCollection<JournalMapLine>
                                                                                                                       (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                                        new BinaryOperator("Company", _locOrderCollectionLine.Company),
                                                                                                                        new BinaryOperator("Workplace", _locOrderCollectionLine.Workplace),
                                                                                                                        new BinaryOperator("JournalMap", _locJournalMapItem),
                                                                                                                        new BinaryOperator("Active", true)));

                                                                    if (_locJournalMapLines != null && _locJournalMapLines.Count() > 0)
                                                                    {
                                                                        foreach (JournalMapLine _locJournalMapLine in _locJournalMapLines)
                                                                        {
                                                                            if (_locJournalMapLine.AccountMap != null)
                                                                            {
                                                                                if (_locJournalMapLine.AccountMap.PostingType == PostingType.Sales && _locJournalMapLine.AccountMap.OrderType == OrderType.Item
                                                                                    && _locJournalMapLine.AccountMap.PostingMethod == PostingMethod.InvoiceAR && _locJournalMapLine.AccountMap.PostingMethodType == PostingMethodType.Discount)
                                                                                {
                                                                                    XPCollection<AccountMapLine> _locAccountMapLines = new XPCollection<AccountMapLine>
                                                                                                                                   (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                                                    new BinaryOperator("Company", _locOrderCollectionLine.Company),
                                                                                                                                    new BinaryOperator("Workplace", _locOrderCollectionLine.Workplace),
                                                                                                                                    new BinaryOperator("AccountMap", _locJournalMapLine.AccountMap),
                                                                                                                                    new BinaryOperator("Active", true)));

                                                                                    if (_locAccountMapLines != null && _locAccountMapLines.Count() > 0)
                                                                                    {
                                                                                        double _locTotalAmountDebit = 0;
                                                                                        double _locTotalAmountCredit = 0;
                                                                                        double _locTotalBalance = 0;

                                                                                        foreach (AccountMapLine _locAccountMapLine in _locAccountMapLines)
                                                                                        {
                                                                                            if (_locAccountMapLine.AccountCharge == AccountCharge.Debit)
                                                                                            {
                                                                                                _locTotalAmountDebit = _locTotalDiscount;
                                                                                            }
                                                                                            if (_locAccountMapLine.AccountCharge == AccountCharge.Credit)
                                                                                            {
                                                                                                _locTotalAmountCredit = _locTotalDiscount;
                                                                                            }

                                                                                            GeneralJournal _saveGeneralJournal = new GeneralJournal(_currSession)
                                                                                            {
                                                                                                Company = _locOrderCollectionLine.Company,
                                                                                                Workplace = _locOrderCollectionLine.Workplace,
                                                                                                PostingDate = now,
                                                                                                PostingType = PostingType.Sales,
                                                                                                OrderType = OrderType.Item,
                                                                                                PostingMethod = PostingMethod.InvoiceAR,
                                                                                                PostingMethodType = PostingMethodType.Discount,
                                                                                                Account = _locAccountMapLine.Account,
                                                                                                Debit = _locTotalAmountDebit,
                                                                                                Credit = _locTotalAmountCredit,
                                                                                                JournalMonth = _locOrderCollectionLine.JournalMonth,
                                                                                                JournalYear = _locOrderCollectionLine.JournalYear,
                                                                                                OrderCollection = _locOrderCollectionXPO,
                                                                                            };

                                                                                            _saveGeneralJournal.Save();
                                                                                            _saveGeneralJournal.Session.CommitTransaction();

                                                                                            #region AccountingPeriodicLine

                                                                                            if (_locAccountMapLine.Account.Code != null)
                                                                                            {

                                                                                                //Cari Account Periodic Line
                                                                                                AccountingPeriodicLine _locAPL = _currSession.FindObject<AccountingPeriodicLine>
                                                                                                                                (new GroupOperator(GroupOperatorType.And,
                                                                                                                                new BinaryOperator("Company", _locOrderCollectionLine.Company),
                                                                                                                                new BinaryOperator("Workplace", _locOrderCollectionLine.Workplace),
                                                                                                                                new BinaryOperator("AccountNo", _locAccountMapLine.Account),
                                                                                                                                new BinaryOperator("Month", _locOrderCollectionLine.JournalMonth),
                                                                                                                                new BinaryOperator("Year", _locOrderCollectionLine.JournalYear)));
                                                                                                if (_locAPL != null)
                                                                                                {
                                                                                                    if (_locAPL.AccountNo != null)
                                                                                                    {
                                                                                                        if (_locAPL.AccountNo.BalanceType == BalanceType.Change)
                                                                                                        {
                                                                                                            if (_locAPL.AccountNo.AccountCharge == AccountCharge.Debit)
                                                                                                            {
                                                                                                                if (_locTotalAmountDebit > 0)
                                                                                                                {
                                                                                                                    _locTotalBalance = _locAPL.Balance + _locTotalAmountDebit;
                                                                                                                }
                                                                                                                if (_locTotalAmountCredit > 0)
                                                                                                                {
                                                                                                                    _locTotalBalance = _locAPL.Balance - _locTotalAmountCredit;
                                                                                                                }
                                                                                                            }
                                                                                                            if (_locAPL.AccountNo.AccountCharge == AccountCharge.Credit)
                                                                                                            {
                                                                                                                if (_locTotalAmountDebit > 0)
                                                                                                                {
                                                                                                                    _locTotalBalance = _locAPL.Balance - _locTotalAmountDebit;
                                                                                                                }
                                                                                                                if (_locTotalAmountCredit > 0)
                                                                                                                {
                                                                                                                    _locTotalBalance = _locAPL.Balance + _locTotalAmountCredit;
                                                                                                                }
                                                                                                            }
                                                                                                        }

                                                                                                        _locAPL.Balance = _locTotalBalance;
                                                                                                        _locAPL.Debit = _locAPL.Debit + _locTotalAmountDebit;
                                                                                                        _locAPL.Credit = _locAPL.Credit + _locTotalAmountCredit;
                                                                                                        _locAPL.Save();
                                                                                                        _locAPL.Session.CommitTransaction();

                                                                                                    }
                                                                                                }
                                                                                            }
                                                                                            #endregion COA
                                                                                        }
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                        #endregion DiscountAccountGroup
                                                        if (_locSalesInvoiceMonitoring.SalesInvoiceLine.AccountingPeriodicLine != null)
                                                        {
                                                            if(_locSalesInvoiceMonitoring.SalesInvoiceLine.AccountingPeriodicLine.BalHold < _locTotalDiscount)
                                                            {
                                                                _locTotalDiscount = _locSalesInvoiceMonitoring.SalesInvoiceLine.AccountingPeriodicLine.BalHold;
                                                            }
                                                            _locSalesInvoiceMonitoring.SalesInvoiceLine.AccountingPeriodicLine.BalHold = _locSalesInvoiceMonitoring.SalesInvoiceLine.AccountingPeriodicLine.BalHold - _locTotalDiscount;
                                                            _locSalesInvoiceMonitoring.SalesInvoiceLine.AccountingPeriodicLine.Save();
                                                            _locSalesInvoiceMonitoring.SalesInvoiceLine.AccountingPeriodicLine.Session.CommitTransaction();
                                                        }
                                                        
                                                    }
                                                    #endregion DiscountJournal  
                                                }
                                                #endregion JournalMapByNormalItems
                                                #region JournalMapByFreeItems
                                                if (_locSalesInvoiceMonitoring.SalesInvoiceFreeItem != null)
                                                {

                                                    if(_locSalesInvoiceMonitoring.SalesInvoiceFreeItem.FreeItemRule != null)
                                                    {
                                                        if(_locSalesInvoiceMonitoring.SalesInvoiceFreeItem.FreeItemRule.DiscountAccountGroup != null)
                                                        {
                                                            _locTotalFreeItemAmount = _locSalesInvoiceMonitoring.SalesInvoiceFreeItem.TUAmount;

                                                            _locFreeAccountGroup = _locSalesInvoiceMonitoring.SalesInvoiceFreeItem.FreeItemRule.DiscountAccountGroup;

                                                            XPCollection<JournalMap> _locJournalMapItems = new XPCollection<JournalMap>
                                                                                               (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                new BinaryOperator("Company", _locOrderCollectionLine.Company),
                                                                                                new BinaryOperator("Workplace", _locOrderCollectionLine.Workplace),
                                                                                                new BinaryOperator("DiscountAccountGroup", _locFreeAccountGroup),
                                                                                                new BinaryOperator("Active", true)));

                                                            if (_locJournalMapItems != null && _locJournalMapItems.Count() > 0)
                                                            {
                                                                foreach (JournalMap _locJournalMapItem in _locJournalMapItems)
                                                                {
                                                                    XPCollection<JournalMapLine> _locJournalMapLines = new XPCollection<JournalMapLine>
                                                                                                                       (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                                        new BinaryOperator("Company", _locOrderCollectionLine.Company),
                                                                                                                        new BinaryOperator("Workplace", _locOrderCollectionLine.Workplace),
                                                                                                                        new BinaryOperator("JournalMap", _locJournalMapItem),
                                                                                                                        new BinaryOperator("Active", true)));

                                                                    if (_locJournalMapLines != null && _locJournalMapLines.Count() > 0)
                                                                    {
                                                                        foreach (JournalMapLine _locJournalMapLine in _locJournalMapLines)
                                                                        {
                                                                            if (_locJournalMapLine.AccountMap != null)
                                                                            {
                                                                                if (_locJournalMapLine.AccountMap.PostingType == PostingType.Sales && _locJournalMapLine.AccountMap.OrderType == OrderType.Item
                                                                                    && _locJournalMapLine.AccountMap.PostingMethod == PostingMethod.InvoiceAR && _locJournalMapLine.AccountMap.PostingMethodType == PostingMethodType.FreeItem)
                                                                                {
                                                                                    XPCollection<AccountMapLine> _locAccountMapLines = new XPCollection<AccountMapLine>
                                                                                                                                   (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                                                    new BinaryOperator("Company", _locOrderCollectionLine.Company),
                                                                                                                                    new BinaryOperator("Workplace", _locOrderCollectionLine.Workplace),
                                                                                                                                    new BinaryOperator("AccountMap", _locJournalMapLine.AccountMap),
                                                                                                                                    new BinaryOperator("Active", true)));

                                                                                    if (_locAccountMapLines != null && _locAccountMapLines.Count() > 0)
                                                                                    {
                                                                                        double _locTotalAmountDebit = 0;
                                                                                        double _locTotalAmountCredit = 0;
                                                                                        double _locTotalBalance = 0;

                                                                                        foreach (AccountMapLine _locAccountMapLine in _locAccountMapLines)
                                                                                        {
                                                                                            if (_locAccountMapLine.AccountCharge == AccountCharge.Debit)
                                                                                            {
                                                                                                _locTotalAmountDebit = _locTotalFreeItemAmount;
                                                                                            }
                                                                                            if (_locAccountMapLine.AccountCharge == AccountCharge.Credit)
                                                                                            {
                                                                                                _locTotalAmountCredit = _locTotalFreeItemAmount;
                                                                                            }

                                                                                            GeneralJournal _saveGeneralJournal = new GeneralJournal(_currSession)
                                                                                            {
                                                                                                Company = _locOrderCollectionLine.Company,
                                                                                                Workplace = _locOrderCollectionLine.Workplace,
                                                                                                PostingDate = now,
                                                                                                PostingType = PostingType.Sales,
                                                                                                OrderType = OrderType.Item,
                                                                                                PostingMethod = PostingMethod.InvoiceAR,
                                                                                                PostingMethodType = PostingMethodType.FreeItem,
                                                                                                Account = _locAccountMapLine.Account,
                                                                                                Debit = _locTotalAmountDebit,
                                                                                                Credit = _locTotalAmountCredit,
                                                                                                JournalMonth = _locOrderCollectionLine.JournalMonth,
                                                                                                JournalYear = _locOrderCollectionLine.JournalYear,
                                                                                                OrderCollection = _locOrderCollectionXPO,
                                                                                            };

                                                                                            _saveGeneralJournal.Save();
                                                                                            _saveGeneralJournal.Session.CommitTransaction();

                                                                                            #region AccountingPeriodicLine

                                                                                            if (_locAccountMapLine.Account.Code != null)
                                                                                            {

                                                                                                //Cari Account Periodic Line
                                                                                                AccountingPeriodicLine _locAPL = _currSession.FindObject<AccountingPeriodicLine>
                                                                                                                                (new GroupOperator(GroupOperatorType.And,
                                                                                                                                new BinaryOperator("Company", _locOrderCollectionLine.Company),
                                                                                                                                new BinaryOperator("Workplace", _locOrderCollectionLine.Workplace),
                                                                                                                                new BinaryOperator("AccountNo", _locAccountMapLine.Account),
                                                                                                                                new BinaryOperator("Month", _locOrderCollectionLine.JournalMonth),
                                                                                                                                new BinaryOperator("Year", _locOrderCollectionLine.JournalYear)));
                                                                                                if (_locAPL != null)
                                                                                                {
                                                                                                    if (_locAPL.AccountNo != null)
                                                                                                    {
                                                                                                        if (_locAPL.AccountNo.BalanceType == BalanceType.Change)
                                                                                                        {
                                                                                                            if (_locAPL.AccountNo.AccountCharge == AccountCharge.Debit)
                                                                                                            {
                                                                                                                if (_locTotalAmountDebit > 0)
                                                                                                                {
                                                                                                                    _locTotalBalance = _locAPL.Balance + _locTotalAmountDebit;
                                                                                                                }
                                                                                                                if (_locTotalAmountCredit > 0)
                                                                                                                {
                                                                                                                    _locTotalBalance = _locAPL.Balance - _locTotalAmountCredit;
                                                                                                                }
                                                                                                            }
                                                                                                            if (_locAPL.AccountNo.AccountCharge == AccountCharge.Credit)
                                                                                                            {
                                                                                                                if (_locTotalAmountDebit > 0)
                                                                                                                {
                                                                                                                    _locTotalBalance = _locAPL.Balance - _locTotalAmountDebit;
                                                                                                                }
                                                                                                                if (_locTotalAmountCredit > 0)
                                                                                                                {
                                                                                                                    _locTotalBalance = _locAPL.Balance + _locTotalAmountCredit;
                                                                                                                }
                                                                                                            }
                                                                                                        }

                                                                                                        _locAPL.Balance = _locTotalBalance;
                                                                                                        _locAPL.Debit = _locAPL.Debit + _locTotalAmountDebit;
                                                                                                        _locAPL.Credit = _locAPL.Credit + _locTotalAmountCredit;
                                                                                                        _locAPL.Save();
                                                                                                        _locAPL.Session.CommitTransaction();

                                                                                                    }
                                                                                                }
                                                                                            }
                                                                                            #endregion COA
                                                                                        }
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                                #endregion JournalMapByFreeItems

                                                _locTotalUnitAmount = 0;
                                                _locTotalTaxAmount = 0;
                                                _locTotalDiscount = 0;
                                                _locTotalFreeItemAmount = 0;
                                            }
                                            
                                        }
                                        #endregion JournalMapByItems

                                        #region JournalMapBusinessPartnerAccountGroup

                                        if (_locOrderCollectionLine.SalesInvoice.BillToCustomer != null)
                                        {
                                            BusinessPartnerAccount _locBusinessPartnerAccount = _currSession.FindObject<BusinessPartnerAccount>(new GroupOperator(GroupOperatorType.And,
                                                                                        new BinaryOperator("Company", _locOrderCollectionLine.Company),
                                                                                        new BinaryOperator("Workplace", _locOrderCollectionLine.Workplace),
                                                                                        new BinaryOperator("BusinessPartner", _locOrderCollectionLine.SalesInvoice.BillToCustomer),
                                                                                        new BinaryOperator("Active", true)));
                                            if(_locBusinessPartnerAccount != null)
                                            {
                                                if (_locBusinessPartnerAccount.BusinessPartnerAccountGroup != null)
                                                {
                                                    _locBusinessPartnerAccountGroup = _locBusinessPartnerAccount.BusinessPartnerAccountGroup;

                                                    _locTotalAmountCollBP = _locOrderCollectionLine.AmountColl;
                                                    _locTotalAmountCollBPDN = _locOrderCollectionLine.AmountDN;
                                                    _locTotalAmountCollBPCN = _locOrderCollectionLine.AmountCN;

                                                    XPCollection<JournalMap> _locJournalMapItems = new XPCollection<JournalMap>
                                                                                       (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                        new BinaryOperator("Company", _locOrderCollectionLine.Company),
                                                                                        new BinaryOperator("Workplace", _locOrderCollectionLine.Workplace),
                                                                                        new BinaryOperator("BusinessPartnerAccountGroup", _locBusinessPartnerAccountGroup),
                                                                                        new BinaryOperator("Active", true)));

                                                    if (_locJournalMapItems != null && _locJournalMapItems.Count() > 0)
                                                    {
                                                        foreach (JournalMap _locJournalMapItem in _locJournalMapItems)
                                                        {
                                                            XPCollection<JournalMapLine> _locJournalMapLines = new XPCollection<JournalMapLine>
                                                                                                               (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                                new BinaryOperator("Company", _locOrderCollectionLine.Company),
                                                                                                                new BinaryOperator("Workplace", _locOrderCollectionLine.Workplace),
                                                                                                                new BinaryOperator("JournalMap", _locJournalMapItem),
                                                                                                                new BinaryOperator("Active", true)));

                                                            if (_locJournalMapLines != null && _locJournalMapLines.Count() > 0)
                                                            {
                                                                foreach (JournalMapLine _locJournalMapLine in _locJournalMapLines)
                                                                {
                                                                    if (_locJournalMapLine.AccountMap != null)
                                                                    {
                                                                        #region NormalAmount
                                                                        if (_locJournalMapLine.AccountMap.PostingType == PostingType.Sales && _locJournalMapLine.AccountMap.OrderType == OrderType.Item
                                                                            && _locJournalMapLine.AccountMap.PostingMethod == PostingMethod.InvoiceAR && _locJournalMapLine.AccountMap.PostingMethodType == PostingMethodType.Normal)
                                                                        {
                                                                            XPCollection<AccountMapLine> _locAccountMapLines = new XPCollection<AccountMapLine>
                                                                                                                           (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                                            new BinaryOperator("Company", _locOrderCollectionLine.Company),
                                                                                                                            new BinaryOperator("Workplace", _locOrderCollectionLine.Workplace),
                                                                                                                            new BinaryOperator("AccountMap", _locJournalMapLine.AccountMap),
                                                                                                                            new BinaryOperator("Active", true)));

                                                                            if (_locAccountMapLines != null && _locAccountMapLines.Count() > 0)
                                                                            {
                                                                                double _locTotalAmountDebit = 0;
                                                                                double _locTotalAmountCredit = 0;
                                                                                double _locTotalBalance = 0;

                                                                                foreach (AccountMapLine _locAccountMapLine in _locAccountMapLines)
                                                                                {
                                                                                    if (_locAccountMapLine.AccountCharge == AccountCharge.Debit)
                                                                                    {
                                                                                        _locTotalAmountDebit = _locTotalAmountCollBP;
                                                                                    }
                                                                                    if (_locAccountMapLine.AccountCharge == AccountCharge.Credit)
                                                                                    {
                                                                                        _locTotalAmountCredit = _locTotalAmountCollBP;
                                                                                    }

                                                                                    GeneralJournal _saveGeneralJournal = new GeneralJournal(_currSession)
                                                                                    {
                                                                                        Company = _locOrderCollectionLine.Company,
                                                                                        Workplace = _locOrderCollectionLine.Workplace,
                                                                                        PostingDate = now,
                                                                                        PostingType = PostingType.Sales,
                                                                                        OrderType = OrderType.Item,
                                                                                        PostingMethod = PostingMethod.InvoiceAR,
                                                                                        PostingMethodType = PostingMethodType.Normal,
                                                                                        Account = _locAccountMapLine.Account,
                                                                                        Debit = _locTotalAmountDebit,
                                                                                        Credit = _locTotalAmountCredit,
                                                                                        JournalMonth = _locOrderCollectionLine.JournalMonth,
                                                                                        JournalYear = _locOrderCollectionLine.JournalYear,
                                                                                        OrderCollection = _locOrderCollectionXPO,
                                                                                    };

                                                                                    _saveGeneralJournal.Save();
                                                                                    _saveGeneralJournal.Session.CommitTransaction();

                                                                                    #region AccountingPeriodicLine

                                                                                    if (_locAccountMapLine.Account.Code != null)
                                                                                    {

                                                                                        //Cari Account Periodic Line
                                                                                        AccountingPeriodicLine _locAPL = _currSession.FindObject<AccountingPeriodicLine>
                                                                                                                        (new GroupOperator(GroupOperatorType.And,
                                                                                                                        new BinaryOperator("Company", _locOrderCollectionLine.Company),
                                                                                                                        new BinaryOperator("Workplace", _locOrderCollectionLine.Workplace),
                                                                                                                        new BinaryOperator("AccountNo", _locAccountMapLine.Account),
                                                                                                                        new BinaryOperator("Month", _locOrderCollectionLine.JournalMonth),
                                                                                                                        new BinaryOperator("Year", _locOrderCollectionLine.JournalYear)));
                                                                                        if (_locAPL != null)
                                                                                        {
                                                                                            if (_locAPL.AccountNo != null)
                                                                                            {
                                                                                                if (_locAPL.AccountNo.BalanceType == BalanceType.Change)
                                                                                                {
                                                                                                    if (_locAPL.AccountNo.AccountCharge == AccountCharge.Debit)
                                                                                                    {
                                                                                                        if (_locTotalAmountDebit > 0)
                                                                                                        {
                                                                                                            _locTotalBalance = _locAPL.Balance + _locTotalAmountDebit;
                                                                                                        }
                                                                                                        if (_locTotalAmountCredit > 0)
                                                                                                        {
                                                                                                            _locTotalBalance = _locAPL.Balance - _locTotalAmountCredit;
                                                                                                        }
                                                                                                    }
                                                                                                    if (_locAPL.AccountNo.AccountCharge == AccountCharge.Credit)
                                                                                                    {
                                                                                                        if (_locTotalAmountDebit > 0)
                                                                                                        {
                                                                                                            _locTotalBalance = _locAPL.Balance - _locTotalAmountDebit;
                                                                                                        }
                                                                                                        if (_locTotalAmountCredit > 0)
                                                                                                        {
                                                                                                            _locTotalBalance = _locAPL.Balance + _locTotalAmountCredit;
                                                                                                        }
                                                                                                    }
                                                                                                }

                                                                                                _locAPL.Balance = _locTotalBalance;
                                                                                                _locAPL.Debit = _locAPL.Debit + _locTotalAmountDebit;
                                                                                                _locAPL.Credit = _locAPL.Credit + _locTotalAmountCredit;
                                                                                                _locAPL.Save();
                                                                                                _locAPL.Session.CommitTransaction();

                                                                                            }
                                                                                        }
                                                                                    }
                                                                                    #endregion COA
                                                                                }
                                                                            }
                                                                        }
                                                                        #endregion NormalAmount
                                                                        #region AmountDN
                                                                        if (_locTotalAmountCollBPDN > 0)
                                                                        {
                                                                            if (_locJournalMapLine.AccountMap.PostingType == PostingType.Sales && _locJournalMapLine.AccountMap.OrderType == OrderType.Item
                                                                            && _locJournalMapLine.AccountMap.PostingMethod == PostingMethod.InvoiceAR && _locJournalMapLine.AccountMap.PostingMethodType == PostingMethodType.AmountDN)
                                                                            {
                                                                                XPCollection<AccountMapLine> _locAccountMapLines = new XPCollection<AccountMapLine>
                                                                                                                               (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                                                new BinaryOperator("Company", _locOrderCollectionLine.Company),
                                                                                                                                new BinaryOperator("Workplace", _locOrderCollectionLine.Workplace),
                                                                                                                                new BinaryOperator("AccountMap", _locJournalMapLine.AccountMap),
                                                                                                                                new BinaryOperator("Active", true)));

                                                                                if (_locAccountMapLines != null && _locAccountMapLines.Count() > 0)
                                                                                {
                                                                                    double _locTotalAmountDebit = 0;
                                                                                    double _locTotalAmountCredit = 0;
                                                                                    double _locTotalBalance = 0;

                                                                                    foreach (AccountMapLine _locAccountMapLine in _locAccountMapLines)
                                                                                    {
                                                                                        if (_locAccountMapLine.AccountCharge == AccountCharge.Debit)
                                                                                        {
                                                                                            _locTotalAmountDebit = _locTotalAmountCollBPDN;
                                                                                        }
                                                                                        if (_locAccountMapLine.AccountCharge == AccountCharge.Credit)
                                                                                        {
                                                                                            _locTotalAmountCredit = _locTotalAmountCollBPDN;
                                                                                        }

                                                                                        GeneralJournal _saveGeneralJournal = new GeneralJournal(_currSession)
                                                                                        {
                                                                                            Company = _locOrderCollectionLine.Company,
                                                                                            Workplace = _locOrderCollectionLine.Workplace,
                                                                                            PostingDate = now,
                                                                                            PostingType = PostingType.Sales,
                                                                                            OrderType = OrderType.Item,
                                                                                            PostingMethod = PostingMethod.InvoiceAR,
                                                                                            PostingMethodType = PostingMethodType.AmountDN,
                                                                                            Account = _locAccountMapLine.Account,
                                                                                            Debit = _locTotalAmountDebit,
                                                                                            Credit = _locTotalAmountCredit,
                                                                                            JournalMonth = _locOrderCollectionLine.JournalMonth,
                                                                                            JournalYear = _locOrderCollectionLine.JournalYear,
                                                                                            OrderCollection = _locOrderCollectionXPO,
                                                                                        };

                                                                                        _saveGeneralJournal.Save();
                                                                                        _saveGeneralJournal.Session.CommitTransaction();

                                                                                        #region AccountingPeriodicLine

                                                                                        if (_locAccountMapLine.Account.Code != null)
                                                                                        {

                                                                                            //Cari Account Periodic Line
                                                                                            AccountingPeriodicLine _locAPL = _currSession.FindObject<AccountingPeriodicLine>
                                                                                                                            (new GroupOperator(GroupOperatorType.And,
                                                                                                                            new BinaryOperator("Company", _locOrderCollectionLine.Company),
                                                                                                                            new BinaryOperator("Workplace", _locOrderCollectionLine.Workplace),
                                                                                                                            new BinaryOperator("AccountNo", _locAccountMapLine.Account),
                                                                                                                            new BinaryOperator("Month", _locOrderCollectionLine.JournalMonth),
                                                                                                                            new BinaryOperator("Year", _locOrderCollectionLine.JournalYear)));
                                                                                            if (_locAPL != null)
                                                                                            {
                                                                                                if (_locAPL.AccountNo != null)
                                                                                                {
                                                                                                    if (_locAPL.AccountNo.BalanceType == BalanceType.Change)
                                                                                                    {
                                                                                                        if (_locAPL.AccountNo.AccountCharge == AccountCharge.Debit)
                                                                                                        {
                                                                                                            if (_locTotalAmountDebit > 0)
                                                                                                            {
                                                                                                                _locTotalBalance = _locAPL.Balance + _locTotalAmountDebit;
                                                                                                            }
                                                                                                            if (_locTotalAmountCredit > 0)
                                                                                                            {
                                                                                                                _locTotalBalance = _locAPL.Balance - _locTotalAmountCredit;
                                                                                                            }
                                                                                                        }
                                                                                                        if (_locAPL.AccountNo.AccountCharge == AccountCharge.Credit)
                                                                                                        {
                                                                                                            if (_locTotalAmountDebit > 0)
                                                                                                            {
                                                                                                                _locTotalBalance = _locAPL.Balance - _locTotalAmountDebit;
                                                                                                            }
                                                                                                            if (_locTotalAmountCredit > 0)
                                                                                                            {
                                                                                                                _locTotalBalance = _locAPL.Balance + _locTotalAmountCredit;
                                                                                                            }
                                                                                                        }
                                                                                                    }

                                                                                                    _locAPL.Balance = _locTotalBalance;
                                                                                                    _locAPL.Debit = _locAPL.Debit + _locTotalAmountDebit;
                                                                                                    _locAPL.Credit = _locAPL.Credit + _locTotalAmountCredit;
                                                                                                    _locAPL.Save();
                                                                                                    _locAPL.Session.CommitTransaction();

                                                                                                }
                                                                                            }
                                                                                        }
                                                                                        #endregion COA
                                                                                    }
                                                                                }
                                                                            }
                                                                        }

                                                                        #endregion AmountDN
                                                                        #region AmountCN
                                                                        if (_locTotalAmountCollBPCN > 0)
                                                                        {
                                                                            if (_locJournalMapLine.AccountMap.PostingType == PostingType.Sales && _locJournalMapLine.AccountMap.OrderType == OrderType.Item
                                                                            && _locJournalMapLine.AccountMap.PostingMethod == PostingMethod.InvoiceAR && _locJournalMapLine.AccountMap.PostingMethodType == PostingMethodType.AmountCN)
                                                                            {
                                                                                XPCollection<AccountMapLine> _locAccountMapLines = new XPCollection<AccountMapLine>
                                                                                                                               (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                                                new BinaryOperator("Company", _locOrderCollectionLine.Company),
                                                                                                                                new BinaryOperator("Workplace", _locOrderCollectionLine.Workplace),
                                                                                                                                new BinaryOperator("AccountMap", _locJournalMapLine.AccountMap),
                                                                                                                                new BinaryOperator("Active", true)));

                                                                                if (_locAccountMapLines != null && _locAccountMapLines.Count() > 0)
                                                                                {
                                                                                    double _locTotalAmountDebit = 0;
                                                                                    double _locTotalAmountCredit = 0;
                                                                                    double _locTotalBalance = 0;

                                                                                    foreach (AccountMapLine _locAccountMapLine in _locAccountMapLines)
                                                                                    {
                                                                                        if (_locAccountMapLine.AccountCharge == AccountCharge.Debit)
                                                                                        {
                                                                                            _locTotalAmountDebit = _locTotalAmountCollBPCN;
                                                                                        }
                                                                                        if (_locAccountMapLine.AccountCharge == AccountCharge.Credit)
                                                                                        {
                                                                                            _locTotalAmountCredit = _locTotalAmountCollBPCN;
                                                                                        }

                                                                                        GeneralJournal _saveGeneralJournal = new GeneralJournal(_currSession)
                                                                                        {
                                                                                            Company = _locOrderCollectionLine.Company,
                                                                                            Workplace = _locOrderCollectionLine.Workplace,
                                                                                            PostingDate = now,
                                                                                            PostingType = PostingType.Sales,
                                                                                            OrderType = OrderType.Item,
                                                                                            PostingMethod = PostingMethod.InvoiceAR,
                                                                                            PostingMethodType = PostingMethodType.AmountCN,
                                                                                            Account = _locAccountMapLine.Account,
                                                                                            Debit = _locTotalAmountDebit,
                                                                                            Credit = _locTotalAmountCredit,
                                                                                            JournalMonth = _locOrderCollectionLine.JournalMonth,
                                                                                            JournalYear = _locOrderCollectionLine.JournalYear,
                                                                                            OrderCollection = _locOrderCollectionXPO,
                                                                                        };

                                                                                        _saveGeneralJournal.Save();
                                                                                        _saveGeneralJournal.Session.CommitTransaction();

                                                                                        #region AccountingPeriodicLine

                                                                                        if (_locAccountMapLine.Account.Code != null)
                                                                                        {

                                                                                            //Cari Account Periodic Line
                                                                                            AccountingPeriodicLine _locAPL = _currSession.FindObject<AccountingPeriodicLine>
                                                                                                                            (new GroupOperator(GroupOperatorType.And,
                                                                                                                            new BinaryOperator("Company", _locOrderCollectionLine.Company),
                                                                                                                            new BinaryOperator("Workplace", _locOrderCollectionLine.Workplace),
                                                                                                                            new BinaryOperator("AccountNo", _locAccountMapLine.Account),
                                                                                                                            new BinaryOperator("Month", _locOrderCollectionLine.JournalMonth),
                                                                                                                            new BinaryOperator("Year", _locOrderCollectionLine.JournalYear)));
                                                                                            if (_locAPL != null)
                                                                                            {
                                                                                                if (_locAPL.AccountNo != null)
                                                                                                {
                                                                                                    if (_locAPL.AccountNo.BalanceType == BalanceType.Change)
                                                                                                    {
                                                                                                        if (_locAPL.AccountNo.AccountCharge == AccountCharge.Debit)
                                                                                                        {
                                                                                                            if (_locTotalAmountDebit > 0)
                                                                                                            {
                                                                                                                _locTotalBalance = _locAPL.Balance + _locTotalAmountDebit;
                                                                                                            }
                                                                                                            if (_locTotalAmountCredit > 0)
                                                                                                            {
                                                                                                                _locTotalBalance = _locAPL.Balance - _locTotalAmountCredit;
                                                                                                            }
                                                                                                        }
                                                                                                        if (_locAPL.AccountNo.AccountCharge == AccountCharge.Credit)
                                                                                                        {
                                                                                                            if (_locTotalAmountDebit > 0)
                                                                                                            {
                                                                                                                _locTotalBalance = _locAPL.Balance - _locTotalAmountDebit;
                                                                                                            }
                                                                                                            if (_locTotalAmountCredit > 0)
                                                                                                            {
                                                                                                                _locTotalBalance = _locAPL.Balance + _locTotalAmountCredit;
                                                                                                            }
                                                                                                        }
                                                                                                    }

                                                                                                    _locAPL.Balance = _locTotalBalance;
                                                                                                    _locAPL.Debit = _locAPL.Debit + _locTotalAmountDebit;
                                                                                                    _locAPL.Credit = _locAPL.Credit + _locTotalAmountCredit;
                                                                                                    _locAPL.Save();
                                                                                                    _locAPL.Session.CommitTransaction();

                                                                                                }
                                                                                            }
                                                                                        }
                                                                                        #endregion COA
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                        #endregion AmountCN
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            } 
                                        }
                                        #endregion JournalMapBusinessPartnerAccountGroup

                                        #region JournalMapCompanyAccountGroup
                                        if (_locOrderCollectionLine.Company != null)
                                        {
                                            _locTotalAmountCollC = _locOrderCollectionLine.AmountColl;
                                            _locTotalAmountCollCDN = _locOrderCollectionLine.AmountDN;
                                            _locTotalAmountCollCCN = _locOrderCollectionLine.AmountCN;

                                            CompanyAccount _locCompanyAccount = _currSession.FindObject<CompanyAccount>(new GroupOperator(GroupOperatorType.And,
                                                                    new BinaryOperator("Company", _locOrderCollectionLine.Company),
                                                                    new BinaryOperator("Workplace", _locOrderCollectionLine.Workplace),
                                                                    new BinaryOperator("Active", true)));

                                            if (_locCompanyAccount != null)
                                            {
                                                if (_locCompanyAccount.CompanyAccountGroup != null)
                                                {
                                                    _locCompanyAccountGroup = _locCompanyAccount.CompanyAccountGroup;

                                                    XPCollection<JournalMap> _locJournalMapItems = new XPCollection<JournalMap>
                                                                                   (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                    new BinaryOperator("Company", _locOrderCollectionLine.Company),
                                                                                    new BinaryOperator("Workplace", _locOrderCollectionLine.Workplace),
                                                                                    new BinaryOperator("CompanyAccountGroup", _locCompanyAccountGroup),
                                                                                    new BinaryOperator("Active", true)));

                                                    if (_locJournalMapItems != null && _locJournalMapItems.Count() > 0)
                                                    {
                                                        foreach (JournalMap _locJournalMapItem in _locJournalMapItems)
                                                        {
                                                            XPCollection<JournalMapLine> _locJournalMapLines = new XPCollection<JournalMapLine>
                                                                                                               (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                                new BinaryOperator("Company", _locOrderCollectionLine.Company),
                                                                                                                new BinaryOperator("Workplace", _locOrderCollectionLine.Workplace),
                                                                                                                new BinaryOperator("JournalMap", _locJournalMapItem),
                                                                                                                new BinaryOperator("Active", true)));

                                                            if (_locJournalMapLines != null && _locJournalMapLines.Count() > 0)
                                                            {
                                                                foreach (JournalMapLine _locJournalMapLine in _locJournalMapLines)
                                                                {
                                                                    if (_locJournalMapLine.AccountMap != null)
                                                                    {
                                                                        #region NormalAmount
                                                                        if (_locJournalMapLine.AccountMap.PostingType == PostingType.Sales && _locJournalMapLine.AccountMap.OrderType == OrderType.Item
                                                                            && _locJournalMapLine.AccountMap.PostingMethod == PostingMethod.InvoiceAR && _locJournalMapLine.AccountMap.PostingMethodType == PostingMethodType.Normal)
                                                                        {
                                                                            XPCollection<AccountMapLine> _locAccountMapLines = new XPCollection<AccountMapLine>
                                                                                                                           (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                                            new BinaryOperator("Company", _locOrderCollectionLine.Company),
                                                                                                                            new BinaryOperator("Workplace", _locOrderCollectionLine.Workplace),
                                                                                                                            new BinaryOperator("AccountMap", _locJournalMapLine.AccountMap),
                                                                                                                            new BinaryOperator("Active", true)));

                                                                            if (_locAccountMapLines != null && _locAccountMapLines.Count() > 0)
                                                                            {
                                                                                double _locTotalAmountDebit = 0;
                                                                                double _locTotalAmountCredit = 0;
                                                                                double _locTotalBalance = 0;

                                                                                foreach (AccountMapLine _locAccountMapLine in _locAccountMapLines)
                                                                                {
                                                                                    if (_locAccountMapLine.AccountCharge == AccountCharge.Debit)
                                                                                    {
                                                                                        _locTotalAmountDebit = _locTotalAmountCollC;
                                                                                    }
                                                                                    if (_locAccountMapLine.AccountCharge == AccountCharge.Credit)
                                                                                    {
                                                                                        _locTotalAmountCredit = _locTotalAmountCollC;
                                                                                    }

                                                                                    GeneralJournal _saveGeneralJournal = new GeneralJournal(_currSession)
                                                                                    {
                                                                                        Company = _locOrderCollectionLine.Company,
                                                                                        Workplace = _locOrderCollectionLine.Workplace,
                                                                                        PostingDate = now,
                                                                                        PostingType = PostingType.Sales,
                                                                                        OrderType = OrderType.Item,
                                                                                        PostingMethod = PostingMethod.InvoiceAR,
                                                                                        PostingMethodType = PostingMethodType.Normal,
                                                                                        Account = _locAccountMapLine.Account,
                                                                                        Debit = _locTotalAmountDebit,
                                                                                        Credit = _locTotalAmountCredit,
                                                                                        JournalMonth = _locOrderCollectionLine.JournalMonth,
                                                                                        JournalYear = _locOrderCollectionLine.JournalYear,
                                                                                        OrderCollection = _locOrderCollectionXPO,
                                                                                    };

                                                                                    _saveGeneralJournal.Save();
                                                                                    _saveGeneralJournal.Session.CommitTransaction();

                                                                                    #region AccountingPeriodicLine

                                                                                    if (_locAccountMapLine.Account.Code != null)
                                                                                    {

                                                                                        //Cari Account Periodic Line
                                                                                        AccountingPeriodicLine _locAPL = _currSession.FindObject<AccountingPeriodicLine>
                                                                                                                        (new GroupOperator(GroupOperatorType.And,
                                                                                                                        new BinaryOperator("Company", _locOrderCollectionLine.Company),
                                                                                                                        new BinaryOperator("Workplace", _locOrderCollectionLine.Workplace),
                                                                                                                        new BinaryOperator("AccountNo", _locAccountMapLine.Account),
                                                                                                                        new BinaryOperator("Month", _locOrderCollectionLine.JournalMonth),
                                                                                                                        new BinaryOperator("Year", _locOrderCollectionLine.JournalYear)));
                                                                                        if (_locAPL != null)
                                                                                        {
                                                                                            if (_locAPL.AccountNo != null)
                                                                                            {
                                                                                                if (_locAPL.AccountNo.BalanceType == BalanceType.Change)
                                                                                                {
                                                                                                    if (_locAPL.AccountNo.AccountCharge == AccountCharge.Debit)
                                                                                                    {
                                                                                                        if (_locTotalAmountDebit > 0)
                                                                                                        {
                                                                                                            _locTotalBalance = _locAPL.Balance + _locTotalAmountDebit;
                                                                                                        }
                                                                                                        if (_locTotalAmountCredit > 0)
                                                                                                        {
                                                                                                            _locTotalBalance = _locAPL.Balance - _locTotalAmountCredit;
                                                                                                        }
                                                                                                    }
                                                                                                    if (_locAPL.AccountNo.AccountCharge == AccountCharge.Credit)
                                                                                                    {
                                                                                                        if (_locTotalAmountDebit > 0)
                                                                                                        {
                                                                                                            _locTotalBalance = _locAPL.Balance - _locTotalAmountDebit;
                                                                                                        }
                                                                                                        if (_locTotalAmountCredit > 0)
                                                                                                        {
                                                                                                            _locTotalBalance = _locAPL.Balance + _locTotalAmountCredit;
                                                                                                        }
                                                                                                    }
                                                                                                }

                                                                                                _locAPL.Balance = _locTotalBalance;
                                                                                                _locAPL.Debit = _locAPL.Debit + _locTotalAmountDebit;
                                                                                                _locAPL.Credit = _locAPL.Credit + _locTotalAmountCredit;
                                                                                                _locAPL.Save();
                                                                                                _locAPL.Session.CommitTransaction();

                                                                                            }
                                                                                        }
                                                                                    }
                                                                                    #endregion COA
                                                                                }
                                                                            }
                                                                        }
                                                                        #endregion NormalAmount
                                                                        #region AmountDN
                                                                        if(_locTotalAmountCollCDN > 0)
                                                                        {
                                                                            if (_locJournalMapLine.AccountMap.PostingType == PostingType.Sales && _locJournalMapLine.AccountMap.OrderType == OrderType.Item
                                                                            && _locJournalMapLine.AccountMap.PostingMethod == PostingMethod.InvoiceAR && _locJournalMapLine.AccountMap.PostingMethodType == PostingMethodType.AmountDN)
                                                                            {
                                                                                XPCollection<AccountMapLine> _locAccountMapLines = new XPCollection<AccountMapLine>
                                                                                                                               (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                                                new BinaryOperator("Company", _locOrderCollectionLine.Company),
                                                                                                                                new BinaryOperator("Workplace", _locOrderCollectionLine.Workplace),
                                                                                                                                new BinaryOperator("AccountMap", _locJournalMapLine.AccountMap),
                                                                                                                                new BinaryOperator("Active", true)));

                                                                                if (_locAccountMapLines != null && _locAccountMapLines.Count() > 0)
                                                                                {
                                                                                    double _locTotalAmountDebit = 0;
                                                                                    double _locTotalAmountCredit = 0;
                                                                                    double _locTotalBalance = 0;

                                                                                    foreach (AccountMapLine _locAccountMapLine in _locAccountMapLines)
                                                                                    {
                                                                                        if (_locAccountMapLine.AccountCharge == AccountCharge.Debit)
                                                                                        {
                                                                                            _locTotalAmountDebit = _locTotalAmountCollCDN;
                                                                                        }
                                                                                        if (_locAccountMapLine.AccountCharge == AccountCharge.Credit)
                                                                                        {
                                                                                            _locTotalAmountCredit = _locTotalAmountCollCDN;
                                                                                        }

                                                                                        GeneralJournal _saveGeneralJournal = new GeneralJournal(_currSession)
                                                                                        {
                                                                                            Company = _locOrderCollectionLine.Company,
                                                                                            Workplace = _locOrderCollectionLine.Workplace,
                                                                                            PostingDate = now,
                                                                                            PostingType = PostingType.Sales,
                                                                                            OrderType = OrderType.Item,
                                                                                            PostingMethod = PostingMethod.InvoiceAR,
                                                                                            PostingMethodType = PostingMethodType.AmountDN,
                                                                                            Account = _locAccountMapLine.Account,
                                                                                            Debit = _locTotalAmountDebit,
                                                                                            Credit = _locTotalAmountCredit,
                                                                                            JournalMonth = _locOrderCollectionLine.JournalMonth,
                                                                                            JournalYear = _locOrderCollectionLine.JournalYear,
                                                                                            OrderCollection = _locOrderCollectionXPO,
                                                                                        };

                                                                                        _saveGeneralJournal.Save();
                                                                                        _saveGeneralJournal.Session.CommitTransaction();

                                                                                        #region AccountingPeriodicLine

                                                                                        if (_locAccountMapLine.Account.Code != null)
                                                                                        {

                                                                                            //Cari Account Periodic Line
                                                                                            AccountingPeriodicLine _locAPL = _currSession.FindObject<AccountingPeriodicLine>
                                                                                                                            (new GroupOperator(GroupOperatorType.And,
                                                                                                                            new BinaryOperator("Company", _locOrderCollectionLine.Company),
                                                                                                                            new BinaryOperator("Workplace", _locOrderCollectionLine.Workplace),
                                                                                                                            new BinaryOperator("AccountNo", _locAccountMapLine.Account),
                                                                                                                            new BinaryOperator("Month", _locOrderCollectionLine.JournalMonth),
                                                                                                                            new BinaryOperator("Year", _locOrderCollectionLine.JournalYear)));
                                                                                            if (_locAPL != null)
                                                                                            {
                                                                                                if (_locAPL.AccountNo != null)
                                                                                                {
                                                                                                    if (_locAPL.AccountNo.BalanceType == BalanceType.Change)
                                                                                                    {
                                                                                                        if (_locAPL.AccountNo.AccountCharge == AccountCharge.Debit)
                                                                                                        {
                                                                                                            if (_locTotalAmountDebit > 0)
                                                                                                            {
                                                                                                                _locTotalBalance = _locAPL.Balance + _locTotalAmountDebit;
                                                                                                            }
                                                                                                            if (_locTotalAmountCredit > 0)
                                                                                                            {
                                                                                                                _locTotalBalance = _locAPL.Balance - _locTotalAmountCredit;
                                                                                                            }
                                                                                                        }
                                                                                                        if (_locAPL.AccountNo.AccountCharge == AccountCharge.Credit)
                                                                                                        {
                                                                                                            if (_locTotalAmountDebit > 0)
                                                                                                            {
                                                                                                                _locTotalBalance = _locAPL.Balance - _locTotalAmountDebit;
                                                                                                            }
                                                                                                            if (_locTotalAmountCredit > 0)
                                                                                                            {
                                                                                                                _locTotalBalance = _locAPL.Balance + _locTotalAmountCredit;
                                                                                                            }
                                                                                                        }
                                                                                                    }

                                                                                                    _locAPL.Balance = _locTotalBalance;
                                                                                                    _locAPL.Debit = _locAPL.Debit + _locTotalAmountDebit;
                                                                                                    _locAPL.Credit = _locAPL.Credit + _locTotalAmountCredit;
                                                                                                    _locAPL.Save();
                                                                                                    _locAPL.Session.CommitTransaction();

                                                                                                }
                                                                                            }
                                                                                        }
                                                                                        #endregion COA
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                        
                                                                        #endregion AmountDN
                                                                        #region AmountCN
                                                                        if(_locTotalAmountCollCCN > 0)
                                                                        {
                                                                            if (_locJournalMapLine.AccountMap.PostingType == PostingType.Sales && _locJournalMapLine.AccountMap.OrderType == OrderType.Item
                                                                            && _locJournalMapLine.AccountMap.PostingMethod == PostingMethod.InvoiceAR && _locJournalMapLine.AccountMap.PostingMethodType == PostingMethodType.AmountCN)
                                                                            {
                                                                                XPCollection<AccountMapLine> _locAccountMapLines = new XPCollection<AccountMapLine>
                                                                                                                               (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                                                new BinaryOperator("Company", _locOrderCollectionLine.Company),
                                                                                                                                new BinaryOperator("Workplace", _locOrderCollectionLine.Workplace),
                                                                                                                                new BinaryOperator("AccountMap", _locJournalMapLine.AccountMap),
                                                                                                                                new BinaryOperator("Active", true)));

                                                                                if (_locAccountMapLines != null && _locAccountMapLines.Count() > 0)
                                                                                {
                                                                                    double _locTotalAmountDebit = 0;
                                                                                    double _locTotalAmountCredit = 0;
                                                                                    double _locTotalBalance = 0;

                                                                                    foreach (AccountMapLine _locAccountMapLine in _locAccountMapLines)
                                                                                    {
                                                                                        if (_locAccountMapLine.AccountCharge == AccountCharge.Debit)
                                                                                        {
                                                                                            _locTotalAmountDebit = _locTotalAmountCollCCN;
                                                                                        }
                                                                                        if (_locAccountMapLine.AccountCharge == AccountCharge.Credit)
                                                                                        {
                                                                                            _locTotalAmountCredit = _locTotalAmountCollCCN;
                                                                                        }

                                                                                        GeneralJournal _saveGeneralJournal = new GeneralJournal(_currSession)
                                                                                        {
                                                                                            Company = _locOrderCollectionLine.Company,
                                                                                            Workplace = _locOrderCollectionLine.Workplace,
                                                                                            PostingDate = now,
                                                                                            PostingType = PostingType.Sales,
                                                                                            OrderType = OrderType.Item,
                                                                                            PostingMethod = PostingMethod.InvoiceAR,
                                                                                            PostingMethodType = PostingMethodType.AmountCN,
                                                                                            Account = _locAccountMapLine.Account,
                                                                                            Debit = _locTotalAmountDebit,
                                                                                            Credit = _locTotalAmountCredit,
                                                                                            JournalMonth = _locOrderCollectionLine.JournalMonth,
                                                                                            JournalYear = _locOrderCollectionLine.JournalYear,
                                                                                            OrderCollection = _locOrderCollectionXPO,
                                                                                        };

                                                                                        _saveGeneralJournal.Save();
                                                                                        _saveGeneralJournal.Session.CommitTransaction();

                                                                                        #region AccountingPeriodicLine

                                                                                        if (_locAccountMapLine.Account.Code != null)
                                                                                        {

                                                                                            //Cari Account Periodic Line
                                                                                            AccountingPeriodicLine _locAPL = _currSession.FindObject<AccountingPeriodicLine>
                                                                                                                            (new GroupOperator(GroupOperatorType.And,
                                                                                                                            new BinaryOperator("Company", _locOrderCollectionLine.Company),
                                                                                                                            new BinaryOperator("Workplace", _locOrderCollectionLine.Workplace),
                                                                                                                            new BinaryOperator("AccountNo", _locAccountMapLine.Account),
                                                                                                                            new BinaryOperator("Month", _locOrderCollectionLine.JournalMonth),
                                                                                                                            new BinaryOperator("Year", _locOrderCollectionLine.JournalYear)));
                                                                                            if (_locAPL != null)
                                                                                            {
                                                                                                if (_locAPL.AccountNo != null)
                                                                                                {
                                                                                                    if (_locAPL.AccountNo.BalanceType == BalanceType.Change)
                                                                                                    {
                                                                                                        if (_locAPL.AccountNo.AccountCharge == AccountCharge.Debit)
                                                                                                        {
                                                                                                            if (_locTotalAmountDebit > 0)
                                                                                                            {
                                                                                                                _locTotalBalance = _locAPL.Balance + _locTotalAmountDebit;
                                                                                                            }
                                                                                                            if (_locTotalAmountCredit > 0)
                                                                                                            {
                                                                                                                _locTotalBalance = _locAPL.Balance - _locTotalAmountCredit;
                                                                                                            }
                                                                                                        }
                                                                                                        if (_locAPL.AccountNo.AccountCharge == AccountCharge.Credit)
                                                                                                        {
                                                                                                            if (_locTotalAmountDebit > 0)
                                                                                                            {
                                                                                                                _locTotalBalance = _locAPL.Balance - _locTotalAmountDebit;
                                                                                                            }
                                                                                                            if (_locTotalAmountCredit > 0)
                                                                                                            {
                                                                                                                _locTotalBalance = _locAPL.Balance + _locTotalAmountCredit;
                                                                                                            }
                                                                                                        }
                                                                                                    }

                                                                                                    _locAPL.Balance = _locTotalBalance;
                                                                                                    _locAPL.Debit = _locAPL.Debit + _locTotalAmountDebit;
                                                                                                    _locAPL.Credit = _locAPL.Credit + _locTotalAmountCredit;
                                                                                                    _locAPL.Save();
                                                                                                    _locAPL.Session.CommitTransaction();

                                                                                                }
                                                                                            }
                                                                                        }
                                                                                        #endregion COA
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                        
                                                                        #endregion AmountCN
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        #endregion JournalMapCompanyAccountGroup

                                        #region DiscountOnHeader
                                        if(_locOrderCollectionLine.SalesInvoice.DiscountRule != null)
                                        {
                                            _locTotalHeaderDiscount = _locOrderCollectionLine.SalesInvoice.AmountDisc;
                                            #region DiscountAccountGroup
                                            if (_locOrderCollectionLine.SalesInvoice.DiscountRule.DiscountAccountGroup != null)
                                            {
                                                _locHeaderDiscountAccountGroup = _locOrderCollectionLine.SalesInvoice.DiscountRule.DiscountAccountGroup;

                                                XPCollection<JournalMap> _locJournalMapItems = new XPCollection<JournalMap>
                                                                                               (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                new BinaryOperator("Company", _locOrderCollectionLine.Company),
                                                                                                new BinaryOperator("Workplace", _locOrderCollectionLine.Workplace),
                                                                                                new BinaryOperator("DiscountAccountGroup", _locHeaderDiscountAccountGroup),
                                                                                                new BinaryOperator("Active", true)));

                                                if (_locJournalMapItems != null && _locJournalMapItems.Count() > 0)
                                                {
                                                    foreach (JournalMap _locJournalMapItem in _locJournalMapItems)
                                                    {
                                                        XPCollection<JournalMapLine> _locJournalMapLines = new XPCollection<JournalMapLine>
                                                                                                           (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                            new BinaryOperator("Company", _locOrderCollectionLine.Company),
                                                                                                            new BinaryOperator("Workplace", _locOrderCollectionLine.Workplace),
                                                                                                            new BinaryOperator("JournalMap", _locJournalMapItem),
                                                                                                            new BinaryOperator("Active", true)));

                                                        if (_locJournalMapLines != null && _locJournalMapLines.Count() > 0)
                                                        {
                                                            foreach (JournalMapLine _locJournalMapLine in _locJournalMapLines)
                                                            {
                                                                if (_locJournalMapLine.AccountMap != null)
                                                                {
                                                                    if (_locJournalMapLine.AccountMap.PostingType == PostingType.Sales && _locJournalMapLine.AccountMap.OrderType == OrderType.Item
                                                                        && _locJournalMapLine.AccountMap.PostingMethod == PostingMethod.InvoiceAR && _locJournalMapLine.AccountMap.PostingMethodType == PostingMethodType.Discount)
                                                                    {
                                                                        XPCollection<AccountMapLine> _locAccountMapLines = new XPCollection<AccountMapLine>
                                                                                                                       (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                                        new BinaryOperator("Company", _locOrderCollectionLine.Company),
                                                                                                                        new BinaryOperator("Workplace", _locOrderCollectionLine.Workplace),
                                                                                                                        new BinaryOperator("AccountMap", _locJournalMapLine.AccountMap),
                                                                                                                        new BinaryOperator("Active", true)));

                                                                        if (_locAccountMapLines != null && _locAccountMapLines.Count() > 0)
                                                                        {
                                                                            double _locTotalAmountDebit = 0;
                                                                            double _locTotalAmountCredit = 0;
                                                                            double _locTotalBalance = 0;

                                                                            foreach (AccountMapLine _locAccountMapLine in _locAccountMapLines)
                                                                            {
                                                                                if (_locAccountMapLine.AccountCharge == AccountCharge.Debit)
                                                                                {
                                                                                    _locTotalAmountDebit = _locTotalHeaderDiscount;
                                                                                }
                                                                                if (_locAccountMapLine.AccountCharge == AccountCharge.Credit)
                                                                                {
                                                                                    _locTotalAmountCredit = _locTotalHeaderDiscount;
                                                                                }

                                                                                GeneralJournal _saveGeneralJournal = new GeneralJournal(_currSession)
                                                                                {
                                                                                    Company = _locOrderCollectionLine.Company,
                                                                                    Workplace = _locOrderCollectionLine.Workplace,
                                                                                    PostingDate = now,
                                                                                    PostingType = PostingType.Sales,
                                                                                    PostingMethod = PostingMethod.InvoiceAR,
                                                                                    PostingMethodType = PostingMethodType.Discount,
                                                                                    Account = _locAccountMapLine.Account,
                                                                                    Debit = _locTotalAmountDebit,
                                                                                    Credit = _locTotalAmountCredit,
                                                                                    JournalMonth = _locOrderCollectionLine.JournalMonth,
                                                                                    JournalYear = _locOrderCollectionLine.JournalYear,
                                                                                    OrderCollection = _locOrderCollectionXPO,
                                                                                };

                                                                                _saveGeneralJournal.Save();
                                                                                _saveGeneralJournal.Session.CommitTransaction();

                                                                                #region AccountingPeriodicLine

                                                                                if (_locAccountMapLine.Account.Code != null)
                                                                                {

                                                                                    //Cari Account Periodic Line
                                                                                    AccountingPeriodicLine _locAPL = _currSession.FindObject<AccountingPeriodicLine>
                                                                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                                                                    new BinaryOperator("Company", _locOrderCollectionLine.Company),
                                                                                                                    new BinaryOperator("Workplace", _locOrderCollectionLine.Workplace),
                                                                                                                    new BinaryOperator("AccountNo", _locAccountMapLine.Account),
                                                                                                                    new BinaryOperator("Month", _locOrderCollectionLine.JournalMonth),
                                                                                                                    new BinaryOperator("Year", _locOrderCollectionLine.JournalYear)));
                                                                                    if (_locAPL != null)
                                                                                    {
                                                                                        if (_locAPL.AccountNo != null)
                                                                                        {
                                                                                            if (_locAPL.AccountNo.BalanceType == BalanceType.Change)
                                                                                            {
                                                                                                if (_locAPL.AccountNo.AccountCharge == AccountCharge.Debit)
                                                                                                {
                                                                                                    if (_locTotalAmountDebit > 0)
                                                                                                    {
                                                                                                        _locTotalBalance = _locAPL.Balance + _locTotalAmountDebit;
                                                                                                    }
                                                                                                    if (_locTotalAmountCredit > 0)
                                                                                                    {
                                                                                                        _locTotalBalance = _locAPL.Balance - _locTotalAmountCredit;
                                                                                                    }
                                                                                                }
                                                                                                if (_locAPL.AccountNo.AccountCharge == AccountCharge.Credit)
                                                                                                {
                                                                                                    if (_locTotalAmountDebit > 0)
                                                                                                    {
                                                                                                        _locTotalBalance = _locAPL.Balance - _locTotalAmountDebit;
                                                                                                    }
                                                                                                    if (_locTotalAmountCredit > 0)
                                                                                                    {
                                                                                                        _locTotalBalance = _locAPL.Balance + _locTotalAmountCredit;
                                                                                                    }
                                                                                                }
                                                                                            }

                                                                                            _locAPL.Balance = _locTotalBalance;
                                                                                            _locAPL.Debit = _locAPL.Debit + _locTotalAmountDebit;
                                                                                            _locAPL.Credit = _locAPL.Credit + _locTotalAmountCredit;
                                                                                            _locAPL.Save();
                                                                                            _locAPL.Session.CommitTransaction();

                                                                                        }
                                                                                    }
                                                                                }
                                                                                #endregion COA
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                            #endregion DiscountAccountGroup
                                            if(_locOrderCollectionLine.SalesInvoice.AccountingPeriodicLine != null)
                                            {
                                                if(_locOrderCollectionLine.SalesInvoice.AccountingPeriodicLine.BalHold < _locTotalDiscount)
                                                {
                                                    _locTotalDiscount = _locOrderCollectionLine.SalesInvoice.AccountingPeriodicLine.BalHold;
                                                }
                                                _locOrderCollectionLine.SalesInvoice.AccountingPeriodicLine.BalHold = _locOrderCollectionLine.SalesInvoice.AccountingPeriodicLine.BalHold - _locTotalDiscount;
                                                _locOrderCollectionLine.SalesInvoice.AccountingPeriodicLine.Save();
                                                _locOrderCollectionLine.SalesInvoice.AccountingPeriodicLine.Session.CommitTransaction();
                                            }
                                            _locTotalHeaderDiscount = 0;
                                        }
                                        #endregion DiscountOnHeader
                                    }
                                }
                                #endregion BasedOnSalesInvoice
                                #region BasedOnSalesInvoiceCmp
                                else
                                {
                                    if(_locOrderCollectionLine.PickingMonitoring.AmendDelivery != null && _locOrderCollectionLine.PickingMonitoring.SalesInvoiceCmp != null)
                                    {
                                        #region JournalMapByItems
                                        #region JournalMapByNormalItems
                                        XPCollection<SalesInvoiceLineCmp> _locSalesInvoiceLineCmps = new XPCollection<SalesInvoiceLineCmp>(_currSession, 
                                                                                                new GroupOperator(GroupOperatorType.And,
                                                                                                new BinaryOperator("SalesInvoiceCmp", _locOrderCollectionLine.PickingMonitoring.SalesInvoiceCmp)));
                                        if(_locSalesInvoiceLineCmps != null && _locSalesInvoiceLineCmps.Count() > 0)
                                        {
                                            foreach(SalesInvoiceLineCmp _locSalesInvoiceLineCmp in _locSalesInvoiceLineCmps)
                                            {
                                                if(_locSalesInvoiceLineCmp.Item != null)
                                                {
                                                    #region NormalJournal
                                                    if (_locSalesInvoiceLineCmp.PriceLine != null)
                                                    {
                                                        _locTotalUnitAmount = _locSalesInvoiceLineCmp.PriceLine.Amount1a * _locSalesInvoiceLineCmp.TQty;

                                                        ItemAccount _locItemAccount = _currSession.FindObject<ItemAccount>(new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("Company", _locOrderCollectionLine.Company),
                                                                new BinaryOperator("Workplace", _locOrderCollectionLine.Workplace),
                                                                new BinaryOperator("Item", _locSalesInvoiceLineCmp.Item),
                                                                new BinaryOperator("Active", true)));

                                                        if (_locItemAccount != null)
                                                        {
                                                            if (_locItemAccount.ItemAccountGroup != null)
                                                            {
                                                                _locItemAccountGroup = _locItemAccount.ItemAccountGroup;

                                                                XPCollection<JournalMap> _locJournalMapItems = new XPCollection<JournalMap>
                                                                                               (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                new BinaryOperator("Company", _locOrderCollectionLine.Company),
                                                                                                new BinaryOperator("Workplace", _locOrderCollectionLine.Workplace),
                                                                                                new BinaryOperator("ItemAccountGroup", _locItemAccountGroup),
                                                                                                new BinaryOperator("Active", true)));

                                                                if (_locJournalMapItems != null && _locJournalMapItems.Count() > 0)
                                                                {
                                                                    foreach (JournalMap _locJournalMapItem in _locJournalMapItems)
                                                                    {
                                                                        XPCollection<JournalMapLine> _locJournalMapLines = new XPCollection<JournalMapLine>
                                                                                                                           (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                                            new BinaryOperator("Company", _locOrderCollectionLine.Company),
                                                                                                                            new BinaryOperator("Workplace", _locOrderCollectionLine.Workplace),
                                                                                                                            new BinaryOperator("JournalMap", _locJournalMapItem),
                                                                                                                            new BinaryOperator("Active", true)));

                                                                        if (_locJournalMapLines != null && _locJournalMapLines.Count() > 0)
                                                                        {
                                                                            foreach (JournalMapLine _locJournalMapLine in _locJournalMapLines)
                                                                            {
                                                                                if (_locJournalMapLine.AccountMap != null)
                                                                                {
                                                                                    if (_locJournalMapLine.AccountMap.PostingType == PostingType.Sales && _locJournalMapLine.AccountMap.OrderType == OrderType.Item
                                                                                        && _locJournalMapLine.AccountMap.PostingMethod == PostingMethod.InvoiceAR && _locJournalMapLine.AccountMap.PostingMethodType == PostingMethodType.Normal)
                                                                                    {
                                                                                        XPCollection<AccountMapLine> _locAccountMapLines = new XPCollection<AccountMapLine>
                                                                                                                                       (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                                                        new BinaryOperator("Company", _locOrderCollectionLine.Company),
                                                                                                                                        new BinaryOperator("Workplace", _locOrderCollectionLine.Workplace),
                                                                                                                                        new BinaryOperator("AccountMap", _locJournalMapLine.AccountMap),
                                                                                                                                        new BinaryOperator("Active", true)));

                                                                                        if (_locAccountMapLines != null && _locAccountMapLines.Count() > 0)
                                                                                        {
                                                                                            double _locTotalAmountDebit = 0;
                                                                                            double _locTotalAmountCredit = 0;
                                                                                            double _locTotalBalance = 0;

                                                                                            foreach (AccountMapLine _locAccountMapLine in _locAccountMapLines)
                                                                                            {
                                                                                                if (_locAccountMapLine.AccountCharge == AccountCharge.Debit)
                                                                                                {
                                                                                                    _locTotalAmountDebit = _locTotalUnitAmount;
                                                                                                }
                                                                                                if (_locAccountMapLine.AccountCharge == AccountCharge.Credit)
                                                                                                {
                                                                                                    _locTotalAmountCredit = _locTotalUnitAmount;
                                                                                                }

                                                                                                GeneralJournal _saveGeneralJournal = new GeneralJournal(_currSession)
                                                                                                {
                                                                                                    Company = _locOrderCollectionLine.Company,
                                                                                                    Workplace = _locOrderCollectionLine.Workplace,
                                                                                                    PostingDate = now,
                                                                                                    PostingType = PostingType.Sales,
                                                                                                    OrderType = OrderType.Item,
                                                                                                    PostingMethod = PostingMethod.InvoiceAR,
                                                                                                    PostingMethodType = PostingMethodType.Normal,
                                                                                                    Account = _locAccountMapLine.Account,
                                                                                                    Debit = _locTotalAmountDebit,
                                                                                                    Credit = _locTotalAmountCredit,
                                                                                                    JournalMonth = _locOrderCollectionLine.JournalMonth,
                                                                                                    JournalYear = _locOrderCollectionLine.JournalYear,

                                                                                                    OrderCollection = _locOrderCollectionXPO,
                                                                                                };

                                                                                                _saveGeneralJournal.Save();
                                                                                                _saveGeneralJournal.Session.CommitTransaction();

                                                                                                #region AccountingPeriodicLine

                                                                                                if (_locAccountMapLine.Account.Code != null)
                                                                                                {

                                                                                                    //Cari Account Periodic Line
                                                                                                    AccountingPeriodicLine _locAPL = _currSession.FindObject<AccountingPeriodicLine>
                                                                                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                                                                                    new BinaryOperator("Company", _locOrderCollectionLine.Company),
                                                                                                                                    new BinaryOperator("Workplace", _locOrderCollectionLine.Workplace),
                                                                                                                                    new BinaryOperator("AccountNo", _locAccountMapLine.Account),
                                                                                                                                    new BinaryOperator("Month", _locOrderCollectionLine.JournalMonth),
                                                                                                                                    new BinaryOperator("Year", _locOrderCollectionLine.JournalYear)));
                                                                                                    if (_locAPL != null)
                                                                                                    {
                                                                                                        if (_locAPL.AccountNo != null)
                                                                                                        {
                                                                                                            if (_locAPL.AccountNo.BalanceType == BalanceType.Change)
                                                                                                            {
                                                                                                                if (_locAPL.AccountNo.AccountCharge == AccountCharge.Debit)
                                                                                                                {
                                                                                                                    if (_locTotalAmountDebit > 0)
                                                                                                                    {
                                                                                                                        _locTotalBalance = _locAPL.Balance + _locTotalAmountDebit;
                                                                                                                    }
                                                                                                                    if (_locTotalAmountCredit > 0)
                                                                                                                    {
                                                                                                                        _locTotalBalance = _locAPL.Balance - _locTotalAmountCredit;
                                                                                                                    }
                                                                                                                }
                                                                                                                if (_locAPL.AccountNo.AccountCharge == AccountCharge.Credit)
                                                                                                                {
                                                                                                                    if (_locTotalAmountDebit > 0)
                                                                                                                    {
                                                                                                                        _locTotalBalance = _locAPL.Balance - _locTotalAmountDebit;
                                                                                                                    }
                                                                                                                    if (_locTotalAmountCredit > 0)
                                                                                                                    {
                                                                                                                        _locTotalBalance = _locAPL.Balance + _locTotalAmountCredit;
                                                                                                                    }
                                                                                                                }
                                                                                                            }

                                                                                                            _locAPL.Balance = _locTotalBalance;
                                                                                                            _locAPL.Debit = _locAPL.Debit + _locTotalAmountDebit;
                                                                                                            _locAPL.Credit = _locAPL.Credit + _locTotalAmountCredit;
                                                                                                            _locAPL.Save();
                                                                                                            _locAPL.Session.CommitTransaction();

                                                                                                        }
                                                                                                    }
                                                                                                }
                                                                                                #endregion COA
                                                                                            }
                                                                                        }
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                    #endregion NormalJournal
                                                    #region TaxJournal
                                                    if (_locSalesInvoiceLineCmp.Tax != null)
                                                    {
                                                        _locTotalTaxAmount = _locSalesInvoiceLineCmp.TxAmount;


                                                        TaxAccount _locTaxAccount = _currSession.FindObject<TaxAccount>(new GroupOperator(GroupOperatorType.And,
                                                                            new BinaryOperator("Company", _locOrderCollectionLine.Company),
                                                                            new BinaryOperator("Workplace", _locOrderCollectionLine.Workplace),
                                                                            new BinaryOperator("Tax", _locSalesInvoiceLineCmp.Tax),
                                                                            new BinaryOperator("Active", true)));

                                                        if (_locTaxAccount != null)
                                                        {
                                                            if (_locTaxAccount.TaxAccountGroup != null)
                                                            {
                                                                _locTaxAccountGroup = _locTaxAccount.TaxAccountGroup;

                                                                XPCollection<JournalMap> _locJournalMapItems = new XPCollection<JournalMap>
                                                                                               (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                new BinaryOperator("Company", _locOrderCollectionLine.Company),
                                                                                                new BinaryOperator("Workplace", _locOrderCollectionLine.Workplace),
                                                                                                new BinaryOperator("TaxAccountGroup", _locTaxAccountGroup),
                                                                                                new BinaryOperator("Active", true)));

                                                                if (_locJournalMapItems != null && _locJournalMapItems.Count() > 0)
                                                                {
                                                                    foreach (JournalMap _locJournalMapItem in _locJournalMapItems)
                                                                    {
                                                                        XPCollection<JournalMapLine> _locJournalMapLines = new XPCollection<JournalMapLine>
                                                                                                                           (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                                            new BinaryOperator("Company", _locOrderCollectionLine.Company),
                                                                                                                            new BinaryOperator("Workplace", _locOrderCollectionLine.Workplace),
                                                                                                                            new BinaryOperator("JournalMap", _locJournalMapItem),
                                                                                                                            new BinaryOperator("Active", true)));

                                                                        if (_locJournalMapLines != null && _locJournalMapLines.Count() > 0)
                                                                        {
                                                                            foreach (JournalMapLine _locJournalMapLine in _locJournalMapLines)
                                                                            {
                                                                                if (_locJournalMapLine.AccountMap != null)
                                                                                {
                                                                                    if (_locJournalMapLine.AccountMap.PostingType == PostingType.Sales && _locJournalMapLine.AccountMap.OrderType == OrderType.Item
                                                                                        && _locJournalMapLine.AccountMap.PostingMethod == PostingMethod.InvoiceAR && _locJournalMapLine.AccountMap.PostingMethodType == PostingMethodType.Tax)
                                                                                    {
                                                                                        XPCollection<AccountMapLine> _locAccountMapLines = new XPCollection<AccountMapLine>
                                                                                                                                       (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                                                        new BinaryOperator("Company", _locOrderCollectionLine.Company),
                                                                                                                                        new BinaryOperator("Workplace", _locOrderCollectionLine.Workplace),
                                                                                                                                        new BinaryOperator("AccountMap", _locJournalMapLine.AccountMap),
                                                                                                                                        new BinaryOperator("Active", true)));

                                                                                        if (_locAccountMapLines != null && _locAccountMapLines.Count() > 0)
                                                                                        {
                                                                                            double _locTotalAmountDebit = 0;
                                                                                            double _locTotalAmountCredit = 0;
                                                                                            double _locTotalBalance = 0;

                                                                                            foreach (AccountMapLine _locAccountMapLine in _locAccountMapLines)
                                                                                            {
                                                                                                if (_locAccountMapLine.AccountCharge == AccountCharge.Debit)
                                                                                                {
                                                                                                    _locTotalAmountDebit = _locTotalTaxAmount;
                                                                                                }
                                                                                                if (_locAccountMapLine.AccountCharge == AccountCharge.Credit)
                                                                                                {
                                                                                                    _locTotalAmountCredit = _locTotalTaxAmount;
                                                                                                }

                                                                                                GeneralJournal _saveGeneralJournal = new GeneralJournal(_currSession)
                                                                                                {
                                                                                                    Company = _locOrderCollectionLine.Company,
                                                                                                    Workplace = _locOrderCollectionLine.Workplace,
                                                                                                    PostingDate = now,
                                                                                                    PostingType = PostingType.Sales,
                                                                                                    OrderType = OrderType.Item,
                                                                                                    PostingMethod = PostingMethod.InvoiceAR,
                                                                                                    PostingMethodType = PostingMethodType.Tax,
                                                                                                    Account = _locAccountMapLine.Account,
                                                                                                    Debit = _locTotalAmountDebit,
                                                                                                    Credit = _locTotalAmountCredit,
                                                                                                    JournalMonth = _locOrderCollectionLine.JournalMonth,
                                                                                                    JournalYear = _locOrderCollectionLine.JournalYear,

                                                                                                    OrderCollection = _locOrderCollectionXPO,
                                                                                                };

                                                                                                _saveGeneralJournal.Save();
                                                                                                _saveGeneralJournal.Session.CommitTransaction();

                                                                                                #region AccountingPeriodicLine

                                                                                                if (_locAccountMapLine.Account.Code != null)
                                                                                                {

                                                                                                    //Cari Account Periodic Line
                                                                                                    AccountingPeriodicLine _locAPL = _currSession.FindObject<AccountingPeriodicLine>
                                                                                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                                                                                    new BinaryOperator("Company", _locOrderCollectionLine.Company),
                                                                                                                                    new BinaryOperator("Workplace", _locOrderCollectionLine.Workplace),
                                                                                                                                    new BinaryOperator("AccountNo", _locAccountMapLine.Account),
                                                                                                                                    new BinaryOperator("Month", _locOrderCollectionLine.JournalMonth),
                                                                                                                                    new BinaryOperator("Year", _locOrderCollectionLine.JournalYear)));
                                                                                                    if (_locAPL != null)
                                                                                                    {
                                                                                                        if (_locAPL.AccountNo != null)
                                                                                                        {
                                                                                                            if (_locAPL.AccountNo.BalanceType == BalanceType.Change)
                                                                                                            {
                                                                                                                if (_locAPL.AccountNo.AccountCharge == AccountCharge.Debit)
                                                                                                                {
                                                                                                                    if (_locTotalAmountDebit > 0)
                                                                                                                    {
                                                                                                                        _locTotalBalance = _locAPL.Balance + _locTotalAmountDebit;
                                                                                                                    }
                                                                                                                    if (_locTotalAmountCredit > 0)
                                                                                                                    {
                                                                                                                        _locTotalBalance = _locAPL.Balance - _locTotalAmountCredit;
                                                                                                                    }
                                                                                                                }
                                                                                                                if (_locAPL.AccountNo.AccountCharge == AccountCharge.Credit)
                                                                                                                {
                                                                                                                    if (_locTotalAmountDebit > 0)
                                                                                                                    {
                                                                                                                        _locTotalBalance = _locAPL.Balance - _locTotalAmountDebit;
                                                                                                                    }
                                                                                                                    if (_locTotalAmountCredit > 0)
                                                                                                                    {
                                                                                                                        _locTotalBalance = _locAPL.Balance + _locTotalAmountCredit;
                                                                                                                    }
                                                                                                                }
                                                                                                            }

                                                                                                            _locAPL.Balance = _locTotalBalance;
                                                                                                            _locAPL.Debit = _locAPL.Debit + _locTotalAmountDebit;
                                                                                                            _locAPL.Credit = _locAPL.Credit + _locTotalAmountCredit;
                                                                                                            _locAPL.Save();
                                                                                                            _locAPL.Session.CommitTransaction();

                                                                                                        }
                                                                                                    }
                                                                                                }
                                                                                                #endregion COA
                                                                                            }
                                                                                        }
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                    #endregion TaxJournal
                                                    #region DiscountJournal
                                                    if (_locSalesInvoiceLineCmp.DiscountRule != null)
                                                    {

                                                        _locTotalDiscount = _locSalesInvoiceLineCmp.DiscAmount;

                                                        #region DiscountAccountGroup
                                                        if (_locSalesInvoiceLineCmp.DiscountRule.DiscountAccountGroup != null)
                                                        {
                                                            _locDiscountAccountGroup = _locSalesInvoiceLineCmp.DiscountRule.DiscountAccountGroup;

                                                            XPCollection<JournalMap> _locJournalMapItems = new XPCollection<JournalMap>
                                                                                               (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                new BinaryOperator("Company", _locOrderCollectionLine.Company),
                                                                                                new BinaryOperator("Workplace", _locOrderCollectionLine.Workplace),
                                                                                                new BinaryOperator("DiscountAccountGroup", _locDiscountAccountGroup),
                                                                                                new BinaryOperator("Active", true)));

                                                            if (_locJournalMapItems != null && _locJournalMapItems.Count() > 0)
                                                            {
                                                                foreach (JournalMap _locJournalMapItem in _locJournalMapItems)
                                                                {
                                                                    XPCollection<JournalMapLine> _locJournalMapLines = new XPCollection<JournalMapLine>
                                                                                                                       (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                                        new BinaryOperator("Company", _locOrderCollectionLine.Company),
                                                                                                                        new BinaryOperator("Workplace", _locOrderCollectionLine.Workplace),
                                                                                                                        new BinaryOperator("JournalMap", _locJournalMapItem),
                                                                                                                        new BinaryOperator("Active", true)));

                                                                    if (_locJournalMapLines != null && _locJournalMapLines.Count() > 0)
                                                                    {
                                                                        foreach (JournalMapLine _locJournalMapLine in _locJournalMapLines)
                                                                        {
                                                                            if (_locJournalMapLine.AccountMap != null)
                                                                            {
                                                                                if (_locJournalMapLine.AccountMap.PostingType == PostingType.Sales && _locJournalMapLine.AccountMap.OrderType == OrderType.Item
                                                                                    && _locJournalMapLine.AccountMap.PostingMethod == PostingMethod.InvoiceAR && _locJournalMapLine.AccountMap.PostingMethodType == PostingMethodType.Discount)
                                                                                {
                                                                                    XPCollection<AccountMapLine> _locAccountMapLines = new XPCollection<AccountMapLine>
                                                                                                                                   (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                                                    new BinaryOperator("Company", _locOrderCollectionLine.Company),
                                                                                                                                    new BinaryOperator("Workplace", _locOrderCollectionLine.Workplace),
                                                                                                                                    new BinaryOperator("AccountMap", _locJournalMapLine.AccountMap),
                                                                                                                                    new BinaryOperator("Active", true)));

                                                                                    if (_locAccountMapLines != null && _locAccountMapLines.Count() > 0)
                                                                                    {
                                                                                        double _locTotalAmountDebit = 0;
                                                                                        double _locTotalAmountCredit = 0;
                                                                                        double _locTotalBalance = 0;

                                                                                        foreach (AccountMapLine _locAccountMapLine in _locAccountMapLines)
                                                                                        {
                                                                                            if (_locAccountMapLine.AccountCharge == AccountCharge.Debit)
                                                                                            {
                                                                                                _locTotalAmountDebit = _locTotalDiscount;
                                                                                            }
                                                                                            if (_locAccountMapLine.AccountCharge == AccountCharge.Credit)
                                                                                            {
                                                                                                _locTotalAmountCredit = _locTotalDiscount;
                                                                                            }

                                                                                            GeneralJournal _saveGeneralJournal = new GeneralJournal(_currSession)
                                                                                            {
                                                                                                Company = _locOrderCollectionLine.Company,
                                                                                                Workplace = _locOrderCollectionLine.Workplace,
                                                                                                PostingDate = now,
                                                                                                PostingType = PostingType.Sales,
                                                                                                OrderType = OrderType.Item,
                                                                                                PostingMethod = PostingMethod.InvoiceAR,
                                                                                                PostingMethodType = PostingMethodType.Discount,
                                                                                                Account = _locAccountMapLine.Account,
                                                                                                Debit = _locTotalAmountDebit,
                                                                                                Credit = _locTotalAmountCredit,
                                                                                                JournalMonth = _locOrderCollectionLine.JournalMonth,
                                                                                                JournalYear = _locOrderCollectionLine.JournalYear,
                                                                                                OrderCollection = _locOrderCollectionXPO,
                                                                                            };

                                                                                            _saveGeneralJournal.Save();
                                                                                            _saveGeneralJournal.Session.CommitTransaction();

                                                                                            #region AccountingPeriodicLine

                                                                                            if (_locAccountMapLine.Account.Code != null)
                                                                                            {

                                                                                                //Cari Account Periodic Line
                                                                                                AccountingPeriodicLine _locAPL = _currSession.FindObject<AccountingPeriodicLine>
                                                                                                                                (new GroupOperator(GroupOperatorType.And,
                                                                                                                                new BinaryOperator("Company", _locOrderCollectionLine.Company),
                                                                                                                                new BinaryOperator("Workplace", _locOrderCollectionLine.Workplace),
                                                                                                                                new BinaryOperator("AccountNo", _locAccountMapLine.Account),
                                                                                                                                new BinaryOperator("Month", _locOrderCollectionLine.JournalMonth),
                                                                                                                                new BinaryOperator("Year", _locOrderCollectionLine.JournalYear)));
                                                                                                if (_locAPL != null)
                                                                                                {
                                                                                                    if (_locAPL.AccountNo != null)
                                                                                                    {
                                                                                                        if (_locAPL.AccountNo.BalanceType == BalanceType.Change)
                                                                                                        {
                                                                                                            if (_locAPL.AccountNo.AccountCharge == AccountCharge.Debit)
                                                                                                            {
                                                                                                                if (_locTotalAmountDebit > 0)
                                                                                                                {
                                                                                                                    _locTotalBalance = _locAPL.Balance + _locTotalAmountDebit;
                                                                                                                }
                                                                                                                if (_locTotalAmountCredit > 0)
                                                                                                                {
                                                                                                                    _locTotalBalance = _locAPL.Balance - _locTotalAmountCredit;
                                                                                                                }
                                                                                                            }
                                                                                                            if (_locAPL.AccountNo.AccountCharge == AccountCharge.Credit)
                                                                                                            {
                                                                                                                if (_locTotalAmountDebit > 0)
                                                                                                                {
                                                                                                                    _locTotalBalance = _locAPL.Balance - _locTotalAmountDebit;
                                                                                                                }
                                                                                                                if (_locTotalAmountCredit > 0)
                                                                                                                {
                                                                                                                    _locTotalBalance = _locAPL.Balance + _locTotalAmountCredit;
                                                                                                                }
                                                                                                            }
                                                                                                        }

                                                                                                        _locAPL.Balance = _locTotalBalance;
                                                                                                        _locAPL.Debit = _locAPL.Debit + _locTotalAmountDebit;
                                                                                                        _locAPL.Credit = _locAPL.Credit + _locTotalAmountCredit;
                                                                                                        _locAPL.Save();
                                                                                                        _locAPL.Session.CommitTransaction();

                                                                                                    }
                                                                                                }
                                                                                            }
                                                                                            #endregion AccountingPeriodicLine
                                                                                        }
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                        #endregion DiscountAccountGroup
                                                        if (_locSalesInvoiceLineCmp.AccountingPeriodicLine != null)
                                                        {
                                                            if(_locSalesInvoiceLineCmp.AccountingPeriodicLine.BalHold < _locTotalDiscount)
                                                            {
                                                                _locTotalDiscount = _locSalesInvoiceLineCmp.AccountingPeriodicLine.BalHold;
                                                            }
                                                            _locSalesInvoiceLineCmp.AccountingPeriodicLine.BalHold = _locSalesInvoiceLineCmp.AccountingPeriodicLine.BalHold - _locTotalDiscount;
                                                            _locSalesInvoiceLineCmp.AccountingPeriodicLine.Save();
                                                            _locSalesInvoiceLineCmp.AccountingPeriodicLine.Session.CommitTransaction();
                                                        }
                                                    }
                                                    #endregion DiscountJournal  

                                                    _locTotalUnitAmount = 0;
                                                    _locTotalTaxAmount = 0;
                                                    _locTotalDiscount = 0;
                                                }
                                            }
                                        }
                                        #endregion JournalMapByNormalItems
                                        #region JournalMapByFreeItems
                                        XPCollection<SalesInvoiceFreeItemCmp> _locSalesInvoiceFreeItemCmps = new XPCollection<SalesInvoiceFreeItemCmp>(_currSession,
                                                                                                new GroupOperator(GroupOperatorType.And,
                                                                                                new BinaryOperator("SalesInvoiceCmp", _locOrderCollectionLine.PickingMonitoring.SalesInvoiceCmp)));
                                        if(_locSalesInvoiceFreeItemCmps != null && _locSalesInvoiceFreeItemCmps.Count() > 0)
                                        {
                                            foreach(SalesInvoiceFreeItemCmp _locSalesInvoiceFreeItemCmp in _locSalesInvoiceFreeItemCmps)
                                            {
                                                if (_locSalesInvoiceFreeItemCmp.FreeItemRule != null)
                                                {
                                                    if (_locSalesInvoiceFreeItemCmp.FreeItemRule.DiscountAccountGroup != null)
                                                    {
                                                        _locTotalFreeItemAmount = _locSalesInvoiceFreeItemCmp.TUAmount;

                                                        _locFreeAccountGroup = _locSalesInvoiceFreeItemCmp.FreeItemRule.DiscountAccountGroup;

                                                        XPCollection<JournalMap> _locJournalMapItems = new XPCollection<JournalMap>
                                                                                           (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                            new BinaryOperator("Company", _locOrderCollectionLine.Company),
                                                                                            new BinaryOperator("Workplace", _locOrderCollectionLine.Workplace),
                                                                                            new BinaryOperator("DiscountAccountGroup", _locFreeAccountGroup),
                                                                                            new BinaryOperator("Active", true)));

                                                        if (_locJournalMapItems != null && _locJournalMapItems.Count() > 0)
                                                        {
                                                            foreach (JournalMap _locJournalMapItem in _locJournalMapItems)
                                                            {
                                                                XPCollection<JournalMapLine> _locJournalMapLines = new XPCollection<JournalMapLine>
                                                                                                                   (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                                    new BinaryOperator("Company", _locOrderCollectionLine.Company),
                                                                                                                    new BinaryOperator("Workplace", _locOrderCollectionLine.Workplace),
                                                                                                                    new BinaryOperator("JournalMap", _locJournalMapItem),
                                                                                                                    new BinaryOperator("Active", true)));

                                                                if (_locJournalMapLines != null && _locJournalMapLines.Count() > 0)
                                                                {
                                                                    foreach (JournalMapLine _locJournalMapLine in _locJournalMapLines)
                                                                    {
                                                                        if (_locJournalMapLine.AccountMap != null)
                                                                        {
                                                                            if (_locJournalMapLine.AccountMap.PostingType == PostingType.Sales && _locJournalMapLine.AccountMap.OrderType == OrderType.Item
                                                                                && _locJournalMapLine.AccountMap.PostingMethod == PostingMethod.InvoiceAR && _locJournalMapLine.AccountMap.PostingMethodType == PostingMethodType.FreeItem)
                                                                            {
                                                                                XPCollection<AccountMapLine> _locAccountMapLines = new XPCollection<AccountMapLine>
                                                                                                                               (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                                                new BinaryOperator("Company", _locOrderCollectionLine.Company),
                                                                                                                                new BinaryOperator("Workplace", _locOrderCollectionLine.Workplace),
                                                                                                                                new BinaryOperator("AccountMap", _locJournalMapLine.AccountMap),
                                                                                                                                new BinaryOperator("Active", true)));

                                                                                if (_locAccountMapLines != null && _locAccountMapLines.Count() > 0)
                                                                                {
                                                                                    double _locTotalAmountDebit = 0;
                                                                                    double _locTotalAmountCredit = 0;
                                                                                    double _locTotalBalance = 0;

                                                                                    foreach (AccountMapLine _locAccountMapLine in _locAccountMapLines)
                                                                                    {
                                                                                        if (_locAccountMapLine.AccountCharge == AccountCharge.Debit)
                                                                                        {
                                                                                            _locTotalAmountDebit = _locTotalFreeItemAmount;
                                                                                        }
                                                                                        if (_locAccountMapLine.AccountCharge == AccountCharge.Credit)
                                                                                        {
                                                                                            _locTotalAmountCredit = _locTotalFreeItemAmount;
                                                                                        }

                                                                                        GeneralJournal _saveGeneralJournal = new GeneralJournal(_currSession)
                                                                                        {
                                                                                            Company = _locOrderCollectionLine.Company,
                                                                                            Workplace = _locOrderCollectionLine.Workplace,
                                                                                            PostingDate = now,
                                                                                            PostingType = PostingType.Sales,
                                                                                            OrderType = OrderType.Item,
                                                                                            PostingMethod = PostingMethod.InvoiceAR,
                                                                                            PostingMethodType = PostingMethodType.FreeItem,
                                                                                            Account = _locAccountMapLine.Account,
                                                                                            Debit = _locTotalAmountDebit,
                                                                                            Credit = _locTotalAmountCredit,
                                                                                            JournalMonth = _locOrderCollectionLine.JournalMonth,
                                                                                            JournalYear = _locOrderCollectionLine.JournalYear,
                                                                                            OrderCollection = _locOrderCollectionXPO,
                                                                                        };

                                                                                        _saveGeneralJournal.Save();
                                                                                        _saveGeneralJournal.Session.CommitTransaction();

                                                                                        #region AccountingPeriodicLine

                                                                                        if (_locAccountMapLine.Account.Code != null)
                                                                                        {

                                                                                            //Cari Account Periodic Line
                                                                                            AccountingPeriodicLine _locAPL = _currSession.FindObject<AccountingPeriodicLine>
                                                                                                                            (new GroupOperator(GroupOperatorType.And,
                                                                                                                            new BinaryOperator("Company", _locOrderCollectionLine.Company),
                                                                                                                            new BinaryOperator("Workplace", _locOrderCollectionLine.Workplace),
                                                                                                                            new BinaryOperator("AccountNo", _locAccountMapLine.Account),
                                                                                                                            new BinaryOperator("Month", _locOrderCollectionLine.JournalMonth),
                                                                                                                            new BinaryOperator("Year", _locOrderCollectionLine.JournalYear)));
                                                                                            if (_locAPL != null)
                                                                                            {
                                                                                                if (_locAPL.AccountNo != null)
                                                                                                {
                                                                                                    if (_locAPL.AccountNo.BalanceType == BalanceType.Change)
                                                                                                    {
                                                                                                        if (_locAPL.AccountNo.AccountCharge == AccountCharge.Debit)
                                                                                                        {
                                                                                                            if (_locTotalAmountDebit > 0)
                                                                                                            {
                                                                                                                _locTotalBalance = _locAPL.Balance + _locTotalAmountDebit;
                                                                                                            }
                                                                                                            if (_locTotalAmountCredit > 0)
                                                                                                            {
                                                                                                                _locTotalBalance = _locAPL.Balance - _locTotalAmountCredit;
                                                                                                            }
                                                                                                        }
                                                                                                        if (_locAPL.AccountNo.AccountCharge == AccountCharge.Credit)
                                                                                                        {
                                                                                                            if (_locTotalAmountDebit > 0)
                                                                                                            {
                                                                                                                _locTotalBalance = _locAPL.Balance - _locTotalAmountDebit;
                                                                                                            }
                                                                                                            if (_locTotalAmountCredit > 0)
                                                                                                            {
                                                                                                                _locTotalBalance = _locAPL.Balance + _locTotalAmountCredit;
                                                                                                            }
                                                                                                        }
                                                                                                    }

                                                                                                    _locAPL.Balance = _locTotalBalance;
                                                                                                    _locAPL.Debit = _locAPL.Debit + _locTotalAmountDebit;
                                                                                                    _locAPL.Credit = _locAPL.Credit + _locTotalAmountCredit;
                                                                                                    _locAPL.Save();
                                                                                                    _locAPL.Session.CommitTransaction();

                                                                                                }
                                                                                            }
                                                                                        }
                                                                                        #endregion COA
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                                _locTotalFreeItemAmount = 0;
                                            }
                                        }
                                        #endregion JournalMapByFreeItems
                                        #endregion JournalMapByItems

                                        #region JournalMapBusinessPartnerAccountGroup

                                        if (_locOrderCollectionLine.SalesInvoice.BillToCustomer != null)
                                        {
                                            BusinessPartnerAccount _locBusinessPartnerAccount = _currSession.FindObject<BusinessPartnerAccount>(new GroupOperator(GroupOperatorType.And,
                                                                                        new BinaryOperator("Company", _locOrderCollectionLine.Company),
                                                                                        new BinaryOperator("Workplace", _locOrderCollectionLine.Workplace),
                                                                                        new BinaryOperator("BusinessPartner", _locOrderCollectionLine.SalesInvoice.BillToCustomer),
                                                                                        new BinaryOperator("Active", true)));
                                            if(_locBusinessPartnerAccount != null)
                                            {
                                                if (_locBusinessPartnerAccount.BusinessPartnerAccountGroup != null)
                                                {
                                                    _locBusinessPartnerAccountGroup = _locBusinessPartnerAccount.BusinessPartnerAccountGroup;

                                                    _locTotalAmountCollBP = _locOrderCollectionLine.AmountColl;
                                                    _locTotalAmountCollBPDN = _locOrderCollectionLine.AmountDN;
                                                    _locTotalAmountCollBPCN = _locOrderCollectionLine.AmountCN;

                                                    XPCollection<JournalMap> _locJournalMapItems = new XPCollection<JournalMap>
                                                                                       (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                        new BinaryOperator("Company", _locOrderCollectionLine.Company),
                                                                                        new BinaryOperator("Workplace", _locOrderCollectionLine.Workplace),
                                                                                        new BinaryOperator("BusinessPartnerAccountGroup", _locBusinessPartnerAccountGroup),
                                                                                        new BinaryOperator("Active", true)));

                                                    if (_locJournalMapItems != null && _locJournalMapItems.Count() > 0)
                                                    {
                                                        foreach (JournalMap _locJournalMapItem in _locJournalMapItems)
                                                        {
                                                            XPCollection<JournalMapLine> _locJournalMapLines = new XPCollection<JournalMapLine>
                                                                                                               (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                                new BinaryOperator("Company", _locOrderCollectionLine.Company),
                                                                                                                new BinaryOperator("Workplace", _locOrderCollectionLine.Workplace),
                                                                                                                new BinaryOperator("JournalMap", _locJournalMapItem),
                                                                                                                new BinaryOperator("Active", true)));

                                                            if (_locJournalMapLines != null && _locJournalMapLines.Count() > 0)
                                                            {
                                                                foreach (JournalMapLine _locJournalMapLine in _locJournalMapLines)
                                                                {
                                                                    if (_locJournalMapLine.AccountMap != null)
                                                                    {
                                                                        #region NormalAmount
                                                                        if (_locJournalMapLine.AccountMap.PostingType == PostingType.Sales && _locJournalMapLine.AccountMap.OrderType == OrderType.Item
                                                                            && _locJournalMapLine.AccountMap.PostingMethod == PostingMethod.InvoiceAR && _locJournalMapLine.AccountMap.PostingMethodType == PostingMethodType.Normal)
                                                                        {
                                                                            XPCollection<AccountMapLine> _locAccountMapLines = new XPCollection<AccountMapLine>
                                                                                                                           (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                                            new BinaryOperator("Company", _locOrderCollectionLine.Company),
                                                                                                                            new BinaryOperator("Workplace", _locOrderCollectionLine.Workplace),
                                                                                                                            new BinaryOperator("AccountMap", _locJournalMapLine.AccountMap),
                                                                                                                            new BinaryOperator("Active", true)));

                                                                            if (_locAccountMapLines != null && _locAccountMapLines.Count() > 0)
                                                                            {
                                                                                double _locTotalAmountDebit = 0;
                                                                                double _locTotalAmountCredit = 0;
                                                                                double _locTotalBalance = 0;

                                                                                foreach (AccountMapLine _locAccountMapLine in _locAccountMapLines)
                                                                                {
                                                                                    if (_locAccountMapLine.AccountCharge == AccountCharge.Debit)
                                                                                    {
                                                                                        _locTotalAmountDebit = _locTotalAmountCollBP;
                                                                                    }
                                                                                    if (_locAccountMapLine.AccountCharge == AccountCharge.Credit)
                                                                                    {
                                                                                        _locTotalAmountCredit = _locTotalAmountCollBP;
                                                                                    }

                                                                                    GeneralJournal _saveGeneralJournal = new GeneralJournal(_currSession)
                                                                                    {
                                                                                        Company = _locOrderCollectionLine.Company,
                                                                                        Workplace = _locOrderCollectionLine.Workplace,
                                                                                        PostingDate = now,
                                                                                        PostingType = PostingType.Sales,
                                                                                        OrderType = OrderType.Item,
                                                                                        PostingMethod = PostingMethod.InvoiceAR,
                                                                                        PostingMethodType = PostingMethodType.Normal,
                                                                                        Account = _locAccountMapLine.Account,
                                                                                        Debit = _locTotalAmountDebit,
                                                                                        Credit = _locTotalAmountCredit,
                                                                                        JournalMonth = _locOrderCollectionLine.JournalMonth,
                                                                                        JournalYear = _locOrderCollectionLine.JournalYear,
                                                                                        OrderCollection = _locOrderCollectionXPO,
                                                                                    };

                                                                                    _saveGeneralJournal.Save();
                                                                                    _saveGeneralJournal.Session.CommitTransaction();

                                                                                    #region AccountingPeriodicLine

                                                                                    if (_locAccountMapLine.Account.Code != null)
                                                                                    {

                                                                                        //Cari Account Periodic Line
                                                                                        AccountingPeriodicLine _locAPL = _currSession.FindObject<AccountingPeriodicLine>
                                                                                                                        (new GroupOperator(GroupOperatorType.And,
                                                                                                                        new BinaryOperator("Company", _locOrderCollectionLine.Company),
                                                                                                                        new BinaryOperator("Workplace", _locOrderCollectionLine.Workplace),
                                                                                                                        new BinaryOperator("AccountNo", _locAccountMapLine.Account),
                                                                                                                        new BinaryOperator("Month", _locOrderCollectionLine.JournalMonth),
                                                                                                                        new BinaryOperator("Year", _locOrderCollectionLine.JournalYear)));
                                                                                        if (_locAPL != null)
                                                                                        {
                                                                                            if (_locAPL.AccountNo != null)
                                                                                            {
                                                                                                if (_locAPL.AccountNo.BalanceType == BalanceType.Change)
                                                                                                {
                                                                                                    if (_locAPL.AccountNo.AccountCharge == AccountCharge.Debit)
                                                                                                    {
                                                                                                        if (_locTotalAmountDebit > 0)
                                                                                                        {
                                                                                                            _locTotalBalance = _locAPL.Balance + _locTotalAmountDebit;
                                                                                                        }
                                                                                                        if (_locTotalAmountCredit > 0)
                                                                                                        {
                                                                                                            _locTotalBalance = _locAPL.Balance - _locTotalAmountCredit;
                                                                                                        }
                                                                                                    }
                                                                                                    if (_locAPL.AccountNo.AccountCharge == AccountCharge.Credit)
                                                                                                    {
                                                                                                        if (_locTotalAmountDebit > 0)
                                                                                                        {
                                                                                                            _locTotalBalance = _locAPL.Balance - _locTotalAmountDebit;
                                                                                                        }
                                                                                                        if (_locTotalAmountCredit > 0)
                                                                                                        {
                                                                                                            _locTotalBalance = _locAPL.Balance + _locTotalAmountCredit;
                                                                                                        }
                                                                                                    }
                                                                                                }

                                                                                                _locAPL.Balance = _locTotalBalance;
                                                                                                _locAPL.Debit = _locAPL.Debit + _locTotalAmountDebit;
                                                                                                _locAPL.Credit = _locAPL.Credit + _locTotalAmountCredit;
                                                                                                _locAPL.Save();
                                                                                                _locAPL.Session.CommitTransaction();

                                                                                            }
                                                                                        }
                                                                                    }
                                                                                    #endregion COA
                                                                                }
                                                                            }
                                                                        }
                                                                        #endregion NormalAmount
                                                                        #region AmountDN
                                                                        if (_locTotalAmountCollBPDN > 0)
                                                                        {
                                                                            if (_locJournalMapLine.AccountMap.PostingType == PostingType.Sales && _locJournalMapLine.AccountMap.OrderType == OrderType.Item
                                                                            && _locJournalMapLine.AccountMap.PostingMethod == PostingMethod.InvoiceAR && _locJournalMapLine.AccountMap.PostingMethodType == PostingMethodType.AmountDN)
                                                                            {
                                                                                XPCollection<AccountMapLine> _locAccountMapLines = new XPCollection<AccountMapLine>
                                                                                                                               (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                                                new BinaryOperator("Company", _locOrderCollectionLine.Company),
                                                                                                                                new BinaryOperator("Workplace", _locOrderCollectionLine.Workplace),
                                                                                                                                new BinaryOperator("AccountMap", _locJournalMapLine.AccountMap),
                                                                                                                                new BinaryOperator("Active", true)));

                                                                                if (_locAccountMapLines != null && _locAccountMapLines.Count() > 0)
                                                                                {
                                                                                    double _locTotalAmountDebit = 0;
                                                                                    double _locTotalAmountCredit = 0;
                                                                                    double _locTotalBalance = 0;

                                                                                    foreach (AccountMapLine _locAccountMapLine in _locAccountMapLines)
                                                                                    {
                                                                                        if (_locAccountMapLine.AccountCharge == AccountCharge.Debit)
                                                                                        {
                                                                                            _locTotalAmountDebit = _locTotalAmountCollBPDN;
                                                                                        }
                                                                                        if (_locAccountMapLine.AccountCharge == AccountCharge.Credit)
                                                                                        {
                                                                                            _locTotalAmountCredit = _locTotalAmountCollBPDN;
                                                                                        }

                                                                                        GeneralJournal _saveGeneralJournal = new GeneralJournal(_currSession)
                                                                                        {
                                                                                            Company = _locOrderCollectionLine.Company,
                                                                                            Workplace = _locOrderCollectionLine.Workplace,
                                                                                            PostingDate = now,
                                                                                            PostingType = PostingType.Sales,
                                                                                            PostingMethod = PostingMethod.InvoiceAR,
                                                                                            PostingMethodType = PostingMethodType.AmountDN,
                                                                                            Account = _locAccountMapLine.Account,
                                                                                            Debit = _locTotalAmountDebit,
                                                                                            Credit = _locTotalAmountCredit,
                                                                                            JournalMonth = _locOrderCollectionLine.JournalMonth,
                                                                                            JournalYear = _locOrderCollectionLine.JournalYear,
                                                                                            OrderCollection = _locOrderCollectionXPO,
                                                                                        };

                                                                                        _saveGeneralJournal.Save();
                                                                                        _saveGeneralJournal.Session.CommitTransaction();

                                                                                        #region AccountingPeriodicLine

                                                                                        if (_locAccountMapLine.Account.Code != null)
                                                                                        {

                                                                                            //Cari Account Periodic Line
                                                                                            AccountingPeriodicLine _locAPL = _currSession.FindObject<AccountingPeriodicLine>
                                                                                                                            (new GroupOperator(GroupOperatorType.And,
                                                                                                                            new BinaryOperator("Company", _locOrderCollectionLine.Company),
                                                                                                                            new BinaryOperator("Workplace", _locOrderCollectionLine.Workplace),
                                                                                                                            new BinaryOperator("AccountNo", _locAccountMapLine.Account),
                                                                                                                            new BinaryOperator("Month", _locOrderCollectionLine.JournalMonth),
                                                                                                                            new BinaryOperator("Year", _locOrderCollectionLine.JournalYear)));
                                                                                            if (_locAPL != null)
                                                                                            {
                                                                                                if (_locAPL.AccountNo != null)
                                                                                                {
                                                                                                    if (_locAPL.AccountNo.BalanceType == BalanceType.Change)
                                                                                                    {
                                                                                                        if (_locAPL.AccountNo.AccountCharge == AccountCharge.Debit)
                                                                                                        {
                                                                                                            if (_locTotalAmountDebit > 0)
                                                                                                            {
                                                                                                                _locTotalBalance = _locAPL.Balance + _locTotalAmountDebit;
                                                                                                            }
                                                                                                            if (_locTotalAmountCredit > 0)
                                                                                                            {
                                                                                                                _locTotalBalance = _locAPL.Balance - _locTotalAmountCredit;
                                                                                                            }
                                                                                                        }
                                                                                                        if (_locAPL.AccountNo.AccountCharge == AccountCharge.Credit)
                                                                                                        {
                                                                                                            if (_locTotalAmountDebit > 0)
                                                                                                            {
                                                                                                                _locTotalBalance = _locAPL.Balance - _locTotalAmountDebit;
                                                                                                            }
                                                                                                            if (_locTotalAmountCredit > 0)
                                                                                                            {
                                                                                                                _locTotalBalance = _locAPL.Balance + _locTotalAmountCredit;
                                                                                                            }
                                                                                                        }
                                                                                                    }

                                                                                                    _locAPL.Balance = _locTotalBalance;
                                                                                                    _locAPL.Debit = _locAPL.Debit + _locTotalAmountDebit;
                                                                                                    _locAPL.Credit = _locAPL.Credit + _locTotalAmountCredit;
                                                                                                    _locAPL.Save();
                                                                                                    _locAPL.Session.CommitTransaction();

                                                                                                }
                                                                                            }
                                                                                        }
                                                                                        #endregion COA
                                                                                    }
                                                                                }
                                                                            }
                                                                        }

                                                                        #endregion AmountDN
                                                                        #region AmountCN
                                                                        if (_locTotalAmountCollBPCN > 0)
                                                                        {
                                                                            if (_locJournalMapLine.AccountMap.PostingType == PostingType.Sales && _locJournalMapLine.AccountMap.OrderType == OrderType.Item
                                                                            && _locJournalMapLine.AccountMap.PostingMethod == PostingMethod.InvoiceAR && _locJournalMapLine.AccountMap.PostingMethodType == PostingMethodType.AmountCN)
                                                                            {
                                                                                XPCollection<AccountMapLine> _locAccountMapLines = new XPCollection<AccountMapLine>
                                                                                                                               (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                                                new BinaryOperator("Company", _locOrderCollectionLine.Company),
                                                                                                                                new BinaryOperator("Workplace", _locOrderCollectionLine.Workplace),
                                                                                                                                new BinaryOperator("AccountMap", _locJournalMapLine.AccountMap),
                                                                                                                                new BinaryOperator("Active", true)));

                                                                                if (_locAccountMapLines != null && _locAccountMapLines.Count() > 0)
                                                                                {
                                                                                    double _locTotalAmountDebit = 0;
                                                                                    double _locTotalAmountCredit = 0;
                                                                                    double _locTotalBalance = 0;

                                                                                    foreach (AccountMapLine _locAccountMapLine in _locAccountMapLines)
                                                                                    {
                                                                                        if (_locAccountMapLine.AccountCharge == AccountCharge.Debit)
                                                                                        {
                                                                                            _locTotalAmountDebit = _locTotalAmountCollBPCN;
                                                                                        }
                                                                                        if (_locAccountMapLine.AccountCharge == AccountCharge.Credit)
                                                                                        {
                                                                                            _locTotalAmountCredit = _locTotalAmountCollBPCN;
                                                                                        }

                                                                                        GeneralJournal _saveGeneralJournal = new GeneralJournal(_currSession)
                                                                                        {
                                                                                            Company = _locOrderCollectionLine.Company,
                                                                                            Workplace = _locOrderCollectionLine.Workplace,
                                                                                            PostingDate = now,
                                                                                            PostingType = PostingType.Sales,
                                                                                            PostingMethod = PostingMethod.InvoiceAR,
                                                                                            PostingMethodType = PostingMethodType.AmountCN,
                                                                                            Account = _locAccountMapLine.Account,
                                                                                            Debit = _locTotalAmountDebit,
                                                                                            Credit = _locTotalAmountCredit,
                                                                                            JournalMonth = _locOrderCollectionLine.JournalMonth,
                                                                                            JournalYear = _locOrderCollectionLine.JournalYear,
                                                                                            OrderCollection = _locOrderCollectionXPO,
                                                                                        };

                                                                                        _saveGeneralJournal.Save();
                                                                                        _saveGeneralJournal.Session.CommitTransaction();

                                                                                        #region AccountingPeriodicLine

                                                                                        if (_locAccountMapLine.Account.Code != null)
                                                                                        {

                                                                                            //Cari Account Periodic Line
                                                                                            AccountingPeriodicLine _locAPL = _currSession.FindObject<AccountingPeriodicLine>
                                                                                                                            (new GroupOperator(GroupOperatorType.And,
                                                                                                                            new BinaryOperator("Company", _locOrderCollectionLine.Company),
                                                                                                                            new BinaryOperator("Workplace", _locOrderCollectionLine.Workplace),
                                                                                                                            new BinaryOperator("AccountNo", _locAccountMapLine.Account),
                                                                                                                            new BinaryOperator("Month", _locOrderCollectionLine.JournalMonth),
                                                                                                                            new BinaryOperator("Year", _locOrderCollectionLine.JournalYear)));
                                                                                            if (_locAPL != null)
                                                                                            {
                                                                                                if (_locAPL.AccountNo != null)
                                                                                                {
                                                                                                    if (_locAPL.AccountNo.BalanceType == BalanceType.Change)
                                                                                                    {
                                                                                                        if (_locAPL.AccountNo.AccountCharge == AccountCharge.Debit)
                                                                                                        {
                                                                                                            if (_locTotalAmountDebit > 0)
                                                                                                            {
                                                                                                                _locTotalBalance = _locAPL.Balance + _locTotalAmountDebit;
                                                                                                            }
                                                                                                            if (_locTotalAmountCredit > 0)
                                                                                                            {
                                                                                                                _locTotalBalance = _locAPL.Balance - _locTotalAmountCredit;
                                                                                                            }
                                                                                                        }
                                                                                                        if (_locAPL.AccountNo.AccountCharge == AccountCharge.Credit)
                                                                                                        {
                                                                                                            if (_locTotalAmountDebit > 0)
                                                                                                            {
                                                                                                                _locTotalBalance = _locAPL.Balance - _locTotalAmountDebit;
                                                                                                            }
                                                                                                            if (_locTotalAmountCredit > 0)
                                                                                                            {
                                                                                                                _locTotalBalance = _locAPL.Balance + _locTotalAmountCredit;
                                                                                                            }
                                                                                                        }
                                                                                                    }

                                                                                                    _locAPL.Balance = _locTotalBalance;
                                                                                                    _locAPL.Debit = _locAPL.Debit + _locTotalAmountDebit;
                                                                                                    _locAPL.Credit = _locAPL.Credit + _locTotalAmountCredit;
                                                                                                    _locAPL.Save();
                                                                                                    _locAPL.Session.CommitTransaction();

                                                                                                }
                                                                                            }
                                                                                        }
                                                                                        #endregion COA
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                        #endregion AmountCN
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            } 
                                        }
                                        #endregion JournalMapBusinessPartnerAccountGroup

                                        #region JournalMapCompanyAccountGroup
                                        if (_locOrderCollectionLine.Company != null)
                                        {
                                            _locTotalAmountCollC = _locOrderCollectionLine.AmountColl;
                                            _locTotalAmountCollCDN = _locOrderCollectionLine.AmountDN;
                                            _locTotalAmountCollCCN = _locOrderCollectionLine.AmountCN;

                                            CompanyAccount _locCompanyAccount = _currSession.FindObject<CompanyAccount>(new GroupOperator(GroupOperatorType.And,
                                                                    new BinaryOperator("Company", _locOrderCollectionLine.Company),
                                                                    new BinaryOperator("Workplace", _locOrderCollectionLine.Workplace),
                                                                    new BinaryOperator("Active", true)));

                                            if (_locCompanyAccount != null)
                                            {
                                                if (_locCompanyAccount.CompanyAccountGroup != null)
                                                {
                                                    _locCompanyAccountGroup = _locCompanyAccount.CompanyAccountGroup;

                                                    XPCollection<JournalMap> _locJournalMapItems = new XPCollection<JournalMap>
                                                                                   (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                    new BinaryOperator("Company", _locOrderCollectionLine.Company),
                                                                                    new BinaryOperator("Workplace", _locOrderCollectionLine.Workplace),
                                                                                    new BinaryOperator("CompanyAccountGroup", _locCompanyAccountGroup),
                                                                                    new BinaryOperator("Active", true)));

                                                    if (_locJournalMapItems != null && _locJournalMapItems.Count() > 0)
                                                    {
                                                        foreach (JournalMap _locJournalMapItem in _locJournalMapItems)
                                                        {
                                                            XPCollection<JournalMapLine> _locJournalMapLines = new XPCollection<JournalMapLine>
                                                                                                               (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                                new BinaryOperator("Company", _locOrderCollectionLine.Company),
                                                                                                                new BinaryOperator("Workplace", _locOrderCollectionLine.Workplace),
                                                                                                                new BinaryOperator("JournalMap", _locJournalMapItem),
                                                                                                                new BinaryOperator("Active", true)));

                                                            if (_locJournalMapLines != null && _locJournalMapLines.Count() > 0)
                                                            {
                                                                foreach (JournalMapLine _locJournalMapLine in _locJournalMapLines)
                                                                {
                                                                    if (_locJournalMapLine.AccountMap != null)
                                                                    {
                                                                        #region NormalAmount
                                                                        if (_locJournalMapLine.AccountMap.PostingType == PostingType.Sales && _locJournalMapLine.AccountMap.OrderType == OrderType.Item
                                                                            && _locJournalMapLine.AccountMap.PostingMethod == PostingMethod.InvoiceAR && _locJournalMapLine.AccountMap.PostingMethodType == PostingMethodType.Normal)
                                                                        {
                                                                            XPCollection<AccountMapLine> _locAccountMapLines = new XPCollection<AccountMapLine>
                                                                                                                           (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                                            new BinaryOperator("Company", _locOrderCollectionLine.Company),
                                                                                                                            new BinaryOperator("Workplace", _locOrderCollectionLine.Workplace),
                                                                                                                            new BinaryOperator("AccountMap", _locJournalMapLine.AccountMap),
                                                                                                                            new BinaryOperator("Active", true)));

                                                                            if (_locAccountMapLines != null && _locAccountMapLines.Count() > 0)
                                                                            {
                                                                                double _locTotalAmountDebit = 0;
                                                                                double _locTotalAmountCredit = 0;
                                                                                double _locTotalBalance = 0;

                                                                                foreach (AccountMapLine _locAccountMapLine in _locAccountMapLines)
                                                                                {
                                                                                    if (_locAccountMapLine.AccountCharge == AccountCharge.Debit)
                                                                                    {
                                                                                        _locTotalAmountDebit = _locTotalAmountCollC;
                                                                                    }
                                                                                    if (_locAccountMapLine.AccountCharge == AccountCharge.Credit)
                                                                                    {
                                                                                        _locTotalAmountCredit = _locTotalAmountCollC;
                                                                                    }

                                                                                    GeneralJournal _saveGeneralJournal = new GeneralJournal(_currSession)
                                                                                    {
                                                                                        Company = _locOrderCollectionLine.Company,
                                                                                        Workplace = _locOrderCollectionLine.Workplace,
                                                                                        PostingDate = now,
                                                                                        PostingType = PostingType.Sales,
                                                                                        OrderType = OrderType.Item,
                                                                                        PostingMethod = PostingMethod.InvoiceAR,
                                                                                        PostingMethodType = PostingMethodType.Normal,
                                                                                        Account = _locAccountMapLine.Account,
                                                                                        Debit = _locTotalAmountDebit,
                                                                                        Credit = _locTotalAmountCredit,
                                                                                        JournalMonth = _locOrderCollectionLine.JournalMonth,
                                                                                        JournalYear = _locOrderCollectionLine.JournalYear,

                                                                                        OrderCollection = _locOrderCollectionXPO,
                                                                                    };

                                                                                    _saveGeneralJournal.Save();
                                                                                    _saveGeneralJournal.Session.CommitTransaction();

                                                                                    #region AccountingPeriodicLine

                                                                                    if (_locAccountMapLine.Account.Code != null)
                                                                                    {

                                                                                        //Cari Account Periodic Line
                                                                                        AccountingPeriodicLine _locAPL = _currSession.FindObject<AccountingPeriodicLine>
                                                                                                                        (new GroupOperator(GroupOperatorType.And,
                                                                                                                        new BinaryOperator("Company", _locOrderCollectionLine.Company),
                                                                                                                        new BinaryOperator("Workplace", _locOrderCollectionLine.Workplace),
                                                                                                                        new BinaryOperator("AccountNo", _locAccountMapLine.Account),
                                                                                                                        new BinaryOperator("Month", _locOrderCollectionLine.JournalMonth),
                                                                                                                        new BinaryOperator("Year", _locOrderCollectionLine.JournalYear)));
                                                                                        if (_locAPL != null)
                                                                                        {
                                                                                            if (_locAPL.AccountNo != null)
                                                                                            {
                                                                                                if (_locAPL.AccountNo.BalanceType == BalanceType.Change)
                                                                                                {
                                                                                                    if (_locAPL.AccountNo.AccountCharge == AccountCharge.Debit)
                                                                                                    {
                                                                                                        if (_locTotalAmountDebit > 0)
                                                                                                        {
                                                                                                            _locTotalBalance = _locAPL.Balance + _locTotalAmountDebit;
                                                                                                        }
                                                                                                        if (_locTotalAmountCredit > 0)
                                                                                                        {
                                                                                                            _locTotalBalance = _locAPL.Balance - _locTotalAmountCredit;
                                                                                                        }
                                                                                                    }
                                                                                                    if (_locAPL.AccountNo.AccountCharge == AccountCharge.Credit)
                                                                                                    {
                                                                                                        if (_locTotalAmountDebit > 0)
                                                                                                        {
                                                                                                            _locTotalBalance = _locAPL.Balance - _locTotalAmountDebit;
                                                                                                        }
                                                                                                        if (_locTotalAmountCredit > 0)
                                                                                                        {
                                                                                                            _locTotalBalance = _locAPL.Balance + _locTotalAmountCredit;
                                                                                                        }
                                                                                                    }
                                                                                                }

                                                                                                _locAPL.Balance = _locTotalBalance;
                                                                                                _locAPL.Debit = _locAPL.Debit + _locTotalAmountDebit;
                                                                                                _locAPL.Credit = _locAPL.Credit + _locTotalAmountCredit;
                                                                                                _locAPL.Save();
                                                                                                _locAPL.Session.CommitTransaction();

                                                                                            }
                                                                                        }
                                                                                    }
                                                                                    #endregion COA
                                                                                }
                                                                            }
                                                                        }
                                                                        #endregion NormalAmount
                                                                        #region AmountDN
                                                                        if(_locTotalAmountCollCDN > 0)
                                                                        {
                                                                            if (_locJournalMapLine.AccountMap.PostingType == PostingType.Sales && _locJournalMapLine.AccountMap.OrderType == OrderType.Item
                                                                            && _locJournalMapLine.AccountMap.PostingMethod == PostingMethod.InvoiceAR && _locJournalMapLine.AccountMap.PostingMethodType == PostingMethodType.AmountDN)
                                                                            {
                                                                                XPCollection<AccountMapLine> _locAccountMapLines = new XPCollection<AccountMapLine>
                                                                                                                               (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                                                new BinaryOperator("Company", _locOrderCollectionLine.Company),
                                                                                                                                new BinaryOperator("Workplace", _locOrderCollectionLine.Workplace),
                                                                                                                                new BinaryOperator("AccountMap", _locJournalMapLine.AccountMap),
                                                                                                                                new BinaryOperator("Active", true)));

                                                                                if (_locAccountMapLines != null && _locAccountMapLines.Count() > 0)
                                                                                {
                                                                                    double _locTotalAmountDebit = 0;
                                                                                    double _locTotalAmountCredit = 0;
                                                                                    double _locTotalBalance = 0;

                                                                                    foreach (AccountMapLine _locAccountMapLine in _locAccountMapLines)
                                                                                    {
                                                                                        if (_locAccountMapLine.AccountCharge == AccountCharge.Debit)
                                                                                        {
                                                                                            _locTotalAmountDebit = _locTotalAmountCollCDN;
                                                                                        }
                                                                                        if (_locAccountMapLine.AccountCharge == AccountCharge.Credit)
                                                                                        {
                                                                                            _locTotalAmountCredit = _locTotalAmountCollCDN;
                                                                                        }

                                                                                        GeneralJournal _saveGeneralJournal = new GeneralJournal(_currSession)
                                                                                        {
                                                                                            Company = _locOrderCollectionLine.Company,
                                                                                            Workplace = _locOrderCollectionLine.Workplace,
                                                                                            PostingDate = now,
                                                                                            PostingType = PostingType.Sales,
                                                                                            OrderType = OrderType.Item,
                                                                                            PostingMethod = PostingMethod.InvoiceAR,
                                                                                            PostingMethodType = PostingMethodType.AmountDN,
                                                                                            Account = _locAccountMapLine.Account,
                                                                                            Debit = _locTotalAmountDebit,
                                                                                            Credit = _locTotalAmountCredit,
                                                                                            JournalMonth = _locOrderCollectionLine.JournalMonth,
                                                                                            JournalYear = _locOrderCollectionLine.JournalYear,

                                                                                            OrderCollection = _locOrderCollectionXPO,
                                                                                        };

                                                                                        _saveGeneralJournal.Save();
                                                                                        _saveGeneralJournal.Session.CommitTransaction();

                                                                                        #region AccountingPeriodicLine

                                                                                        if (_locAccountMapLine.Account.Code != null)
                                                                                        {

                                                                                            //Cari Account Periodic Line
                                                                                            AccountingPeriodicLine _locAPL = _currSession.FindObject<AccountingPeriodicLine>
                                                                                                                            (new GroupOperator(GroupOperatorType.And,
                                                                                                                            new BinaryOperator("Company", _locOrderCollectionLine.Company),
                                                                                                                            new BinaryOperator("Workplace", _locOrderCollectionLine.Workplace),
                                                                                                                            new BinaryOperator("AccountNo", _locAccountMapLine.Account),
                                                                                                                            new BinaryOperator("Month", _locOrderCollectionLine.JournalMonth),
                                                                                                                            new BinaryOperator("Year", _locOrderCollectionLine.JournalYear)));
                                                                                            if (_locAPL != null)
                                                                                            {
                                                                                                if (_locAPL.AccountNo != null)
                                                                                                {
                                                                                                    if (_locAPL.AccountNo.BalanceType == BalanceType.Change)
                                                                                                    {
                                                                                                        if (_locAPL.AccountNo.AccountCharge == AccountCharge.Debit)
                                                                                                        {
                                                                                                            if (_locTotalAmountDebit > 0)
                                                                                                            {
                                                                                                                _locTotalBalance = _locAPL.Balance + _locTotalAmountDebit;
                                                                                                            }
                                                                                                            if (_locTotalAmountCredit > 0)
                                                                                                            {
                                                                                                                _locTotalBalance = _locAPL.Balance - _locTotalAmountCredit;
                                                                                                            }
                                                                                                        }
                                                                                                        if (_locAPL.AccountNo.AccountCharge == AccountCharge.Credit)
                                                                                                        {
                                                                                                            if (_locTotalAmountDebit > 0)
                                                                                                            {
                                                                                                                _locTotalBalance = _locAPL.Balance - _locTotalAmountDebit;
                                                                                                            }
                                                                                                            if (_locTotalAmountCredit > 0)
                                                                                                            {
                                                                                                                _locTotalBalance = _locAPL.Balance + _locTotalAmountCredit;
                                                                                                            }
                                                                                                        }
                                                                                                    }

                                                                                                    _locAPL.Balance = _locTotalBalance;
                                                                                                    _locAPL.Debit = _locAPL.Debit + _locTotalAmountDebit;
                                                                                                    _locAPL.Credit = _locAPL.Credit + _locTotalAmountCredit;
                                                                                                    _locAPL.Save();
                                                                                                    _locAPL.Session.CommitTransaction();

                                                                                                }
                                                                                            }
                                                                                        }
                                                                                        #endregion COA
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                        
                                                                        #endregion AmountDN
                                                                        #region AmountCN
                                                                        if(_locTotalAmountCollCCN > 0)
                                                                        {
                                                                            if (_locJournalMapLine.AccountMap.PostingType == PostingType.Sales && _locJournalMapLine.AccountMap.OrderType == OrderType.Item
                                                                            && _locJournalMapLine.AccountMap.PostingMethod == PostingMethod.InvoiceAR && _locJournalMapLine.AccountMap.PostingMethodType == PostingMethodType.AmountCN)
                                                                            {
                                                                                XPCollection<AccountMapLine> _locAccountMapLines = new XPCollection<AccountMapLine>
                                                                                                                               (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                                                new BinaryOperator("Company", _locOrderCollectionLine.Company),
                                                                                                                                new BinaryOperator("Workplace", _locOrderCollectionLine.Workplace),
                                                                                                                                new BinaryOperator("AccountMap", _locJournalMapLine.AccountMap),
                                                                                                                                new BinaryOperator("Active", true)));

                                                                                if (_locAccountMapLines != null && _locAccountMapLines.Count() > 0)
                                                                                {
                                                                                    double _locTotalAmountDebit = 0;
                                                                                    double _locTotalAmountCredit = 0;
                                                                                    double _locTotalBalance = 0;

                                                                                    foreach (AccountMapLine _locAccountMapLine in _locAccountMapLines)
                                                                                    {
                                                                                        if (_locAccountMapLine.AccountCharge == AccountCharge.Debit)
                                                                                        {
                                                                                            _locTotalAmountDebit = _locTotalAmountCollCCN;
                                                                                        }
                                                                                        if (_locAccountMapLine.AccountCharge == AccountCharge.Credit)
                                                                                        {
                                                                                            _locTotalAmountCredit = _locTotalAmountCollCCN;
                                                                                        }

                                                                                        GeneralJournal _saveGeneralJournal = new GeneralJournal(_currSession)
                                                                                        {
                                                                                            Company = _locOrderCollectionLine.Company,
                                                                                            Workplace = _locOrderCollectionLine.Workplace,
                                                                                            PostingDate = now,
                                                                                            PostingType = PostingType.Sales,
                                                                                            OrderType = OrderType.Item,
                                                                                            PostingMethod = PostingMethod.InvoiceAR,
                                                                                            PostingMethodType = PostingMethodType.AmountCN,
                                                                                            Account = _locAccountMapLine.Account,
                                                                                            Debit = _locTotalAmountDebit,
                                                                                            Credit = _locTotalAmountCredit,
                                                                                            JournalMonth = _locOrderCollectionLine.JournalMonth,
                                                                                            JournalYear = _locOrderCollectionLine.JournalYear,

                                                                                            OrderCollection = _locOrderCollectionXPO,
                                                                                        };

                                                                                        _saveGeneralJournal.Save();
                                                                                        _saveGeneralJournal.Session.CommitTransaction();

                                                                                        #region AccountingPeriodicLine

                                                                                        if (_locAccountMapLine.Account.Code != null)
                                                                                        {

                                                                                            //Cari Account Periodic Line
                                                                                            AccountingPeriodicLine _locAPL = _currSession.FindObject<AccountingPeriodicLine>
                                                                                                                            (new GroupOperator(GroupOperatorType.And,
                                                                                                                            new BinaryOperator("Company", _locOrderCollectionLine.Company),
                                                                                                                            new BinaryOperator("Workplace", _locOrderCollectionLine.Workplace),
                                                                                                                            new BinaryOperator("AccountNo", _locAccountMapLine.Account),
                                                                                                                            new BinaryOperator("Month", _locOrderCollectionLine.JournalMonth),
                                                                                                                            new BinaryOperator("Year", _locOrderCollectionLine.JournalYear)));
                                                                                            if (_locAPL != null)
                                                                                            {
                                                                                                if (_locAPL.AccountNo != null)
                                                                                                {
                                                                                                    if (_locAPL.AccountNo.BalanceType == BalanceType.Change)
                                                                                                    {
                                                                                                        if (_locAPL.AccountNo.AccountCharge == AccountCharge.Debit)
                                                                                                        {
                                                                                                            if (_locTotalAmountDebit > 0)
                                                                                                            {
                                                                                                                _locTotalBalance = _locAPL.Balance + _locTotalAmountDebit;
                                                                                                            }
                                                                                                            if (_locTotalAmountCredit > 0)
                                                                                                            {
                                                                                                                _locTotalBalance = _locAPL.Balance - _locTotalAmountCredit;
                                                                                                            }
                                                                                                        }
                                                                                                        if (_locAPL.AccountNo.AccountCharge == AccountCharge.Credit)
                                                                                                        {
                                                                                                            if (_locTotalAmountDebit > 0)
                                                                                                            {
                                                                                                                _locTotalBalance = _locAPL.Balance - _locTotalAmountDebit;
                                                                                                            }
                                                                                                            if (_locTotalAmountCredit > 0)
                                                                                                            {
                                                                                                                _locTotalBalance = _locAPL.Balance + _locTotalAmountCredit;
                                                                                                            }
                                                                                                        }
                                                                                                    }

                                                                                                    _locAPL.Balance = _locTotalBalance;
                                                                                                    _locAPL.Debit = _locAPL.Debit + _locTotalAmountDebit;
                                                                                                    _locAPL.Credit = _locAPL.Credit + _locTotalAmountCredit;
                                                                                                    _locAPL.Save();
                                                                                                    _locAPL.Session.CommitTransaction();

                                                                                                }
                                                                                            }
                                                                                        }
                                                                                        #endregion COA
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                        
                                                                        #endregion AmountCN
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        #endregion JournalMapCompanyAccountGroup

                                        #region DiscountOnHeader
                                        if (_locOrderCollectionLine.PickingMonitoring.SalesInvoiceCmp.DiscountRule != null)
                                        {
                                            _locTotalHeaderDiscount = _locOrderCollectionLine.PickingMonitoring.SalesInvoiceCmp.AmountDisc;

                                            #region DiscountAccountGroup
                                            if (_locOrderCollectionLine.PickingMonitoring.SalesInvoiceCmp.DiscountRule.DiscountAccountGroup != null)
                                            {
                                                _locHeaderDiscountAccountGroup = _locOrderCollectionLine.SalesInvoice.DiscountRule.DiscountAccountGroup;

                                                XPCollection<JournalMap> _locJournalMapItems = new XPCollection<JournalMap>
                                                                                               (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                new BinaryOperator("Company", _locOrderCollectionLine.Company),
                                                                                                new BinaryOperator("Workplace", _locOrderCollectionLine.Workplace),
                                                                                                new BinaryOperator("DiscountAccountGroup", _locHeaderDiscountAccountGroup),
                                                                                                new BinaryOperator("Active", true)));

                                                if (_locJournalMapItems != null && _locJournalMapItems.Count() > 0)
                                                {
                                                    foreach (JournalMap _locJournalMapItem in _locJournalMapItems)
                                                    {
                                                        XPCollection<JournalMapLine> _locJournalMapLines = new XPCollection<JournalMapLine>
                                                                                                           (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                            new BinaryOperator("Company", _locOrderCollectionLine.Company),
                                                                                                            new BinaryOperator("Workplace", _locOrderCollectionLine.Workplace),
                                                                                                            new BinaryOperator("JournalMap", _locJournalMapItem),
                                                                                                            new BinaryOperator("Active", true)));

                                                        if (_locJournalMapLines != null && _locJournalMapLines.Count() > 0)
                                                        {
                                                            foreach (JournalMapLine _locJournalMapLine in _locJournalMapLines)
                                                            {
                                                                if (_locJournalMapLine.AccountMap != null)
                                                                {
                                                                    if (_locJournalMapLine.AccountMap.PostingType == PostingType.Sales && _locJournalMapLine.AccountMap.OrderType == OrderType.Item
                                                                        && _locJournalMapLine.AccountMap.PostingMethod == PostingMethod.InvoiceAR && _locJournalMapLine.AccountMap.PostingMethodType == PostingMethodType.Discount)
                                                                    {
                                                                        XPCollection<AccountMapLine> _locAccountMapLines = new XPCollection<AccountMapLine>
                                                                                                                       (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                                        new BinaryOperator("Company", _locOrderCollectionLine.Company),
                                                                                                                        new BinaryOperator("Workplace", _locOrderCollectionLine.Workplace),
                                                                                                                        new BinaryOperator("AccountMap", _locJournalMapLine.AccountMap),
                                                                                                                        new BinaryOperator("Active", true)));

                                                                        if (_locAccountMapLines != null && _locAccountMapLines.Count() > 0)
                                                                        {
                                                                            double _locTotalAmountDebit = 0;
                                                                            double _locTotalAmountCredit = 0;
                                                                            double _locTotalBalance = 0;

                                                                            foreach (AccountMapLine _locAccountMapLine in _locAccountMapLines)
                                                                            {
                                                                                if (_locAccountMapLine.AccountCharge == AccountCharge.Debit)
                                                                                {
                                                                                    _locTotalAmountDebit = _locTotalHeaderDiscount;
                                                                                }
                                                                                if (_locAccountMapLine.AccountCharge == AccountCharge.Credit)
                                                                                {
                                                                                    _locTotalAmountCredit = _locTotalHeaderDiscount;
                                                                                }

                                                                                GeneralJournal _saveGeneralJournal = new GeneralJournal(_currSession)
                                                                                {
                                                                                    Company = _locOrderCollectionLine.Company,
                                                                                    Workplace = _locOrderCollectionLine.Workplace,
                                                                                    PostingDate = now,
                                                                                    PostingType = PostingType.Sales,
                                                                                    OrderType = OrderType.Item,
                                                                                    PostingMethod = PostingMethod.InvoiceAR,
                                                                                    PostingMethodType = PostingMethodType.Discount,
                                                                                    Account = _locAccountMapLine.Account,
                                                                                    Debit = _locTotalAmountDebit,
                                                                                    Credit = _locTotalAmountCredit,
                                                                                    JournalMonth = _locOrderCollectionLine.JournalMonth,
                                                                                    JournalYear = _locOrderCollectionLine.JournalYear,
                                                                                    OrderCollection = _locOrderCollectionXPO,
                                                                                };

                                                                                _saveGeneralJournal.Save();
                                                                                _saveGeneralJournal.Session.CommitTransaction();

                                                                                #region AccountingPeriodicLine

                                                                                if (_locAccountMapLine.Account.Code != null)
                                                                                {

                                                                                    //Cari Account Periodic Line
                                                                                    AccountingPeriodicLine _locAPL = _currSession.FindObject<AccountingPeriodicLine>
                                                                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                                                                    new BinaryOperator("Company", _locOrderCollectionLine.Company),
                                                                                                                    new BinaryOperator("Workplace", _locOrderCollectionLine.Workplace),
                                                                                                                    new BinaryOperator("AccountNo", _locAccountMapLine.Account),
                                                                                                                    new BinaryOperator("Month", _locOrderCollectionLine.JournalMonth),
                                                                                                                    new BinaryOperator("Year", _locOrderCollectionLine.JournalYear)));
                                                                                    if (_locAPL != null)
                                                                                    {
                                                                                        if (_locAPL.AccountNo != null)
                                                                                        {
                                                                                            if (_locAPL.AccountNo.BalanceType == BalanceType.Change)
                                                                                            {
                                                                                                if (_locAPL.AccountNo.AccountCharge == AccountCharge.Debit)
                                                                                                {
                                                                                                    if (_locTotalAmountDebit > 0)
                                                                                                    {
                                                                                                        _locTotalBalance = _locAPL.Balance + _locTotalAmountDebit;
                                                                                                    }
                                                                                                    if (_locTotalAmountCredit > 0)
                                                                                                    {
                                                                                                        _locTotalBalance = _locAPL.Balance - _locTotalAmountCredit;
                                                                                                    }
                                                                                                }
                                                                                                if (_locAPL.AccountNo.AccountCharge == AccountCharge.Credit)
                                                                                                {
                                                                                                    if (_locTotalAmountDebit > 0)
                                                                                                    {
                                                                                                        _locTotalBalance = _locAPL.Balance - _locTotalAmountDebit;
                                                                                                    }
                                                                                                    if (_locTotalAmountCredit > 0)
                                                                                                    {
                                                                                                        _locTotalBalance = _locAPL.Balance + _locTotalAmountCredit;
                                                                                                    }
                                                                                                }
                                                                                            }

                                                                                            _locAPL.Balance = _locTotalBalance;
                                                                                            _locAPL.Debit = _locAPL.Debit + _locTotalAmountDebit;
                                                                                            _locAPL.Credit = _locAPL.Credit + _locTotalAmountCredit;
                                                                                            _locAPL.Save();
                                                                                            _locAPL.Session.CommitTransaction();

                                                                                        }
                                                                                    }
                                                                                }
                                                                                #endregion COA
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                            #endregion DiscountAccountGroup
                                            if (_locOrderCollectionLine.PickingMonitoring.SalesInvoiceCmp.AccountingPeriodicLine != null)
                                            {
                                                if(_locOrderCollectionLine.PickingMonitoring.SalesInvoiceCmp.AccountingPeriodicLine.BalHold < _locTotalDiscount)
                                                {
                                                    _locTotalDiscount = _locOrderCollectionLine.PickingMonitoring.SalesInvoiceCmp.AccountingPeriodicLine.BalHold;
                                                }
                                                _locOrderCollectionLine.PickingMonitoring.SalesInvoiceCmp.AccountingPeriodicLine.BalHold = _locOrderCollectionLine.PickingMonitoring.SalesInvoiceCmp.AccountingPeriodicLine.BalHold - _locTotalDiscount;
                                                _locOrderCollectionLine.PickingMonitoring.SalesInvoiceCmp.AccountingPeriodicLine.Save();
                                                _locOrderCollectionLine.PickingMonitoring.SalesInvoiceCmp.AccountingPeriodicLine.Session.CommitTransaction();
                                            }
                                        }
                                        #endregion DiscountOnHeader
                                    }

                                }
                                #endregion BasedOnSalesInvoiceCmp
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = SalesInvoice ", ex.ToString());
            }
        }

        #endregion JournalInvoiceAR

        private void SetStatusOrderCollectionLine(Session _currSession, OrderCollection _locOrderCollectionXPO)
        {
            try
            {
                DateTime now = DateTime.Now;
                if (_locOrderCollectionXPO != null)
                {
                    XPCollection<OrderCollectionLine> _locOrderCollectionLines = new XPCollection<OrderCollectionLine>(_currSession,
                                                                        new GroupOperator(GroupOperatorType.And,
                                                                        new BinaryOperator("OrderCollection", _locOrderCollectionXPO),
                                                                        new BinaryOperator("Select", true),
                                                                        new BinaryOperator("CollectStatus", CollectStatus.Collect),
                                                                        new GroupOperator(GroupOperatorType.Or,
                                                                        new BinaryOperator("Status", Status.Progress),
                                                                        new BinaryOperator("Status", Status.Posted))));

                    if (_locOrderCollectionLines != null && _locOrderCollectionLines.Count() > 0)
                    {

                        foreach (OrderCollectionLine _locOrderCollectionLine in _locOrderCollectionLines)
                        {

                            _locOrderCollectionLine.Status = Status.Close;
                            _locOrderCollectionLine.ActivationPosting = true;
                            _locOrderCollectionLine.StatusDate = now;
                            _locOrderCollectionLine.Save();
                            _locOrderCollectionLine.Session.CommitTransaction();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = OrderCollection " + ex.ToString());
            }
        }

        private void SetPostedCount(Session _currSession, OrderCollection _locOrderCollectionXPO)
        {
            try
            {
                if (_locOrderCollectionXPO != null)
                {
                    XPCollection<OrderCollectionLine> _locOrderCollectionLines = new XPCollection<OrderCollectionLine>(_currSession,
                                                                        new GroupOperator(GroupOperatorType.And,
                                                                        new BinaryOperator("OrderCollection", _locOrderCollectionXPO),
                                                                        new BinaryOperator("Select", true),
                                                                        new BinaryOperator("CollectStatus", CollectStatus.Collect),
                                                                        new GroupOperator(GroupOperatorType.Or,
                                                                        new BinaryOperator("Status", Status.Progress),
                                                                        new BinaryOperator("Status", Status.Posted))));

                    if (_locOrderCollectionLines != null && _locOrderCollectionLines.Count() > 0)
                    {
                        foreach (OrderCollectionLine _locOrderCollectionLine in _locOrderCollectionLines)
                        {
                            _locOrderCollectionLine.PostedCount = _locOrderCollectionLine.PostedCount + 1;
                            _locOrderCollectionLine.Save();
                            _locOrderCollectionLine.Session.CommitTransaction();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = OrderCollection " + ex.ToString());
            }
        }

        private void SetNormal(Session _currSession, OrderCollection _locOrderCollectionXPO)
        {
            try
            {
                DateTime now = DateTime.Now;

                if (_locOrderCollectionXPO != null)
                {
                    XPCollection<OrderCollectionLine> _locOrderCollectionLines = new XPCollection<OrderCollectionLine>(_currSession,
                                                                                     new GroupOperator(GroupOperatorType.And,
                                                                                     new BinaryOperator("OrderCollection", _locOrderCollectionXPO),
                                                                                     new BinaryOperator("Select", true),
                                                                                     new BinaryOperator("CollectStatus", CollectStatus.Collect),
                                                                                     new GroupOperator(GroupOperatorType.Or,
                                                                                     new BinaryOperator("Status", Status.Progress),
                                                                                     new BinaryOperator("Status", Status.Posted))));

                    if (_locOrderCollectionLines != null && _locOrderCollectionLines.Count > 0)
                    {
                        foreach (OrderCollectionLine _locOrderCollectionLine in _locOrderCollectionLines)
                        {
                            _locOrderCollectionLine.Select = false;
                            _locOrderCollectionLine.Save();
                            _locOrderCollectionLine.Session.CommitTransaction();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = OrderCollection " + ex.ToString());
            }
        }

        private void SetFinalStatusOrderCollection(Session _currSession, OrderCollection _locOrderCollectionXPO)
        {
            try
            {
                DateTime now = DateTime.Now;
                int _locStatusCount = 0;
                int _locOrderCollLineCount = 0;

                if (_locOrderCollectionXPO != null)
                {
                    XPCollection<OrderCollectionLine> _locOrderCollectionLines = new XPCollection<OrderCollectionLine>(_currSession,
                                                            new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("OrderCollection", _locOrderCollectionXPO)));

                    if (_locOrderCollectionLines != null && _locOrderCollectionLines.Count() > 0)
                    {
                        _locOrderCollLineCount = _locOrderCollectionLines.Count();

                        foreach (OrderCollectionLine _locOrderCollectionLine in _locOrderCollectionLines)
                        {
                            if (_locOrderCollectionLine.Status == Status.Close)
                            {
                                _locStatusCount = _locStatusCount + 1;
                            }
                        }

                        if (_locStatusCount == _locOrderCollLineCount)
                        {
                            _locOrderCollectionXPO.ActivationPosting = true;
                            _locOrderCollectionXPO.Status = Status.Posted;
                            _locOrderCollectionXPO.StatusDate = now;
                            _locOrderCollectionXPO.Save();
                            _locOrderCollectionXPO.Session.CommitTransaction();
                        }
                        else
                        {
                            _locOrderCollectionXPO.Status = Status.Posted;
                            _locOrderCollectionXPO.StatusDate = now;
                            _locOrderCollectionXPO.Save();
                            _locOrderCollectionXPO.Session.CommitTransaction();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = OrderCollection " + ex.ToString());
            }
        }

        private void SetFinalPickingStatus(Session _currSession, OrderCollection _locOrderCollectionXPO)
        {
            try
            {
                DateTime now = DateTime.Now;
                int _locTotCountRouteInvoice = 0;
                int _locTotCountFalseRouteInvoice = 0;
                if (_locOrderCollectionXPO != null)
                {
                    if (_locOrderCollectionXPO.Picking != null)
                    {
                        XPCollection<PickingMonitoring> _locPickingMonitorings = new XPCollection<PickingMonitoring>(_currSession,
                                                                    new GroupOperator(GroupOperatorType.And,
                                                                    new BinaryOperator("Picking", _locOrderCollectionXPO.Picking)));
                        if (_locPickingMonitorings != null && _locPickingMonitorings.Count() > 0)
                        {
                            _locTotCountRouteInvoice = _locPickingMonitorings.Count();

                            foreach (PickingMonitoring _locPickingMonitoring in _locPickingMonitorings)
                            {
                                if (_locPickingMonitoring.AD == false && _locPickingMonitoring.OC == false && _locPickingMonitoring.Active == false)
                                {
                                    _locTotCountFalseRouteInvoice = _locTotCountFalseRouteInvoice + 1;
                                }
                                
                            }

                            if(_locTotCountRouteInvoice == _locTotCountFalseRouteInvoice)
                            {
                                _locOrderCollectionXPO.Picking.AD = false;
                                _locOrderCollectionXPO.Picking.OC = false;
                                _locOrderCollectionXPO.Picking.Active = false;
                                _locOrderCollectionXPO.Picking.Save();
                                _locOrderCollectionXPO.Picking.Session.CommitTransaction();
                            }  
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = OrderCollection " + ex.ToString());
            }
        }

        #endregion Posting

        #region Global Method

        private void SuccessMessageShow(string _locMessage)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 4000;
            options.Message = string.Format("{0}", _locMessage);
            options.Type = InformationType.Success;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Success";
            options.Win.Type = WinMessageType.Flyout;
            Application.ShowViewStrategy.ShowMessage(options);
        }

        private void ErrorMessageShow(string _locMessage)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 2000;
            options.Message = string.Format(_locMessage);
            options.Type = InformationType.Warning;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Error";
            options.Win.Type = WinMessageType.Alert;
            Application.ShowViewStrategy.ShowMessage(options);
        }



        #endregion Global Method

    }
}
