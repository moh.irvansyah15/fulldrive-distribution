﻿namespace FullDrive.Module.Controllers
{
    partial class InvoiceCanvassingMonitoringActionController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.InvoiceCanvassingMonitoringSelectAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.InvoiceCanvassingMonitoringUnselectAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            // 
            // InvoiceCanvassingMonitoringSelectAction
            // 
            this.InvoiceCanvassingMonitoringSelectAction.Caption = "Select";
            this.InvoiceCanvassingMonitoringSelectAction.ConfirmationMessage = null;
            this.InvoiceCanvassingMonitoringSelectAction.Id = "InvoiceCanvassingMonitoringSelectActionId";
            this.InvoiceCanvassingMonitoringSelectAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.InvoiceCanvassingMonitoring);
            this.InvoiceCanvassingMonitoringSelectAction.ToolTip = null;
            this.InvoiceCanvassingMonitoringSelectAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.InvoiceCanvassingMonitoringSelectAction_Execute);
            // 
            // InvoiceCanvassingMonitoringUnselectAction
            // 
            this.InvoiceCanvassingMonitoringUnselectAction.Caption = "Unselect";
            this.InvoiceCanvassingMonitoringUnselectAction.ConfirmationMessage = null;
            this.InvoiceCanvassingMonitoringUnselectAction.Id = "InvoiceCanvassingMonitoringUnselectActionId";
            this.InvoiceCanvassingMonitoringUnselectAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.InvoiceCanvassingMonitoring);
            this.InvoiceCanvassingMonitoringUnselectAction.ToolTip = null;
            this.InvoiceCanvassingMonitoringUnselectAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.InvoiceCanvassingMonitoringUnselectAction_Execute);
            // 
            // InvoiceCanvassingMonitoringActionController
            // 
            this.Actions.Add(this.InvoiceCanvassingMonitoringSelectAction);
            this.Actions.Add(this.InvoiceCanvassingMonitoringUnselectAction);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SimpleAction InvoiceCanvassingMonitoringSelectAction;
        private DevExpress.ExpressApp.Actions.SimpleAction InvoiceCanvassingMonitoringUnselectAction;
    }
}
