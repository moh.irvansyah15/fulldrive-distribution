﻿namespace FullDrive.Module.Controllers
{
    partial class CanvassingCollectionLineActionController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.CanvassingCollectionLineTaxAndDiscountAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            // 
            // CanvassingCollectionLineTaxAndDiscountAction
            // 
            this.CanvassingCollectionLineTaxAndDiscountAction.Caption = "Tax And Discount";
            this.CanvassingCollectionLineTaxAndDiscountAction.ConfirmationMessage = null;
            this.CanvassingCollectionLineTaxAndDiscountAction.Id = "CanvassingCollectionLineTaxAndDiscountActionId";
            this.CanvassingCollectionLineTaxAndDiscountAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.CanvassingCollectionLine);
            this.CanvassingCollectionLineTaxAndDiscountAction.ToolTip = null;
            this.CanvassingCollectionLineTaxAndDiscountAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.CanvassingCollectionLineTaxAndDiscountAction_Execute);
            // 
            // CanvassingCollectionLineActionController
            // 
            this.Actions.Add(this.CanvassingCollectionLineTaxAndDiscountAction);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SimpleAction CanvassingCollectionLineTaxAndDiscountAction;
    }
}
