﻿namespace FullDrive.Module.Controllers
{
    partial class AmendDeliveryActionController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.AmendDeliveryGetStockAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.AmendDeliverySetCollectionAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.AmendDeliveryProgressAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.AmendDeliveryListviewFilterSelectionAction = new DevExpress.ExpressApp.Actions.SingleChoiceAction(this.components);
            this.AmendDeliveryPostingAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            // 
            // AmendDeliveryGetStockAction
            // 
            this.AmendDeliveryGetStockAction.Caption = "Get Stock";
            this.AmendDeliveryGetStockAction.ConfirmationMessage = null;
            this.AmendDeliveryGetStockAction.Id = "AmendDeliveryGetStockActionId";
            this.AmendDeliveryGetStockAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.AmendDelivery);
            this.AmendDeliveryGetStockAction.ToolTip = null;
            this.AmendDeliveryGetStockAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.AmendDeliveryGetStockAction_Execute);
            // 
            // AmendDeliverySetCollectionAction
            // 
            this.AmendDeliverySetCollectionAction.Caption = "Set Collection";
            this.AmendDeliverySetCollectionAction.ConfirmationMessage = null;
            this.AmendDeliverySetCollectionAction.Id = "AmendDeliverySetCollectionActionId";
            this.AmendDeliverySetCollectionAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.AmendDelivery);
            this.AmendDeliverySetCollectionAction.ToolTip = null;
            this.AmendDeliverySetCollectionAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.AmendDeliverySetCollectionAction_Execute);
            // 
            // AmendDeliveryProgressAction
            // 
            this.AmendDeliveryProgressAction.Caption = "Progress";
            this.AmendDeliveryProgressAction.ConfirmationMessage = null;
            this.AmendDeliveryProgressAction.Id = "AmendDeliveryProgressActionId";
            this.AmendDeliveryProgressAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.AmendDelivery);
            this.AmendDeliveryProgressAction.ToolTip = null;
            this.AmendDeliveryProgressAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.AmendDeliveryProgressAction_Execute);
            // 
            // AmendDeliveryListviewFilterSelectionAction
            // 
            this.AmendDeliveryListviewFilterSelectionAction.Caption = "Filter";
            this.AmendDeliveryListviewFilterSelectionAction.ConfirmationMessage = null;
            this.AmendDeliveryListviewFilterSelectionAction.Id = "AmendDeliveryListviewFilterSelectionActionId";
            this.AmendDeliveryListviewFilterSelectionAction.ItemType = DevExpress.ExpressApp.Actions.SingleChoiceActionItemType.ItemIsOperation;
            this.AmendDeliveryListviewFilterSelectionAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.AmendDelivery);
            this.AmendDeliveryListviewFilterSelectionAction.ToolTip = null;
            this.AmendDeliveryListviewFilterSelectionAction.Execute += new DevExpress.ExpressApp.Actions.SingleChoiceActionExecuteEventHandler(this.AmendDeliveryListviewFilterSelectionAction_Execute);
            // 
            // AmendDeliveryPostingAction
            // 
            this.AmendDeliveryPostingAction.Caption = "Posting";
            this.AmendDeliveryPostingAction.ConfirmationMessage = null;
            this.AmendDeliveryPostingAction.Id = "AmendDeliveryPostingActionId";
            this.AmendDeliveryPostingAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.AmendDelivery);
            this.AmendDeliveryPostingAction.ToolTip = null;
            this.AmendDeliveryPostingAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.AmendDeliveryPostingAction_Execute);
            // 
            // AmendDeliveryActionController
            // 
            this.Actions.Add(this.AmendDeliveryGetStockAction);
            this.Actions.Add(this.AmendDeliverySetCollectionAction);
            this.Actions.Add(this.AmendDeliveryProgressAction);
            this.Actions.Add(this.AmendDeliveryListviewFilterSelectionAction);
            this.Actions.Add(this.AmendDeliveryPostingAction);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SimpleAction AmendDeliveryGetStockAction;
        private DevExpress.ExpressApp.Actions.SimpleAction AmendDeliverySetCollectionAction;
        private DevExpress.ExpressApp.Actions.SimpleAction AmendDeliveryProgressAction;
        private DevExpress.ExpressApp.Actions.SingleChoiceAction AmendDeliveryListviewFilterSelectionAction;
        private DevExpress.ExpressApp.Actions.SimpleAction AmendDeliveryPostingAction;
    }
}
