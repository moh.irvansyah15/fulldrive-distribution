﻿namespace FullDrive.Module.Controllers
{
    partial class PurchasePrePaymentInvoiceMonitoringActionController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.PurchasePrePaymentInvoiceMonitoringSelectAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.PurchasePrePaymentInvoiceMonitoringUnselectAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.PurchasePrePaymentInvoiceMonitoringCancelPayableAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            // 
            // PurchasePrePaymentInvoiceMonitoringSelectAction
            // 
            this.PurchasePrePaymentInvoiceMonitoringSelectAction.Caption = "Select";
            this.PurchasePrePaymentInvoiceMonitoringSelectAction.ConfirmationMessage = null;
            this.PurchasePrePaymentInvoiceMonitoringSelectAction.Id = "PurchasePrePaymentInvoiceMonitoringSelectActionId";
            this.PurchasePrePaymentInvoiceMonitoringSelectAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.PurchasePrePaymentInvoiceMonitoring);
            this.PurchasePrePaymentInvoiceMonitoringSelectAction.ToolTip = null;
            this.PurchasePrePaymentInvoiceMonitoringSelectAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.PurchasePrePaymentInvoiceMonitoringSelectAction_Execute);
            // 
            // PurchasePrePaymentInvoiceMonitoringUnselectAction
            // 
            this.PurchasePrePaymentInvoiceMonitoringUnselectAction.Caption = "Unselect";
            this.PurchasePrePaymentInvoiceMonitoringUnselectAction.ConfirmationMessage = null;
            this.PurchasePrePaymentInvoiceMonitoringUnselectAction.Id = "PurchasePrePaymentInvoiceMonitoringUnselectActionId";
            this.PurchasePrePaymentInvoiceMonitoringUnselectAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.PurchasePrePaymentInvoiceMonitoring);
            this.PurchasePrePaymentInvoiceMonitoringUnselectAction.ToolTip = null;
            this.PurchasePrePaymentInvoiceMonitoringUnselectAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.PurchasePrePaymentInvoiceMonitoringUnselectAction_Execute);
            // 
            // PurchasePrePaymentInvoiceMonitoringCancelPayableAction
            // 
            this.PurchasePrePaymentInvoiceMonitoringCancelPayableAction.Caption = "Cancel Payable";
            this.PurchasePrePaymentInvoiceMonitoringCancelPayableAction.ConfirmationMessage = null;
            this.PurchasePrePaymentInvoiceMonitoringCancelPayableAction.Id = "PurchasePrePaymentInvoiceMonitoringCancelPayableActionId";
            this.PurchasePrePaymentInvoiceMonitoringCancelPayableAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.PurchasePrePaymentInvoiceMonitoring);
            this.PurchasePrePaymentInvoiceMonitoringCancelPayableAction.ToolTip = null;
            this.PurchasePrePaymentInvoiceMonitoringCancelPayableAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.PurchasePrePaymentInvoiceMonitoringCancelPayableAction_Execute);
            // 
            // PurchasePrePaymentInvoiceMonitoringActionController
            // 
            this.Actions.Add(this.PurchasePrePaymentInvoiceMonitoringSelectAction);
            this.Actions.Add(this.PurchasePrePaymentInvoiceMonitoringUnselectAction);
            this.Actions.Add(this.PurchasePrePaymentInvoiceMonitoringCancelPayableAction);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SimpleAction PurchasePrePaymentInvoiceMonitoringSelectAction;
        private DevExpress.ExpressApp.Actions.SimpleAction PurchasePrePaymentInvoiceMonitoringUnselectAction;
        private DevExpress.ExpressApp.Actions.SimpleAction PurchasePrePaymentInvoiceMonitoringCancelPayableAction;
    }
}
