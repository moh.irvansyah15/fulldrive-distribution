﻿namespace FullDrive.Module.Controllers
{
    partial class CustomerInvoiceActionController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.CustomerInvoiceProgressAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.CustomerInvoiceSetFreeItemAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.CustomerInvoicePostingAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.CustomerInvoiceListviewFilterSelectionAction = new DevExpress.ExpressApp.Actions.SingleChoiceAction(this.components);
            // 
            // CustomerInvoiceProgressAction
            // 
            this.CustomerInvoiceProgressAction.Caption = "Progress";
            this.CustomerInvoiceProgressAction.ConfirmationMessage = null;
            this.CustomerInvoiceProgressAction.Id = "CustomerInvoiceProgressActionId";
            this.CustomerInvoiceProgressAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.CustomerInvoice);
            this.CustomerInvoiceProgressAction.ToolTip = null;
            this.CustomerInvoiceProgressAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.CustomerInvoiceProgressAction_Execute);
            // 
            // CustomerInvoiceSetFreeItemAction
            // 
            this.CustomerInvoiceSetFreeItemAction.Caption = "Set Free Item";
            this.CustomerInvoiceSetFreeItemAction.ConfirmationMessage = null;
            this.CustomerInvoiceSetFreeItemAction.Id = "CustomerInvoiceSetFreeItemActionId";
            this.CustomerInvoiceSetFreeItemAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.CustomerInvoice);
            this.CustomerInvoiceSetFreeItemAction.ToolTip = null;
            this.CustomerInvoiceSetFreeItemAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.CustomerInvoiceSetFreeItemAction_Execute);
            // 
            // CustomerInvoicePostingAction
            // 
            this.CustomerInvoicePostingAction.Caption = "Posting";
            this.CustomerInvoicePostingAction.ConfirmationMessage = null;
            this.CustomerInvoicePostingAction.Id = "CustomerInvoicePostingActionId";
            this.CustomerInvoicePostingAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.CustomerInvoice);
            this.CustomerInvoicePostingAction.ToolTip = null;
            this.CustomerInvoicePostingAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.CustomerInvoicePostingAction_Execute);
            // 
            // CustomerInvoiceListviewFilterSelectionAction
            // 
            this.CustomerInvoiceListviewFilterSelectionAction.Caption = "Filter";
            this.CustomerInvoiceListviewFilterSelectionAction.ConfirmationMessage = null;
            this.CustomerInvoiceListviewFilterSelectionAction.Id = "CustomerInvoiceListviewFilterSelectionActionId";
            this.CustomerInvoiceListviewFilterSelectionAction.ItemType = DevExpress.ExpressApp.Actions.SingleChoiceActionItemType.ItemIsOperation;
            this.CustomerInvoiceListviewFilterSelectionAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.CustomerInvoice);
            this.CustomerInvoiceListviewFilterSelectionAction.ToolTip = null;
            this.CustomerInvoiceListviewFilterSelectionAction.Execute += new DevExpress.ExpressApp.Actions.SingleChoiceActionExecuteEventHandler(this.CustomerInvoiceListviewFilterSelectionAction_Execute);
            // 
            // CustomerInvoiceActionController
            // 
            this.Actions.Add(this.CustomerInvoiceProgressAction);
            this.Actions.Add(this.CustomerInvoiceSetFreeItemAction);
            this.Actions.Add(this.CustomerInvoicePostingAction);
            this.Actions.Add(this.CustomerInvoiceListviewFilterSelectionAction);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SimpleAction CustomerInvoiceProgressAction;
        private DevExpress.ExpressApp.Actions.SimpleAction CustomerInvoiceSetFreeItemAction;
        private DevExpress.ExpressApp.Actions.SimpleAction CustomerInvoicePostingAction;
        private DevExpress.ExpressApp.Actions.SingleChoiceAction CustomerInvoiceListviewFilterSelectionAction;
    }
}
