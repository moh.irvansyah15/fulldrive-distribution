﻿namespace FullDrive.Module.Controllers
{
    partial class InventoryPurchaseCollectionActionController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.InventoryPurchaseCollectionShowPOMAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            // 
            // InventoryPurchaseCollectionShowPOMAction
            // 
            this.InventoryPurchaseCollectionShowPOMAction.Caption = "Show POM";
            this.InventoryPurchaseCollectionShowPOMAction.ConfirmationMessage = null;
            this.InventoryPurchaseCollectionShowPOMAction.Id = "InventoryPurchaseCollectionShowPOMActionId";
            this.InventoryPurchaseCollectionShowPOMAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.InventoryPurchaseCollection);
            this.InventoryPurchaseCollectionShowPOMAction.ToolTip = null;
            this.InventoryPurchaseCollectionShowPOMAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.InventoryPurchaseCollectionShowPOMAction_Execute);
            // 
            // InventoryPurchaseCollectionActionController
            // 
            this.Actions.Add(this.InventoryPurchaseCollectionShowPOMAction);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SimpleAction InventoryPurchaseCollectionShowPOMAction;
    }
}
