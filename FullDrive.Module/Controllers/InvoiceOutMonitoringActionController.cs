﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;

using FullDrive.Module.BusinessObjects;
using FullDrive.Module.CustomProcess;
using System.Collections;
using DevExpress.Xpo;
using DevExpress.ExpressApp.Xpo;
using System.IO;
using System.Web;

namespace FullDrive.Module.Controllers
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class InvoiceOutMonitoringActionController : ViewController
    {
        public InvoiceOutMonitoringActionController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.
        }
        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }
        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        private void InvoiceOutMonitoringImportAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;

                if (_objectsToProcess != null)
                {
                    foreach (Object obj in _objectsToProcess)
                    {
                        InvoiceOutMonitoring _locInvoiceOutMonitoringOS = (InvoiceOutMonitoring)_objectSpace.GetObject(obj);

                        if (_locInvoiceOutMonitoringOS != null)
                        {
                            if (_locInvoiceOutMonitoringOS.Session != null)
                            {
                                _currSession = _locInvoiceOutMonitoringOS.Session;
                            }

                            if (_locInvoiceOutMonitoringOS.Code != null && _currSession != null)
                            {
                                _currObjectId = _locInvoiceOutMonitoringOS.Code;

                                InvoiceOutMonitoring _locInvoiceOutMonitoringXPO = _currSession.FindObject<InvoiceOutMonitoring>
                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                     new BinaryOperator("Code", _currObjectId)));

                                if (_locInvoiceOutMonitoringXPO != null)
                                {
                                    string targetpath = null;
                                    if (_locInvoiceOutMonitoringXPO.DataImport != null && _locInvoiceOutMonitoringXPO.Message != "File is already available in server")
                                    {
                                        targetpath = HttpContext.Current.Server.MapPath("~/UploadFile/" + _locInvoiceOutMonitoringXPO.DataImport.FileName);
                                        string ext = System.IO.Path.GetExtension(_locInvoiceOutMonitoringXPO.DataImport.FileName);
                                        FileStream fileStream = new FileStream(targetpath, FileMode.OpenOrCreate);
                                        _locInvoiceOutMonitoringXPO.DataImport.SaveToStream(fileStream);
                                        fileStream.Close();
                                        if (File.Exists(targetpath))
                                        {
                                            _globFunc.ImportDataExcelForInvoiceOutMonitoring(_currSession, targetpath, ext, _locInvoiceOutMonitoringXPO.ObjectList, 
                                                _locInvoiceOutMonitoringXPO.Company, _locInvoiceOutMonitoringXPO.Workplace);
                                        }
                                        _locInvoiceOutMonitoringXPO.DataImport.Clear();
                                        SuccessMessageShow(_locInvoiceOutMonitoringXPO.Code + " has been change successfully to Import Data");
                                    }

                                }
                                else
                                {
                                    ErrorMessageShow("Account Reclass Journal Transaction Not Available");
                                }

                            }
                            else
                            {
                                ErrorMessageShow("Account Reclass Journal Transaction Not Available");
                            }
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = InvoiceOutMonitoring " + ex.ToString());
            }
        }

        private void InvoiceOutMonitoringGetDataAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;

                if (_objectsToProcess != null)
                {
                    foreach (Object obj in _objectsToProcess)
                    {
                        InvoiceOutMonitoring _locInvoiceOutMonitoringOS = (InvoiceOutMonitoring)_objectSpace.GetObject(obj);

                        if (_locInvoiceOutMonitoringOS != null)
                        {
                            if (_locInvoiceOutMonitoringOS.Session != null)
                            {
                                _currSession = _locInvoiceOutMonitoringOS.Session;
                            }

                            if (_locInvoiceOutMonitoringOS.Code != null && _currSession != null)
                            {
                                _currObjectId = _locInvoiceOutMonitoringOS.Code;

                                InvoiceOutMonitoring _locInvoiceOutMonitoringXPO = _currSession.FindObject<InvoiceOutMonitoring>
                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                     new BinaryOperator("Code", _currObjectId)));

                                if (_locInvoiceOutMonitoringXPO != null)
                                {
                                    SetHeader1(_currSession, _locInvoiceOutMonitoringXPO);
                                    SetHeader2(_currSession, _locInvoiceOutMonitoringXPO);
                                    SetHeader3(_currSession, _locInvoiceOutMonitoringXPO);
                                    if(_locInvoiceOutMonitoringXPO.ObjectImport == ObjectImport.SalesInvoice)
                                    {
                                        SetContain1(_currSession, _locInvoiceOutMonitoringXPO);
                                    }
                                    if (_locInvoiceOutMonitoringXPO.ObjectImport == ObjectImport.CustomerInvoice)
                                    {
                                        SetContain2(_currSession, _locInvoiceOutMonitoringXPO);
                                    }
                                    SuccessMessageShow(_locInvoiceOutMonitoringXPO.Code + " has been change successfully to Get Data");

                                }
                                else
                                {
                                    ErrorMessageShow("Account Reclass Journal Transaction Not Available");
                                }

                            }
                            else
                            {
                                ErrorMessageShow("Account Reclass Journal Transaction Not Available");
                            }
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = InvoiceOutMonitoring " + ex.ToString());
            }
        }

        //============================================== Code In Here ==============================================

        private void SetHeader1(Session _currSession, InvoiceOutMonitoring _locInvoiceOutMonitoringXPO)
        {
            try
            {
                DateTime now = DateTime.Now;
                if (_locInvoiceOutMonitoringXPO != null)
                {
                    if(_locInvoiceOutMonitoringXPO.Company != null && _locInvoiceOutMonitoringXPO.Workplace != null)
                    {
                        InvoiceOutMonitoringLine _saveDataInvoiceOutMonitoringLine = new InvoiceOutMonitoringLine(_currSession)
                        {
                            Company = _locInvoiceOutMonitoringXPO.Company,
                            Workplace = _locInvoiceOutMonitoringXPO.Workplace,
                            StartDate = _locInvoiceOutMonitoringXPO.StartDate,
                            EndDate = _locInvoiceOutMonitoringXPO.EndDate,
                            Column1 = "FK",
                            Column2 = "KD_JENIS_TRANSAKSI",
                            Column3 = "FG_PENGGANTI",
                            Column4 = "NOMOR_FAKTUR",
                            Column5 = "MASA_PAJAK",
                            Column6 = "TAHUN_PAJAK",
                            Column7 = "TANGGAL_FAKTUR",
                            Column8 = "NPWP",
                            Column9 = "NAMA",
                            Column10 = "ALAMAT_LENGKAP",
                            Column11 = "JUMLAH_DPP",
                            Column12 = "JUMLAH_PPN",
                            Column13 = "JUMLAH_PPNBM",
                            Column14 = "ID_KETERANGAN_TAMBAHAN",
                            Column15 = "FG_UANG_MUKA",
                            Column16 = "UANG_MUKA_DPP",
                            Column17 = "UANG_MUKA_PPN",
                            Column18 = "UANG_MUKA_PPNBM",
                            Column19 = "REFERENSI",
                            InvoiceOutMonitoring = _locInvoiceOutMonitoringXPO,
                        };
                        _saveDataInvoiceOutMonitoringLine.Save();
                        _saveDataInvoiceOutMonitoringLine.Session.CommitTransaction();
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = InvoiceOutMonitoring " + ex.ToString());
            }
        }

        private void SetHeader2(Session _currSession, InvoiceOutMonitoring _locInvoiceOutMonitoringXPO)
        {
            try
            {
                DateTime now = DateTime.Now;
                if (_locInvoiceOutMonitoringXPO != null)
                {
                    if (_locInvoiceOutMonitoringXPO.Company != null && _locInvoiceOutMonitoringXPO.Workplace != null)
                    {
                        InvoiceOutMonitoringLine _saveDataInvoiceOutMonitoringLine = new InvoiceOutMonitoringLine(_currSession)
                        {
                            Company = _locInvoiceOutMonitoringXPO.Company,
                            Workplace = _locInvoiceOutMonitoringXPO.Workplace,
                            StartDate = _locInvoiceOutMonitoringXPO.StartDate,
                            EndDate = _locInvoiceOutMonitoringXPO.EndDate,
                            Column1 = "LT",
                            Column2 = "NPWP",
                            Column3 = "NAMA",
                            Column4 = "JALAN",
                            Column5 = "BLOK",
                            Column6 = "NOMOR",
                            Column7 = "RT",
                            Column8 = "RW",
                            Column9 = "KECAMATAN",
                            Column10 = "KELURAHAN",
                            Column11 = "KABUPATEN",
                            Column12 = "PROPINSI",
                            Column13 = "KODE_POS",
                            Column14 = "NOMOR_TELEPON",
                            Column15 = "",
                            Column16 = "",
                            Column17 = "",
                            Column18 = "",
                            Column19 = "",
                            InvoiceOutMonitoring = _locInvoiceOutMonitoringXPO,
                        };
                        _saveDataInvoiceOutMonitoringLine.Save();
                        _saveDataInvoiceOutMonitoringLine.Session.CommitTransaction();
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = InvoiceOutMonitoring " + ex.ToString());
            }
        }

        private void SetHeader3(Session _currSession, InvoiceOutMonitoring _locInvoiceOutMonitoringXPO)
        {
            try
            {
                DateTime now = DateTime.Now;
                if (_locInvoiceOutMonitoringXPO != null)
                {
                    if (_locInvoiceOutMonitoringXPO.Company != null && _locInvoiceOutMonitoringXPO.Workplace != null)
                    {
                        InvoiceOutMonitoringLine _saveDataInvoiceOutMonitoringLine = new InvoiceOutMonitoringLine(_currSession)
                        {
                            Company = _locInvoiceOutMonitoringXPO.Company,
                            Workplace = _locInvoiceOutMonitoringXPO.Workplace,
                            StartDate = _locInvoiceOutMonitoringXPO.StartDate,
                            EndDate = _locInvoiceOutMonitoringXPO.EndDate,
                            Column1 = "OF",
                            Column2 = "KODE_OBJEK",
                            Column3 = "NAMA",
                            Column4 = "HARGA_SATUAN",
                            Column5 = "JUMLAH_BARANG",
                            Column6 = "HARGA_TOTAL",
                            Column7 = "DISKON",
                            Column8 = "DPP",
                            Column9 = "PPN",
                            Column10 = "TARIF_PPNBM",
                            Column11 = "PPNBM",
                            Column12 = "PROPINSI",
                            Column13 = "",
                            Column14 = "",
                            Column15 = "",
                            Column16 = "",
                            Column17 = "",
                            Column18 = "",
                            Column19 = "",
                            InvoiceOutMonitoring = _locInvoiceOutMonitoringXPO,
                        };
                        _saveDataInvoiceOutMonitoringLine.Save();
                        _saveDataInvoiceOutMonitoringLine.Session.CommitTransaction();
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = InvoiceOutMonitoring " + ex.ToString());
            }
        }

        private void SetContain1(Session _currSession, InvoiceOutMonitoring _locInvoiceOutMonitoringXPO)
        {
            try
            {
                DateTime now = DateTime.Now;
                if (_locInvoiceOutMonitoringXPO.Company != null && _locInvoiceOutMonitoringXPO.Workplace != null)
                {
                    if (_locInvoiceOutMonitoringXPO.Company != null)
                    {
                        XPCollection<SalesInvoice> _locSalesInvoices = new XPCollection<SalesInvoice>(_currSession, 
                                                                    new GroupOperator(GroupOperatorType.And,
                                                                    new BinaryOperator("DocDate", _locInvoiceOutMonitoringXPO.StartDate.AddDays(-1), BinaryOperatorType.Greater),
                                                                    new BinaryOperator("DocDate", _locInvoiceOutMonitoringXPO.EndDate.AddDays(1), BinaryOperatorType.Less)));
                        if(_locSalesInvoices != null && _locSalesInvoices.Count() > 0)
                        {
                            foreach (SalesInvoice _locSalesInvoice in _locSalesInvoices)
                            {
                                #region Customer
                                if(_locSalesInvoice.SalesToCustomer != null)
                                {
                                    InvoiceOutMonitoringLine _saveDataInvoiceOutMonitoringLine = new InvoiceOutMonitoringLine(_currSession)
                                    {
                                        Company = _locInvoiceOutMonitoringXPO.Company,
                                        Workplace = _locInvoiceOutMonitoringXPO.Workplace,
                                        StartDate = _locInvoiceOutMonitoringXPO.StartDate,
                                        EndDate = _locInvoiceOutMonitoringXPO.EndDate,
                                        Column1 = "FK",
                                        Column2 = "1",
                                        Column3 = "0",
                                        Column4 = _locSalesInvoice.DJP_No,
                                        Column5 = _locSalesInvoice.DocDate.Month.ToString(),
                                        Column6 = _locSalesInvoice.DocDate.Year.ToString(),
                                        Column7 = _locSalesInvoice.DocDate.ToShortDateString(),
                                        Column8 = _locSalesInvoice.SalesToCustomer.TaxNo,
                                        Column9 = _locSalesInvoice.SalesToCustomer.TaxName,
                                        Column10 = _locSalesInvoice.SalesToAddress,
                                        Column11 = _locSalesInvoice.Amount.ToString(),
                                        Column12 = _locSalesInvoice.TotTaxAmount.ToString(),
                                        Column13 = "0",
                                        Column14 = "",
                                        Column15 = "0",
                                        Column16 = "0",
                                        Column17 = "0",
                                        Column18 = "0",
                                        Column19 = "",
                                        InvoiceOutMonitoring = _locInvoiceOutMonitoringXPO,
                                    };
                                    _saveDataInvoiceOutMonitoringLine.Save();
                                    _saveDataInvoiceOutMonitoringLine.Session.CommitTransaction();
                                }
                                #endregion Customer

                                #region Company
                                if (_locSalesInvoice.Company != null)
                                {
                                    InvoiceOutMonitoringLine _saveDataInvoiceOutMonitoringLine = new InvoiceOutMonitoringLine(_currSession)
                                    {
                                        Company = _locInvoiceOutMonitoringXPO.Company,
                                        Workplace = _locInvoiceOutMonitoringXPO.Workplace,
                                        StartDate = _locInvoiceOutMonitoringXPO.StartDate,
                                        EndDate = _locInvoiceOutMonitoringXPO.EndDate,
                                        Column1 = "FAPR",
                                        Column2 = _locSalesInvoice.Company.TaxName,
                                        Column3 = _locSalesInvoice.Company.Address,
                                        Column4 = "",
                                        Column5 = "",
                                        Column6 = "",
                                        Column7 = "",
                                        Column8 = "",
                                        Column9 = "",
                                        Column10 = "",
                                        Column11 = "",
                                        Column12 = "",
                                        Column13 = "",
                                        Column14 = "",
                                        Column15 = "",
                                        Column16 = "",
                                        Column17 = "",
                                        Column18 = "",
                                        Column19 = "",
                                        InvoiceOutMonitoring = _locInvoiceOutMonitoringXPO,
                                    };
                                    _saveDataInvoiceOutMonitoringLine.Save();
                                    _saveDataInvoiceOutMonitoringLine.Session.CommitTransaction();
                                }
                                #endregion Company

                                #region SalesInvoiceLine
                                XPCollection<SalesInvoiceLine> _locSalesInvoiceLines = new XPCollection<SalesInvoiceLine>(_currSession,
                                                                                    new GroupOperator(GroupOperatorType.And,
                                                                                    new BinaryOperator("SalesInvoice", _locInvoiceOutMonitoringXPO)));
                                if(_locSalesInvoiceLines != null && _locSalesInvoiceLines.Count() > 0)
                                {
                                    foreach(SalesInvoiceLine _locSalesInvoiceLine in _locSalesInvoiceLines)
                                    {
                                        if(_locSalesInvoiceLine.Item != null)
                                        {
                                            InvoiceOutMonitoringLine _saveDataInvoiceOutMonitoringLine = new InvoiceOutMonitoringLine(_currSession)
                                            {
                                                Company = _locInvoiceOutMonitoringXPO.Company,
                                                Workplace = _locInvoiceOutMonitoringXPO.Workplace,
                                                StartDate = _locInvoiceOutMonitoringXPO.StartDate,
                                                EndDate = _locInvoiceOutMonitoringXPO.EndDate,
                                                Column1 = "OF",
                                                Column2 = _locSalesInvoiceLine.Item.Code,
                                                Column3 = _locSalesInvoiceLine.Item.Name,
                                                Column4 = _locSalesInvoiceLine.UAmount.ToString(),
                                                Column5 = _locSalesInvoiceLine.TQty.ToString(),
                                                Column6 = _locSalesInvoiceLine.TUAmount.ToString(),
                                                Column7 = _locSalesInvoiceLine.DiscAmount.ToString(),
                                                Column8 = _locSalesInvoiceLine.TAmount.ToString(),
                                                Column9 = _locSalesInvoiceLine.TxAmount.ToString(),
                                                Column10 = "0",
                                                Column11 = "0",
                                                Column12 = "",
                                                Column13 = "",
                                                Column14 = "",
                                                Column15 = "",
                                                Column16 = "",
                                                Column17 = "",
                                                Column18 = "",
                                                Column19 = "",
                                                InvoiceOutMonitoring = _locInvoiceOutMonitoringXPO,
                                            };
                                            _saveDataInvoiceOutMonitoringLine.Save();
                                            _saveDataInvoiceOutMonitoringLine.Session.CommitTransaction();
                                        }
                                    }
                                }
                                #endregion SalesInvoiceLine
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = InvoiceOutMonitoring " + ex.ToString());
            }
        }

        private void SetContain2(Session _currSession, InvoiceOutMonitoring _locInvoiceOutMonitoringXPO)
        {
            try
            {
                DateTime now = DateTime.Now;
                if (_locInvoiceOutMonitoringXPO.Company != null && _locInvoiceOutMonitoringXPO.Workplace != null)
                {
                    if (_locInvoiceOutMonitoringXPO.Company != null)
                    {
                        XPCollection<CustomerInvoice> _locCustomerInvoices = new XPCollection<CustomerInvoice>(_currSession,
                                                                    new GroupOperator(GroupOperatorType.And,
                                                                    new BinaryOperator("DocDate", _locInvoiceOutMonitoringXPO.StartDate.AddDays(-1), BinaryOperatorType.Greater),
                                                                    new BinaryOperator("DocDate", _locInvoiceOutMonitoringXPO.EndDate.AddDays(1), BinaryOperatorType.Less)));
                        if (_locCustomerInvoices != null && _locCustomerInvoices.Count() > 0)
                        {
                            foreach (CustomerInvoice _locCustomerInvoice in _locCustomerInvoices)
                            {
                                #region Customer
                                if (_locCustomerInvoice.SalesToCustomer != null)
                                {
                                    InvoiceOutMonitoringLine _saveDataInvoiceOutMonitoringLine = new InvoiceOutMonitoringLine(_currSession)
                                    {
                                        Company = _locInvoiceOutMonitoringXPO.Company,
                                        Workplace = _locInvoiceOutMonitoringXPO.Workplace,
                                        StartDate = _locInvoiceOutMonitoringXPO.StartDate,
                                        EndDate = _locInvoiceOutMonitoringXPO.EndDate,
                                        Column1 = "FK",
                                        Column2 = "1",
                                        Column3 = "0",
                                        Column4 = _locCustomerInvoice.DJP_No,
                                        Column5 = _locCustomerInvoice.DocDate.Month.ToString(),
                                        Column6 = _locCustomerInvoice.DocDate.Year.ToString(),
                                        Column7 = _locCustomerInvoice.DocDate.ToShortDateString(),
                                        Column8 = _locCustomerInvoice.SalesToCustomer.TaxNo,
                                        Column9 = _locCustomerInvoice.SalesToCustomer.TaxName,
                                        Column10 = _locCustomerInvoice.SalesToAddress,
                                        Column11 = _locCustomerInvoice.Amount.ToString(),
                                        Column12 = _locCustomerInvoice.TotTaxAmount.ToString(),
                                        Column13 = "0",
                                        Column14 = "",
                                        Column15 = "0",
                                        Column16 = "0",
                                        Column17 = "0",
                                        Column18 = "0",
                                        Column19 = "",
                                        InvoiceOutMonitoring = _locInvoiceOutMonitoringXPO,
                                    };
                                    _saveDataInvoiceOutMonitoringLine.Save();
                                    _saveDataInvoiceOutMonitoringLine.Session.CommitTransaction();
                                }
                                #endregion Customer

                                #region Company
                                if (_locCustomerInvoice.Company != null)
                                {
                                    InvoiceOutMonitoringLine _saveDataInvoiceOutMonitoringLine = new InvoiceOutMonitoringLine(_currSession)
                                    {
                                        Company = _locInvoiceOutMonitoringXPO.Company,
                                        Workplace = _locInvoiceOutMonitoringXPO.Workplace,
                                        StartDate = _locInvoiceOutMonitoringXPO.StartDate,
                                        EndDate = _locInvoiceOutMonitoringXPO.EndDate,
                                        Column1 = "FAPR",
                                        Column2 = _locCustomerInvoice.Company.TaxName,
                                        Column3 = _locCustomerInvoice.Company.Address,
                                        Column4 = "",
                                        Column5 = "",
                                        Column6 = "",
                                        Column7 = "",
                                        Column8 = "",
                                        Column9 = "",
                                        Column10 = "",
                                        Column11 = "",
                                        Column12 = "",
                                        Column13 = "",
                                        Column14 = "",
                                        Column15 = "",
                                        Column16 = "",
                                        Column17 = "",
                                        Column18 = "",
                                        Column19 = "",
                                        InvoiceOutMonitoring = _locInvoiceOutMonitoringXPO,
                                    };
                                    _saveDataInvoiceOutMonitoringLine.Save();
                                    _saveDataInvoiceOutMonitoringLine.Session.CommitTransaction();
                                }
                                #endregion Company

                                #region SalesInvoiceLine
                                XPCollection<CustomerInvoiceLine> _locCustomerInvoiceLines = new XPCollection<CustomerInvoiceLine>(_currSession,
                                                                                    new GroupOperator(GroupOperatorType.And,
                                                                                    new BinaryOperator("CustomerInvoice", _locInvoiceOutMonitoringXPO)));
                                if (_locCustomerInvoiceLines != null && _locCustomerInvoiceLines.Count() > 0)
                                {
                                    foreach (CustomerInvoiceLine _locCustomerInvoiceLine in _locCustomerInvoiceLines)
                                    {
                                        if (_locCustomerInvoiceLine.Item != null)
                                        {
                                            InvoiceOutMonitoringLine _saveDataInvoiceOutMonitoringLine = new InvoiceOutMonitoringLine(_currSession)
                                            {
                                                Company = _locInvoiceOutMonitoringXPO.Company,
                                                Workplace = _locInvoiceOutMonitoringXPO.Workplace,
                                                StartDate = _locInvoiceOutMonitoringXPO.StartDate,
                                                EndDate = _locInvoiceOutMonitoringXPO.EndDate,
                                                Column1 = "OF",
                                                Column2 = _locCustomerInvoiceLine.Item.Code,
                                                Column3 = _locCustomerInvoiceLine.Item.Name,
                                                Column4 = _locCustomerInvoiceLine.UAmount.ToString(),
                                                Column5 = _locCustomerInvoiceLine.TQty.ToString(),
                                                Column6 = _locCustomerInvoiceLine.TUAmount.ToString(),
                                                Column7 = _locCustomerInvoiceLine.DiscAmount.ToString(),
                                                Column8 = _locCustomerInvoiceLine.TAmount.ToString(),
                                                Column9 = _locCustomerInvoiceLine.TxAmount.ToString(),
                                                Column10 = "0",
                                                Column11 = "0",
                                                Column12 = "",
                                                Column13 = "",
                                                Column14 = "",
                                                Column15 = "",
                                                Column16 = "",
                                                Column17 = "",
                                                Column18 = "",
                                                Column19 = "",
                                                InvoiceOutMonitoring = _locInvoiceOutMonitoringXPO,
                                            };
                                            _saveDataInvoiceOutMonitoringLine.Save();
                                            _saveDataInvoiceOutMonitoringLine.Session.CommitTransaction();
                                        }
                                    }
                                }
                                #endregion SalesInvoiceLine
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = InvoiceOutMonitoring " + ex.ToString());
            }
        }

        #region Global Method

        private void SuccessMessageShow(string _locMessage)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 4000;
            options.Message = string.Format("{0}", _locMessage);
            options.Type = InformationType.Success;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Success";
            options.Win.Type = WinMessageType.Flyout;
            Application.ShowViewStrategy.ShowMessage(options);
        }

        private void ErrorMessageShow(string _locMessage)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 2000;
            options.Message = string.Format(_locMessage);
            options.Type = InformationType.Warning;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Error";
            options.Win.Type = WinMessageType.Alert;
            Application.ShowViewStrategy.ShowMessage(options);
        }

        #endregion Global Method

        
    }
}
