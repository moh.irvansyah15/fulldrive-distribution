﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;

using FullDrive.Module.BusinessObjects;
using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.Xpo;
using System.Collections;
using DevExpress.Xpo;

namespace FullDrive.Module.Controllers
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class CanvassingCollectionLineActionController : ViewController
    {
        public CanvassingCollectionLineActionController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.
        }
        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }
        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        private void CanvassingCollectionLineTaxAndDiscountAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;

                if (_objectToProcess != null)
                {
                    foreach (Object obj in _objectToProcess)
                    {
                        CanvassingCollectionLine _locCanvassingCollectionLineOS = (CanvassingCollectionLine)_objectSpace.GetObject(obj);

                        if (_locCanvassingCollectionLineOS != null)
                        {
                            if (_locCanvassingCollectionLineOS.Session != null)
                            {
                                _currSession = _locCanvassingCollectionLineOS.Session;
                            }

                            if (_locCanvassingCollectionLineOS.Code != null && _currSession != null)
                            {
                                _currObjectId = _locCanvassingCollectionLineOS.Code;

                                XPCollection<CanvassingCollectionLine> _locCanvassingCollectionLines = new XPCollection<CanvassingCollectionLine>
                                                                                                     (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                      new BinaryOperator("Code", _currObjectId),
                                                                                                      new GroupOperator(GroupOperatorType.Or,
                                                                                                      new BinaryOperator("Status", Status.Progress),
                                                                                                      new BinaryOperator("Status", Status.Open)
                                                                                                      )));

                                if (_locCanvassingCollectionLines != null && _locCanvassingCollectionLines.Count > 0)
                                {
                                    foreach (CanvassingCollectionLine _locCanvassingCollectionLine in _locCanvassingCollectionLines)
                                    {
                                        SetTaxBeforeDiscount(_currSession, _locCanvassingCollectionLine);
                                        SetDiscountGross(_currSession, _locCanvassingCollectionLine);
                                        SetTaxAfterDiscount(_currSession, _locCanvassingCollectionLine);
                                        SetDiscountNet(_currSession, _locCanvassingCollectionLine);
                                    }

                                    SuccessMessageShow("CanvassingCollectionLine Line has been Processed");
                                }
                                else
                                {
                                    ErrorMessageShow("Data CanvassingCollectionLine Line Not Available");
                                }
                            }
                        }
                    }
                    if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                    {
                        _objectSpace.CommitChanges();
                        _objectSpace.Refresh();
                    }
                    if (View is ListView)
                    {
                        _objectSpace.CommitChanges();
                        View.ObjectSpace.Refresh();
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("BusinessObject = CanvassingCollectionLine" + ex.ToString());
            }
        }

        //======================================== Code In Here ========================================

        #region Tax

        private void SetTaxBeforeDiscount(Session _currSession, CanvassingCollectionLine _locCanvassingCollectionLineXPO)
        {
            try
            {
                double _locTxAmount = 0;
                double _locTAmount = 0;
                if (_locCanvassingCollectionLineXPO != null)
                {
                    if (_locCanvassingCollectionLineXPO.Tax != null)
                    {
                        if (_locCanvassingCollectionLineXPO.Tax.TaxRule == TaxRule.BeforeDiscount)
                        {
                            _locTxAmount = _locCanvassingCollectionLineXPO.TxValue / 100 * _locCanvassingCollectionLineXPO.TUAmount;
                            if (_locCanvassingCollectionLineXPO.Tax.TaxNature == TaxNature.Increase)
                            {
                                _locTAmount = _locCanvassingCollectionLineXPO.TAmount + _locTxAmount;
                            }
                            if (_locCanvassingCollectionLineXPO.Tax.TaxNature == TaxNature.Decrease)
                            {
                                _locTAmount = _locCanvassingCollectionLineXPO.TAmount - _locTxAmount;
                            }
                            _locCanvassingCollectionLineXPO.TxAmount = _locTxAmount;
                            _locCanvassingCollectionLineXPO.TAmount = _locTAmount;
                            _locCanvassingCollectionLineXPO.Save();
                            _locCanvassingCollectionLineXPO.Session.CommitTransaction();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("BusinessObject = CanvassingCollectionLine" + ex.ToString());
            }
        }

        private void SetDiscountGross(Session _currSession, CanvassingCollectionLine _locCanvassingCollectionLineXPO)
        {
            try
            {
                if (_locCanvassingCollectionLineXPO != null)
                {
                    if (_locCanvassingCollectionLineXPO.DiscountRule != null)
                    {
                        double _locTUAmountDiscount = 0;

                        #region Gross

                        if (_locCanvassingCollectionLineXPO.DiscountRule.GrossAmountRule == true)
                        {
                            if (_locCanvassingCollectionLineXPO.TUAmount > 0 && _locCanvassingCollectionLineXPO.TUAmount >= _locCanvassingCollectionLineXPO.DiscountRule.GrossTotalUAmount)
                            {
                                if (_locCanvassingCollectionLineXPO.DiscountRule.DiscountType == DiscountType.Percentage)
                                {
                                    _locTUAmountDiscount = _locCanvassingCollectionLineXPO.TUAmount * _locCanvassingCollectionLineXPO.DiscountRule.Value / 100;
                                }
                                if (_locCanvassingCollectionLineXPO.DiscountRule.DiscountType == DiscountType.Value)
                                {
                                    _locTUAmountDiscount = _locCanvassingCollectionLineXPO.DiscountRule.Value;
                                }

                                _locCanvassingCollectionLineXPO.DiscountChecked = true;
                                _locCanvassingCollectionLineXPO.DiscAmount = _locTUAmountDiscount;
                                _locCanvassingCollectionLineXPO.TAmount = _locCanvassingCollectionLineXPO.TAmount - _locTUAmountDiscount;
                                _locCanvassingCollectionLineXPO.Save();
                                _locCanvassingCollectionLineXPO.Session.CommitTransaction();
                            }

                        }

                        #endregion Gross
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("BusinessObject = CanvassingCollectionLine" + ex.ToString());
            }
        }

        private void SetTaxAfterDiscount(Session _currSession, CanvassingCollectionLine _locCanvassingCollectionLineXPO)
        {
            try
            {
                double _locTxAmount = 0;
                double _locTAmount = 0;
                if (_locCanvassingCollectionLineXPO != null)
                {
                    if (_locCanvassingCollectionLineXPO.Tax != null && _locCanvassingCollectionLineXPO.DiscountRule != null)
                    {
                        if (_locCanvassingCollectionLineXPO.Tax.TaxRule == TaxRule.AfterDiscount && _locCanvassingCollectionLineXPO.DiscountRule.GrossAmountRule == true)
                        {
                            _locTxAmount = _locCanvassingCollectionLineXPO.TxValue / 100 * (_locCanvassingCollectionLineXPO.TUAmount - _locCanvassingCollectionLineXPO.DiscAmount);

                            if (_locCanvassingCollectionLineXPO.Tax.TaxNature == TaxNature.Increase)
                            {
                                _locTAmount = _locCanvassingCollectionLineXPO.TAmount + _locTxAmount;
                            }
                            if (_locCanvassingCollectionLineXPO.Tax.TaxNature == TaxNature.Decrease)
                            {
                                _locTAmount = _locCanvassingCollectionLineXPO.TAmount - _locTxAmount;
                            }
                            _locCanvassingCollectionLineXPO.TxAmount = _locTxAmount;
                            _locCanvassingCollectionLineXPO.TAmount = _locTAmount;
                            _locCanvassingCollectionLineXPO.Save();
                            _locCanvassingCollectionLineXPO.Session.CommitTransaction();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("BusinessObject = CanvassingCollectionLine" + ex.ToString());
            }
        }

        private void SetDiscountNet(Session _currSession, CanvassingCollectionLine _locCanvassingCollectionLineXPO)
        {
            try
            {
                if (_locCanvassingCollectionLineXPO != null)
                {
                    if (_locCanvassingCollectionLineXPO.DiscountRule != null && _locCanvassingCollectionLineXPO.Tax != null)
                    {
                        double _locTUAmountDiscount = 0;

                        #region Net
                        if (_locCanvassingCollectionLineXPO.DiscountRule.NetAmountRule == true && _locCanvassingCollectionLineXPO.Tax.TaxRule == TaxRule.BeforeDiscount)
                        {
                            if (_locCanvassingCollectionLineXPO.TAmount > 0 && _locCanvassingCollectionLineXPO.TAmount >= _locCanvassingCollectionLineXPO.DiscountRule.NetTotalUAmount)
                            {
                                if (_locCanvassingCollectionLineXPO.DiscountRule.DiscountType == DiscountType.Percentage)
                                {
                                    _locTUAmountDiscount = _locCanvassingCollectionLineXPO.TAmount * _locCanvassingCollectionLineXPO.DiscountRule.Value / 100;
                                }
                                if (_locCanvassingCollectionLineXPO.DiscountRule.DiscountType == DiscountType.Value)
                                {
                                    _locTUAmountDiscount = _locCanvassingCollectionLineXPO.DiscountRule.Value;
                                }

                                _locCanvassingCollectionLineXPO.DiscountChecked = true;
                                _locCanvassingCollectionLineXPO.DiscAmount = _locTUAmountDiscount;
                                _locCanvassingCollectionLineXPO.TAmount = _locCanvassingCollectionLineXPO.TAmount - _locTUAmountDiscount;
                                _locCanvassingCollectionLineXPO.Save();
                                _locCanvassingCollectionLineXPO.Session.CommitTransaction();
                            }

                        }
                        #endregion Net
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("BusinessObject = CanvassingCollectionLine" + ex.ToString());
            }
        }

        #endregion Tax

        #region Global Method

        private void SuccessMessageShow(string _locActionName)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 2000;
            options.Message = string.Format("{0}", _locActionName);
            options.Type = InformationType.Success;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Success";
            options.Win.Type = WinMessageType.Flyout;
            Application.ShowViewStrategy.ShowMessage(options);
        }

        private void ErrorMessageShow(string _locMessage)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 2000;
            options.Message = string.Format(_locMessage);
            options.Type = InformationType.Warning;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Error";
            options.Win.Type = WinMessageType.Alert;
            Application.ShowViewStrategy.ShowMessage(options);
        }

        #endregion Global Method
    }
}
