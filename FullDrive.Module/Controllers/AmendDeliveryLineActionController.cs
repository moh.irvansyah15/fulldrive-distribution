﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;

using FullDrive.Module.BusinessObjects;
using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.Xpo;
using System.Collections;
using DevExpress.Xpo;
using System.IO;
using System.Web;

namespace FullDrive.Module.Controllers
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class AmendDeliveryLineActionController : ViewController
    {
        public AmendDeliveryLineActionController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.
        }
        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }
        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        private void AmendDeliveryLineSelectAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;

                if (_objectToProcess != null)
                {
                    foreach (Object obj in _objectToProcess)
                    {
                        AmendDeliveryLine _locAmendDeliveryLineOS = (AmendDeliveryLine)_objectSpace.GetObject(obj);

                        if (_locAmendDeliveryLineOS != null)
                        {
                            if (_locAmendDeliveryLineOS.Session != null)
                            {
                                _currSession = _locAmendDeliveryLineOS.Session;
                            }

                            if (_locAmendDeliveryLineOS.Code != null && _currSession != null)
                            {
                                _currObjectId = _locAmendDeliveryLineOS.Code;

                                XPCollection<AmendDeliveryLine> _locAmendDeliveryLines = new XPCollection<AmendDeliveryLine>
                                                                                                     (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                      new BinaryOperator("Code", _currObjectId),
                                                                                                      new GroupOperator(GroupOperatorType.Or,
                                                                                                      new BinaryOperator("Status", Status.Progress),
                                                                                                      new BinaryOperator("Status", Status.Open)
                                                                                                      )));

                                if (_locAmendDeliveryLines != null && _locAmendDeliveryLines.Count > 0)
                                {
                                    foreach (AmendDeliveryLine _locAmendDeliveryLine in _locAmendDeliveryLines)
                                    {
                                        _locAmendDeliveryLine.Select = true;
                                        _locAmendDeliveryLine.Save();
                                        _locAmendDeliveryLine.Session.CommitTransaction();
                                    }

                                    SuccessMessageShow("AmendDeliveryLine has been Select");
                                }
                                else
                                {
                                    ErrorMessageShow("Data AmendDeliveryLine Not Available");
                                }
                            }
                        }
                    }
                    if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                    {
                        _objectSpace.CommitChanges();
                        _objectSpace.Refresh();
                    }
                    if (View is ListView)
                    {
                        _objectSpace.CommitChanges();
                        View.ObjectSpace.Refresh();
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("BusinessObject = AmendDeliveryLine" + ex.ToString());
            }
        }

        private void AmendDeliveryLineUnselectAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;

                if (_objectToProcess != null)
                {
                    foreach (Object obj in _objectToProcess)
                    {
                        AmendDeliveryLine _locAmendDeliveryLineOS = (AmendDeliveryLine)_objectSpace.GetObject(obj);

                        if (_locAmendDeliveryLineOS != null)
                        {
                            if (_locAmendDeliveryLineOS.Session != null)
                            {
                                _currSession = _locAmendDeliveryLineOS.Session;
                            }

                            if (_locAmendDeliveryLineOS.Code != null && _currSession != null)
                            {
                                _currObjectId = _locAmendDeliveryLineOS.Code;

                                XPCollection<AmendDeliveryLine> _locAmendDeliveryLines = new XPCollection<AmendDeliveryLine>
                                                                                                     (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                      new BinaryOperator("Code", _currObjectId),
                                                                                                      new GroupOperator(GroupOperatorType.Or,
                                                                                                      new BinaryOperator("Status", Status.Progress),
                                                                                                      new BinaryOperator("Status", Status.Open)
                                                                                                      )));

                                if (_locAmendDeliveryLines != null && _locAmendDeliveryLines.Count > 0)
                                {
                                    foreach (AmendDeliveryLine _locAmendDeliveryLine in _locAmendDeliveryLines)
                                    {
                                        _locAmendDeliveryLine.Select = false;
                                        _locAmendDeliveryLine.Save();
                                        _locAmendDeliveryLine.Session.CommitTransaction();
                                    }

                                    SuccessMessageShow("AmendDeliveryLine has been Unselect");
                                }
                                else
                                {
                                    ErrorMessageShow("Data AmendDeliveryLine Not Available");
                                }
                            }
                        }
                    }
                    if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                    {
                        _objectSpace.CommitChanges();
                        _objectSpace.Refresh();
                    }
                    if (View is ListView)
                    {
                        _objectSpace.CommitChanges();
                        View.ObjectSpace.Refresh();
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("BusinessObject = AmendDeliveryLine" + ex.ToString());
            }
        }

        private void AmendDeliveryLineShowSIAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
            var ListView = View as ListView;
            IObjectSpace os = Application.CreateObjectSpace();
            string listViewId = Application.FindListViewId(typeof(SalesInvoice));
            CollectionSourceBase cs = Application.CreateCollectionSource(os, typeof(SalesInvoice), listViewId);
            //Tinggal buat 
            ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
            string _stringSI = null;
            if (_objectsToProcess != null)
            {
                foreach (Object obj in _objectsToProcess)
                {
                    AmendDeliveryLine _locAmendDeliveryLine = (AmendDeliveryLine)_objectSpace.GetObject(obj);
                    if (_locAmendDeliveryLine != null)
                    {
                        if (_locAmendDeliveryLine.SalesInvoice != null)
                        {
                            if (_stringSI == null)
                            {
                                if (_locAmendDeliveryLine.SalesInvoice.Code != null)
                                {
                                    _stringSI = "( [Code]=='" + _locAmendDeliveryLine.SalesInvoice.Code + "' )";
                                }
                            }
                            else
                            {
                                if (_locAmendDeliveryLine.SalesInvoice.Code != null)
                                {
                                    _stringSI = _stringSI + " OR ( [Code]=='" + _locAmendDeliveryLine.SalesInvoice.Code + "' )";
                                }
                            }
                        }
                    }
                }
            }
            if (_stringSI != null)
            {
                cs.Criteria["SalesInvoiceFilter"] = CriteriaOperator.Parse(_stringSI);
            }
            e.ShowViewParameters.CreatedView = Application.CreateListView(
                listViewId,
                cs,
                true
            );
            e.ShowViewParameters.Context = TemplateContext.View;
            e.ShowViewParameters.TargetWindow = TargetWindow.NewModalWindow;
            
        }

        private void AmendDeliveryLineShowSICAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
            var ListView = View as ListView;
            IObjectSpace os = Application.CreateObjectSpace();
            string listViewId = Application.FindListViewId(typeof(SalesInvoiceCmp));
            CollectionSourceBase cs = Application.CreateCollectionSource(os, typeof(SalesInvoiceCmp), listViewId);
            //Tinggal buat 
            ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
            string _stringSIM = null;
            if (_objectsToProcess != null)
            {
                foreach (Object obj in _objectsToProcess)
                {
                    AmendDeliveryLine _locAmendDeliveryLine = (AmendDeliveryLine)_objectSpace.GetObject(obj);
                    if (_locAmendDeliveryLine != null)
                    {
                        if (_locAmendDeliveryLine.SalesInvoice != null)
                        {
                            if (_stringSIM == null)
                            {
                                if (_locAmendDeliveryLine.SalesInvoice.Code != null)
                                {
                                    _stringSIM = "( [SalesInvoice.Code]=='" + _locAmendDeliveryLine.SalesInvoice.Code + "' )";
                                }
                            }
                            else
                            {
                                if (_locAmendDeliveryLine.SalesInvoice.Code != null)
                                {
                                    _stringSIM = _stringSIM + " OR ( [SalesInvoice.Code]=='" + _locAmendDeliveryLine.SalesInvoice.Code + "' )";
                                }
                            }
                        }
                    }
                }
            }
            if (_stringSIM != null)
            {
                cs.Criteria["SalesInvoiceCmpFilter"] = CriteriaOperator.Parse(_stringSIM);
            }
            e.ShowViewParameters.CreatedView = Application.CreateListView(
                listViewId,
                cs,
                true
            );
            e.ShowViewParameters.Context = TemplateContext.View;
            e.ShowViewParameters.TargetWindow = TargetWindow.NewModalWindow;
        }
        //======================================== Code In Here ========================================

        #region Global Method

        private void SuccessMessageShow(string _locActionName)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 2000;
            options.Message = string.Format("{0}", _locActionName);
            options.Type = InformationType.Success;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Success";
            options.Win.Type = WinMessageType.Flyout;
            Application.ShowViewStrategy.ShowMessage(options);
        }

        private void ErrorMessageShow(string _locMessage)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 2000;
            options.Message = string.Format(_locMessage);
            options.Type = InformationType.Warning;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Error";
            options.Win.Type = WinMessageType.Alert;
            Application.ShowViewStrategy.ShowMessage(options);
        }





        #endregion Global Method

        
    }
}
