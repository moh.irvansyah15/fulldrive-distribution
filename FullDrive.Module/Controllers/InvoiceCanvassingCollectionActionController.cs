﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;

using FullDrive.Module.BusinessObjects;
using FullDrive.Module.CustomProcess;
using System.Collections;
using DevExpress.Xpo;

namespace FullDrive.Module.Controllers
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class InvoiceCanvassingCollectionActionController : ViewController
    {
        public InvoiceCanvassingCollectionActionController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.
        }
        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }
        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        private void InvoiceCanvassingCollectionShowICMAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
            var ListView = View as ListView;
            IObjectSpace os = Application.CreateObjectSpace();
            string listViewId = Application.FindListViewId(typeof(InvoiceCanvassingMonitoring));
            CollectionSourceBase cs = Application.CreateCollectionSource(os, typeof(InvoiceCanvassingMonitoring), listViewId);
            //Tinggal buat 
            ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
            string _stringICM = null;
            if (_objectsToProcess != null)
            {
                foreach (Object obj in _objectsToProcess)
                {
                    InvoiceCanvassingCollection _locInvoiceCanvassingCollection = (InvoiceCanvassingCollection)_objectSpace.GetObject(obj);
                    if (_locInvoiceCanvassingCollection != null)
                    {
                        if (_locInvoiceCanvassingCollection.InvoiceCanvassing != null)
                        {
                            if (_stringICM == null)
                            {
                                if (_locInvoiceCanvassingCollection.InvoiceCanvassing.Code != null)
                                {
                                    _stringICM = "( [InvoiceCanvassing.Code]=='" + _locInvoiceCanvassingCollection.InvoiceCanvassing.Code + "' )";
                                }
                            }
                            else
                            {
                                if (_locInvoiceCanvassingCollection.InvoiceCanvassing.Code != null)
                                {
                                    _stringICM = _stringICM + " OR ( [InvoiceCanvassing.Code]=='" + _locInvoiceCanvassingCollection.InvoiceCanvassing.Code + "' )";
                                }
                            }
                        }
                    }
                }
            }
            if (_stringICM != null)
            {
                cs.Criteria["InvoiceCanvassingCollectionFilter"] = CriteriaOperator.Parse(_stringICM);
            }
            e.ShowViewParameters.CreatedView = Application.CreateListView(
                listViewId,
                cs,
                true
            );
            e.ShowViewParameters.Context = TemplateContext.View;
            e.ShowViewParameters.TargetWindow = TargetWindow.NewModalWindow;
            DialogController dc = Application.CreateController<DialogController>();
            dc.Accepting += new EventHandler<DialogControllerAcceptingEventArgs>(dc_Accepting);
            e.ShowViewParameters.Controllers.Add(dc);
        }

        void dc_Accepting(object sender, DialogControllerAcceptingEventArgs e)
        {
            try
            {
                CanvassingCollection _locCanvassingCollection = null;
                View popupView = ((Controller)sender).Frame.View;
                ArrayList _objectsToProcess = new ArrayList(View.SelectedObjects);
                if (_objectsToProcess != null)
                {
                    foreach (Object _obj in _objectsToProcess)
                    {
                        InvoiceCanvassingCollection _locInvoiceCanvassingCollection = (InvoiceCanvassingCollection)popupView.ObjectSpace.GetObject(_obj);
                        if (_locInvoiceCanvassingCollection != null)
                        {
                            if (_locInvoiceCanvassingCollection.CanvassingCollection != null)
                            {
                                _locCanvassingCollection = _locInvoiceCanvassingCollection.CanvassingCollection;
                            }
                        }
                    }
                }
                if (_locCanvassingCollection != null)
                {
                    ArrayList _objectsToProcess2 = new ArrayList(popupView.SelectedObjects);
                    if (_objectsToProcess2 != null)
                    {
                        foreach (Object _obj2 in _objectsToProcess2)
                        {
                            InvoiceCanvassingMonitoring _locInvoiceCanvassingMonitoring = (InvoiceCanvassingMonitoring)popupView.ObjectSpace.GetObject(_obj2);
                            if (_locInvoiceCanvassingMonitoring != null && _locInvoiceCanvassingMonitoring.Select == true && (_locInvoiceCanvassingMonitoring.Status == Status.Open || _locInvoiceCanvassingMonitoring.Status == Status.Posted))
                            {
                                _locInvoiceCanvassingMonitoring.CanvassingCollection = _locCanvassingCollection;
                            }
                        }
                    }
                }
                popupView.ObjectSpace.CommitChanges();
            }
            catch (ValidationException)
            {
            }
        }
    }
}
