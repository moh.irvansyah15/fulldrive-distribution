﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;

using FullDrive.Module.BusinessObjects;
using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.Xpo;
using System.Collections;
using DevExpress.Xpo;

namespace FullDrive.Module.Controllers
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class InventoryTransferInActionController : ViewController
    {
        private ChoiceActionItem _selectionListviewFilter;

        public InventoryTransferInActionController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.
            #region Filter Status
            InventoryTransferInListviewFilterSelectionAction.Items.Clear();
            foreach (object _currApproval in Enum.GetValues(typeof(CustomProcess.Status)))
            {
                EnumDescriptor _ed = new EnumDescriptor(typeof(CustomProcess.Status));
                _selectionListviewFilter = new ChoiceActionItem(_ed.GetCaption(_currApproval), _currApproval);
                InventoryTransferInListviewFilterSelectionAction.Items.Add(_selectionListviewFilter);
            }
            #endregion Filter Status
        }
        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }
        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        private void InventoryTransferInGetPOAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;

                if (_objectsToProcess != null)
                {
                    foreach (Object obj in _objectsToProcess)
                    {
                        InventoryTransferIn _locInventoryTransferInOS = (InventoryTransferIn)_objectSpace.GetObject(obj);

                        if (_locInventoryTransferInOS != null)
                        {
                            if(_locInventoryTransferInOS.Session != null)
                            {
                                _currSession = _locInventoryTransferInOS.Session;
                            }

                            if (_locInventoryTransferInOS.Code != null && _currSession != null)
                            {
                                _currObjectId = _locInventoryTransferInOS.Code;

                                InventoryTransferIn _locInventoryTransferInXPO = _currSession.FindObject<InventoryTransferIn>
                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                     new BinaryOperator("Code", _currObjectId),
                                                                     new BinaryOperator("Status", Status.Open)));

                                if (_locInventoryTransferInXPO != null)
                                {
                                    XPCollection<InventoryPurchaseCollection> _locInventoryPurchaseCollections = new XPCollection<InventoryPurchaseCollection>
                                                                                        (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                        new BinaryOperator("InventoryTransferIn", _locInventoryTransferInXPO),
                                                                                        new GroupOperator(GroupOperatorType.Or,
                                                                                        new BinaryOperator("Status", Status.Open),
                                                                                        new BinaryOperator("Status", Status.Progress))));

                                    if (_locInventoryPurchaseCollections != null && _locInventoryPurchaseCollections.Count() > 0)
                                    {
                                        foreach (InventoryPurchaseCollection _locInventoryPurchaseCollection in _locInventoryPurchaseCollections)
                                        {
                                            if (_locInventoryPurchaseCollection.PurchaseOrder != null)
                                            {
                                                ApprovalLine _locApprovalLineXPO = _currSession.FindObject<ApprovalLine>
                                                                            (new GroupOperator(GroupOperatorType.And,
                                                                            new BinaryOperator("EndApproval", true),
                                                                            new BinaryOperator("PurchaseOrder", _locInventoryPurchaseCollection.PurchaseOrder)));

                                                if (_locApprovalLineXPO != null)
                                                {
                                                    GetPurchaseOrderMonitoring(_currSession, _locInventoryPurchaseCollection.PurchaseOrder, _locInventoryTransferInXPO);
                                                }
                                            }
                                        }
                                        SuccessMessageShow("Purchase Order Has Been Successfully Getting into Inventory Transfer In");
                                    }
                                }
                                else
                                {
                                    ErrorMessageShow("Data Purchase Order Not Available");
                                }
                            }
                            else
                            {
                                ErrorMessageShow("Data Purchase Order Not Available");
                            }
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = InventoryTransferIn " + ex.ToString());
            }
        }

        private void InventoryTransferInProgressAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;
                Status _locStatus = Status.None;
                DateTime _locNow;
                Status _locStatus2 = Status.None;
                DateTime _locNow2;

                if (_objectsToProcess != null)
                {
                    foreach (Object obj in _objectsToProcess)
                    {
                        InventoryTransferIn _locInventoryTransferInOS = (InventoryTransferIn)_objectSpace.GetObject(obj);

                        if (_locInventoryTransferInOS != null)
                        {
                            if(_locInventoryTransferInOS.Session != null)
                            {
                                _currSession = _locInventoryTransferInOS.Session;
                            }
                            if (_locInventoryTransferInOS.Code != null && _currSession != null)
                            {
                                _currObjectId = _locInventoryTransferInOS.Code;

                                InventoryTransferIn _locInventoryTransferInXPO = _currSession.FindObject<InventoryTransferIn>
                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                     new BinaryOperator("Code", _currObjectId),
                                                                     new GroupOperator(GroupOperatorType.Or,
                                                                     new BinaryOperator("Status", Status.Open),
                                                                     new BinaryOperator("Status", Status.Progress))));

                                if (_locInventoryTransferInXPO != null)
                                {
                                    if (_locInventoryTransferInXPO.Status == Status.Open)
                                    {
                                        _locStatus = Status.Progress;
                                        _locNow = now;
                                    }
                                    else
                                    {
                                        _locStatus = _locInventoryTransferInXPO.Status;
                                        _locNow = _locInventoryTransferInXPO.StatusDate;
                                    }
                                    _locInventoryTransferInXPO.Status = _locStatus;
                                    _locInventoryTransferInXPO.StatusDate = _locNow;
                                    _locInventoryTransferInXPO.Save();
                                    _locInventoryTransferInXPO.Session.CommitTransaction();

                                    #region InventoryTransferInLine
                                    XPCollection<InventoryTransferInLine> _locInventoryTransferInLines = new XPCollection<InventoryTransferInLine>
                                                           (_currSession, new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("InventoryTransferIn", _locInventoryTransferInXPO),
                                                            new BinaryOperator("Status", Status.Open)));

                                    if (_locInventoryTransferInLines != null && _locInventoryTransferInLines.Count > 0)
                                    {
                                        foreach (InventoryTransferInLine _locInventoryTransferInLine in _locInventoryTransferInLines)
                                        {
                                            if (_locInventoryTransferInLine.Status == Status.Open)
                                            {
                                                _locStatus2 = Status.Progress;
                                                _locNow2 = now;
                                            }
                                            else
                                            {
                                                _locStatus2 = _locInventoryTransferInLine.Status;
                                                _locNow2 = _locInventoryTransferInLine.StatusDate;
                                            }

                                            _locInventoryTransferInLine.Status = _locStatus2;
                                            _locInventoryTransferInLine.StatusDate = _locNow2;
                                            _locInventoryTransferInLine.Save();
                                            _locInventoryTransferInLine.Session.CommitTransaction();

                                            XPCollection<InventoryTransferInLot> _locInventoryTransferInLots = new XPCollection<InventoryTransferInLot>
                                                                                                    (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                    new BinaryOperator("InventoryTransferInLine", _locInventoryTransferInLine),
                                                                                                    new BinaryOperator("Status", Status.Open)));

                                            if (_locInventoryTransferInLots != null && _locInventoryTransferInLots.Count() > 0)
                                            {
                                                foreach (InventoryTransferInLot _locInventoryTransferInLot in _locInventoryTransferInLots)
                                                {
                                                    _locInventoryTransferInLot.Status = Status.Progress;
                                                    _locInventoryTransferInLot.StatusDate = now;
                                                    _locInventoryTransferInLot.Save();
                                                    _locInventoryTransferInLot.Session.CommitTransaction();
                                                }
                                            }
                                        }
                                    }
                                    #endregion InventoryTransferInLine

                                    #region InventoryPurchaseCollection
                                    XPCollection<InventoryPurchaseCollection> _locInventoryPurchaseCollections = new XPCollection<InventoryPurchaseCollection>
                                                                                    (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                    new BinaryOperator("Status", Status.Open)));
                                    if (_locInventoryPurchaseCollections != null && _locInventoryPurchaseCollections.Count() > 0)
                                    {
                                        foreach (InventoryPurchaseCollection _locInventoryPurchaseCollection in _locInventoryPurchaseCollections)
                                        {
                                            _locInventoryPurchaseCollection.Status = Status.Progress;
                                            _locInventoryPurchaseCollection.StatusDate = now;
                                            _locInventoryPurchaseCollection.Save();
                                            _locInventoryPurchaseCollection.Session.CommitTransaction();
                                        }
                                    }
                                    #endregion InventoryPurchaseCollection

                                    SuccessMessageShow(_locInventoryTransferInXPO.Code + " has been change successfully to Progress");
                                }
                                else
                                {
                                    ErrorMessageShow("Data Inventory Transfer In Not Available");
                                }
                            }
                            else
                            {
                                ErrorMessageShow("Data Inventory Transfer In Not Available");
                            }
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = InventoryTransferIn " + ex.ToString());
            }
        }

        private void InventoryTransferInPostingAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;

                if (_objectsToProcess != null)
                {
                    foreach (Object obj in _objectsToProcess)
                    {
                        InventoryTransferIn _locInventoryTransferInOS = (InventoryTransferIn)_objectSpace.GetObject(obj);

                        if (_locInventoryTransferInOS != null)
                        {
                            if(_locInventoryTransferInOS.Session != null)
                            {
                                _currSession = _locInventoryTransferInOS.Session;
                            }

                            if(_locInventoryTransferInOS.Code != null && _currSession != null)
                            {
                                _currObjectId = _locInventoryTransferInOS.Code;

                                InventoryTransferIn _locInventoryTransferInXPO = _currSession.FindObject<InventoryTransferIn>
                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                     new BinaryOperator("Code", _currObjectId),
                                                                     new BinaryOperator("InventoryMovingType", InventoryMovingType.Receive),
                                                                     new GroupOperator(GroupOperatorType.Or,
                                                                     new BinaryOperator("Status", Status.Progress),
                                                                     new BinaryOperator("Status", Status.Posted))));

                                if (_locInventoryTransferInXPO != null)
                                {
                                    SetInventoryTransferInMonitoring(_currSession, _locInventoryTransferInXPO);
                                    SetReceiveBeginingInventory(_currSession, _locInventoryTransferInXPO);
                                    SetReceiveInventoryJournal(_currSession, _locInventoryTransferInXPO);
                                    if (CheckJournalSetup(_currSession, _locInventoryTransferInXPO) == true && CheckGoodsReceiveJournal(_currSession, _locInventoryTransferInXPO) == false)
                                    {
                                        SetGoodsReceiveJournal(_currSession, _locInventoryTransferInXPO);
                                    }
                                    SetStatusReceiveInventoryTransferInLine(_currSession, _locInventoryTransferInXPO);
                                    SetNormal(_currSession, _locInventoryTransferInXPO);
                                    SetFinalStatusReceiveInventoryTransferIn(_currSession, _locInventoryTransferInXPO);
                                    SuccessMessageShow(_locInventoryTransferInXPO.Code + " has been change successfully to Receive");
                                }
                                else
                                {
                                    ErrorMessageShow("Inventory Transfer In Data Not Available");
                                }
                            }
                            else
                            {
                                ErrorMessageShow("Inventory Transfer In Data Not Available");
                            }
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = InventoryTransferIn " + ex.ToString());
            }
        }

        private void InventoryTransferInListviewFilterSelectionAction_Execute(object sender, SingleChoiceActionExecuteEventArgs e)
        {
            try
            {
                if ((View is ListView) & (View.ObjectTypeInfo.Type == typeof(InventoryTransferIn)))
                {
                    if ((Status)e.SelectedChoiceActionItem.Data == Status.None)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.None, BinaryOperatorType.NotEqual);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Open)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Open, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Progress)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Progress, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Posted)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Posted, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Lock)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Lock, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Close)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Close, BinaryOperatorType.Equal);
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = InventoryTransferIn " + ex.ToString());
            }
        }

        //============================================ Code In Here ============================================

        #region GetPO

        private void GetPurchaseOrderMonitoring(Session _currSession, PurchaseOrder _locPurchaseOrderXPO, InventoryTransferIn _locInventoryTransferInXPO)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                DateTime now = DateTime.Now;
                string _currSignCode = null;
                DateTime _locDocDate = now;
                int _locJournalMonth = 0;
                int _locJournalYear = 0;

                if (_locPurchaseOrderXPO != null && _locInventoryTransferInXPO != null)
                {
                    //Hanya memindahkan PurchaseOrderMonitoring ke InventoryTransferInLine

                    XPCollection<PurchaseOrderMonitoring> _locPurchaseOrderMonitorings = new XPCollection<PurchaseOrderMonitoring>
                                                                                        (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                        new BinaryOperator("PurchaseOrder", _locPurchaseOrderXPO),
                                                                                        new BinaryOperator("InventoryTransferIn", _locInventoryTransferInXPO),
                                                                                        new BinaryOperator("Select", true),
                                                                                        new GroupOperator(GroupOperatorType.Or,
                                                                                        new BinaryOperator("Status", Status.Open),
                                                                                        new BinaryOperator("Status", Status.Posted))));

                    if (_locPurchaseOrderMonitorings != null && _locPurchaseOrderMonitorings.Count() > 0)
                    {
                        foreach (PurchaseOrderMonitoring _locPurchaseOrderMonitoring in _locPurchaseOrderMonitorings)
                        {
                            if (_locPurchaseOrderMonitoring.PurchaseOrderLine != null)
                            {
                                _currSignCode = _globFunc.GetNumberingSignUnlockOptimisticRecordBasedOrganization(_currSession.DataLayer, ObjectList.InventoryTransferInLine, _locInventoryTransferInXPO.Company, _locInventoryTransferInXPO.Workplace);
                                
                                if (_currSignCode != null)
                                {
                                    #region SaveInventoryTransferInLine
                                    InventoryTransferInLine _saveDataInventoryTransferInLine = new InventoryTransferInLine(_currSession)
                                    {
                                        SignCode = _currSignCode,
                                        PurchaseType = _locPurchaseOrderMonitoring.PurchaseType,
                                        Item = _locPurchaseOrderMonitoring.Item,
                                        Brand = _locPurchaseOrderMonitoring.PurchaseOrderLine.Brand,
                                        Description = _locPurchaseOrderMonitoring.PurchaseOrderLine.Description,
                                        DQty = _locPurchaseOrderMonitoring.DQty,
                                        DUOM = _locPurchaseOrderMonitoring.DUOM,
                                        Qty = _locPurchaseOrderMonitoring.Qty,
                                        UOM = _locPurchaseOrderMonitoring.UOM,
                                        TQty = _locPurchaseOrderMonitoring.TQty,
                                        Company = _locPurchaseOrderMonitoring.Company,
                                        Workplace = _locPurchaseOrderMonitoring.Workplace,
                                        InventoryTransferIn = _locInventoryTransferInXPO,
                                        PurchaseOrderMonitoring = _locPurchaseOrderMonitoring,
                                        DocDate = _locPurchaseOrderMonitoring.DocDate,
                                        JournalMonth = _locPurchaseOrderMonitoring.JournalMonth,
                                        JournalYear = _locPurchaseOrderMonitoring.JournalYear,
                                    };
                                    _saveDataInventoryTransferInLine.Save();
                                    _saveDataInventoryTransferInLine.Session.CommitTransaction();
                                    #endregion SaveInventoryTransferInLine

                                    _locDocDate = _locPurchaseOrderMonitoring.DocDate;
                                    _locJournalMonth = _locPurchaseOrderMonitoring.JournalMonth;
                                    _locJournalYear = _locPurchaseOrderMonitoring.JournalYear;

                                    InventoryTransferInLine _locInvTransInLine = _currSession.FindObject<InventoryTransferInLine>
                                                                                 (new GroupOperator(GroupOperatorType.And,
                                                                                 new BinaryOperator("SignCode", _currSignCode)));
                                    if (_locInvTransInLine != null)
                                    {
                                        SetRemainQty(_currSession, _locPurchaseOrderMonitoring);
                                        SetPostingQty(_currSession, _locPurchaseOrderMonitoring);
                                        SetPostingAmount(_currSession, _locPurchaseOrderMonitoring);
                                        SetProcessCount(_currSession, _locPurchaseOrderMonitoring);
                                        SetStatusPurchaseOrderMonitoring(_currSession, _locPurchaseOrderMonitoring);
                                        SetNormalQuantity(_currSession, _locPurchaseOrderMonitoring);
                                    }
                                }
                            }
                        }
                        _locInventoryTransferInXPO.DocDate = _locDocDate;
                        _locInventoryTransferInXPO.JournalMonth = _locJournalMonth;
                        _locInventoryTransferInXPO.JournalYear = _locJournalYear;
                        _locInventoryTransferInXPO.Save();
                        _locInventoryTransferInXPO.Session.CommitTransaction();
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = InventoryTransferIn ", ex.ToString());
            }
        }

        private void SetRemainQty(Session _currSession, PurchaseOrderMonitoring _locPurchaseOrderMonitoringXPO)
        {
            try
            {
                if (_locPurchaseOrderMonitoringXPO != null)
                {
                    double _locRmDQty = 0;
                    double _locRmQty = 0;
                    double _locInvLineTotal = 0;
                    ItemUnitOfMeasure _locItemUOM = null;

                    #region ProcessCount=0
                    if (_locPurchaseOrderMonitoringXPO.PostedCount == 0)
                    {
                        #region MaxQuantity
                        if (_locPurchaseOrderMonitoringXPO.MxDQty > 0)
                        {
                            if (_locPurchaseOrderMonitoringXPO.DQty > 0 && _locPurchaseOrderMonitoringXPO.DQty <= _locPurchaseOrderMonitoringXPO.MxDQty)
                            {
                                _locRmDQty = _locPurchaseOrderMonitoringXPO.MxDQty - _locPurchaseOrderMonitoringXPO.DQty;
                            }

                            if (_locPurchaseOrderMonitoringXPO.Qty > 0 && _locPurchaseOrderMonitoringXPO.Qty <= _locPurchaseOrderMonitoringXPO.MxQty)
                            {
                                _locRmQty = _locPurchaseOrderMonitoringXPO.MxQty - _locPurchaseOrderMonitoringXPO.Qty;
                            }


                            _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                       (new GroupOperator(GroupOperatorType.And,
                                                        new BinaryOperator("Item", _locPurchaseOrderMonitoringXPO.Item),
                                                        new BinaryOperator("UOM", _locPurchaseOrderMonitoringXPO.MxUOM),
                                                        new BinaryOperator("DefaultUOM", _locPurchaseOrderMonitoringXPO.MxDUOM),
                                                        new BinaryOperator("Active", true)));
                            if (_locItemUOM != null)
                            {
                                if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                {
                                    _locInvLineTotal = _locRmQty * _locItemUOM.DefaultConversion + _locRmDQty;
                                }
                                else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                {
                                    _locInvLineTotal = _locRmQty / _locItemUOM.Conversion + _locRmDQty;
                                }
                                else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                {
                                    _locInvLineTotal = _locRmQty + _locRmDQty;
                                }
                            }
                            else
                            {
                                _locInvLineTotal = _locRmQty + _locRmDQty;
                            }

                        }
                        #endregion MaxQuantity
                        #region NonMaxQuantity
                        else
                        {
                            if (_locPurchaseOrderMonitoringXPO.DQty > 0)
                            {
                                _locRmDQty = 0;
                            }

                            if (_locPurchaseOrderMonitoringXPO.Qty > 0)
                            {
                                _locRmQty = 0;
                            }


                            _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                       (new GroupOperator(GroupOperatorType.And,
                                                        new BinaryOperator("Item", _locPurchaseOrderMonitoringXPO.Item),
                                                        new BinaryOperator("UOM", _locPurchaseOrderMonitoringXPO.UOM),
                                                        new BinaryOperator("DefaultUOM", _locPurchaseOrderMonitoringXPO.DUOM),
                                                        new BinaryOperator("Active", true)));
                            if (_locItemUOM != null)
                            {
                                if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                {
                                    _locInvLineTotal = _locRmQty * _locItemUOM.DefaultConversion + _locRmDQty;
                                }
                                else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                {
                                    _locInvLineTotal = _locRmQty / _locItemUOM.Conversion + _locRmDQty;
                                }
                                else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                {
                                    _locInvLineTotal = _locRmQty + _locRmDQty;
                                }
                            }
                            else
                            {
                                _locInvLineTotal = _locRmQty + _locRmDQty;
                            }
                        }
                        #endregion NonMaxQuantity
                    }
                    #endregion ProcessCount=0

                    #region ProcessCount>0
                    if (_locPurchaseOrderMonitoringXPO.PostedCount > 0)
                    {
                        if (_locPurchaseOrderMonitoringXPO.RmDQty > 0)
                        {
                            _locRmDQty = _locPurchaseOrderMonitoringXPO.RmDQty - _locPurchaseOrderMonitoringXPO.DQty;
                        }

                        if (_locPurchaseOrderMonitoringXPO.RmQty > 0)
                        {
                            _locRmQty = _locPurchaseOrderMonitoringXPO.RmQty - _locPurchaseOrderMonitoringXPO.Qty;
                        }

                        if (_locPurchaseOrderMonitoringXPO.MxDQty > 0 || _locPurchaseOrderMonitoringXPO.MxQty > 0)
                        {
                            _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                       (new GroupOperator(GroupOperatorType.And,
                                                        new BinaryOperator("Item", _locPurchaseOrderMonitoringXPO.Item),
                                                        new BinaryOperator("UOM", _locPurchaseOrderMonitoringXPO.MxUOM),
                                                        new BinaryOperator("DefaultUOM", _locPurchaseOrderMonitoringXPO.MxDUOM),
                                                        new BinaryOperator("Active", true)));
                        }
                        else
                        {
                            _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                       (new GroupOperator(GroupOperatorType.And,
                                                        new BinaryOperator("Item", _locPurchaseOrderMonitoringXPO.Item),
                                                        new BinaryOperator("UOM", _locPurchaseOrderMonitoringXPO.UOM),
                                                        new BinaryOperator("DefaultUOM", _locPurchaseOrderMonitoringXPO.DUOM),
                                                        new BinaryOperator("Active", true)));
                        }


                        if (_locItemUOM != null)
                        {
                            if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                            {
                                _locInvLineTotal = _locRmQty * _locItemUOM.DefaultConversion + _locRmDQty;
                            }
                            else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                            {
                                _locInvLineTotal = _locRmQty / _locItemUOM.Conversion + _locRmDQty;
                            }
                            else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                            {
                                _locInvLineTotal = _locRmQty + _locRmDQty;
                            }
                        }
                        else
                        {
                            _locInvLineTotal = _locRmQty + _locRmDQty;
                        }

                    }
                    #endregion ProcessCount>0

                    _locPurchaseOrderMonitoringXPO.RmDQty = _locRmDQty;
                    _locPurchaseOrderMonitoringXPO.RmQty = _locRmQty;
                    _locPurchaseOrderMonitoringXPO.RmTQty = _locInvLineTotal;
                    _locPurchaseOrderMonitoringXPO.Save();
                    _locPurchaseOrderMonitoringXPO.Session.CommitTransaction();

                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = InventoryTransferIn " + ex.ToString());
            }
        }

        private void SetPostingQty(Session _currSession, PurchaseOrderMonitoring _locPurchaseOrderMonitoringXPO)
        {
            try
            {
                if (_locPurchaseOrderMonitoringXPO != null)
                {
                    double _locPDQty = 0;
                    double _locPQty = 0;
                    double _locInvLineTotal = 0;
                    ItemUnitOfMeasure _locItemUOM = null;

                    #region ProcessCount=0
                    if (_locPurchaseOrderMonitoringXPO.PostedCount == 0)
                    {
                        #region MaxQuantity
                        if (_locPurchaseOrderMonitoringXPO.MxDQty > 0)
                        {
                            if (_locPurchaseOrderMonitoringXPO.DQty > 0 && _locPurchaseOrderMonitoringXPO.DQty <= _locPurchaseOrderMonitoringXPO.MxDQty)
                            {
                                _locPDQty = _locPurchaseOrderMonitoringXPO.DQty;
                            }

                            if (_locPurchaseOrderMonitoringXPO.Qty > 0 && _locPurchaseOrderMonitoringXPO.Qty <= _locPurchaseOrderMonitoringXPO.MxQty)
                            {
                                _locPQty = _locPurchaseOrderMonitoringXPO.Qty;
                            }


                            _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                       (new GroupOperator(GroupOperatorType.And,
                                                        new BinaryOperator("Item", _locPurchaseOrderMonitoringXPO.Item),
                                                        new BinaryOperator("UOM", _locPurchaseOrderMonitoringXPO.MxUOM),
                                                        new BinaryOperator("DefaultUOM", _locPurchaseOrderMonitoringXPO.MxDUOM),
                                                        new BinaryOperator("Active", true)));
                            if (_locItemUOM != null)
                            {
                                if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                {
                                    _locInvLineTotal = _locPQty * _locItemUOM.DefaultConversion + _locPDQty;
                                }
                                else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                {
                                    _locInvLineTotal = _locPQty / _locItemUOM.Conversion + _locPDQty;
                                }
                                else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                {
                                    _locInvLineTotal = _locPQty + _locPDQty;
                                }
                            }
                            else
                            {
                                _locInvLineTotal = _locPQty + _locPDQty;
                            }

                        }
                        #endregion MaxQuantity
                        #region NonMaxQuantity
                        else
                        {
                            if (_locPurchaseOrderMonitoringXPO.DQty > 0)
                            {
                                _locPDQty = _locPurchaseOrderMonitoringXPO.DQty;
                            }

                            if (_locPurchaseOrderMonitoringXPO.Qty > 0)
                            {
                                _locPQty = _locPurchaseOrderMonitoringXPO.Qty;
                            }


                            _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                       (new GroupOperator(GroupOperatorType.And,
                                                        new BinaryOperator("Item", _locPurchaseOrderMonitoringXPO.Item),
                                                        new BinaryOperator("UOM", _locPurchaseOrderMonitoringXPO.UOM),
                                                        new BinaryOperator("DefaultUOM", _locPurchaseOrderMonitoringXPO.DUOM),
                                                        new BinaryOperator("Active", true)));
                            if (_locItemUOM != null)
                            {
                                if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                {
                                    _locInvLineTotal = _locPQty * _locItemUOM.DefaultConversion + _locPDQty;
                                }
                                else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                {
                                    _locInvLineTotal = _locPQty / _locItemUOM.Conversion + _locPDQty;
                                }
                                else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                {
                                    _locInvLineTotal = _locPQty + _locPDQty;
                                }
                            }
                            else
                            {
                                _locInvLineTotal = _locPQty + _locPDQty;
                            }
                        }
                        #endregion NonMaxQuantity
                    }
                    #endregion ProcessCount=0

                    #region ProcessCount>0
                    if (_locPurchaseOrderMonitoringXPO.PostedCount > 0)
                    {
                        if (_locPurchaseOrderMonitoringXPO.PDQty > 0)
                        {
                            _locPDQty = _locPurchaseOrderMonitoringXPO.PDQty + _locPurchaseOrderMonitoringXPO.DQty;
                        }

                        if (_locPurchaseOrderMonitoringXPO.PQty > 0)
                        {
                            _locPQty = _locPurchaseOrderMonitoringXPO.PQty + _locPurchaseOrderMonitoringXPO.Qty;
                        }

                        if (_locPurchaseOrderMonitoringXPO.MxDQty > 0 || _locPurchaseOrderMonitoringXPO.MxQty > 0)
                        {
                            _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                   (new GroupOperator(GroupOperatorType.And,
                                                    new BinaryOperator("Item", _locPurchaseOrderMonitoringXPO.Item),
                                                    new BinaryOperator("UOM", _locPurchaseOrderMonitoringXPO.MxUOM),
                                                    new BinaryOperator("DefaultUOM", _locPurchaseOrderMonitoringXPO.MxDUOM),
                                                    new BinaryOperator("Active", true)));
                        }
                        else
                        {
                            _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                   (new GroupOperator(GroupOperatorType.And,
                                                    new BinaryOperator("Item", _locPurchaseOrderMonitoringXPO.Item),
                                                    new BinaryOperator("UOM", _locPurchaseOrderMonitoringXPO.UOM),
                                                    new BinaryOperator("DefaultUOM", _locPurchaseOrderMonitoringXPO.DUOM),
                                                    new BinaryOperator("Active", true)));
                        }

                        if (_locItemUOM != null)
                        {
                            if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                            {
                                _locInvLineTotal = _locPQty * _locItemUOM.DefaultConversion + _locPDQty;
                            }
                            else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                            {
                                _locInvLineTotal = _locPQty / _locItemUOM.Conversion + _locPDQty;
                            }
                            else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                            {
                                _locInvLineTotal = _locPQty + _locPDQty;
                            }
                        }
                        else
                        {
                            _locInvLineTotal = _locPQty + _locPDQty;
                        }

                    }
                    #endregion ProcessCount>0

                    _locPurchaseOrderMonitoringXPO.PDQty = _locPDQty;
                    _locPurchaseOrderMonitoringXPO.PDUOM = _locPurchaseOrderMonitoringXPO.DUOM;
                    _locPurchaseOrderMonitoringXPO.PQty = _locPQty;
                    _locPurchaseOrderMonitoringXPO.PUOM = _locPurchaseOrderMonitoringXPO.UOM;
                    _locPurchaseOrderMonitoringXPO.PTQty = _locInvLineTotal;
                    _locPurchaseOrderMonitoringXPO.Save();
                    _locPurchaseOrderMonitoringXPO.Session.CommitTransaction();

                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = InventoryTransferIn " + ex.ToString());
            }
        }

        private void SetPostingAmount(Session _currSession, PurchaseOrderMonitoring _locPurchaseOrderMonitoringXPO)
        {
            try
            {
                if (_locPurchaseOrderMonitoringXPO != null)
                {
                    double _locPTUAmount = 0;
                    double _locPTxAmount = 0;
                    double _locPTAmount = 0;

                    if (_locPurchaseOrderMonitoringXPO.TUAmount > 0)
                    {
                        _locPTUAmount = _locPTUAmount + _locPurchaseOrderMonitoringXPO.TUAmount;
                    }
                    if (_locPurchaseOrderMonitoringXPO.TxAmount > 0)
                    {
                        _locPTxAmount = _locPTxAmount + _locPurchaseOrderMonitoringXPO.TxAmount;
                    }
                    if (_locPurchaseOrderMonitoringXPO.TAmount > 0)
                    {
                        _locPTAmount = _locPTAmount + _locPurchaseOrderMonitoringXPO.TAmount;
                    }

                    _locPurchaseOrderMonitoringXPO.PTUAmount = _locPTUAmount;
                    _locPurchaseOrderMonitoringXPO.PTxAmount = _locPTxAmount;
                    _locPurchaseOrderMonitoringXPO.PTAmount = _locPTAmount;
                    _locPurchaseOrderMonitoringXPO.Save();
                    _locPurchaseOrderMonitoringXPO.Session.CommitTransaction();

                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = InventoryTransferIn " + ex.ToString());
            }
        }

        private void SetProcessCount(Session _currSession, PurchaseOrderMonitoring _locPurchaseOrderMonitoringXPO)
        {
            try
            {
                if (_locPurchaseOrderMonitoringXPO != null)
                {
                    if (_locPurchaseOrderMonitoringXPO.Status == Status.Open || _locPurchaseOrderMonitoringXPO.Status == Status.Progress || _locPurchaseOrderMonitoringXPO.Status == Status.Posted)
                    {
                        _locPurchaseOrderMonitoringXPO.PostedCount = _locPurchaseOrderMonitoringXPO.PostedCount + 1;
                        _locPurchaseOrderMonitoringXPO.Save();
                        _locPurchaseOrderMonitoringXPO.Session.CommitTransaction();
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = InventoryTransferIn " + ex.ToString());
            }
        }

        private void SetStatusPurchaseOrderMonitoring(Session _currSession, PurchaseOrderMonitoring _locPurchaseOrderMonitoringXPO)
        {
            try
            {
                DateTime now = DateTime.Now;
                if (_locPurchaseOrderMonitoringXPO != null)
                {
                    if (_locPurchaseOrderMonitoringXPO.Status == Status.Open || _locPurchaseOrderMonitoringXPO.Status == Status.Progress || _locPurchaseOrderMonitoringXPO.Status == Status.Posted)
                    {
                        if (_locPurchaseOrderMonitoringXPO.RmDQty == 0 && _locPurchaseOrderMonitoringXPO.RmQty == 0 && _locPurchaseOrderMonitoringXPO.RmTQty == 0)
                        {
                            _locPurchaseOrderMonitoringXPO.Status = Status.Close;
                            _locPurchaseOrderMonitoringXPO.ActivationPosting = true;
                            _locPurchaseOrderMonitoringXPO.StatusDate = now;
                        }
                        else
                        {
                            _locPurchaseOrderMonitoringXPO.Status = Status.Posted;
                            _locPurchaseOrderMonitoringXPO.StatusDate = now;
                        }
                        _locPurchaseOrderMonitoringXPO.Save();
                        _locPurchaseOrderMonitoringXPO.Session.CommitTransaction();
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = InventoryTransferIn " + ex.ToString());
            }
        }

        private void SetNormalQuantity(Session _currSession, PurchaseOrderMonitoring _locPurchaseOrderMonitoringXPO)
        {
            try
            {
                if (_locPurchaseOrderMonitoringXPO != null)
                {
                    if (_locPurchaseOrderMonitoringXPO.Status == Status.Open || _locPurchaseOrderMonitoringXPO.Status == Status.Progress || _locPurchaseOrderMonitoringXPO.Status == Status.Posted || _locPurchaseOrderMonitoringXPO.Status == Status.Close)
                    {
                        if (_locPurchaseOrderMonitoringXPO.DQty > 0 || _locPurchaseOrderMonitoringXPO.Qty > 0)
                        {
                            _locPurchaseOrderMonitoringXPO.Select = false;
                            _locPurchaseOrderMonitoringXPO.DQty = 0;
                            _locPurchaseOrderMonitoringXPO.Qty = 0;
                            _locPurchaseOrderMonitoringXPO.InventoryTransferIn = null;
                            _locPurchaseOrderMonitoringXPO.Save();
                            _locPurchaseOrderMonitoringXPO.Session.CommitTransaction();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = InventoryTransferIn " + ex.ToString());
            }
        }

        #endregion GetPO

        #region Method Posting

        private void SetInventoryTransferInMonitoring(Session _currSession, InventoryTransferIn _locInventoryTransferInXPO)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                DateTime now = DateTime.Now;

                if (_locInventoryTransferInXPO != null)
                {
                    if (_locInventoryTransferInXPO.Status == Status.Progress || _locInventoryTransferInXPO.Status == Status.Posted)
                    {
                        XPCollection<InventoryTransferInLine> _locInventoryTransferInLines = new XPCollection<InventoryTransferInLine>
                                                            (_currSession, new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("InventoryTransferIn", _locInventoryTransferInXPO),
                                                            new BinaryOperator("Select", true),
                                                            new GroupOperator(GroupOperatorType.Or,
                                                            new BinaryOperator("Status", Status.Progress),
                                                            new BinaryOperator("Status", Status.Posted))));

                        if (_locInventoryTransferInLines != null && _locInventoryTransferInLines.Count() > 0)
                        {
                            foreach (InventoryTransferInLine _locInventoryTransferInLine in _locInventoryTransferInLines)
                            {
                                #region SaveInventoryTransferInMonitoring
                                InventoryTransferInMonitoring _saveDataInventoryTransferInMonitoring = new InventoryTransferInMonitoring(_currSession)
                                {
                                    InventoryTransferIn = _locInventoryTransferInXPO,
                                    InventoryTransferInLine = _locInventoryTransferInLine,
                                    PurchaseType = _locInventoryTransferInLine.PurchaseType,
                                    Item = _locInventoryTransferInLine.Item,
                                    Brand = _locInventoryTransferInLine.Brand,
                                    Description = _locInventoryTransferInLine.Description,
                                    Location = _locInventoryTransferInLine.Location,
                                    BinLocation = _locInventoryTransferInLine.BinLocation,
                                    StockType = _locInventoryTransferInLine.StockType,
                                    StockGroup = _locInventoryTransferInLine.StockGroup,
                                    MxDQty = _locInventoryTransferInLine.DQty,
                                    MxDUOM = _locInventoryTransferInLine.DUOM,
                                    MxQty = _locInventoryTransferInLine.Qty,
                                    MxUOM = _locInventoryTransferInLine.UOM,
                                    MxTQty = _locInventoryTransferInLine.TQty,
                                    DQty = _locInventoryTransferInLine.DQty,
                                    DUOM = _locInventoryTransferInLine.DUOM,
                                    Qty = _locInventoryTransferInLine.Qty,
                                    UOM = _locInventoryTransferInLine.UOM,
                                    TQty = _locInventoryTransferInLine.TQty,
                                    Company = _locInventoryTransferInLine.Company,
                                    Workplace = _locInventoryTransferInLine.Workplace,
                                    Division = _locInventoryTransferInLine.Division,
                                    Department = _locInventoryTransferInLine.Department,
                                    Section = _locInventoryTransferInLine.Section,
                                    Employee = _locInventoryTransferInLine.Employee,
                                    PurchaseOrderMonitoring = _locInventoryTransferInLine.PurchaseOrderMonitoring,
                                    DocDate = _locInventoryTransferInLine.DocDate,
                                    JournalMonth = _locInventoryTransferInLine.JournalMonth,
                                    JournalYear = _locInventoryTransferInLine.JournalYear,
                                };
                                _saveDataInventoryTransferInMonitoring.Save();
                                _saveDataInventoryTransferInMonitoring.Session.CommitTransaction();
                                #endregion SaveInventoryTransferInMonitoring
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = InventoryTransferIn ", ex.ToString());
            }
        }

        private void SetReceiveBeginingInventory(Session _currSession, InventoryTransferIn _locInventoryTransferInXPO)
        {
            try
            {
                XPCollection<InventoryTransferInLine> _locInvTransInLines = new XPCollection<InventoryTransferInLine>(_currSession,
                                                                        new GroupOperator(GroupOperatorType.And,
                                                                        new BinaryOperator("InventoryTransferIn", _locInventoryTransferInXPO),
                                                                        new BinaryOperator("Select", true),
                                                                        new GroupOperator(GroupOperatorType.Or,
                                                                        new BinaryOperator("Status", Status.Progress),
                                                                        new BinaryOperator("Status", Status.Posted))));

                if (_locInvTransInLines != null && _locInvTransInLines.Count() > 0)
                {
                    GlobalFunction _globFunc = new GlobalFunction();
                    double _locInvLineTotal = 0;
                    XPCollection<BeginningInventoryLine> _locBegInventoryLines = null;
                    ItemUnitOfMeasure _locItemUOM = null;
                    string _fullString = null;
                    string _locLocationParse = null;
                    string _locBinLocationParse = null;
                    string _locItemParse = null;
                    string _locLocationTypeParse = null;
                    string _locStockTypeParse = null;
                    string _locStockGroupParse = null;
                    string _locActiveParse = null;

                    foreach (InventoryTransferInLine _locInvTransInLine in _locInvTransInLines)
                    {
                        if (_locInvTransInLine.DQty > 0 || _locInvTransInLine.Qty > 0)
                        {
                            XPCollection<InventoryTransferInLot> _locInventoryTransferInLots = new XPCollection<InventoryTransferInLot>
                                                                                    (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                    new BinaryOperator("InventoryTransferInLine", _locInvTransInLine),
                                                                                    new BinaryOperator("Select", true),
                                                                                    new GroupOperator(GroupOperatorType.Or,
                                                                                    new BinaryOperator("Status", Status.Progress),
                                                                                    new BinaryOperator("Status", Status.Posted))));

                            #region LotNumber
                            if (_locInventoryTransferInLots != null && _locInventoryTransferInLots.Count() > 0)
                            {
                                foreach (InventoryTransferInLot _locInventoryTransferInLot in _locInventoryTransferInLots)
                                {
                                    if (_locInventoryTransferInLot.Select == true)
                                    {
                                        if (_locInventoryTransferInLot.BegInvLine != null)
                                        {
                                            if (_locInventoryTransferInLot.BegInvLine.Code != null)
                                            {
                                                BeginningInventoryLine _locBegInvLine = _currSession.FindObject<BeginningInventoryLine>
                                                                                (new GroupOperator(GroupOperatorType.And,
                                                                                 new BinaryOperator("Code", _locInventoryTransferInLot.BegInvLine.Code)));
                                                if (_locBegInvLine != null)
                                                {
                                                    if (_locBegInvLine.QtyAvailable >= _locInventoryTransferInLot.TQty)
                                                    {
                                                        _locBegInvLine.QtyAvailable = _locBegInvLine.QtyAvailable + _locInventoryTransferInLot.TQty;
                                                        _locBegInvLine.Save();
                                                        _locBegInvLine.Session.CommitTransaction();
                                                    }

                                                    if (_locBegInvLine.BeginningInventory != null)
                                                    {
                                                        if (_locBegInvLine.BeginningInventory.Code != null)
                                                        {
                                                            BeginningInventory _locBegInv = _currSession.FindObject<BeginningInventory>
                                                                                (new GroupOperator(GroupOperatorType.And,
                                                                                 new BinaryOperator("Code", _locBegInvLine.BeginningInventory.Code)));
                                                            if (_locBegInv != null)
                                                            {
                                                                _locBegInv.QtyAvailable = _locBegInv.QtyAvailable + _locInventoryTransferInLot.TQty;
                                                                _locBegInv.Save();
                                                                _locBegInv.Session.CommitTransaction();
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            #endregion LotNumber

                            #region NonLotNumber
                            else
                            {
                                if (_locInvTransInLine.Item != null && _locInvTransInLine.UOM != null && _locInvTransInLine.DUOM != null)
                                {
                                    //for conversion
                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                        (new GroupOperator(GroupOperatorType.And,
                                                         new BinaryOperator("Item", _locInvTransInLine.Item),
                                                         new BinaryOperator("UOM", _locInvTransInLine.UOM),
                                                         new BinaryOperator("DefaultUOM", _locInvTransInLine.DUOM),
                                                         new BinaryOperator("Active", true)));
                                }

                                //for query
                                BeginningInventory _locBeginningInventory = _currSession.FindObject<BeginningInventory>(new GroupOperator(GroupOperatorType.And,
                                                                          new BinaryOperator("Location", _locInvTransInLine.Location)));

                                #region UpdateBeginingInventory
                                if (_locBeginningInventory != null)
                                {
                                    #region code
                                    if (_locInvTransInLine.Location != null) { _locLocationParse = "[Location.Code]=='" + _locInvTransInLine.Location.Code + "'"; }
                                    else { _locLocationParse = ""; }

                                    if (_locInvTransInLine.BinLocation != null && _locInvTransInLine.Location != null)
                                    { _locBinLocationParse = " AND [BinLocation.Code]=='" + _locInvTransInLine.BinLocation.Code + "'"; }
                                    else if (_locInvTransInLine.BinLocation != null && _locInvTransInLine.Location == null)
                                    { _locBinLocationParse = " [BinLocation.Code]=='" + _locInvTransInLine.BinLocation.Code + "'"; }
                                    else { _locBinLocationParse = ""; }

                                    if (_locInvTransInLine.Item != null && (_locInvTransInLine.Location != null || _locInvTransInLine.BinLocation != null))
                                    { _locItemParse = " AND [Item.Code]=='" + _locInvTransInLine.Item.Code + "'"; }
                                    else if (_locInvTransInLine.Item != null && (_locInvTransInLine.Location == null && _locInvTransInLine.BinLocation == null))
                                    { _locItemParse = " [Item.Code]=='" + _locInvTransInLine.Item.Code + "'"; }
                                    else { _locItemParse = ""; }

                                    if (_locInvTransInLine.LocationType != LocationType.None && (_locInvTransInLine.Location != null || _locInvTransInLine.BinLocation != null))
                                    { _locLocationTypeParse = " AND [LocationType]=='" + GetLocationType(_locInvTransInLine.LocationType).ToString() + "'"; }
                                    else if (_locInvTransInLine.LocationType != LocationType.None && _locInvTransInLine.Location == null && _locInvTransInLine.BinLocation == null)
                                    { _locLocationTypeParse = " [LocationType]=='" + GetLocationType(_locInvTransInLine.LocationType).ToString() + "'"; }
                                    else { _locLocationTypeParse = ""; }

                                    if (_locInvTransInLine.StockType != StockType.None && (_locInvTransInLine.Location != null || _locInvTransInLine.BinLocation != null))
                                    { _locStockTypeParse = " AND [StockType]=='" + GetStockType(_locInvTransInLine.StockType).ToString() + "'"; }
                                    else if (_locInvTransInLine.StockType != StockType.None && _locInvTransInLine.Location == null && _locInvTransInLine.BinLocation == null)
                                    { _locStockTypeParse = " [StockType]=='" + GetStockType(_locInvTransInLine.StockType).ToString() + "'"; }
                                    else { _locStockTypeParse = ""; }

                                    if (_locInvTransInLine.StockGroup != StockGroup.None && (_locInvTransInLine.Location != null || _locInvTransInLine.BinLocation != null
                                    || _locInvTransInLine.StockType != StockType.None))
                                    { _locStockGroupParse = " AND [StockGroup]=='" + GetStockGroup(_locInvTransInLine.StockGroup).ToString() + "'"; }
                                    else if (_locInvTransInLine.StockGroup != StockGroup.None && _locInvTransInLine.Location == null && _locInvTransInLine.BinLocation == null
                                        && _locInvTransInLine.StockType == StockType.None)
                                    { _locStockGroupParse = " [StockGroup]=='" + GetStockGroup(_locInvTransInLine.StockGroup).ToString() + "'"; }
                                    else { _locStockGroupParse = ""; }

                                    if (_locInvTransInLine.Location == null && _locInvTransInLine.BinLocation == null && _locInvTransInLine.StockType != StockType.None)
                                    { _locActiveParse = " [Active]=='" + GetActive(true).ToString() + "'"; }
                                    else { _locActiveParse = " AND [Active]=='" + GetActive(true).ToString() + "'"; }

                                    if (_locLocationParse != null || _locBinLocationParse != null || _locItemParse != null || _locLocationTypeParse != null | _locStockTypeParse != null || _locStockGroupParse != null)
                                    {
                                        _fullString = _locLocationParse + _locBinLocationParse + _locItemParse + _locLocationTypeParse + _locStockTypeParse + _locStockGroupParse + _locActiveParse;
                                    }
                                    else
                                    {
                                        _fullString = _locActiveParse;
                                    }
                                    #endregion code

                                    _locBegInventoryLines = new XPCollection<BeginningInventoryLine>(_currSession, CriteriaOperator.Parse(_fullString));

                                    #region UpdateBeginingInventoryLine
                                    if (_locBegInventoryLines != null && _locBegInventoryLines.Count() > 0)
                                    {
                                        foreach (BeginningInventoryLine _locBegInventoryLine in _locBegInventoryLines)
                                        {
                                            if (_locItemUOM != null)
                                            {
                                                if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                                {
                                                    _locInvLineTotal = _locInvTransInLine.Qty * _locItemUOM.DefaultConversion + _locInvTransInLine.DQty;
                                                }
                                                else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                                {
                                                    _locInvLineTotal = _locInvTransInLine.Qty / _locItemUOM.Conversion + _locInvTransInLine.DQty;
                                                }
                                                else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                                {
                                                    _locInvLineTotal = _locInvTransInLine.Qty + _locInvTransInLine.DQty;
                                                }
                                            }

                                            else
                                            {
                                                _locInvLineTotal = _locInvTransInLine.Qty + _locInvTransInLine.DQty;
                                            }

                                            _locBegInventoryLine.QtyAvailable = _locBegInventoryLine.QtyAvailable + _locInvLineTotal;
                                            _locBegInventoryLine.Save();
                                            _locBegInventoryLine.Session.CommitTransaction();

                                            _locBeginningInventory.QtyAvailable = _locBeginningInventory.QtyAvailable + _locInvLineTotal;
                                            _locBeginningInventory.Save();
                                            _locBeginningInventory.Session.CommitTransaction();
                                        }
                                    }
                                    #endregion UpdateBeginingInventoryLine
                                    #region CreateNewBeginingInventoryLine
                                    else
                                    {
                                        if (_locItemUOM != null)
                                        {
                                            if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                            {
                                                _locInvLineTotal = _locInvTransInLine.Qty * _locItemUOM.DefaultConversion + _locInvTransInLine.DQty;
                                            }
                                            else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                            {
                                                _locInvLineTotal = _locInvTransInLine.Qty / _locItemUOM.Conversion + _locInvTransInLine.DQty;
                                            }
                                            else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                            {
                                                _locInvLineTotal = _locInvTransInLine.Qty + _locInvTransInLine.DQty;
                                            }
                                        }
                                        else
                                        {
                                            _locInvLineTotal = _locInvTransInLine.Qty + _locInvTransInLine.DQty;
                                        }

                                        BeginningInventoryLine _locSaveDataBeginingInventoryLine = new BeginningInventoryLine(_currSession)
                                        {
                                            Company = _locInventoryTransferInXPO.Company,
                                            Workplace = _locInventoryTransferInXPO.Workplace,
                                            Item = _locInvTransInLine.Item,
                                            Location = _locInvTransInLine.Location,
                                            BinLocation = _locInvTransInLine.BinLocation,
                                            QtyAvailable = _locInvLineTotal,
                                            DefaultUOM = _locInvTransInLine.DUOM,
                                            LocationType = _locInvTransInLine.LocationType,
                                            StockType = _locInvTransInLine.StockType,
                                            StockGroup = _locInvTransInLine.StockGroup,
                                            Active = true,
                                            BeginningInventory = _locBeginningInventory,
                                        };
                                        _locSaveDataBeginingInventoryLine.Save();
                                        _locSaveDataBeginingInventoryLine.Session.CommitTransaction();

                                        _locBeginningInventory.QtyAvailable = _locBeginningInventory.QtyAvailable + _locInvLineTotal;
                                        _locBeginningInventory.Save();
                                        _locBeginningInventory.Session.CommitTransaction();
                                    }
                                    #endregion CreateNewBeginingInventoryLine
                                }
                                #endregion UpdateBeginingInventory
                            }
                            #endregion NonLotNumber
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = InventoryTransferIn ", ex.ToString());
            }
        }

        private void SetReceiveInventoryJournal(Session _currSession, InventoryTransferIn _locInventoryTransferInXPO)
        {
            try
            {
                XPCollection<InventoryTransferInLine> _locInvTransInLines = new XPCollection<InventoryTransferInLine>(_currSession,
                                                                        new GroupOperator(GroupOperatorType.And,
                                                                        new BinaryOperator("InventoryTransferIn", _locInventoryTransferInXPO),
                                                                        new BinaryOperator("Select", true),
                                                                        new GroupOperator(GroupOperatorType.Or,
                                                                        new BinaryOperator("Status", Status.Progress),
                                                                        new BinaryOperator("Status", Status.Posted))));

                if (_locInvTransInLines != null && _locInvTransInLines.Count > 0)
                {
                    double _locInvLineTotal = 0;
                    DateTime now = DateTime.Now;

                    foreach (InventoryTransferInLine _locInvTransInLine in _locInvTransInLines)
                    {
                        if (_locInvTransInLine.DQty > 0 || _locInvTransInLine.Qty > 0)
                        {
                            XPCollection<InventoryTransferInLot> _locInventoryTransferInLots = new XPCollection<InventoryTransferInLot>
                                                                                    (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                    new BinaryOperator("InventoryTransferInLine", _locInvTransInLine),
                                                                                    new BinaryOperator("Select", true),
                                                                                    new GroupOperator(GroupOperatorType.Or,
                                                                                    new BinaryOperator("Status", Status.Progress),
                                                                                    new BinaryOperator("Status", Status.Posted))));

                            #region LotNumber
                            if (_locInventoryTransferInLots != null && _locInventoryTransferInLots.Count() > 0)
                            {
                                foreach (InventoryTransferInLot _locInventoryTransferInLot in _locInventoryTransferInLots)
                                {
                                    if (_locInventoryTransferInLot.Select == true)
                                    {
                                        if (_locInventoryTransferInLot.BegInvLine != null)
                                        {
                                            if (_locInventoryTransferInLot.BegInvLine.Code != null)
                                            {
                                                BeginningInventoryLine _locBegInvLine = _currSession.FindObject<BeginningInventoryLine>
                                                                                (new GroupOperator(GroupOperatorType.And,
                                                                                 new BinaryOperator("Code", _locInventoryTransferInLot.BegInvLine.Code)));
                                                if (_locBegInvLine != null)
                                                {
                                                    InventoryJournal _locPositiveInventoryJournal = new InventoryJournal(_currSession)
                                                    {
                                                        DocumentType = _locInventoryTransferInXPO.DocumentType,
                                                        DocNo = _locInventoryTransferInXPO.DocNo,
                                                        Location = _locInventoryTransferInLot.Location,
                                                        BinLocation = _locInventoryTransferInLot.BinLocation,
                                                        StockType = _locInventoryTransferInLot.StockType,
                                                        Item = _locInventoryTransferInLot.Item,
                                                        Company = _locInventoryTransferInLot.Company,
                                                        QtyPos = _locInventoryTransferInLot.TQty,
                                                        QtyNeg = 0,
                                                        DUOM = _locInventoryTransferInLot.DUOM,
                                                        JournalDate = now,
                                                        LotNumber = _locInventoryTransferInLot.LotNumber,
                                                        InventoryTransferIn = _locInventoryTransferInXPO,
                                                    };
                                                    _locPositiveInventoryJournal.Save();
                                                    _locPositiveInventoryJournal.Session.CommitTransaction();
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            #endregion LotNumber
                            #region NonLotNumber
                            else
                            {
                                if (_locInvTransInLine.Item != null && _locInvTransInLine.UOM != null && _locInvTransInLine.DUOM != null)
                                {
                                    ItemUnitOfMeasure _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                        (new GroupOperator(GroupOperatorType.And,
                                                         new BinaryOperator("Item", _locInvTransInLine.Item),
                                                         new BinaryOperator("UOM", _locInvTransInLine.UOM),
                                                         new BinaryOperator("DefaultUOM", _locInvTransInLine.DUOM),
                                                         new BinaryOperator("Active", true)));
                                    if (_locItemUOM != null)
                                    {
                                        #region Code
                                        if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locInvTransInLine.Qty * _locItemUOM.DefaultConversion + _locInvTransInLine.DQty;
                                        }
                                        else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locInvTransInLine.Qty / _locItemUOM.Conversion + _locInvTransInLine.DQty;
                                        }
                                        else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locInvTransInLine.Qty + _locInvTransInLine.DQty;
                                        }
                                        #endregion Code

                                        InventoryJournal _locPositiveInventoryJournal = new InventoryJournal(_currSession)
                                        {
                                            DocumentType = _locInventoryTransferInXPO.DocumentType,
                                            DocNo = _locInventoryTransferInXPO.DocNo,
                                            Location = _locInvTransInLine.Location,
                                            BinLocation = _locInvTransInLine.BinLocation,
                                            LocationType = _locInvTransInLine.LocationType,
                                            StockType = _locInvTransInLine.StockType,
                                            StockGroup = _locInvTransInLine.StockGroup,
                                            Item = _locInvTransInLine.Item,
                                            Company = _locInvTransInLine.Company,
                                            Workplace = _locInvTransInLine.Workplace,
                                            QtyPos = _locInvLineTotal,
                                            QtyNeg = 0,
                                            DUOM = _locInvTransInLine.DUOM,
                                            JournalDate = now,
                                            InventoryTransferIn = _locInventoryTransferInXPO,
                                        };
                                        _locPositiveInventoryJournal.Save();
                                        _locPositiveInventoryJournal.Session.CommitTransaction();
                                    }
                                }
                                else
                                {
                                    _locInvLineTotal = _locInvTransInLine.Qty + _locInvTransInLine.DQty;

                                    InventoryJournal _locPositiveInventoryJournal = new InventoryJournal(_currSession)
                                    {
                                        DocumentType = _locInventoryTransferInXPO.DocumentType,
                                        DocNo = _locInventoryTransferInXPO.DocNo,
                                        Location = _locInvTransInLine.Location,
                                        BinLocation = _locInvTransInLine.BinLocation,
                                        LocationType = _locInvTransInLine.LocationType,
                                        StockType = _locInvTransInLine.StockType,
                                        StockGroup = _locInvTransInLine.StockGroup,
                                        Item = _locInvTransInLine.Item,
                                        Company = _locInvTransInLine.Company,
                                        Workplace = _locInvTransInLine.Workplace,
                                        QtyPos = _locInvLineTotal,
                                        QtyNeg = 0,
                                        DUOM = _locInvTransInLine.DUOM,
                                        JournalDate = now,
                                        InventoryTransferIn = _locInventoryTransferInXPO,
                                    };
                                    _locPositiveInventoryJournal.Save();
                                    _locPositiveInventoryJournal.Session.CommitTransaction();
                                }
                            }
                            #endregion NonLotNumber
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = InventoryTransferIn ", ex.ToString());
            }
        }

        #region GoodsReceiveJournal

        private bool CheckJournalSetup(Session _currSession, InventoryTransferIn _locInventoryTransferInXPO)
        {
            bool _result = false;
            try
            {
                if (_locInventoryTransferInXPO != null)
                {
                    if (_locInventoryTransferInXPO.Company != null && _locInventoryTransferInXPO.Workplace != null)
                    {
                        JournalSetupDetail _locJournalSetupDetail = _currSession.FindObject<JournalSetupDetail>
                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                    new BinaryOperator("Company", _locInventoryTransferInXPO.Company),
                                                                    new BinaryOperator("Workplace", _locInventoryTransferInXPO.Workplace),
                                                                    new BinaryOperator("JournalSetupType", JournalSetupType.Journal),
                                                                    new BinaryOperator("ObjectList", ObjectList.InventoryTransferIn),
                                                                    new BinaryOperator("JournalProcess", JournalProcess.JournalGoodReceipt),
                                                                    new BinaryOperator("Active", true)));

                        if (_locJournalSetupDetail != null)
                        {
                            _result = true;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = InventoryTransferIn ", ex.ToString());
            }
            return _result;
        }

        private bool CheckGoodsReceiveJournal(Session _currSession, InventoryTransferIn _locInventoryTransferInXPO)
        {
            bool _result = false;
            try
            {
                if (_locInventoryTransferInXPO != null)
                {
                    if (_locInventoryTransferInXPO.Company != null && _locInventoryTransferInXPO.Workplace != null)
                    {
                        XPCollection<GeneralJournal> _locGeneralJournals = new XPCollection<GeneralJournal>(_currSession,
                                                                        new GroupOperator(GroupOperatorType.And,
                                                                        new BinaryOperator("Company", _locInventoryTransferInXPO.Company),
                                                                        new BinaryOperator("Workplace", _locInventoryTransferInXPO.Workplace),
                                                                        new BinaryOperator("InventoryTransferIn", _locInventoryTransferInXPO)));
                        if (_locGeneralJournals != null && _locGeneralJournals.Count() > 0)
                        {
                            _result = true;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = InventoryTransferIn ", ex.ToString());
            }
            return _result;
        }

        private void SetGoodsReceiveJournal(Session _currSession, InventoryTransferIn _locInventoryTransferInXPO)
        {
            try
            {
                DateTime now = DateTime.Now;
                double _locTotalUnitAmount = 0;
                ItemAccountGroup _locItemAccountGroup = null;

                if (_locInventoryTransferInXPO != null)
                {
                    XPCollection<InventoryTransferInLine> _locInventoryTransferInLines = new XPCollection<InventoryTransferInLine>(_currSession,
                                                                            new GroupOperator(GroupOperatorType.And,
                                                                            new BinaryOperator("InventoryTransferIn", _locInventoryTransferInXPO),
                                                                            new BinaryOperator("StockGroup", StockGroup.Normal),
                                                                            new GroupOperator(GroupOperatorType.Or,
                                                                            new BinaryOperator("Status", Status.Progress),
                                                                            new BinaryOperator("Status", Status.Posted))));

                    if (_locInventoryTransferInLines != null && _locInventoryTransferInLines.Count() > 0)
                    {
                        foreach (InventoryTransferInLine _locInventoryTransferInLine in _locInventoryTransferInLines)
                        {
                            if (_locInventoryTransferInLine.Item != null && _locInventoryTransferInLine.Company != null && _locInventoryTransferInLine.Workplace != null)
                            {
                                //Cari ItemAccountGroup
                                ItemAccount _locItemAccount = _currSession.FindObject<ItemAccount>(new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("Company", _locInventoryTransferInLine.Company),
                                                                new BinaryOperator("Workplace", _locInventoryTransferInLine.Workplace),
                                                                new BinaryOperator("Item", _locInventoryTransferInLine.Item),
                                                                new BinaryOperator("Active", true)));

                                if (_locInventoryTransferInLine.JournalMonth > 0 && _locItemAccount != null)
                                {
                                    if (_locItemAccount.ItemAccountGroup != null)
                                    {
                                        _locItemAccountGroup = _locItemAccount.ItemAccountGroup;
                                    }

                                    if (_locInventoryTransferInLine.PurchaseOrderMonitoring.PurchaseOrderLine != null)
                                    {
                                        if (_locInventoryTransferInLine.PurchaseOrderMonitoring.PurchaseOrderLine.UAmount > 0)
                                        {
                                            _locTotalUnitAmount = _locInventoryTransferInLine.PurchaseOrderMonitoring.PurchaseOrderLine.UAmount * _locInventoryTransferInLine.TQty;
                                        }
                                    }

                                    #region JournalMapByItems

                                    XPCollection<JournalMap> _locJournalMapItems = new XPCollection<JournalMap>
                                                                                   (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                    new BinaryOperator("Company", _locInventoryTransferInLine.Company),
                                                                                    new BinaryOperator("Workplace", _locInventoryTransferInLine.Workplace),
                                                                                    new BinaryOperator("ItemAccountGroup", _locItemAccountGroup),
                                                                                    new BinaryOperator("Active", true)));

                                    if (_locJournalMapItems != null && _locJournalMapItems.Count() > 0)
                                    {
                                        foreach (JournalMap _locJournalMapItem in _locJournalMapItems)
                                        {
                                            XPCollection<JournalMapLine> _locJournalMapLines = new XPCollection<JournalMapLine>
                                                                                               (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                new BinaryOperator("Company", _locInventoryTransferInLine.Company),
                                                                                                new BinaryOperator("Workplace", _locInventoryTransferInLine.Workplace),
                                                                                                new BinaryOperator("JournalMap", _locJournalMapItem),
                                                                                                new BinaryOperator("Active", true)));

                                            if (_locJournalMapLines != null && _locJournalMapLines.Count() > 0)
                                            {
                                                foreach (JournalMapLine _locJournalMapLine in _locJournalMapLines)
                                                {
                                                    if (_locJournalMapLine.AccountMap != null)
                                                    {
                                                        if (_locJournalMapLine.AccountMap.PostingType == PostingType.Purchase && _locJournalMapLine.AccountMap.OrderType == OrderType.Item
                                                            && _locJournalMapLine.AccountMap.PostingMethod == PostingMethod.Receipt && _locJournalMapLine.AccountMap.PostingMethodType == PostingMethodType.Normal)
                                                        {
                                                            XPCollection<AccountMapLine> _locAccountMapLines = new XPCollection<AccountMapLine>
                                                                                                           (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                            new BinaryOperator("Company", _locInventoryTransferInLine.Company),
                                                                                                            new BinaryOperator("Workplace", _locInventoryTransferInLine.Workplace),
                                                                                                            new BinaryOperator("AccountMap", _locJournalMapLine.AccountMap),
                                                                                                            new BinaryOperator("Active", true)));

                                                            if (_locAccountMapLines != null && _locAccountMapLines.Count() > 0)
                                                            {
                                                                double _locTotalAmountDebit = 0;
                                                                double _locTotalAmountCredit = 0;
                                                                double _locTotalBalance = 0;

                                                                foreach (AccountMapLine _locAccountMapLine in _locAccountMapLines)
                                                                {
                                                                    if (_locAccountMapLine.AccountCharge == AccountCharge.Debit)
                                                                    {
                                                                        _locTotalAmountDebit = _locTotalUnitAmount;
                                                                    }
                                                                    if (_locAccountMapLine.AccountCharge == AccountCharge.Credit)
                                                                    {
                                                                        _locTotalAmountCredit = _locTotalUnitAmount;
                                                                    }

                                                                    GeneralJournal _saveGeneralJournal = new GeneralJournal(_currSession)
                                                                    {
                                                                        Company = _locInventoryTransferInLine.Company,
                                                                        Workplace = _locInventoryTransferInLine.Workplace,
                                                                        PostingDate = now,
                                                                        PostingType = PostingType.Purchase,
                                                                        OrderType = OrderType.Item,
                                                                        PostingMethod = PostingMethod.Receipt,
                                                                        PostingMethodType = PostingMethodType.Normal,
                                                                        Account = _locAccountMapLine.Account,
                                                                        Debit = _locTotalAmountDebit,
                                                                        Credit = _locTotalAmountCredit,
                                                                        JournalMonth = _locInventoryTransferInLine.JournalMonth,
                                                                        JournalYear = _locInventoryTransferInLine.JournalYear,
                                                                        InventoryTransferIn = _locInventoryTransferInXPO,
                                                                    };

                                                                    _saveGeneralJournal.Save();
                                                                    _saveGeneralJournal.Session.CommitTransaction();

                                                                    #region AccountingPeriodicLine

                                                                    if (_locAccountMapLine.Account.Code != null)
                                                                    {
                                                                        //Cari Account Periodic Line
                                                                        AccountingPeriodicLine _locAPL = _currSession.FindObject<AccountingPeriodicLine>
                                                                                                        (new GroupOperator(GroupOperatorType.And,
                                                                                                        new BinaryOperator("Company", _locInventoryTransferInLine.Company),
                                                                                                        new BinaryOperator("Workplace", _locInventoryTransferInLine.Workplace),
                                                                                                        new BinaryOperator("AccountNo", _locAccountMapLine.Account),
                                                                                                        new BinaryOperator("Month", _locInventoryTransferInLine.JournalMonth),
                                                                                                        new BinaryOperator("Year", _locInventoryTransferInLine.JournalYear)));
                                                                        if (_locAPL != null)
                                                                        {
                                                                            if (_locAPL.AccountNo != null)
                                                                            {
                                                                                if (_locAPL.AccountNo.BalanceType == BalanceType.Change)
                                                                                {
                                                                                    if (_locAPL.AccountNo.AccountCharge == AccountCharge.Debit)
                                                                                    {
                                                                                        if (_locTotalAmountDebit > 0)
                                                                                        {
                                                                                            _locTotalBalance = _locAPL.Balance + _locTotalAmountDebit;
                                                                                        }
                                                                                        if (_locTotalAmountCredit > 0)
                                                                                        {
                                                                                            _locTotalBalance = _locAPL.Balance - _locTotalAmountCredit;
                                                                                        }
                                                                                    }
                                                                                    if (_locAPL.AccountNo.AccountCharge == AccountCharge.Credit)
                                                                                    {
                                                                                        if (_locTotalAmountDebit > 0)
                                                                                        {
                                                                                            _locTotalBalance = _locAPL.Balance - _locTotalAmountDebit;
                                                                                        }
                                                                                        if (_locTotalAmountCredit > 0)
                                                                                        {
                                                                                            _locTotalBalance = _locAPL.Balance + _locTotalAmountCredit;
                                                                                        }
                                                                                    }
                                                                                }

                                                                                _locAPL.Balance = _locTotalBalance;
                                                                                _locAPL.Debit = _locAPL.Debit + _locTotalAmountDebit;
                                                                                _locAPL.Credit = _locAPL.Credit + _locTotalAmountCredit;
                                                                                _locAPL.Save();
                                                                                _locAPL.Session.CommitTransaction();

                                                                            }
                                                                        }
                                                                    }
                                                                    #endregion AccountingPeriodicLine
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    #endregion JournalMapByItems
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = InventoryTransferIn ", ex.ToString());
            }
        }

        #endregion GoodsReceiveJournal

        private void SetStatusReceiveInventoryTransferInLine(Session _currSession, InventoryTransferIn _locInventoryTransferInXPO)
        {
            try
            {
                DateTime now = DateTime.Now;
                if (_locInventoryTransferInXPO != null)
                {
                    XPCollection<InventoryTransferInLine> _locInventoryTransferInLines = new XPCollection<InventoryTransferInLine>(_currSession,
                                                            new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("InventoryTransferIn", _locInventoryTransferInXPO),
                                                            new BinaryOperator("Select", true),
                                                            new GroupOperator(GroupOperatorType.Or,
                                                            new BinaryOperator("Status", Status.Progress),
                                                            new BinaryOperator("Status", Status.Posted))));

                    if (_locInventoryTransferInLines != null && _locInventoryTransferInLines.Count > 0)
                    {
                        foreach (InventoryTransferInLine _locInventoryTransferInLine in _locInventoryTransferInLines)
                        {
                            _locInventoryTransferInLine.Status = Status.Close;
                            _locInventoryTransferInLine.ActivationPosting = true;
                            _locInventoryTransferInLine.StatusDate = now;
                            _locInventoryTransferInLine.ActualDate = now;
                            _locInventoryTransferInLine.Save();
                            _locInventoryTransferInLine.Session.CommitTransaction();

                            XPCollection<InventoryTransferInLot> _locInventoryTransferInLots = new XPCollection<InventoryTransferInLot>(_currSession,
                                                                                     new GroupOperator(GroupOperatorType.And,
                                                                                     new BinaryOperator("InventoryTransferInLine", _locInventoryTransferInLine),
                                                                                     new BinaryOperator("Select", true),
                                                                                     new GroupOperator(GroupOperatorType.Or,
                                                                                     new BinaryOperator("Status", Status.Progress),
                                                                                     new BinaryOperator("Status", Status.Posted))));

                            if (_locInventoryTransferInLots != null && _locInventoryTransferInLots.Count() > 0)
                            {
                                foreach (InventoryTransferInLot _locInventoryTransferInLot in _locInventoryTransferInLots)
                                {
                                    _locInventoryTransferInLot.Status = Status.Close;
                                    _locInventoryTransferInLot.ActivationPosting = true;
                                    _locInventoryTransferInLot.StatusDate = now;
                                    _locInventoryTransferInLot.Save();
                                    _locInventoryTransferInLot.Session.CommitTransaction();
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = InventoryTransferIn " + ex.ToString());
            }
        }

        private void SetNormal(Session _currSession, InventoryTransferIn _locInventoryTransferInXPO)
        {
            try
            {
                if (_locInventoryTransferInXPO != null)
                {
                    XPCollection<InventoryTransferInLine> _locInventoryTransferInLines = new XPCollection<InventoryTransferInLine>(_currSession,
                                                                                     new GroupOperator(GroupOperatorType.And,
                                                                                     new BinaryOperator("InventoryTransferIn", _locInventoryTransferInXPO),
                                                                                     new BinaryOperator("Select", true),
                                                                                     new GroupOperator(GroupOperatorType.Or,
                                                                                     new BinaryOperator("Status", Status.Progress),
                                                                                     new BinaryOperator("Status", Status.Posted),
                                                                                     new BinaryOperator("Status", Status.Close))));

                    if (_locInventoryTransferInLines != null && _locInventoryTransferInLines.Count > 0)
                    {
                        foreach (InventoryTransferInLine _locInventoryTransferInLine in _locInventoryTransferInLines)
                        {
                            _locInventoryTransferInLine.Select = false;
                            _locInventoryTransferInLine.Save();
                            _locInventoryTransferInLine.Session.CommitTransaction();

                            XPCollection<InventoryTransferInLot> _locInventoryTransferInLots = new XPCollection<InventoryTransferInLot>(_currSession,
                                                                                 new GroupOperator(GroupOperatorType.And,
                                                                                 new BinaryOperator("InventoryTransferInLine", _locInventoryTransferInLine),
                                                                                 new BinaryOperator("Select", true),
                                                                                 new GroupOperator(GroupOperatorType.Or,
                                                                                 new BinaryOperator("Status", Status.Progress),
                                                                                 new BinaryOperator("Status", Status.Posted))));

                            if (_locInventoryTransferInLots != null && _locInventoryTransferInLots.Count() > 0)
                            {
                                foreach (InventoryTransferInLot _locInventoryTransferInLot in _locInventoryTransferInLots)
                                {
                                    _locInventoryTransferInLot.Select = false;
                                    _locInventoryTransferInLot.Save();
                                    _locInventoryTransferInLot.Session.CommitTransaction();
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = InventoryTransferIn " + ex.ToString());
            }
        }

        private void SetFinalStatusReceiveInventoryTransferIn(Session _currSession, InventoryTransferIn _locInventoryTransferInXPO)
        {
            try
            {
                DateTime now = DateTime.Now;

                if (_locInventoryTransferInXPO != null)
                {
                    XPCollection<InventoryTransferInLine> _locInventoryTransferInLines = new XPCollection<InventoryTransferInLine>(_currSession,
                                                                                     new GroupOperator(GroupOperatorType.And,
                                                                                     new BinaryOperator("InventoryTransferIn", _locInventoryTransferInXPO)));

                    if (_locInventoryTransferInLines != null && _locInventoryTransferInLines.Count() > 0)
                    {
                        foreach (InventoryTransferInLine _locInventoryTransferInLine in _locInventoryTransferInLines)
                        {
                            if (_locInventoryTransferInLine.Status == Status.Close)
                            {
                                _locInventoryTransferInXPO.ActivationPosting = true;
                                _locInventoryTransferInXPO.Status = Status.Close;
                                _locInventoryTransferInXPO.StatusDate = now;
                                _locInventoryTransferInXPO.Save();
                                _locInventoryTransferInXPO.Session.CommitTransaction();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = InventoryTransferIn" + ex.ToString());
            }
        }       

        #endregion Method Posting

        #region Global Method

        private void SuccessMessageShow(string _locMessage)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 4000;
            options.Message = string.Format("{0}", _locMessage);
            options.Type = InformationType.Success;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Success";
            options.Win.Type = WinMessageType.Flyout;
            Application.ShowViewStrategy.ShowMessage(options);
        }

        private void ErrorMessageShow(string _locMessage)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 2000;
            options.Message = string.Format(_locMessage);
            options.Type = InformationType.Warning;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Error";
            options.Win.Type = WinMessageType.Alert;
            Application.ShowViewStrategy.ShowMessage(options);
        }

        private int GetStockType(StockType objectName)
        {
            int _result = 0;
            try
            {
                if (objectName == StockType.Good)
                {
                    _result = 1;
                }
                else if (objectName == StockType.Bad)
                {
                    _result = 2;
                }
                else
                {
                    _result = 0;
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Module = InventoryTransferAction " + ex.ToString());
            }
            return _result;
        }

        private int GetStockGroup(StockGroup objectName)
        {
            int _result = 0;
            try
            {
                if (objectName == StockGroup.Normal)
                {
                    _result = 1;
                }
                else if (objectName == StockGroup.FreeItem)
                {
                    _result = 2;
                }
                else
                {
                    _result = 0;
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Module = InventoryTransferIn " + ex.ToString());
            }
            return _result;
        }

        private int GetActive(bool objectName)
        {
            int _result = 0;
            try
            {
                if (objectName == true)
                {
                    _result = 1;
                }
                else
                {
                    _result = 0;
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Module = InventoryTransferAction " + ex.ToString());
            }
            return _result;
        }

        private int GetLocationType(LocationType objectName)
        {
            int _result = 0;
            try
            {
                if (objectName == LocationType.Main)
                {
                    _result = 1;
                }
                else if (objectName == LocationType.Transit)
                {
                    _result = 2;
                }
                else if (objectName == LocationType.Site)
                {
                    _result = 3;
                }
                else if (objectName == LocationType.Vehicle)
                {
                    _result = 4;
                }
                else
                {
                    _result = 0;
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Module = InventoryTransferAction " + ex.ToString());
            }
            return _result;
        }

        private double GetUnitPriceByPurchaseOrder(Session _currSession, PurchaseOrder _locPurchaseOrder, Item _locItem)
        {
            double _result = 0;
            try
            {
                if (_locPurchaseOrder != null)
                {
                    PurchaseOrderLine _locPurchaseOrderLine = _currSession.FindObject<PurchaseOrderLine>
                                                            (new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("PurchaseOrder", _locPurchaseOrder),
                                                            new BinaryOperator("Item", _locItem)));
                    if (_locPurchaseOrderLine != null)
                    {
                        _result = _locPurchaseOrderLine.UAmount;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = InventoryTransferIn " + ex.ToString());
            }
            return _result;
        }

        #endregion Global Method

    }
}
