﻿namespace FullDrive.Module.Controllers
{
    partial class OrderCollectionActionController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.OrderCollectionGetSIAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.OrderCollectionProgressAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.OrderCollectionPostingAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.OrderCollectionListviewFilterSelectionAction = new DevExpress.ExpressApp.Actions.SingleChoiceAction(this.components);
            // 
            // OrderCollectionGetSIAction
            // 
            this.OrderCollectionGetSIAction.Caption = "Get SI";
            this.OrderCollectionGetSIAction.ConfirmationMessage = null;
            this.OrderCollectionGetSIAction.Id = "OrderCollectionGetSIActionId";
            this.OrderCollectionGetSIAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.OrderCollection);
            this.OrderCollectionGetSIAction.ToolTip = null;
            this.OrderCollectionGetSIAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.OrderCollectionGetSIAction_Execute);
            // 
            // OrderCollectionProgressAction
            // 
            this.OrderCollectionProgressAction.Caption = "Progress";
            this.OrderCollectionProgressAction.ConfirmationMessage = null;
            this.OrderCollectionProgressAction.Id = "OrderCollectionProgressActionId";
            this.OrderCollectionProgressAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.OrderCollection);
            this.OrderCollectionProgressAction.ToolTip = null;
            this.OrderCollectionProgressAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.OrderCollectionProgressAction_Execute);
            // 
            // OrderCollectionPostingAction
            // 
            this.OrderCollectionPostingAction.Caption = "Posting";
            this.OrderCollectionPostingAction.ConfirmationMessage = null;
            this.OrderCollectionPostingAction.Id = "OrderCollectionPostingActionId";
            this.OrderCollectionPostingAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.OrderCollection);
            this.OrderCollectionPostingAction.ToolTip = null;
            this.OrderCollectionPostingAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.OrderCollectionPostingAction_Execute);
            // 
            // OrderCollectionListviewFilterSelectionAction
            // 
            this.OrderCollectionListviewFilterSelectionAction.Caption = "Filter";
            this.OrderCollectionListviewFilterSelectionAction.ConfirmationMessage = null;
            this.OrderCollectionListviewFilterSelectionAction.Id = "OrderCollectionListviewFilterSelectionActionId";
            this.OrderCollectionListviewFilterSelectionAction.ItemType = DevExpress.ExpressApp.Actions.SingleChoiceActionItemType.ItemIsOperation;
            this.OrderCollectionListviewFilterSelectionAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.OrderCollection);
            this.OrderCollectionListviewFilterSelectionAction.ToolTip = null;
            this.OrderCollectionListviewFilterSelectionAction.Execute += new DevExpress.ExpressApp.Actions.SingleChoiceActionExecuteEventHandler(this.OrderCollectionListviewFilterSelectionAction_Execute);
            // 
            // OrderCollectionActionController
            // 
            this.Actions.Add(this.OrderCollectionGetSIAction);
            this.Actions.Add(this.OrderCollectionProgressAction);
            this.Actions.Add(this.OrderCollectionPostingAction);
            this.Actions.Add(this.OrderCollectionListviewFilterSelectionAction);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SimpleAction OrderCollectionGetSIAction;
        private DevExpress.ExpressApp.Actions.SimpleAction OrderCollectionProgressAction;
        private DevExpress.ExpressApp.Actions.SimpleAction OrderCollectionPostingAction;
        private DevExpress.ExpressApp.Actions.SingleChoiceAction OrderCollectionListviewFilterSelectionAction;
    }
}
