﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;

using FullDrive.Module.BusinessObjects;
using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.Xpo;
using System.Collections;
using DevExpress.Xpo;
using System.IO;
using System.Web;

namespace FullDrive.Module.Controllers
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class AmendDeliveryActionController : ViewController
    {
        private ChoiceActionItem _selectionListviewFilter;

        public AmendDeliveryActionController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.
            #region FilterStatus
            AmendDeliveryListviewFilterSelectionAction.Items.Clear();
            foreach (object _currApproval in Enum.GetValues(typeof(CustomProcess.Status)))
            {
                EnumDescriptor _ed = new EnumDescriptor(typeof(CustomProcess.Status));
                _selectionListviewFilter = new ChoiceActionItem(_ed.GetCaption(_currApproval), _currApproval);
                AmendDeliveryListviewFilterSelectionAction.Items.Add(_selectionListviewFilter);
            }
            #endregion FilterStatus
        }

        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }
        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        private void AmendDeliveryGetStockAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;

                if (_objectsToProcess != null)
                {
                    foreach (Object obj in _objectsToProcess)
                    {
                        AmendDelivery _locAmendDeliveryOS = (AmendDelivery)_objectSpace.GetObject(obj);

                        if (_locAmendDeliveryOS != null)
                        {
                            if (_locAmendDeliveryOS.Session != null)
                            {
                                _currSession = _locAmendDeliveryOS.Session;
                            }

                            if (_locAmendDeliveryOS.Code != null && _currSession != null)
                            {
                                _currObjectId = _locAmendDeliveryOS.Code;

                                AmendDelivery _locAmendDeliveryXPO = _currSession.FindObject<AmendDelivery>
                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                     new BinaryOperator("Code", _currObjectId),
                                                                     new GroupOperator(GroupOperatorType.Or,
                                                                     new BinaryOperator("Status", Status.Open),
                                                                     new BinaryOperator("Status", Status.Progress))));

                                if (_locAmendDeliveryXPO != null)
                                {
                                    GetPickingMonitoring(_currSession, _locAmendDeliveryXPO);
                                    SuccessMessageShow("SalesInvoice Has Been Successfully Getting into AmendDelivery");
                                }
                                else
                                {
                                    ErrorMessageShow("Data SalesInvoice Not Available");
                                }
                            }
                            else
                            {
                                ErrorMessageShow("Data SalesInvoice Not Available");
                            }
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = AmendDelivery " + ex.ToString());
            }

        }

        private void AmendDeliveryProgressAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;
                Status _locStatus = Status.Open;

                if (_objectsToProcess != null)
                {
                    foreach (Object obj in _objectsToProcess)
                    {
                        AmendDelivery _locAmendDeliveryOS = (AmendDelivery)_objectSpace.GetObject(obj);

                        if (_locAmendDeliveryOS != null)
                        {
                            if (_locAmendDeliveryOS.Session != null)
                            {
                                _currSession = _locAmendDeliveryOS.Session;
                            }

                            if (_locAmendDeliveryOS.Code != null && _currSession != null)
                            {
                                _currObjectId = _locAmendDeliveryOS.Code;

                                AmendDelivery _locAmendDeliveryXPO = _currSession.FindObject<AmendDelivery>
                                                                   (new GroupOperator(GroupOperatorType.And,
                                                                    new BinaryOperator("Code", _currObjectId),
                                                                    new GroupOperator(GroupOperatorType.Or,
                                                                    new BinaryOperator("Status", Status.Open),
                                                                    new BinaryOperator("Status", Status.Progress))));

                                if (_locAmendDeliveryXPO != null)
                                {
                                    if (_locAmendDeliveryXPO.Status == Status.Open)
                                    {
                                        _locStatus = Status.Progress;

                                    }
                                    else
                                    {
                                        _locStatus = _locAmendDeliveryXPO.Status;
                                    }

                                    _locAmendDeliveryXPO.Status = _locStatus;
                                    _locAmendDeliveryXPO.StatusDate = now;
                                    _locAmendDeliveryXPO.Save();
                                    _locAmendDeliveryXPO.Session.CommitTransaction();

                                    #region SalesInvoiceLine
                                    XPCollection<AmendDeliveryLine> _locAmendDeliveryLines = new XPCollection<AmendDeliveryLine>
                                                                                           (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                            new BinaryOperator("AmendDelivery", _locAmendDeliveryXPO)));

                                    if (_locAmendDeliveryLines != null && _locAmendDeliveryLines.Count > 0)
                                    {
                                        foreach (AmendDeliveryLine _locAmendDeliveryLine in _locAmendDeliveryLines)
                                        {
                                            if (_locAmendDeliveryLine.Status == Status.Open)
                                            {
                                                _locAmendDeliveryLine.Status = Status.Progress;
                                                _locAmendDeliveryLine.StatusDate = now;
                                                _locAmendDeliveryLine.ActivationPosting = true;
                                                _locAmendDeliveryLine.Save();
                                                _locAmendDeliveryLine.Session.CommitTransaction();
                                            }
                                        }
                                    }
                                    #endregion SalesInvoiceLine
                                }
                                else
                                {
                                    ErrorMessageShow("Data SalesInvoice Not Available");
                                }
                            }
                            else
                            {
                                ErrorMessageShow("Data SalesInvoice Not Available");
                            }
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = AmendDelivery " + ex.ToString());
            }
        }

        private void AmendDeliveryListviewFilterSelectionAction_Execute(object sender, SingleChoiceActionExecuteEventArgs e)
        {
            try
            {
                if ((View is ListView) & (View.ObjectTypeInfo.Type == typeof(AmendDelivery)))
                {
                    if ((Status)e.SelectedChoiceActionItem.Data == Status.None)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.None, BinaryOperatorType.NotEqual);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Open)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Open, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Progress)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Progress, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Posted)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Posted, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Lock)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Lock, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Close)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Close, BinaryOperatorType.Equal);
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = AmendDelivery " + ex.ToString());
            }
        }

        private void AmendDeliverySetCollectionAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {

                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;

                if (_objectsToProcess != null)
                {
                    foreach (Object obj in _objectsToProcess)
                    {
                        AmendDelivery _locAmendDeliveryOS = (AmendDelivery)_objectSpace.GetObject(obj);

                        if (_locAmendDeliveryOS != null)
                        {
                            if (_locAmendDeliveryOS.Session != null)
                            {
                                _currSession = _locAmendDeliveryOS.Session;
                            }

                            if (_locAmendDeliveryOS.Code != null && _currSession != null)
                            {
                                _currObjectId = _locAmendDeliveryOS.Code;

                                AmendDelivery _locAmendDeliveryXPO = _currSession.FindObject<AmendDelivery>
                                                                   (new GroupOperator(GroupOperatorType.And,
                                                                    new BinaryOperator("Code", _currObjectId),
                                                                    new GroupOperator(GroupOperatorType.Or,
                                                                    new BinaryOperator("Status", Status.Progress),
                                                                    new BinaryOperator("Status", Status.Posted))));

                                if (_locAmendDeliveryXPO != null)
                                {
                                    SetCollectionAmountInAmendDeliveryLine(_currSession, _locAmendDeliveryXPO);
                                    SetGrandTotalAmount(_currSession, _locAmendDeliveryXPO);
                                }
                                else
                                {
                                    ErrorMessageShow("Data SalesInvoice Not Available");
                                }
                            }
                            else
                            {
                                ErrorMessageShow("Data SalesInvoice Not Available");
                            }
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = AmendDelivery " + ex.ToString());
            }
        }

        private void AmendDeliveryPostingAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {

                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;

                if (_objectsToProcess != null)
                {
                    foreach (Object obj in _objectsToProcess)
                    {
                        AmendDelivery _locAmendDeliveryOS = (AmendDelivery)_objectSpace.GetObject(obj);

                        if (_locAmendDeliveryOS != null)
                        {
                            if (_locAmendDeliveryOS.Session != null)
                            {
                                _currSession = _locAmendDeliveryOS.Session;
                            }

                            if (_locAmendDeliveryOS.Code != null && _currSession != null)
                            {
                                _currObjectId = _locAmendDeliveryOS.Code;

                                AmendDelivery _locAmendDeliveryXPO = _currSession.FindObject<AmendDelivery>
                                                                   (new GroupOperator(GroupOperatorType.And,
                                                                    new BinaryOperator("Code", _currObjectId),
                                                                    new GroupOperator(GroupOperatorType.Or,
                                                                    new BinaryOperator("Status", Status.Progress),
                                                                    new BinaryOperator("Status", Status.Posted))));

                                if (_locAmendDeliveryXPO != null)
                                {
                                    SetPickingMonitoring(_currSession, _locAmendDeliveryXPO);
                                    SetPicking(_currSession, _locAmendDeliveryXPO);
                                    if (CheckJournalSetup(_currSession, _locAmendDeliveryXPO) == true)
                                    {
                                        SetReturnSalesJournal(_currSession, _locAmendDeliveryXPO);
                                    }
                                    SetStatusAmendDeliveryLine(_currSession, _locAmendDeliveryXPO);
                                    SetAmendDeliveryMonitoring(_currSession, _locAmendDeliveryXPO);
                                    SetNormalAmendDeliveryLine(_currSession, _locAmendDeliveryXPO);
                                    SetFinalAmendDelivery(_currSession, _locAmendDeliveryXPO);
                                    SuccessMessageShow(_locAmendDeliveryXPO.Code + " has been change successfully to Posted");
                                }
                                else
                                {
                                    ErrorMessageShow("Data AmendDelivery Not Available");
                                }
                            }
                            else
                            {
                                ErrorMessageShow("Data AmendDelivery Not Available");
                            }
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = AmendDelivery " + ex.ToString());
            }
        }

        //=============================================== Code In Here =============================================

        #region GetSIBasedOnRouteInvoice

        private void GetPickingMonitoring(Session _currSession, AmendDelivery _locAmendDeliveryXPO)
        {
            try
            {

                DateTime now = DateTime.Now;

                if (_locAmendDeliveryXPO != null)
                {
                    if (_locAmendDeliveryXPO.Picking != null)
                    {
                        //Hanya memindahkan Route Invoice ke OrderCollection Line                        
                        XPCollection<PickingMonitoring> _locPickingMonitorings = new XPCollection<PickingMonitoring>
                                                                                            (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                            new BinaryOperator("Picking", _locAmendDeliveryXPO.Picking),
                                                                                            new BinaryOperator("CollectStatus", CollectStatus.NotReady),
                                                                                            new BinaryOperator("AD", true)));

                        if (_locPickingMonitorings != null && _locPickingMonitorings.Count() > 0)
                        {
                            SalesInvoiceCmp _iniSalesInvoiceCmp = null;

                            foreach (PickingMonitoring _locPickingMonitoring in _locPickingMonitorings)
                            {
                                if (_locPickingMonitoring.AmendDelivery == null && _locPickingMonitoring.SalesInvoice != null)
                                {
                                    #region SalesInvoiceCmp
                                    SalesInvoiceCmp _locSalesInvoiceCmp = _currSession.FindObject<SalesInvoiceCmp>
                                                                        (new GroupOperator (GroupOperatorType.And, 
                                                                        new BinaryOperator("SalesInvoice", _locPickingMonitoring.SalesInvoice)));
                                    if(_locSalesInvoiceCmp == null)
                                    {
                                        SalesInvoiceCmp _saveDateSalesInvoiceCmp = new SalesInvoiceCmp(_currSession)
                                        {
                                            SalesInvoice = _locPickingMonitoring.SalesInvoice,
                                            Company = _locAmendDeliveryXPO.Company,
                                            Workplace = _locAmendDeliveryXPO.Workplace,
                                            Division = _locAmendDeliveryXPO.Division,
                                            Department = _locAmendDeliveryXPO.Department,
                                            Section = _locAmendDeliveryXPO.Section,
                                            Employee = _locAmendDeliveryXPO.Employee,
                                            PIC = _locPickingMonitoring.SalesInvoice.PIC,
                                            ETD = _locPickingMonitoring.SalesInvoice.ETD,
                                            ETA = _locPickingMonitoring.SalesInvoice.ETA,
                                            SalesToCustomer = _locPickingMonitoring.SalesInvoice.SalesToCustomer,
                                            SalesToContact = _locPickingMonitoring.SalesInvoice.SalesToContact,
                                            SalesToCountry = _locPickingMonitoring.SalesInvoice.SalesToCountry,
                                            SalesToCity = _locPickingMonitoring.SalesInvoice.SalesToCity,
                                            SalesToAddress = _locPickingMonitoring.SalesInvoice.SalesToAddress,
                                            BillToCustomer = _locPickingMonitoring.SalesInvoice.BillToCustomer,
                                            BillToContact = _locPickingMonitoring.SalesInvoice.BillToContact,
                                            BillToCountry = _locPickingMonitoring.SalesInvoice.BillToCountry,
                                            BillToCity = _locPickingMonitoring.SalesInvoice.BillToCity,
                                            BillToAddress = _locPickingMonitoring.SalesInvoice.BillToAddress,
                                            Currency = _locPickingMonitoring.SalesInvoice.Currency,
                                            PriceGroup = _locPickingMonitoring.SalesInvoice.PriceGroup,
                                            PaymentMethod = _locPickingMonitoring.SalesInvoice.PaymentMethod,
                                            PaymentMethodType = _locPickingMonitoring.SalesInvoice.PaymentMethodType,
                                            PaymentType = _locPickingMonitoring.SalesInvoice.PaymentType,
                                            TOP = _locPickingMonitoring.SalesInvoice.TOP,
                                            DueDate = _locPickingMonitoring.SalesInvoice.DueDate,
                                            CompanyBankAccount = _locPickingMonitoring.SalesInvoice.CompanyBankAccount,
                                            CompanyAccountNo = _locPickingMonitoring.SalesInvoice.CompanyAccountNo,
                                            CompanyAccountName = _locPickingMonitoring.SalesInvoice.CompanyAccountName,
                                            BankAccount = _locPickingMonitoring.SalesInvoice.BankAccount,
                                            AccountNo = _locPickingMonitoring.SalesInvoice.AccountNo,
                                            AccountName = _locPickingMonitoring.SalesInvoice.AccountName,
                                            Description = _locPickingMonitoring.SalesInvoice.Description,
                                            DocDate = _locPickingMonitoring.SalesInvoice.DocDate,
                                            JournalMonth = _locPickingMonitoring.SalesInvoice.JournalMonth,
                                            JournalYear = _locPickingMonitoring.SalesInvoice.JournalYear,
                                            DiscountRule = _locPickingMonitoring.SalesInvoice.DiscountRule,
                                            AccountingPeriodicLine = _locPickingMonitoring.SalesInvoice.AccountingPeriodicLine,
                                            OldAmountDisc = _locPickingMonitoring.SalesInvoice.AmountDisc,
                                        };
                                        _saveDateSalesInvoiceCmp.Save();
                                        _saveDateSalesInvoiceCmp.Session.CommitTransaction();

                                        SalesInvoiceCmp _locSalesInvoiceCmp2 = _currSession.FindObject<SalesInvoiceCmp>
                                                                        (new GroupOperator(GroupOperatorType.And,
                                                                        new BinaryOperator("SalesInvoice", _locPickingMonitoring.SalesInvoice)));
                                        if(_locSalesInvoiceCmp2 != null)
                                        {
                                            _iniSalesInvoiceCmp = _locSalesInvoiceCmp2;
                                            XPCollection<SalesInvoiceMonitoring> _locSalesInvoiceMonitorings = new XPCollection<SalesInvoiceMonitoring>
                                                                                                                (_currSession, new GroupOperator(GroupOperatorType.And, 
                                                                                                                new BinaryOperator("SalesInvoice", _locPickingMonitoring.SalesInvoice)));
                                            if (_locSalesInvoiceMonitorings != null && _locSalesInvoiceMonitorings.Count() > 0)
                                            {
                                                foreach(SalesInvoiceMonitoring _locSalesInvoiceMonitoring in _locSalesInvoiceMonitorings)
                                                {

                                                    if(_locSalesInvoiceMonitoring.StockGroup == StockGroup.Normal)
                                                    {
                                                        #region SalesInvoiceLineCmp
                                                        SalesInvoiceLineCmp _saveDataSalesInvoiceLineCmp = new SalesInvoiceLineCmp(_currSession)
                                                        {
                                                            Company = _locAmendDeliveryXPO.Company,
                                                            Workplace = _locAmendDeliveryXPO.Workplace,
                                                            Division = _locAmendDeliveryXPO.Division,
                                                            Department = _locAmendDeliveryXPO.Department,
                                                            Section = _locAmendDeliveryXPO.Section,
                                                            Employee = _locAmendDeliveryXPO.Employee,
                                                            SalesType = _locSalesInvoiceMonitoring.SalesType,
                                                            Item = _locSalesInvoiceMonitoring.Item,
                                                            Description = _locSalesInvoiceMonitoring.Description,
                                                            DQty = _locSalesInvoiceMonitoring.DlvPDQty,
                                                            DUOM = _locSalesInvoiceMonitoring.DlvPDUOM,
                                                            Qty = _locSalesInvoiceMonitoring.DlvPQty,
                                                            UOM = _locSalesInvoiceMonitoring.DlvPUOM,
                                                            TQty = _locSalesInvoiceMonitoring.DlvPTQty,
                                                            StockType = _locSalesInvoiceMonitoring.StockType,
                                                            Currency = _locSalesInvoiceCmp2.Currency,
                                                            PriceGroup = _locSalesInvoiceMonitoring.SalesInvoiceLine.PriceGroup,
                                                            PriceLine = _locSalesInvoiceMonitoring.SalesInvoiceLine.PriceLine,
                                                            UAmount = _locSalesInvoiceMonitoring.SalesInvoiceLine.UAmount,
                                                            Tax = _locSalesInvoiceMonitoring.SalesInvoiceLine.Tax,
                                                            TxValue = _locSalesInvoiceMonitoring.SalesInvoiceLine.TxValue,
                                                            DiscountRule = _locSalesInvoiceMonitoring.SalesInvoiceLine.DiscountRule,
                                                            OldDiscAmount = _locSalesInvoiceMonitoring.SalesInvoiceLine.DiscAmount,
                                                            AccountingPeriodicLine = _locSalesInvoiceMonitoring.SalesInvoiceLine.AccountingPeriodicLine,
                                                            ETD = _locSalesInvoiceMonitoring.SalesInvoiceLine.ETD,
                                                            ETA = _locSalesInvoiceMonitoring.SalesInvoiceLine.ETA,
                                                            SalesInvoiceCmp = _locSalesInvoiceCmp2,
                                                            SalesInvoiceLine = _locSalesInvoiceMonitoring.SalesInvoiceLine,
                                                            DocDate = _locSalesInvoiceCmp2.DocDate,
                                                            JournalMonth = _locSalesInvoiceCmp2.JournalMonth,
                                                            JournalYear = _locSalesInvoiceCmp2.JournalYear,
                                                        };
                                                        _saveDataSalesInvoiceLineCmp.Save();
                                                        _saveDataSalesInvoiceLineCmp.Session.CommitTransaction();
                                                        #endregion SalesInvoiceLineCmp
                                                    }
                                                    //Jadikan Code lama (Amount Free Item SI - Amount Free Item SIC - Amount Seharusnya Free Item SIC)
                                                    if (_locSalesInvoiceMonitoring.StockGroup == StockGroup.FreeItem)
                                                    {
                                                        #region SalesInvoiceFreeItemCmp
                                                        SalesInvoiceFreeItemCmp _saveDataSalesInvoiceCmpFreeItem = new SalesInvoiceFreeItemCmp(_currSession)
                                                        {
                                                            Company = _locAmendDeliveryXPO.Company,
                                                            Workplace = _locAmendDeliveryXPO.Workplace,
                                                            Division = _locAmendDeliveryXPO.Division,
                                                            Department = _locAmendDeliveryXPO.Department,
                                                            Section = _locAmendDeliveryXPO.Section,
                                                            Employee = _locAmendDeliveryXPO.Employee,
                                                            FreeItemRule = _locSalesInvoiceMonitoring.SalesInvoiceFreeItem.FreeItemRule,
                                                            FpItem = _locSalesInvoiceMonitoring.Item,
                                                            FpDQty = _locSalesInvoiceMonitoring.DlvPDQty,
                                                            FpDUOM = _locSalesInvoiceMonitoring.DlvPDUOM,
                                                            FpQty = _locSalesInvoiceMonitoring.DlvPQty,
                                                            FpUOM = _locSalesInvoiceMonitoring.DlvPUOM,
                                                            FpTQty = _locSalesInvoiceMonitoring.DlvPTQty,
                                                            Currency = _locSalesInvoiceMonitoring.SalesInvoiceFreeItem.Currency,
                                                            PriceGroup = _locSalesInvoiceMonitoring.SalesInvoiceFreeItem.PriceGroup,
                                                            PriceLine = _locSalesInvoiceMonitoring.SalesInvoiceFreeItem.PriceLine,
                                                            UAmount = _locSalesInvoiceMonitoring.SalesInvoiceFreeItem.UAmount,
                                                            ETD = _locSalesInvoiceMonitoring.ETD,
                                                            ETA = _locSalesInvoiceMonitoring.ETA,
                                                            FpStockType = _locSalesInvoiceMonitoring.StockType,
                                                            SalesInvoiceCmp = _locSalesInvoiceCmp2,
                                                            SalesInvoiceFreeItem = _locSalesInvoiceMonitoring.SalesInvoiceFreeItem,
                                                            DocDate = _locSalesInvoiceCmp2.DocDate,
                                                            JournalMonth = _locSalesInvoiceCmp2.JournalMonth,
                                                            JournalYear = _locSalesInvoiceCmp2.JournalYear,
                                                        };
                                                        _saveDataSalesInvoiceCmpFreeItem.Save();
                                                        _saveDataSalesInvoiceCmpFreeItem.Session.CommitTransaction();
                                                        #endregion SalesInvoiceFreeItemCmp
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    #endregion SalesInvoiceCmp

                                    #region SaveAmendDeliveryLine
                                    AmendDeliveryLine _saveDataAmendDeliveryLine = new AmendDeliveryLine(_currSession)
                                    {
                                        AmendDelivery = _locAmendDeliveryXPO,
                                        Company = _locAmendDeliveryXPO.Company,
                                        Workplace = _locAmendDeliveryXPO.Workplace,
                                        Division = _locAmendDeliveryXPO.Division,
                                        Department = _locAmendDeliveryXPO.Department,
                                        Section = _locAmendDeliveryXPO.Section,
                                        Employee = _locAmendDeliveryXPO.Employee,
                                        Salesman = _locAmendDeliveryXPO.PIC,
                                        Vehicle = _locAmendDeliveryXPO.Vehicle,
                                        SalesInvoice = _locPickingMonitoring.SalesInvoice,
                                        DocDate = _locPickingMonitoring.SalesInvoice.DocDate,
                                        JournalMonth = _locPickingMonitoring.SalesInvoice.JournalMonth,
                                        JournalYear = _locPickingMonitoring.SalesInvoice.JournalYear,
                                        Customer = _locPickingMonitoring.Customer,
                                        Currency = _locPickingMonitoring.SalesInvoice.Currency,
                                        PriceGroup = _locPickingMonitoring.SalesInvoice.PriceGroup,
                                        TotAmount = _locPickingMonitoring.SalesInvoice.TotAmount,
                                        TotTaxAmount = _locPickingMonitoring.SalesInvoice.TotTaxAmount,
                                        TotDiscAmount = _locPickingMonitoring.SalesInvoice.TotDiscAmount,
                                        Amount = _locPickingMonitoring.SalesInvoice.Amount,
                                        PaymentType = _locPickingMonitoring.SalesInvoice.PaymentType,
                                        TOP = _locPickingMonitoring.SalesInvoice.TOP,
                                        PickingMonitoring = _locPickingMonitoring,
                                        CollectStatus = _locPickingMonitoring.CollectStatus,
                                        CollectStatusDate = now,
                                        SalesInvoiceCmp = _iniSalesInvoiceCmp,
                                    };
                                    _saveDataAmendDeliveryLine.Save();
                                    _saveDataAmendDeliveryLine.Session.CommitTransaction();

                                    #endregion SaveAmendDeliveryLine

                                    #region SetOrderCollection
                                    _locPickingMonitoring.AD = false;
                                    _locPickingMonitoring.AmendDelivery = _locAmendDeliveryXPO;
                                    _locPickingMonitoring.Save();
                                    _locPickingMonitoring.Session.CommitTransaction();
                                    #endregion SetOrderCollection
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = OrderCollection ", ex.ToString());
            }
        }

        #endregion GetSIBasedOnRouteInvoice

        #region SetCollection

        private void SetCollectionAmountInAmendDeliveryLine(Session _currSession, AmendDelivery _locAmendDeliveryXPO)
        {
            try
            {
                DateTime now = DateTime.Now;
                double _locTotAmountCn = 0;
                double _locTotAmountDN = 0;
                double _locTotAmountCn1 = 0;
                double _locTotAmountCn2 = 0;
                if (_locAmendDeliveryXPO != null)
                {
                    XPCollection<AmendDeliveryLine> _locAmendDeliveryLines = new XPCollection<AmendDeliveryLine>
                                                                        (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                        new BinaryOperator("AmendDelivery", _locAmendDeliveryXPO),
                                                                        new BinaryOperator("Select", true),
                                                                        new GroupOperator(GroupOperatorType.Or,
                                                                        new BinaryOperator("Status", Status.Progress))));

                    if(_locAmendDeliveryLines != null && _locAmendDeliveryLines.Count() > 0)
                    {
                        foreach(AmendDeliveryLine _locAmendDeliveryLine in _locAmendDeliveryLines)
                        {
                            if(_locAmendDeliveryLine.SalesInvoiceCmp != null && _locAmendDeliveryLine.SalesInvoice != null)
                            {
                                #region FreeItemAmount
                                if(_locAmendDeliveryLine.SalesInvoice.TotFIAmount > 0)
                                {
                                    if (_locAmendDeliveryLine.SalesInvoiceCmp.TotFIAmount > 0)
                                    {
                                        if (_locAmendDeliveryLine.SalesInvoiceCmp.TotFIAmount > _locAmendDeliveryLine.SalesInvoiceCmp.TotNFIAmount)
                                        {
                                            _locTotAmountDN = _locAmendDeliveryLine.SalesInvoiceCmp.TotFIAmount - _locAmendDeliveryLine.SalesInvoiceCmp.TotNFIAmount;
                                        }else
                                        {
                                            _locTotAmountCn2 = _locAmendDeliveryLine.SalesInvoiceCmp.TotNFIAmount - _locAmendDeliveryLine.SalesInvoiceCmp.TotFIAmount;
                                        }
                                    }
                                }
                                #endregion FreeItemAmount

                                _locTotAmountCn1 = _locAmendDeliveryLine.Amount - _locAmendDeliveryLine.SalesInvoiceCmp.Amount;
                                _locTotAmountCn = _locTotAmountCn1 + _locTotAmountCn2;
                                _locAmendDeliveryLine.TotAmountCmp = _locAmendDeliveryLine.SalesInvoiceCmp.TotAmount;
                                _locAmendDeliveryLine.TotDiscAmountCmp = _locAmendDeliveryLine.SalesInvoiceCmp.TotDiscAmount;
                                _locAmendDeliveryLine.TotTaxAmountCmp = _locAmendDeliveryLine.SalesInvoiceCmp.TotTaxAmount;
                                _locAmendDeliveryLine.AmountCmp = _locAmendDeliveryLine.SalesInvoiceCmp.Amount;
                                _locAmendDeliveryLine.AmountDN = _locTotAmountDN;
                                _locAmendDeliveryLine.AmountCN = _locTotAmountCn;
                                _locAmendDeliveryLine.AmountColl = _locAmendDeliveryLine.SalesInvoiceCmp.Amount + _locTotAmountDN - _locTotAmountCn;
                                _locAmendDeliveryLine.Save();
                                _locAmendDeliveryLine.Session.CommitTransaction();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = OrderCollection ", ex.ToString());
            }
        }

        private void SetGrandTotalAmount(Session _currSession, AmendDelivery _locAmendDeliveryXPO)
        {
            try
            {
                DateTime now = DateTime.Now;
                double _locTotAmount = 0;
                double _locTotAmountCn = 0;
                double _locTotAmountDN = 0;
                double _locTotAmountColl = 0;
                if (_locAmendDeliveryXPO != null)
                {
                    XPCollection<AmendDeliveryLine> _locAmendDeliveryLines = new XPCollection<AmendDeliveryLine>
                                                                        (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                        new BinaryOperator("AmendDelivery", _locAmendDeliveryXPO),
                                                                        new BinaryOperator("Select", true),
                                                                        new GroupOperator(GroupOperatorType.Or,
                                                                        new BinaryOperator("Status", Status.Progress))));

                    if (_locAmendDeliveryLines != null && _locAmendDeliveryLines.Count() > 0)
                    {
                        foreach (AmendDeliveryLine _locAmendDeliveryLine in _locAmendDeliveryLines)
                        {
                            if(_locAmendDeliveryLine.TotAmountCmp > 0 )
                            {
                                _locTotAmount = _locTotAmount + _locAmendDeliveryLine.TotAmountCmp;
                            }
                            if (_locAmendDeliveryLine.AmountCN > 0)
                            {
                                _locTotAmountCn = _locTotAmountCn + _locAmendDeliveryLine.AmountCN;
                            }
                            if (_locAmendDeliveryLine.AmountDN > 0)
                            {
                                _locTotAmountDN = _locTotAmountDN + _locAmendDeliveryLine.AmountDN;
                            }
                            if (_locAmendDeliveryLine.AmountColl > 0)
                            {
                                _locTotAmountColl = _locTotAmountColl + _locAmendDeliveryLine.AmountColl;
                            }
                        }

                        _locAmendDeliveryXPO.GrandTotalAmount = _locTotAmount;
                        _locAmendDeliveryXPO.GrandTotalAmountCN = _locTotAmountCn;
                        _locAmendDeliveryXPO.GrandTotalAmountDN = _locTotAmountDN;
                        _locAmendDeliveryXPO.GrandTotalAmountColl = _locTotAmountColl;
                        _locAmendDeliveryXPO.Save();
                        _locAmendDeliveryXPO.Session.CommitTransaction();
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = OrderCollection ", ex.ToString());
            }
        }

        #endregion SetCollection

        #region Posting

        private void SetPickingMonitoring(Session _currSession, AmendDelivery _locAmendDeliveryXPO)
        {
            try
            {
                DateTime now = DateTime.Now;
                if (_locAmendDeliveryXPO != null && _locAmendDeliveryXPO.Picking != null)
                {
                    XPCollection<AmendDeliveryLine> _locAmendDeliveryLines = new XPCollection<AmendDeliveryLine>(_currSession,
                                                            new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("AmendDelivery", _locAmendDeliveryXPO),
                                                            new BinaryOperator("Select", true),
                                                            new GroupOperator(GroupOperatorType.Or,
                                                            new BinaryOperator("Status", Status.Progress),
                                                            new BinaryOperator("Status", Status.Posted))));

                    if (_locAmendDeliveryLines != null && _locAmendDeliveryLines.Count > 0)
                    {
                        foreach (AmendDeliveryLine _locAmendDeliveryLine in _locAmendDeliveryLines)
                        {
                            if(_locAmendDeliveryLine.PickingMonitoring != null)
                            {
                                _locAmendDeliveryLine.PickingMonitoring.Active = false;
                                _locAmendDeliveryLine.PickingMonitoring.OC = true;
                                _locAmendDeliveryLine.PickingMonitoring.AD = false;
                                _locAmendDeliveryLine.PickingMonitoring.CollectStatus = CollectStatus.Ready;
                                _locAmendDeliveryLine.PickingMonitoring.SalesInvoiceCmp = _locAmendDeliveryLine.SalesInvoiceCmp;
                                _locAmendDeliveryLine.PickingMonitoring.TotAmount = _locAmendDeliveryLine.TotAmountCmp;
                                _locAmendDeliveryLine.PickingMonitoring.TotTaxAmount = _locAmendDeliveryLine.TotTaxAmountCmp;
                                _locAmendDeliveryLine.PickingMonitoring.TotDiscAmount = _locAmendDeliveryLine.TotDiscAmountCmp;
                                _locAmendDeliveryLine.PickingMonitoring.Amount = _locAmendDeliveryLine.AmountCmp;
                                _locAmendDeliveryLine.PickingMonitoring.AmountCN = _locAmendDeliveryLine.AmountCN;
                                _locAmendDeliveryLine.PickingMonitoring.AmountDN = _locAmendDeliveryLine.AmountDN;
                                _locAmendDeliveryLine.PickingMonitoring.AmountColl = _locAmendDeliveryLine.AmountColl;
                                _locAmendDeliveryLine.PickingMonitoring.Save();
                                _locAmendDeliveryLine.PickingMonitoring.Session.CommitTransaction();
                            }
                        }

                        _locAmendDeliveryXPO.Picking.OC = true;
                        _locAmendDeliveryXPO.Picking.Save();
                        _locAmendDeliveryXPO.Picking.Session.CommitTransaction();
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = AmendDelivery " + ex.ToString());
            }
        }

        private void SetPicking(Session _currSession, AmendDelivery _locAmendDeliveryXPO)
        {
            
            try
            {
                DateTime now = DateTime.Now;
                bool _locBoolAD = false;
                bool _locBoolOC = false;
                bool _locBoolActive = false;
                if (_locAmendDeliveryXPO != null)
                {
                    if (_locAmendDeliveryXPO.Picking != null)
                    {
                        XPCollection<RouteInvoice> _locPickingMonitorings = new XPCollection<RouteInvoice>(_currSession,
                                                                    new GroupOperator(GroupOperatorType.And,
                                                                    new BinaryOperator("Picking", _locAmendDeliveryXPO.Picking)));
                        if (_locPickingMonitorings != null && _locPickingMonitorings.Count() > 0)
                        {
                            foreach (RouteInvoice _locPickingMonitoring in _locPickingMonitorings)
                            {
                                if (_locPickingMonitoring.AD == true)
                                {
                                    _locBoolAD = true;
                                }
                                if (_locPickingMonitoring.OC == true)
                                {
                                    _locBoolOC = true;
                                }
                                if (_locPickingMonitoring.Active == true)
                                {
                                    _locBoolActive = true;
                                }
                            }

                            _locAmendDeliveryXPO.Picking.AD = _locBoolAD;
                            _locAmendDeliveryXPO.Picking.OC = _locBoolOC;
                            _locAmendDeliveryXPO.Picking.Active = _locBoolActive;
                            _locAmendDeliveryXPO.Picking.Save();
                            _locAmendDeliveryXPO.Picking.Session.CommitTransaction();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = AmendDelivery " + ex.ToString());
            }
        }

        #region JournalReturnDO

        private bool CheckJournalSetup(Session _currSession, AmendDelivery _locAmendDeliveryXPO)
        {
            bool _result = false;
            try
            {
                if (_locAmendDeliveryXPO != null)
                {
                    if (_locAmendDeliveryXPO.Company != null && _locAmendDeliveryXPO.Workplace != null)
                    {
                        JournalSetupDetail _locJournalSetupDetail = _currSession.FindObject<JournalSetupDetail>
                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                    new BinaryOperator("Company", _locAmendDeliveryXPO.Company),
                                                                    new BinaryOperator("Workplace", _locAmendDeliveryXPO.Workplace),
                                                                    new BinaryOperator("JournalSetupType", JournalSetupType.Journal),
                                                                    new BinaryOperator("ObjectList", ObjectList.AmendDelivery),
                                                                    new BinaryOperator("JournalProcess", JournalProcess.JournalReturnSales),
                                                                    new BinaryOperator("Active", true)));

                        if (_locJournalSetupDetail != null)
                        {
                            _result = true;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = AmendDelivery ", ex.ToString());
            }

            return _result;
        }

        private void SetReturnSalesJournal(Session _currSession, AmendDelivery _locAmendDeliveryXPO)
        {
            try
            {
                DateTime now = DateTime.Now;
                double _locTotalAmountCollBPDN = 0;
                double _locTotalAmountCollBPCN = 0;
                double _locTotalAmountCollCDN = 0;
                double _locTotalAmountCollCCN = 0;
                BusinessPartnerAccountGroup _locBusinessPartnerAccountGroup = null;
                CompanyAccountGroup _locCompanyAccountGroup = null;

                if (_locAmendDeliveryXPO != null)
                {
                    XPCollection<AmendDeliveryLine> _locAmendDeliveryLines = new XPCollection<AmendDeliveryLine>(_currSession, 
                                                                        new GroupOperator(GroupOperatorType.And,
                                                                        new BinaryOperator("AmendDelivery", _locAmendDeliveryXPO),
                                                                        new BinaryOperator("Select", true),
                                                                        new GroupOperator(GroupOperatorType.Or,
                                                                        new BinaryOperator("Status", Status.Progress),
                                                                        new BinaryOperator("Status", Status.Posted))));
                    if(_locAmendDeliveryLines != null && _locAmendDeliveryLines.Count() > 0)
                    {
                        foreach(AmendDeliveryLine _locAmendDeliveryLine in _locAmendDeliveryLines)
                        {
                            #region JournalMapBusinessPartnerAccountGroup

                            if (_locAmendDeliveryLine.SalesInvoice.BillToCustomer != null)
                            {
                                if (_locAmendDeliveryLine.SalesInvoice.BillToCustomer.BusinessPartnerAccountGroup != null)
                                {
                                    _locBusinessPartnerAccountGroup = _locAmendDeliveryLine.SalesInvoice.BillToCustomer.BusinessPartnerAccountGroup;

                                    _locTotalAmountCollBPDN = _locAmendDeliveryLine.AmountDN;
                                    _locTotalAmountCollBPCN = _locAmendDeliveryLine.AmountCN;

                                    XPCollection<JournalMap> _locJournalMapItems = new XPCollection<JournalMap>
                                                                       (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                        new BinaryOperator("Company", _locAmendDeliveryLine.Company),
                                                                        new BinaryOperator("Workplace", _locAmendDeliveryLine.Workplace),
                                                                        new BinaryOperator("BusinessPartnerAccountGroup", _locBusinessPartnerAccountGroup),
                                                                        new BinaryOperator("Active", true)));

                                    if (_locJournalMapItems != null && _locJournalMapItems.Count() > 0)
                                    {
                                        foreach (JournalMap _locJournalMapItem in _locJournalMapItems)
                                        {
                                            XPCollection<JournalMapLine> _locJournalMapLines = new XPCollection<JournalMapLine>
                                                                                               (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                new BinaryOperator("Company", _locAmendDeliveryLine.Company),
                                                                                                new BinaryOperator("Workplace", _locAmendDeliveryLine.Workplace),
                                                                                                new BinaryOperator("JournalMap", _locJournalMapItem),
                                                                                                new BinaryOperator("Active", true)));

                                            if (_locJournalMapLines != null && _locJournalMapLines.Count() > 0)
                                            {
                                                foreach (JournalMapLine _locJournalMapLine in _locJournalMapLines)
                                                {
                                                    if (_locJournalMapLine.AccountMap != null)
                                                    {
                                                        #region AmountDN
                                                        if (_locJournalMapLine.AccountMap.PostingType == PostingType.Sales && _locJournalMapLine.AccountMap.OrderType == OrderType.Item
                                                            && _locJournalMapLine.AccountMap.PostingMethod == PostingMethod.Return && _locJournalMapLine.AccountMap.PostingMethodType == PostingMethodType.AmountDN)
                                                        {
                                                            XPCollection<AccountMapLine> _locAccountMapLines = new XPCollection<AccountMapLine>
                                                                                                           (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                            new BinaryOperator("Company", _locAmendDeliveryLine.Company),
                                                                                                            new BinaryOperator("Workplace", _locAmendDeliveryLine.Workplace),
                                                                                                            new BinaryOperator("AccountMap", _locJournalMapLine.AccountMap),
                                                                                                            new BinaryOperator("Active", true)));

                                                            if (_locAccountMapLines != null && _locAccountMapLines.Count() > 0)
                                                            {
                                                                double _locTotalAmountDebit = 0;
                                                                double _locTotalAmountCredit = 0;
                                                                double _locTotalBalance = 0;

                                                                foreach (AccountMapLine _locAccountMapLine in _locAccountMapLines)
                                                                {
                                                                    if (_locAccountMapLine.AccountCharge == AccountCharge.Debit)
                                                                    {
                                                                        _locTotalAmountDebit = _locTotalAmountCollBPDN;
                                                                    }
                                                                    if (_locAccountMapLine.AccountCharge == AccountCharge.Credit)
                                                                    {
                                                                        _locTotalAmountCredit = _locTotalAmountCollBPDN;
                                                                    }

                                                                    GeneralJournal _saveGeneralJournal = new GeneralJournal(_currSession)
                                                                    {
                                                                        Company = _locAmendDeliveryLine.Company,
                                                                        Workplace = _locAmendDeliveryLine.Workplace,
                                                                        PostingDate = now,
                                                                        PostingType = PostingType.Sales,
                                                                        OrderType = OrderType.Item,
                                                                        PostingMethod = PostingMethod.Return,
                                                                        PostingMethodType = PostingMethodType.AmountDN,
                                                                        Account = _locAccountMapLine.Account,
                                                                        Debit = _locTotalAmountDebit,
                                                                        Credit = _locTotalAmountCredit,
                                                                        JournalMonth = _locAmendDeliveryLine.JournalMonth,
                                                                        JournalYear = _locAmendDeliveryLine.JournalYear,
                                                                        AmendDelivery = _locAmendDeliveryXPO,
                                                                    };

                                                                    _saveGeneralJournal.Save();
                                                                    _saveGeneralJournal.Session.CommitTransaction();

                                                                    #region AccountingPeriodicLine

                                                                    if (_locAccountMapLine.Account.Code != null)
                                                                    {

                                                                        //Cari Account Periodic Line
                                                                        AccountingPeriodicLine _locAPL = _currSession.FindObject<AccountingPeriodicLine>
                                                                                                        (new GroupOperator(GroupOperatorType.And,
                                                                                                        new BinaryOperator("Company", _locAmendDeliveryLine.Company),
                                                                                                        new BinaryOperator("Workplace", _locAmendDeliveryLine.Workplace),
                                                                                                        new BinaryOperator("AccountNo", _locAccountMapLine.Account),
                                                                                                        new BinaryOperator("Month", _locAmendDeliveryLine.JournalMonth),
                                                                                                        new BinaryOperator("Year", _locAmendDeliveryLine.JournalYear)));
                                                                        if (_locAPL != null)
                                                                        {
                                                                            if (_locAPL.AccountNo != null)
                                                                            {
                                                                                if (_locAPL.AccountNo.BalanceType == BalanceType.Change)
                                                                                {
                                                                                    if (_locAPL.AccountNo.AccountCharge == AccountCharge.Debit)
                                                                                    {
                                                                                        if (_locTotalAmountDebit > 0)
                                                                                        {
                                                                                            _locTotalBalance = _locAPL.Balance + _locTotalAmountDebit;
                                                                                        }
                                                                                        if (_locTotalAmountCredit > 0)
                                                                                        {
                                                                                            _locTotalBalance = _locAPL.Balance - _locTotalAmountCredit;
                                                                                        }
                                                                                    }
                                                                                    if (_locAPL.AccountNo.AccountCharge == AccountCharge.Credit)
                                                                                    {
                                                                                        if (_locTotalAmountDebit > 0)
                                                                                        {
                                                                                            _locTotalBalance = _locAPL.Balance - _locTotalAmountDebit;
                                                                                        }
                                                                                        if (_locTotalAmountCredit > 0)
                                                                                        {
                                                                                            _locTotalBalance = _locAPL.Balance + _locTotalAmountCredit;
                                                                                        }
                                                                                    }
                                                                                }

                                                                                _locAPL.Balance = _locTotalBalance;
                                                                                _locAPL.Debit = _locAPL.Debit + _locTotalAmountDebit;
                                                                                _locAPL.Credit = _locAPL.Credit + _locTotalAmountCredit;
                                                                                _locAPL.Save();
                                                                                _locAPL.Session.CommitTransaction();

                                                                            }
                                                                        }
                                                                    }
                                                                    #endregion COA
                                                                }
                                                            }
                                                        }
                                                        #endregion AmountDN
                                                        #region AmountCN
                                                        if (_locJournalMapLine.AccountMap.PostingType == PostingType.Sales && _locJournalMapLine.AccountMap.OrderType == OrderType.Item
                                                            && _locJournalMapLine.AccountMap.PostingMethod == PostingMethod.Return && _locJournalMapLine.AccountMap.PostingMethodType == PostingMethodType.AmountCN)
                                                        {
                                                            XPCollection<AccountMapLine> _locAccountMapLines = new XPCollection<AccountMapLine>
                                                                                                           (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                            new BinaryOperator("Company", _locAmendDeliveryLine.Company),
                                                                                                            new BinaryOperator("Workplace", _locAmendDeliveryLine.Workplace),
                                                                                                            new BinaryOperator("AccountMap", _locJournalMapLine.AccountMap),
                                                                                                            new BinaryOperator("Active", true)));

                                                            if (_locAccountMapLines != null && _locAccountMapLines.Count() > 0)
                                                            {
                                                                double _locTotalAmountDebit = 0;
                                                                double _locTotalAmountCredit = 0;
                                                                double _locTotalBalance = 0;

                                                                foreach (AccountMapLine _locAccountMapLine in _locAccountMapLines)
                                                                {
                                                                    if (_locAccountMapLine.AccountCharge == AccountCharge.Debit)
                                                                    {
                                                                        _locTotalAmountDebit = _locTotalAmountCollBPDN;
                                                                    }
                                                                    if (_locAccountMapLine.AccountCharge == AccountCharge.Credit)
                                                                    {
                                                                        _locTotalAmountCredit = _locTotalAmountCollBPDN;
                                                                    }

                                                                    GeneralJournal _saveGeneralJournal = new GeneralJournal(_currSession)
                                                                    {
                                                                        Company = _locAmendDeliveryLine.Company,
                                                                        Workplace = _locAmendDeliveryLine.Workplace,
                                                                        PostingDate = now,
                                                                        PostingType = PostingType.Sales,
                                                                        OrderType = OrderType.Item,
                                                                        PostingMethod = PostingMethod.Return,
                                                                        PostingMethodType = PostingMethodType.AmountCN,
                                                                        Account = _locAccountMapLine.Account,
                                                                        Debit = _locTotalAmountDebit,
                                                                        Credit = _locTotalAmountCredit,
                                                                        JournalMonth = _locAmendDeliveryLine.JournalMonth,
                                                                        JournalYear = _locAmendDeliveryLine.JournalYear,
                                                                        AmendDelivery = _locAmendDeliveryXPO,
                                                                    };

                                                                    _saveGeneralJournal.Save();
                                                                    _saveGeneralJournal.Session.CommitTransaction();

                                                                    #region AccountingPeriodicLine

                                                                    if (_locAccountMapLine.Account.Code != null)
                                                                    {

                                                                        //Cari Account Periodic Line
                                                                        AccountingPeriodicLine _locAPL = _currSession.FindObject<AccountingPeriodicLine>
                                                                                                        (new GroupOperator(GroupOperatorType.And,
                                                                                                        new BinaryOperator("Company", _locAmendDeliveryLine.Company),
                                                                                                        new BinaryOperator("Workplace", _locAmendDeliveryLine.Workplace),
                                                                                                        new BinaryOperator("AccountNo", _locAccountMapLine.Account),
                                                                                                        new BinaryOperator("Month", _locAmendDeliveryLine.JournalMonth),
                                                                                                        new BinaryOperator("Year", _locAmendDeliveryLine.JournalYear)));
                                                                        if (_locAPL != null)
                                                                        {
                                                                            if (_locAPL.AccountNo != null)
                                                                            {
                                                                                if (_locAPL.AccountNo.BalanceType == BalanceType.Change)
                                                                                {
                                                                                    if (_locAPL.AccountNo.AccountCharge == AccountCharge.Debit)
                                                                                    {
                                                                                        if (_locTotalAmountDebit > 0)
                                                                                        {
                                                                                            _locTotalBalance = _locAPL.Balance + _locTotalAmountDebit;
                                                                                        }
                                                                                        if (_locTotalAmountCredit > 0)
                                                                                        {
                                                                                            _locTotalBalance = _locAPL.Balance - _locTotalAmountCredit;
                                                                                        }
                                                                                    }
                                                                                    if (_locAPL.AccountNo.AccountCharge == AccountCharge.Credit)
                                                                                    {
                                                                                        if (_locTotalAmountDebit > 0)
                                                                                        {
                                                                                            _locTotalBalance = _locAPL.Balance - _locTotalAmountDebit;
                                                                                        }
                                                                                        if (_locTotalAmountCredit > 0)
                                                                                        {
                                                                                            _locTotalBalance = _locAPL.Balance + _locTotalAmountCredit;
                                                                                        }
                                                                                    }
                                                                                }

                                                                                _locAPL.Balance = _locTotalBalance;
                                                                                _locAPL.Debit = _locAPL.Debit + _locTotalAmountDebit;
                                                                                _locAPL.Credit = _locAPL.Credit + _locTotalAmountCredit;
                                                                                _locAPL.Save();
                                                                                _locAPL.Session.CommitTransaction();

                                                                            }
                                                                        }
                                                                    }
                                                                    #endregion COA
                                                                }
                                                            }
                                                        }
                                                        #endregion AmountCN
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            #endregion JournalMapBusinessPartnerAccountGroup

                            #region JournalMapCompanyAccountGroup
                            if (_locAmendDeliveryLine.Company != null)
                            {
                                _locTotalAmountCollCDN = _locAmendDeliveryLine.AmountDN;
                                _locTotalAmountCollCCN = _locAmendDeliveryLine.AmountCN;

                                CompanyAccount _locCompanyAccount = _currSession.FindObject<CompanyAccount>(new GroupOperator(GroupOperatorType.And,
                                                        new BinaryOperator("Company", _locAmendDeliveryLine.Company),
                                                        new BinaryOperator("Workplace", _locAmendDeliveryLine.Workplace),
                                                        new BinaryOperator("Active", true)));

                                if (_locCompanyAccount != null)
                                {
                                    if (_locCompanyAccount.CompanyAccountGroup != null)
                                    {
                                        _locCompanyAccountGroup = _locCompanyAccount.CompanyAccountGroup;

                                        XPCollection<JournalMap> _locJournalMapItems = new XPCollection<JournalMap>
                                                                       (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                        new BinaryOperator("Company", _locAmendDeliveryLine.Company),
                                                                        new BinaryOperator("Workplace", _locAmendDeliveryLine.Workplace),
                                                                        new BinaryOperator("CompanyAccountGroup", _locCompanyAccountGroup),
                                                                        new BinaryOperator("Active", true)));

                                        if (_locJournalMapItems != null && _locJournalMapItems.Count() > 0)
                                        {
                                            foreach (JournalMap _locJournalMapItem in _locJournalMapItems)
                                            {
                                                XPCollection<JournalMapLine> _locJournalMapLines = new XPCollection<JournalMapLine>
                                                                                                   (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                    new BinaryOperator("Company", _locAmendDeliveryLine.Company),
                                                                                                    new BinaryOperator("Workplace", _locAmendDeliveryLine.Workplace),
                                                                                                    new BinaryOperator("JournalMap", _locJournalMapItem),
                                                                                                    new BinaryOperator("Active", true)));

                                                if (_locJournalMapLines != null && _locJournalMapLines.Count() > 0)
                                                {
                                                    foreach (JournalMapLine _locJournalMapLine in _locJournalMapLines)
                                                    {
                                                        if (_locJournalMapLine.AccountMap != null)
                                                        {
                                                            #region AmountDN
                                                            if (_locJournalMapLine.AccountMap.PostingType == PostingType.Sales && _locJournalMapLine.AccountMap.OrderType == OrderType.Item
                                                                && _locJournalMapLine.AccountMap.PostingMethod == PostingMethod.Return && _locJournalMapLine.AccountMap.PostingMethodType == PostingMethodType.AmountDN)
                                                            {
                                                                XPCollection<AccountMapLine> _locAccountMapLines = new XPCollection<AccountMapLine>
                                                                                                               (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                                new BinaryOperator("Company", _locAmendDeliveryLine.Company),
                                                                                                                new BinaryOperator("Workplace", _locAmendDeliveryLine.Workplace),
                                                                                                                new BinaryOperator("AccountMap", _locJournalMapLine.AccountMap),
                                                                                                                new BinaryOperator("Active", true)));

                                                                if (_locAccountMapLines != null && _locAccountMapLines.Count() > 0)
                                                                {
                                                                    double _locTotalAmountDebit = 0;
                                                                    double _locTotalAmountCredit = 0;
                                                                    double _locTotalBalance = 0;

                                                                    foreach (AccountMapLine _locAccountMapLine in _locAccountMapLines)
                                                                    {
                                                                        if (_locAccountMapLine.AccountCharge == AccountCharge.Debit)
                                                                        {
                                                                            _locTotalAmountDebit = _locTotalAmountCollCDN;
                                                                        }
                                                                        if (_locAccountMapLine.AccountCharge == AccountCharge.Credit)
                                                                        {
                                                                            _locTotalAmountCredit = _locTotalAmountCollCDN;
                                                                        }

                                                                        GeneralJournal _saveGeneralJournal = new GeneralJournal(_currSession)
                                                                        {
                                                                            Company = _locAmendDeliveryLine.Company,
                                                                            Workplace = _locAmendDeliveryLine.Workplace,
                                                                            PostingDate = now,
                                                                            PostingType = PostingType.Sales,
                                                                            OrderType = OrderType.Item,
                                                                            PostingMethod = PostingMethod.Return,
                                                                            PostingMethodType = PostingMethodType.AmountDN,
                                                                            Account = _locAccountMapLine.Account,
                                                                            Debit = _locTotalAmountDebit,
                                                                            Credit = _locTotalAmountCredit,
                                                                            JournalMonth = _locAmendDeliveryLine.JournalMonth,
                                                                            JournalYear = _locAmendDeliveryLine.JournalYear,
                                                                            AmendDelivery = _locAmendDeliveryXPO,
                                                                        };

                                                                        _saveGeneralJournal.Save();
                                                                        _saveGeneralJournal.Session.CommitTransaction();

                                                                        #region AccountingPeriodicLine

                                                                        if (_locAccountMapLine.Account.Code != null)
                                                                        {

                                                                            //Cari Account Periodic Line
                                                                            AccountingPeriodicLine _locAPL = _currSession.FindObject<AccountingPeriodicLine>
                                                                                                            (new GroupOperator(GroupOperatorType.And,
                                                                                                            new BinaryOperator("Company", _locAmendDeliveryLine.Company),
                                                                                                            new BinaryOperator("Workplace", _locAmendDeliveryLine.Workplace),
                                                                                                            new BinaryOperator("AccountNo", _locAccountMapLine.Account),
                                                                                                            new BinaryOperator("Month", _locAmendDeliveryLine.JournalMonth),
                                                                                                            new BinaryOperator("Year", _locAmendDeliveryLine.JournalYear)));
                                                                            if (_locAPL != null)
                                                                            {
                                                                                if (_locAPL.AccountNo != null)
                                                                                {
                                                                                    if (_locAPL.AccountNo.BalanceType == BalanceType.Change)
                                                                                    {
                                                                                        if (_locAPL.AccountNo.AccountCharge == AccountCharge.Debit)
                                                                                        {
                                                                                            if (_locTotalAmountDebit > 0)
                                                                                            {
                                                                                                _locTotalBalance = _locAPL.Balance + _locTotalAmountDebit;
                                                                                            }
                                                                                            if (_locTotalAmountCredit > 0)
                                                                                            {
                                                                                                _locTotalBalance = _locAPL.Balance - _locTotalAmountCredit;
                                                                                            }
                                                                                        }
                                                                                        if (_locAPL.AccountNo.AccountCharge == AccountCharge.Credit)
                                                                                        {
                                                                                            if (_locTotalAmountDebit > 0)
                                                                                            {
                                                                                                _locTotalBalance = _locAPL.Balance - _locTotalAmountDebit;
                                                                                            }
                                                                                            if (_locTotalAmountCredit > 0)
                                                                                            {
                                                                                                _locTotalBalance = _locAPL.Balance + _locTotalAmountCredit;
                                                                                            }
                                                                                        }
                                                                                    }

                                                                                    _locAPL.Balance = _locTotalBalance;
                                                                                    _locAPL.Debit = _locAPL.Debit + _locTotalAmountDebit;
                                                                                    _locAPL.Credit = _locAPL.Credit + _locTotalAmountCredit;
                                                                                    _locAPL.Save();
                                                                                    _locAPL.Session.CommitTransaction();

                                                                                }
                                                                            }
                                                                        }
                                                                        #endregion COA
                                                                    }
                                                                }
                                                            }
                                                            #endregion AmountDN
                                                            #region AmountCN
                                                            if (_locJournalMapLine.AccountMap.PostingType == PostingType.Sales && _locJournalMapLine.AccountMap.OrderType == OrderType.Item
                                                                && _locJournalMapLine.AccountMap.PostingMethod == PostingMethod.Return && _locJournalMapLine.AccountMap.PostingMethodType == PostingMethodType.AmountCN)
                                                            {
                                                                XPCollection<AccountMapLine> _locAccountMapLines = new XPCollection<AccountMapLine>
                                                                                                               (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                                new BinaryOperator("Company", _locAmendDeliveryLine.Company),
                                                                                                                new BinaryOperator("Workplace", _locAmendDeliveryLine.Workplace),
                                                                                                                new BinaryOperator("AccountMap", _locJournalMapLine.AccountMap),
                                                                                                                new BinaryOperator("Active", true)));

                                                                if (_locAccountMapLines != null && _locAccountMapLines.Count() > 0)
                                                                {
                                                                    double _locTotalAmountDebit = 0;
                                                                    double _locTotalAmountCredit = 0;
                                                                    double _locTotalBalance = 0;

                                                                    foreach (AccountMapLine _locAccountMapLine in _locAccountMapLines)
                                                                    {
                                                                        if (_locAccountMapLine.AccountCharge == AccountCharge.Debit)
                                                                        {
                                                                            _locTotalAmountDebit = _locTotalAmountCollCCN;
                                                                        }
                                                                        if (_locAccountMapLine.AccountCharge == AccountCharge.Credit)
                                                                        {
                                                                            _locTotalAmountCredit = _locTotalAmountCollCCN;
                                                                        }

                                                                        GeneralJournal _saveGeneralJournal = new GeneralJournal(_currSession)
                                                                        {
                                                                            Company = _locAmendDeliveryLine.Company,
                                                                            Workplace = _locAmendDeliveryLine.Workplace,
                                                                            PostingDate = now,
                                                                            PostingType = PostingType.Sales,
                                                                            OrderType = OrderType.Item,
                                                                            PostingMethod = PostingMethod.Return,
                                                                            PostingMethodType = PostingMethodType.AmountCN,
                                                                            Account = _locAccountMapLine.Account,
                                                                            Debit = _locTotalAmountDebit,
                                                                            Credit = _locTotalAmountCredit,
                                                                            JournalMonth = _locAmendDeliveryLine.JournalMonth,
                                                                            JournalYear = _locAmendDeliveryLine.JournalYear,
                                                                            AmendDelivery = _locAmendDeliveryXPO,
                                                                        };

                                                                        _saveGeneralJournal.Save();
                                                                        _saveGeneralJournal.Session.CommitTransaction();

                                                                        #region AccountingPeriodicLine

                                                                        if (_locAccountMapLine.Account.Code != null)
                                                                        {

                                                                            //Cari Account Periodic Line
                                                                            AccountingPeriodicLine _locAPL = _currSession.FindObject<AccountingPeriodicLine>
                                                                                                            (new GroupOperator(GroupOperatorType.And,
                                                                                                            new BinaryOperator("Company", _locAmendDeliveryLine.Company),
                                                                                                            new BinaryOperator("Workplace", _locAmendDeliveryLine.Workplace),
                                                                                                            new BinaryOperator("AccountNo", _locAccountMapLine.Account),
                                                                                                            new BinaryOperator("Month", _locAmendDeliveryLine.JournalMonth),
                                                                                                            new BinaryOperator("Year", _locAmendDeliveryLine.JournalYear)));
                                                                            if (_locAPL != null)
                                                                            {
                                                                                if (_locAPL.AccountNo != null)
                                                                                {
                                                                                    if (_locAPL.AccountNo.BalanceType == BalanceType.Change)
                                                                                    {
                                                                                        if (_locAPL.AccountNo.AccountCharge == AccountCharge.Debit)
                                                                                        {
                                                                                            if (_locTotalAmountDebit > 0)
                                                                                            {
                                                                                                _locTotalBalance = _locAPL.Balance + _locTotalAmountDebit;
                                                                                            }
                                                                                            if (_locTotalAmountCredit > 0)
                                                                                            {
                                                                                                _locTotalBalance = _locAPL.Balance - _locTotalAmountCredit;
                                                                                            }
                                                                                        }
                                                                                        if (_locAPL.AccountNo.AccountCharge == AccountCharge.Credit)
                                                                                        {
                                                                                            if (_locTotalAmountDebit > 0)
                                                                                            {
                                                                                                _locTotalBalance = _locAPL.Balance - _locTotalAmountDebit;
                                                                                            }
                                                                                            if (_locTotalAmountCredit > 0)
                                                                                            {
                                                                                                _locTotalBalance = _locAPL.Balance + _locTotalAmountCredit;
                                                                                            }
                                                                                        }
                                                                                    }

                                                                                    _locAPL.Balance = _locTotalBalance;
                                                                                    _locAPL.Debit = _locAPL.Debit + _locTotalAmountDebit;
                                                                                    _locAPL.Credit = _locAPL.Credit + _locTotalAmountCredit;
                                                                                    _locAPL.Save();
                                                                                    _locAPL.Session.CommitTransaction();

                                                                                }
                                                                            }
                                                                        }
                                                                        #endregion COA
                                                                    }
                                                                }
                                                            }
                                                            #endregion AmountCN
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            #endregion JournalMapCompanyAccountGroup
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = AmendDelivery ", ex.ToString());
            }
        }

        #endregion JournalReturnDO

        private void SetStatusAmendDeliveryLine(Session _currSession, AmendDelivery _locAmendDeliveryXPO)
        {
            try
            {
                DateTime now = DateTime.Now;
                if (_locAmendDeliveryXPO != null)
                {
                    XPCollection<AmendDeliveryLine> _locAmendDeliveryLines = new XPCollection<AmendDeliveryLine>(_currSession,
                                                            new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("AmendDelivery", _locAmendDeliveryXPO),
                                                            new BinaryOperator("Select", true),
                                                            new GroupOperator(GroupOperatorType.Or,
                                                            new BinaryOperator("Status", Status.Progress),
                                                            new BinaryOperator("Status", Status.Posted))));

                    if (_locAmendDeliveryLines != null && _locAmendDeliveryLines.Count > 0)
                    {

                        foreach (AmendDeliveryLine _locAmendDeliveryLine in _locAmendDeliveryLines)
                        {
                            _locAmendDeliveryLine.CollectStatus = CollectStatus.Ready;
                            _locAmendDeliveryLine.CollectStatusDate = now;
                            _locAmendDeliveryLine.Status = Status.Close;
                            _locAmendDeliveryLine.ActivationPosting = true;
                            _locAmendDeliveryLine.StatusDate = now;
                            _locAmendDeliveryLine.Save();
                            _locAmendDeliveryLine.Session.CommitTransaction();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = AmendDelivery " + ex.ToString());
            }
        }

        private void SetAmendDeliveryMonitoring(Session _currSession, AmendDelivery _locAmendDeliveryXPO)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                DateTime now = DateTime.Now;

                if (_locAmendDeliveryXPO != null)
                {
                    #region AmendDeliveryLine
                    XPCollection<AmendDeliveryLine> _locAmendDeliveryLines = new XPCollection<AmendDeliveryLine>
                                                        (_currSession, new GroupOperator(GroupOperatorType.And,
                                                        new BinaryOperator("AmendDelivery", _locAmendDeliveryXPO),
                                                        new BinaryOperator("Select", true),
                                                        new GroupOperator(GroupOperatorType.Or,
                                                        new BinaryOperator("Status", Status.Progress),
                                                        new BinaryOperator("Status", Status.Posted),
                                                        new BinaryOperator("Status", Status.Close))));

                    if (_locAmendDeliveryLines != null && _locAmendDeliveryLines.Count() > 0)
                    {
                        foreach (AmendDeliveryLine _locAmendDeliveryLine in _locAmendDeliveryLines)
                        {
                            AmendDeliveryMonitoring _saveDataAmendDeliveryMonitoring = new AmendDeliveryMonitoring(_currSession)
                            {
                                AmendDelivery = _locAmendDeliveryXPO,
                                AmendDeliveryLine = _locAmendDeliveryLine,
                                Company = _locAmendDeliveryXPO.Company,
                                Workplace = _locAmendDeliveryXPO.Workplace,
                                Division = _locAmendDeliveryXPO.Division,
                                Department = _locAmendDeliveryXPO.Department,
                                Section = _locAmendDeliveryXPO.Section,
                                Employee = _locAmendDeliveryXPO.Employee,
                                Salesman = _locAmendDeliveryLine.Salesman,
                                Vehicle = _locAmendDeliveryLine.Vehicle,
                                Customer = _locAmendDeliveryLine.Customer,
                                SalesInvoice = _locAmendDeliveryLine.SalesInvoice,
                                Currency = _locAmendDeliveryLine.Currency,
                                PriceGroup = _locAmendDeliveryLine.PriceGroup,
                                TotAmount = _locAmendDeliveryLine.TotAmount,
                                TotTaxAmount = _locAmendDeliveryLine.TotTaxAmount,
                                TotDiscAmount = _locAmendDeliveryLine.TotDiscAmount,
                                Amount = _locAmendDeliveryLine.Amount,
                                TotAmountCmp = _locAmendDeliveryLine.TotAmountCmp,
                                TotTaxAmountCmp = _locAmendDeliveryLine.TotTaxAmountCmp,
                                TotDiscAmountCmp = _locAmendDeliveryLine.TotDiscAmountCmp,
                                AmountCmp = _locAmendDeliveryLine.AmountCmp,
                                AmountDN = _locAmendDeliveryLine.AmountDN,
                                AmountCN = _locAmendDeliveryLine.AmountCN,
                                AmountColl = _locAmendDeliveryLine.AmountColl,
                                PaymentType = _locAmendDeliveryLine.PaymentType,
                                TOP = _locAmendDeliveryLine.TOP,
                                DueDate = _locAmendDeliveryLine.DueDate,
                                CollectStatus = _locAmendDeliveryLine.CollectStatus,
                                CollectStatusDate = _locAmendDeliveryLine.CollectStatusDate,
                                Picking = _locAmendDeliveryXPO.Picking,
                                PickingDate = _locAmendDeliveryXPO.PickingDate,
                                PickingMonitoring = _locAmendDeliveryLine.PickingMonitoring,
                            };
                            _saveDataAmendDeliveryMonitoring.Save();
                            _saveDataAmendDeliveryMonitoring.Session.CommitTransaction();
                        }
                    }
                    #endregion AmendDeliveryLine 
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = AmendDelivery ", ex.ToString());
            }
        }

        private void SetNormalAmendDeliveryLine(Session _currSession, AmendDelivery _locAmendDeliveryXPO)
        {
            try
            {
                DateTime now = DateTime.Now;
                if (_locAmendDeliveryXPO != null)
                {
                    XPCollection<AmendDeliveryLine> _locAmendDeliveryLines = new XPCollection<AmendDeliveryLine>(_currSession,
                                                            new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("AmendDelivery", _locAmendDeliveryXPO),
                                                            new BinaryOperator("Select", true),
                                                            new GroupOperator(GroupOperatorType.Or,
                                                            new BinaryOperator("Status", Status.Progress),
                                                            new BinaryOperator("Status", Status.Posted),
                                                            new BinaryOperator("Status", Status.Close))));

                    if (_locAmendDeliveryLines != null && _locAmendDeliveryLines.Count > 0)
                    {

                        foreach (AmendDeliveryLine _locAmendDeliveryLine in _locAmendDeliveryLines)
                        {
                            _locAmendDeliveryLine.Select = false;
                            _locAmendDeliveryLine.Save();
                            _locAmendDeliveryLine.Session.CommitTransaction();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = AmendDelivery " + ex.ToString());
            }
        }

        private void SetFinalAmendDelivery(Session _currSession, AmendDelivery _locAmendDeliveryXPO)
        {
            try
            {
                DateTime now = DateTime.Now;
                int _locStatusCount = 0;
                int _locInvCanvLineCount = 0;

                if (_locAmendDeliveryXPO != null)
                {

                    XPCollection<AmendDeliveryLine> _locAmendDeliveryLines = new XPCollection<AmendDeliveryLine>(_currSession,
                                                            new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("AmendDelivery", _locAmendDeliveryXPO)));

                    if (_locAmendDeliveryLines != null && _locAmendDeliveryLines.Count() > 0)
                    {
                        _locInvCanvLineCount = _locAmendDeliveryLines.Count();

                        foreach (AmendDeliveryLine _locAmendDeliveryLine in _locAmendDeliveryLines)
                        {
                            if (_locAmendDeliveryLine.Status == Status.Close)
                            {
                                _locStatusCount = _locStatusCount + 1;
                            }
                        }
                    }

                    if (_locStatusCount == _locInvCanvLineCount)
                    {
                        _locAmendDeliveryXPO.ActivationPosting = true;
                        _locAmendDeliveryXPO.Status = Status.Close;
                        _locAmendDeliveryXPO.StatusDate = now;
                        _locAmendDeliveryXPO.Save();
                        _locAmendDeliveryXPO.Session.CommitTransaction();
                    }
                    else
                    {
                        _locAmendDeliveryXPO.Status = Status.Posted;
                        _locAmendDeliveryXPO.StatusDate = now;
                        _locAmendDeliveryXPO.Save();
                        _locAmendDeliveryXPO.Session.CommitTransaction();
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = SalesInvoice " + ex.ToString());
            }
        }

        #endregion Posting

        #region Global Method

        private void SuccessMessageShow(string _locMessage)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 4000;
            options.Message = string.Format("{0}", _locMessage);
            options.Type = InformationType.Success;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Success";
            options.Win.Type = WinMessageType.Flyout;
            Application.ShowViewStrategy.ShowMessage(options);
        }

        private void ErrorMessageShow(string _locMessage)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 2000;
            options.Message = string.Format(_locMessage);
            options.Type = InformationType.Warning;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Error";
            options.Win.Type = WinMessageType.Alert;
            Application.ShowViewStrategy.ShowMessage(options);
        }


        #endregion Global Method

        
    }
}
