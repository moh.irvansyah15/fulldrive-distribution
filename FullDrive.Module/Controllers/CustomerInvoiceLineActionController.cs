﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;

using FullDrive.Module.BusinessObjects;
using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.Xpo;
using System.Collections;
using DevExpress.Xpo;

namespace FullDrive.Module.Controllers
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class CustomerInvoiceLineActionController : ViewController
    {
        private ChoiceActionItem _selectionListviewFilter;

        public CustomerInvoiceLineActionController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.
            #region FilterStatus
            CustomerInvoiceLineListviewFilterSelectionAction.Items.Clear();
            foreach (object _currApproval in Enum.GetValues(typeof(CustomProcess.Status)))
            {
                EnumDescriptor _ed = new EnumDescriptor(typeof(CustomProcess.Status));
                if (_ed.GetCaption(_currApproval) != "Approved" && _ed.GetCaption(_currApproval) != "Lock")
                {
                    _selectionListviewFilter = new ChoiceActionItem(_ed.GetCaption(_currApproval), _currApproval);
                }
                CustomerInvoiceLineListviewFilterSelectionAction.Items.Add(_selectionListviewFilter);
            }
            #endregion FilterStatus
        }

        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
        }

        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }

        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        private void CustomerInvoiceLineSelectAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;

                if (_objectToProcess != null)
                {
                    foreach (Object obj in _objectToProcess)
                    {
                        CustomerInvoiceLine _locCustomerInvoiceLineOS = (CustomerInvoiceLine)_objectSpace.GetObject(obj);

                        if (_locCustomerInvoiceLineOS != null)
                        {
                            if (_locCustomerInvoiceLineOS.Session != null)
                            {
                                _currSession = _locCustomerInvoiceLineOS.Session;
                            }

                            if (_locCustomerInvoiceLineOS.Code != null && _currSession != null)
                            {
                                _currObjectId = _locCustomerInvoiceLineOS.Code;

                                XPCollection<CustomerInvoiceLine> _locCustomerInvoiceLines = new XPCollection<CustomerInvoiceLine>
                                                                                                     (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                      new BinaryOperator("Code", _currObjectId),
                                                                                                      new GroupOperator(GroupOperatorType.Or,
                                                                                                      new BinaryOperator("Status", Status.Progress),
                                                                                                      new BinaryOperator("Status", Status.Open)
                                                                                                      )));

                                if (_locCustomerInvoiceLines != null && _locCustomerInvoiceLines.Count > 0)
                                {
                                    foreach (CustomerInvoiceLine _locCustomerInvoiceLine in _locCustomerInvoiceLines)
                                    {
                                        _locCustomerInvoiceLine.Select = true;
                                        _locCustomerInvoiceLine.Save();
                                        _locCustomerInvoiceLine.Session.CommitTransaction();
                                    }

                                    SuccessMessageShow("Customer Invoice Line has been Select");
                                }
                                else
                                {
                                    ErrorMessageShow("Data Customer Invoice Line Not Available");
                                }
                            }
                        }
                    }
                    if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                    {
                        _objectSpace.CommitChanges();
                        _objectSpace.Refresh();
                    }
                    if (View is ListView)
                    {
                        _objectSpace.CommitChanges();
                        View.ObjectSpace.Refresh();
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("BusinessObject = CustomerInvoiceLine" + ex.ToString());
            }
        }

        private void CustomerInvoiceLineUnselectAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;

                if (_objectToProcess != null)
                {
                    foreach (Object obj in _objectToProcess)
                    {
                        CustomerInvoiceLine _locCustomerInvoiceLineOS = (CustomerInvoiceLine)_objectSpace.GetObject(obj);

                        if (_locCustomerInvoiceLineOS != null)
                        {
                            if (_locCustomerInvoiceLineOS.Session != null)
                            {
                                _currSession = _locCustomerInvoiceLineOS.Session;
                            }

                            if (_locCustomerInvoiceLineOS.Code != null && _currSession != null)
                            {
                                _currObjectId = _locCustomerInvoiceLineOS.Code;

                                XPCollection<CustomerInvoiceLine> _locCustomerInvoiceLines = new XPCollection<CustomerInvoiceLine>
                                                                                                     (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                      new BinaryOperator("Code", _currObjectId),
                                                                                                      new GroupOperator(GroupOperatorType.Or,
                                                                                                      new BinaryOperator("Status", Status.Progress),
                                                                                                      new BinaryOperator("Status", Status.Open)
                                                                                                      )));

                                if (_locCustomerInvoiceLines != null && _locCustomerInvoiceLines.Count > 0)
                                {
                                    foreach (CustomerInvoiceLine _locCustomerInvoiceLine in _locCustomerInvoiceLines)
                                    {
                                        _locCustomerInvoiceLine.Select = false;
                                        _locCustomerInvoiceLine.Save();
                                        _locCustomerInvoiceLine.Session.CommitTransaction();
                                    }

                                    SuccessMessageShow("Customer Invoice Line has been Unselect");
                                }
                                else
                                {
                                    ErrorMessageShow("Data Customer Invoice Line Not Available");
                                }
                            }
                        }
                    }
                    if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                    {
                        _objectSpace.CommitChanges();
                        _objectSpace.Refresh();
                    }
                    if (View is ListView)
                    {
                        _objectSpace.CommitChanges();
                        View.ObjectSpace.Refresh();
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("BusinessObject = CustomerInvoiceLine" + ex.ToString());
            }
        }

        private void CustomerInvoiceLineTaxAndDiscountAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;

                if (_objectToProcess != null)
                {
                    foreach (Object obj in _objectToProcess)
                    {
                        CustomerInvoiceLine _locCustomerInvoiceLineOS = (CustomerInvoiceLine)_objectSpace.GetObject(obj);

                        if (_locCustomerInvoiceLineOS != null)
                        {
                            if (_locCustomerInvoiceLineOS.Session != null)
                            {
                                _currSession = _locCustomerInvoiceLineOS.Session;
                            }

                            if (_locCustomerInvoiceLineOS.Code != null && _currSession != null)
                            {
                                _currObjectId = _locCustomerInvoiceLineOS.Code;

                                XPCollection<CustomerInvoiceLine> _locCustomerInvoiceLines = new XPCollection<CustomerInvoiceLine>
                                                                                                     (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                      new BinaryOperator("Code", _currObjectId),
                                                                                                      new GroupOperator(GroupOperatorType.Or,
                                                                                                      new BinaryOperator("Status", Status.Progress),
                                                                                                      new BinaryOperator("Status", Status.Open)
                                                                                                      )));

                                if (_locCustomerInvoiceLines != null && _locCustomerInvoiceLines.Count > 0)
                                {
                                    foreach (CustomerInvoiceLine _locCustomerInvoiceLine in _locCustomerInvoiceLines)
                                    {
                                        SetTaxBeforeDiscount(_currSession, _locCustomerInvoiceLine);
                                        SetDiscountGross(_currSession, _locCustomerInvoiceLine);
                                        SetTaxAfterDiscount(_currSession, _locCustomerInvoiceLine);
                                        SetDiscountNet(_currSession, _locCustomerInvoiceLine);
                                    }

                                    SuccessMessageShow("Customer Invoice Line has been Processed");
                                }
                                else
                                {
                                    ErrorMessageShow("Data Customer Invoice Line Not Available");
                                }
                            }
                        }
                    }
                    if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                    {
                        _objectSpace.CommitChanges();
                        _objectSpace.Refresh();
                    }
                    if (View is ListView)
                    {
                        _objectSpace.CommitChanges();
                        View.ObjectSpace.Refresh();
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("BusinessObject = CustomerInvoiceLine " + ex.ToString());
            }
        }

        private void CustomerInvoiceLineListviewFilterSelectionAction_Execute(object sender, SingleChoiceActionExecuteEventArgs e)
        {
            try
            {
                if ((View is ListView) & (View.ObjectTypeInfo.Type == typeof(CustomerInvoiceLine)))
                {
                    if ((Status)e.SelectedChoiceActionItem.Data == Status.None)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.None, BinaryOperatorType.NotEqual);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Open)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Open, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Progress)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Progress, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Posted)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Posted, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Lock)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Lock, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Close)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Close, BinaryOperatorType.Equal);
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = CustomerInvoiceLine " + ex.ToString());
            }
        }

        //======================================== Code In Here ========================================

        #region Tax

        private void SetTaxBeforeDiscount(Session _currSession, CustomerInvoiceLine _locCustomerInvoiceLineXPO)
        {
            try
            {
                double _locTxAmount = 0;
                double _locTAmount = 0;
                if (_locCustomerInvoiceLineXPO != null)
                {
                    if (_locCustomerInvoiceLineXPO.Tax != null && _locCustomerInvoiceLineXPO.TaxChecked == false)
                    {
                        if (_locCustomerInvoiceLineXPO.Tax.TaxRule == TaxRule.BeforeDiscount)
                        {
                            _locTxAmount = _locCustomerInvoiceLineXPO.TxValue / 100 * _locCustomerInvoiceLineXPO.TUAmount;
                            if (_locCustomerInvoiceLineXPO.Tax.TaxNature == TaxNature.Increase)
                            {
                                _locTAmount = _locCustomerInvoiceLineXPO.TAmount + _locTxAmount;
                            }
                            if (_locCustomerInvoiceLineXPO.Tax.TaxNature == TaxNature.Decrease)
                            {
                                _locTAmount = _locCustomerInvoiceLineXPO.TAmount - _locTxAmount;
                            }
                            _locCustomerInvoiceLineXPO.TxAmount = _locTxAmount;
                            _locCustomerInvoiceLineXPO.TAmount = _locTAmount;
                            _locCustomerInvoiceLineXPO.TaxChecked = true;
                            _locCustomerInvoiceLineXPO.Save();
                            _locCustomerInvoiceLineXPO.Session.CommitTransaction();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("BusinessObject = CustomerInvoiceLine" + ex.ToString());
            }
        }

        private void SetDiscountGross(Session _currSession, CustomerInvoiceLine _locCustomerInvoiceLineXPO)
        {
            try
            {
                if (_locCustomerInvoiceLineXPO != null)
                {
                    if (_locCustomerInvoiceLineXPO.DiscountRule != null)
                    {
                        double _locTUAmountDiscount = 0;
                        double _locTUAmountDiscount2 = 0;
                        AccountingPeriodicLine _locAcctPerLine = null;

                        #region Gross

                        if (_locCustomerInvoiceLineXPO.DiscountRule.GrossAmountRule == true)
                        {
                            if (_locCustomerInvoiceLineXPO.TUAmount > 0 && _locCustomerInvoiceLineXPO.TUAmount >= _locCustomerInvoiceLineXPO.DiscountRule.GrossTotalUAmount)
                            {
                                if (_locCustomerInvoiceLineXPO.DiscountRule.DiscountType == DiscountType.Percentage)
                                {
                                    _locTUAmountDiscount = _locCustomerInvoiceLineXPO.TUAmount * _locCustomerInvoiceLineXPO.DiscountRule.Value / 100;
                                }
                                if (_locCustomerInvoiceLineXPO.DiscountRule.DiscountType == DiscountType.Value)
                                {
                                    _locTUAmountDiscount = _locCustomerInvoiceLineXPO.DiscountRule.Value;
                                }

                                #region ValBasedAccount
                                if (_locCustomerInvoiceLineXPO.DiscountRule.ValBasedAccount == true)
                                {
                                    if (_locCustomerInvoiceLineXPO.AccountingPeriodicLine != null)
                                    {
                                        _locAcctPerLine = _locCustomerInvoiceLineXPO.AccountingPeriodicLine;
                                        if (_locAcctPerLine.BalHold < _locTUAmountDiscount)
                                        {
                                            _locTUAmountDiscount = _locAcctPerLine.BalHold;
                                        }

                                        _locAcctPerLine.BalHold = _locAcctPerLine.BalHold + _locTUAmountDiscount;
                                        _locAcctPerLine.Save();
                                        _locAcctPerLine.Session.CommitTransaction();
                                    }
                                    else
                                    {
                                        if (_locCustomerInvoiceLineXPO.DiscountRule.AccountNo != null)
                                        {
                                            AccountingPeriodic _locAccountPeriodic = _currSession.FindObject<AccountingPeriodic>
                                                                            (new GroupOperator(GroupOperatorType.And,
                                                                            new BinaryOperator("Company", _locCustomerInvoiceLineXPO.Company),
                                                                            new BinaryOperator("Workplace", _locCustomerInvoiceLineXPO.Workplace),
                                                                            new BinaryOperator("Active", true)));
                                            if (_locAccountPeriodic != null)
                                            {
                                                AccountingPeriodicLine _locAccountingPeriodicLine = _currSession.FindObject<AccountingPeriodicLine>
                                                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                                                    new BinaryOperator("AccountingPeriodic", _locAccountPeriodic),
                                                                                                    new BinaryOperator("AccountNo", _locCustomerInvoiceLineXPO.DiscountRule.AccountNo)));
                                                if (_locAccountingPeriodicLine != null)
                                                {
                                                    _locAcctPerLine = _locAccountingPeriodicLine;
                                                    if (_locAccountingPeriodicLine.BalHold < _locAccountingPeriodicLine.Balance)
                                                    {
                                                        _locTUAmountDiscount2 = _locAccountingPeriodicLine.Balance - _locAccountingPeriodicLine.BalHold;
                                                        if (_locTUAmountDiscount2 < _locTUAmountDiscount)
                                                        {
                                                            _locTUAmountDiscount = _locTUAmountDiscount2;
                                                        }
                                                    }
                                                    else
                                                    {
                                                        _locTUAmountDiscount = 0;
                                                    }
                                                    _locAcctPerLine = _locAccountingPeriodicLine;
                                                    _locAccountingPeriodicLine.BalHold = _locAccountingPeriodicLine.BalHold + _locTUAmountDiscount;
                                                    _locAccountingPeriodicLine.Save();
                                                    _locAccountingPeriodicLine.Session.CommitTransaction();
                                                }
                                            }
                                        }
                                    }
                                }
                                #endregion ValBasedAccount

                                _locCustomerInvoiceLineXPO.AccountingPeriodicLine = _locAcctPerLine;
                                _locCustomerInvoiceLineXPO.DiscountChecked = true;
                                _locCustomerInvoiceLineXPO.DiscAmount = _locTUAmountDiscount;
                                _locCustomerInvoiceLineXPO.TAmount = _locCustomerInvoiceLineXPO.TAmount - _locTUAmountDiscount;
                                _locCustomerInvoiceLineXPO.Save();
                                _locCustomerInvoiceLineXPO.Session.CommitTransaction();
                            }
                        }
                        #endregion Gross
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("BusinessObject = CustomerInvoiceLine" + ex.ToString());
            }
        }

        private void SetTaxAfterDiscount(Session _currSession, CustomerInvoiceLine _locCustomerInvoiceLineXPO)
        {
            try
            {
                double _locTxAmount = 0;
                double _locTAmount = 0;
                if (_locCustomerInvoiceLineXPO != null)
                {
                    if (_locCustomerInvoiceLineXPO.Tax != null )
                    {
                        if(_locCustomerInvoiceLineXPO.DiscountRule != null)
                        {
                            if (_locCustomerInvoiceLineXPO.Tax.TaxRule == TaxRule.AfterDiscount && _locCustomerInvoiceLineXPO.DiscountRule.GrossAmountRule == true)
                            {
                                _locTxAmount = _locCustomerInvoiceLineXPO.TxValue / 100 * (_locCustomerInvoiceLineXPO.TUAmount - _locCustomerInvoiceLineXPO.DiscAmount);

                                if (_locCustomerInvoiceLineXPO.Tax.TaxNature == TaxNature.Increase)
                                {
                                    _locTAmount = _locCustomerInvoiceLineXPO.TAmount + _locTxAmount;
                                }
                                if (_locCustomerInvoiceLineXPO.Tax.TaxNature == TaxNature.Decrease)
                                {
                                    _locTAmount = _locCustomerInvoiceLineXPO.TAmount - _locTxAmount;
                                }

                                _locCustomerInvoiceLineXPO.TxAmount = _locTxAmount;
                                _locCustomerInvoiceLineXPO.TAmount = _locTAmount;
                                _locCustomerInvoiceLineXPO.TaxChecked = true;
                                _locCustomerInvoiceLineXPO.Save();
                                _locCustomerInvoiceLineXPO.Session.CommitTransaction();
                            }
                        }
                        else
                        {
                            if (_locCustomerInvoiceLineXPO.Tax.TaxRule == TaxRule.AfterDiscount )
                            {
                                _locTxAmount = _locCustomerInvoiceLineXPO.TxValue / 100 * (_locCustomerInvoiceLineXPO.TUAmount);

                                if (_locCustomerInvoiceLineXPO.Tax.TaxNature == TaxNature.Increase)
                                {
                                    _locTAmount = _locCustomerInvoiceLineXPO.TAmount + _locTxAmount;
                                }
                                if (_locCustomerInvoiceLineXPO.Tax.TaxNature == TaxNature.Decrease)
                                {
                                    _locTAmount = _locCustomerInvoiceLineXPO.TAmount - _locTxAmount;
                                }

                                _locCustomerInvoiceLineXPO.TxAmount = _locTxAmount;
                                _locCustomerInvoiceLineXPO.TAmount = _locTAmount;
                                _locCustomerInvoiceLineXPO.TaxChecked = true;
                                _locCustomerInvoiceLineXPO.Save();
                                _locCustomerInvoiceLineXPO.Session.CommitTransaction();
                            }
                        }
                        
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("BusinessObject = CustomerInvoiceLine" + ex.ToString());
            }
        }

        private void SetDiscountNet(Session _currSession, CustomerInvoiceLine _locCustomerInvoiceLineXPO)
        {
            try
            {
                if (_locCustomerInvoiceLineXPO != null)
                {
                    if (_locCustomerInvoiceLineXPO.DiscountRule != null && _locCustomerInvoiceLineXPO.Tax != null)
                    {
                        double _locTUAmountDiscount = 0;
                        double _locTUAmountDiscount2 = 0;
                        AccountingPeriodicLine _locAcctPerLine = null;

                        #region Net
                        if (_locCustomerInvoiceLineXPO.DiscountRule.NetAmountRule == true && _locCustomerInvoiceLineXPO.Tax.TaxRule == TaxRule.BeforeDiscount)
                        {
                            if (_locCustomerInvoiceLineXPO.TAmount > 0 && _locCustomerInvoiceLineXPO.TAmount >= _locCustomerInvoiceLineXPO.DiscountRule.NetTotalUAmount)
                            {
                                if (_locCustomerInvoiceLineXPO.DiscountRule.DiscountType == DiscountType.Percentage)
                                {
                                    _locTUAmountDiscount = _locCustomerInvoiceLineXPO.TAmount * _locCustomerInvoiceLineXPO.DiscountRule.Value / 100;
                                }
                                if (_locCustomerInvoiceLineXPO.DiscountRule.DiscountType == DiscountType.Value)
                                {
                                    _locTUAmountDiscount = _locCustomerInvoiceLineXPO.DiscountRule.Value;
                                }

                                #region ValBasedAccount
                                if (_locCustomerInvoiceLineXPO.DiscountRule.ValBasedAccount == true)
                                {
                                    if (_locCustomerInvoiceLineXPO.AccountingPeriodicLine != null)
                                    {
                                        _locAcctPerLine = _locCustomerInvoiceLineXPO.AccountingPeriodicLine;
                                        if (_locAcctPerLine.BalHold < _locTUAmountDiscount)
                                        {
                                            _locTUAmountDiscount = _locAcctPerLine.BalHold;
                                        }

                                        _locAcctPerLine.BalHold = _locAcctPerLine.BalHold + _locTUAmountDiscount;
                                        _locAcctPerLine.Save();
                                        _locAcctPerLine.Session.CommitTransaction();
                                    }
                                    else
                                    {
                                        if (_locCustomerInvoiceLineXPO.DiscountRule.AccountNo != null)
                                        {
                                            AccountingPeriodic _locAccountPeriodic = _currSession.FindObject<AccountingPeriodic>
                                                                            (new GroupOperator(GroupOperatorType.And,
                                                                            new BinaryOperator("Company", _locCustomerInvoiceLineXPO.Company),
                                                                            new BinaryOperator("Workplace", _locCustomerInvoiceLineXPO.Workplace),
                                                                            new BinaryOperator("Active", true)));
                                            if (_locAccountPeriodic != null)
                                            {
                                                AccountingPeriodicLine _locAccountingPeriodicLine = _currSession.FindObject<AccountingPeriodicLine>
                                                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                                                    new BinaryOperator("AccountingPeriodic", _locAccountPeriodic),
                                                                                                    new BinaryOperator("AccountNo", _locCustomerInvoiceLineXPO.DiscountRule.AccountNo)));
                                                if (_locAccountingPeriodicLine != null)
                                                {
                                                    _locAcctPerLine = _locAccountingPeriodicLine;
                                                    if (_locAccountingPeriodicLine.BalHold < _locAccountingPeriodicLine.Balance)
                                                    {
                                                        _locTUAmountDiscount2 = _locAccountingPeriodicLine.Balance - _locAccountingPeriodicLine.BalHold;
                                                        if (_locTUAmountDiscount2 < _locTUAmountDiscount)
                                                        {
                                                            _locTUAmountDiscount = _locTUAmountDiscount2;
                                                        }
                                                    }
                                                    else
                                                    {
                                                        _locTUAmountDiscount = 0;
                                                    }

                                                    _locAccountingPeriodicLine.BalHold = _locAccountingPeriodicLine.BalHold + _locTUAmountDiscount;
                                                    _locAccountingPeriodicLine.Save();
                                                    _locAccountingPeriodicLine.Session.CommitTransaction();
                                                }
                                            }
                                        }
                                    }
                                }
                                #endregion ValBasedAccount

                                _locCustomerInvoiceLineXPO.AccountingPeriodicLine = _locAcctPerLine;
                                _locCustomerInvoiceLineXPO.DiscountChecked = true;
                                _locCustomerInvoiceLineXPO.DiscAmount = _locTUAmountDiscount;
                                _locCustomerInvoiceLineXPO.TAmount = _locCustomerInvoiceLineXPO.TAmount - _locTUAmountDiscount;
                                _locCustomerInvoiceLineXPO.Save();
                                _locCustomerInvoiceLineXPO.Session.CommitTransaction();
                            }
                        }
                        #endregion Net
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("BusinessObject = CustomerInvoiceLine" + ex.ToString());
            }
        }

        #endregion Tax

        #region Global Method

        private void SuccessMessageShow(string _locActionName)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 2000;
            options.Message = string.Format("{0}", _locActionName);
            options.Type = InformationType.Success;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Success";
            options.Win.Type = WinMessageType.Flyout;
            Application.ShowViewStrategy.ShowMessage(options);
        }

        private void ErrorMessageShow(string _locMessage)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 2000;
            options.Message = string.Format(_locMessage);
            options.Type = InformationType.Warning;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Error";
            options.Win.Type = WinMessageType.Alert;
            Application.ShowViewStrategy.ShowMessage(options);
        }

        #endregion Global Method

    }
}
