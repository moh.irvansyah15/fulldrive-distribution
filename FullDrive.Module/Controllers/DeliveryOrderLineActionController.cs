﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;

using FullDrive.Module.BusinessObjects;
using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.Xpo;
using System.Collections;
using DevExpress.Xpo;
using System.IO;
using System.Web;

namespace FullDrive.Module.Controllers
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class DeliveryOrderLineActionController : ViewController
    {
        private ChoiceActionItem _selectionDeliveryStatus;

        public DeliveryOrderLineActionController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.
            #region DeliveryStatus
            DeliveryOrderLineDeliveryStatusAction.Items.Clear();
            foreach (object _currApproval in Enum.GetValues(typeof(CustomProcess.DeliveryStatus)))
            {
                EnumDescriptor _ed = new EnumDescriptor(typeof(CustomProcess.DeliveryStatus));
                _selectionDeliveryStatus = new ChoiceActionItem(_ed.GetCaption(_currApproval), _currApproval);
                DeliveryOrderLineDeliveryStatusAction.Items.Add(_selectionDeliveryStatus);
            }
            #endregion DeliveryStatus

        }
        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
        }

        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }

        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        private void DeliveryOrderLineDeliveryStatusAction_Execute(object sender, SingleChoiceActionExecuteEventArgs e)
        {
            try
            {
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currentSession = null;
                string _currObjectId = null;
                string _locMessage = null;

                foreach (Object obj in _objectsToProcess)
                {
                    DeliveryOrderLine _objInNewObjectSpace = (DeliveryOrderLine)_objectSpace.GetObject(obj);

                    if (_objInNewObjectSpace != null)
                    {
                        if (_objInNewObjectSpace.Code != null)
                        {
                            _currObjectId = _objInNewObjectSpace.Code;
                        }
                        if (_objInNewObjectSpace.Session != null)
                        {
                            _currentSession = _objInNewObjectSpace.Session;
                        }
                    }

                    if (_currObjectId != null && _currentSession != null)
                    {
                        DeliveryStatus _locDeliveryStatus = DeliveryStatus.None;

                        DeliveryOrderLine _locDeliveryOrderLineXPO = _currentSession.FindObject<DeliveryOrderLine>
                                                                        (new GroupOperator(GroupOperatorType.And,
                                                                         new BinaryOperator("Code", _currObjectId),
                                                                         new GroupOperator(GroupOperatorType.Or,
                                                                         new BinaryOperator("Status", Status.Open),
                                                                         new BinaryOperator("Status", Status.Progress),
                                                                         new BinaryOperator("Status", Status.Posted))));

                        if (_locDeliveryOrderLineXPO != null)
                        {
                            if ((DeliveryStatus)e.SelectedChoiceActionItem.Data == DeliveryStatus.CancelAll)
                            {
                                _locDeliveryStatus = DeliveryStatus.CancelAll;
                                _locMessage = "Cancel Transaction";
                            }
                            if ((DeliveryStatus)e.SelectedChoiceActionItem.Data == DeliveryStatus.PendingAll)
                            {
                                _locDeliveryStatus = DeliveryStatus.PendingAll;
                                _locMessage = "Pending Transaction";
                            }
                            if ((DeliveryStatus)e.SelectedChoiceActionItem.Data != DeliveryStatus.CancelAll
                                || (DeliveryStatus)e.SelectedChoiceActionItem.Data != DeliveryStatus.PendingAll)
                            {
                                _locDeliveryStatus = (DeliveryStatus)e.SelectedChoiceActionItem.Data;
                            }

                            _locDeliveryOrderLineXPO.DeliveryStatus = _locDeliveryStatus;
                            _locDeliveryOrderLineXPO.DeliveryStatusDate = now;
                            _locDeliveryOrderLineXPO.Message = _locMessage;
                            _locDeliveryOrderLineXPO.Save();
                            _locDeliveryOrderLineXPO.Session.CommitTransaction();
                            SuccessMessageShow("DeliveryOrderLine has been Change DeliveryStatus");
                        }
                        else
                        {
                            ErrorMessageShow("DeliveryOrderLine Not Available");
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = DeliveryOrderLine " + ex.ToString());
            }
        }

        private void DeliveryOrderLineSelectAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;

                if (_objectToProcess != null)
                {
                    foreach (Object obj in _objectToProcess)
                    {
                        DeliveryOrderLine _locDeliveryOrderLineOS = (DeliveryOrderLine)_objectSpace.GetObject(obj);

                        if (_locDeliveryOrderLineOS != null)
                        {
                            if (_locDeliveryOrderLineOS.Session != null)
                            {
                                _currSession = _locDeliveryOrderLineOS.Session;
                            }

                            if (_locDeliveryOrderLineOS.Code != null && _currSession != null)
                            {
                                _currObjectId = _locDeliveryOrderLineOS.Code;

                                XPCollection<DeliveryOrderLine> _locDeliveryOrderLines = new XPCollection<DeliveryOrderLine>
                                                                                                     (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                      new BinaryOperator("Code", _currObjectId),
                                                                                                      new GroupOperator(GroupOperatorType.Or,
                                                                                                      new BinaryOperator("Status", Status.Progress),
                                                                                                      new BinaryOperator("Status", Status.Open)
                                                                                                      )));

                                if (_locDeliveryOrderLines != null && _locDeliveryOrderLines.Count > 0)
                                {
                                    foreach (DeliveryOrderLine _locDeliveryOrderLine in _locDeliveryOrderLines)
                                    {
                                        _locDeliveryOrderLine.Select = true;
                                        _locDeliveryOrderLine.Save();
                                        _locDeliveryOrderLine.Session.CommitTransaction();
                                    }

                                    SuccessMessageShow("DeliveryOrderLine has been Select");
                                }
                                else
                                {
                                    ErrorMessageShow("Data DeliveryOrderLine Not Available");
                                }
                            }
                        }
                    }
                    if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                    {
                        _objectSpace.CommitChanges();
                        _objectSpace.Refresh();
                    }
                    if (View is ListView)
                    {
                        _objectSpace.CommitChanges();
                        View.ObjectSpace.Refresh();
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("BusinessObject = DeliveryOrderLine" + ex.ToString());
            }
        }

        private void DeliveryOrderLineUnselectAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;

                if (_objectToProcess != null)
                {
                    foreach (Object obj in _objectToProcess)
                    {
                        DeliveryOrderLine _locDeliveryOrderLineOS = (DeliveryOrderLine)_objectSpace.GetObject(obj);

                        if (_locDeliveryOrderLineOS != null)
                        {
                            if (_locDeliveryOrderLineOS.Session != null)
                            {
                                _currSession = _locDeliveryOrderLineOS.Session;
                            }

                            if (_locDeliveryOrderLineOS.Code != null && _currSession != null)
                            {
                                _currObjectId = _locDeliveryOrderLineOS.Code;

                                XPCollection<DeliveryOrderLine> _locDeliveryOrderLines = new XPCollection<DeliveryOrderLine>
                                                                                                     (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                      new BinaryOperator("Code", _currObjectId),
                                                                                                      new GroupOperator(GroupOperatorType.Or,
                                                                                                      new BinaryOperator("Status", Status.Progress),
                                                                                                      new BinaryOperator("Status", Status.Open)
                                                                                                      )));

                                if (_locDeliveryOrderLines != null && _locDeliveryOrderLines.Count > 0)
                                {
                                    foreach (DeliveryOrderLine _locDeliveryOrderLine in _locDeliveryOrderLines)
                                    {
                                        _locDeliveryOrderLine.Select = false;
                                        _locDeliveryOrderLine.Save();
                                        _locDeliveryOrderLine.Session.CommitTransaction();
                                    }

                                    SuccessMessageShow("DeliveryOrderLine has been Unselect");
                                }
                                else
                                {
                                    ErrorMessageShow("Data DeliveryOrderLine Not Available");
                                }
                            }
                        }
                    }
                    if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                    {
                        _objectSpace.CommitChanges();
                        _objectSpace.Refresh();
                    }
                    if (View is ListView)
                    {
                        _objectSpace.CommitChanges();
                        View.ObjectSpace.Refresh();
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("BusinessObject = DeliveryOrderLine" + ex.ToString());
            }
        }

        //=========================================== Code In Here ===========================================

        #region Global Method

        private void SuccessMessageShow(string _locActionName)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 2000;
            options.Message = string.Format("{0} !", _locActionName);
            options.Type = InformationType.Success;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Success";
            options.Win.Type = WinMessageType.Flyout;
            Application.ShowViewStrategy.ShowMessage(options);
        }

        private void ErrorMessageShow(string _locMessage)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 2000;
            options.Message = string.Format(_locMessage);
            options.Type = InformationType.Warning;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Error";
            options.Win.Type = WinMessageType.Alert;
            Application.ShowViewStrategy.ShowMessage(options);
        }

        #endregion Global Method

        
    }
}
