﻿namespace FullDrive.Module.Controllers
{
    partial class SalesInvoiceCollectionActionController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.SalesInvoiceCollectionShowSIMAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            // 
            // SalesInvoiceCollectionShowSIMAction
            // 
            this.SalesInvoiceCollectionShowSIMAction.Caption = "Show SIM";
            this.SalesInvoiceCollectionShowSIMAction.ConfirmationMessage = null;
            this.SalesInvoiceCollectionShowSIMAction.Id = "SalesInvoiceCollectionShowSIMActionId";
            this.SalesInvoiceCollectionShowSIMAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.SalesInvoiceCollection);
            this.SalesInvoiceCollectionShowSIMAction.ToolTip = null;
            this.SalesInvoiceCollectionShowSIMAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.SalesInvoiceCollectionShowSIMAction_Execute);
            // 
            // SalesInvoiceCollectionActionController
            // 
            this.Actions.Add(this.SalesInvoiceCollectionShowSIMAction);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SimpleAction SalesInvoiceCollectionShowSIMAction;
    }
}
