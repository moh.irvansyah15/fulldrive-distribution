﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;

using FullDrive.Module.BusinessObjects;
using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.Xpo;
using System.Collections;
using DevExpress.Xpo;
using System.Web;
using System.IO;

namespace FullDrive.Module.Controllers
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class PaymentRealizationActionController : ViewController
    {
        private ChoiceActionItem _selectionListviewFilter;
        private ChoiceActionItem _selectionApprovalListviewFilter;
        private ChoiceActionItem _setApprovalLevel;

        public PaymentRealizationActionController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.
            #region FilterStatus
            PaymentRealizationListviewFilterSelectionAction.Items.Clear();
            foreach (object _currApproval in Enum.GetValues(typeof(CustomProcess.Status)))
            {
                EnumDescriptor _ed = new EnumDescriptor(typeof(CustomProcess.Status));
                _selectionListviewFilter = new ChoiceActionItem(_ed.GetCaption(_currApproval), _currApproval);
                PaymentRealizationListviewFilterSelectionAction.Items.Add(_selectionListviewFilter);
            }
            #endregion FilterStatus

            #region FilterApproval
            PaymentRealizationListviewFilterApprovalSelectionAction.Items.Clear();
            foreach (object _currApprovalFilter in Enum.GetValues(typeof(CustomProcess.ApprovalFilter)))
            {
                EnumDescriptor _ed = new EnumDescriptor(typeof(CustomProcess.ApprovalFilter));
                _selectionApprovalListviewFilter = new ChoiceActionItem(_ed.GetCaption(_currApprovalFilter), _currApprovalFilter);
                PaymentRealizationListviewFilterApprovalSelectionAction.Items.Add(_selectionApprovalListviewFilter);
            }
            #endregion FilterApproval
           
        }

        protected override void OnActivated()
        {
            TypeOfView = typeof(ObjectView);
            base.OnActivated();
            // Perform various tasks depending on the target View.
            #region Approval
            var _locUser = SecuritySystem.CurrentUserName;

            if (this.ObjectSpace != null)
            {
                UserAccess _locUserAccess = this.ObjectSpace.FindObject<UserAccess>(new BinaryOperator("UserName", _locUser));

                if (_locUserAccess != null)
                {
                    PaymentRealizationApprovalAction.Items.Clear();

                    XPCollection<ApplicationSetupDetail> _locAppSetupDetails = new XPCollection<ApplicationSetupDetail>
                                                                                 (_locUserAccess.Session, new GroupOperator(GroupOperatorType.And,
                                                                                  new BinaryOperator("UserAccess", _locUserAccess),
                                                                                  new BinaryOperator("ObjectList", CustomProcess.ObjectList.PaymentRealization),
                                                                                  new BinaryOperator("FunctionList", CustomProcess.FunctionList.Approval),
                                                                                  new BinaryOperator("Active", true)));

                    if (_locAppSetupDetails != null && _locAppSetupDetails.Count > 0)
                    {

                        foreach (ApplicationSetupDetail _locAppSetupDetail in _locAppSetupDetails)
                        {
                            foreach (object _currApproval in Enum.GetValues(typeof(CustomProcess.ApprovalLevel)))
                            {
                                if (_locAppSetupDetail.ApprovalLevel != CustomProcess.ApprovalLevel.None)
                                {
                                    EnumDescriptor _ed = new EnumDescriptor(typeof(CustomProcess.ApprovalLevel));
                                    _setApprovalLevel = new ChoiceActionItem(_ed.GetCaption(_locAppSetupDetail.ApprovalLevel), _locAppSetupDetail.ApprovalLevel);
                                }
                            }
                            PaymentRealizationApprovalAction.Items.Add(_setApprovalLevel);
                        }
                    }
                }
            }
            #endregion Approval

        }

        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }

        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        private void PaymentRealizationGetCAAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;

                if (_objectsToProcess != null)
                {
                    foreach (Object obj in _objectsToProcess)
                    {
                        PaymentRealization _locPaymentRealizationOS = (PaymentRealization)_objectSpace.GetObject(obj);

                        if (_locPaymentRealizationOS != null)
                        {
                            if(_locPaymentRealizationOS.Session != null)
                            {
                                _currSession = _locPaymentRealizationOS.Session;
                            }

                            if (_locPaymentRealizationOS.Code != null && _currSession != null)
                            {
                                _currObjectId = _locPaymentRealizationOS.Code;

                                PaymentRealization _locPaymentRealizationXPO = _currSession.FindObject<PaymentRealization>
                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                     new BinaryOperator("Code", _currObjectId)));

                                if (_locPaymentRealizationXPO != null)
                                {
                                    if (_locPaymentRealizationXPO.Status == Status.Open || _locPaymentRealizationXPO.Status == Status.Progress)
                                    {
                                        XPCollection<CashAdvanceCollection> _locCashAdvanceCollections = new XPCollection<CashAdvanceCollection>
                                                                                        (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                        new BinaryOperator("PaymentRealization", _locPaymentRealizationXPO),
                                                                                        new GroupOperator(GroupOperatorType.Or,
                                                                                        new BinaryOperator("Status", Status.Open),
                                                                                        new BinaryOperator("Status", Status.Progress))));

                                        if (_locCashAdvanceCollections != null && _locCashAdvanceCollections.Count() > 0)
                                        {
                                            foreach (CashAdvanceCollection _locCashAdvanceCollection in _locCashAdvanceCollections)
                                            {
                                                if (_locCashAdvanceCollection.CashAdvance != null && _locCashAdvanceCollection.PaymentRealization != null)
                                                {
                                                    GetCashAdvanceMonitoring(_currSession, _locCashAdvanceCollection.CashAdvance, _locPaymentRealizationXPO);
                                                }
                                            }
                                            SuccessMessageShow("Cash Advance Has Been Successfully Getting into Payment Realization");
                                        }
                                    }
                                }
                                else
                                {
                                    ErrorMessageShow("Cash Advance Order Not Available");
                                }
                            }
                            else
                            {
                                ErrorMessageShow("Cash Advance Order Not Available");
                            }
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject =  PaymentRealization " + ex.ToString());
            }
        }

        private void PaymentRealizationProgressAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;

                if (_objectToProcess != null)
                {
                    foreach (Object obj in _objectToProcess)
                    {
                        PaymentRealization _locPaymentRealizationOS = (PaymentRealization)_objectSpace.GetObject(obj);

                        if (_locPaymentRealizationOS != null)
                        {
                            if(_locPaymentRealizationOS.Session != null)
                            {
                                _currSession = _locPaymentRealizationOS.Session;
                            }

                            if (_locPaymentRealizationOS.Code != null && _currSession != null)
                            {
                                _currObjectId = _locPaymentRealizationOS.Code;

                                PaymentRealization _locPaymentRealizationXPO = _currSession.FindObject<PaymentRealization>
                                                                 (new GroupOperator(GroupOperatorType.And,
                                                                  new BinaryOperator("Code", _currObjectId)));

                                if (_locPaymentRealizationXPO != null)
                                {
                                    if (_locPaymentRealizationXPO.Status == Status.Open || _locPaymentRealizationXPO.Status == Status.Progress)
                                    {
                                        if (_locPaymentRealizationXPO.Status == Status.Open)
                                        {
                                            _locPaymentRealizationXPO.Status = Status.Progress;
                                            _locPaymentRealizationXPO.StatusDate = now;
                                            _locPaymentRealizationXPO.Save();
                                            _locPaymentRealizationXPO.Session.CommitTransaction();
                                        }

                                        XPCollection<PaymentRealizationLine> _locPaymentRealizationLines = new XPCollection<PaymentRealizationLine>
                                                                                                           (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                            new BinaryOperator("PaymentRealization", _locPaymentRealizationXPO),
                                                                                                            new BinaryOperator("Status", Status.Open)));

                                        if (_locPaymentRealizationLines != null && _locPaymentRealizationLines.Count > 0)
                                        {
                                            foreach (PaymentRealizationLine _locPaymentRealizationLine in _locPaymentRealizationLines)
                                            {
                                                _locPaymentRealizationLine.Status = Status.Progress;
                                                _locPaymentRealizationLine.StatusDate = now;
                                                _locPaymentRealizationLine.Save();
                                                _locPaymentRealizationLine.Session.CommitTransaction();
                                            }
                                        }

                                        XPCollection<CashAdvanceCollection> _locCashAdvanceCollections = new XPCollection<CashAdvanceCollection>
                                                                                                        (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                        new BinaryOperator("PaymentRealization", _locPaymentRealizationXPO),
                                                                                                        new BinaryOperator("Status", Status.Open)));
                                        if (_locCashAdvanceCollections != null && _locCashAdvanceCollections.Count() > 0)
                                        {
                                            foreach (CashAdvanceCollection _locCashAdvanceCollection in _locCashAdvanceCollections)
                                            {
                                                _locCashAdvanceCollection.Status = Status.Progress;
                                                _locCashAdvanceCollection.StatusDate = now;
                                                _locCashAdvanceCollection.Save();
                                                _locCashAdvanceCollection.Session.CommitTransaction();
                                            }
                                        }
                                    }

                                    SuccessMessageShow("Payment Realization has successfully updated to progress");
                                }
                                else
                                {
                                    ErrorMessageShow("Payment Realization Not Available");
                                }
                            }
                            else
                            {
                                ErrorMessageShow("Payment Realization Not Available");
                            }
                        }
                    }
                }
                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("BusinessObject = PaymentRealization" + ex.ToString());
            }
        }

        private void PaymentRealizationPostingAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;

                if (_objectsToProcess != null)
                {
                    foreach (Object obj in _objectsToProcess)
                    {
                        PaymentRealization _locPaymentRealizationOS = (PaymentRealization)_objectSpace.GetObject(obj);

                        if (_locPaymentRealizationOS != null)
                        {
                            if(_locPaymentRealizationOS.Session != null)
                            {
                                _currSession = _locPaymentRealizationOS.Session;
                            }

                            if (_locPaymentRealizationOS.Code != null && _currSession != null)
                            {
                                _currObjectId = _locPaymentRealizationOS.Code;

                                PaymentRealization _locPaymentRealizationXPO = _currSession.FindObject<PaymentRealization>
                                                                               (new GroupOperator(GroupOperatorType.And,
                                                                                new BinaryOperator("Code", _currObjectId)));

                                if (_locPaymentRealizationXPO != null)
                                {
                                    if (_locPaymentRealizationXPO.Status == Status.Progress || _locPaymentRealizationXPO.Status == Status.Posted)
                                    {
                                        ApprovalLine _locApprovalLineXPO = _currSession.FindObject<ApprovalLine>
                                                                           (new GroupOperator(GroupOperatorType.And,
                                                                            new BinaryOperator("EndApproval", true),
                                                                            new BinaryOperator("PaymentRealization", _locPaymentRealizationXPO)));

                                        if (_locApprovalLineXPO != null)
                                        {
                                            SetPaymentRealizationMonitoring(_currSession, _locPaymentRealizationXPO);
                                            if (CheckJournalSetup(_currSession, _locPaymentRealizationXPO) == true)
                                            {
                                                SetJournalRealization(_currSession, _locPaymentRealizationXPO);
                                            }   
                                            SetStatusPaymentRealizationLine(_currSession, _locPaymentRealizationXPO);
                                            SetStatusCashAdvanceMonitoring(_currSession, _locPaymentRealizationXPO);
                                            SetStatusPaymentRealization(_currSession, _locPaymentRealizationXPO);
                                            
                                        }
                                        else
                                        {
                                            ErrorMessageShow("Data Payment Realization Not Available");
                                        }
                                    }
                                    else
                                    {
                                        ErrorMessageShow("Data Payment Realization Not Available");
                                    }
                                }
                                else
                                {
                                    ErrorMessageShow("Data Payment Realization Not Available");
                                }
                            }
                            else
                            {
                                ErrorMessageShow("Data Payment Realization Not Available");
                            }
                        }
                        else
                        {
                            ErrorMessageShow("Data Payment Realization Not Available");
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("BusinessObject = PaymentRealization" + ex.ToString());
            }
        }

        private void PaymentRealizationApprovalAction_Execute(object sender, SingleChoiceActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                ApplicationSetupDetail _locAppSetDetail = null;
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                var User = SecuritySystem.CurrentUserName;
                Session _currentSession = null;
                string _currObjectId = null;

                foreach (Object obj in _objectsToProcess)
                {
                    PaymentRealization _objInNewObjectSpace = (PaymentRealization)_objectSpace.GetObject(obj);

                    if (_objInNewObjectSpace != null)
                    {
                        if (_objInNewObjectSpace.Code != null)
                        {
                            _currObjectId = _objInNewObjectSpace.Code;
                        }
                        if (_objInNewObjectSpace.Session != null)
                        {
                            _currentSession = _objInNewObjectSpace.Session;
                        }
                    }

                    if (_currObjectId != null && _currentSession != null)
                    {
                        UserAccess _locUserAccess = _currentSession.FindObject<UserAccess>(new BinaryOperator("UserName", User));

                        PaymentRealization _locPaymentRealizationXPO = _currentSession.FindObject<PaymentRealization>
                                                                        (new GroupOperator(GroupOperatorType.And,
                                                                         new BinaryOperator("Code", _currObjectId)));

                        if (_locPaymentRealizationXPO != null)
                        {
                            if (_locPaymentRealizationXPO.Status == Status.Progress)
                            {
                                ApprovalLine _locApprovalLine = _currentSession.FindObject<ApprovalLine>
                                                                            (new GroupOperator(GroupOperatorType.And,
                                                                             new BinaryOperator("PaymentRealization", _locPaymentRealizationXPO),
                                                                             new BinaryOperator("EndApproval", true)));
                                if (_locApprovalLine == null)
                                {
                                    #region Approval Level 1
                                    if ((ApprovalLevel)e.SelectedChoiceActionItem.Data == ApprovalLevel.Level1)
                                    {
                                        _locAppSetDetail = _globFunc.GetApplicationSetupDetailByApprovalLevel(_currentSession, _locUserAccess, ApprovalLevel.Level1, ObjectList.PaymentRealization);
                                        if (_locAppSetDetail != null)
                                        {
                                            //Buat bs input langsung ke approvalline
                                            ApprovalLine _locApprovalLineXPO = _currentSession.FindObject<ApprovalLine>
                                                                                (new GroupOperator(GroupOperatorType.And,
                                                                                 new BinaryOperator("PaymentRealization", _locPaymentRealizationXPO),
                                                                                 new BinaryOperator("ApprovalLevel", ApprovalLevel.Level1)));
                                            if (_locApprovalLineXPO == null)
                                            {

                                                if (_locAppSetDetail.EndApproval == true)
                                                {
                                                    ApprovalLine _saveDataAL = new ApprovalLine(_currentSession)
                                                    {
                                                        ApprovalDate = now,
                                                        ApprovalStatus = Status.Approved,
                                                        ApprovalLevel = ApprovalLevel.Level1,
                                                        EndApproval = true,
                                                        PaymentRealization = _locPaymentRealizationXPO,
                                                    };
                                                    _saveDataAL.Save();
                                                    _saveDataAL.Session.CommitTransaction();

                                                    _locPaymentRealizationXPO.ActivationPosting = true;
                                                    _locPaymentRealizationXPO.ActiveApproved1 = false;
                                                    _locPaymentRealizationXPO.ActiveApproved2 = false;
                                                    _locPaymentRealizationXPO.ActiveApproved3 = true;
                                                    _locPaymentRealizationXPO.Save();
                                                    _locPaymentRealizationXPO.Session.CommitTransaction();
                                                }
                                                else
                                                {
                                                    ApprovalLine _saveDataAL = new ApprovalLine(_currentSession)
                                                    {
                                                        ApprovalDate = now,
                                                        ApprovalStatus = Status.Approved,
                                                        ApprovalLevel = ApprovalLevel.Level1,
                                                        PaymentRealization = _locPaymentRealizationXPO,
                                                    };
                                                    _saveDataAL.Save();
                                                    _saveDataAL.Session.CommitTransaction();

                                                    _locPaymentRealizationXPO.ActiveApproved1 = true;
                                                    _locPaymentRealizationXPO.ActiveApproved2 = false;
                                                    _locPaymentRealizationXPO.ActiveApproved3 = false;
                                                    _locPaymentRealizationXPO.Save();
                                                    _locPaymentRealizationXPO.Session.CommitTransaction();
                                                }

                                                //Send Email
                                                ApprovalLine _locApprovalLineXPO2 = _currentSession.FindObject<ApprovalLine>
                                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                                     new BinaryOperator("PaymentRealization", _locPaymentRealizationXPO)));

                                                if (_locApprovalLineXPO2 != null)
                                                {
                                                    SendEmail(_currentSession, _locPaymentRealizationXPO, Status.Progress, _locApprovalLineXPO2);
                                                }

                                                SuccessMessageShow("PaymentRealization has successfully Approve");
                                            }
                                        }
                                    }
                                    #endregion Approval Level 1

                                    #region Approval Level 2
                                    if ((ApprovalLevel)e.SelectedChoiceActionItem.Data == ApprovalLevel.Level2)
                                    {
                                        _locAppSetDetail = _globFunc.GetApplicationSetupDetailByApprovalLevel(_currentSession, _locUserAccess, ApprovalLevel.Level2, ObjectList.PaymentRealization);

                                        if (_locAppSetDetail != null)
                                        {
                                            ApprovalLine _locApprovalLineXPO = _currentSession.FindObject<ApprovalLine>
                                                                                (new GroupOperator(GroupOperatorType.And,
                                                                                 new BinaryOperator("PaymentRealization", _locPaymentRealizationXPO),
                                                                                 new BinaryOperator("ApprovalLevel", ApprovalLevel.Level2)));

                                            if (_locApprovalLineXPO == null)
                                            {
                                                if (_locAppSetDetail.EndApproval == true)
                                                {
                                                    ApprovalLine _saveDataAL = new ApprovalLine(_currentSession)
                                                    {
                                                        ApprovalDate = now,
                                                        ApprovalStatus = Status.Approved,
                                                        ApprovalLevel = ApprovalLevel.Level2,
                                                        EndApproval = true,
                                                        PaymentRealization = _locPaymentRealizationXPO,
                                                    };
                                                    _saveDataAL.Save();
                                                    _saveDataAL.Session.CommitTransaction();

                                                    _locPaymentRealizationXPO.ActivationPosting = true;
                                                    _locPaymentRealizationXPO.ActiveApproved1 = false;
                                                    _locPaymentRealizationXPO.ActiveApproved2 = false;
                                                    _locPaymentRealizationXPO.ActiveApproved3 = true;
                                                    _locPaymentRealizationXPO.Save();
                                                    _locPaymentRealizationXPO.Session.CommitTransaction();
                                                }
                                                else
                                                {
                                                    ApprovalLine _saveDataAL = new ApprovalLine(_currentSession)
                                                    {
                                                        ApprovalDate = now,
                                                        ApprovalStatus = Status.Approved,
                                                        ApprovalLevel = ApprovalLevel.Level2,
                                                        PaymentRealization = _locPaymentRealizationXPO,
                                                    };
                                                    _saveDataAL.Save();
                                                    _saveDataAL.Session.CommitTransaction();

                                                    SetApprovalLine(_currentSession, _locPaymentRealizationXPO, ApprovalLevel.Level1);

                                                    _locPaymentRealizationXPO.ActiveApproved1 = false;
                                                    _locPaymentRealizationXPO.ActiveApproved2 = true;
                                                    _locPaymentRealizationXPO.ActiveApproved3 = false;
                                                    _locPaymentRealizationXPO.Save();
                                                    _locPaymentRealizationXPO.Session.CommitTransaction();
                                                }

                                                //Send Email
                                                ApprovalLine _locApprovalLineXPO2 = _currentSession.FindObject<ApprovalLine>
                                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                                     new BinaryOperator("PaymentRealization", _locPaymentRealizationXPO)));

                                                if (_locApprovalLineXPO2 != null)
                                                {
                                                    SendEmail(_currentSession, _locPaymentRealizationXPO, Status.Progress, _locApprovalLineXPO2);
                                                }

                                                SuccessMessageShow("PaymentRealization has successfully Approve");
                                            }
                                        }
                                    }
                                    #endregion Approval Level 2

                                    #region Approval Level 3
                                    if ((ApprovalLevel)e.SelectedChoiceActionItem.Data == ApprovalLevel.Level3)
                                    {
                                        _locAppSetDetail = _globFunc.GetApplicationSetupDetailByApprovalLevel(_currentSession, _locUserAccess, ApprovalLevel.Level3, ObjectList.PaymentRealization);

                                        if (_locAppSetDetail != null)
                                        {
                                            ApprovalLine _locApprovalLineXPO = _currentSession.FindObject<ApprovalLine>
                                                                                (new GroupOperator(GroupOperatorType.And,
                                                                                 new BinaryOperator("PaymentRealization", _locPaymentRealizationXPO),
                                                                                 new BinaryOperator("ApprovalLevel", ApprovalLevel.Level3)));

                                            if (_locApprovalLineXPO == null)
                                            {
                                                if (_locAppSetDetail.EndApproval == true)
                                                {
                                                    ApprovalLine _saveDataAL = new ApprovalLine(_currentSession)
                                                    {
                                                        ApprovalDate = now,
                                                        ApprovalStatus = Status.Approved,
                                                        ApprovalLevel = ApprovalLevel.Level3,
                                                        EndApproval = true,
                                                        PaymentRealization = _locPaymentRealizationXPO,
                                                    };
                                                    _saveDataAL.Save();
                                                    _saveDataAL.Session.CommitTransaction();

                                                    _locPaymentRealizationXPO.ActivationPosting = true;
                                                    _locPaymentRealizationXPO.ActiveApproved1 = false;
                                                    _locPaymentRealizationXPO.ActiveApproved2 = false;
                                                    _locPaymentRealizationXPO.ActiveApproved3 = true;
                                                    _locPaymentRealizationXPO.Save();
                                                    _locPaymentRealizationXPO.Session.CommitTransaction();
                                                }
                                                else
                                                {
                                                    ApprovalLine _saveDataAL = new ApprovalLine(_currentSession)
                                                    {
                                                        ApprovalDate = now,
                                                        ApprovalStatus = Status.Approved,
                                                        ApprovalLevel = ApprovalLevel.Level3,
                                                        PaymentRealization = _locPaymentRealizationXPO,
                                                    };
                                                    _saveDataAL.Save();
                                                    _saveDataAL.Session.CommitTransaction();

                                                    SetApprovalLine(_currentSession, _locPaymentRealizationXPO, ApprovalLevel.Level2);
                                                    SetApprovalLine(_currentSession, _locPaymentRealizationXPO, ApprovalLevel.Level1);

                                                    _locPaymentRealizationXPO.ActiveApproved1 = false;
                                                    _locPaymentRealizationXPO.ActiveApproved2 = false;
                                                    _locPaymentRealizationXPO.ActiveApproved3 = true;
                                                    _locPaymentRealizationXPO.Save();
                                                    _locPaymentRealizationXPO.Session.CommitTransaction();
                                                }

                                                //Send Email
                                                ApprovalLine _locApprovalLineXPO2 = _currentSession.FindObject<ApprovalLine>
                                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                                     new BinaryOperator("PaymentRealization", _locPaymentRealizationXPO)));

                                                if (_locApprovalLineXPO2 != null)
                                                {
                                                    SendEmail(_currentSession, _locPaymentRealizationXPO, Status.Progress, _locApprovalLineXPO2);
                                                }

                                                SuccessMessageShow("Payment Realization has successfully Approve");
                                            }
                                        }
                                    }
                                    #endregion Approval Level 3
                                }
                            }
                            else
                            {
                                ErrorMessageShow("Payment Realization Status Not Available");
                            }
                        }
                        else
                        {
                            ErrorMessageShow("Payment Realization Not Available");
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PaymentRealization " + ex.ToString());
            }
        }

        private void PaymentRealizationListviewFilterApprovalSelectionAction_Execute(object sender, SingleChoiceActionExecuteEventArgs e)
        {
            try
            {
                if ((View is ListView) & (View.ObjectTypeInfo.Type == typeof(PaymentRealization)))
                {
                    if ((ApprovalFilter)e.SelectedChoiceActionItem.Data == ApprovalFilter.Approval)
                    {
                        ((ListView)View).CollectionSource.Criteria["FilterApproval1"] = new GroupOperator(GroupOperatorType.And,
                                                                                        new BinaryOperator("ActiveApproved3", false, BinaryOperatorType.Equal),
                                                                                        new GroupOperator(GroupOperatorType.Or,
                                                                                        new BinaryOperator("ActiveApproved1", true, BinaryOperatorType.Equal),
                                                                                        new BinaryOperator("ActiveApproved2", true, BinaryOperatorType.Equal)));
                    }
                    else if ((ApprovalFilter)e.SelectedChoiceActionItem.Data == ApprovalFilter.EndApproval)
                    {
                        ((ListView)View).CollectionSource.Criteria["FilterApproval1"] = new BinaryOperator("ActiveApproved3", true, BinaryOperatorType.Equal);
                    }
                    else if ((ApprovalFilter)e.SelectedChoiceActionItem.Data == ApprovalFilter.AllData)
                    {
                        ((ListView)View).CollectionSource.Criteria["FilterApproval1"] = new GroupOperator(GroupOperatorType.Or,
                                                                                        new BinaryOperator("ActiveApproved1", true, BinaryOperatorType.Equal),
                                                                                        new BinaryOperator("ActiveApproved2", true, BinaryOperatorType.Equal),
                                                                                        new BinaryOperator("ActiveApproved3", true, BinaryOperatorType.Equal),
                                                                                        new BinaryOperator("ActiveApproved1", false, BinaryOperatorType.Equal),
                                                                                        new BinaryOperator("ActiveApproved2", false, BinaryOperatorType.Equal),
                                                                                        new BinaryOperator("ActiveApproved3", false, BinaryOperatorType.Equal));
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PaymentRealization " + ex.ToString());
            }
        }

        private void PaymentRealizationListviewFilterSelectionAction_Execute(object sender, SingleChoiceActionExecuteEventArgs e)
        {
            try
            {
                if ((View is ListView) & (View.ObjectTypeInfo.Type == typeof(PaymentRealization)))
                {
                    if ((Status)e.SelectedChoiceActionItem.Data == Status.None)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.None, BinaryOperatorType.NotEqual);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Open)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Open, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Progress)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Progress, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Posted)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Posted, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Lock)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Lock, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Close)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Close, BinaryOperatorType.Equal);
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PaymentRealization " + ex.ToString());
            }
        }

        //=========================================== Code In Here ===========================================

        #region GetCA

        private void GetCashAdvanceMonitoring(Session _currSession, CashAdvance _locCashAdvanceXPO, PaymentRealization _locPaymentRealizationXPO)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                DateTime now = DateTime.Now;
                string _currSignCode = null;
                DateTime _locDocDate = now;
                int _locJournalMonth = 0;
                int _locJournalYear = 0;

                if (_locCashAdvanceXPO != null && _locPaymentRealizationXPO != null)
                {
                    XPCollection<CashAdvanceMonitoring> _locCashAdvanceMonitorings = new XPCollection<CashAdvanceMonitoring>
                                                                                     (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                        new BinaryOperator("CashAdvance", _locCashAdvanceXPO),
                                                                                        new BinaryOperator("PaymentRealization", _locPaymentRealizationXPO),
                                                                                        new BinaryOperator("Select", true),
                                                                                        new GroupOperator(GroupOperatorType.Or,
                                                                                        new BinaryOperator("Status", Status.Open),
                                                                                        new BinaryOperator("Status", Status.Posted))));

                    if (_locCashAdvanceMonitorings != null && _locCashAdvanceMonitorings.Count() > 0)
                    {
                        foreach (CashAdvanceMonitoring _locCashAdvanceMonitoring in _locCashAdvanceMonitorings)
                        {

                            if (_locCashAdvanceMonitoring.CashAdvanceLine != null && _locCashAdvanceMonitoring.PaymentRealizationLine == null)
                            {
                                _locDocDate = _locCashAdvanceMonitoring.DocDate;
                                _locJournalMonth = _locCashAdvanceMonitoring.JournalMonth;
                                _locJournalYear = _locCashAdvanceMonitoring.JournalYear;

                                _currSignCode = _globFunc.GetNumberingSignUnlockOptimisticRecordBasedOrganization(_currSession.DataLayer, ObjectList.PaymentRealizationLine, _locPaymentRealizationXPO.Company, _locPaymentRealizationXPO.Workplace);
                                
                                if(_currSignCode != null)
                                {
                                    #region SavePaymentRealizationLine
                                    PaymentRealizationLine _saveDataPaymentRealizationLine = new PaymentRealizationLine(_currSession)
                                    {
                                        SignCode = _currSignCode,
                                        Company = _locPaymentRealizationXPO.Company,
                                        Workplace = _locPaymentRealizationXPO.Workplace,
                                        DivisionCA = _locCashAdvanceMonitoring.Division,
                                        DepartmentCA = _locCashAdvanceMonitoring.Department,
                                        SectionCA = _locCashAdvanceMonitoring.Section,
                                        EmployeeCA = _locCashAdvanceMonitoring.Employee,
                                        CashAdvanceType = _locCashAdvanceMonitoring.CashAdvanceType,
                                        Amount = _locCashAdvanceMonitoring.AmountApproved,
                                        AmountRealized = _locCashAdvanceMonitoring.AmountApproved,
                                        Description = _locCashAdvanceMonitoring.Description,
                                        CashAdvanceMonitoring = _locCashAdvanceMonitoring,
                                        CashAdvance = _locCashAdvanceMonitoring.CashAdvance,
                                        PaymentRealization = _locPaymentRealizationXPO,
                                        DocDate = _locCashAdvanceMonitoring.DocDate,
                                        JournalMonth = _locCashAdvanceMonitoring.JournalMonth,
                                        JournalYear = _locCashAdvanceMonitoring.JournalYear,
                                    };
                                    _saveDataPaymentRealizationLine.Save();
                                    _saveDataPaymentRealizationLine.Session.CommitTransaction();
                                    #endregion SavePurchaseOrderLine

                                    PaymentRealizationLine _locPaymentRealizationLine = _currSession.FindObject<PaymentRealizationLine>
                                                                (new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("SignCode", _currSignCode)));
                                    if (_locPaymentRealizationLine != null)
                                    {
                                        _locCashAdvanceMonitoring.Status = Status.Posted;
                                        _locCashAdvanceMonitoring.StatusDate = now;
                                        _locCashAdvanceMonitoring.Select = false;
                                        _locCashAdvanceMonitoring.PaymentRealizationLine = _locPaymentRealizationLine;
                                        _locCashAdvanceMonitoring.Save();
                                        _locCashAdvanceMonitoring.Session.CommitTransaction();
                                    }
                                }
                            }
                        }
                    }

                    _locPaymentRealizationXPO.DocDate = _locDocDate;
                    _locPaymentRealizationXPO.JournalMonth = _locJournalMonth;
                    _locPaymentRealizationXPO.JournalYear = _locJournalYear;
                    _locPaymentRealizationXPO.Save();
                    _locPaymentRealizationXPO.Session.CommitTransaction();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = PaymentRealization ", ex.ToString());
            }
        }

        #endregion GetCA

        #region PostingForPaymentRealization

        private void SetPaymentRealizationMonitoring(Session _currSession, PaymentRealization _locPaymentRealizationXPO)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                DateTime now = DateTime.Now;
                string _currSignCode = null;

                if (_locPaymentRealizationXPO != null)
                {
                    XPCollection<PaymentRealizationLine> _locPaymentRealizationLines = new XPCollection<PaymentRealizationLine>
                                                                                (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                new BinaryOperator("PaymentRealization", _locPaymentRealizationXPO),
                                                                                new BinaryOperator("Select", true),
                                                                                new BinaryOperator("PaymentStatus", PaymentStatus.Settled),
                                                                                new GroupOperator(GroupOperatorType.Or,
                                                                                new BinaryOperator("Status", Status.Progress),
                                                                                new BinaryOperator("Status", Status.Posted))));

                    if (_locPaymentRealizationLines != null && _locPaymentRealizationLines.Count() > 0)
                    {
                        foreach (PaymentRealizationLine _locPaymentRealizationLine in _locPaymentRealizationLines)
                        {
                            if (_locPaymentRealizationLine.AmountRealized > 0)
                            {
                                PaymentRealizationMonitoring _saveDataPaymentRealizationMonitoring = new PaymentRealizationMonitoring(_currSession)
                                {
                                    SignCode = _currSignCode,
                                    PaymentRealization = _locPaymentRealizationXPO,
                                    PaymentRealizationLine = _locPaymentRealizationLine,
                                    Company = _locPaymentRealizationLine.Company,
                                    Workplace = _locPaymentRealizationLine.Workplace,
                                    Division = _locPaymentRealizationLine.Division,
                                    Department = _locPaymentRealizationLine.Department,
                                    Section = _locPaymentRealizationLine.Section,
                                    Employee = _locPaymentRealizationLine.Employee,
                                    DivisionCA = _locPaymentRealizationLine.DivisionCA,
                                    DepartmentCA = _locPaymentRealizationLine.DepartmentCA,
                                    SectionCA = _locPaymentRealizationLine.SectionCA,
                                    EmployeeCA = _locPaymentRealizationLine.EmployeeCA,
                                    CashAdvanceType = _locPaymentRealizationLine.CashAdvanceType,
                                    Amount = _locPaymentRealizationLine.AmountRealized,
                                    AmountRealized = _locPaymentRealizationLine.AmountRealized,
                                    AmountRefund = _locPaymentRealizationLine.AmountRefund,
                                    AmountOutstanding = _locPaymentRealizationLine.AmountOutstanding,
                                    Description = _locPaymentRealizationLine.Description,
                                    PaymentStatus = _locPaymentRealizationLine.PaymentStatus,
                                    PaymentStatusDate = _locPaymentRealizationLine.PaymentStatusDate,
                                    CashAdvance = _locPaymentRealizationLine.CashAdvance,
                                    CashAdvanceMonitoring = _locPaymentRealizationLine.CashAdvanceMonitoring,
                                    DocDate = _locPaymentRealizationLine.DocDate,
                                    JournalMonth = _locPaymentRealizationLine.JournalMonth,
                                    JournalYear = _locPaymentRealizationLine.JournalYear,
                                };
                                _saveDataPaymentRealizationMonitoring.Save();
                                _saveDataPaymentRealizationMonitoring.Session.CommitTransaction();
                                _currSignCode = _globFunc.GetNumberingSignUnlockOptimisticRecord(_currSession.DataLayer, ObjectList.PaymentRealizationMonitoring);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = PaymentRealization ", ex.ToString());
            }
        }

        private bool CheckJournalSetup(Session _currSession, PaymentRealization _locPaymentRealizationXPO)
        {
            bool _result = false;
            try
            {
                if (_locPaymentRealizationXPO != null)
                {
                    if (_locPaymentRealizationXPO.Company != null && _locPaymentRealizationXPO.Workplace != null)
                    {
                        JournalSetupDetail _locJournalSetupDetail = _currSession.FindObject<JournalSetupDetail>
                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                    new BinaryOperator("Company", _locPaymentRealizationXPO.Company),
                                                                    new BinaryOperator("Workplace", _locPaymentRealizationXPO.Workplace),
                                                                    new BinaryOperator("JournalSetupType", JournalSetupType.Journal),
                                                                    new BinaryOperator("ObjectList", ObjectList.PaymentRealization),
                                                                    new BinaryOperator("JournalProcess", JournalProcess.JournalPaymentRealization),
                                                                    new BinaryOperator("Active", true)));

                        if (_locJournalSetupDetail != null)
                        {
                            _result = true;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = PaymentRealization ", ex.ToString());
            }

            return _result;
        }

        private void SetJournalRealization(Session _currSession, PaymentRealization _locPaymentRealizationXPO)
        {
            try
            {
                DateTime now = DateTime.Now;
                double _locAmountRealized = 0;
                AdvanceAccountGroup _locAdvanceAccountGroup = null;

                if (_locPaymentRealizationXPO != null)
                {

                    XPCollection<PaymentRealizationLine> _locPaymentRealizationLines = new XPCollection<PaymentRealizationLine>
                                                                                    (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                    new BinaryOperator("PaymentRealization", _locPaymentRealizationXPO),
                                                                                    new BinaryOperator("Select", true),
                                                                                    new BinaryOperator("PaymentStatus", PaymentStatus.Settled),
                                                                                    new GroupOperator(GroupOperatorType.Or,
                                                                                    new BinaryOperator("Status", Status.Progress),
                                                                                    new BinaryOperator("Status", Status.Posted)
                                                                                    )));
                    if (_locPaymentRealizationLines != null && _locPaymentRealizationLines.Count() > 0)
                    {
                        foreach (PaymentRealizationLine _locPaymentRealizationLine in _locPaymentRealizationLines)
                        {
                            if (_locPaymentRealizationLine.AmountRealized > 0)
                            {
                                _locAmountRealized = _locPaymentRealizationLine.AmountRealized;

                                #region JournalMapCompanyAccountGroup
                                if (_locPaymentRealizationLine.CashAdvanceType != null)
                                {
                                    if (_locPaymentRealizationLine.CashAdvanceType.AdvanceAccountGroup != null)
                                    {
                                        _locAdvanceAccountGroup = _locPaymentRealizationLine.CashAdvanceType.AdvanceAccountGroup;

                                        XPCollection<JournalMap> _locJournalMapItems = new XPCollection<JournalMap>
                                                                       (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                        new BinaryOperator("Company", _locPaymentRealizationLine.Company),
                                                                        new BinaryOperator("Workplace", _locPaymentRealizationLine.Workplace),
                                                                        new BinaryOperator("AdvanceAccountGroup", _locAdvanceAccountGroup),
                                                                        new BinaryOperator("Active", true)));

                                        if (_locJournalMapItems != null && _locJournalMapItems.Count() > 0)
                                        {
                                            foreach (JournalMap _locJournalMapItem in _locJournalMapItems)
                                            {
                                                XPCollection<JournalMapLine> _locJournalMapLines = new XPCollection<JournalMapLine>
                                                                                                   (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                    new BinaryOperator("Company", _locPaymentRealizationLine.Company),
                                                                                                    new BinaryOperator("Workplace", _locPaymentRealizationLine.Workplace),
                                                                                                    new BinaryOperator("JournalMap", _locJournalMapItem),
                                                                                                    new BinaryOperator("Active", true)));

                                                if (_locJournalMapLines != null && _locJournalMapLines.Count() > 0)
                                                {
                                                    foreach (JournalMapLine _locJournalMapLine in _locJournalMapLines)
                                                    {
                                                        if (_locJournalMapLine.AccountMap != null)
                                                        {
                                                            #region NormalAmount
                                                            if (_locJournalMapLine.AccountMap.PostingType == PostingType.Advances && _locJournalMapLine.AccountMap.OrderType == OrderType.Account
                                                                && _locJournalMapLine.AccountMap.PostingMethod == PostingMethod.CashRealization && _locJournalMapLine.AccountMap.PostingMethodType == PostingMethodType.Advances)
                                                            {
                                                                XPCollection<AccountMapLine> _locAccountMapLines = new XPCollection<AccountMapLine>
                                                                                                               (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                                new BinaryOperator("Company", _locPaymentRealizationLine.Company),
                                                                                                                new BinaryOperator("Workplace", _locPaymentRealizationLine.Workplace),
                                                                                                                new BinaryOperator("AccountMap", _locJournalMapLine.AccountMap),
                                                                                                                new BinaryOperator("Active", true)));

                                                                if (_locAccountMapLines != null && _locAccountMapLines.Count() > 0)
                                                                {
                                                                    double _locTotalAmountDebit = 0;
                                                                    double _locTotalAmountCredit = 0;
                                                                    double _locTotalBalance = 0;

                                                                    foreach (AccountMapLine _locAccountMapLine in _locAccountMapLines)
                                                                    {
                                                                        if (_locAccountMapLine.AccountCharge == AccountCharge.Debit)
                                                                        {
                                                                            _locTotalAmountDebit = _locAmountRealized;
                                                                        }
                                                                        if (_locAccountMapLine.AccountCharge == AccountCharge.Credit)
                                                                        {
                                                                            _locTotalAmountCredit = _locAmountRealized;
                                                                        }

                                                                        GeneralJournal _saveGeneralJournal = new GeneralJournal(_currSession)
                                                                        {
                                                                            Company = _locPaymentRealizationLine.Company,
                                                                            Workplace = _locPaymentRealizationLine.Workplace,
                                                                            PostingDate = now,
                                                                            PostingType = PostingType.Advances,
                                                                            OrderType = OrderType.Account,
                                                                            PostingMethod = PostingMethod.CashRealization,
                                                                            PostingMethodType = PostingMethodType.Advances,
                                                                            Account = _locAccountMapLine.Account,
                                                                            Debit = _locTotalAmountDebit,
                                                                            Credit = _locTotalAmountCredit,
                                                                            JournalMonth = _locPaymentRealizationLine.JournalMonth,
                                                                            JournalYear = _locPaymentRealizationLine.JournalYear,
                                                                            PaymentRealization = _locPaymentRealizationXPO,
                                                                        };

                                                                        _saveGeneralJournal.Save();
                                                                        _saveGeneralJournal.Session.CommitTransaction();

                                                                        #region AccountingPeriodicLine

                                                                        if (_locAccountMapLine.Account.Code != null)
                                                                        {

                                                                            //Cari Account Periodic Line
                                                                            AccountingPeriodicLine _locAPL = _currSession.FindObject<AccountingPeriodicLine>
                                                                                                            (new GroupOperator(GroupOperatorType.And,
                                                                                                            new BinaryOperator("Company", _locPaymentRealizationLine.Company),
                                                                                                            new BinaryOperator("Workplace", _locPaymentRealizationLine.Workplace),
                                                                                                            new BinaryOperator("AccountNo", _locAccountMapLine.Account),
                                                                                                            new BinaryOperator("Month", _locPaymentRealizationLine.JournalMonth),
                                                                                                            new BinaryOperator("Year", _locPaymentRealizationLine.JournalYear)));
                                                                            if (_locAPL != null)
                                                                            {
                                                                                if (_locAPL.AccountNo != null)
                                                                                {
                                                                                    if (_locAPL.AccountNo.BalanceType == BalanceType.Change)
                                                                                    {
                                                                                        if (_locAPL.AccountNo.AccountCharge == AccountCharge.Debit)
                                                                                        {
                                                                                            if (_locTotalAmountDebit > 0)
                                                                                            {
                                                                                                _locTotalBalance = _locAPL.Balance + _locTotalAmountDebit;
                                                                                            }
                                                                                            if (_locTotalAmountCredit > 0)
                                                                                            {
                                                                                                _locTotalBalance = _locAPL.Balance - _locTotalAmountCredit;
                                                                                            }
                                                                                        }
                                                                                        if (_locAPL.AccountNo.AccountCharge == AccountCharge.Credit)
                                                                                        {
                                                                                            if (_locTotalAmountDebit > 0)
                                                                                            {
                                                                                                _locTotalBalance = _locAPL.Balance - _locTotalAmountDebit;
                                                                                            }
                                                                                            if (_locTotalAmountCredit > 0)
                                                                                            {
                                                                                                _locTotalBalance = _locAPL.Balance + _locTotalAmountCredit;
                                                                                            }
                                                                                        }
                                                                                    }

                                                                                    _locAPL.Balance = _locTotalBalance;
                                                                                    _locAPL.Debit = _locAPL.Debit + _locTotalAmountDebit;
                                                                                    _locAPL.Credit = _locAPL.Credit + _locTotalAmountCredit;
                                                                                    _locAPL.Save();
                                                                                    _locAPL.Session.CommitTransaction();

                                                                                }
                                                                            }
                                                                        }
                                                                        #endregion AccountingPeriodicLine
                                                                    }
                                                                }
                                                            }
                                                            #endregion NormalAmount
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                                #endregion JournalMapCompanyAccountGroup

                                #region JournalMapByEmployee
                                if (_locPaymentRealizationXPO.Employee != null)
                                {
                                    OrganizationAccountGroup _locOrganizationAccountGroup = _currSession.FindObject<OrganizationAccountGroup>
                                                                                           (new GroupOperator(GroupOperatorType.And,
                                                                                            new BinaryOperator("Company", _locPaymentRealizationLine.Company),
                                                                                            new BinaryOperator("Workplace", _locPaymentRealizationLine.Workplace),
                                                                                            new BinaryOperator("Employee", _locPaymentRealizationXPO.Employee),
                                                                                            new BinaryOperator("Active", true)));
                                    if (_locOrganizationAccountGroup != null)
                                    {
                                        XPCollection<JournalMap> _locJournalMaps = new XPCollection<JournalMap>
                                                                               (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                new BinaryOperator("Company", _locPaymentRealizationLine.Company),
                                                                                new BinaryOperator("Workplace", _locPaymentRealizationLine.Workplace),
                                                                                new BinaryOperator("OrganizationAccountGroup", _locOrganizationAccountGroup),
                                                                                new BinaryOperator("Active", true)));

                                        if (_locJournalMaps != null && _locJournalMaps.Count() > 0)
                                        {
                                            foreach (JournalMap _locJournalMap in _locJournalMaps)
                                            {
                                                XPCollection<JournalMapLine> _locJournalMapLines = new XPCollection<JournalMapLine>
                                                                                                   (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                    new BinaryOperator("Company", _locPaymentRealizationLine.Company),
                                                                                                    new BinaryOperator("Workplace", _locPaymentRealizationLine.Workplace),
                                                                                                    new BinaryOperator("JournalMap", _locJournalMap),
                                                                                                    new BinaryOperator("Active", true)));

                                                if (_locJournalMapLines != null && _locJournalMapLines.Count() > 0)
                                                {
                                                    foreach (JournalMapLine _locJournalMapLine in _locJournalMapLines)
                                                    {
                                                        if (_locJournalMapLine.AccountMap != null)
                                                        {
                                                            if (_locJournalMapLine.AccountMap.PostingType == PostingType.Advances && _locJournalMapLine.AccountMap.OrderType == OrderType.Account
                                                            && _locJournalMapLine.AccountMap.PostingMethod == PostingMethod.CashRealization && _locJournalMapLine.AccountMap.PostingMethodType == PostingMethodType.Advances)
                                                            {
                                                                XPCollection<AccountMapLine> _locAccountMapLines = new XPCollection<AccountMapLine>
                                                                                                                   (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                                    new BinaryOperator("Company", _locPaymentRealizationLine.Company),
                                                                                                                    new BinaryOperator("Workplace", _locPaymentRealizationLine.Workplace),
                                                                                                                    new BinaryOperator("AccountMap", _locJournalMapLine.AccountMap),
                                                                                                                    new BinaryOperator("Active", true)));

                                                                if (_locAccountMapLines != null && _locAccountMapLines.Count() > 0)
                                                                {
                                                                    double _locTotalAmountDebit = 0;
                                                                    double _locTotalAmountCredit = 0;
                                                                    double _locTotalBalance = 0;

                                                                    foreach (AccountMapLine _locAccountMapLine in _locAccountMapLines)
                                                                    {
                                                                        if (_locAccountMapLine.AccountCharge == AccountCharge.Debit)
                                                                        {
                                                                            _locTotalAmountDebit = _locAmountRealized;
                                                                        }
                                                                        if (_locAccountMapLine.AccountCharge == AccountCharge.Credit)
                                                                        {
                                                                            _locTotalAmountCredit = _locAmountRealized;
                                                                        }

                                                                        GeneralJournal _saveGeneralJournal = new GeneralJournal(_currSession)
                                                                        {
                                                                            Company = _locPaymentRealizationLine.Company,
                                                                            Workplace = _locPaymentRealizationLine.Workplace,
                                                                            PostingDate = now,
                                                                            PostingType = PostingType.Advances,
                                                                            OrderType = OrderType.Account,
                                                                            PostingMethod = PostingMethod.CashRealization,
                                                                            PostingMethodType = PostingMethodType.Advances,
                                                                            Account = _locAccountMapLine.Account,
                                                                            Debit = _locTotalAmountDebit,
                                                                            Credit = _locTotalAmountCredit,
                                                                            JournalMonth = _locPaymentRealizationLine.JournalMonth,
                                                                            JournalYear = _locPaymentRealizationLine.JournalYear,
                                                                            PaymentRealization = _locPaymentRealizationXPO,
                                                                        };

                                                                        _saveGeneralJournal.Save();
                                                                        _saveGeneralJournal.Session.CommitTransaction();

                                                                        #region AccountingPeriodicLine

                                                                        if (_locAccountMapLine.Account.Code != null)
                                                                        {

                                                                            //Cari Account Periodic Line
                                                                            AccountingPeriodicLine _locAPL = _currSession.FindObject<AccountingPeriodicLine>
                                                                                                            (new GroupOperator(GroupOperatorType.And,
                                                                                                            new BinaryOperator("Company", _locPaymentRealizationLine.Company),
                                                                                                            new BinaryOperator("Workplace", _locPaymentRealizationLine.Workplace),
                                                                                                            new BinaryOperator("AccountNo", _locAccountMapLine.Account),
                                                                                                            new BinaryOperator("Month", _locPaymentRealizationLine.JournalMonth),
                                                                                                            new BinaryOperator("Year", _locPaymentRealizationLine.JournalYear)));
                                                                            if (_locAPL != null)
                                                                            {
                                                                                if (_locAPL.AccountNo != null)
                                                                                {
                                                                                    if (_locAPL.AccountNo.BalanceType == BalanceType.Change)
                                                                                    {
                                                                                        if (_locAPL.AccountNo.AccountCharge == AccountCharge.Debit)
                                                                                        {
                                                                                            if (_locTotalAmountDebit > 0)
                                                                                            {
                                                                                                _locTotalBalance = _locAPL.Balance + _locTotalAmountDebit;
                                                                                            }
                                                                                            if (_locTotalAmountCredit > 0)
                                                                                            {
                                                                                                _locTotalBalance = _locAPL.Balance - _locTotalAmountCredit;
                                                                                            }
                                                                                        }
                                                                                        if (_locAPL.AccountNo.AccountCharge == AccountCharge.Credit)
                                                                                        {
                                                                                            if (_locTotalAmountDebit > 0)
                                                                                            {
                                                                                                _locTotalBalance = _locAPL.Balance - _locTotalAmountDebit;
                                                                                            }
                                                                                            if (_locTotalAmountCredit > 0)
                                                                                            {
                                                                                                _locTotalBalance = _locAPL.Balance + _locTotalAmountCredit;
                                                                                            }
                                                                                        }
                                                                                    }

                                                                                    _locAPL.Balance = _locTotalBalance;
                                                                                    _locAPL.Debit = _locAPL.Debit + _locTotalAmountDebit;
                                                                                    _locAPL.Credit = _locAPL.Credit + _locTotalAmountCredit;
                                                                                    _locAPL.Save();
                                                                                    _locAPL.Session.CommitTransaction();

                                                                                }
                                                                            }
                                                                        }
                                                                        #endregion AccountingPeriodicLine
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                                #endregion JournalMapByEmployee

                                _locAmountRealized = 0;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = PaymentRealization ", ex.ToString());
            }
        }

        private void SetStatusPaymentRealizationLine(Session _currSession, PaymentRealization _locPaymentRealizationXPO)
        {
            try
            {
                DateTime now = DateTime.Now;
                if (_locPaymentRealizationXPO != null)
                {
                    XPCollection<PaymentRealizationLine> _locPaymentRealizationLines = new XPCollection<PaymentRealizationLine>(_currSession,
                                                                         new GroupOperator(GroupOperatorType.And,
                                                                         new BinaryOperator("PaymentRealization", _locPaymentRealizationXPO),
                                                                         new BinaryOperator("Select", true),
                                                                         new BinaryOperator("PaymentStatus", PaymentStatus.Settled),
                                                                         new GroupOperator(GroupOperatorType.Or,
                                                                         new BinaryOperator("Status", Status.Progress),
                                                                         new BinaryOperator("Status", Status.Posted))));

                    if (_locPaymentRealizationLines != null && _locPaymentRealizationLines.Count > 0)
                    {
                        foreach (PaymentRealizationLine _locPaymentRealizationLine in _locPaymentRealizationLines)
                        {
                            if(_locPaymentRealizationLine.PaymentStatus == PaymentStatus.Settled)
                            {
                                _locPaymentRealizationLine.Status = Status.Close;
                                _locPaymentRealizationLine.ActivationPosting = true;
                                _locPaymentRealizationLine.Select = false;
                                _locPaymentRealizationLine.StatusDate = now;
                                _locPaymentRealizationLine.Save();
                                _locPaymentRealizationLine.Session.CommitTransaction();
                            }  
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PaymentRealization " + ex.ToString());
            }
        }

        private void SetStatusCashAdvanceMonitoring(Session _currSession, PaymentRealization _locPaymentRealizationXPO)
        {
            try
            {
                DateTime now = DateTime.Now;
                if (_locPaymentRealizationXPO != null)
                {
                    XPCollection<PaymentRealizationLine> _locPaymentRealizationLines = new XPCollection<PaymentRealizationLine>(_currSession,
                                                                         new GroupOperator(GroupOperatorType.And,
                                                                         new BinaryOperator("PaymentRealization", _locPaymentRealizationXPO),
                                                                         new BinaryOperator("Select", true),
                                                                         new BinaryOperator("PaymentStatus", PaymentStatus.Settled),
                                                                         new BinaryOperator("Status", Status.Close)
                                                                         ));

                    if (_locPaymentRealizationLines != null && _locPaymentRealizationLines.Count > 0)
                    {
                        foreach (PaymentRealizationLine _locPaymentRealizationLine in _locPaymentRealizationLines)
                        {
                            if (_locPaymentRealizationLine.CashAdvanceMonitoring != null )
                            {
                                _locPaymentRealizationLine.CashAdvanceMonitoring.Status = Status.Close;
                                _locPaymentRealizationLine.CashAdvanceMonitoring.StatusDate = now;
                                _locPaymentRealizationLine.CashAdvanceMonitoring.Save();
                                _locPaymentRealizationLine.CashAdvanceMonitoring.Session.CommitTransaction();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PaymentRealization " + ex.ToString());
            }
        }

        private void SetStatusPaymentRealization(Session _currSession, PaymentRealization _locPaymentRealizationXPO)
        {
            try
            {
                DateTime now = DateTime.Now;
                int _locStatusCount = 0;
                int _locPaymentRealizationLineCount = 0;

                if (_locPaymentRealizationXPO != null)
                {
                    XPCollection<PaymentRealizationLine> _locPaymentRealizationLines = new XPCollection<PaymentRealizationLine>(_currSession,
                                                                         new GroupOperator(GroupOperatorType.And,
                                                                         new BinaryOperator("PaymentRealization", _locPaymentRealizationXPO)));

                    if (_locPaymentRealizationLines != null && _locPaymentRealizationLines.Count() > 0)
                    {
                        _locPaymentRealizationLineCount = _locPaymentRealizationLines.Count();

                        foreach (PaymentRealizationLine _locPaymentRealizationLine in _locPaymentRealizationLines)
                        {
                            if (_locPaymentRealizationLine.Status == Status.Close)
                            {
                                _locStatusCount = _locStatusCount + 1;
                            }
                        }

                        if (_locPaymentRealizationLineCount == _locStatusCount)
                        {
                            _locPaymentRealizationXPO.ActivationPosting = true;
                            _locPaymentRealizationXPO.Status = Status.Close;
                            _locPaymentRealizationXPO.StatusDate = now;
                            _locPaymentRealizationXPO.Save();
                            _locPaymentRealizationXPO.Session.CommitTransaction();
                        }
                        else
                        {
                            _locPaymentRealizationXPO.ActivationPosting = true;
                            _locPaymentRealizationXPO.Status = Status.Posted;
                            _locPaymentRealizationXPO.StatusDate = now;
                            _locPaymentRealizationXPO.Save();
                            _locPaymentRealizationXPO.Session.CommitTransaction();
                        }

                        SuccessMessageShow(_locPaymentRealizationXPO.Code + " has been change successfully to Close");
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PaymentRealization " + ex.ToString());
            }
        }

        #endregion PostingForPaymentRealization

        #region Approval Process

        private void SetApprovalLine(Session _currentSession, PaymentRealization _locPaymentRealizationXPO, ApprovalLevel _locApprovalLevel)
        {
            try
            {
                DateTime now = DateTime.Now;

                ApprovalLine _locApprovalLineXPO = _currentSession.FindObject<ApprovalLine>
                                                                        (new GroupOperator(GroupOperatorType.And,
                                                                         new BinaryOperator("PaymentRealization", _locPaymentRealizationXPO),
                                                                         new BinaryOperator("ApprovalLevel", _locApprovalLevel)));

                if (_locApprovalLineXPO == null)
                {
                    ApprovalLine _saveDataAL2 = new ApprovalLine(_currentSession)
                    {
                        ApprovalDate = now,
                        ApprovalStatus = Status.Approved,
                        ApprovalLevel = _locApprovalLevel,
                        PaymentRealization = _locPaymentRealizationXPO,
                    };
                    _saveDataAL2.Save();
                    _saveDataAL2.Session.CommitTransaction();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PaymentRealization " + ex.ToString());
            }
        }

        #endregion Approval Process

        #region Email

        private string BackgroundBody(Session _currentSession, PaymentRealization _locPaymentRealizationXPO, MailSetupDetail _locMailSetDetail)
        {
            string body = string.Empty;

            if (_locPaymentRealizationXPO != null)
            {
                ApprovalLine _locApprovalLineXPO = _currentSession.FindObject<ApprovalLine>
                                               (new GroupOperator(GroupOperatorType.And,
                                                new BinaryOperator("PaymentRealization", _locPaymentRealizationXPO)));

                string Status = _locApprovalLineXPO.ApprovalStatus.ToString();

                using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/Email/EmailTemplate.html")))
                {
                    body = reader.ReadToEnd();
                }
                body = body.Replace("{Code}", _locPaymentRealizationXPO.Code);

                #region Level
                if (_locPaymentRealizationXPO.ActiveApproved1 == true)
                {
                    string Active = "Level 1";
                    body = body.Replace("{Level}", Active);
                }
                else if (_locPaymentRealizationXPO.ActiveApproved2 == true)
                {
                    string Active = "Level 2";
                    body = body.Replace("{Level}", Active);
                }
                else if (_locPaymentRealizationXPO.ActiveApproved3 == true)
                {
                    string Active = "Level 3";
                    body = body.Replace("{Level}", Active);
                }
                else
                {
                    string Active = "Not Available";
                    body = body.Replace("{Level}", Active);
                }
                #endregion Level

                body = body.Replace("{Status}", Status);
                body = body.Replace("{Message}", _locMailSetDetail.MessageBody);
            }
            return body;
        }

        protected void SendEmail(Session _currentSession, PaymentRealization _locPaymentRealizationXPO, Status _locStatus, ApprovalLine _locApprovalLineXPO2)
        {
            DateTime now = DateTime.Now;
            var User = SecuritySystem.CurrentUserName;
            GlobalFunction _globFunc = new GlobalFunction();
            UserAccess _numLineUserAccess = _currentSession.FindObject<UserAccess>(new BinaryOperator("UserName", User));
            string _locMessageBody = null;

            try
            {
                if (_locPaymentRealizationXPO != null)
                {
                    XPCollection<MailSetupDetail> _locMailSetDetails = new XPCollection<MailSetupDetail>
                                                                       (_currentSession, new GroupOperator(GroupOperatorType.And,
                                                                        new BinaryOperator("ObjectList", ObjectList.PaymentRealization),
                                                                        new BinaryOperator("UserAccess", _numLineUserAccess),
                                                                        new BinaryOperator("Active", true)));

                    if (_locMailSetDetails != null && _locMailSetDetails.Count > 0)
                    {
                        foreach (MailSetupDetail _locMailSetDetail in _locMailSetDetails)
                        {
                            if (_locMailSetDetail.MessageBody != null)
                            {
                                if (_locApprovalLineXPO2.EndApproval == true)
                                {
                                    _locMessageBody = this.BackgroundBody(_currentSession, _locPaymentRealizationXPO, _locMailSetDetail);
                                }
                                else
                                {
                                    _locMessageBody = this.BackgroundBody(_currentSession, _locPaymentRealizationXPO, _locMailSetDetail);
                                }
                            }

                            _globFunc.SetAndSendMail(_locMailSetDetail.SmtpHost, _locMailSetDetail.SmtpPort,
                            _locMailSetDetail.MailFrom, _locMailSetDetail.MailFromPassword, _locMailSetDetail.MailTo, _locMailSetDetail.MailSubject, _locMessageBody);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PaymentRealization " + ex.ToString());
            }
        }

        #endregion Email

        #region GlobalMethod

        private void SuccessMessageShow(string _locActionName)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 2000;
            options.Message = string.Format("{0} !", _locActionName);
            options.Type = InformationType.Success;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Success";
            options.Win.Type = WinMessageType.Flyout;
            Application.ShowViewStrategy.ShowMessage(options);
        }

        private void ErrorMessageShow(string _locMessage)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 2000;
            options.Message = string.Format(_locMessage);
            options.Type = InformationType.Warning;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Error";
            options.Win.Type = WinMessageType.Alert;
            Application.ShowViewStrategy.ShowMessage(options);
        }

        #endregion GlobalMethod

        
    }
}
