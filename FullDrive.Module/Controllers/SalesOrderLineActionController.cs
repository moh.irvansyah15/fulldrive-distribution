﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;

using FullDrive.Module.BusinessObjects;
using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.Xpo;
using System.Collections;
using DevExpress.Xpo;

namespace FullDrive.Module.Controllers
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class SalesOrderLineActionController : ViewController
    {
        public SalesOrderLineActionController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.
        }
        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }
        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        private void SalesOrderLineSelectAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;

                if (_objectToProcess != null)
                {
                    foreach (Object obj in _objectToProcess)
                    {
                        SalesOrderLine _locSalesOrderLineOS = (SalesOrderLine)_objectSpace.GetObject(obj);

                        if (_locSalesOrderLineOS != null)
                        {
                            if (_locSalesOrderLineOS.Session != null)
                            {
                                _currSession = _locSalesOrderLineOS.Session;
                            }

                            if (_locSalesOrderLineOS.Code != null && _currSession != null)
                            {
                                _currObjectId = _locSalesOrderLineOS.Code;

                                XPCollection<SalesOrderLine> _locSalesOrderLines = new XPCollection<SalesOrderLine>
                                                                                                     (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                      new BinaryOperator("Code", _currObjectId),
                                                                                                      new GroupOperator(GroupOperatorType.Or,
                                                                                                      new BinaryOperator("Status", Status.Progress),
                                                                                                      new BinaryOperator("Status", Status.Open)
                                                                                                      )));

                                if (_locSalesOrderLines != null && _locSalesOrderLines.Count > 0)
                                {
                                    foreach (SalesOrderLine _locSalesOrderLine in _locSalesOrderLines)
                                    {
                                        _locSalesOrderLine.Select = true;
                                        _locSalesOrderLine.Save();
                                        _locSalesOrderLine.Session.CommitTransaction();
                                    }

                                    SuccessMessageShow("SalesOrderLine has been Select");
                                }
                                else
                                {
                                    ErrorMessageShow("Data SalesOrderLine Not Available");
                                }
                            }
                        }
                    }
                    if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                    {
                        _objectSpace.CommitChanges();
                        _objectSpace.Refresh();
                    }
                    if (View is ListView)
                    {
                        _objectSpace.CommitChanges();
                        View.ObjectSpace.Refresh();
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("BusinessObject = SalesOrderLine" + ex.ToString());
            }
        }

        private void SalesOrderLineUnselectAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;

                if (_objectToProcess != null)
                {
                    foreach (Object obj in _objectToProcess)
                    {
                        SalesOrderLine _locSalesOrderLineOS = (SalesOrderLine)_objectSpace.GetObject(obj);

                        if (_locSalesOrderLineOS != null)
                        {
                            if (_locSalesOrderLineOS.Session != null)
                            {
                                _currSession = _locSalesOrderLineOS.Session;
                            }

                            if (_locSalesOrderLineOS.Code != null && _currSession != null)
                            {
                                _currObjectId = _locSalesOrderLineOS.Code;

                                XPCollection<SalesOrderLine> _locSalesOrderLines = new XPCollection<SalesOrderLine>
                                                                                                     (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                      new BinaryOperator("Code", _currObjectId),
                                                                                                      new GroupOperator(GroupOperatorType.Or,
                                                                                                      new BinaryOperator("Status", Status.Progress),
                                                                                                      new BinaryOperator("Status", Status.Open)
                                                                                                      )));

                                if (_locSalesOrderLines != null && _locSalesOrderLines.Count > 0)
                                {
                                    foreach (SalesOrderLine _locSalesOrderLine in _locSalesOrderLines)
                                    {
                                        _locSalesOrderLine.Select = false;
                                        _locSalesOrderLine.Save();
                                        _locSalesOrderLine.Session.CommitTransaction();
                                    }

                                    SuccessMessageShow("SalesOrderLine has been Unselect");
                                }
                                else
                                {
                                    ErrorMessageShow("Data SalesOrderLine Not Available");
                                }
                            }
                        }
                    }
                    if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                    {
                        _objectSpace.CommitChanges();
                        _objectSpace.Refresh();
                    }
                    if (View is ListView)
                    {
                        _objectSpace.CommitChanges();
                        View.ObjectSpace.Refresh();
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("BusinessObject = SalesOrderLine" + ex.ToString());
            }
        }

        private void SalesOrderLineTaxAndDiscountAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;

                if (_objectToProcess != null)
                {
                    foreach (Object obj in _objectToProcess)
                    {
                        SalesOrderLine _locSalesOrderLineOS = (SalesOrderLine)_objectSpace.GetObject(obj);

                        if (_locSalesOrderLineOS != null)
                        {
                            if (_locSalesOrderLineOS.Session != null)
                            {
                                _currSession = _locSalesOrderLineOS.Session;
                            }

                            if (_locSalesOrderLineOS.Code != null && _currSession != null)
                            {
                                _currObjectId = _locSalesOrderLineOS.Code;

                                XPCollection<SalesOrderLine> _locSalesOrderLines = new XPCollection<SalesOrderLine>
                                                                                                     (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                      new BinaryOperator("Code", _currObjectId),
                                                                                                      new GroupOperator(GroupOperatorType.Or,
                                                                                                      new BinaryOperator("Status", Status.Progress),
                                                                                                      new BinaryOperator("Status", Status.Open)
                                                                                                      )));

                                if (_locSalesOrderLines != null && _locSalesOrderLines.Count > 0)
                                {
                                    foreach (SalesOrderLine _locSalesOrderLine in _locSalesOrderLines)
                                    {
                                        SetTaxBeforeDiscount(_currSession, _locSalesOrderLine);
                                        SetDiscountGross(_currSession, _locSalesOrderLine);
                                        SetTaxAfterDiscount(_currSession, _locSalesOrderLine);
                                        SetDiscountNet(_currSession, _locSalesOrderLine);
                                    }

                                    SuccessMessageShow("SalesOrderLine Line has been Processed");
                                }
                                else
                                {
                                    ErrorMessageShow("Data SalesOrderLine Line Not Available");
                                }
                            }
                        }
                    }
                    if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                    {
                        _objectSpace.CommitChanges();
                        _objectSpace.Refresh();
                    }
                    if (View is ListView)
                    {
                        _objectSpace.CommitChanges();
                        View.ObjectSpace.Refresh();
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("BusinessObject = SalesOrderLine " + ex.ToString());
            }
        }

        //======================================== Code In Here ========================================

        #region Tax

        private void SetTaxBeforeDiscount(Session _currSession, SalesOrderLine _locSalesOrderLineXPO)
        {
            try
            {
                double _locTxAmount = 0;
                double _locTAmount = 0;
                if (_locSalesOrderLineXPO != null)
                {
                    if (_locSalesOrderLineXPO.Tax != null)
                    {
                        if (_locSalesOrderLineXPO.Tax.TaxRule == TaxRule.BeforeDiscount)
                        {
                            _locTxAmount = _locSalesOrderLineXPO.TxValue / 100 * _locSalesOrderLineXPO.TUAmount;
                            if (_locSalesOrderLineXPO.Tax.TaxNature == TaxNature.Increase)
                            {
                                _locTAmount = _locSalesOrderLineXPO.TAmount + _locTxAmount;
                            }
                            if (_locSalesOrderLineXPO.Tax.TaxNature == TaxNature.Decrease)
                            {
                                _locTAmount = _locSalesOrderLineXPO.TAmount - _locTxAmount;
                            }
                            _locSalesOrderLineXPO.TxAmount = _locTxAmount;
                            _locSalesOrderLineXPO.TAmount = _locTAmount;
                            _locSalesOrderLineXPO.Save();
                            _locSalesOrderLineXPO.Session.CommitTransaction();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("BusinessObject = SalesOrderLine" + ex.ToString());
            }
        }

        private void SetDiscountGross(Session _currSession, SalesOrderLine _locSalesOrderLineXPO)
        {
            try
            {
                if (_locSalesOrderLineXPO != null )
                {
                    if (_locSalesOrderLineXPO.DiscountRule != null )
                    {
                        double _locTUAmountDiscount = 0;
                        double _locTUAmountDiscount2 = 0;
                        AccountingPeriodicLine _locAcctPerLine = null;

                        #region Gross

                        if (_locSalesOrderLineXPO.DiscountRule.GrossAmountRule == true)
                        {
                            if (_locSalesOrderLineXPO.TUAmount > 0 && _locSalesOrderLineXPO.TUAmount >= _locSalesOrderLineXPO.DiscountRule.GrossTotalUAmount)
                            {
                                if (_locSalesOrderLineXPO.DiscountRule.DiscountType == DiscountType.Percentage)
                                {
                                    _locTUAmountDiscount = _locSalesOrderLineXPO.TUAmount * _locSalesOrderLineXPO.DiscountRule.Value / 100;
                                }
                                if (_locSalesOrderLineXPO.DiscountRule.DiscountType == DiscountType.Value)
                                {
                                    _locTUAmountDiscount = _locSalesOrderLineXPO.DiscountRule.Value;
                                }

                                #region ValBasedAccount
                                if (_locSalesOrderLineXPO.DiscountRule.ValBasedAccount == true)
                                {
                                    if(_locSalesOrderLineXPO.DiscountRule.AccountNo != null)
                                    {
                                        AccountingPeriodic _locAccountPeriodic = _currSession.FindObject<AccountingPeriodic>
                                                                        (new GroupOperator(GroupOperatorType.And,
                                                                        new BinaryOperator("Company", _locSalesOrderLineXPO.Company),
                                                                        new BinaryOperator("Workplace", _locSalesOrderLineXPO.Workplace),
                                                                        new BinaryOperator("Active", true)));
                                        if (_locAccountPeriodic != null)
                                        {
                                            AccountingPeriodicLine _locAccountingPeriodicLine = _currSession.FindObject<AccountingPeriodicLine>
                                                                                                (new GroupOperator(GroupOperatorType.And,
                                                                                                new BinaryOperator("AccountingPeriodic", _locAccountPeriodic),
                                                                                                new BinaryOperator("AccountNo", _locSalesOrderLineXPO.DiscountRule.AccountNo)));
                                            if (_locAccountingPeriodicLine != null)
                                            {
                                                _locAcctPerLine = _locAccountingPeriodicLine;
                                                if (_locAccountingPeriodicLine.BalHold < _locAccountingPeriodicLine.Balance)
                                                {
                                                    _locTUAmountDiscount2 = _locAccountingPeriodicLine.Balance - _locAccountingPeriodicLine.BalHold;

                                                    if(_locTUAmountDiscount2 < _locTUAmountDiscount)
                                                    {
                                                        _locTUAmountDiscount = _locTUAmountDiscount2;
                                                    }
                                                    
                                                }else
                                                {
                                                    _locTUAmountDiscount = 0;
                                                }
                                                _locAccountingPeriodicLine.BalHold = _locAccountingPeriodicLine.BalHold + _locTUAmountDiscount;
                                                _locAccountingPeriodicLine.Save();
                                                _locAccountingPeriodicLine.Session.CommitTransaction();
                                            }
                                        }
                                    }
                                    
                                }
                                #endregion ValBasedAccount

                                _locSalesOrderLineXPO.AccountingPeriodicLine = _locAcctPerLine;
                                _locSalesOrderLineXPO.DiscountChecked = true;
                                _locSalesOrderLineXPO.DiscAmount = _locTUAmountDiscount;
                                _locSalesOrderLineXPO.TAmount = _locSalesOrderLineXPO.TAmount - _locTUAmountDiscount;
                                _locSalesOrderLineXPO.Save();
                                _locSalesOrderLineXPO.Session.CommitTransaction();
                            }

                        }

                        #endregion Gross
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("BusinessObject = SalesOrderLine" + ex.ToString());
            }
        }

        private void SetTaxAfterDiscount(Session _currSession, SalesOrderLine _locSalesOrderLineXPO)
        {
            try
            {
                double _locTxAmount = 0;
                double _locTAmount = 0;
                if (_locSalesOrderLineXPO != null)
                {
                    if (_locSalesOrderLineXPO.Tax != null )
                    {
                        if(_locSalesOrderLineXPO.DiscountRule != null)
                        {
                            if (_locSalesOrderLineXPO.Tax.TaxRule == TaxRule.AfterDiscount && _locSalesOrderLineXPO.DiscountRule.GrossAmountRule == true)
                            {
                                _locTxAmount = _locSalesOrderLineXPO.TxValue / 100 * (_locSalesOrderLineXPO.TUAmount - _locSalesOrderLineXPO.DiscAmount);

                                if (_locSalesOrderLineXPO.Tax.TaxNature == TaxNature.Increase)
                                {
                                    _locTAmount = _locSalesOrderLineXPO.TAmount + _locTxAmount;
                                }
                                if (_locSalesOrderLineXPO.Tax.TaxNature == TaxNature.Decrease)
                                {
                                    _locTAmount = _locSalesOrderLineXPO.TAmount - _locTxAmount;
                                }
                                _locSalesOrderLineXPO.TxAmount = _locTxAmount;
                                _locSalesOrderLineXPO.TAmount = _locTAmount;
                                _locSalesOrderLineXPO.Save();
                                _locSalesOrderLineXPO.Session.CommitTransaction();
                            }
                        }else
                        {
                            if (_locSalesOrderLineXPO.Tax.TaxRule == TaxRule.AfterDiscount )
                            {
                                _locTxAmount = _locSalesOrderLineXPO.TxValue / 100 * (_locSalesOrderLineXPO.TUAmount);

                                if (_locSalesOrderLineXPO.Tax.TaxNature == TaxNature.Increase)
                                {
                                    _locTAmount = _locSalesOrderLineXPO.TAmount + _locTxAmount;
                                }
                                if (_locSalesOrderLineXPO.Tax.TaxNature == TaxNature.Decrease)
                                {
                                    _locTAmount = _locSalesOrderLineXPO.TAmount - _locTxAmount;
                                }
                                _locSalesOrderLineXPO.TxAmount = _locTxAmount;
                                _locSalesOrderLineXPO.TAmount = _locTAmount;
                                _locSalesOrderLineXPO.Save();
                                _locSalesOrderLineXPO.Session.CommitTransaction();
                            }
                        }
                        
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("BusinessObject = SalesOrderLine" + ex.ToString());
            }
        }

        private void SetDiscountNet(Session _currSession, SalesOrderLine _locSalesOrderLineXPO)
        {
            try
            {
                if (_locSalesOrderLineXPO != null)
                {
                    if (_locSalesOrderLineXPO.DiscountRule != null && _locSalesOrderLineXPO.Tax != null)
                    {
                        double _locTUAmountDiscount = 0;
                        double _locTUAmountDiscount2 = 0;
                        AccountingPeriodicLine _locAcctPerLine = null;

                        #region Net
                        if (_locSalesOrderLineXPO.DiscountRule.NetAmountRule == true && _locSalesOrderLineXPO.Tax.TaxRule == TaxRule.BeforeDiscount)
                        {
                            if (_locSalesOrderLineXPO.TAmount > 0 && _locSalesOrderLineXPO.TAmount >= _locSalesOrderLineXPO.DiscountRule.NetTotalUAmount)
                            {
                                if (_locSalesOrderLineXPO.DiscountRule.DiscountType == DiscountType.Percentage)
                                {
                                    _locTUAmountDiscount = _locSalesOrderLineXPO.TAmount * _locSalesOrderLineXPO.DiscountRule.Value / 100;
                                }
                                if (_locSalesOrderLineXPO.DiscountRule.DiscountType == DiscountType.Value)
                                {
                                    _locTUAmountDiscount = _locSalesOrderLineXPO.DiscountRule.Value;
                                }
                                #region ValBasedAccount
                                if (_locSalesOrderLineXPO.DiscountRule.ValBasedAccount == true)
                                {
                                    if (_locSalesOrderLineXPO.DiscountRule.AccountNo != null)
                                    {
                                        AccountingPeriodic _locAccountPeriodic = _currSession.FindObject<AccountingPeriodic>
                                                                        (new GroupOperator(GroupOperatorType.And,
                                                                        new BinaryOperator("Company", _locSalesOrderLineXPO.Company),
                                                                        new BinaryOperator("Workplace", _locSalesOrderLineXPO.Workplace),
                                                                        new BinaryOperator("Active", true)));
                                        if (_locAccountPeriodic != null)
                                        {
                                            AccountingPeriodicLine _locAccountingPeriodicLine = _currSession.FindObject<AccountingPeriodicLine>
                                                                                                (new GroupOperator(GroupOperatorType.And,
                                                                                                new BinaryOperator("AccountingPeriodic", _locAccountPeriodic),
                                                                                                new BinaryOperator("AccountNo", _locSalesOrderLineXPO.DiscountRule.AccountNo)));
                                            if (_locAccountingPeriodicLine != null)
                                            {
                                                _locAcctPerLine = _locAccountingPeriodicLine;
                                                if (_locAccountingPeriodicLine.BalHold < _locAccountingPeriodicLine.Balance)
                                                {
                                                    _locTUAmountDiscount2 = _locAccountingPeriodicLine.Balance - _locAccountingPeriodicLine.BalHold;

                                                    if (_locTUAmountDiscount2 < _locTUAmountDiscount)
                                                    {
                                                        _locTUAmountDiscount = _locTUAmountDiscount2;
                                                    }

                                                }
                                                else
                                                {
                                                    _locTUAmountDiscount = 0;
                                                }

                                                _locAccountingPeriodicLine.BalHold = _locAccountingPeriodicLine.BalHold + _locTUAmountDiscount;
                                                _locAccountingPeriodicLine.Save();
                                                _locAccountingPeriodicLine.Session.CommitTransaction();
                                            }
                                        }
                                    }
                                }
                                #endregion ValBasedAccount

                                _locSalesOrderLineXPO.AccountingPeriodicLine = _locAcctPerLine;
                                _locSalesOrderLineXPO.DiscountChecked = true;
                                _locSalesOrderLineXPO.DiscAmount = _locTUAmountDiscount;
                                _locSalesOrderLineXPO.TAmount = _locSalesOrderLineXPO.TAmount - _locTUAmountDiscount;
                                _locSalesOrderLineXPO.Save();
                                _locSalesOrderLineXPO.Session.CommitTransaction();
                            }

                        }
                        #endregion Net
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("BusinessObject = SalesOrderLine" + ex.ToString());
            }
        }

        #endregion Tax

        #region Global Method

        private void SuccessMessageShow(string _locActionName)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 2000;
            options.Message = string.Format("{0}", _locActionName);
            options.Type = InformationType.Success;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Success";
            options.Win.Type = WinMessageType.Flyout;
            Application.ShowViewStrategy.ShowMessage(options);
        }

        private void ErrorMessageShow(string _locMessage)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 2000;
            options.Message = string.Format(_locMessage);
            options.Type = InformationType.Warning;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Error";
            options.Win.Type = WinMessageType.Alert;
            Application.ShowViewStrategy.ShowMessage(options);
        }


        #endregion Global Method

        
    }
}
