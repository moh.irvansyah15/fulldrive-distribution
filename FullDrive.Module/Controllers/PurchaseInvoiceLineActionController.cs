﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;

using FullDrive.Module.BusinessObjects;
using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.Xpo;
using System.Collections;
using DevExpress.Xpo;

namespace FullDrive.Module.Controllers
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class PurchaseInvoiceLineActionController : ViewController
    {
        private ChoiceActionItem _selectionListviewFilter;

        public PurchaseInvoiceLineActionController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.
            #region Filter Status
            PurchaseInvoiceLineListviewFilterSelectionAction.Items.Clear();
            foreach (object _currApproval in Enum.GetValues(typeof(CustomProcess.Status)))
            {
                EnumDescriptor _ed = new EnumDescriptor(typeof(CustomProcess.Status));
                if (_ed.GetCaption(_currApproval) != "Approved" && _ed.GetCaption(_currApproval) != "Lock")
                {
                    _selectionListviewFilter = new ChoiceActionItem(_ed.GetCaption(_currApproval), _currApproval);
                }
                PurchaseInvoiceLineListviewFilterSelectionAction.Items.Add(_selectionListviewFilter);
            }
            #endregion Filter Status
        }

        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
        }

        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }

        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        private void PurchaseInvoiceLineTaxAndTotalAmountAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;

                if (_objectToProcess != null)
                {
                    foreach (Object obj in _objectToProcess)
                    {
                        PurchaseInvoiceLine _locPurchaseInvoiceLineOS = (PurchaseInvoiceLine)_objectSpace.GetObject(obj);

                        if (_locPurchaseInvoiceLineOS != null)
                        {
                            if (_locPurchaseInvoiceLineOS.Session != null)
                            {
                                _currSession = _locPurchaseInvoiceLineOS.Session;
                            }

                            if (_locPurchaseInvoiceLineOS.Code != null && _currSession != null)
                            {
                                _currObjectId = _locPurchaseInvoiceLineOS.Code;

                                XPCollection<PurchaseInvoiceLine> _locPurchaseInvoiceLines = new XPCollection<PurchaseInvoiceLine>
                                                                                                     (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                      new BinaryOperator("Code", _currObjectId),
                                                                                                      new GroupOperator(GroupOperatorType.Or,
                                                                                                      new BinaryOperator("Status", Status.Progress),
                                                                                                      new BinaryOperator("Status", Status.Open)
                                                                                                      )));

                                if (_locPurchaseInvoiceLines != null && _locPurchaseInvoiceLines.Count > 0)
                                {
                                    foreach (PurchaseInvoiceLine _locPurchaseInvoiceLine in _locPurchaseInvoiceLines)
                                    {
                                        GetSumTotalTax(_currSession, _locPurchaseInvoiceLine);
                                        SetTaxAfterDiscount(_currSession, _locPurchaseInvoiceLine);
                                        SetTaxBeforeDiscount(_currSession, _locPurchaseInvoiceLine);
                                    }

                                    SuccessMessageShow("Purchase Invoice Line Line has been Processed");
                                }
                                else
                                {
                                    ErrorMessageShow("Data Purchase Invoice Line Line Not Available");
                                }
                            }
                        }
                    }
                    if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                    {
                        _objectSpace.CommitChanges();
                        _objectSpace.Refresh();
                    }
                    if (View is ListView)
                    {
                        _objectSpace.CommitChanges();
                        View.ObjectSpace.Refresh();
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("BusinessObject = PurchaseInvoiceLine " + ex.ToString());
            }
        }

        private void PurchaseInvoiceLineSelectAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;

                if (_objectToProcess != null)
                {
                    foreach (Object obj in _objectToProcess)
                    {
                        PurchaseInvoiceLine _locPurchaseInvoiceLineOS = (PurchaseInvoiceLine)_objectSpace.GetObject(obj);

                        if (_locPurchaseInvoiceLineOS != null)
                        {
                            if (_locPurchaseInvoiceLineOS.Session != null)
                            {
                                _currSession = _locPurchaseInvoiceLineOS.Session;
                            }

                            if (_locPurchaseInvoiceLineOS.Code != null && _currSession != null)
                            {
                                _currObjectId = _locPurchaseInvoiceLineOS.Code;

                                XPCollection<PurchaseInvoiceLine> _locPurchaseInvoiceLines = new XPCollection<PurchaseInvoiceLine>
                                                                                                     (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                      new BinaryOperator("Code", _currObjectId),
                                                                                                      new GroupOperator(GroupOperatorType.Or,
                                                                                                      new BinaryOperator("Status", Status.Posted),
                                                                                                      new BinaryOperator("Status", Status.Progress),
                                                                                                      new BinaryOperator("Status", Status.Open)
                                                                                                      )));

                                if (_locPurchaseInvoiceLines != null && _locPurchaseInvoiceLines.Count > 0)
                                {
                                    foreach (PurchaseInvoiceLine _locPurchaseInvoiceLine in _locPurchaseInvoiceLines)
                                    {
                                        _locPurchaseInvoiceLine.Select = true;
                                        _locPurchaseInvoiceLine.Save();
                                        _locPurchaseInvoiceLine.Session.CommitTransaction();
                                    }

                                    SuccessMessageShow("Purchase Invoice Line has been Select");
                                }
                                else
                                {
                                    ErrorMessageShow("Data Purchase Invoice Line Not Available");
                                }
                            }
                        }
                    }
                    if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                    {
                        _objectSpace.CommitChanges();
                        _objectSpace.Refresh();
                    }
                    if (View is ListView)
                    {
                        _objectSpace.CommitChanges();
                        View.ObjectSpace.Refresh();
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("BusinessObject = PurchaseInvoiceLine" + ex.ToString());
            }
        }

        private void PurchaseInvoiceLineUnselectAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;

                if (_objectToProcess != null)
                {
                    foreach (Object obj in _objectToProcess)
                    {
                        PurchaseInvoiceLine _locPurchaseInvoiceLineOS = (PurchaseInvoiceLine)_objectSpace.GetObject(obj);

                        if (_locPurchaseInvoiceLineOS != null)
                        {
                            if (_locPurchaseInvoiceLineOS.Session != null)
                            {
                                _currSession = _locPurchaseInvoiceLineOS.Session;
                            }

                            if (_locPurchaseInvoiceLineOS.Code != null && _currSession != null)
                            {
                                _currObjectId = _locPurchaseInvoiceLineOS.Code;

                                XPCollection<PurchaseInvoiceLine> _locPurchaseInvoiceLines = new XPCollection<PurchaseInvoiceLine>
                                                                                                     (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                      new BinaryOperator("Code", _currObjectId),
                                                                                                      new GroupOperator(GroupOperatorType.Or,
                                                                                                      new BinaryOperator("Status", Status.Posted),
                                                                                                      new BinaryOperator("Status", Status.Progress),
                                                                                                      new BinaryOperator("Status", Status.Open)
                                                                                                      )));

                                if (_locPurchaseInvoiceLines != null && _locPurchaseInvoiceLines.Count > 0)
                                {
                                    foreach (PurchaseInvoiceLine _locPurchaseInvoiceLine in _locPurchaseInvoiceLines)
                                    {
                                        _locPurchaseInvoiceLine.Select = false;
                                        _locPurchaseInvoiceLine.Save();
                                        _locPurchaseInvoiceLine.Session.CommitTransaction();
                                    }

                                    SuccessMessageShow("Purchase Invoice Line has been Unselect");
                                }
                                else
                                {
                                    ErrorMessageShow("Data Purchase Invoice Line Not Available");
                                }
                            }
                        }
                    }
                    if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                    {
                        _objectSpace.CommitChanges();
                        _objectSpace.Refresh();
                    }
                    if (View is ListView)
                    {
                        _objectSpace.CommitChanges();
                        View.ObjectSpace.Refresh();
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("BusinessObject = PurchaseInvoiceLine" + ex.ToString());
            }
        }

        private void PurchaseInvoiceLineListviewFilterSelectionAction_Execute(object sender, SingleChoiceActionExecuteEventArgs e)
        {
            try
            {
                if ((View is ListView) & (View.ObjectTypeInfo.Type == typeof(PurchaseInvoiceLine)))
                {
                    if ((Status)e.SelectedChoiceActionItem.Data == Status.None)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.None, BinaryOperatorType.NotEqual);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Open)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Open, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Progress)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Progress, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Posted)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Posted, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Lock)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Lock, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Close)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Close, BinaryOperatorType.Equal);
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PurchaseInvoiceLine " + ex.ToString());
            }
        }

        //=============================================== Code In Here ===============================================

        #region Tax

        private void GetSumTotalTax(Session _currSession, PurchaseInvoiceLine _locPurchaseInvoiceLineXPO)
        {
            try
            {
                double _locTxValue = 0;

                if (_locPurchaseInvoiceLineXPO != null)
                {
                    XPCollection<TaxLine> _locTaxLines = new XPCollection<TaxLine>(_currSession,
                                                     new BinaryOperator("PurchaseInvoiceLine", _locPurchaseInvoiceLineXPO));

                    if (_locTaxLines != null && _locTaxLines.Count() > 0)
                    {
                        foreach (TaxLine _locTaxLine in _locTaxLines)
                        {
                            if (_locTaxLine.TxValue > 0 && _locTaxLine.TxAmount > 0)
                            {
                                if (_locTaxLine.TaxNature == TaxNature.Increase)
                                {
                                    _locTxValue = _locTxValue + _locTaxLine.TxValue;
                                }
                                else if (_locTaxLine.TaxNature == TaxNature.Decrease)
                                {
                                    _locTxValue = _locTxValue - _locTaxLine.TxValue;
                                }
                            }
                        }

                        _locPurchaseInvoiceLineXPO.TxValue = _locTxValue;
                        _locPurchaseInvoiceLineXPO.Save();
                        _locPurchaseInvoiceLineXPO.Session.CommitTransaction();
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("BusinessObject = PurchaseInvoiceLine" + ex.ToString());
            }
        }

        private void SetTaxAfterDiscount(Session _currSession, PurchaseInvoiceLine _locPurchaseInvoiceLineXPO)
        {
            try
            {
                double _locTxAmount = 0;
                double _locTAmount = 0;

                if (_locPurchaseInvoiceLineXPO != null)
                {
                    if (_locPurchaseInvoiceLineXPO.TaxRule == TaxRule.AfterDiscount && _locPurchaseInvoiceLineXPO.MultiTax == false)
                    {
                        _locTxAmount = _locPurchaseInvoiceLineXPO.TxValue / 100 * (_locPurchaseInvoiceLineXPO.TUAmount - _locPurchaseInvoiceLineXPO.DiscAmount);

                        if (_locPurchaseInvoiceLineXPO.Tax.TaxNature == TaxNature.Increase)
                        {
                            _locTAmount = _locPurchaseInvoiceLineXPO.TAmount + _locTxAmount;
                        }
                        if (_locPurchaseInvoiceLineXPO.Tax.TaxNature == TaxNature.Decrease)
                        {
                            _locTAmount = _locPurchaseInvoiceLineXPO.TAmount - _locTxAmount;
                        }
                        _locPurchaseInvoiceLineXPO.TxAmount = _locTxAmount;
                        _locPurchaseInvoiceLineXPO.TAmount = _locTAmount;
                        _locPurchaseInvoiceLineXPO.Save();
                        _locPurchaseInvoiceLineXPO.Session.CommitTransaction();
                    }

                    if (_locPurchaseInvoiceLineXPO.TaxRule == TaxRule.AfterDiscount && _locPurchaseInvoiceLineXPO.MultiTax == true)
                    {
                        XPCollection<TaxLine> _locTaxLines = new XPCollection<TaxLine>(_currSession,
                                                     new BinaryOperator("PurchaseInvoiceLine", _locPurchaseInvoiceLineXPO));

                        if (_locTaxLines != null && _locTaxLines.Count() > 0)
                        {
                            foreach (TaxLine _locTaxLine in _locTaxLines)
                            {
                                if (_locTaxLine.TxValue > 0 && _locTaxLine.TxAmount > 0)
                                {
                                    if (_locTaxLine.TaxNature == TaxNature.Increase)
                                    {
                                        _locTxAmount = _locTxAmount + (_locTaxLine.TxValue / 100) * (_locPurchaseInvoiceLineXPO.TUAmount - _locPurchaseInvoiceLineXPO.DiscAmount);
                                    }
                                    else if (_locTaxLine.TaxNature == TaxNature.Decrease)
                                    {
                                        _locTxAmount = _locTxAmount - (_locTaxLine.TxValue / 100) * (_locPurchaseInvoiceLineXPO.TUAmount - _locPurchaseInvoiceLineXPO.DiscAmount);
                                    }
                                }
                            }

                            _locPurchaseInvoiceLineXPO.TxAmount = _locTxAmount;
                            _locPurchaseInvoiceLineXPO.TAmount = _locPurchaseInvoiceLineXPO.TAmount;
                            _locPurchaseInvoiceLineXPO.Save();
                            _locPurchaseInvoiceLineXPO.Session.CommitTransaction();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("BusinessObject = PurchaseInvoiceLine" + ex.ToString());
            }
        }

        private void SetTaxBeforeDiscount(Session _currSession, PurchaseInvoiceLine _locPurchaseInvoiceLineXPO)
        {
            try
            {
                double _locTxAmount = 0;
                double _locTAmount = 0;

                if (_locPurchaseInvoiceLineXPO != null)
                {
                    if ((_locPurchaseInvoiceLineXPO.TaxRule == TaxRule.BeforeDiscount || _locPurchaseInvoiceLineXPO.TaxRule == TaxRule.None) && _locPurchaseInvoiceLineXPO.MultiTax == false)
                    {
                        _locTxAmount = _locPurchaseInvoiceLineXPO.TxValue / 100 * _locPurchaseInvoiceLineXPO.TUAmount;

                        if (_locPurchaseInvoiceLineXPO.Tax.TaxNature == TaxNature.Increase)
                        {
                            _locTAmount = _locPurchaseInvoiceLineXPO.TAmount + _locTxAmount;
                        }
                        if (_locPurchaseInvoiceLineXPO.Tax.TaxNature == TaxNature.Decrease)
                        {
                            _locTAmount = _locPurchaseInvoiceLineXPO.TAmount - _locTxAmount;
                        }
                        _locPurchaseInvoiceLineXPO.TxAmount = _locTxAmount;
                        _locPurchaseInvoiceLineXPO.TAmount = _locTAmount;
                        _locPurchaseInvoiceLineXPO.Save();
                        _locPurchaseInvoiceLineXPO.Session.CommitTransaction();
                    }

                    if ((_locPurchaseInvoiceLineXPO.TaxRule == TaxRule.BeforeDiscount || _locPurchaseInvoiceLineXPO.TaxRule == TaxRule.None) && _locPurchaseInvoiceLineXPO.MultiTax == true)
                    {
                        XPCollection<TaxLine> _locTaxLines = new XPCollection<TaxLine>(_currSession,
                                                     new BinaryOperator("PurchaseInvoiceLine", _locPurchaseInvoiceLineXPO));

                        if (_locTaxLines != null && _locTaxLines.Count() > 0)
                        {
                            foreach (TaxLine _locTaxLine in _locTaxLines)
                            {
                                if (_locTaxLine.TxValue > 0 && _locTaxLine.TxAmount > 0)
                                {
                                    if (_locTaxLine.TaxNature == TaxNature.Increase)
                                    {
                                        _locTxAmount = _locTxAmount + (_locPurchaseInvoiceLineXPO.UAmount * _locPurchaseInvoiceLineXPO.TQty * _locTaxLine.TxValue / 100);
                                    }
                                    else if (_locTaxLine.TaxNature == TaxNature.Decrease)
                                    {
                                        _locTxAmount = _locTxAmount - (_locPurchaseInvoiceLineXPO.UAmount * _locPurchaseInvoiceLineXPO.TQty * _locTaxLine.TxValue / 100);
                                    }
                                }
                            }

                            _locPurchaseInvoiceLineXPO.TxAmount = _locTxAmount;
                            _locPurchaseInvoiceLineXPO.TAmount = _locPurchaseInvoiceLineXPO.TAmount; ;
                            _locPurchaseInvoiceLineXPO.Save();
                            _locPurchaseInvoiceLineXPO.Session.CommitTransaction();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("BusinessObject = PurchaseInvoiceLine" + ex.ToString());
            }
        }

        #endregion Tax

        #region Global Method

        private void SuccessMessageShow(string _locActionName)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 2000;
            options.Message = string.Format("{0}", _locActionName);
            options.Type = InformationType.Success;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Success";
            options.Win.Type = WinMessageType.Flyout;
            Application.ShowViewStrategy.ShowMessage(options);
        }

        private void ErrorMessageShow(string _locMessage)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 2000;
            options.Message = string.Format(_locMessage);
            options.Type = InformationType.Warning;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Error";
            options.Win.Type = WinMessageType.Alert;
            Application.ShowViewStrategy.ShowMessage(options);
        }

        #endregion Global Method

        
    }
}
