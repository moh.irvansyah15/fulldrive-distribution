﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;

using FullDrive.Module.BusinessObjects;
using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.Xpo;
using System.Collections;
using DevExpress.Xpo;
using System.Web;
using System.IO;

namespace FullDrive.Module.Controllers
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class PurchasePrePaymentInvoiceActionController : ViewController
    {
        private ChoiceActionItem _selectionListviewFilter;
        private ChoiceActionItem _selectionApprovalListviewFilter;
        private ChoiceActionItem _setApprovalLevel;

        public PurchasePrePaymentInvoiceActionController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.
            #region Filter Status
            PurchasePrePaymentInvoiceListviewFilterSelectionAction.Items.Clear();
            foreach (object _currApproval in Enum.GetValues(typeof(CustomProcess.Status)))
            {
                EnumDescriptor _ed = new EnumDescriptor(typeof(CustomProcess.Status));
                _selectionListviewFilter = new ChoiceActionItem(_ed.GetCaption(_currApproval), _currApproval);
                PurchasePrePaymentInvoiceListviewFilterSelectionAction.Items.Add(_selectionListviewFilter);
            }
            #endregion Filter Status

            #region FilterApproval
            PurchasePrePaymentInvoiceListviewFilterApprovalSelectionAction.Items.Clear();
            foreach (object _currApprovalFilter in Enum.GetValues(typeof(CustomProcess.ApprovalFilter)))
            {
                EnumDescriptor _ed = new EnumDescriptor(typeof(CustomProcess.ApprovalFilter));
                _selectionApprovalListviewFilter = new ChoiceActionItem(_ed.GetCaption(_currApprovalFilter), _currApprovalFilter);
                PurchasePrePaymentInvoiceListviewFilterApprovalSelectionAction.Items.Add(_selectionApprovalListviewFilter);
            }
            #endregion FilterApproval
        }

        protected override void OnActivated()
        {
            TypeOfView = typeof(ObjectView);
            base.OnActivated();
            // Perform various tasks depending on the target View.
            #region Approval
            var _locUser = SecuritySystem.CurrentUserName;
           
            if (this.ObjectSpace != null)
            {
                UserAccess _locUserAccess = this.ObjectSpace.FindObject<UserAccess>(new BinaryOperator("UserName", _locUser));

                if (_locUserAccess != null)
                {
                    PurchasePrePaymentInvoiceApprovalAction.Items.Clear();

                    XPCollection<ApplicationSetupDetail> _locAppSetupDetails = new XPCollection<ApplicationSetupDetail>
                                                                                 (_locUserAccess.Session, new GroupOperator(GroupOperatorType.And,
                                                                                  new BinaryOperator("UserAccess", _locUserAccess),
                                                                                  new BinaryOperator("ObjectList", CustomProcess.ObjectList.PurchasePrePaymentInvoice),
                                                                                  new BinaryOperator("FunctionList", CustomProcess.FunctionList.Approval),
                                                                                  new BinaryOperator("Active", true)));

                    if (_locAppSetupDetails != null && _locAppSetupDetails.Count > 0)
                    {

                        foreach (ApplicationSetupDetail _locAppSetupDetail in _locAppSetupDetails)
                        {
                            foreach (object _currApproval in Enum.GetValues(typeof(CustomProcess.ApprovalLevel)))
                            {
                                if (_locAppSetupDetail.ApprovalLevel != CustomProcess.ApprovalLevel.None)
                                {
                                    EnumDescriptor _ed = new EnumDescriptor(typeof(CustomProcess.ApprovalLevel));
                                    _setApprovalLevel = new ChoiceActionItem(_ed.GetCaption(_locAppSetupDetail.ApprovalLevel), _locAppSetupDetail.ApprovalLevel);
                                }
                            }
                            PurchasePrePaymentInvoiceApprovalAction.Items.Add(_setApprovalLevel);
                        }
                    }
                }
            }
            #endregion Approval
        }

        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }

        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        private void PurchasePrePaymentInvoiceGetAmountAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;

                if (_objectsToProcess != null)
                {
                    foreach (Object obj in _objectsToProcess)
                    {
                        PurchasePrePaymentInvoice _locPurchasePrePaymentInvoiceOS = (PurchasePrePaymentInvoice)_objectSpace.GetObject(obj);

                        if (_locPurchasePrePaymentInvoiceOS != null)
                        {
                            if (_locPurchasePrePaymentInvoiceOS.Session != null)
                            {
                                _currSession = _locPurchasePrePaymentInvoiceOS.Session;
                            }

                            if (_locPurchasePrePaymentInvoiceOS.Code != null && _currSession != null)
                            {
                                _currObjectId = _locPurchasePrePaymentInvoiceOS.Code;

                                PurchasePrePaymentInvoice _locPurchasePrePaymentInvoiceXPO = _currSession.FindObject<PurchasePrePaymentInvoice>
                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                     new BinaryOperator("Code", _currObjectId),
                                                                     new GroupOperator(GroupOperatorType.Or,
                                                                     new BinaryOperator("Status", Status.Open),
                                                                     new BinaryOperator("Status", Status.Progress))));

                                if (_locPurchasePrePaymentInvoiceXPO != null)
                                {
                                    GetAmountFromPO(_currSession, _locPurchasePrePaymentInvoiceXPO);
                                }
                                else
                                {
                                    ErrorMessageShow("Data Purchase Pre Payment Invoice Not Available");
                                }
                            }
                            else
                            {
                                ErrorMessageShow("Data Purchase Pre Payment Invoice Not Available");
                            }
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PurchasePrePaymentInvoice " + ex.ToString());
            }
        }

        private void PurchasePrePaymentInvoiceApprovalAction_Execute(object sender, SingleChoiceActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                ApplicationSetupDetail _locAppSetDetail = null;
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                var User = SecuritySystem.CurrentUserName;
                Session _currentSession = null;
                string _currObjectId = null;

                foreach (Object obj in _objectsToProcess)
                {
                    PurchasePrePaymentInvoice _objInNewObjectSpace = (PurchasePrePaymentInvoice)_objectSpace.GetObject(obj);

                    if (_objInNewObjectSpace != null)
                    {
                        if (_objInNewObjectSpace.Code != null)
                        {
                            _currObjectId = _objInNewObjectSpace.Code;
                        }
                        if (_objInNewObjectSpace.Session != null)
                        {
                            _currentSession = _objInNewObjectSpace.Session;
                        }
                    }

                    if (_currObjectId != null && _currentSession != null)
                    {
                        UserAccess _locUserAccess = _currentSession.FindObject<UserAccess>(new BinaryOperator("UserName", User));

                        PurchasePrePaymentInvoice _locPurchasePrePaymentInvoiceXPO = _currentSession.FindObject<PurchasePrePaymentInvoice>
                                                                        (new GroupOperator(GroupOperatorType.And,
                                                                         new BinaryOperator("Code", _currObjectId),
                                                                         new BinaryOperator("Status", Status.Open)));

                        if (_locPurchasePrePaymentInvoiceXPO != null)
                        {
                            ApprovalLine _locApprovalLine = _currentSession.FindObject<ApprovalLine>
                                                                            (new GroupOperator(GroupOperatorType.And,
                                                                             new BinaryOperator("PurchasePrePaymentInvoice", _locPurchasePrePaymentInvoiceXPO),
                                                                             new BinaryOperator("EndApproval", true)));
                            if (_locApprovalLine == null)
                            {
                                #region Approval Level 1
                                if ((ApprovalLevel)e.SelectedChoiceActionItem.Data == ApprovalLevel.Level1)
                                {
                                    _locAppSetDetail = _globFunc.GetApplicationSetupDetailByApprovalLevel(_currentSession, _locUserAccess, ApprovalLevel.Level1, ObjectList.PurchasePrePaymentInvoice);
                                    if (_locAppSetDetail != null)
                                    {
                                        //Buat bs input langsung ke approvalline
                                        ApprovalLine _locApprovalLineXPO = _currentSession.FindObject<ApprovalLine>
                                                                            (new GroupOperator(GroupOperatorType.And,
                                                                             new BinaryOperator("PurchasePrePaymentInvoice", _locPurchasePrePaymentInvoiceXPO),
                                                                             new BinaryOperator("ApprovalLevel", ApprovalLevel.Level1)));
                                        if (_locApprovalLineXPO == null)
                                        {

                                            if (_locAppSetDetail.EndApproval == true)
                                            {
                                                ApprovalLine _saveDataAL = new ApprovalLine(_currentSession)
                                                {
                                                    ApprovalDate = now,
                                                    ApprovalStatus = Status.Approved,
                                                    ApprovalLevel = ApprovalLevel.Level1,
                                                    EndApproval = true,
                                                    PurchasePrePaymentInvoice = _locPurchasePrePaymentInvoiceXPO,
                                                };
                                                _saveDataAL.Save();
                                                _saveDataAL.Session.CommitTransaction();

                                                _locPurchasePrePaymentInvoiceXPO.ActivationPosting = true;
                                                _locPurchasePrePaymentInvoiceXPO.ActiveApproved1 = false;
                                                _locPurchasePrePaymentInvoiceXPO.ActiveApproved2 = false;
                                                _locPurchasePrePaymentInvoiceXPO.ActiveApproved3 = true;
                                                _locPurchasePrePaymentInvoiceXPO.Save();
                                                _locPurchasePrePaymentInvoiceXPO.Session.CommitTransaction();
                                            }
                                            else
                                            {
                                                ApprovalLine _saveDataAL = new ApprovalLine(_currentSession)
                                                {
                                                    ApprovalDate = now,
                                                    ApprovalStatus = Status.Approved,
                                                    ApprovalLevel = ApprovalLevel.Level1,
                                                    PurchasePrePaymentInvoice = _locPurchasePrePaymentInvoiceXPO,
                                                };
                                                _saveDataAL.Save();
                                                _saveDataAL.Session.CommitTransaction();

                                                _locPurchasePrePaymentInvoiceXPO.ActiveApproved1 = true;
                                                _locPurchasePrePaymentInvoiceXPO.ActiveApproved2 = false;
                                                _locPurchasePrePaymentInvoiceXPO.ActiveApproved3 = false;
                                                _locPurchasePrePaymentInvoiceXPO.Save();
                                                _locPurchasePrePaymentInvoiceXPO.Session.CommitTransaction();
                                            }

                                            //Send Email
                                            ApprovalLine _locApprovalLineXPO2 = _currentSession.FindObject<ApprovalLine>
                                                                                (new GroupOperator(GroupOperatorType.And,
                                                                                 new BinaryOperator("PurchasePrePaymentInvoice", _locPurchasePrePaymentInvoiceXPO)));

                                            if (_locApprovalLineXPO2 != null)
                                            {
                                                SendEmail(_currentSession, _locPurchasePrePaymentInvoiceXPO, Status.Progress, _locApprovalLineXPO2);
                                            }

                                            SuccessMessageShow("Purchase Pre Payment Invoice has successfully Approve");
                                        }
                                    }
                                }
                                #endregion Approval Level 1

                                #region Approval Level 2
                                if ((ApprovalLevel)e.SelectedChoiceActionItem.Data == ApprovalLevel.Level2)
                                {
                                    _locAppSetDetail = _globFunc.GetApplicationSetupDetailByApprovalLevel(_currentSession, _locUserAccess, ApprovalLevel.Level2, ObjectList.PurchasePrePaymentInvoice);

                                    if (_locAppSetDetail != null)
                                    {
                                        ApprovalLine _locApprovalLineXPO = _currentSession.FindObject<ApprovalLine>
                                                                            (new GroupOperator(GroupOperatorType.And,
                                                                             new BinaryOperator("PurchasePrePaymentInvoice", _locPurchasePrePaymentInvoiceXPO),
                                                                             new BinaryOperator("ApprovalLevel", ApprovalLevel.Level2)));

                                        if (_locApprovalLineXPO == null)
                                        {
                                            if (_locAppSetDetail.EndApproval == true)
                                            {
                                                ApprovalLine _saveDataAL = new ApprovalLine(_currentSession)
                                                {
                                                    ApprovalDate = now,
                                                    ApprovalStatus = Status.Approved,
                                                    ApprovalLevel = ApprovalLevel.Level2,
                                                    EndApproval = true,
                                                    PurchasePrePaymentInvoice = _locPurchasePrePaymentInvoiceXPO,
                                                };
                                                _saveDataAL.Save();
                                                _saveDataAL.Session.CommitTransaction();

                                                _locPurchasePrePaymentInvoiceXPO.ActivationPosting = true;
                                                _locPurchasePrePaymentInvoiceXPO.ActiveApproved1 = false;
                                                _locPurchasePrePaymentInvoiceXPO.ActiveApproved2 = false;
                                                _locPurchasePrePaymentInvoiceXPO.ActiveApproved3 = true;
                                                _locPurchasePrePaymentInvoiceXPO.Save();
                                                _locPurchasePrePaymentInvoiceXPO.Session.CommitTransaction();
                                            }
                                            else
                                            {
                                                ApprovalLine _saveDataAL = new ApprovalLine(_currentSession)
                                                {
                                                    ApprovalDate = now,
                                                    ApprovalStatus = Status.Approved,
                                                    ApprovalLevel = ApprovalLevel.Level2,
                                                    PurchasePrePaymentInvoice = _locPurchasePrePaymentInvoiceXPO,
                                                };
                                                _saveDataAL.Save();
                                                _saveDataAL.Session.CommitTransaction();

                                                SetApprovalLine(_currentSession, _locPurchasePrePaymentInvoiceXPO, ApprovalLevel.Level1);

                                                _locPurchasePrePaymentInvoiceXPO.ActiveApproved1 = false;
                                                _locPurchasePrePaymentInvoiceXPO.ActiveApproved2 = true;
                                                _locPurchasePrePaymentInvoiceXPO.ActiveApproved3 = false;
                                                _locPurchasePrePaymentInvoiceXPO.Save();
                                                _locPurchasePrePaymentInvoiceXPO.Session.CommitTransaction();
                                            }

                                            //Send Email
                                            ApprovalLine _locApprovalLineXPO2 = _currentSession.FindObject<ApprovalLine>
                                                                                (new GroupOperator(GroupOperatorType.And,
                                                                                 new BinaryOperator("PurchasePrePaymentInvoice", _locPurchasePrePaymentInvoiceXPO)));

                                            if (_locApprovalLineXPO2 != null)
                                            {
                                                SendEmail(_currentSession, _locPurchasePrePaymentInvoiceXPO, Status.Progress, _locApprovalLineXPO2);
                                            }

                                            SuccessMessageShow("Purchase Pre Payment Invoice has successfully Approve");
                                        }
                                    }
                                }
                                #endregion Approval Level 2

                                #region Approval Level 3
                                if ((ApprovalLevel)e.SelectedChoiceActionItem.Data == ApprovalLevel.Level3)
                                {
                                    _locAppSetDetail = _globFunc.GetApplicationSetupDetailByApprovalLevel(_currentSession, _locUserAccess, ApprovalLevel.Level3, ObjectList.PurchasePrePaymentInvoice);

                                    if (_locAppSetDetail != null)
                                    {
                                        ApprovalLine _locApprovalLineXPO = _currentSession.FindObject<ApprovalLine>
                                                                            (new GroupOperator(GroupOperatorType.And,
                                                                             new BinaryOperator("PurchasePrePaymentInvoice", _locPurchasePrePaymentInvoiceXPO),
                                                                             new BinaryOperator("ApprovalLevel", ApprovalLevel.Level3)));

                                        if (_locApprovalLineXPO == null)
                                        {
                                            if (_locAppSetDetail.EndApproval == true)
                                            {
                                                ApprovalLine _saveDataAL = new ApprovalLine(_currentSession)
                                                {
                                                    ApprovalDate = now,
                                                    ApprovalStatus = Status.Approved,
                                                    ApprovalLevel = ApprovalLevel.Level3,
                                                    EndApproval = true,
                                                    PurchasePrePaymentInvoice = _locPurchasePrePaymentInvoiceXPO,
                                                };
                                                _saveDataAL.Save();
                                                _saveDataAL.Session.CommitTransaction();

                                                _locPurchasePrePaymentInvoiceXPO.ActivationPosting = true;
                                                _locPurchasePrePaymentInvoiceXPO.ActiveApproved1 = false;
                                                _locPurchasePrePaymentInvoiceXPO.ActiveApproved2 = false;
                                                _locPurchasePrePaymentInvoiceXPO.ActiveApproved3 = true;
                                                _locPurchasePrePaymentInvoiceXPO.Save();
                                                _locPurchasePrePaymentInvoiceXPO.Session.CommitTransaction();
                                            }
                                            else
                                            {
                                                ApprovalLine _saveDataAL = new ApprovalLine(_currentSession)
                                                {
                                                    ApprovalDate = now,
                                                    ApprovalStatus = Status.Approved,
                                                    ApprovalLevel = ApprovalLevel.Level3,
                                                    PurchasePrePaymentInvoice = _locPurchasePrePaymentInvoiceXPO,
                                                };
                                                _saveDataAL.Save();
                                                _saveDataAL.Session.CommitTransaction();

                                                SetApprovalLine(_currentSession, _locPurchasePrePaymentInvoiceXPO, ApprovalLevel.Level2);
                                                SetApprovalLine(_currentSession, _locPurchasePrePaymentInvoiceXPO, ApprovalLevel.Level1);

                                                _locPurchasePrePaymentInvoiceXPO.ActiveApproved1 = false;
                                                _locPurchasePrePaymentInvoiceXPO.ActiveApproved2 = false;
                                                _locPurchasePrePaymentInvoiceXPO.ActiveApproved3 = true;
                                                _locPurchasePrePaymentInvoiceXPO.Save();
                                                _locPurchasePrePaymentInvoiceXPO.Session.CommitTransaction();
                                            }

                                            //Send Email
                                            ApprovalLine _locApprovalLineXPO2 = _currentSession.FindObject<ApprovalLine>
                                                                                (new GroupOperator(GroupOperatorType.And,
                                                                                 new BinaryOperator("PurchasePrePaymentInvoice", _locPurchasePrePaymentInvoiceXPO)));

                                            if (_locApprovalLineXPO2 != null)
                                            {
                                                SendEmail(_currentSession, _locPurchasePrePaymentInvoiceXPO, Status.Progress, _locApprovalLineXPO2);
                                            }

                                            SuccessMessageShow("Purchase Pre Payment Invoice has successfully Approve");
                                        }
                                    }
                                }
                                #endregion Approval Level 3
                            }
                        }
                        else
                        {
                            ErrorMessageShow("Purchase Invoice Not Available");
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PurchasePrePaymentInvoice " + ex.ToString());
            }
        }

        private void PurchasePrePaymentInvoiceProgressAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;
                Status _locStatus = Status.Open;

                if (_objectsToProcess != null)
                {
                    foreach (Object obj in _objectsToProcess)
                    {
                        PurchasePrePaymentInvoice _locPurchasePrePaymentInvoiceOS = (PurchasePrePaymentInvoice)_objectSpace.GetObject(obj);

                        if (_locPurchasePrePaymentInvoiceOS != null)
                        {
                            if (_locPurchasePrePaymentInvoiceOS.Session != null)
                            {
                                _currSession = _locPurchasePrePaymentInvoiceOS.Session;
                            }

                            if (_locPurchasePrePaymentInvoiceOS.Code != null && _currSession != null)
                            {
                                _currObjectId = _locPurchasePrePaymentInvoiceOS.Code;

                                PurchasePrePaymentInvoice _locPurchasePrePaymentInvoiceXPO = _currSession.FindObject<PurchasePrePaymentInvoice>
                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                     new BinaryOperator("Code", _currObjectId),
                                                                     new GroupOperator(GroupOperatorType.Or,
                                                                     new BinaryOperator("Status", Status.Open),
                                                                     new BinaryOperator("Status", Status.Progress))));

                                if (_locPurchasePrePaymentInvoiceXPO != null)
                                {
                                    ApprovalLine _locApprovalLine = _currSession.FindObject<ApprovalLine>
                                                                            (new GroupOperator(GroupOperatorType.And,
                                                                             new BinaryOperator("PurchasePrePaymentInvoice", _locPurchasePrePaymentInvoiceXPO),
                                                                             new BinaryOperator("EndApproval", true)));
                                    if (_locApprovalLine != null)
                                    {
                                        if (_locPurchasePrePaymentInvoiceXPO.Status == Status.Open)
                                        {
                                            _locStatus = Status.Progress;
                                        }
                                        else
                                        {
                                            _locStatus = _locPurchasePrePaymentInvoiceXPO.Status;
                                        }

                                        _locPurchasePrePaymentInvoiceXPO.Status = _locStatus;
                                        _locPurchasePrePaymentInvoiceXPO.StatusDate = now;
                                        _locPurchasePrePaymentInvoiceXPO.Save();
                                        _locPurchasePrePaymentInvoiceXPO.Session.CommitTransaction();
                                        SuccessMessageShow(_locPurchasePrePaymentInvoiceXPO.Code + "has been successfully updated to progress");
                                    }
                                }
                                else
                                {
                                    ErrorMessageShow("Data Purchase Pre Payment Invoice Not Available");
                                }
                            }
                            else
                            {
                                ErrorMessageShow("Data Purchase Pre Payment Invoice Not Available");
                            }
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PurchasePrePaymentInvoice " + ex.ToString());
            }
        }

        private void PurchasePrePaymentInvoicePostingAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;

                if (_objectsToProcess != null)
                {
                    foreach (Object obj in _objectsToProcess)
                    {
                        PurchasePrePaymentInvoice _locPurchasePrePaymentInvoiceOS = (PurchasePrePaymentInvoice)_objectSpace.GetObject(obj);

                        if (_locPurchasePrePaymentInvoiceOS != null)
                        {
                            if (_locPurchasePrePaymentInvoiceOS.Session != null)
                            {
                                _currSession = _locPurchasePrePaymentInvoiceOS.Session;
                            }

                            if (_locPurchasePrePaymentInvoiceOS.Code != null && _currSession != null)
                            {
                                _currObjectId = _locPurchasePrePaymentInvoiceOS.Code;

                                PurchasePrePaymentInvoice _locPurchasePrePaymentInvoiceXPO = _currSession.FindObject<PurchasePrePaymentInvoice>
                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                     new BinaryOperator("Code", _currObjectId),
                                                                     new GroupOperator(GroupOperatorType.Or,
                                                                     new BinaryOperator("Status", Status.Progress),
                                                                     new BinaryOperator("Status", Status.Posted))));

                                if (_locPurchasePrePaymentInvoiceXPO != null)
                                {
                                    ApprovalLine _locApprovalLineXPO = _currSession.FindObject<ApprovalLine>
                                                                           (new GroupOperator(GroupOperatorType.And,
                                                                            new BinaryOperator("EndApproval", true),
                                                                            new BinaryOperator("PurchasePrePaymentInvoice", _locPurchasePrePaymentInvoiceXPO)));

                                    if (_locApprovalLineXPO != null)
                                    {
                                        if (_locPurchasePrePaymentInvoiceXPO.DP_Amount > 0)
                                        {
                                            SetPurchasePrePaymentInvoiceMonitoring(_currSession, _locPurchasePrePaymentInvoiceXPO);
                                            if (CheckJournalSetup(_currSession, _locPurchasePrePaymentInvoiceXPO) == true)
                                            {
                                                SetPrePaymentJournal(_currSession, _locPurchasePrePaymentInvoiceXPO);
                                            }
                                            SetFinalPurchasePrePaymentInvoice(_currSession, _locPurchasePrePaymentInvoiceXPO);
                                            SuccessMessageShow(_locPurchasePrePaymentInvoiceXPO.Code + " has been change successfully to Posted");
                                        }
                                    }
                                }
                                else
                                {
                                    ErrorMessageShow("Data Purchase Pre Payment Invoice Not Available");
                                }
                            }
                            else
                            {
                                ErrorMessageShow("Data Purchase Pre Payment Invoice Not Available");
                            }
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PurchasePrePaymentInvoice " + ex.ToString());
            }
        }

        private void PurchasePrePaymentInvoiceListviewFilterApprovalSelectionAction_Execute(object sender, SingleChoiceActionExecuteEventArgs e)
        {
            try
            {
                if ((View is ListView) & (View.ObjectTypeInfo.Type == typeof(PurchasePrePaymentInvoice)))
                {
                    if ((ApprovalFilter)e.SelectedChoiceActionItem.Data == ApprovalFilter.Approval)
                    {
                        ((ListView)View).CollectionSource.Criteria["FilterApproval1"] = new GroupOperator(GroupOperatorType.And,
                                                                                        new BinaryOperator("ActiveApproved3", false, BinaryOperatorType.Equal),
                                                                                        new GroupOperator(GroupOperatorType.Or,
                                                                                        new BinaryOperator("ActiveApproved1", true, BinaryOperatorType.Equal),
                                                                                        new BinaryOperator("ActiveApproved2", true, BinaryOperatorType.Equal)));
                    }
                    else if ((ApprovalFilter)e.SelectedChoiceActionItem.Data == ApprovalFilter.EndApproval)
                    {
                        ((ListView)View).CollectionSource.Criteria["FilterApproval1"] = new BinaryOperator("ActiveApproved3", true, BinaryOperatorType.Equal);
                    }
                    else if ((ApprovalFilter)e.SelectedChoiceActionItem.Data == ApprovalFilter.AllData)
                    {
                        ((ListView)View).CollectionSource.Criteria["FilterApproval1"] = new GroupOperator(GroupOperatorType.Or,
                                                                                        new BinaryOperator("ActiveApproved1", true, BinaryOperatorType.Equal),
                                                                                        new BinaryOperator("ActiveApproved2", true, BinaryOperatorType.Equal),
                                                                                        new BinaryOperator("ActiveApproved3", true, BinaryOperatorType.Equal),
                                                                                        new BinaryOperator("ActiveApproved1", false, BinaryOperatorType.Equal),
                                                                                        new BinaryOperator("ActiveApproved2", false, BinaryOperatorType.Equal),
                                                                                        new BinaryOperator("ActiveApproved3", false, BinaryOperatorType.Equal));
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PurchaseOrder " + ex.ToString());
            }
        }

        private void PurchasePrePaymentInvoiceListviewFilterSelectionAction_Execute(object sender, SingleChoiceActionExecuteEventArgs e)
        {
            try
            {
                if ((View is ListView) & (View.ObjectTypeInfo.Type == typeof(PurchasePrePaymentInvoice)))
                {
                    if ((Status)e.SelectedChoiceActionItem.Data == Status.None)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.None, BinaryOperatorType.NotEqual);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Open)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Open, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Progress)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Progress, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Posted)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Posted, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Lock)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Lock, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Close)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Close, BinaryOperatorType.Equal);
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PurchasePrePaymentInvoice " + ex.ToString());
            }
        }

        //============================================ Code In Here ============================================

        #region GetAmountFromPO

        private void GetAmountFromPO(Session _currSession, PurchasePrePaymentInvoice _locPurchasePrePaymentInvoiceXPO)
        {
            try
            {
                DateTime now = DateTime.Now;
                double _locAmount = 0;
                double _locTAmount = 0;
                if (_locPurchasePrePaymentInvoiceXPO != null && _locPurchasePrePaymentInvoiceXPO.PurchaseOrder != null)
                {
                    //Cek Payment Out Plan
                    XPCollection<PaymentOutPlan> _locPaymentOutPlans = new XPCollection<PaymentOutPlan>
                                                                (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("PurchaseOrder", _locPurchasePrePaymentInvoiceXPO.PurchaseOrder)));
                    if (_locPaymentOutPlans != null && _locPaymentOutPlans.Count() > 0)
                    {
                        foreach (PaymentOutPlan _locPaymentOutPlan in _locPaymentOutPlans)
                        {
                            if (_locPaymentOutPlan.Plan > 0)
                            {
                                _locAmount = _locAmount + _locPaymentOutPlan.Plan;
                            }
                        }

                        if (_locAmount <= _locPurchasePrePaymentInvoiceXPO.PurchaseOrder.Amount)
                        {
                            _locTAmount = _locPurchasePrePaymentInvoiceXPO.PurchaseOrder.Amount - _locAmount;
                        }
                    }
                    else
                    {
                        _locTAmount = _locPurchasePrePaymentInvoiceXPO.PurchaseOrder.Amount;
                    }

                    _locPurchasePrePaymentInvoiceXPO.TAmount = _locTAmount;
                    _locPurchasePrePaymentInvoiceXPO.Amount = _locTAmount;
                    _locPurchasePrePaymentInvoiceXPO.DP_Percentage = _locPurchasePrePaymentInvoiceXPO.PurchaseOrder.DP_Percentage;
                    _locPurchasePrePaymentInvoiceXPO.StatusDate = now;
                    _locPurchasePrePaymentInvoiceXPO.Save();
                    _locPurchasePrePaymentInvoiceXPO.Session.CommitTransaction();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = PurchasePrePaymentInvoice ", ex.ToString());
            }
        }

        #endregion GetAmountFromPO

        #region Posting Method

        private void SetPurchasePrePaymentInvoiceMonitoring(Session _currSession, PurchasePrePaymentInvoice _locPurchasePrePaymentInvoiceXPO)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                DateTime now = DateTime.Now;
                string _currSignCode = null;

                if (_locPurchasePrePaymentInvoiceXPO != null)
                {
                    if(_locPurchasePrePaymentInvoiceXPO.Status == Status.Progress)
                    {
                        _currSignCode = _globFunc.GetNumberingSignUnlockOptimisticRecordBasedOrganization(_currSession.DataLayer, ObjectList.PurchasePrePaymentInvoiceMonitoring, _locPurchasePrePaymentInvoiceXPO.Company, _locPurchasePrePaymentInvoiceXPO.Workplace);
                        
                        if(_currSignCode != null)
                        {
                            #region SavePurchasePrePaymentInvoiceMonitoring
                            PurchasePrePaymentInvoiceMonitoring _saveDataPurchasePrePaymentInvoiceMonitoring = new PurchasePrePaymentInvoiceMonitoring(_currSession)
                            {
                                SignCode = _currSignCode,
                                PurchasePrePaymentInvoice = _locPurchasePrePaymentInvoiceXPO,
                                PurchaseOrder = _locPurchasePrePaymentInvoiceXPO.PurchaseOrder,
                                Company = _locPurchasePrePaymentInvoiceXPO.Company,
                                Workplace = _locPurchasePrePaymentInvoiceXPO.Workplace,
                                Division = _locPurchasePrePaymentInvoiceXPO.Division,
                                Department = _locPurchasePrePaymentInvoiceXPO.Department,
                                Section = _locPurchasePrePaymentInvoiceXPO.Section,
                                Employee = _locPurchasePrePaymentInvoiceXPO.Employee,
                                DocDate = _locPurchasePrePaymentInvoiceXPO.DocDate,
                                JournalMonth = _locPurchasePrePaymentInvoiceXPO.JournalMonth,
                                JournalYear = _locPurchasePrePaymentInvoiceXPO.JournalYear,
                                Currency = _locPurchasePrePaymentInvoiceXPO.Currency,
                                TAmount = _locPurchasePrePaymentInvoiceXPO.TAmount,
                                Amount = _locPurchasePrePaymentInvoiceXPO.Amount,
                                DP_Percentage = _locPurchasePrePaymentInvoiceXPO.DP_Percentage,
                                DP_Amount = _locPurchasePrePaymentInvoiceXPO.DP_Amount,
                            };
                            _saveDataPurchasePrePaymentInvoiceMonitoring.Save();
                            _saveDataPurchasePrePaymentInvoiceMonitoring.Session.CommitTransaction();
                            #endregion SavePurchasePrePaymentInvoiceMonitoring

                            PurchasePrePaymentInvoiceMonitoring _locPurchasePrePaymentInvoiceMonitoring = _currSession.FindObject<PurchasePrePaymentInvoiceMonitoring>
                                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                                    new BinaryOperator("SignCode", _currSignCode)));
                            if (_locPurchasePrePaymentInvoiceMonitoring != null)
                            {
                                SetPaymentOutPlan(_currSession, _locPurchasePrePaymentInvoiceXPO, _locPurchasePrePaymentInvoiceMonitoring);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PurchasePrePaymentInvoice " + ex.ToString());
            }
        }

        private void SetPaymentOutPlan(Session _currSession, PurchasePrePaymentInvoice _locPurchasePrePaymentInvoiceXPO, PurchasePrePaymentInvoiceMonitoring _locPurchasePrePaymentInvoiceMonitoringXPO)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                DateTime now = DateTime.Now;

                if (_locPurchasePrePaymentInvoiceXPO != null && _locPurchasePrePaymentInvoiceMonitoringXPO != null)
                {
                    PaymentOutPlan _locPaymentOutPlan = _currSession.FindObject<PaymentOutPlan>
                                                    (new GroupOperator(GroupOperatorType.And,
                                                    new BinaryOperator("PurchasePrePaymentInvoiceMonitoring", _locPurchasePrePaymentInvoiceMonitoringXPO)));
                    if (_locPaymentOutPlan == null)
                    {
                        #region SavePaymentOutPlan
                        PaymentOutPlan _savePaymentOutPlan = new PaymentOutPlan(_currSession)
                        {
                            PaymentMethod = _locPurchasePrePaymentInvoiceXPO.PaymentMethod,
                            PaymentMethodType = _locPurchasePrePaymentInvoiceXPO.PaymentMethodType,
                            PaymentType = _locPurchasePrePaymentInvoiceXPO.PaymentType,
                            EstimatedDate = _locPurchasePrePaymentInvoiceXPO.EstimatedDate,
                            Plan = _locPurchasePrePaymentInvoiceXPO.DP_Amount,
                            Outstanding = _locPurchasePrePaymentInvoiceXPO.DP_Amount,
                            PurchaseOrder = _locPurchasePrePaymentInvoiceXPO.PurchaseOrder,
                            PurchasePrePaymentInvoice = _locPurchasePrePaymentInvoiceXPO,
                            PurchasePrePaymentInvoiceMonitoring = _locPurchasePrePaymentInvoiceMonitoringXPO,
                            Company = _locPurchasePrePaymentInvoiceXPO.Company,
                            Workplace = _locPurchasePrePaymentInvoiceXPO.Workplace,
                        };
                        _savePaymentOutPlan.Save();
                        _savePaymentOutPlan.Session.CommitTransaction();
                        #endregion SavePaymentOutPlan
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = PurchasePrePaymentInvoice ", ex.ToString());
            }
        }

        #region PrePaymentJournal

        private bool CheckJournalSetup(Session _currSession, PurchasePrePaymentInvoice _locPurchasePrePaymentInvoiceXPO)
        {
            bool _result = false;
            try
            {
                if (_locPurchasePrePaymentInvoiceXPO != null)
                {
                    if (_locPurchasePrePaymentInvoiceXPO.Company != null && _locPurchasePrePaymentInvoiceXPO.Workplace != null)
                    {
                        JournalSetupDetail _locJournalSetupDetail = _currSession.FindObject<JournalSetupDetail>
                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                    new BinaryOperator("Company", _locPurchasePrePaymentInvoiceXPO.Company),
                                                                    new BinaryOperator("Workplace", _locPurchasePrePaymentInvoiceXPO.Workplace),
                                                                    new BinaryOperator("JournalSetupType", JournalSetupType.Journal),
                                                                    new BinaryOperator("ObjectList", ObjectList.PurchasePrePaymentInvoice),
                                                                    new BinaryOperator("JournalProcess", JournalProcess.JournalPrePayment),
                                                                    new BinaryOperator("Active", true)));

                        if (_locJournalSetupDetail != null)
                        {
                            _result = true;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = PurchasePrePaymentInvoice ", ex.ToString());
            }
            return _result;
        }

        private void SetPrePaymentJournal(Session _currSession, PurchasePrePaymentInvoice _locPurchasePrePaymentInvoiceXPO)
        {
            try
            {
                DateTime now = DateTime.Now;
                double _locDP = 0;
                BusinessPartnerAccountGroup _locBusinessPartnerAccountGroup = null;
                CompanyAccountGroup _locCompanyAccountGroup = null;

                #region CreateInvoiceAPJournalDownPayment

                if (_locPurchasePrePaymentInvoiceXPO.DP_Amount > 0)
                {
                    _locDP = _locPurchasePrePaymentInvoiceXPO.DP_Amount;

                    #region JournalMapCompanyAccountGroup
                    if (_locPurchasePrePaymentInvoiceXPO.Company != null)
                    {
                        CompanyAccount _locCompanyAccount = _currSession.FindObject<CompanyAccount>(new GroupOperator(GroupOperatorType.And,
                                                        new BinaryOperator("Company", _locPurchasePrePaymentInvoiceXPO.Company),
                                                        new BinaryOperator("Workplace", _locPurchasePrePaymentInvoiceXPO.Workplace),
                                                        new BinaryOperator("Active", true)));
                        if(_locCompanyAccount != null)
                        {
                            if(_locCompanyAccount.CompanyAccountGroup != null)
                            {
                                _locCompanyAccountGroup = _locCompanyAccount.CompanyAccountGroup;

                                XPCollection<JournalMap> _locJournalMapItems = new XPCollection<JournalMap>
                                                                       (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                        new BinaryOperator("Company", _locPurchasePrePaymentInvoiceXPO.Company),
                                                                        new BinaryOperator("Workplace", _locPurchasePrePaymentInvoiceXPO.Workplace),
                                                                        new BinaryOperator("CompanyAccountGroup", _locCompanyAccountGroup),
                                                                        new BinaryOperator("Active", true)));

                                if (_locJournalMapItems != null && _locJournalMapItems.Count() > 0)
                                {
                                    foreach (JournalMap _locJournalMapItem in _locJournalMapItems)
                                    {
                                        XPCollection<JournalMapLine> _locJournalMapLines = new XPCollection<JournalMapLine>
                                                                                           (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                            new BinaryOperator("Company", _locPurchasePrePaymentInvoiceXPO.Company),
                                                                                            new BinaryOperator("Workplace", _locPurchasePrePaymentInvoiceXPO.Workplace),
                                                                                            new BinaryOperator("JournalMap", _locJournalMapItem),
                                                                                            new BinaryOperator("Active", true)));

                                        if (_locJournalMapLines != null && _locJournalMapLines.Count() > 0)
                                        {
                                            foreach (JournalMapLine _locJournalMapLine in _locJournalMapLines)
                                            {
                                                if (_locJournalMapLine.AccountMap != null)
                                                {
                                                    #region NormalAmount
                                                    if (_locJournalMapLine.AccountMap.PostingType == PostingType.Purchase && _locJournalMapLine.AccountMap.OrderType == OrderType.Item
                                                        && _locJournalMapLine.AccountMap.PostingMethod == PostingMethod.PrePayment && _locJournalMapLine.AccountMap.PostingMethodType == PostingMethodType.DownPayment)
                                                    {
                                                        XPCollection<AccountMapLine> _locAccountMapLines = new XPCollection<AccountMapLine>
                                                                                                       (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                        new BinaryOperator("Company", _locPurchasePrePaymentInvoiceXPO.Company),
                                                                                                        new BinaryOperator("Workplace", _locPurchasePrePaymentInvoiceXPO.Workplace),
                                                                                                        new BinaryOperator("AccountMap", _locJournalMapLine.AccountMap),
                                                                                                        new BinaryOperator("Active", true)));

                                                        if (_locAccountMapLines != null && _locAccountMapLines.Count() > 0)
                                                        {
                                                            double _locTotalAmountDebit = 0;
                                                            double _locTotalAmountCredit = 0;
                                                            double _locTotalBalance = 0;

                                                            foreach (AccountMapLine _locAccountMapLine in _locAccountMapLines)
                                                            {
                                                                if (_locAccountMapLine.AccountCharge == AccountCharge.Debit)
                                                                {
                                                                    _locTotalAmountDebit = _locDP;
                                                                }
                                                                if (_locAccountMapLine.AccountCharge == AccountCharge.Credit)
                                                                {
                                                                    _locTotalAmountCredit = _locDP;
                                                                }

                                                                GeneralJournal _saveGeneralJournal = new GeneralJournal(_currSession)
                                                                {
                                                                    Company = _locPurchasePrePaymentInvoiceXPO.Company,
                                                                    Workplace = _locPurchasePrePaymentInvoiceXPO.Workplace,
                                                                    PostingDate = now,
                                                                    PostingType = PostingType.Purchase,
                                                                    OrderType = OrderType.Item,
                                                                    PostingMethod = PostingMethod.PrePayment,
                                                                    PostingMethodType = PostingMethodType.DownPayment,
                                                                    Account = _locAccountMapLine.Account,
                                                                    Debit = _locTotalAmountDebit,
                                                                    Credit = _locTotalAmountCredit,
                                                                    PurchaseOrder = _locPurchasePrePaymentInvoiceXPO.PurchaseOrder,
                                                                    PurchasePrePaymentInvoice = _locPurchasePrePaymentInvoiceXPO,
                                                                    JournalMonth = _locPurchasePrePaymentInvoiceXPO.JournalMonth,
                                                                    JournalYear = _locPurchasePrePaymentInvoiceXPO.JournalYear,
                                                                };

                                                                _saveGeneralJournal.Save();
                                                                _saveGeneralJournal.Session.CommitTransaction();

                                                                #region AccountingPeriodicLine

                                                                if (_locAccountMapLine.Account.Code != null)
                                                                {
                                                                    //Cari Account Periodic Line
                                                                    AccountingPeriodicLine _locAPL = _currSession.FindObject<AccountingPeriodicLine>
                                                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                                                    new BinaryOperator("Company", _locPurchasePrePaymentInvoiceXPO.Company),
                                                                                                    new BinaryOperator("Workplace", _locPurchasePrePaymentInvoiceXPO.Workplace),
                                                                                                    new BinaryOperator("AccountNo", _locAccountMapLine.Account),
                                                                                                    new BinaryOperator("Month", _locPurchasePrePaymentInvoiceXPO.JournalMonth),
                                                                                                    new BinaryOperator("Year", _locPurchasePrePaymentInvoiceXPO.JournalYear)));
                                                                    if (_locAPL != null)
                                                                    {
                                                                        if (_locAPL.AccountNo != null)
                                                                        {
                                                                            if (_locAPL.AccountNo.BalanceType == BalanceType.Change)
                                                                            {
                                                                                if (_locAPL.AccountNo.AccountCharge == AccountCharge.Debit)
                                                                                {
                                                                                    if (_locTotalAmountDebit > 0)
                                                                                    {
                                                                                        _locTotalBalance = _locAPL.Balance + _locTotalAmountDebit;
                                                                                    }
                                                                                    if (_locTotalAmountCredit > 0)
                                                                                    {
                                                                                        _locTotalBalance = _locAPL.Balance - _locTotalAmountCredit;
                                                                                    }
                                                                                }
                                                                                if (_locAPL.AccountNo.AccountCharge == AccountCharge.Credit)
                                                                                {
                                                                                    if (_locTotalAmountDebit > 0)
                                                                                    {
                                                                                        _locTotalBalance = _locAPL.Balance - _locTotalAmountDebit;
                                                                                    }
                                                                                    if (_locTotalAmountCredit > 0)
                                                                                    {
                                                                                        _locTotalBalance = _locAPL.Balance + _locTotalAmountCredit;
                                                                                    }
                                                                                }
                                                                            }

                                                                            _locAPL.Balance = _locTotalBalance;
                                                                            _locAPL.Debit = _locAPL.Debit + _locTotalAmountDebit;
                                                                            _locAPL.Credit = _locAPL.Credit + _locTotalAmountCredit;
                                                                            _locAPL.Save();
                                                                            _locAPL.Session.CommitTransaction();

                                                                        }
                                                                    }
                                                                }
                                                                #endregion AccountingPeriodicLine
                                                            }
                                                        }
                                                    }
                                                    #endregion NormalAmount 
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    #endregion JournalMapCompanyAccountGroup

                    #region JournalMapBusinessPartnerAccountGroup
                    if (_locPurchasePrePaymentInvoiceXPO.PayToVendor != null)
                    {
                        if (_locPurchasePrePaymentInvoiceXPO.PayToVendor.BusinessPartnerAccountGroup != null)
                        {
                            _locBusinessPartnerAccountGroup = _locPurchasePrePaymentInvoiceXPO.PayToVendor.BusinessPartnerAccountGroup;

                            XPCollection<JournalMap> _locJournalMapItems = new XPCollection<JournalMap>
                                                               (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("Company", _locPurchasePrePaymentInvoiceXPO.Company),
                                                                new BinaryOperator("Workplace", _locPurchasePrePaymentInvoiceXPO.Workplace),
                                                                new BinaryOperator("BusinessPartnerAccountGroup", _locBusinessPartnerAccountGroup),
                                                                new BinaryOperator("Active", true)));

                            if (_locJournalMapItems != null && _locJournalMapItems.Count() > 0)
                            {
                                foreach (JournalMap _locJournalMapItem in _locJournalMapItems)
                                {
                                    XPCollection<JournalMapLine> _locJournalMapLines = new XPCollection<JournalMapLine>
                                                                                       (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                        new BinaryOperator("Company", _locPurchasePrePaymentInvoiceXPO.Company),
                                                                                        new BinaryOperator("Workplace", _locPurchasePrePaymentInvoiceXPO.Workplace),
                                                                                        new BinaryOperator("JournalMap", _locJournalMapItem),
                                                                                        new BinaryOperator("Active", true)));

                                    if (_locJournalMapLines != null && _locJournalMapLines.Count() > 0)
                                    {
                                        foreach (JournalMapLine _locJournalMapLine in _locJournalMapLines)
                                        {
                                            if (_locJournalMapLine.AccountMap != null)
                                            {
                                                #region NormalAmount
                                                if (_locJournalMapLine.AccountMap.PostingType == PostingType.Purchase && _locJournalMapLine.AccountMap.OrderType == OrderType.Item
                                                    && _locJournalMapLine.AccountMap.PostingMethod == PostingMethod.PrePayment && _locJournalMapLine.AccountMap.PostingMethodType == PostingMethodType.DownPayment)
                                                {
                                                    XPCollection<AccountMapLine> _locAccountMapLines = new XPCollection<AccountMapLine>
                                                                                                   (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                    new BinaryOperator("Company", _locPurchasePrePaymentInvoiceXPO.Company),
                                                                                                    new BinaryOperator("Workplace", _locPurchasePrePaymentInvoiceXPO.Workplace),
                                                                                                    new BinaryOperator("AccountMap", _locJournalMapLine.AccountMap),
                                                                                                    new BinaryOperator("Active", true)));

                                                    if (_locAccountMapLines != null && _locAccountMapLines.Count() > 0)
                                                    {
                                                        double _locTotalAmountDebit = 0;
                                                        double _locTotalAmountCredit = 0;
                                                        double _locTotalBalance = 0;

                                                        foreach (AccountMapLine _locAccountMapLine in _locAccountMapLines)
                                                        {
                                                            if (_locAccountMapLine.AccountCharge == AccountCharge.Debit)
                                                            {
                                                                _locTotalAmountDebit = _locDP;
                                                            }
                                                            if (_locAccountMapLine.AccountCharge == AccountCharge.Credit)
                                                            {
                                                                _locTotalAmountCredit = _locDP;
                                                            }

                                                            GeneralJournal _saveGeneralJournal = new GeneralJournal(_currSession)
                                                            {
                                                                Company = _locPurchasePrePaymentInvoiceXPO.Company,
                                                                Workplace = _locPurchasePrePaymentInvoiceXPO.Workplace,
                                                                PostingDate = now,
                                                                PostingType = PostingType.Purchase,
                                                                OrderType = OrderType.Item,
                                                                PostingMethod = PostingMethod.PrePayment,
                                                                PostingMethodType = PostingMethodType.DownPayment,
                                                                Account = _locAccountMapLine.Account,
                                                                Debit = _locTotalAmountDebit,
                                                                Credit = _locTotalAmountCredit,
                                                                PurchaseOrder = _locPurchasePrePaymentInvoiceXPO.PurchaseOrder,
                                                                PurchasePrePaymentInvoice = _locPurchasePrePaymentInvoiceXPO,
                                                                JournalMonth = _locPurchasePrePaymentInvoiceXPO.JournalMonth,
                                                                JournalYear = _locPurchasePrePaymentInvoiceXPO.JournalYear,
                                                            };
                                                            _saveGeneralJournal.Save();
                                                            _saveGeneralJournal.Session.CommitTransaction();

                                                            #region AccountingPeriodicLine

                                                            if (_locAccountMapLine.Account.Code != null)
                                                            {
                                                                //Cari Account Periodic Line
                                                                AccountingPeriodicLine _locAPL = _currSession.FindObject<AccountingPeriodicLine>
                                                                                                (new GroupOperator(GroupOperatorType.And,
                                                                                                new BinaryOperator("Company", _locPurchasePrePaymentInvoiceXPO.Company),
                                                                                                new BinaryOperator("Workplace", _locPurchasePrePaymentInvoiceXPO.Workplace),
                                                                                                new BinaryOperator("AccountNo", _locAccountMapLine.Account),
                                                                                                new BinaryOperator("Month", _locPurchasePrePaymentInvoiceXPO.JournalMonth),
                                                                                                new BinaryOperator("Year", _locPurchasePrePaymentInvoiceXPO.JournalYear)));
                                                                if (_locAPL != null)
                                                                {
                                                                    if (_locAPL.AccountNo != null)
                                                                    {
                                                                        if (_locAPL.AccountNo.BalanceType == BalanceType.Change)
                                                                        {
                                                                            if (_locAPL.AccountNo.AccountCharge == AccountCharge.Debit)
                                                                            {
                                                                                if (_locTotalAmountDebit > 0)
                                                                                {
                                                                                    _locTotalBalance = _locAPL.Balance + _locTotalAmountDebit;
                                                                                }
                                                                                if (_locTotalAmountCredit > 0)
                                                                                {
                                                                                    _locTotalBalance = _locAPL.Balance - _locTotalAmountCredit;
                                                                                }
                                                                            }
                                                                            if (_locAPL.AccountNo.AccountCharge == AccountCharge.Credit)
                                                                            {
                                                                                if (_locTotalAmountDebit > 0)
                                                                                {
                                                                                    _locTotalBalance = _locAPL.Balance - _locTotalAmountDebit;
                                                                                }
                                                                                if (_locTotalAmountCredit > 0)
                                                                                {
                                                                                    _locTotalBalance = _locAPL.Balance + _locTotalAmountCredit;
                                                                                }
                                                                            }
                                                                        }

                                                                        _locAPL.Balance = _locTotalBalance;
                                                                        _locAPL.Debit = _locAPL.Debit + _locTotalAmountDebit;
                                                                        _locAPL.Credit = _locAPL.Credit + _locTotalAmountCredit;
                                                                        _locAPL.Save();
                                                                        _locAPL.Session.CommitTransaction();

                                                                    }
                                                                }
                                                            }
                                                            #endregion AccountingPeriodicLine
                                                        }
                                                    }
                                                }
                                                #endregion NormalAmount   
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    #endregion JournalMapBusinessPartnerAccountGroup
                }

                #endregion CreateInvoiceAPJournalDownPayment

            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = PurchasePrePaymentInvoice ", ex.ToString());
            }
        }

        #endregion PrePaymentJournal       

        private void SetFinalPurchasePrePaymentInvoice(Session _currSession, PurchasePrePaymentInvoice _locPurchasePrePaymentInvoiceXPO)
        {
            try
            {
                DateTime now = DateTime.Now;

                if (_locPurchasePrePaymentInvoiceXPO != null)
                {
                    _locPurchasePrePaymentInvoiceXPO.ActivationPosting = true;
                    _locPurchasePrePaymentInvoiceXPO.Status = Status.Posted;
                    _locPurchasePrePaymentInvoiceXPO.StatusDate = now;
                    _locPurchasePrePaymentInvoiceXPO.Save();
                    _locPurchasePrePaymentInvoiceXPO.Session.CommitTransaction();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PurchasePrePaymentInvoice " + ex.ToString());
            }
        }

        #endregion Posting Method

        #region Approval Process

        private void SetApprovalLine(Session _currentSession, PurchasePrePaymentInvoice _locPurchasePrePaymentInvoiceXPO, ApprovalLevel _locApprovalLevel)
        {
            try
            {
                DateTime now = DateTime.Now;

                ApprovalLine _locApprovalLineXPO = _currentSession.FindObject<ApprovalLine>
                                                                        (new GroupOperator(GroupOperatorType.And,
                                                                         new BinaryOperator("PurchasePrePaymentInvoice", _locPurchasePrePaymentInvoiceXPO),
                                                                         new BinaryOperator("ApprovalLevel", _locApprovalLevel)));

                if (_locApprovalLineXPO == null)
                {
                    ApprovalLine _saveDataAL2 = new ApprovalLine(_currentSession)
                    {
                        ApprovalDate = now,
                        ApprovalStatus = Status.Approved,
                        ApprovalLevel = _locApprovalLevel,
                        PurchasePrePaymentInvoice = _locPurchasePrePaymentInvoiceXPO,
                    };
                    _saveDataAL2.Save();
                    _saveDataAL2.Session.CommitTransaction();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PurchasePrePaymentInvoice " + ex.ToString());
            }
        }

        #endregion Approval Process

        #region Email

        private string BackgroundBody(Session _currentSession, PurchasePrePaymentInvoice _locPurchasePrePaymentInvoiceXPO, MailSetupDetail _locMailSetDetail)
        {
            string body = string.Empty;

            PurchasePrePaymentInvoice _locPurchasePrePaymentInvoice = _currentSession.FindObject<PurchasePrePaymentInvoice>
                                           (new GroupOperator(GroupOperatorType.And,
                                            new BinaryOperator("Code", _locPurchasePrePaymentInvoiceXPO.Code)));

            ApprovalLine _locApprovalLineXPO = _currentSession.FindObject<ApprovalLine>
                                               (new GroupOperator(GroupOperatorType.And,
                                                new BinaryOperator("PurchasePrePaymentInvoice", _locPurchasePrePaymentInvoice)));

            string Status = _locApprovalLineXPO.ApprovalStatus.ToString();

            using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/Email/EmailTemplate.html")))
            {
                body = reader.ReadToEnd();
            }
            body = body.Replace("{Code}", _locPurchasePrePaymentInvoiceXPO.Code);

            #region Level
            if (_locPurchasePrePaymentInvoice.ActiveApproved1 == true)
            {
                string Active = "Level 1";
                body = body.Replace("{Level}", Active);
            }
            else if (_locPurchasePrePaymentInvoice.ActiveApproved2 == true)
            {
                string Active = "Level 2";
                body = body.Replace("{Level}", Active);
            }
            else if (_locPurchasePrePaymentInvoice.ActiveApproved3 == true)
            {
                string Active = "Level 3";
                body = body.Replace("{Level}", Active);
            }
            else
            {
                string Active = "Not Available";
                body = body.Replace("{Level}", Active);
            }
            #endregion Level

            body = body.Replace("{Status}", Status);
            body = body.Replace("{Message}", _locMailSetDetail.MessageBody);
            return body;
        }

        protected void SendEmail(Session _currentSession, PurchasePrePaymentInvoice _locPurchasePrePaymentInvoiceXPO, Status _locStatus, ApprovalLine _locApprovalLineXPO2)
        {
            DateTime now = DateTime.Now;
            var User = SecuritySystem.CurrentUserName;
            GlobalFunction _globFunc = new GlobalFunction();
            UserAccess _numLineUserAccess = _currentSession.FindObject<UserAccess>(new BinaryOperator("UserName", User));
            string _locMessageBody = null;

            try
            {
                if (_locPurchasePrePaymentInvoiceXPO != null)
                {
                    XPCollection<MailSetupDetail> _locMailSetDetails = new XPCollection<MailSetupDetail>
                                                                       (_currentSession, new GroupOperator(GroupOperatorType.And,
                                                                        new BinaryOperator("ObjectList", ObjectList.PurchasePrePaymentInvoice),
                                                                        new BinaryOperator("UserAccess", _numLineUserAccess),
                                                                        new BinaryOperator("Active", true)));

                    if (_locMailSetDetails != null && _locMailSetDetails.Count > 0)
                    {
                        foreach (MailSetupDetail _locMailSetDetail in _locMailSetDetails)
                        {
                            if (_locMailSetDetail.MessageBody != null)
                            {
                                if (_locApprovalLineXPO2.EndApproval == true)
                                {
                                    _locMessageBody = this.BackgroundBody(_currentSession, _locPurchasePrePaymentInvoiceXPO, _locMailSetDetail);
                                }
                                else
                                {
                                    _locMessageBody = this.BackgroundBody(_currentSession, _locPurchasePrePaymentInvoiceXPO, _locMailSetDetail);
                                }
                            }

                            _globFunc.SetAndSendMail(_locMailSetDetail.SmtpHost, _locMailSetDetail.SmtpPort,
                            _locMailSetDetail.MailFrom, _locMailSetDetail.MailFromPassword, _locMailSetDetail.MailTo, _locMailSetDetail.MailSubject, _locMessageBody);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PurchasePrePaymentInvoice " + ex.ToString());
            }
        }

        #endregion Email

        #region Global Method

        private void SuccessMessageShow(string _locMessage)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 4000;
            options.Message = string.Format("{0}", _locMessage);
            options.Type = InformationType.Success;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Success";
            options.Win.Type = WinMessageType.Flyout;
            Application.ShowViewStrategy.ShowMessage(options);
        }

        private void ErrorMessageShow(string _locMessage)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 2000;
            options.Message = string.Format(_locMessage);
            options.Type = InformationType.Warning;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Error";
            options.Win.Type = WinMessageType.Alert;
            Application.ShowViewStrategy.ShowMessage(options);
        }

        #endregion Global Method

        
    }
}
