﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;

using FullDrive.Module.BusinessObjects;
using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.Xpo;
using System.Collections;
using DevExpress.Xpo;


namespace FullDrive.Module.Controllers
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class RoutePlanActionController : ViewController
    {
        public RoutePlanActionController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.
        }
        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }
        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        private void RoutePlanSelectAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;

                if (_objectToProcess != null)
                {
                    foreach (Object obj in _objectToProcess)
                    {
                        RoutePlan _locRoutePlanOS = (RoutePlan)_objectSpace.GetObject(obj);

                        if (_locRoutePlanOS != null)
                        {
                            if (_locRoutePlanOS.Session != null)
                            {
                                _currSession = _locRoutePlanOS.Session;
                            }

                            if (_locRoutePlanOS.Code != null && _currSession != null)
                            {
                                _currObjectId = _locRoutePlanOS.Code;

                                XPCollection<RoutePlan> _locRoutePlans = new XPCollection<RoutePlan>
                                                                    (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                        new BinaryOperator("Code", _currObjectId)));

                                if (_locRoutePlans != null && _locRoutePlans.Count > 0)
                                {
                                    foreach (RoutePlan _locRoutePlan in _locRoutePlans)
                                    {
                                        _locRoutePlan.Select = true;
                                        _locRoutePlan.Save();
                                        _locRoutePlan.Session.CommitTransaction();
                                    }

                                    SuccessMessageShow("RoutePlan has been Select");
                                }
                                else
                                {
                                    ErrorMessageShow("Data RoutePlan Not Available");
                                }
                            }
                        }
                    }
                    if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                    {
                        _objectSpace.CommitChanges();
                        _objectSpace.Refresh();
                    }
                    if (View is ListView)
                    {
                        _objectSpace.CommitChanges();
                        View.ObjectSpace.Refresh();
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("BusinessObject = RoutePlan" + ex.ToString());
            }
        }

        private void RoutePlanUnselectAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;

                if (_objectToProcess != null)
                {
                    foreach (Object obj in _objectToProcess)
                    {
                        RoutePlan _locRoutePlanOS = (RoutePlan)_objectSpace.GetObject(obj);

                        if (_locRoutePlanOS != null)
                        {
                            if (_locRoutePlanOS.Session != null)
                            {
                                _currSession = _locRoutePlanOS.Session;
                            }

                            if (_locRoutePlanOS.Code != null && _currSession != null)
                            {
                                _currObjectId = _locRoutePlanOS.Code;

                                XPCollection<RoutePlan> _locRoutePlans = new XPCollection<RoutePlan>
                                                                    (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                        new BinaryOperator("Code", _currObjectId)));

                                if (_locRoutePlans != null && _locRoutePlans.Count > 0)
                                {
                                    foreach (RoutePlan _locRoutePlan in _locRoutePlans)
                                    {
                                        _locRoutePlan.Select = false;
                                        _locRoutePlan.Save();
                                        _locRoutePlan.Session.CommitTransaction();
                                    }

                                    SuccessMessageShow("RoutePlan has been Unselect");
                                }
                                else
                                {
                                    ErrorMessageShow("Data RoutePlan Not Available");
                                }
                            }
                        }
                    }
                    if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                    {
                        _objectSpace.CommitChanges();
                        _objectSpace.Refresh();
                    }
                    if (View is ListView)
                    {
                        _objectSpace.CommitChanges();
                        View.ObjectSpace.Refresh();
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("BusinessObject = RoutePlan" + ex.ToString());
            }
        }

        //======================================== Code In Here ========================================

        #region Global Method

        private void SuccessMessageShow(string _locActionName)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 2000;
            options.Message = string.Format("{0}", _locActionName);
            options.Type = InformationType.Success;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Success";
            options.Win.Type = WinMessageType.Flyout;
            Application.ShowViewStrategy.ShowMessage(options);
        }

        private void ErrorMessageShow(string _locMessage)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 2000;
            options.Message = string.Format(_locMessage);
            options.Type = InformationType.Warning;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Error";
            options.Win.Type = WinMessageType.Alert;
            Application.ShowViewStrategy.ShowMessage(options);
        }


        #endregion Global Method
    }
}
