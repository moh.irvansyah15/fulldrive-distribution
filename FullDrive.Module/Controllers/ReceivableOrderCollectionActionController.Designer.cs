﻿namespace FullDrive.Module.Controllers
{
    partial class ReceivableOrderCollectionActionController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.ReceivableOrderCollectionShowOCMAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            // 
            // ReceivableOrderCollectionShowOCMAction
            // 
            this.ReceivableOrderCollectionShowOCMAction.Caption = "Show OCM";
            this.ReceivableOrderCollectionShowOCMAction.ConfirmationMessage = null;
            this.ReceivableOrderCollectionShowOCMAction.Id = "ReceivableOrderCollectionShowOCMActionId";
            this.ReceivableOrderCollectionShowOCMAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.ReceivableOrderCollection);
            this.ReceivableOrderCollectionShowOCMAction.ToolTip = null;
            this.ReceivableOrderCollectionShowOCMAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.ReceivableOrderCollectionShowOCMAction_Execute);
            // 
            // ReceivableOrderCollectionActionController
            // 
            this.Actions.Add(this.ReceivableOrderCollectionShowOCMAction);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SimpleAction ReceivableOrderCollectionShowOCMAction;
    }
}
