﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;

using FullDrive.Module.BusinessObjects;
using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.Xpo;
using System.Collections;
using DevExpress.Xpo;
using System.IO;
using System.Web;

namespace FullDrive.Module.Controllers
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class ReceivableTransactionActionController : ViewController
    {
        private ChoiceActionItem _selectionListviewFilter;

        public ReceivableTransactionActionController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.
            #region Filter Status
            ReceivableTransactionListviewFilterSelectionAction.Items.Clear();
            foreach (object _currApproval in Enum.GetValues(typeof(CustomProcess.Status)))
            {
                EnumDescriptor _ed = new EnumDescriptor(typeof(CustomProcess.Status));
                _selectionListviewFilter = new ChoiceActionItem(_ed.GetCaption(_currApproval), _currApproval);
                ReceivableTransactionListviewFilterSelectionAction.Items.Add(_selectionListviewFilter);
            }
            #endregion Filter Status
        }

        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
        }

        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }

        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        private void ReceivableTransactionGetOCMAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;

                if (_objectsToProcess != null)
                {
                    foreach (Object obj in _objectsToProcess)
                    {
                        ReceivableTransaction _locReceivableTransactionOS = (ReceivableTransaction)_objectSpace.GetObject(obj);

                        if (_locReceivableTransactionOS != null)
                        {
                            if (_locReceivableTransactionOS.Session != null)
                            {
                                _currSession = _locReceivableTransactionOS.Session;
                            }

                            if (_locReceivableTransactionOS.Code != null && _currSession != null)
                            {
                                _currObjectId = _locReceivableTransactionOS.Code;

                                ReceivableTransaction _locReceivableTransactionXPO = _currSession.FindObject<ReceivableTransaction>
                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                     new BinaryOperator("Code", _currObjectId)));

                                if (_locReceivableTransactionXPO != null)
                                {
                                    if (_locReceivableTransactionXPO.Status == Status.Open || _locReceivableTransactionXPO.Status == Status.Progress)
                                    {
                                        XPCollection<ReceivableOrderCollection> _locReceivableOrderCollections = new XPCollection<ReceivableOrderCollection>
                                                                                        (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                        new BinaryOperator("ReceivableTransaction", _locReceivableTransactionXPO),
                                                                                        new GroupOperator(GroupOperatorType.Or,
                                                                                        new BinaryOperator("Status", Status.Open),
                                                                                        new BinaryOperator("Status", Status.Progress))));

                                        if (_locReceivableOrderCollections != null && _locReceivableOrderCollections.Count() > 0)
                                        {
                                            foreach (ReceivableOrderCollection _locReceivableOrderCollection in _locReceivableOrderCollections)
                                            {
                                                if (_locReceivableOrderCollection.ReceivableTransaction != null)
                                                {
                                                    GetOrderCollectionMonitoring(_currSession, _locReceivableOrderCollection.OrderCollection, _locReceivableTransactionXPO);
                                                }
                                            }
                                            SuccessMessageShow("Order Collection Has Been Successfully Getting into Receivable Transaction");
                                        }
                                    }
                                }
                                else
                                {
                                    ErrorMessageShow("Data Receivable Transaction Not Available");
                                }
                            }
                            else
                            {
                                ErrorMessageShow("Data Receivable Transaction Not Available");
                            }
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = ReceivableTransaction " + ex.ToString());
            }
        }

        private void ReceivableTransactionProgressAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;
                double _locTotalAmount = 0;
                double _locTotalAmountCN = 0;
                double _locTotalAmountDN = 0;
                double _locTotalPlan = 0;

                if (_objectsToProcess != null)
                {
                    foreach (Object obj in _objectsToProcess)
                    {
                        ReceivableTransaction _locReceivableTransactionOS = (ReceivableTransaction)_objectSpace.GetObject(obj);

                        if (_locReceivableTransactionOS != null)
                        {
                            if (_locReceivableTransactionOS.Session != null)
                            {
                                _currSession = _locReceivableTransactionOS.Session;
                            }

                            if (_locReceivableTransactionOS.Code != null && _currSession != null)
                            {
                                _currObjectId = _locReceivableTransactionOS.Code;

                                ReceivableTransaction _locReceivableTransactionXPO = _currSession.FindObject<ReceivableTransaction>
                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                     new BinaryOperator("Code", _currObjectId),
                                                                     new GroupOperator(GroupOperatorType.Or,
                                                                     new BinaryOperator("Status", Status.Open),
                                                                     new BinaryOperator("Status", Status.Progress))));

                                if (_locReceivableTransactionXPO != null)
                                {
                                    if (_locReceivableTransactionXPO.Status == Status.Open)
                                    {
                                        _locReceivableTransactionXPO.Status = Status.Progress;
                                        _locReceivableTransactionXPO.StatusDate = now;
                                        _locReceivableTransactionXPO.Save();
                                        _locReceivableTransactionXPO.Session.CommitTransaction();
                                    }

                                    XPCollection<ReceivableTransactionLine> _locReceivableTransactionLines = new XPCollection<ReceivableTransactionLine>
                                                           (_currSession, new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("ReceivableTransaction", _locReceivableTransactionXPO),
                                                            new BinaryOperator("Status", Status.Open)));

                                    if (_locReceivableTransactionLines != null && _locReceivableTransactionLines.Count > 0)
                                    {
                                        foreach (ReceivableTransactionLine _locReceivableTransactionLine in _locReceivableTransactionLines)
                                        {
                                            _locTotalAmount = _locTotalAmount + _locReceivableTransactionLine.Amount;
                                            _locTotalAmountCN = _locTotalAmountCN + _locReceivableTransactionLine.AmountCN;
                                            _locTotalAmountDN = _locTotalAmountDN + _locReceivableTransactionLine.AmountDN;
                                            _locTotalPlan = _locTotalPlan + _locReceivableTransactionLine.Plan;
                                            _locReceivableTransactionLine.Status = Status.Progress;
                                            _locReceivableTransactionLine.StatusDate = now;
                                            _locReceivableTransactionLine.Save();
                                            _locReceivableTransactionLine.Session.CommitTransaction();
                                        }
                                    }

                                    _locReceivableTransactionXPO.GrandTotalAmount = _locTotalAmount;
                                    _locReceivableTransactionXPO.GrandTotalAmountCN = _locTotalAmountCN;
                                    _locReceivableTransactionXPO.GrandTotalAmountDN = _locTotalAmountDN;
                                    _locReceivableTransactionXPO.GrandTotalPlan = _locTotalPlan;
                                    _locReceivableTransactionXPO.Status = Status.Progress;
                                    _locReceivableTransactionXPO.StatusDate = now;
                                    _locReceivableTransactionXPO.Save();
                                    _locReceivableTransactionXPO.Session.CommitTransaction();

                                    XPCollection<ReceivableOrderCollection> _locReceivableOrderCollections = new XPCollection<ReceivableOrderCollection>
                                                           (_currSession, new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("ReceivableTransaction", _locReceivableTransactionXPO),
                                                            new BinaryOperator("Status", Status.Open)));

                                    if (_locReceivableOrderCollections != null && _locReceivableOrderCollections.Count > 0)
                                    {
                                        foreach (ReceivableOrderCollection _locReceivableOrderCollection in _locReceivableOrderCollections)
                                        {
                                            _locReceivableOrderCollection.Status = Status.Progress;
                                            _locReceivableOrderCollection.StatusDate = now;
                                            _locReceivableOrderCollection.Save();
                                            _locReceivableOrderCollection.Session.CommitTransaction();
                                        }
                                    }
                                    SuccessMessageShow(_locReceivableTransactionXPO.Code + "has been successfully updated to progress");
                                }
                                else
                                {
                                    ErrorMessageShow("Data Receivable Transaction Not Available");
                                }
                            }
                            else
                            {
                                ErrorMessageShow("Data Receivable Transaction Not Available");
                            }
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = ReceivableTransaction " + ex.ToString());
            }
        }

        private void ReceivableTransactionPostingAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;

                if (_objectsToProcess != null)
                {
                    foreach (Object obj in _objectsToProcess)
                    {
                        ReceivableTransaction _locReceivableTransactionOS = (ReceivableTransaction)_objectSpace.GetObject(obj);

                        if (_locReceivableTransactionOS != null)
                        {
                            if (_locReceivableTransactionOS.Session != null)
                            {
                                _currSession = _locReceivableTransactionOS.Session;
                            }

                            if (_locReceivableTransactionOS.Code != null && _currSession != null)
                            {
                                _currObjectId = _locReceivableTransactionOS.Code;

                                ReceivableTransaction _locReceivableTransactionXPO = _currSession.FindObject<ReceivableTransaction>
                                                                   (new GroupOperator(GroupOperatorType.And,
                                                                    new BinaryOperator("Code", _currObjectId),
                                                                    new GroupOperator(GroupOperatorType.Or,
                                                                    new BinaryOperator("Status", Status.Progress),
                                                                    new BinaryOperator("Status", Status.Posted))));

                                if (_locReceivableTransactionXPO != null)
                                {
                                    ReleaseCreditLimitAmount(_currSession, _locReceivableTransactionXPO);
                                    SetOutstanding(_currSession, _locReceivableTransactionXPO);
                                    SetActualAndOverPayment(_currSession, _locReceivableTransactionXPO);
                                    SetPostedCount(_currSession, _locReceivableTransactionXPO);
                                    SetReceivableTransactionMonitoring(_currSession, _locReceivableTransactionXPO);
                                    if (CheckJournalSetup(_currSession, _locReceivableTransactionXPO) == true)
                                    {
                                        SetReceivableJournal(_currSession, _locReceivableTransactionXPO);
                                    }
                                    SetStatusReceivableTransactionLine(_currSession, _locReceivableTransactionXPO);
                                    SetNormal(_currSession, _locReceivableTransactionXPO);
                                    SetFinalStatusReceivableTransaction(_currSession, _locReceivableTransactionXPO);
                                    SuccessMessageShow(_locReceivableTransactionXPO.Code + " has been change successfully to Posted");
                                }
                                else
                                {
                                    ErrorMessageShow("Data Receivable Transaction Not Available");
                                }
                            }
                            else
                            {
                                ErrorMessageShow("Data Receivable Transaction Not Available");
                            }
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = ReceivableTransaction " + ex.ToString());
            }
        }

        private void ReceivableTransactionListviewFilterSelectionAction_Execute(object sender, SingleChoiceActionExecuteEventArgs e)
        {
            try
            {
                if ((View is ListView) & (View.ObjectTypeInfo.Type == typeof(ReceivableTransaction)))
                {
                    if ((Status)e.SelectedChoiceActionItem.Data == Status.None)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.None, BinaryOperatorType.NotEqual);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Open)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Open, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Progress)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Progress, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Posted)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Posted, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Close)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Close, BinaryOperatorType.Equal);
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = ReceivableTransaction " + ex.ToString());
            }
        }

        //=============================================== Code In Here =============================================

        #region GetOrderCollection

        private void GetOrderCollectionMonitoring(Session _currSession, OrderCollection _locOrderCollectionXPO, ReceivableTransaction _locReceivableTransactionXPO)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                DateTime now = DateTime.Now;
                string _currSignCode = null;

                SalesInvoice _locSalesInvoice = null;
                BankAccount _locCompanyBankAccount = null;
                string _locCompanyAccountName = null;
                string _locCompanyAccountNo = null;
                BankAccount _locBankAccount = null;
                string _locAccountName = null;
                string _locAccountNo = null;

                if (_locOrderCollectionXPO != null && _locReceivableTransactionXPO != null)
                {
                    #region OrderCollectionMonitoring
                    //Hanya memindahkan OrderCollectionMonitoring ke ReceivableTransactionLine
                    XPCollection<OrderCollectionMonitoring> _locOrderCollectionMonitorings = new XPCollection<OrderCollectionMonitoring>
                                                                                        (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                        new BinaryOperator("OrderCollection", _locOrderCollectionXPO),
                                                                                        new BinaryOperator("ReceivableTransaction", _locReceivableTransactionXPO),
                                                                                        new BinaryOperator("Select", true),
                                                                                        new BinaryOperator("Status", Status.Close, BinaryOperatorType.NotEqual)));

                    if (_locOrderCollectionMonitorings != null && _locOrderCollectionMonitorings.Count() > 0)
                    {
                        foreach (OrderCollectionMonitoring _locOrderCollectionMonitoring in _locOrderCollectionMonitorings)
                        {
                            if (_locOrderCollectionMonitoring.OrderCollectionLine != null)
                            {
                                _currSignCode = _globFunc.GetNumberingSignUnlockOptimisticRecordBasedOrganization(_currSession.DataLayer, ObjectList.ReceivableTransactionLine, _locReceivableTransactionXPO.Company, _locReceivableTransactionXPO.Workplace);
                                //_currSignCode = _globFunc.GetNumberingSignUnlockOptimisticRecord(_currSession.DataLayer, ObjectList.ReceivableTransactionLine);

                                if (_currSignCode != null)
                                {
                                    #region SalesInvoice
                                    if (_locOrderCollectionMonitoring.SalesInvoice != null)
                                    {
                                        _locSalesInvoice = _locOrderCollectionMonitoring.SalesInvoice;
                                        #region Bank
                                        if (_locOrderCollectionMonitoring.SalesInvoice.CompanyBankAccount != null)
                                        {
                                            _locCompanyBankAccount = _locOrderCollectionMonitoring.SalesInvoice.CompanyBankAccount;
                                        }
                                        if (_locOrderCollectionMonitoring.SalesInvoice.CompanyAccountName != null)
                                        {
                                            _locCompanyAccountName = _locOrderCollectionMonitoring.SalesInvoice.CompanyAccountName;
                                        }
                                        if (_locOrderCollectionMonitoring.SalesInvoice.CompanyAccountNo != null)
                                        {
                                            _locCompanyAccountNo = _locOrderCollectionMonitoring.SalesInvoice.CompanyAccountNo;
                                        }
                                        if (_locOrderCollectionMonitoring.SalesInvoice.BankAccount != null)
                                        {
                                            _locBankAccount = _locOrderCollectionMonitoring.SalesInvoice.BankAccount;
                                        }
                                        if (_locOrderCollectionMonitoring.SalesInvoice.AccountName != null)
                                        {
                                            _locAccountName = _locOrderCollectionMonitoring.SalesInvoice.AccountName;
                                        }
                                        if (_locOrderCollectionMonitoring.SalesInvoice.AccountNo != null)
                                        {
                                            _locAccountNo = _locOrderCollectionMonitoring.SalesInvoice.AccountNo;
                                        }
                                        #endregion Bank
                                    }
                                    #endregion SalesInvoice
                                    #region ReceivableTransactionLine
                                    ReceivableTransactionLine _saveReceivableTransactionLine = new ReceivableTransactionLine(_currSession)
                                    {
                                        SignCode = _currSignCode,
                                        ReceivableTransaction = _locReceivableTransactionXPO,
                                        #region Organization
                                        Company = _locReceivableTransactionXPO.Company,
                                        Workplace = _locReceivableTransactionXPO.Workplace,
                                        Division = _locReceivableTransactionXPO.Division,
                                        Department = _locReceivableTransactionXPO.Department,
                                        Section = _locReceivableTransactionXPO.Section,
                                        Employee = _locOrderCollectionMonitoring.Employee,
                                        Div = _locOrderCollectionMonitoring.Div,
                                        Dept = _locOrderCollectionMonitoring.Dept,
                                        Sect = _locOrderCollectionMonitoring.Sect,
                                        PIC = _locOrderCollectionMonitoring.PIC,
                                        Collector = _locOrderCollectionMonitoring.Collector,
                                        #endregion Organization
                                        Vehicle = _locOrderCollectionMonitoring.Vehicle,
                                        #region Bank
                                        CompanyBankAccount = _locCompanyBankAccount,
                                        CompanyAccountName = _locCompanyAccountName,
                                        CompanyAccountNo = _locCompanyAccountNo,
                                        BankAccount = _locBankAccount,
                                        AccountName = _locAccountName,
                                        AccountNo = _locAccountNo,
                                        #endregion Bank
                                        Amount = _locOrderCollectionMonitoring.AmountColl,
                                        AmountDN = _locOrderCollectionMonitoring.AmountDN,
                                        AmountCN = _locOrderCollectionMonitoring.AmountCN,
                                        Plan = _locOrderCollectionMonitoring.AmountColl,
                                        PickingDate = _locOrderCollectionMonitoring.PickingDate,
                                        Picking = _locOrderCollectionMonitoring.Picking,
                                        PickingMonitoring = _locOrderCollectionMonitoring.PickingMonitoring,
                                        OrderCollectionMonitoring = _locOrderCollectionMonitoring,
                                        DocDate = _locOrderCollectionMonitoring.SalesInvoice.DocDate,
                                        JournalMonth = _locOrderCollectionMonitoring.SalesInvoice.JournalMonth,
                                        JournalYear = _locOrderCollectionMonitoring.SalesInvoice.JournalYear,
                                    };
                                    _saveReceivableTransactionLine.Save();
                                    _saveReceivableTransactionLine.Session.CommitTransaction();
                                    #endregion ReceivableTransactionLine

                                    ReceivableTransactionLine _locReceivableTransactionLine = _currSession.FindObject<ReceivableTransactionLine>
                                                                (new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("SignCode", _currSignCode)));
                                    if (_locReceivableTransactionLine != null)
                                    {
                                        SetProcessCount(_currSession, _locOrderCollectionMonitoring);
                                        SetStatusOrderCollectionMonitoring(_currSession, _locOrderCollectionMonitoring);
                                        SetNormal(_currSession, _locOrderCollectionMonitoring);
                                    }
                                }
                            }
                        }
                    }
                    #endregion OrderCollectionMonitoring
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = ReceivableTransaction ", ex.ToString());
            }
        }

        private void SetProcessCount(Session _currSession, OrderCollectionMonitoring _locOrderCollectionMonitoringXPO)
        {
            try
            {
                if (_locOrderCollectionMonitoringXPO != null)
                {
                    _locOrderCollectionMonitoringXPO.PostedCount = _locOrderCollectionMonitoringXPO.PostedCount + 1;
                    _locOrderCollectionMonitoringXPO.Save();
                    _locOrderCollectionMonitoringXPO.Session.CommitTransaction();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = ReceivableTransaction " + ex.ToString());
            }
        }

        private void SetStatusOrderCollectionMonitoring(Session _currSession, OrderCollectionMonitoring _locOrderCollectionMonitoringXPO)
        {
            try
            {
                DateTime now = DateTime.Now;
                if (_locOrderCollectionMonitoringXPO != null)
                {
                    _locOrderCollectionMonitoringXPO.Status = Status.Close;
                    _locOrderCollectionMonitoringXPO.ActivationPosting = true;
                    _locOrderCollectionMonitoringXPO.StatusDate = now;
                    _locOrderCollectionMonitoringXPO.Save();
                    _locOrderCollectionMonitoringXPO.Session.CommitTransaction();             
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = ReceivableTransaction " + ex.ToString());
            }
        }

        private void SetNormal(Session _currSession, OrderCollectionMonitoring _locOrderCollectionMonitoringXPO)
        {
            try
            {
                if (_locOrderCollectionMonitoringXPO != null)
                {
                    _locOrderCollectionMonitoringXPO.Select = false;
                    _locOrderCollectionMonitoringXPO.Save();
                    _locOrderCollectionMonitoringXPO.Session.CommitTransaction();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = ReceivableTransaction " + ex.ToString());
            }
        }

        #endregion GetOrderCollection

        #region Posting

        private void SetOutstanding(Session _currSession, ReceivableTransaction _locReceivableTransactionXPO)
        {
            try
            {
                if (_locReceivableTransactionXPO != null)
                {
                    XPCollection<ReceivableTransactionLine> _locReceivableTransactionLines = new XPCollection<ReceivableTransactionLine>(_currSession,
                                                            new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("ReceivableTransaction", _locReceivableTransactionXPO),
                                                            new BinaryOperator("Select", true),
                                                            new GroupOperator(GroupOperatorType.Or,
                                                            new BinaryOperator("Status", Status.Progress),
                                                            new BinaryOperator("Status", Status.Posted))));

                    if (_locReceivableTransactionLines != null && _locReceivableTransactionLines.Count > 0)
                    {
                        double _locOutstanding = 0;

                        foreach (ReceivableTransactionLine _locReceivableTransactionLine in _locReceivableTransactionLines)
                        {
                            #region ProcessCount=0
                            if (_locReceivableTransactionLine.PostedCount == 0)
                            {
                                if (_locReceivableTransactionLine.Plan > 0)
                                {
                                    if (_locReceivableTransactionLine.Amount > 0 && _locReceivableTransactionLine.Amount <= _locReceivableTransactionLine.Plan)
                                    {
                                        _locOutstanding = _locReceivableTransactionLine.Plan - _locReceivableTransactionLine.Amount;
                                    }
                                }
                                else
                                {
                                    if (_locReceivableTransactionLine.Amount > 0)
                                    {
                                        _locOutstanding = 0;
                                    }
                                }
                            }
                            #endregion ProcessCount=0

                            #region ProcessCount>0
                            if (_locReceivableTransactionLine.PostedCount > 0)
                            {
                                if (_locReceivableTransactionLine.Outstanding > 0)
                                {
                                    _locOutstanding = _locReceivableTransactionLine.Outstanding - _locReceivableTransactionLine.Amount;
                                }
                            }
                            #endregion ProcessCount>0

                            _locReceivableTransactionLine.Outstanding = _locOutstanding;
                            _locReceivableTransactionLine.Save();
                            _locReceivableTransactionLine.Session.CommitTransaction();
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = ReceivableTransaction " + ex.ToString());
            }
        }

        private void SetActualAndOverPayment(Session _currSession, ReceivableTransaction _locReceivableTransactionXPO)
        {
            try
            {
                if (_locReceivableTransactionXPO != null)
                {
                    XPCollection<ReceivableTransactionLine> _locReceivableTransactionLines = new XPCollection<ReceivableTransactionLine>(_currSession,
                                                            new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("ReceivableTransaction", _locReceivableTransactionXPO),
                                                            new BinaryOperator("Select", true),
                                                            new GroupOperator(GroupOperatorType.Or,
                                                            new BinaryOperator("Status", Status.Progress),
                                                            new BinaryOperator("Status", Status.Posted))));

                    if (_locReceivableTransactionLines != null && _locReceivableTransactionLines.Count > 0)
                    {
                        double _locActual = 0;
                        double _locOverPayment = 0;
                        foreach (ReceivableTransactionLine _locReceivableTransactionLine in _locReceivableTransactionLines)
                        {
                            #region ProcessCount=0
                            if (_locReceivableTransactionLine.PostedCount == 0)
                            {
                                if (_locReceivableTransactionLine.Plan > 0)
                                {
                                    if (_locReceivableTransactionLine.Amount > 0 )
                                    {
                                        if(_locReceivableTransactionLine.Amount <= _locReceivableTransactionLine.Plan)
                                        {
                                            _locActual = _locReceivableTransactionLine.Amount;
                                        }else
                                        {
                                            _locActual = _locReceivableTransactionLine.Plan;
                                            _locOverPayment = _locReceivableTransactionLine.Amount - _locReceivableTransactionLine.Plan;
                                        } 
                                    }
                                }
                            }
                            #endregion ProcessCount=0

                            #region ProcessCount>0
                            if (_locReceivableTransactionLine.PostedCount > 0)
                            {
                                if (_locReceivableTransactionLine.Actual > 0)
                                {
                                    _locActual = _locReceivableTransactionLine.Actual + _locReceivableTransactionLine.Amount;
                                    if(_locActual > _locReceivableTransactionLine.Plan)
                                    {
                                        _locOverPayment = _locActual - _locReceivableTransactionLine.Plan;
                                        _locActual = _locReceivableTransactionLine.Plan;
                                    }
                                }
                            }
                            #endregion ProcessCount>0

                            _locReceivableTransactionLine.Actual = _locActual;
                            _locReceivableTransactionLine.OverPayment = _locOverPayment;
                            _locReceivableTransactionLine.Save();
                            _locReceivableTransactionLine.Session.CommitTransaction();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = ReceivableTransaction " + ex.ToString());
            }
        }

        private void ReleaseCreditLimitAmount(Session _currSession, ReceivableTransaction _locReceivableTransactionXPO)
        {
            try
            {
                if (_locReceivableTransactionXPO != null)
                {
                    XPCollection<ReceivableTransactionLine> _locReceivableTransactionLines = new XPCollection<ReceivableTransactionLine>(_currSession,
                                                            new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("ReceivableTransaction", _locReceivableTransactionXPO),
                                                            new BinaryOperator("Select", true),
                                                            new GroupOperator(GroupOperatorType.Or,
                                                            new BinaryOperator("Status", Status.Progress),
                                                            new BinaryOperator("Status", Status.Posted))));

                    if (_locReceivableTransactionLines != null && _locReceivableTransactionLines.Count > 0)
                    {
                        
                        foreach (ReceivableTransactionLine _locReceivableTransactionLine in _locReceivableTransactionLines)
                        {
                            if(_locReceivableTransactionLine.SalesInvoice != null)
                            {
                                double _locCurrCredit = 0;
                                if(_locReceivableTransactionLine.SalesInvoice.CreditLimitList != null)
                                {
                                    _locCurrCredit = _locReceivableTransactionLine.SalesInvoice.CreditLimitList.CurrCredit - _locReceivableTransactionLine.Amount;
                                    if (_locCurrCredit < 0)
                                    {
                                        _locCurrCredit = 0;
                                    }
                                    _locReceivableTransactionLine.SalesInvoice.CreditLimitList.CurrCredit = _locCurrCredit;
                                    _locReceivableTransactionLine.SalesInvoice.CreditLimitList.Save();
                                    _locReceivableTransactionLine.SalesInvoice.CreditLimitList.Session.CommitTransaction();
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = ReceivableTransaction " + ex.ToString());
            }
        }

        private void SetPostedCount(Session _currSession, ReceivableTransaction _locReceivableTransactionXPO)
        {
            try
            {
                if (_locReceivableTransactionXPO != null)
                {
                    XPCollection<ReceivableTransactionLine> _locReceivableTransactionLines = new XPCollection<ReceivableTransactionLine>(_currSession,
                                                                        new GroupOperator(GroupOperatorType.And,
                                                                        new BinaryOperator("ReceivableTransaction", _locReceivableTransactionXPO),
                                                                        new BinaryOperator("Select", true),
                                                                        new GroupOperator(GroupOperatorType.Or,
                                                                        new BinaryOperator("Status", Status.Progress),
                                                                        new BinaryOperator("Status", Status.Posted))));

                    if (_locReceivableTransactionLines != null && _locReceivableTransactionLines.Count() > 0)
                    {
                        foreach (ReceivableTransactionLine _locReceivableTransactionLine in _locReceivableTransactionLines)
                        {
                            if(_locReceivableTransactionLine.PostedCount == 0)
                            {
                                _locReceivableTransactionLine.OpenCN = true;
                                _locReceivableTransactionLine.OpenDN = true;
                            }
                            _locReceivableTransactionLine.PostedCount = _locReceivableTransactionLine.PostedCount + 1;
                            _locReceivableTransactionLine.Save();
                            _locReceivableTransactionLine.Session.CommitTransaction();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = ReceivableTransaction " + ex.ToString());
            }
        }

        private void SetReceivableTransactionMonitoring(Session _currSession, ReceivableTransaction _locReceivableTransactionXPO)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                DateTime now = DateTime.Now;

                BusinessPartner _locSalesToCustomer = null;
                string _locSalesToAddress = null;
                string _locSalesToContact = null;
                City _locSalesToCity = null;
                Country _locSalesToCountry = null;
                BusinessPartner _locBillToCustomer = null;
                string _locBillToAddress = null;
                string _locBillToContact = null;
                City _locBillToCity = null;
                Country _locBillToCountry = null;


                if (_locReceivableTransactionXPO != null)
                {
                    XPCollection<ReceivableTransactionLine> _locReceivableTransactionLines = new XPCollection<ReceivableTransactionLine>(_currSession,
                                                                        new GroupOperator(GroupOperatorType.And,
                                                                        new BinaryOperator("ReceivableTransaction", _locReceivableTransactionXPO),
                                                                        new BinaryOperator("Select", true),
                                                                        new GroupOperator(GroupOperatorType.Or,
                                                                        new BinaryOperator("Status", Status.Progress),
                                                                        new BinaryOperator("Status", Status.Posted))));

                    if (_locReceivableTransactionLines != null && _locReceivableTransactionLines.Count() > 0)
                    {
                        foreach (ReceivableTransactionLine _locReceivableTransactionLine in _locReceivableTransactionLines)
                        {
                            #region SalesInvoice
                            if (_locReceivableTransactionLine.SalesInvoice != null)
                            {
                                #region SalesInvoiceInfo
                                if (_locReceivableTransactionLine.SalesInvoice.SalesToCustomer != null)
                                {
                                    _locSalesToCustomer = _locReceivableTransactionLine.SalesInvoice.SalesToCustomer;
                                }
                                if (_locReceivableTransactionLine.SalesInvoice.SalesToAddress != null)
                                {
                                    _locSalesToAddress = _locReceivableTransactionLine.SalesInvoice.SalesToAddress;
                                }
                                if (_locReceivableTransactionLine.SalesInvoice.SalesToContact != null)
                                {
                                    _locSalesToContact = _locReceivableTransactionLine.SalesInvoice.SalesToContact;
                                }
                                if (_locReceivableTransactionLine.SalesInvoice.SalesToCity != null)
                                {
                                    _locSalesToCity = _locReceivableTransactionLine.SalesInvoice.SalesToCity;
                                }
                                if (_locReceivableTransactionLine.SalesInvoice.SalesToCountry != null)
                                {
                                    _locSalesToCountry = _locReceivableTransactionLine.SalesInvoice.SalesToCountry;
                                }
                                if (_locReceivableTransactionLine.SalesInvoice.BillToCustomer != null)
                                {
                                    _locBillToCustomer = _locReceivableTransactionLine.SalesInvoice.BillToCustomer;
                                }
                                if (_locReceivableTransactionLine.SalesInvoice.BillToAddress != null)
                                {
                                    _locBillToAddress = _locReceivableTransactionLine.SalesInvoice.BillToAddress;
                                }
                                if (_locReceivableTransactionLine.SalesInvoice.BillToContact != null)
                                {
                                    _locBillToContact = _locReceivableTransactionLine.SalesInvoice.BillToContact;
                                }
                                if (_locReceivableTransactionLine.SalesInvoice.BillToCity != null)
                                {
                                    _locBillToCity = _locReceivableTransactionLine.SalesInvoice.BillToCity;
                                }
                                if (_locReceivableTransactionLine.SalesInvoice.BillToCountry != null)
                                {
                                    _locBillToCountry = _locReceivableTransactionLine.SalesInvoice.BillToCountry;
                                }
                                #endregion SalesInvoiceInfo

                            }
                            #endregion SalesInvoice
                            ReceivableTransactionMonitoring _saveDataReceivableTransactionMonitoring = new ReceivableTransactionMonitoring(_currSession)
                            {
                                ReceivableTransaction = _locReceivableTransactionXPO,
                                ReceivableTransactionLine = _locReceivableTransactionLine,
                                #region Organization
                                Company = _locReceivableTransactionXPO.Company,
                                Workplace = _locReceivableTransactionXPO.Workplace,
                                Division = _locReceivableTransactionXPO.Division,
                                Department = _locReceivableTransactionXPO.Department,
                                Section = _locReceivableTransactionXPO.Section,
                                Employee = _locReceivableTransactionXPO.Employee,
                                Div = _locReceivableTransactionLine.Div,
                                Dept = _locReceivableTransactionLine.Dept,
                                Sect = _locReceivableTransactionLine.Sect,
                                PIC = _locReceivableTransactionLine.PIC,
                                Collector = _locReceivableTransactionLine.Collector,
                                #endregion Organization
                                Vehicle = _locReceivableTransactionLine.Vehicle,
                                Customer = _locBillToCustomer,
                                #region SalesInvoiceInfo
                                SalesInvoice = _locReceivableTransactionLine.SalesInvoice,
                                SalesToCustomer = _locSalesToCustomer,
                                SalesToAddress = _locSalesToAddress,
                                SalesToContact = _locSalesToContact,
                                SalesToCity = _locSalesToCity,
                                SalesToCountry = _locSalesToCountry,
                                BillToCustomer = _locBillToCustomer,
                                BillToAddress = _locBillToAddress,
                                BillToContact = _locBillToContact,
                                BillToCity = _locBillToCity,
                                BillToCountry = _locBillToCountry,
                                #endregion SalesInvoiceInfo
                                #region Bank
                                CompanyBankAccount = _locReceivableTransactionLine.CompanyBankAccount,
                                CompanyAccountName = _locReceivableTransactionLine.CompanyAccountName,
                                CompanyAccountNo = _locReceivableTransactionLine.CompanyAccountNo,
                                BankAccount = _locReceivableTransactionLine.BankAccount,
                                AccountName = _locReceivableTransactionLine.AccountName,
                                AccountNo = _locReceivableTransactionLine.AccountNo,
                                #endregion Bank
                                Currency = _locReceivableTransactionLine.Currency,
                                PriceGroup = _locReceivableTransactionLine.PriceGroup,
                                PostingMethod = _locReceivableTransactionLine.PostingMethod,
                                PaymentMethod = _locReceivableTransactionLine.PaymentMethod,
                                PaymentMethodType = _locReceivableTransactionLine.PaymentMethodType,
                                PaymentType = _locReceivableTransactionLine.PaymentType,
                                EstimatedDate = _locReceivableTransactionLine.EstimatedDate,
                                ActualDate = _locReceivableTransactionLine.ActualDate,
                                DueDate = _locReceivableTransactionLine.DueDate,
                                Amount = _locReceivableTransactionLine.Amount,
                                AmountCN = _locReceivableTransactionLine.AmountCN,
                                AmountDN = _locReceivableTransactionLine.AmountDN,
                                Plan = _locReceivableTransactionLine.Plan,
                                Outstanding = _locReceivableTransactionLine.Outstanding,
                                Actual = _locReceivableTransactionLine.Actual,
                                DocDate = _locReceivableTransactionLine.DocDate,
                                JournalMonth = _locReceivableTransactionLine.JournalMonth,
                                JournalYear = _locReceivableTransactionLine.JournalYear,
                                Picking = _locReceivableTransactionLine.Picking,
                                PickingDate = _locReceivableTransactionLine.PickingDate,
                                PickingMonitoring = _locReceivableTransactionLine.PickingMonitoring,
                                PickingLine = _locReceivableTransactionLine.PickingMonitoring.PickingLine,
                            };
                            _saveDataReceivableTransactionMonitoring.Save();
                            _saveDataReceivableTransactionMonitoring.Session.CommitTransaction();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = ReceivableTransaction ", ex.ToString());
            }
        }

        #region JournalReceivable

        private bool CheckJournalSetup(Session _currSession, ReceivableTransaction _locReceivableTransactionXPO)
        {
            bool _result = false;
            try
            {
                if (_locReceivableTransactionXPO != null)
                {
                    if (_locReceivableTransactionXPO.Company != null && _locReceivableTransactionXPO.Workplace != null)
                    {
                        JournalSetupDetail _locJournalSetupDetail = _currSession.FindObject<JournalSetupDetail>
                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                    new BinaryOperator("Company", _locReceivableTransactionXPO.Company),
                                                                    new BinaryOperator("Workplace", _locReceivableTransactionXPO.Workplace),
                                                                    new BinaryOperator("JournalSetupType", JournalSetupType.Journal),
                                                                    new BinaryOperator("ObjectList", ObjectList.ReceivableTransaction),
                                                                    new BinaryOperator("JournalProcess", JournalProcess.JournalReceivable),
                                                                    new BinaryOperator("Active", true)));

                        if (_locJournalSetupDetail != null)
                        {
                            _result = true;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = ReceivableTransaction ", ex.ToString());
            }

            return _result;
        }

        private void SetReceivableJournal(Session _currSession, ReceivableTransaction _locReceivableTransactionXPO)
        {
            try
            {
                DateTime now = DateTime.Now;
                double _locTotalAmountCollBP = 0;
                double _locTotalAmountCollBPDN = 0;
                double _locTotalAmountCollBPCN = 0;
                double _locTotalAmountCollBPOP = 0;
                double _locTotalAmountCollBPLP = 0;
                double _locTotalAmountCollC = 0;
                double _locTotalAmountCollCDN = 0;
                double _locTotalAmountCollCCN = 0;
                double _locTotalAmountCollCOP = 0;
                double _locTotalAmountCollCLP = 0;
                BankAccountGroup _locBankAccountGroup = null;
                BankAccountGroup _locCompanyBankAccountGroup = null;
                BusinessPartnerAccountGroup _locBusinessPartnerAccountGroup = null;
                CompanyAccountGroup _locCompanyAccountGroup = null;

                if (_locReceivableTransactionXPO != null)
                {
                    XPCollection<ReceivableTransactionLine> _locReceivableTransactionLines = new XPCollection<ReceivableTransactionLine>(_currSession,
                                                                        new GroupOperator(GroupOperatorType.And,
                                                                        new BinaryOperator("ReceivableTransaction", _locReceivableTransactionXPO),
                                                                        new BinaryOperator("Select", true),
                                                                        new GroupOperator(GroupOperatorType.Or,
                                                                        new BinaryOperator("Status", Status.Progress),
                                                                        new BinaryOperator("Status", Status.Posted))));

                    if (_locReceivableTransactionLines != null && _locReceivableTransactionLines.Count() > 0)
                    {
                        foreach (ReceivableTransactionLine _locReceivableTransactionLine in _locReceivableTransactionLines)
                        {
                            #region NonBankAccount

                            #region JournalMapBusinessPartnerAccountGroup

                            if (_locReceivableTransactionLine.SalesInvoice != null)
                            {
                                if (_locReceivableTransactionLine.SalesInvoice.BillToCustomer != null)
                                {
                                    BusinessPartnerAccount _locBusinessPartnerAccount = _currSession.FindObject<BusinessPartnerAccount>(new GroupOperator(GroupOperatorType.And,
                                                                            new BinaryOperator("Company", _locReceivableTransactionLine.Company),
                                                                            new BinaryOperator("Workplace", _locReceivableTransactionLine.Workplace),
                                                                            new BinaryOperator("BusinessPartner", _locReceivableTransactionLine.SalesInvoice.BillToCustomer),
                                                                            new BinaryOperator("Active", true)));
                                    if(_locBusinessPartnerAccount != null)
                                    {
                                        if (_locBusinessPartnerAccount.BusinessPartnerAccountGroup != null)
                                        {
                                            _locBusinessPartnerAccountGroup = _locBusinessPartnerAccount.BusinessPartnerAccountGroup;

                                            _locTotalAmountCollBP = _locReceivableTransactionLine.Amount;
                                            _locTotalAmountCollBPDN = _locReceivableTransactionLine.AmountDN;
                                            _locTotalAmountCollBPCN = _locReceivableTransactionLine.AmountCN;
                                            _locTotalAmountCollBPOP = _locReceivableTransactionLine.OverPayment;
                                            _locTotalAmountCollBPLP = _locReceivableTransactionLine.Outstanding;

                                            XPCollection<JournalMap> _locJournalMapItems = new XPCollection<JournalMap>
                                                                               (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                new BinaryOperator("Company", _locReceivableTransactionLine.Company),
                                                                                new BinaryOperator("Workplace", _locReceivableTransactionLine.Workplace),
                                                                                new BinaryOperator("BusinessPartnerAccountGroup", _locBusinessPartnerAccountGroup),
                                                                                new BinaryOperator("Active", true)));

                                            if (_locJournalMapItems != null && _locJournalMapItems.Count() > 0)
                                            {
                                                foreach (JournalMap _locJournalMapItem in _locJournalMapItems)
                                                {
                                                    XPCollection<JournalMapLine> _locJournalMapLines = new XPCollection<JournalMapLine>
                                                                                                       (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                        new BinaryOperator("Company", _locReceivableTransactionLine.Company),
                                                                                                        new BinaryOperator("Workplace", _locReceivableTransactionLine.Workplace),
                                                                                                        new BinaryOperator("JournalMap", _locJournalMapItem),
                                                                                                        new BinaryOperator("Active", true)));

                                                    if (_locJournalMapLines != null && _locJournalMapLines.Count() > 0)
                                                    {
                                                        foreach (JournalMapLine _locJournalMapLine in _locJournalMapLines)
                                                        {
                                                            if (_locJournalMapLine.AccountMap != null)
                                                            {
                                                                #region NormalAmount
                                                                if (_locJournalMapLine.AccountMap.PostingType == PostingType.Sales && _locJournalMapLine.AccountMap.OrderType == OrderType.Item
                                                                    && _locJournalMapLine.AccountMap.PostingMethod == PostingMethod.Bill && _locJournalMapLine.AccountMap.PostingMethodType == PostingMethodType.Normal)
                                                                {
                                                                    XPCollection<AccountMapLine> _locAccountMapLines = new XPCollection<AccountMapLine>
                                                                                                                   (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                                    new BinaryOperator("Company", _locReceivableTransactionLine.Company),
                                                                                                                    new BinaryOperator("Workplace", _locReceivableTransactionLine.Workplace),
                                                                                                                    new BinaryOperator("AccountMap", _locJournalMapLine.AccountMap),
                                                                                                                    new BinaryOperator("Active", true)));

                                                                    if (_locAccountMapLines != null && _locAccountMapLines.Count() > 0)
                                                                    {
                                                                        double _locTotalAmountDebit = 0;
                                                                        double _locTotalAmountCredit = 0;
                                                                        double _locTotalBalance = 0;

                                                                        foreach (AccountMapLine _locAccountMapLine in _locAccountMapLines)
                                                                        {
                                                                            if (_locAccountMapLine.AccountCharge == AccountCharge.Debit)
                                                                            {
                                                                                _locTotalAmountDebit = _locTotalAmountCollBP;
                                                                            }
                                                                            if (_locAccountMapLine.AccountCharge == AccountCharge.Credit)
                                                                            {
                                                                                _locTotalAmountCredit = _locTotalAmountCollBP;
                                                                            }

                                                                            GeneralJournal _saveGeneralJournal = new GeneralJournal(_currSession)
                                                                            {
                                                                                Company = _locReceivableTransactionLine.Company,
                                                                                Workplace = _locReceivableTransactionLine.Workplace,
                                                                                PostingDate = now,
                                                                                PostingType = PostingType.Sales,
                                                                                OrderType = OrderType.Item,
                                                                                PostingMethod = PostingMethod.Bill,
                                                                                PostingMethodType = PostingMethodType.Normal,
                                                                                Account = _locAccountMapLine.Account,
                                                                                Debit = _locTotalAmountDebit,
                                                                                Credit = _locTotalAmountCredit,
                                                                                JournalMonth = _locReceivableTransactionLine.JournalMonth,
                                                                                JournalYear = _locReceivableTransactionLine.JournalYear,
                                                                                ReceivableTransaction = _locReceivableTransactionXPO,
                                                                            };

                                                                            _saveGeneralJournal.Save();
                                                                            _saveGeneralJournal.Session.CommitTransaction();

                                                                            #region AccountingPeriodicLine

                                                                            if (_locAccountMapLine.Account.Code != null)
                                                                            {

                                                                                //Cari Account Periodic Line
                                                                                AccountingPeriodicLine _locAPL = _currSession.FindObject<AccountingPeriodicLine>
                                                                                                                (new GroupOperator(GroupOperatorType.And,
                                                                                                                new BinaryOperator("Company", _locReceivableTransactionLine.Company),
                                                                                                                new BinaryOperator("Workplace", _locReceivableTransactionLine.Workplace),
                                                                                                                new BinaryOperator("AccountNo", _locAccountMapLine.Account),
                                                                                                                new BinaryOperator("Month", _locReceivableTransactionLine.JournalMonth),
                                                                                                                new BinaryOperator("Year", _locReceivableTransactionLine.JournalYear)));
                                                                                if (_locAPL != null)
                                                                                {
                                                                                    if (_locAPL.AccountNo != null)
                                                                                    {
                                                                                        if (_locAPL.AccountNo.BalanceType == BalanceType.Change)
                                                                                        {
                                                                                            if (_locAPL.AccountNo.AccountCharge == AccountCharge.Debit)
                                                                                            {
                                                                                                if (_locTotalAmountDebit > 0)
                                                                                                {
                                                                                                    _locTotalBalance = _locAPL.Balance + _locTotalAmountDebit;
                                                                                                }
                                                                                                if (_locTotalAmountCredit > 0)
                                                                                                {
                                                                                                    _locTotalBalance = _locAPL.Balance - _locTotalAmountCredit;
                                                                                                }
                                                                                            }
                                                                                            if (_locAPL.AccountNo.AccountCharge == AccountCharge.Credit)
                                                                                            {
                                                                                                if (_locTotalAmountDebit > 0)
                                                                                                {
                                                                                                    _locTotalBalance = _locAPL.Balance - _locTotalAmountDebit;
                                                                                                }
                                                                                                if (_locTotalAmountCredit > 0)
                                                                                                {
                                                                                                    _locTotalBalance = _locAPL.Balance + _locTotalAmountCredit;
                                                                                                }
                                                                                            }
                                                                                        }

                                                                                        _locAPL.Balance = _locTotalBalance;
                                                                                        _locAPL.Debit = _locAPL.Debit + _locTotalAmountDebit;
                                                                                        _locAPL.Credit = _locAPL.Credit + _locTotalAmountCredit;
                                                                                        _locAPL.Save();
                                                                                        _locAPL.Session.CommitTransaction();

                                                                                    }
                                                                                }
                                                                            }
                                                                            #endregion COA
                                                                        }
                                                                    }
                                                                }
                                                                #endregion NormalAmount   
                                                                #region AmountDN
                                                                if (_locTotalAmountCollBPDN > 0)
                                                                {
                                                                    if (_locJournalMapLine.AccountMap.PostingType == PostingType.Sales && _locJournalMapLine.AccountMap.OrderType == OrderType.Item
                                                                    && _locJournalMapLine.AccountMap.PostingMethod == PostingMethod.Bill && _locJournalMapLine.AccountMap.PostingMethodType == PostingMethodType.AmountDN)
                                                                    {
                                                                        XPCollection<AccountMapLine> _locAccountMapLines = new XPCollection<AccountMapLine>
                                                                                                                       (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                                        new BinaryOperator("Company", _locReceivableTransactionLine.Company),
                                                                                                                        new BinaryOperator("Workplace", _locReceivableTransactionLine.Workplace),
                                                                                                                        new BinaryOperator("AccountMap", _locJournalMapLine.AccountMap),
                                                                                                                        new BinaryOperator("Active", true)));

                                                                        if (_locAccountMapLines != null && _locAccountMapLines.Count() > 0)
                                                                        {
                                                                            double _locTotalAmountDebit = 0;
                                                                            double _locTotalAmountCredit = 0;
                                                                            double _locTotalBalance = 0;

                                                                            foreach (AccountMapLine _locAccountMapLine in _locAccountMapLines)
                                                                            {
                                                                                if (_locAccountMapLine.AccountCharge == AccountCharge.Debit)
                                                                                {
                                                                                    _locTotalAmountDebit = _locTotalAmountCollBPDN;
                                                                                }
                                                                                if (_locAccountMapLine.AccountCharge == AccountCharge.Credit)
                                                                                {
                                                                                    _locTotalAmountCredit = _locTotalAmountCollBPDN;
                                                                                }

                                                                                GeneralJournal _saveGeneralJournal = new GeneralJournal(_currSession)
                                                                                {
                                                                                    Company = _locReceivableTransactionLine.Company,
                                                                                    Workplace = _locReceivableTransactionLine.Workplace,
                                                                                    PostingDate = now,
                                                                                    PostingType = PostingType.Sales,
                                                                                    PostingMethod = PostingMethod.Bill,
                                                                                    PostingMethodType = PostingMethodType.AmountDN,
                                                                                    Account = _locAccountMapLine.Account,
                                                                                    Debit = _locTotalAmountDebit,
                                                                                    Credit = _locTotalAmountCredit,
                                                                                    JournalMonth = _locReceivableTransactionLine.JournalMonth,
                                                                                    JournalYear = _locReceivableTransactionLine.JournalYear,
                                                                                    ReceivableTransaction = _locReceivableTransactionXPO,
                                                                                };

                                                                                _saveGeneralJournal.Save();
                                                                                _saveGeneralJournal.Session.CommitTransaction();

                                                                                #region AccountingPeriodicLine

                                                                                if (_locAccountMapLine.Account.Code != null)
                                                                                {

                                                                                    //Cari Account Periodic Line
                                                                                    AccountingPeriodicLine _locAPL = _currSession.FindObject<AccountingPeriodicLine>
                                                                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                                                                    new BinaryOperator("Company", _locReceivableTransactionLine.Company),
                                                                                                                    new BinaryOperator("Workplace", _locReceivableTransactionLine.Workplace),
                                                                                                                    new BinaryOperator("AccountNo", _locAccountMapLine.Account),
                                                                                                                    new BinaryOperator("Month", _locReceivableTransactionLine.JournalMonth),
                                                                                                                    new BinaryOperator("Year", _locReceivableTransactionLine.JournalYear)));
                                                                                    if (_locAPL != null)
                                                                                    {
                                                                                        if (_locAPL.AccountNo != null)
                                                                                        {
                                                                                            if (_locAPL.AccountNo.BalanceType == BalanceType.Change)
                                                                                            {
                                                                                                if (_locAPL.AccountNo.AccountCharge == AccountCharge.Debit)
                                                                                                {
                                                                                                    if (_locTotalAmountDebit > 0)
                                                                                                    {
                                                                                                        _locTotalBalance = _locAPL.Balance + _locTotalAmountDebit;
                                                                                                    }
                                                                                                    if (_locTotalAmountCredit > 0)
                                                                                                    {
                                                                                                        _locTotalBalance = _locAPL.Balance - _locTotalAmountCredit;
                                                                                                    }
                                                                                                }
                                                                                                if (_locAPL.AccountNo.AccountCharge == AccountCharge.Credit)
                                                                                                {
                                                                                                    if (_locTotalAmountDebit > 0)
                                                                                                    {
                                                                                                        _locTotalBalance = _locAPL.Balance - _locTotalAmountDebit;
                                                                                                    }
                                                                                                    if (_locTotalAmountCredit > 0)
                                                                                                    {
                                                                                                        _locTotalBalance = _locAPL.Balance + _locTotalAmountCredit;
                                                                                                    }
                                                                                                }
                                                                                            }

                                                                                            _locAPL.Balance = _locTotalBalance;
                                                                                            _locAPL.Debit = _locAPL.Debit + _locTotalAmountDebit;
                                                                                            _locAPL.Credit = _locAPL.Credit + _locTotalAmountCredit;
                                                                                            _locAPL.Save();
                                                                                            _locAPL.Session.CommitTransaction();

                                                                                        }
                                                                                    }
                                                                                }
                                                                                #endregion COA
                                                                            }
                                                                        }
                                                                    }
                                                                }

                                                                #endregion AmountDN
                                                                #region AmountCN
                                                                if (_locTotalAmountCollBPCN > 0)
                                                                {
                                                                    if (_locJournalMapLine.AccountMap.PostingType == PostingType.Sales && _locJournalMapLine.AccountMap.OrderType == OrderType.Item
                                                                    && _locJournalMapLine.AccountMap.PostingMethod == PostingMethod.Bill && _locJournalMapLine.AccountMap.PostingMethodType == PostingMethodType.AmountCN)
                                                                    {
                                                                        XPCollection<AccountMapLine> _locAccountMapLines = new XPCollection<AccountMapLine>
                                                                                                                       (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                                        new BinaryOperator("Company", _locReceivableTransactionLine.Company),
                                                                                                                        new BinaryOperator("Workplace", _locReceivableTransactionLine.Workplace),
                                                                                                                        new BinaryOperator("AccountMap", _locJournalMapLine.AccountMap),
                                                                                                                        new BinaryOperator("Active", true)));

                                                                        if (_locAccountMapLines != null && _locAccountMapLines.Count() > 0)
                                                                        {
                                                                            double _locTotalAmountDebit = 0;
                                                                            double _locTotalAmountCredit = 0;
                                                                            double _locTotalBalance = 0;

                                                                            foreach (AccountMapLine _locAccountMapLine in _locAccountMapLines)
                                                                            {
                                                                                if (_locAccountMapLine.AccountCharge == AccountCharge.Debit)
                                                                                {
                                                                                    _locTotalAmountDebit = _locTotalAmountCollBPCN;
                                                                                }
                                                                                if (_locAccountMapLine.AccountCharge == AccountCharge.Credit)
                                                                                {
                                                                                    _locTotalAmountCredit = _locTotalAmountCollBPCN;
                                                                                }

                                                                                GeneralJournal _saveGeneralJournal = new GeneralJournal(_currSession)
                                                                                {
                                                                                    Company = _locReceivableTransactionLine.Company,
                                                                                    Workplace = _locReceivableTransactionLine.Workplace,
                                                                                    PostingDate = now,
                                                                                    PostingType = PostingType.Sales,
                                                                                    PostingMethod = PostingMethod.Bill,
                                                                                    PostingMethodType = PostingMethodType.AmountCN,
                                                                                    Account = _locAccountMapLine.Account,
                                                                                    Debit = _locTotalAmountDebit,
                                                                                    Credit = _locTotalAmountCredit,
                                                                                    JournalMonth = _locReceivableTransactionLine.JournalMonth,
                                                                                    JournalYear = _locReceivableTransactionLine.JournalYear,
                                                                                    ReceivableTransaction = _locReceivableTransactionXPO,
                                                                                };

                                                                                _saveGeneralJournal.Save();
                                                                                _saveGeneralJournal.Session.CommitTransaction();

                                                                                #region AccountingPeriodicLine

                                                                                if (_locAccountMapLine.Account.Code != null)
                                                                                {

                                                                                    //Cari Account Periodic Line
                                                                                    AccountingPeriodicLine _locAPL = _currSession.FindObject<AccountingPeriodicLine>
                                                                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                                                                    new BinaryOperator("Company", _locReceivableTransactionLine.Company),
                                                                                                                    new BinaryOperator("Workplace", _locReceivableTransactionLine.Workplace),
                                                                                                                    new BinaryOperator("AccountNo", _locAccountMapLine.Account),
                                                                                                                    new BinaryOperator("Month", _locReceivableTransactionLine.JournalMonth),
                                                                                                                    new BinaryOperator("Year", _locReceivableTransactionLine.JournalYear)));
                                                                                    if (_locAPL != null)
                                                                                    {
                                                                                        if (_locAPL.AccountNo != null)
                                                                                        {
                                                                                            if (_locAPL.AccountNo.BalanceType == BalanceType.Change)
                                                                                            {
                                                                                                if (_locAPL.AccountNo.AccountCharge == AccountCharge.Debit)
                                                                                                {
                                                                                                    if (_locTotalAmountDebit > 0)
                                                                                                    {
                                                                                                        _locTotalBalance = _locAPL.Balance + _locTotalAmountDebit;
                                                                                                    }
                                                                                                    if (_locTotalAmountCredit > 0)
                                                                                                    {
                                                                                                        _locTotalBalance = _locAPL.Balance - _locTotalAmountCredit;
                                                                                                    }
                                                                                                }
                                                                                                if (_locAPL.AccountNo.AccountCharge == AccountCharge.Credit)
                                                                                                {
                                                                                                    if (_locTotalAmountDebit > 0)
                                                                                                    {
                                                                                                        _locTotalBalance = _locAPL.Balance - _locTotalAmountDebit;
                                                                                                    }
                                                                                                    if (_locTotalAmountCredit > 0)
                                                                                                    {
                                                                                                        _locTotalBalance = _locAPL.Balance + _locTotalAmountCredit;
                                                                                                    }
                                                                                                }
                                                                                            }

                                                                                            _locAPL.Balance = _locTotalBalance;
                                                                                            _locAPL.Debit = _locAPL.Debit + _locTotalAmountDebit;
                                                                                            _locAPL.Credit = _locAPL.Credit + _locTotalAmountCredit;
                                                                                            _locAPL.Save();
                                                                                            _locAPL.Session.CommitTransaction();

                                                                                        }
                                                                                    }
                                                                                }
                                                                                #endregion COA
                                                                            }
                                                                        }
                                                                    }
                                                                }

                                                                #endregion AmountCN
                                                                #region OverPayment
                                                                if (_locTotalAmountCollBPOP > 0)
                                                                {
                                                                    if (_locJournalMapLine.AccountMap.PostingType == PostingType.Sales && _locJournalMapLine.AccountMap.OrderType == OrderType.Item
                                                                    && _locJournalMapLine.AccountMap.PostingMethod == PostingMethod.Bill && _locJournalMapLine.AccountMap.PostingMethodType == PostingMethodType.OverPayment)
                                                                    {
                                                                        XPCollection<AccountMapLine> _locAccountMapLines = new XPCollection<AccountMapLine>
                                                                                                                       (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                                        new BinaryOperator("Company", _locReceivableTransactionLine.Company),
                                                                                                                        new BinaryOperator("Workplace", _locReceivableTransactionLine.Workplace),
                                                                                                                        new BinaryOperator("AccountMap", _locJournalMapLine.AccountMap),
                                                                                                                        new BinaryOperator("Active", true)));

                                                                        if (_locAccountMapLines != null && _locAccountMapLines.Count() > 0)
                                                                        {
                                                                            double _locTotalAmountDebit = 0;
                                                                            double _locTotalAmountCredit = 0;
                                                                            double _locTotalBalance = 0;

                                                                            foreach (AccountMapLine _locAccountMapLine in _locAccountMapLines)
                                                                            {
                                                                                if (_locAccountMapLine.AccountCharge == AccountCharge.Debit)
                                                                                {
                                                                                    _locTotalAmountDebit = _locTotalAmountCollBPOP;
                                                                                }
                                                                                if (_locAccountMapLine.AccountCharge == AccountCharge.Credit)
                                                                                {
                                                                                    _locTotalAmountCredit = _locTotalAmountCollBPOP;
                                                                                }

                                                                                GeneralJournal _saveGeneralJournal = new GeneralJournal(_currSession)
                                                                                {
                                                                                    Company = _locReceivableTransactionLine.Company,
                                                                                    Workplace = _locReceivableTransactionLine.Workplace,
                                                                                    PostingDate = now,
                                                                                    PostingType = PostingType.Sales,
                                                                                    PostingMethod = PostingMethod.Bill,
                                                                                    PostingMethodType = PostingMethodType.OverPayment,
                                                                                    Account = _locAccountMapLine.Account,
                                                                                    Debit = _locTotalAmountDebit,
                                                                                    Credit = _locTotalAmountCredit,
                                                                                    JournalMonth = _locReceivableTransactionLine.JournalMonth,
                                                                                    JournalYear = _locReceivableTransactionLine.JournalYear,
                                                                                    ReceivableTransaction = _locReceivableTransactionXPO,
                                                                                };

                                                                                _saveGeneralJournal.Save();
                                                                                _saveGeneralJournal.Session.CommitTransaction();

                                                                                #region AccountingPeriodicLine

                                                                                if (_locAccountMapLine.Account.Code != null)
                                                                                {

                                                                                    //Cari Account Periodic Line
                                                                                    AccountingPeriodicLine _locAPL = _currSession.FindObject<AccountingPeriodicLine>
                                                                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                                                                    new BinaryOperator("Company", _locReceivableTransactionLine.Company),
                                                                                                                    new BinaryOperator("Workplace", _locReceivableTransactionLine.Workplace),
                                                                                                                    new BinaryOperator("AccountNo", _locAccountMapLine.Account),
                                                                                                                    new BinaryOperator("Month", _locReceivableTransactionLine.JournalMonth),
                                                                                                                    new BinaryOperator("Year", _locReceivableTransactionLine.JournalYear)));
                                                                                    if (_locAPL != null)
                                                                                    {
                                                                                        if (_locAPL.AccountNo != null)
                                                                                        {
                                                                                            if (_locAPL.AccountNo.BalanceType == BalanceType.Change)
                                                                                            {
                                                                                                if (_locAPL.AccountNo.AccountCharge == AccountCharge.Debit)
                                                                                                {
                                                                                                    if (_locTotalAmountDebit > 0)
                                                                                                    {
                                                                                                        _locTotalBalance = _locAPL.Balance + _locTotalAmountDebit;
                                                                                                    }
                                                                                                    if (_locTotalAmountCredit > 0)
                                                                                                    {
                                                                                                        _locTotalBalance = _locAPL.Balance - _locTotalAmountCredit;
                                                                                                    }
                                                                                                }
                                                                                                if (_locAPL.AccountNo.AccountCharge == AccountCharge.Credit)
                                                                                                {
                                                                                                    if (_locTotalAmountDebit > 0)
                                                                                                    {
                                                                                                        _locTotalBalance = _locAPL.Balance - _locTotalAmountDebit;
                                                                                                    }
                                                                                                    if (_locTotalAmountCredit > 0)
                                                                                                    {
                                                                                                        _locTotalBalance = _locAPL.Balance + _locTotalAmountCredit;
                                                                                                    }
                                                                                                }
                                                                                            }

                                                                                            _locAPL.Balance = _locTotalBalance;
                                                                                            _locAPL.Debit = _locAPL.Debit + _locTotalAmountDebit;
                                                                                            _locAPL.Credit = _locAPL.Credit + _locTotalAmountCredit;
                                                                                            _locAPL.Save();
                                                                                            _locAPL.Session.CommitTransaction();

                                                                                        }
                                                                                    }
                                                                                }
                                                                                #endregion COA
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                                #endregion OverPayment
                                                                #region LowerPayment
                                                                if (_locTotalAmountCollBPLP > 0 && _locReceivableTransactionLine.Settled == true)
                                                                {
                                                                    if (_locJournalMapLine.AccountMap.PostingType == PostingType.Sales && _locJournalMapLine.AccountMap.OrderType == OrderType.Item
                                                                    && _locJournalMapLine.AccountMap.PostingMethod == PostingMethod.Bill && _locJournalMapLine.AccountMap.PostingMethodType == PostingMethodType.LowerPayment)
                                                                    {
                                                                        XPCollection<AccountMapLine> _locAccountMapLines = new XPCollection<AccountMapLine>
                                                                                                                       (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                                        new BinaryOperator("Company", _locReceivableTransactionLine.Company),
                                                                                                                        new BinaryOperator("Workplace", _locReceivableTransactionLine.Workplace),
                                                                                                                        new BinaryOperator("AccountMap", _locJournalMapLine.AccountMap),
                                                                                                                        new BinaryOperator("Active", true)));

                                                                        if (_locAccountMapLines != null && _locAccountMapLines.Count() > 0)
                                                                        {
                                                                            double _locTotalAmountDebit = 0;
                                                                            double _locTotalAmountCredit = 0;
                                                                            double _locTotalBalance = 0;

                                                                            foreach (AccountMapLine _locAccountMapLine in _locAccountMapLines)
                                                                            {
                                                                                if (_locAccountMapLine.AccountCharge == AccountCharge.Debit)
                                                                                {
                                                                                    _locTotalAmountDebit = _locTotalAmountCollBPLP;
                                                                                }
                                                                                if (_locAccountMapLine.AccountCharge == AccountCharge.Credit)
                                                                                {
                                                                                    _locTotalAmountCredit = _locTotalAmountCollBPLP;
                                                                                }

                                                                                GeneralJournal _saveGeneralJournal = new GeneralJournal(_currSession)
                                                                                {
                                                                                    Company = _locReceivableTransactionLine.Company,
                                                                                    Workplace = _locReceivableTransactionLine.Workplace,
                                                                                    PostingDate = now,
                                                                                    PostingType = PostingType.Sales,
                                                                                    PostingMethod = PostingMethod.Bill,
                                                                                    PostingMethodType = PostingMethodType.LowerPayment,
                                                                                    Account = _locAccountMapLine.Account,
                                                                                    Debit = _locTotalAmountDebit,
                                                                                    Credit = _locTotalAmountCredit,
                                                                                    JournalMonth = _locReceivableTransactionLine.JournalMonth,
                                                                                    JournalYear = _locReceivableTransactionLine.JournalYear,
                                                                                    ReceivableTransaction = _locReceivableTransactionXPO,
                                                                                };

                                                                                _saveGeneralJournal.Save();
                                                                                _saveGeneralJournal.Session.CommitTransaction();

                                                                                #region AccountingPeriodicLine

                                                                                if (_locAccountMapLine.Account.Code != null)
                                                                                {

                                                                                    //Cari Account Periodic Line
                                                                                    AccountingPeriodicLine _locAPL = _currSession.FindObject<AccountingPeriodicLine>
                                                                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                                                                    new BinaryOperator("Company", _locReceivableTransactionLine.Company),
                                                                                                                    new BinaryOperator("Workplace", _locReceivableTransactionLine.Workplace),
                                                                                                                    new BinaryOperator("AccountNo", _locAccountMapLine.Account),
                                                                                                                    new BinaryOperator("Month", _locReceivableTransactionLine.JournalMonth),
                                                                                                                    new BinaryOperator("Year", _locReceivableTransactionLine.JournalYear)));
                                                                                    if (_locAPL != null)
                                                                                    {
                                                                                        if (_locAPL.AccountNo != null)
                                                                                        {
                                                                                            if (_locAPL.AccountNo.BalanceType == BalanceType.Change)
                                                                                            {
                                                                                                if (_locAPL.AccountNo.AccountCharge == AccountCharge.Debit)
                                                                                                {
                                                                                                    if (_locTotalAmountDebit > 0)
                                                                                                    {
                                                                                                        _locTotalBalance = _locAPL.Balance + _locTotalAmountDebit;
                                                                                                    }
                                                                                                    if (_locTotalAmountCredit > 0)
                                                                                                    {
                                                                                                        _locTotalBalance = _locAPL.Balance - _locTotalAmountCredit;
                                                                                                    }
                                                                                                }
                                                                                                if (_locAPL.AccountNo.AccountCharge == AccountCharge.Credit)
                                                                                                {
                                                                                                    if (_locTotalAmountDebit > 0)
                                                                                                    {
                                                                                                        _locTotalBalance = _locAPL.Balance - _locTotalAmountDebit;
                                                                                                    }
                                                                                                    if (_locTotalAmountCredit > 0)
                                                                                                    {
                                                                                                        _locTotalBalance = _locAPL.Balance + _locTotalAmountCredit;
                                                                                                    }
                                                                                                }
                                                                                            }

                                                                                            _locAPL.Balance = _locTotalBalance;
                                                                                            _locAPL.Debit = _locAPL.Debit + _locTotalAmountDebit;
                                                                                            _locAPL.Credit = _locAPL.Credit + _locTotalAmountCredit;
                                                                                            _locAPL.Save();
                                                                                            _locAPL.Session.CommitTransaction();

                                                                                        }
                                                                                    }
                                                                                }
                                                                                #endregion COA
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                                #endregion LowerPayment
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    
                                }
                            }
                            #endregion JournalMapBusinessPartnerAccountGroup

                            #region JournalMapCompanyAccountGroup
                            if (_locReceivableTransactionLine.Company != null)
                            {
                                _locTotalAmountCollC = _locReceivableTransactionLine.Amount;
                                _locTotalAmountCollCDN = _locReceivableTransactionLine.AmountDN;
                                _locTotalAmountCollCCN = _locReceivableTransactionLine.AmountCN;
                                _locTotalAmountCollCOP = _locReceivableTransactionLine.OverPayment;
                                _locTotalAmountCollCLP = _locReceivableTransactionLine.Outstanding;

                                CompanyAccount _locCompanyAccount = _currSession.FindObject<CompanyAccount>(new GroupOperator(GroupOperatorType.And,
                                                                    new BinaryOperator("Company", _locReceivableTransactionLine.Company),
                                                                    new BinaryOperator("Workplace", _locReceivableTransactionLine.Workplace),
                                                                    new BinaryOperator("Active", true)));

                                if (_locCompanyAccount != null)
                                {
                                    if(_locCompanyAccount.CompanyAccountGroup != null)
                                    {
                                        _locCompanyAccountGroup = _locCompanyAccount.CompanyAccountGroup;

                                        XPCollection<JournalMap> _locJournalMapItems = new XPCollection<JournalMap>
                                                                   (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                    new BinaryOperator("Company", _locReceivableTransactionLine.Company),
                                                                    new BinaryOperator("Workplace", _locReceivableTransactionLine.Workplace),
                                                                    new BinaryOperator("CompanyAccountGroup", _locCompanyAccountGroup),
                                                                    new BinaryOperator("Active", true)));

                                        if (_locJournalMapItems != null && _locJournalMapItems.Count() > 0)
                                        {
                                            foreach (JournalMap _locJournalMapItem in _locJournalMapItems)
                                            {
                                                XPCollection<JournalMapLine> _locJournalMapLines = new XPCollection<JournalMapLine>
                                                                                                   (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                    new BinaryOperator("Company", _locReceivableTransactionLine.Company),
                                                                                                    new BinaryOperator("Workplace", _locReceivableTransactionLine.Workplace),
                                                                                                    new BinaryOperator("JournalMap", _locJournalMapItem),
                                                                                                    new BinaryOperator("Active", true)));

                                                if (_locJournalMapLines != null && _locJournalMapLines.Count() > 0)
                                                {
                                                    foreach (JournalMapLine _locJournalMapLine in _locJournalMapLines)
                                                    {
                                                        if (_locJournalMapLine.AccountMap != null)
                                                        {
                                                            #region NormalAmount
                                                            if (_locJournalMapLine.AccountMap.PostingType == PostingType.Sales && _locJournalMapLine.AccountMap.OrderType == OrderType.Item
                                                                && _locJournalMapLine.AccountMap.PostingMethod == PostingMethod.Bill && _locJournalMapLine.AccountMap.PostingMethodType == PostingMethodType.Normal)
                                                            {
                                                                XPCollection<AccountMapLine> _locAccountMapLines = new XPCollection<AccountMapLine>
                                                                                                               (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                                new BinaryOperator("Company", _locReceivableTransactionLine.Company),
                                                                                                                new BinaryOperator("Workplace", _locReceivableTransactionLine.Workplace),
                                                                                                                new BinaryOperator("AccountMap", _locJournalMapLine.AccountMap),
                                                                                                                new BinaryOperator("Active", true)));

                                                                if (_locAccountMapLines != null && _locAccountMapLines.Count() > 0)
                                                                {
                                                                    double _locTotalAmountDebit = 0;
                                                                    double _locTotalAmountCredit = 0;
                                                                    double _locTotalBalance = 0;

                                                                    foreach (AccountMapLine _locAccountMapLine in _locAccountMapLines)
                                                                    {
                                                                        if (_locAccountMapLine.AccountCharge == AccountCharge.Debit)
                                                                        {
                                                                            _locTotalAmountDebit = _locTotalAmountCollC;
                                                                        }
                                                                        if (_locAccountMapLine.AccountCharge == AccountCharge.Credit)
                                                                        {
                                                                            _locTotalAmountCredit = _locTotalAmountCollC;
                                                                        }

                                                                        GeneralJournal _saveGeneralJournal = new GeneralJournal(_currSession)
                                                                        {
                                                                            Company = _locReceivableTransactionLine.Company,
                                                                            Workplace = _locReceivableTransactionLine.Workplace,
                                                                            PostingDate = now,
                                                                            PostingType = PostingType.Sales,
                                                                            OrderType = OrderType.Item,
                                                                            PostingMethod = PostingMethod.Bill,
                                                                            PostingMethodType = PostingMethodType.Normal,
                                                                            Account = _locAccountMapLine.Account,
                                                                            Debit = _locTotalAmountDebit,
                                                                            Credit = _locTotalAmountCredit,
                                                                            JournalMonth = _locReceivableTransactionLine.JournalMonth,
                                                                            JournalYear = _locReceivableTransactionLine.JournalYear,
                                                                            ReceivableTransaction = _locReceivableTransactionXPO,
                                                                        };

                                                                        _saveGeneralJournal.Save();
                                                                        _saveGeneralJournal.Session.CommitTransaction();

                                                                        #region AccountingPeriodicLine

                                                                        if (_locAccountMapLine.Account.Code != null)
                                                                        {

                                                                            //Cari Account Periodic Line
                                                                            AccountingPeriodicLine _locAPL = _currSession.FindObject<AccountingPeriodicLine>
                                                                                                            (new GroupOperator(GroupOperatorType.And,
                                                                                                            new BinaryOperator("Company", _locReceivableTransactionLine.Company),
                                                                                                            new BinaryOperator("Workplace", _locReceivableTransactionLine.Workplace),
                                                                                                            new BinaryOperator("AccountNo", _locAccountMapLine.Account),
                                                                                                            new BinaryOperator("Month", _locReceivableTransactionLine.JournalMonth),
                                                                                                            new BinaryOperator("Year", _locReceivableTransactionLine.JournalYear)));
                                                                            if (_locAPL != null)
                                                                            {
                                                                                if (_locAPL.AccountNo != null)
                                                                                {
                                                                                    if (_locAPL.AccountNo.BalanceType == BalanceType.Change)
                                                                                    {
                                                                                        if (_locAPL.AccountNo.AccountCharge == AccountCharge.Debit)
                                                                                        {
                                                                                            if (_locTotalAmountDebit > 0)
                                                                                            {
                                                                                                _locTotalBalance = _locAPL.Balance + _locTotalAmountDebit;
                                                                                            }
                                                                                            if (_locTotalAmountCredit > 0)
                                                                                            {
                                                                                                _locTotalBalance = _locAPL.Balance - _locTotalAmountCredit;
                                                                                            }
                                                                                        }
                                                                                        if (_locAPL.AccountNo.AccountCharge == AccountCharge.Credit)
                                                                                        {
                                                                                            if (_locTotalAmountDebit > 0)
                                                                                            {
                                                                                                _locTotalBalance = _locAPL.Balance - _locTotalAmountDebit;
                                                                                            }
                                                                                            if (_locTotalAmountCredit > 0)
                                                                                            {
                                                                                                _locTotalBalance = _locAPL.Balance + _locTotalAmountCredit;
                                                                                            }
                                                                                        }
                                                                                    }

                                                                                    _locAPL.Balance = _locTotalBalance;
                                                                                    _locAPL.Debit = _locAPL.Debit + _locTotalAmountDebit;
                                                                                    _locAPL.Credit = _locAPL.Credit + _locTotalAmountCredit;
                                                                                    _locAPL.Save();
                                                                                    _locAPL.Session.CommitTransaction();

                                                                                }
                                                                            }
                                                                        }
                                                                        #endregion COA
                                                                    }
                                                                }
                                                            }
                                                            #endregion NormalAmount 
                                                            #region AmountDN
                                                            if (_locTotalAmountCollCDN > 0)
                                                            {
                                                                if (_locJournalMapLine.AccountMap.PostingType == PostingType.Sales && _locJournalMapLine.AccountMap.OrderType == OrderType.Item
                                                                && _locJournalMapLine.AccountMap.PostingMethod == PostingMethod.Bill && _locJournalMapLine.AccountMap.PostingMethodType == PostingMethodType.AmountDN)
                                                                {
                                                                    XPCollection<AccountMapLine> _locAccountMapLines = new XPCollection<AccountMapLine>
                                                                                                                   (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                                    new BinaryOperator("Company", _locReceivableTransactionLine.Company),
                                                                                                                    new BinaryOperator("Workplace", _locReceivableTransactionLine.Workplace),
                                                                                                                    new BinaryOperator("AccountMap", _locJournalMapLine.AccountMap),
                                                                                                                    new BinaryOperator("Active", true)));

                                                                    if (_locAccountMapLines != null && _locAccountMapLines.Count() > 0)
                                                                    {
                                                                        double _locTotalAmountDebit = 0;
                                                                        double _locTotalAmountCredit = 0;
                                                                        double _locTotalBalance = 0;

                                                                        foreach (AccountMapLine _locAccountMapLine in _locAccountMapLines)
                                                                        {
                                                                            if (_locAccountMapLine.AccountCharge == AccountCharge.Debit)
                                                                            {
                                                                                _locTotalAmountDebit = _locTotalAmountCollCDN;
                                                                            }
                                                                            if (_locAccountMapLine.AccountCharge == AccountCharge.Credit)
                                                                            {
                                                                                _locTotalAmountCredit = _locTotalAmountCollCDN;
                                                                            }

                                                                            GeneralJournal _saveGeneralJournal = new GeneralJournal(_currSession)
                                                                            {
                                                                                Company = _locReceivableTransactionLine.Company,
                                                                                Workplace = _locReceivableTransactionLine.Workplace,
                                                                                PostingDate = now,
                                                                                PostingType = PostingType.Sales,
                                                                                OrderType = OrderType.Item,
                                                                                PostingMethod = PostingMethod.Bill,
                                                                                PostingMethodType = PostingMethodType.AmountDN,
                                                                                Account = _locAccountMapLine.Account,
                                                                                Debit = _locTotalAmountDebit,
                                                                                Credit = _locTotalAmountCredit,
                                                                                JournalMonth = _locReceivableTransactionLine.JournalMonth,
                                                                                JournalYear = _locReceivableTransactionLine.JournalYear,
                                                                                ReceivableTransaction = _locReceivableTransactionXPO,
                                                                            };

                                                                            _saveGeneralJournal.Save();
                                                                            _saveGeneralJournal.Session.CommitTransaction();

                                                                            #region AccountingPeriodicLine

                                                                            if (_locAccountMapLine.Account.Code != null)
                                                                            {

                                                                                //Cari Account Periodic Line
                                                                                AccountingPeriodicLine _locAPL = _currSession.FindObject<AccountingPeriodicLine>
                                                                                                                (new GroupOperator(GroupOperatorType.And,
                                                                                                                new BinaryOperator("Company", _locReceivableTransactionLine.Company),
                                                                                                                new BinaryOperator("Workplace", _locReceivableTransactionLine.Workplace),
                                                                                                                new BinaryOperator("AccountNo", _locAccountMapLine.Account),
                                                                                                                new BinaryOperator("Month", _locReceivableTransactionLine.JournalMonth),
                                                                                                                new BinaryOperator("Year", _locReceivableTransactionLine.JournalYear)));
                                                                                if (_locAPL != null)
                                                                                {
                                                                                    if (_locAPL.AccountNo != null)
                                                                                    {
                                                                                        if (_locAPL.AccountNo.BalanceType == BalanceType.Change)
                                                                                        {
                                                                                            if (_locAPL.AccountNo.AccountCharge == AccountCharge.Debit)
                                                                                            {
                                                                                                if (_locTotalAmountDebit > 0)
                                                                                                {
                                                                                                    _locTotalBalance = _locAPL.Balance + _locTotalAmountDebit;
                                                                                                }
                                                                                                if (_locTotalAmountCredit > 0)
                                                                                                {
                                                                                                    _locTotalBalance = _locAPL.Balance - _locTotalAmountCredit;
                                                                                                }
                                                                                            }
                                                                                            if (_locAPL.AccountNo.AccountCharge == AccountCharge.Credit)
                                                                                            {
                                                                                                if (_locTotalAmountDebit > 0)
                                                                                                {
                                                                                                    _locTotalBalance = _locAPL.Balance - _locTotalAmountDebit;
                                                                                                }
                                                                                                if (_locTotalAmountCredit > 0)
                                                                                                {
                                                                                                    _locTotalBalance = _locAPL.Balance + _locTotalAmountCredit;
                                                                                                }
                                                                                            }
                                                                                        }

                                                                                        _locAPL.Balance = _locTotalBalance;
                                                                                        _locAPL.Debit = _locAPL.Debit + _locTotalAmountDebit;
                                                                                        _locAPL.Credit = _locAPL.Credit + _locTotalAmountCredit;
                                                                                        _locAPL.Save();
                                                                                        _locAPL.Session.CommitTransaction();

                                                                                    }
                                                                                }
                                                                            }
                                                                            #endregion COA
                                                                        }
                                                                    }
                                                                }
                                                            }

                                                            #endregion AmountDN
                                                            #region AmountCN
                                                            if (_locTotalAmountCollCCN > 0)
                                                            {
                                                                if (_locJournalMapLine.AccountMap.PostingType == PostingType.Sales && _locJournalMapLine.AccountMap.OrderType == OrderType.Item
                                                                && _locJournalMapLine.AccountMap.PostingMethod == PostingMethod.Bill && _locJournalMapLine.AccountMap.PostingMethodType == PostingMethodType.AmountCN)
                                                                {
                                                                    XPCollection<AccountMapLine> _locAccountMapLines = new XPCollection<AccountMapLine>
                                                                                                                   (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                                    new BinaryOperator("Company", _locReceivableTransactionLine.Company),
                                                                                                                    new BinaryOperator("Workplace", _locReceivableTransactionLine.Workplace),
                                                                                                                    new BinaryOperator("AccountMap", _locJournalMapLine.AccountMap),
                                                                                                                    new BinaryOperator("Active", true)));

                                                                    if (_locAccountMapLines != null && _locAccountMapLines.Count() > 0)
                                                                    {
                                                                        double _locTotalAmountDebit = 0;
                                                                        double _locTotalAmountCredit = 0;
                                                                        double _locTotalBalance = 0;

                                                                        foreach (AccountMapLine _locAccountMapLine in _locAccountMapLines)
                                                                        {
                                                                            if (_locAccountMapLine.AccountCharge == AccountCharge.Debit)
                                                                            {
                                                                                _locTotalAmountDebit = _locTotalAmountCollCCN;
                                                                            }
                                                                            if (_locAccountMapLine.AccountCharge == AccountCharge.Credit)
                                                                            {
                                                                                _locTotalAmountCredit = _locTotalAmountCollCCN;
                                                                            }

                                                                            GeneralJournal _saveGeneralJournal = new GeneralJournal(_currSession)
                                                                            {
                                                                                Company = _locReceivableTransactionLine.Company,
                                                                                Workplace = _locReceivableTransactionLine.Workplace,
                                                                                PostingDate = now,
                                                                                PostingType = PostingType.Sales,
                                                                                OrderType = OrderType.Item,
                                                                                PostingMethod = PostingMethod.Bill,
                                                                                PostingMethodType = PostingMethodType.AmountCN,
                                                                                Account = _locAccountMapLine.Account,
                                                                                Debit = _locTotalAmountDebit,
                                                                                Credit = _locTotalAmountCredit,
                                                                                JournalMonth = _locReceivableTransactionLine.JournalMonth,
                                                                                JournalYear = _locReceivableTransactionLine.JournalYear,
                                                                                ReceivableTransaction = _locReceivableTransactionXPO,
                                                                            };

                                                                            _saveGeneralJournal.Save();
                                                                            _saveGeneralJournal.Session.CommitTransaction();

                                                                            #region AccountingPeriodicLine

                                                                            if (_locAccountMapLine.Account.Code != null)
                                                                            {

                                                                                //Cari Account Periodic Line
                                                                                AccountingPeriodicLine _locAPL = _currSession.FindObject<AccountingPeriodicLine>
                                                                                                                (new GroupOperator(GroupOperatorType.And,
                                                                                                                new BinaryOperator("Company", _locReceivableTransactionLine.Company),
                                                                                                                new BinaryOperator("Workplace", _locReceivableTransactionLine.Workplace),
                                                                                                                new BinaryOperator("AccountNo", _locAccountMapLine.Account),
                                                                                                                new BinaryOperator("Month", _locReceivableTransactionLine.JournalMonth),
                                                                                                                new BinaryOperator("Year", _locReceivableTransactionLine.JournalYear)));
                                                                                if (_locAPL != null)
                                                                                {
                                                                                    if (_locAPL.AccountNo != null)
                                                                                    {
                                                                                        if (_locAPL.AccountNo.BalanceType == BalanceType.Change)
                                                                                        {
                                                                                            if (_locAPL.AccountNo.AccountCharge == AccountCharge.Debit)
                                                                                            {
                                                                                                if (_locTotalAmountDebit > 0)
                                                                                                {
                                                                                                    _locTotalBalance = _locAPL.Balance + _locTotalAmountDebit;
                                                                                                }
                                                                                                if (_locTotalAmountCredit > 0)
                                                                                                {
                                                                                                    _locTotalBalance = _locAPL.Balance - _locTotalAmountCredit;
                                                                                                }
                                                                                            }
                                                                                            if (_locAPL.AccountNo.AccountCharge == AccountCharge.Credit)
                                                                                            {
                                                                                                if (_locTotalAmountDebit > 0)
                                                                                                {
                                                                                                    _locTotalBalance = _locAPL.Balance - _locTotalAmountDebit;
                                                                                                }
                                                                                                if (_locTotalAmountCredit > 0)
                                                                                                {
                                                                                                    _locTotalBalance = _locAPL.Balance + _locTotalAmountCredit;
                                                                                                }
                                                                                            }
                                                                                        }

                                                                                        _locAPL.Balance = _locTotalBalance;
                                                                                        _locAPL.Debit = _locAPL.Debit + _locTotalAmountDebit;
                                                                                        _locAPL.Credit = _locAPL.Credit + _locTotalAmountCredit;
                                                                                        _locAPL.Save();
                                                                                        _locAPL.Session.CommitTransaction();

                                                                                    }
                                                                                }
                                                                            }
                                                                            #endregion COA
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                            #endregion AmountCN
                                                            #region OverPayment
                                                            if(_locTotalAmountCollCOP > 0)
                                                            {
                                                                if (_locJournalMapLine.AccountMap.PostingType == PostingType.Sales && _locJournalMapLine.AccountMap.OrderType == OrderType.Item
                                                                && _locJournalMapLine.AccountMap.PostingMethod == PostingMethod.Bill && _locJournalMapLine.AccountMap.PostingMethodType == PostingMethodType.OverPayment)
                                                                {
                                                                    XPCollection<AccountMapLine> _locAccountMapLines = new XPCollection<AccountMapLine>
                                                                                                                   (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                                    new BinaryOperator("Company", _locReceivableTransactionLine.Company),
                                                                                                                    new BinaryOperator("Workplace", _locReceivableTransactionLine.Workplace),
                                                                                                                    new BinaryOperator("AccountMap", _locJournalMapLine.AccountMap),
                                                                                                                    new BinaryOperator("Active", true)));

                                                                    if (_locAccountMapLines != null && _locAccountMapLines.Count() > 0)
                                                                    {
                                                                        double _locTotalAmountDebit = 0;
                                                                        double _locTotalAmountCredit = 0;
                                                                        double _locTotalBalance = 0;

                                                                        foreach (AccountMapLine _locAccountMapLine in _locAccountMapLines)
                                                                        {
                                                                            if (_locAccountMapLine.AccountCharge == AccountCharge.Debit)
                                                                            {
                                                                                _locTotalAmountDebit = _locTotalAmountCollCOP;
                                                                            }
                                                                            if (_locAccountMapLine.AccountCharge == AccountCharge.Credit)
                                                                            {
                                                                                _locTotalAmountCredit = _locTotalAmountCollCOP;
                                                                            }

                                                                            GeneralJournal _saveGeneralJournal = new GeneralJournal(_currSession)
                                                                            {
                                                                                Company = _locReceivableTransactionLine.Company,
                                                                                Workplace = _locReceivableTransactionLine.Workplace,
                                                                                PostingDate = now,
                                                                                PostingType = PostingType.Sales,
                                                                                OrderType = OrderType.Item,
                                                                                PostingMethod = PostingMethod.Bill,
                                                                                PostingMethodType = PostingMethodType.OverPayment,
                                                                                Account = _locAccountMapLine.Account,
                                                                                Debit = _locTotalAmountDebit,
                                                                                Credit = _locTotalAmountCredit,
                                                                                JournalMonth = _locReceivableTransactionLine.JournalMonth,
                                                                                JournalYear = _locReceivableTransactionLine.JournalYear,
                                                                                ReceivableTransaction = _locReceivableTransactionXPO,
                                                                            };

                                                                            _saveGeneralJournal.Save();
                                                                            _saveGeneralJournal.Session.CommitTransaction();

                                                                            #region AccountingPeriodicLine

                                                                            if (_locAccountMapLine.Account.Code != null)
                                                                            {

                                                                                //Cari Account Periodic Line
                                                                                AccountingPeriodicLine _locAPL = _currSession.FindObject<AccountingPeriodicLine>
                                                                                                                (new GroupOperator(GroupOperatorType.And,
                                                                                                                new BinaryOperator("Company", _locReceivableTransactionLine.Company),
                                                                                                                new BinaryOperator("Workplace", _locReceivableTransactionLine.Workplace),
                                                                                                                new BinaryOperator("AccountNo", _locAccountMapLine.Account),
                                                                                                                new BinaryOperator("Month", _locReceivableTransactionLine.JournalMonth),
                                                                                                                new BinaryOperator("Year", _locReceivableTransactionLine.JournalYear)));
                                                                                if (_locAPL != null)
                                                                                {
                                                                                    if (_locAPL.AccountNo != null)
                                                                                    {
                                                                                        if (_locAPL.AccountNo.BalanceType == BalanceType.Change)
                                                                                        {
                                                                                            if (_locAPL.AccountNo.AccountCharge == AccountCharge.Debit)
                                                                                            {
                                                                                                if (_locTotalAmountDebit > 0)
                                                                                                {
                                                                                                    _locTotalBalance = _locAPL.Balance + _locTotalAmountDebit;
                                                                                                }
                                                                                                if (_locTotalAmountCredit > 0)
                                                                                                {
                                                                                                    _locTotalBalance = _locAPL.Balance - _locTotalAmountCredit;
                                                                                                }
                                                                                            }
                                                                                            if (_locAPL.AccountNo.AccountCharge == AccountCharge.Credit)
                                                                                            {
                                                                                                if (_locTotalAmountDebit > 0)
                                                                                                {
                                                                                                    _locTotalBalance = _locAPL.Balance - _locTotalAmountDebit;
                                                                                                }
                                                                                                if (_locTotalAmountCredit > 0)
                                                                                                {
                                                                                                    _locTotalBalance = _locAPL.Balance + _locTotalAmountCredit;
                                                                                                }
                                                                                            }
                                                                                        }

                                                                                        _locAPL.Balance = _locTotalBalance;
                                                                                        _locAPL.Debit = _locAPL.Debit + _locTotalAmountDebit;
                                                                                        _locAPL.Credit = _locAPL.Credit + _locTotalAmountCredit;
                                                                                        _locAPL.Save();
                                                                                        _locAPL.Session.CommitTransaction();

                                                                                    }
                                                                                }
                                                                            }
                                                                            #endregion COA
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                            #endregion OverPayment
                                                            #region LowerPayment
                                                            if(_locTotalAmountCollCLP > 0 && _locReceivableTransactionLine.Settled == true)
                                                            {
                                                                if (_locJournalMapLine.AccountMap.PostingType == PostingType.Sales && _locJournalMapLine.AccountMap.OrderType == OrderType.Item
                                                                && _locJournalMapLine.AccountMap.PostingMethod == PostingMethod.Bill && _locJournalMapLine.AccountMap.PostingMethodType == PostingMethodType.LowerPayment)
                                                                {
                                                                    XPCollection<AccountMapLine> _locAccountMapLines = new XPCollection<AccountMapLine>
                                                                                                                   (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                                    new BinaryOperator("Company", _locReceivableTransactionLine.Company),
                                                                                                                    new BinaryOperator("Workplace", _locReceivableTransactionLine.Workplace),
                                                                                                                    new BinaryOperator("AccountMap", _locJournalMapLine.AccountMap),
                                                                                                                    new BinaryOperator("Active", true)));

                                                                    if (_locAccountMapLines != null && _locAccountMapLines.Count() > 0)
                                                                    {
                                                                        double _locTotalAmountDebit = 0;
                                                                        double _locTotalAmountCredit = 0;
                                                                        double _locTotalBalance = 0;

                                                                        foreach (AccountMapLine _locAccountMapLine in _locAccountMapLines)
                                                                        {
                                                                            if (_locAccountMapLine.AccountCharge == AccountCharge.Debit)
                                                                            {
                                                                                _locTotalAmountDebit = _locTotalAmountCollCOP;
                                                                            }
                                                                            if (_locAccountMapLine.AccountCharge == AccountCharge.Credit)
                                                                            {
                                                                                _locTotalAmountCredit = _locTotalAmountCollCOP;
                                                                            }

                                                                            GeneralJournal _saveGeneralJournal = new GeneralJournal(_currSession)
                                                                            {
                                                                                Company = _locReceivableTransactionLine.Company,
                                                                                Workplace = _locReceivableTransactionLine.Workplace,
                                                                                PostingDate = now,
                                                                                PostingType = PostingType.Sales,
                                                                                OrderType = OrderType.Item,
                                                                                PostingMethod = PostingMethod.Bill,
                                                                                PostingMethodType = PostingMethodType.LowerPayment,
                                                                                Account = _locAccountMapLine.Account,
                                                                                Debit = _locTotalAmountDebit,
                                                                                Credit = _locTotalAmountCredit,
                                                                                JournalMonth = _locReceivableTransactionLine.JournalMonth,
                                                                                JournalYear = _locReceivableTransactionLine.JournalYear,
                                                                                ReceivableTransaction = _locReceivableTransactionXPO,
                                                                            };

                                                                            _saveGeneralJournal.Save();
                                                                            _saveGeneralJournal.Session.CommitTransaction();

                                                                            #region AccountingPeriodicLine

                                                                            if (_locAccountMapLine.Account.Code != null)
                                                                            {

                                                                                //Cari Account Periodic Line
                                                                                AccountingPeriodicLine _locAPL = _currSession.FindObject<AccountingPeriodicLine>
                                                                                                                (new GroupOperator(GroupOperatorType.And,
                                                                                                                new BinaryOperator("Company", _locReceivableTransactionLine.Company),
                                                                                                                new BinaryOperator("Workplace", _locReceivableTransactionLine.Workplace),
                                                                                                                new BinaryOperator("AccountNo", _locAccountMapLine.Account),
                                                                                                                new BinaryOperator("Month", _locReceivableTransactionLine.JournalMonth),
                                                                                                                new BinaryOperator("Year", _locReceivableTransactionLine.JournalYear)));
                                                                                if (_locAPL != null)
                                                                                {
                                                                                    if (_locAPL.AccountNo != null)
                                                                                    {
                                                                                        if (_locAPL.AccountNo.BalanceType == BalanceType.Change)
                                                                                        {
                                                                                            if (_locAPL.AccountNo.AccountCharge == AccountCharge.Debit)
                                                                                            {
                                                                                                if (_locTotalAmountDebit > 0)
                                                                                                {
                                                                                                    _locTotalBalance = _locAPL.Balance + _locTotalAmountDebit;
                                                                                                }
                                                                                                if (_locTotalAmountCredit > 0)
                                                                                                {
                                                                                                    _locTotalBalance = _locAPL.Balance - _locTotalAmountCredit;
                                                                                                }
                                                                                            }
                                                                                            if (_locAPL.AccountNo.AccountCharge == AccountCharge.Credit)
                                                                                            {
                                                                                                if (_locTotalAmountDebit > 0)
                                                                                                {
                                                                                                    _locTotalBalance = _locAPL.Balance - _locTotalAmountDebit;
                                                                                                }
                                                                                                if (_locTotalAmountCredit > 0)
                                                                                                {
                                                                                                    _locTotalBalance = _locAPL.Balance + _locTotalAmountCredit;
                                                                                                }
                                                                                            }
                                                                                        }

                                                                                        _locAPL.Balance = _locTotalBalance;
                                                                                        _locAPL.Debit = _locAPL.Debit + _locTotalAmountDebit;
                                                                                        _locAPL.Credit = _locAPL.Credit + _locTotalAmountCredit;
                                                                                        _locAPL.Save();
                                                                                        _locAPL.Session.CommitTransaction();

                                                                                    }
                                                                                }
                                                                            }
                                                                            #endregion COA
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                            #endregion LowerPayment
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    } 
                                }
                            }
                            #endregion JournalMapCompanyAccountGroup

                            #endregion NonBankAccount

                            #region BankAccount
                            if (_locReceivableTransactionLine.MultiBank == true)
                            {
                                XPCollection<ReceivableTransactionBank> _locReceivableTransactionBanks = new XPCollection<ReceivableTransactionBank>(_currSession,
                                                                                                         new GroupOperator(GroupOperatorType.And,
                                                                                                         new BinaryOperator("ReceivableTransactionLine", _locReceivableTransactionLine),
                                                                                                         new GroupOperator(GroupOperatorType.Or,
                                                                                                         new BinaryOperator("Status", Status.Open),
                                                                                                         new BinaryOperator("Status", Status.Progress),
                                                                                                         new BinaryOperator("Status", Status.Posted))));
                                if (_locReceivableTransactionBanks != null && _locReceivableTransactionBanks.Count() > 0)
                                {
                                    foreach (ReceivableTransactionBank _locReceivableTransactionBank in _locReceivableTransactionBanks)
                                    {
                                        #region JournalMapBusinessPartnerAccountGroup
                                        if (_locReceivableTransactionBank.BankAccount != null)
                                        {
                                            BankAccountMap _locBankAccountMap = _currSession.FindObject<BankAccountMap>(new GroupOperator(GroupOperatorType.And,
                                                                                    new BinaryOperator("Company", _locReceivableTransactionLine.Company),
                                                                                    new BinaryOperator("Workplace", _locReceivableTransactionLine.Workplace),
                                                                                    new BinaryOperator("BankAccount", _locReceivableTransactionBank.BankAccount),
                                                                                    new BinaryOperator("Active", true)));

                                            if(_locBankAccountMap != null)
                                            {
                                                if (_locBankAccountMap.BankAccountGroup != null)
                                                {
                                                    _locBankAccountGroup = _locBankAccountMap.BankAccountGroup;

                                                    _locTotalAmountCollBP = _locReceivableTransactionBank.Amount;

                                                    XPCollection<JournalMap> _locJournalMapItems = new XPCollection<JournalMap>
                                                                                       (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                        new BinaryOperator("Company", _locReceivableTransactionBank.Company),
                                                                                        new BinaryOperator("Workplace", _locReceivableTransactionBank.Workplace),
                                                                                        new BinaryOperator("BankAccountGroup", _locBankAccountGroup),
                                                                                        new BinaryOperator("Active", true)));

                                                    if (_locJournalMapItems != null && _locJournalMapItems.Count() > 0)
                                                    {
                                                        foreach (JournalMap _locJournalMapItem in _locJournalMapItems)
                                                        {
                                                            XPCollection<JournalMapLine> _locJournalMapLines = new XPCollection<JournalMapLine>
                                                                                                               (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                                new BinaryOperator("Company", _locReceivableTransactionBank.Company),
                                                                                                                new BinaryOperator("Workplace", _locReceivableTransactionBank.Workplace),
                                                                                                                new BinaryOperator("JournalMap", _locJournalMapItem),
                                                                                                                new BinaryOperator("Active", true)));

                                                            if (_locJournalMapLines != null && _locJournalMapLines.Count() > 0)
                                                            {
                                                                foreach (JournalMapLine _locJournalMapLine in _locJournalMapLines)
                                                                {
                                                                    if (_locJournalMapLine.AccountMap != null)
                                                                    {
                                                                        #region NormalAmount
                                                                        if (_locJournalMapLine.AccountMap.PostingType == PostingType.Sales && _locJournalMapLine.AccountMap.OrderType == OrderType.Item
                                                                            && _locJournalMapLine.AccountMap.PostingMethod == PostingMethod.Bill && _locJournalMapLine.AccountMap.PostingMethodType == PostingMethodType.Normal)
                                                                        {
                                                                            XPCollection<AccountMapLine> _locAccountMapLines = new XPCollection<AccountMapLine>
                                                                                                                           (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                                            new BinaryOperator("Company", _locReceivableTransactionBank.Company),
                                                                                                                            new BinaryOperator("Workplace", _locReceivableTransactionBank.Workplace),
                                                                                                                            new BinaryOperator("AccountMap", _locJournalMapLine.AccountMap),
                                                                                                                            new BinaryOperator("Active", true)));

                                                                            if (_locAccountMapLines != null && _locAccountMapLines.Count() > 0)
                                                                            {
                                                                                double _locTotalAmountDebit = 0;
                                                                                double _locTotalAmountCredit = 0;
                                                                                double _locTotalBalance = 0;

                                                                                foreach (AccountMapLine _locAccountMapLine in _locAccountMapLines)
                                                                                {
                                                                                    if (_locAccountMapLine.AccountCharge == AccountCharge.Debit)
                                                                                    {
                                                                                        _locTotalAmountDebit = _locTotalAmountCollBP;
                                                                                    }
                                                                                    if (_locAccountMapLine.AccountCharge == AccountCharge.Credit)
                                                                                    {
                                                                                        _locTotalAmountCredit = _locTotalAmountCollBP;
                                                                                    }

                                                                                    GeneralJournal _saveGeneralJournal = new GeneralJournal(_currSession)
                                                                                    {
                                                                                        Company = _locReceivableTransactionBank.Company,
                                                                                        Workplace = _locReceivableTransactionBank.Workplace,
                                                                                        PostingDate = now,
                                                                                        PostingType = PostingType.Sales,
                                                                                        OrderType = OrderType.Item,
                                                                                        PostingMethod = PostingMethod.Bill,
                                                                                        PostingMethodType = PostingMethodType.Normal,
                                                                                        Account = _locAccountMapLine.Account,
                                                                                        Debit = _locTotalAmountDebit,
                                                                                        Credit = _locTotalAmountCredit,
                                                                                        JournalMonth = _locReceivableTransactionLine.JournalMonth,
                                                                                        JournalYear = _locReceivableTransactionLine.JournalYear,
                                                                                        ReceivableTransaction = _locReceivableTransactionXPO,
                                                                                    };

                                                                                    _saveGeneralJournal.Save();
                                                                                    _saveGeneralJournal.Session.CommitTransaction();

                                                                                    #region AccountingPeriodicLine

                                                                                    if (_locAccountMapLine.Account.Code != null)
                                                                                    {

                                                                                        //Cari Account Periodic Line
                                                                                        AccountingPeriodicLine _locAPL = _currSession.FindObject<AccountingPeriodicLine>
                                                                                                                        (new GroupOperator(GroupOperatorType.And,
                                                                                                                        new BinaryOperator("Company", _locReceivableTransactionBank.Company),
                                                                                                                        new BinaryOperator("Workplace", _locReceivableTransactionBank.Workplace),
                                                                                                                        new BinaryOperator("AccountNo", _locAccountMapLine.Account),
                                                                                                                        new BinaryOperator("Month", _locReceivableTransactionLine.JournalMonth),
                                                                                                                        new BinaryOperator("Year", _locReceivableTransactionLine.JournalYear)));
                                                                                        if (_locAPL != null)
                                                                                        {
                                                                                            if (_locAPL.AccountNo != null)
                                                                                            {
                                                                                                if (_locAPL.AccountNo.BalanceType == BalanceType.Change)
                                                                                                {
                                                                                                    if (_locAPL.AccountNo.AccountCharge == AccountCharge.Debit)
                                                                                                    {
                                                                                                        if (_locTotalAmountDebit > 0)
                                                                                                        {
                                                                                                            _locTotalBalance = _locAPL.Balance + _locTotalAmountDebit;
                                                                                                        }
                                                                                                        if (_locTotalAmountCredit > 0)
                                                                                                        {
                                                                                                            _locTotalBalance = _locAPL.Balance - _locTotalAmountCredit;
                                                                                                        }
                                                                                                    }
                                                                                                    if (_locAPL.AccountNo.AccountCharge == AccountCharge.Credit)
                                                                                                    {
                                                                                                        if (_locTotalAmountDebit > 0)
                                                                                                        {
                                                                                                            _locTotalBalance = _locAPL.Balance - _locTotalAmountDebit;
                                                                                                        }
                                                                                                        if (_locTotalAmountCredit > 0)
                                                                                                        {
                                                                                                            _locTotalBalance = _locAPL.Balance + _locTotalAmountCredit;
                                                                                                        }
                                                                                                    }
                                                                                                }

                                                                                                _locAPL.Balance = _locTotalBalance;
                                                                                                _locAPL.Debit = _locAPL.Debit + _locTotalAmountDebit;
                                                                                                _locAPL.Credit = _locAPL.Credit + _locTotalAmountCredit;
                                                                                                _locAPL.Save();
                                                                                                _locAPL.Session.CommitTransaction();

                                                                                            }
                                                                                        }
                                                                                    }
                                                                                    #endregion COA
                                                                                }
                                                                            }
                                                                        }
                                                                        #endregion NormalAmount   
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        #endregion JournalMapBusinessPartnerAccountGroup

                                        #region JournalMapCompanyAccountGroup
                                        if (_locReceivableTransactionBank.CompanyBankAccount != null)
                                        {
                                            BankAccountMap _locBankAccountMap = _currSession.FindObject<BankAccountMap>(new GroupOperator(GroupOperatorType.And,
                                                                                    new BinaryOperator("Company", _locReceivableTransactionLine.Company),
                                                                                    new BinaryOperator("Workplace", _locReceivableTransactionLine.Workplace),
                                                                                    new BinaryOperator("BankAccount", _locReceivableTransactionBank.CompanyBankAccount),
                                                                                    new BinaryOperator("Active", true)));
                                            if(_locBankAccountMap != null)
                                            {
                                                if (_locBankAccountMap.BankAccountGroup != null)
                                                {
                                                    _locTotalAmountCollC = _locReceivableTransactionBank.Amount;
                                                    _locCompanyBankAccountGroup = _locBankAccountMap.BankAccountGroup;

                                                    XPCollection<JournalMap> _locJournalMapItems = new XPCollection<JournalMap>
                                                                                   (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                    new BinaryOperator("Company", _locReceivableTransactionBank.Company),
                                                                                    new BinaryOperator("Workplace", _locReceivableTransactionBank.Workplace),
                                                                                    new BinaryOperator("BankAccountGroup", _locCompanyBankAccountGroup),
                                                                                    new BinaryOperator("Active", true)));

                                                    if (_locJournalMapItems != null && _locJournalMapItems.Count() > 0)
                                                    {
                                                        foreach (JournalMap _locJournalMapItem in _locJournalMapItems)
                                                        {
                                                            XPCollection<JournalMapLine> _locJournalMapLines = new XPCollection<JournalMapLine>
                                                                                                               (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                                new BinaryOperator("Company", _locReceivableTransactionBank.Company),
                                                                                                                new BinaryOperator("Workplace", _locReceivableTransactionBank.Workplace),
                                                                                                                new BinaryOperator("JournalMap", _locJournalMapItem),
                                                                                                                new BinaryOperator("Active", true)));

                                                            if (_locJournalMapLines != null && _locJournalMapLines.Count() > 0)
                                                            {
                                                                foreach (JournalMapLine _locJournalMapLine in _locJournalMapLines)
                                                                {
                                                                    if (_locJournalMapLine.AccountMap != null)
                                                                    {
                                                                        #region NormalAmount
                                                                        if (_locJournalMapLine.AccountMap.PostingType == PostingType.Sales && _locJournalMapLine.AccountMap.OrderType == OrderType.Item
                                                                            && _locJournalMapLine.AccountMap.PostingMethod == PostingMethod.Bill && _locJournalMapLine.AccountMap.PostingMethodType == PostingMethodType.Normal)
                                                                        {
                                                                            XPCollection<AccountMapLine> _locAccountMapLines = new XPCollection<AccountMapLine>
                                                                                                                           (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                                            new BinaryOperator("Company", _locReceivableTransactionBank.Company),
                                                                                                                            new BinaryOperator("Workplace", _locReceivableTransactionBank.Workplace),
                                                                                                                            new BinaryOperator("AccountMap", _locJournalMapLine.AccountMap),
                                                                                                                            new BinaryOperator("Active", true)));

                                                                            if (_locAccountMapLines != null && _locAccountMapLines.Count() > 0)
                                                                            {
                                                                                double _locTotalAmountDebit = 0;
                                                                                double _locTotalAmountCredit = 0;
                                                                                double _locTotalBalance = 0;

                                                                                foreach (AccountMapLine _locAccountMapLine in _locAccountMapLines)
                                                                                {
                                                                                    if (_locAccountMapLine.AccountCharge == AccountCharge.Debit)
                                                                                    {
                                                                                        _locTotalAmountDebit = _locTotalAmountCollC;
                                                                                    }
                                                                                    if (_locAccountMapLine.AccountCharge == AccountCharge.Credit)
                                                                                    {
                                                                                        _locTotalAmountCredit = _locTotalAmountCollC;
                                                                                    }

                                                                                    GeneralJournal _saveGeneralJournal = new GeneralJournal(_currSession)
                                                                                    {
                                                                                        Company = _locReceivableTransactionBank.Company,
                                                                                        Workplace = _locReceivableTransactionBank.Workplace,
                                                                                        PostingDate = now,
                                                                                        PostingType = PostingType.Sales,
                                                                                        OrderType = OrderType.Item,
                                                                                        PostingMethod = PostingMethod.Bill,
                                                                                        PostingMethodType = PostingMethodType.Normal,
                                                                                        Account = _locAccountMapLine.Account,
                                                                                        Debit = _locTotalAmountDebit,
                                                                                        Credit = _locTotalAmountCredit,
                                                                                        JournalMonth = _locReceivableTransactionLine.JournalMonth,
                                                                                        JournalYear = _locReceivableTransactionLine.JournalYear,
                                                                                        ReceivableTransaction = _locReceivableTransactionXPO,
                                                                                    };

                                                                                    _saveGeneralJournal.Save();
                                                                                    _saveGeneralJournal.Session.CommitTransaction();

                                                                                    #region AccountingPeriodicLine

                                                                                    if (_locAccountMapLine.Account.Code != null)
                                                                                    {

                                                                                        //Cari Account Periodic Line
                                                                                        AccountingPeriodicLine _locAPL = _currSession.FindObject<AccountingPeriodicLine>
                                                                                                                        (new GroupOperator(GroupOperatorType.And,
                                                                                                                        new BinaryOperator("Company", _locReceivableTransactionBank.Company),
                                                                                                                        new BinaryOperator("Workplace", _locReceivableTransactionBank.Workplace),
                                                                                                                        new BinaryOperator("AccountNo", _locAccountMapLine.Account),
                                                                                                                        new BinaryOperator("Month", _locReceivableTransactionLine.JournalMonth),
                                                                                                                        new BinaryOperator("Year", _locReceivableTransactionLine.JournalYear)));
                                                                                        if (_locAPL != null)
                                                                                        {
                                                                                            if (_locAPL.AccountNo != null)
                                                                                            {
                                                                                                if (_locAPL.AccountNo.BalanceType == BalanceType.Change)
                                                                                                {
                                                                                                    if (_locAPL.AccountNo.AccountCharge == AccountCharge.Debit)
                                                                                                    {
                                                                                                        if (_locTotalAmountDebit > 0)
                                                                                                        {
                                                                                                            _locTotalBalance = _locAPL.Balance + _locTotalAmountDebit;
                                                                                                        }
                                                                                                        if (_locTotalAmountCredit > 0)
                                                                                                        {
                                                                                                            _locTotalBalance = _locAPL.Balance - _locTotalAmountCredit;
                                                                                                        }
                                                                                                    }
                                                                                                    if (_locAPL.AccountNo.AccountCharge == AccountCharge.Credit)
                                                                                                    {
                                                                                                        if (_locTotalAmountDebit > 0)
                                                                                                        {
                                                                                                            _locTotalBalance = _locAPL.Balance - _locTotalAmountDebit;
                                                                                                        }
                                                                                                        if (_locTotalAmountCredit > 0)
                                                                                                        {
                                                                                                            _locTotalBalance = _locAPL.Balance + _locTotalAmountCredit;
                                                                                                        }
                                                                                                    }
                                                                                                }

                                                                                                _locAPL.Balance = _locTotalBalance;
                                                                                                _locAPL.Debit = _locAPL.Debit + _locTotalAmountDebit;
                                                                                                _locAPL.Credit = _locAPL.Credit + _locTotalAmountCredit;
                                                                                                _locAPL.Save();
                                                                                                _locAPL.Session.CommitTransaction();

                                                                                            }
                                                                                        }
                                                                                    }
                                                                                    #endregion COA
                                                                                }
                                                                            }
                                                                        }
                                                                        #endregion NormalAmount 
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            } 
                                        }

                                        #endregion JournalMapCompanyAccountGroup
                                    }
                                }
                            }
                            else
                            {
                                #region JournalMapBusinessPartnerAccountGroup

                                if (_locReceivableTransactionLine.BankAccount != null)
                                {
                                    BankAccountMap _locBankAccountMap = _currSession.FindObject<BankAccountMap>(new GroupOperator(GroupOperatorType.And,
                                                                                    new BinaryOperator("Company", _locReceivableTransactionLine.Company),
                                                                                    new BinaryOperator("Workplace", _locReceivableTransactionLine.Workplace),
                                                                                    new BinaryOperator("BankAccount", _locReceivableTransactionLine.BankAccount),
                                                                                    new BinaryOperator("Active", true)));
                                    if(_locBankAccountMap != null)
                                    {
                                        if (_locBankAccountMap.BankAccountGroup != null)
                                        {
                                            _locBankAccountGroup = _locBankAccountMap.BankAccountGroup;
                                            _locTotalAmountCollBP = _locReceivableTransactionLine.Amount;


                                            XPCollection<JournalMap> _locJournalMapItems = new XPCollection<JournalMap>
                                                                               (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                new BinaryOperator("Company", _locReceivableTransactionLine.Company),
                                                                                new BinaryOperator("Workplace", _locReceivableTransactionLine.Workplace),
                                                                                new BinaryOperator("BankAccountGroup", _locBankAccountGroup),
                                                                                new BinaryOperator("Active", true)));

                                            if (_locJournalMapItems != null && _locJournalMapItems.Count() > 0)
                                            {
                                                foreach (JournalMap _locJournalMapItem in _locJournalMapItems)
                                                {
                                                    XPCollection<JournalMapLine> _locJournalMapLines = new XPCollection<JournalMapLine>
                                                                                                       (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                        new BinaryOperator("Company", _locReceivableTransactionLine.Company),
                                                                                                        new BinaryOperator("Workplace", _locReceivableTransactionLine.Workplace),
                                                                                                        new BinaryOperator("JournalMap", _locJournalMapItem),
                                                                                                        new BinaryOperator("Active", true)));

                                                    if (_locJournalMapLines != null && _locJournalMapLines.Count() > 0)
                                                    {
                                                        foreach (JournalMapLine _locJournalMapLine in _locJournalMapLines)
                                                        {
                                                            if (_locJournalMapLine.AccountMap != null)
                                                            {
                                                                #region NormalAmount
                                                                if (_locJournalMapLine.AccountMap.PostingType == PostingType.Sales && _locJournalMapLine.AccountMap.OrderType == OrderType.Item
                                                                    && _locJournalMapLine.AccountMap.PostingMethod == PostingMethod.Bill && _locJournalMapLine.AccountMap.PostingMethodType == PostingMethodType.Normal)
                                                                {
                                                                    XPCollection<AccountMapLine> _locAccountMapLines = new XPCollection<AccountMapLine>
                                                                                                                   (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                                    new BinaryOperator("Company", _locReceivableTransactionLine.Company),
                                                                                                                    new BinaryOperator("Workplace", _locReceivableTransactionLine.Workplace),
                                                                                                                    new BinaryOperator("AccountMap", _locJournalMapLine.AccountMap),
                                                                                                                    new BinaryOperator("Active", true)));

                                                                    if (_locAccountMapLines != null && _locAccountMapLines.Count() > 0)
                                                                    {
                                                                        double _locTotalAmountDebit = 0;
                                                                        double _locTotalAmountCredit = 0;
                                                                        double _locTotalBalance = 0;

                                                                        foreach (AccountMapLine _locAccountMapLine in _locAccountMapLines)
                                                                        {
                                                                            if (_locAccountMapLine.AccountCharge == AccountCharge.Debit)
                                                                            {
                                                                                _locTotalAmountDebit = _locTotalAmountCollBP;
                                                                            }
                                                                            if (_locAccountMapLine.AccountCharge == AccountCharge.Credit)
                                                                            {
                                                                                _locTotalAmountCredit = _locTotalAmountCollBP;
                                                                            }

                                                                            GeneralJournal _saveGeneralJournal = new GeneralJournal(_currSession)
                                                                            {
                                                                                Company = _locReceivableTransactionLine.Company,
                                                                                Workplace = _locReceivableTransactionLine.Workplace,
                                                                                PostingDate = now,
                                                                                PostingType = PostingType.Sales,
                                                                                OrderType = OrderType.Item,
                                                                                PostingMethod = PostingMethod.Bill,
                                                                                PostingMethodType = PostingMethodType.Normal,
                                                                                Account = _locAccountMapLine.Account,
                                                                                Debit = _locTotalAmountDebit,
                                                                                Credit = _locTotalAmountCredit,
                                                                                JournalMonth = _locReceivableTransactionLine.JournalMonth,
                                                                                JournalYear = _locReceivableTransactionLine.JournalYear,
                                                                                ReceivableTransaction = _locReceivableTransactionXPO,
                                                                            };

                                                                            _saveGeneralJournal.Save();
                                                                            _saveGeneralJournal.Session.CommitTransaction();

                                                                            #region AccountingPeriodicLine

                                                                            if (_locAccountMapLine.Account.Code != null)
                                                                            {

                                                                                //Cari Account Periodic Line
                                                                                AccountingPeriodicLine _locAPL = _currSession.FindObject<AccountingPeriodicLine>
                                                                                                                (new GroupOperator(GroupOperatorType.And,
                                                                                                                new BinaryOperator("Company", _locReceivableTransactionLine.Company),
                                                                                                                new BinaryOperator("Workplace", _locReceivableTransactionLine.Workplace),
                                                                                                                new BinaryOperator("AccountNo", _locAccountMapLine.Account),
                                                                                                                new BinaryOperator("Month", _locReceivableTransactionLine.JournalMonth),
                                                                                                                new BinaryOperator("Year", _locReceivableTransactionLine.JournalYear)));
                                                                                if (_locAPL != null)
                                                                                {
                                                                                    if (_locAPL.AccountNo != null)
                                                                                    {
                                                                                        if (_locAPL.AccountNo.BalanceType == BalanceType.Change)
                                                                                        {
                                                                                            if (_locAPL.AccountNo.AccountCharge == AccountCharge.Debit)
                                                                                            {
                                                                                                if (_locTotalAmountDebit > 0)
                                                                                                {
                                                                                                    _locTotalBalance = _locAPL.Balance + _locTotalAmountDebit;
                                                                                                }
                                                                                                if (_locTotalAmountCredit > 0)
                                                                                                {
                                                                                                    _locTotalBalance = _locAPL.Balance - _locTotalAmountCredit;
                                                                                                }
                                                                                            }
                                                                                            if (_locAPL.AccountNo.AccountCharge == AccountCharge.Credit)
                                                                                            {
                                                                                                if (_locTotalAmountDebit > 0)
                                                                                                {
                                                                                                    _locTotalBalance = _locAPL.Balance - _locTotalAmountDebit;
                                                                                                }
                                                                                                if (_locTotalAmountCredit > 0)
                                                                                                {
                                                                                                    _locTotalBalance = _locAPL.Balance + _locTotalAmountCredit;
                                                                                                }
                                                                                            }
                                                                                        }

                                                                                        _locAPL.Balance = _locTotalBalance;
                                                                                        _locAPL.Debit = _locAPL.Debit + _locTotalAmountDebit;
                                                                                        _locAPL.Credit = _locAPL.Credit + _locTotalAmountCredit;
                                                                                        _locAPL.Save();
                                                                                        _locAPL.Session.CommitTransaction();

                                                                                    }
                                                                                }
                                                                            }
                                                                            #endregion COA
                                                                        }
                                                                    }
                                                                }
                                                                #endregion NormalAmount   
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    
                                }
                                #endregion JournalMapBusinessPartnerAccountGroup

                                #region JournalMapCompanyAccountGroup
                                if (_locReceivableTransactionLine.CompanyBankAccount != null)
                                {
                                    BankAccountMap _locBankAccountMap = _currSession.FindObject<BankAccountMap>(new GroupOperator(GroupOperatorType.And,
                                                                                    new BinaryOperator("Company", _locReceivableTransactionLine.Company),
                                                                                    new BinaryOperator("Workplace", _locReceivableTransactionLine.Workplace),
                                                                                    new BinaryOperator("BankAccount", _locReceivableTransactionLine.CompanyBankAccount),
                                                                                    new BinaryOperator("Active", true)));
                                    if(_locBankAccountMap != null)
                                    {
                                        if (_locBankAccountMap.BankAccountGroup != null)
                                        {
                                            _locCompanyBankAccountGroup = _locBankAccountMap.BankAccountGroup;
                                            _locTotalAmountCollC = _locReceivableTransactionLine.Amount;

                                            XPCollection<JournalMap> _locJournalMapItems = new XPCollection<JournalMap>
                                                                           (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                            new BinaryOperator("Company", _locReceivableTransactionLine.Company),
                                                                            new BinaryOperator("Workplace", _locReceivableTransactionLine.Workplace),
                                                                            new BinaryOperator("BankAccountGroup", _locCompanyBankAccountGroup),
                                                                            new BinaryOperator("Active", true)));

                                            if (_locJournalMapItems != null && _locJournalMapItems.Count() > 0)
                                            {
                                                foreach (JournalMap _locJournalMapItem in _locJournalMapItems)
                                                {
                                                    XPCollection<JournalMapLine> _locJournalMapLines = new XPCollection<JournalMapLine>
                                                                                                       (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                        new BinaryOperator("Company", _locReceivableTransactionLine.Company),
                                                                                                        new BinaryOperator("Workplace", _locReceivableTransactionLine.Workplace),
                                                                                                        new BinaryOperator("JournalMap", _locJournalMapItem),
                                                                                                        new BinaryOperator("Active", true)));

                                                    if (_locJournalMapLines != null && _locJournalMapLines.Count() > 0)
                                                    {
                                                        foreach (JournalMapLine _locJournalMapLine in _locJournalMapLines)
                                                        {
                                                            if (_locJournalMapLine.AccountMap != null)
                                                            {
                                                                #region NormalAmount
                                                                if (_locJournalMapLine.AccountMap.PostingType == PostingType.Sales && _locJournalMapLine.AccountMap.OrderType == OrderType.Item
                                                                    && _locJournalMapLine.AccountMap.PostingMethod == PostingMethod.Bill && _locJournalMapLine.AccountMap.PostingMethodType == PostingMethodType.Normal)
                                                                {
                                                                    XPCollection<AccountMapLine> _locAccountMapLines = new XPCollection<AccountMapLine>
                                                                                                                   (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                                    new BinaryOperator("Company", _locReceivableTransactionLine.Company),
                                                                                                                    new BinaryOperator("Workplace", _locReceivableTransactionLine.Workplace),
                                                                                                                    new BinaryOperator("AccountMap", _locJournalMapLine.AccountMap),
                                                                                                                    new BinaryOperator("Active", true)));

                                                                    if (_locAccountMapLines != null && _locAccountMapLines.Count() > 0)
                                                                    {
                                                                        double _locTotalAmountDebit = 0;
                                                                        double _locTotalAmountCredit = 0;
                                                                        double _locTotalBalance = 0;

                                                                        foreach (AccountMapLine _locAccountMapLine in _locAccountMapLines)
                                                                        {
                                                                            if (_locAccountMapLine.AccountCharge == AccountCharge.Debit)
                                                                            {
                                                                                _locTotalAmountDebit = _locTotalAmountCollC;
                                                                            }
                                                                            if (_locAccountMapLine.AccountCharge == AccountCharge.Credit)
                                                                            {
                                                                                _locTotalAmountCredit = _locTotalAmountCollC;
                                                                            }

                                                                            GeneralJournal _saveGeneralJournal = new GeneralJournal(_currSession)
                                                                            {
                                                                                Company = _locReceivableTransactionLine.Company,
                                                                                Workplace = _locReceivableTransactionLine.Workplace,
                                                                                PostingDate = now,
                                                                                PostingType = PostingType.Sales,
                                                                                OrderType = OrderType.Item,
                                                                                PostingMethod = PostingMethod.Bill,
                                                                                PostingMethodType = PostingMethodType.Normal,
                                                                                Account = _locAccountMapLine.Account,
                                                                                Debit = _locTotalAmountDebit,
                                                                                Credit = _locTotalAmountCredit,
                                                                                JournalMonth = _locReceivableTransactionLine.JournalMonth,
                                                                                JournalYear = _locReceivableTransactionLine.JournalYear,
                                                                                ReceivableTransaction = _locReceivableTransactionXPO,
                                                                            };

                                                                            _saveGeneralJournal.Save();
                                                                            _saveGeneralJournal.Session.CommitTransaction();

                                                                            #region AccountingPeriodicLine

                                                                            if (_locAccountMapLine.Account.Code != null)
                                                                            {

                                                                                //Cari Account Periodic Line
                                                                                AccountingPeriodicLine _locAPL = _currSession.FindObject<AccountingPeriodicLine>
                                                                                                                (new GroupOperator(GroupOperatorType.And,
                                                                                                                new BinaryOperator("Company", _locReceivableTransactionLine.Company),
                                                                                                                new BinaryOperator("Workplace", _locReceivableTransactionLine.Workplace),
                                                                                                                new BinaryOperator("AccountNo", _locAccountMapLine.Account),
                                                                                                                new BinaryOperator("Month", _locReceivableTransactionLine.JournalMonth),
                                                                                                                new BinaryOperator("Year", _locReceivableTransactionLine.JournalYear)));
                                                                                if (_locAPL != null)
                                                                                {
                                                                                    if (_locAPL.AccountNo != null)
                                                                                    {
                                                                                        if (_locAPL.AccountNo.BalanceType == BalanceType.Change)
                                                                                        {
                                                                                            if (_locAPL.AccountNo.AccountCharge == AccountCharge.Debit)
                                                                                            {
                                                                                                if (_locTotalAmountDebit > 0)
                                                                                                {
                                                                                                    _locTotalBalance = _locAPL.Balance + _locTotalAmountDebit;
                                                                                                }
                                                                                                if (_locTotalAmountCredit > 0)
                                                                                                {
                                                                                                    _locTotalBalance = _locAPL.Balance - _locTotalAmountCredit;
                                                                                                }
                                                                                            }
                                                                                            if (_locAPL.AccountNo.AccountCharge == AccountCharge.Credit)
                                                                                            {
                                                                                                if (_locTotalAmountDebit > 0)
                                                                                                {
                                                                                                    _locTotalBalance = _locAPL.Balance - _locTotalAmountDebit;
                                                                                                }
                                                                                                if (_locTotalAmountCredit > 0)
                                                                                                {
                                                                                                    _locTotalBalance = _locAPL.Balance + _locTotalAmountCredit;
                                                                                                }
                                                                                            }
                                                                                        }

                                                                                        _locAPL.Balance = _locTotalBalance;
                                                                                        _locAPL.Debit = _locAPL.Debit + _locTotalAmountDebit;
                                                                                        _locAPL.Credit = _locAPL.Credit + _locTotalAmountCredit;
                                                                                        _locAPL.Save();
                                                                                        _locAPL.Session.CommitTransaction();

                                                                                    }
                                                                                }
                                                                            }
                                                                            #endregion COA
                                                                        }
                                                                    }
                                                                }
                                                                #endregion NormalAmount 
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    
                                }
                                #endregion JournalMapCompanyAccountGroup
                            }
                            #endregion BankAccount
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = ReceivableTransaction ", ex.ToString());
            }
        }

        #endregion JournalReceivable

        private void SetStatusReceivableTransactionLine(Session _currSession, ReceivableTransaction _locReceivableTransactionXPO)
        {
            try
            {
                DateTime now = DateTime.Now;
                if (_locReceivableTransactionXPO != null)
                {
                    XPCollection<ReceivableTransactionLine> _locReceivableTransactionLines = new XPCollection<ReceivableTransactionLine>(_currSession,
                                                                        new GroupOperator(GroupOperatorType.And,
                                                                        new BinaryOperator("ReceivableTransaction", _locReceivableTransactionXPO),
                                                                        new BinaryOperator("Select", true),
                                                                        new GroupOperator(GroupOperatorType.Or,
                                                                        new BinaryOperator("Status", Status.Progress),
                                                                        new BinaryOperator("Status", Status.Posted))));

                    if (_locReceivableTransactionLines != null && _locReceivableTransactionLines.Count() > 0)
                    {
                        foreach (ReceivableTransactionLine _locReceivableTransactionLine in _locReceivableTransactionLines)
                        {
                            if(_locReceivableTransactionLine.Outstanding == 0 || _locReceivableTransactionLine.Settled == true)
                            {
                                _locReceivableTransactionLine.Status = Status.Close;
                                _locReceivableTransactionLine.ActivationPosting = true;
                                _locReceivableTransactionLine.StatusDate = now;

                                #region Invoice
                                if(_locReceivableTransactionLine.SalesInvoice != null)
                                {
                                    _locReceivableTransactionLine.SalesInvoice.Status = Status.Close;
                                    _locReceivableTransactionLine.SalesInvoice.StatusDate = now;
                                    _locReceivableTransactionLine.SalesInvoice.Save();
                                    _locReceivableTransactionLine.SalesInvoice.Session.CommitTransaction();

                                    XPCollection<SalesOrderCollection> _locSalesOrderCollections = new XPCollection<SalesOrderCollection>
                                                                                                (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                new BinaryOperator("SalesInvoice", _locReceivableTransactionLine.SalesInvoice)));
                                    if(_locSalesOrderCollections != null && _locSalesOrderCollections.Count() > 0)
                                    {
                                        foreach(SalesOrderCollection _locSalesOrderCollection in _locSalesOrderCollections)
                                        {
                                            if(_locSalesOrderCollection.SalesOrder != null)
                                            {
                                                _locSalesOrderCollection.SalesOrder.Status = Status.Close;
                                                _locSalesOrderCollection.SalesOrder.StatusDate = now;
                                                _locSalesOrderCollection.SalesOrder.Save();
                                                _locSalesOrderCollection.SalesOrder.Session.CommitTransaction();
                                            }
                                        }
                                    }
                                }
                                
                                #endregion Invoice

                                #region RouteInvoice
                                if (_locReceivableTransactionLine.PickingMonitoring != null)
                                {
                                    _locReceivableTransactionLine.PickingMonitoring.CollectStatus = CollectStatus.Finish;
                                    _locReceivableTransactionLine.PickingMonitoring.CollectStatusDate = now;
                                    _locReceivableTransactionLine.PickingMonitoring.Save();
                                    _locReceivableTransactionLine.PickingMonitoring.Session.CommitTransaction();
                                }

                                #endregion RouteInvoice

                                #region OrderCollection
                                if(_locReceivableTransactionLine.OrderCollectionMonitoring != null)
                                {
                                    #region OrderCollectionMonitoring
                                    _locReceivableTransactionLine.OrderCollectionMonitoring.CollectStatus = CollectStatus.Finish;
                                    _locReceivableTransactionLine.OrderCollectionMonitoring.CollectStatusDate = now;
                                    _locReceivableTransactionLine.OrderCollectionMonitoring.Status = Status.Close;
                                    _locReceivableTransactionLine.OrderCollectionMonitoring.StatusDate = now;
                                    _locReceivableTransactionLine.OrderCollectionMonitoring.Save();
                                    _locReceivableTransactionLine.OrderCollectionMonitoring.Session.CommitTransaction();
                                    #endregion OrderCollectionMonitoring

                                    #region OrderCollectionLine
                                    if(_locReceivableTransactionLine.OrderCollectionMonitoring.OrderCollectionLine != null)
                                    {
                                        _locReceivableTransactionLine.OrderCollectionMonitoring.OrderCollectionLine.CollectStatus = CollectStatus.Finish;
                                        _locReceivableTransactionLine.OrderCollectionMonitoring.OrderCollectionLine.CollectStatusDate = now;
                                        _locReceivableTransactionLine.OrderCollectionMonitoring.OrderCollectionLine.Status = Status.Close;
                                        _locReceivableTransactionLine.OrderCollectionMonitoring.OrderCollectionLine.StatusDate = now;
                                        _locReceivableTransactionLine.OrderCollectionMonitoring.OrderCollectionLine.Save();
                                        _locReceivableTransactionLine.OrderCollectionMonitoring.OrderCollectionLine.Session.CommitTransaction();
                                    }
                                    #endregion OrderCollectionLine
                                }

                                #endregion OrderCollection
                            }
                            else
                            {
                                _locReceivableTransactionLine.Status = Status.Posted;
                                _locReceivableTransactionLine.StatusDate = now;

                                #region RouteInvoice
                                if (_locReceivableTransactionLine.PickingMonitoring != null)
                                {
                                    _locReceivableTransactionLine.PickingMonitoring.CollectStatus = CollectStatus.Collect;
                                    _locReceivableTransactionLine.PickingMonitoring.CollectStatusDate = now;
                                    _locReceivableTransactionLine.PickingMonitoring.Save();
                                    _locReceivableTransactionLine.PickingMonitoring.Session.CommitTransaction();
                                }

                                #endregion RouteInvoice

                                #region OrderCollection
                                if (_locReceivableTransactionLine.OrderCollectionMonitoring != null)
                                {
                                    #region OrderCollectionMonitoring
                                    _locReceivableTransactionLine.OrderCollectionMonitoring.CollectStatus = CollectStatus.Collect;
                                    _locReceivableTransactionLine.OrderCollectionMonitoring.CollectStatusDate = now;
                                    _locReceivableTransactionLine.OrderCollectionMonitoring.Status = Status.Posted;
                                    _locReceivableTransactionLine.OrderCollectionMonitoring.StatusDate = now;
                                    _locReceivableTransactionLine.OrderCollectionMonitoring.Save();
                                    _locReceivableTransactionLine.OrderCollectionMonitoring.Session.CommitTransaction();
                                    #endregion OrderCollectionMonitoring

                                    #region OrderCollectionLine
                                    if (_locReceivableTransactionLine.OrderCollectionMonitoring.OrderCollectionLine != null)
                                    {
                                        _locReceivableTransactionLine.OrderCollectionMonitoring.OrderCollectionLine.CollectStatus = CollectStatus.Collect;
                                        _locReceivableTransactionLine.OrderCollectionMonitoring.OrderCollectionLine.CollectStatusDate = now;
                                        _locReceivableTransactionLine.OrderCollectionMonitoring.OrderCollectionLine.Status = Status.Posted;
                                        _locReceivableTransactionLine.OrderCollectionMonitoring.OrderCollectionLine.StatusDate = now;
                                        _locReceivableTransactionLine.OrderCollectionMonitoring.OrderCollectionLine.Save();
                                        _locReceivableTransactionLine.OrderCollectionMonitoring.OrderCollectionLine.Session.CommitTransaction();
                                    }
                                    #endregion OrderCollectionLine
                                }

                                #endregion OrderCollection
                            }
                            _locReceivableTransactionLine.Save();
                            _locReceivableTransactionLine.Session.CommitTransaction();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = ReceivableTransaction " + ex.ToString());
            }
        }

        private void SetNormal(Session _currSession, ReceivableTransaction _locReceivableTransactionXPO)
        {
            try
            {
                DateTime now = DateTime.Now;

                if (_locReceivableTransactionXPO != null)
                {
                    XPCollection<ReceivableTransactionLine> _locReceivableTransactionLines = new XPCollection<ReceivableTransactionLine>(_currSession,
                                                                                     new GroupOperator(GroupOperatorType.And,
                                                                                     new BinaryOperator("ReceivableTransaction", _locReceivableTransactionXPO),
                                                                                     new BinaryOperator("Select", true)));

                    if (_locReceivableTransactionLines != null && _locReceivableTransactionLines.Count > 0)
                    {
                        foreach (ReceivableTransactionLine _locReceivableTransactionLine in _locReceivableTransactionLines)
                        {
                            if (_locReceivableTransactionLine.Status == Status.Progress || _locReceivableTransactionLine.Status == Status.Posted || _locReceivableTransactionLine.Status == Status.Close)
                            {
                                if(_locReceivableTransactionLine.Outstanding == 0 || _locReceivableTransactionLine.Settled == true)
                                {
                                    _locReceivableTransactionLine.Select = false;
                                }
                                else
                                {
                                    _locReceivableTransactionLine.Select = true;
                                }
                                _locReceivableTransactionLine.Amount = 0;
                                _locReceivableTransactionLine.AmountCN = 0;
                                _locReceivableTransactionLine.AmountDN = 0;
                                _locReceivableTransactionLine.Save();
                                _locReceivableTransactionLine.Session.CommitTransaction();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = ReceivableTransaction " + ex.ToString());
            }
        }

        private void SetFinalStatusReceivableTransaction(Session _currSession, ReceivableTransaction _locReceivableTransactionXPO)
        {
            try
            {
                DateTime now = DateTime.Now;
                int _locStatusCount = 0;
                int _locReceiTransLineCount = 0;

                if (_locReceivableTransactionXPO != null)
                {
                    XPCollection<ReceivableTransactionLine> _locReceivableTransactionLines = new XPCollection<ReceivableTransactionLine>(_currSession,
                                                                                     new GroupOperator(GroupOperatorType.And,
                                                                                     new BinaryOperator("ReceivableTransaction", _locReceivableTransactionXPO)));

                    if (_locReceivableTransactionLines != null && _locReceivableTransactionLines.Count > 0)
                    {
                        _locReceiTransLineCount = _locReceivableTransactionLines.Count();

                        foreach (ReceivableTransactionLine _locReceivableTransactionLine in _locReceivableTransactionLines)
                        {
                            if (_locReceivableTransactionLine.Status == Status.Close)
                            {
                                _locStatusCount = _locStatusCount + 1;
                            }
                        }

                        if (_locStatusCount == _locReceiTransLineCount)
                        {
                            _locReceivableTransactionXPO.ActivationPosting = true;
                            _locReceivableTransactionXPO.Status = Status.Close;
                            _locReceivableTransactionXPO.StatusDate = now;
                            _locReceivableTransactionXPO.Save();
                            _locReceivableTransactionXPO.Session.CommitTransaction();
                        }
                        else
                        {
                            _locReceivableTransactionXPO.Status = Status.Posted;
                            _locReceivableTransactionXPO.StatusDate = now;
                            _locReceivableTransactionXPO.Save();
                            _locReceivableTransactionXPO.Session.CommitTransaction();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = ReceivableTransaction " + ex.ToString());
            }
        }

        private void SetFinalStatusOrderCollection(Session _currSession, ReceivableTransaction _locReceivableTransactionXPO)
        {
            try
            {
                DateTime now = DateTime.Now;
                int _locStatusCount = 0;
                int _locOrderCollLineCount = 0;

                if (_locReceivableTransactionXPO != null)
                {
                    XPCollection<ReceivableOrderCollection> _locReceivableOrderCollections = new XPCollection<ReceivableOrderCollection>(_currSession,
                                                                                     new GroupOperator(GroupOperatorType.And,
                                                                                     new BinaryOperator("ReceivableTransaction", _locReceivableTransactionXPO)));

                    if (_locReceivableOrderCollections != null && _locReceivableOrderCollections.Count > 0)
                    {
                        

                        foreach (ReceivableOrderCollection _locReceivableOrderCollection in _locReceivableOrderCollections)
                        {
                            if(_locReceivableOrderCollection.OrderCollection != null)
                            {
                                XPCollection<OrderCollectionLine> _locOrderCollectionLines = new XPCollection<OrderCollectionLine>(_currSession,
                                                                                          new GroupOperator(GroupOperatorType.And,
                                                                                          new BinaryOperator("OrderCollection", _locReceivableOrderCollection.OrderCollection)));
                                if(_locOrderCollectionLines != null && _locOrderCollectionLines.Count() > 0)
                                {
                                    _locOrderCollLineCount = _locOrderCollectionLines.Count();
                                    
                                    foreach(OrderCollectionLine _locOrderCollectionLine in _locOrderCollectionLines)
                                    {
                                        if (_locOrderCollectionLine.Status == Status.Close)
                                        {
                                            _locStatusCount = _locStatusCount + 1;
                                        }
                                    }

                                    if (_locStatusCount == _locOrderCollLineCount)
                                    {
                                        _locReceivableOrderCollection.OrderCollection.ActivationPosting = true;
                                        _locReceivableOrderCollection.OrderCollection.Status = Status.Close;
                                        _locReceivableOrderCollection.OrderCollection.StatusDate = now;
                                        _locReceivableOrderCollection.OrderCollection.Save();
                                        _locReceivableOrderCollection.OrderCollection.Session.CommitTransaction();
                                    }
                                    else
                                    {
                                        _locReceivableOrderCollection.OrderCollection.Status = Status.Posted;
                                        _locReceivableOrderCollection.OrderCollection.StatusDate = now;
                                        _locReceivableOrderCollection.OrderCollection.Save();
                                        _locReceivableOrderCollection.OrderCollection.Session.CommitTransaction();
                                    }
                                }
                            }
                        }

                        
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = ReceivableTransaction " + ex.ToString());
            }
        }

        #endregion Posting

        #region Global Method

        private void SuccessMessageShow(string _locMessage)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 4000;
            options.Message = string.Format("{0}", _locMessage);
            options.Type = InformationType.Success;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Success";
            options.Win.Type = WinMessageType.Flyout;
            Application.ShowViewStrategy.ShowMessage(options);
        }

        private void ErrorMessageShow(string _locMessage)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 2000;
            options.Message = string.Format(_locMessage);
            options.Type = InformationType.Warning;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Error";
            options.Win.Type = WinMessageType.Alert;
            Application.ShowViewStrategy.ShowMessage(options);
        }

        #endregion Global Method

    }
}
