﻿namespace FullDrive.Module.Controllers
{
    partial class PurchaseInvoiceMonitoringActionController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.PurchaseInvoiceMonitoringSelectAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.PurchaseInvoiceMonitoringUnselectAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.PurchaseInvoiceMonitoringCancelPayableAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            // 
            // PurchaseInvoiceMonitoringSelectAction
            // 
            this.PurchaseInvoiceMonitoringSelectAction.Caption = "Select";
            this.PurchaseInvoiceMonitoringSelectAction.ConfirmationMessage = null;
            this.PurchaseInvoiceMonitoringSelectAction.Id = "PurchaseInvoiceMonitoringSelectActionId";
            this.PurchaseInvoiceMonitoringSelectAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.PurchaseInvoiceMonitoring);
            this.PurchaseInvoiceMonitoringSelectAction.ToolTip = null;
            this.PurchaseInvoiceMonitoringSelectAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.PurchaseInvoiceMonitoringSelectAction_Execute);
            // 
            // PurchaseInvoiceMonitoringUnselectAction
            // 
            this.PurchaseInvoiceMonitoringUnselectAction.Caption = "Unselect";
            this.PurchaseInvoiceMonitoringUnselectAction.ConfirmationMessage = null;
            this.PurchaseInvoiceMonitoringUnselectAction.Id = "PurchaseInvoiceMonitoringUnselectActionId";
            this.PurchaseInvoiceMonitoringUnselectAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.PurchaseInvoiceMonitoring);
            this.PurchaseInvoiceMonitoringUnselectAction.ToolTip = null;
            this.PurchaseInvoiceMonitoringUnselectAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.PurchaseInvoiceMonitoringUnselectAction_Execute);
            // 
            // PurchaseInvoiceMonitoringCancelPayableAction
            // 
            this.PurchaseInvoiceMonitoringCancelPayableAction.Caption = "Cancel Payable";
            this.PurchaseInvoiceMonitoringCancelPayableAction.ConfirmationMessage = null;
            this.PurchaseInvoiceMonitoringCancelPayableAction.Id = "PurchaseInvoiceMonitoringCancelPayableActionId";
            this.PurchaseInvoiceMonitoringCancelPayableAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.PurchaseInvoiceMonitoring);
            this.PurchaseInvoiceMonitoringCancelPayableAction.ToolTip = null;
            this.PurchaseInvoiceMonitoringCancelPayableAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.PurchaseInvoiceMonitoringCancelPayableAction_Execute);
            // 
            // PurchaseInvoiceMonitoringActionController
            // 
            this.Actions.Add(this.PurchaseInvoiceMonitoringSelectAction);
            this.Actions.Add(this.PurchaseInvoiceMonitoringUnselectAction);
            this.Actions.Add(this.PurchaseInvoiceMonitoringCancelPayableAction);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SimpleAction PurchaseInvoiceMonitoringSelectAction;
        private DevExpress.ExpressApp.Actions.SimpleAction PurchaseInvoiceMonitoringUnselectAction;
        private DevExpress.ExpressApp.Actions.SimpleAction PurchaseInvoiceMonitoringCancelPayableAction;
    }
}
