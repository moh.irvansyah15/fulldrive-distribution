﻿namespace FullDrive.Module.Controllers
{
    partial class SalesOrderCollectionActionController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.SalesOrderCollectionShowSOMAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            // 
            // SalesOrderCollectionShowSOMAction
            // 
            this.SalesOrderCollectionShowSOMAction.Caption = "Show SOM";
            this.SalesOrderCollectionShowSOMAction.ConfirmationMessage = null;
            this.SalesOrderCollectionShowSOMAction.Id = "SalesOrderCollectionShowSOMActionId";
            this.SalesOrderCollectionShowSOMAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.SalesOrderCollection);
            this.SalesOrderCollectionShowSOMAction.TargetViewType = DevExpress.ExpressApp.ViewType.ListView;
            this.SalesOrderCollectionShowSOMAction.ToolTip = null;
            this.SalesOrderCollectionShowSOMAction.TypeOfView = typeof(DevExpress.ExpressApp.ListView);
            this.SalesOrderCollectionShowSOMAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.SalesOrderCollectionShowSOMAction_Execute);
            // 
            // SalesOrderCollectionActionController
            // 
            this.Actions.Add(this.SalesOrderCollectionShowSOMAction);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SimpleAction SalesOrderCollectionShowSOMAction;
    }
}
