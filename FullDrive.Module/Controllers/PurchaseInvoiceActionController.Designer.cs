﻿namespace FullDrive.Module.Controllers
{
    partial class PurchaseInvoiceActionController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.PurchaseInvoiceGetITIAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.PurchaseInvoiceProgressAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.PurchaseInvoicePostingAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.PurchaseInvoiceApprovalAction = new DevExpress.ExpressApp.Actions.SingleChoiceAction(this.components);
            this.PurchaseInvoiceListviewFilterSelectionAction = new DevExpress.ExpressApp.Actions.SingleChoiceAction(this.components);
            // 
            // PurchaseInvoiceGetITIAction
            // 
            this.PurchaseInvoiceGetITIAction.Caption = "Get ITI";
            this.PurchaseInvoiceGetITIAction.ConfirmationMessage = null;
            this.PurchaseInvoiceGetITIAction.Id = "PurchaseInvoiceGetITIActionId";
            this.PurchaseInvoiceGetITIAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.PurchaseInvoice);
            this.PurchaseInvoiceGetITIAction.ToolTip = null;
            this.PurchaseInvoiceGetITIAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.PurchaseInvoiceGetITIAction_Execute);
            // 
            // PurchaseInvoiceProgressAction
            // 
            this.PurchaseInvoiceProgressAction.Caption = "Progress";
            this.PurchaseInvoiceProgressAction.ConfirmationMessage = null;
            this.PurchaseInvoiceProgressAction.Id = "PurchaseInvoiceProgressActionId";
            this.PurchaseInvoiceProgressAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.PurchaseInvoice);
            this.PurchaseInvoiceProgressAction.ToolTip = null;
            this.PurchaseInvoiceProgressAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.PurchaseInvoiceProgressAction_Execute);
            // 
            // PurchaseInvoicePostingAction
            // 
            this.PurchaseInvoicePostingAction.Caption = "Posting";
            this.PurchaseInvoicePostingAction.ConfirmationMessage = null;
            this.PurchaseInvoicePostingAction.Id = "PurchaseInvoicePostingActionId";
            this.PurchaseInvoicePostingAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.PurchaseInvoice);
            this.PurchaseInvoicePostingAction.ToolTip = null;
            this.PurchaseInvoicePostingAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.PurchaseInvoicePostingAction_Execute);
            // 
            // PurchaseInvoiceApprovalAction
            // 
            this.PurchaseInvoiceApprovalAction.Caption = "Approval";
            this.PurchaseInvoiceApprovalAction.ConfirmationMessage = null;
            this.PurchaseInvoiceApprovalAction.Id = "PurchaseInvoiceApprovalActionId";
            this.PurchaseInvoiceApprovalAction.ItemType = DevExpress.ExpressApp.Actions.SingleChoiceActionItemType.ItemIsOperation;
            this.PurchaseInvoiceApprovalAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.PurchaseInvoice);
            this.PurchaseInvoiceApprovalAction.ToolTip = null;
            this.PurchaseInvoiceApprovalAction.Execute += new DevExpress.ExpressApp.Actions.SingleChoiceActionExecuteEventHandler(this.PurchaseInvoiceApprovalAction_Execute);
            // 
            // PurchaseInvoiceListviewFilterSelectionAction
            // 
            this.PurchaseInvoiceListviewFilterSelectionAction.Caption = "Filter";
            this.PurchaseInvoiceListviewFilterSelectionAction.ConfirmationMessage = null;
            this.PurchaseInvoiceListviewFilterSelectionAction.Id = "PurchaseInvoiceListviewFilterSelectionActionId";
            this.PurchaseInvoiceListviewFilterSelectionAction.ItemType = DevExpress.ExpressApp.Actions.SingleChoiceActionItemType.ItemIsOperation;
            this.PurchaseInvoiceListviewFilterSelectionAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.PurchaseInvoice);
            this.PurchaseInvoiceListviewFilterSelectionAction.ToolTip = null;
            this.PurchaseInvoiceListviewFilterSelectionAction.Execute += new DevExpress.ExpressApp.Actions.SingleChoiceActionExecuteEventHandler(this.PurchaseInvoiceListviewFilterSelectionAction_Execute);
            // 
            // PurchaseInvoiceActionController
            // 
            this.Actions.Add(this.PurchaseInvoiceGetITIAction);
            this.Actions.Add(this.PurchaseInvoiceProgressAction);
            this.Actions.Add(this.PurchaseInvoicePostingAction);
            this.Actions.Add(this.PurchaseInvoiceApprovalAction);
            this.Actions.Add(this.PurchaseInvoiceListviewFilterSelectionAction);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SimpleAction PurchaseInvoiceGetITIAction;
        private DevExpress.ExpressApp.Actions.SimpleAction PurchaseInvoiceProgressAction;
        private DevExpress.ExpressApp.Actions.SimpleAction PurchaseInvoicePostingAction;
        private DevExpress.ExpressApp.Actions.SingleChoiceAction PurchaseInvoiceApprovalAction;
        private DevExpress.ExpressApp.Actions.SingleChoiceAction PurchaseInvoiceListviewFilterSelectionAction;
    }
}
