﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;

using FullDrive.Module.BusinessObjects;
using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.Xpo;
using System.Collections;
using DevExpress.Xpo;

namespace FullDrive.Module.Controllers
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class PickingActionController : ViewController
    {
        public PickingActionController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.
        }
        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }
        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        private void PickingProgressAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;
                Status _locStatus = Status.None;
                DateTime _locNow;
                Status _locStatus2 = Status.None;
                DateTime _locNow2;

                if (_objectsToProcess != null)
                {
                    foreach (Object obj in _objectsToProcess)
                    {
                        Picking _locPickingOS = (Picking)_objectSpace.GetObject(obj);

                        if (_locPickingOS != null)
                        {
                            if (_locPickingOS.Session != null)
                            {
                                _currSession = _locPickingOS.Session;
                            }
                            if (_locPickingOS.Code != null && _currSession != null)
                            {
                                _currObjectId = _locPickingOS.Code;

                                Picking _locPickingXPO = _currSession.FindObject<Picking>
                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                     new BinaryOperator("Code", _currObjectId),
                                                                     new GroupOperator(GroupOperatorType.Or,
                                                                     new BinaryOperator("Status", Status.Open),
                                                                     new BinaryOperator("Status", Status.Progress))));

                                if (_locPickingXPO != null)
                                {
                                    if (_locPickingXPO.Status == Status.Open)
                                    {
                                        _locStatus = Status.Progress;
                                        _locNow = now;
                                    }
                                    else
                                    {
                                        _locStatus = _locPickingXPO.Status;
                                        _locNow = _locPickingXPO.StatusDate;
                                    }
                                    _locPickingXPO.Status = _locStatus;
                                    _locPickingXPO.StatusDate = _locNow;
                                    _locPickingXPO.Save();
                                    _locPickingXPO.Session.CommitTransaction();

                                    #region TransferOutLine and TransferOutLot
                                    XPCollection<PickingLine> _locPickingLines = new XPCollection<PickingLine>
                                                           (_currSession, new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("Picking", _locPickingXPO),
                                                            new GroupOperator(GroupOperatorType.Or,
                                                            new BinaryOperator("Status", Status.Open),
                                                            new BinaryOperator("Status", Status.Progress))));

                                    if (_locPickingLines != null && _locPickingLines.Count > 0)
                                    {
                                        foreach (PickingLine _locPickingLine in _locPickingLines)
                                        {
                                            if (_locPickingLine.Status == Status.Open)
                                            {
                                                _locStatus2 = Status.Progress;
                                                _locNow2 = now;
                                            }
                                            else
                                            {
                                                _locStatus2 = _locPickingLine.Status;
                                                _locNow2 = _locPickingLine.StatusDate;
                                            }

                                            _locPickingLine.Status = _locStatus2;
                                            _locPickingLine.StatusDate = _locNow2;
                                            _locPickingLine.Save();
                                            _locPickingLine.Session.CommitTransaction();

                                            
                                        }
                                    }
                                    #endregion TransferOutLine and TransferOutLot

                                    SuccessMessageShow(_locPickingXPO.Code + " has been change successfully to Progress");

                                }
                                else
                                {
                                    ErrorMessageShow("Picking Data Not Available");
                                }
                            }
                            else
                            {
                                ErrorMessageShow("Picking Data Not Available");
                            }
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = Picking " + ex.ToString());
            }
        }

        private void PickingPostingAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;

                if (_objectsToProcess != null)
                {
                    foreach (Object obj in _objectsToProcess)
                    {
                        Picking _locPickingOS = (Picking)_objectSpace.GetObject(obj);

                        if (_locPickingOS != null)
                        {
                            if (_locPickingOS.Status == Status.Progress || _locPickingOS.Status == Status.Posted)
                            {
                                if (_locPickingOS.Session != null)
                                {
                                    _currSession = _locPickingOS.Session;
                                }
                                if (_locPickingOS.Code != null && _currSession != null)
                                {
                                    _currObjectId = _locPickingOS.Code;

                                    Picking _locPickingXPO = _currSession.FindObject<Picking>
                                                                        (new GroupOperator(GroupOperatorType.And,
                                                                         new BinaryOperator("Code", _currObjectId)));

                                    if (_locPickingXPO != null)
                                    {
                                        SetPickingMonitoring(_currSession, _locPickingXPO);
                                        SetStatusPickingLine(_currSession, _locPickingXPO);
                                        SetNormalDeliverQuantity(_currSession, _locPickingXPO);
                                        SetFinalStatusPicking(_currSession, _locPickingXPO);
                                        SuccessMessageShow(_locPickingXPO.Code + " has been change successfully to Picking Monitoring"); 
                                    }
                                    else
                                    {
                                        ErrorMessageShow("Picking Data Not Available");
                                    }
                                }
                            }
                            else
                            {
                                ErrorMessageShow("Picking Data Not Available");
                            }
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = Picking " + ex.ToString());
            }
        }

        //========================================= Code Only ==========================================

        #region Posting

        private void SetPickingMonitoring(Session _currSession, Picking _locPickingXPO)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                DateTime now = DateTime.Now;

                if (_locPickingXPO != null)
                {
                    XPCollection<PickingLine> _locPickingLines = new XPCollection<PickingLine>
                                                            (_currSession, new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("Picking", _locPickingXPO),
                                                            new BinaryOperator("Select", true),
                                                            new GroupOperator(GroupOperatorType.Or,
                                                            new BinaryOperator("Status", Status.Progress),
                                                            new BinaryOperator("Status", Status.Posted))));

                    if (_locPickingLines != null && _locPickingLines.Count() > 0)
                    {
                        foreach (PickingLine _locPickingLine in _locPickingLines)
                        {
                            PickingMonitoring _saveDataPickingMonitoring = new PickingMonitoring(_currSession)
                            {
                                Picking = _locPickingXPO,
                                PickingLine = _locPickingLine,
                                Company = _locPickingLine.Company,
                                Workplace = _locPickingLine.Workplace,
                                Division = _locPickingLine.Division,
                                Department = _locPickingLine.Department,
                                Section = _locPickingLine.Section,
                                Employee = _locPickingLine.Employee,
                                Div = _locPickingLine.Div,
                                Dept = _locPickingLine.Dept,
                                Sect = _locPickingLine.Sect,
                                PIC = _locPickingLine.PIC,
                                SalesInvoice = _locPickingLine.SalesInvoice,
                                Customer = _locPickingLine.Customer,
                                Vehicle = _locPickingLine.Vehicle,
                                BegInv = _locPickingLine.BegInv,
                                PickingDate = _locPickingLine.PickingDate,
                                Address = _locPickingLine.Address,
                            };
                            _saveDataPickingMonitoring.Save();
                            _saveDataPickingMonitoring.Session.CommitTransaction();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = Picking ", ex.ToString());
            }
        }

        private void SetStatusPickingLine(Session _currSession, Picking _locPickingXPO)
        {
            try
            {
                DateTime now = DateTime.Now;
                if (_locPickingXPO != null)
                {
                    XPCollection<PickingLine> _locPickingLines = new XPCollection<PickingLine>(_currSession,
                                                            new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("Picking", _locPickingXPO),
                                                            new BinaryOperator("Select", true),
                                                            new GroupOperator(GroupOperatorType.Or,
                                                            new BinaryOperator("Status", Status.Progress),
                                                            new BinaryOperator("Status", Status.Posted))));

                    if (_locPickingLines != null && _locPickingLines.Count > 0)
                    {
                        foreach (PickingLine _locPickingLine in _locPickingLines)
                        {
                            _locPickingLine.Status = Status.Close;
                            _locPickingLine.ActivationPosting = true;
                            _locPickingLine.StatusDate = now;
                            _locPickingLine.Save();
                            _locPickingLine.Session.CommitTransaction();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = Picking " + ex.ToString());
            }
        }

        private void SetNormalDeliverQuantity(Session _currSession, Picking _locPickingXPO)
        {
            try
            {
                if (_locPickingXPO != null)
                {
                    XPCollection<PickingLine> _locPickingLines = new XPCollection<PickingLine>(_currSession,
                                                                                     new GroupOperator(GroupOperatorType.And,
                                                                                     new BinaryOperator("Picking", _locPickingXPO),
                                                                                     new BinaryOperator("Select", true),
                                                                                     new GroupOperator(GroupOperatorType.Or,
                                                                                     new BinaryOperator("Status", Status.Progress),
                                                                                     new BinaryOperator("Status", Status.Posted),
                                                                                     new BinaryOperator("Status", Status.Close))));

                    if (_locPickingLines != null && _locPickingLines.Count > 0)
                    {
                        foreach (PickingLine _locPickingLine in _locPickingLines)
                        {
                            if(_locPickingLine.Select == true)
                            {
                                _locPickingLine.Select = false;
                                _locPickingLine.Save();
                                _locPickingLine.Session.CommitTransaction();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = Picking " + ex.ToString());
            }
        }

        private void SetFinalStatusPicking(Session _currSession, Picking _locPickingXPO)
        {
            try
            {
                DateTime now = DateTime.Now;
                int _locStatusCount = 0;
                int _locTransOutLineCount = 0;

                if (_locPickingXPO != null)
                {
                    XPCollection<PickingLine> _locPickingLines = new XPCollection<PickingLine>(_currSession,
                                                                                     new GroupOperator(GroupOperatorType.And,
                                                                                     new BinaryOperator("Picking", _locPickingXPO)));

                    if (_locPickingLines != null && _locPickingLines.Count() > 0)
                    {
                        _locTransOutLineCount = _locPickingLines.Count();

                        foreach (PickingLine _locPickingLine in _locPickingLines)
                        {

                            if (_locPickingLine.Status == Status.Close)
                            {
                                _locStatusCount = _locStatusCount + 1;
                            }
                        }

                        if (_locTransOutLineCount == _locStatusCount)
                        {
                            _locPickingXPO.ActivationPosting = true;
                            _locPickingXPO.Status = Status.Close;
                            _locPickingXPO.StatusDate = now;
                            
                            _locPickingXPO.Save();
                            _locPickingXPO.Session.CommitTransaction();
                        }
                        else
                        {
                            _locPickingXPO.Status = Status.Posted;
                            _locPickingXPO.StatusDate = now;
                            _locPickingXPO.Save();
                            _locPickingXPO.Session.CommitTransaction();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PickingLine " + ex.ToString());
            }
        }

        #endregion Posting

        #region Global Method

        private void SuccessMessageShow(string _locMessage)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 4000;
            options.Message = string.Format("{0}", _locMessage);
            options.Type = InformationType.Success;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Success";
            options.Win.Type = WinMessageType.Flyout;
            Application.ShowViewStrategy.ShowMessage(options);
        }

        private void ErrorMessageShow(string _locMessage)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 2000;
            options.Message = string.Format(_locMessage);
            options.Type = InformationType.Warning;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Error";
            options.Win.Type = WinMessageType.Alert;
            Application.ShowViewStrategy.ShowMessage(options);
        }

        #endregion Global Method
    }
}
