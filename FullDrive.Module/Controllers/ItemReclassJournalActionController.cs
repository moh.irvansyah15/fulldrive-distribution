﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;

using FullDrive.Module.BusinessObjects;
using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.Xpo;
using System.Collections;
using DevExpress.Xpo;

namespace FullDrive.Module.Controllers
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class ItemReclassJournalActionController : ViewController
    {
        private ChoiceActionItem _selectionListviewFilter;

        public ItemReclassJournalActionController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.
            #region Filter Status
            ItemReclassJournalListviewFilterSelectionAction.Items.Clear();
            foreach (object _currApproval in Enum.GetValues(typeof(CustomProcess.Status)))
            {
                EnumDescriptor _ed = new EnumDescriptor(typeof(CustomProcess.Status));
                _selectionListviewFilter = new ChoiceActionItem(_ed.GetCaption(_currApproval), _currApproval);
                ItemReclassJournalListviewFilterSelectionAction.Items.Add(_selectionListviewFilter);
            }
            #endregion Filter Status
        }

        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
        }

        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }

        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        private void ItemReclassJournalGetStockAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;
                

                if (_objectsToProcess != null)
                {
                    foreach (Object obj in _objectsToProcess)
                    {
                        ItemReclassJournal _locItemReclassJournalOS = (ItemReclassJournal)_objectSpace.GetObject(obj);

                        if (_locItemReclassJournalOS != null)
                        {
                            if (_locItemReclassJournalOS.Session != null)
                            {
                                _currSession = _locItemReclassJournalOS.Session;
                            }
                            if (_locItemReclassJournalOS.Code != null && _currSession != null)
                            {
                                _currObjectId = _locItemReclassJournalOS.Code;

                                ItemReclassJournal _locItemReclassJournalXPO = _currSession.FindObject<ItemReclassJournal>
                                                                                (new GroupOperator(GroupOperatorType.And,
                                                                                 new BinaryOperator("Code", _currObjectId),
                                                                                 new GroupOperator(GroupOperatorType.Or,
                                                                                 new BinaryOperator("Status", Status.Open),
                                                                                 new BinaryOperator("Status", Status.Progress))));

                                if (_locItemReclassJournalXPO != null)
                                {
                                    SetStockFromBegInvLine(_currSession, _locItemReclassJournalXPO);
                                    SuccessMessageShow(_locItemReclassJournalXPO.Code + " has been change successfully to Progress");
                                }
                                else
                                {
                                    ErrorMessageShow("Data Item Reclass Journal Not Available");
                                }
                            }
                            else
                            {
                                ErrorMessageShow("Data Item Reclass Journal Not Available");
                            }
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = ItemReclassJournal " + ex.ToString());
            }
        }

        private void ItemReclassJournalProgressAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;
                Status _locStatus = Status.None;
                DateTime _locNow;
                Status _locStatus2 = Status.None;
                DateTime _locNow2;

                if (_objectsToProcess != null)
                {
                    foreach (Object obj in _objectsToProcess)
                    {
                        ItemReclassJournal _locItemReclassJournalOS = (ItemReclassJournal)_objectSpace.GetObject(obj);

                        if (_locItemReclassJournalOS != null)
                        {
                            if (_locItemReclassJournalOS.Session != null)
                            {
                                _currSession = _locItemReclassJournalOS.Session;
                            }

                            if (_locItemReclassJournalOS.Code != null && _currSession != null)
                            {
                                _currObjectId = _locItemReclassJournalOS.Code;

                                ItemReclassJournal _locItemReclassJournalXPO = _currSession.FindObject<ItemReclassJournal>
                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                     new BinaryOperator("Code", _currObjectId),
                                                                     new GroupOperator(GroupOperatorType.Or,
                                                                     new BinaryOperator("Status", Status.Open),
                                                                     new BinaryOperator("Status", Status.Progress))));

                                if (_locItemReclassJournalXPO != null)
                                {
                                    if (_locItemReclassJournalXPO.Status == Status.Open)
                                    {
                                        _locStatus = Status.Progress;
                                        _locNow = now;
                                    }
                                    else
                                    {
                                        _locStatus = _locItemReclassJournalXPO.Status;
                                        _locNow = _locItemReclassJournalXPO.StatusDate;
                                    }

                                    _locItemReclassJournalXPO.Status = _locStatus;
                                    _locItemReclassJournalXPO.StatusDate = _locNow;
                                    _locItemReclassJournalXPO.Save();
                                    _locItemReclassJournalXPO.Session.CommitTransaction();

                                    #region ItemReclassJournalLine
                                    XPCollection<ItemReclassJournalLine> _locItemReclassJournalLines = new XPCollection<ItemReclassJournalLine>
                                                           (_currSession, new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("ItemReclassJournal", _locItemReclassJournalXPO),
                                                            new BinaryOperator("Status", Status.Open)));

                                    if (_locItemReclassJournalLines != null && _locItemReclassJournalLines.Count > 0)
                                    {
                                        foreach (ItemReclassJournalLine _locItemReclassJournalLine in _locItemReclassJournalLines)
                                        {
                                            if (_locItemReclassJournalLine.Status == Status.Open)
                                            {
                                                _locStatus2 = Status.Progress;
                                                _locNow2 = now;
                                            }
                                            else
                                            {
                                                _locStatus2 = _locItemReclassJournalLine.Status;
                                                _locNow2 = _locItemReclassJournalLine.StatusDate;
                                            }

                                            _locItemReclassJournalLine.Status = _locStatus2;
                                            _locItemReclassJournalLine.StatusDate = _locNow2;
                                            _locItemReclassJournalLine.Save();
                                            _locItemReclassJournalLine.Session.CommitTransaction();

                                            if(_locItemReclassJournalLine.BegInvLine != null)
                                            {
                                                _locItemReclassJournalLine.BegInvLine.Lock = true;
                                                _locItemReclassJournalLine.BegInvLine.Save();
                                                _locItemReclassJournalLine.BegInvLine.Session.CommitTransaction();
                                            }
                                        }
                                    }
                                    #endregion ItemReclassJournalLine

                                    SuccessMessageShow(_locItemReclassJournalXPO.Code + " has been change successfully to Progress");
                                    
                                }
                                else
                                {
                                    ErrorMessageShow("Data Item Reclass Journal Not Available");
                                }
                            }
                            else
                            {
                                ErrorMessageShow("Data Item Reclass Journal Not Available");
                            }
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = ItemReclassJournal " + ex.ToString());
            }
        }

        private void ItemReclassJournalPostingAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;


                if (_objectsToProcess != null)
                {
                    foreach (Object obj in _objectsToProcess)
                    {
                        ItemReclassJournal _locItemReclassJournalOS = (ItemReclassJournal)_objectSpace.GetObject(obj);

                        if (_locItemReclassJournalOS != null)
                        {
                            if (_locItemReclassJournalOS.Session != null)
                            {
                                _currSession = _locItemReclassJournalOS.Session;
                            }
                            if (_locItemReclassJournalOS.Code != null && _currSession != null)
                            {
                                _currObjectId = _locItemReclassJournalOS.Code;

                                ItemReclassJournal _locItemReclassJournalXPO = _currSession.FindObject<ItemReclassJournal>
                                                                                (new GroupOperator(GroupOperatorType.And,
                                                                                 new BinaryOperator("Code", _currObjectId),
                                                                                 new GroupOperator(GroupOperatorType.Or,
                                                                                 new BinaryOperator("Status", Status.Progress),
                                                                                 new BinaryOperator("Status", Status.Posted))));

                                if (_locItemReclassJournalXPO != null)
                                {
                                    SetItemReclassJournalMonitoring(_currSession, _locItemReclassJournalXPO);
                                    SetAdjustBeginingInventory(_currSession, _locItemReclassJournalXPO);
                                    SetAdjustInventoryJournal(_currSession, _locItemReclassJournalXPO);
                                    SetStatuItemReclassJournalLine(_currSession, _locItemReclassJournalXPO);
                                    SetFinalStatusDeliverTransferOut(_currSession, _locItemReclassJournalXPO);
                                    SuccessMessageShow(_locItemReclassJournalXPO.Code + " has been change successfully to Posted");
                                }
                                else
                                {
                                    ErrorMessageShow("Data Item Reclass Journal Not Available");
                                }
                            }
                            else
                            {
                                ErrorMessageShow("Data Item Reclass Journal Not Available");
                            }
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = ItemReclassJournal " + ex.ToString());
            }
        }

        private void ItemReclassJournalListviewFilterSelectionAction_Execute(object sender, SingleChoiceActionExecuteEventArgs e)
        {
            try
            {
                if ((View is ListView) & (View.ObjectTypeInfo.Type == typeof(ItemReclassJournal)))
                {
                    if ((Status)e.SelectedChoiceActionItem.Data == Status.None)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.None, BinaryOperatorType.NotEqual);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Open)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Open, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Progress)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Progress, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Posted)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Posted, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Lock)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Lock, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Close)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Close, BinaryOperatorType.Equal);
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = ItemReclassJournal " + ex.ToString());
            }
        }

        //============================================ Code In Here ============================================

        #region GetStock

        private void SetStockFromBegInvLine(Session _currSession, ItemReclassJournal _locItemReclassJournalXPO)
        {
            try
            {

                DateTime now = DateTime.Now;
                UnitOfMeasure _locDUOM = null;
                UnitOfMeasure _locUOM = null;
                double _locDQty = 0;
                double _locQty = 0;

                if (_locItemReclassJournalXPO != null)
                {
                    if (_locItemReclassJournalXPO.Company != null && _locItemReclassJournalXPO.Workplace != null && _locItemReclassJournalXPO.BeginningInventory != null )
                    {
                        XPCollection<BeginningInventoryLine> _locBeginningInventoryLines = null;

                        if(_locItemReclassJournalXPO.StockType != StockType.None)
                        {
                            _locBeginningInventoryLines = new XPCollection<BeginningInventoryLine>
                                                        (_currSession, new GroupOperator(GroupOperatorType.And,
                                                        new BinaryOperator("Company", _locItemReclassJournalXPO.Company),
                                                        new BinaryOperator("Workplace", _locItemReclassJournalXPO.Workplace),
                                                        new BinaryOperator("BeginningInventory", _locItemReclassJournalXPO.BeginningInventory),
                                                        new BinaryOperator("StockType", _locItemReclassJournalXPO.StockType),
                                                        new BinaryOperator("Lock", false),
                                                        new BinaryOperator("Active", true)));
                        }
                        else
                        {
                            _locBeginningInventoryLines = new XPCollection<BeginningInventoryLine>
                                                        (_currSession, new GroupOperator(GroupOperatorType.And,
                                                        new BinaryOperator("Company", _locItemReclassJournalXPO.Company),
                                                        new BinaryOperator("Workplace", _locItemReclassJournalXPO.Workplace),
                                                        new BinaryOperator("BeginningInventory", _locItemReclassJournalXPO.BeginningInventory),
                                                        new BinaryOperator("Lock", false),
                                                        new BinaryOperator("Active", true)));
                        }
                        

                        if (_locBeginningInventoryLines != null && _locBeginningInventoryLines.Count() > 0)
                        {
                            foreach (BeginningInventoryLine _locBeginningInventoryLine in _locBeginningInventoryLines)
                            {
                                if (_locBeginningInventoryLine.Item != null)
                                {
                                    #region SaveItemReclassJournalLine

                                    ItemReclassJournalLine _locItemReclassJournalLine = _currSession.FindObject<ItemReclassJournalLine>(new GroupOperator(GroupOperatorType.And,
                                                                                                            new BinaryOperator("Company", _locItemReclassJournalXPO.Company),
                                                                                                            new BinaryOperator("Workplace", _locItemReclassJournalXPO.Workplace),
                                                                                                            new BinaryOperator("BegInv", _locItemReclassJournalXPO.BeginningInventory),
                                                                                                            new BinaryOperator("BegInvLine", _locBeginningInventoryLine),
                                                                                                            new BinaryOperator("Item", _locBeginningInventoryLine.Item),
                                                                                                            new BinaryOperator("ItemReclassJournal", _locItemReclassJournalXPO)));
                                    if(_locItemReclassJournalLine == null)
                                    {
                                        if (_locBeginningInventoryLine.DefaultUOM != null)
                                        {
                                            _locDUOM = _locBeginningInventoryLine.DefaultUOM;
                                        }

                                        _locUOM = GetUOM(_currSession, _locBeginningInventoryLine);

                                        if (_locDUOM != null && _locUOM != null)
                                        {
                                            _locDQty = GetDQty(_currSession, _locBeginningInventoryLine.Item, _locDUOM, _locUOM, _locBeginningInventoryLine.QtyAvailable);
                                            _locQty = GetQty(_currSession, _locBeginningInventoryLine.Item, _locDUOM, _locUOM, _locBeginningInventoryLine.QtyAvailable);
                                        }

                                        ItemReclassJournalLine _saveDataItemReclassJournalLine = new ItemReclassJournalLine(_currSession)
                                        {
                                            ItemReclassJournal = _locItemReclassJournalXPO,
                                            Company = _locItemReclassJournalXPO.Company,
                                            Workplace = _locItemReclassJournalXPO.Workplace,
                                            Division = _locItemReclassJournalXPO.Division,
                                            Department = _locItemReclassJournalXPO.Department,
                                            Section = _locItemReclassJournalXPO.Section,
                                            Employee = _locItemReclassJournalXPO.Employee,
                                            StockType = _locBeginningInventoryLine.StockType,
                                            LocationType = _locBeginningInventoryLine.LocationType,
                                            Location = _locItemReclassJournalXPO.Location,
                                            BinLocation = _locBeginningInventoryLine.BinLocation,
                                            BegInv = _locItemReclassJournalXPO.BeginningInventory,
                                            BegInvLine = _locBeginningInventoryLine,
                                            Item = _locBeginningInventoryLine.Item,
                                            DQty = _locDQty,
                                            DUOM = _locDUOM,
                                            Qty = _locQty,
                                            UOM = _locUOM,
                                            StockGroup = _locBeginningInventoryLine.StockGroup,

                                        };
                                        _saveDataItemReclassJournalLine.Save();
                                        _saveDataItemReclassJournalLine.Session.CommitTransaction();
                                    }
 
                                    #endregion SaveItemReclassJournalLine   
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = ItemReclassJournal ", ex.ToString());
            }
        }

        private UnitOfMeasure GetUOM(Session _currSession, BeginningInventoryLine _locBeginningInventoryLineXPO)
        {
            UnitOfMeasure _result = null;
            try
            {
                if (_locBeginningInventoryLineXPO.Company != null && _locBeginningInventoryLineXPO.Item != null && _locBeginningInventoryLineXPO.DefaultUOM != null)
                {
                    ItemUnitOfMeasure _locIUOM = _currSession.FindObject<ItemUnitOfMeasure>(new GroupOperator(GroupOperatorType.And,
                                                                            new BinaryOperator("Company", _locBeginningInventoryLineXPO.Company),
                                                                            new BinaryOperator("Item", _locBeginningInventoryLineXPO.Item),
                                                                            new BinaryOperator("DefaultUOM", _locBeginningInventoryLineXPO.DefaultUOM),
                                                                            new BinaryOperator("Default", true),
                                                                            new BinaryOperator("Active", true)));
                    if (_locIUOM != null)
                    {
                        if(_locIUOM.UOM != null)
                        {
                            _result = _locIUOM.UOM;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = ItemReclassJournal " + ex.ToString());
            }
            return _result;
        }

        private double GetDQty(Session _currSession, Item _locItem, UnitOfMeasure _locDUOM, UnitOfMeasure _locUOM, double _locTQty)
        {
            double _result = 0;
            

            try
            {
                if (_locItem != null && _locDUOM != null && _locUOM != null && _locTQty > 0)
                {
                    ItemUnitOfMeasure _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                            (new GroupOperator(GroupOperatorType.And,
                                             new BinaryOperator("Item", _locItem),
                                             new BinaryOperator("UOM", _locUOM),
                                             new BinaryOperator("DefaultUOM", _locDUOM),
                                             new BinaryOperator("Default", true),
                                             new BinaryOperator("Active", true)));
                    if (_locItemUOM != null)
                    {
                        if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                        {
                            _result = _locTQty % _locItemUOM.DefaultConversion;
                            
                        }
                        else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                        {
                            _result = System.Math.Floor(_locTQty / _locItemUOM.DefaultConversion);
                            
                        }
                        else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                        {
                            _result = _locTQty;
                        }
                    }
                }
                
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = ItemReclassJournal " + ex.ToString());
            }
            return _result;
        }

        private double GetQty(Session _currSession, Item _locItem, UnitOfMeasure _locDUOM, UnitOfMeasure _locUOM, double _locTQty)
        {
            double _result = 0;
            try
            {
                if (_locItem != null && _locDUOM != null && _locUOM != null && _locTQty > 0)
                {
                    ItemUnitOfMeasure _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                            (new GroupOperator(GroupOperatorType.And,
                                             new BinaryOperator("Item", _locItem),
                                             new BinaryOperator("UOM", _locUOM),
                                             new BinaryOperator("DefaultUOM", _locDUOM),
                                             new BinaryOperator("Default", true),
                                             new BinaryOperator("Active", true)));
                    if (_locItemUOM != null)
                    {
                        if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                        {

                            _result = System.Math.Floor(_locTQty / _locItemUOM.DefaultConversion);
                        }
                        else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                        {

                            _result = (_locTQty % _locItemUOM.DefaultConversion) * _locItemUOM.Conversion;
                        }
                        else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                        {
                            _result = _locTQty;
                        }
                    }
                }
                
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = ItemReclassJournal " + ex.ToString());
            }
            return _result;
        }

        #endregion GetStock

        #region Posting

        private void SetItemReclassJournalMonitoring(Session _currSession, ItemReclassJournal _locItemReclassJournalXPO)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                DateTime now = DateTime.Now;

                if (_locItemReclassJournalXPO != null)
                {
                    XPCollection<ItemReclassJournalLine> _locItemReclassJournalLines = new XPCollection<ItemReclassJournalLine>
                                                                    (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                    new BinaryOperator("ItemReclassJournal", _locItemReclassJournalXPO),
                                                                    new BinaryOperator("Select", true),
                                                                    new GroupOperator(GroupOperatorType.Or,
                                                                    new BinaryOperator("Status", Status.Progress),
                                                                    new BinaryOperator("Status", Status.Posted))));

                    if (_locItemReclassJournalLines != null && _locItemReclassJournalLines.Count() > 0)
                    {
                        foreach (ItemReclassJournalLine _locItemReclassJournalLine in _locItemReclassJournalLines)
                        {
                            ItemReclassJournalMonitoring _saveDataItemReclassJournalMonitoring = new ItemReclassJournalMonitoring(_currSession)
                            {
                                ItemReclassJournal = _locItemReclassJournalXPO,
                                ItemReclassJournalLine = _locItemReclassJournalLine,
                                Company = _locItemReclassJournalLine.Company,
                                Workplace = _locItemReclassJournalLine.Workplace,
                                Division = _locItemReclassJournalLine.Division,
                                Department = _locItemReclassJournalLine.Department,
                                Section = _locItemReclassJournalLine.Section,
                                Employee = _locItemReclassJournalLine.Employee,
                                LocationType = _locItemReclassJournalLine.LocationType,
                                Location = _locItemReclassJournalLine.Location,
                                BinLocation = _locItemReclassJournalLine.BinLocation,
                                StockType = _locItemReclassJournalLine.StockType,
                                StockGroup = _locItemReclassJournalLine.StockGroup,
                                BegInv = _locItemReclassJournalLine.BegInv,
                                BegInvLine = _locItemReclassJournalLine.BegInvLine,
                                Item = _locItemReclassJournalLine.Item,
                                Brand = _locItemReclassJournalLine.Brand,
                                Description = _locItemReclassJournalLine.Description,
                                DQty = _locItemReclassJournalLine.DQty,
                                DUOM = _locItemReclassJournalLine.DUOM,
                                Qty = _locItemReclassJournalLine.Qty,
                                UOM = _locItemReclassJournalLine.UOM,
                                TQty = _locItemReclassJournalLine.TQty,
                            };
                            _saveDataItemReclassJournalMonitoring.Save();
                            _saveDataItemReclassJournalMonitoring.Session.CommitTransaction();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = ItemReclassJournal ", ex.ToString());
            }
        }

        private void SetAdjustBeginingInventory(Session _currSession, ItemReclassJournal _locItemReclassJournalXPO)
        {
            try
            {
                XPCollection<ItemReclassJournalLine> _locItemReclassJournalLines = new XPCollection<ItemReclassJournalLine>(_currSession,
                                                                        new GroupOperator(GroupOperatorType.And,
                                                                        new BinaryOperator("ItemReclassJournal", _locItemReclassJournalXPO),
                                                                        new BinaryOperator("Select", true),
                                                                        new GroupOperator(GroupOperatorType.Or,
                                                                        new BinaryOperator("Status", Status.Progress),
                                                                        new BinaryOperator("Status", Status.Posted))));

                if (_locItemReclassJournalLines != null && _locItemReclassJournalLines.Count() > 0)
                {
                    
                    foreach (ItemReclassJournalLine _locItemReclassJournalLine in _locItemReclassJournalLines)
                    {
                        #region NonLotNumber
                        if (_locItemReclassJournalLine.BegInv != null)
                        {
                            if (_locItemReclassJournalLine.BegInvLine != null)
                            {
                                #region UpdateBeginingInventoryLine

                                _locItemReclassJournalLine.BegInvLine.QtyAvailable = _locItemReclassJournalLine.TQty;
                                _locItemReclassJournalLine.BegInvLine.Save();
                                _locItemReclassJournalLine.BegInvLine.Session.CommitTransaction();

                                #endregion UpdateBeginingInventoryLine
                            }
                            else
                            {
                                BeginningInventoryLine _saveDataBegInvLine = new BeginningInventoryLine(_currSession)
                                {
                                    Company = _locItemReclassJournalXPO.Company,
                                    Workplace = _locItemReclassJournalXPO.Workplace,
                                    LocationType = _locItemReclassJournalLine.LocationType,
                                    Location = _locItemReclassJournalXPO.Location,
                                    BinLocation = _locItemReclassJournalLine.BinLocation,
                                    Item = _locItemReclassJournalLine.Item,
                                    Brand = _locItemReclassJournalLine.Brand,
                                    ItemType = _locItemReclassJournalLine.ItemType,
                                    DefaultUOM = _locItemReclassJournalLine.DUOM,
                                    BeginningInventory = _locItemReclassJournalLine.BegInv,
                                    StockType = _locItemReclassJournalLine.StockType,
                                    StockGroup = _locItemReclassJournalLine.StockGroup,
                                    QtyBegining = _locItemReclassJournalLine.TQty,
                                    QtyAvailable = _locItemReclassJournalLine.TQty,
                                    Active = true,
                                };
                                _saveDataBegInvLine.Save();
                                _saveDataBegInvLine.Session.CommitTransaction();
                            }

                            _locItemReclassJournalLine.BegInv.QtyAvailable = _locItemReclassJournalLine.BegInv.QtyAvailable + _locItemReclassJournalLine.TQty;
                            _locItemReclassJournalLine.BegInv.Save();
                            _locItemReclassJournalLine.BegInv.Session.CommitTransaction();
                        }
                        #endregion NonLotNumber  
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = ItemReclassJournal ", ex.ToString());
            }
        }

        private void SetAdjustInventoryJournal(Session _currSession, ItemReclassJournal _locItemReclassJournalXPO)
        {
            try
            {
                XPCollection<ItemReclassJournalLine> _locItemReclassJournalLines = new XPCollection<ItemReclassJournalLine>(_currSession,
                                                                        new GroupOperator(GroupOperatorType.And,
                                                                        new BinaryOperator("ItemReclassJournal", _locItemReclassJournalXPO),
                                                                        new BinaryOperator("Select", true),
                                                                        new GroupOperator(GroupOperatorType.Or,
                                                                        new BinaryOperator("Status", Status.Progress),
                                                                        new BinaryOperator("Status", Status.Posted))));

                if (_locItemReclassJournalLines != null && _locItemReclassJournalLines.Count > 0)
                {
                    DateTime now = DateTime.Now;

                    foreach (ItemReclassJournalLine _locItemReclassJournalLine in _locItemReclassJournalLines)
                    {
                        InventoryJournal _saveDateInventoryJournal = new InventoryJournal(_currSession)
                        {
                            Company = _locItemReclassJournalLine.Company,
                            Workplace = _locItemReclassJournalLine.Workplace,
                            DocumentType = _locItemReclassJournalXPO.DocumentType,
                            DocNo = _locItemReclassJournalXPO.DocNo,
                            Location = _locItemReclassJournalLine.Location,
                            BinLocation = _locItemReclassJournalLine.BinLocation,
                            StockType = _locItemReclassJournalLine.StockType,
                            StockGroup = _locItemReclassJournalLine.StockGroup,
                            Item = _locItemReclassJournalLine.Item,
                            QtyPos = _locItemReclassJournalLine.TQty,
                            QtyNeg = 0,
                            DUOM = _locItemReclassJournalLine.DUOM,
                            JournalDate = now,
                            ItemReclassJournal = _locItemReclassJournalXPO,
                        };
                        _saveDateInventoryJournal.Save();
                        _saveDateInventoryJournal.Session.CommitTransaction();
                        
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = ItemReclassJournal ", ex.ToString());
            }
        }

        private void SetStatuItemReclassJournalLine(Session _currSession, ItemReclassJournal _locItemReclassJournalXPO)
        {
            try
            {
                DateTime now = DateTime.Now;
                if (_locItemReclassJournalXPO != null)
                {
                    XPCollection<ItemReclassJournalLine> _locItemReclassJournalLines = new XPCollection<ItemReclassJournalLine>(_currSession,
                                                                        new GroupOperator(GroupOperatorType.And,
                                                                        new BinaryOperator("ItemReclassJournal", _locItemReclassJournalXPO),
                                                                        new BinaryOperator("Select", true),
                                                                        new GroupOperator(GroupOperatorType.Or,
                                                                        new BinaryOperator("Status", Status.Progress),
                                                                        new BinaryOperator("Status", Status.Posted))));

                    if (_locItemReclassJournalLines != null && _locItemReclassJournalLines.Count > 0)
                    {
                        foreach (ItemReclassJournalLine _locItemReclassJournalLine in _locItemReclassJournalLines)
                        {
                            _locItemReclassJournalLine.Status = Status.Close;
                            _locItemReclassJournalLine.ActivationPosting = true;
                            _locItemReclassJournalLine.StatusDate = now;
                            _locItemReclassJournalLine.Select = false;
                            _locItemReclassJournalLine.Save();
                            _locItemReclassJournalLine.Session.CommitTransaction(); 

                            if(_locItemReclassJournalLine.BegInvLine != null)
                            {
                                _locItemReclassJournalLine.BegInvLine.Lock = false;
                                _locItemReclassJournalLine.BegInvLine.Save();
                                _locItemReclassJournalLine.BegInvLine.Session.CommitTransaction();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = ItemReclassJournal " + ex.ToString());
            }
        }

        private void SetFinalStatusDeliverTransferOut(Session _currSession, ItemReclassJournal _locItemReclassJournalXPO)
        {
            try
            {
                DateTime now = DateTime.Now;
                int _locStatusCount = 0;
                int _locTransOutLineCount = 0;

                if (_locItemReclassJournalXPO != null)
                {
                    XPCollection<ItemReclassJournalLine> _locItemReclassJournalLines = new XPCollection<ItemReclassJournalLine>(_currSession,
                                                                                     new GroupOperator(GroupOperatorType.And,
                                                                                     new BinaryOperator("ItemReclassJournal", _locItemReclassJournalXPO)));

                    if (_locItemReclassJournalLines != null && _locItemReclassJournalLines.Count() > 0)
                    {
                        _locTransOutLineCount = _locItemReclassJournalLines.Count();

                        foreach (ItemReclassJournalLine _locItemReclassJournalLine in _locItemReclassJournalLines)
                        {

                            if (_locItemReclassJournalLine.Status == Status.Close)
                            {
                                _locStatusCount = _locStatusCount + 1;
                            }
                        }

                        if (_locTransOutLineCount == _locStatusCount)
                        {
                            _locItemReclassJournalXPO.ActivationPosting = true;
                            _locItemReclassJournalXPO.Status = Status.Close;
                            _locItemReclassJournalXPO.StatusDate = now;
                            _locItemReclassJournalXPO.Save();
                            _locItemReclassJournalXPO.Session.CommitTransaction();
                        }
                        else
                        {
                            _locItemReclassJournalXPO.Status = Status.Posted;
                            _locItemReclassJournalXPO.StatusDate = now;
                            _locItemReclassJournalXPO.Save();
                            _locItemReclassJournalXPO.Session.CommitTransaction();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = ItemReclassJournal " + ex.ToString());
            }
        }

        #endregion Posting

        #region Global Method

        private void SuccessMessageShow(string _locMessage)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 4000;
            options.Message = string.Format("{0}", _locMessage);
            options.Type = InformationType.Success;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Success";
            options.Win.Type = WinMessageType.Flyout;
            Application.ShowViewStrategy.ShowMessage(options);
        }

        private void ErrorMessageShow(string _locMessage)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 2000;
            options.Message = string.Format(_locMessage);
            options.Type = InformationType.Warning;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Error";
            options.Win.Type = WinMessageType.Alert;
            Application.ShowViewStrategy.ShowMessage(options);
        }

        #endregion Global Method

        
    }
}
