﻿namespace FullDrive.Module.Controllers
{
    partial class TransferInLineActionController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.TransferInLineSelectAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.TransferInLineUnselectAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            // 
            // TransferInLineSelectAction
            // 
            this.TransferInLineSelectAction.Caption = "Select";
            this.TransferInLineSelectAction.ConfirmationMessage = null;
            this.TransferInLineSelectAction.Id = "TransferInLineSelectActionId";
            this.TransferInLineSelectAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.TransferInLine);
            this.TransferInLineSelectAction.ToolTip = null;
            this.TransferInLineSelectAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.TransferInLineSelectAction_Execute);
            // 
            // TransferInLineUnselectAction
            // 
            this.TransferInLineUnselectAction.Caption = "Unselect";
            this.TransferInLineUnselectAction.ConfirmationMessage = null;
            this.TransferInLineUnselectAction.Id = "TransferInLineUnselectActionId";
            this.TransferInLineUnselectAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.TransferInLine);
            this.TransferInLineUnselectAction.ToolTip = null;
            this.TransferInLineUnselectAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.TransferInLineUnselectAction_Execute);
            // 
            // TransferInLineActionController
            // 
            this.Actions.Add(this.TransferInLineSelectAction);
            this.Actions.Add(this.TransferInLineUnselectAction);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SimpleAction TransferInLineSelectAction;
        private DevExpress.ExpressApp.Actions.SimpleAction TransferInLineUnselectAction;
    }
}
