﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;

using FullDrive.Module.BusinessObjects;
using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.Xpo;
using System.Collections;
using DevExpress.Xpo;
using System.IO;
using System.Web;

namespace FullDrive.Module.Controllers
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class InvoiceCanvassingActionController : ViewController
    {

        private ChoiceActionItem _selectionListviewFilter;

        public InvoiceCanvassingActionController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.
            #region Filter Status
            InvoiceCanvassingListviewFilterSelectionAction.Items.Clear();
            foreach (object _currApproval in Enum.GetValues(typeof(CustomProcess.Status)))
            {
                EnumDescriptor _ed = new EnumDescriptor(typeof(CustomProcess.Status));
                _selectionListviewFilter = new ChoiceActionItem(_ed.GetCaption(_currApproval), _currApproval);
                InvoiceCanvassingListviewFilterSelectionAction.Items.Add(_selectionListviewFilter);
            }
            #endregion Filter Status
        }
        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }
        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        private void InvoiceCanvassingListviewFilterSelectionAction_Execute(object sender, SingleChoiceActionExecuteEventArgs e)
        {
            try
            {
                if ((View is ListView) & (View.ObjectTypeInfo.Type == typeof(InvoiceCanvassing)))
                {
                    if ((Status)e.SelectedChoiceActionItem.Data == Status.None)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.None, BinaryOperatorType.NotEqual);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Open)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Open, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Progress)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Progress, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Posted)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Posted, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Close)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Close, BinaryOperatorType.Equal);
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = InvoiceCanvassing " + ex.ToString());
            }
        }

        private void InvoiceCanvassingProgressAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;
                Status _locStatus = Status.Open;

                if (_objectsToProcess != null)
                {
                    foreach (Object obj in _objectsToProcess)
                    {
                        InvoiceCanvassing _locInvoiceCanvassingOS = (InvoiceCanvassing)_objectSpace.GetObject(obj);

                        if (_locInvoiceCanvassingOS != null)
                        {
                            if (_locInvoiceCanvassingOS.Session != null)
                            {
                                _currSession = _locInvoiceCanvassingOS.Session;
                            }

                            if (_locInvoiceCanvassingOS.Code != null && _currSession != null)
                            {
                                _currObjectId = _locInvoiceCanvassingOS.Code;

                                InvoiceCanvassing _locInvoiceCanvassingXPO = _currSession.FindObject<InvoiceCanvassing>
                                                                   (new GroupOperator(GroupOperatorType.And,
                                                                    new BinaryOperator("Code", _currObjectId)));

                                if (_locInvoiceCanvassingXPO != null)
                                {
                                    if (_locInvoiceCanvassingXPO.Status == Status.Open || _locInvoiceCanvassingXPO.Status == Status.Progress || _locInvoiceCanvassingXPO.Status == Status.Posted)
                                    {
                                        if (_locInvoiceCanvassingXPO.Status == Status.Open )
                                        {
                                            _locStatus = Status.Progress;

                                        }
                                        else 
                                        {
                                            _locStatus = _locInvoiceCanvassingXPO.Status;
                                        }

                                        _locInvoiceCanvassingXPO.Status = _locStatus;
                                        _locInvoiceCanvassingXPO.StatusDate = now;
                                        _locInvoiceCanvassingXPO.Save();
                                        _locInvoiceCanvassingXPO.Session.CommitTransaction();

                                        #region PurchaseInvoiceLine
                                        XPCollection<InvoiceCanvassingLine> _locInvoiceCanvassingLines = new XPCollection<InvoiceCanvassingLine>
                                                                                               (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                new BinaryOperator("InvoiceCanvassing", _locInvoiceCanvassingXPO)));

                                        if (_locInvoiceCanvassingLines != null && _locInvoiceCanvassingLines.Count > 0)
                                        {
                                            foreach (InvoiceCanvassingLine _locInvoiceCanvassingLine in _locInvoiceCanvassingLines)
                                            {
                                                if (_locInvoiceCanvassingLine.Status == Status.Open)
                                                {
                                                    _locInvoiceCanvassingLine.Status = Status.Progress;
                                                    _locInvoiceCanvassingLine.StatusDate = now;
                                                    _locInvoiceCanvassingLine.ActivationPosting = true;
                                                    _locInvoiceCanvassingLine.Save();
                                                    _locInvoiceCanvassingLine.Session.CommitTransaction();
                                                }
                                            }
                                        }
                                        #endregion PurchaseInvoiceLine

                                        SetTotalAmountAndTaxAmountAndDiscountAmount(_currSession, _locInvoiceCanvassingXPO);

                                        SuccessMessageShow(_locInvoiceCanvassingXPO.Code + " has been change successfully to Progress ");
                                    }
                                }
                                else
                                {
                                    ErrorMessageShow("Data InvoiceCanvassing Not Available");
                                }
                            }
                            else
                            {
                                ErrorMessageShow("Data InvoiceCanvassing Not Available");
                            }
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = InvoiceCanvassing " + ex.ToString());
            }
        }

        private void InvoiceCanvassingSetFreeItemAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;

                if (_objectsToProcess != null)
                {
                    foreach (Object obj in _objectsToProcess)
                    {
                        InvoiceCanvassing _locInvoiceCanvassingOS = (InvoiceCanvassing)_objectSpace.GetObject(obj);

                        if (_locInvoiceCanvassingOS != null)
                        {
                            if (_locInvoiceCanvassingOS.Session != null)
                            {
                                _currSession = _locInvoiceCanvassingOS.Session;
                            }

                            if (_locInvoiceCanvassingOS.Code != null && _currSession != null)
                            {
                                _currObjectId = _locInvoiceCanvassingOS.Code;

                                InvoiceCanvassing _locInvoiceCanvassingXPO = _currSession.FindObject<InvoiceCanvassing>
                                                                   (new GroupOperator(GroupOperatorType.And,
                                                                    new BinaryOperator("Code", _currObjectId),
                                                                    new BinaryOperator("Status", Status.Progress)));

                                if (_locInvoiceCanvassingXPO != null)
                                {
                                    SetFreeItem(_currSession, _locInvoiceCanvassingXPO);
                                }
                                else
                                {
                                    ErrorMessageShow("Data InvoiceCanvassing Not Available");
                                }
                            }
                            else
                            {
                                ErrorMessageShow("Data InvoiceCanvassing Not Available");
                            }
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = InvoiceCanvassing " + ex.ToString());
            }
        }

        private void InvoiceCanvassingPostingAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;

                if (_objectsToProcess != null)
                {
                    foreach (Object obj in _objectsToProcess)
                    {
                        InvoiceCanvassing _locInvoiceCanvassingOS = (InvoiceCanvassing)_objectSpace.GetObject(obj);

                        if (_locInvoiceCanvassingOS != null)
                        {
                            if (_locInvoiceCanvassingOS.Session != null)
                            {
                                _currSession = _locInvoiceCanvassingOS.Session;
                            }

                            if (_locInvoiceCanvassingOS.Code != null && _currSession != null)
                            {
                                _currObjectId = _locInvoiceCanvassingOS.Code;

                                InvoiceCanvassing _locInvoiceCanvassingXPO = _currSession.FindObject<InvoiceCanvassing>
                                                                   (new GroupOperator(GroupOperatorType.And,
                                                                    new BinaryOperator("Code", _currObjectId),
                                                                    new GroupOperator(GroupOperatorType.Or,
                                                                    new BinaryOperator("Status", Status.Progress),
                                                                    new BinaryOperator("Status", Status.Posted))));

                                if (_locInvoiceCanvassingXPO != null)
                                {
                                    if (_locInvoiceCanvassingXPO.TotAmount > 0)
                                    {
                                        //Berdasarkan Outstanding dari PaymentOutPlan yg mana di hitung berdasarkan jumlah mak Outstanding 
                                        if (_locInvoiceCanvassingXPO.TotAmount == GetInvoiceCanvassingLineTotalAmount(_currSession, _locInvoiceCanvassingXPO))
                                        {
                                            SetInvoiceCanvassingMonitoring(_currSession, _locInvoiceCanvassingXPO);
                                            SetCanvasPaymentIn(_currSession, _locInvoiceCanvassingXPO);
                                            SetDeliverBeginingInventory(_currSession, _locInvoiceCanvassingXPO);
                                            SetDeliverInventoryJournal(_currSession, _locInvoiceCanvassingXPO);
                                            SetStatusInvoiceCanvassingLine(_currSession, _locInvoiceCanvassingXPO);
                                            SetStatusInvoiceCanvassingFreeItem(_currSession, _locInvoiceCanvassingXPO);
                                            SetFinalInvoiceCanvassing(_currSession, _locInvoiceCanvassingXPO);
                                            SuccessMessageShow(_locInvoiceCanvassingXPO.Code + " has been change successfully to Posted");
                                        }
                                    }
                                }
                                else
                                {
                                    ErrorMessageShow("Data InvoiceCanvassing Not Available");
                                }
                            }
                            else
                            {
                                ErrorMessageShow("Data InvoiceCanvassing Not Available");
                            }
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = InvoiceCanvassing " + ex.ToString());
            }
        }

        //========================================== Code In Here ==========================================

        #region GetAmount

        private void SetTotalAmountAndTaxAmountAndDiscountAmount(Session _currSession, InvoiceCanvassing _locInvoiceCanvassingXPO)
        {
            try
            {
                double _locTAmount = 0;
                double _locDiscAmountRule2 = 0;
                double _locTxAmount = 0;
                double _locDiscAmountRule1 = 0;
                if(_locInvoiceCanvassingXPO != null)
                {
                    XPCollection<InvoiceCanvassingLine> _locInvoiceCanvassingLines = new XPCollection<BusinessObjects.InvoiceCanvassingLine>
                                                                                (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                new BinaryOperator("InvoiceCanvassing", _locInvoiceCanvassingXPO),
                                                                                new GroupOperator(GroupOperatorType.Or,
                                                                                new BinaryOperator("Status", Status.Open),
                                                                                new BinaryOperator("Status", Status.Progress))));
                    if(_locInvoiceCanvassingLines != null && _locInvoiceCanvassingLines.Count() > 0)
                    {
                        foreach(InvoiceCanvassingLine _locInvoiceCanvassingLine in _locInvoiceCanvassingLines)
                        {
                            _locTAmount = _locTAmount + _locInvoiceCanvassingLine.TAmount;
                            _locDiscAmountRule2 = _locDiscAmountRule2 + _locInvoiceCanvassingLine.DiscAmount;
                            if (_locInvoiceCanvassingLine.Tax != null)
                            {
                                if(_locInvoiceCanvassingLine.Tax.TaxNature == TaxNature.Increase)
                                {
                                    _locTxAmount = _locTxAmount + _locInvoiceCanvassingLine.TxAmount;
                                }
                                if (_locInvoiceCanvassingLine.Tax.TaxNature == TaxNature.Decrease)
                                {
                                    _locTxAmount = _locTxAmount - _locInvoiceCanvassingLine.TxAmount;
                                }
                            }  
                        }

                        _locDiscAmountRule1 = GetDiscountRule1(_currSession, _locInvoiceCanvassingXPO);
                    }

                    _locInvoiceCanvassingXPO.TotAmount = _locTAmount;
                    _locInvoiceCanvassingXPO.TotDiscAmount = _locDiscAmountRule2 + _locDiscAmountRule1;
                    _locInvoiceCanvassingXPO.TotTaxAmount = _locTxAmount;
                    _locInvoiceCanvassingXPO.Amount = _locTAmount - _locDiscAmountRule1;
                    _locInvoiceCanvassingXPO.Save();
                    _locInvoiceCanvassingXPO.Session.CommitTransaction();
                }              
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = InvoiceCanvassing ", ex.ToString());
            }
            
        }

        #endregion GetAmount

        #region FreeItem

        private void SetFreeItem(Session _currSession, InvoiceCanvassing _locInvoiceCanvassingXPO)
        {
            try
            {
                if (_locInvoiceCanvassingXPO != null)
                {
                    XPQuery<InvoiceCanvassingLine> _invoiceCanvassingLinesQuery = new XPQuery<InvoiceCanvassingLine>(_currSession);

                    var _invoiceCanvassingLines = from icl in _invoiceCanvassingLinesQuery
                                                   where (icl.InvoiceCanvassing == _locInvoiceCanvassingXPO
                                                   && icl.Status == Status.Progress
                                                   )
                                                   group icl by icl.Item into g
                                                   select new { Item = g.Key };

                    if (_invoiceCanvassingLines != null && _invoiceCanvassingLines.Count() > 0)
                    {
                        foreach (var _invoiceCanvassingLine in _invoiceCanvassingLines)
                        {
                            XPCollection<FreeItemRule> _locFreeItemRules = new XPCollection<FreeItemRule>
                                                            (_currSession, new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("Company", _locInvoiceCanvassingXPO.Company),
                                                            new BinaryOperator("Workplace", _locInvoiceCanvassingXPO.Workplace),
                                                            new BinaryOperator("PriceGroup", _locInvoiceCanvassingXPO.PriceGroup),
                                                            new BinaryOperator("MpItem", _invoiceCanvassingLine.Item),
                                                            new BinaryOperator("Active", true)));

                            if (_locFreeItemRules != null && _locFreeItemRules.Count() > 0)
                            {
                                foreach (FreeItemRule _locFreeItemRule in _locFreeItemRules)
                                {
                                    #region MultiItem=false
                                    if (_locFreeItemRule.MultiItem == false)
                                    {
                                        //1+1+1+1+1
                                        #region Rule1
                                        if(_locFreeItemRule.RuleType == RuleType.Rule1)
                                        {
                                            XPCollection<InvoiceCanvassingLine> _locInvoiceCanvassingLines = new XPCollection<InvoiceCanvassingLine>
                                                                                                   (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                   new BinaryOperator("InvoiceCanvassing", _locInvoiceCanvassingXPO),
                                                                                                   new BinaryOperator("Item", _invoiceCanvassingLine.Item),
                                                                                                   new BinaryOperator("Status", Status.Progress),
                                                                                                   new BinaryOperator("FreeItemChecked", false)));

                                            if (_locInvoiceCanvassingLines != null && _locInvoiceCanvassingLines.Count() > 0)
                                            {
                                                #region Foreach
                                                foreach (InvoiceCanvassingLine _locInvoiceCanvassingLine in _locInvoiceCanvassingLines)
                                                {
                                                    double _locQtyFI = 0;
                                                    double _locTotQtyFI = 0;
                                                    double _locFpDqty = 0;
                                                    double _locFpQty = 0;
                                                    int _locMaxLoop = 0;
                                                    int _locTotMaxLoopVal = 0;

                                                    if (_locInvoiceCanvassingLine.TQty >= _locFreeItemRule.MpTQty)
                                                    {
                                                        _locQtyFI = System.Math.Floor(_locInvoiceCanvassingLine.TQty / _locFreeItemRule.MpTQty);

                                                        if (_locFreeItemRule.MaxLoop == 0)
                                                        {
                                                            _locMaxLoop = (int)_locQtyFI;
                                                        }
                                                        else
                                                        {
                                                            if(_locFreeItemRule.MaxLoop > (int)_locQtyFI)
                                                            {
                                                                _locMaxLoop = (int)_locQtyFI;
                                                            }
                                                            else
                                                            {
                                                                _locMaxLoop = _locFreeItemRule.MaxLoop;
                                                            }
                                                            
                                                        }

                                                        for (int i = 1; i <= _locMaxLoop; i++)
                                                        {
                                                            _locTotMaxLoopVal = _locTotMaxLoopVal + 1;
                                                        }

                                                        _locTotQtyFI = _locTotMaxLoopVal * _locFreeItemRule.FpTQty;

                                                       
                                                        if (_locFreeItemRule.FpItem != null && _locFreeItemRule.FpDUOM != null && _locFreeItemRule.FpUOM != null)
                                                        {
                                                            ItemUnitOfMeasure _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                                     new BinaryOperator("Item", _locFreeItemRule.FpItem),
                                                                                     new BinaryOperator("UOM", _locFreeItemRule.FpUOM),
                                                                                     new BinaryOperator("DefaultUOM", _locFreeItemRule.FpDUOM),
                                                                                     new BinaryOperator("Active", true)));
                                                            if (_locItemUOM != null)
                                                            {
                                                                if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                                                {
                                                                    _locFpDqty = _locTotQtyFI % _locItemUOM.DefaultConversion ;
                                                                    _locFpQty = System.Math.Floor(_locTotQtyFI / _locItemUOM.DefaultConversion);  
                                                                }
                                                                else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                                                {
                                                                    _locFpDqty = System.Math.Floor(_locTotQtyFI/ _locItemUOM.DefaultConversion);
                                                                    _locFpQty = (_locTotQtyFI % _locItemUOM.DefaultConversion ) * _locItemUOM.Conversion;
                                                                }
                                                                else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                                                {
                                                                    _locFpDqty = _locTotQtyFI;
                                                                }
                                                            }
                                                        }
                                                        else
                                                        {
                                                            _locFpDqty = _locTotQtyFI;
                                                        }

                                                        InvoiceCanvassingFreeItem _saveDataInvoiceCanvassingFreeItem = new InvoiceCanvassingFreeItem(_currSession)
                                                        {
                                                            Company = _locInvoiceCanvassingXPO.Company,
                                                            Workplace = _locInvoiceCanvassingXPO.Workplace,
                                                            Division = _locInvoiceCanvassingXPO.Division,
                                                            Department = _locInvoiceCanvassingXPO.Department,
                                                            Section = _locInvoiceCanvassingXPO.Section,
                                                            Employee = _locInvoiceCanvassingXPO.Employee,
                                                            InvoiceCanvassing = _locInvoiceCanvassingXPO,
                                                            InvoiceCanvassingLine = _locInvoiceCanvassingLine,
                                                            FpItem = _locFreeItemRule.FpItem,
                                                            FpDQty = _locFpDqty,
                                                            FpDUOM = _locFreeItemRule.FpDUOM,
                                                            FpQty = _locFpQty,
                                                            FpUOM = _locFreeItemRule.FpUOM,
                                                            FpTQty = _locTotQtyFI,
                                                        };
                                                        _saveDataInvoiceCanvassingFreeItem.Save();
                                                        _saveDataInvoiceCanvassingFreeItem.Session.CommitTransaction();

                                                        _locInvoiceCanvassingLine.FreeItemChecked = true;
                                                        _locInvoiceCanvassingLine.Save();
                                                        _locInvoiceCanvassingLine.Session.CommitTransaction();
                                                    }
                                                }
                                                #endregion Foreach
                                            }
                                        }

                                        #endregion Rule1

                                        //1, 2+1, 3+1, 4+1
                                        #region Rule2
                                        if (_locFreeItemRule.RuleType == RuleType.Rule2)
                                        {
                                            XPCollection<InvoiceCanvassingLine> _locInvoiceCanvassingLines = new XPCollection<InvoiceCanvassingLine>
                                                                                                   (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                   new BinaryOperator("InvoiceCanvassing", _locInvoiceCanvassingXPO),
                                                                                                   new BinaryOperator("Item", _invoiceCanvassingLine.Item),
                                                                                                   new BinaryOperator("Status", Status.Progress),
                                                                                                   new BinaryOperator("FreeItemChecked", false)));

                                            if (_locInvoiceCanvassingLines != null && _locInvoiceCanvassingLines.Count() > 0)
                                            {

                                                foreach (InvoiceCanvassingLine _locInvoiceCanvassingLine in _locInvoiceCanvassingLines)
                                                {
                                                    double _locQtyFI = 0;
                                                    double _locTotQtyFI = 0;
                                                    double _locFpDqty = 0;
                                                    double _locFpQty = 0;
                                                    int _locMaxLoop = 0;

                                                    if (_locInvoiceCanvassingLine.TQty >= _locFreeItemRule.MpTQty)
                                                    {
                                                        _locQtyFI = System.Math.Floor(_locInvoiceCanvassingLine.TQty / _locFreeItemRule.MpTQty);

                                                        if(_locFreeItemRule.MaxLoop == 0)
                                                        {
                                                            _locMaxLoop = (int)_locQtyFI;
                                                        }else
                                                        {
                                                            if (_locFreeItemRule.MaxLoop > (int)_locQtyFI)
                                                            {
                                                                _locMaxLoop = (int)_locQtyFI;
                                                            }
                                                            else
                                                            {
                                                                _locMaxLoop = _locFreeItemRule.MaxLoop;
                                                            }
                                                        }

                                                        for (int i = 1; i <= _locMaxLoop; i++)
                                                        {
                                                            if(i == 1)
                                                            {
                                                                _locTotQtyFI = _locFreeItemRule.FpTQty;
                                                            }else
                                                            {
                                                                _locTotQtyFI = _locTotQtyFI + (i + _locFreeItemRule.FpTQty);
                                                            }
                                                            
                                                        }

                                                        if (_locFreeItemRule.FpItem != null && _locFreeItemRule.FpDUOM != null && _locFreeItemRule.FpUOM != null)
                                                        {
                                                            ItemUnitOfMeasure _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                                     new BinaryOperator("Item", _locFreeItemRule.FpItem),
                                                                                     new BinaryOperator("UOM", _locFreeItemRule.FpUOM),
                                                                                     new BinaryOperator("DefaultUOM", _locFreeItemRule.FpDUOM),
                                                                                     new BinaryOperator("Active", true)));
                                                            if (_locItemUOM != null)
                                                            {
                                                                if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                                                {
                                                                    _locFpDqty = _locTotQtyFI % _locItemUOM.DefaultConversion;
                                                                    _locFpQty = System.Math.Floor(_locTotQtyFI / _locItemUOM.DefaultConversion);
                                                                }
                                                                else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                                                {
                                                                    _locFpDqty = System.Math.Floor(_locTotQtyFI / _locItemUOM.DefaultConversion);
                                                                    _locFpQty = (_locTotQtyFI % _locItemUOM.DefaultConversion) * _locItemUOM.Conversion;
                                                                }
                                                                else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                                                {
                                                                    _locFpDqty = _locTotQtyFI;
                                                                }
                                                            }
                                                        }
                                                        else
                                                        {
                                                            _locFpDqty = _locTotQtyFI;
                                                        }
                                                        InvoiceCanvassingFreeItem _saveDataInvoiceCanvassingFreeItem = new InvoiceCanvassingFreeItem(_currSession)
                                                        {
                                                            Company = _locInvoiceCanvassingXPO.Company,
                                                            Workplace = _locInvoiceCanvassingXPO.Workplace,
                                                            Division = _locInvoiceCanvassingXPO.Division,
                                                            Department = _locInvoiceCanvassingXPO.Department,
                                                            Section = _locInvoiceCanvassingXPO.Section,
                                                            Employee = _locInvoiceCanvassingXPO.Employee,
                                                            InvoiceCanvassing = _locInvoiceCanvassingXPO,
                                                            InvoiceCanvassingLine = _locInvoiceCanvassingLine,
                                                            FpItem = _locFreeItemRule.FpItem,
                                                            FpDQty = _locFpDqty,
                                                            FpDUOM = _locFreeItemRule.FpDUOM,
                                                            FpQty = _locFpQty,
                                                            FpUOM = _locFreeItemRule.FpUOM,
                                                            FpTQty = _locTotQtyFI,
                                                        };
                                                        _saveDataInvoiceCanvassingFreeItem.Save();
                                                        _saveDataInvoiceCanvassingFreeItem.Session.CommitTransaction();

                                                        _locInvoiceCanvassingLine.FreeItemChecked = true;
                                                        _locInvoiceCanvassingLine.Save();
                                                        _locInvoiceCanvassingLine.Session.CommitTransaction();
                                                    }
                                                }
                                            }
                                        }
                                        #endregion Rule2

                                        //2 * 2 * 2 * 2
                                        #region Rule3
                                        if (_locFreeItemRule.RuleType == RuleType.Rule3)
                                        {
                                            XPCollection<InvoiceCanvassingLine> _locInvoiceCanvassingLines = new XPCollection<InvoiceCanvassingLine>
                                                                                                   (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                   new BinaryOperator("InvoiceCanvassing", _locInvoiceCanvassingXPO),
                                                                                                   new BinaryOperator("Item", _invoiceCanvassingLine.Item),
                                                                                                   new BinaryOperator("Status", Status.Progress),
                                                                                                   new BinaryOperator("FreeItemChecked", false)));

                                            if (_locInvoiceCanvassingLines != null && _locInvoiceCanvassingLines.Count() > 0)
                                            {

                                                foreach (InvoiceCanvassingLine _locInvoiceCanvassingLine in _locInvoiceCanvassingLines)
                                                {
                                                    double _locQtyFI = 0;
                                                    double _locTotQtyFI = 0;
                                                    double _locFpDqty = 0;
                                                    double _locFpQty = 0;
                                                    int _locMaxLoop = 0;

                                                    if (_locInvoiceCanvassingLine.TQty >= _locFreeItemRule.MpTQty)
                                                    {
                                                        _locQtyFI = System.Math.Floor(_locInvoiceCanvassingLine.TQty / _locFreeItemRule.MpTQty);

                                                        if (_locFreeItemRule.MaxLoop == 0)
                                                        {
                                                            _locMaxLoop = (int)_locQtyFI;
                                                        }
                                                        else
                                                        {
                                                            if (_locFreeItemRule.MaxLoop > (int)_locQtyFI)
                                                            {
                                                                _locMaxLoop = (int)_locQtyFI;
                                                            }
                                                            else
                                                            {
                                                                _locMaxLoop = _locFreeItemRule.MaxLoop;
                                                            }
                                                        }

                                                        for (int i = 1; i <= _locMaxLoop; i++)
                                                        {
                                                            _locTotQtyFI = _locTotQtyFI * _locFreeItemRule.FpTQty;
                                                        }

                                                        if (_locFreeItemRule.FpItem != null && _locFreeItemRule.FpDUOM != null && _locFreeItemRule.FpUOM != null)
                                                        {
                                                            ItemUnitOfMeasure _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                                     new BinaryOperator("Item", _locFreeItemRule.FpItem),
                                                                                     new BinaryOperator("UOM", _locFreeItemRule.FpUOM),
                                                                                     new BinaryOperator("DefaultUOM", _locFreeItemRule.FpDUOM),
                                                                                     new BinaryOperator("Active", true)));
                                                            if (_locItemUOM != null)
                                                            {
                                                                if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                                                {
                                                                    _locFpDqty = _locTotQtyFI % _locItemUOM.DefaultConversion;
                                                                    _locFpQty = System.Math.Floor(_locTotQtyFI / _locItemUOM.DefaultConversion);
                                                                }
                                                                else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                                                {
                                                                    _locFpDqty = System.Math.Floor(_locTotQtyFI / _locItemUOM.DefaultConversion);
                                                                    _locFpQty = (_locTotQtyFI % _locItemUOM.DefaultConversion) * _locItemUOM.Conversion;
                                                                }
                                                                else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                                                {
                                                                    _locFpDqty = _locTotQtyFI;
                                                                }
                                                            }
                                                        }
                                                        else
                                                        {
                                                            _locFpDqty = _locTotQtyFI;
                                                        }

                                                        InvoiceCanvassingFreeItem _saveDataInvoiceCanvassingFreeItem = new InvoiceCanvassingFreeItem(_currSession)
                                                        {
                                                            Company = _locInvoiceCanvassingXPO.Company,
                                                            Workplace = _locInvoiceCanvassingXPO.Workplace,
                                                            Division = _locInvoiceCanvassingXPO.Division,
                                                            Department = _locInvoiceCanvassingXPO.Department,
                                                            Section = _locInvoiceCanvassingXPO.Section,
                                                            Employee = _locInvoiceCanvassingXPO.Employee,
                                                            InvoiceCanvassing = _locInvoiceCanvassingXPO,
                                                            InvoiceCanvassingLine = _locInvoiceCanvassingLine,
                                                            FpItem = _locFreeItemRule.FpItem,
                                                            FpDQty = _locFpDqty,
                                                            FpDUOM = _locFreeItemRule.FpDUOM,
                                                            FpQty = _locFpQty,
                                                            FpUOM = _locFreeItemRule.FpUOM,
                                                            FpTQty = _locTotQtyFI,
                                                        };
                                                        _saveDataInvoiceCanvassingFreeItem.Save();
                                                        _saveDataInvoiceCanvassingFreeItem.Session.CommitTransaction();

                                                        _locInvoiceCanvassingLine.FreeItemChecked = true;
                                                        _locInvoiceCanvassingLine.Save();
                                                        _locInvoiceCanvassingLine.Session.CommitTransaction();
                                                    }
                                                }
                                            }
                                        }
                                        #endregion Rule3

                                        //2^(1-1) + 2^(2-1) + 2^(3-1) + 2^(4-1)
                                        #region Rule4
                                        if (_locFreeItemRule.RuleType == RuleType.Rule4)
                                        {
                                            XPCollection<InvoiceCanvassingLine> _locInvoiceCanvassingLines = new XPCollection<InvoiceCanvassingLine>
                                                                                                   (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                   new BinaryOperator("InvoiceCanvassing", _locInvoiceCanvassingXPO),
                                                                                                   new BinaryOperator("Item", _invoiceCanvassingLine.Item),
                                                                                                   new BinaryOperator("Status", Status.Progress),
                                                                                                   new BinaryOperator("FreeItemChecked", false)));

                                            if (_locInvoiceCanvassingLines != null && _locInvoiceCanvassingLines.Count() > 0)
                                            {

                                                foreach (InvoiceCanvassingLine _locInvoiceCanvassingLine in _locInvoiceCanvassingLines)
                                                {
                                                    double _locQtyFI = 0;
                                                    double _locTotQtyFI = 0;
                                                    double _locFpDqty = 0;
                                                    double _locFpQty = 0;
                                                    int _locMaxLoop = 0;

                                                    if (_locInvoiceCanvassingLine.TQty >= _locFreeItemRule.MpTQty)
                                                    {
                                                        _locQtyFI = System.Math.Floor(_locInvoiceCanvassingLine.TQty / _locFreeItemRule.MpTQty);

                                                        if (_locFreeItemRule.MaxLoop == 0)
                                                        {
                                                            _locMaxLoop = (int)_locQtyFI;
                                                        }
                                                        else
                                                        {
                                                            if (_locFreeItemRule.MaxLoop > (int)_locQtyFI)
                                                            {
                                                                _locMaxLoop = (int)_locQtyFI;
                                                            }
                                                            else
                                                            {
                                                                _locMaxLoop = _locFreeItemRule.MaxLoop;
                                                            }
                                                        }

                                                        for (int i = 1; i <= _locMaxLoop; i++)
                                                        {
                                                            
                                                            _locTotQtyFI = _locTotQtyFI + System.Math.Pow(_locFreeItemRule.FpTQty, i-1);
                                                        }

                                                        if (_locFreeItemRule.FpItem != null && _locFreeItemRule.FpDUOM != null && _locFreeItemRule.FpUOM != null)
                                                        {
                                                            ItemUnitOfMeasure _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                                     new BinaryOperator("Item", _locFreeItemRule.FpItem),
                                                                                     new BinaryOperator("UOM", _locFreeItemRule.FpUOM),
                                                                                     new BinaryOperator("DefaultUOM", _locFreeItemRule.FpDUOM),
                                                                                     new BinaryOperator("Active", true)));
                                                            if (_locItemUOM != null)
                                                            {
                                                                if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                                                {
                                                                    _locFpDqty = _locTotQtyFI % _locItemUOM.DefaultConversion;
                                                                    _locFpQty = System.Math.Floor(_locTotQtyFI / _locItemUOM.DefaultConversion);
                                                                }
                                                                else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                                                {
                                                                    _locFpDqty = System.Math.Floor(_locTotQtyFI / _locItemUOM.DefaultConversion);
                                                                    _locFpQty = (_locTotQtyFI % _locItemUOM.DefaultConversion) * _locItemUOM.Conversion;
                                                                }
                                                                else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                                                {
                                                                    _locFpDqty = _locTotQtyFI;
                                                                }
                                                            }
                                                        }
                                                        else
                                                        {
                                                            _locFpDqty = _locTotQtyFI;
                                                        }

                                                        InvoiceCanvassingFreeItem _saveDataInvoiceCanvassingFreeItem = new InvoiceCanvassingFreeItem(_currSession)
                                                        {
                                                            Company = _locInvoiceCanvassingXPO.Company,
                                                            Workplace = _locInvoiceCanvassingXPO.Workplace,
                                                            Division = _locInvoiceCanvassingXPO.Division,
                                                            Department = _locInvoiceCanvassingXPO.Department,
                                                            Section = _locInvoiceCanvassingXPO.Section,
                                                            Employee = _locInvoiceCanvassingXPO.Employee,
                                                            InvoiceCanvassing = _locInvoiceCanvassingXPO,
                                                            InvoiceCanvassingLine = _locInvoiceCanvassingLine,
                                                            FpItem = _locFreeItemRule.FpItem,
                                                            FpDQty = _locFpDqty,
                                                            FpDUOM = _locFreeItemRule.FpDUOM,
                                                            FpQty = _locFpQty,
                                                            FpUOM = _locFreeItemRule.FpUOM,
                                                            FpTQty = _locTotQtyFI,
                                                        };
                                                        _saveDataInvoiceCanvassingFreeItem.Save();
                                                        _saveDataInvoiceCanvassingFreeItem.Session.CommitTransaction();

                                                        _locInvoiceCanvassingLine.FreeItemChecked = true;
                                                        _locInvoiceCanvassingLine.Save();
                                                        _locInvoiceCanvassingLine.Session.CommitTransaction();
                                                    }  
                                                }
                                            }
                                        }
                                        #endregion Rule4
                                    }
                                    #endregion MultiItem=false
                                    #region MultiItem=true
                                    else
                                    {
                                        InvoiceCanvassingLine _locComboInvoiceCanvassingLine = _currSession.FindObject<InvoiceCanvassingLine>
                                                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                                                    new BinaryOperator("InvoiceCanvassing", _locInvoiceCanvassingXPO),
                                                                                                    new BinaryOperator("Item", _locFreeItemRule.CpItem),
                                                                                                    new BinaryOperator("Status", Status.Progress),
                                                                                                    new BinaryOperator("FreeItemChecked", false)));

                                        InvoiceCanvassingLine _locMainInvoiceCanvassingLine = _currSession.FindObject<InvoiceCanvassingLine>
                                                                                                   (new GroupOperator(GroupOperatorType.And,
                                                                                                   new BinaryOperator("InvoiceCanvassing", _locInvoiceCanvassingXPO),
                                                                                                   new BinaryOperator("Item", _locFreeItemRule.MpItem),
                                                                                                   new BinaryOperator("Status", Status.Progress),
                                                                                                   new BinaryOperator("FreeItemChecked", false)));

                                        if (_locMainInvoiceCanvassingLine != null && _locComboInvoiceCanvassingLine != null)
                                        {
                                            double _locComboQtyCI = 0;
                                            double _locComboTotQtyFI = 0;
                                            double _locComboCpDqty = 0;
                                            double _locComboCpQty = 0;
                                            int _locComboMaxLoop = 0;
                                            int _locComboTotMaxLoopVal = 0;

                                            double _locMainQtyFI = 0;
                                            double _locTotQtyFI = 0;
                                            double _locFpDqty = 0;
                                            double _locFpQty = 0;
                                            int _locMaxLoop = 0;
                                            int _locTotMaxLoopVal = 0;

                                            if (_locMainInvoiceCanvassingLine.TQty >= _locFreeItemRule.MpTQty && _locComboInvoiceCanvassingLine.TQty >= _locFreeItemRule.CpTQty)
                                            {
                                                _locMainQtyFI = System.Math.Floor(_locMainInvoiceCanvassingLine.TQty / _locFreeItemRule.MpTQty);
                                                _locComboQtyCI = System.Math.Floor(_locComboInvoiceCanvassingLine.TQty / _locFreeItemRule.CpTQty);

                                                if (_locComboQtyCI < _locMainQtyFI)
                                                {
                                                    #region ComboPromoItem

                                                    //1+1+1+1+1
                                                    #region Rule1
                                                    if (_locFreeItemRule.RuleType == RuleType.Rule1)
                                                    {

                                                        if (_locFreeItemRule.MaxLoop == 0)
                                                        {
                                                            _locComboMaxLoop = (int)_locComboQtyCI;
                                                        }
                                                        else
                                                        {
                                                            if (_locFreeItemRule.MaxLoop > (int)_locComboQtyCI)
                                                            {
                                                                _locComboMaxLoop = _locFreeItemRule.MaxLoop;
                                                            }
                                                            else
                                                            {
                                                                _locComboMaxLoop = _locFreeItemRule.MaxLoop;
                                                            }
                                                            
                                                        }

                                                        for (int i = 1; i <= _locComboMaxLoop; i++)
                                                        {
                                                            _locComboTotMaxLoopVal = _locComboTotMaxLoopVal + 1;
                                                        }

                                                        _locComboTotQtyFI = _locComboTotMaxLoopVal * _locFreeItemRule.FpTQty;


                                                        if (_locFreeItemRule.FpItem != null && _locFreeItemRule.FpDUOM != null && _locFreeItemRule.FpUOM != null)
                                                        {
                                                            ItemUnitOfMeasure _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                                     new BinaryOperator("Item", _locFreeItemRule.FpItem),
                                                                                     new BinaryOperator("UOM", _locFreeItemRule.FpUOM),
                                                                                     new BinaryOperator("DefaultUOM", _locFreeItemRule.FpDUOM),
                                                                                     new BinaryOperator("Active", true)));
                                                            if (_locItemUOM != null)
                                                            {
                                                                if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                                                {
                                                                    _locComboCpDqty = _locComboTotQtyFI % _locItemUOM.DefaultConversion;
                                                                    _locComboCpQty = System.Math.Floor(_locComboTotQtyFI / _locItemUOM.DefaultConversion);
                                                                }
                                                                else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                                                {
                                                                    _locComboCpDqty = System.Math.Floor(_locComboTotQtyFI / _locItemUOM.DefaultConversion);
                                                                    _locComboCpQty = (_locComboTotQtyFI % _locItemUOM.DefaultConversion) * _locItemUOM.Conversion;
                                                                }
                                                                else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                                                {
                                                                    _locComboCpDqty = _locComboTotQtyFI;
                                                                }
                                                            }
                                                        }
                                                        else
                                                        {
                                                            _locComboCpDqty = _locComboTotQtyFI;
                                                        }

                                                        InvoiceCanvassingFreeItem _saveDataInvoiceCanvassingFreeItem = new InvoiceCanvassingFreeItem(_currSession)
                                                        {
                                                            Company = _locInvoiceCanvassingXPO.Company,
                                                            Workplace = _locInvoiceCanvassingXPO.Workplace,
                                                            Division = _locInvoiceCanvassingXPO.Division,
                                                            Department = _locInvoiceCanvassingXPO.Department,
                                                            Section = _locInvoiceCanvassingXPO.Section,
                                                            Employee = _locInvoiceCanvassingXPO.Employee,
                                                            InvoiceCanvassing = _locInvoiceCanvassingXPO,
                                                            FpItem = _locFreeItemRule.FpItem,
                                                            FpDQty = _locComboCpDqty,
                                                            FpDUOM = _locFreeItemRule.FpDUOM,
                                                            FpQty = _locComboCpQty,
                                                            FpUOM = _locFreeItemRule.FpUOM,
                                                            FpTQty = _locComboTotQtyFI,
                                                        };
                                                        _saveDataInvoiceCanvassingFreeItem.Save();
                                                        _saveDataInvoiceCanvassingFreeItem.Session.CommitTransaction();

                                                        _locComboInvoiceCanvassingLine.FreeItemChecked = true;
                                                        _locComboInvoiceCanvassingLine.Save();
                                                        _locComboInvoiceCanvassingLine.Session.CommitTransaction();
                                                        _locMainInvoiceCanvassingLine.FreeItemChecked = true;
                                                        _locMainInvoiceCanvassingLine.Save();
                                                        _locMainInvoiceCanvassingLine.Session.CommitTransaction();

                                                    }

                                                    #endregion Rule1

                                                    //1, 2+1, 3+1, 4+1
                                                    #region Rule2

                                                    if (_locFreeItemRule.RuleType == RuleType.Rule2)
                                                    {
                                                        if (_locFreeItemRule.MaxLoop == 0)
                                                        {
                                                            _locComboMaxLoop = (int)_locComboQtyCI;
                                                        }
                                                        else
                                                        {
                                                            if (_locFreeItemRule.MaxLoop > (int)_locComboQtyCI)
                                                            {
                                                                _locComboMaxLoop = _locFreeItemRule.MaxLoop;
                                                            }
                                                            else
                                                            {
                                                                _locComboMaxLoop = _locFreeItemRule.MaxLoop;
                                                            }
                                                        }

                                                        for (int i = 1; i <= _locComboMaxLoop; i++)
                                                        {
                                                            if (i == 1)
                                                            {
                                                                _locComboTotQtyFI = _locFreeItemRule.FpTQty;
                                                            }
                                                            else
                                                            {
                                                                _locComboTotQtyFI = _locComboTotQtyFI + (i + _locFreeItemRule.FpTQty);
                                                            }
                                                        }

                                                        if (_locFreeItemRule.FpItem != null && _locFreeItemRule.FpDUOM != null && _locFreeItemRule.FpUOM != null)
                                                        {
                                                            ItemUnitOfMeasure _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                                     new BinaryOperator("Item", _locFreeItemRule.FpItem),
                                                                                     new BinaryOperator("UOM", _locFreeItemRule.FpUOM),
                                                                                     new BinaryOperator("DefaultUOM", _locFreeItemRule.FpDUOM),
                                                                                     new BinaryOperator("Active", true)));
                                                            if (_locItemUOM != null)
                                                            {
                                                                if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                                                {
                                                                    _locComboCpDqty = _locComboTotQtyFI % _locItemUOM.DefaultConversion;
                                                                    _locComboCpQty = System.Math.Floor(_locComboTotQtyFI / _locItemUOM.DefaultConversion);
                                                                }
                                                                else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                                                {
                                                                    _locComboCpDqty = System.Math.Floor(_locComboTotQtyFI / _locItemUOM.DefaultConversion);
                                                                    _locComboCpQty = (_locComboTotQtyFI % _locItemUOM.DefaultConversion) * _locItemUOM.Conversion;
                                                                }
                                                                else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                                                {
                                                                    _locComboCpDqty = _locComboTotQtyFI;
                                                                }
                                                            }
                                                        }
                                                        else
                                                        {
                                                            _locComboCpDqty = _locComboTotQtyFI;
                                                        }

                                                        InvoiceCanvassingFreeItem _saveDataInvoiceCanvassingFreeItem = new InvoiceCanvassingFreeItem(_currSession)
                                                        {
                                                            Company = _locInvoiceCanvassingXPO.Company,
                                                            Workplace = _locInvoiceCanvassingXPO.Workplace,
                                                            Division = _locInvoiceCanvassingXPO.Division,
                                                            Department = _locInvoiceCanvassingXPO.Department,
                                                            Section = _locInvoiceCanvassingXPO.Section,
                                                            Employee = _locInvoiceCanvassingXPO.Employee,
                                                            InvoiceCanvassing = _locInvoiceCanvassingXPO,
                                                            FpItem = _locFreeItemRule.FpItem,
                                                            FpDQty = _locComboCpDqty,
                                                            FpDUOM = _locFreeItemRule.FpDUOM,
                                                            FpQty = _locComboCpQty,
                                                            FpUOM = _locFreeItemRule.FpUOM,
                                                            FpTQty = _locComboTotQtyFI,
                                                        };
                                                        _saveDataInvoiceCanvassingFreeItem.Save();
                                                        _saveDataInvoiceCanvassingFreeItem.Session.CommitTransaction();

                                                        _locComboInvoiceCanvassingLine.FreeItemChecked = true;
                                                        _locComboInvoiceCanvassingLine.Save();
                                                        _locComboInvoiceCanvassingLine.Session.CommitTransaction();
                                                        _locMainInvoiceCanvassingLine.FreeItemChecked = true;
                                                        _locMainInvoiceCanvassingLine.Save();
                                                        _locMainInvoiceCanvassingLine.Session.CommitTransaction();

                                                    }
                                                    #endregion Rule2

                                                    //2 * 2 * 2 * 2
                                                    #region Rule3

                                                    if (_locFreeItemRule.RuleType == RuleType.Rule3)
                                                    {

                                                        if (_locFreeItemRule.MaxLoop == 0)
                                                        {
                                                            _locComboMaxLoop = (int)_locComboQtyCI;
                                                        }
                                                        else
                                                        {
                                                            if (_locFreeItemRule.MaxLoop > (int)_locComboQtyCI)
                                                            {
                                                                _locComboMaxLoop = _locFreeItemRule.MaxLoop;
                                                            }
                                                            else
                                                            {
                                                                _locComboMaxLoop = _locFreeItemRule.MaxLoop;
                                                            }
                                                        }

                                                        for (int i = 1; i <= _locComboMaxLoop; i++)
                                                        {
                                                            _locComboTotQtyFI = _locComboTotQtyFI * _locFreeItemRule.FpTQty;
                                                        }

                                                        if (_locFreeItemRule.FpItem != null && _locFreeItemRule.FpDUOM != null && _locFreeItemRule.FpUOM != null)
                                                        {
                                                            ItemUnitOfMeasure _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                                     new BinaryOperator("Item", _locFreeItemRule.FpItem),
                                                                                     new BinaryOperator("UOM", _locFreeItemRule.FpUOM),
                                                                                     new BinaryOperator("DefaultUOM", _locFreeItemRule.FpDUOM),
                                                                                     new BinaryOperator("Active", true)));
                                                            if (_locItemUOM != null)
                                                            {
                                                                if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                                                {
                                                                    _locComboCpDqty = _locComboTotQtyFI % _locItemUOM.DefaultConversion;
                                                                    _locComboCpQty = System.Math.Floor(_locComboTotQtyFI / _locItemUOM.DefaultConversion);
                                                                }
                                                                else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                                                {
                                                                    _locComboCpDqty = System.Math.Floor(_locComboTotQtyFI / _locItemUOM.DefaultConversion);
                                                                    _locComboCpQty = (_locComboTotQtyFI % _locItemUOM.DefaultConversion) * _locItemUOM.Conversion;
                                                                }
                                                                else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                                                {
                                                                    _locComboCpDqty = _locComboTotQtyFI;
                                                                }
                                                            }
                                                        }
                                                        else
                                                        {
                                                            _locComboCpDqty = _locComboTotQtyFI;
                                                        }

                                                        InvoiceCanvassingFreeItem _saveDataInvoiceCanvassingFreeItem = new InvoiceCanvassingFreeItem(_currSession)
                                                        {
                                                            Company = _locInvoiceCanvassingXPO.Company,
                                                            Workplace = _locInvoiceCanvassingXPO.Workplace,
                                                            Division = _locInvoiceCanvassingXPO.Division,
                                                            Department = _locInvoiceCanvassingXPO.Department,
                                                            Section = _locInvoiceCanvassingXPO.Section,
                                                            Employee = _locInvoiceCanvassingXPO.Employee,
                                                            InvoiceCanvassing = _locInvoiceCanvassingXPO,
                                                            FpItem = _locFreeItemRule.FpItem,
                                                            FpDQty = _locComboCpDqty,
                                                            FpDUOM = _locFreeItemRule.FpDUOM,
                                                            FpQty = _locComboCpQty,
                                                            FpUOM = _locFreeItemRule.FpUOM,
                                                            FpTQty = _locComboTotQtyFI,
                                                        };
                                                        _saveDataInvoiceCanvassingFreeItem.Save();
                                                        _saveDataInvoiceCanvassingFreeItem.Session.CommitTransaction();

                                                        _locComboInvoiceCanvassingLine.FreeItemChecked = true;
                                                        _locComboInvoiceCanvassingLine.Save();
                                                        _locComboInvoiceCanvassingLine.Session.CommitTransaction();
                                                        _locMainInvoiceCanvassingLine.FreeItemChecked = true;
                                                        _locMainInvoiceCanvassingLine.Save();
                                                        _locMainInvoiceCanvassingLine.Session.CommitTransaction();

                                                    }
                                                    #endregion Rule3

                                                    //2^(1-1) + 2^(2-1) + 2^(3-1) + 2^(4-1)
                                                    #region Rule4

                                                    if (_locFreeItemRule.RuleType == RuleType.Rule4)
                                                    {
                                                        if (_locFreeItemRule.MaxLoop == 0)
                                                        {
                                                            _locComboMaxLoop = (int)_locComboQtyCI;
                                                        }
                                                        else
                                                        {
                                                            if (_locFreeItemRule.MaxLoop > (int)_locComboQtyCI)
                                                            {
                                                                _locComboMaxLoop = _locFreeItemRule.MaxLoop;
                                                            }
                                                            else
                                                            {
                                                                _locComboMaxLoop = _locFreeItemRule.MaxLoop;
                                                            }
                                                        }

                                                        for (int i = 1; i <= _locComboMaxLoop; i++)
                                                        {
                                                            _locComboTotQtyFI = _locComboTotQtyFI + System.Math.Pow(_locFreeItemRule.FpTQty, i - 1);
                                                        }

                                                        if (_locFreeItemRule.FpItem != null && _locFreeItemRule.FpDUOM != null && _locFreeItemRule.FpUOM != null)
                                                        {
                                                            ItemUnitOfMeasure _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                                     new BinaryOperator("Item", _locFreeItemRule.FpItem),
                                                                                     new BinaryOperator("UOM", _locFreeItemRule.FpUOM),
                                                                                     new BinaryOperator("DefaultUOM", _locFreeItemRule.FpDUOM),
                                                                                     new BinaryOperator("Active", true)));
                                                            if (_locItemUOM != null)
                                                            {
                                                                if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                                                {
                                                                    _locComboCpDqty = _locComboTotQtyFI % _locItemUOM.DefaultConversion;
                                                                    _locComboCpQty = System.Math.Floor(_locComboTotQtyFI / _locItemUOM.DefaultConversion);
                                                                }
                                                                else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                                                {
                                                                    _locComboCpDqty = System.Math.Floor(_locComboTotQtyFI / _locItemUOM.DefaultConversion);
                                                                    _locComboCpQty = (_locComboTotQtyFI % _locItemUOM.DefaultConversion) * _locItemUOM.Conversion;
                                                                }
                                                                else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                                                {
                                                                    _locComboCpDqty = _locComboTotQtyFI;
                                                                }
                                                            }
                                                        }
                                                        else
                                                        {
                                                            _locComboCpDqty = _locComboTotQtyFI;
                                                        }

                                                        InvoiceCanvassingFreeItem _saveDataInvoiceCanvassingFreeItem = new InvoiceCanvassingFreeItem(_currSession)
                                                        {
                                                            Company = _locInvoiceCanvassingXPO.Company,
                                                            Workplace = _locInvoiceCanvassingXPO.Workplace,
                                                            Division = _locInvoiceCanvassingXPO.Division,
                                                            Department = _locInvoiceCanvassingXPO.Department,
                                                            Section = _locInvoiceCanvassingXPO.Section,
                                                            Employee = _locInvoiceCanvassingXPO.Employee,
                                                            InvoiceCanvassing = _locInvoiceCanvassingXPO,
                                                            FpItem = _locFreeItemRule.FpItem,
                                                            FpDQty = _locComboCpDqty,
                                                            FpDUOM = _locFreeItemRule.FpDUOM,
                                                            FpQty = _locComboCpQty,
                                                            FpUOM = _locFreeItemRule.FpUOM,
                                                            FpTQty = _locComboTotQtyFI,
                                                        };
                                                        _saveDataInvoiceCanvassingFreeItem.Save();
                                                        _saveDataInvoiceCanvassingFreeItem.Session.CommitTransaction();

                                                        _locComboInvoiceCanvassingLine.FreeItemChecked = true;
                                                        _locComboInvoiceCanvassingLine.Save();
                                                        _locComboInvoiceCanvassingLine.Session.CommitTransaction();
                                                        _locMainInvoiceCanvassingLine.FreeItemChecked = true;
                                                        _locMainInvoiceCanvassingLine.Save();
                                                        _locMainInvoiceCanvassingLine.Session.CommitTransaction();

                                                    }
                                                    #endregion Rule4

                                                    #endregion ComboPromoItem
                                                }
                                                else
                                                {
                                                    #region MainPromoItem

                                                    #region Rule1

                                                    if (_locFreeItemRule.RuleType == RuleType.Rule1)
                                                    {

                                                        if (_locFreeItemRule.MaxLoop == 0)
                                                        {
                                                            _locMaxLoop = (int)_locMainQtyFI;
                                                        }
                                                        else
                                                        {
                                                            if(_locFreeItemRule.MaxLoop > (int)_locMainQtyFI)
                                                            {
                                                                _locMaxLoop = _locFreeItemRule.MaxLoop;
                                                            }
                                                            else
                                                            {
                                                                _locMaxLoop = _locFreeItemRule.MaxLoop;
                                                            }   
                                                        }

                                                        for (int i = 1; i <= _locMaxLoop; i++)
                                                        {
                                                            _locTotMaxLoopVal = _locTotMaxLoopVal + 1;
                                                        }

                                                        _locTotQtyFI = _locTotMaxLoopVal * _locFreeItemRule.FpTQty;


                                                        if (_locFreeItemRule.FpItem != null && _locFreeItemRule.FpDUOM != null && _locFreeItemRule.FpUOM != null)
                                                        {
                                                            ItemUnitOfMeasure _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                                     new BinaryOperator("Item", _locFreeItemRule.FpItem),
                                                                                     new BinaryOperator("UOM", _locFreeItemRule.FpUOM),
                                                                                     new BinaryOperator("DefaultUOM", _locFreeItemRule.FpDUOM),
                                                                                     new BinaryOperator("Active", true)));
                                                            if (_locItemUOM != null)
                                                            {
                                                                if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                                                {
                                                                    _locFpDqty = _locTotQtyFI % _locItemUOM.DefaultConversion;
                                                                    _locFpQty = System.Math.Floor(_locTotQtyFI / _locItemUOM.DefaultConversion);
                                                                }
                                                                else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                                                {
                                                                    _locFpDqty = System.Math.Floor(_locTotQtyFI / _locItemUOM.DefaultConversion);
                                                                    _locFpQty = (_locTotQtyFI % _locItemUOM.DefaultConversion) * _locItemUOM.Conversion;
                                                                }
                                                                else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                                                {
                                                                    _locFpDqty = _locTotQtyFI;
                                                                }
                                                            }
                                                        }
                                                        else
                                                        {
                                                            _locFpDqty = _locTotQtyFI;
                                                        }

                                                        InvoiceCanvassingFreeItem _saveDataInvoiceCanvassingFreeItem = new InvoiceCanvassingFreeItem(_currSession)
                                                        {
                                                            Company = _locInvoiceCanvassingXPO.Company,
                                                            Workplace = _locInvoiceCanvassingXPO.Workplace,
                                                            Division = _locInvoiceCanvassingXPO.Division,
                                                            Department = _locInvoiceCanvassingXPO.Department,
                                                            Section = _locInvoiceCanvassingXPO.Section,
                                                            Employee = _locInvoiceCanvassingXPO.Employee,
                                                            InvoiceCanvassing = _locInvoiceCanvassingXPO,
                                                            FpItem = _locFreeItemRule.FpItem,
                                                            FpDQty = _locFpDqty,
                                                            FpDUOM = _locFreeItemRule.FpDUOM,
                                                            FpQty = _locFpQty,
                                                            FpUOM = _locFreeItemRule.FpUOM,
                                                            FpTQty = _locTotQtyFI,
                                                        };
                                                        _saveDataInvoiceCanvassingFreeItem.Save();
                                                        _saveDataInvoiceCanvassingFreeItem.Session.CommitTransaction();

                                                        _locComboInvoiceCanvassingLine.FreeItemChecked = true;
                                                        _locComboInvoiceCanvassingLine.Save();
                                                        _locComboInvoiceCanvassingLine.Session.CommitTransaction();
                                                        _locMainInvoiceCanvassingLine.FreeItemChecked = true;
                                                        _locMainInvoiceCanvassingLine.Save();
                                                        _locMainInvoiceCanvassingLine.Session.CommitTransaction();

                                                    }

                                                    #endregion Rule1

                                                    //1, 2+1, 3+1, 4+1
                                                    #region Rule2
                                                    if (_locFreeItemRule.RuleType == RuleType.Rule2)
                                                    {

                                                        if (_locFreeItemRule.MaxLoop == 0)
                                                        {
                                                            _locMaxLoop = (int)_locMainQtyFI;
                                                        }
                                                        else
                                                        {
                                                            if (_locFreeItemRule.MaxLoop > (int)_locMainQtyFI)
                                                            {
                                                                _locMaxLoop = _locFreeItemRule.MaxLoop;
                                                            }
                                                            else
                                                            {
                                                                _locMaxLoop = _locFreeItemRule.MaxLoop;
                                                            }
                                                        }

                                                        for (int i = 1; i <= _locMaxLoop; i++)
                                                        {

                                                            if (i == 1)
                                                            {
                                                                _locTotQtyFI = _locFreeItemRule.FpTQty;
                                                            }
                                                            else
                                                            {
                                                                _locTotQtyFI = _locTotQtyFI + (i + _locFreeItemRule.FpTQty);
                                                            }

                                                        }

                                                        if (_locFreeItemRule.FpItem != null && _locFreeItemRule.FpDUOM != null && _locFreeItemRule.FpUOM != null)
                                                        {
                                                            ItemUnitOfMeasure _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                                     new BinaryOperator("Item", _locFreeItemRule.FpItem),
                                                                                     new BinaryOperator("UOM", _locFreeItemRule.FpUOM),
                                                                                     new BinaryOperator("DefaultUOM", _locFreeItemRule.FpDUOM),
                                                                                     new BinaryOperator("Active", true)));
                                                            if (_locItemUOM != null)
                                                            {
                                                                if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                                                {
                                                                    _locFpDqty = _locTotQtyFI % _locItemUOM.DefaultConversion;
                                                                    _locFpQty = System.Math.Floor(_locTotQtyFI / _locItemUOM.DefaultConversion);
                                                                }
                                                                else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                                                {
                                                                    _locFpDqty = System.Math.Floor(_locTotQtyFI / _locItemUOM.DefaultConversion);
                                                                    _locFpQty = (_locTotQtyFI % _locItemUOM.DefaultConversion) * _locItemUOM.Conversion;
                                                                }
                                                                else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                                                {
                                                                    _locFpDqty = _locTotQtyFI;
                                                                }
                                                            }
                                                        }
                                                        else
                                                        {
                                                            _locFpDqty = _locTotQtyFI;
                                                        }

                                                        InvoiceCanvassingFreeItem _saveDataInvoiceCanvassingFreeItem = new InvoiceCanvassingFreeItem(_currSession)
                                                        {
                                                            Company = _locInvoiceCanvassingXPO.Company,
                                                            Workplace = _locInvoiceCanvassingXPO.Workplace,
                                                            Division = _locInvoiceCanvassingXPO.Division,
                                                            Department = _locInvoiceCanvassingXPO.Department,
                                                            Section = _locInvoiceCanvassingXPO.Section,
                                                            Employee = _locInvoiceCanvassingXPO.Employee,
                                                            InvoiceCanvassing = _locInvoiceCanvassingXPO,
                                                            FpItem = _locFreeItemRule.FpItem,
                                                            FpDQty = _locFpDqty,
                                                            FpDUOM = _locFreeItemRule.FpDUOM,
                                                            FpQty = _locFpQty,
                                                            FpUOM = _locFreeItemRule.FpUOM,
                                                            FpTQty = _locTotQtyFI,
                                                        };
                                                        _saveDataInvoiceCanvassingFreeItem.Save();
                                                        _saveDataInvoiceCanvassingFreeItem.Session.CommitTransaction();

                                                        _locComboInvoiceCanvassingLine.FreeItemChecked = true;
                                                        _locComboInvoiceCanvassingLine.Save();
                                                        _locComboInvoiceCanvassingLine.Session.CommitTransaction();
                                                        _locMainInvoiceCanvassingLine.FreeItemChecked = true;
                                                        _locMainInvoiceCanvassingLine.Save();
                                                        _locMainInvoiceCanvassingLine.Session.CommitTransaction();

                                                    }
                                                    #endregion Rule2

                                                    //2 * 2 * 2 * 2
                                                    #region Rule3
                                                    if (_locFreeItemRule.RuleType == RuleType.Rule1)
                                                    {

                                                        if (_locFreeItemRule.MaxLoop == 0)
                                                        {
                                                            _locMaxLoop = (int)_locMainQtyFI;
                                                        }
                                                        else
                                                        {
                                                            if (_locFreeItemRule.MaxLoop > (int)_locMainQtyFI)
                                                            {
                                                                _locMaxLoop = _locFreeItemRule.MaxLoop;
                                                            }
                                                            else
                                                            {
                                                                _locMaxLoop = _locFreeItemRule.MaxLoop;
                                                            }
                                                        }

                                                        for (int i = 1; i <= _locMaxLoop; i++)
                                                        {
                                                            _locTotQtyFI = _locTotQtyFI * _locFreeItemRule.FpTQty;
                                                        }

                                                        if (_locFreeItemRule.FpItem != null && _locFreeItemRule.FpDUOM != null && _locFreeItemRule.FpUOM != null)
                                                        {
                                                            ItemUnitOfMeasure _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                                     new BinaryOperator("Item", _locFreeItemRule.FpItem),
                                                                                     new BinaryOperator("UOM", _locFreeItemRule.FpUOM),
                                                                                     new BinaryOperator("DefaultUOM", _locFreeItemRule.FpDUOM),
                                                                                     new BinaryOperator("Active", true)));
                                                            if (_locItemUOM != null)
                                                            {
                                                                if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                                                {
                                                                    _locFpDqty = _locTotQtyFI % _locItemUOM.DefaultConversion;
                                                                    _locFpQty = System.Math.Floor(_locTotQtyFI / _locItemUOM.DefaultConversion);
                                                                }
                                                                else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                                                {
                                                                    _locFpDqty = System.Math.Floor(_locTotQtyFI / _locItemUOM.DefaultConversion);
                                                                    _locFpQty = (_locTotQtyFI % _locItemUOM.DefaultConversion) * _locItemUOM.Conversion;
                                                                }
                                                                else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                                                {
                                                                    _locFpDqty = _locTotQtyFI;
                                                                }
                                                            }
                                                        }
                                                        else
                                                        {
                                                            _locFpDqty = _locTotQtyFI;
                                                        }

                                                        InvoiceCanvassingFreeItem _saveDataInvoiceCanvassingFreeItem = new InvoiceCanvassingFreeItem(_currSession)
                                                        {
                                                            Company = _locInvoiceCanvassingXPO.Company,
                                                            Workplace = _locInvoiceCanvassingXPO.Workplace,
                                                            Division = _locInvoiceCanvassingXPO.Division,
                                                            Department = _locInvoiceCanvassingXPO.Department,
                                                            Section = _locInvoiceCanvassingXPO.Section,
                                                            Employee = _locInvoiceCanvassingXPO.Employee,
                                                            InvoiceCanvassing = _locInvoiceCanvassingXPO,
                                                            FpItem = _locFreeItemRule.FpItem,
                                                            FpDQty = _locFpDqty,
                                                            FpDUOM = _locFreeItemRule.FpDUOM,
                                                            FpQty = _locFpQty,
                                                            FpUOM = _locFreeItemRule.FpUOM,
                                                            FpTQty = _locTotQtyFI,
                                                        };
                                                        _saveDataInvoiceCanvassingFreeItem.Save();
                                                        _saveDataInvoiceCanvassingFreeItem.Session.CommitTransaction();

                                                        _locComboInvoiceCanvassingLine.FreeItemChecked = true;
                                                        _locComboInvoiceCanvassingLine.Save();
                                                        _locComboInvoiceCanvassingLine.Session.CommitTransaction();
                                                        _locMainInvoiceCanvassingLine.FreeItemChecked = true;
                                                        _locMainInvoiceCanvassingLine.Save();
                                                        _locMainInvoiceCanvassingLine.Session.CommitTransaction();
                                                    }
                                                    #endregion Rule3

                                                    //2^(1-1) + 2^(2-1) + 2^(3-1) + 2^(4-1)
                                                    #region Rule4

                                                    if (_locFreeItemRule.RuleType == RuleType.Rule1)
                                                    {
                                                        if (_locFreeItemRule.MaxLoop == 0)
                                                        {
                                                            _locMaxLoop = (int)_locMainQtyFI;
                                                        }
                                                        else
                                                        {
                                                            if (_locFreeItemRule.MaxLoop > (int)_locMainQtyFI)
                                                            {
                                                                _locMaxLoop = _locFreeItemRule.MaxLoop;
                                                            }
                                                            else
                                                            {
                                                                _locMaxLoop = _locFreeItemRule.MaxLoop;
                                                            }
                                                        }

                                                        for (int i = 1; i <= _locMaxLoop; i++)
                                                        {
                                                            _locTotQtyFI = _locTotQtyFI + System.Math.Pow(_locFreeItemRule.FpTQty, i - 1);
                                                        }

                                                        if (_locFreeItemRule.FpItem != null && _locFreeItemRule.FpDUOM != null && _locFreeItemRule.FpUOM != null)
                                                        {
                                                            ItemUnitOfMeasure _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                                     new BinaryOperator("Item", _locFreeItemRule.FpItem),
                                                                                     new BinaryOperator("UOM", _locFreeItemRule.FpUOM),
                                                                                     new BinaryOperator("DefaultUOM", _locFreeItemRule.FpDUOM),
                                                                                     new BinaryOperator("Active", true)));
                                                            if (_locItemUOM != null)
                                                            {
                                                                if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                                                {
                                                                    _locFpDqty = _locTotQtyFI % _locItemUOM.DefaultConversion;
                                                                    _locFpQty = System.Math.Floor(_locTotQtyFI / _locItemUOM.DefaultConversion);
                                                                }
                                                                else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                                                {
                                                                    _locFpDqty = System.Math.Floor(_locTotQtyFI / _locItemUOM.DefaultConversion);
                                                                    _locFpQty = (_locTotQtyFI % _locItemUOM.DefaultConversion) * _locItemUOM.Conversion;
                                                                }
                                                                else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                                                {
                                                                    _locFpDqty = _locTotQtyFI;
                                                                }
                                                            }
                                                        }
                                                        else
                                                        {
                                                            _locFpDqty = _locTotQtyFI;
                                                        }

                                                        InvoiceCanvassingFreeItem _saveDataInvoiceCanvassingFreeItem = new InvoiceCanvassingFreeItem(_currSession)
                                                        {
                                                            Company = _locInvoiceCanvassingXPO.Company,
                                                            Workplace = _locInvoiceCanvassingXPO.Workplace,
                                                            Division = _locInvoiceCanvassingXPO.Division,
                                                            Department = _locInvoiceCanvassingXPO.Department,
                                                            Section = _locInvoiceCanvassingXPO.Section,
                                                            Employee = _locInvoiceCanvassingXPO.Employee,
                                                            InvoiceCanvassing = _locInvoiceCanvassingXPO,
                                                            FpItem = _locFreeItemRule.FpItem,
                                                            FpDQty = _locFpDqty,
                                                            FpDUOM = _locFreeItemRule.FpDUOM,
                                                            FpQty = _locFpQty,
                                                            FpUOM = _locFreeItemRule.FpUOM,
                                                            FpTQty = _locTotQtyFI,
                                                        };
                                                        _saveDataInvoiceCanvassingFreeItem.Save();
                                                        _saveDataInvoiceCanvassingFreeItem.Session.CommitTransaction();

                                                        _locComboInvoiceCanvassingLine.FreeItemChecked = true;
                                                        _locComboInvoiceCanvassingLine.Save();
                                                        _locComboInvoiceCanvassingLine.Session.CommitTransaction();
                                                        _locMainInvoiceCanvassingLine.FreeItemChecked = true;
                                                        _locMainInvoiceCanvassingLine.Save();
                                                        _locMainInvoiceCanvassingLine.Session.CommitTransaction();
                                                    }
                                                    #endregion Rule4

                                                    #endregion MainPromoItem
                                                }
                                            }
                                            
                                        }
                                    }
                                    #endregion MultiItem=true
                                }
                            }
                        }
                    }
                } 
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = InvoiceCanvassing ", ex.ToString());
            }
        }

        #endregion FreeItem

        #region Discount
 
        private double GetDiscountRule1(Session _currSession, InvoiceCanvassing _locInvoiceCanvassingXPO)
        {
            double _result = 0;
            try
            {
                if (_locInvoiceCanvassingXPO != null)
                {
                    DiscountRule _locDiscountRule1 = _currSession.FindObject<DiscountRule>
                                                            (new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("Company", _locInvoiceCanvassingXPO.Company),
                                                            new BinaryOperator("Workplace", _locInvoiceCanvassingXPO.Workplace),
                                                            new BinaryOperator("PriceGroup", _locInvoiceCanvassingXPO.PriceGroup),
                                                            new BinaryOperator("RuleType", RuleType.Rule1),
                                                            new BinaryOperator("Active", true)));

                    #region Rule1

                    //Grand Total Amount Per Invoice
                    if (_locDiscountRule1 != null)
                    {
                        double _locTUAmount = 0;
                        double _locTAmount = 0;
                        double _locTUAmountDiscount = 0;

                        #region Gross
                        if (_locDiscountRule1.GrossAmountRule == true)
                        {
                            XPCollection<InvoiceCanvassingLine> _locMainInvoiceCanvassingLines = new XPCollection<InvoiceCanvassingLine>
                                                                                           (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                           new BinaryOperator("InvoiceCanvassing", _locInvoiceCanvassingXPO),
                                                                                           new GroupOperator(GroupOperatorType.Or,
                                                                                           new BinaryOperator("Status", Status.Open),
                                                                                           new BinaryOperator("Status", Status.Progress))));

                            
                            if (_locMainInvoiceCanvassingLines != null && _locMainInvoiceCanvassingLines.Count() > 0)
                            {
                                foreach (InvoiceCanvassingLine _locMainInvoiceCanvassingLine in _locMainInvoiceCanvassingLines)
                                {
                                    if (_locMainInvoiceCanvassingLine.TUAmount > 0)
                                    {
                                        _locTUAmount = _locTUAmount + _locMainInvoiceCanvassingLine.TUAmount;
                                        _locMainInvoiceCanvassingLine.DiscountChecked = true;
                                        _locMainInvoiceCanvassingLine.Save();
                                        _locMainInvoiceCanvassingLine.Session.CommitTransaction();
                                    }
                                }

                                if (_locTUAmount > 0 && _locTUAmount >= _locDiscountRule1.GrossTotalUAmount)
                                {
                                    if (_locDiscountRule1.DiscountType == DiscountType.Percentage)
                                    {
                                        _locTUAmountDiscount = _locTUAmount * _locDiscountRule1.Value / 100;
                                    }
                                    if (_locDiscountRule1.DiscountType == DiscountType.Value)
                                    {
                                        _locTUAmountDiscount = _locDiscountRule1.Value;
                                    }
                                }
                            }

                        }
                        #endregion Gross
                        #region Net
                        if (_locDiscountRule1.NetAmountRule == true)
                        {
                            XPCollection<InvoiceCanvassingLine> _locMainInvoiceCanvassingLines = new XPCollection<InvoiceCanvassingLine>
                                                                                           (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                           new BinaryOperator("InvoiceCanvassing", _locInvoiceCanvassingXPO),
                                                                                           new GroupOperator(GroupOperatorType.Or,
                                                                                           new BinaryOperator("Status", Status.Open),
                                                                                           new BinaryOperator("Status", Status.Progress))));

                            if (_locMainInvoiceCanvassingLines != null && _locMainInvoiceCanvassingLines.Count() > 0)
                            {
                                foreach (InvoiceCanvassingLine _locMainInvoiceCanvassingLine in _locMainInvoiceCanvassingLines)
                                {
                                    if (_locMainInvoiceCanvassingLine.TAmount > 0)
                                    {
                                        _locTAmount = _locTAmount + _locMainInvoiceCanvassingLine.TAmount;
                                        _locMainInvoiceCanvassingLine.DiscountChecked = true;
                                        _locMainInvoiceCanvassingLine.Save();
                                        _locMainInvoiceCanvassingLine.Session.CommitTransaction();
                                    }
                                }

                                if (_locTAmount > 0 && _locTAmount >= _locDiscountRule1.NetTotalUAmount)
                                {
                                    if (_locDiscountRule1.DiscountType == DiscountType.Percentage)
                                    {
                                        _locTUAmountDiscount = _locTAmount * _locDiscountRule1.Value / 100;
                                    }
                                    if (_locDiscountRule1.DiscountType == DiscountType.Value)
                                    {
                                        _locTUAmountDiscount = _locDiscountRule1.Value;
                                    }
                                }
                            }
                        }
                        #endregion Net

                        _result = _locTUAmountDiscount;
                    }
                    #endregion Rule1
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = InvoiceCanvassing ", ex.ToString());
            }
            return _result;
        }

        #endregion Discount

        #region Posting Method

        private double GetInvoiceCanvassingLineTotalAmount(Session _currSession, InvoiceCanvassing _locInvoiceCanvassingXPO)
        {
            double _return = 0;
            try
            {
                if (_locInvoiceCanvassingXPO != null)
                {
                    double _locTAmount = 0;
                    XPCollection<InvoiceCanvassingLine> _locInvoiceCanvassingLines = new XPCollection<InvoiceCanvassingLine>
                                                                                               (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                new BinaryOperator("InvoiceCanvassing", _locInvoiceCanvassingXPO),
                                                                                                new GroupOperator(GroupOperatorType.Or,
                                                                                                new BinaryOperator("Status", Status.Progress),
                                                                                                new BinaryOperator("Status", Status.Posted))));

                    if (_locInvoiceCanvassingLines != null && _locInvoiceCanvassingLines.Count > 0)
                    {
                        foreach (InvoiceCanvassingLine _locInvoiceCanvassingLine in _locInvoiceCanvassingLines)
                        {
                            _locTAmount = _locTAmount + _locInvoiceCanvassingLine.TAmount;
                        }
                        _return = _locTAmount;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("BusinessObject = InvoiceCanvassing" + ex.ToString());
            }
            return _return;
        }

        private void SetInvoiceCanvassingMonitoring(Session _currSession, InvoiceCanvassing _locInvoiceCanvassingXPO)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                DateTime now = DateTime.Now;

                if (_locInvoiceCanvassingXPO != null)
                {
                    if (_locInvoiceCanvassingXPO.Status == Status.Progress || _locInvoiceCanvassingXPO.Status == Status.Posted)
                    {
                        XPCollection<InvoiceCanvassingLine> _locInvoiceCanvassingLines = new XPCollection<InvoiceCanvassingLine>
                                                            (_currSession, new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("InvoiceCanvassing", _locInvoiceCanvassingXPO),
                                                            new GroupOperator(GroupOperatorType.Or,
                                                            new BinaryOperator("Status", Status.Progress),
                                                            new BinaryOperator("Status", Status.Posted))));

                        if (_locInvoiceCanvassingLines != null && _locInvoiceCanvassingLines.Count() > 0)
                        {
                            foreach (InvoiceCanvassingLine _locInvoiceCanvassingLine in _locInvoiceCanvassingLines)
                            {
                                if (_locInvoiceCanvassingLine.TAmount > 0)
                                {
                                    InvoiceCanvassingMonitoring _saveDataInvoiceCanvassingMonitoring = new InvoiceCanvassingMonitoring(_currSession)
                                    {
                                        FreeItemChecked = _locInvoiceCanvassingLine.FreeItemChecked,
                                        DiscountChecked = _locInvoiceCanvassingLine.DiscountChecked,
                                        Company = _locInvoiceCanvassingLine.Company,
                                        Workplace = _locInvoiceCanvassingLine.Workplace,
                                        Division = _locInvoiceCanvassingLine.Division,
                                        Department = _locInvoiceCanvassingLine.Department,
                                        Section = _locInvoiceCanvassingLine.Section,
                                        Employee = _locInvoiceCanvassingLine.Employee,
                                        Salesman = _locInvoiceCanvassingLine.Salesman,
                                        Vehicle = _locInvoiceCanvassingLine.Vehicle,
                                        BeginningInventory = _locInvoiceCanvassingLine.BeginningInventory,
                                        Item = _locInvoiceCanvassingLine.Item,
                                        DQty = _locInvoiceCanvassingLine.DQty,
                                        DUOM = _locInvoiceCanvassingLine.DUOM,
                                        Qty = _locInvoiceCanvassingLine.Qty,
                                        UOM = _locInvoiceCanvassingLine.UOM,
                                        TQty = _locInvoiceCanvassingLine.TQty,
                                        StockType = _locInvoiceCanvassingLine.StockType,
                                        Currency = _locInvoiceCanvassingLine.Currency,
                                        PriceGroup = _locInvoiceCanvassingLine.PriceGroup,
                                        PriceLine = _locInvoiceCanvassingLine.PriceLine,
                                        UAmount = _locInvoiceCanvassingLine.UAmount,
                                        TUAmount = _locInvoiceCanvassingLine.TUAmount,
                                        Tax = _locInvoiceCanvassingLine.Tax,
                                        TxValue = _locInvoiceCanvassingLine.TxValue,
                                        TxAmount = _locInvoiceCanvassingLine.TxAmount,
                                        DiscountRule = _locInvoiceCanvassingLine.DiscountRule,
                                        DiscAmount = _locInvoiceCanvassingLine.DiscAmount,
                                        TAmount = _locInvoiceCanvassingLine.TAmount,
                                        InvoiceCanvassing = _locInvoiceCanvassingXPO,
                                        InvoiceCanvassingLine = _locInvoiceCanvassingLine,
                                    };
                                    _saveDataInvoiceCanvassingMonitoring.Save();
                                    _saveDataInvoiceCanvassingMonitoring.Session.CommitTransaction();
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = InvoiceCanvassing ", ex.ToString());
            }
        }

        private void SetCanvasPaymentIn(Session _currSession, InvoiceCanvassing _locInvoiceCanvassingXPO)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                DateTime now = DateTime.Now;

                if (_locInvoiceCanvassingXPO != null)
                {
                    CanvasPaymentIn _locCanvasPaymentIn = _currSession.FindObject<CanvasPaymentIn>
                                                    (new GroupOperator(GroupOperatorType.And,
                                                    new BinaryOperator("Company", _locInvoiceCanvassingXPO.Company),
                                                    new BinaryOperator("Workplace", _locInvoiceCanvassingXPO.Workplace),
                                                    new BinaryOperator("Salesman", _locInvoiceCanvassingXPO.Salesman),
                                                    new BinaryOperator("InvoiceCanvassing", _locInvoiceCanvassingXPO)));
                    if (_locCanvasPaymentIn == null)
                    {
                        CanvasPaymentIn _saveCanvasPaymentIn = new CanvasPaymentIn(_currSession)
                        {
                            Company = _locInvoiceCanvassingXPO.Company,
                            Workplace = _locInvoiceCanvassingXPO.Workplace,
                            PaymentType = _locInvoiceCanvassingXPO.PaymentType,
                            Salesman = _locInvoiceCanvassingXPO.Salesman,
                            Vehicle = _locInvoiceCanvassingXPO.Vehicle,
                            Plan = _locInvoiceCanvassingXPO.Amount,
                            InvoiceCanvassing = _locInvoiceCanvassingXPO,
                        };
                        _saveCanvasPaymentIn.Save();
                        _saveCanvasPaymentIn.Session.CommitTransaction();
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = InvoiceCanvassing ", ex.ToString());
            }
        }

        private void SetDeliverBeginingInventory(Session _currSession, InvoiceCanvassing _locInvoiceCanvassingXPO)
        {
            try
            {
                if(_locInvoiceCanvassingXPO != null)
                {
                    if(_locInvoiceCanvassingXPO.Company != null && _locInvoiceCanvassingXPO.Workplace != null)
                    {
                        #region NormalItem
                        XPCollection<InvoiceCanvassingLine> _locInvoiceCanvassingLines = new XPCollection<InvoiceCanvassingLine>(_currSession,
                                                                        new GroupOperator(GroupOperatorType.And,
                                                                        new BinaryOperator("InvoiceCanvassing", _locInvoiceCanvassingXPO),
                                                                        new GroupOperator(GroupOperatorType.Or,
                                                                        new BinaryOperator("Status", Status.Progress),
                                                                        new BinaryOperator("Status", Status.Posted))));

                        if (_locInvoiceCanvassingLines != null && _locInvoiceCanvassingLines.Count() > 0)
                        {
                            foreach (InvoiceCanvassingLine _locInvoiceCanvassingLine in _locInvoiceCanvassingLines)
                            {
                                if (_locInvoiceCanvassingLine.BeginningInventory != null && _locInvoiceCanvassingLine.Item != null)
                                {
                                    if(_locInvoiceCanvassingLine.SelectItem != null)
                                    {
                                        _locInvoiceCanvassingLine.SelectItem.QtyAvailable = _locInvoiceCanvassingLine.SelectItem.QtyAvailable - _locInvoiceCanvassingLine.TQty;
                                        _locInvoiceCanvassingLine.SelectItem.QtySale = _locInvoiceCanvassingLine.SelectItem.QtySale + _locInvoiceCanvassingLine.TQty;
                                        _locInvoiceCanvassingLine.SelectItem.Save();
                                        _locInvoiceCanvassingLine.SelectItem.Session.CommitTransaction();

                                        _locInvoiceCanvassingLine.BeginningInventory.QtyAvailable = _locInvoiceCanvassingLine.BeginningInventory.QtyAvailable - _locInvoiceCanvassingLine.TQty;
                                        _locInvoiceCanvassingLine.BeginningInventory.QtySale = _locInvoiceCanvassingLine.BeginningInventory.QtySale + _locInvoiceCanvassingLine.TQty;
                                        _locInvoiceCanvassingLine.BeginningInventory.Save();
                                        _locInvoiceCanvassingLine.BeginningInventory.Session.CommitTransaction();
                                    }
                                    else
                                    {
                                        BeginningInventoryLine _locBegInventoryLine = _currSession.FindObject<BeginningInventoryLine>
                                                                                (new GroupOperator(GroupOperatorType.And,
                                                                                new BinaryOperator("Company", _locInvoiceCanvassingXPO.Company),
                                                                                new BinaryOperator("Workplace", _locInvoiceCanvassingXPO.Workplace),
                                                                                new BinaryOperator("BeginningInventory", _locInvoiceCanvassingLine.BeginningInventory),
                                                                                new BinaryOperator("Item", _locInvoiceCanvassingLine.Item),
                                                                                new BinaryOperator("Active", true)));
                                        if (_locBegInventoryLine != null)
                                        {
                                            if (_locBegInventoryLine.QtyAvailable >= _locInvoiceCanvassingLine.TQty)
                                            {
                                                _locBegInventoryLine.QtyAvailable = _locBegInventoryLine.QtyAvailable - _locInvoiceCanvassingLine.TQty;
                                                _locBegInventoryLine.QtySale = _locBegInventoryLine.QtySale + _locInvoiceCanvassingLine.TQty;
                                                _locBegInventoryLine.Save();
                                                _locBegInventoryLine.Session.CommitTransaction();

                                                _locInvoiceCanvassingLine.BeginningInventory.QtyAvailable = _locInvoiceCanvassingLine.BeginningInventory.QtyAvailable - _locInvoiceCanvassingLine.TQty;
                                                _locInvoiceCanvassingLine.BeginningInventory.QtySale = _locInvoiceCanvassingLine.BeginningInventory.QtySale + _locInvoiceCanvassingLine.TQty;
                                                _locInvoiceCanvassingLine.BeginningInventory.Save();
                                                _locInvoiceCanvassingLine.BeginningInventory.Session.CommitTransaction();
                                            }

                                        }
                                    }
                                }
                            }
                        }
                        #endregion NormalItem
                        #region FreeItem
                        XPCollection<InvoiceCanvassingFreeItem> _locInvoiceCanvassingFreeItems = new XPCollection<InvoiceCanvassingFreeItem>(_currSession,
                                                                        new GroupOperator(GroupOperatorType.And,
                                                                        new BinaryOperator("InvoiceCanvassing", _locInvoiceCanvassingXPO),
                                                                        new GroupOperator(GroupOperatorType.Or,
                                                                        new BinaryOperator("Status", Status.Open),
                                                                        new BinaryOperator("Status", Status.Progress),
                                                                        new BinaryOperator("Status", Status.Posted))));

                        if (_locInvoiceCanvassingFreeItems != null && _locInvoiceCanvassingFreeItems.Count() > 0)
                        {
                            foreach (InvoiceCanvassingFreeItem _locInvoiceCanvassingFreeItem in _locInvoiceCanvassingFreeItems)
                            {
                                if (_locInvoiceCanvassingXPO.BeginningInventory != null && _locInvoiceCanvassingFreeItem.FpItem != null)
                                {
                                    BeginningInventoryLine _locBegInventoryLine = _currSession.FindObject<BeginningInventoryLine>
                                                                                (new GroupOperator(GroupOperatorType.And,
                                                                                new BinaryOperator("Company", _locInvoiceCanvassingXPO.Company),
                                                                                new BinaryOperator("Workplace", _locInvoiceCanvassingXPO.Workplace),
                                                                                new BinaryOperator("BeginningInventory", _locInvoiceCanvassingXPO.BeginningInventory),
                                                                                new BinaryOperator("Item", _locInvoiceCanvassingFreeItem.FpItem)));
                                    if (_locBegInventoryLine != null)
                                    {
                                        if (_locBegInventoryLine.QtyAvailable >= _locInvoiceCanvassingFreeItem.FpTQty)
                                        {
                                            _locBegInventoryLine.QtyAvailable = _locBegInventoryLine.QtyAvailable - _locInvoiceCanvassingFreeItem.FpTQty;
                                            _locBegInventoryLine.QtySale = _locBegInventoryLine.QtySale + _locInvoiceCanvassingFreeItem.FpTQty;
                                            _locBegInventoryLine.Save();
                                            _locBegInventoryLine.Session.CommitTransaction();

                                            _locInvoiceCanvassingXPO.BeginningInventory.QtyAvailable = _locInvoiceCanvassingXPO.BeginningInventory.QtyAvailable - _locInvoiceCanvassingFreeItem.FpTQty;
                                            _locInvoiceCanvassingXPO.BeginningInventory.QtySale = _locInvoiceCanvassingXPO.BeginningInventory.QtySale + _locInvoiceCanvassingFreeItem.FpTQty;
                                            _locInvoiceCanvassingXPO.BeginningInventory.Save();
                                            _locInvoiceCanvassingXPO.BeginningInventory.Session.CommitTransaction();
                                        }

                                    }
                                }
                            }
                        }
                        #endregion FreeItem
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = InvoiceCanvassing ", ex.ToString());
            }
        }

        private void SetDeliverInventoryJournal(Session _currSession, InvoiceCanvassing _locInvoiceCanvassingXPO)
        {
            try
            {
                DateTime now = DateTime.Now;

                if (_locInvoiceCanvassingXPO != null)
                {
                    if(_locInvoiceCanvassingXPO.Company != null && _locInvoiceCanvassingXPO.Workplace != null && _locInvoiceCanvassingXPO.Salesman != null && _locInvoiceCanvassingXPO.BeginningInventory != null)
                    {
                        #region NormalItem
                        XPCollection<InvoiceCanvassingLine> _locInvoiceCanvassingLines = new XPCollection<InvoiceCanvassingLine>(_currSession,
                                                                        new GroupOperator(GroupOperatorType.And,
                                                                        new BinaryOperator("InvoiceCanvassing", _locInvoiceCanvassingXPO),
                                                                        new GroupOperator(GroupOperatorType.Or,
                                                                        new BinaryOperator("Status", Status.Progress),
                                                                        new BinaryOperator("Status", Status.Posted))));

                        if (_locInvoiceCanvassingLines != null && _locInvoiceCanvassingLines.Count() > 0)
                        {
                            foreach (InvoiceCanvassingLine _locInvoiceCanvassingLine in _locInvoiceCanvassingLines)
                            {
                                if (_locInvoiceCanvassingLine.SelectItem != null)
                                {
                                    InventoryJournal _locNegatifInventoryJournal = new InventoryJournal(_currSession)
                                    {
                                        Company = _locInvoiceCanvassingXPO.Company,
                                        Workplace = _locInvoiceCanvassingXPO.Workplace,
                                        Location = _locInvoiceCanvassingXPO.BeginningInventory.Location,
                                        BinLocation = _locInvoiceCanvassingLine.SelectItem.BinLocation,
                                        Salesman = _locInvoiceCanvassingXPO.Salesman,
                                        StockType = _locInvoiceCanvassingLine.StockType,
                                        StockGroup = StockGroup.Normal,
                                        Item = _locInvoiceCanvassingLine.Item,
                                        QtyPos = 0,
                                        QtyNeg = _locInvoiceCanvassingLine.TQty,
                                        DUOM = _locInvoiceCanvassingLine.DUOM,
                                        JournalDate = now,
                                        InvoiceCanvassing = _locInvoiceCanvassingXPO,
                                    };
                                    _locNegatifInventoryJournal.Save();
                                    _locNegatifInventoryJournal.Session.CommitTransaction();
                                }else
                                {
                                    BeginningInventoryLine _locBegInventoryLine = _currSession.FindObject<BeginningInventoryLine>
                                                                                (new GroupOperator(GroupOperatorType.And,
                                                                                new BinaryOperator("Company", _locInvoiceCanvassingXPO.Company),
                                                                                new BinaryOperator("Workplace", _locInvoiceCanvassingXPO.Workplace),
                                                                                new BinaryOperator("BeginningInventory", _locInvoiceCanvassingLine.BeginningInventory),
                                                                                new BinaryOperator("Item", _locInvoiceCanvassingLine.Item)));
                                    if(_locBegInventoryLine != null)
                                    {
                                        InventoryJournal _locNegatifInventoryJournal = new InventoryJournal(_currSession)
                                        {
                                            Company = _locInvoiceCanvassingXPO.Company,
                                            Workplace = _locInvoiceCanvassingXPO.Workplace,
                                            Location = _locBegInventoryLine.Location,
                                            BinLocation = _locBegInventoryLine.BinLocation,
                                            Salesman = _locInvoiceCanvassingXPO.Salesman,
                                            StockType = _locInvoiceCanvassingLine.StockType,
                                            StockGroup = StockGroup.Normal,
                                            Item = _locInvoiceCanvassingLine.Item,
                                            QtyPos = 0,
                                            QtyNeg = _locInvoiceCanvassingLine.TQty,
                                            DUOM = _locInvoiceCanvassingLine.DUOM,
                                            JournalDate = now,
                                            InvoiceCanvassing = _locInvoiceCanvassingXPO,
                                        };
                                        _locNegatifInventoryJournal.Save();
                                        _locNegatifInventoryJournal.Session.CommitTransaction();
                                    }
                                }
                            }
                        }
                        #endregion NormalItem
                        #region FreeItem
                        XPCollection<InvoiceCanvassingFreeItem> _locInvoiceCanvassingFreeItems = new XPCollection<InvoiceCanvassingFreeItem>(_currSession,
                                                                        new GroupOperator(GroupOperatorType.And,
                                                                        new BinaryOperator("InvoiceCanvassing", _locInvoiceCanvassingXPO),
                                                                        new GroupOperator(GroupOperatorType.Or,
                                                                        new BinaryOperator("Status", Status.Open),
                                                                        new BinaryOperator("Status", Status.Progress),
                                                                        new BinaryOperator("Status", Status.Posted))));

                        if (_locInvoiceCanvassingFreeItems != null && _locInvoiceCanvassingFreeItems.Count() > 0)
                        {
                            foreach (InvoiceCanvassingFreeItem _locInvoiceCanvassingFreeItem in _locInvoiceCanvassingFreeItems)
                            {
                                BeginningInventoryLine _locBegInventoryLine = _currSession.FindObject<BeginningInventoryLine>
                                                                                (new GroupOperator(GroupOperatorType.And,
                                                                                new BinaryOperator("Company", _locInvoiceCanvassingXPO.Company),
                                                                                new BinaryOperator("Workplace", _locInvoiceCanvassingXPO.Workplace),
                                                                                new BinaryOperator("BeginningInventory", _locInvoiceCanvassingXPO.BeginningInventory),
                                                                                new BinaryOperator("Item", _locInvoiceCanvassingFreeItem.FpItem)));
                                if (_locBegInventoryLine != null)
                                {
                                    InventoryJournal _locNegatifInventoryJournal = new InventoryJournal(_currSession)
                                    {
                                        Company = _locInvoiceCanvassingXPO.Company,
                                        Workplace = _locInvoiceCanvassingXPO.Workplace,
                                        Location = _locBegInventoryLine.Location,
                                        BinLocation = _locBegInventoryLine.BinLocation,
                                        Salesman = _locInvoiceCanvassingXPO.Salesman,
                                        StockType = _locInvoiceCanvassingFreeItem.FpStockType,
                                        StockGroup = StockGroup.FreeItem,
                                        Item = _locInvoiceCanvassingFreeItem.FpItem,
                                        QtyPos = 0,
                                        QtyNeg = _locInvoiceCanvassingFreeItem.FpTQty,
                                        DUOM = _locInvoiceCanvassingFreeItem.FpDUOM,
                                        JournalDate = now,
                                        InvoiceCanvassing = _locInvoiceCanvassingXPO,
                                    };
                                    _locNegatifInventoryJournal.Save();
                                    _locNegatifInventoryJournal.Session.CommitTransaction();
                                }
                            }
                        }
                        #endregion FreeItem
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = InventoryTransferOut ", ex.ToString());
            }
        }

        private void SetStatusInvoiceCanvassingLine(Session _currSession, InvoiceCanvassing _locInvoiceCanvassingXPO)
        {
            try
            {
                DateTime now = DateTime.Now;
                if (_locInvoiceCanvassingXPO != null)
                {
                    XPCollection<InvoiceCanvassingLine> _locInvoiceCanvassingLines = new XPCollection<InvoiceCanvassingLine>(_currSession,
                                                            new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("InvoiceCanvassing", _locInvoiceCanvassingXPO),
                                                            new GroupOperator(GroupOperatorType.Or,
                                                            new BinaryOperator("Status", Status.Progress),
                                                            new BinaryOperator("Status", Status.Posted))));

                    if (_locInvoiceCanvassingLines != null && _locInvoiceCanvassingLines.Count > 0)
                    {

                        foreach (InvoiceCanvassingLine _locInvoiceCanvassingLine in _locInvoiceCanvassingLines)
                        {
                            _locInvoiceCanvassingLine.Status = Status.Close;
                            _locInvoiceCanvassingLine.ActivationPosting = true;
                            _locInvoiceCanvassingLine.StatusDate = now;
                            _locInvoiceCanvassingLine.Save();
                            _locInvoiceCanvassingLine.Session.CommitTransaction();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = InvoiceCanvassing " + ex.ToString());
            }
        }

        private void SetStatusInvoiceCanvassingFreeItem(Session _currSession, InvoiceCanvassing _locInvoiceCanvassingXPO)
        {
            try
            {
                DateTime now = DateTime.Now;
                if (_locInvoiceCanvassingXPO != null)
                {
                    XPCollection<InvoiceCanvassingFreeItem> _locInvoiceCanvassingFreeItems = new XPCollection<InvoiceCanvassingFreeItem>(_currSession,
                                                            new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("InvoiceCanvassing", _locInvoiceCanvassingXPO),
                                                            new GroupOperator(GroupOperatorType.Or,
                                                            new BinaryOperator("Status", Status.Open),
                                                            new BinaryOperator("Status", Status.Progress),
                                                            new BinaryOperator("Status", Status.Posted))));

                    if (_locInvoiceCanvassingFreeItems != null && _locInvoiceCanvassingFreeItems.Count > 0)
                    {

                        foreach (InvoiceCanvassingFreeItem _locInvoiceCanvassingFreeItem in _locInvoiceCanvassingFreeItems)
                        {
                            _locInvoiceCanvassingFreeItem.Status = Status.Close;
                            _locInvoiceCanvassingFreeItem.StatusDate = now;
                            _locInvoiceCanvassingFreeItem.Save();
                            _locInvoiceCanvassingFreeItem.Session.CommitTransaction();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = InvoiceCanvassing " + ex.ToString());
            }
        }

        private void SetFinalInvoiceCanvassing(Session _currSession, InvoiceCanvassing _locInvoiceCanvassingXPO)
        {
            try
            {
                DateTime now = DateTime.Now;
                int _locStatusCount = 0;
                int _locInvCanvLineCount = 0;

                if (_locInvoiceCanvassingXPO != null)
                {

                    XPCollection<InvoiceCanvassingLine> _locInvoiceCanvassingLines = new XPCollection<InvoiceCanvassingLine>(_currSession,
                                                            new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("InvoiceCanvassing", _locInvoiceCanvassingXPO)));

                    if (_locInvoiceCanvassingLines != null && _locInvoiceCanvassingLines.Count() > 0)
                    {
                        _locInvCanvLineCount = _locInvoiceCanvassingLines.Count();

                        foreach (InvoiceCanvassingLine _locInvoiceCanvassingLine in _locInvoiceCanvassingLines)
                        {
                            if (_locInvoiceCanvassingLine.Status == Status.Close)
                            {
                                _locStatusCount = _locStatusCount + 1;
                            }
                        }
                    }

                    if (_locStatusCount == _locInvCanvLineCount)
                    {
                        _locInvoiceCanvassingXPO.ActivationPosting = true;
                        _locInvoiceCanvassingXPO.Status = Status.Posted;
                        _locInvoiceCanvassingXPO.StatusDate = now;
                        _locInvoiceCanvassingXPO.Save();
                        _locInvoiceCanvassingXPO.Session.CommitTransaction();
                    }
                    else
                    {
                        _locInvoiceCanvassingXPO.Status = Status.Posted;
                        _locInvoiceCanvassingXPO.StatusDate = now;
                        _locInvoiceCanvassingXPO.Save();
                        _locInvoiceCanvassingXPO.Session.CommitTransaction();
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = InvoiceCanvassing " + ex.ToString());
            }
        }

        #endregion Posting Method

        #region Global Method

        private void SuccessMessageShow(string _locMessage)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 4000;
            options.Message = string.Format("{0}", _locMessage);
            options.Type = InformationType.Success;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Success";
            options.Win.Type = WinMessageType.Flyout;
            Application.ShowViewStrategy.ShowMessage(options);
        }

        private void ErrorMessageShow(string _locMessage)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 2000;
            options.Message = string.Format(_locMessage);
            options.Type = InformationType.Warning;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Error";
            options.Win.Type = WinMessageType.Alert;
            Application.ShowViewStrategy.ShowMessage(options);
        }

        #endregion Global Method

        
    }
}
