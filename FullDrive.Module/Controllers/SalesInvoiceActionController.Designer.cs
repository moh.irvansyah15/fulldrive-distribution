﻿namespace FullDrive.Module.Controllers
{
    partial class SalesInvoiceActionController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.SalesInvoiceListviewFilterSelectionAction = new DevExpress.ExpressApp.Actions.SingleChoiceAction(this.components);
            this.SalesInvoiceListviewFilterApprovalSelectionAction = new DevExpress.ExpressApp.Actions.SingleChoiceAction(this.components);
            this.SalesInvoiceApprovalAction = new DevExpress.ExpressApp.Actions.SingleChoiceAction(this.components);
            this.SalesInvoiceProgressAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.SalesInvoiceSetFreeItemAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.SalesInvoicePostingAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.SalesInvoiceGetSOAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.SalesInvoiceETDAndETAAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.SalesInvoiceReleaseCLAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            // 
            // SalesInvoiceListviewFilterSelectionAction
            // 
            this.SalesInvoiceListviewFilterSelectionAction.Caption = "Filter";
            this.SalesInvoiceListviewFilterSelectionAction.ConfirmationMessage = null;
            this.SalesInvoiceListviewFilterSelectionAction.Id = "SalesInvoiceListviewFilterSelectionActionId";
            this.SalesInvoiceListviewFilterSelectionAction.ItemType = DevExpress.ExpressApp.Actions.SingleChoiceActionItemType.ItemIsOperation;
            this.SalesInvoiceListviewFilterSelectionAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.SalesInvoice);
            this.SalesInvoiceListviewFilterSelectionAction.TargetViewType = DevExpress.ExpressApp.ViewType.ListView;
            this.SalesInvoiceListviewFilterSelectionAction.ToolTip = null;
            this.SalesInvoiceListviewFilterSelectionAction.TypeOfView = typeof(DevExpress.ExpressApp.ListView);
            this.SalesInvoiceListviewFilterSelectionAction.Execute += new DevExpress.ExpressApp.Actions.SingleChoiceActionExecuteEventHandler(this.SalesInvoiceListviewFilterSelectionAction_Execute);
            // 
            // SalesInvoiceListviewFilterApprovalSelectionAction
            // 
            this.SalesInvoiceListviewFilterApprovalSelectionAction.Caption = "Approval Selection";
            this.SalesInvoiceListviewFilterApprovalSelectionAction.ConfirmationMessage = null;
            this.SalesInvoiceListviewFilterApprovalSelectionAction.Id = "SalesInvoiceListviewFilterApprovalSelectionActionId";
            this.SalesInvoiceListviewFilterApprovalSelectionAction.ItemType = DevExpress.ExpressApp.Actions.SingleChoiceActionItemType.ItemIsOperation;
            this.SalesInvoiceListviewFilterApprovalSelectionAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.SalesInvoice);
            this.SalesInvoiceListviewFilterApprovalSelectionAction.TargetViewType = DevExpress.ExpressApp.ViewType.ListView;
            this.SalesInvoiceListviewFilterApprovalSelectionAction.ToolTip = null;
            this.SalesInvoiceListviewFilterApprovalSelectionAction.TypeOfView = typeof(DevExpress.ExpressApp.ListView);
            this.SalesInvoiceListviewFilterApprovalSelectionAction.Execute += new DevExpress.ExpressApp.Actions.SingleChoiceActionExecuteEventHandler(this.SalesInvoiceListviewFilterApprovalSelectionAction_Execute);
            // 
            // SalesInvoiceApprovalAction
            // 
            this.SalesInvoiceApprovalAction.Caption = "Approval";
            this.SalesInvoiceApprovalAction.ConfirmationMessage = null;
            this.SalesInvoiceApprovalAction.Id = "SalesInvoiceApprovalActionId";
            this.SalesInvoiceApprovalAction.ItemType = DevExpress.ExpressApp.Actions.SingleChoiceActionItemType.ItemIsOperation;
            this.SalesInvoiceApprovalAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.SalesInvoice);
            this.SalesInvoiceApprovalAction.ToolTip = null;
            this.SalesInvoiceApprovalAction.Execute += new DevExpress.ExpressApp.Actions.SingleChoiceActionExecuteEventHandler(this.SalesInvoiceApprovalAction_Execute);
            // 
            // SalesInvoiceProgressAction
            // 
            this.SalesInvoiceProgressAction.Caption = "Progress";
            this.SalesInvoiceProgressAction.ConfirmationMessage = null;
            this.SalesInvoiceProgressAction.Id = "SalesInvoiceProgressActionId";
            this.SalesInvoiceProgressAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.SalesInvoice);
            this.SalesInvoiceProgressAction.ToolTip = null;
            this.SalesInvoiceProgressAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.SalesInvoiceProgressAction_Execute);
            // 
            // SalesInvoiceSetFreeItemAction
            // 
            this.SalesInvoiceSetFreeItemAction.Caption = "Set Free Item";
            this.SalesInvoiceSetFreeItemAction.ConfirmationMessage = null;
            this.SalesInvoiceSetFreeItemAction.Id = "SalesInvoiceSetFreeItemActionId";
            this.SalesInvoiceSetFreeItemAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.SalesInvoice);
            this.SalesInvoiceSetFreeItemAction.ToolTip = null;
            this.SalesInvoiceSetFreeItemAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.SalesInvoiceSetFreeItemAction_Execute);
            // 
            // SalesInvoicePostingAction
            // 
            this.SalesInvoicePostingAction.Caption = "Posting";
            this.SalesInvoicePostingAction.ConfirmationMessage = null;
            this.SalesInvoicePostingAction.Id = "SalesInvoicePostingActionId";
            this.SalesInvoicePostingAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.SalesInvoice);
            this.SalesInvoicePostingAction.ToolTip = null;
            this.SalesInvoicePostingAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.SalesInvoicePostingAction_Execute);
            // 
            // SalesInvoiceGetSOAction
            // 
            this.SalesInvoiceGetSOAction.Caption = "Get SO";
            this.SalesInvoiceGetSOAction.ConfirmationMessage = null;
            this.SalesInvoiceGetSOAction.Id = "SalesInvoiceGetSOActionId";
            this.SalesInvoiceGetSOAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.SalesInvoice);
            this.SalesInvoiceGetSOAction.ToolTip = null;
            this.SalesInvoiceGetSOAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.SalesInvoiceGetSOAction_Execute);
            // 
            // SalesInvoiceETDAndETAAction
            // 
            this.SalesInvoiceETDAndETAAction.Caption = "ETD And ETA";
            this.SalesInvoiceETDAndETAAction.ConfirmationMessage = null;
            this.SalesInvoiceETDAndETAAction.Id = "SalesInvoiceETDAndETAActionId";
            this.SalesInvoiceETDAndETAAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.SalesInvoice);
            this.SalesInvoiceETDAndETAAction.ToolTip = null;
            this.SalesInvoiceETDAndETAAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.SalesInvoiceETDAndETAAction_Execute);
            // 
            // SalesInvoiceReleaseCLAction
            // 
            this.SalesInvoiceReleaseCLAction.Caption = "Release CL";
            this.SalesInvoiceReleaseCLAction.ConfirmationMessage = null;
            this.SalesInvoiceReleaseCLAction.Id = "SalesInvoiceReleaseCLActionId";
            this.SalesInvoiceReleaseCLAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.SalesInvoice);
            this.SalesInvoiceReleaseCLAction.ToolTip = null;
            this.SalesInvoiceReleaseCLAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.SalesInvoiceReleaseCLAction_Execute);
            // 
            // SalesInvoiceActionController
            // 
            this.Actions.Add(this.SalesInvoiceListviewFilterSelectionAction);
            this.Actions.Add(this.SalesInvoiceListviewFilterApprovalSelectionAction);
            this.Actions.Add(this.SalesInvoiceApprovalAction);
            this.Actions.Add(this.SalesInvoiceProgressAction);
            this.Actions.Add(this.SalesInvoiceSetFreeItemAction);
            this.Actions.Add(this.SalesInvoicePostingAction);
            this.Actions.Add(this.SalesInvoiceGetSOAction);
            this.Actions.Add(this.SalesInvoiceETDAndETAAction);
            this.Actions.Add(this.SalesInvoiceReleaseCLAction);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SingleChoiceAction SalesInvoiceListviewFilterSelectionAction;
        private DevExpress.ExpressApp.Actions.SingleChoiceAction SalesInvoiceListviewFilterApprovalSelectionAction;
        private DevExpress.ExpressApp.Actions.SingleChoiceAction SalesInvoiceApprovalAction;
        private DevExpress.ExpressApp.Actions.SimpleAction SalesInvoiceProgressAction;
        private DevExpress.ExpressApp.Actions.SimpleAction SalesInvoiceSetFreeItemAction;
        private DevExpress.ExpressApp.Actions.SimpleAction SalesInvoicePostingAction;
        private DevExpress.ExpressApp.Actions.SimpleAction SalesInvoiceGetSOAction;
        private DevExpress.ExpressApp.Actions.SimpleAction SalesInvoiceETDAndETAAction;
        private DevExpress.ExpressApp.Actions.SimpleAction SalesInvoiceReleaseCLAction;
    }
}
