﻿namespace FullDrive.Module.Controllers
{
    partial class AccountReclassJournalActionController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.AccountReclassJournalPostingAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.AccountReclassJournalProgressAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            // 
            // AccountReclassJournalPostingAction
            // 
            this.AccountReclassJournalPostingAction.Caption = "Posting";
            this.AccountReclassJournalPostingAction.ConfirmationMessage = null;
            this.AccountReclassJournalPostingAction.Id = "AccountReclassJournalPostingActionId";
            this.AccountReclassJournalPostingAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.AccountReclassJournal);
            this.AccountReclassJournalPostingAction.ToolTip = null;
            this.AccountReclassJournalPostingAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.AccountReclassJournalPostingAction_Execute);
            // 
            // AccountReclassJournalProgressAction
            // 
            this.AccountReclassJournalProgressAction.Caption = "Progress";
            this.AccountReclassJournalProgressAction.ConfirmationMessage = null;
            this.AccountReclassJournalProgressAction.Id = "AccountReclassJournalProgressActionId";
            this.AccountReclassJournalProgressAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.AccountReclassJournal);
            this.AccountReclassJournalProgressAction.ToolTip = null;
            this.AccountReclassJournalProgressAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.AccountReclassJournalProgressAction_Execute);
            // 
            // AccountReclassJournalActionController
            // 
            this.Actions.Add(this.AccountReclassJournalPostingAction);
            this.Actions.Add(this.AccountReclassJournalProgressAction);

        }

        #endregion
        private DevExpress.ExpressApp.Actions.SimpleAction AccountReclassJournalPostingAction;
        private DevExpress.ExpressApp.Actions.SimpleAction AccountReclassJournalProgressAction;
    }
}
