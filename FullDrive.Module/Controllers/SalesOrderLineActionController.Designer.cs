﻿namespace FullDrive.Module.Controllers
{
    partial class SalesOrderLineActionController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.SalesOrderLineSelectAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.SalesOrderLineUnselectAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.SalesOrderLineTaxAndDiscountAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            // 
            // SalesOrderLineSelectAction
            // 
            this.SalesOrderLineSelectAction.Caption = "Select";
            this.SalesOrderLineSelectAction.ConfirmationMessage = null;
            this.SalesOrderLineSelectAction.Id = "SalesOrderLineSelectActionId";
            this.SalesOrderLineSelectAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.SalesOrderLine);
            this.SalesOrderLineSelectAction.TargetViewType = DevExpress.ExpressApp.ViewType.ListView;
            this.SalesOrderLineSelectAction.ToolTip = null;
            this.SalesOrderLineSelectAction.TypeOfView = typeof(DevExpress.ExpressApp.ListView);
            this.SalesOrderLineSelectAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.SalesOrderLineSelectAction_Execute);
            // 
            // SalesOrderLineUnselectAction
            // 
            this.SalesOrderLineUnselectAction.Caption = "Unselect";
            this.SalesOrderLineUnselectAction.ConfirmationMessage = null;
            this.SalesOrderLineUnselectAction.Id = "SalesOrderLineUnselectActionId";
            this.SalesOrderLineUnselectAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.SalesOrderLine);
            this.SalesOrderLineUnselectAction.TargetViewType = DevExpress.ExpressApp.ViewType.ListView;
            this.SalesOrderLineUnselectAction.ToolTip = null;
            this.SalesOrderLineUnselectAction.TypeOfView = typeof(DevExpress.ExpressApp.ListView);
            this.SalesOrderLineUnselectAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.SalesOrderLineUnselectAction_Execute);
            // 
            // SalesOrderLineTaxAndDiscountAction
            // 
            this.SalesOrderLineTaxAndDiscountAction.Caption = "Tax And Discount";
            this.SalesOrderLineTaxAndDiscountAction.ConfirmationMessage = null;
            this.SalesOrderLineTaxAndDiscountAction.Id = "SalesOrderLineTaxAndDiscountActionId";
            this.SalesOrderLineTaxAndDiscountAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.SalesOrderLine);
            this.SalesOrderLineTaxAndDiscountAction.TargetViewType = DevExpress.ExpressApp.ViewType.ListView;
            this.SalesOrderLineTaxAndDiscountAction.ToolTip = null;
            this.SalesOrderLineTaxAndDiscountAction.TypeOfView = typeof(DevExpress.ExpressApp.ListView);
            this.SalesOrderLineTaxAndDiscountAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.SalesOrderLineTaxAndDiscountAction_Execute);
            // 
            // SalesOrderLineActionController
            // 
            this.Actions.Add(this.SalesOrderLineSelectAction);
            this.Actions.Add(this.SalesOrderLineUnselectAction);
            this.Actions.Add(this.SalesOrderLineTaxAndDiscountAction);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SimpleAction SalesOrderLineSelectAction;
        private DevExpress.ExpressApp.Actions.SimpleAction SalesOrderLineUnselectAction;
        private DevExpress.ExpressApp.Actions.SimpleAction SalesOrderLineTaxAndDiscountAction;
    }
}
