﻿namespace FullDrive.Module.Controllers
{
    partial class InvoiceCanvassingActionController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.InvoiceCanvassingListviewFilterSelectionAction = new DevExpress.ExpressApp.Actions.SingleChoiceAction(this.components);
            this.InvoiceCanvassingProgressAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.InvoiceCanvassingPostingAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.InvoiceCanvassingSetFreeItemAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            // 
            // InvoiceCanvassingListviewFilterSelectionAction
            // 
            this.InvoiceCanvassingListviewFilterSelectionAction.Caption = "Filter";
            this.InvoiceCanvassingListviewFilterSelectionAction.ConfirmationMessage = null;
            this.InvoiceCanvassingListviewFilterSelectionAction.Id = "InvoiceCanvassingListviewFilterSelectionActionId";
            this.InvoiceCanvassingListviewFilterSelectionAction.ItemType = DevExpress.ExpressApp.Actions.SingleChoiceActionItemType.ItemIsOperation;
            this.InvoiceCanvassingListviewFilterSelectionAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.InvoiceCanvassing);
            this.InvoiceCanvassingListviewFilterSelectionAction.TargetViewType = DevExpress.ExpressApp.ViewType.ListView;
            this.InvoiceCanvassingListviewFilterSelectionAction.ToolTip = null;
            this.InvoiceCanvassingListviewFilterSelectionAction.TypeOfView = typeof(DevExpress.ExpressApp.ListView);
            this.InvoiceCanvassingListviewFilterSelectionAction.Execute += new DevExpress.ExpressApp.Actions.SingleChoiceActionExecuteEventHandler(this.InvoiceCanvassingListviewFilterSelectionAction_Execute);
            // 
            // InvoiceCanvassingProgressAction
            // 
            this.InvoiceCanvassingProgressAction.Caption = "Progress";
            this.InvoiceCanvassingProgressAction.ConfirmationMessage = null;
            this.InvoiceCanvassingProgressAction.Id = "InvoiceCanvassingProgressActionId";
            this.InvoiceCanvassingProgressAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.InvoiceCanvassing);
            this.InvoiceCanvassingProgressAction.ToolTip = null;
            this.InvoiceCanvassingProgressAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.InvoiceCanvassingProgressAction_Execute);
            // 
            // InvoiceCanvassingPostingAction
            // 
            this.InvoiceCanvassingPostingAction.Caption = "Posting";
            this.InvoiceCanvassingPostingAction.ConfirmationMessage = null;
            this.InvoiceCanvassingPostingAction.Id = "InvoiceCanvassingPostingActionId";
            this.InvoiceCanvassingPostingAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.InvoiceCanvassing);
            this.InvoiceCanvassingPostingAction.ToolTip = null;
            this.InvoiceCanvassingPostingAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.InvoiceCanvassingPostingAction_Execute);
            // 
            // InvoiceCanvassingSetFreeItemAction
            // 
            this.InvoiceCanvassingSetFreeItemAction.Caption = "Set Free Item";
            this.InvoiceCanvassingSetFreeItemAction.ConfirmationMessage = null;
            this.InvoiceCanvassingSetFreeItemAction.Id = "InvoiceCanvassingSetFreeItemActionId";
            this.InvoiceCanvassingSetFreeItemAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.InvoiceCanvassing);
            this.InvoiceCanvassingSetFreeItemAction.ToolTip = null;
            this.InvoiceCanvassingSetFreeItemAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.InvoiceCanvassingSetFreeItemAction_Execute);
            // 
            // InvoiceCanvassingActionController
            // 
            this.Actions.Add(this.InvoiceCanvassingListviewFilterSelectionAction);
            this.Actions.Add(this.InvoiceCanvassingProgressAction);
            this.Actions.Add(this.InvoiceCanvassingPostingAction);
            this.Actions.Add(this.InvoiceCanvassingSetFreeItemAction);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SingleChoiceAction InvoiceCanvassingListviewFilterSelectionAction;
        private DevExpress.ExpressApp.Actions.SimpleAction InvoiceCanvassingProgressAction;
        private DevExpress.ExpressApp.Actions.SimpleAction InvoiceCanvassingPostingAction;
        private DevExpress.ExpressApp.Actions.SimpleAction InvoiceCanvassingSetFreeItemAction;
    }
}
