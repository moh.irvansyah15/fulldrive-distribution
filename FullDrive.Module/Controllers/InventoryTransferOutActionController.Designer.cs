﻿namespace FullDrive.Module.Controllers
{
    partial class InventoryTransferOutActionController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.InventoryTransferOutProgressAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.InventoryTransferOutPostingAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.InventoryTransferOutListviewFilterSelectionAction = new DevExpress.ExpressApp.Actions.SingleChoiceAction(this.components);
            // 
            // InventoryTransferOutProgressAction
            // 
            this.InventoryTransferOutProgressAction.Caption = "Progress";
            this.InventoryTransferOutProgressAction.ConfirmationMessage = null;
            this.InventoryTransferOutProgressAction.Id = "InventoryTransferOutProgressActionId";
            this.InventoryTransferOutProgressAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.InventoryTransferOut);
            this.InventoryTransferOutProgressAction.ToolTip = null;
            this.InventoryTransferOutProgressAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.InventoryTransferOutProgressAction_Execute);
            // 
            // InventoryTransferOutPostingAction
            // 
            this.InventoryTransferOutPostingAction.Caption = "Posting";
            this.InventoryTransferOutPostingAction.ConfirmationMessage = null;
            this.InventoryTransferOutPostingAction.Id = "InventoryTransferOutPostingActionId";
            this.InventoryTransferOutPostingAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.InventoryTransferOut);
            this.InventoryTransferOutPostingAction.ToolTip = null;
            this.InventoryTransferOutPostingAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.InventoryTransferOutPostingAction_Execute);
            // 
            // InventoryTransferOutListviewFilterSelectionAction
            // 
            this.InventoryTransferOutListviewFilterSelectionAction.Caption = "Filter";
            this.InventoryTransferOutListviewFilterSelectionAction.ConfirmationMessage = null;
            this.InventoryTransferOutListviewFilterSelectionAction.Id = "InventoryTransferOutListviewFilterSelectionActionId";
            this.InventoryTransferOutListviewFilterSelectionAction.ItemType = DevExpress.ExpressApp.Actions.SingleChoiceActionItemType.ItemIsOperation;
            this.InventoryTransferOutListviewFilterSelectionAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.InventoryTransferOut);
            this.InventoryTransferOutListviewFilterSelectionAction.ToolTip = null;
            this.InventoryTransferOutListviewFilterSelectionAction.Execute += new DevExpress.ExpressApp.Actions.SingleChoiceActionExecuteEventHandler(this.InventoryTransferOutListviewFilterSelectionAction_Execute);
            // 
            // InventoryTransferOutActionController
            // 
            this.Actions.Add(this.InventoryTransferOutProgressAction);
            this.Actions.Add(this.InventoryTransferOutPostingAction);
            this.Actions.Add(this.InventoryTransferOutListviewFilterSelectionAction);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SimpleAction InventoryTransferOutProgressAction;
        private DevExpress.ExpressApp.Actions.SimpleAction InventoryTransferOutPostingAction;
        private DevExpress.ExpressApp.Actions.SingleChoiceAction InventoryTransferOutListviewFilterSelectionAction;
    }
}
