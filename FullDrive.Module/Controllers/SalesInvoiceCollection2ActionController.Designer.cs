﻿namespace FullDrive.Module.Controllers
{
    partial class SalesInvoiceCollection2ActionController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.SalesInvoiceCollection2ShowSIMAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            // 
            // SalesInvoiceCollection2ShowSIMAction
            // 
            this.SalesInvoiceCollection2ShowSIMAction.Caption = "Show SIM";
            this.SalesInvoiceCollection2ShowSIMAction.ConfirmationMessage = null;
            this.SalesInvoiceCollection2ShowSIMAction.Id = "SalesInvoiceCollection2ShowSIMActionId";
            this.SalesInvoiceCollection2ShowSIMAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.SalesInvoiceCollection2);
            this.SalesInvoiceCollection2ShowSIMAction.ToolTip = null;
            this.SalesInvoiceCollection2ShowSIMAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.SalesInvoiceCollection2ShowSIMAction_Execute);
            // 
            // SalesInvoiceCollection2ActionController
            // 
            this.Actions.Add(this.SalesInvoiceCollection2ShowSIMAction);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SimpleAction SalesInvoiceCollection2ShowSIMAction;
    }
}
