﻿namespace FullDrive.Module.Controllers
{
    partial class InvoiceInMonitoringActionController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.InvoiceInMonitoringImportAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.InvoiceInMonitoringGetDataAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            // 
            // InvoiceInMonitoringImportAction
            // 
            this.InvoiceInMonitoringImportAction.Caption = "Import";
            this.InvoiceInMonitoringImportAction.ConfirmationMessage = null;
            this.InvoiceInMonitoringImportAction.Id = "InvoiceInMonitoringImportActionId";
            this.InvoiceInMonitoringImportAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.InvoiceInMonitoring);
            this.InvoiceInMonitoringImportAction.ToolTip = null;
            this.InvoiceInMonitoringImportAction.TypeOfView = typeof(DevExpress.ExpressApp.View);
            this.InvoiceInMonitoringImportAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.InvoiceInMonitoringImportAction_Execute);
            // 
            // InvoiceInMonitoringGetDataAction
            // 
            this.InvoiceInMonitoringGetDataAction.Caption = "Get Data";
            this.InvoiceInMonitoringGetDataAction.ConfirmationMessage = null;
            this.InvoiceInMonitoringGetDataAction.Id = "InvoiceInMonitoringGetDataActionId";
            this.InvoiceInMonitoringGetDataAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.InvoiceInMonitoring);
            this.InvoiceInMonitoringGetDataAction.ToolTip = null;
            this.InvoiceInMonitoringGetDataAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.InvoiceInMonitoringGetDataAction_Execute);
            // 
            // InvoiceInMonitoringActionController
            // 
            this.Actions.Add(this.InvoiceInMonitoringImportAction);
            this.Actions.Add(this.InvoiceInMonitoringGetDataAction);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SimpleAction InvoiceInMonitoringImportAction;
        private DevExpress.ExpressApp.Actions.SimpleAction InvoiceInMonitoringGetDataAction;
    }
}
