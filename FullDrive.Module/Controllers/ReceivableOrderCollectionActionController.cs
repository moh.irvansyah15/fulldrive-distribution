﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;

using FullDrive.Module.BusinessObjects;
using FullDrive.Module.CustomProcess;
using System.Collections;
using DevExpress.Xpo;

namespace FullDrive.Module.Controllers
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class ReceivableOrderCollectionActionController : ViewController
    {
        public ReceivableOrderCollectionActionController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.
        }

        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
        }

        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }

        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        private void ReceivableOrderCollectionShowOCMAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
            var ListView = View as ListView;
            IObjectSpace os = Application.CreateObjectSpace();
            string listViewId = Application.FindListViewId(typeof(OrderCollectionMonitoring));
            CollectionSourceBase cs = Application.CreateCollectionSource(os, typeof(OrderCollectionMonitoring), listViewId);
            //Tinggal buat 
            ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
            string _stringOCM = null;
            if (_objectsToProcess != null)
            {
                foreach (Object obj in _objectsToProcess)
                {
                    ReceivableOrderCollection _locReceivableOrderCollection = (ReceivableOrderCollection)_objectSpace.GetObject(obj);
                    if (_locReceivableOrderCollection != null)
                    {
                        if (_locReceivableOrderCollection.OrderCollection != null)
                        {
                            if (_stringOCM == null)
                            {
                                if (_locReceivableOrderCollection.OrderCollection.Code != null)
                                {
                                    _stringOCM = "( [OrderCollection.Code]=='" + _locReceivableOrderCollection.OrderCollection.Code + "' )";
                                }
                            }
                            else
                            {
                                if (_locReceivableOrderCollection.OrderCollection.Code != null)
                                {
                                    _stringOCM = _stringOCM + " OR ( [OrderCollection.Code]=='" + _locReceivableOrderCollection.OrderCollection.Code + "' )";
                                }
                            }
                        }
                    }
                }
            }
            if (_stringOCM != null)
            {
                cs.Criteria["ReceivableOrderCollectionFilter"] = CriteriaOperator.Parse(_stringOCM);
            }
            e.ShowViewParameters.CreatedView = Application.CreateListView(
                listViewId,
                cs,
                true
            );
            e.ShowViewParameters.Context = TemplateContext.View;
            e.ShowViewParameters.TargetWindow = TargetWindow.NewModalWindow;
            DialogController dc = Application.CreateController<DialogController>();
            dc.Accepting += new EventHandler<DialogControllerAcceptingEventArgs>(dc_Accepting);
            e.ShowViewParameters.Controllers.Add(dc);
        }

        void dc_Accepting(object sender, DialogControllerAcceptingEventArgs e)
        {
            try
            {
                ReceivableTransaction _locReceivableTransaction = null;
                View popupView = ((Controller)sender).Frame.View;
                ArrayList _objectsToProcess = new ArrayList(View.SelectedObjects);
                if (_objectsToProcess != null)
                {
                    foreach (Object _obj in _objectsToProcess)
                    {
                        ReceivableOrderCollection _locReceivableOrderCollection = (ReceivableOrderCollection)popupView.ObjectSpace.GetObject(_obj);
                        if (_locReceivableOrderCollection != null)
                        {
                            if (_locReceivableOrderCollection.ReceivableTransaction != null)
                            {
                                _locReceivableTransaction = _locReceivableOrderCollection.ReceivableTransaction;
                            }
                        }
                    }
                }
                if (_locReceivableTransaction != null)
                {
                    ArrayList _objectsToProcess2 = new ArrayList(popupView.SelectedObjects);
                    if (_objectsToProcess2 != null)
                    {
                        foreach (Object _obj2 in _objectsToProcess2)
                        {
                            OrderCollectionMonitoring _locOrderCollectionMonitoring = (OrderCollectionMonitoring)popupView.ObjectSpace.GetObject(_obj2);
                            if (_locOrderCollectionMonitoring != null && _locOrderCollectionMonitoring.Select == true && _locOrderCollectionMonitoring.Status != Status.Close)
                            {
                                _locOrderCollectionMonitoring.ReceivableTransaction = _locReceivableTransaction;
                            }
                        }
                    }
                }
                popupView.ObjectSpace.CommitChanges();
            }
            catch (ValidationException)
            {
            }
        }

    }
}
