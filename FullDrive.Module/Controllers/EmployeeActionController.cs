﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;

using FullDrive.Module.BusinessObjects;
using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.Xpo;
using System.Collections;
using DevExpress.Xpo;

namespace FullDrive.Module.Controllers
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class EmployeeActionController : ViewController
    {
        public EmployeeActionController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.
        }
        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }
        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        private void EmployeeCreateRouteAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;

                if (_objectsToProcess != null)
                {
                    foreach (Object obj in _objectsToProcess)
                    {
                        Employee _locEmployeeOS = (Employee)_objectSpace.GetObject(obj);

                        if (_locEmployeeOS != null)
                        {
                            if (_locEmployeeOS.Session != null)
                            {
                                _currSession = _locEmployeeOS.Session;
                            }

                            if (_locEmployeeOS.Code != null && _currSession != null)
                            {
                                _currObjectId = _locEmployeeOS.Code;

                                Employee _locEmployeeXPO = _currSession.FindObject<Employee>
                                                                   (new GroupOperator(GroupOperatorType.And,
                                                                    new BinaryOperator("Code", _currObjectId)));

                                if (_locEmployeeXPO != null)
                                {
                                    SetCreateRoute(_currSession, _locEmployeeXPO);
                                    SuccessMessageShow(_locEmployeeXPO.Code + " has been successfully create to Route ");
                                }
                                else
                                {
                                    ErrorMessageShow("Data RoutePlan Not Available");
                                }
                            }
                            else
                            {
                                ErrorMessageShow("Data RoutePlan Not Available");
                            }
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = Employee " + ex.ToString());
            }
        }

        private void EmployeeAddRouteAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;

                if (_objectsToProcess != null)
                {
                    foreach (Object obj in _objectsToProcess)
                    {
                        Employee _locEmployeeOS = (Employee)_objectSpace.GetObject(obj);

                        if (_locEmployeeOS != null)
                        {
                            if (_locEmployeeOS.Session != null)
                            {
                                _currSession = _locEmployeeOS.Session;
                            }

                            if (_locEmployeeOS.Code != null && _currSession != null)
                            {
                                _currObjectId = _locEmployeeOS.Code;

                                Employee _locEmployeeXPO = _currSession.FindObject<Employee>
                                                                   (new GroupOperator(GroupOperatorType.And,
                                                                    new BinaryOperator("Code", _currObjectId)));

                                if (_locEmployeeXPO != null)
                                {
                                    SetAddRoute(_currSession, _locEmployeeXPO);
                                    SuccessMessageShow(_locEmployeeXPO.Code + " has been successfully add to Route ");
                                }
                                else
                                {
                                    ErrorMessageShow("Data RoutePlan Not Available");
                                }
                            }
                            else
                            {
                                ErrorMessageShow("Data RoutePlan Not Available");
                            }
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = Employee " + ex.ToString());
            }
        }

        //====================================== Code Only ===========================================

        #region CreateRoute

        private void SetCreateRoute(Session _currSession, Employee _locEmployeeXPO)
        {
            try
            {
                Vehicle _locVehicle = null;
                DateTime now = DateTime.Now;
                if(_locEmployeeXPO != null)
                {
                    #region DeleteRouteBaseOnSalesman

                    XPCollection<Route> _locRoutes = new XPCollection<Route>(_currSession, new GroupOperator(GroupOperatorType.And,
                                                        new BinaryOperator("Salesman", _locEmployeeXPO)));
                    if(_locRoutes != null && _locRoutes.Count() > 0)
                    {
                        _currSession.Delete(_locRoutes);
                        _currSession.Save(_locRoutes);

                        #region CreateRoute
                        XPCollection<RoutePlan> _locRoutePlans = new XPCollection<RoutePlan>(_currSession, new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("Salesman", _locEmployeeXPO),
                                                                new BinaryOperator("Select", true),
                                                                new BinaryOperator("Active", true)));
                        if (_locRoutePlans != null && _locRoutePlans.Count() > 0)
                        {
                            foreach (RoutePlan _locRoutePlan in _locRoutePlans)
                            {
                                if (_locRoutePlan.Company != null && _locRoutePlan.Workplace != null && _locRoutePlan.Salesman != null && _locRoutePlan.SalesArea != null && _locRoutePlan.SalesAreaLine != null)
                                {
                                    SalesmanVehicleSetup _locSalesmanVehicleSetup = _currSession.FindObject<SalesmanVehicleSetup>(
                                                                           new GroupOperator(GroupOperatorType.And,
                                                                           new BinaryOperator("Company", _locRoutePlan.Company),
                                                                           new BinaryOperator("Workplace", _locRoutePlan.Workplace),
                                                                           new BinaryOperator("Salesman", _locRoutePlan.Salesman),
                                                                           new BinaryOperator("Active", true)));
                                    if (_locSalesmanVehicleSetup != null)
                                    {
                                        if (_locSalesmanVehicleSetup.Vehicle != null)
                                        {
                                            _locVehicle = _locSalesmanVehicleSetup.Vehicle;
                                        }
                                    }

                                    if (_locRoutePlan.IsRangeDate == true)
                                    {
                                        Route _saveDataRoute = new Route(_currSession)
                                        {
                                            Company = _locRoutePlan.Company,
                                            Workplace = _locRoutePlan.Workplace,
                                            Salesman = _locRoutePlan.Salesman,
                                            Vehicle = _locVehicle,
                                            PriceGroup = _locRoutePlan.PriceGroup,
                                            SalesArea = _locRoutePlan.SalesArea,
                                            SalesAreaLine = _locRoutePlan.SalesAreaLine,
                                            Customer = _locRoutePlan.Customer,
                                            Subject = _locRoutePlan.Customer.Name,
                                            StartOn = _locRoutePlan.StartDate,
                                            EndOn = _locRoutePlan.EndDate,
                                            Active = true,
                                        };
                                        _saveDataRoute.Save();
                                        _saveDataRoute.Session.CommitTransaction();
                                    }
                                    else
                                    {
                                        Route _saveDataRoute = new Route(_currSession)
                                        {
                                            Company = _locRoutePlan.Company,
                                            Workplace = _locRoutePlan.Workplace,
                                            Salesman = _locRoutePlan.Salesman,
                                            Vehicle = _locVehicle,
                                            PriceGroup = _locRoutePlan.PriceGroup,
                                            SalesArea = _locRoutePlan.SalesArea,
                                            SalesAreaLine = _locRoutePlan.SalesAreaLine,
                                            Customer = _locRoutePlan.Customer,
                                            Subject = _locRoutePlan.Customer.Name,
                                            StartOn = now,
                                            EndOn = now,
                                            Active = true,
                                        };
                                        _saveDataRoute.Save();
                                        _saveDataRoute.Session.CommitTransaction();
                                    }
                                }
                            }
                        }

                        #endregion CreateRoute
                    }
                    else
                    {
                        #region CreateRoute
                        XPCollection<RoutePlan> _locRoutePlans = new XPCollection<RoutePlan>(_currSession, new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("Salesman", _locEmployeeXPO),
                                                                new BinaryOperator("Select", true),
                                                                new BinaryOperator("Active", true)));
                        if (_locRoutePlans != null && _locRoutePlans.Count() > 0)
                        {
                            foreach (RoutePlan _locRoutePlan in _locRoutePlans)
                            {
                                if (_locRoutePlan.Company != null && _locRoutePlan.Workplace != null && _locRoutePlan.Salesman != null && _locRoutePlan.SalesArea != null && _locRoutePlan.SalesAreaLine != null)
                                {
                                    SalesmanVehicleSetup _locSalesmanVehicleSetup = _currSession.FindObject<SalesmanVehicleSetup>(
                                                                           new GroupOperator(GroupOperatorType.And,
                                                                           new BinaryOperator("Company", _locRoutePlan.Company),
                                                                           new BinaryOperator("Workplace", _locRoutePlan.Workplace),
                                                                           new BinaryOperator("Salesman", _locRoutePlan.Salesman),
                                                                           new BinaryOperator("Active", true)));
                                    if (_locSalesmanVehicleSetup != null)
                                    {
                                        if (_locSalesmanVehicleSetup.Vehicle != null)
                                        {
                                            _locVehicle = _locSalesmanVehicleSetup.Vehicle;
                                        }
                                    }

                                    if (_locRoutePlan.IsRangeDate == true)
                                    {
                                        Route _saveDataRoute = new Route(_currSession)
                                        {
                                            Company = _locRoutePlan.Company,
                                            Workplace = _locRoutePlan.Workplace,
                                            Salesman = _locRoutePlan.Salesman,
                                            Vehicle = _locVehicle,
                                            PriceGroup = _locRoutePlan.PriceGroup,
                                            SalesArea = _locRoutePlan.SalesArea,
                                            SalesAreaLine = _locRoutePlan.SalesAreaLine,
                                            Customer = _locRoutePlan.Customer,
                                            Subject = _locRoutePlan.Customer.Name,
                                            StartOn = _locRoutePlan.StartDate,
                                            EndOn = _locRoutePlan.EndDate,
                                            Active = true,
                                        };
                                        _saveDataRoute.Save();
                                        _saveDataRoute.Session.CommitTransaction();
                                    }
                                    else
                                    {
                                        Route _saveDataRoute = new Route(_currSession)
                                        {
                                            Company = _locRoutePlan.Company,
                                            Workplace = _locRoutePlan.Workplace,
                                            Salesman = _locRoutePlan.Salesman,
                                            Vehicle = _locVehicle,
                                            PriceGroup = _locRoutePlan.PriceGroup,
                                            SalesArea = _locRoutePlan.SalesArea,
                                            SalesAreaLine = _locRoutePlan.SalesAreaLine,
                                            Customer = _locRoutePlan.Customer,
                                            Subject = _locRoutePlan.Customer.Name,
                                            StartOn = now,
                                            EndOn = now,
                                            Active = true,
                                        };
                                        _saveDataRoute.Save();
                                        _saveDataRoute.Session.CommitTransaction();
                                    }
                                }
                            }
                        }

                        #endregion CreateRoute
                    }

                    #endregion DeleteRouteBaseOnSalesman


                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("BusinessObject = Employee" + ex.ToString());
            }
        }

        #endregion CreateRoute

        #region AddRoute

        private void SetAddRoute(Session _currSession, Employee _locEmployeeXPO)
        {
            try
            {
                Vehicle _locVehicle = null;
                DateTime now = DateTime.Now;
                if (_locEmployeeXPO != null)
                {
                    #region AddRoute
                    XPCollection<RoutePlan> _locRoutePlans = new XPCollection<RoutePlan>(_currSession, new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("Salesman", _locEmployeeXPO),
                                                            new BinaryOperator("Select", true),
                                                            new BinaryOperator("Active", true)));
                    if (_locRoutePlans != null && _locRoutePlans.Count() > 0)
                    {
                        foreach (RoutePlan _locRoutePlan in _locRoutePlans)
                        {
                            if (_locRoutePlan.Company != null && _locRoutePlan.Workplace != null && _locRoutePlan.Salesman != null && _locRoutePlan.SalesArea != null && _locRoutePlan.SalesAreaLine != null)
                            {
                                SalesmanVehicleSetup _locSalesmanVehicleSetup = _currSession.FindObject<SalesmanVehicleSetup>(
                                                                       new GroupOperator(GroupOperatorType.And,
                                                                       new BinaryOperator("Company", _locRoutePlan.Company),
                                                                       new BinaryOperator("Workplace", _locRoutePlan.Workplace),
                                                                       new BinaryOperator("Salesman", _locRoutePlan.Salesman),
                                                                       new BinaryOperator("Active", true)));
                                if (_locSalesmanVehicleSetup != null)
                                {
                                    if (_locSalesmanVehicleSetup.Vehicle != null)
                                    {
                                        _locVehicle = _locSalesmanVehicleSetup.Vehicle;
                                    }
                                }

                                if (_locRoutePlan.IsRangeDate == true)
                                {
                                    Route _saveDataRoute = new Route(_currSession)
                                    {
                                        Company = _locRoutePlan.Company,
                                        Workplace = _locRoutePlan.Workplace,
                                        Salesman = _locRoutePlan.Salesman,
                                        Vehicle = _locVehicle,
                                        PriceGroup = _locRoutePlan.PriceGroup,
                                        SalesArea = _locRoutePlan.SalesArea,
                                        SalesAreaLine = _locRoutePlan.SalesAreaLine,
                                        Customer = _locRoutePlan.Customer,
                                        Subject = _locRoutePlan.Customer.Name,
                                        StartOn = _locRoutePlan.StartDate,
                                        EndOn = _locRoutePlan.EndDate,
                                        Active = true,
                                    };
                                    _saveDataRoute.Save();
                                    _saveDataRoute.Session.CommitTransaction();
                                }
                                else
                                {
                                    Route _saveDataRoute = new Route(_currSession)
                                    {
                                        Company = _locRoutePlan.Company,
                                        Workplace = _locRoutePlan.Workplace,
                                        Salesman = _locRoutePlan.Salesman,
                                        Vehicle = _locVehicle,
                                        PriceGroup = _locRoutePlan.PriceGroup,
                                        SalesArea = _locRoutePlan.SalesArea,
                                        SalesAreaLine = _locRoutePlan.SalesAreaLine,
                                        Customer = _locRoutePlan.Customer,
                                        Subject = _locRoutePlan.Customer.Name,
                                        StartOn = now,
                                        EndOn = now,
                                        Active = true,
                                    };
                                    _saveDataRoute.Save();
                                    _saveDataRoute.Session.CommitTransaction();
                                }
                            }
                        }
                    }

                    #endregion AddRoute
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("BusinessObject = Employee" + ex.ToString());
            }
        }

        #endregion AddRoute

        #region Global Method

        private void SuccessMessageShow(string _locActionName)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 2000;
            options.Message = string.Format("{0}", _locActionName);
            options.Type = InformationType.Success;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Success";
            options.Win.Type = WinMessageType.Flyout;
            Application.ShowViewStrategy.ShowMessage(options);
        }

        private void ErrorMessageShow(string _locMessage)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 2000;
            options.Message = string.Format(_locMessage);
            options.Type = InformationType.Warning;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Error";
            options.Win.Type = WinMessageType.Alert;
            Application.ShowViewStrategy.ShowMessage(options);
        }

        #endregion Global Method
    }
}
