﻿namespace FullDrive.Module.Controllers
{
    partial class SalesOrderActionController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.SalesOrderProgressAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.SalesOrderListviewFilterSelectionAction = new DevExpress.ExpressApp.Actions.SingleChoiceAction(this.components);
            this.SalesOrderPostingAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.SalesOrderSetFreeItemAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.SalesOrderApprovalAction = new DevExpress.ExpressApp.Actions.SingleChoiceAction(this.components);
            this.SalesOrderListviewFilterApprovalSelectionAction = new DevExpress.ExpressApp.Actions.SingleChoiceAction(this.components);
            this.SalesOrderReleaseCLAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.SalesOrderCancelOrderAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            // 
            // SalesOrderProgressAction
            // 
            this.SalesOrderProgressAction.Caption = "Progress";
            this.SalesOrderProgressAction.ConfirmationMessage = null;
            this.SalesOrderProgressAction.Id = "SalesOrderProgressActionId";
            this.SalesOrderProgressAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.SalesOrder);
            this.SalesOrderProgressAction.ToolTip = null;
            this.SalesOrderProgressAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.SalesOrderProgressAction_Execute);
            // 
            // SalesOrderListviewFilterSelectionAction
            // 
            this.SalesOrderListviewFilterSelectionAction.Caption = "Filter";
            this.SalesOrderListviewFilterSelectionAction.ConfirmationMessage = null;
            this.SalesOrderListviewFilterSelectionAction.Id = "SalesOrderListviewFilterSelectionActionId";
            this.SalesOrderListviewFilterSelectionAction.ItemType = DevExpress.ExpressApp.Actions.SingleChoiceActionItemType.ItemIsOperation;
            this.SalesOrderListviewFilterSelectionAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.SalesOrder);
            this.SalesOrderListviewFilterSelectionAction.TargetViewType = DevExpress.ExpressApp.ViewType.ListView;
            this.SalesOrderListviewFilterSelectionAction.ToolTip = null;
            this.SalesOrderListviewFilterSelectionAction.TypeOfView = typeof(DevExpress.ExpressApp.ListView);
            this.SalesOrderListviewFilterSelectionAction.Execute += new DevExpress.ExpressApp.Actions.SingleChoiceActionExecuteEventHandler(this.SalesOrderListviewFilterSelectionAction_Execute);
            // 
            // SalesOrderPostingAction
            // 
            this.SalesOrderPostingAction.Caption = "Posting";
            this.SalesOrderPostingAction.ConfirmationMessage = null;
            this.SalesOrderPostingAction.Id = "SalesOrderPostingActionId";
            this.SalesOrderPostingAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.SalesOrder);
            this.SalesOrderPostingAction.ToolTip = null;
            this.SalesOrderPostingAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.SalesOrderPostingAction_Execute);
            // 
            // SalesOrderSetFreeItemAction
            // 
            this.SalesOrderSetFreeItemAction.Caption = "Set Free Item";
            this.SalesOrderSetFreeItemAction.ConfirmationMessage = null;
            this.SalesOrderSetFreeItemAction.Id = "SalesOrderSetFreeItemActionId";
            this.SalesOrderSetFreeItemAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.SalesOrder);
            this.SalesOrderSetFreeItemAction.ToolTip = null;
            this.SalesOrderSetFreeItemAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.SalesOrderSetFreeItemAction_Execute);
            // 
            // SalesOrderApprovalAction
            // 
            this.SalesOrderApprovalAction.Caption = "Approval";
            this.SalesOrderApprovalAction.ConfirmationMessage = null;
            this.SalesOrderApprovalAction.Id = "SalesOrderApprovalActionId";
            this.SalesOrderApprovalAction.ItemType = DevExpress.ExpressApp.Actions.SingleChoiceActionItemType.ItemIsOperation;
            this.SalesOrderApprovalAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.SalesOrder);
            this.SalesOrderApprovalAction.ToolTip = null;
            this.SalesOrderApprovalAction.Execute += new DevExpress.ExpressApp.Actions.SingleChoiceActionExecuteEventHandler(this.SalesOrderApprovalAction_Execute);
            // 
            // SalesOrderListviewFilterApprovalSelectionAction
            // 
            this.SalesOrderListviewFilterApprovalSelectionAction.Caption = "Filter Approval";
            this.SalesOrderListviewFilterApprovalSelectionAction.ConfirmationMessage = null;
            this.SalesOrderListviewFilterApprovalSelectionAction.Id = "SalesOrderListviewFilterApprovalSelectionActionId";
            this.SalesOrderListviewFilterApprovalSelectionAction.ItemType = DevExpress.ExpressApp.Actions.SingleChoiceActionItemType.ItemIsOperation;
            this.SalesOrderListviewFilterApprovalSelectionAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.SalesOrder);
            this.SalesOrderListviewFilterApprovalSelectionAction.TargetViewType = DevExpress.ExpressApp.ViewType.ListView;
            this.SalesOrderListviewFilterApprovalSelectionAction.ToolTip = null;
            this.SalesOrderListviewFilterApprovalSelectionAction.TypeOfView = typeof(DevExpress.ExpressApp.ListView);
            this.SalesOrderListviewFilterApprovalSelectionAction.Execute += new DevExpress.ExpressApp.Actions.SingleChoiceActionExecuteEventHandler(this.SalesOrderListviewFilterApprovalSelectionAction_Execute);
            // 
            // SalesOrderReleaseCLAction
            // 
            this.SalesOrderReleaseCLAction.Caption = "Release CL";
            this.SalesOrderReleaseCLAction.ConfirmationMessage = null;
            this.SalesOrderReleaseCLAction.Id = "SalesOrderReleaseCLActionId";
            this.SalesOrderReleaseCLAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.SalesOrder);
            this.SalesOrderReleaseCLAction.ToolTip = null;
            this.SalesOrderReleaseCLAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.SalesOrderReleaseCLAction_Execute);
            // 
            // SalesOrderCancelOrderAction
            // 
            this.SalesOrderCancelOrderAction.Caption = "Cancel Order";
            this.SalesOrderCancelOrderAction.ConfirmationMessage = null;
            this.SalesOrderCancelOrderAction.Id = "SalesOrderCancelOrderActionId";
            this.SalesOrderCancelOrderAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.SalesOrder);
            this.SalesOrderCancelOrderAction.ToolTip = null;
            this.SalesOrderCancelOrderAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.SalesOrderCancelOrderAction_Execute);
            // 
            // SalesOrderActionController
            // 
            this.Actions.Add(this.SalesOrderProgressAction);
            this.Actions.Add(this.SalesOrderListviewFilterSelectionAction);
            this.Actions.Add(this.SalesOrderPostingAction);
            this.Actions.Add(this.SalesOrderSetFreeItemAction);
            this.Actions.Add(this.SalesOrderApprovalAction);
            this.Actions.Add(this.SalesOrderListviewFilterApprovalSelectionAction);
            this.Actions.Add(this.SalesOrderReleaseCLAction);
            this.Actions.Add(this.SalesOrderCancelOrderAction);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SimpleAction SalesOrderProgressAction;
        private DevExpress.ExpressApp.Actions.SingleChoiceAction SalesOrderListviewFilterSelectionAction;
        private DevExpress.ExpressApp.Actions.SimpleAction SalesOrderPostingAction;
        private DevExpress.ExpressApp.Actions.SimpleAction SalesOrderSetFreeItemAction;
        private DevExpress.ExpressApp.Actions.SingleChoiceAction SalesOrderApprovalAction;
        private DevExpress.ExpressApp.Actions.SingleChoiceAction SalesOrderListviewFilterApprovalSelectionAction;
        private DevExpress.ExpressApp.Actions.SimpleAction SalesOrderReleaseCLAction;
        private DevExpress.ExpressApp.Actions.SimpleAction SalesOrderCancelOrderAction;
    }
}
