﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;

using FullDrive.Module.BusinessObjects;
using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.Xpo;
using System.Collections;
using DevExpress.Xpo;
using System.IO;
using System.Web;

namespace FullDrive.Module.Controllers
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class PaymentRealizationLineActionController : ViewController
    {
        private ChoiceActionItem _setPaymentStatus;

        public PaymentRealizationLineActionController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.
            #region PaymentStatus
            PaymentRealizationLinePaymentStatusAction.Items.Clear();
            foreach (object _currApprovalFilter in Enum.GetValues(typeof(CustomProcess.PaymentStatus)))
            {
                EnumDescriptor _ed = new EnumDescriptor(typeof(CustomProcess.PaymentStatus));
                _setPaymentStatus = new ChoiceActionItem(_ed.GetCaption(_currApprovalFilter), _currApprovalFilter);
                PaymentRealizationLinePaymentStatusAction.Items.Add(_setPaymentStatus);
            }
            #endregion PaymentStatus
        }

        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
        }

        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }
        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        private void PaymentRealizationLineSelectAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectToProcess = new ArrayList(e.SelectedObjects);
                Session _currSession = null;
                string _currObjectId = null;

                if (_objectToProcess != null)
                {
                    foreach (Object obj in _objectToProcess)
                    {
                        PaymentRealizationLine _locPaymentRealizationLineOS = (PaymentRealizationLine)_objectSpace.GetObject(obj);

                        if (_locPaymentRealizationLineOS != null)
                        {
                            if (_locPaymentRealizationLineOS.Session != null)
                            {
                                _currSession = _locPaymentRealizationLineOS.Session;
                            }

                            if (_locPaymentRealizationLineOS.Code != null && _currSession != null)
                            {
                                _currObjectId = _locPaymentRealizationLineOS.Code;

                                XPCollection<PaymentRealizationLine> _locPaymentRealizationLines = new XPCollection<PaymentRealizationLine>
                                                                                                     (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                      new BinaryOperator("Code", _currObjectId),
                                                                                                      new GroupOperator(GroupOperatorType.Or,
                                                                                                      new BinaryOperator("Status", Status.Open),
                                                                                                      new BinaryOperator("Status", Status.Progress),
                                                                                                      new BinaryOperator("Status", Status.Posted)
                                                                                                      )));

                                if (_locPaymentRealizationLines != null && _locPaymentRealizationLines.Count > 0)
                                {
                                    foreach (PaymentRealizationLine _locPaymentRealizationLine in _locPaymentRealizationLines)
                                    {
                                        _locPaymentRealizationLine.Select = true;
                                        _locPaymentRealizationLine.Save();
                                        _locPaymentRealizationLine.Session.CommitTransaction();
                                    }

                                    SuccessMessageShow("PaymentRealizationLine has been Select");
                                }
                                else
                                {
                                    ErrorMessageShow("Data PaymentRealizationLine Not Available");
                                }
                            }
                        }
                    }
                    if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                    {
                        _objectSpace.CommitChanges();
                        _objectSpace.Refresh();
                    }
                    if (View is ListView)
                    {
                        _objectSpace.CommitChanges();
                        View.ObjectSpace.Refresh();
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("BusinessObject = PaymentRealizationLine" + ex.ToString());
            }
        }

        private void PaymentRealizationLineUnselectAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectToProcess = new ArrayList(e.SelectedObjects);
                Session _currSession = null;
                string _currObjectId = null;

                if (_objectToProcess != null)
                {
                    foreach (Object obj in _objectToProcess)
                    {
                        PaymentRealizationLine _locPaymentRealizationLineOS = (PaymentRealizationLine)_objectSpace.GetObject(obj);

                        if (_locPaymentRealizationLineOS != null)
                        {
                            if (_locPaymentRealizationLineOS.Session != null)
                            {
                                _currSession = _locPaymentRealizationLineOS.Session;
                            }

                            if (_locPaymentRealizationLineOS.Code != null && _currSession != null)
                            {
                                _currObjectId = _locPaymentRealizationLineOS.Code;

                                XPCollection<PaymentRealizationLine> _locPaymentRealizationLines = new XPCollection<PaymentRealizationLine>
                                                                                                     (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                      new BinaryOperator("Code", _currObjectId),
                                                                                                      new GroupOperator(GroupOperatorType.Or,
                                                                                                      new BinaryOperator("Status", Status.Open),
                                                                                                      new BinaryOperator("Status", Status.Progress),
                                                                                                      new BinaryOperator("Status", Status.Posted)
                                                                                                      )));

                                if (_locPaymentRealizationLines != null && _locPaymentRealizationLines.Count > 0)
                                {
                                    foreach (PaymentRealizationLine _locPaymentRealizationLine in _locPaymentRealizationLines)
                                    {
                                        _locPaymentRealizationLine.Select = false;
                                        _locPaymentRealizationLine.Save();
                                        _locPaymentRealizationLine.Session.CommitTransaction();
                                    }

                                    SuccessMessageShow("PaymentRealizationLine has been Unselect");
                                }
                                else
                                {
                                    ErrorMessageShow("Data PaymentRealizationLine Not Available");
                                }
                            }
                        }
                    }
                    if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                    {
                        _objectSpace.CommitChanges();
                        _objectSpace.Refresh();
                    }
                    if (View is ListView)
                    {
                        _objectSpace.CommitChanges();
                        View.ObjectSpace.Refresh();
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("BusinessObject = PaymentRealizationLine" + ex.ToString());
            }
        }

        private void PaymentRealizationLinePaymentStatusAction_Execute(object sender, SingleChoiceActionExecuteEventArgs e)
        {
            try
            {
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currentSession = null;
                string _currObjectId = null;

                foreach (Object obj in _objectsToProcess)
                {
                    PaymentRealizationLine _objInNewObjectSpace = (PaymentRealizationLine)_objectSpace.GetObject(obj);

                    if (_objInNewObjectSpace != null)
                    {
                        if (_objInNewObjectSpace.Code != null)
                        {
                            _currObjectId = _objInNewObjectSpace.Code;
                        }
                        if (_objInNewObjectSpace.Session != null)
                        {
                            _currentSession = _objInNewObjectSpace.Session;
                        }
                    }

                    if (_currObjectId != null && _currentSession != null)
                    {
                        PaymentStatus _locPaymentStatus = PaymentStatus.None;

                        PaymentRealizationLine _locPaymentRealizationLineXPO = _currentSession.FindObject<PaymentRealizationLine>
                                                                        (new GroupOperator(GroupOperatorType.And,
                                                                         new BinaryOperator("Code", _currObjectId),
                                                                         new GroupOperator(GroupOperatorType.Or,
                                                                         new BinaryOperator("Status", Status.Open),
                                                                         new BinaryOperator("Status", Status.Progress),
                                                                         new BinaryOperator("Status", Status.Posted))));

                        if (_locPaymentRealizationLineXPO != null)
                        {
                            if ((PaymentStatus)e.SelectedChoiceActionItem.Data == PaymentStatus.Settled)
                            {
                                _locPaymentStatus = PaymentStatus.Settled;
                            }
                            if ((PaymentStatus)e.SelectedChoiceActionItem.Data == PaymentStatus.Debt)
                            {
                                _locPaymentStatus = PaymentStatus.Debt;
                                
                            }

                            _locPaymentRealizationLineXPO.PaymentStatus = _locPaymentStatus;
                            _locPaymentRealizationLineXPO.PaymentStatusDate = now;
                            _locPaymentRealizationLineXPO.Save();
                            _locPaymentRealizationLineXPO.Session.CommitTransaction();
                            SuccessMessageShow("PaymentRealizationLine has been Change PaymentStatus");
                        }
                        else
                        {
                            ErrorMessageShow("PaymentRealizationLine Not Available");
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PaymentRealizationLine " + ex.ToString());
            }
        }

        //======================================== Code In Here ========================================

        #region Global Method

        private void SuccessMessageShow(string _locActionName)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 2000;
            options.Message = string.Format("{0}", _locActionName);
            options.Type = InformationType.Success;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Success";
            options.Win.Type = WinMessageType.Flyout;
            Application.ShowViewStrategy.ShowMessage(options);
        }

        private void ErrorMessageShow(string _locMessage)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 2000;
            options.Message = string.Format(_locMessage);
            options.Type = InformationType.Warning;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Error";
            options.Win.Type = WinMessageType.Alert;
            Application.ShowViewStrategy.ShowMessage(options);
        }


        #endregion Global Method

    }
}
