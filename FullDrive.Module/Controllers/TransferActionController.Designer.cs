﻿namespace FullDrive.Module.Controllers
{
    partial class TransferActionController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.TransferProgressAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.TransferPostingAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.TransferListviewFilterSelectionAction = new DevExpress.ExpressApp.Actions.SingleChoiceAction(this.components);
            // 
            // TransferProgressAction
            // 
            this.TransferProgressAction.Caption = "Progress";
            this.TransferProgressAction.ConfirmationMessage = null;
            this.TransferProgressAction.Id = "TransferProgressActionId";
            this.TransferProgressAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.Transfer);
            this.TransferProgressAction.ToolTip = null;
            this.TransferProgressAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.TransferProgressAction_Execute);
            // 
            // TransferPostingAction
            // 
            this.TransferPostingAction.Caption = "Posting";
            this.TransferPostingAction.ConfirmationMessage = null;
            this.TransferPostingAction.Id = "TransferPostingActionId";
            this.TransferPostingAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.Transfer);
            this.TransferPostingAction.ToolTip = null;
            this.TransferPostingAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.TransferPostingAction_Execute);
            // 
            // TransferListviewFilterSelectionAction
            // 
            this.TransferListviewFilterSelectionAction.Caption = "Filter";
            this.TransferListviewFilterSelectionAction.ConfirmationMessage = null;
            this.TransferListviewFilterSelectionAction.Id = "TransferListviewFilterSelectionActionId";
            this.TransferListviewFilterSelectionAction.ItemType = DevExpress.ExpressApp.Actions.SingleChoiceActionItemType.ItemIsOperation;
            this.TransferListviewFilterSelectionAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.Transfer);
            this.TransferListviewFilterSelectionAction.ToolTip = null;
            this.TransferListviewFilterSelectionAction.Execute += new DevExpress.ExpressApp.Actions.SingleChoiceActionExecuteEventHandler(this.TransferListviewFilterSelectionAction_Execute);
            // 
            // TransferActionController
            // 
            this.Actions.Add(this.TransferProgressAction);
            this.Actions.Add(this.TransferPostingAction);
            this.Actions.Add(this.TransferListviewFilterSelectionAction);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SimpleAction TransferProgressAction;
        private DevExpress.ExpressApp.Actions.SimpleAction TransferPostingAction;
        private DevExpress.ExpressApp.Actions.SingleChoiceAction TransferListviewFilterSelectionAction;
    }
}
