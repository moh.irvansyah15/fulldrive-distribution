﻿namespace FullDrive.Module.Controllers
{
    partial class InvoiceCanvassingCollectionActionController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.InvoiceCanvassingCollectionShowICMAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            // 
            // InvoiceCanvassingCollectionShowICMAction
            // 
            this.InvoiceCanvassingCollectionShowICMAction.Caption = "Show ICM";
            this.InvoiceCanvassingCollectionShowICMAction.ConfirmationMessage = null;
            this.InvoiceCanvassingCollectionShowICMAction.Id = "InvoiceCanvassingCollectionShowICMActionId";
            this.InvoiceCanvassingCollectionShowICMAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.InvoiceCanvassingCollection);
            this.InvoiceCanvassingCollectionShowICMAction.TargetViewType = DevExpress.ExpressApp.ViewType.ListView;
            this.InvoiceCanvassingCollectionShowICMAction.ToolTip = null;
            this.InvoiceCanvassingCollectionShowICMAction.TypeOfView = typeof(DevExpress.ExpressApp.ListView);
            this.InvoiceCanvassingCollectionShowICMAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.InvoiceCanvassingCollectionShowICMAction_Execute);
            // 
            // InvoiceCanvassingCollectionActionController
            // 
            this.Actions.Add(this.InvoiceCanvassingCollectionShowICMAction);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SimpleAction InvoiceCanvassingCollectionShowICMAction;
    }
}
