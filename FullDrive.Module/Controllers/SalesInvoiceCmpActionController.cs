﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;


using FullDrive.Module.BusinessObjects;
using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.Xpo;
using System.Collections;
using DevExpress.Xpo;
using System.IO;
using System.Web;

namespace FullDrive.Module.Controllers
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class SalesInvoiceCmpActionController : ViewController
    {
        public SalesInvoiceCmpActionController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.
        }
        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }
        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        private void SalesInvoiceCmpEditInvoiceAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;
                Status _locStatus = Status.Open;

                if (_objectsToProcess != null)
                {
                    foreach (Object obj in _objectsToProcess)
                    {
                        SalesInvoiceCmp _locSalesInvoiceCmpOS = (SalesInvoiceCmp)_objectSpace.GetObject(obj);

                        if (_locSalesInvoiceCmpOS != null)
                        {
                            if (_locSalesInvoiceCmpOS.Session != null)
                            {
                                _currSession = _locSalesInvoiceCmpOS.Session;
                            }

                            if (_locSalesInvoiceCmpOS.Code != null && _currSession != null)
                            {
                                _currObjectId = _locSalesInvoiceCmpOS.Code;

                                SalesInvoiceCmp _locSalesInvoiceCmpXPO = _currSession.FindObject<SalesInvoiceCmp>
                                                                   (new GroupOperator(GroupOperatorType.And,
                                                                    new BinaryOperator("Code", _currObjectId)));

                                if (_locSalesInvoiceCmpXPO != null)
                                {
                                    //Menghitung SalesInvoiceLineCmp 
                                    SetTotalUnitAmountInSalesInvoiceLineCmp(_currSession, _locSalesInvoiceCmpXPO);
                                    SetTotalAmountAndTaxAmountAndDiscAmountInSalesInvoiceLineCmp(_currSession, _locSalesInvoiceCmpXPO);
                                    SetTotalAmountAndTaxAmountAndDiscountAmount(_currSession, _locSalesInvoiceCmpXPO);
                                    SetReleaseBalHold(_currSession, _locSalesInvoiceCmpXPO);
                                    SetFreeItem(_currSession, _locSalesInvoiceCmpXPO);
                                    SetTotalFreeItemAmount(_currSession, _locSalesInvoiceCmpXPO);
                                    SuccessMessageShow(_locSalesInvoiceCmpXPO.Code + " has been change successfully Count Amount ");
                                }
                                else
                                {
                                    ErrorMessageShow("Data SalesInvoiceCmp Not Available");
                                }
                            }
                            else
                            {
                                ErrorMessageShow("Data SalesInvoiceCmp Not Available");
                            }
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = SalesOrder " + ex.ToString());
            }
        }

        //=============================================== Code In Here =============================================

        #region EditInvoice

        private void SetTotalUnitAmountInSalesInvoiceLineCmp(Session _currSession, SalesInvoiceCmp _locSalesInvoiceCmpXPO)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                if (_locSalesInvoiceCmpXPO != null)
                {
                    XPCollection<SalesInvoiceLineCmp> _locSalesInvoiceLineCmps = new XPCollection<SalesInvoiceLineCmp>
                                                                                (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                new BinaryOperator("SalesInvoiceCmp", _locSalesInvoiceCmpXPO),
                                                                                new BinaryOperator("Select", true),
                                                                                new GroupOperator(GroupOperatorType.Or,
                                                                                new BinaryOperator("Status", Status.Open))));
                    if (_locSalesInvoiceLineCmps != null && _locSalesInvoiceLineCmps.Count() > 0)
                    {
                        foreach (SalesInvoiceLineCmp _locSalesInvoiceLineCmp in _locSalesInvoiceLineCmps)
                        {

                            if (_globFunc.GetRoundingList(_currSession, ObjectList.SalesInvoiceLineCmp, FieldName.TUAmount) == true)
                            {
                                _locSalesInvoiceLineCmp.TUAmount = _globFunc.GetRoundUp(_currSession, (_locSalesInvoiceLineCmp.TQty * _locSalesInvoiceLineCmp.UAmount), ObjectList.SalesInvoiceLineCmp, FieldName.TUAmount);
                            }
                            else
                            {
                                _locSalesInvoiceLineCmp.TUAmount = _locSalesInvoiceLineCmp.TQty * _locSalesInvoiceLineCmp.UAmount;
                            }
                            _locSalesInvoiceLineCmp.Save();
                            _locSalesInvoiceLineCmp.Session.CommitTransaction();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = SalesInvoiceCmp ", ex.ToString());
            }

        }

        private void SetTotalAmountAndTaxAmountAndDiscAmountInSalesInvoiceLineCmp(Session _currSession, SalesInvoiceCmp _locSalesInvoiceCmpXPO)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                if (_locSalesInvoiceCmpXPO != null)
                {
                    XPCollection<SalesInvoiceLineCmp> _locSalesInvoiceLineCmps = new XPCollection<SalesInvoiceLineCmp>
                                                                                (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                new BinaryOperator("SalesInvoiceCmp", _locSalesInvoiceCmpXPO),
                                                                                new BinaryOperator("Select", true),
                                                                                new GroupOperator(GroupOperatorType.Or,
                                                                                new BinaryOperator("Status", Status.Open))));
                    if (_locSalesInvoiceLineCmps != null && _locSalesInvoiceLineCmps.Count() > 0)
                    {
                        foreach (SalesInvoiceLineCmp _locSalesInvoiceLineCmp in _locSalesInvoiceLineCmps)
                        {
                            SetTaxBeforeDiscount(_currSession, _locSalesInvoiceLineCmp);
                            SetDiscountGross(_currSession, _locSalesInvoiceLineCmp);
                            SetTaxAfterDiscount(_currSession, _locSalesInvoiceLineCmp);
                            SetDiscountNet(_currSession, _locSalesInvoiceLineCmp);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = SalesInvoiceCmp ", ex.ToString());
            }

        }

        private void SetTotalAmountAndTaxAmountAndDiscountAmount(Session _currSession, SalesInvoiceCmp _locSalesInvoiceCmpXPO)
        {
            try
            {
                double _locTAmount = 0;
                double _locTUAmount = 0;
                double _locDiscAmountRule2 = 0;
                double _locTxAmount = 0;
                double _locDiscAmountRule1 = 0;
                if (_locSalesInvoiceCmpXPO != null)
                {
                    XPCollection<SalesInvoiceLineCmp> _locSalesInvoiceLineCmps = new XPCollection<SalesInvoiceLineCmp>
                                                                                (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                new BinaryOperator("SalesInvoiceCmp", _locSalesInvoiceCmpXPO),
                                                                                new BinaryOperator("Select", true),
                                                                                new GroupOperator(GroupOperatorType.Or,
                                                                                new BinaryOperator("Status", Status.Open))));
                    if (_locSalesInvoiceLineCmps != null && _locSalesInvoiceLineCmps.Count() > 0)
                    {
                        foreach (SalesInvoiceLineCmp _locSalesInvoiceLineCmp in _locSalesInvoiceLineCmps)
                        {
                            _locTAmount = _locTAmount + _locSalesInvoiceLineCmp.TAmount;
                            _locTUAmount = _locTUAmount + _locSalesInvoiceLineCmp.TUAmount;
                            _locDiscAmountRule2 = _locDiscAmountRule2 + _locSalesInvoiceLineCmp.DiscAmount;
                            if (_locSalesInvoiceLineCmp.Tax != null)
                            {
                                if (_locSalesInvoiceLineCmp.Tax.TaxNature == TaxNature.Increase)
                                {
                                    _locTxAmount = _locTxAmount + _locSalesInvoiceLineCmp.TxAmount;
                                }
                                if (_locSalesInvoiceLineCmp.Tax.TaxNature == TaxNature.Decrease)
                                {
                                    _locTxAmount = _locTxAmount - _locSalesInvoiceLineCmp.TxAmount;
                                }
                            }
                        }

                        _locDiscAmountRule1 = GetDiscountRule1(_currSession, _locSalesInvoiceCmpXPO);
                    }

                    _locSalesInvoiceCmpXPO.TotAmount = _locTAmount;
                    _locSalesInvoiceCmpXPO.TotUnitAmount = _locTUAmount;
                    _locSalesInvoiceCmpXPO.AmountDisc = _locDiscAmountRule1;
                    _locSalesInvoiceCmpXPO.TotDiscAmount = _locDiscAmountRule2 + _locDiscAmountRule1;
                    _locSalesInvoiceCmpXPO.TotTaxAmount = _locTxAmount;
                    _locSalesInvoiceCmpXPO.Amount = _locTAmount - _locDiscAmountRule1;
                    _locSalesInvoiceCmpXPO.Save();
                    _locSalesInvoiceCmpXPO.Session.CommitTransaction();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = SalesInvoiceCmp ", ex.ToString());
            }

        }

        private void SetFreeItem(Session _currSession, SalesInvoiceCmp _locSalesInvoiceCmpXPO)
        {
            try
            {
                if (_locSalesInvoiceCmpXPO != null)
                {
                    FreeItemRule _locFreeItemRule = null;
                    XPCollection<SalesInvoiceFreeItemCmp> _locSalesInvoiceFreeItemCmps = new XPCollection<BusinessObjects.SalesInvoiceFreeItemCmp>
                                                                                        (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                        new BinaryOperator("SalesInvoiceCmp", _locSalesInvoiceCmpXPO)));


                    if (_locSalesInvoiceFreeItemCmps != null && _locSalesInvoiceFreeItemCmps.Count() > 0)
                    {
                        foreach (SalesInvoiceFreeItemCmp _locSalesInvoiceFreeItemCmp in _locSalesInvoiceFreeItemCmps)
                        {

                            if (_locSalesInvoiceFreeItemCmp.FreeItemRule != null)
                            {
                                _locFreeItemRule = _locSalesInvoiceFreeItemCmp.FreeItemRule;

                                #region Header
                                if (_locFreeItemRule.FreeItemType1 == FreeItemType1.Header)
                                {
                                    if (_locFreeItemRule.OpenAmount == true)
                                    {
                                        //1+1+1+1+1
                                        #region Rule1
                                        if (_locFreeItemRule.RuleType == RuleType.Rule1)
                                        {
                                            double _locQtyFI = 0;
                                            double _locTotQtyFI = 0;
                                            double _locFpDqty = 0;
                                            double _locFpQty = 0;
                                            int _locMaxLoop = 0;
                                            int _locTotMaxLoopVal = 0;
                                            #region Amount
                                            if (_locFreeItemRule.NetAmountRule == true)
                                            {
                                                if (_locSalesInvoiceCmpXPO.TotAmount >= _locFreeItemRule.NetTotalUAmount)
                                                {
                                                    _locQtyFI = System.Math.Floor(_locSalesInvoiceCmpXPO.TotAmount / _locFreeItemRule.NetTotalUAmount);
                                                }
                                            }
                                            if (_locFreeItemRule.GrossAmountRule == true)
                                            {
                                                if (_locSalesInvoiceCmpXPO.TotUnitAmount >= _locFreeItemRule.GrossTotalUAmount)
                                                {
                                                    _locQtyFI = System.Math.Floor(_locSalesInvoiceCmpXPO.TotUnitAmount / _locFreeItemRule.GrossTotalUAmount);
                                                }
                                            }
                                            #endregion Amount

                                            if (_locFreeItemRule.MaxLoop == 0)
                                            {
                                                _locMaxLoop = (int)_locQtyFI;
                                            }
                                            else
                                            {
                                                if (_locFreeItemRule.MaxLoop > (int)_locQtyFI)
                                                {
                                                    _locMaxLoop = (int)_locQtyFI;
                                                }
                                                else
                                                {
                                                    _locMaxLoop = _locFreeItemRule.MaxLoop;
                                                }
                                            }

                                            for (int i = 1; i <= _locMaxLoop; i++)
                                            {
                                                _locTotMaxLoopVal = _locTotMaxLoopVal + 1;
                                            }

                                            _locTotQtyFI = _locTotMaxLoopVal * _locFreeItemRule.FpTQty;
                                            if (_locFreeItemRule.FpItem != null && _locFreeItemRule.FpDUOM != null && _locFreeItemRule.FpUOM != null)
                                            {
                                                ItemUnitOfMeasure _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                                        (new GroupOperator(GroupOperatorType.And,
                                                                         new BinaryOperator("Item", _locFreeItemRule.FpItem),
                                                                         new BinaryOperator("UOM", _locFreeItemRule.FpUOM),
                                                                         new BinaryOperator("DefaultUOM", _locFreeItemRule.FpDUOM),
                                                                         new BinaryOperator("Active", true)));
                                                if (_locItemUOM != null)
                                                {
                                                    if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                                    {
                                                        _locFpDqty = _locTotQtyFI % _locItemUOM.DefaultConversion;
                                                        _locFpQty = System.Math.Floor(_locTotQtyFI / _locItemUOM.DefaultConversion);
                                                    }
                                                    else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                                    {
                                                        _locFpDqty = System.Math.Floor(_locTotQtyFI / _locItemUOM.DefaultConversion);
                                                        _locFpQty = (_locTotQtyFI % _locItemUOM.DefaultConversion) * _locItemUOM.Conversion;
                                                    }
                                                    else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                                    {
                                                        _locFpDqty = _locTotQtyFI;
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                _locFpDqty = _locTotQtyFI;
                                            }

                                            _locSalesInvoiceFreeItemCmp.NFpDQty = _locFpDqty;
                                            _locSalesInvoiceFreeItemCmp.NFpDUOM = _locSalesInvoiceFreeItemCmp.FpDUOM;
                                            _locSalesInvoiceFreeItemCmp.NFpQty = _locFpQty;
                                            _locSalesInvoiceFreeItemCmp.NFpUOM = _locSalesInvoiceFreeItemCmp.FpUOM;
                                            _locSalesInvoiceFreeItemCmp.NFpTQty = _locTotQtyFI;
                                            _locSalesInvoiceFreeItemCmp.NTUAmount = _locTotQtyFI * _locSalesInvoiceFreeItemCmp.UAmount;
                                            _locSalesInvoiceFreeItemCmp.Save();
                                            _locSalesInvoiceFreeItemCmp.Session.CommitTransaction();

                                        }

                                        #endregion Rule1

                                        //1, 2+1, 3+1, 4+1
                                        #region Rule2
                                        if (_locFreeItemRule.RuleType == RuleType.Rule2)
                                        {
                                            double _locQtyFI = 0;
                                            double _locTotQtyFI = 0;
                                            double _locFpDqty = 0;
                                            double _locFpQty = 0;
                                            int _locMaxLoop = 0;

                                            #region Amount
                                            if (_locFreeItemRule.NetAmountRule == true)
                                            {
                                                if (_locSalesInvoiceCmpXPO.TotAmount >= _locFreeItemRule.NetTotalUAmount)
                                                {
                                                    _locQtyFI = System.Math.Floor(_locSalesInvoiceCmpXPO.TotAmount / _locFreeItemRule.NetTotalUAmount);
                                                }
                                            }
                                            if (_locFreeItemRule.GrossAmountRule == true)
                                            {
                                                if (_locSalesInvoiceCmpXPO.TotUnitAmount >= _locFreeItemRule.GrossTotalUAmount)
                                                {
                                                    _locQtyFI = System.Math.Floor(_locSalesInvoiceCmpXPO.TotUnitAmount / _locFreeItemRule.GrossTotalUAmount);
                                                }
                                            }
                                            #endregion Amount

                                            if (_locFreeItemRule.MaxLoop == 0)
                                            {
                                                _locMaxLoop = (int)_locQtyFI;
                                            }
                                            else
                                            {
                                                if (_locFreeItemRule.MaxLoop > (int)_locQtyFI)
                                                {
                                                    _locMaxLoop = (int)_locQtyFI;
                                                }
                                                else
                                                {
                                                    _locMaxLoop = _locFreeItemRule.MaxLoop;
                                                }
                                            }

                                            for (int i = 1; i <= _locMaxLoop; i++)
                                            {
                                                if (i == 1)
                                                {
                                                    _locTotQtyFI = _locFreeItemRule.FpTQty;
                                                }
                                                else
                                                {
                                                    _locTotQtyFI = _locTotQtyFI + (i + _locFreeItemRule.FpTQty);
                                                }

                                            }

                                            if (_locFreeItemRule.FpItem != null && _locFreeItemRule.FpDUOM != null && _locFreeItemRule.FpUOM != null)
                                            {
                                                ItemUnitOfMeasure _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                                        (new GroupOperator(GroupOperatorType.And,
                                                                         new BinaryOperator("Item", _locFreeItemRule.FpItem),
                                                                         new BinaryOperator("UOM", _locFreeItemRule.FpUOM),
                                                                         new BinaryOperator("DefaultUOM", _locFreeItemRule.FpDUOM),
                                                                         new BinaryOperator("Active", true)));
                                                if (_locItemUOM != null)
                                                {
                                                    if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                                    {
                                                        _locFpDqty = _locTotQtyFI % _locItemUOM.DefaultConversion;
                                                        _locFpQty = System.Math.Floor(_locTotQtyFI / _locItemUOM.DefaultConversion);
                                                    }
                                                    else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                                    {
                                                        _locFpDqty = System.Math.Floor(_locTotQtyFI / _locItemUOM.DefaultConversion);
                                                        _locFpQty = (_locTotQtyFI % _locItemUOM.DefaultConversion) * _locItemUOM.Conversion;
                                                    }
                                                    else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                                    {
                                                        _locFpDqty = _locTotQtyFI;
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                _locFpDqty = _locTotQtyFI;
                                            }

                                            _locSalesInvoiceFreeItemCmp.NFpDQty = _locFpDqty;
                                            _locSalesInvoiceFreeItemCmp.NFpDUOM = _locSalesInvoiceFreeItemCmp.FpDUOM;
                                            _locSalesInvoiceFreeItemCmp.NFpQty = _locFpQty;
                                            _locSalesInvoiceFreeItemCmp.NFpUOM = _locSalesInvoiceFreeItemCmp.FpUOM;
                                            _locSalesInvoiceFreeItemCmp.NFpTQty = _locTotQtyFI;
                                            _locSalesInvoiceFreeItemCmp.NTUAmount = _locTotQtyFI * _locSalesInvoiceFreeItemCmp.UAmount;
                                            _locSalesInvoiceFreeItemCmp.Save();
                                            _locSalesInvoiceFreeItemCmp.Session.CommitTransaction();
                                        }
                                        #endregion Rule2

                                        //2 * 2 * 2 * 2
                                        #region Rule3
                                        if (_locFreeItemRule.RuleType == RuleType.Rule3)
                                        {
                                            double _locQtyFI = 0;
                                            double _locTotQtyFI = 0;
                                            double _locFpDqty = 0;
                                            double _locFpQty = 0;
                                            int _locMaxLoop = 0;

                                            #region Amount
                                            if (_locFreeItemRule.NetAmountRule == true)
                                            {
                                                if (_locSalesInvoiceCmpXPO.TotAmount >= _locFreeItemRule.NetTotalUAmount)
                                                {
                                                    _locQtyFI = System.Math.Floor(_locSalesInvoiceCmpXPO.TotAmount / _locFreeItemRule.NetTotalUAmount);
                                                }
                                            }
                                            if (_locFreeItemRule.GrossAmountRule == true)
                                            {
                                                if (_locSalesInvoiceCmpXPO.TotUnitAmount >= _locFreeItemRule.GrossTotalUAmount)
                                                {
                                                    _locQtyFI = System.Math.Floor(_locSalesInvoiceCmpXPO.TotUnitAmount / _locFreeItemRule.GrossTotalUAmount);
                                                }
                                            }
                                            #endregion Amount

                                            if (_locFreeItemRule.MaxLoop == 0)
                                            {
                                                _locMaxLoop = (int)_locQtyFI;
                                            }
                                            else
                                            {
                                                if (_locFreeItemRule.MaxLoop > (int)_locQtyFI)
                                                {
                                                    _locMaxLoop = (int)_locQtyFI;
                                                }
                                                else
                                                {
                                                    _locMaxLoop = _locFreeItemRule.MaxLoop;
                                                }
                                            }

                                            for (int i = 1; i <= _locMaxLoop; i++)
                                            {
                                                _locTotQtyFI = _locTotQtyFI * _locFreeItemRule.FpTQty;
                                            }

                                            if (_locFreeItemRule.FpItem != null && _locFreeItemRule.FpDUOM != null && _locFreeItemRule.FpUOM != null)
                                            {
                                                ItemUnitOfMeasure _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                                        (new GroupOperator(GroupOperatorType.And,
                                                                         new BinaryOperator("Item", _locFreeItemRule.FpItem),
                                                                         new BinaryOperator("UOM", _locFreeItemRule.FpUOM),
                                                                         new BinaryOperator("DefaultUOM", _locFreeItemRule.FpDUOM),
                                                                         new BinaryOperator("Active", true)));
                                                if (_locItemUOM != null)
                                                {
                                                    if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                                    {
                                                        _locFpDqty = _locTotQtyFI % _locItemUOM.DefaultConversion;
                                                        _locFpQty = System.Math.Floor(_locTotQtyFI / _locItemUOM.DefaultConversion);
                                                    }
                                                    else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                                    {
                                                        _locFpDqty = System.Math.Floor(_locTotQtyFI / _locItemUOM.DefaultConversion);
                                                        _locFpQty = (_locTotQtyFI % _locItemUOM.DefaultConversion) * _locItemUOM.Conversion;
                                                    }
                                                    else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                                    {
                                                        _locFpDqty = _locTotQtyFI;
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                _locFpDqty = _locTotQtyFI;
                                            }

                                            _locSalesInvoiceFreeItemCmp.NFpDQty = _locFpDqty;
                                            _locSalesInvoiceFreeItemCmp.NFpDUOM = _locSalesInvoiceFreeItemCmp.FpDUOM;
                                            _locSalesInvoiceFreeItemCmp.NFpQty = _locFpQty;
                                            _locSalesInvoiceFreeItemCmp.NFpUOM = _locSalesInvoiceFreeItemCmp.FpUOM;
                                            _locSalesInvoiceFreeItemCmp.NFpTQty = _locTotQtyFI;
                                            _locSalesInvoiceFreeItemCmp.NTUAmount = _locTotQtyFI * _locSalesInvoiceFreeItemCmp.UAmount;
                                            _locSalesInvoiceFreeItemCmp.Save();
                                            _locSalesInvoiceFreeItemCmp.Session.CommitTransaction();

                                        }
                                        #endregion Rule3

                                        //2^(1-1) + 2^(2-1) + 2^(3-1) + 2^(4-1)
                                        #region Rule4
                                        if (_locFreeItemRule.RuleType == RuleType.Rule4)
                                        {
                                            double _locQtyFI = 0;
                                            double _locTotQtyFI = 0;
                                            double _locFpDqty = 0;
                                            double _locFpQty = 0;
                                            int _locMaxLoop = 0;
                                            #region Amount
                                            if (_locFreeItemRule.NetAmountRule == true)
                                            {
                                                if (_locSalesInvoiceCmpXPO.TotAmount >= _locFreeItemRule.NetTotalUAmount)
                                                {
                                                    _locQtyFI = System.Math.Floor(_locSalesInvoiceCmpXPO.TotAmount / _locFreeItemRule.NetTotalUAmount);
                                                }
                                            }
                                            if (_locFreeItemRule.GrossAmountRule == true)
                                            {
                                                if (_locSalesInvoiceCmpXPO.TotUnitAmount >= _locFreeItemRule.GrossTotalUAmount)
                                                {
                                                    _locQtyFI = System.Math.Floor(_locSalesInvoiceCmpXPO.TotUnitAmount / _locFreeItemRule.GrossTotalUAmount);
                                                }
                                            }
                                            #endregion Amount


                                            if (_locFreeItemRule.MaxLoop == 0)
                                            {
                                                _locMaxLoop = (int)_locQtyFI;
                                            }
                                            else
                                            {
                                                if (_locFreeItemRule.MaxLoop > (int)_locQtyFI)
                                                {
                                                    _locMaxLoop = (int)_locQtyFI;
                                                }
                                                else
                                                {
                                                    _locMaxLoop = _locFreeItemRule.MaxLoop;
                                                }
                                            }

                                            for (int i = 1; i <= _locMaxLoop; i++)
                                            {

                                                _locTotQtyFI = _locTotQtyFI + System.Math.Pow(_locFreeItemRule.FpTQty, i - 1);
                                            }

                                            if (_locFreeItemRule.FpItem != null && _locFreeItemRule.FpDUOM != null && _locFreeItemRule.FpUOM != null)
                                            {
                                                ItemUnitOfMeasure _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                                        (new GroupOperator(GroupOperatorType.And,
                                                                         new BinaryOperator("Item", _locFreeItemRule.FpItem),
                                                                         new BinaryOperator("UOM", _locFreeItemRule.FpUOM),
                                                                         new BinaryOperator("DefaultUOM", _locFreeItemRule.FpDUOM),
                                                                         new BinaryOperator("Active", true)));
                                                if (_locItemUOM != null)
                                                {
                                                    if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                                    {
                                                        _locFpDqty = _locTotQtyFI % _locItemUOM.DefaultConversion;
                                                        _locFpQty = System.Math.Floor(_locTotQtyFI / _locItemUOM.DefaultConversion);
                                                    }
                                                    else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                                    {
                                                        _locFpDqty = System.Math.Floor(_locTotQtyFI / _locItemUOM.DefaultConversion);
                                                        _locFpQty = (_locTotQtyFI % _locItemUOM.DefaultConversion) * _locItemUOM.Conversion;
                                                    }
                                                    else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                                    {
                                                        _locFpDqty = _locTotQtyFI;
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                _locFpDqty = _locTotQtyFI;
                                            }

                                            _locSalesInvoiceFreeItemCmp.NFpDQty = _locFpDqty;
                                            _locSalesInvoiceFreeItemCmp.NFpDUOM = _locSalesInvoiceFreeItemCmp.FpDUOM;
                                            _locSalesInvoiceFreeItemCmp.NFpQty = _locFpQty;
                                            _locSalesInvoiceFreeItemCmp.NFpUOM = _locSalesInvoiceFreeItemCmp.FpUOM;
                                            _locSalesInvoiceFreeItemCmp.NFpTQty = _locTotQtyFI;
                                            _locSalesInvoiceFreeItemCmp.NTUAmount = _locTotQtyFI * _locSalesInvoiceFreeItemCmp.UAmount;
                                            _locSalesInvoiceFreeItemCmp.Save();
                                            _locSalesInvoiceFreeItemCmp.Session.CommitTransaction();
                                        }
                                        #endregion Rule4
                                    }
                                }
                                #endregion Header
                                #region Line
                                if (_locFreeItemRule.FreeItemType1 == FreeItemType1.Line)
                                {
                                    XPQuery<SalesInvoiceLineCmp> _salesInvoiceLinesCmpQuery = new XPQuery<SalesInvoiceLineCmp>(_currSession);

                                    var _salesInvoiceLineCmps = from silc in _salesInvoiceLinesCmpQuery
                                                                where (silc.SalesInvoiceCmp == _locSalesInvoiceCmpXPO
                                                                && silc.Select == true
                                                                )
                                                                group silc by silc.Item into g
                                                                select new { Item = g.Key };

                                    if (_salesInvoiceLineCmps != null && _salesInvoiceLineCmps.Count() > 0)
                                    {
                                        foreach (var _salesInvoiceLineCmp in _salesInvoiceLineCmps)
                                        {
                                            #region MultiItem=false
                                            if (_locFreeItemRule.MultiItem == false)
                                            {
                                                if (_locFreeItemRule.MpItem == _salesInvoiceLineCmp.Item)
                                                {
                                                    //1+1+1+1+1
                                                    #region Rule1
                                                    if (_locFreeItemRule.RuleType == RuleType.Rule1)
                                                    {
                                                        XPCollection<SalesInvoiceLineCmp> _locSalesInvoiceLineCmps = new XPCollection<SalesInvoiceLineCmp>
                                                                                                               (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                               new BinaryOperator("SalesInvoiceCmp", _locSalesInvoiceCmpXPO),
                                                                                                               new BinaryOperator("Item", _salesInvoiceLineCmp.Item),
                                                                                                               new BinaryOperator("FreeItemChecked", false),
                                                                                                               new BinaryOperator("Select", true)));

                                                        if (_locSalesInvoiceLineCmps != null && _locSalesInvoiceLineCmps.Count() > 0)
                                                        {
                                                            #region Foreach
                                                            foreach (SalesInvoiceLineCmp _locSalesInvoiceLineCmp in _locSalesInvoiceLineCmps)
                                                            {
                                                                double _locQtyFI = 0;
                                                                double _locTotQtyFI = 0;
                                                                double _locFpDqty = 0;
                                                                double _locFpQty = 0;
                                                                int _locMaxLoop = 0;
                                                                int _locTotMaxLoopVal = 0;

                                                                if (_locSalesInvoiceLineCmp.TQty >= _locFreeItemRule.MpTQty)
                                                                {
                                                                    #region Amount
                                                                    if (_locFreeItemRule.OpenAmount == true)
                                                                    {
                                                                        if (_locFreeItemRule.NetAmountRule == true)
                                                                        {
                                                                            if (_locSalesInvoiceLineCmp.TAmount >= _locFreeItemRule.NetTotalUAmount)
                                                                            {
                                                                                _locQtyFI = System.Math.Floor(_locSalesInvoiceLineCmp.TAmount / _locFreeItemRule.NetTotalUAmount);
                                                                            }
                                                                        }
                                                                        if (_locFreeItemRule.GrossAmountRule == true)
                                                                        {
                                                                            if (_locSalesInvoiceLineCmp.TUAmount >= _locFreeItemRule.GrossTotalUAmount)
                                                                            {
                                                                                _locQtyFI = System.Math.Floor(_locSalesInvoiceLineCmp.TUAmount / _locFreeItemRule.GrossTotalUAmount);
                                                                            }
                                                                        }

                                                                    }
                                                                    #endregion Amount
                                                                    #region Item
                                                                    else
                                                                    {
                                                                        if (_locSalesInvoiceLineCmp.TQty >= _locFreeItemRule.MpTQty)
                                                                        {
                                                                            _locQtyFI = System.Math.Floor(_locSalesInvoiceLineCmp.TQty / _locFreeItemRule.MpTQty);
                                                                        }
                                                                    }
                                                                    #endregion Item

                                                                    if (_locFreeItemRule.MaxLoop == 0)
                                                                    {
                                                                        _locMaxLoop = (int)_locQtyFI;
                                                                    }
                                                                    else
                                                                    {
                                                                        if (_locFreeItemRule.MaxLoop > (int)_locQtyFI)
                                                                        {
                                                                            _locMaxLoop = (int)_locQtyFI;
                                                                        }
                                                                        else
                                                                        {
                                                                            _locMaxLoop = _locFreeItemRule.MaxLoop;
                                                                        }
                                                                    }

                                                                    for (int i = 1; i <= _locMaxLoop; i++)
                                                                    {
                                                                        _locTotMaxLoopVal = _locTotMaxLoopVal + 1;
                                                                    }

                                                                    _locTotQtyFI = _locTotMaxLoopVal * _locFreeItemRule.FpTQty;

                                                                    if (_locFreeItemRule.FpItem != null && _locFreeItemRule.FpDUOM != null && _locFreeItemRule.FpUOM != null)
                                                                    {
                                                                        ItemUnitOfMeasure _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                                                                (new GroupOperator(GroupOperatorType.And,
                                                                                                 new BinaryOperator("Item", _locFreeItemRule.FpItem),
                                                                                                 new BinaryOperator("UOM", _locFreeItemRule.FpUOM),
                                                                                                 new BinaryOperator("DefaultUOM", _locFreeItemRule.FpDUOM),
                                                                                                 new BinaryOperator("Active", true)));
                                                                        if (_locItemUOM != null)
                                                                        {
                                                                            if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                                                            {
                                                                                _locFpDqty = _locTotQtyFI % _locItemUOM.DefaultConversion;
                                                                                _locFpQty = System.Math.Floor(_locTotQtyFI / _locItemUOM.DefaultConversion);
                                                                            }
                                                                            else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                                                            {
                                                                                _locFpDqty = System.Math.Floor(_locTotQtyFI / _locItemUOM.DefaultConversion);
                                                                                _locFpQty = (_locTotQtyFI % _locItemUOM.DefaultConversion) * _locItemUOM.Conversion;
                                                                            }
                                                                            else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                                                            {
                                                                                _locFpDqty = _locTotQtyFI;
                                                                            }
                                                                        }
                                                                    }
                                                                    else
                                                                    {
                                                                        _locFpDqty = _locTotQtyFI;
                                                                    }

                                                                    _locSalesInvoiceFreeItemCmp.NFpDQty = _locFpDqty;
                                                                    _locSalesInvoiceFreeItemCmp.NFpDUOM = _locSalesInvoiceFreeItemCmp.FpDUOM;
                                                                    _locSalesInvoiceFreeItemCmp.NFpQty = _locFpQty;
                                                                    _locSalesInvoiceFreeItemCmp.NFpUOM = _locSalesInvoiceFreeItemCmp.FpUOM;
                                                                    _locSalesInvoiceFreeItemCmp.NFpTQty = _locTotQtyFI;
                                                                    _locSalesInvoiceFreeItemCmp.NTUAmount = _locTotQtyFI * _locSalesInvoiceFreeItemCmp.UAmount;
                                                                    _locSalesInvoiceFreeItemCmp.Save();
                                                                    _locSalesInvoiceFreeItemCmp.Session.CommitTransaction();

                                                                    _locSalesInvoiceLineCmp.FreeItemChecked = true;
                                                                    _locSalesInvoiceLineCmp.Save();
                                                                    _locSalesInvoiceLineCmp.Session.CommitTransaction();

                                                                }
                                                            }
                                                            #endregion Foreach
                                                        }
                                                    }

                                                    #endregion Rule1

                                                    //1, 2+1, 3+1, 4+1
                                                    #region Rule2
                                                    if (_locFreeItemRule.RuleType == RuleType.Rule2)
                                                    {
                                                        XPCollection<SalesInvoiceLineCmp> _locSalesInvoiceLineCmps = new XPCollection<SalesInvoiceLineCmp>
                                                                                                               (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                               new BinaryOperator("SalesInvoiceCmp", _locSalesInvoiceCmpXPO),
                                                                                                               new BinaryOperator("Item", _salesInvoiceLineCmp.Item),
                                                                                                               new BinaryOperator("FreeItemChecked", false),
                                                                                                               new BinaryOperator("Select", true)));

                                                        if (_locSalesInvoiceLineCmps != null && _locSalesInvoiceLineCmps.Count() > 0)
                                                        {

                                                            foreach (SalesInvoiceLineCmp _locSalesInvoiceLineCmp in _locSalesInvoiceLineCmps)
                                                            {
                                                                double _locQtyFI = 0;
                                                                double _locTotQtyFI = 0;
                                                                double _locFpDqty = 0;
                                                                double _locFpQty = 0;
                                                                int _locMaxLoop = 0;

                                                                if (_locSalesInvoiceLineCmp.TQty >= _locFreeItemRule.MpTQty)
                                                                {
                                                                    #region Amount
                                                                    if (_locFreeItemRule.OpenAmount == true)
                                                                    {
                                                                        if (_locFreeItemRule.NetAmountRule == true)
                                                                        {
                                                                            if (_locSalesInvoiceLineCmp.TAmount >= _locFreeItemRule.NetTotalUAmount)
                                                                            {
                                                                                _locQtyFI = System.Math.Floor(_locSalesInvoiceLineCmp.TAmount / _locFreeItemRule.NetTotalUAmount);
                                                                            }
                                                                        }
                                                                        if (_locFreeItemRule.GrossAmountRule == true)
                                                                        {
                                                                            if (_locSalesInvoiceLineCmp.TUAmount >= _locFreeItemRule.GrossTotalUAmount)
                                                                            {
                                                                                _locQtyFI = System.Math.Floor(_locSalesInvoiceLineCmp.TUAmount / _locFreeItemRule.GrossTotalUAmount);
                                                                            }
                                                                        }

                                                                    }
                                                                    #endregion Amount
                                                                    #region Item
                                                                    else
                                                                    {
                                                                        if (_locSalesInvoiceLineCmp.TQty >= _locFreeItemRule.MpTQty)
                                                                        {
                                                                            _locQtyFI = System.Math.Floor(_locSalesInvoiceLineCmp.TQty / _locFreeItemRule.MpTQty);
                                                                        }
                                                                    }
                                                                    #endregion Item

                                                                    if (_locFreeItemRule.MaxLoop == 0)
                                                                    {
                                                                        _locMaxLoop = (int)_locQtyFI;
                                                                    }
                                                                    else
                                                                    {
                                                                        if (_locFreeItemRule.MaxLoop > (int)_locQtyFI)
                                                                        {
                                                                            _locMaxLoop = (int)_locQtyFI;
                                                                        }
                                                                        else
                                                                        {
                                                                            _locMaxLoop = _locFreeItemRule.MaxLoop;
                                                                        }
                                                                    }

                                                                    for (int i = 1; i <= _locMaxLoop; i++)
                                                                    {
                                                                        if (i == 1)
                                                                        {
                                                                            _locTotQtyFI = _locFreeItemRule.FpTQty;
                                                                        }
                                                                        else
                                                                        {
                                                                            _locTotQtyFI = _locTotQtyFI + (i + _locFreeItemRule.FpTQty);
                                                                        }

                                                                    }

                                                                    if (_locFreeItemRule.FpItem != null && _locFreeItemRule.FpDUOM != null && _locFreeItemRule.FpUOM != null)
                                                                    {
                                                                        ItemUnitOfMeasure _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                                                                (new GroupOperator(GroupOperatorType.And,
                                                                                                 new BinaryOperator("Item", _locFreeItemRule.FpItem),
                                                                                                 new BinaryOperator("UOM", _locFreeItemRule.FpUOM),
                                                                                                 new BinaryOperator("DefaultUOM", _locFreeItemRule.FpDUOM),
                                                                                                 new BinaryOperator("Active", true)));
                                                                        if (_locItemUOM != null)
                                                                        {
                                                                            if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                                                            {
                                                                                _locFpDqty = _locTotQtyFI % _locItemUOM.DefaultConversion;
                                                                                _locFpQty = System.Math.Floor(_locTotQtyFI / _locItemUOM.DefaultConversion);
                                                                            }
                                                                            else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                                                            {
                                                                                _locFpDqty = System.Math.Floor(_locTotQtyFI / _locItemUOM.DefaultConversion);
                                                                                _locFpQty = (_locTotQtyFI % _locItemUOM.DefaultConversion) * _locItemUOM.Conversion;
                                                                            }
                                                                            else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                                                            {
                                                                                _locFpDqty = _locTotQtyFI;
                                                                            }
                                                                        }
                                                                    }
                                                                    else
                                                                    {
                                                                        _locFpDqty = _locTotQtyFI;
                                                                    }

                                                                    _locSalesInvoiceFreeItemCmp.NFpDQty = _locFpDqty;
                                                                    _locSalesInvoiceFreeItemCmp.NFpDUOM = _locSalesInvoiceFreeItemCmp.FpDUOM;
                                                                    _locSalesInvoiceFreeItemCmp.NFpQty = _locFpQty;
                                                                    _locSalesInvoiceFreeItemCmp.NFpUOM = _locSalesInvoiceFreeItemCmp.FpUOM;
                                                                    _locSalesInvoiceFreeItemCmp.NFpTQty = _locTotQtyFI;
                                                                    _locSalesInvoiceFreeItemCmp.NTUAmount = _locTotQtyFI * _locSalesInvoiceFreeItemCmp.UAmount;
                                                                    _locSalesInvoiceFreeItemCmp.Save();
                                                                    _locSalesInvoiceFreeItemCmp.Session.CommitTransaction();

                                                                    _locSalesInvoiceLineCmp.FreeItemChecked = true;
                                                                    _locSalesInvoiceLineCmp.Save();
                                                                    _locSalesInvoiceLineCmp.Session.CommitTransaction();

                                                                }
                                                            }
                                                        }
                                                    }
                                                    #endregion Rule2

                                                    //2 * 2 * 2 * 2
                                                    #region Rule3
                                                    if (_locFreeItemRule.RuleType == RuleType.Rule3)
                                                    {
                                                        XPCollection<SalesInvoiceLineCmp> _locSalesInvoiceLineCmps = new XPCollection<SalesInvoiceLineCmp>
                                                                                                               (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                               new BinaryOperator("SalesInvoiceCmp", _locSalesInvoiceCmpXPO),
                                                                                                               new BinaryOperator("Item", _salesInvoiceLineCmp.Item),
                                                                                                               new BinaryOperator("FreeItemChecked", false),
                                                                                                               new BinaryOperator("Select", true)));

                                                        if (_locSalesInvoiceLineCmps != null && _locSalesInvoiceLineCmps.Count() > 0)
                                                        {

                                                            foreach (SalesInvoiceLineCmp _locSalesInvoiceLineCmp in _locSalesInvoiceLineCmps)
                                                            {
                                                                double _locQtyFI = 0;
                                                                double _locTotQtyFI = 0;
                                                                double _locFpDqty = 0;
                                                                double _locFpQty = 0;
                                                                int _locMaxLoop = 0;

                                                                if (_locSalesInvoiceLineCmp.TQty >= _locFreeItemRule.MpTQty)
                                                                {
                                                                    #region Amount
                                                                    if (_locFreeItemRule.OpenAmount == true)
                                                                    {
                                                                        if (_locFreeItemRule.NetAmountRule == true)
                                                                        {
                                                                            if (_locSalesInvoiceLineCmp.TAmount >= _locFreeItemRule.NetTotalUAmount)
                                                                            {
                                                                                _locQtyFI = System.Math.Floor(_locSalesInvoiceLineCmp.TAmount / _locFreeItemRule.NetTotalUAmount);
                                                                            }
                                                                        }
                                                                        if (_locFreeItemRule.GrossAmountRule == true)
                                                                        {
                                                                            if (_locSalesInvoiceLineCmp.TUAmount >= _locFreeItemRule.GrossTotalUAmount)
                                                                            {
                                                                                _locQtyFI = System.Math.Floor(_locSalesInvoiceLineCmp.TUAmount / _locFreeItemRule.GrossTotalUAmount);
                                                                            }
                                                                        }

                                                                    }
                                                                    #endregion Amount
                                                                    #region Item
                                                                    else
                                                                    {
                                                                        if (_locSalesInvoiceLineCmp.TQty >= _locFreeItemRule.MpTQty)
                                                                        {
                                                                            _locQtyFI = System.Math.Floor(_locSalesInvoiceLineCmp.TQty / _locFreeItemRule.MpTQty);
                                                                        }
                                                                    }
                                                                    #endregion Item

                                                                    if (_locFreeItemRule.MaxLoop == 0)
                                                                    {
                                                                        _locMaxLoop = (int)_locQtyFI;
                                                                    }
                                                                    else
                                                                    {
                                                                        if (_locFreeItemRule.MaxLoop > (int)_locQtyFI)
                                                                        {
                                                                            _locMaxLoop = (int)_locQtyFI;
                                                                        }
                                                                        else
                                                                        {
                                                                            _locMaxLoop = _locFreeItemRule.MaxLoop;
                                                                        }
                                                                    }

                                                                    for (int i = 1; i <= _locMaxLoop; i++)
                                                                    {
                                                                        _locTotQtyFI = _locTotQtyFI * _locFreeItemRule.FpTQty;
                                                                    }

                                                                    if (_locFreeItemRule.FpItem != null && _locFreeItemRule.FpDUOM != null && _locFreeItemRule.FpUOM != null)
                                                                    {
                                                                        ItemUnitOfMeasure _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                                                                (new GroupOperator(GroupOperatorType.And,
                                                                                                 new BinaryOperator("Item", _locFreeItemRule.FpItem),
                                                                                                 new BinaryOperator("UOM", _locFreeItemRule.FpUOM),
                                                                                                 new BinaryOperator("DefaultUOM", _locFreeItemRule.FpDUOM),
                                                                                                 new BinaryOperator("Active", true)));
                                                                        if (_locItemUOM != null)
                                                                        {
                                                                            if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                                                            {
                                                                                _locFpDqty = _locTotQtyFI % _locItemUOM.DefaultConversion;
                                                                                _locFpQty = System.Math.Floor(_locTotQtyFI / _locItemUOM.DefaultConversion);
                                                                            }
                                                                            else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                                                            {
                                                                                _locFpDqty = System.Math.Floor(_locTotQtyFI / _locItemUOM.DefaultConversion);
                                                                                _locFpQty = (_locTotQtyFI % _locItemUOM.DefaultConversion) * _locItemUOM.Conversion;
                                                                            }
                                                                            else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                                                            {
                                                                                _locFpDqty = _locTotQtyFI;
                                                                            }
                                                                        }
                                                                    }
                                                                    else
                                                                    {
                                                                        _locFpDqty = _locTotQtyFI;
                                                                    }

                                                                    _locSalesInvoiceFreeItemCmp.NFpDQty = _locFpDqty;
                                                                    _locSalesInvoiceFreeItemCmp.NFpDUOM = _locSalesInvoiceFreeItemCmp.FpDUOM;
                                                                    _locSalesInvoiceFreeItemCmp.NFpQty = _locFpQty;
                                                                    _locSalesInvoiceFreeItemCmp.NFpUOM = _locSalesInvoiceFreeItemCmp.FpUOM;
                                                                    _locSalesInvoiceFreeItemCmp.NFpTQty = _locTotQtyFI;
                                                                    _locSalesInvoiceFreeItemCmp.NTUAmount = _locTotQtyFI * _locSalesInvoiceFreeItemCmp.UAmount;
                                                                    _locSalesInvoiceFreeItemCmp.Save();
                                                                    _locSalesInvoiceFreeItemCmp.Session.CommitTransaction();

                                                                    _locSalesInvoiceLineCmp.FreeItemChecked = true;
                                                                    _locSalesInvoiceLineCmp.Save();
                                                                    _locSalesInvoiceLineCmp.Session.CommitTransaction();

                                                                }
                                                            }
                                                        }
                                                    }
                                                    #endregion Rule3

                                                    //2^(1-1) + 2^(2-1) + 2^(3-1) + 2^(4-1)
                                                    #region Rule4
                                                    if (_locFreeItemRule.RuleType == RuleType.Rule4)
                                                    {
                                                        XPCollection<SalesInvoiceLineCmp> _locSalesInvoiceLineCmps = new XPCollection<SalesInvoiceLineCmp>
                                                                                                               (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                               new BinaryOperator("SalesInvoiceCmp", _locSalesInvoiceCmpXPO),
                                                                                                               new BinaryOperator("Item", _salesInvoiceLineCmp.Item),
                                                                                                               new BinaryOperator("Status", Status.Progress),
                                                                                                               new BinaryOperator("FreeItemChecked", false),
                                                                                                               new BinaryOperator("Select", true)));

                                                        if (_locSalesInvoiceLineCmps != null && _locSalesInvoiceLineCmps.Count() > 0)
                                                        {

                                                            foreach (SalesInvoiceLineCmp _locSalesInvoiceLineCmp in _locSalesInvoiceLineCmps)
                                                            {
                                                                double _locQtyFI = 0;
                                                                double _locTotQtyFI = 0;
                                                                double _locFpDqty = 0;
                                                                double _locFpQty = 0;
                                                                int _locMaxLoop = 0;

                                                                if (_locSalesInvoiceLineCmp.TQty >= _locFreeItemRule.MpTQty)
                                                                {
                                                                    #region Amount
                                                                    if (_locFreeItemRule.OpenAmount == true)
                                                                    {
                                                                        if (_locFreeItemRule.NetAmountRule == true)
                                                                        {
                                                                            if (_locSalesInvoiceLineCmp.TAmount >= _locFreeItemRule.NetTotalUAmount)
                                                                            {
                                                                                _locQtyFI = System.Math.Floor(_locSalesInvoiceLineCmp.TAmount / _locFreeItemRule.NetTotalUAmount);
                                                                            }
                                                                        }
                                                                        if (_locFreeItemRule.GrossAmountRule == true)
                                                                        {
                                                                            if (_locSalesInvoiceLineCmp.TUAmount >= _locFreeItemRule.GrossTotalUAmount)
                                                                            {
                                                                                _locQtyFI = System.Math.Floor(_locSalesInvoiceLineCmp.TUAmount / _locFreeItemRule.GrossTotalUAmount);
                                                                            }
                                                                        }

                                                                    }
                                                                    #endregion Amount
                                                                    #region Item
                                                                    else
                                                                    {
                                                                        if (_locSalesInvoiceLineCmp.TQty >= _locFreeItemRule.MpTQty)
                                                                        {
                                                                            _locQtyFI = System.Math.Floor(_locSalesInvoiceLineCmp.TQty / _locFreeItemRule.MpTQty);
                                                                        }
                                                                    }
                                                                    #endregion Item

                                                                    if (_locFreeItemRule.MaxLoop == 0)
                                                                    {
                                                                        _locMaxLoop = (int)_locQtyFI;
                                                                    }
                                                                    else
                                                                    {
                                                                        if (_locFreeItemRule.MaxLoop > (int)_locQtyFI)
                                                                        {
                                                                            _locMaxLoop = (int)_locQtyFI;
                                                                        }
                                                                        else
                                                                        {
                                                                            _locMaxLoop = _locFreeItemRule.MaxLoop;
                                                                        }
                                                                    }

                                                                    for (int i = 1; i <= _locMaxLoop; i++)
                                                                    {

                                                                        _locTotQtyFI = _locTotQtyFI + System.Math.Pow(_locFreeItemRule.FpTQty, i - 1);
                                                                    }

                                                                    if (_locFreeItemRule.FpItem != null && _locFreeItemRule.FpDUOM != null && _locFreeItemRule.FpUOM != null)
                                                                    {
                                                                        ItemUnitOfMeasure _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                                                                (new GroupOperator(GroupOperatorType.And,
                                                                                                 new BinaryOperator("Item", _locFreeItemRule.FpItem),
                                                                                                 new BinaryOperator("UOM", _locFreeItemRule.FpUOM),
                                                                                                 new BinaryOperator("DefaultUOM", _locFreeItemRule.FpDUOM),
                                                                                                 new BinaryOperator("Active", true)));
                                                                        if (_locItemUOM != null)
                                                                        {
                                                                            if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                                                            {
                                                                                _locFpDqty = _locTotQtyFI % _locItemUOM.DefaultConversion;
                                                                                _locFpQty = System.Math.Floor(_locTotQtyFI / _locItemUOM.DefaultConversion);
                                                                            }
                                                                            else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                                                            {
                                                                                _locFpDqty = System.Math.Floor(_locTotQtyFI / _locItemUOM.DefaultConversion);
                                                                                _locFpQty = (_locTotQtyFI % _locItemUOM.DefaultConversion) * _locItemUOM.Conversion;
                                                                            }
                                                                            else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                                                            {
                                                                                _locFpDqty = _locTotQtyFI;
                                                                            }
                                                                        }
                                                                    }
                                                                    else
                                                                    {
                                                                        _locFpDqty = _locTotQtyFI;
                                                                    }

                                                                    _locSalesInvoiceFreeItemCmp.NFpDQty = _locFpDqty;
                                                                    _locSalesInvoiceFreeItemCmp.NFpDUOM = _locSalesInvoiceFreeItemCmp.FpDUOM;
                                                                    _locSalesInvoiceFreeItemCmp.NFpQty = _locFpQty;
                                                                    _locSalesInvoiceFreeItemCmp.NFpUOM = _locSalesInvoiceFreeItemCmp.FpUOM;
                                                                    _locSalesInvoiceFreeItemCmp.NFpTQty = _locTotQtyFI;
                                                                    _locSalesInvoiceFreeItemCmp.NTUAmount = _locTotQtyFI * _locSalesInvoiceFreeItemCmp.UAmount;
                                                                    _locSalesInvoiceFreeItemCmp.Save();
                                                                    _locSalesInvoiceFreeItemCmp.Session.CommitTransaction();

                                                                    _locSalesInvoiceLineCmp.FreeItemChecked = true;
                                                                    _locSalesInvoiceLineCmp.Save();
                                                                    _locSalesInvoiceLineCmp.Session.CommitTransaction();

                                                                }
                                                            }
                                                        }
                                                    }
                                                    #endregion Rule4
                                                }
                                            }
                                            #endregion MultiItem=false
                                            #region MultiItem=true
                                            else
                                            {
                                                SalesInvoiceLineCmp _locComboSalesInvoiceLineCmp = _currSession.FindObject<SalesInvoiceLineCmp>
                                                                                                            (new GroupOperator(GroupOperatorType.And,
                                                                                                            new BinaryOperator("SalesInvoice", _locSalesInvoiceCmpXPO),
                                                                                                            new BinaryOperator("Item", _locFreeItemRule.CpItem),
                                                                                                            new BinaryOperator("FreeItemChecked", false),
                                                                                                            new BinaryOperator("Select", true)));

                                                SalesInvoiceLineCmp _locMainSalesInvoiceLineCmp = _currSession.FindObject<SalesInvoiceLineCmp>
                                                                                                           (new GroupOperator(GroupOperatorType.And,
                                                                                                           new BinaryOperator("SalesInvoice", _locSalesInvoiceCmpXPO),
                                                                                                           new BinaryOperator("Item", _locFreeItemRule.MpItem),
                                                                                                           new BinaryOperator("FreeItemChecked", false),
                                                                                                           new BinaryOperator("Select", true)));

                                                if (_locMainSalesInvoiceLineCmp != null && _locComboSalesInvoiceLineCmp != null)
                                                {
                                                    double _locComboQtyCI = 0;
                                                    double _locComboTotQtyFI = 0;
                                                    double _locComboCpDqty = 0;
                                                    double _locComboCpQty = 0;
                                                    int _locComboMaxLoop = 0;
                                                    int _locComboTotMaxLoopVal = 0;

                                                    double _locMainQtyFI = 0;
                                                    double _locTotQtyFI = 0;
                                                    double _locFpDqty = 0;
                                                    double _locFpQty = 0;
                                                    int _locMaxLoop = 0;
                                                    int _locTotMaxLoopVal = 0;

                                                    if (_locMainSalesInvoiceLineCmp.TQty >= _locFreeItemRule.MpTQty && _locComboSalesInvoiceLineCmp.TQty >= _locFreeItemRule.CpTQty)
                                                    {
                                                        _locMainQtyFI = System.Math.Floor(_locMainSalesInvoiceLineCmp.TQty / _locFreeItemRule.MpTQty);
                                                        _locComboQtyCI = System.Math.Floor(_locComboSalesInvoiceLineCmp.TQty / _locFreeItemRule.CpTQty);

                                                        if (_locComboQtyCI < _locMainQtyFI)
                                                        {
                                                            #region ComboPromoItem

                                                            //1+1+1+1+1
                                                            #region Rule1
                                                            if (_locFreeItemRule.RuleType == RuleType.Rule1)
                                                            {

                                                                if (_locFreeItemRule.MaxLoop == 0)
                                                                {
                                                                    _locComboMaxLoop = (int)_locComboQtyCI;
                                                                }
                                                                else
                                                                {
                                                                    if (_locFreeItemRule.MaxLoop > (int)_locComboQtyCI)
                                                                    {
                                                                        _locComboMaxLoop = (int)_locComboQtyCI;
                                                                    }
                                                                    else
                                                                    {
                                                                        _locComboMaxLoop = _locFreeItemRule.MaxLoop;
                                                                    }
                                                                }

                                                                for (int i = 1; i <= _locComboMaxLoop; i++)
                                                                {
                                                                    _locComboTotMaxLoopVal = _locComboTotMaxLoopVal + 1;
                                                                }

                                                                _locComboTotQtyFI = _locComboTotMaxLoopVal * _locFreeItemRule.FpTQty;

                                                                if (_locFreeItemRule.FpItem != null && _locFreeItemRule.FpDUOM != null && _locFreeItemRule.FpUOM != null)
                                                                {
                                                                    ItemUnitOfMeasure _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                                                            (new GroupOperator(GroupOperatorType.And,
                                                                                             new BinaryOperator("Item", _locFreeItemRule.FpItem),
                                                                                             new BinaryOperator("UOM", _locFreeItemRule.FpUOM),
                                                                                             new BinaryOperator("DefaultUOM", _locFreeItemRule.FpDUOM),
                                                                                             new BinaryOperator("Active", true)));
                                                                    if (_locItemUOM != null)
                                                                    {
                                                                        if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                                                        {
                                                                            _locComboCpDqty = _locComboTotQtyFI % _locItemUOM.DefaultConversion;
                                                                            _locComboCpQty = System.Math.Floor(_locComboTotQtyFI / _locItemUOM.DefaultConversion);
                                                                        }
                                                                        else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                                                        {
                                                                            _locComboCpDqty = System.Math.Floor(_locComboTotQtyFI / _locItemUOM.DefaultConversion);
                                                                            _locComboCpQty = (_locComboTotQtyFI % _locItemUOM.DefaultConversion) * _locItemUOM.Conversion;
                                                                        }
                                                                        else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                                                        {
                                                                            _locComboCpDqty = _locComboTotQtyFI;
                                                                        }
                                                                    }
                                                                }
                                                                else
                                                                {
                                                                    _locComboCpDqty = _locComboTotQtyFI;
                                                                }

                                                                _locSalesInvoiceFreeItemCmp.NFpDQty = _locComboCpDqty;
                                                                _locSalesInvoiceFreeItemCmp.NFpDUOM = _locSalesInvoiceFreeItemCmp.FpDUOM;
                                                                _locSalesInvoiceFreeItemCmp.NFpQty = _locComboCpQty;
                                                                _locSalesInvoiceFreeItemCmp.NFpUOM = _locSalesInvoiceFreeItemCmp.FpUOM;
                                                                _locSalesInvoiceFreeItemCmp.NFpTQty = _locComboTotQtyFI;
                                                                _locSalesInvoiceFreeItemCmp.NTUAmount = _locComboTotQtyFI * _locSalesInvoiceFreeItemCmp.UAmount;
                                                                _locSalesInvoiceFreeItemCmp.Save();
                                                                _locSalesInvoiceFreeItemCmp.Session.CommitTransaction();

                                                                _locComboSalesInvoiceLineCmp.FreeItemChecked = true;
                                                                _locComboSalesInvoiceLineCmp.Save();
                                                                _locComboSalesInvoiceLineCmp.Session.CommitTransaction();

                                                                _locMainSalesInvoiceLineCmp.FreeItemChecked = true;
                                                                _locMainSalesInvoiceLineCmp.Save();
                                                                _locMainSalesInvoiceLineCmp.Session.CommitTransaction();

                                                            }

                                                            #endregion Rule1

                                                            //1, 2+1, 3+1, 4+1
                                                            #region Rule2

                                                            if (_locFreeItemRule.RuleType == RuleType.Rule2)
                                                            {
                                                                if (_locFreeItemRule.MaxLoop == 0)
                                                                {
                                                                    _locComboMaxLoop = (int)_locComboQtyCI;
                                                                }
                                                                else
                                                                {
                                                                    if (_locFreeItemRule.MaxLoop > (int)_locComboQtyCI)
                                                                    {
                                                                        _locComboMaxLoop = (int)_locComboQtyCI;
                                                                    }
                                                                    else
                                                                    {
                                                                        _locComboMaxLoop = _locFreeItemRule.MaxLoop;
                                                                    }
                                                                }

                                                                for (int i = 1; i <= _locComboMaxLoop; i++)
                                                                {
                                                                    if (i == 1)
                                                                    {
                                                                        _locComboTotQtyFI = _locFreeItemRule.FpTQty;
                                                                    }
                                                                    else
                                                                    {
                                                                        _locComboTotQtyFI = _locComboTotQtyFI + (i + _locFreeItemRule.FpTQty);
                                                                    }
                                                                }

                                                                if (_locFreeItemRule.FpItem != null && _locFreeItemRule.FpDUOM != null && _locFreeItemRule.FpUOM != null)
                                                                {
                                                                    ItemUnitOfMeasure _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                                                            (new GroupOperator(GroupOperatorType.And,
                                                                                             new BinaryOperator("Item", _locFreeItemRule.FpItem),
                                                                                             new BinaryOperator("UOM", _locFreeItemRule.FpUOM),
                                                                                             new BinaryOperator("DefaultUOM", _locFreeItemRule.FpDUOM),
                                                                                             new BinaryOperator("Active", true)));
                                                                    if (_locItemUOM != null)
                                                                    {
                                                                        if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                                                        {
                                                                            _locComboCpDqty = _locComboTotQtyFI % _locItemUOM.DefaultConversion;
                                                                            _locComboCpQty = System.Math.Floor(_locComboTotQtyFI / _locItemUOM.DefaultConversion);
                                                                        }
                                                                        else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                                                        {
                                                                            _locComboCpDqty = System.Math.Floor(_locComboTotQtyFI / _locItemUOM.DefaultConversion);
                                                                            _locComboCpQty = (_locComboTotQtyFI % _locItemUOM.DefaultConversion) * _locItemUOM.Conversion;
                                                                        }
                                                                        else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                                                        {
                                                                            _locComboCpDqty = _locComboTotQtyFI;
                                                                        }
                                                                    }
                                                                }
                                                                else
                                                                {
                                                                    _locComboCpDqty = _locComboTotQtyFI;
                                                                }

                                                                _locSalesInvoiceFreeItemCmp.NFpDQty = _locComboCpDqty;
                                                                _locSalesInvoiceFreeItemCmp.NFpDUOM = _locSalesInvoiceFreeItemCmp.FpDUOM;
                                                                _locSalesInvoiceFreeItemCmp.NFpQty = _locComboCpQty;
                                                                _locSalesInvoiceFreeItemCmp.NFpUOM = _locSalesInvoiceFreeItemCmp.FpUOM;
                                                                _locSalesInvoiceFreeItemCmp.NFpTQty = _locComboTotQtyFI;
                                                                _locSalesInvoiceFreeItemCmp.NTUAmount = _locComboTotQtyFI * _locSalesInvoiceFreeItemCmp.UAmount;
                                                                _locSalesInvoiceFreeItemCmp.Save();
                                                                _locSalesInvoiceFreeItemCmp.Session.CommitTransaction();

                                                                _locComboSalesInvoiceLineCmp.FreeItemChecked = true;
                                                                _locComboSalesInvoiceLineCmp.Save();
                                                                _locComboSalesInvoiceLineCmp.Session.CommitTransaction();

                                                                _locMainSalesInvoiceLineCmp.FreeItemChecked = true;
                                                                _locMainSalesInvoiceLineCmp.Save();
                                                                _locMainSalesInvoiceLineCmp.Session.CommitTransaction();

                                                            }
                                                            #endregion Rule2

                                                            //2 * 2 * 2 * 2
                                                            #region Rule3

                                                            if (_locFreeItemRule.RuleType == RuleType.Rule3)
                                                            {

                                                                if (_locFreeItemRule.MaxLoop == 0)
                                                                {
                                                                    _locComboMaxLoop = (int)_locComboQtyCI;
                                                                }
                                                                else
                                                                {
                                                                    if (_locFreeItemRule.MaxLoop > (int)_locComboQtyCI)
                                                                    {
                                                                        _locComboMaxLoop = (int)_locComboQtyCI;
                                                                    }
                                                                    else
                                                                    {
                                                                        _locComboMaxLoop = _locFreeItemRule.MaxLoop;
                                                                    }
                                                                }

                                                                for (int i = 1; i <= _locComboMaxLoop; i++)
                                                                {
                                                                    _locComboTotQtyFI = _locComboTotQtyFI * _locFreeItemRule.FpTQty;
                                                                }

                                                                if (_locFreeItemRule.FpItem != null && _locFreeItemRule.FpDUOM != null && _locFreeItemRule.FpUOM != null)
                                                                {
                                                                    ItemUnitOfMeasure _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                                                            (new GroupOperator(GroupOperatorType.And,
                                                                                             new BinaryOperator("Item", _locFreeItemRule.FpItem),
                                                                                             new BinaryOperator("UOM", _locFreeItemRule.FpUOM),
                                                                                             new BinaryOperator("DefaultUOM", _locFreeItemRule.FpDUOM),
                                                                                             new BinaryOperator("Active", true)));
                                                                    if (_locItemUOM != null)
                                                                    {
                                                                        if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                                                        {
                                                                            _locComboCpDqty = _locComboTotQtyFI % _locItemUOM.DefaultConversion;
                                                                            _locComboCpQty = System.Math.Floor(_locComboTotQtyFI / _locItemUOM.DefaultConversion);
                                                                        }
                                                                        else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                                                        {
                                                                            _locComboCpDqty = System.Math.Floor(_locComboTotQtyFI / _locItemUOM.DefaultConversion);
                                                                            _locComboCpQty = (_locComboTotQtyFI % _locItemUOM.DefaultConversion) * _locItemUOM.Conversion;
                                                                        }
                                                                        else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                                                        {
                                                                            _locComboCpDqty = _locComboTotQtyFI;
                                                                        }
                                                                    }
                                                                }
                                                                else
                                                                {
                                                                    _locComboCpDqty = _locComboTotQtyFI;
                                                                }

                                                                _locSalesInvoiceFreeItemCmp.NFpDQty = _locComboCpDqty;
                                                                _locSalesInvoiceFreeItemCmp.NFpDUOM = _locSalesInvoiceFreeItemCmp.FpDUOM;
                                                                _locSalesInvoiceFreeItemCmp.NFpQty = _locComboCpQty;
                                                                _locSalesInvoiceFreeItemCmp.NFpUOM = _locSalesInvoiceFreeItemCmp.FpUOM;
                                                                _locSalesInvoiceFreeItemCmp.NFpTQty = _locComboTotQtyFI;
                                                                _locSalesInvoiceFreeItemCmp.NTUAmount = _locComboTotQtyFI * _locSalesInvoiceFreeItemCmp.UAmount;
                                                                _locSalesInvoiceFreeItemCmp.Save();
                                                                _locSalesInvoiceFreeItemCmp.Session.CommitTransaction();

                                                                _locComboSalesInvoiceLineCmp.FreeItemChecked = true;
                                                                _locComboSalesInvoiceLineCmp.Save();
                                                                _locComboSalesInvoiceLineCmp.Session.CommitTransaction();

                                                                _locMainSalesInvoiceLineCmp.FreeItemChecked = true;
                                                                _locMainSalesInvoiceLineCmp.Save();
                                                                _locMainSalesInvoiceLineCmp.Session.CommitTransaction();

                                                            }
                                                            #endregion Rule3

                                                            //2^(1-1) + 2^(2-1) + 2^(3-1) + 2^(4-1)
                                                            #region Rule4

                                                            if (_locFreeItemRule.RuleType == RuleType.Rule4)
                                                            {
                                                                if (_locFreeItemRule.MaxLoop == 0)
                                                                {
                                                                    _locComboMaxLoop = (int)_locComboQtyCI;
                                                                }
                                                                else
                                                                {
                                                                    if (_locFreeItemRule.MaxLoop > (int)_locComboQtyCI)
                                                                    {
                                                                        _locComboMaxLoop = (int)_locComboQtyCI;
                                                                    }
                                                                    else
                                                                    {
                                                                        _locComboMaxLoop = _locFreeItemRule.MaxLoop;
                                                                    }
                                                                }

                                                                for (int i = 1; i <= _locComboMaxLoop; i++)
                                                                {
                                                                    _locComboTotQtyFI = _locComboTotQtyFI + System.Math.Pow(_locFreeItemRule.FpTQty, i - 1);
                                                                }

                                                                if (_locFreeItemRule.FpItem != null && _locFreeItemRule.FpDUOM != null && _locFreeItemRule.FpUOM != null)
                                                                {
                                                                    ItemUnitOfMeasure _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                                                            (new GroupOperator(GroupOperatorType.And,
                                                                                             new BinaryOperator("Item", _locFreeItemRule.FpItem),
                                                                                             new BinaryOperator("UOM", _locFreeItemRule.FpUOM),
                                                                                             new BinaryOperator("DefaultUOM", _locFreeItemRule.FpDUOM),
                                                                                             new BinaryOperator("Active", true)));
                                                                    if (_locItemUOM != null)
                                                                    {
                                                                        if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                                                        {
                                                                            _locComboCpDqty = _locComboTotQtyFI % _locItemUOM.DefaultConversion;
                                                                            _locComboCpQty = System.Math.Floor(_locComboTotQtyFI / _locItemUOM.DefaultConversion);
                                                                        }
                                                                        else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                                                        {
                                                                            _locComboCpDqty = System.Math.Floor(_locComboTotQtyFI / _locItemUOM.DefaultConversion);
                                                                            _locComboCpQty = (_locComboTotQtyFI % _locItemUOM.DefaultConversion) * _locItemUOM.Conversion;
                                                                        }
                                                                        else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                                                        {
                                                                            _locComboCpDqty = _locComboTotQtyFI;
                                                                        }
                                                                    }
                                                                }
                                                                else
                                                                {
                                                                    _locComboCpDqty = _locComboTotQtyFI;
                                                                }

                                                                _locSalesInvoiceFreeItemCmp.NFpDQty = _locComboCpDqty;
                                                                _locSalesInvoiceFreeItemCmp.NFpDUOM = _locSalesInvoiceFreeItemCmp.FpDUOM;
                                                                _locSalesInvoiceFreeItemCmp.NFpQty = _locComboCpQty;
                                                                _locSalesInvoiceFreeItemCmp.NFpUOM = _locSalesInvoiceFreeItemCmp.FpUOM;
                                                                _locSalesInvoiceFreeItemCmp.NFpTQty = _locComboTotQtyFI;
                                                                _locSalesInvoiceFreeItemCmp.NTUAmount = _locComboTotQtyFI * _locSalesInvoiceFreeItemCmp.UAmount;
                                                                _locSalesInvoiceFreeItemCmp.Save();
                                                                _locSalesInvoiceFreeItemCmp.Session.CommitTransaction();

                                                                _locComboSalesInvoiceLineCmp.FreeItemChecked = true;
                                                                _locComboSalesInvoiceLineCmp.Save();
                                                                _locComboSalesInvoiceLineCmp.Session.CommitTransaction();

                                                                _locMainSalesInvoiceLineCmp.FreeItemChecked = true;
                                                                _locMainSalesInvoiceLineCmp.Save();
                                                                _locMainSalesInvoiceLineCmp.Session.CommitTransaction();
                                                            }
                                                            #endregion Rule4

                                                            #endregion ComboPromoItem
                                                        }
                                                        else
                                                        {
                                                            #region MainPromoItem

                                                            #region Rule1

                                                            if (_locFreeItemRule.RuleType == RuleType.Rule1)
                                                            {

                                                                if (_locFreeItemRule.MaxLoop == 0)
                                                                {
                                                                    _locMaxLoop = (int)_locMainQtyFI;
                                                                }
                                                                else
                                                                {
                                                                    if (_locFreeItemRule.MaxLoop > (int)_locMainQtyFI)
                                                                    {
                                                                        _locMaxLoop = (int)_locMainQtyFI;
                                                                    }
                                                                    else
                                                                    {
                                                                        _locMaxLoop = _locFreeItemRule.MaxLoop;
                                                                    }
                                                                }

                                                                for (int i = 1; i <= _locMaxLoop; i++)
                                                                {
                                                                    _locTotMaxLoopVal = _locTotMaxLoopVal + 1;
                                                                }

                                                                _locTotQtyFI = _locTotMaxLoopVal * _locFreeItemRule.FpTQty;

                                                                if (_locFreeItemRule.FpItem != null && _locFreeItemRule.FpDUOM != null && _locFreeItemRule.FpUOM != null)
                                                                {
                                                                    ItemUnitOfMeasure _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                                                            (new GroupOperator(GroupOperatorType.And,
                                                                                             new BinaryOperator("Item", _locFreeItemRule.FpItem),
                                                                                             new BinaryOperator("UOM", _locFreeItemRule.FpUOM),
                                                                                             new BinaryOperator("DefaultUOM", _locFreeItemRule.FpDUOM),
                                                                                             new BinaryOperator("Active", true)));
                                                                    if (_locItemUOM != null)
                                                                    {
                                                                        if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                                                        {
                                                                            _locFpDqty = _locTotQtyFI % _locItemUOM.DefaultConversion;
                                                                            _locFpQty = System.Math.Floor(_locTotQtyFI / _locItemUOM.DefaultConversion);
                                                                        }
                                                                        else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                                                        {
                                                                            _locFpDqty = System.Math.Floor(_locTotQtyFI / _locItemUOM.DefaultConversion);
                                                                            _locFpQty = (_locTotQtyFI % _locItemUOM.DefaultConversion) * _locItemUOM.Conversion;
                                                                        }
                                                                        else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                                                        {
                                                                            _locFpDqty = _locTotQtyFI;
                                                                        }
                                                                    }
                                                                }
                                                                else
                                                                {
                                                                    _locFpDqty = _locTotQtyFI;
                                                                }

                                                                _locSalesInvoiceFreeItemCmp.NFpDQty = _locFpDqty;
                                                                _locSalesInvoiceFreeItemCmp.NFpDUOM = _locSalesInvoiceFreeItemCmp.FpDUOM;
                                                                _locSalesInvoiceFreeItemCmp.NFpQty = _locFpQty;
                                                                _locSalesInvoiceFreeItemCmp.NFpUOM = _locSalesInvoiceFreeItemCmp.FpUOM;
                                                                _locSalesInvoiceFreeItemCmp.NFpTQty = _locTotQtyFI;
                                                                _locSalesInvoiceFreeItemCmp.NTUAmount = _locTotQtyFI * _locSalesInvoiceFreeItemCmp.UAmount;
                                                                _locSalesInvoiceFreeItemCmp.Save();
                                                                _locSalesInvoiceFreeItemCmp.Session.CommitTransaction();

                                                                _locComboSalesInvoiceLineCmp.FreeItemChecked = true;
                                                                _locComboSalesInvoiceLineCmp.Save();
                                                                _locComboSalesInvoiceLineCmp.Session.CommitTransaction();

                                                                _locMainSalesInvoiceLineCmp.FreeItemChecked = true;
                                                                _locMainSalesInvoiceLineCmp.Save();
                                                                _locMainSalesInvoiceLineCmp.Session.CommitTransaction();

                                                            }

                                                            #endregion Rule1

                                                            //1, 2+1, 3+1, 4+1
                                                            #region Rule2
                                                            if (_locFreeItemRule.RuleType == RuleType.Rule2)
                                                            {

                                                                if (_locFreeItemRule.MaxLoop == 0)
                                                                {
                                                                    _locMaxLoop = (int)_locMainQtyFI;
                                                                }
                                                                else
                                                                {
                                                                    if (_locFreeItemRule.MaxLoop > (int)_locMainQtyFI)
                                                                    {
                                                                        _locMaxLoop = (int)_locMainQtyFI;
                                                                    }
                                                                    else
                                                                    {
                                                                        _locMaxLoop = _locFreeItemRule.MaxLoop;
                                                                    }
                                                                }

                                                                for (int i = 1; i <= _locMaxLoop; i++)
                                                                {

                                                                    if (i == 1)
                                                                    {
                                                                        _locTotQtyFI = _locFreeItemRule.FpTQty;
                                                                    }
                                                                    else
                                                                    {
                                                                        _locTotQtyFI = _locTotQtyFI + (i + _locFreeItemRule.FpTQty);
                                                                    }

                                                                }

                                                                if (_locFreeItemRule.FpItem != null && _locFreeItemRule.FpDUOM != null && _locFreeItemRule.FpUOM != null)
                                                                {
                                                                    ItemUnitOfMeasure _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                                                            (new GroupOperator(GroupOperatorType.And,
                                                                                             new BinaryOperator("Item", _locFreeItemRule.FpItem),
                                                                                             new BinaryOperator("UOM", _locFreeItemRule.FpUOM),
                                                                                             new BinaryOperator("DefaultUOM", _locFreeItemRule.FpDUOM),
                                                                                             new BinaryOperator("Active", true)));
                                                                    if (_locItemUOM != null)
                                                                    {
                                                                        if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                                                        {
                                                                            _locFpDqty = _locTotQtyFI % _locItemUOM.DefaultConversion;
                                                                            _locFpQty = System.Math.Floor(_locTotQtyFI / _locItemUOM.DefaultConversion);
                                                                        }
                                                                        else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                                                        {
                                                                            _locFpDqty = System.Math.Floor(_locTotQtyFI / _locItemUOM.DefaultConversion);
                                                                            _locFpQty = (_locTotQtyFI % _locItemUOM.DefaultConversion) * _locItemUOM.Conversion;
                                                                        }
                                                                        else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                                                        {
                                                                            _locFpDqty = _locTotQtyFI;
                                                                        }
                                                                    }
                                                                }
                                                                else
                                                                {
                                                                    _locFpDqty = _locTotQtyFI;
                                                                }

                                                                _locSalesInvoiceFreeItemCmp.NFpDQty = _locFpDqty;
                                                                _locSalesInvoiceFreeItemCmp.NFpDUOM = _locSalesInvoiceFreeItemCmp.FpDUOM;
                                                                _locSalesInvoiceFreeItemCmp.NFpQty = _locFpQty;
                                                                _locSalesInvoiceFreeItemCmp.NFpUOM = _locSalesInvoiceFreeItemCmp.FpUOM;
                                                                _locSalesInvoiceFreeItemCmp.NFpTQty = _locTotQtyFI;
                                                                _locSalesInvoiceFreeItemCmp.NTUAmount = _locTotQtyFI * _locSalesInvoiceFreeItemCmp.UAmount;
                                                                _locSalesInvoiceFreeItemCmp.Save();
                                                                _locSalesInvoiceFreeItemCmp.Session.CommitTransaction();

                                                                _locComboSalesInvoiceLineCmp.FreeItemChecked = true;
                                                                _locComboSalesInvoiceLineCmp.Save();
                                                                _locComboSalesInvoiceLineCmp.Session.CommitTransaction();

                                                                _locMainSalesInvoiceLineCmp.FreeItemChecked = true;
                                                                _locMainSalesInvoiceLineCmp.Save();
                                                                _locMainSalesInvoiceLineCmp.Session.CommitTransaction();

                                                            }
                                                            #endregion Rule2

                                                            //2 * 2 * 2 * 2
                                                            #region Rule3
                                                            if (_locFreeItemRule.RuleType == RuleType.Rule3)
                                                            {

                                                                if (_locFreeItemRule.MaxLoop == 0)
                                                                {
                                                                    _locMaxLoop = (int)_locMainQtyFI;
                                                                }
                                                                else
                                                                {
                                                                    if (_locFreeItemRule.MaxLoop > (int)_locMainQtyFI)
                                                                    {
                                                                        _locMaxLoop = (int)_locMainQtyFI;
                                                                    }
                                                                    else
                                                                    {
                                                                        _locMaxLoop = _locFreeItemRule.MaxLoop;
                                                                    }
                                                                }

                                                                for (int i = 1; i <= _locMaxLoop; i++)
                                                                {
                                                                    _locTotQtyFI = _locTotQtyFI * _locFreeItemRule.FpTQty;
                                                                }

                                                                if (_locFreeItemRule.FpItem != null && _locFreeItemRule.FpDUOM != null && _locFreeItemRule.FpUOM != null)
                                                                {
                                                                    ItemUnitOfMeasure _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                                                            (new GroupOperator(GroupOperatorType.And,
                                                                                             new BinaryOperator("Item", _locFreeItemRule.FpItem),
                                                                                             new BinaryOperator("UOM", _locFreeItemRule.FpUOM),
                                                                                             new BinaryOperator("DefaultUOM", _locFreeItemRule.FpDUOM),
                                                                                             new BinaryOperator("Active", true)));
                                                                    if (_locItemUOM != null)
                                                                    {
                                                                        if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                                                        {
                                                                            _locFpDqty = _locTotQtyFI % _locItemUOM.DefaultConversion;
                                                                            _locFpQty = System.Math.Floor(_locTotQtyFI / _locItemUOM.DefaultConversion);
                                                                        }
                                                                        else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                                                        {
                                                                            _locFpDqty = System.Math.Floor(_locTotQtyFI / _locItemUOM.DefaultConversion);
                                                                            _locFpQty = (_locTotQtyFI % _locItemUOM.DefaultConversion) * _locItemUOM.Conversion;
                                                                        }
                                                                        else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                                                        {
                                                                            _locFpDqty = _locTotQtyFI;
                                                                        }
                                                                    }
                                                                }
                                                                else
                                                                {
                                                                    _locFpDqty = _locTotQtyFI;
                                                                }

                                                                _locSalesInvoiceFreeItemCmp.NFpDQty = _locFpDqty;
                                                                _locSalesInvoiceFreeItemCmp.NFpDUOM = _locSalesInvoiceFreeItemCmp.FpDUOM;
                                                                _locSalesInvoiceFreeItemCmp.NFpQty = _locFpQty;
                                                                _locSalesInvoiceFreeItemCmp.NFpUOM = _locSalesInvoiceFreeItemCmp.FpUOM;
                                                                _locSalesInvoiceFreeItemCmp.NFpTQty = _locTotQtyFI;
                                                                _locSalesInvoiceFreeItemCmp.NTUAmount = _locTotQtyFI * _locSalesInvoiceFreeItemCmp.UAmount;
                                                                _locSalesInvoiceFreeItemCmp.Save();
                                                                _locSalesInvoiceFreeItemCmp.Session.CommitTransaction();

                                                                _locComboSalesInvoiceLineCmp.FreeItemChecked = true;
                                                                _locComboSalesInvoiceLineCmp.Save();
                                                                _locComboSalesInvoiceLineCmp.Session.CommitTransaction();

                                                                _locMainSalesInvoiceLineCmp.FreeItemChecked = true;
                                                                _locMainSalesInvoiceLineCmp.Save();
                                                                _locMainSalesInvoiceLineCmp.Session.CommitTransaction();

                                                            }
                                                            #endregion Rule3

                                                            //2^(1-1) + 2^(2-1) + 2^(3-1) + 2^(4-1)
                                                            #region Rule4

                                                            if (_locFreeItemRule.RuleType == RuleType.Rule4)
                                                            {
                                                                if (_locFreeItemRule.MaxLoop == 0)
                                                                {
                                                                    _locMaxLoop = (int)_locMainQtyFI;
                                                                }
                                                                else
                                                                {
                                                                    if (_locFreeItemRule.MaxLoop > (int)_locMainQtyFI)
                                                                    {
                                                                        _locMaxLoop = (int)_locMainQtyFI;
                                                                    }
                                                                    else
                                                                    {
                                                                        _locMaxLoop = _locFreeItemRule.MaxLoop;
                                                                    }
                                                                }

                                                                for (int i = 1; i <= _locMaxLoop; i++)
                                                                {
                                                                    _locTotQtyFI = _locTotQtyFI + System.Math.Pow(_locFreeItemRule.FpTQty, i - 1);
                                                                }

                                                                if (_locFreeItemRule.FpItem != null && _locFreeItemRule.FpDUOM != null && _locFreeItemRule.FpUOM != null)
                                                                {
                                                                    ItemUnitOfMeasure _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                                                            (new GroupOperator(GroupOperatorType.And,
                                                                                             new BinaryOperator("Item", _locFreeItemRule.FpItem),
                                                                                             new BinaryOperator("UOM", _locFreeItemRule.FpUOM),
                                                                                             new BinaryOperator("DefaultUOM", _locFreeItemRule.FpDUOM),
                                                                                             new BinaryOperator("Active", true)));
                                                                    if (_locItemUOM != null)
                                                                    {
                                                                        if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                                                        {
                                                                            _locFpDqty = _locTotQtyFI % _locItemUOM.DefaultConversion;
                                                                            _locFpQty = System.Math.Floor(_locTotQtyFI / _locItemUOM.DefaultConversion);
                                                                        }
                                                                        else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                                                        {
                                                                            _locFpDqty = System.Math.Floor(_locTotQtyFI / _locItemUOM.DefaultConversion);
                                                                            _locFpQty = (_locTotQtyFI % _locItemUOM.DefaultConversion) * _locItemUOM.Conversion;
                                                                        }
                                                                        else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                                                        {
                                                                            _locFpDqty = _locTotQtyFI;
                                                                        }
                                                                    }
                                                                }
                                                                else
                                                                {
                                                                    _locFpDqty = _locTotQtyFI;
                                                                }

                                                                _locSalesInvoiceFreeItemCmp.NFpDQty = _locFpDqty;
                                                                _locSalesInvoiceFreeItemCmp.NFpDUOM = _locSalesInvoiceFreeItemCmp.FpDUOM;
                                                                _locSalesInvoiceFreeItemCmp.NFpQty = _locFpQty;
                                                                _locSalesInvoiceFreeItemCmp.NFpUOM = _locSalesInvoiceFreeItemCmp.FpUOM;
                                                                _locSalesInvoiceFreeItemCmp.NFpTQty = _locTotQtyFI;
                                                                _locSalesInvoiceFreeItemCmp.NTUAmount = _locTotQtyFI * _locSalesInvoiceFreeItemCmp.UAmount;
                                                                _locSalesInvoiceFreeItemCmp.Save();
                                                                _locSalesInvoiceFreeItemCmp.Session.CommitTransaction();

                                                                _locComboSalesInvoiceLineCmp.FreeItemChecked = true;
                                                                _locComboSalesInvoiceLineCmp.Save();
                                                                _locComboSalesInvoiceLineCmp.Session.CommitTransaction();

                                                                _locMainSalesInvoiceLineCmp.FreeItemChecked = true;
                                                                _locMainSalesInvoiceLineCmp.Save();
                                                                _locMainSalesInvoiceLineCmp.Session.CommitTransaction();

                                                            }
                                                            #endregion Rule4

                                                            #endregion MainPromoItem
                                                        }
                                                    }

                                                }
                                            }
                                            #endregion MultiItem=true

                                        }
                                    }
                                }
                                #endregion Line
                            }


                        }
                    }

                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = SalesInvoiceCmp ", ex.ToString());
            }
        }

        private void SetTotalFreeItemAmount(Session _currSession, SalesInvoiceCmp _locSalesInvoiceCmpXPO)
        {
            try
            {
                double _locTUAmount = 0;
                double _locNTUAmount = 0;
                double _locGetTUAmount = 0;
                double _locGetNTUAmount = 0;
                if (_locSalesInvoiceCmpXPO != null)
                {
                    XPCollection<SalesInvoiceFreeItemCmp> _locSalesInvoiceFreeItemCmps = new XPCollection<SalesInvoiceFreeItemCmp>
                                                                                (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                new BinaryOperator("SalesInvoiceCmp", _locSalesInvoiceCmpXPO)));
                    if (_locSalesInvoiceFreeItemCmps != null && _locSalesInvoiceFreeItemCmps.Count() > 0)
                    {
                        foreach (SalesInvoiceFreeItemCmp _locSalesInvoiceFreeItemCmp in _locSalesInvoiceFreeItemCmps)
                        {
                            _locGetTUAmount = GetTotalUnitAmount(_currSession, _locSalesInvoiceFreeItemCmp.FpTQty, _locSalesInvoiceFreeItemCmp.UAmount);
                            _locGetNTUAmount = GetTotalUnitAmount(_currSession, _locSalesInvoiceFreeItemCmp.NFpTQty, _locSalesInvoiceFreeItemCmp.UAmount);
                            _locTUAmount = _locTUAmount + _locGetTUAmount;
                            _locNTUAmount = _locNTUAmount + _locGetNTUAmount;

                            
                            _locSalesInvoiceFreeItemCmp.TUAmount = _locGetTUAmount;
                            _locSalesInvoiceFreeItemCmp.NTUAmount = _locGetNTUAmount;
                            _locSalesInvoiceFreeItemCmp.Save();
                            _locSalesInvoiceFreeItemCmp.Session.CommitTransaction();
                        }

                        
                        _locSalesInvoiceCmpXPO.TotFIAmount = _locTUAmount;
                        _locSalesInvoiceCmpXPO.TotNFIAmount = _locNTUAmount;
                        _locSalesInvoiceCmpXPO.Save();
                        _locSalesInvoiceCmpXPO.Session.CommitTransaction();
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = SalesInvoiceCmp ", ex.ToString());
            }
        }

        private void SetReleaseBalHold(Session _currSession, SalesInvoiceCmp _locSalesInvoiceCmpXPO)
        {
            try
            {
                double _locDeviationAmountDiscHeader = 0;
                double _locDeviationAmountDiscLine = 0;
                if (_locSalesInvoiceCmpXPO != null)
                {
                    #region Header
                    if(_locSalesInvoiceCmpXPO.AccountingPeriodicLine != null)
                    {
                        _locDeviationAmountDiscHeader = _locSalesInvoiceCmpXPO.OldAmountDisc - _locSalesInvoiceCmpXPO.AmountDisc;
                        if(_locDeviationAmountDiscHeader > 0)
                        {
                            if(_locSalesInvoiceCmpXPO.AccountingPeriodicLine.BalHold < _locDeviationAmountDiscHeader)
                            {
                                _locDeviationAmountDiscHeader = _locSalesInvoiceCmpXPO.AccountingPeriodicLine.BalHold;
                            }
                            _locSalesInvoiceCmpXPO.AccountingPeriodicLine.BalHold = _locSalesInvoiceCmpXPO.AccountingPeriodicLine.BalHold - _locDeviationAmountDiscHeader;
                            _locSalesInvoiceCmpXPO.AccountingPeriodicLine.Save();
                            _locSalesInvoiceCmpXPO.AccountingPeriodicLine.Session.CommitTransaction();
                        }
                    }
                    #endregion Header
                    #region Line
                    XPCollection<SalesInvoiceLineCmp> _locSalesInvoiceLineCmps = new XPCollection<SalesInvoiceLineCmp>(_currSession, 
                                                                                new GroupOperator(GroupOperatorType.And,
                                                                                new BinaryOperator("SalesInvoiceCmp", _locSalesInvoiceCmpXPO)));
                    if(_locSalesInvoiceLineCmps != null && _locSalesInvoiceLineCmps.Count() > 0)
                    {
                        foreach(SalesInvoiceLineCmp _locSalesInvoiceLineCmp in _locSalesInvoiceLineCmps)
                        {
                            if(_locSalesInvoiceLineCmp.AccountingPeriodicLine != null)
                            {
                                _locDeviationAmountDiscLine = _locSalesInvoiceLineCmp.OldDiscAmount - _locSalesInvoiceLineCmp.DiscAmount;
                                if (_locDeviationAmountDiscLine > 0)
                                {
                                    if(_locSalesInvoiceLineCmp.AccountingPeriodicLine.BalHold < _locDeviationAmountDiscLine)
                                    {
                                        _locDeviationAmountDiscLine = _locSalesInvoiceLineCmp.AccountingPeriodicLine.BalHold;
                                    }
                                    _locSalesInvoiceLineCmp.AccountingPeriodicLine.BalHold = _locSalesInvoiceLineCmp.AccountingPeriodicLine.BalHold - _locDeviationAmountDiscLine;
                                    _locSalesInvoiceLineCmp.AccountingPeriodicLine.Save();
                                    _locSalesInvoiceLineCmp.AccountingPeriodicLine.Session.CommitTransaction();
                                }
                            }
                            _locDeviationAmountDiscLine = 0;
                        }
                    }
                    #endregion Line
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = SalesInvoiceCmp ", ex.ToString());
            }
        }

        #endregion EditInvoice

        #region GetAmount

        private double GetTotalUnitAmount(Session _currSession, double _locFpTQty, double _locUAmount)
        {
            double _result = 0;
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();

                if (_locFpTQty >= 0 & _locUAmount >= 0)
                {
                    if (_globFunc.GetRoundingList(_currSession, ObjectList.SalesInvoiceFreeItemCmp, FieldName.TUAmount) == true)
                    {
                        _result = _globFunc.GetRoundUp(_currSession, (_locFpTQty * _locUAmount), ObjectList.SalesInvoiceFreeItemCmp, FieldName.TUAmount);
                    }
                    else
                    {
                        _result = _locFpTQty * _locUAmount;
                    }

                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = SalesInvoice " + ex.ToString());
            }
            return _result;
        }

        #endregion GetAmount

        #region Discount

        private double GetDiscountRule1(Session _currSession, SalesInvoiceCmp _locSalesInvoiceCmpXPO)
        {
            double _result = 0;
            try
            {
                DiscountRule _locDiscountRule1 = null;
                
                if (_locSalesInvoiceCmpXPO != null)
                {
                    if (_locSalesInvoiceCmpXPO.Company != null && _locSalesInvoiceCmpXPO.Workplace != null)
                    {
                        
                        if (_locSalesInvoiceCmpXPO.DiscountRule != null)
                        {
                            _locDiscountRule1 = _locSalesInvoiceCmpXPO.DiscountRule;
                            
                        }
                        else
                        {
                            DiscountRule _locDiscountRule1a = _currSession.FindObject<DiscountRule>
                                                            (new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("Company", _locSalesInvoiceCmpXPO.Company),
                                                            new BinaryOperator("Workplace", _locSalesInvoiceCmpXPO.Workplace),
                                                            new BinaryOperator("PriceGroup", _locSalesInvoiceCmpXPO.PriceGroup),
                                                            new BinaryOperator("RuleType", RuleType.Rule1),
                                                            new BinaryOperator("Active", true)));
                            if (_locDiscountRule1a != null)
                            {
                                _locDiscountRule1 = _locDiscountRule1a;
                            }
                        }

                        #region Rule1

                        //Grand Total Amount Per Invoice
                        if (_locDiscountRule1 != null)
                        {
                            double _locTUAmount = 0;
                            double _locTAmount = 0;
                            double _locTUAmountDiscount = 0;

                            #region Gross
                            if (_locDiscountRule1.GrossAmountRule == true)
                            {
                                XPCollection<SalesInvoiceLineCmp> _locMainSalesInvoiceLineCmps = new XPCollection<SalesInvoiceLineCmp>
                                                                                               (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                               new BinaryOperator("SalesInvoice", _locSalesInvoiceCmpXPO),
                                                                                               new BinaryOperator("Select", true),
                                                                                               new GroupOperator(GroupOperatorType.Or,
                                                                                               new BinaryOperator("Status", Status.Open))));


                                if (_locMainSalesInvoiceLineCmps != null && _locMainSalesInvoiceLineCmps.Count() > 0)
                                {
                                    foreach (SalesInvoiceLineCmp _locMainSalesInvoiceLineCmp in _locMainSalesInvoiceLineCmps)
                                    {
                                        if (_locMainSalesInvoiceLineCmp.TUAmount > 0)
                                        {
                                            _locTUAmount = _locTUAmount + _locMainSalesInvoiceLineCmp.TUAmount;
                                            _locMainSalesInvoiceLineCmp.DiscountChecked = true;
                                            _locMainSalesInvoiceLineCmp.Save();
                                            _locMainSalesInvoiceLineCmp.Session.CommitTransaction();
                                        }
                                    }

                                    if (_locTUAmount > 0 && _locTUAmount >= _locDiscountRule1.GrossTotalUAmount)
                                    {
                                        if (_locDiscountRule1.DiscountType == DiscountType.Percentage)
                                        {
                                            _locTUAmountDiscount = _locTUAmount * _locDiscountRule1.Value / 100;
                                        }
                                        if (_locDiscountRule1.DiscountType == DiscountType.Value)
                                        {
                                            _locTUAmountDiscount = _locDiscountRule1.Value;
                                        }
                                    }
                                }

                            }
                            #endregion Gross
                            #region Net
                            if (_locDiscountRule1.NetAmountRule == true)
                            {
                                XPCollection<SalesInvoiceLineCmp> _locMainSalesInvoiceLineCmps = new XPCollection<SalesInvoiceLineCmp>
                                                                                               (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                               new BinaryOperator("SalesInvoice", _locSalesInvoiceCmpXPO),
                                                                                               new BinaryOperator("Select", true),
                                                                                               new GroupOperator(GroupOperatorType.Or,
                                                                                               new BinaryOperator("Status", Status.Open))));

                                if (_locMainSalesInvoiceLineCmps != null && _locMainSalesInvoiceLineCmps.Count() > 0)
                                {
                                    foreach (SalesInvoiceLineCmp _locMainSalesInvoiceLineCmp in _locMainSalesInvoiceLineCmps)
                                    {
                                        if (_locMainSalesInvoiceLineCmp.TAmount > 0)
                                        {
                                            _locTAmount = _locTAmount + _locMainSalesInvoiceLineCmp.TAmount;
                                            _locMainSalesInvoiceLineCmp.DiscountChecked = true;
                                            _locMainSalesInvoiceLineCmp.Save();
                                            _locMainSalesInvoiceLineCmp.Session.CommitTransaction();
                                        }
                                    }

                                    if (_locTAmount > 0 && _locTAmount >= _locDiscountRule1.NetTotalUAmount)
                                    {
                                        if (_locDiscountRule1.DiscountType == DiscountType.Percentage)
                                        {
                                            _locTUAmountDiscount = _locTAmount * _locDiscountRule1.Value / 100;
                                        }
                                        if (_locDiscountRule1.DiscountType == DiscountType.Value)
                                        {
                                            _locTUAmountDiscount = _locDiscountRule1.Value;
                                        }
                                    }
                                }
                            }
                            #endregion Net

                            _result = _locTUAmountDiscount;
                        }
                        #endregion Rule1
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = SalesInvoiceCmp ", ex.ToString());
            }
            return _result;
        }

        #endregion Discount

        #region TaxAndDiscountInLine

        private void SetTaxBeforeDiscount(Session _currSession, SalesInvoiceLineCmp _locSalesInvoiceLineCmpXPO)
        {
            try
            {
                double _locTxAmount = 0;
                double _locTAmount = 0;
                if (_locSalesInvoiceLineCmpXPO != null)
                {
                    if (_locSalesInvoiceLineCmpXPO.Tax != null && _locSalesInvoiceLineCmpXPO.TaxChecked == false)
                    {
                        if (_locSalesInvoiceLineCmpXPO.Tax.TaxRule == TaxRule.BeforeDiscount)
                        {
                            _locTxAmount = _locSalesInvoiceLineCmpXPO.TxValue / 100 * _locSalesInvoiceLineCmpXPO.TUAmount;
                            if (_locSalesInvoiceLineCmpXPO.Tax.TaxNature == TaxNature.Increase)
                            {
                                _locTAmount = _locSalesInvoiceLineCmpXPO.TAmount + _locTxAmount;
                            }
                            if (_locSalesInvoiceLineCmpXPO.Tax.TaxNature == TaxNature.Decrease)
                            {
                                _locTAmount = _locSalesInvoiceLineCmpXPO.TAmount - _locTxAmount;
                            }
                            _locSalesInvoiceLineCmpXPO.TxAmount = _locTxAmount;
                            _locSalesInvoiceLineCmpXPO.TAmount = _locTAmount;
                            _locSalesInvoiceLineCmpXPO.TaxChecked = true;
                            _locSalesInvoiceLineCmpXPO.Save();
                            _locSalesInvoiceLineCmpXPO.Session.CommitTransaction();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("BusinessObject = SalesInvoiceLine" + ex.ToString());
            }
        }

        private void SetDiscountGross(Session _currSession, SalesInvoiceLineCmp _locSalesInvoiceLineCmpXPO)
        {
            try
            {
                if (_locSalesInvoiceLineCmpXPO != null)
                {
                    if (_locSalesInvoiceLineCmpXPO.DiscountRule != null)
                    {
                        double _locTUAmountDiscount = 0;

                        #region Gross

                        if (_locSalesInvoiceLineCmpXPO.DiscountRule.GrossAmountRule == true)
                        {
                            if (_locSalesInvoiceLineCmpXPO.TUAmount > 0 && _locSalesInvoiceLineCmpXPO.TUAmount >= _locSalesInvoiceLineCmpXPO.DiscountRule.GrossTotalUAmount)
                            {
                                if (_locSalesInvoiceLineCmpXPO.DiscountRule.DiscountType == DiscountType.Percentage)
                                {
                                    _locTUAmountDiscount = _locSalesInvoiceLineCmpXPO.TUAmount * _locSalesInvoiceLineCmpXPO.DiscountRule.Value / 100;
                                }
                                if (_locSalesInvoiceLineCmpXPO.DiscountRule.DiscountType == DiscountType.Value)
                                {
                                    _locTUAmountDiscount = _locSalesInvoiceLineCmpXPO.DiscountRule.Value;
                                }

                                _locSalesInvoiceLineCmpXPO.DiscountChecked = true;
                                _locSalesInvoiceLineCmpXPO.DiscAmount = _locTUAmountDiscount;
                                _locSalesInvoiceLineCmpXPO.TAmount = _locSalesInvoiceLineCmpXPO.TAmount - _locTUAmountDiscount;
                                _locSalesInvoiceLineCmpXPO.Save();
                                _locSalesInvoiceLineCmpXPO.Session.CommitTransaction();
                            }

                        }

                        #endregion Gross
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("BusinessObject = SalesInvoiceLine" + ex.ToString());
            }
        }

        private void SetTaxAfterDiscount(Session _currSession, SalesInvoiceLineCmp _locSalesInvoiceLineCmpXPO)
        {
            try
            {
                double _locTxAmount = 0;
                double _locTAmount = 0;
                if (_locSalesInvoiceLineCmpXPO != null)
                {
                    if (_locSalesInvoiceLineCmpXPO.Tax != null)
                    {
                        if (_locSalesInvoiceLineCmpXPO.DiscountRule != null)
                        {
                            if (_locSalesInvoiceLineCmpXPO.Tax.TaxRule == TaxRule.AfterDiscount && _locSalesInvoiceLineCmpXPO.DiscountRule.GrossAmountRule == true)
                            {
                                _locTxAmount = _locSalesInvoiceLineCmpXPO.TxValue / 100 * (_locSalesInvoiceLineCmpXPO.TUAmount - _locSalesInvoiceLineCmpXPO.DiscAmount);

                                if (_locSalesInvoiceLineCmpXPO.Tax.TaxNature == TaxNature.Increase)
                                {
                                    _locTAmount = _locSalesInvoiceLineCmpXPO.TAmount + _locTxAmount;
                                }
                                if (_locSalesInvoiceLineCmpXPO.Tax.TaxNature == TaxNature.Decrease)
                                {
                                    _locTAmount = _locSalesInvoiceLineCmpXPO.TAmount - _locTxAmount;
                                }
                                _locSalesInvoiceLineCmpXPO.TxAmount = _locTxAmount;
                                _locSalesInvoiceLineCmpXPO.TAmount = _locTAmount;
                                _locSalesInvoiceLineCmpXPO.Save();
                                _locSalesInvoiceLineCmpXPO.Session.CommitTransaction();
                            }
                        }
                        else
                        {
                            if (_locSalesInvoiceLineCmpXPO.Tax.TaxRule == TaxRule.AfterDiscount)
                            {
                                _locTxAmount = _locSalesInvoiceLineCmpXPO.TxValue / 100 * (_locSalesInvoiceLineCmpXPO.TUAmount);

                                if (_locSalesInvoiceLineCmpXPO.Tax.TaxNature == TaxNature.Increase)
                                {
                                    _locTAmount = _locSalesInvoiceLineCmpXPO.TAmount + _locTxAmount;
                                }
                                if (_locSalesInvoiceLineCmpXPO.Tax.TaxNature == TaxNature.Decrease)
                                {
                                    _locTAmount = _locSalesInvoiceLineCmpXPO.TAmount - _locTxAmount;
                                }
                                _locSalesInvoiceLineCmpXPO.TxAmount = _locTxAmount;
                                _locSalesInvoiceLineCmpXPO.TAmount = _locTAmount;
                                _locSalesInvoiceLineCmpXPO.Save();
                                _locSalesInvoiceLineCmpXPO.Session.CommitTransaction();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("BusinessObject = SalesInvoiceLine" + ex.ToString());
            }
        }

        private void SetDiscountNet(Session _currSession, SalesInvoiceLineCmp _locSalesInvoiceLineCmpXPO)
        {
            try
            {
                if (_locSalesInvoiceLineCmpXPO != null)
                {
                    if (_locSalesInvoiceLineCmpXPO.DiscountRule != null && _locSalesInvoiceLineCmpXPO.Tax != null)
                    {
                        double _locTUAmountDiscount = 0;

                        #region Net
                        if (_locSalesInvoiceLineCmpXPO.DiscountRule.NetAmountRule == true && _locSalesInvoiceLineCmpXPO.Tax.TaxRule == TaxRule.BeforeDiscount)
                        {
                            if (_locSalesInvoiceLineCmpXPO.TAmount > 0 && _locSalesInvoiceLineCmpXPO.TAmount >= _locSalesInvoiceLineCmpXPO.DiscountRule.NetTotalUAmount)
                            {
                                if (_locSalesInvoiceLineCmpXPO.DiscountRule.DiscountType == DiscountType.Percentage)
                                {
                                    _locTUAmountDiscount = _locSalesInvoiceLineCmpXPO.TAmount * _locSalesInvoiceLineCmpXPO.DiscountRule.Value / 100;
                                }
                                if (_locSalesInvoiceLineCmpXPO.DiscountRule.DiscountType == DiscountType.Value)
                                {
                                    _locTUAmountDiscount = _locSalesInvoiceLineCmpXPO.DiscountRule.Value;
                                }

                                _locSalesInvoiceLineCmpXPO.DiscountChecked = true;
                                _locSalesInvoiceLineCmpXPO.DiscAmount = _locTUAmountDiscount;
                                _locSalesInvoiceLineCmpXPO.TAmount = _locSalesInvoiceLineCmpXPO.TAmount - _locTUAmountDiscount;
                                _locSalesInvoiceLineCmpXPO.Save();
                                _locSalesInvoiceLineCmpXPO.Session.CommitTransaction();
                            }

                        }
                        #endregion Net
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("BusinessObject = SalesInvoiceLine" + ex.ToString());
            }
        }

        #endregion TaxAndDiscountInLine

        #region Global Method

        private void SuccessMessageShow(string _locMessage)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 4000;
            options.Message = string.Format("{0}", _locMessage);
            options.Type = InformationType.Success;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Success";
            options.Win.Type = WinMessageType.Flyout;
            Application.ShowViewStrategy.ShowMessage(options);
        }

        private void ErrorMessageShow(string _locMessage)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 2000;
            options.Message = string.Format(_locMessage);
            options.Type = InformationType.Warning;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Error";
            options.Win.Type = WinMessageType.Alert;
            Application.ShowViewStrategy.ShowMessage(options);
        }

        #endregion Global Method
    }
}
