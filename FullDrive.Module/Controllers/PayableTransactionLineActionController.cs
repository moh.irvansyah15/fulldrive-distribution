﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;

using FullDrive.Module.BusinessObjects;
using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.Xpo;
using System.Collections;
using DevExpress.Xpo;

namespace FullDrive.Module.Controllers
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class PayableTransactionLineActionController : ViewController
    {
        private ChoiceActionItem _selectionListviewFilter;

        public PayableTransactionLineActionController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.
            #region FilterStatus
            PayableTransactionLineListviewFilterSelectionAction.Items.Clear();
            foreach (object _currApproval in Enum.GetValues(typeof(CustomProcess.Status)))
            {
                EnumDescriptor _ed = new EnumDescriptor(typeof(CustomProcess.Status));
                if (_ed.GetCaption(_currApproval) != "Approved" && _ed.GetCaption(_currApproval) != "Lock")
                {
                    _selectionListviewFilter = new ChoiceActionItem(_ed.GetCaption(_currApproval), _currApproval);
                }
                PayableTransactionLineListviewFilterSelectionAction.Items.Add(_selectionListviewFilter);
            }
            #endregion FilterStatus
        }

        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
        }

        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }

        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        private void PayableTransactionLineExchangeRatesAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;

                if (_objectsToProcess != null)
                {
                    foreach (Object obj in _objectsToProcess)
                    {
                        PayableTransactionLine _locPayableTransactionLineOS = (PayableTransactionLine)_objectSpace.GetObject(obj);

                        if (_locPayableTransactionLineOS != null)
                        {
                            if(_locPayableTransactionLineOS.Session != null)
                            {
                                _currSession = _locPayableTransactionLineOS.Session;
                            }

                            if (_locPayableTransactionLineOS.Debit != 0 && _locPayableTransactionLineOS.Credit != 0)
                            {
                                if (_locPayableTransactionLineOS.Code != null && _currSession != null)
                                {
                                    _currObjectId = _locPayableTransactionLineOS.Code;

                                    PayableTransactionLine _locPayableTransactionLineXPO = _currSession.FindObject<PayableTransactionLine>
                                                                                           (new GroupOperator(GroupOperatorType.And,
                                                                                            new BinaryOperator("Code", _currObjectId)));

                                    if (_locPayableTransactionLineXPO != null)
                                    {
                                        GetChangeRates(_currSession, _locPayableTransactionLineOS);
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("BusinessObject = PayableTransactionLine" + ex.ToString());
            }
        }

        private void PayableTransactionLineSelectAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;

                if (_objectToProcess != null)
                {
                    foreach (Object obj in _objectToProcess)
                    {
                        PayableTransactionLine _locPayableTransactionLineOS = (PayableTransactionLine)_objectSpace.GetObject(obj);

                        if (_locPayableTransactionLineOS != null)
                        {
                            if (_locPayableTransactionLineOS.Session != null)
                            {
                                _currSession = _locPayableTransactionLineOS.Session;
                            }

                            if (_locPayableTransactionLineOS.Code != null && _currSession != null)
                            {
                                _currObjectId = _locPayableTransactionLineOS.Code;

                                XPCollection<PayableTransactionLine> _locPayableTransactionLines = new XPCollection<PayableTransactionLine>
                                                                                                     (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                      new BinaryOperator("Code", _currObjectId),
                                                                                                      new GroupOperator(GroupOperatorType.Or,
                                                                                                      new BinaryOperator("Status", Status.Posted),
                                                                                                      new BinaryOperator("Status", Status.Progress),
                                                                                                      new BinaryOperator("Status", Status.Open)
                                                                                                      )));

                                if (_locPayableTransactionLines != null && _locPayableTransactionLines.Count > 0)
                                {
                                    foreach (PayableTransactionLine _locPayableTransactionLine in _locPayableTransactionLines)
                                    {
                                        _locPayableTransactionLine.Select = true;
                                        _locPayableTransactionLine.Save();
                                        _locPayableTransactionLine.Session.CommitTransaction();
                                    }

                                    SuccessMessageShow("Payable Transaction Line has been Select");
                                }
                                else
                                {
                                    ErrorMessageShow("Data Payable Transaction Line Not Available");
                                }
                            }
                        }
                    }
                    if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                    {
                        _objectSpace.CommitChanges();
                        _objectSpace.Refresh();
                    }
                    if (View is ListView)
                    {
                        _objectSpace.CommitChanges();
                        View.ObjectSpace.Refresh();
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("BusinessObject = PayableTransactionLine" + ex.ToString());
            }
        }

        private void PayableTransactionLineUnselectAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;

                if (_objectToProcess != null)
                {
                    foreach (Object obj in _objectToProcess)
                    {
                        PayableTransactionLine _locPayableTransactionLineOS = (PayableTransactionLine)_objectSpace.GetObject(obj);

                        if (_locPayableTransactionLineOS != null)
                        {
                            if (_locPayableTransactionLineOS.Session != null)
                            {
                                _currSession = _locPayableTransactionLineOS.Session;
                            }

                            if (_locPayableTransactionLineOS.Code != null && _currSession != null)
                            {
                                _currObjectId = _locPayableTransactionLineOS.Code;

                                XPCollection<PayableTransactionLine> _locPayableTransactionLines = new XPCollection<PayableTransactionLine>
                                                                                                     (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                      new BinaryOperator("Code", _currObjectId),
                                                                                                      new GroupOperator(GroupOperatorType.Or,
                                                                                                      new BinaryOperator("Status", Status.Posted),
                                                                                                      new BinaryOperator("Status", Status.Progress),
                                                                                                      new BinaryOperator("Status", Status.Open)
                                                                                                      )));

                                if (_locPayableTransactionLines != null && _locPayableTransactionLines.Count > 0)
                                {
                                    foreach (PayableTransactionLine _locPayableTransactionLine in _locPayableTransactionLines)
                                    {
                                        _locPayableTransactionLine.Select = false;
                                        _locPayableTransactionLine.Save();
                                        _locPayableTransactionLine.Session.CommitTransaction();
                                    }

                                    SuccessMessageShow("Payable Transaction Line has been Unselect");
                                }
                                else
                                {
                                    ErrorMessageShow("Data Payable Transaction Line Not Available");
                                }
                            }
                        }
                    }
                    if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                    {
                        _objectSpace.CommitChanges();
                        _objectSpace.Refresh();
                    }
                    if (View is ListView)
                    {
                        _objectSpace.CommitChanges();
                        View.ObjectSpace.Refresh();
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("BusinessObject = PayableTransactionLine" + ex.ToString());
            }
        }

        private void PayableTransactionLineListviewFilterSelectionAction_Execute(object sender, SingleChoiceActionExecuteEventArgs e)
        {
            try
            {
                if ((View is ListView) & (View.ObjectTypeInfo.Type == typeof(PayableTransactionLine)))
                {
                    if ((Status)e.SelectedChoiceActionItem.Data == Status.None)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.None, BinaryOperatorType.NotEqual);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Open)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Open, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Progress)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Progress, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Posted)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Posted, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Lock)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Lock, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Close)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Close, BinaryOperatorType.Equal);
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PayableTransactionLine " + ex.ToString());
            }
        }

        //============================================= Code In Here =============================================

        #region Method

        private void GetChangeRates(Session _currSession, PayableTransactionLine _PayableTransactionLine)
        {
            try
            {
                XPCollection<ExchangeRate> _locExchangeRates = new XPCollection<ExchangeRate>(_currSession,
                                                               new BinaryOperator("PayableTransactionLine", _PayableTransactionLine));

                if (_locExchangeRates.Count() > 0)
                {
                    foreach (ExchangeRate _locExchangeRate in _locExchangeRates)
                    {
                        if (_locExchangeRate.Rate > 0 && _locExchangeRate.BillDebit > 0 || _locExchangeRate.BillCredit > 0)
                        {
                            PayableTransactionLine _locPayableTransactionLine = _currSession.FindObject<PayableTransactionLine>
                                                                                      (new GroupOperator(GroupOperatorType.And,
                                                                                       new BinaryOperator("Code", _PayableTransactionLine.Code)));

                            if (_PayableTransactionLine != null)
                            {
                                _PayableTransactionLine.Debit = _locExchangeRate.ForeignAmountDebit;
                                _PayableTransactionLine.Credit = _locExchangeRate.ForeignAmountCredit;
                                _PayableTransactionLine.Save();
                                _PayableTransactionLine.Session.CommitTransaction();
                            }
                        }
                        else
                        {
                            PayableTransactionLine _locPayableTransactionLine = _currSession.FindObject<PayableTransactionLine>
                                                                (new GroupOperator(GroupOperatorType.And,
                                                                 new BinaryOperator("Code", _PayableTransactionLine.Code)));

                            _PayableTransactionLine.Debit = 0;
                            _PayableTransactionLine.Credit = 0;
                            _PayableTransactionLine.Save();
                            _PayableTransactionLine.Session.CommitTransaction();
                        }
                    }
                }
                else
                {
                    PayableTransactionLine _locPayableTransactionLine = _currSession.FindObject<PayableTransactionLine>
                                                                              (new GroupOperator(GroupOperatorType.And,
                                                                               new BinaryOperator("Code", _PayableTransactionLine.Code)));

                    _PayableTransactionLine.Debit = _PayableTransactionLine.Debit;
                    _PayableTransactionLine.Credit = _PayableTransactionLine.Credit;
                    _PayableTransactionLine.Save();
                    _PayableTransactionLine.Session.CommitTransaction();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("BusinessObject = PayableTransactionLine" + ex.ToString());
            }
        }

        #endregion Method

        #region Global Method

        private void SuccessMessageShow(string _locActionName)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 2000;
            options.Message = string.Format("{0}", _locActionName);
            options.Type = InformationType.Success;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Success";
            options.Win.Type = WinMessageType.Flyout;
            Application.ShowViewStrategy.ShowMessage(options);
        }

        private void ErrorMessageShow(string _locMessage)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 2000;
            options.Message = string.Format(_locMessage);
            options.Type = InformationType.Warning;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Error";
            options.Win.Type = WinMessageType.Alert;
            Application.ShowViewStrategy.ShowMessage(options);
        }

        #endregion Global Method

    }
}
