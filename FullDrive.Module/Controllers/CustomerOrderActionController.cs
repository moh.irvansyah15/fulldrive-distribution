﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;

using FullDrive.Module.BusinessObjects;
using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.Xpo;
using System.Collections;
using DevExpress.Xpo;
using System.IO;
using System.Web;

namespace FullDrive.Module.Controllers
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class CustomerOrderActionController : ViewController
    {
        private ChoiceActionItem _selectionListviewFilter;

        public CustomerOrderActionController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.
            #region FilterStatus
            CustomerOrderListviewFilterSelectionAction.Items.Clear();
            foreach (object _currApproval in Enum.GetValues(typeof(CustomProcess.Status)))
            {
                EnumDescriptor _ed = new EnumDescriptor(typeof(CustomProcess.Status));
                _selectionListviewFilter = new ChoiceActionItem(_ed.GetCaption(_currApproval), _currApproval);
                CustomerOrderListviewFilterSelectionAction.Items.Add(_selectionListviewFilter);
            }
            #endregion FilterStatus
        }

        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
        }

        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }

        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        private void CustomerOrderProgressAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;
                Status _locStatus = Status.Open;

                if (_objectsToProcess != null)
                {
                    foreach (Object obj in _objectsToProcess)
                    {
                        CustomerOrder _locCustomerOrderOS = (CustomerOrder)_objectSpace.GetObject(obj);

                        if (_locCustomerOrderOS != null)
                        {
                            if (_locCustomerOrderOS.Session != null)
                            {
                                _currSession = _locCustomerOrderOS.Session;
                            }

                            if (_locCustomerOrderOS.Code != null && _currSession != null)
                            {
                                _currObjectId = _locCustomerOrderOS.Code;

                                CustomerOrder _locCustomerOrderXPO = _currSession.FindObject<CustomerOrder>
                                                                   (new GroupOperator(GroupOperatorType.And,
                                                                    new BinaryOperator("Code", _currObjectId),
                                                                    new GroupOperator(GroupOperatorType.Or,
                                                                    new BinaryOperator("Status", Status.Open),
                                                                    new BinaryOperator("Status", Status.Progress))));

                                if (_locCustomerOrderXPO != null)
                                {
                                    if (_locCustomerOrderXPO.Status == Status.Open)
                                    {
                                        _locStatus = Status.Progress;
                                    }
                                    else
                                    {
                                        _locStatus = _locCustomerOrderXPO.Status;
                                    }

                                    _locCustomerOrderXPO.Status = _locStatus;
                                    _locCustomerOrderXPO.StatusDate = now;
                                    _locCustomerOrderXPO.Save();
                                    _locCustomerOrderXPO.Session.CommitTransaction();

                                    #region CustomerOrderLine
                                    if (GetAndSetForCheckQtyOnHandAlsoChangeStatusToProgress(_currSession, _locCustomerOrderXPO) == false)
                                    {
                                        SetTotalAmountAndTaxAmountAndDiscountAmount(_currSession, _locCustomerOrderXPO);
                                    }
                                    #endregion CustomerOrderLine

                                    SuccessMessageShow(_locCustomerOrderXPO.Code + " has been change successfully to Progress ");
                                }
                                else
                                {
                                    ErrorMessageShow("Data Customer Order Not Available");
                                }
                            }
                            else
                            {
                                ErrorMessageShow("Data Customer Order Not Available");
                            }
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = CustomerOrder " + ex.ToString());
            }
        }

        private void CustomerOrderSetFreeItemAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;

                if (_objectsToProcess != null)
                {
                    foreach (Object obj in _objectsToProcess)
                    {
                        CustomerOrder _locCustomerOrderOS = (CustomerOrder)_objectSpace.GetObject(obj);

                        if (_locCustomerOrderOS != null)
                        {
                            if (_locCustomerOrderOS.Session != null)
                            {
                                _currSession = _locCustomerOrderOS.Session;
                            }

                            if (_locCustomerOrderOS.Code != null && _currSession != null)
                            {
                                _currObjectId = _locCustomerOrderOS.Code;

                                CustomerOrder _locCustomerOrderXPO = _currSession.FindObject<CustomerOrder>
                                                                   (new GroupOperator(GroupOperatorType.And,
                                                                    new BinaryOperator("Code", _currObjectId),
                                                                    new BinaryOperator("Status", Status.Progress)));

                                if (_locCustomerOrderXPO != null)
                                {
                                    SetFreeItem(_currSession, _locCustomerOrderXPO);
                                    SetTotalFreeItemAmount(_currSession, _locCustomerOrderXPO);
                                    SuccessMessageShow(_locCustomerOrderXPO.Code + " has been successfully create Free Item  ");
                                }
                                else
                                {
                                    ErrorMessageShow("Data Customer Order Not Available");
                                }
                            }
                            else
                            {
                                ErrorMessageShow("Data Customer Order Not Available");
                            }
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = CustomerOrder " + ex.ToString());
            }
        }

        private void CustomerOrderPostingAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;

                if (_objectsToProcess != null)
                {
                    foreach (Object obj in _objectsToProcess)
                    {
                        CustomerOrder _locCustomerOrderOS = (CustomerOrder)_objectSpace.GetObject(obj);

                        if (_locCustomerOrderOS != null)
                        {
                            if (_locCustomerOrderOS.Session != null)
                            {
                                _currSession = _locCustomerOrderOS.Session;
                            }

                            if (_locCustomerOrderOS.Code != null && _currSession != null)
                            {
                                _currObjectId = _locCustomerOrderOS.Code;

                                CustomerOrder _locCustomerOrderXPO = _currSession.FindObject<CustomerOrder>
                                                                   (new GroupOperator(GroupOperatorType.And,
                                                                    new BinaryOperator("Code", _currObjectId),
                                                                    new GroupOperator(GroupOperatorType.Or,
                                                                    new BinaryOperator("Status", Status.Progress),
                                                                    new BinaryOperator("Status", Status.Posted))));

                                if (_locCustomerOrderXPO != null)
                                {
                                    if (_locCustomerOrderXPO.TotAmount > 0)
                                    { 
                                        if (_locCustomerOrderXPO.TotAmount == GetCustomerOrderLineTotalAmount(_currSession, _locCustomerOrderXPO))
                                        {
                                            SetCustomerInvoice(_currSession, _locCustomerOrderXPO);
                                            SetStatusCustomerOrderLine(_currSession, _locCustomerOrderXPO);
                                            SetStatusCustomerOrderFreeItem(_currSession, _locCustomerOrderXPO);
                                            SetNormalQuantity(_currSession, _locCustomerOrderXPO);
                                            SetFinalStatusCustomerOrder(_currSession, _locCustomerOrderXPO);
                                            SuccessMessageShow(_locCustomerOrderXPO.Code + " has been change successfully to Posted");
                                        }
                                    }
                                }
                                else
                                {
                                    ErrorMessageShow("Data Customer Order Not Available");
                                }
                            }
                            else
                            {
                                ErrorMessageShow("Data Customer Order Not Available");
                            }
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = CustomerOrder " + ex.ToString());
            }
        }

        private void CustomerOrderCancelOrderAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;
                var User = SecuritySystem.CurrentUserName;

                if (_objectsToProcess != null)
                {
                    foreach (Object obj in _objectsToProcess)
                    {
                        CustomerOrder _locCustomerOrderOS = (CustomerOrder)_objectSpace.GetObject(obj);

                        if (_locCustomerOrderOS != null)
                        {
                            if (_locCustomerOrderOS.Session != null)
                            {
                                _currSession = _locCustomerOrderOS.Session;
                            }

                            if (_locCustomerOrderOS.Code != null && _currSession != null)
                            {
                                _currObjectId = _locCustomerOrderOS.Code;

                                CustomerOrder _locCustomerOrderXPO = _currSession.FindObject<CustomerOrder>
                                                                   (new GroupOperator(GroupOperatorType.And,
                                                                    new BinaryOperator("Code", _currObjectId)));

                                if (_locCustomerOrderXPO != null)
                                {
                                    if (_locCustomerOrderXPO.Status == Status.Open)
                                    {
                                        XPCollection<CustomerOrderLine> _locCustomerOrderLines = new XPCollection<CustomerOrderLine>
                                                                                               (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                new BinaryOperator("CustomerOrder", _locCustomerOrderXPO),
                                                                                                new BinaryOperator("Status", Status.Open)));
                                        if (_locCustomerOrderLines != null && _locCustomerOrderLines.Count() > 0)
                                        {
                                            foreach (CustomerOrderLine _locCustomerOrderLine in _locCustomerOrderLines)
                                            {
                                                _locCustomerOrderLine.Status = Status.Cancel;
                                                _locCustomerOrderLine.StatusDate = now;
                                                _locCustomerOrderLine.Save();
                                                _locCustomerOrderLine.Session.CommitTransaction();
                                            }
                                        }
                                    }
                                    else if (_locCustomerOrderXPO.Status == Status.Progress || _locCustomerOrderXPO.Status == Status.Posted || _locCustomerOrderXPO.Status == Status.Close)
                                    {
                                        double _locQtyOnHand = 0;
                                        double _locBalHold = 0;
                                        double _locQtyFreeItemOnHand = 0;

                                        #region CustomerOrderLineandCustomerInvoiceLine
                                        XPCollection<CustomerOrderLine> _locCustomerOrderLines = new XPCollection<CustomerOrderLine>
                                                                                               (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                new BinaryOperator("CustomerOrder", _locCustomerOrderXPO),
                                                                                                new GroupOperator(GroupOperatorType.Or,
                                                                                                new BinaryOperator("Status", Status.Progress),
                                                                                                new BinaryOperator("Status", Status.Posted),
                                                                                                new BinaryOperator("Status", Status.Close))));
                                        if (_locCustomerOrderLines != null && _locCustomerOrderLines.Count() > 0)
                                        {
                                            foreach (CustomerOrderLine _locCustomerOrderLine in _locCustomerOrderLines)
                                            {
                                                if (_locCustomerOrderLine.BegInvLine != null)
                                                {
                                                    _locQtyOnHand = _locCustomerOrderLine.BegInvLine.QtyOnHand - _locCustomerOrderLine.TQty;
                                                    if (_locQtyOnHand < 0)
                                                    {
                                                        _locQtyOnHand = 0;
                                                    }
                                                    _locCustomerOrderLine.BegInvLine.QtyOnHand = _locQtyOnHand;
                                                    _locCustomerOrderLine.BegInvLine.Save();
                                                    _locCustomerOrderLine.BegInvLine.Session.CommitTransaction();
                                                }

                                                if (_locCustomerOrderLine.AccountingPeriodicLine != null)
                                                {
                                                    _locBalHold = _locCustomerOrderLine.AccountingPeriodicLine.BalHold - _locCustomerOrderLine.DiscAmount;
                                                    if (_locBalHold < 0)
                                                    {
                                                        _locBalHold = 0;
                                                    }
                                                    _locCustomerOrderLine.AccountingPeriodicLine.BalHold = _locBalHold;
                                                    _locCustomerOrderLine.AccountingPeriodicLine.Save();
                                                    _locCustomerOrderLine.AccountingPeriodicLine.Session.CommitTransaction();
                                                }

                                                _locCustomerOrderLine.Status = Status.Cancel;
                                                _locCustomerOrderLine.StatusDate = now;
                                                _locCustomerOrderLine.Save();
                                                _locCustomerOrderLine.Session.CommitTransaction();

                                                XPCollection<CustomerInvoiceLine> _locCustomerInvoiceLines = new XPCollection<CustomerInvoiceLine>
                                                                                               (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                new BinaryOperator("CustomerOrderLine", _locCustomerOrderLine)
                                                                                                ));
                                                if (_locCustomerInvoiceLines != null && _locCustomerInvoiceLines.Count() > 0)
                                                {
                                                    foreach (CustomerInvoiceLine _locCustomerInvoiceLine in _locCustomerInvoiceLines)
                                                    {
                                                        _locCustomerInvoiceLine.Status = Status.Cancel;
                                                        _locCustomerInvoiceLine.StatusDate = now;
                                                        _locCustomerInvoiceLine.Save();
                                                        _locCustomerInvoiceLine.Session.CommitTransaction();
                                                    }
                                                }
                                            }
                                        }
                                        #endregion CustomerOrderLineandCustomerInvoiceLine

                                        #region CustomerInvoice
                                        XPCollection<CustomerInvoice> _locCustomerInvoices = new XPCollection<CustomerInvoice>
                                                                                               (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                new BinaryOperator("CustomerOrder", _locCustomerOrderXPO)
                                                                                                ));
                                        if (_locCustomerInvoices != null && _locCustomerInvoices.Count() > 0)
                                        {
                                            foreach (CustomerInvoice _locCustomerInvoice in _locCustomerInvoices)
                                            {
                                                _locCustomerInvoice.Status = Status.Cancel;
                                                _locCustomerInvoice.StatusDate = now;
                                                _locCustomerInvoice.Save();
                                                _locCustomerInvoice.Session.CommitTransaction();
                                            }
                                        }
                                        #endregion CustomerInvoice

                                        #region CustomerOrderFreeItem
                                        XPCollection<CustomerOrderFreeItem> _locCustomerOrderFreeItems = new XPCollection<CustomerOrderFreeItem>
                                                                                               (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                new BinaryOperator("CustomerOrder", _locCustomerOrderXPO)
                                                                                                ));
                                        if (_locCustomerOrderFreeItems != null && _locCustomerOrderFreeItems.Count() > 0)
                                        {
                                            foreach (CustomerOrderFreeItem _locCustomerOrderFreeItem in _locCustomerOrderFreeItems)
                                            {
                                                if(_locCustomerOrderFreeItem.BegInvLine != null)
                                                {
                                                    _locQtyFreeItemOnHand = _locCustomerOrderFreeItem.BegInvLine.QtyOnHand - _locCustomerOrderFreeItem.FpTQty;
                                                    if (_locQtyFreeItemOnHand < 0)
                                                    {
                                                        _locQtyFreeItemOnHand = 0;
                                                    }
                                                    _locCustomerOrderFreeItem.BegInvLine.QtyOnHand = _locQtyFreeItemOnHand;
                                                    _locCustomerOrderFreeItem.BegInvLine.Save();
                                                    _locCustomerOrderFreeItem.BegInvLine.Session.CommitTransaction();
                                                }
                                                _locCustomerOrderFreeItem.Status = Status.Cancel;
                                                _locCustomerOrderFreeItem.StatusDate = now;
                                                _locCustomerOrderFreeItem.Save();
                                                _locCustomerOrderFreeItem.Session.CommitTransaction();
                                            }
                                        }
                                        #endregion CustomerOrderFreeItem
                                    }

                                    _locCustomerOrderXPO.Status = Status.Cancel;
                                    _locCustomerOrderXPO.StatusDate = now;
                                    _locCustomerOrderXPO.Save();
                                    _locCustomerOrderXPO.Session.CommitTransaction();
                                    SuccessMessageShow(_locCustomerOrderXPO.Code + " has been change successfully to Cancel ");
                                }
                                else
                                {
                                    ErrorMessageShow("Data Customer Order Not Available");
                                }
                            }
                            else
                            {
                                ErrorMessageShow("Data Customer Order Not Available");
                            }
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = CustomerOrder " + ex.ToString());
            }
        }

        private void CustomerOrderListviewFilterSelectionAction_Execute(object sender, SingleChoiceActionExecuteEventArgs e)
        {
            try
            {
                if ((View is ListView) & (View.ObjectTypeInfo.Type == typeof(CustomerOrder)))
                {
                    if ((Status)e.SelectedChoiceActionItem.Data == Status.None)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.None, BinaryOperatorType.NotEqual);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Open)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Open, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Progress)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Progress, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Posted)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Posted, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Lock)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Lock, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Close)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Close, BinaryOperatorType.Equal);
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = CustomerOrder " + ex.ToString());
            }
        }

        //=========================================== Code In Here ===========================================

        #region Progress

        private bool GetAndSetForCheckQtyOnHandAlsoChangeStatusToProgress(Session _currSession, CustomerOrder _locCustomerOrderXPO)
        {
            bool _return = false;
            try
            {
                DateTime now = DateTime.Now;
                bool _locRedColor = false;
                if (_locCustomerOrderXPO != null)
                {
                    XPCollection<CustomerOrderLine> _locCustomerOrderLines = new XPCollection<CustomerOrderLine>
                                                                                               (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                new BinaryOperator("CustomerOrder", _locCustomerOrderXPO),
                                                                                                new BinaryOperator("Select", true),
                                                                                                new BinaryOperator("Status", Status.Open)));

                    if (_locCustomerOrderLines != null && _locCustomerOrderLines.Count > 0)
                    {
                        foreach (CustomerOrderLine _locCustomerOrderLine in _locCustomerOrderLines)
                        {
                            if (_locCustomerOrderLine.BegInvLine != null)
                            {
                                if ((_locCustomerOrderLine.BegInvLine.QtyAvailable - _locCustomerOrderLine.BegInvLine.QtyOnHand) >= _locCustomerOrderLine.TQty)
                                {
                                    _locCustomerOrderLine.Status = Status.Progress;
                                    _locCustomerOrderLine.StatusDate = now;
                                    _locCustomerOrderLine.RedColor = false;
                                    _locCustomerOrderLine.Message = null;
                                    _locCustomerOrderLine.Save();
                                    _locCustomerOrderLine.Session.CommitTransaction();

                                    _locCustomerOrderLine.BegInvLine.QtyOnHand = _locCustomerOrderLine.BegInvLine.QtyOnHand + _locCustomerOrderLine.TQty;
                                    _locCustomerOrderLine.BegInvLine.Save();
                                    _locCustomerOrderLine.BegInvLine.Session.CommitTransaction();
                                }
                                else
                                {
                                    _locRedColor = true;
                                    _locCustomerOrderLine.RedColor = true;
                                    _locCustomerOrderLine.Message = "Please delete the records and create new one also update stock in warehouse";
                                    _locCustomerOrderLine.Save();
                                    _locCustomerOrderLine.Session.CommitTransaction();
                                }
                            }
                        }
                    }
                    _return = _locRedColor;
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("BusinessObject = CustomerOrder" + ex.ToString());
            }
            return _return;
        }

        #region GetAmount

        private void SetTotalAmountAndTaxAmountAndDiscountAmount(Session _currSession, CustomerOrder _locCustomerOrderXPO)
        {
            try
            {
                double _locTAmount = 0;
                double _locTUAmount = 0;
                double _locDiscAmountRule2 = 0;
                double _locTxAmount = 0;
                double _locDiscAmountRule1 = 0;
                if (_locCustomerOrderXPO != null)
                {
                    XPCollection<CustomerOrderLine> _locCustomerOrderLines = new XPCollection<CustomerOrderLine>
                                                                                (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                new BinaryOperator("CustomerOrder", _locCustomerOrderXPO),
                                                                                new BinaryOperator("Select", true),
                                                                                new GroupOperator(GroupOperatorType.Or,
                                                                                new BinaryOperator("Status", Status.Progress))));
                    if (_locCustomerOrderLines != null && _locCustomerOrderLines.Count() > 0)
                    {
                        foreach (CustomerOrderLine _locCustomerOrderLine in _locCustomerOrderLines)
                        {
                            _locTAmount = _locTAmount + _locCustomerOrderLine.TAmount;
                            _locTUAmount = _locTUAmount + _locCustomerOrderLine.TUAmount;
                            _locDiscAmountRule2 = _locDiscAmountRule2 + _locCustomerOrderLine.DiscAmount;
                            if (_locCustomerOrderLine.Tax != null)
                            {
                                if (_locCustomerOrderLine.Tax.TaxNature == TaxNature.Increase)
                                {
                                    _locTxAmount = _locTxAmount + _locCustomerOrderLine.TxAmount;
                                }
                                if (_locCustomerOrderLine.Tax.TaxNature == TaxNature.Decrease)
                                {
                                    _locTxAmount = _locTxAmount - _locCustomerOrderLine.TxAmount;
                                }
                            }
                        }

                        _locDiscAmountRule1 = GetDiscountRule1(_currSession, _locCustomerOrderXPO);
                    }

                    _locCustomerOrderXPO.TotUnitAmount = _locTUAmount;
                    _locCustomerOrderXPO.TotAmount = _locTAmount;
                    _locCustomerOrderXPO.AmountDisc = _locDiscAmountRule1;
                    _locCustomerOrderXPO.TotDiscAmount = (_locDiscAmountRule2 + _locDiscAmountRule1);
                    _locCustomerOrderXPO.TotTaxAmount = _locTxAmount;
                    _locCustomerOrderXPO.Amount = _locTAmount - _locDiscAmountRule1;
                    _locCustomerOrderXPO.Save();
                    _locCustomerOrderXPO.Session.CommitTransaction();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = CustomerOrder ", ex.ToString());
            }
        }

        #endregion GetAmount

        #endregion Progress

        #region Discount

        private double GetDiscountRule1(Session _currSession, CustomerOrder _locCustomerOrderXPO)
        {
            double _result = 0;
            try
            {
                if (_locCustomerOrderXPO != null)
                {
                    if (_locCustomerOrderXPO.Company != null && _locCustomerOrderXPO.Workplace != null)
                    {
                        DiscountRule _locDiscountRule1 = _currSession.FindObject<DiscountRule>
                                                            (new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("Company", _locCustomerOrderXPO.Company),
                                                            new BinaryOperator("Workplace", _locCustomerOrderXPO.Workplace),
                                                            new BinaryOperator("PriceGroup", _locCustomerOrderXPO.PriceGroup),
                                                            new BinaryOperator("RuleType", RuleType.Rule1),
                                                            new BinaryOperator("Active", true)));

                        #region Rule1

                        //Grand Total Amount Per Invoice
                        if (_locDiscountRule1 != null)
                        {
                            double _locTUAmount = 0;
                            double _locTAmount = 0;
                            double _locTUAmountDiscount = 0;
                            double _locTUAmountDiscount2 = 0;
                            AccountingPeriodicLine _locAcctPerLine = null;

                            #region Gross
                            if (_locDiscountRule1.GrossAmountRule == true)
                            {
                                XPCollection<CustomerOrderLine> _locMainCustomerOrderLines = new XPCollection<CustomerOrderLine>
                                                                                               (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                               new BinaryOperator("CustomerOrder", _locCustomerOrderXPO),
                                                                                               new BinaryOperator("Select", true),
                                                                                               new GroupOperator(GroupOperatorType.Or,
                                                                                               new BinaryOperator("Status", Status.Open),
                                                                                               new BinaryOperator("Status", Status.Progress))));


                                if (_locMainCustomerOrderLines != null && _locMainCustomerOrderLines.Count() > 0)
                                {
                                    foreach (CustomerOrderLine _locMainCustomerOrderLine in _locMainCustomerOrderLines)
                                    {
                                        if (_locMainCustomerOrderLine.TUAmount > 0)
                                        {
                                            _locTUAmount = _locTUAmount + _locMainCustomerOrderLine.TUAmount;
                                            _locMainCustomerOrderLine.DiscountChecked = true;
                                            _locMainCustomerOrderLine.Save();
                                            _locMainCustomerOrderLine.Session.CommitTransaction();
                                        }
                                    }

                                    if (_locTUAmount > 0 && _locTUAmount >= _locDiscountRule1.GrossTotalUAmount)
                                    {
                                        if (_locDiscountRule1.DiscountType == DiscountType.Percentage)
                                        {
                                            _locTUAmountDiscount = _locTUAmount * _locDiscountRule1.Value / 100;
                                        }
                                        if (_locDiscountRule1.DiscountType == DiscountType.Value)
                                        {
                                            _locTUAmountDiscount = _locDiscountRule1.Value;
                                        }
                                    }
                                }
                            }
                            #endregion Gross
                            #region Net
                            if (_locDiscountRule1.NetAmountRule == true)
                            {
                                XPCollection<CustomerOrderLine> _locMainCustomerOrderLines = new XPCollection<CustomerOrderLine>
                                                                                               (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                               new BinaryOperator("CustomerOrder", _locCustomerOrderXPO),
                                                                                               new BinaryOperator("Select", true),
                                                                                               new GroupOperator(GroupOperatorType.Or,
                                                                                               new BinaryOperator("Status", Status.Open),
                                                                                               new BinaryOperator("Status", Status.Progress))));

                                if (_locMainCustomerOrderLines != null && _locMainCustomerOrderLines.Count() > 0)
                                {
                                    foreach (CustomerOrderLine _locMainCustomerOrderLine in _locMainCustomerOrderLines)
                                    {
                                        if (_locMainCustomerOrderLine.TAmount > 0)
                                        {
                                            _locTAmount = _locTAmount + _locMainCustomerOrderLine.TAmount;
                                            _locMainCustomerOrderLine.DiscountChecked = true;
                                            _locMainCustomerOrderLine.Save();
                                            _locMainCustomerOrderLine.Session.CommitTransaction();
                                        }
                                    }

                                    if (_locTAmount > 0 && _locTAmount >= _locDiscountRule1.NetTotalUAmount)
                                    {
                                        if (_locDiscountRule1.DiscountType == DiscountType.Percentage)
                                        {
                                            _locTUAmountDiscount = _locTAmount * _locDiscountRule1.Value / 100;
                                        }
                                        if (_locDiscountRule1.DiscountType == DiscountType.Value)
                                        {
                                            _locTUAmountDiscount = _locDiscountRule1.Value;
                                        }
                                    }
                                }
                            }
                            #endregion Net
                            #region ValBasedAccount
                            if (_locDiscountRule1.ValBasedAccount == true)
                            {
                                if (_locDiscountRule1.AccountNo != null)
                                {
                                    AccountingPeriodic _locAccountPeriodic = _currSession.FindObject<AccountingPeriodic>
                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                    new BinaryOperator("Company", _locCustomerOrderXPO.Company),
                                                                    new BinaryOperator("Workplace", _locCustomerOrderXPO.Workplace),
                                                                    new BinaryOperator("Active", true)));
                                    if (_locAccountPeriodic != null)
                                    {
                                        AccountingPeriodicLine _locAccountingPeriodicLine = _currSession.FindObject<AccountingPeriodicLine>
                                                                                            (new GroupOperator(GroupOperatorType.And,
                                                                                            new BinaryOperator("AccountingPeriodic", _locAccountPeriodic),
                                                                                            new BinaryOperator("AccountNo", _locDiscountRule1.AccountNo)));
                                        if (_locAccountingPeriodicLine != null)
                                        {
                                            _locAcctPerLine = _locAccountingPeriodicLine;
                                            if (_locAccountingPeriodicLine.BalHold < _locAccountingPeriodicLine.Balance)
                                            {
                                                _locTUAmountDiscount2 = _locAccountingPeriodicLine.Balance - _locAccountingPeriodicLine.BalHold;
                                                if (_locTUAmountDiscount2 < _locTUAmountDiscount)
                                                {
                                                    _locTUAmountDiscount = _locTUAmountDiscount2;
                                                }
                                            }
                                            else
                                            {
                                                _locTUAmountDiscount = 0;
                                            }
                                            _locAccountingPeriodicLine.BalHold = _locAccountingPeriodicLine.BalHold + _locTUAmountDiscount;
                                            _locAccountingPeriodicLine.Save();
                                            _locAccountingPeriodicLine.Session.CommitTransaction();
                                        }
                                    }
                                }
                            }
                            #endregion ValBasedAccount

                            _locCustomerOrderXPO.AccountingPeriodicLine = _locAcctPerLine;
                            _locCustomerOrderXPO.DiscountRule = _locDiscountRule1;
                            _locCustomerOrderXPO.Save();
                            _locCustomerOrderXPO.Session.CommitTransaction();

                            _result = _locTUAmountDiscount;

                        }
                        #endregion Rule1
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = CustomerOrder ", ex.ToString());
            }
            return _result;
        }

        #endregion Discount

        #region FreeItem

        private void SetFreeItem(Session _currSession, CustomerOrder _locCustomerOrderXPO)
        {
            try
            {
                //Harus di perhatikan QtyOnHand
                if (_locCustomerOrderXPO != null)
                {
                    XPCollection<FreeItemRule> _locFreeItemRules = new XPCollection<FreeItemRule>
                                                            (_currSession, new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("Company", _locCustomerOrderXPO.Company),
                                                            new BinaryOperator("Workplace", _locCustomerOrderXPO.Workplace),
                                                            new BinaryOperator("Location", _locCustomerOrderXPO.Location),
                                                            new BinaryOperator("PriceGroup", _locCustomerOrderXPO.PriceGroup),
                                                            new BinaryOperator("Active", true)));

                    if (_locFreeItemRules != null && _locFreeItemRules.Count() > 0)
                    {
                        foreach (FreeItemRule _locFreeItemRule in _locFreeItemRules)
                        {
                            #region Header
                            if (_locFreeItemRule.FreeItemType1 == FreeItemType1.Header)
                            {
                                if (_locFreeItemRule.OpenAmount == true)
                                {
                                    #region Rule1
                                    if (_locFreeItemRule.RuleType == RuleType.Rule1)
                                    {
                                        double _locQtyFI = 0;
                                        double _locTotQtyFI = 0;
                                        double _locFpDqty = 0;
                                        double _locFpQty = 0;
                                        int _locMaxLoop = 0;
                                        int _locTotMaxLoopVal = 0;

                                        #region Amount
                                        if (_locFreeItemRule.NetAmountRule == true)
                                        {
                                            if (_locCustomerOrderXPO.TotAmount >= _locFreeItemRule.NetTotalUAmount)
                                            {
                                                _locQtyFI = System.Math.Floor(_locCustomerOrderXPO.TotAmount / _locFreeItemRule.NetTotalUAmount);
                                            }
                                        }
                                        if (_locFreeItemRule.GrossAmountRule == true)
                                        {
                                            if (_locCustomerOrderXPO.TotUnitAmount >= _locFreeItemRule.GrossTotalUAmount)
                                            {
                                                _locQtyFI = System.Math.Floor(_locCustomerOrderXPO.TotUnitAmount / _locFreeItemRule.GrossTotalUAmount);
                                            }
                                        }
                                        #endregion Amount

                                        if (_locFreeItemRule.MaxLoop == 0)
                                        {
                                            _locMaxLoop = (int)_locQtyFI;
                                        }
                                        else
                                        {
                                            if (_locFreeItemRule.MaxLoop > (int)_locQtyFI)
                                            {
                                                _locMaxLoop = (int)_locQtyFI;
                                            }
                                            else
                                            {
                                                _locMaxLoop = _locFreeItemRule.MaxLoop;
                                            }
                                        }

                                        for (int i = 1; i <= _locMaxLoop; i++)
                                        {
                                            _locTotMaxLoopVal = _locTotMaxLoopVal + 1;
                                        }

                                        _locTotQtyFI = _locTotMaxLoopVal * _locFreeItemRule.FpTQty;

                                        if (_locFreeItemRule.FpBegInvLine != null)
                                        {
                                            if ((_locFreeItemRule.FpBegInvLine.QtyAvailable - _locFreeItemRule.FpBegInvLine.QtyOnHand) > 0)
                                            {
                                                if((_locFreeItemRule.FpBegInvLine.QtyAvailable - _locFreeItemRule.FpBegInvLine.QtyOnHand) < _locTotQtyFI)
                                                {
                                                    _locTotQtyFI = _locFreeItemRule.FpBegInvLine.QtyAvailable - _locFreeItemRule.FpBegInvLine.QtyOnHand;
                                                }

                                                if (_locFreeItemRule.FpItem != null && _locFreeItemRule.FpDUOM != null && _locFreeItemRule.FpUOM != null)
                                                {
                                                    ItemUnitOfMeasure _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                                            (new GroupOperator(GroupOperatorType.And,
                                                                             new BinaryOperator("Item", _locFreeItemRule.FpItem),
                                                                             new BinaryOperator("UOM", _locFreeItemRule.FpUOM),
                                                                             new BinaryOperator("DefaultUOM", _locFreeItemRule.FpDUOM),
                                                                             new BinaryOperator("Active", true)));
                                                    if (_locItemUOM != null)
                                                    {
                                                        if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                                        {
                                                            _locFpDqty = _locTotQtyFI % _locItemUOM.DefaultConversion;
                                                            _locFpQty = System.Math.Floor(_locTotQtyFI / _locItemUOM.DefaultConversion);
                                                        }
                                                        else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                                        {
                                                            _locFpDqty = System.Math.Floor(_locTotQtyFI / _locItemUOM.DefaultConversion);
                                                            _locFpQty = (_locTotQtyFI % _locItemUOM.DefaultConversion) * _locItemUOM.Conversion;
                                                        }
                                                        else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                                        {
                                                            _locFpDqty = _locTotQtyFI;
                                                        }
                                                    }
                                                }
                                                else
                                                {
                                                    _locFpDqty = _locTotQtyFI;
                                                }

                                                CustomerOrderFreeItem _saveDataCustomerOrderFreeItem = new CustomerOrderFreeItem(_currSession)
                                                {
                                                    //Organization
                                                    Company = _locCustomerOrderXPO.Company,
                                                    Workplace = _locCustomerOrderXPO.Workplace,
                                                    Division = _locCustomerOrderXPO.Division,
                                                    Department = _locCustomerOrderXPO.Department,
                                                    Section = _locCustomerOrderXPO.Section,
                                                    Employee = _locCustomerOrderXPO.Employee,
                                                    //Item
                                                    CustomerOrder = _locCustomerOrderXPO,
                                                    Location = _locCustomerOrderXPO.Location,
                                                    BegInv = _locFreeItemRule.FpBegInv,
                                                    FpLocationType = _locFreeItemRule.LocationType,
                                                    FpStockType = _locFreeItemRule.StockType,
                                                    FpStockGroup = _locFreeItemRule.FpStockGroup,
                                                    FreeItemRule = _locFreeItemRule,
                                                    FpItem = _locFreeItemRule.FpItem,
                                                    FpDQty = _locFpDqty,
                                                    FpDUOM = _locFreeItemRule.FpDUOM,
                                                    FpQty = _locFpQty,
                                                    FpUOM = _locFreeItemRule.FpUOM,
                                                    FpTQty = _locTotQtyFI,
                                                    BegInvLine = _locFreeItemRule.FpBegInvLine,
                                                    //Amount
                                                    Currency = _locCustomerOrderXPO.Currency,
                                                    PriceGroup = _locCustomerOrderXPO.PriceGroup,
                                                    PriceLine = GetPriceLine(_currSession, _locCustomerOrderXPO, _locFreeItemRule.FpItem),
                                                    UAmount = GetPriceLine(_currSession, _locCustomerOrderXPO, _locFreeItemRule.FpItem).Amount2a,
                                                };
                                                _saveDataCustomerOrderFreeItem.Save();
                                                _saveDataCustomerOrderFreeItem.Session.CommitTransaction();

                                                if (_locFreeItemRule.FpBegInv != null && _locFreeItemRule.FpBegInvLine != null)
                                                {
                                                    _locFreeItemRule.FpBegInvLine.QtyOnHand = _locFreeItemRule.FpBegInvLine.QtyOnHand + _locTotQtyFI;
                                                    _locFreeItemRule.FpBegInvLine.Save();
                                                    _locFreeItemRule.FpBegInvLine.Session.CommitTransaction();
                                                }
                                            }
                                        }                   
                                    }

                                    #endregion Rule1

                                    //1, 2+1, 3+1, 4+1
                                    #region Rule2
                                    if (_locFreeItemRule.RuleType == RuleType.Rule2)
                                    {
                                        double _locQtyFI = 0;
                                        double _locTotQtyFI = 0;
                                        double _locFpDqty = 0;
                                        double _locFpQty = 0;
                                        int _locMaxLoop = 0;

                                        #region Amount
                                        if (_locFreeItemRule.NetAmountRule == true)
                                        {
                                            if (_locCustomerOrderXPO.TotAmount >= _locFreeItemRule.NetTotalUAmount)
                                            {
                                                _locQtyFI = System.Math.Floor(_locCustomerOrderXPO.TotAmount / _locFreeItemRule.NetTotalUAmount);
                                            }
                                        }
                                        if (_locFreeItemRule.GrossAmountRule == true)
                                        {
                                            if (_locCustomerOrderXPO.TotUnitAmount >= _locFreeItemRule.GrossTotalUAmount)
                                            {
                                                _locQtyFI = System.Math.Floor(_locCustomerOrderXPO.TotUnitAmount / _locFreeItemRule.GrossTotalUAmount);
                                            }
                                        }
                                        #endregion Amount

                                        if (_locFreeItemRule.MaxLoop == 0)
                                        {
                                            _locMaxLoop = (int)_locQtyFI;
                                        }
                                        else
                                        {
                                            if (_locFreeItemRule.MaxLoop > (int)_locQtyFI)
                                            {
                                                _locMaxLoop = (int)_locQtyFI;
                                            }
                                            else
                                            {
                                                _locMaxLoop = _locFreeItemRule.MaxLoop;
                                            }
                                        }

                                        for (int i = 1; i <= _locMaxLoop; i++)
                                        {
                                            if (i == 1)
                                            {
                                                _locTotQtyFI = _locFreeItemRule.FpTQty;
                                            }
                                            else
                                            {
                                                _locTotQtyFI = _locTotQtyFI + (i + _locFreeItemRule.FpTQty);
                                            }
                                        }

                                        if (_locFreeItemRule.FpBegInvLine != null)
                                        {
                                            if ((_locFreeItemRule.FpBegInvLine.QtyAvailable - _locFreeItemRule.FpBegInvLine.QtyOnHand) > 0)
                                            {
                                                if ((_locFreeItemRule.FpBegInvLine.QtyAvailable - _locFreeItemRule.FpBegInvLine.QtyOnHand) < _locTotQtyFI)
                                                {
                                                    _locTotQtyFI = _locFreeItemRule.FpBegInvLine.QtyAvailable - _locFreeItemRule.FpBegInvLine.QtyOnHand;
                                                }

                                                if (_locFreeItemRule.FpItem != null && _locFreeItemRule.FpDUOM != null && _locFreeItemRule.FpUOM != null)
                                                {
                                                    ItemUnitOfMeasure _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                                            (new GroupOperator(GroupOperatorType.And,
                                                                             new BinaryOperator("Item", _locFreeItemRule.FpItem),
                                                                             new BinaryOperator("UOM", _locFreeItemRule.FpUOM),
                                                                             new BinaryOperator("DefaultUOM", _locFreeItemRule.FpDUOM),
                                                                             new BinaryOperator("Active", true)));
                                                    if (_locItemUOM != null)
                                                    {
                                                        if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                                        {
                                                            _locFpDqty = _locTotQtyFI % _locItemUOM.DefaultConversion;
                                                            _locFpQty = System.Math.Floor(_locTotQtyFI / _locItemUOM.DefaultConversion);
                                                        }
                                                        else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                                        {
                                                            _locFpDqty = System.Math.Floor(_locTotQtyFI / _locItemUOM.DefaultConversion);
                                                            _locFpQty = (_locTotQtyFI % _locItemUOM.DefaultConversion) * _locItemUOM.Conversion;
                                                        }
                                                        else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                                        {
                                                            _locFpDqty = _locTotQtyFI;
                                                        }
                                                    }
                                                }
                                                else
                                                {
                                                    _locFpDqty = _locTotQtyFI;
                                                }

                                                CustomerOrderFreeItem _saveDataCustomerOrderFreeItem = new CustomerOrderFreeItem(_currSession)
                                                {
                                                    //Organization
                                                    Company = _locCustomerOrderXPO.Company,
                                                    Workplace = _locCustomerOrderXPO.Workplace,
                                                    Division = _locCustomerOrderXPO.Division,
                                                    Department = _locCustomerOrderXPO.Department,
                                                    Section = _locCustomerOrderXPO.Section,
                                                    Employee = _locCustomerOrderXPO.Employee,
                                                    //Item
                                                    CustomerOrder = _locCustomerOrderXPO,
                                                    Location = _locCustomerOrderXPO.Location,
                                                    BegInv = _locFreeItemRule.FpBegInv,
                                                    FpLocationType = _locFreeItemRule.LocationType,
                                                    FpStockType = _locFreeItemRule.StockType,
                                                    FpStockGroup = _locFreeItemRule.FpStockGroup,
                                                    FreeItemRule = _locFreeItemRule,
                                                    FpItem = _locFreeItemRule.FpItem,
                                                    FpDQty = _locFpDqty,
                                                    FpDUOM = _locFreeItemRule.FpDUOM,
                                                    FpQty = _locFpQty,
                                                    FpUOM = _locFreeItemRule.FpUOM,
                                                    FpTQty = _locTotQtyFI,
                                                    BegInvLine = _locFreeItemRule.FpBegInvLine,
                                                    //Amount
                                                    Currency = _locCustomerOrderXPO.Currency,
                                                    PriceGroup = _locCustomerOrderXPO.PriceGroup,
                                                    PriceLine = GetPriceLine(_currSession, _locCustomerOrderXPO, _locFreeItemRule.FpItem),
                                                    UAmount = GetPriceLine(_currSession, _locCustomerOrderXPO, _locFreeItemRule.FpItem).Amount2a,
                                                };
                                                _saveDataCustomerOrderFreeItem.Save();
                                                _saveDataCustomerOrderFreeItem.Session.CommitTransaction();

                                                if (_locFreeItemRule.FpBegInv != null && _locFreeItemRule.FpBegInvLine != null)
                                                {
                                                    _locFreeItemRule.FpBegInvLine.QtyOnHand = _locFreeItemRule.FpBegInvLine.QtyOnHand + _locTotQtyFI;
                                                    _locFreeItemRule.FpBegInvLine.Save();
                                                    _locFreeItemRule.FpBegInvLine.Session.CommitTransaction();
                                                }
                                            }
                                        }
                                    }
                                    #endregion Rule2

                                    //2 * 2 * 2 * 2
                                    #region Rule3
                                    if (_locFreeItemRule.RuleType == RuleType.Rule3)
                                    {
                                        double _locQtyFI = 0;
                                        double _locTotQtyFI = 0;
                                        double _locFpDqty = 0;
                                        double _locFpQty = 0;
                                        int _locMaxLoop = 0;

                                        #region Amount
                                        if (_locFreeItemRule.NetAmountRule == true)
                                        {
                                            if (_locCustomerOrderXPO.TotAmount >= _locFreeItemRule.NetTotalUAmount)
                                            {
                                                _locQtyFI = System.Math.Floor(_locCustomerOrderXPO.TotAmount / _locFreeItemRule.NetTotalUAmount);
                                            }
                                        }
                                        if (_locFreeItemRule.GrossAmountRule == true)
                                        {
                                            if (_locCustomerOrderXPO.TotUnitAmount >= _locFreeItemRule.GrossTotalUAmount)
                                            {
                                                _locQtyFI = System.Math.Floor(_locCustomerOrderXPO.TotUnitAmount / _locFreeItemRule.GrossTotalUAmount);
                                            }
                                        }
                                        #endregion Amount


                                        if (_locFreeItemRule.MaxLoop == 0)
                                        {
                                            _locMaxLoop = (int)_locQtyFI;
                                        }
                                        else
                                        {
                                            if (_locFreeItemRule.MaxLoop > (int)_locQtyFI)
                                            {
                                                _locMaxLoop = (int)_locQtyFI;
                                            }
                                            else
                                            {
                                                _locMaxLoop = _locFreeItemRule.MaxLoop;
                                            }
                                        }

                                        for (int i = 1; i <= _locMaxLoop; i++)
                                        {
                                            _locTotQtyFI = _locTotQtyFI * _locFreeItemRule.FpTQty;
                                        }

                                        if (_locFreeItemRule.FpBegInvLine != null)
                                        {
                                            if ((_locFreeItemRule.FpBegInvLine.QtyAvailable - _locFreeItemRule.FpBegInvLine.QtyOnHand) > 0)
                                            {
                                                if ((_locFreeItemRule.FpBegInvLine.QtyAvailable - _locFreeItemRule.FpBegInvLine.QtyOnHand) < _locTotQtyFI)
                                                {
                                                    _locTotQtyFI = _locFreeItemRule.FpBegInvLine.QtyAvailable - _locFreeItemRule.FpBegInvLine.QtyOnHand;
                                                }

                                                if (_locFreeItemRule.FpItem != null && _locFreeItemRule.FpDUOM != null && _locFreeItemRule.FpUOM != null)
                                                {
                                                    ItemUnitOfMeasure _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                                            (new GroupOperator(GroupOperatorType.And,
                                                                             new BinaryOperator("Item", _locFreeItemRule.FpItem),
                                                                             new BinaryOperator("UOM", _locFreeItemRule.FpUOM),
                                                                             new BinaryOperator("DefaultUOM", _locFreeItemRule.FpDUOM),
                                                                             new BinaryOperator("Active", true)));
                                                    if (_locItemUOM != null)
                                                    {
                                                        if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                                        {
                                                            _locFpDqty = _locTotQtyFI % _locItemUOM.DefaultConversion;
                                                            _locFpQty = System.Math.Floor(_locTotQtyFI / _locItemUOM.DefaultConversion);
                                                        }
                                                        else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                                        {
                                                            _locFpDqty = System.Math.Floor(_locTotQtyFI / _locItemUOM.DefaultConversion);
                                                            _locFpQty = (_locTotQtyFI % _locItemUOM.DefaultConversion) * _locItemUOM.Conversion;
                                                        }
                                                        else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                                        {
                                                            _locFpDqty = _locTotQtyFI;
                                                        }
                                                    }
                                                }
                                                else
                                                {
                                                    _locFpDqty = _locTotQtyFI;
                                                }

                                                CustomerOrderFreeItem _saveDataCustomerOrderFreeItem = new CustomerOrderFreeItem(_currSession)
                                                {
                                                    //Organization
                                                    Company = _locCustomerOrderXPO.Company,
                                                    Workplace = _locCustomerOrderXPO.Workplace,
                                                    Division = _locCustomerOrderXPO.Division,
                                                    Department = _locCustomerOrderXPO.Department,
                                                    Section = _locCustomerOrderXPO.Section,
                                                    Employee = _locCustomerOrderXPO.Employee,
                                                    //Item
                                                    CustomerOrder = _locCustomerOrderXPO,
                                                    Location = _locCustomerOrderXPO.Location,
                                                    BegInv = _locFreeItemRule.FpBegInv,
                                                    FpLocationType = _locFreeItemRule.LocationType,
                                                    FpStockType = _locFreeItemRule.StockType,
                                                    FpStockGroup = _locFreeItemRule.FpStockGroup,
                                                    FreeItemRule = _locFreeItemRule,
                                                    FpItem = _locFreeItemRule.FpItem,
                                                    FpDQty = _locFpDqty,
                                                    FpDUOM = _locFreeItemRule.FpDUOM,
                                                    FpQty = _locFpQty,
                                                    FpUOM = _locFreeItemRule.FpUOM,
                                                    FpTQty = _locTotQtyFI,
                                                    BegInvLine = _locFreeItemRule.FpBegInvLine,
                                                    //Amount
                                                    Currency = _locCustomerOrderXPO.Currency,
                                                    PriceGroup = _locCustomerOrderXPO.PriceGroup,
                                                    PriceLine = GetPriceLine(_currSession, _locCustomerOrderXPO, _locFreeItemRule.FpItem),
                                                    UAmount = GetPriceLine(_currSession, _locCustomerOrderXPO, _locFreeItemRule.FpItem).Amount2a,
                                                };
                                                _saveDataCustomerOrderFreeItem.Save();
                                                _saveDataCustomerOrderFreeItem.Session.CommitTransaction();

                                                if (_locFreeItemRule.FpBegInv != null && _locFreeItemRule.FpBegInvLine != null)
                                                {
                                                    _locFreeItemRule.FpBegInvLine.QtyOnHand = _locFreeItemRule.FpBegInvLine.QtyOnHand + _locTotQtyFI;
                                                    _locFreeItemRule.FpBegInvLine.Save();
                                                    _locFreeItemRule.FpBegInvLine.Session.CommitTransaction();
                                                }
                                            }
                                        }                                   
                                    }
                                    #endregion Rule3

                                    //2^(1-1) + 2^(2-1) + 2^(3-1) + 2^(4-1)
                                    #region Rule4
                                    if (_locFreeItemRule.RuleType == RuleType.Rule4)
                                    {
                                        double _locQtyFI = 0;
                                        double _locTotQtyFI = 0;
                                        double _locFpDqty = 0;
                                        double _locFpQty = 0;
                                        int _locMaxLoop = 0;

                                        #region Amount
                                        if (_locFreeItemRule.NetAmountRule == true)
                                        {
                                            if (_locCustomerOrderXPO.TotAmount >= _locFreeItemRule.NetTotalUAmount)
                                            {
                                                _locQtyFI = System.Math.Floor(_locCustomerOrderXPO.TotAmount / _locFreeItemRule.NetTotalUAmount);
                                            }
                                        }
                                        if (_locFreeItemRule.GrossAmountRule == true)
                                        {
                                            if (_locCustomerOrderXPO.TotUnitAmount >= _locFreeItemRule.GrossTotalUAmount)
                                            {
                                                _locQtyFI = System.Math.Floor(_locCustomerOrderXPO.TotUnitAmount / _locFreeItemRule.GrossTotalUAmount);
                                            }
                                        }
                                        #endregion Amount      

                                        if (_locFreeItemRule.MaxLoop == 0)
                                        {
                                            _locMaxLoop = (int)_locQtyFI;
                                        }
                                        else
                                        {
                                            if (_locFreeItemRule.MaxLoop > (int)_locQtyFI)
                                            {
                                                _locMaxLoop = (int)_locQtyFI;
                                            }
                                            else
                                            {
                                                _locMaxLoop = _locFreeItemRule.MaxLoop;
                                            }
                                        }

                                        for (int i = 1; i <= _locMaxLoop; i++)
                                        {

                                            _locTotQtyFI = _locTotQtyFI + System.Math.Pow(_locFreeItemRule.FpTQty, i - 1);
                                        }

                                        if(_locFreeItemRule.FpBegInvLine != null)
                                        {
                                            if ((_locFreeItemRule.FpBegInvLine.QtyAvailable - _locFreeItemRule.FpBegInvLine.QtyOnHand) > 0)
                                            {
                                                if ((_locFreeItemRule.FpBegInvLine.QtyAvailable - _locFreeItemRule.FpBegInvLine.QtyOnHand) < _locTotQtyFI)
                                                {
                                                    _locTotQtyFI = _locFreeItemRule.FpBegInvLine.QtyAvailable - _locFreeItemRule.FpBegInvLine.QtyOnHand;
                                                }

                                                if (_locFreeItemRule.FpItem != null && _locFreeItemRule.FpDUOM != null && _locFreeItemRule.FpUOM != null)
                                                {
                                                    ItemUnitOfMeasure _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                                            (new GroupOperator(GroupOperatorType.And,
                                                                             new BinaryOperator("Item", _locFreeItemRule.FpItem),
                                                                             new BinaryOperator("UOM", _locFreeItemRule.FpUOM),
                                                                             new BinaryOperator("DefaultUOM", _locFreeItemRule.FpDUOM),
                                                                             new BinaryOperator("Active", true)));
                                                    if (_locItemUOM != null)
                                                    {
                                                        if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                                        {
                                                            _locFpDqty = _locTotQtyFI % _locItemUOM.DefaultConversion;
                                                            _locFpQty = System.Math.Floor(_locTotQtyFI / _locItemUOM.DefaultConversion);
                                                        }
                                                        else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                                        {
                                                            _locFpDqty = System.Math.Floor(_locTotQtyFI / _locItemUOM.DefaultConversion);
                                                            _locFpQty = (_locTotQtyFI % _locItemUOM.DefaultConversion) * _locItemUOM.Conversion;
                                                        }
                                                        else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                                        {
                                                            _locFpDqty = _locTotQtyFI;
                                                        }
                                                    }
                                                }
                                                else
                                                {
                                                    _locFpDqty = _locTotQtyFI;
                                                }

                                                CustomerOrderFreeItem _saveDataCustomerOrderFreeItem = new CustomerOrderFreeItem(_currSession)
                                                {
                                                    //Organization
                                                    Company = _locCustomerOrderXPO.Company,
                                                    Workplace = _locCustomerOrderXPO.Workplace,
                                                    Division = _locCustomerOrderXPO.Division,
                                                    Department = _locCustomerOrderXPO.Department,
                                                    Section = _locCustomerOrderXPO.Section,
                                                    Employee = _locCustomerOrderXPO.Employee,
                                                    //Item
                                                    CustomerOrder = _locCustomerOrderXPO,
                                                    Location = _locCustomerOrderXPO.Location,
                                                    BegInv = _locFreeItemRule.FpBegInv,
                                                    FpLocationType = _locFreeItemRule.LocationType,
                                                    FpStockType = _locFreeItemRule.StockType,
                                                    FpStockGroup = _locFreeItemRule.FpStockGroup,
                                                    FreeItemRule = _locFreeItemRule,
                                                    FpItem = _locFreeItemRule.FpItem,
                                                    FpDQty = _locFpDqty,
                                                    FpDUOM = _locFreeItemRule.FpDUOM,
                                                    FpQty = _locFpQty,
                                                    FpUOM = _locFreeItemRule.FpUOM,
                                                    FpTQty = _locTotQtyFI,
                                                    BegInvLine = _locFreeItemRule.FpBegInvLine,
                                                    //Amount
                                                    Currency = _locCustomerOrderXPO.Currency,
                                                    PriceGroup = _locCustomerOrderXPO.PriceGroup,
                                                    PriceLine = GetPriceLine(_currSession, _locCustomerOrderXPO, _locFreeItemRule.FpItem),
                                                    UAmount = GetPriceLine(_currSession, _locCustomerOrderXPO, _locFreeItemRule.FpItem).Amount2a,
                                                };
                                                _saveDataCustomerOrderFreeItem.Save();
                                                _saveDataCustomerOrderFreeItem.Session.CommitTransaction();

                                                if (_locFreeItemRule.FpBegInv != null && _locFreeItemRule.FpBegInvLine != null)
                                                {
                                                    _locFreeItemRule.FpBegInvLine.QtyOnHand = _locFreeItemRule.FpBegInvLine.QtyOnHand + _locTotQtyFI;
                                                    _locFreeItemRule.FpBegInvLine.Save();
                                                    _locFreeItemRule.FpBegInvLine.Session.CommitTransaction();
                                                }
                                            }
                                        }             
                                    }
                                    #endregion Rule4
                                }
                            }
                            #endregion Header
                            #region Line
                            if (_locFreeItemRule.FreeItemType1 == FreeItemType1.Line)
                            {
                                XPQuery<CustomerOrderLine> _customerOrderLinesQuery = new XPQuery<CustomerOrderLine>(_currSession);

                                var _customerOrderLines = from col in _customerOrderLinesQuery
                                                       where (col.CustomerOrder == _locCustomerOrderXPO
                                                       && col.Status == Status.Progress
                                                       && col.Select == true
                                                       )
                                                       group col by col.Item into g
                                                       select new { Item = g.Key };

                                if (_customerOrderLines != null && _customerOrderLines.Count() > 0)
                                {
                                    foreach (var _customerOrderLine in _customerOrderLines)
                                    {
                                        #region MultiItem=false
                                        if (_locFreeItemRule.MultiItem == false)
                                        {
                                            if (_locFreeItemRule.MpItem == _customerOrderLine.Item)
                                            {
                                                //1+1+1+1+1
                                                #region Rule1
                                                if (_locFreeItemRule.RuleType == RuleType.Rule1)
                                                {
                                                    XPCollection<CustomerOrderLine> _locCustomerOrderLines = new XPCollection<CustomerOrderLine>
                                                                                                           (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                           new BinaryOperator("CustomerOrder", _locCustomerOrderXPO),
                                                                                                           new BinaryOperator("Item", _customerOrderLine.Item),
                                                                                                           new BinaryOperator("Status", Status.Progress),
                                                                                                           new BinaryOperator("FreeItemChecked", false),
                                                                                                           new BinaryOperator("Select", true)));

                                                    if (_locCustomerOrderLines != null && _locCustomerOrderLines.Count() > 0)
                                                    {
                                                        #region Foreach
                                                        foreach (CustomerOrderLine _locCustomerOrderLine in _locCustomerOrderLines)
                                                        {
                                                            double _locQtyFI = 0;
                                                            double _locTotQtyFI = 0;
                                                            double _locFpDqty = 0;
                                                            double _locFpQty = 0;
                                                            int _locMaxLoop = 0;
                                                            int _locTotMaxLoopVal = 0;

                                                            #region Amount
                                                            if (_locFreeItemRule.OpenAmount == true)
                                                            {
                                                                if (_locFreeItemRule.NetAmountRule == true)
                                                                {
                                                                    if (_locCustomerOrderLine.TAmount >= _locFreeItemRule.NetTotalUAmount)
                                                                    {
                                                                        _locQtyFI = System.Math.Floor(_locCustomerOrderLine.TAmount / _locFreeItemRule.NetTotalUAmount);
                                                                    }
                                                                }
                                                                if (_locFreeItemRule.GrossAmountRule == true)
                                                                {
                                                                    if (_locCustomerOrderLine.TUAmount >= _locFreeItemRule.GrossTotalUAmount)
                                                                    {
                                                                        _locQtyFI = System.Math.Floor(_locCustomerOrderLine.TUAmount / _locFreeItemRule.GrossTotalUAmount);
                                                                    }
                                                                }
                                                            }
                                                            #endregion Amount
                                                            #region Item
                                                            else
                                                            {
                                                                if (_locCustomerOrderLine.TQty >= _locFreeItemRule.MpTQty)
                                                                {
                                                                    _locQtyFI = System.Math.Floor(_locCustomerOrderLine.TQty / _locFreeItemRule.MpTQty);
                                                                }
                                                            }
                                                            #endregion Item

                                                            if (_locFreeItemRule.MaxLoop == 0)
                                                            {
                                                                _locMaxLoop = (int)_locQtyFI;
                                                            }
                                                            else
                                                            {
                                                                if (_locFreeItemRule.MaxLoop > (int)_locQtyFI)
                                                                {
                                                                    _locMaxLoop = (int)_locQtyFI;
                                                                }
                                                                else
                                                                {
                                                                    _locMaxLoop = _locFreeItemRule.MaxLoop;
                                                                }
                                                            }

                                                            for (int i = 1; i <= _locMaxLoop; i++)
                                                            {
                                                                _locTotMaxLoopVal = _locTotMaxLoopVal + 1;
                                                            }

                                                            _locTotQtyFI = _locTotMaxLoopVal * _locFreeItemRule.FpTQty;

                                                            if(_locFreeItemRule.FpBegInvLine != null)
                                                            {
                                                                if ((_locFreeItemRule.FpBegInvLine.QtyAvailable - _locFreeItemRule.FpBegInvLine.QtyOnHand) > 0)
                                                                {
                                                                    if ((_locFreeItemRule.FpBegInvLine.QtyAvailable - _locFreeItemRule.FpBegInvLine.QtyOnHand) < _locTotQtyFI)
                                                                    {
                                                                        _locTotQtyFI = _locFreeItemRule.FpBegInvLine.QtyAvailable - _locFreeItemRule.FpBegInvLine.QtyOnHand;
                                                                    }

                                                                    if (_locFreeItemRule.FpItem != null && _locFreeItemRule.FpDUOM != null && _locFreeItemRule.FpUOM != null)
                                                                    {
                                                                        ItemUnitOfMeasure _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                                                                (new GroupOperator(GroupOperatorType.And,
                                                                                                 new BinaryOperator("Item", _locFreeItemRule.FpItem),
                                                                                                 new BinaryOperator("UOM", _locFreeItemRule.FpUOM),
                                                                                                 new BinaryOperator("DefaultUOM", _locFreeItemRule.FpDUOM),
                                                                                                 new BinaryOperator("Active", true)));
                                                                        if (_locItemUOM != null)
                                                                        {
                                                                            if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                                                            {
                                                                                _locFpDqty = _locTotQtyFI % _locItemUOM.DefaultConversion;
                                                                                _locFpQty = System.Math.Floor(_locTotQtyFI / _locItemUOM.DefaultConversion);
                                                                            }
                                                                            else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                                                            {
                                                                                _locFpDqty = System.Math.Floor(_locTotQtyFI / _locItemUOM.DefaultConversion);
                                                                                _locFpQty = (_locTotQtyFI % _locItemUOM.DefaultConversion) * _locItemUOM.Conversion;
                                                                            }
                                                                            else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                                                            {
                                                                                _locFpDqty = _locTotQtyFI;
                                                                            }
                                                                        }
                                                                    }
                                                                    else
                                                                    {
                                                                        _locFpDqty = _locTotQtyFI;
                                                                    }

                                                                    CustomerOrderFreeItem _saveDataCustomerOrderFreeItem = new CustomerOrderFreeItem(_currSession)
                                                                    {
                                                                        //Organization
                                                                        Company = _locCustomerOrderXPO.Company,
                                                                        Workplace = _locCustomerOrderXPO.Workplace,
                                                                        Division = _locCustomerOrderXPO.Division,
                                                                        Department = _locCustomerOrderXPO.Department,
                                                                        Section = _locCustomerOrderXPO.Section,
                                                                        Employee = _locCustomerOrderXPO.Employee,
                                                                        //Item
                                                                        CustomerOrder = _locCustomerOrderXPO,
                                                                        CustomerOrderLine = _locCustomerOrderLine,
                                                                        Location = _locCustomerOrderXPO.Location,
                                                                        BegInv = _locFreeItemRule.FpBegInv,
                                                                        FpLocationType = _locFreeItemRule.LocationType,
                                                                        FpStockType = _locFreeItemRule.StockType,
                                                                        FpStockGroup = _locFreeItemRule.FpStockGroup,
                                                                        FreeItemRule = _locFreeItemRule,
                                                                        FpItem = _locFreeItemRule.FpItem,
                                                                        FpDQty = _locFpDqty,
                                                                        FpDUOM = _locFreeItemRule.FpDUOM,
                                                                        FpQty = _locFpQty,
                                                                        FpUOM = _locFreeItemRule.FpUOM,
                                                                        FpTQty = _locTotQtyFI,
                                                                        BegInvLine = _locFreeItemRule.FpBegInvLine,
                                                                        //Amount
                                                                        Currency = _locCustomerOrderXPO.Currency,
                                                                        PriceGroup = _locCustomerOrderXPO.PriceGroup,
                                                                        PriceLine = GetPriceLine(_currSession, _locCustomerOrderXPO, _locFreeItemRule.FpItem),
                                                                        UAmount = GetPriceLine(_currSession, _locCustomerOrderXPO, _locFreeItemRule.FpItem).Amount2a,
                                                                    };
                                                                    _saveDataCustomerOrderFreeItem.Save();
                                                                    _saveDataCustomerOrderFreeItem.Session.CommitTransaction();

                                                                    _locCustomerOrderLine.FreeItemChecked = true;
                                                                    _locCustomerOrderLine.Save();
                                                                    _locCustomerOrderLine.Session.CommitTransaction();

                                                                    if (_locFreeItemRule.FpBegInv != null && _locFreeItemRule.FpBegInvLine != null)
                                                                    {
                                                                        _locFreeItemRule.FpBegInvLine.QtyOnHand = _locFreeItemRule.FpBegInvLine.QtyOnHand + _locTotQtyFI;
                                                                        _locFreeItemRule.FpBegInvLine.Save();
                                                                        _locFreeItemRule.FpBegInvLine.Session.CommitTransaction();
                                                                    }
                                                                }
                                                            }                                                            
                                                        }
                                                        #endregion Foreach
                                                    }
                                                }

                                                #endregion Rule1

                                                //1, 2+1, 3+1, 4+1
                                                #region Rule2
                                                if (_locFreeItemRule.RuleType == RuleType.Rule2)
                                                {
                                                    XPCollection<CustomerOrderLine> _locCustomerOrderLines = new XPCollection<CustomerOrderLine>
                                                                                                           (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                           new BinaryOperator("CustomerOrder", _locCustomerOrderXPO),
                                                                                                           new BinaryOperator("Item", _customerOrderLine.Item),
                                                                                                           new BinaryOperator("Status", Status.Progress),
                                                                                                           new BinaryOperator("FreeItemChecked", false),
                                                                                                           new BinaryOperator("Select", true)));

                                                    if (_locCustomerOrderLines != null && _locCustomerOrderLines.Count() > 0)
                                                    {

                                                        foreach (CustomerOrderLine _locCustomerOrderLine in _locCustomerOrderLines)
                                                        {
                                                            double _locQtyFI = 0;
                                                            double _locTotQtyFI = 0;
                                                            double _locFpDqty = 0;
                                                            double _locFpQty = 0;
                                                            int _locMaxLoop = 0;

                                                            #region Amount
                                                            if (_locFreeItemRule.OpenAmount == true)
                                                            {
                                                                if (_locFreeItemRule.NetAmountRule == true)
                                                                {
                                                                    if (_locCustomerOrderLine.TAmount >= _locFreeItemRule.NetTotalUAmount)
                                                                    {
                                                                        _locQtyFI = System.Math.Floor(_locCustomerOrderLine.TAmount / _locFreeItemRule.NetTotalUAmount);
                                                                    }
                                                                }
                                                                if (_locFreeItemRule.GrossAmountRule == true)
                                                                {
                                                                    if (_locCustomerOrderLine.TUAmount >= _locFreeItemRule.GrossTotalUAmount)
                                                                    {
                                                                        _locQtyFI = System.Math.Floor(_locCustomerOrderLine.TUAmount / _locFreeItemRule.GrossTotalUAmount);
                                                                    }
                                                                }
                                                            }
                                                            #endregion Amount
                                                            #region Item
                                                            else
                                                            {
                                                                if (_locCustomerOrderLine.TQty >= _locFreeItemRule.MpTQty)
                                                                {
                                                                    _locQtyFI = System.Math.Floor(_locCustomerOrderLine.TQty / _locFreeItemRule.MpTQty);
                                                                }
                                                            }
                                                            #endregion Item

                                                            if (_locFreeItemRule.MaxLoop == 0)
                                                            {
                                                                _locMaxLoop = (int)_locQtyFI;
                                                            }
                                                            else
                                                            {
                                                                if (_locFreeItemRule.MaxLoop > (int)_locQtyFI)
                                                                {
                                                                    _locMaxLoop = (int)_locQtyFI;
                                                                }
                                                                else
                                                                {
                                                                    _locMaxLoop = _locFreeItemRule.MaxLoop;
                                                                }
                                                            }

                                                            for (int i = 1; i <= _locMaxLoop; i++)
                                                            {
                                                                if (i == 1)
                                                                {
                                                                    _locTotQtyFI = _locFreeItemRule.FpTQty;
                                                                }
                                                                else
                                                                {
                                                                    _locTotQtyFI = _locTotQtyFI + (i + _locFreeItemRule.FpTQty);
                                                                }
                                                            }

                                                            if (_locFreeItemRule.FpBegInvLine != null)
                                                            {
                                                                if ((_locFreeItemRule.FpBegInvLine.QtyAvailable - _locFreeItemRule.FpBegInvLine.QtyOnHand) > 0)
                                                                {
                                                                    if ((_locFreeItemRule.FpBegInvLine.QtyAvailable - _locFreeItemRule.FpBegInvLine.QtyOnHand) < _locTotQtyFI)
                                                                    {
                                                                        _locTotQtyFI = _locFreeItemRule.FpBegInvLine.QtyAvailable - _locFreeItemRule.FpBegInvLine.QtyOnHand;
                                                                    }

                                                                    if (_locFreeItemRule.FpItem != null && _locFreeItemRule.FpDUOM != null && _locFreeItemRule.FpUOM != null)
                                                                    {
                                                                        ItemUnitOfMeasure _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                                                                (new GroupOperator(GroupOperatorType.And,
                                                                                                 new BinaryOperator("Item", _locFreeItemRule.FpItem),
                                                                                                 new BinaryOperator("UOM", _locFreeItemRule.FpUOM),
                                                                                                 new BinaryOperator("DefaultUOM", _locFreeItemRule.FpDUOM),
                                                                                                 new BinaryOperator("Active", true)));
                                                                        if (_locItemUOM != null)
                                                                        {
                                                                            if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                                                            {
                                                                                _locFpDqty = _locTotQtyFI % _locItemUOM.DefaultConversion;
                                                                                _locFpQty = System.Math.Floor(_locTotQtyFI / _locItemUOM.DefaultConversion);
                                                                            }
                                                                            else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                                                            {
                                                                                _locFpDqty = System.Math.Floor(_locTotQtyFI / _locItemUOM.DefaultConversion);
                                                                                _locFpQty = (_locTotQtyFI % _locItemUOM.DefaultConversion) * _locItemUOM.Conversion;
                                                                            }
                                                                            else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                                                            {
                                                                                _locFpDqty = _locTotQtyFI;
                                                                            }
                                                                        }
                                                                    }
                                                                    else
                                                                    {
                                                                        _locFpDqty = _locTotQtyFI;
                                                                    }

                                                                    CustomerOrderFreeItem _saveDataCustomerOrderFreeItem = new CustomerOrderFreeItem(_currSession)
                                                                    {
                                                                        //Organization
                                                                        Company = _locCustomerOrderXPO.Company,
                                                                        Workplace = _locCustomerOrderXPO.Workplace,
                                                                        Division = _locCustomerOrderXPO.Division,
                                                                        Department = _locCustomerOrderXPO.Department,
                                                                        Section = _locCustomerOrderXPO.Section,
                                                                        Employee = _locCustomerOrderXPO.Employee,
                                                                        //Item
                                                                        CustomerOrder = _locCustomerOrderXPO,
                                                                        CustomerOrderLine = _locCustomerOrderLine,
                                                                        Location = _locCustomerOrderXPO.Location,
                                                                        BegInv = _locFreeItemRule.FpBegInv,
                                                                        FpLocationType = _locFreeItemRule.LocationType,
                                                                        FpStockType = _locFreeItemRule.StockType,
                                                                        FpStockGroup = _locFreeItemRule.FpStockGroup,
                                                                        FreeItemRule = _locFreeItemRule,
                                                                        FpItem = _locFreeItemRule.FpItem,
                                                                        FpDQty = _locFpDqty,
                                                                        FpDUOM = _locFreeItemRule.FpDUOM,
                                                                        FpQty = _locFpQty,
                                                                        FpUOM = _locFreeItemRule.FpUOM,
                                                                        FpTQty = _locTotQtyFI,
                                                                        BegInvLine = _locFreeItemRule.FpBegInvLine,
                                                                        //Amount
                                                                        Currency = _locCustomerOrderXPO.Currency,
                                                                        PriceGroup = _locCustomerOrderXPO.PriceGroup,
                                                                        PriceLine = GetPriceLine(_currSession, _locCustomerOrderXPO, _locFreeItemRule.FpItem),
                                                                        UAmount = GetPriceLine(_currSession, _locCustomerOrderXPO, _locFreeItemRule.FpItem).Amount2a,
                                                                    };
                                                                    _saveDataCustomerOrderFreeItem.Save();
                                                                    _saveDataCustomerOrderFreeItem.Session.CommitTransaction();

                                                                    _locCustomerOrderLine.FreeItemChecked = true;
                                                                    _locCustomerOrderLine.Save();
                                                                    _locCustomerOrderLine.Session.CommitTransaction();

                                                                    if (_locFreeItemRule.FpBegInv != null && _locFreeItemRule.FpBegInvLine != null)
                                                                    {
                                                                        _locFreeItemRule.FpBegInvLine.QtyOnHand = _locFreeItemRule.FpBegInvLine.QtyOnHand + _locTotQtyFI;
                                                                        _locFreeItemRule.FpBegInvLine.Save();
                                                                        _locFreeItemRule.FpBegInvLine.Session.CommitTransaction();
                                                                    }
                                                                }
                                                            }                                            
                                                        }
                                                    }
                                                }
                                                #endregion Rule2

                                                //2 * 2 * 2 * 2
                                                #region Rule3
                                                if (_locFreeItemRule.RuleType == RuleType.Rule3)
                                                {
                                                    XPCollection<CustomerOrderLine> _locCustomerOrderLines = new XPCollection<CustomerOrderLine>
                                                                                                           (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                           new BinaryOperator("CustomerOrder", _locCustomerOrderXPO),
                                                                                                           new BinaryOperator("Item", _customerOrderLine.Item),
                                                                                                           new BinaryOperator("Status", Status.Progress),
                                                                                                           new BinaryOperator("FreeItemChecked", false),
                                                                                                           new BinaryOperator("Select", true)));

                                                    if (_locCustomerOrderLines != null && _locCustomerOrderLines.Count() > 0)
                                                    {
                                                        foreach (CustomerOrderLine _locCustomerOrderLine in _locCustomerOrderLines)
                                                        {
                                                            double _locQtyFI = 0;
                                                            double _locTotQtyFI = 0;
                                                            double _locFpDqty = 0;
                                                            double _locFpQty = 0;
                                                            int _locMaxLoop = 0;

                                                            #region Amount
                                                            if (_locFreeItemRule.OpenAmount == true)
                                                            {
                                                                if (_locFreeItemRule.NetAmountRule == true)
                                                                {
                                                                    if (_locCustomerOrderLine.TAmount >= _locFreeItemRule.NetTotalUAmount)
                                                                    {
                                                                        _locQtyFI = System.Math.Floor(_locCustomerOrderLine.TAmount / _locFreeItemRule.NetTotalUAmount);
                                                                    }
                                                                }
                                                                if (_locFreeItemRule.GrossAmountRule == true)
                                                                {
                                                                    if (_locCustomerOrderLine.TUAmount >= _locFreeItemRule.GrossTotalUAmount)
                                                                    {
                                                                        _locQtyFI = System.Math.Floor(_locCustomerOrderLine.TUAmount / _locFreeItemRule.GrossTotalUAmount);
                                                                    }
                                                                }
                                                            }
                                                            #endregion Amount
                                                            #region Item
                                                            else
                                                            {
                                                                if (_locCustomerOrderLine.TQty >= _locFreeItemRule.MpTQty)
                                                                {
                                                                    _locQtyFI = System.Math.Floor(_locCustomerOrderLine.TQty / _locFreeItemRule.MpTQty);
                                                                }
                                                            }
                                                            #endregion Item

                                                            if (_locFreeItemRule.MaxLoop == 0)
                                                            {
                                                                _locMaxLoop = (int)_locQtyFI;
                                                            }
                                                            else
                                                            {
                                                                if (_locFreeItemRule.MaxLoop > (int)_locQtyFI)
                                                                {
                                                                    _locMaxLoop = (int)_locQtyFI;
                                                                }
                                                                else
                                                                {
                                                                    _locMaxLoop = _locFreeItemRule.MaxLoop;
                                                                }
                                                            }

                                                            for (int i = 1; i <= _locMaxLoop; i++)
                                                            {
                                                                _locTotQtyFI = _locTotQtyFI * _locFreeItemRule.FpTQty;
                                                            }

                                                            if (_locFreeItemRule.FpBegInvLine != null)
                                                            {
                                                                if ((_locFreeItemRule.FpBegInvLine.QtyAvailable - _locFreeItemRule.FpBegInvLine.QtyOnHand) > 0)
                                                                {
                                                                    if ((_locFreeItemRule.FpBegInvLine.QtyAvailable - _locFreeItemRule.FpBegInvLine.QtyOnHand) < _locTotQtyFI)
                                                                    {
                                                                        _locTotQtyFI = _locFreeItemRule.FpBegInvLine.QtyAvailable - _locFreeItemRule.FpBegInvLine.QtyOnHand;
                                                                    }

                                                                    if (_locFreeItemRule.FpItem != null && _locFreeItemRule.FpDUOM != null && _locFreeItemRule.FpUOM != null)
                                                                    {
                                                                        ItemUnitOfMeasure _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                                                                (new GroupOperator(GroupOperatorType.And,
                                                                                                 new BinaryOperator("Item", _locFreeItemRule.FpItem),
                                                                                                 new BinaryOperator("UOM", _locFreeItemRule.FpUOM),
                                                                                                 new BinaryOperator("DefaultUOM", _locFreeItemRule.FpDUOM),
                                                                                                 new BinaryOperator("Active", true)));
                                                                        if (_locItemUOM != null)
                                                                        {
                                                                            if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                                                            {
                                                                                _locFpDqty = _locTotQtyFI % _locItemUOM.DefaultConversion;
                                                                                _locFpQty = System.Math.Floor(_locTotQtyFI / _locItemUOM.DefaultConversion);
                                                                            }
                                                                            else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                                                            {
                                                                                _locFpDqty = System.Math.Floor(_locTotQtyFI / _locItemUOM.DefaultConversion);
                                                                                _locFpQty = (_locTotQtyFI % _locItemUOM.DefaultConversion) * _locItemUOM.Conversion;
                                                                            }
                                                                            else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                                                            {
                                                                                _locFpDqty = _locTotQtyFI;
                                                                            }
                                                                        }
                                                                    }
                                                                    else
                                                                    {
                                                                        _locFpDqty = _locTotQtyFI;
                                                                    }

                                                                    CustomerOrderFreeItem _saveDataCustomerOrderFreeItem = new CustomerOrderFreeItem(_currSession)
                                                                    {
                                                                        //Organization
                                                                        Company = _locCustomerOrderXPO.Company,
                                                                        Workplace = _locCustomerOrderXPO.Workplace,
                                                                        Division = _locCustomerOrderXPO.Division,
                                                                        Department = _locCustomerOrderXPO.Department,
                                                                        Section = _locCustomerOrderXPO.Section,
                                                                        Employee = _locCustomerOrderXPO.Employee,
                                                                        //Item
                                                                        CustomerOrder = _locCustomerOrderXPO,
                                                                        CustomerOrderLine = _locCustomerOrderLine,
                                                                        Location = _locCustomerOrderXPO.Location,
                                                                        BegInv = _locFreeItemRule.FpBegInv,
                                                                        FpLocationType = _locFreeItemRule.LocationType,
                                                                        FpStockType = _locFreeItemRule.StockType,
                                                                        FpStockGroup = _locFreeItemRule.FpStockGroup,
                                                                        FreeItemRule = _locFreeItemRule,
                                                                        FpItem = _locFreeItemRule.FpItem,
                                                                        FpDQty = _locFpDqty,
                                                                        FpDUOM = _locFreeItemRule.FpDUOM,
                                                                        FpQty = _locFpQty,
                                                                        FpUOM = _locFreeItemRule.FpUOM,
                                                                        FpTQty = _locTotQtyFI,
                                                                        BegInvLine = _locFreeItemRule.FpBegInvLine,
                                                                        //Amount
                                                                        Currency = _locCustomerOrderXPO.Currency,
                                                                        PriceGroup = _locCustomerOrderXPO.PriceGroup,
                                                                        PriceLine = GetPriceLine(_currSession, _locCustomerOrderXPO, _locFreeItemRule.FpItem),
                                                                        UAmount = GetPriceLine(_currSession, _locCustomerOrderXPO, _locFreeItemRule.FpItem).Amount2a,
                                                                    };
                                                                    _saveDataCustomerOrderFreeItem.Save();
                                                                    _saveDataCustomerOrderFreeItem.Session.CommitTransaction();

                                                                    _locCustomerOrderLine.FreeItemChecked = true;
                                                                    _locCustomerOrderLine.Save();
                                                                    _locCustomerOrderLine.Session.CommitTransaction();

                                                                    if (_locFreeItemRule.FpBegInv != null && _locFreeItemRule.FpBegInvLine != null)
                                                                    {
                                                                        _locFreeItemRule.FpBegInvLine.QtyOnHand = _locFreeItemRule.FpBegInvLine.QtyOnHand + _locTotQtyFI;
                                                                        _locFreeItemRule.FpBegInvLine.Save();
                                                                        _locFreeItemRule.FpBegInvLine.Session.CommitTransaction();
                                                                    }
                                                                }
                                                            }                          
                                                        }
                                                    }
                                                }
                                                #endregion Rule3

                                                //2^(1-1) + 2^(2-1) + 2^(3-1) + 2^(4-1)
                                                #region Rule4
                                                if (_locFreeItemRule.RuleType == RuleType.Rule4)
                                                {
                                                    XPCollection<CustomerOrderLine> _locCustomerOrderLines = new XPCollection<CustomerOrderLine>
                                                                                                           (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                           new BinaryOperator("CustomerOrder", _locCustomerOrderXPO),
                                                                                                           new BinaryOperator("Item", _customerOrderLine.Item),
                                                                                                           new BinaryOperator("Status", Status.Progress),
                                                                                                           new BinaryOperator("FreeItemChecked", false),
                                                                                                           new BinaryOperator("Select", true)));

                                                    if (_locCustomerOrderLines != null && _locCustomerOrderLines.Count() > 0)
                                                    {
                                                        foreach (CustomerOrderLine _locCustomerOrderLine in _locCustomerOrderLines)
                                                        {
                                                            double _locQtyFI = 0;
                                                            double _locTotQtyFI = 0;
                                                            double _locFpDqty = 0;
                                                            double _locFpQty = 0;
                                                            int _locMaxLoop = 0;

                                                            #region Amount
                                                            if (_locFreeItemRule.OpenAmount == true)
                                                            {
                                                                if (_locFreeItemRule.NetAmountRule == true)
                                                                {
                                                                    if (_locCustomerOrderLine.TAmount >= _locFreeItemRule.NetTotalUAmount)
                                                                    {
                                                                        _locQtyFI = System.Math.Floor(_locCustomerOrderLine.TAmount / _locFreeItemRule.NetTotalUAmount);
                                                                    }
                                                                }
                                                                if (_locFreeItemRule.GrossAmountRule == true)
                                                                {
                                                                    if (_locCustomerOrderLine.TUAmount >= _locFreeItemRule.GrossTotalUAmount)
                                                                    {
                                                                        _locQtyFI = System.Math.Floor(_locCustomerOrderLine.TUAmount / _locFreeItemRule.GrossTotalUAmount);
                                                                    }
                                                                }
                                                            }
                                                            #endregion Amount
                                                            #region Item
                                                            else
                                                            {
                                                                if (_locCustomerOrderLine.TQty >= _locFreeItemRule.MpTQty)
                                                                {
                                                                    _locQtyFI = System.Math.Floor(_locCustomerOrderLine.TQty / _locFreeItemRule.MpTQty);
                                                                }
                                                            }
                                                            #endregion Item

                                                            if (_locFreeItemRule.MaxLoop == 0)
                                                            {
                                                                _locMaxLoop = (int)_locQtyFI;
                                                            }
                                                            else
                                                            {
                                                                if (_locFreeItemRule.MaxLoop > (int)_locQtyFI)
                                                                {
                                                                    _locMaxLoop = (int)_locQtyFI;
                                                                }
                                                                else
                                                                {
                                                                    _locMaxLoop = _locFreeItemRule.MaxLoop;
                                                                }
                                                            }

                                                            for (int i = 1; i <= _locMaxLoop; i++)
                                                            {

                                                                _locTotQtyFI = _locTotQtyFI + System.Math.Pow(_locFreeItemRule.FpTQty, i - 1);
                                                            }

                                                            if (_locFreeItemRule.FpBegInvLine != null)
                                                            {
                                                                if ((_locFreeItemRule.FpBegInvLine.QtyAvailable - _locFreeItemRule.FpBegInvLine.QtyOnHand) > 0)
                                                                {
                                                                    if ((_locFreeItemRule.FpBegInvLine.QtyAvailable - _locFreeItemRule.FpBegInvLine.QtyOnHand) < _locTotQtyFI)
                                                                    {
                                                                        _locTotQtyFI = _locFreeItemRule.FpBegInvLine.QtyAvailable - _locFreeItemRule.FpBegInvLine.QtyOnHand;
                                                                    }

                                                                    if (_locFreeItemRule.FpItem != null && _locFreeItemRule.FpDUOM != null && _locFreeItemRule.FpUOM != null)
                                                                    {
                                                                        ItemUnitOfMeasure _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                                                                (new GroupOperator(GroupOperatorType.And,
                                                                                                 new BinaryOperator("Item", _locFreeItemRule.FpItem),
                                                                                                 new BinaryOperator("UOM", _locFreeItemRule.FpUOM),
                                                                                                 new BinaryOperator("DefaultUOM", _locFreeItemRule.FpDUOM),
                                                                                                 new BinaryOperator("Active", true)));
                                                                        if (_locItemUOM != null)
                                                                        {
                                                                            if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                                                            {
                                                                                _locFpDqty = _locTotQtyFI % _locItemUOM.DefaultConversion;
                                                                                _locFpQty = System.Math.Floor(_locTotQtyFI / _locItemUOM.DefaultConversion);
                                                                            }
                                                                            else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                                                            {
                                                                                _locFpDqty = System.Math.Floor(_locTotQtyFI / _locItemUOM.DefaultConversion);
                                                                                _locFpQty = (_locTotQtyFI % _locItemUOM.DefaultConversion) * _locItemUOM.Conversion;
                                                                            }
                                                                            else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                                                            {
                                                                                _locFpDqty = _locTotQtyFI;
                                                                            }
                                                                        }
                                                                    }
                                                                    else
                                                                    {
                                                                        _locFpDqty = _locTotQtyFI;
                                                                    }

                                                                    CustomerOrderFreeItem _saveDataCustomerOrderFreeItem = new CustomerOrderFreeItem(_currSession)
                                                                    {
                                                                        //Organization
                                                                        Company = _locCustomerOrderXPO.Company,
                                                                        Workplace = _locCustomerOrderXPO.Workplace,
                                                                        Division = _locCustomerOrderXPO.Division,
                                                                        Department = _locCustomerOrderXPO.Department,
                                                                        Section = _locCustomerOrderXPO.Section,
                                                                        Employee = _locCustomerOrderXPO.Employee,
                                                                        //Item
                                                                        CustomerOrder = _locCustomerOrderXPO,
                                                                        CustomerOrderLine = _locCustomerOrderLine,
                                                                        Location = _locCustomerOrderXPO.Location,
                                                                        BegInv = _locFreeItemRule.FpBegInv,
                                                                        FpLocationType = _locFreeItemRule.LocationType,
                                                                        FpStockType = _locFreeItemRule.StockType,
                                                                        FpStockGroup = _locFreeItemRule.FpStockGroup,
                                                                        FreeItemRule = _locFreeItemRule,
                                                                        FpItem = _locFreeItemRule.FpItem,
                                                                        FpDQty = _locFpDqty,
                                                                        FpDUOM = _locFreeItemRule.FpDUOM,
                                                                        FpQty = _locFpQty,
                                                                        FpUOM = _locFreeItemRule.FpUOM,
                                                                        FpTQty = _locTotQtyFI,
                                                                        BegInvLine = _locFreeItemRule.FpBegInvLine,
                                                                        //Amount
                                                                        Currency = _locCustomerOrderXPO.Currency,
                                                                        PriceGroup = _locCustomerOrderXPO.PriceGroup,
                                                                        PriceLine = GetPriceLine(_currSession, _locCustomerOrderXPO, _locFreeItemRule.FpItem),
                                                                        UAmount = GetPriceLine(_currSession, _locCustomerOrderXPO, _locFreeItemRule.FpItem).Amount2a,
                                                                    };
                                                                    _saveDataCustomerOrderFreeItem.Save();
                                                                    _saveDataCustomerOrderFreeItem.Session.CommitTransaction();

                                                                    _locCustomerOrderLine.FreeItemChecked = true;
                                                                    _locCustomerOrderLine.Save();
                                                                    _locCustomerOrderLine.Session.CommitTransaction();

                                                                    if (_locFreeItemRule.FpBegInv != null && _locFreeItemRule.FpBegInvLine != null)
                                                                    {
                                                                        _locFreeItemRule.FpBegInvLine.QtyOnHand = _locFreeItemRule.FpBegInvLine.QtyOnHand + _locTotQtyFI;
                                                                        _locFreeItemRule.FpBegInvLine.Save();
                                                                        _locFreeItemRule.FpBegInvLine.Session.CommitTransaction();
                                                                    }
                                                                }
                                                            }                                                           
                                                        }
                                                    }
                                                }
                                                #endregion Rule4
                                            }
                                        }
                                        #endregion MultiItem=false
                                        #region MultiItem=true
                                        else
                                        {
                                            CustomerOrderLine _locComboCustomerOrderLine = _currSession.FindObject<CustomerOrderLine>
                                                                                                        (new GroupOperator(GroupOperatorType.And,
                                                                                                        new BinaryOperator("CustomerOrder", _locCustomerOrderXPO),
                                                                                                        new BinaryOperator("Item", _locFreeItemRule.CpItem),
                                                                                                        new BinaryOperator("Status", Status.Progress),
                                                                                                        new BinaryOperator("FreeItemChecked", false),
                                                                                                        new BinaryOperator("Select", true)));

                                            CustomerOrderLine _locMainCustomerOrderLine = _currSession.FindObject<CustomerOrderLine>
                                                                                                       (new GroupOperator(GroupOperatorType.And,
                                                                                                       new BinaryOperator("CustomerOrder", _locCustomerOrderXPO),
                                                                                                       new BinaryOperator("Item", _locFreeItemRule.MpItem),
                                                                                                       new BinaryOperator("Status", Status.Progress),
                                                                                                       new BinaryOperator("FreeItemChecked", false),
                                                                                                       new BinaryOperator("Select", true)));

                                            if (_locMainCustomerOrderLine != null && _locComboCustomerOrderLine != null)
                                            {
                                                double _locComboQtyCI = 0;
                                                double _locComboTotQtyFI = 0;
                                                double _locComboCpDqty = 0;
                                                double _locComboCpQty = 0;
                                                int _locComboMaxLoop = 0;
                                                int _locComboTotMaxLoopVal = 0;

                                                double _locMainQtyFI = 0;
                                                double _locTotQtyFI = 0;
                                                double _locFpDqty = 0;
                                                double _locFpQty = 0;
                                                int _locMaxLoop = 0;
                                                int _locTotMaxLoopVal = 0;

                                                if (_locMainCustomerOrderLine.TQty >= _locFreeItemRule.MpTQty && _locComboCustomerOrderLine.TQty >= _locFreeItemRule.CpTQty)
                                                {
                                                    _locMainQtyFI = System.Math.Floor(_locMainCustomerOrderLine.TQty / _locFreeItemRule.MpTQty);
                                                    _locComboQtyCI = System.Math.Floor(_locComboCustomerOrderLine.TQty / _locFreeItemRule.CpTQty);

                                                    if (_locComboQtyCI < _locMainQtyFI)
                                                    {
                                                        #region ComboPromoItem

                                                        //1+1+1+1+1
                                                        #region Rule1
                                                        if (_locFreeItemRule.RuleType == RuleType.Rule1)
                                                        {

                                                            if (_locFreeItemRule.MaxLoop == 0)
                                                            {
                                                                _locComboMaxLoop = (int)_locComboQtyCI;
                                                            }
                                                            else
                                                            {
                                                                if (_locFreeItemRule.MaxLoop > (int)_locComboQtyCI)
                                                                {
                                                                    _locComboMaxLoop = (int)_locComboQtyCI;
                                                                }
                                                                else
                                                                {
                                                                    _locComboMaxLoop = _locFreeItemRule.MaxLoop;
                                                                }
                                                            }

                                                            for (int i = 1; i <= _locComboMaxLoop; i++)
                                                            {
                                                                _locComboTotMaxLoopVal = _locComboTotMaxLoopVal + 1;
                                                            }

                                                            _locComboTotQtyFI = _locComboTotMaxLoopVal * _locFreeItemRule.FpTQty;

                                                            if(_locFreeItemRule.FpBegInvLine != null)
                                                            {
                                                                if ((_locFreeItemRule.FpBegInvLine.QtyAvailable - _locFreeItemRule.FpBegInvLine.QtyOnHand) > 0)
                                                                {
                                                                    if ((_locFreeItemRule.FpBegInvLine.QtyAvailable - _locFreeItemRule.FpBegInvLine.QtyOnHand) < _locComboTotQtyFI)
                                                                    {
                                                                        _locComboTotQtyFI = _locFreeItemRule.FpBegInvLine.QtyAvailable - _locFreeItemRule.FpBegInvLine.QtyOnHand;
                                                                    }

                                                                    if (_locFreeItemRule.FpItem != null && _locFreeItemRule.FpDUOM != null && _locFreeItemRule.FpUOM != null)
                                                                    {
                                                                        ItemUnitOfMeasure _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                                                                (new GroupOperator(GroupOperatorType.And,
                                                                                                 new BinaryOperator("Item", _locFreeItemRule.FpItem),
                                                                                                 new BinaryOperator("UOM", _locFreeItemRule.FpUOM),
                                                                                                 new BinaryOperator("DefaultUOM", _locFreeItemRule.FpDUOM),
                                                                                                 new BinaryOperator("Active", true)));
                                                                        if (_locItemUOM != null)
                                                                        {
                                                                            if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                                                            {
                                                                                _locComboCpDqty = _locComboTotQtyFI % _locItemUOM.DefaultConversion;
                                                                                _locComboCpQty = System.Math.Floor(_locComboTotQtyFI / _locItemUOM.DefaultConversion);
                                                                            }
                                                                            else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                                                            {
                                                                                _locComboCpDqty = System.Math.Floor(_locComboTotQtyFI / _locItemUOM.DefaultConversion);
                                                                                _locComboCpQty = (_locComboTotQtyFI % _locItemUOM.DefaultConversion) * _locItemUOM.Conversion;
                                                                            }
                                                                            else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                                                            {
                                                                                _locComboCpDqty = _locComboTotQtyFI;
                                                                            }
                                                                        }
                                                                    }
                                                                    else
                                                                    {
                                                                        _locComboCpDqty = _locComboTotQtyFI;
                                                                    }

                                                                    CustomerOrderFreeItem _saveDataCustomerOrderFreeItem = new CustomerOrderFreeItem(_currSession)
                                                                    {
                                                                        //Organization
                                                                        Company = _locCustomerOrderXPO.Company,
                                                                        Workplace = _locCustomerOrderXPO.Workplace,
                                                                        Division = _locCustomerOrderXPO.Division,
                                                                        Department = _locCustomerOrderXPO.Department,
                                                                        Section = _locCustomerOrderXPO.Section,
                                                                        Employee = _locCustomerOrderXPO.Employee,
                                                                        //Item
                                                                        CustomerOrder = _locCustomerOrderXPO,
                                                                        Location = _locFreeItemRule.Location,
                                                                        FreeItemRule = _locFreeItemRule,
                                                                        BegInv = _locFreeItemRule.FpBegInv,
                                                                        FpLocationType = _locFreeItemRule.LocationType,
                                                                        FpStockType = _locFreeItemRule.StockType,
                                                                        FpItem = _locFreeItemRule.FpItem,
                                                                        FpDQty = _locComboCpDqty,
                                                                        FpDUOM = _locFreeItemRule.FpDUOM,
                                                                        FpQty = _locComboCpQty,
                                                                        FpUOM = _locFreeItemRule.FpUOM,
                                                                        FpTQty = _locComboTotQtyFI,
                                                                        FpStockGroup = _locFreeItemRule.FpStockGroup,
                                                                        BegInvLine = _locFreeItemRule.FpBegInvLine,
                                                                        //Amount
                                                                        Currency = _locCustomerOrderXPO.Currency,
                                                                        PriceGroup = _locCustomerOrderXPO.PriceGroup,
                                                                        PriceLine = GetPriceLine(_currSession, _locCustomerOrderXPO, _locFreeItemRule.FpItem),
                                                                        UAmount = GetPriceLine(_currSession, _locCustomerOrderXPO, _locFreeItemRule.FpItem).Amount2a,
                                                                    };
                                                                    _saveDataCustomerOrderFreeItem.Save();
                                                                    _saveDataCustomerOrderFreeItem.Session.CommitTransaction();

                                                                    _locComboCustomerOrderLine.FreeItemChecked = true;
                                                                    _locComboCustomerOrderLine.Save();
                                                                    _locComboCustomerOrderLine.Session.CommitTransaction();

                                                                    _locMainCustomerOrderLine.FreeItemChecked = true;
                                                                    _locMainCustomerOrderLine.Save();
                                                                    _locMainCustomerOrderLine.Session.CommitTransaction();

                                                                    if (_locFreeItemRule.FpBegInv != null && _locFreeItemRule.FpBegInvLine != null)
                                                                    {
                                                                        _locFreeItemRule.FpBegInvLine.QtyOnHand = _locFreeItemRule.FpBegInvLine.QtyOnHand + _locComboTotQtyFI;
                                                                        _locFreeItemRule.FpBegInvLine.Save();
                                                                        _locFreeItemRule.FpBegInvLine.Session.CommitTransaction();
                                                                    }
                                                                }
                                                            }                    
                                                        }

                                                        #endregion Rule1

                                                        //1, 2+1, 3+1, 4+1
                                                        #region Rule2

                                                        if (_locFreeItemRule.RuleType == RuleType.Rule2)
                                                        {
                                                            if (_locFreeItemRule.MaxLoop == 0)
                                                            {
                                                                _locComboMaxLoop = (int)_locComboQtyCI;
                                                            }
                                                            else
                                                            {
                                                                if (_locFreeItemRule.MaxLoop > (int)_locComboQtyCI)
                                                                {
                                                                    _locComboMaxLoop = (int)_locComboQtyCI;
                                                                }
                                                                else
                                                                {
                                                                    _locComboMaxLoop = _locFreeItemRule.MaxLoop;
                                                                }
                                                            }

                                                            for (int i = 1; i <= _locComboMaxLoop; i++)
                                                            {
                                                                if (i == 1)
                                                                {
                                                                    _locComboTotQtyFI = _locFreeItemRule.FpTQty;
                                                                }
                                                                else
                                                                {
                                                                    _locComboTotQtyFI = _locComboTotQtyFI + (i + _locFreeItemRule.FpTQty);
                                                                }
                                                            }

                                                            if (_locFreeItemRule.FpBegInvLine != null)
                                                            {
                                                                if ((_locFreeItemRule.FpBegInvLine.QtyAvailable - _locFreeItemRule.FpBegInvLine.QtyOnHand) > 0)
                                                                {
                                                                    if ((_locFreeItemRule.FpBegInvLine.QtyAvailable - _locFreeItemRule.FpBegInvLine.QtyOnHand) < _locComboTotQtyFI)
                                                                    {
                                                                        _locComboTotQtyFI = _locFreeItemRule.FpBegInvLine.QtyAvailable - _locFreeItemRule.FpBegInvLine.QtyOnHand;
                                                                    }

                                                                    if (_locFreeItemRule.FpItem != null && _locFreeItemRule.FpDUOM != null && _locFreeItemRule.FpUOM != null)
                                                                    {
                                                                        ItemUnitOfMeasure _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                                                                (new GroupOperator(GroupOperatorType.And,
                                                                                                 new BinaryOperator("Item", _locFreeItemRule.FpItem),
                                                                                                 new BinaryOperator("UOM", _locFreeItemRule.FpUOM),
                                                                                                 new BinaryOperator("DefaultUOM", _locFreeItemRule.FpDUOM),
                                                                                                 new BinaryOperator("Active", true)));
                                                                        if (_locItemUOM != null)
                                                                        {
                                                                            if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                                                            {
                                                                                _locComboCpDqty = _locComboTotQtyFI % _locItemUOM.DefaultConversion;
                                                                                _locComboCpQty = System.Math.Floor(_locComboTotQtyFI / _locItemUOM.DefaultConversion);
                                                                            }
                                                                            else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                                                            {
                                                                                _locComboCpDqty = System.Math.Floor(_locComboTotQtyFI / _locItemUOM.DefaultConversion);
                                                                                _locComboCpQty = (_locComboTotQtyFI % _locItemUOM.DefaultConversion) * _locItemUOM.Conversion;
                                                                            }
                                                                            else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                                                            {
                                                                                _locComboCpDqty = _locComboTotQtyFI;
                                                                            }
                                                                        }
                                                                    }
                                                                    else
                                                                    {
                                                                        _locComboCpDqty = _locComboTotQtyFI;
                                                                    }

                                                                    CustomerOrderFreeItem _saveDataCustomerOrderFreeItem = new CustomerOrderFreeItem(_currSession)
                                                                    {
                                                                        //Organization
                                                                        Company = _locCustomerOrderXPO.Company,
                                                                        Workplace = _locCustomerOrderXPO.Workplace,
                                                                        Division = _locCustomerOrderXPO.Division,
                                                                        Department = _locCustomerOrderXPO.Department,
                                                                        Section = _locCustomerOrderXPO.Section,
                                                                        Employee = _locCustomerOrderXPO.Employee,
                                                                        //Item
                                                                        CustomerOrder = _locCustomerOrderXPO,
                                                                        Location = _locFreeItemRule.Location,
                                                                        FreeItemRule = _locFreeItemRule,
                                                                        BegInv = _locFreeItemRule.FpBegInv,
                                                                        FpLocationType = _locFreeItemRule.LocationType,
                                                                        FpStockType = _locFreeItemRule.StockType,
                                                                        FpItem = _locFreeItemRule.FpItem,
                                                                        FpDQty = _locComboCpDqty,
                                                                        FpDUOM = _locFreeItemRule.FpDUOM,
                                                                        FpQty = _locComboCpQty,
                                                                        FpUOM = _locFreeItemRule.FpUOM,
                                                                        FpTQty = _locComboTotQtyFI,
                                                                        FpStockGroup = _locFreeItemRule.FpStockGroup,
                                                                        BegInvLine = _locFreeItemRule.FpBegInvLine,
                                                                        //Amount
                                                                        Currency = _locCustomerOrderXPO.Currency,
                                                                        PriceGroup = _locCustomerOrderXPO.PriceGroup,
                                                                        PriceLine = GetPriceLine(_currSession, _locCustomerOrderXPO, _locFreeItemRule.FpItem),
                                                                        UAmount = GetPriceLine(_currSession, _locCustomerOrderXPO, _locFreeItemRule.FpItem).Amount2a,
                                                                    };
                                                                    _saveDataCustomerOrderFreeItem.Save();
                                                                    _saveDataCustomerOrderFreeItem.Session.CommitTransaction();

                                                                    _locComboCustomerOrderLine.FreeItemChecked = true;
                                                                    _locComboCustomerOrderLine.Save();
                                                                    _locComboCustomerOrderLine.Session.CommitTransaction();

                                                                    _locMainCustomerOrderLine.FreeItemChecked = true;
                                                                    _locMainCustomerOrderLine.Save();
                                                                    _locMainCustomerOrderLine.Session.CommitTransaction();

                                                                    if (_locFreeItemRule.FpBegInv != null && _locFreeItemRule.FpBegInvLine != null)
                                                                    {
                                                                        _locFreeItemRule.FpBegInvLine.QtyOnHand = _locFreeItemRule.FpBegInvLine.QtyOnHand + _locComboTotQtyFI;
                                                                        _locFreeItemRule.FpBegInvLine.Save();
                                                                        _locFreeItemRule.FpBegInvLine.Session.CommitTransaction();
                                                                    }
                                                                }
                                                            }                                                            
                                                        }
                                                        #endregion Rule2

                                                        //2 * 2 * 2 * 2
                                                        #region Rule3

                                                        if (_locFreeItemRule.RuleType == RuleType.Rule3)
                                                        {

                                                            if (_locFreeItemRule.MaxLoop == 0)
                                                            {
                                                                _locComboMaxLoop = (int)_locComboQtyCI;
                                                            }
                                                            else
                                                            {
                                                                if (_locFreeItemRule.MaxLoop > (int)_locComboQtyCI)
                                                                {
                                                                    _locComboMaxLoop = (int)_locComboQtyCI;
                                                                }
                                                                else
                                                                {
                                                                    _locComboMaxLoop = _locFreeItemRule.MaxLoop;
                                                                }
                                                            }

                                                            for (int i = 1; i <= _locComboMaxLoop; i++)
                                                            {
                                                                _locComboTotQtyFI = _locComboTotQtyFI * _locFreeItemRule.FpTQty;
                                                            }

                                                            if (_locFreeItemRule.FpBegInvLine != null)
                                                            {
                                                                if ((_locFreeItemRule.FpBegInvLine.QtyAvailable - _locFreeItemRule.FpBegInvLine.QtyOnHand) > 0)
                                                                {
                                                                    if ((_locFreeItemRule.FpBegInvLine.QtyAvailable - _locFreeItemRule.FpBegInvLine.QtyOnHand) < _locComboTotQtyFI)
                                                                    {
                                                                        _locComboTotQtyFI = _locFreeItemRule.FpBegInvLine.QtyAvailable - _locFreeItemRule.FpBegInvLine.QtyOnHand;
                                                                    }

                                                                    if (_locFreeItemRule.FpItem != null && _locFreeItemRule.FpDUOM != null && _locFreeItemRule.FpUOM != null)
                                                                    {
                                                                        ItemUnitOfMeasure _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                                                                (new GroupOperator(GroupOperatorType.And,
                                                                                                 new BinaryOperator("Item", _locFreeItemRule.FpItem),
                                                                                                 new BinaryOperator("UOM", _locFreeItemRule.FpUOM),
                                                                                                 new BinaryOperator("DefaultUOM", _locFreeItemRule.FpDUOM),
                                                                                                 new BinaryOperator("Active", true)));
                                                                        if (_locItemUOM != null)
                                                                        {
                                                                            if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                                                            {
                                                                                _locComboCpDqty = _locComboTotQtyFI % _locItemUOM.DefaultConversion;
                                                                                _locComboCpQty = System.Math.Floor(_locComboTotQtyFI / _locItemUOM.DefaultConversion);
                                                                            }
                                                                            else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                                                            {
                                                                                _locComboCpDqty = System.Math.Floor(_locComboTotQtyFI / _locItemUOM.DefaultConversion);
                                                                                _locComboCpQty = (_locComboTotQtyFI % _locItemUOM.DefaultConversion) * _locItemUOM.Conversion;
                                                                            }
                                                                            else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                                                            {
                                                                                _locComboCpDqty = _locComboTotQtyFI;
                                                                            }
                                                                        }
                                                                    }
                                                                    else
                                                                    {
                                                                        _locComboCpDqty = _locComboTotQtyFI;
                                                                    }

                                                                    CustomerOrderFreeItem _saveDataCustomerOrderFreeItem = new CustomerOrderFreeItem(_currSession)
                                                                    {
                                                                        //Organization
                                                                        Company = _locCustomerOrderXPO.Company,
                                                                        Workplace = _locCustomerOrderXPO.Workplace,
                                                                        Division = _locCustomerOrderXPO.Division,
                                                                        Department = _locCustomerOrderXPO.Department,
                                                                        Section = _locCustomerOrderXPO.Section,
                                                                        Employee = _locCustomerOrderXPO.Employee,
                                                                        //Item
                                                                        CustomerOrder = _locCustomerOrderXPO,
                                                                        Location = _locFreeItemRule.Location,
                                                                        FreeItemRule = _locFreeItemRule,
                                                                        BegInv = _locFreeItemRule.FpBegInv,
                                                                        FpLocationType = _locFreeItemRule.LocationType,
                                                                        FpStockType = _locFreeItemRule.StockType,
                                                                        FpItem = _locFreeItemRule.FpItem,
                                                                        FpDQty = _locComboCpDqty,
                                                                        FpDUOM = _locFreeItemRule.FpDUOM,
                                                                        FpQty = _locComboCpQty,
                                                                        FpUOM = _locFreeItemRule.FpUOM,
                                                                        FpTQty = _locComboTotQtyFI,
                                                                        FpStockGroup = _locFreeItemRule.FpStockGroup,
                                                                        BegInvLine = _locFreeItemRule.FpBegInvLine,
                                                                        //Amount
                                                                        Currency = _locCustomerOrderXPO.Currency,
                                                                        PriceGroup = _locCustomerOrderXPO.PriceGroup,
                                                                        PriceLine = GetPriceLine(_currSession, _locCustomerOrderXPO, _locFreeItemRule.FpItem),
                                                                        UAmount = GetPriceLine(_currSession, _locCustomerOrderXPO, _locFreeItemRule.FpItem).Amount2a,
                                                                    };
                                                                    _saveDataCustomerOrderFreeItem.Save();
                                                                    _saveDataCustomerOrderFreeItem.Session.CommitTransaction();

                                                                    _locComboCustomerOrderLine.FreeItemChecked = true;
                                                                    _locComboCustomerOrderLine.Save();
                                                                    _locComboCustomerOrderLine.Session.CommitTransaction();
                                                                    _locMainCustomerOrderLine.FreeItemChecked = true;
                                                                    _locMainCustomerOrderLine.Save();
                                                                    _locMainCustomerOrderLine.Session.CommitTransaction();

                                                                    if (_locFreeItemRule.FpBegInv != null && _locFreeItemRule.FpBegInvLine != null)
                                                                    {
                                                                        _locFreeItemRule.FpBegInvLine.QtyOnHand = _locFreeItemRule.FpBegInvLine.QtyOnHand + _locComboTotQtyFI;
                                                                        _locFreeItemRule.FpBegInvLine.Save();
                                                                        _locFreeItemRule.FpBegInvLine.Session.CommitTransaction();
                                                                    }
                                                                }
                                                            }                    
                                                        }
                                                        #endregion Rule3

                                                        //2^(1-1) + 2^(2-1) + 2^(3-1) + 2^(4-1)
                                                        #region Rule4

                                                        if (_locFreeItemRule.RuleType == RuleType.Rule4)
                                                        {
                                                            if (_locFreeItemRule.MaxLoop == 0)
                                                            {
                                                                _locComboMaxLoop = (int)_locComboQtyCI;
                                                            }
                                                            else
                                                            {
                                                                if (_locFreeItemRule.MaxLoop > (int)_locComboQtyCI)
                                                                {
                                                                    _locComboMaxLoop = (int)_locComboQtyCI;
                                                                }
                                                                else
                                                                {
                                                                    _locComboMaxLoop = _locFreeItemRule.MaxLoop;
                                                                }
                                                            }

                                                            for (int i = 1; i <= _locComboMaxLoop; i++)
                                                            {
                                                                _locComboTotQtyFI = _locComboTotQtyFI + System.Math.Pow(_locFreeItemRule.FpTQty, i - 1);
                                                            }

                                                            if (_locFreeItemRule.FpBegInvLine != null)
                                                            {
                                                                if ((_locFreeItemRule.FpBegInvLine.QtyAvailable - _locFreeItemRule.FpBegInvLine.QtyOnHand) > 0)
                                                                {
                                                                    if ((_locFreeItemRule.FpBegInvLine.QtyAvailable - _locFreeItemRule.FpBegInvLine.QtyOnHand) < _locComboTotQtyFI)
                                                                    {
                                                                        _locComboTotQtyFI = _locFreeItemRule.FpBegInvLine.QtyAvailable - _locFreeItemRule.FpBegInvLine.QtyOnHand;
                                                                    }

                                                                    if (_locFreeItemRule.FpItem != null && _locFreeItemRule.FpDUOM != null && _locFreeItemRule.FpUOM != null)
                                                                    {
                                                                        ItemUnitOfMeasure _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                                                                (new GroupOperator(GroupOperatorType.And,
                                                                                                 new BinaryOperator("Item", _locFreeItemRule.FpItem),
                                                                                                 new BinaryOperator("UOM", _locFreeItemRule.FpUOM),
                                                                                                 new BinaryOperator("DefaultUOM", _locFreeItemRule.FpDUOM),
                                                                                                 new BinaryOperator("Active", true)));
                                                                        if (_locItemUOM != null)
                                                                        {
                                                                            if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                                                            {
                                                                                _locComboCpDqty = _locComboTotQtyFI % _locItemUOM.DefaultConversion;
                                                                                _locComboCpQty = System.Math.Floor(_locComboTotQtyFI / _locItemUOM.DefaultConversion);
                                                                            }
                                                                            else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                                                            {
                                                                                _locComboCpDqty = System.Math.Floor(_locComboTotQtyFI / _locItemUOM.DefaultConversion);
                                                                                _locComboCpQty = (_locComboTotQtyFI % _locItemUOM.DefaultConversion) * _locItemUOM.Conversion;
                                                                            }
                                                                            else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                                                            {
                                                                                _locComboCpDqty = _locComboTotQtyFI;
                                                                            }
                                                                        }
                                                                    }
                                                                    else
                                                                    {
                                                                        _locComboCpDqty = _locComboTotQtyFI;
                                                                    }

                                                                    CustomerOrderFreeItem _saveDataCustomerOrderFreeItem = new CustomerOrderFreeItem(_currSession)
                                                                    {
                                                                        //Organization
                                                                        Company = _locCustomerOrderXPO.Company,
                                                                        Workplace = _locCustomerOrderXPO.Workplace,
                                                                        Division = _locCustomerOrderXPO.Division,
                                                                        Department = _locCustomerOrderXPO.Department,
                                                                        Section = _locCustomerOrderXPO.Section,
                                                                        Employee = _locCustomerOrderXPO.Employee,
                                                                        //Item
                                                                        CustomerOrder = _locCustomerOrderXPO,
                                                                        Location = _locFreeItemRule.Location,
                                                                        FreeItemRule = _locFreeItemRule,
                                                                        BegInv = _locFreeItemRule.FpBegInv,
                                                                        FpLocationType = _locFreeItemRule.LocationType,
                                                                        FpStockType = _locFreeItemRule.StockType,
                                                                        FpItem = _locFreeItemRule.FpItem,
                                                                        FpDQty = _locComboCpDqty,
                                                                        FpDUOM = _locFreeItemRule.FpDUOM,
                                                                        FpQty = _locComboCpQty,
                                                                        FpUOM = _locFreeItemRule.FpUOM,
                                                                        FpTQty = _locComboTotQtyFI,
                                                                        FpStockGroup = _locFreeItemRule.FpStockGroup,
                                                                        BegInvLine = _locFreeItemRule.FpBegInvLine,
                                                                        //Amount
                                                                        Currency = _locCustomerOrderXPO.Currency,
                                                                        PriceGroup = _locCustomerOrderXPO.PriceGroup,
                                                                        PriceLine = GetPriceLine(_currSession, _locCustomerOrderXPO, _locFreeItemRule.FpItem),
                                                                        UAmount = GetPriceLine(_currSession, _locCustomerOrderXPO, _locFreeItemRule.FpItem).Amount2a,
                                                                    };
                                                                    _saveDataCustomerOrderFreeItem.Save();
                                                                    _saveDataCustomerOrderFreeItem.Session.CommitTransaction();

                                                                    _locComboCustomerOrderLine.FreeItemChecked = true;
                                                                    _locComboCustomerOrderLine.Save();
                                                                    _locComboCustomerOrderLine.Session.CommitTransaction();

                                                                    _locMainCustomerOrderLine.FreeItemChecked = true;
                                                                    _locMainCustomerOrderLine.Save();
                                                                    _locMainCustomerOrderLine.Session.CommitTransaction();

                                                                    if (_locFreeItemRule.FpBegInv != null && _locFreeItemRule.FpBegInvLine != null)
                                                                    {
                                                                        _locFreeItemRule.FpBegInvLine.QtyOnHand = _locFreeItemRule.FpBegInvLine.QtyOnHand + _locComboTotQtyFI;
                                                                        _locFreeItemRule.FpBegInvLine.Save();
                                                                        _locFreeItemRule.FpBegInvLine.Session.CommitTransaction();
                                                                    }
                                                                }
                                                            }                           
                                                        }
                                                        #endregion Rule4

                                                        #endregion ComboPromoItem
                                                    }
                                                    else
                                                    {
                                                        #region MainPromoItem

                                                        #region Rule1

                                                        if (_locFreeItemRule.RuleType == RuleType.Rule1)
                                                        {

                                                            if (_locFreeItemRule.MaxLoop == 0)
                                                            {
                                                                _locMaxLoop = (int)_locMainQtyFI;
                                                            }
                                                            else
                                                            {
                                                                if (_locFreeItemRule.MaxLoop > (int)_locMainQtyFI)
                                                                {
                                                                    _locMaxLoop = (int)_locMainQtyFI;
                                                                }
                                                                else
                                                                {
                                                                    _locMaxLoop = _locFreeItemRule.MaxLoop;
                                                                }
                                                            }

                                                            for (int i = 1; i <= _locMaxLoop; i++)
                                                            {
                                                                _locTotMaxLoopVal = _locTotMaxLoopVal + 1;
                                                            }

                                                            _locTotQtyFI = _locTotMaxLoopVal * _locFreeItemRule.FpTQty;

                                                            if(_locFreeItemRule.FpBegInvLine != null)
                                                            {
                                                                if ((_locFreeItemRule.FpBegInvLine.QtyAvailable - _locFreeItemRule.FpBegInvLine.QtyOnHand) > 0)
                                                                {
                                                                    if ((_locFreeItemRule.FpBegInvLine.QtyAvailable - _locFreeItemRule.FpBegInvLine.QtyOnHand) < _locTotQtyFI)
                                                                    {
                                                                        _locTotQtyFI = _locFreeItemRule.FpBegInvLine.QtyAvailable - _locFreeItemRule.FpBegInvLine.QtyOnHand;
                                                                    }

                                                                    if (_locFreeItemRule.FpItem != null && _locFreeItemRule.FpDUOM != null && _locFreeItemRule.FpUOM != null)
                                                                    {
                                                                        ItemUnitOfMeasure _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                                                                (new GroupOperator(GroupOperatorType.And,
                                                                                                 new BinaryOperator("Item", _locFreeItemRule.FpItem),
                                                                                                 new BinaryOperator("UOM", _locFreeItemRule.FpUOM),
                                                                                                 new BinaryOperator("DefaultUOM", _locFreeItemRule.FpDUOM),
                                                                                                 new BinaryOperator("Active", true)));
                                                                        if (_locItemUOM != null)
                                                                        {
                                                                            if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                                                            {
                                                                                _locFpDqty = _locTotQtyFI % _locItemUOM.DefaultConversion;
                                                                                _locFpQty = System.Math.Floor(_locTotQtyFI / _locItemUOM.DefaultConversion);
                                                                            }
                                                                            else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                                                            {
                                                                                _locFpDqty = System.Math.Floor(_locTotQtyFI / _locItemUOM.DefaultConversion);
                                                                                _locFpQty = (_locTotQtyFI % _locItemUOM.DefaultConversion) * _locItemUOM.Conversion;
                                                                            }
                                                                            else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                                                            {
                                                                                _locFpDqty = _locTotQtyFI;
                                                                            }
                                                                        }
                                                                    }
                                                                    else
                                                                    {
                                                                        _locFpDqty = _locTotQtyFI;
                                                                    }

                                                                    CustomerOrderFreeItem _saveDataCustomerOrderFreeItem = new CustomerOrderFreeItem(_currSession)
                                                                    {
                                                                        //Organization
                                                                        Company = _locCustomerOrderXPO.Company,
                                                                        Workplace = _locCustomerOrderXPO.Workplace,
                                                                        Division = _locCustomerOrderXPO.Division,
                                                                        Department = _locCustomerOrderXPO.Department,
                                                                        Section = _locCustomerOrderXPO.Section,
                                                                        Employee = _locCustomerOrderXPO.Employee,
                                                                        //Item
                                                                        CustomerOrder = _locCustomerOrderXPO,
                                                                        Location = _locFreeItemRule.Location,
                                                                        FreeItemRule = _locFreeItemRule,
                                                                        FpLocationType = _locFreeItemRule.LocationType,
                                                                        FpStockType = _locFreeItemRule.StockType,
                                                                        BegInv = _locFreeItemRule.FpBegInv,
                                                                        FpItem = _locFreeItemRule.FpItem,
                                                                        FpDQty = _locFpDqty,
                                                                        FpDUOM = _locFreeItemRule.FpDUOM,
                                                                        FpQty = _locFpQty,
                                                                        FpUOM = _locFreeItemRule.FpUOM,
                                                                        FpTQty = _locTotQtyFI,
                                                                        FpStockGroup = _locFreeItemRule.FpStockGroup,
                                                                        BegInvLine = _locFreeItemRule.FpBegInvLine,
                                                                        //Amount
                                                                        Currency = _locCustomerOrderXPO.Currency,
                                                                        PriceGroup = _locCustomerOrderXPO.PriceGroup,
                                                                        PriceLine = GetPriceLine(_currSession, _locCustomerOrderXPO, _locFreeItemRule.FpItem),
                                                                        UAmount = GetPriceLine(_currSession, _locCustomerOrderXPO, _locFreeItemRule.FpItem).Amount2a,
                                                                    };
                                                                    _saveDataCustomerOrderFreeItem.Save();
                                                                    _saveDataCustomerOrderFreeItem.Session.CommitTransaction();

                                                                    _locComboCustomerOrderLine.FreeItemChecked = true;
                                                                    _locComboCustomerOrderLine.Save();
                                                                    _locComboCustomerOrderLine.Session.CommitTransaction();
                                                                    _locMainCustomerOrderLine.FreeItemChecked = true;
                                                                    _locMainCustomerOrderLine.Save();
                                                                    _locMainCustomerOrderLine.Session.CommitTransaction();

                                                                    if (_locFreeItemRule.FpBegInv != null && _locFreeItemRule.FpBegInvLine != null)
                                                                    {
                                                                        _locFreeItemRule.FpBegInvLine.QtyOnHand = _locFreeItemRule.FpBegInvLine.QtyOnHand + _locTotQtyFI;
                                                                        _locFreeItemRule.FpBegInvLine.Save();
                                                                        _locFreeItemRule.FpBegInvLine.Session.CommitTransaction();
                                                                    }
                                                                }
                                                            }            
                                                        }

                                                        #endregion Rule1

                                                        //1, 2+1, 3+1, 4+1
                                                        #region Rule2
                                                        if (_locFreeItemRule.RuleType == RuleType.Rule2)
                                                        {

                                                            if (_locFreeItemRule.MaxLoop == 0)
                                                            {
                                                                _locMaxLoop = (int)_locMainQtyFI;
                                                            }
                                                            else
                                                            {
                                                                if (_locFreeItemRule.MaxLoop > (int)_locMainQtyFI)
                                                                {
                                                                    _locMaxLoop = (int)_locMainQtyFI;
                                                                }
                                                                else
                                                                {
                                                                    _locMaxLoop = _locFreeItemRule.MaxLoop;
                                                                }
                                                            }

                                                            for (int i = 1; i <= _locMaxLoop; i++)
                                                            {

                                                                if (i == 1)
                                                                {
                                                                    _locTotQtyFI = _locFreeItemRule.FpTQty;
                                                                }
                                                                else
                                                                {
                                                                    _locTotQtyFI = _locTotQtyFI + (i + _locFreeItemRule.FpTQty);
                                                                }
                                                            }

                                                            if (_locFreeItemRule.FpBegInvLine != null)
                                                            {
                                                                if ((_locFreeItemRule.FpBegInvLine.QtyAvailable - _locFreeItemRule.FpBegInvLine.QtyOnHand) > 0)
                                                                {
                                                                    if ((_locFreeItemRule.FpBegInvLine.QtyAvailable - _locFreeItemRule.FpBegInvLine.QtyOnHand) < _locTotQtyFI)
                                                                    {
                                                                        _locTotQtyFI = _locFreeItemRule.FpBegInvLine.QtyAvailable - _locFreeItemRule.FpBegInvLine.QtyOnHand;
                                                                    }

                                                                    if (_locFreeItemRule.FpItem != null && _locFreeItemRule.FpDUOM != null && _locFreeItemRule.FpUOM != null)
                                                                    {
                                                                        ItemUnitOfMeasure _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                                                                (new GroupOperator(GroupOperatorType.And,
                                                                                                 new BinaryOperator("Item", _locFreeItemRule.FpItem),
                                                                                                 new BinaryOperator("UOM", _locFreeItemRule.FpUOM),
                                                                                                 new BinaryOperator("DefaultUOM", _locFreeItemRule.FpDUOM),
                                                                                                 new BinaryOperator("Active", true)));
                                                                        if (_locItemUOM != null)
                                                                        {
                                                                            if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                                                            {
                                                                                _locFpDqty = _locTotQtyFI % _locItemUOM.DefaultConversion;
                                                                                _locFpQty = System.Math.Floor(_locTotQtyFI / _locItemUOM.DefaultConversion);
                                                                            }
                                                                            else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                                                            {
                                                                                _locFpDqty = System.Math.Floor(_locTotQtyFI / _locItemUOM.DefaultConversion);
                                                                                _locFpQty = (_locTotQtyFI % _locItemUOM.DefaultConversion) * _locItemUOM.Conversion;
                                                                            }
                                                                            else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                                                            {
                                                                                _locFpDqty = _locTotQtyFI;
                                                                            }
                                                                        }
                                                                    }
                                                                    else
                                                                    {
                                                                        _locFpDqty = _locTotQtyFI;
                                                                    }

                                                                    CustomerOrderFreeItem _saveDataCustomerOrderFreeItem = new CustomerOrderFreeItem(_currSession)
                                                                    {
                                                                        //Organization
                                                                        Company = _locCustomerOrderXPO.Company,
                                                                        Workplace = _locCustomerOrderXPO.Workplace,
                                                                        Division = _locCustomerOrderXPO.Division,
                                                                        Department = _locCustomerOrderXPO.Department,
                                                                        Section = _locCustomerOrderXPO.Section,
                                                                        Employee = _locCustomerOrderXPO.Employee,
                                                                        //Item
                                                                        CustomerOrder = _locCustomerOrderXPO,
                                                                        Location = _locFreeItemRule.Location,
                                                                        FreeItemRule = _locFreeItemRule,
                                                                        FpLocationType = _locFreeItemRule.LocationType,
                                                                        FpStockType = _locFreeItemRule.StockType,
                                                                        BegInv = _locFreeItemRule.FpBegInv,
                                                                        FpItem = _locFreeItemRule.FpItem,
                                                                        FpDQty = _locFpDqty,
                                                                        FpDUOM = _locFreeItemRule.FpDUOM,
                                                                        FpQty = _locFpQty,
                                                                        FpUOM = _locFreeItemRule.FpUOM,
                                                                        FpTQty = _locTotQtyFI,
                                                                        FpStockGroup = _locFreeItemRule.FpStockGroup,
                                                                        BegInvLine = _locFreeItemRule.FpBegInvLine,
                                                                        //Amount
                                                                        Currency = _locCustomerOrderXPO.Currency,
                                                                        PriceGroup = _locCustomerOrderXPO.PriceGroup,
                                                                        PriceLine = GetPriceLine(_currSession, _locCustomerOrderXPO, _locFreeItemRule.FpItem),
                                                                        UAmount = GetPriceLine(_currSession, _locCustomerOrderXPO, _locFreeItemRule.FpItem).Amount2a,
                                                                    };
                                                                    _saveDataCustomerOrderFreeItem.Save();
                                                                    _saveDataCustomerOrderFreeItem.Session.CommitTransaction();

                                                                    _locComboCustomerOrderLine.FreeItemChecked = true;
                                                                    _locComboCustomerOrderLine.Save();
                                                                    _locComboCustomerOrderLine.Session.CommitTransaction();
                                                                    _locMainCustomerOrderLine.FreeItemChecked = true;
                                                                    _locMainCustomerOrderLine.Save();
                                                                    _locMainCustomerOrderLine.Session.CommitTransaction();

                                                                    if (_locFreeItemRule.FpBegInv != null && _locFreeItemRule.FpBegInvLine != null)
                                                                    {
                                                                        _locFreeItemRule.FpBegInvLine.QtyOnHand = _locFreeItemRule.FpBegInvLine.QtyOnHand + _locTotQtyFI;
                                                                        _locFreeItemRule.FpBegInvLine.Save();
                                                                        _locFreeItemRule.FpBegInvLine.Session.CommitTransaction();
                                                                    }
                                                                }
                                                            }                  
                                                        }
                                                        #endregion Rule2

                                                        //2 * 2 * 2 * 2
                                                        #region Rule3
                                                        if (_locFreeItemRule.RuleType == RuleType.Rule3)
                                                        {

                                                            if (_locFreeItemRule.MaxLoop == 0)
                                                            {
                                                                _locMaxLoop = (int)_locMainQtyFI;
                                                            }
                                                            else
                                                            {
                                                                if (_locFreeItemRule.MaxLoop > (int)_locMainQtyFI)
                                                                {
                                                                    _locMaxLoop = (int)_locMainQtyFI;
                                                                }
                                                                else
                                                                {
                                                                    _locMaxLoop = _locFreeItemRule.MaxLoop;
                                                                }
                                                            }

                                                            for (int i = 1; i <= _locMaxLoop; i++)
                                                            {
                                                                _locTotQtyFI = _locTotQtyFI * _locFreeItemRule.FpTQty;
                                                            }

                                                            if (_locFreeItemRule.FpBegInvLine != null)
                                                            {
                                                                if ((_locFreeItemRule.FpBegInvLine.QtyAvailable - _locFreeItemRule.FpBegInvLine.QtyOnHand) > 0)
                                                                {
                                                                    if ((_locFreeItemRule.FpBegInvLine.QtyAvailable - _locFreeItemRule.FpBegInvLine.QtyOnHand) < _locTotQtyFI)
                                                                    {
                                                                        _locTotQtyFI = _locFreeItemRule.FpBegInvLine.QtyAvailable - _locFreeItemRule.FpBegInvLine.QtyOnHand;
                                                                    }

                                                                    if (_locFreeItemRule.FpItem != null && _locFreeItemRule.FpDUOM != null && _locFreeItemRule.FpUOM != null)
                                                                    {
                                                                        ItemUnitOfMeasure _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                                                                (new GroupOperator(GroupOperatorType.And,
                                                                                                 new BinaryOperator("Item", _locFreeItemRule.FpItem),
                                                                                                 new BinaryOperator("UOM", _locFreeItemRule.FpUOM),
                                                                                                 new BinaryOperator("DefaultUOM", _locFreeItemRule.FpDUOM),
                                                                                                 new BinaryOperator("Active", true)));
                                                                        if (_locItemUOM != null)
                                                                        {
                                                                            if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                                                            {
                                                                                _locFpDqty = _locTotQtyFI % _locItemUOM.DefaultConversion;
                                                                                _locFpQty = System.Math.Floor(_locTotQtyFI / _locItemUOM.DefaultConversion);
                                                                            }
                                                                            else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                                                            {
                                                                                _locFpDqty = System.Math.Floor(_locTotQtyFI / _locItemUOM.DefaultConversion);
                                                                                _locFpQty = (_locTotQtyFI % _locItemUOM.DefaultConversion) * _locItemUOM.Conversion;
                                                                            }
                                                                            else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                                                            {
                                                                                _locFpDqty = _locTotQtyFI;
                                                                            }
                                                                        }
                                                                    }
                                                                    else
                                                                    {
                                                                        _locFpDqty = _locTotQtyFI;
                                                                    }

                                                                    CustomerOrderFreeItem _saveDataCustomerOrderFreeItem = new CustomerOrderFreeItem(_currSession)
                                                                    {
                                                                        //Organization
                                                                        Company = _locCustomerOrderXPO.Company,
                                                                        Workplace = _locCustomerOrderXPO.Workplace,
                                                                        Division = _locCustomerOrderXPO.Division,
                                                                        Department = _locCustomerOrderXPO.Department,
                                                                        Section = _locCustomerOrderXPO.Section,
                                                                        Employee = _locCustomerOrderXPO.Employee,
                                                                        //Item
                                                                        CustomerOrder = _locCustomerOrderXPO,
                                                                        Location = _locFreeItemRule.Location,
                                                                        FreeItemRule = _locFreeItemRule,
                                                                        FpLocationType = _locFreeItemRule.LocationType,
                                                                        FpStockType = _locFreeItemRule.StockType,
                                                                        BegInv = _locFreeItemRule.FpBegInv,
                                                                        FpItem = _locFreeItemRule.FpItem,
                                                                        FpDQty = _locFpDqty,
                                                                        FpDUOM = _locFreeItemRule.FpDUOM,
                                                                        FpQty = _locFpQty,
                                                                        FpUOM = _locFreeItemRule.FpUOM,
                                                                        FpTQty = _locTotQtyFI,
                                                                        FpStockGroup = _locFreeItemRule.FpStockGroup,
                                                                        BegInvLine = _locFreeItemRule.FpBegInvLine,
                                                                        //Amount
                                                                        Currency = _locCustomerOrderXPO.Currency,
                                                                        PriceGroup = _locCustomerOrderXPO.PriceGroup,
                                                                        PriceLine = GetPriceLine(_currSession, _locCustomerOrderXPO, _locFreeItemRule.FpItem),
                                                                        UAmount = GetPriceLine(_currSession, _locCustomerOrderXPO, _locFreeItemRule.FpItem).Amount2a,
                                                                    };
                                                                    _saveDataCustomerOrderFreeItem.Save();
                                                                    _saveDataCustomerOrderFreeItem.Session.CommitTransaction();

                                                                    _locComboCustomerOrderLine.FreeItemChecked = true;
                                                                    _locComboCustomerOrderLine.Save();
                                                                    _locComboCustomerOrderLine.Session.CommitTransaction();
                                                                    _locMainCustomerOrderLine.FreeItemChecked = true;
                                                                    _locMainCustomerOrderLine.Save();
                                                                    _locMainCustomerOrderLine.Session.CommitTransaction();

                                                                    if (_locFreeItemRule.FpBegInv != null && _locFreeItemRule.FpBegInvLine != null)
                                                                    {
                                                                        _locFreeItemRule.FpBegInvLine.QtyOnHand = _locFreeItemRule.FpBegInvLine.QtyOnHand + _locTotQtyFI;
                                                                        _locFreeItemRule.FpBegInvLine.Save();
                                                                        _locFreeItemRule.FpBegInvLine.Session.CommitTransaction();
                                                                    }
                                                                }
                                                            }                           
                                                        }
                                                        #endregion Rule3

                                                        //2^(1-1) + 2^(2-1) + 2^(3-1) + 2^(4-1)
                                                        #region Rule4

                                                        if (_locFreeItemRule.RuleType == RuleType.Rule4)
                                                        {
                                                            if (_locFreeItemRule.MaxLoop == 0)
                                                            {
                                                                _locMaxLoop = (int)_locMainQtyFI;
                                                            }
                                                            else
                                                            {
                                                                if (_locFreeItemRule.MaxLoop > (int)_locMainQtyFI)
                                                                {
                                                                    _locMaxLoop = (int)_locMainQtyFI;
                                                                }
                                                                else
                                                                {
                                                                    _locMaxLoop = _locFreeItemRule.MaxLoop;
                                                                }
                                                            }

                                                            for (int i = 1; i <= _locMaxLoop; i++)
                                                            {
                                                                _locTotQtyFI = _locTotQtyFI + System.Math.Pow(_locFreeItemRule.FpTQty, i - 1);
                                                            }

                                                            if (_locFreeItemRule.FpBegInvLine != null)
                                                            {
                                                                if ((_locFreeItemRule.FpBegInvLine.QtyAvailable - _locFreeItemRule.FpBegInvLine.QtyOnHand) > 0)
                                                                {
                                                                    if ((_locFreeItemRule.FpBegInvLine.QtyAvailable - _locFreeItemRule.FpBegInvLine.QtyOnHand) < _locTotQtyFI)
                                                                    {
                                                                        _locTotQtyFI = _locFreeItemRule.FpBegInvLine.QtyAvailable - _locFreeItemRule.FpBegInvLine.QtyOnHand;
                                                                    }

                                                                    if (_locFreeItemRule.FpItem != null && _locFreeItemRule.FpDUOM != null && _locFreeItemRule.FpUOM != null)
                                                                    {
                                                                        ItemUnitOfMeasure _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                                                                (new GroupOperator(GroupOperatorType.And,
                                                                                                 new BinaryOperator("Item", _locFreeItemRule.FpItem),
                                                                                                 new BinaryOperator("UOM", _locFreeItemRule.FpUOM),
                                                                                                 new BinaryOperator("DefaultUOM", _locFreeItemRule.FpDUOM),
                                                                                                 new BinaryOperator("Active", true)));
                                                                        if (_locItemUOM != null)
                                                                        {
                                                                            if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                                                            {
                                                                                _locFpDqty = _locTotQtyFI % _locItemUOM.DefaultConversion;
                                                                                _locFpQty = System.Math.Floor(_locTotQtyFI / _locItemUOM.DefaultConversion);
                                                                            }
                                                                            else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                                                            {
                                                                                _locFpDqty = System.Math.Floor(_locTotQtyFI / _locItemUOM.DefaultConversion);
                                                                                _locFpQty = (_locTotQtyFI % _locItemUOM.DefaultConversion) * _locItemUOM.Conversion;
                                                                            }
                                                                            else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                                                            {
                                                                                _locFpDqty = _locTotQtyFI;
                                                                            }
                                                                        }
                                                                    }
                                                                    else
                                                                    {
                                                                        _locFpDqty = _locTotQtyFI;
                                                                    }

                                                                    CustomerOrderFreeItem _saveDataCustomerOrderFreeItem = new CustomerOrderFreeItem(_currSession)
                                                                    {
                                                                        //Organization
                                                                        Company = _locCustomerOrderXPO.Company,
                                                                        Workplace = _locCustomerOrderXPO.Workplace,
                                                                        Division = _locCustomerOrderXPO.Division,
                                                                        Department = _locCustomerOrderXPO.Department,
                                                                        Section = _locCustomerOrderXPO.Section,
                                                                        Employee = _locCustomerOrderXPO.Employee,
                                                                        //Item
                                                                        CustomerOrder = _locCustomerOrderXPO,
                                                                        Location = _locFreeItemRule.Location,
                                                                        FreeItemRule = _locFreeItemRule,
                                                                        FpLocationType = _locFreeItemRule.LocationType,
                                                                        FpStockType = _locFreeItemRule.StockType,
                                                                        BegInv = _locFreeItemRule.FpBegInv,
                                                                        FpItem = _locFreeItemRule.FpItem,
                                                                        FpDQty = _locFpDqty,
                                                                        FpDUOM = _locFreeItemRule.FpDUOM,
                                                                        FpQty = _locFpQty,
                                                                        FpUOM = _locFreeItemRule.FpUOM,
                                                                        FpTQty = _locTotQtyFI,
                                                                        FpStockGroup = _locFreeItemRule.FpStockGroup,
                                                                        BegInvLine = _locFreeItemRule.FpBegInvLine,
                                                                        //Amount
                                                                        Currency = _locCustomerOrderXPO.Currency,
                                                                        PriceGroup = _locCustomerOrderXPO.PriceGroup,
                                                                        PriceLine = GetPriceLine(_currSession, _locCustomerOrderXPO, _locFreeItemRule.FpItem),
                                                                        UAmount = GetPriceLine(_currSession, _locCustomerOrderXPO, _locFreeItemRule.FpItem).Amount2a,
                                                                    };
                                                                    _saveDataCustomerOrderFreeItem.Save();
                                                                    _saveDataCustomerOrderFreeItem.Session.CommitTransaction();

                                                                    _locComboCustomerOrderLine.FreeItemChecked = true;
                                                                    _locComboCustomerOrderLine.Save();
                                                                    _locComboCustomerOrderLine.Session.CommitTransaction();
                                                                    _locMainCustomerOrderLine.FreeItemChecked = true;
                                                                    _locMainCustomerOrderLine.Save();
                                                                    _locMainCustomerOrderLine.Session.CommitTransaction();

                                                                    if (_locFreeItemRule.FpBegInv != null && _locFreeItemRule.FpBegInvLine != null)
                                                                    {
                                                                        _locFreeItemRule.FpBegInvLine.QtyOnHand = _locFreeItemRule.FpBegInvLine.QtyOnHand + _locTotQtyFI;
                                                                        _locFreeItemRule.FpBegInvLine.Save();
                                                                        _locFreeItemRule.FpBegInvLine.Session.CommitTransaction();
                                                                    }
                                                                }
                                                            }                                    
                                                        }
                                                        #endregion Rule4

                                                        #endregion MainPromoItem
                                                    }
                                                }
                                            }
                                        }
                                        #endregion MultiItem=true  
                                    }
                                }
                            }
                            #endregion Line
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = CustomerOrder ", ex.ToString());
            }
        }

        private void SetTotalFreeItemAmount(Session _currSession, CustomerOrder _locCustomerOrderXPO)
        {
            try
            {
                double _locTUAmount = 0;
                double _locGetTUAmount = 0;
                if (_locCustomerOrderXPO != null)
                {
                    XPCollection<CustomerOrderFreeItem> _locCustomerOrderFreeItems = new XPCollection<CustomerOrderFreeItem>
                                                                                (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                new BinaryOperator("CustomerOrder", _locCustomerOrderXPO),
                                                                                new GroupOperator(GroupOperatorType.Or,
                                                                                new BinaryOperator("Status", Status.Open),
                                                                                new BinaryOperator("Status", Status.Progress))));
                    if (_locCustomerOrderFreeItems != null && _locCustomerOrderFreeItems.Count() > 0)
                    {
                        foreach (CustomerOrderFreeItem _locCustomerOrderFreeItem in _locCustomerOrderFreeItems)
                        {
                            _locGetTUAmount = GetTotalUnitAmount(_currSession, _locCustomerOrderFreeItem.FpTQty, _locCustomerOrderFreeItem.UAmount);
                            _locTUAmount = _locTUAmount + _locGetTUAmount;

                            _locCustomerOrderFreeItem.TUAmount = _locGetTUAmount;
                            _locCustomerOrderFreeItem.Save();
                            _locCustomerOrderFreeItem.Session.CommitTransaction();
                        }
                        _locCustomerOrderXPO.TotFIAmount = _locTUAmount;
                        _locCustomerOrderXPO.Save();
                        _locCustomerOrderXPO.Session.CommitTransaction();
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = CustomerOrder ", ex.ToString());
            }
        }

        private PriceLine GetPriceLine(Session _currSession, CustomerOrder _locCustomerOrderXPO, Item _locFpItem)
        {
            PriceLine _result = null;
            try
            {

                if (_locCustomerOrderXPO.Company != null && _locCustomerOrderXPO.Workplace != null && _locFpItem != null && _locCustomerOrderXPO.PriceGroup != null)
                {
                    PriceLine _locPriceLine = _currSession.FindObject<PriceLine>(new GroupOperator(GroupOperatorType.And,
                                                                            new BinaryOperator("Company", _locCustomerOrderXPO.Company),
                                                                            new BinaryOperator("Workplace", _locCustomerOrderXPO.Workplace),
                                                                            new BinaryOperator("Item", _locFpItem),
                                                                            new BinaryOperator("PriceGroup", _locCustomerOrderXPO.PriceGroup),
                                                                            new BinaryOperator("Active", true)));
                    if (_locPriceLine != null)
                    {
                        _result = _locPriceLine;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = CustomerOrder " + ex.ToString());
            }
            return _result;
        }

        private double GetTotalUnitAmount(Session _currSession, double _locFpTQty, double _locUAmount)
        {
            double _result = 0;
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();

                if (_locFpTQty >= 0 & _locUAmount >= 0)
                {
                    if (_globFunc.GetRoundingList(_currSession, ObjectList.CustomerOrderFreeItem, FieldName.TUAmount) == true)
                    {
                        _result = _globFunc.GetRoundUp(_currSession, (_locFpTQty * _locUAmount), ObjectList.CustomerOrderFreeItem, FieldName.TUAmount);
                    }
                    else
                    {
                        _result = _locFpTQty * _locUAmount;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = CustomerOrder " + ex.ToString());
            }
            return _result;
        }

        #endregion FreeItem

        #region Posting

        private double GetCustomerOrderLineTotalAmount(Session _currSession, CustomerOrder _locCustomerOrderXPO)
        {
            double _return = 0;
            try
            {
                if (_locCustomerOrderXPO != null)
                {
                    double _locTAmount = 0;
                    XPCollection<CustomerOrderLine> _locCustomerOrderLines = new XPCollection<CustomerOrderLine>
                                                                        (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                        new BinaryOperator("CustomerOrder", _locCustomerOrderXPO),
                                                                        new BinaryOperator("Select", true),
                                                                        new GroupOperator(GroupOperatorType.Or,
                                                                        new BinaryOperator("Status", Status.Progress),
                                                                        new BinaryOperator("Status", Status.Posted))));

                    if (_locCustomerOrderLines != null && _locCustomerOrderLines.Count > 0)
                    {
                        foreach (CustomerOrderLine _locCustomerOrderLine in _locCustomerOrderLines)
                        {
                            _locTAmount = _locTAmount + _locCustomerOrderLine.TAmount;
                        }
                        _return = _locTAmount;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("BusinessObject = CustomerOrder" + ex.ToString());
            }
            return _return;
        }

        private void SetCustomerInvoice(Session _currSession, CustomerOrder _locCustomerOrderXPO)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                DateTime now = DateTime.Now;
                string _currSignCode = null;

                if (_locCustomerOrderXPO != null)
                {
                    if (_locCustomerOrderXPO.Status == Status.Progress || _locCustomerOrderXPO.Status == Status.Posted)
                    {
                        _currSignCode = _globFunc.GetNumberingSignUnlockOptimisticRecordBasedOrganization(_currSession.DataLayer, ObjectList.CustomerInvoice, _locCustomerOrderXPO.Company, _locCustomerOrderXPO.Workplace);
                        //_currSignCode = _globFunc.GetNumberingSignUnlockOptimisticRecord(_currSession.DataLayer, ObjectList.CustomerInvoice);

                        if(_currSignCode != null)
                        {
                            #region SaveCustomerInvoice
                            CustomerInvoice _saveDataCustomerInvoice = new CustomerInvoice(_currSession)
                            {
                                SignCode = _currSignCode,
                                CustomerOrder = _locCustomerOrderXPO,
                                Company = _locCustomerOrderXPO.Company,
                                Workplace = _locCustomerOrderXPO.Workplace,
                                SalesToCustomer = _locCustomerOrderXPO.SalesToCustomer,
                                BillToCustomer = _locCustomerOrderXPO.SalesToCustomer,
                                Currency = _locCustomerOrderXPO.Currency,
                                PriceGroup = _locCustomerOrderXPO.PriceGroup,
                                TOP = _locCustomerOrderXPO.TOP,
                                TaxNo = _locCustomerOrderXPO.TaxNo,
                                Location = _locCustomerOrderXPO.Location,
                                BegInv = _locCustomerOrderXPO.BegInv,
                                AccountingPeriodicLine = _locCustomerOrderXPO.AccountingPeriodicLine,
                            };
                            _saveDataCustomerInvoice.Save();
                            _saveDataCustomerInvoice.Session.CommitTransaction();
                            #endregion SaveCustomerInvoice

                            #region SaveCustomerInvoiceLine
                            XPCollection<CustomerOrderLine> _locCustomerOrderLineLines = new XPCollection<CustomerOrderLine>(_currSession,
                                                                                new GroupOperator(GroupOperatorType.And,
                                                                                new BinaryOperator("CustomerOrder", _locCustomerOrderXPO),
                                                                                new BinaryOperator("Select", true)));

                            CustomerInvoice _locCustomerInvoice2 = _currSession.FindObject<CustomerInvoice>
                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                     new BinaryOperator("SignCode", _currSignCode)));

                            if(_locCustomerInvoice2 != null)
                            {
                                if(_locCustomerOrderLineLines != null && _locCustomerOrderLineLines.Count > 0)
                                {
                                    foreach(CustomerOrderLine _locCustomerOrderLine in _locCustomerOrderLineLines)
                                    {
                                        if(_locCustomerOrderLine.Status == Status.Progress || _locCustomerOrderLine.Status == Status.Posted)
                                        {                                            
                                            CustomerInvoiceLine _saveDataCustomerInvoiceLine = new CustomerInvoiceLine(_currSession)
                                            {
                                                CustomerInvoice = _locCustomerInvoice2,
                                                CustomerOrderLine = _locCustomerOrderLine,
                                                Company = _locCustomerOrderLine.Company,
                                                Workplace = _locCustomerOrderLine.Workplace,
                                                Division = _locCustomerInvoice2.Division,
                                                Department = _locCustomerInvoice2.Department,
                                                Section = _locCustomerInvoice2.Section,
                                                Employee = _locCustomerInvoice2.Employee,
                                                SalesType = _locCustomerOrderLine.SalesType,
                                                Item = _locCustomerOrderLine.Item,
                                                Brand = _locCustomerOrderLine.Brand,
                                                Description = _locCustomerOrderLine.Description,
                                                DQty = _locCustomerOrderLine.DQty,
                                                DUOM = _locCustomerOrderLine.DUOM,
                                                Qty = _locCustomerOrderLine.Qty,
                                                UOM = _locCustomerOrderLine.UOM,
                                                TQty = _locCustomerOrderLine.TQty,
                                                StockType = _locCustomerOrderLine.StockType,
                                                Currency = _locCustomerOrderLine.Currency,
                                                PriceGroup = _locCustomerOrderLine.PriceGroup,
                                                PriceLine = _locCustomerOrderLine.PriceLine,
                                                UAmount = _locCustomerOrderLine.UAmount,
                                                TUAmount = _locCustomerOrderLine.TUAmount,
                                                Tax = _locCustomerOrderLine.Tax,
                                                TxValue = _locCustomerOrderLine.TxValue,
                                                TxAmount = _locCustomerOrderLine.TxAmount,
                                                DiscountRule = _locCustomerOrderLine.DiscountRule,
                                                DiscAmount = _locCustomerOrderLine.DiscAmount,
                                                TAmount = _locCustomerOrderLine.TAmount,
                                                Location = _locCustomerOrderLine.Location,
                                                BegInv = _locCustomerInvoice2.BegInv,
                                                AccountingPeriodicLine = _locCustomerOrderLine.AccountingPeriodicLine,
                                            };
                                            _saveDataCustomerInvoiceLine.Save();
                                            _saveDataCustomerInvoiceLine.Session.CommitTransaction();
                                        }
                                    }
                                }
                            }
                            #endregion SaveCustomerInvoiceLine

                            #region SaveCustomerInvoiceFreeItem
                            XPCollection<CustomerOrderFreeItem> _locCustomerOrderFreeItems = new XPCollection<CustomerOrderFreeItem>(_currSession,
                                                                                new GroupOperator(GroupOperatorType.And,
                                                                                new BinaryOperator("CustomerOrder", _locCustomerOrderXPO)));

                            CustomerInvoice _locCustomerInvoice3 = _currSession.FindObject<CustomerInvoice>
                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                     new BinaryOperator("SignCode", _currSignCode)));

                            if (_locCustomerInvoice3 != null)
                            {
                                if (_locCustomerOrderFreeItems != null && _locCustomerOrderFreeItems.Count > 0)
                                {
                                    foreach (CustomerOrderFreeItem _locCustomerOrderFreeItem in _locCustomerOrderFreeItems)
                                    {
                                        if (_locCustomerOrderFreeItem.Status == Status.Open || _locCustomerOrderFreeItem.Status == Status.Progress || _locCustomerOrderFreeItem.Status == Status.Posted)
                                        {
                                            CustomerInvoiceFreeItem _saveDataCustomerInvoiceFreeItem = new CustomerInvoiceFreeItem(_currSession)
                                            {
                                                CustomerInvoice = _locCustomerInvoice3,
                                                Company = _locCustomerOrderFreeItem.Company,
                                                Workplace = _locCustomerOrderFreeItem.Workplace,
                                                Division = _locCustomerInvoice3.Division,
                                                Department = _locCustomerInvoice3.Department,
                                                Section = _locCustomerInvoice3.Section,
                                                Employee = _locCustomerInvoice3.Employee,
                                                FreeItemRule = _locCustomerOrderFreeItem.FreeItemRule,
                                                FpItem = _locCustomerOrderFreeItem.FpItem,
                                                FpDQty = _locCustomerOrderFreeItem.FpDQty,
                                                FpDUOM = _locCustomerOrderFreeItem.FpDUOM,
                                                FpQty = _locCustomerOrderFreeItem.FpQty,
                                                FpUOM = _locCustomerOrderFreeItem.FpUOM,
                                                FpTQty = _locCustomerOrderFreeItem.FpTQty,
                                                FpStockType = _locCustomerOrderFreeItem.FpStockType,
                                                FpStockGroup = _locCustomerOrderFreeItem.FpStockGroup,
                                                Currency = _locCustomerOrderFreeItem.Currency,
                                                PriceGroup = _locCustomerOrderFreeItem.PriceGroup,
                                                PriceLine = _locCustomerOrderFreeItem.PriceLine,
                                                UAmount = _locCustomerOrderFreeItem.UAmount,
                                                TUAmount = _locCustomerOrderFreeItem.TUAmount,
                                                Location = _locCustomerOrderFreeItem.Location,
                                                BegInv = _locCustomerOrderFreeItem.BegInv,
                                                BegInvLine = _locCustomerOrderFreeItem.BegInvLine,
                                            };
                                            _saveDataCustomerInvoiceFreeItem.Save();
                                            _saveDataCustomerInvoiceFreeItem.Session.CommitTransaction();
                                            
                                        }
                                    }
                                }
                            }
                            #endregion SaveCustomerInvoiceFreeItem
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = CustomerOrder ", ex.ToString());
            }
        }

        private void SetStatusCustomerOrderLine(Session _currSession, CustomerOrder _locCustomerOrderXPO)
        {
            try
            {
                DateTime now = DateTime.Now;
                if (_locCustomerOrderXPO != null)
                {
                    XPCollection<CustomerOrderLine> _locCustomerOrderLines = new XPCollection<CustomerOrderLine>(_currSession,
                                                            new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("CustomerOrder", _locCustomerOrderXPO),
                                                            new BinaryOperator("Select", true),
                                                            new GroupOperator(GroupOperatorType.Or,
                                                            new BinaryOperator("Status", Status.Progress),
                                                            new BinaryOperator("Status", Status.Posted))));

                    if (_locCustomerOrderLines != null && _locCustomerOrderLines.Count > 0)
                    {
                        foreach (CustomerOrderLine _locCustomerOrderLine in _locCustomerOrderLines)
                        {
                            _locCustomerOrderLine.Status = Status.Close;
                            _locCustomerOrderLine.ActivationPosting = true;
                            _locCustomerOrderLine.Select = false;
                            _locCustomerOrderLine.StatusDate = now;
                            _locCustomerOrderLine.Save();
                            _locCustomerOrderLine.Session.CommitTransaction();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = CustomerOrder " + ex.ToString());
            }
        }

        private void SetStatusCustomerOrderFreeItem(Session _currSession, CustomerOrder _locCustomerOrderXPO)
        {
            try
            {
                DateTime now = DateTime.Now;
                if (_locCustomerOrderXPO != null)
                {
                    XPCollection<CustomerOrderFreeItem> _locCustomerOrderFreeItems = new XPCollection<CustomerOrderFreeItem>(_currSession,
                                                            new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("CustomerOrder", _locCustomerOrderXPO),
                                                            new GroupOperator(GroupOperatorType.Or,
                                                            new BinaryOperator("Status", Status.Open),
                                                            new BinaryOperator("Status", Status.Progress),
                                                            new BinaryOperator("Status", Status.Posted))));

                    if (_locCustomerOrderFreeItems != null && _locCustomerOrderFreeItems.Count > 0)
                    {
                        foreach (CustomerOrderFreeItem _locCustomerOrderFreeItem in _locCustomerOrderFreeItems)
                        {
                            _locCustomerOrderFreeItem.Status = Status.Close;
                            _locCustomerOrderFreeItem.StatusDate = now;
                            _locCustomerOrderFreeItem.Save();
                            _locCustomerOrderFreeItem.Session.CommitTransaction();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = CustomerOrder " + ex.ToString());
            }
        }

        private void SetNormalQuantity(Session _currSession, CustomerOrder _locCustomerOrderXPO)
        {
            try
            {
                if (_locCustomerOrderXPO != null)
                {
                    XPCollection<CustomerOrderLine> _locCustomerOrderLines = new XPCollection<CustomerOrderLine>(_currSession,
                                                                                     new GroupOperator(GroupOperatorType.And,
                                                                                     new BinaryOperator("CustomerOrder", _locCustomerOrderXPO),
                                                                                     new BinaryOperator("Select", true),
                                                                                     new GroupOperator(GroupOperatorType.Or,
                                                                                     new BinaryOperator("Status", Status.Progress),
                                                                                     new BinaryOperator("Status", Status.Posted))));

                    if (_locCustomerOrderLines != null && _locCustomerOrderLines.Count > 0)
                    {
                        foreach (CustomerOrderLine _locCustomerOrderLine in _locCustomerOrderLines)
                        {
                            if (_locCustomerOrderLine.DQty > 0 || _locCustomerOrderLine.Qty > 0)
                            {
                                _locCustomerOrderLine.Select = false;
                                _locCustomerOrderLine.Save();
                                _locCustomerOrderLine.Session.CommitTransaction();                           
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = CustomerOrder " + ex.ToString());
            }
        }

        private void SetFinalStatusCustomerOrder(Session _currSession, CustomerOrder _locCustomerOrderXPO)
        {
            try
            {
                DateTime now = DateTime.Now;
                int _locStatusCount = 0;
                int _locCustOrderLineCount = 0;

                if (_locCustomerOrderXPO != null)
                {

                    XPCollection<CustomerOrderLine> _locCustomerOrderLines = new XPCollection<CustomerOrderLine>(_currSession,
                                                            new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("CustomerOrder", _locCustomerOrderXPO)));

                    if (_locCustomerOrderLines != null && _locCustomerOrderLines.Count() > 0)
                    {
                        _locCustOrderLineCount = _locCustomerOrderLines.Count();

                        foreach (CustomerOrderLine _locCustomerOrderLine in _locCustomerOrderLines)
                        {
                            if (_locCustomerOrderLine.Status == Status.Close)
                            {
                                _locStatusCount = _locStatusCount + 1;
                            }
                        }
                    }

                    if (_locStatusCount == _locCustOrderLineCount)
                    {
                        _locCustomerOrderXPO.ActivationPosting = true;
                        _locCustomerOrderXPO.Status = Status.Close;
                        _locCustomerOrderXPO.StatusDate = now;
                        _locCustomerOrderXPO.PostedCount = _locCustomerOrderXPO.PostedCount + 1;
                        _locCustomerOrderXPO.Save();
                        _locCustomerOrderXPO.Session.CommitTransaction();
                    }
                    else
                    {
                        _locCustomerOrderXPO.Status = Status.Posted;
                        _locCustomerOrderXPO.StatusDate = now;
                        _locCustomerOrderXPO.PostedCount = _locCustomerOrderXPO.PostedCount + 1;
                        _locCustomerOrderXPO.Save();
                        _locCustomerOrderXPO.Session.CommitTransaction();
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = CustomerOrder " + ex.ToString());
            }
        }

        #endregion Posting

        #region Global Method

        private void SuccessMessageShow(string _locActionName)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 2000;
            options.Message = string.Format("{0} !", _locActionName);
            options.Type = InformationType.Success;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Success";
            options.Win.Type = WinMessageType.Flyout;
            Application.ShowViewStrategy.ShowMessage(options);
        }

        private void ErrorMessageShow(string _locMessage)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 2000;
            options.Message = string.Format(_locMessage);
            options.Type = InformationType.Warning;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Error";
            options.Win.Type = WinMessageType.Alert;
            Application.ShowViewStrategy.ShowMessage(options);
        }

        #endregion Global Method

        
    }
}
