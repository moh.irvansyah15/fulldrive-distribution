﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;

using FullDrive.Module.BusinessObjects;
using DevExpress.ExpressApp.Xpo;
using DevExpress.Xpo;

namespace FullDrive.Module.Controllers
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class CustomerOrderListviewFilterController : ViewController
    {
        public CustomerOrderListviewFilterController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.
            TargetViewId = "CustomerOrder_ListView";
        }

        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
            #region FilterBaseOnOrganization
            XPCollection<OrganizationSetupDetail> _locOrgSetDetail1s = null;
            XPCollection<OrganizationSetupDetail> _locOrgSetDetail2s = null;
            XPCollection<OrganizationSetupDetail> _locOrganizationSetupDetails = null;
            var User = SecuritySystem.CurrentUserName;
            string _beginString1 = null;
            string _endString1 = null;
            string _fullString1 = null;
            string _beginString2 = null;
            string _endString2 = null;
            string _fullString2 = null;
            string _beginString3 = null;
            string _endString3 = null;
            string _fullString3 = null;
            string _beginString4 = null;
            string _endString4 = null;
            string _fullString4 = null;
            string _beginString5 = null;
            string _endString5 = null;
            string _fullString5 = null;
            string _beginString6 = null;
            string _endString6 = null;
            string _fullString6 = null;
            string _fullString = null;
            var ListView = View as ListView;
            Session currentSession = null;

            if (this.ObjectSpace != null)
            {
                UserAccess _locUserAccess = this.ObjectSpace.FindObject<UserAccess>(new BinaryOperator("UserName", User));
                if (_locUserAccess.Session != null)
                {
                    currentSession = _locUserAccess.Session;
                }
                if (_locUserAccess != null && currentSession != null)
                {
                    if (_locUserAccess.Employee != null)
                    {
                        List<string> _stringCompany = new List<string>();
                        List<string> _stringWorkplace = new List<string>();
                        List<string> _stringDivision = new List<string>();
                        List<string> _stringDepartment = new List<string>();
                        List<string> _stringSection = new List<string>();
                        List<string> _stringEmployee = new List<string>();

                        _locOrgSetDetail1s = new XPCollection<OrganizationSetupDetail>
                                            (currentSession, new GroupOperator(GroupOperatorType.And,
                                            new BinaryOperator("UserAccess", _locUserAccess),
                                            new BinaryOperator("Process", CustomProcess.Process.CustomerOrder),
                                            new BinaryOperator("PageType", CustomProcess.PageType.ListView),
                                            new BinaryOperator("Active", true)));

                        if (_locOrgSetDetail1s != null && _locOrgSetDetail1s.Count() > 0)
                        {
                            _locOrganizationSetupDetails = _locOrgSetDetail1s;
                        }
                        else
                        {
                            _locOrgSetDetail2s = new XPCollection<OrganizationSetupDetail>
                                                (currentSession, new GroupOperator(GroupOperatorType.And,
                                                new BinaryOperator("UserAccess", _locUserAccess),
                                                new BinaryOperator("ObjectList", CustomProcess.ObjectList.CustomerOrder),
                                                new BinaryOperator("PageType", CustomProcess.PageType.ListView),
                                                new BinaryOperator("Active", true)));
                            if (_locOrgSetDetail2s != null && _locOrgSetDetail2s.Count() > 0)
                            {
                                _locOrganizationSetupDetails = _locOrgSetDetail2s;
                            }
                        }

                        #region TakeDataFromOrganizationSetupDetail
                        if (_locOrganizationSetupDetails != null && _locOrganizationSetupDetails.Count > 0)
                        {
                            foreach (OrganizationSetupDetail _locOrganizationSetupDetail in _locOrganizationSetupDetails)
                            {
                                if (_locOrganizationSetupDetail.Company != null)
                                {
                                    if (_locOrganizationSetupDetail.Company.Code != null) { _stringCompany.Add(_locOrganizationSetupDetail.Company.Code); }
                                }
                                if (_locOrganizationSetupDetail.Workplace != null)
                                {
                                    if (_locOrganizationSetupDetail.Workplace.Code != null) { _stringWorkplace.Add(_locOrganizationSetupDetail.Workplace.Code); }
                                }
                                if (_locOrganizationSetupDetail.Division != null)
                                {
                                    if (_locOrganizationSetupDetail.Division.Code != null) { _stringDivision.Add(_locOrganizationSetupDetail.Division.Code); }
                                }
                                if (_locOrganizationSetupDetail.Department != null)
                                {
                                    if (_locOrganizationSetupDetail.Department.Code != null) { _stringDepartment.Add(_locOrganizationSetupDetail.Department.Code); }
                                }
                                if (_locOrganizationSetupDetail.Section != null)
                                {
                                    if (_locOrganizationSetupDetail.Section.Code != null) { _stringSection.Add(_locOrganizationSetupDetail.Section.Code); }
                                }
                                if (_locOrganizationSetupDetail.Employee != null)
                                {
                                    if (_locOrganizationSetupDetail.Employee.Code != null) { _stringEmployee.Add(_locOrganizationSetupDetail.Employee.Code); }
                                }
                            }
                        }
                        #endregion TakeDataFromOrganizationSetupDetail
                        #region TakeDataDirectFromUser
                        else
                        {
                            if (_locUserAccess.Employee.Company != null)
                            {
                                if (_locUserAccess.Employee.Company.Code != null) { _stringCompany.Add(_locUserAccess.Employee.Company.Code); }
                            }
                            if (_locUserAccess.Employee.Workplace != null)
                            {
                                if (_locUserAccess.Employee.Workplace.Code != null) { _stringWorkplace.Add(_locUserAccess.Employee.Workplace.Code); }
                            }
                            if (_locUserAccess.Employee.Division != null)
                            {
                                if (_locUserAccess.Employee.Division.Code != null) { _stringDivision.Add(_locUserAccess.Employee.Division.Code); }
                            }
                            if (_locUserAccess.Employee.Department != null)
                            {
                                if (_locUserAccess.Employee.Department.Code != null) { _stringDepartment.Add(_locUserAccess.Employee.Department.Code); }
                            }
                            if (_locUserAccess.Employee.Section != null)
                            {
                                if (_locUserAccess.Employee.Section.Code != null) { _stringSection.Add(_locUserAccess.Employee.Section.Code); }
                            }
                            if (_locUserAccess.Employee.Code != null)
                            {
                                _stringEmployee.Add(_locUserAccess.Employee.Code);
                            }
                        }
                        #endregion TakeDataDirectFromUserv

                        #region Company
                        IEnumerable<string> _stringArrayCompanyDistinct = _stringCompany.Distinct();
                        string[] _stringArrayCompanyList = _stringArrayCompanyDistinct.ToArray();
                        if (_stringArrayCompanyList.Length == 1)
                        {
                            for (int i = 0; i < _stringArrayCompanyList.Length; i++)
                            {
                                Company _locCompany = currentSession.FindObject<Company>(new BinaryOperator("Code", _stringArrayCompanyList[i]));
                                if (_locCompany != null)
                                {
                                    if (i == 0)
                                    {
                                        _beginString1 = "[Company.Code]=='" + _locCompany.Code + "'";
                                    }
                                }
                            }
                        }
                        else if (_stringArrayCompanyList.Length > 1)
                        {
                            for (int i = 0; i < _stringArrayCompanyList.Length; i++)
                            {
                                Company _locCompany = currentSession.FindObject<Company>(new BinaryOperator("Code", _stringArrayCompanyList[i]));
                                if (_locCompany != null)
                                {
                                    if (i == 0)
                                    {
                                        _beginString1 = "[Company.Code]=='" + _locCompany.Code + "'";
                                    }
                                    else
                                    {
                                        _endString1 = _endString1 + " OR [Company.Code]=='" + _locCompany.Code + "'";
                                    }
                                }
                            }
                        }
                        _fullString1 = _beginString1 + _endString1;

                        #endregion Company

                        #region Workplace
                        IEnumerable<string> _stringArrayWorkplaceDistinct = _stringWorkplace.Distinct();
                        string[] _stringArrayWorkplaceList = _stringArrayWorkplaceDistinct.ToArray();
                        if (_stringArrayWorkplaceList.Length == 1)
                        {
                            for (int i = 0; i < _stringArrayWorkplaceList.Length; i++)
                            {
                                Workplace _locWorkplace = currentSession.FindObject<Workplace>(new BinaryOperator("Code", _stringArrayWorkplaceList[i]));
                                if (_locWorkplace != null)
                                {
                                    if (i == 0)
                                    {
                                        _beginString2 = "[Workplace.Code]=='" + _locWorkplace.Code + "'";
                                    }
                                }
                            }
                        }
                        else if (_stringArrayWorkplaceList.Length > 1)
                        {
                            for (int i = 0; i < _stringArrayWorkplaceList.Length; i++)
                            {
                                Workplace _locWorkplace = currentSession.FindObject<Workplace>(new BinaryOperator("Code", _stringArrayWorkplaceList[i]));
                                if (_locWorkplace != null)
                                {
                                    if (i == 0)
                                    {
                                        _beginString2 = "[Workplace.Code]=='" + _locWorkplace.Code + "'";
                                    }
                                    else
                                    {
                                        _endString2 = _endString2 + " OR [Workplace.Code]=='" + _locWorkplace.Code + "'";
                                    }
                                }

                            }
                        }
                        _fullString2 = _beginString2 + _endString2;
                        #endregion Workplace

                        #region Division
                        IEnumerable<string> _stringArrayDivisionDistinct = _stringDivision.Distinct();
                        string[] _stringArrayDivisionList = _stringArrayDivisionDistinct.ToArray();
                        if (_stringArrayDivisionList.Length == 1)
                        {
                            for (int i = 0; i < _stringArrayDivisionList.Length; i++)
                            {
                                Division _locDivision = currentSession.FindObject<Division>(new BinaryOperator("Code", _stringArrayDivisionList[i]));
                                if (_locDivision != null)
                                {
                                    if (i == 0)
                                    {
                                        _beginString3 = "[Division.Code]=='" + _locDivision.Code + "'";
                                    }
                                }
                            }
                        }
                        else if (_stringArrayDivisionList.Length > 1)
                        {
                            for (int i = 0; i < _stringArrayDivisionList.Length; i++)
                            {
                                Division _locDivision = currentSession.FindObject<Division>(new BinaryOperator("Code", _stringArrayDivisionList[i]));
                                if (_locDivision != null)
                                {
                                    if (i == 0)
                                    {
                                        _beginString3 = "[Division.Code]=='" + _locDivision.Code + "'";
                                    }
                                    else
                                    {
                                        _endString3 = _endString3 + " OR [Division.Code]=='" + _locDivision.Code + "'";
                                    }
                                }
                            }
                        }
                        _fullString3 = _beginString3 + _endString3;

                        #endregion Division

                        #region Department
                        IEnumerable<string> _stringArrayDepartmentDistinct = _stringDepartment.Distinct();
                        string[] _stringArrayDepartmentList = _stringArrayDepartmentDistinct.ToArray();
                        if (_stringArrayDepartmentList.Length == 1)
                        {
                            for (int i = 0; i < _stringArrayDepartmentList.Length; i++)
                            {
                                Department _locDepartment = currentSession.FindObject<Department>(new BinaryOperator("Code", _stringArrayDepartmentList[i]));
                                if (_locDepartment != null)
                                {
                                    if (i == 0)
                                    {
                                        _beginString4 = "[Department.Code]=='" + _locDepartment.Code + "'";
                                    }
                                }
                            }
                        }
                        else if (_stringArrayDepartmentList.Length > 1)
                        {
                            for (int i = 0; i < _stringArrayDepartmentList.Length; i++)
                            {
                                Department _locDepartment = currentSession.FindObject<Department>(new BinaryOperator("Code", _stringArrayDepartmentList[i]));
                                if (_locDepartment != null)
                                {
                                    if (i == 0)
                                    {
                                        _beginString4 = "[Department.Code]=='" + _locDepartment.Code + "'";
                                    }
                                    else
                                    {
                                        _endString4 = _endString4 + " OR [Department.Code]=='" + _locDepartment.Code + "'";
                                    }
                                }

                            }
                        }
                        _fullString4 = _beginString4 + _endString4;

                        #endregion Department

                        #region Section
                        IEnumerable<string> _stringArraySectionDistinct = _stringSection.Distinct();
                        string[] _stringArraySectionList = _stringArraySectionDistinct.ToArray();
                        if (_stringArraySectionList.Length == 1)
                        {
                            for (int i = 0; i < _stringArraySectionList.Length; i++)
                            {
                                Section _locSection = currentSession.FindObject<Section>(new BinaryOperator("Code", _stringArraySectionList[i]));
                                if (_locSection != null)
                                {
                                    if (i == 0)
                                    {
                                        _beginString5 = "[Section.Code]=='" + _locSection.Code + "'";
                                    }
                                }
                            }
                        }
                        else if (_stringArraySectionList.Length > 1)
                        {
                            for (int i = 0; i < _stringArraySectionList.Length; i++)
                            {
                                Section _locSection = currentSession.FindObject<Section>(new BinaryOperator("Code", _stringArraySectionList[i]));
                                if (_locSection != null)
                                {
                                    if (i == 0)
                                    {
                                        _beginString5 = "[Section.Code]=='" + _locSection.Code + "'";
                                    }
                                    else
                                    {
                                        _endString5 = _endString5 + " OR [Section.Code]=='" + _locSection.Code + "'";
                                    }
                                }
                            }
                        }
                        _fullString5 = _beginString5 + _endString5;

                        #endregion Section

                        #region Employee
                        IEnumerable<string> _stringArrayEmployeeDistinct = _stringEmployee.Distinct();
                        string[] _stringArrayEmployeeList = _stringArrayEmployeeDistinct.ToArray();
                        if (_stringArrayEmployeeList.Length == 1)
                        {
                            for (int i = 0; i < _stringArrayEmployeeList.Length; i++)
                            {
                                Employee _locEmployee = currentSession.FindObject<Employee>(new BinaryOperator("Code", _stringArrayEmployeeList[i]));
                                if (_locEmployee != null)
                                {
                                    if (i == 0)
                                    {
                                        _beginString6 = "[Employee.Code]=='" + _locEmployee.Code + "'";
                                    }
                                }
                            }
                        }
                        else if (_stringArrayEmployeeList.Length > 1)
                        {
                            for (int i = 0; i < _stringArrayEmployeeList.Length; i++)
                            {
                                Employee _locEmployee = currentSession.FindObject<Employee>(new BinaryOperator("Code", _stringArrayEmployeeList[i]));
                                if (_locEmployee != null)
                                {
                                    if (i == 0)
                                    {
                                        _beginString6 = "[Employee.Code]=='" + _locEmployee.Code + "'";
                                    }
                                    else
                                    {
                                        _endString6 = _endString6 + " OR [Employee.Code]=='" + _locEmployee.Code + "'";
                                    }
                                }

                            }
                        }
                        _fullString6 = _beginString6 + _endString6;

                        #endregion Employee

                        //Part1
                        if (_stringArrayCompanyList.Length >= 1 && _stringArrayWorkplaceList.Length >= 1 && _stringArrayDivisionList.Length >= 1 && _stringArrayDepartmentList.Length >= 1 && _stringArraySectionList.Length >= 1 && _stringArrayEmployeeList.Length >= 1)
                        {
                            _fullString = "( " + _fullString1 + " ) AND ( " + _fullString2 + " ) AND ( " + _fullString3 + " ) AND ( " + _fullString4 + " ) AND ( " + _fullString5 + " ) AND ( " + _fullString6 + " )";
                        }
                        else if (_stringArrayCompanyList.Length >= 1 && _stringArrayWorkplaceList.Length >= 1 && _stringArrayDivisionList.Length >= 1 && _stringArrayDepartmentList.Length >= 1 && _stringArraySectionList.Length >= 1 && _stringArrayEmployeeList.Length == 0)
                        {
                            _fullString = "( " + _fullString1 + " ) AND ( " + _fullString2 + " ) AND ( " + _fullString3 + " ) AND ( " + _fullString4 + " ) AND ( " + _fullString5 + " ) ";
                        }
                        else if (_stringArrayCompanyList.Length >= 1 && _stringArrayWorkplaceList.Length >= 1 && _stringArrayDivisionList.Length >= 1 && _stringArrayDepartmentList.Length >= 1 && _stringArraySectionList.Length == 0 && _stringArrayEmployeeList.Length == 0)
                        {
                            _fullString = "( " + _fullString1 + " ) AND ( " + _fullString2 + " ) AND ( " + _fullString3 + " ) AND ( " + _fullString4 + " )";
                        }
                        else if (_stringArrayCompanyList.Length >= 1 && _stringArrayWorkplaceList.Length >= 1 && _stringArrayDivisionList.Length >= 1 && _stringArrayDepartmentList.Length == 0 && _stringArraySectionList.Length == 0 && _stringArrayEmployeeList.Length == 0)
                        {
                            _fullString = "( " + _fullString1 + " ) AND ( " + _fullString2 + " ) AND ( " + _fullString3 + " )";
                        }
                        else if (_stringArrayCompanyList.Length >= 1 && _stringArrayWorkplaceList.Length >= 1 && _stringArrayDivisionList.Length == 0 && _stringArrayDepartmentList.Length == 0 && _stringArraySectionList.Length == 0 && _stringArrayEmployeeList.Length == 0)
                        {
                            _fullString = "( " + _fullString1 + " ) AND ( " + _fullString2 + " )";
                        }
                        else if (_stringArrayCompanyList.Length >= 1 && _stringArrayWorkplaceList.Length == 0 && _stringArrayDivisionList.Length == 0 && _stringArrayDepartmentList.Length == 0 && _stringArraySectionList.Length == 0 && _stringArrayEmployeeList.Length == 0)
                        {
                            _fullString = _fullString1;
                        }
                        else if (_stringArrayCompanyList.Length >= 1 && _stringArrayWorkplaceList.Length >= 1 && _stringArrayDivisionList.Length >= 1 && _stringArrayDepartmentList.Length >= 1 && _stringArraySectionList.Length == 0 && _stringArrayEmployeeList.Length >= 1)
                        {
                            _fullString = "( " + _fullString1 + " ) AND ( " + _fullString2 + " ) AND ( " + _fullString3 + " ) AND ( " + _fullString4 + " ) AND ( " + _fullString6 + " )";
                        }
                        else if (_stringArrayCompanyList.Length >= 1 && _stringArrayWorkplaceList.Length >= 1 && _stringArrayDivisionList.Length >= 1 && _stringArrayDepartmentList.Length == 0 && _stringArraySectionList.Length == 0 && _stringArrayEmployeeList.Length >= 1)
                        {
                            _fullString = "( " + _fullString1 + " ) AND ( " + _fullString2 + " ) AND ( " + _fullString3 + " ) AND ( " + _fullString6 + " )";
                        }
                        else if (_stringArrayCompanyList.Length >= 1 && _stringArrayWorkplaceList.Length >= 1 && _stringArrayDivisionList.Length == 0 && _stringArrayDepartmentList.Length == 0 && _stringArraySectionList.Length == 0 && _stringArrayEmployeeList.Length >= 1)
                        {
                            _fullString = "( " + _fullString1 + " ) AND ( " + _fullString2 + " ) AND ( " + _fullString6 + " )";
                        }
                        else if (_stringArrayCompanyList.Length >= 1 && _stringArrayWorkplaceList.Length == 0 && _stringArrayDivisionList.Length == 0 && _stringArrayDepartmentList.Length == 0 && _stringArraySectionList.Length == 0 && _stringArrayEmployeeList.Length >= 1)
                        {
                            _fullString = "( " + _fullString1 + " ) AND ( " + _fullString6 + " )";
                        }

                        if(_fullString != null)
                        {
                            ListView.CollectionSource.Criteria["CustomerOrderFilter"] = CriteriaOperator.Parse(_fullString);
                        }
                        
                    }
                }
            }
            #endregion FilterBaseOnOrganization
        }

        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }

        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }
    }
}
