﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;

using FullDrive.Module.BusinessObjects;
using FullDrive.Module.CustomProcess;
using System.Collections;
using DevExpress.Xpo;
using DevExpress.ExpressApp.Xpo;
using System.IO;
using System.Web;

namespace FullDrive.Module.Controllers
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class ApplicationImportActionController : ViewController
    {
        public ApplicationImportActionController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.
        }
        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }
        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        private void ApplicationImportImportDataAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;

                if (_objectsToProcess != null)
                {
                    foreach (Object obj in _objectsToProcess)
                    {
                        ApplicationImport _locApplicationImportOS = (ApplicationImport)_objectSpace.GetObject(obj);

                        if (_locApplicationImportOS != null)
                        {
                            if (_locApplicationImportOS.Session != null)
                            {
                                _currSession = _locApplicationImportOS.Session;
                            }

                            if (_locApplicationImportOS.Code != null && _currSession != null)
                            {
                                _currObjectId = _locApplicationImportOS.Code;

                                ApplicationImport _locApplicationImportXPO = _currSession.FindObject<ApplicationImport>
                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                     new BinaryOperator("Code", _currObjectId)));

                                if (_locApplicationImportXPO != null)
                                {
                                    string targetpath = null;
                                    if (_locApplicationImportXPO.DataImport != null && _locApplicationImportXPO.Message != "File is already available in server")
                                    {
                                        targetpath = HttpContext.Current.Server.MapPath("~/UploadFile/" + _locApplicationImportXPO.DataImport.FileName);
                                        string ext = System.IO.Path.GetExtension(_locApplicationImportXPO.DataImport.FileName);
                                        FileStream fileStream = new FileStream(targetpath, FileMode.OpenOrCreate);
                                        _locApplicationImportXPO.DataImport.SaveToStream(fileStream);
                                        fileStream.Close();
                                        if (File.Exists(targetpath))
                                        {
                                            _globFunc.ImportDataExcel(_currSession, targetpath, ext, _locApplicationImportXPO.ObjectList);
                                        }
                                        _locApplicationImportXPO.DataImport.Clear();
                                        SuccessMessageShow(_locApplicationImportXPO.Code + " has been change successfully to Import Data");
                                    }

                                }
                                else
                                {
                                    ErrorMessageShow("Data Not Available");
                                }

                            }
                            else
                            {
                                ErrorMessageShow("Account Reclass Journal Transaction Not Available");
                            }
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = ApplicationImport " + ex.ToString());
            }
        }

        //============================================== Code In Here ==============================================

        #region Global Method

        private void SuccessMessageShow(string _locMessage)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 4000;
            options.Message = string.Format("{0}", _locMessage);
            options.Type = InformationType.Success;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Success";
            options.Win.Type = WinMessageType.Flyout;
            Application.ShowViewStrategy.ShowMessage(options);
        }

        private void ErrorMessageShow(string _locMessage)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 2000;
            options.Message = string.Format(_locMessage);
            options.Type = InformationType.Warning;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Error";
            options.Win.Type = WinMessageType.Alert;
            Application.ShowViewStrategy.ShowMessage(options);
        }

        #endregion Global Method
    }
}
