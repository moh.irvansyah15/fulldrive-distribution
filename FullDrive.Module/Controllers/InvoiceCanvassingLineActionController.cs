﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;

using FullDrive.Module.BusinessObjects;
using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.Xpo;
using System.Collections;
using DevExpress.Xpo;

namespace FullDrive.Module.Controllers
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class InvoiceCanvassingLineActionController : ViewController
    {
        public InvoiceCanvassingLineActionController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.
        }
        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }
        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        private void InvoiceCanvassingLineTaxAndDiscountAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;

                if (_objectToProcess != null)
                {
                    foreach (Object obj in _objectToProcess)
                    {
                        InvoiceCanvassingLine _locInvoiceCanvassingLineOS = (InvoiceCanvassingLine)_objectSpace.GetObject(obj);

                        if (_locInvoiceCanvassingLineOS != null)
                        {
                            if (_locInvoiceCanvassingLineOS.Session != null)
                            {
                                _currSession = _locInvoiceCanvassingLineOS.Session;
                            }

                            if (_locInvoiceCanvassingLineOS.Code != null && _currSession != null)
                            {
                                _currObjectId = _locInvoiceCanvassingLineOS.Code;

                                XPCollection<InvoiceCanvassingLine> _locInvoiceCanvassingLines = new XPCollection<InvoiceCanvassingLine>
                                                                                                     (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                      new BinaryOperator("Code", _currObjectId),
                                                                                                      new GroupOperator(GroupOperatorType.Or,
                                                                                                      new BinaryOperator("Status", Status.Progress),
                                                                                                      new BinaryOperator("Status", Status.Open)
                                                                                                      )));

                                if (_locInvoiceCanvassingLines != null && _locInvoiceCanvassingLines.Count > 0)
                                {
                                    foreach (InvoiceCanvassingLine _locInvoiceCanvassingLine in _locInvoiceCanvassingLines)
                                    {
                                        SetTaxBeforeDiscount(_currSession, _locInvoiceCanvassingLine);
                                        SetDiscountGross(_currSession, _locInvoiceCanvassingLine);
                                        SetTaxAfterDiscount(_currSession, _locInvoiceCanvassingLine);
                                        SetDiscountNet(_currSession, _locInvoiceCanvassingLine);
                                    }

                                    SuccessMessageShow("InvoiceCanvassingLine Line has been Processed");
                                }
                                else
                                {
                                    ErrorMessageShow("Data InvoiceCanvassingLine Line Not Available");
                                }
                            }
                        }
                    }
                    if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                    {
                        _objectSpace.CommitChanges();
                        _objectSpace.Refresh();
                    }
                    if (View is ListView)
                    {
                        _objectSpace.CommitChanges();
                        View.ObjectSpace.Refresh();
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("BusinessObject = InvoiceCanvassingLine" + ex.ToString());
            }
        }

        //======================================== Code In Here ========================================

        #region Tax
        private void SetTaxBeforeDiscount(Session _currSession, InvoiceCanvassingLine _locInvoiceCanvassingLineXPO)
        {
            try
            {
                double _locTxAmount = 0;
                double _locTAmount = 0;
                if(_locInvoiceCanvassingLineXPO != null)
                {
                    if(_locInvoiceCanvassingLineXPO.Tax != null)
                    {
                        if(_locInvoiceCanvassingLineXPO.Tax.TaxRule == TaxRule.BeforeDiscount )
                        {
                            _locTxAmount = _locInvoiceCanvassingLineXPO.TxValue / 100 * _locInvoiceCanvassingLineXPO.TUAmount;
                            if(_locInvoiceCanvassingLineXPO.Tax.TaxNature == TaxNature.Increase)
                            {
                                _locTAmount = _locInvoiceCanvassingLineXPO.TAmount + _locTxAmount;
                            }
                            if(_locInvoiceCanvassingLineXPO.Tax.TaxNature == TaxNature.Decrease)
                            {
                                _locTAmount = _locInvoiceCanvassingLineXPO.TAmount - _locTxAmount;
                            }
                            _locInvoiceCanvassingLineXPO.TxAmount = _locTxAmount;
                            _locInvoiceCanvassingLineXPO.TAmount = _locTAmount;
                            _locInvoiceCanvassingLineXPO.Save();
                            _locInvoiceCanvassingLineXPO.Session.CommitTransaction();
                        }  
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("BusinessObject = InvoiceCanvassingLine" + ex.ToString());
            }
        }

        private void SetDiscountGross(Session _currSession, InvoiceCanvassingLine _locInvoiceCanvassingLineXPO)
        {
            try
            {
                if (_locInvoiceCanvassingLineXPO != null)
                {
                    if (_locInvoiceCanvassingLineXPO.DiscountRule != null)
                    {
                        double _locTUAmountDiscount = 0;

                        #region Gross
                        if(_locInvoiceCanvassingLineXPO.DiscountRule.RuleType == RuleType.Rule2 )
                        {
                            if (_locInvoiceCanvassingLineXPO.DiscountRule.GrossAmountRule == true)
                            {
                                if (_locInvoiceCanvassingLineXPO.TUAmount > 0 && _locInvoiceCanvassingLineXPO.TUAmount >= _locInvoiceCanvassingLineXPO.DiscountRule.GrossTotalUAmount)
                                {
                                    if (_locInvoiceCanvassingLineXPO.DiscountRule.DiscountType == DiscountType.Percentage)
                                    {
                                        _locTUAmountDiscount = _locInvoiceCanvassingLineXPO.TUAmount * _locInvoiceCanvassingLineXPO.DiscountRule.Value / 100;
                                    }
                                    if (_locInvoiceCanvassingLineXPO.DiscountRule.DiscountType == DiscountType.Value)
                                    {
                                        _locTUAmountDiscount = _locInvoiceCanvassingLineXPO.DiscountRule.Value;
                                    }

                                    _locInvoiceCanvassingLineXPO.DiscountChecked = true;
                                    _locInvoiceCanvassingLineXPO.DiscAmount = _locTUAmountDiscount;
                                    _locInvoiceCanvassingLineXPO.TAmount = _locInvoiceCanvassingLineXPO.TAmount - _locTUAmountDiscount;
                                    _locInvoiceCanvassingLineXPO.Save();
                                    _locInvoiceCanvassingLineXPO.Session.CommitTransaction();
                                }

                            }
                        }
                        #endregion Gross
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("BusinessObject = InvoiceCanvassingLine" + ex.ToString());
            }
        }

        private void SetTaxAfterDiscount(Session _currSession, InvoiceCanvassingLine _locInvoiceCanvassingLineXPO)
        {
            try
            {
                double _locTxAmount = 0;
                double _locTAmount = 0;
                if (_locInvoiceCanvassingLineXPO != null)
                {
                    if (_locInvoiceCanvassingLineXPO.Tax != null && _locInvoiceCanvassingLineXPO.DiscountRule != null)
                    {
                        if (_locInvoiceCanvassingLineXPO.Tax.TaxRule == TaxRule.AfterDiscount && _locInvoiceCanvassingLineXPO.DiscountRule.GrossAmountRule == true)
                        {
                            _locTxAmount = _locInvoiceCanvassingLineXPO.TxValue / 100 * (_locInvoiceCanvassingLineXPO.TUAmount - _locInvoiceCanvassingLineXPO.DiscAmount);

                            if (_locInvoiceCanvassingLineXPO.Tax.TaxNature == TaxNature.Increase)
                            {
                                _locTAmount = _locInvoiceCanvassingLineXPO.TAmount + _locTxAmount;
                            }
                            if (_locInvoiceCanvassingLineXPO.Tax.TaxNature == TaxNature.Decrease)
                            {
                                _locTAmount = _locInvoiceCanvassingLineXPO.TAmount - _locTxAmount;
                            }
                            _locInvoiceCanvassingLineXPO.TxAmount = _locTxAmount;
                            _locInvoiceCanvassingLineXPO.TAmount = _locTAmount;
                            _locInvoiceCanvassingLineXPO.Save();
                            _locInvoiceCanvassingLineXPO.Session.CommitTransaction();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("BusinessObject = InvoiceCanvassingLine" + ex.ToString());
            }
        }

        private void SetDiscountNet(Session _currSession, InvoiceCanvassingLine _locInvoiceCanvassingLineXPO)
        {
            try
            {
                if (_locInvoiceCanvassingLineXPO != null)
                {
                    if (_locInvoiceCanvassingLineXPO.DiscountRule != null && _locInvoiceCanvassingLineXPO.Tax != null)
                    {
                        double _locTUAmountDiscount = 0;
                        
                        #region Net

                        if(_locInvoiceCanvassingLineXPO.DiscountRule.RuleType == RuleType.Rule2)
                        {
                            if (_locInvoiceCanvassingLineXPO.DiscountRule.NetAmountRule == true && _locInvoiceCanvassingLineXPO.Tax.TaxRule == TaxRule.BeforeDiscount)
                            {
                                if (_locInvoiceCanvassingLineXPO.TAmount > 0 && _locInvoiceCanvassingLineXPO.TAmount >= _locInvoiceCanvassingLineXPO.DiscountRule.NetTotalUAmount)
                                {
                                    if (_locInvoiceCanvassingLineXPO.DiscountRule.DiscountType == DiscountType.Percentage)
                                    {
                                        _locTUAmountDiscount = _locInvoiceCanvassingLineXPO.TAmount * _locInvoiceCanvassingLineXPO.DiscountRule.Value / 100;
                                    }
                                    if (_locInvoiceCanvassingLineXPO.DiscountRule.DiscountType == DiscountType.Value)
                                    {
                                        _locTUAmountDiscount = _locInvoiceCanvassingLineXPO.DiscountRule.Value;
                                    }

                                    _locInvoiceCanvassingLineXPO.DiscountChecked = true;
                                    _locInvoiceCanvassingLineXPO.DiscAmount = _locTUAmountDiscount;
                                    _locInvoiceCanvassingLineXPO.TAmount = _locInvoiceCanvassingLineXPO.TAmount - _locTUAmountDiscount;
                                    _locInvoiceCanvassingLineXPO.Save();
                                    _locInvoiceCanvassingLineXPO.Session.CommitTransaction();
                                }
                            }
                        }
                        
                        #endregion Net
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("BusinessObject = InvoiceCanvassingLine" + ex.ToString());
            }
        }
        #endregion Tax

        #region Global Method

        private void SuccessMessageShow(string _locActionName)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 2000;
            options.Message = string.Format("{0}", _locActionName);
            options.Type = InformationType.Success;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Success";
            options.Win.Type = WinMessageType.Flyout;
            Application.ShowViewStrategy.ShowMessage(options);
        }

        private void ErrorMessageShow(string _locMessage)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 2000;
            options.Message = string.Format(_locMessage);
            options.Type = InformationType.Warning;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Error";
            options.Win.Type = WinMessageType.Alert;
            Application.ShowViewStrategy.ShowMessage(options);
        }

        #endregion Global Method
    }
}
