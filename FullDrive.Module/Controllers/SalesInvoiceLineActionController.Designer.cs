﻿namespace FullDrive.Module.Controllers
{
    partial class SalesInvoiceLineActionController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.SalesInvoiceLineSelectAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.SalesInvoiceLineUnselectAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.SalesInvoiceLineTaxAndDiscountAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            // 
            // SalesInvoiceLineSelectAction
            // 
            this.SalesInvoiceLineSelectAction.Caption = "Select";
            this.SalesInvoiceLineSelectAction.ConfirmationMessage = null;
            this.SalesInvoiceLineSelectAction.Id = "SalesInvoiceLineSelectActionId";
            this.SalesInvoiceLineSelectAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.SalesInvoiceLine);
            this.SalesInvoiceLineSelectAction.TargetViewType = DevExpress.ExpressApp.ViewType.ListView;
            this.SalesInvoiceLineSelectAction.ToolTip = null;
            this.SalesInvoiceLineSelectAction.TypeOfView = typeof(DevExpress.ExpressApp.ListView);
            this.SalesInvoiceLineSelectAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.SalesInvoiceLineSelectAction_Execute);
            // 
            // SalesInvoiceLineUnselectAction
            // 
            this.SalesInvoiceLineUnselectAction.Caption = "Unselect";
            this.SalesInvoiceLineUnselectAction.ConfirmationMessage = null;
            this.SalesInvoiceLineUnselectAction.Id = "SalesInvoiceLineUnselectActionId";
            this.SalesInvoiceLineUnselectAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.SalesInvoiceLine);
            this.SalesInvoiceLineUnselectAction.TargetViewType = DevExpress.ExpressApp.ViewType.ListView;
            this.SalesInvoiceLineUnselectAction.ToolTip = null;
            this.SalesInvoiceLineUnselectAction.TypeOfView = typeof(DevExpress.ExpressApp.ListView);
            this.SalesInvoiceLineUnselectAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.SalesInvoiceLineUnselectAction_Execute);
            // 
            // SalesInvoiceLineTaxAndDiscountAction
            // 
            this.SalesInvoiceLineTaxAndDiscountAction.Caption = "Tax And Discount";
            this.SalesInvoiceLineTaxAndDiscountAction.ConfirmationMessage = null;
            this.SalesInvoiceLineTaxAndDiscountAction.Id = "SalesInvoiceLineTaxAndDiscountActionId";
            this.SalesInvoiceLineTaxAndDiscountAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.SalesInvoiceLine);
            this.SalesInvoiceLineTaxAndDiscountAction.ToolTip = null;
            this.SalesInvoiceLineTaxAndDiscountAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.SalesInvoiceLineTaxAndDiscountAction_Execute);
            // 
            // SalesInvoiceLineActionController
            // 
            this.Actions.Add(this.SalesInvoiceLineSelectAction);
            this.Actions.Add(this.SalesInvoiceLineUnselectAction);
            this.Actions.Add(this.SalesInvoiceLineTaxAndDiscountAction);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SimpleAction SalesInvoiceLineSelectAction;
        private DevExpress.ExpressApp.Actions.SimpleAction SalesInvoiceLineUnselectAction;
        private DevExpress.ExpressApp.Actions.SimpleAction SalesInvoiceLineTaxAndDiscountAction;
    }
}
