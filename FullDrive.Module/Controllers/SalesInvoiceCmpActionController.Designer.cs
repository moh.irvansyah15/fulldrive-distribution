﻿namespace FullDrive.Module.Controllers
{
    partial class SalesInvoiceCmpActionController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.SalesInvoiceCmpEditInvoiceAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            // 
            // SalesInvoiceCmpEditInvoiceAction
            // 
            this.SalesInvoiceCmpEditInvoiceAction.Caption = "Edit Invoice";
            this.SalesInvoiceCmpEditInvoiceAction.ConfirmationMessage = null;
            this.SalesInvoiceCmpEditInvoiceAction.Id = "SalesInvoiceCmpEditInvoiceActionId";
            this.SalesInvoiceCmpEditInvoiceAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.SalesInvoiceCmp);
            this.SalesInvoiceCmpEditInvoiceAction.ToolTip = null;
            this.SalesInvoiceCmpEditInvoiceAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.SalesInvoiceCmpEditInvoiceAction_Execute);
            // 
            // SalesInvoiceCmpActionController
            // 
            this.Actions.Add(this.SalesInvoiceCmpEditInvoiceAction);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SimpleAction SalesInvoiceCmpEditInvoiceAction;
    }
}
