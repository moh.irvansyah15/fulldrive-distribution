﻿namespace FullDrive.Module.Controllers
{
    partial class PurchaseRequisitionLineActionController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.PurchaseRequisitionLineSelectAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.PurchaseRequisitionLineUnselectAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.PurchaseRequisitionLineListviewFilterSelectionAction = new DevExpress.ExpressApp.Actions.SingleChoiceAction(this.components);
            // 
            // PurchaseRequisitionLineSelectAction
            // 
            this.PurchaseRequisitionLineSelectAction.Caption = "Select";
            this.PurchaseRequisitionLineSelectAction.ConfirmationMessage = null;
            this.PurchaseRequisitionLineSelectAction.Id = "PurchaseRequisitionLineSelectActionId";
            this.PurchaseRequisitionLineSelectAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.PurchaseRequisitionLine);
            this.PurchaseRequisitionLineSelectAction.ToolTip = null;
            this.PurchaseRequisitionLineSelectAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.PurchaseRequisitionLineSelectAction_Execute);
            // 
            // PurchaseRequisitionLineUnselectAction
            // 
            this.PurchaseRequisitionLineUnselectAction.Caption = "Unselect";
            this.PurchaseRequisitionLineUnselectAction.ConfirmationMessage = null;
            this.PurchaseRequisitionLineUnselectAction.Id = "PurchaseRequisitionLineUnselectActionId";
            this.PurchaseRequisitionLineUnselectAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.PurchaseRequisitionLine);
            this.PurchaseRequisitionLineUnselectAction.ToolTip = null;
            this.PurchaseRequisitionLineUnselectAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.PurchaseRequisitionLineUnselectAction_Execute);
            // 
            // PurchaseRequisitionLineListviewFilterSelectionAction
            // 
            this.PurchaseRequisitionLineListviewFilterSelectionAction.Caption = "Filter";
            this.PurchaseRequisitionLineListviewFilterSelectionAction.ConfirmationMessage = null;
            this.PurchaseRequisitionLineListviewFilterSelectionAction.Id = "PurchaseRequisitionLineListviewFilterSelectionActionId";
            this.PurchaseRequisitionLineListviewFilterSelectionAction.ItemType = DevExpress.ExpressApp.Actions.SingleChoiceActionItemType.ItemIsOperation;
            this.PurchaseRequisitionLineListviewFilterSelectionAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.PurchaseRequisitionLine);
            this.PurchaseRequisitionLineListviewFilterSelectionAction.ToolTip = null;
            this.PurchaseRequisitionLineListviewFilterSelectionAction.Execute += new DevExpress.ExpressApp.Actions.SingleChoiceActionExecuteEventHandler(this.PurchaseRequisitionLineListviewFilterSelectionAction_Execute);
            // 
            // PurchaseRequisitionLineActionController
            // 
            this.Actions.Add(this.PurchaseRequisitionLineSelectAction);
            this.Actions.Add(this.PurchaseRequisitionLineUnselectAction);
            this.Actions.Add(this.PurchaseRequisitionLineListviewFilterSelectionAction);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SimpleAction PurchaseRequisitionLineSelectAction;
        private DevExpress.ExpressApp.Actions.SimpleAction PurchaseRequisitionLineUnselectAction;
        private DevExpress.ExpressApp.Actions.SingleChoiceAction PurchaseRequisitionLineListviewFilterSelectionAction;
    }
}
