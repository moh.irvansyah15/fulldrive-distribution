﻿namespace FullDrive.Module.Controllers
{
    partial class DeliveryOrderLineActionController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.DeliveryOrderLineDeliveryStatusAction = new DevExpress.ExpressApp.Actions.SingleChoiceAction(this.components);
            this.DeliveryOrderLineSelectAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.DeliveryOrderLineUnselectAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            // 
            // DeliveryOrderLineDeliveryStatusAction
            // 
            this.DeliveryOrderLineDeliveryStatusAction.Caption = "Delivery Status";
            this.DeliveryOrderLineDeliveryStatusAction.ConfirmationMessage = null;
            this.DeliveryOrderLineDeliveryStatusAction.Id = "DeliveryOrderLineDeliveryStatusActionId";
            this.DeliveryOrderLineDeliveryStatusAction.ItemType = DevExpress.ExpressApp.Actions.SingleChoiceActionItemType.ItemIsOperation;
            this.DeliveryOrderLineDeliveryStatusAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.DeliveryOrderLine);
            this.DeliveryOrderLineDeliveryStatusAction.ToolTip = null;
            this.DeliveryOrderLineDeliveryStatusAction.Execute += new DevExpress.ExpressApp.Actions.SingleChoiceActionExecuteEventHandler(this.DeliveryOrderLineDeliveryStatusAction_Execute);
            // 
            // DeliveryOrderLineSelectAction
            // 
            this.DeliveryOrderLineSelectAction.Caption = "Select";
            this.DeliveryOrderLineSelectAction.ConfirmationMessage = null;
            this.DeliveryOrderLineSelectAction.Id = "DeliveryOrderLineSelectActionId";
            this.DeliveryOrderLineSelectAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.DeliveryOrderLine);
            this.DeliveryOrderLineSelectAction.ToolTip = null;
            this.DeliveryOrderLineSelectAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.DeliveryOrderLineSelectAction_Execute);
            // 
            // DeliveryOrderLineUnselectAction
            // 
            this.DeliveryOrderLineUnselectAction.Caption = "Unselect";
            this.DeliveryOrderLineUnselectAction.ConfirmationMessage = null;
            this.DeliveryOrderLineUnselectAction.Id = "DeliveryOrderLineUnselectActionId";
            this.DeliveryOrderLineUnselectAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.DeliveryOrderLine);
            this.DeliveryOrderLineUnselectAction.ToolTip = null;
            this.DeliveryOrderLineUnselectAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.DeliveryOrderLineUnselectAction_Execute);
            // 
            // DeliveryOrderLineActionController
            // 
            this.Actions.Add(this.DeliveryOrderLineDeliveryStatusAction);
            this.Actions.Add(this.DeliveryOrderLineSelectAction);
            this.Actions.Add(this.DeliveryOrderLineUnselectAction);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SingleChoiceAction DeliveryOrderLineDeliveryStatusAction;
        private DevExpress.ExpressApp.Actions.SimpleAction DeliveryOrderLineSelectAction;
        private DevExpress.ExpressApp.Actions.SimpleAction DeliveryOrderLineUnselectAction;
    }
}
