﻿namespace FullDrive.Module.Controllers
{
    partial class TransferOrderActionController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.TransferOrderGetStockAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.TransferOrderProgressAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.TransferOrderPostingAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.TransferOrderListviewFilterSelectionAction = new DevExpress.ExpressApp.Actions.SingleChoiceAction(this.components);
            // 
            // TransferOrderGetStockAction
            // 
            this.TransferOrderGetStockAction.Caption = "Get Stock";
            this.TransferOrderGetStockAction.ConfirmationMessage = null;
            this.TransferOrderGetStockAction.Id = "TransferOrderGetStockActionId";
            this.TransferOrderGetStockAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.TransferOrder);
            this.TransferOrderGetStockAction.ToolTip = null;
            this.TransferOrderGetStockAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.TransferOrderGetStockAction_Execute);
            // 
            // TransferOrderProgressAction
            // 
            this.TransferOrderProgressAction.Caption = "Progress";
            this.TransferOrderProgressAction.ConfirmationMessage = null;
            this.TransferOrderProgressAction.Id = "TransferOrderProgressActionId";
            this.TransferOrderProgressAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.TransferOrder);
            this.TransferOrderProgressAction.ToolTip = null;
            this.TransferOrderProgressAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.TransferOrderProgressAction_Execute);
            // 
            // TransferOrderPostingAction
            // 
            this.TransferOrderPostingAction.Caption = "Posting";
            this.TransferOrderPostingAction.ConfirmationMessage = null;
            this.TransferOrderPostingAction.Id = "TransferOrderPostingActionId";
            this.TransferOrderPostingAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.TransferOrder);
            this.TransferOrderPostingAction.ToolTip = null;
            this.TransferOrderPostingAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.TransferOrderPostingAction_Execute);
            // 
            // TransferOrderListviewFilterSelectionAction
            // 
            this.TransferOrderListviewFilterSelectionAction.Caption = "Filter";
            this.TransferOrderListviewFilterSelectionAction.ConfirmationMessage = null;
            this.TransferOrderListviewFilterSelectionAction.Id = "TransferOrderListviewFilterSelectionActionId";
            this.TransferOrderListviewFilterSelectionAction.ItemType = DevExpress.ExpressApp.Actions.SingleChoiceActionItemType.ItemIsOperation;
            this.TransferOrderListviewFilterSelectionAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.TransferOrder);
            this.TransferOrderListviewFilterSelectionAction.ToolTip = null;
            this.TransferOrderListviewFilterSelectionAction.Execute += new DevExpress.ExpressApp.Actions.SingleChoiceActionExecuteEventHandler(this.TransferOrderListviewFilterSelectionAction_Execute);
            // 
            // TransferOrderActionController
            // 
            this.Actions.Add(this.TransferOrderGetStockAction);
            this.Actions.Add(this.TransferOrderProgressAction);
            this.Actions.Add(this.TransferOrderPostingAction);
            this.Actions.Add(this.TransferOrderListviewFilterSelectionAction);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SimpleAction TransferOrderGetStockAction;
        private DevExpress.ExpressApp.Actions.SimpleAction TransferOrderProgressAction;
        private DevExpress.ExpressApp.Actions.SimpleAction TransferOrderPostingAction;
        private DevExpress.ExpressApp.Actions.SingleChoiceAction TransferOrderListviewFilterSelectionAction;
    }
}
