﻿namespace FullDrive.Module.Controllers
{
    partial class PurchasePrePaymentInvoiceActionController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.PurchasePrePaymentInvoiceProgressAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.PurchasePrePaymentInvoicePostingAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.PurchasePrePaymentInvoiceApprovalAction = new DevExpress.ExpressApp.Actions.SingleChoiceAction(this.components);
            this.PurchasePrePaymentInvoiceListviewFilterSelectionAction = new DevExpress.ExpressApp.Actions.SingleChoiceAction(this.components);
            this.PurchasePrePaymentInvoiceListviewFilterApprovalSelectionAction = new DevExpress.ExpressApp.Actions.SingleChoiceAction(this.components);
            this.PurchasePrePaymentInvoiceGetAmountAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            // 
            // PurchasePrePaymentInvoiceProgressAction
            // 
            this.PurchasePrePaymentInvoiceProgressAction.Caption = "Progress";
            this.PurchasePrePaymentInvoiceProgressAction.ConfirmationMessage = null;
            this.PurchasePrePaymentInvoiceProgressAction.Id = "PurchasePrePaymentInvoiceProgressActionId";
            this.PurchasePrePaymentInvoiceProgressAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.PurchasePrePaymentInvoice);
            this.PurchasePrePaymentInvoiceProgressAction.ToolTip = null;
            this.PurchasePrePaymentInvoiceProgressAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.PurchasePrePaymentInvoiceProgressAction_Execute);
            // 
            // PurchasePrePaymentInvoicePostingAction
            // 
            this.PurchasePrePaymentInvoicePostingAction.Caption = "Posting";
            this.PurchasePrePaymentInvoicePostingAction.ConfirmationMessage = null;
            this.PurchasePrePaymentInvoicePostingAction.Id = "PurchasePrePaymentInvoicePostingActionId";
            this.PurchasePrePaymentInvoicePostingAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.PurchasePrePaymentInvoice);
            this.PurchasePrePaymentInvoicePostingAction.ToolTip = null;
            this.PurchasePrePaymentInvoicePostingAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.PurchasePrePaymentInvoicePostingAction_Execute);
            // 
            // PurchasePrePaymentInvoiceApprovalAction
            // 
            this.PurchasePrePaymentInvoiceApprovalAction.Caption = "Approval";
            this.PurchasePrePaymentInvoiceApprovalAction.ConfirmationMessage = null;
            this.PurchasePrePaymentInvoiceApprovalAction.Id = "PurchasePrePaymentInvoiceApprovalActionId";
            this.PurchasePrePaymentInvoiceApprovalAction.ItemType = DevExpress.ExpressApp.Actions.SingleChoiceActionItemType.ItemIsOperation;
            this.PurchasePrePaymentInvoiceApprovalAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.PurchasePrePaymentInvoice);
            this.PurchasePrePaymentInvoiceApprovalAction.ToolTip = null;
            this.PurchasePrePaymentInvoiceApprovalAction.Execute += new DevExpress.ExpressApp.Actions.SingleChoiceActionExecuteEventHandler(this.PurchasePrePaymentInvoiceApprovalAction_Execute);
            // 
            // PurchasePrePaymentInvoiceListviewFilterSelectionAction
            // 
            this.PurchasePrePaymentInvoiceListviewFilterSelectionAction.Caption = "Filter";
            this.PurchasePrePaymentInvoiceListviewFilterSelectionAction.ConfirmationMessage = null;
            this.PurchasePrePaymentInvoiceListviewFilterSelectionAction.Id = "PurchasePrePaymentInvoiceListviewFilterSelectionActionId";
            this.PurchasePrePaymentInvoiceListviewFilterSelectionAction.ItemType = DevExpress.ExpressApp.Actions.SingleChoiceActionItemType.ItemIsOperation;
            this.PurchasePrePaymentInvoiceListviewFilterSelectionAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.PurchasePrePaymentInvoice);
            this.PurchasePrePaymentInvoiceListviewFilterSelectionAction.ToolTip = null;
            this.PurchasePrePaymentInvoiceListviewFilterSelectionAction.Execute += new DevExpress.ExpressApp.Actions.SingleChoiceActionExecuteEventHandler(this.PurchasePrePaymentInvoiceListviewFilterSelectionAction_Execute);
            // 
            // PurchasePrePaymentInvoiceListviewFilterApprovalSelectionAction
            // 
            this.PurchasePrePaymentInvoiceListviewFilterApprovalSelectionAction.Caption = "Filter Approval";
            this.PurchasePrePaymentInvoiceListviewFilterApprovalSelectionAction.ConfirmationMessage = null;
            this.PurchasePrePaymentInvoiceListviewFilterApprovalSelectionAction.Id = "PurchasePrePaymentInvoiceListviewFilterApprovalSelectionActionId";
            this.PurchasePrePaymentInvoiceListviewFilterApprovalSelectionAction.ItemType = DevExpress.ExpressApp.Actions.SingleChoiceActionItemType.ItemIsOperation;
            this.PurchasePrePaymentInvoiceListviewFilterApprovalSelectionAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.PurchasePrePaymentInvoice);
            this.PurchasePrePaymentInvoiceListviewFilterApprovalSelectionAction.ToolTip = null;
            this.PurchasePrePaymentInvoiceListviewFilterApprovalSelectionAction.Execute += new DevExpress.ExpressApp.Actions.SingleChoiceActionExecuteEventHandler(this.PurchasePrePaymentInvoiceListviewFilterApprovalSelectionAction_Execute);
            // 
            // PurchasePrePaymentInvoiceGetAmountAction
            // 
            this.PurchasePrePaymentInvoiceGetAmountAction.Caption = "Get Amount";
            this.PurchasePrePaymentInvoiceGetAmountAction.ConfirmationMessage = null;
            this.PurchasePrePaymentInvoiceGetAmountAction.Id = "PurchasePrePaymentInvoiceGetAmountActionId";
            this.PurchasePrePaymentInvoiceGetAmountAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.PurchasePrePaymentInvoice);
            this.PurchasePrePaymentInvoiceGetAmountAction.ToolTip = null;
            this.PurchasePrePaymentInvoiceGetAmountAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.PurchasePrePaymentInvoiceGetAmountAction_Execute);
            // 
            // PurchasePrePaymentInvoiceActionController
            // 
            this.Actions.Add(this.PurchasePrePaymentInvoiceProgressAction);
            this.Actions.Add(this.PurchasePrePaymentInvoicePostingAction);
            this.Actions.Add(this.PurchasePrePaymentInvoiceApprovalAction);
            this.Actions.Add(this.PurchasePrePaymentInvoiceListviewFilterSelectionAction);
            this.Actions.Add(this.PurchasePrePaymentInvoiceListviewFilterApprovalSelectionAction);
            this.Actions.Add(this.PurchasePrePaymentInvoiceGetAmountAction);

        }

        #endregion
        private DevExpress.ExpressApp.Actions.SimpleAction PurchasePrePaymentInvoiceProgressAction;
        private DevExpress.ExpressApp.Actions.SimpleAction PurchasePrePaymentInvoicePostingAction;
        private DevExpress.ExpressApp.Actions.SingleChoiceAction PurchasePrePaymentInvoiceApprovalAction;
        private DevExpress.ExpressApp.Actions.SingleChoiceAction PurchasePrePaymentInvoiceListviewFilterSelectionAction;
        private DevExpress.ExpressApp.Actions.SingleChoiceAction PurchasePrePaymentInvoiceListviewFilterApprovalSelectionAction;
        private DevExpress.ExpressApp.Actions.SimpleAction PurchasePrePaymentInvoiceGetAmountAction;
    }
}
