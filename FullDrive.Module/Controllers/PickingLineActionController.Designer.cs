﻿namespace FullDrive.Module.Controllers
{
    partial class PickingLineActionController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.PickingLineSelectAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.PickingLineUnselectAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.PickingLineShowSIMAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            // 
            // PickingLineSelectAction
            // 
            this.PickingLineSelectAction.Caption = "Select";
            this.PickingLineSelectAction.ConfirmationMessage = null;
            this.PickingLineSelectAction.Id = "PickingLineSelectActionId";
            this.PickingLineSelectAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.PickingLine);
            this.PickingLineSelectAction.ToolTip = null;
            this.PickingLineSelectAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.PickingLineSelectAction_Execute);
            // 
            // PickingLineUnselectAction
            // 
            this.PickingLineUnselectAction.Caption = "Unselect";
            this.PickingLineUnselectAction.ConfirmationMessage = null;
            this.PickingLineUnselectAction.Id = "PickingLineUnselectActionId";
            this.PickingLineUnselectAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.PickingLine);
            this.PickingLineUnselectAction.ToolTip = null;
            this.PickingLineUnselectAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.PickingLineUnselectAction_Execute);
            // 
            // PickingLineShowSIMAction
            // 
            this.PickingLineShowSIMAction.Caption = "Show SIM";
            this.PickingLineShowSIMAction.ConfirmationMessage = null;
            this.PickingLineShowSIMAction.Id = "PickingLineShowSIMActionId";
            this.PickingLineShowSIMAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.PickingLine);
            this.PickingLineShowSIMAction.ToolTip = null;
            this.PickingLineShowSIMAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.PickingLineShowSIMAction_Execute);
            // 
            // PickingLineActionController
            // 
            this.Actions.Add(this.PickingLineSelectAction);
            this.Actions.Add(this.PickingLineUnselectAction);
            this.Actions.Add(this.PickingLineShowSIMAction);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SimpleAction PickingLineSelectAction;
        private DevExpress.ExpressApp.Actions.SimpleAction PickingLineUnselectAction;
        private DevExpress.ExpressApp.Actions.SimpleAction PickingLineShowSIMAction;
    }
}
