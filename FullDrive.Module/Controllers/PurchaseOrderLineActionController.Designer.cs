﻿namespace FullDrive.Module.Controllers
{
    partial class PurchaseOrderLineActionController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.PurchaseOrderLineSelectAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.PurchaseOrderLineUnselectAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.PurchaseOrderLineListviewFilterSelectionAction = new DevExpress.ExpressApp.Actions.SingleChoiceAction(this.components);
            this.PurchaseOrderLineTaxAndTotalAmountAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            // 
            // PurchaseOrderLineSelectAction
            // 
            this.PurchaseOrderLineSelectAction.Caption = "Select";
            this.PurchaseOrderLineSelectAction.ConfirmationMessage = null;
            this.PurchaseOrderLineSelectAction.Id = "PurchaseOrderLineSelectActionId";
            this.PurchaseOrderLineSelectAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.PurchaseOrderLine);
            this.PurchaseOrderLineSelectAction.ToolTip = null;
            this.PurchaseOrderLineSelectAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.PurchaseOrderLineSelectAction_Execute);
            // 
            // PurchaseOrderLineUnselectAction
            // 
            this.PurchaseOrderLineUnselectAction.Caption = "Unselect";
            this.PurchaseOrderLineUnselectAction.ConfirmationMessage = null;
            this.PurchaseOrderLineUnselectAction.Id = "PurchaseOrderLineUnselectActionId";
            this.PurchaseOrderLineUnselectAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.PurchaseOrderLine);
            this.PurchaseOrderLineUnselectAction.ToolTip = null;
            this.PurchaseOrderLineUnselectAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.PurchaseOrderLineUnselectAction_Execute);
            // 
            // PurchaseOrderLineListviewFilterSelectionAction
            // 
            this.PurchaseOrderLineListviewFilterSelectionAction.Caption = "Filter";
            this.PurchaseOrderLineListviewFilterSelectionAction.ConfirmationMessage = null;
            this.PurchaseOrderLineListviewFilterSelectionAction.Id = "PurchaseOrderLineListviewFilterSelectionActionId";
            this.PurchaseOrderLineListviewFilterSelectionAction.ItemType = DevExpress.ExpressApp.Actions.SingleChoiceActionItemType.ItemIsOperation;
            this.PurchaseOrderLineListviewFilterSelectionAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.PurchaseOrderLine);
            this.PurchaseOrderLineListviewFilterSelectionAction.ToolTip = null;
            this.PurchaseOrderLineListviewFilterSelectionAction.Execute += new DevExpress.ExpressApp.Actions.SingleChoiceActionExecuteEventHandler(this.PurchaseOrderLineListviewFilterSelectionAction_Execute);
            // 
            // PurchaseOrderLineTaxAndTotalAmountAction
            // 
            this.PurchaseOrderLineTaxAndTotalAmountAction.Caption = "Tax And Total Amount";
            this.PurchaseOrderLineTaxAndTotalAmountAction.ConfirmationMessage = null;
            this.PurchaseOrderLineTaxAndTotalAmountAction.Id = "PurchaseOrderLineTaxAndTotalAmountActionId";
            this.PurchaseOrderLineTaxAndTotalAmountAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.PurchaseOrderLine);
            this.PurchaseOrderLineTaxAndTotalAmountAction.ToolTip = null;
            this.PurchaseOrderLineTaxAndTotalAmountAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.PurchaseOrderLineTaxAndTotalAmountAction_Execute);
            // 
            // PurchaseOrderLineActionController
            // 
            this.Actions.Add(this.PurchaseOrderLineSelectAction);
            this.Actions.Add(this.PurchaseOrderLineUnselectAction);
            this.Actions.Add(this.PurchaseOrderLineListviewFilterSelectionAction);
            this.Actions.Add(this.PurchaseOrderLineTaxAndTotalAmountAction);

        }

        #endregion
        private DevExpress.ExpressApp.Actions.SimpleAction PurchaseOrderLineSelectAction;
        private DevExpress.ExpressApp.Actions.SimpleAction PurchaseOrderLineUnselectAction;
        private DevExpress.ExpressApp.Actions.SingleChoiceAction PurchaseOrderLineListviewFilterSelectionAction;
        private DevExpress.ExpressApp.Actions.SimpleAction PurchaseOrderLineTaxAndTotalAmountAction;
    }
}
