﻿namespace FullDrive.Module.Controllers
{
    partial class ReceivableTransaction2ActionController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.ReceivableTransaction2ListviewFilterSelectionAction = new DevExpress.ExpressApp.Actions.SingleChoiceAction(this.components);
            this.ReceivableTransaction2GetOCMAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.ReceivableTransaction2ProgressAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.ReceivableTransaction2PostingAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            // 
            // ReceivableTransaction2ListviewFilterSelectionAction
            // 
            this.ReceivableTransaction2ListviewFilterSelectionAction.Caption = "Filter";
            this.ReceivableTransaction2ListviewFilterSelectionAction.ConfirmationMessage = null;
            this.ReceivableTransaction2ListviewFilterSelectionAction.Id = "ReceivableTransaction2ListviewFilterSelectionActionId";
            this.ReceivableTransaction2ListviewFilterSelectionAction.ItemType = DevExpress.ExpressApp.Actions.SingleChoiceActionItemType.ItemIsOperation;
            this.ReceivableTransaction2ListviewFilterSelectionAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.ReceivableTransaction2);
            this.ReceivableTransaction2ListviewFilterSelectionAction.ToolTip = null;
            this.ReceivableTransaction2ListviewFilterSelectionAction.Execute += new DevExpress.ExpressApp.Actions.SingleChoiceActionExecuteEventHandler(this.ReceivableTransaction2ListviewFilterSelectionAction_Execute);
            // 
            // ReceivableTransaction2GetOCMAction
            // 
            this.ReceivableTransaction2GetOCMAction.Caption = "Get OCM";
            this.ReceivableTransaction2GetOCMAction.ConfirmationMessage = null;
            this.ReceivableTransaction2GetOCMAction.Id = "ReceivableTransaction2GetOCMActionId";
            this.ReceivableTransaction2GetOCMAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.ReceivableTransaction2);
            this.ReceivableTransaction2GetOCMAction.ToolTip = null;
            this.ReceivableTransaction2GetOCMAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.ReceivableTransaction2GetOCMAction_Execute);
            // 
            // ReceivableTransaction2ProgressAction
            // 
            this.ReceivableTransaction2ProgressAction.Caption = "Progress";
            this.ReceivableTransaction2ProgressAction.ConfirmationMessage = null;
            this.ReceivableTransaction2ProgressAction.Id = "ReceivableTransaction2ProgressActionId";
            this.ReceivableTransaction2ProgressAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.ReceivableTransaction2);
            this.ReceivableTransaction2ProgressAction.ToolTip = null;
            this.ReceivableTransaction2ProgressAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.ReceivableTransaction2ProgressAction_Execute);
            // 
            // ReceivableTransaction2PostingAction
            // 
            this.ReceivableTransaction2PostingAction.Caption = "Posting";
            this.ReceivableTransaction2PostingAction.ConfirmationMessage = null;
            this.ReceivableTransaction2PostingAction.Id = "ReceivableTransaction2PostingActionId";
            this.ReceivableTransaction2PostingAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.ReceivableTransaction2);
            this.ReceivableTransaction2PostingAction.ToolTip = null;
            this.ReceivableTransaction2PostingAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.ReceivableTransaction2PostingAction_Execute);
            // 
            // ReceivableTransaction2ActionController
            // 
            this.Actions.Add(this.ReceivableTransaction2ListviewFilterSelectionAction);
            this.Actions.Add(this.ReceivableTransaction2GetOCMAction);
            this.Actions.Add(this.ReceivableTransaction2ProgressAction);
            this.Actions.Add(this.ReceivableTransaction2PostingAction);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SingleChoiceAction ReceivableTransaction2ListviewFilterSelectionAction;
        private DevExpress.ExpressApp.Actions.SimpleAction ReceivableTransaction2GetOCMAction;
        private DevExpress.ExpressApp.Actions.SimpleAction ReceivableTransaction2ProgressAction;
        private DevExpress.ExpressApp.Actions.SimpleAction ReceivableTransaction2PostingAction;
    }
}
