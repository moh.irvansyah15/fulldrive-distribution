﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;

using FullDrive.Module.BusinessObjects;
using DevExpress.ExpressApp.Xpo;
using DevExpress.Xpo;

namespace FullDrive.Module.Controllers
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class RouteListViewFilterController : ViewController
    {
        public RouteListViewFilterController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.
            TargetViewId = "Route_ListView";
        }
        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
            var User = SecuritySystem.CurrentUserName;
            string _beginString1 = null;
            string _endString1 = null;
            string _fullString1 = null;
            string _beginString2 = null;
            string _endString2 = null;
            string _fullString2 = null;
            string _beginString3 = null;
            string _endString3 = null;
            string _fullString3 = null;
            string _fullStringSalesman = null;
            string _fullString = null;
            var ListView = View as ListView;
            Session currentSession = null;
            if (this.ObjectSpace != null)
            {
                UserAccess _locUserAccess = this.ObjectSpace.FindObject<UserAccess>(new BinaryOperator("UserName", User));

                if (_locUserAccess.Session != null)
                {
                    currentSession = _locUserAccess.Session;
                }

                if (_locUserAccess != null && currentSession != null)
                {
                    if (_locUserAccess.Employee != null)
                    {
                        string _stringSalesman = null;
                        if (_locUserAccess.Employee.Code != null)
                        {
                            _stringSalesman = _locUserAccess.Employee.Code;
                        }
                        List<string> _stringCompany = new List<string>();
                        List<string> _stringWorkplace = new List<string>();
                        List<string> _stringVehicle = new List<string>();


                        XPCollection<SalesmanVehicleSetup> _locSalesmanVehicleSetups = new XPCollection<SalesmanVehicleSetup>
                                                                                  (currentSession, new GroupOperator(GroupOperatorType.And,
                                                                                   new BinaryOperator("UserAccess", _locUserAccess),
                                                                                   new BinaryOperator("Salesman", _locUserAccess.Employee),
                                                                                   new BinaryOperator("Active", true)));

                        if (_locSalesmanVehicleSetups != null && _locSalesmanVehicleSetups.Count > 0)
                        {
                            foreach (SalesmanVehicleSetup _locSalesmanVehicleSetup in _locSalesmanVehicleSetups)
                            {
                                if (_locSalesmanVehicleSetup.Company != null)
                                {
                                    _stringCompany.Add(_locSalesmanVehicleSetup.Company.Code);
                                }

                                if (_locSalesmanVehicleSetup.Workplace != null)
                                {
                                    _stringWorkplace.Add(_locSalesmanVehicleSetup.Workplace.Code);
                                }

                                if (_locSalesmanVehicleSetup.Vehicle != null)
                                {
                                    _stringVehicle.Add(_locSalesmanVehicleSetup.Vehicle.Code);
                                }
                            }
                        }

                        #region Company
                        IEnumerable<string> _stringArrayCompanyDistinct = _stringCompany.Distinct();
                        string[] _stringArrayCompanyList = _stringArrayCompanyDistinct.ToArray();
                        if (_stringArrayCompanyList.Length == 1)
                        {
                            for (int i = 0; i < _stringArrayCompanyList.Length; i++)
                            {
                                Company _locCompany = currentSession.FindObject<Company>(new BinaryOperator("Code", _stringArrayCompanyList[i]));
                                if (_locCompany != null)
                                {
                                    if (i == 0)
                                    {
                                        _beginString1 = "[Company.Code]=='" + _locCompany.Code + "'";
                                    }
                                }
                            }
                        }
                        else if (_stringArrayCompanyList.Length > 1)
                        {
                            for (int i = 0; i < _stringArrayCompanyList.Length; i++)
                            {
                                Company _locCompany = currentSession.FindObject<Company>(new BinaryOperator("Code", _stringArrayCompanyList[i]));
                                if (_locCompany != null)
                                {
                                    if (i == 0)
                                    {
                                        _beginString1 = "[Company.Code]=='" + _locCompany.Code + "'";
                                    }
                                    else
                                    {
                                        _endString1 = _endString1 + " OR [Company.Code]=='" + _locCompany.Code + "'";
                                    }
                                }
                            }
                        }
                        _fullString1 = _beginString1 + _endString1;

                        #endregion Company

                        #region Workplace
                        IEnumerable<string> _stringArrayWorkplaceDistinct = _stringWorkplace.Distinct();
                        string[] _stringArrayWorkplaceList = _stringArrayWorkplaceDistinct.ToArray();
                        if (_stringArrayWorkplaceList.Length == 1)
                        {
                            for (int i = 0; i < _stringArrayWorkplaceList.Length; i++)
                            {
                                Workplace _locDivision = currentSession.FindObject<Workplace>(new BinaryOperator("Code", _stringArrayWorkplaceList[i]));
                                if (_locDivision != null)
                                {
                                    if (i == 0)
                                    {
                                        _beginString2 = "[Workplace.Code]=='" + _locDivision.Code + "'";
                                    }
                                }
                            }
                        }
                        else if (_stringArrayWorkplaceList.Length > 1)
                        {
                            for (int i = 0; i < _stringArrayWorkplaceList.Length; i++)
                            {
                                Workplace _locWorkplace = currentSession.FindObject<Workplace>(new BinaryOperator("Code", _stringArrayWorkplaceList[i]));
                                if (_locWorkplace != null)
                                {
                                    if (i == 0)
                                    {
                                        _beginString2 = "[Workplace.Code]=='" + _locWorkplace.Code + "'";
                                    }
                                    else
                                    {
                                        _endString2 = _endString2 + " OR [Workplace.Code]=='" + _locWorkplace.Code + "'";
                                    }
                                }
                            }
                        }
                        _fullString2 = _beginString2 + _endString2;

                        #endregion Workplace

                        #region Vehicle
                        IEnumerable<string> _stringArrayVehicleDistinct = _stringVehicle.Distinct();
                        string[] _stringArrayVehicleList = _stringArrayVehicleDistinct.ToArray();
                        if (_stringArrayVehicleList.Length == 1)
                        {
                            for (int i = 0; i < _stringArrayVehicleList.Length; i++)
                            {
                                Vehicle _locVehicle = currentSession.FindObject<Vehicle>(new BinaryOperator("Code", _stringArrayVehicleList[i]));
                                if (_locVehicle != null)
                                {
                                    if (i == 0)
                                    {
                                        _beginString3 = "[Vehicle.Code]=='" + _locVehicle.Code + "'";
                                    }
                                }
                            }
                        }
                        else if (_stringArrayVehicleList.Length > 1)
                        {
                            for (int i = 0; i < _stringArrayVehicleList.Length; i++)
                            {
                                Vehicle _locVehicle = currentSession.FindObject<Vehicle>(new BinaryOperator("Code", _stringArrayVehicleList[i]));
                                if (_locVehicle != null)
                                {
                                    if (i == 0)
                                    {
                                        _beginString3 = "[Vehicle.Code]=='" + _locVehicle.Code + "'";
                                    }
                                }
                                else
                                {
                                    _endString3 = _endString3 + " OR [Vehicle.Code]=='" + _locVehicle.Code + "'";
                                }
                            }
                        }
                        _fullString3 = _beginString3 + _endString3;

                        #endregion Vehicle

                        #region Salesman

                        _fullStringSalesman = "[Salesman.Code]=='" + _stringSalesman + "'";

                        #endregion Salesman

                        if (_stringArrayCompanyList.Length == 1 && _stringArrayWorkplaceList.Length == 1 && _stringArrayVehicleList.Length == 1 && _stringSalesman != null)
                        {
                            _fullString = _fullString1 + " AND " + _fullString2 + " AND " + _fullString3 + " AND " + _fullStringSalesman;
                        }
                        else if (_stringArrayCompanyList.Length == 1 && _stringArrayWorkplaceList.Length == 1 && _stringArrayVehicleList.Length == 1 && _stringSalesman == null)
                        {
                            _fullString = _fullString1 + " AND " + _fullString2 + " AND " + _fullString3;
                        }
                        else if (_stringArrayCompanyList.Length == 1 && _stringArrayWorkplaceList.Length == 1 && _stringArrayVehicleList.Length == 0 && _stringSalesman == null)
                        {
                            _fullString = _fullString1 + " AND " + _fullString2;
                        }
                        else if (_stringArrayCompanyList.Length == 1 && _stringArrayWorkplaceList.Length == 0 && _stringArrayVehicleList.Length == 0 && _stringSalesman == null)
                        {
                            _fullString = _fullString1;
                        }
                        else if (_stringArrayCompanyList.Length > 1 && _stringArrayWorkplaceList.Length > 1 && _stringArrayVehicleList.Length > 1 && _stringSalesman != null)
                        {
                            _fullString = _fullString1 + " OR " + _fullString2 + " OR " + _fullString3 + " OR " + _fullStringSalesman;
                        }
                        else if (_stringArrayCompanyList.Length > 1 && _stringArrayWorkplaceList.Length > 1 && _stringArrayVehicleList.Length > 1 && _stringSalesman == null)
                        {
                            _fullString = _fullString1 + " OR " + _fullString2 + " OR " + _fullString3;
                        }
                        else if (_stringArrayCompanyList.Length > 1 && _stringArrayWorkplaceList.Length > 1 && _stringArrayVehicleList.Length == 0 && _stringSalesman == null)
                        {
                            _fullString = _fullString1 + " OR " + _fullString2;
                        }
                        else if (_stringArrayCompanyList.Length > 1 && _stringArrayWorkplaceList.Length == 0 && _stringArrayVehicleList.Length == 0 && _stringSalesman == null)
                        {
                            _fullString = _fullString1;
                        }
                        else if (_stringArrayCompanyList.Length == 1 && _stringArrayWorkplaceList.Length > 1 && _stringArrayVehicleList.Length > 1 && _stringSalesman != null)
                        {
                            _fullString = _fullString1 + " OR " + _fullString2 + " OR " + _fullString3 + " OR " + _fullStringSalesman;
                        }
                        else if (_stringArrayCompanyList.Length == 1 && _stringArrayWorkplaceList.Length == 1 && _stringArrayVehicleList.Length > 1 && _stringSalesman != null)
                        {
                            _fullString = _fullString1 + " OR " + _fullString2 + " OR " + _fullString3 + " OR " + _fullStringSalesman;
                        }
                        else if (_stringArrayCompanyList.Length == 1 && _stringArrayWorkplaceList.Length == 1 && _stringArrayVehicleList.Length == 1 && _stringSalesman != null)
                        {
                            _fullString = _fullString1 + " OR " + _fullString2 + " OR " + _fullString3 + " OR " + _fullStringSalesman;
                        }
                        else if (_stringArrayCompanyList.Length == 1 && _stringArrayWorkplaceList.Length > 1 && _stringArrayVehicleList.Length > 1 && _stringSalesman == null)
                        {
                            _fullString = _fullString1 + " OR " + _fullString2 + " OR " + _fullString3;
                        }
                        else if (_stringArrayCompanyList.Length == 1 && _stringArrayWorkplaceList.Length > 1 && _stringArrayVehicleList.Length == 0 && _stringSalesman == null)
                        {
                            _fullString = _fullString1 + " OR " + _fullString2;
                        }
                        else if (_stringArrayCompanyList.Length == 0 && _stringArrayWorkplaceList.Length == 1 && _stringArrayVehicleList.Length == 1 && _stringSalesman != null)
                        {
                            _fullString = _fullString2 + " AND " + _fullString3 + " AND " + _fullStringSalesman;
                        }
                        else if (_stringArrayCompanyList.Length == 0 && _stringArrayWorkplaceList.Length == 1 && _stringArrayVehicleList.Length == 1 && _stringSalesman == null)
                        {
                            _fullString = _fullString2 + " AND " + _fullString3;
                        }
                        else if (_stringArrayCompanyList.Length == 0 && _stringArrayWorkplaceList.Length == 1 && _stringArrayVehicleList.Length == 0 && _stringSalesman == null)
                        {
                            _fullString = _fullString2;
                        }
                        else if (_stringArrayCompanyList.Length == 0 && _stringArrayWorkplaceList.Length > 1 && _stringArrayVehicleList.Length > 1 && _stringSalesman != null)
                        {
                            _fullString = _fullString2 + " OR " + _fullString3 + " OR " + _fullStringSalesman;
                        }
                        else if (_stringArrayCompanyList.Length == 0 && _stringArrayWorkplaceList.Length > 1 && _stringArrayVehicleList.Length > 1 && _stringSalesman != null)
                        {
                            _fullString = _fullString2 + " OR " + _fullString3 + " OR " + _fullStringSalesman;
                        }
                        else if (_stringArrayCompanyList.Length == 0 && _stringArrayWorkplaceList.Length > 1 && _stringArrayVehicleList.Length > 1 && _stringSalesman == null)
                        {
                            _fullString = _fullString2 + " OR " + _fullString3;
                        }
                        else if (_stringArrayCompanyList.Length == 0 && _stringArrayWorkplaceList.Length > 1 && _stringArrayVehicleList.Length == 0 && _stringSalesman == null)
                        {
                            _fullString = _fullString2;
                        }

                        ListView.CollectionSource.Criteria["RouteFilter"] = CriteriaOperator.Parse(_fullString);
                    }
                }
            }
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }
        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }
    }
}
