﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;

using FullDrive.Module.BusinessObjects;
using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.Xpo;
using System.Collections;
using DevExpress.Xpo;
using System.Web;
using System.IO;

namespace FullDrive.Module.Controllers
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class CashAdvanceActionController : ViewController
    {
        private ChoiceActionItem _selectionListviewFilter;
        private ChoiceActionItem _selectionApprovalListviewFilter;
        private ChoiceActionItem _setApprovalLevel;

        public CashAdvanceActionController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.
            #region FilterStatus
            CashAdvanceListviewFilterSelectionAction.Items.Clear();
            foreach (object _currApproval in Enum.GetValues(typeof(CustomProcess.Status)))
            {
                EnumDescriptor _ed = new EnumDescriptor(typeof(CustomProcess.Status));
                _selectionListviewFilter = new ChoiceActionItem(_ed.GetCaption(_currApproval), _currApproval);
                CashAdvanceListviewFilterSelectionAction.Items.Add(_selectionListviewFilter);
            }
            #endregion FilterStatus

            #region FilterApproval
            CashAdvanceListviewFilterApprovalSelectionAction.Items.Clear();
            foreach (object _currApprovalFilter in Enum.GetValues(typeof(CustomProcess.ApprovalFilter)))
            {
                EnumDescriptor _ed = new EnumDescriptor(typeof(CustomProcess.ApprovalFilter));
                _selectionApprovalListviewFilter = new ChoiceActionItem(_ed.GetCaption(_currApprovalFilter), _currApprovalFilter);
                CashAdvanceListviewFilterApprovalSelectionAction.Items.Add(_selectionApprovalListviewFilter);
            }
            #endregion FilterApproval

        }

        protected override void OnActivated()
        {
            TypeOfView = typeof(ObjectView);
            base.OnActivated();
            // Perform various tasks depending on the target View.
            #region Approval
            var _locUser = SecuritySystem.CurrentUserName;
            
            if (this.ObjectSpace != null)
            {
                UserAccess _locUserAccess = this.ObjectSpace.FindObject<UserAccess>(new BinaryOperator("UserName", _locUser));

                if (_locUserAccess != null)
                {
                    CashAdvanceApprovalAction.Items.Clear();

                    XPCollection<ApplicationSetupDetail> _locAppSetupDetails = new XPCollection<ApplicationSetupDetail>
                                                                                 (_locUserAccess.Session, new GroupOperator(GroupOperatorType.And,
                                                                                  new BinaryOperator("UserAccess", _locUserAccess),
                                                                                  new BinaryOperator("ObjectList", CustomProcess.ObjectList.CashAdvance),
                                                                                  new BinaryOperator("FunctionList", CustomProcess.FunctionList.Approval),
                                                                                  new BinaryOperator("Active", true)));

                    if (_locAppSetupDetails != null && _locAppSetupDetails.Count > 0)
                    {

                        foreach (ApplicationSetupDetail _locAppSetupDetail in _locAppSetupDetails)
                        {
                            foreach (object _currApproval in Enum.GetValues(typeof(CustomProcess.ApprovalLevel)))
                            {
                                if (_locAppSetupDetail.ApprovalLevel != CustomProcess.ApprovalLevel.None)
                                {
                                    EnumDescriptor _ed = new EnumDescriptor(typeof(CustomProcess.ApprovalLevel));
                                    _setApprovalLevel = new ChoiceActionItem(_ed.GetCaption(_locAppSetupDetail.ApprovalLevel), _locAppSetupDetail.ApprovalLevel);
                                }
                            }
                            CashAdvanceApprovalAction.Items.Add(_setApprovalLevel);
                        }
                    }
                }
            }
            #endregion Approval

        }

        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }

        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        private void CashAdvanceProgressActionController_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;
                Status _locStatus = Status.None;

                if (_objectToProcess != null)
                {
                    foreach (Object obj in _objectToProcess)
                    {
                        CashAdvance _locCashAdvanceOS = (CashAdvance)_objectSpace.GetObject(obj);

                        if(_locCashAdvanceOS.Session != null)
                        {
                            _currSession = _locCashAdvanceOS.Session;
                        }

                        if (_locCashAdvanceOS != null && _currSession != null)
                        {
                            if (_locCashAdvanceOS.Code != null)
                            {
                                _currObjectId = _locCashAdvanceOS.Code;

                                CashAdvance _locCashAdvanceXPO = _currSession.FindObject<CashAdvance>
                                                                  (new GroupOperator(GroupOperatorType.And,
                                                                  new BinaryOperator("Code", _currObjectId),
                                                                  new GroupOperator(GroupOperatorType.Or,
                                                                  new BinaryOperator("Status", Status.Open),
                                                                  new BinaryOperator("Status", Status.Progress))));

                                if (_locCashAdvanceXPO != null)
                                {
                                    XPCollection<CashAdvanceLine> _locCashAdvanceLines = new XPCollection<CashAdvanceLine>
                                                                                             (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                              new BinaryOperator("CashAdvance", _locCashAdvanceXPO),
                                                                                              new BinaryOperator("Status", Status.Open)));

                                    if (_locCashAdvanceLines != null && _locCashAdvanceLines.Count > 0)
                                    {
                                        foreach (CashAdvanceLine _locCashAdvanceLine in _locCashAdvanceLines)
                                        {
                                            _locCashAdvanceLine.Status = Status.Progress;
                                            _locCashAdvanceLine.StatusDate = now;
                                            _locCashAdvanceLine.Save();
                                            _locCashAdvanceLine.Session.CommitTransaction();
                                        }
                                    }

                                    if (_locCashAdvanceXPO.Status == Status.Open)
                                    {
                                        _locStatus = Status.Progress;
                                    }

                                    _locCashAdvanceXPO.Status = _locStatus;
                                    _locCashAdvanceXPO.TotAmount = GetTotAmount(_currSession, _locCashAdvanceXPO);
                                    _locCashAdvanceXPO.TotAmountApproved = GetTotAmountApproved(_currSession, _locCashAdvanceXPO);
                                    _locCashAdvanceXPO.StatusDate = now;
                                    _locCashAdvanceXPO.Save();
                                    _locCashAdvanceXPO.Session.CommitTransaction();
                                    
                                    
                                    SuccessMessageShow("Cash Advance has successfully updated to progress");
                                }
                                else
                                {
                                    ErrorMessageShow("Cash Advance Not Available");
                                }
                            }
                            else
                            {
                                ErrorMessageShow("Cash Advance Not Available");
                            }
                        }
                    }
                }
                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("BusinessObject = CashAdvance" + ex.ToString());
            }
        }

        private void CashAdvancePostingAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;

                if (_objectsToProcess != null)
                {
                    foreach (Object obj in _objectsToProcess)
                    {
                        CashAdvance _locCashAdvanceOS = (CashAdvance)_objectSpace.GetObject(obj);

                        if(_locCashAdvanceOS.Session != null)
                        {
                            _currSession = _locCashAdvanceOS.Session;
                        }

                        if (_locCashAdvanceOS != null && _currSession != null)
                        {
                            if (_locCashAdvanceOS.Code != null)
                            {
                                _currObjectId = _locCashAdvanceOS.Code;

                                CashAdvance _locCashAdvanceXPO = _currSession.FindObject<CashAdvance>
                                                                 (new GroupOperator(GroupOperatorType.And,
                                                                  new BinaryOperator("Code", _currObjectId)));

                                if (_locCashAdvanceXPO != null)
                                {
                                    if (_locCashAdvanceXPO.Status == Status.Progress)
                                    {
                                        ApprovalLine _locApprovalLineXPO = _currSession.FindObject<ApprovalLine>
                                                                           (new GroupOperator(GroupOperatorType.And,
                                                                            new BinaryOperator("EndApproval", true),
                                                                            new BinaryOperator("CashAdvance", _locCashAdvanceXPO)));

                                        if (_locApprovalLineXPO != null)
                                        {
                                            if (GetMaxAmountApprovedCashAdvanceLine(_currSession, _locCashAdvanceXPO) <= _locCashAdvanceXPO.TotAmountApproved)
                                            {
                                                SetCashAdvanceMonitoring(_currSession, _locCashAdvanceXPO);
                                                if (CheckJournalSetup(_currSession, _locCashAdvanceXPO) == true)
                                                {
                                                    SetJournalCashBon(_currSession, _locCashAdvanceXPO);
                                                }    
                                                SetStatusCashAdvanceLine(_currSession, _locCashAdvanceXPO);
                                                SetNormal(_currSession, _locCashAdvanceXPO);
                                                SetStatusCashAdvance(_currSession, _locCashAdvanceXPO);
                                                SuccessMessageShow(_locCashAdvanceXPO.Code + " has been change successfully to Posted");
                                            }
                                        }
                                        else
                                        {
                                            ErrorMessageShow("Data Cash Advance Not Available");
                                        }
                                    }
                                    else
                                    {
                                        ErrorMessageShow("Data Cash Advance Not Available");
                                    }
                                }
                                else
                                {
                                    ErrorMessageShow("Data Cash Advance Not Available");
                                }
                            }
                            else
                            {
                                ErrorMessageShow("Data Cash Advance Not Available");
                            }
                        }
                        else
                        {
                            ErrorMessageShow("Data Cash Advance Not Available");
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = CashAdvance " + ex.ToString());
            }
        }

        private void CashAdvanceApprovalAction_Execute(object sender, SingleChoiceActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                ApplicationSetupDetail _locAppSetDetail = null;
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                var User = SecuritySystem.CurrentUserName;
                Session _currentSession = null;
                string _currObjectId = null;

                foreach (Object obj in _objectsToProcess)
                {
                    CashAdvance _objInNewObjectSpace = (CashAdvance)_objectSpace.GetObject(obj);

                    if (_objInNewObjectSpace != null)
                    {
                        if (_objInNewObjectSpace.Code != null)
                        {
                            _currObjectId = _objInNewObjectSpace.Code;
                        }
                        if(_objInNewObjectSpace.Session != null)
                        {
                            _currentSession = _objInNewObjectSpace.Session;
                        }
                    }

                    if (_currObjectId != null && _currentSession != null)
                    {
                        UserAccess _locUserAccess = _currentSession.FindObject<UserAccess>(new BinaryOperator("UserName", User));

                        CashAdvance _locCashAdvanceXPO = _currentSession.FindObject<CashAdvance>
                                                                        (new GroupOperator(GroupOperatorType.And,
                                                                         new BinaryOperator("Code", _currObjectId)));

                        if (_locCashAdvanceXPO != null)
                        {
                            if (_locCashAdvanceXPO.Status == Status.Progress)
                            {
                                ApprovalLine _locApprovalLine = _currentSession.FindObject<ApprovalLine>
                                                                            (new GroupOperator(GroupOperatorType.And,
                                                                             new BinaryOperator("CashAdvance", _locCashAdvanceXPO),
                                                                             new BinaryOperator("EndApproval", true)));
                                if (_locApprovalLine == null)
                                {
                                    #region Approval Level 1
                                    if ((ApprovalLevel)e.SelectedChoiceActionItem.Data == ApprovalLevel.Level1)
                                    {
                                        _locAppSetDetail = _globFunc.GetApplicationSetupDetailByApprovalLevel(_currentSession, _locUserAccess, ApprovalLevel.Level1, ObjectList.CashAdvance);
                                        if (_locAppSetDetail != null)
                                        {
                                            //Buat bs input langsung ke approvalline
                                            ApprovalLine _locApprovalLineXPO = _currentSession.FindObject<ApprovalLine>
                                                                                (new GroupOperator(GroupOperatorType.And,
                                                                                 new BinaryOperator("CashAdvance", _locCashAdvanceXPO),
                                                                                 new BinaryOperator("ApprovalLevel", ApprovalLevel.Level1)));
                                            if (_locApprovalLineXPO == null)
                                            {

                                                if (_locAppSetDetail.EndApproval == true)
                                                {
                                                    ApprovalLine _saveDataAL = new ApprovalLine(_currentSession)
                                                    {
                                                        ApprovalDate = now,
                                                        ApprovalStatus = Status.Approved,
                                                        ApprovalLevel = ApprovalLevel.Level1,
                                                        EndApproval = true,
                                                        CashAdvance = _locCashAdvanceXPO,
                                                    };
                                                    _saveDataAL.Save();
                                                    _saveDataAL.Session.CommitTransaction();

                                                    _locCashAdvanceXPO.ActivationPosting = true;
                                                    _locCashAdvanceXPO.ActiveApproved1 = false;
                                                    _locCashAdvanceXPO.ActiveApproved2 = false;
                                                    _locCashAdvanceXPO.ActiveApproved3 = true;
                                                    _locCashAdvanceXPO.Save();
                                                    _locCashAdvanceXPO.Session.CommitTransaction();
                                                }
                                                else
                                                {
                                                    ApprovalLine _saveDataAL = new ApprovalLine(_currentSession)
                                                    {
                                                        ApprovalDate = now,
                                                        ApprovalStatus = Status.Approved,
                                                        ApprovalLevel = ApprovalLevel.Level1,
                                                        CashAdvance = _locCashAdvanceXPO,
                                                    };
                                                    _saveDataAL.Save();
                                                    _saveDataAL.Session.CommitTransaction();

                                                    _locCashAdvanceXPO.ActiveApproved1 = true;
                                                    _locCashAdvanceXPO.ActiveApproved2 = false;
                                                    _locCashAdvanceXPO.ActiveApproved3 = false;
                                                    _locCashAdvanceXPO.Save();
                                                    _locCashAdvanceXPO.Session.CommitTransaction();
                                                }

                                                //Send Email
                                                ApprovalLine _locApprovalLineXPO2 = _currentSession.FindObject<ApprovalLine>
                                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                                     new BinaryOperator("CashAdvance", _locCashAdvanceXPO)));

                                                if (_locApprovalLineXPO2 != null)
                                                {
                                                    SendEmail(_currentSession, _locCashAdvanceXPO, Status.Progress, _locApprovalLineXPO2);
                                                }

                                                SuccessMessageShow("CashAdvance has successfully Approve");
                                            }
                                        }
                                    }
                                    #endregion Approval Level 1

                                    #region Approval Level 2
                                    if ((ApprovalLevel)e.SelectedChoiceActionItem.Data == ApprovalLevel.Level2)
                                    {
                                        _locAppSetDetail = _globFunc.GetApplicationSetupDetailByApprovalLevel(_currentSession, _locUserAccess, ApprovalLevel.Level2, ObjectList.CashAdvance);

                                        if (_locAppSetDetail != null)
                                        {
                                            ApprovalLine _locApprovalLineXPO = _currentSession.FindObject<ApprovalLine>
                                                                                (new GroupOperator(GroupOperatorType.And,
                                                                                 new BinaryOperator("CashAdvance", _locCashAdvanceXPO),
                                                                                 new BinaryOperator("ApprovalLevel", ApprovalLevel.Level2)));

                                            if (_locApprovalLineXPO == null)
                                            {
                                                if (_locAppSetDetail.EndApproval == true)
                                                {
                                                    ApprovalLine _saveDataAL = new ApprovalLine(_currentSession)
                                                    {
                                                        ApprovalDate = now,
                                                        ApprovalStatus = Status.Approved,
                                                        ApprovalLevel = ApprovalLevel.Level2,
                                                        EndApproval = true,
                                                        CashAdvance = _locCashAdvanceXPO,
                                                    };
                                                    _saveDataAL.Save();
                                                    _saveDataAL.Session.CommitTransaction();

                                                    _locCashAdvanceXPO.ActivationPosting = true;
                                                    _locCashAdvanceXPO.ActiveApproved1 = false;
                                                    _locCashAdvanceXPO.ActiveApproved2 = false;
                                                    _locCashAdvanceXPO.ActiveApproved3 = true;
                                                    _locCashAdvanceXPO.Save();
                                                    _locCashAdvanceXPO.Session.CommitTransaction();
                                                }
                                                else
                                                {
                                                    ApprovalLine _saveDataAL = new ApprovalLine(_currentSession)
                                                    {
                                                        ApprovalDate = now,
                                                        ApprovalStatus = Status.Approved,
                                                        ApprovalLevel = ApprovalLevel.Level2,
                                                        CashAdvance = _locCashAdvanceXPO,
                                                    };
                                                    _saveDataAL.Save();
                                                    _saveDataAL.Session.CommitTransaction();

                                                    SetApprovalLine(_currentSession, _locCashAdvanceXPO, ApprovalLevel.Level1);

                                                    _locCashAdvanceXPO.ActiveApproved1 = false;
                                                    _locCashAdvanceXPO.ActiveApproved2 = true;
                                                    _locCashAdvanceXPO.ActiveApproved3 = false;
                                                    _locCashAdvanceXPO.Save();
                                                    _locCashAdvanceXPO.Session.CommitTransaction();
                                                }

                                                //Send Email
                                                ApprovalLine _locApprovalLineXPO2 = _currentSession.FindObject<ApprovalLine>
                                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                                     new BinaryOperator("CashAdvance", _locCashAdvanceXPO)));

                                                if (_locApprovalLineXPO2 != null)
                                                {
                                                    SendEmail(_currentSession, _locCashAdvanceXPO, Status.Progress, _locApprovalLineXPO2);
                                                }

                                                SuccessMessageShow("CashAdvance has successfully Approve");
                                            }
                                        }
                                    }
                                    #endregion Approval Level 2

                                    #region Approval Level 3
                                    if ((ApprovalLevel)e.SelectedChoiceActionItem.Data == ApprovalLevel.Level3)
                                    {
                                        _locAppSetDetail = _globFunc.GetApplicationSetupDetailByApprovalLevel(_currentSession, _locUserAccess, ApprovalLevel.Level3, ObjectList.CashAdvance);

                                        if (_locAppSetDetail != null)
                                        {
                                            ApprovalLine _locApprovalLineXPO = _currentSession.FindObject<ApprovalLine>
                                                                                (new GroupOperator(GroupOperatorType.And,
                                                                                 new BinaryOperator("CashAdvance", _locCashAdvanceXPO),
                                                                                 new BinaryOperator("ApprovalLevel", ApprovalLevel.Level3)));

                                            if (_locApprovalLineXPO == null)
                                            {
                                                if (_locAppSetDetail.EndApproval == true)
                                                {
                                                    ApprovalLine _saveDataAL = new ApprovalLine(_currentSession)
                                                    {
                                                        ApprovalDate = now,
                                                        ApprovalStatus = Status.Approved,
                                                        ApprovalLevel = ApprovalLevel.Level3,
                                                        EndApproval = true,
                                                        CashAdvance = _locCashAdvanceXPO,
                                                    };
                                                    _saveDataAL.Save();
                                                    _saveDataAL.Session.CommitTransaction();

                                                    _locCashAdvanceXPO.ActivationPosting = true;
                                                    _locCashAdvanceXPO.ActiveApproved1 = false;
                                                    _locCashAdvanceXPO.ActiveApproved2 = false;
                                                    _locCashAdvanceXPO.ActiveApproved3 = true;
                                                    _locCashAdvanceXPO.Save();
                                                    _locCashAdvanceXPO.Session.CommitTransaction();
                                                }
                                                else
                                                {
                                                    ApprovalLine _saveDataAL = new ApprovalLine(_currentSession)
                                                    {
                                                        ApprovalDate = now,
                                                        ApprovalStatus = Status.Approved,
                                                        ApprovalLevel = ApprovalLevel.Level3,
                                                        CashAdvance = _locCashAdvanceXPO,
                                                    };
                                                    _saveDataAL.Save();
                                                    _saveDataAL.Session.CommitTransaction();

                                                    SetApprovalLine(_currentSession, _locCashAdvanceXPO, ApprovalLevel.Level2);
                                                    SetApprovalLine(_currentSession, _locCashAdvanceXPO, ApprovalLevel.Level1);

                                                    _locCashAdvanceXPO.ActiveApproved1 = false;
                                                    _locCashAdvanceXPO.ActiveApproved2 = false;
                                                    _locCashAdvanceXPO.ActiveApproved3 = true;
                                                    _locCashAdvanceXPO.Save();
                                                    _locCashAdvanceXPO.Session.CommitTransaction();
                                                }

                                                //Send Email
                                                ApprovalLine _locApprovalLineXPO2 = _currentSession.FindObject<ApprovalLine>
                                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                                     new BinaryOperator("CashAdvance", _locCashAdvanceXPO)));

                                                if (_locApprovalLineXPO2 != null)
                                                {
                                                    SendEmail(_currentSession, _locCashAdvanceXPO, Status.Progress, _locApprovalLineXPO2);
                                                }

                                                SuccessMessageShow("Cash Advance has successfully Approve");
                                            }
                                        }
                                    }
                                    #endregion Approval Level 3
                                }
                            }
                            else
                            {
                                ErrorMessageShow("Cash Advance Status Not Available");
                            }
                        }
                        else
                        {
                            ErrorMessageShow("Cash Advance Not Available");
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = CashAdvance " + ex.ToString());
            }
        }

        private void CashAdvanceListviewFilterSelectionAction_Execute(object sender, SingleChoiceActionExecuteEventArgs e)
        {
            try
            {
                if ((View is ListView) & (View.ObjectTypeInfo.Type == typeof(CashAdvance)))
                {
                    if ((Status)e.SelectedChoiceActionItem.Data == Status.None)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.None, BinaryOperatorType.NotEqual);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Open)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Open, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Progress)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Progress, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Posted)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Posted, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Lock)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Lock, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Close)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Close, BinaryOperatorType.Equal);
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = CashAdvance " + ex.ToString());
            }
        }

        private void CashAdvanceListviewFilterApprovalSelectionAction_Execute(object sender, SingleChoiceActionExecuteEventArgs e)
        {
            try
            {
                if ((View is ListView) & (View.ObjectTypeInfo.Type == typeof(CashAdvance)))
                {
                    if ((ApprovalFilter)e.SelectedChoiceActionItem.Data == ApprovalFilter.Approval)
                    {
                        ((ListView)View).CollectionSource.Criteria["FilterApproval1"] = new GroupOperator(GroupOperatorType.And,
                                                                                        new BinaryOperator("ActiveApproved3", false, BinaryOperatorType.Equal),
                                                                                        new GroupOperator(GroupOperatorType.Or,
                                                                                        new BinaryOperator("ActiveApproved1", true, BinaryOperatorType.Equal),
                                                                                        new BinaryOperator("ActiveApproved2", true, BinaryOperatorType.Equal)));
                    }
                    else if ((ApprovalFilter)e.SelectedChoiceActionItem.Data == ApprovalFilter.EndApproval)
                    {
                        ((ListView)View).CollectionSource.Criteria["FilterApproval1"] = new BinaryOperator("ActiveApproved3", true, BinaryOperatorType.Equal);
                    }
                    else if ((ApprovalFilter)e.SelectedChoiceActionItem.Data == ApprovalFilter.AllData)
                    {
                        ((ListView)View).CollectionSource.Criteria["FilterApproval1"] = new GroupOperator(GroupOperatorType.Or,
                                                                                        new BinaryOperator("ActiveApproved1", true, BinaryOperatorType.Equal),
                                                                                        new BinaryOperator("ActiveApproved2", true, BinaryOperatorType.Equal),
                                                                                        new BinaryOperator("ActiveApproved3", true, BinaryOperatorType.Equal),
                                                                                        new BinaryOperator("ActiveApproved1", false, BinaryOperatorType.Equal),
                                                                                        new BinaryOperator("ActiveApproved2", false, BinaryOperatorType.Equal),
                                                                                        new BinaryOperator("ActiveApproved3", false, BinaryOperatorType.Equal));
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = CashAdvance " + ex.ToString());
            }
        }

        //============================================ Code In Here ============================================

        #region Progress

        private double GetTotAmount(Session _currSession, CashAdvance _locCashAdvanceXPO)
        {
            double _result = 0;
            try
            {
                double _locAmount = 0;

                if (_locCashAdvanceXPO != null)
                {
                    XPCollection<CashAdvanceLine> _locCashAdvanceLines = new XPCollection<CashAdvanceLine>(_currSession,
                                                                            new GroupOperator(GroupOperatorType.And,
                                                                            new BinaryOperator("CashAdvance", _locCashAdvanceXPO),
                                                                            new BinaryOperator("Select", true),
                                                                            new GroupOperator(GroupOperatorType.Or,
                                                                            new BinaryOperator("Status", Status.Open),
                                                                            new BinaryOperator("Status", Status.Progress))));
                    if (_locCashAdvanceLines != null && _locCashAdvanceLines.Count() > 0)
                    {
                        foreach (CashAdvanceLine _locCashAdvanceLine in _locCashAdvanceLines)
                        {
                            if (_locCashAdvanceLine.Amount > 0)
                            {
                                _locAmount = _locAmount + _locCashAdvanceLine.Amount;
                            }
                        }

                        _result = _locAmount;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = CashAdvance ", ex.ToString());
            }
            return _result;
        }

        private double GetTotAmountApproved(Session _currSession, CashAdvance _locCashAdvanceXPO)
        {
            double _result = 0;
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                DateTime now = DateTime.Now;
                double _locAmountApproved = 0;

                if (_locCashAdvanceXPO != null)
                {
                    XPCollection<CashAdvanceLine> _locCashAdvanceLines = new XPCollection<CashAdvanceLine>(_currSession,
                                                                            new GroupOperator(GroupOperatorType.And,
                                                                            new BinaryOperator("CashAdvance", _locCashAdvanceXPO),
                                                                            new BinaryOperator("Select", true),
                                                                            new GroupOperator(GroupOperatorType.Or,
                                                                            new BinaryOperator("Status", Status.Open),
                                                                            new BinaryOperator("Status", Status.Progress))));
                    if (_locCashAdvanceLines != null && _locCashAdvanceLines.Count() > 0)
                    {
                        foreach (CashAdvanceLine _locCashAdvanceLine in _locCashAdvanceLines)
                        {
                            if (_locCashAdvanceLine.Amount > 0)
                            {
                                _locAmountApproved = _locAmountApproved + _locCashAdvanceLine.AmountApproved;
                            }
                        }

                        _result = _locAmountApproved;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = CashAdvance ", ex.ToString());
            }
            return _result;
        }

        #endregion Progress

        #region Posting

        private double GetMaxAmountApprovedCashAdvanceLine(Session _currSession, CashAdvance _locCashAdvanceXPO)
        {
            double _result = 0;
            try
            {
                double _locMaxAmountApproved = 0;

                if (_locCashAdvanceXPO != null)
                {
                    XPCollection<CashAdvanceLine> _locCashAdvanceLines = new XPCollection<CashAdvanceLine>(_currSession,
                                                                            new GroupOperator(GroupOperatorType.And,
                                                                            new BinaryOperator("CashAdvance", _locCashAdvanceXPO),
                                                                            new BinaryOperator("Select", true),
                                                                            new BinaryOperator("Status", Status.Progress)));

                    if (_locCashAdvanceLines != null && _locCashAdvanceLines.Count() > 0)
                    {
                        foreach (CashAdvanceLine _locCashAdvanceLine in _locCashAdvanceLines)
                        {
                            if (_locCashAdvanceLine.AmountApproved > 0)
                            {
                                _locMaxAmountApproved = _locMaxAmountApproved + _locCashAdvanceLine.AmountApproved;
                            }
                        }

                        _result = _locMaxAmountApproved;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = CashAdvance ", ex.ToString());
            }
            return _result;
        }

        private void SetCashAdvanceMonitoring(Session _currSession, CashAdvance _locCashAdvanceXPO)
        {
            try
            {
                
                if (_locCashAdvanceXPO != null)
                {
                    XPCollection<CashAdvanceLine> _locCashAdvanceLines = new XPCollection<CashAdvanceLine>(_currSession,
                                                                            new GroupOperator(GroupOperatorType.And,
                                                                            new BinaryOperator("CashAdvance", _locCashAdvanceXPO),
                                                                            new BinaryOperator("Select", true),
                                                                            new GroupOperator(GroupOperatorType.Or,
                                                                            new BinaryOperator("Status", Status.Progress),
                                                                            new BinaryOperator("Status", Status.Posted))));
                    if (_locCashAdvanceLines != null && _locCashAdvanceLines.Count() > 0)
                    {
                        foreach (CashAdvanceLine _locCashAdvanceLine in _locCashAdvanceLines)
                        {
                            if (_locCashAdvanceLine.AmountApproved > 0)
                            {
                                CashAdvanceMonitoring _saveDataCashAdvanceMonitoring = new CashAdvanceMonitoring(_currSession)
                                {
                                    CashAdvance = _locCashAdvanceXPO,
                                    CashAdvanceLine = _locCashAdvanceLine,
                                    Company = _locCashAdvanceLine.Company,
                                    Workplace = _locCashAdvanceLine.Workplace,
                                    Division = _locCashAdvanceLine.Division,
                                    Department = _locCashAdvanceLine.Department,
                                    Section = _locCashAdvanceLine.Section,
                                    Employee = _locCashAdvanceXPO.Employee,
                                    RequestDate = _locCashAdvanceLine.RequestDate,
                                    RequiredDate = _locCashAdvanceLine.RequiredDate,
                                    CashAdvanceType = _locCashAdvanceLine.CashAdvanceType,
                                    Amount = _locCashAdvanceLine.Amount,
                                    AmountApproved = _locCashAdvanceLine.AmountApproved,
                                    Outstanding = _locCashAdvanceLine.AmountApproved,
                                    Description = _locCashAdvanceLine.Description,
                                    DocDate = _locCashAdvanceLine.DocDate,
                                    JournalMonth = _locCashAdvanceLine.JournalMonth,
                                    JournalYear = _locCashAdvanceLine.JournalYear,
                                };
                                _saveDataCashAdvanceMonitoring.Save();
                                _saveDataCashAdvanceMonitoring.Session.CommitTransaction();
                                
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = CashAdvance ", ex.ToString());
            }
        }

        private bool CheckJournalSetup(Session _currSession, CashAdvance _locCashAdvanceXPO)
        {
            bool _result = false;
            try
            {
                if (_locCashAdvanceXPO != null)
                {
                    if (_locCashAdvanceXPO.Company != null && _locCashAdvanceXPO.Workplace != null)
                    {
                        JournalSetupDetail _locJournalSetupDetail = _currSession.FindObject<JournalSetupDetail>
                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                    new BinaryOperator("Company", _locCashAdvanceXPO.Company),
                                                                    new BinaryOperator("Workplace", _locCashAdvanceXPO.Workplace),
                                                                    new BinaryOperator("JournalSetupType", JournalSetupType.Journal),
                                                                    new BinaryOperator("ObjectList", ObjectList.CashAdvance),
                                                                    new BinaryOperator("JournalProcess", JournalProcess.JournalCashAdvances),
                                                                    new BinaryOperator("Active", true)));

                        if (_locJournalSetupDetail != null)
                        {
                            _result = true;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = CashAdvance ", ex.ToString());
            }

            return _result;
        }

        private void SetJournalCashBon(Session _currSession, CashAdvance _locCashAdvanceXPO)
        {
            try
            {
                DateTime now = DateTime.Now;
                double _locSetCAApproved = 0;
                AdvanceAccountGroup _locAdvanceAccountGroup = null;

                if (_locCashAdvanceXPO != null)
                {
                    XPCollection<CashAdvanceLine> _locCashAdvanceLines = new XPCollection<CashAdvanceLine>(_currSession,
                                                                             new GroupOperator(GroupOperatorType.And,
                                                                             new BinaryOperator("CashAdvance", _locCashAdvanceXPO),
                                                                             new BinaryOperator("Select", true),
                                                                             new GroupOperator(GroupOperatorType.Or,
                                                                             new BinaryOperator("Status", Status.Progress),
                                                                             new BinaryOperator("Status", Status.Posted)
                                                                             )));

                    if (_locCashAdvanceLines != null && _locCashAdvanceLines.Count() > 0)
                    {
                        foreach (CashAdvanceLine _locCashAdvanceLine in _locCashAdvanceLines)
                        {
                            if (_locCashAdvanceLine.AmountApproved > 0)
                            {
                                _locSetCAApproved = _locCashAdvanceLine.AmountApproved;

                                #region JournalMapCompanyAccountGroup
                                if (_locCashAdvanceLine.CashAdvanceType != null)
                                {
                                    if (_locCashAdvanceLine.CashAdvanceType.AdvanceAccountGroup != null)
                                    {
                                        _locAdvanceAccountGroup = _locCashAdvanceLine.CashAdvanceType.AdvanceAccountGroup;

                                        XPCollection<JournalMap> _locJournalMapItems = new XPCollection<JournalMap>
                                                                       (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                        new BinaryOperator("Company", _locCashAdvanceLine.Company),
                                                                        new BinaryOperator("Workplace", _locCashAdvanceLine.Workplace),
                                                                        new BinaryOperator("CompanyAccountGroup", _locAdvanceAccountGroup),
                                                                        new BinaryOperator("Active", true)));

                                        if (_locJournalMapItems != null && _locJournalMapItems.Count() > 0)
                                        {
                                            foreach (JournalMap _locJournalMapItem in _locJournalMapItems)
                                            {
                                                XPCollection<JournalMapLine> _locJournalMapLines = new XPCollection<JournalMapLine>
                                                                                                   (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                    new BinaryOperator("Company", _locCashAdvanceLine.Company),
                                                                                                    new BinaryOperator("Workplace", _locCashAdvanceLine.Workplace),
                                                                                                    new BinaryOperator("JournalMap", _locJournalMapItem),
                                                                                                    new BinaryOperator("Active", true)));

                                                if (_locJournalMapLines != null && _locJournalMapLines.Count() > 0)
                                                {
                                                    foreach (JournalMapLine _locJournalMapLine in _locJournalMapLines)
                                                    {
                                                        if (_locJournalMapLine.AccountMap != null)
                                                        {
                                                            #region NormalAmount
                                                            if (_locJournalMapLine.AccountMap.PostingType == PostingType.Advances && _locJournalMapLine.AccountMap.OrderType == OrderType.Account
                                                                && _locJournalMapLine.AccountMap.PostingMethod == PostingMethod.CashAdvance && _locJournalMapLine.AccountMap.PostingMethodType == PostingMethodType.Advances)
                                                            {
                                                                XPCollection<AccountMapLine> _locAccountMapLines = new XPCollection<AccountMapLine>
                                                                                                               (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                                new BinaryOperator("Company", _locCashAdvanceLine.Company),
                                                                                                                new BinaryOperator("Workplace", _locCashAdvanceLine.Workplace),
                                                                                                                new BinaryOperator("AccountMap", _locJournalMapLine.AccountMap),
                                                                                                                new BinaryOperator("Active", true)));

                                                                if (_locAccountMapLines != null && _locAccountMapLines.Count() > 0)
                                                                {
                                                                    double _locTotalAmountDebit = 0;
                                                                    double _locTotalAmountCredit = 0;
                                                                    double _locTotalBalance = 0;

                                                                    foreach (AccountMapLine _locAccountMapLine in _locAccountMapLines)
                                                                    {
                                                                        if (_locAccountMapLine.AccountCharge == AccountCharge.Debit)
                                                                        {
                                                                            _locTotalAmountDebit = _locSetCAApproved;
                                                                        }
                                                                        if (_locAccountMapLine.AccountCharge == AccountCharge.Credit)
                                                                        {
                                                                            _locTotalAmountCredit = _locSetCAApproved;
                                                                        }

                                                                        GeneralJournal _saveGeneralJournal = new GeneralJournal(_currSession)
                                                                        {
                                                                            Company = _locCashAdvanceLine.Company,
                                                                            Workplace = _locCashAdvanceLine.Workplace,
                                                                            PostingDate = now,
                                                                            PostingType = PostingType.Advances,
                                                                            OrderType = OrderType.Account,
                                                                            PostingMethod = PostingMethod.CashAdvance,
                                                                            PostingMethodType = PostingMethodType.Advances,
                                                                            Account = _locAccountMapLine.Account,
                                                                            Debit = _locTotalAmountDebit,
                                                                            Credit = _locTotalAmountCredit,
                                                                            JournalMonth = _locCashAdvanceLine.JournalMonth,
                                                                            JournalYear = _locCashAdvanceLine.JournalYear,
                                                                            CashAdvance = _locCashAdvanceXPO,
                                                                        };

                                                                        _saveGeneralJournal.Save();
                                                                        _saveGeneralJournal.Session.CommitTransaction();

                                                                        #region AccountingPeriodicLine

                                                                        if (_locAccountMapLine.Account.Code != null)
                                                                        {

                                                                            //Cari Account Periodic Line
                                                                            AccountingPeriodicLine _locAPL = _currSession.FindObject<AccountingPeriodicLine>
                                                                                                            (new GroupOperator(GroupOperatorType.And,
                                                                                                            new BinaryOperator("Company", _locCashAdvanceLine.Company),
                                                                                                            new BinaryOperator("Workplace", _locCashAdvanceLine.Workplace),
                                                                                                            new BinaryOperator("AccountNo", _locAccountMapLine.Account),
                                                                                                            new BinaryOperator("Month", _locCashAdvanceLine.JournalMonth),
                                                                                                            new BinaryOperator("Year", _locCashAdvanceLine.JournalYear)));
                                                                            if (_locAPL != null)
                                                                            {
                                                                                if (_locAPL.AccountNo != null)
                                                                                {
                                                                                    if (_locAPL.AccountNo.BalanceType == BalanceType.Change)
                                                                                    {
                                                                                        if (_locAPL.AccountNo.AccountCharge == AccountCharge.Debit)
                                                                                        {
                                                                                            if (_locTotalAmountDebit > 0)
                                                                                            {
                                                                                                _locTotalBalance = _locAPL.Balance + _locTotalAmountDebit;
                                                                                            }
                                                                                            if (_locTotalAmountCredit > 0)
                                                                                            {
                                                                                                _locTotalBalance = _locAPL.Balance - _locTotalAmountCredit;
                                                                                            }
                                                                                        }
                                                                                        if (_locAPL.AccountNo.AccountCharge == AccountCharge.Credit)
                                                                                        {
                                                                                            if (_locTotalAmountDebit > 0)
                                                                                            {
                                                                                                _locTotalBalance = _locAPL.Balance - _locTotalAmountDebit;
                                                                                            }
                                                                                            if (_locTotalAmountCredit > 0)
                                                                                            {
                                                                                                _locTotalBalance = _locAPL.Balance + _locTotalAmountCredit;
                                                                                            }
                                                                                        }
                                                                                    }

                                                                                    _locAPL.Balance = _locTotalBalance;
                                                                                    _locAPL.Debit = _locAPL.Debit + _locTotalAmountDebit;
                                                                                    _locAPL.Credit = _locAPL.Credit + _locTotalAmountCredit;
                                                                                    _locAPL.Save();
                                                                                    _locAPL.Session.CommitTransaction();

                                                                                }
                                                                            }
                                                                        }
                                                                        #endregion AccountingPeriodicLine
                                                                    }
                                                                }
                                                            }
                                                            #endregion NormalAmount
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                                #endregion JournalMapCompanyAccountGroup

                                #region JournalMapByEmployee
                                if (_locCashAdvanceXPO.Employee != null)
                                {
                                    OrganizationAccountGroup _locOrganizationAccountGroup = _currSession.FindObject<OrganizationAccountGroup>
                                                                                           (new GroupOperator(GroupOperatorType.And,
                                                                                            new BinaryOperator("Company", _locCashAdvanceLine.Company),
                                                                                            new BinaryOperator("Workplace", _locCashAdvanceLine.Workplace),
                                                                                            new BinaryOperator("Employee", _locCashAdvanceXPO.Employee),
                                                                                            new BinaryOperator("Active", true)));
                                    if (_locOrganizationAccountGroup != null)
                                    {
                                        XPCollection<JournalMap> _locJournalMaps = new XPCollection<JournalMap>
                                                                               (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                new BinaryOperator("Company", _locCashAdvanceLine.Company),
                                                                                new BinaryOperator("Workplace", _locCashAdvanceLine.Workplace),
                                                                                new BinaryOperator("OrganizationAccountGroup", _locOrganizationAccountGroup),
                                                                                new BinaryOperator("Active", true)));

                                        if (_locJournalMaps != null && _locJournalMaps.Count() > 0)
                                        {
                                            foreach (JournalMap _locJournalMap in _locJournalMaps)
                                            {
                                                XPCollection<JournalMapLine> _locJournalMapLines = new XPCollection<JournalMapLine>
                                                                                                   (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                    new BinaryOperator("Company", _locCashAdvanceLine.Company),
                                                                                                    new BinaryOperator("Workplace", _locCashAdvanceLine.Workplace),
                                                                                                    new BinaryOperator("JournalMap", _locJournalMap),
                                                                                                    new BinaryOperator("Active", true)));

                                                if (_locJournalMapLines != null && _locJournalMapLines.Count() > 0)
                                                {
                                                    foreach (JournalMapLine _locJournalMapLine in _locJournalMapLines)
                                                    {
                                                        if(_locJournalMapLine.AccountMap != null)
                                                        {
                                                            if (_locJournalMapLine.AccountMap.PostingType == PostingType.Advances && _locJournalMapLine.AccountMap.OrderType == OrderType.Account
                                                            && _locJournalMapLine.AccountMap.PostingMethod == PostingMethod.CashAdvance && _locJournalMapLine.AccountMap.PostingMethodType == PostingMethodType.Advances)
                                                            {
                                                                XPCollection<AccountMapLine> _locAccountMapLines = new XPCollection<AccountMapLine>
                                                                                                                   (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                                    new BinaryOperator("Company", _locCashAdvanceLine.Company),
                                                                                                                    new BinaryOperator("Workplace", _locCashAdvanceLine.Workplace),
                                                                                                                    new BinaryOperator("AccountMap", _locJournalMapLine.AccountMap),
                                                                                                                    new BinaryOperator("Active", true)));

                                                                if (_locAccountMapLines != null && _locAccountMapLines.Count() > 0)
                                                                {
                                                                    double _locTotalAmountDebit = 0;
                                                                    double _locTotalAmountCredit = 0;
                                                                    double _locTotalBalance = 0;

                                                                    foreach (AccountMapLine _locAccountMapLine in _locAccountMapLines)
                                                                    {
                                                                        if (_locAccountMapLine.AccountCharge == AccountCharge.Debit)
                                                                        {
                                                                            _locTotalAmountDebit = _locSetCAApproved;
                                                                        }
                                                                        if (_locAccountMapLine.AccountCharge == AccountCharge.Credit)
                                                                        {
                                                                            _locTotalAmountCredit = _locSetCAApproved;
                                                                        }

                                                                        GeneralJournal _saveGeneralJournal = new GeneralJournal(_currSession)
                                                                        {
                                                                            Company = _locCashAdvanceLine.Company,
                                                                            Workplace = _locCashAdvanceLine.Workplace,
                                                                            PostingDate = now,
                                                                            PostingType = PostingType.Advances,
                                                                            OrderType = OrderType.Account,
                                                                            PostingMethod = PostingMethod.CashAdvance,
                                                                            PostingMethodType = PostingMethodType.Advances,
                                                                            Account = _locAccountMapLine.Account,
                                                                            Debit = _locTotalAmountDebit,
                                                                            Credit = _locTotalAmountCredit,
                                                                            JournalMonth = _locCashAdvanceLine.JournalMonth,
                                                                            JournalYear = _locCashAdvanceLine.JournalYear,
                                                                            CashAdvance = _locCashAdvanceXPO,
                                                                        };

                                                                        _saveGeneralJournal.Save();
                                                                        _saveGeneralJournal.Session.CommitTransaction();

                                                                        #region AccountingPeriodicLine

                                                                        if (_locAccountMapLine.Account.Code != null)
                                                                        {

                                                                            //Cari Account Periodic Line
                                                                            AccountingPeriodicLine _locAPL = _currSession.FindObject<AccountingPeriodicLine>
                                                                                                            (new GroupOperator(GroupOperatorType.And,
                                                                                                            new BinaryOperator("Company", _locCashAdvanceLine.Company),
                                                                                                            new BinaryOperator("Workplace", _locCashAdvanceLine.Workplace),
                                                                                                            new BinaryOperator("AccountNo", _locAccountMapLine.Account),
                                                                                                            new BinaryOperator("Month", _locCashAdvanceLine.JournalMonth),
                                                                                                            new BinaryOperator("Year", _locCashAdvanceLine.JournalYear)));
                                                                            if (_locAPL != null)
                                                                            {
                                                                                if (_locAPL.AccountNo != null)
                                                                                {
                                                                                    if (_locAPL.AccountNo.BalanceType == BalanceType.Change)
                                                                                    {
                                                                                        if (_locAPL.AccountNo.AccountCharge == AccountCharge.Debit)
                                                                                        {
                                                                                            if (_locTotalAmountDebit > 0)
                                                                                            {
                                                                                                _locTotalBalance = _locAPL.Balance + _locTotalAmountDebit;
                                                                                            }
                                                                                            if (_locTotalAmountCredit > 0)
                                                                                            {
                                                                                                _locTotalBalance = _locAPL.Balance - _locTotalAmountCredit;
                                                                                            }
                                                                                        }
                                                                                        if (_locAPL.AccountNo.AccountCharge == AccountCharge.Credit)
                                                                                        {
                                                                                            if (_locTotalAmountDebit > 0)
                                                                                            {
                                                                                                _locTotalBalance = _locAPL.Balance - _locTotalAmountDebit;
                                                                                            }
                                                                                            if (_locTotalAmountCredit > 0)
                                                                                            {
                                                                                                _locTotalBalance = _locAPL.Balance + _locTotalAmountCredit;
                                                                                            }
                                                                                        }
                                                                                    }

                                                                                    _locAPL.Balance = _locTotalBalance;
                                                                                    _locAPL.Debit = _locAPL.Debit + _locTotalAmountDebit;
                                                                                    _locAPL.Credit = _locAPL.Credit + _locTotalAmountCredit;
                                                                                    _locAPL.Save();
                                                                                    _locAPL.Session.CommitTransaction();

                                                                                }
                                                                            }
                                                                        }
                                                                        #endregion AccountingPeriodicLine
                                                                    }
                                                                }
                                                            }
                                                        }      
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                                #endregion JournalMapByEmployee

                                _locSetCAApproved = 0;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = CashAdvance ", ex.ToString());
            }
        }

        private void SetStatusCashAdvanceLine(Session _currSession, CashAdvance _locCashAdvanceXPO)
        {
            try
            {
                DateTime now = DateTime.Now;

                if (_locCashAdvanceXPO != null)
                {
                    XPCollection<CashAdvanceLine> _locCALines = new XPCollection<CashAdvanceLine>(_currSession,
                                                                new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("CashAdvance", _locCashAdvanceXPO),
                                                                new GroupOperator(GroupOperatorType.Or,
                                                                new BinaryOperator("Status", Status.Open),
                                                                new BinaryOperator("Status", Status.Progress),
                                                                new BinaryOperator("Status", Status.Posted))));

                    if (_locCALines != null && _locCALines.Count > 0)
                    {
                        foreach (CashAdvanceLine _locCALine in _locCALines)
                        {
                            _locCALine.ActivationPosting = true;
                            _locCALine.Status = Status.Close;
                            _locCALine.StatusDate = now;
                            _locCALine.Save();
                            _locCALine.Session.CommitTransaction();

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = CashAdvance " + ex.ToString());
            }
        }

        private void SetNormal(Session _currSession, CashAdvance _locCashAdvanceXPO)
        {
            try
            {
                DateTime now = DateTime.Now;

                if (_locCashAdvanceXPO != null)
                {
                    XPCollection<CashAdvanceLine> _locCashAdvanceLines = new XPCollection<CashAdvanceLine>(_currSession,
                                                                                     new GroupOperator(GroupOperatorType.And,
                                                                                     new BinaryOperator("CashAdvance", _locCashAdvanceXPO),
                                                                                     new BinaryOperator("Select", true),
                                                                                     new GroupOperator(GroupOperatorType.Or,
                                                                                     new BinaryOperator("Status", Status.Progress),
                                                                                     new BinaryOperator("Status", Status.Posted))));

                    if (_locCashAdvanceLines != null && _locCashAdvanceLines.Count > 0)
                    {
                        foreach (CashAdvanceLine _locCashAdvanceLine in _locCashAdvanceLines)
                        {
                            _locCashAdvanceLine.Select = false;
                            _locCashAdvanceLine.Save();
                            _locCashAdvanceLine.Session.CommitTransaction();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = CashAdvance " + ex.ToString());
            }
        }

        private void SetStatusCashAdvance(Session _currSession, CashAdvance _locCashAdvanceXPO)
        {
            try
            {
                DateTime now = DateTime.Now;
                int _locStatusCount = 0;
                int _locCashAdvanceLineCount = 0;

                if (_locCashAdvanceXPO != null)
                {
                    XPCollection<CashAdvanceLine> _locCALines = new XPCollection<CashAdvanceLine>
                                                                (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                 new BinaryOperator("CashAdvance", _locCashAdvanceXPO)));

                    if (_locCALines != null && _locCALines.Count() > 0)
                    {
                        _locCashAdvanceLineCount = _locCALines.Count();

                        foreach (CashAdvanceLine _locCALine in _locCALines)
                        {
                            if (_locCALine.Status == Status.Close)
                            {
                                _locStatusCount = _locStatusCount + 1; 
                            }
                        }
                    }

                    if(_locCashAdvanceLineCount == _locStatusCount)
                    {
                        _locCashAdvanceXPO.ActivationPosting = true;
                        _locCashAdvanceXPO.Status = Status.Close;
                        _locCashAdvanceXPO.StatusDate = now;
                        _locCashAdvanceXPO.Save();
                        _locCashAdvanceXPO.Session.CommitTransaction();
                    }else
                    {
                        _locCashAdvanceXPO.ActivationPosting = true;
                        _locCashAdvanceXPO.Status = Status.Close;
                        _locCashAdvanceXPO.StatusDate = now;
                        _locCashAdvanceXPO.Save();
                        _locCashAdvanceXPO.Session.CommitTransaction();
                    }
                    
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = CashAdvance " + ex.ToString());
            }
        }

        #endregion Posting

        #region Approval Process

        private void SetApprovalLine(Session _currentSession, CashAdvance _locCashAdvanceXPO, ApprovalLevel _locApprovalLevel)
        {
            try
            {
                DateTime now = DateTime.Now;

                ApprovalLine _locApprovalLineXPO = _currentSession.FindObject<ApprovalLine>
                                                                        (new GroupOperator(GroupOperatorType.And,
                                                                         new BinaryOperator("CashAdvance", _locCashAdvanceXPO),
                                                                         new BinaryOperator("ApprovalLevel", _locApprovalLevel)));

                if (_locApprovalLineXPO == null)
                {
                    ApprovalLine _saveDataAL2 = new ApprovalLine(_currentSession)
                    {
                        ApprovalDate = now,
                        ApprovalStatus = Status.Approved,
                        ApprovalLevel = _locApprovalLevel,
                        CashAdvance = _locCashAdvanceXPO,
                    };
                    _saveDataAL2.Save();
                    _saveDataAL2.Session.CommitTransaction();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = CashAdvance " + ex.ToString());
            }
        }

        #endregion Approval Process

        #region Email

        private string BackgroundBody(Session _currentSession, CashAdvance _locCashAdvanceXPO, MailSetupDetail _locMailSetDetail)
        {
            string body = string.Empty;

            if (_locCashAdvanceXPO != null)
            {
                ApprovalLine _locApprovalLineXPO = _currentSession.FindObject<ApprovalLine>
                                               (new GroupOperator(GroupOperatorType.And,
                                                new BinaryOperator("CashAdvance", _locCashAdvanceXPO)));

                string Status = _locApprovalLineXPO.ApprovalStatus.ToString();

                using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/Email/EmailTemplate.html")))
                {
                    body = reader.ReadToEnd();
                }
                body = body.Replace("{Code}", _locCashAdvanceXPO.Code);

                #region Level
                if (_locCashAdvanceXPO.ActiveApproved1 == true)
                {
                    string Active = "Level 1";
                    body = body.Replace("{Level}", Active);
                }
                else if (_locCashAdvanceXPO.ActiveApproved2 == true)
                {
                    string Active = "Level 2";
                    body = body.Replace("{Level}", Active);
                }
                else if (_locCashAdvanceXPO.ActiveApproved3 == true)
                {
                    string Active = "Level 3";
                    body = body.Replace("{Level}", Active);
                }
                else
                {
                    string Active = "Not Available";
                    body = body.Replace("{Level}", Active);
                }
                #endregion Level

                body = body.Replace("{Status}", Status);
                body = body.Replace("{Message}", _locMailSetDetail.MessageBody);
            }
            return body;
        }

        protected void SendEmail(Session _currentSession, CashAdvance _locCashAdvanceXPO, Status _locStatus, ApprovalLine _locApprovalLineXPO2)
        {
            DateTime now = DateTime.Now;
            var User = SecuritySystem.CurrentUserName;
            GlobalFunction _globFunc = new GlobalFunction();
            UserAccess _numLineUserAccess = _currentSession.FindObject<UserAccess>(new BinaryOperator("UserName", User));
            string _locMessageBody = null;

            try
            {
                if (_locCashAdvanceXPO != null)
                {
                    XPCollection<MailSetupDetail> _locMailSetDetails = new XPCollection<MailSetupDetail>
                                                                       (_currentSession, new GroupOperator(GroupOperatorType.And,
                                                                        new BinaryOperator("ObjectList", ObjectList.CashAdvance),
                                                                        new BinaryOperator("UserAccess", _numLineUserAccess),
                                                                        new BinaryOperator("Active", true)));

                    if (_locMailSetDetails != null && _locMailSetDetails.Count > 0)
                    {
                        foreach (MailSetupDetail _locMailSetDetail in _locMailSetDetails)
                        {
                            if (_locMailSetDetail.MessageBody != null)
                            {
                                if (_locApprovalLineXPO2.EndApproval == true)
                                {
                                    _locMessageBody = this.BackgroundBody(_currentSession, _locCashAdvanceXPO, _locMailSetDetail);
                                }
                                else
                                {
                                    _locMessageBody = this.BackgroundBody(_currentSession, _locCashAdvanceXPO, _locMailSetDetail);
                                }
                            }

                            _globFunc.SetAndSendMail(_locMailSetDetail.SmtpHost, _locMailSetDetail.SmtpPort,
                            _locMailSetDetail.MailFrom, _locMailSetDetail.MailFromPassword, _locMailSetDetail.MailTo, _locMailSetDetail.MailSubject, _locMessageBody);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = CashAdvance " + ex.ToString());
            }
        }

        #endregion Email

        #region GlobalMethod

        private void SuccessMessageShow(string _locActionName)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 2000;
            options.Message = string.Format("{0} !", _locActionName);
            options.Type = InformationType.Success;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Success";
            options.Win.Type = WinMessageType.Flyout;
            Application.ShowViewStrategy.ShowMessage(options);
        }

        private void ErrorMessageShow(string _locMessage)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 2000;
            options.Message = string.Format(_locMessage);
            options.Type = InformationType.Warning;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Error";
            options.Win.Type = WinMessageType.Alert;
            Application.ShowViewStrategy.ShowMessage(options);
        }

        #endregion GlobalMethod

    }
}
