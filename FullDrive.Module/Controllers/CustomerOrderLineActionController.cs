﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;

using FullDrive.Module.BusinessObjects;
using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.Xpo;
using System.Collections;
using DevExpress.Xpo;

namespace FullDrive.Module.Controllers
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class CustomerOrderLineActionController : ViewController
    {
        private ChoiceActionItem _selectionListviewFilter;

        public CustomerOrderLineActionController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.
            #region FilterStatus
            CustomerOrderLineListviewFilterSelectionAction.Items.Clear();
            foreach (object _currApproval in Enum.GetValues(typeof(CustomProcess.Status)))
            {
                EnumDescriptor _ed = new EnumDescriptor(typeof(CustomProcess.Status));
                if (_ed.GetCaption(_currApproval) != "Approved" && _ed.GetCaption(_currApproval) != "Lock")
                {
                    _selectionListviewFilter = new ChoiceActionItem(_ed.GetCaption(_currApproval), _currApproval);
                }
                CustomerOrderLineListviewFilterSelectionAction.Items.Add(_selectionListviewFilter);
            }
            #endregion FilterStatus
        }

        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
        }

        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }

        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        private void CustomerOrderLineSelectAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;

                if (_objectToProcess != null)
                {
                    foreach (Object obj in _objectToProcess)
                    {
                        CustomerOrderLine _locCustomerOrderLineOS = (CustomerOrderLine)_objectSpace.GetObject(obj);

                        if (_locCustomerOrderLineOS != null)
                        {
                            if (_locCustomerOrderLineOS.Session != null)
                            {
                                _currSession = _locCustomerOrderLineOS.Session;
                            }

                            if (_locCustomerOrderLineOS.Code != null && _currSession != null)
                            {
                                _currObjectId = _locCustomerOrderLineOS.Code;

                                XPCollection<CustomerOrderLine> _locCustomerOrderLines = new XPCollection<CustomerOrderLine>
                                                                                                     (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                      new BinaryOperator("Code", _currObjectId),
                                                                                                      new GroupOperator(GroupOperatorType.Or,
                                                                                                      new BinaryOperator("Status", Status.Progress),
                                                                                                      new BinaryOperator("Status", Status.Open)
                                                                                                      )));

                                if (_locCustomerOrderLines != null && _locCustomerOrderLines.Count > 0)
                                {
                                    foreach (CustomerOrderLine _locCustomerOrderLine in _locCustomerOrderLines)
                                    {
                                        _locCustomerOrderLine.Select = true;
                                        _locCustomerOrderLine.Save();
                                        _locCustomerOrderLine.Session.CommitTransaction();
                                    }

                                    SuccessMessageShow("Customer Order Line has been Select");
                                }
                                else
                                {
                                    ErrorMessageShow("Data Customer Order Line Not Available");
                                }
                            }
                        }
                    }
                    if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                    {
                        _objectSpace.CommitChanges();
                        _objectSpace.Refresh();
                    }
                    if (View is ListView)
                    {
                        _objectSpace.CommitChanges();
                        View.ObjectSpace.Refresh();
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("BusinessObject = CustomerOrderLine" + ex.ToString());
            }
        }

        private void CustomerOrderLineUnselectAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;

                if (_objectToProcess != null)
                {
                    foreach (Object obj in _objectToProcess)
                    {
                        CustomerOrderLine _locCustomerOrderLineOS = (CustomerOrderLine)_objectSpace.GetObject(obj);

                        if (_locCustomerOrderLineOS != null)
                        {
                            if (_locCustomerOrderLineOS.Session != null)
                            {
                                _currSession = _locCustomerOrderLineOS.Session;
                            }

                            if (_locCustomerOrderLineOS.Code != null && _currSession != null)
                            {
                                _currObjectId = _locCustomerOrderLineOS.Code;

                                XPCollection<CustomerOrderLine> _locCustomerOrderLines = new XPCollection<CustomerOrderLine>
                                                                                                     (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                      new BinaryOperator("Code", _currObjectId),
                                                                                                      new GroupOperator(GroupOperatorType.Or,
                                                                                                      new BinaryOperator("Status", Status.Progress),
                                                                                                      new BinaryOperator("Status", Status.Open)
                                                                                                      )));

                                if (_locCustomerOrderLines != null && _locCustomerOrderLines.Count > 0)
                                {
                                    foreach (CustomerOrderLine _locCustomerOrderLine in _locCustomerOrderLines)
                                    {
                                        _locCustomerOrderLine.Select = false;
                                        _locCustomerOrderLine.Save();
                                        _locCustomerOrderLine.Session.CommitTransaction();
                                    }

                                    SuccessMessageShow("Customer Order Line has been Unselect");
                                }
                                else
                                {
                                    ErrorMessageShow("Data Customer Order Line Not Available");
                                }
                            }
                        }
                    }
                    if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                    {
                        _objectSpace.CommitChanges();
                        _objectSpace.Refresh();
                    }
                    if (View is ListView)
                    {
                        _objectSpace.CommitChanges();
                        View.ObjectSpace.Refresh();
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("BusinessObject = CustomerOrderLine" + ex.ToString());
            }
        }

        private void CustomerOrderLineTaxAndDiscountAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;

                if (_objectToProcess != null)
                {
                    foreach (Object obj in _objectToProcess)
                    {
                        CustomerOrderLine _locCustomerOrderLineOS = (CustomerOrderLine)_objectSpace.GetObject(obj);

                        if (_locCustomerOrderLineOS != null)
                        {
                            if (_locCustomerOrderLineOS.Session != null)
                            {
                                _currSession = _locCustomerOrderLineOS.Session;
                            }

                            if (_locCustomerOrderLineOS.Code != null && _currSession != null)
                            {
                                _currObjectId = _locCustomerOrderLineOS.Code;

                                XPCollection<CustomerOrderLine> _locCustomerOrderLines = new XPCollection<CustomerOrderLine>
                                                                                                     (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                      new BinaryOperator("Code", _currObjectId),
                                                                                                      new GroupOperator(GroupOperatorType.Or,
                                                                                                      new BinaryOperator("Status", Status.Progress),
                                                                                                      new BinaryOperator("Status", Status.Open)
                                                                                                      )));

                                if (_locCustomerOrderLines != null && _locCustomerOrderLines.Count > 0)
                                {
                                    foreach (CustomerOrderLine _locCustomerOrderLine in _locCustomerOrderLines)
                                    {
                                        SetTaxBeforeDiscount(_currSession, _locCustomerOrderLine);
                                        SetDiscountGross(_currSession, _locCustomerOrderLine);
                                        SetTaxAfterDiscount(_currSession, _locCustomerOrderLine);
                                        SetDiscountNet(_currSession, _locCustomerOrderLine);
                                    }

                                    SuccessMessageShow("Customer Order Line Line has been Processed");
                                }
                                else
                                {
                                    ErrorMessageShow("Data Customer Order Line Line Not Available");
                                }
                            }
                        }
                    }
                    if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                    {
                        _objectSpace.CommitChanges();
                        _objectSpace.Refresh();
                    }
                    if (View is ListView)
                    {
                        _objectSpace.CommitChanges();
                        View.ObjectSpace.Refresh();
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("BusinessObject = CustomerOrderLine " + ex.ToString());
            }
        }

        private void CustomerOrderLineListviewFilterSelectionAction_Execute(object sender, SingleChoiceActionExecuteEventArgs e)
        {
            try
            {
                if ((View is ListView) & (View.ObjectTypeInfo.Type == typeof(CustomerOrderLine)))
                {
                    if ((Status)e.SelectedChoiceActionItem.Data == Status.None)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.None, BinaryOperatorType.NotEqual);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Open)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Open, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Progress)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Progress, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Posted)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Posted, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Lock)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Lock, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Close)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Close, BinaryOperatorType.Equal);
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = CustomerOrderLine " + ex.ToString());
            }
        }

        //======================================== Code In Here ========================================

        #region Tax

        private void SetTaxBeforeDiscount(Session _currSession, CustomerOrderLine _locCustomerOrderLineXPO)
        {
            try
            {
                double _locTxAmount = 0;
                double _locTAmount = 0;
                if (_locCustomerOrderLineXPO != null)
                {
                    if (_locCustomerOrderLineXPO.Tax != null)
                    {
                        if (_locCustomerOrderLineXPO.Tax.TaxRule == TaxRule.BeforeDiscount)
                        {
                            _locTxAmount = _locCustomerOrderLineXPO.TxValue / 100 * _locCustomerOrderLineXPO.TUAmount;
                            if (_locCustomerOrderLineXPO.Tax.TaxNature == TaxNature.Increase)
                            {
                                _locTAmount = _locCustomerOrderLineXPO.TAmount + _locTxAmount;
                            }
                            if (_locCustomerOrderLineXPO.Tax.TaxNature == TaxNature.Decrease)
                            {
                                _locTAmount = _locCustomerOrderLineXPO.TAmount - _locTxAmount;
                            }
                            _locCustomerOrderLineXPO.TxAmount = _locTxAmount;
                            _locCustomerOrderLineXPO.TAmount = _locTAmount;
                            _locCustomerOrderLineXPO.Save();
                            _locCustomerOrderLineXPO.Session.CommitTransaction();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("BusinessObject = CustomerOrderLine" + ex.ToString());
            }
        }

        private void SetDiscountGross(Session _currSession, CustomerOrderLine _locCustomerOrderLineXPO)
        {
            try
            {
                if (_locCustomerOrderLineXPO != null)
                {
                    if (_locCustomerOrderLineXPO.DiscountRule != null)
                    {
                        double _locTUAmountDiscount = 0;
                        double _locTUAmountDiscount2 = 0;
                        AccountingPeriodicLine _locAcctPerLine = null;

                        #region Gross

                        if (_locCustomerOrderLineXPO.DiscountRule.GrossAmountRule == true)
                        {
                            if (_locCustomerOrderLineXPO.TUAmount > 0 && _locCustomerOrderLineXPO.TUAmount >= _locCustomerOrderLineXPO.DiscountRule.GrossTotalUAmount)
                            {
                                if (_locCustomerOrderLineXPO.DiscountRule.DiscountType == DiscountType.Percentage)
                                {
                                    _locTUAmountDiscount = _locCustomerOrderLineXPO.TUAmount * _locCustomerOrderLineXPO.DiscountRule.Value / 100;
                                }
                                if (_locCustomerOrderLineXPO.DiscountRule.DiscountType == DiscountType.Value)
                                {
                                    _locTUAmountDiscount = _locCustomerOrderLineXPO.DiscountRule.Value;
                                }

                                #region ValBasedAccount
                                if (_locCustomerOrderLineXPO.DiscountRule.ValBasedAccount == true)
                                {
                                    if (_locCustomerOrderLineXPO.DiscountRule.AccountNo != null)
                                    {
                                        AccountingPeriodic _locAccountPeriodic = _currSession.FindObject<AccountingPeriodic>
                                                                        (new GroupOperator(GroupOperatorType.And,
                                                                        new BinaryOperator("Company", _locCustomerOrderLineXPO.Company),
                                                                        new BinaryOperator("Workplace", _locCustomerOrderLineXPO.Workplace),
                                                                        new BinaryOperator("Active", true)));
                                        if (_locAccountPeriodic != null)
                                        {
                                            AccountingPeriodicLine _locAccountingPeriodicLine = _currSession.FindObject<AccountingPeriodicLine>
                                                                                                (new GroupOperator(GroupOperatorType.And,
                                                                                                new BinaryOperator("AccountingPeriodic", _locAccountPeriodic),
                                                                                                new BinaryOperator("AccountNo", _locCustomerOrderLineXPO.DiscountRule.AccountNo)));
                                            if (_locAccountingPeriodicLine != null)
                                            {
                                                _locAcctPerLine = _locAccountingPeriodicLine;
                                                if(_locAccountingPeriodicLine.BalHold < _locAccountingPeriodicLine.Balance)
                                                {
                                                    _locTUAmountDiscount2 = _locAccountingPeriodicLine.Balance - _locAccountingPeriodicLine.BalHold;
                                                    if (_locTUAmountDiscount2 < _locTUAmountDiscount)
                                                    {
                                                        _locTUAmountDiscount = _locTUAmountDiscount2;
                                                    }
                                                }
                                                else
                                                {
                                                    _locTUAmountDiscount = 0;
                                                }
                                                
                                                _locAccountingPeriodicLine.BalHold = _locAccountingPeriodicLine.BalHold + _locTUAmountDiscount;
                                                _locAccountingPeriodicLine.Save();
                                                _locAccountingPeriodicLine.Session.CommitTransaction();
                                            }
                                        }
                                    }
                                }
                                #endregion ValBasedAccount

                                _locCustomerOrderLineXPO.AccountingPeriodicLine = _locAcctPerLine;
                                _locCustomerOrderLineXPO.DiscountChecked = true;
                                _locCustomerOrderLineXPO.DiscAmount = _locTUAmountDiscount;
                                _locCustomerOrderLineXPO.TAmount = _locCustomerOrderLineXPO.TAmount - _locTUAmountDiscount;
                                _locCustomerOrderLineXPO.Save();
                                _locCustomerOrderLineXPO.Session.CommitTransaction();
                            }
                        }

                        #endregion Gross
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("BusinessObject = CustomerOrderLine" + ex.ToString());
            }
        }

        private void SetTaxAfterDiscount(Session _currSession, CustomerOrderLine _locCustomerOrderLineXPO)
        {
            try
            {
                double _locTxAmount = 0;
                double _locTAmount = 0;
                if (_locCustomerOrderLineXPO != null)
                {
                    if (_locCustomerOrderLineXPO.Tax != null )
                    {
                        if(_locCustomerOrderLineXPO.DiscountRule != null)
                        {
                            if (_locCustomerOrderLineXPO.Tax.TaxRule == TaxRule.AfterDiscount && _locCustomerOrderLineXPO.DiscountRule.GrossAmountRule == true)
                            {
                                _locTxAmount = _locCustomerOrderLineXPO.TxValue / 100 * (_locCustomerOrderLineXPO.TUAmount - _locCustomerOrderLineXPO.DiscAmount);

                                if (_locCustomerOrderLineXPO.Tax.TaxNature == TaxNature.Increase)
                                {
                                    _locTAmount = _locCustomerOrderLineXPO.TAmount + _locTxAmount;
                                }
                                if (_locCustomerOrderLineXPO.Tax.TaxNature == TaxNature.Decrease)
                                {
                                    _locTAmount = _locCustomerOrderLineXPO.TAmount - _locTxAmount;
                                }
                                _locCustomerOrderLineXPO.TxAmount = _locTxAmount;
                                _locCustomerOrderLineXPO.TAmount = _locTAmount;
                                _locCustomerOrderLineXPO.Save();
                                _locCustomerOrderLineXPO.Session.CommitTransaction();
                            }
                        }else
                        {
                            if (_locCustomerOrderLineXPO.Tax.TaxRule == TaxRule.AfterDiscount )
                            {
                                _locTxAmount = _locCustomerOrderLineXPO.TxValue / 100 * (_locCustomerOrderLineXPO.TUAmount);

                                if (_locCustomerOrderLineXPO.Tax.TaxNature == TaxNature.Increase)
                                {
                                    _locTAmount = _locCustomerOrderLineXPO.TAmount + _locTxAmount;
                                }
                                if (_locCustomerOrderLineXPO.Tax.TaxNature == TaxNature.Decrease)
                                {
                                    _locTAmount = _locCustomerOrderLineXPO.TAmount - _locTxAmount;
                                }
                                _locCustomerOrderLineXPO.TxAmount = _locTxAmount;
                                _locCustomerOrderLineXPO.TAmount = _locTAmount;
                                _locCustomerOrderLineXPO.Save();
                                _locCustomerOrderLineXPO.Session.CommitTransaction();
                            }
                        }
                        
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("BusinessObject = CustomerOrderLine" + ex.ToString());
            }
        }

        private void SetDiscountNet(Session _currSession, CustomerOrderLine _locCustomerOrderLineXPO)
        {
            try
            {
                if (_locCustomerOrderLineXPO != null)
                {
                    if (_locCustomerOrderLineXPO.DiscountRule != null && _locCustomerOrderLineXPO.Tax != null)
                    {
                        double _locTUAmountDiscount = 0;
                        double _locTUAmountDiscount2 = 0;
                        AccountingPeriodicLine _locAcctPerLine = null;

                        #region Net
                        if (_locCustomerOrderLineXPO.DiscountRule.NetAmountRule == true && _locCustomerOrderLineXPO.Tax.TaxRule == TaxRule.BeforeDiscount)
                        {
                            if (_locCustomerOrderLineXPO.TAmount > 0 && _locCustomerOrderLineXPO.TAmount >= _locCustomerOrderLineXPO.DiscountRule.NetTotalUAmount)
                            {
                                if (_locCustomerOrderLineXPO.DiscountRule.DiscountType == DiscountType.Percentage)
                                {
                                    _locTUAmountDiscount = _locCustomerOrderLineXPO.TAmount * _locCustomerOrderLineXPO.DiscountRule.Value / 100;
                                }
                                if (_locCustomerOrderLineXPO.DiscountRule.DiscountType == DiscountType.Value)
                                {
                                    _locTUAmountDiscount = _locCustomerOrderLineXPO.DiscountRule.Value;
                                }

                                #region ValBasedAccount
                                if (_locCustomerOrderLineXPO.DiscountRule.ValBasedAccount == true)
                                {
                                    if (_locCustomerOrderLineXPO.DiscountRule.AccountNo != null)
                                    {
                                        AccountingPeriodic _locAccountPeriodic = _currSession.FindObject<AccountingPeriodic>
                                                                        (new GroupOperator(GroupOperatorType.And,
                                                                        new BinaryOperator("Company", _locCustomerOrderLineXPO.Company),
                                                                        new BinaryOperator("Workplace", _locCustomerOrderLineXPO.Workplace),
                                                                        new BinaryOperator("Active", true)));
                                        if (_locAccountPeriodic != null)
                                        {
                                            AccountingPeriodicLine _locAccountingPeriodicLine = _currSession.FindObject<AccountingPeriodicLine>
                                                                                                (new GroupOperator(GroupOperatorType.And,
                                                                                                new BinaryOperator("AccountingPeriodic", _locAccountPeriodic),
                                                                                                new BinaryOperator("AccountNo", _locCustomerOrderLineXPO.DiscountRule.AccountNo)));
                                            if (_locAccountingPeriodicLine != null)
                                            {
                                                _locAcctPerLine = _locAccountingPeriodicLine;
                                                if (_locAccountingPeriodicLine.BalHold < _locAccountingPeriodicLine.Balance)
                                                {
                                                    _locTUAmountDiscount2 = _locAccountingPeriodicLine.Balance - _locAccountingPeriodicLine.BalHold;

                                                    if (_locTUAmountDiscount2 < _locTUAmountDiscount)
                                                    {
                                                        _locTUAmountDiscount = _locTUAmountDiscount2;
                                                    }
                                                }
                                                else
                                                {
                                                    _locTUAmountDiscount = 0;
                                                }

                                                _locAccountingPeriodicLine.BalHold = _locAccountingPeriodicLine.BalHold + _locTUAmountDiscount;
                                                _locAccountingPeriodicLine.Save();
                                                _locAccountingPeriodicLine.Session.CommitTransaction();
                                            }
                                        }
                                    }
                                }
                                #endregion ValBasedAccount

                                _locCustomerOrderLineXPO.AccountingPeriodicLine = _locAcctPerLine;
                                _locCustomerOrderLineXPO.DiscountChecked = true;
                                _locCustomerOrderLineXPO.DiscAmount = _locTUAmountDiscount;
                                _locCustomerOrderLineXPO.TAmount = _locCustomerOrderLineXPO.TAmount - _locTUAmountDiscount;
                                _locCustomerOrderLineXPO.Save();
                                _locCustomerOrderLineXPO.Session.CommitTransaction();
                            }
                        }
                        #endregion Net
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("BusinessObject = CustomerOrderLine" + ex.ToString());
            }
        }

        #endregion Tax

        #region Global Method

        private void SuccessMessageShow(string _locActionName)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 2000;
            options.Message = string.Format("{0}", _locActionName);
            options.Type = InformationType.Success;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Success";
            options.Win.Type = WinMessageType.Flyout;
            Application.ShowViewStrategy.ShowMessage(options);
        }

        private void ErrorMessageShow(string _locMessage)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 2000;
            options.Message = string.Format(_locMessage);
            options.Type = InformationType.Warning;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Error";
            options.Win.Type = WinMessageType.Alert;
            Application.ShowViewStrategy.ShowMessage(options);
        }

        #endregion Global Method

        
    }
}
