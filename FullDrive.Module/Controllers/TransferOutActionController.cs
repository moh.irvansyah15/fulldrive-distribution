﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;

using FullDrive.Module.BusinessObjects;
using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.Xpo;
using System.Collections;
using DevExpress.Xpo;

namespace FullDrive.Module.Controllers
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class TransferOutActionController : ViewController
    {
        private ChoiceActionItem _selectionListviewFilter;

        public TransferOutActionController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.
            #region Filter Status
            TransferOutListviewFilterSelectionAction.Items.Clear();
            foreach (object _currApproval in Enum.GetValues(typeof(CustomProcess.Status)))
            {
                EnumDescriptor _ed = new EnumDescriptor(typeof(CustomProcess.Status));
                _selectionListviewFilter = new ChoiceActionItem(_ed.GetCaption(_currApproval), _currApproval);
                TransferOutListviewFilterSelectionAction.Items.Add(_selectionListviewFilter);
            }
            #endregion Filter Status
        }

        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
        }

        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }

        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        private void TransferOutGetStockAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;

                if (_objectsToProcess != null)
                {
                    foreach (Object obj in _objectsToProcess)
                    {
                        TransferOut _locTransferOutOS = (TransferOut)_objectSpace.GetObject(obj);

                        if (_locTransferOutOS != null)
                        {
                            if (_locTransferOutOS.Session != null)
                            {
                                _currSession = _locTransferOutOS.Session;
                            }

                            if (_locTransferOutOS.Code != null && _currSession != null)
                            {
                                _currObjectId = _locTransferOutOS.Code;

                                TransferOut _locTransferOutXPO = _currSession.FindObject<TransferOut>
                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                     new BinaryOperator("Code", _currObjectId),
                                                                     new GroupOperator(GroupOperatorType.Or,
                                                                     new BinaryOperator("Status", Status.Open),
                                                                     new BinaryOperator("Status", Status.Progress))));

                                if (_locTransferOutXPO != null)
                                {
                                    if(_locTransferOutXPO.DocumentRule == DocumentRule.Customer)
                                    {
                                        XPCollection<SalesInvoiceCollection> _locSalesInvoiceCollections = new XPCollection<SalesInvoiceCollection>
                                                                                        (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                        new BinaryOperator("TransferOut", _locTransferOutXPO),
                                                                                        new GroupOperator(GroupOperatorType.Or,
                                                                                        new BinaryOperator("Status", Status.Open),
                                                                                        new BinaryOperator("Status", Status.Progress))));

                                        if (_locSalesInvoiceCollections != null && _locSalesInvoiceCollections.Count() > 0)
                                        {
                                            foreach (SalesInvoiceCollection _locSalesInvoiceCollection in _locSalesInvoiceCollections)
                                            {
                                                if (_locSalesInvoiceCollection.SalesInvoice != null)
                                                {
                                                    GetSalesInvoiceMonitoring(_currSession, _locSalesInvoiceCollection.SalesInvoice, _locTransferOutXPO);
                                                }
                                            }
                                            SuccessMessageShow("Sales Invoice Has Been Successfully Getting into Transfer Out");
                                        }

                                    }
                                    if(_locTransferOutXPO.DocumentRule == DocumentRule.Salesman)
                                    {
                                        GetBeginningInventory(_currSession, _locTransferOutXPO);
                                        SuccessMessageShow("Stock Has Been Successfully Getting into Transfer Out");
                                    }
                                   
                                }
                                else
                                {
                                    ErrorMessageShow("Data Transfer Out Not Available");
                                }
                            }
                            else
                            {
                                ErrorMessageShow("Data Transfer Out Not Available");
                            }
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = TransferOut " + ex.ToString());
            }
        }

        private void TransferOutProgressAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;
                Status _locStatus = Status.None;
                DateTime _locNow;
                Status _locStatus2 = Status.None;
                DateTime _locNow2;

                if (_objectsToProcess != null)
                {
                    foreach (Object obj in _objectsToProcess)
                    {
                        TransferOut _locTransferOutOS = (TransferOut)_objectSpace.GetObject(obj);

                        if (_locTransferOutOS != null)
                        {
                            if (_locTransferOutOS.Session != null)
                            {
                                _currSession = _locTransferOutOS.Session;
                            }
                            if (_locTransferOutOS.Code != null && _currSession != null)
                            {
                                _currObjectId = _locTransferOutOS.Code;

                                TransferOut _locTransferOutXPO = _currSession.FindObject<TransferOut>
                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                     new BinaryOperator("Code", _currObjectId),
                                                                     new GroupOperator(GroupOperatorType.Or,
                                                                     new BinaryOperator("Status", Status.Open),
                                                                     new BinaryOperator("Status", Status.Progress))));

                                if (_locTransferOutXPO != null)
                                {
                                    if (_locTransferOutXPO.Status == Status.Open)
                                    {
                                        _locStatus = Status.Progress;
                                        _locNow = now;
                                    }
                                    else
                                    {
                                        _locStatus = _locTransferOutXPO.Status;
                                        _locNow = _locTransferOutXPO.StatusDate;
                                    }
                                    _locTransferOutXPO.Status = _locStatus;
                                    _locTransferOutXPO.StatusDate = _locNow;
                                    _locTransferOutXPO.Save();
                                    _locTransferOutXPO.Session.CommitTransaction();

                                    #region TransferOutLine and TransferOutLot
                                    XPCollection<TransferOutLine> _locTransferOutLines = new XPCollection<TransferOutLine>
                                                           (_currSession, new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("TransferOut", _locTransferOutXPO),
                                                            new GroupOperator(GroupOperatorType.Or,
                                                            new BinaryOperator("Status", Status.Open),
                                                            new BinaryOperator("Status", Status.Progress))));

                                    if (_locTransferOutLines != null && _locTransferOutLines.Count > 0)
                                    {
                                        foreach (TransferOutLine _locTransferOutLine in _locTransferOutLines)
                                        {
                                            if (_locTransferOutLine.Status == Status.Open)
                                            {
                                                _locStatus2 = Status.Progress;
                                                _locNow2 = now;
                                            }
                                            else
                                            {
                                                _locStatus2 = _locTransferOutLine.Status;
                                                _locNow2 = _locTransferOutLine.StatusDate;
                                            }

                                            _locTransferOutLine.Status = _locStatus2;
                                            _locTransferOutLine.StatusDate = _locNow2;
                                            _locTransferOutLine.Save();
                                            _locTransferOutLine.Session.CommitTransaction();
                                            
                                            XPCollection<TransferOutLot> _locTransferOutLots = new XPCollection<TransferOutLot>
                                                                                                    (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                    new BinaryOperator("TransferOutLine", _locTransferOutLine),
                                                                                                    new BinaryOperator("Status", Status.Open)));

                                            if (_locTransferOutLots != null && _locTransferOutLots.Count() > 0)
                                            {
                                                foreach (TransferOutLot _locTransferOutLot in _locTransferOutLots)
                                                {
                                                    _locTransferOutLot.Status = Status.Progress;
                                                    _locTransferOutLot.StatusDate = now;
                                                    _locTransferOutLot.Save();
                                                    _locTransferOutLot.Session.CommitTransaction();
                                                }
                                            }
                                        }
                                    }
                                    #endregion TransferOutLine and TransferOutLot

                                    #region SalesInvoiceCollection
                                    XPCollection<SalesInvoiceCollection> _locSalesInvoiceCollections = new XPCollection<SalesInvoiceCollection>
                                                                                    (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                    new BinaryOperator("TransferOut", _locTransferOutXPO),
                                                                                    new BinaryOperator("Status", Status.Open)));
                                    if (_locSalesInvoiceCollections != null && _locSalesInvoiceCollections.Count() > 0)
                                    {
                                        foreach (SalesInvoiceCollection _locSalesInvoiceCollection in _locSalesInvoiceCollections)
                                        {
                                            _locSalesInvoiceCollection.Status = Status.Progress;
                                            _locSalesInvoiceCollection.StatusDate = now;
                                            _locSalesInvoiceCollection.Save();
                                            _locSalesInvoiceCollection.Session.CommitTransaction();
                                        }
                                    }
                                    #endregion TransferOutMonitoring

                                    SetBegInvLineTo(_currSession, _locTransferOutXPO);

                                    SuccessMessageShow(_locTransferOutXPO.Code + " has been change successfully to Progress");
                                    
                                }
                                else
                                {
                                    ErrorMessageShow("Data Transfer Out Not Available");
                                }
                            }
                            else
                            {
                                ErrorMessageShow("Data Transfer Out Not Available");
                            }
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = TransferOut " + ex.ToString());
            }
        }

        private void TransferOutPostingAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;

                if (_objectsToProcess != null)
                {
                    foreach (Object obj in _objectsToProcess)
                    {
                        TransferOut _locTransferOutOS = (TransferOut)_objectSpace.GetObject(obj);

                        if (_locTransferOutOS != null)
                        {
                            if (_locTransferOutOS.Status == Status.Progress || _locTransferOutOS.Status == Status.Posted)
                            {
                                if (_locTransferOutOS.Session != null)
                                {
                                    _currSession = _locTransferOutOS.Session;
                                }
                                if(_locTransferOutOS.Code != null && _currSession != null)
                                {
                                    _currObjectId = _locTransferOutOS.Code;

                                    TransferOut _locTransferOutXPO = _currSession.FindObject<TransferOut>
                                                                        (new GroupOperator(GroupOperatorType.And,
                                                                         new BinaryOperator("Code", _currObjectId)));

                                    if (_locTransferOutXPO != null)
                                    {
                                        if (CheckAvailableStock(_currSession, _locTransferOutXPO) == true && CheckLockStock(_currSession, _locTransferOutXPO) == false)
                                        {
                                            SetDeliverBeginingInventory(_currSession, _locTransferOutXPO);
                                            SetDeliverInventoryJournal(_currSession, _locTransferOutXPO);
                                            SetTransferOutMonitoring(_currSession, _locTransferOutXPO);
                                            SetStatusDeliverTransferOutLine(_currSession, _locTransferOutXPO);
                                            SetNormalDeliverQuantity(_currSession, _locTransferOutXPO);
                                            SetFinalStatusDeliverTransferOut(_currSession, _locTransferOutXPO);
                                            
                                            SuccessMessageShow(_locTransferOutXPO.Code + " has been change successfully to Deliver");
                                        }
                                        else
                                        {
                                            ErrorMessageShow("Please check stock quantity");
                                        }
                                    }
                                    else
                                    {
                                        ErrorMessageShow("Transfer Out Data Not Available");
                                    }
                                }
                            }
                            else
                            {
                                ErrorMessageShow("Transfer Out Data Not Available");
                            }
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = TransferOut " + ex.ToString());
            }
        }

        private void TransferOutListviewFilterSelectionAction_Execute(object sender, SingleChoiceActionExecuteEventArgs e)
        {
            try
            {
                if ((View is ListView) & (View.ObjectTypeInfo.Type == typeof(TransferOut)))
                {
                    if ((Status)e.SelectedChoiceActionItem.Data == Status.None)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.None, BinaryOperatorType.NotEqual);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Open)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Open, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Progress)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Progress, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Posted)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Posted, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Lock)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Lock, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Close)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Close, BinaryOperatorType.Equal);
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = TransferOut " + ex.ToString());
            }
        }

        //============================================ Code In Here ============================================

        #region GetStock

        #region GetSIM

        private void GetSalesInvoiceMonitoring(Session _currSession, SalesInvoice _locSalesInvoiceXPO, TransferOut _locTransferOutXPO)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                DateTime now = DateTime.Now;
                string _currSignCode = null;
                

                if (_locSalesInvoiceXPO != null && _locTransferOutXPO != null)
                {
                    if(_locTransferOutXPO.Picking != null)
                    {
                        PickingLine _locPickingLine = _currSession.FindObject<PickingLine>(new GroupOperator(GroupOperatorType.And,
                                                    new BinaryOperator("Picking", _locTransferOutXPO.Picking),
                                                    new BinaryOperator("SalesInvoice", _locSalesInvoiceXPO)));
                        if(_locPickingLine != null)
                        {
                            PickingMonitoring _locPickingMonitoring = _currSession.FindObject<PickingMonitoring>(new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("Picking", _locTransferOutXPO.Picking),
                                                                new BinaryOperator("PickingLine", _locPickingLine)));
                            if (_locPickingMonitoring != null)
                            {
                                _locPickingMonitoring.TransferOut = _locTransferOutXPO;
                                _locPickingMonitoring.Save();
                                _locPickingMonitoring.Session.CommitTransaction();
                            }

                            #region SalesInvoiceMonitoring
                            //Hanya memindahkan SalesInvoiceMonitoring ke TransferOutLine
                            XPCollection<SalesInvoiceMonitoring> _locSalesInvoiceMonitorings = new XPCollection<SalesInvoiceMonitoring>
                                                                                                (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                new BinaryOperator("SalesInvoice", _locSalesInvoiceXPO),
                                                                                                new BinaryOperator("Picking", _locTransferOutXPO.Picking),
                                                                                                new BinaryOperator("Select", true),
                                                                                                new BinaryOperator("Status", Status.Close, BinaryOperatorType.NotEqual)));

                            if (_locSalesInvoiceMonitorings != null && _locSalesInvoiceMonitorings.Count() > 0)
                            {
                                foreach (SalesInvoiceMonitoring _locSalesInvoiceMonitoring in _locSalesInvoiceMonitorings)
                                {

                                    if ((_locSalesInvoiceMonitoring.SalesInvoiceLine != null || _locSalesInvoiceMonitoring.SalesInvoiceFreeItem != null) 
                                        && _locSalesInvoiceMonitoring.TransferOutLine == null && _locSalesInvoiceMonitoring.Item != null)
                                    {
                                        _currSignCode = _globFunc.GetNumberingSignUnlockOptimisticRecordBasedOrganization(_currSession.DataLayer, ObjectList.TransferOutLine, _locTransferOutXPO.Company, _locTransferOutXPO.Workplace);

                                        if (_currSignCode != null)
                                        {
                                            #region TransferOutLine
                                            TransferOutLine _saveDataTransferOutLine = new TransferOutLine(_currSession)
                                            {
                                                SignCode = _currSignCode,
                                                TransferOut = _locTransferOutXPO,
                                                Company = _locTransferOutXPO.Company,
                                                Workplace = _locTransferOutXPO.Workplace,
                                                Division = _locTransferOutXPO.Division,
                                                Department = _locTransferOutXPO.Department,
                                                Section = _locTransferOutXPO.Section,
                                                Employee = _locTransferOutXPO.Employee,
                                                LocationTypeFrom = _locTransferOutXPO.LocationTypeFrom,
                                                WorkplaceFrom = _locSalesInvoiceMonitoring.Workplace,
                                                LocationFrom = _locSalesInvoiceMonitoring.Location,
                                                StockGroupFrom = _locSalesInvoiceMonitoring.StockGroup,
                                                StockTypeFrom = _locSalesInvoiceMonitoring.StockType,
                                                BegInvFrom = _locSalesInvoiceMonitoring.BegInv,
                                                BegInvLineFrom = _locSalesInvoiceMonitoring.BegInvLine,
                                                LocationTypeTo = _locTransferOutXPO.LocationTypeTo,
                                                WorkplaceTo = _locTransferOutXPO.WorkplaceTo,
                                                LocationTo = _locTransferOutXPO.LocationTo,
                                                StockGroupTo = _locSalesInvoiceMonitoring.StockGroup,
                                                StockTypeTo = _locTransferOutXPO.StockTypeTo,
                                                BegInvTo = _locTransferOutXPO.BegInvTo,
                                                Item = _locSalesInvoiceMonitoring.Item,
                                                Description = _locSalesInvoiceMonitoring.Description,
                                                DQty = _locSalesInvoiceMonitoring.DQty,
                                                DUOM = _locSalesInvoiceMonitoring.DUOM,
                                                Qty = _locSalesInvoiceMonitoring.Qty,
                                                UOM = _locSalesInvoiceMonitoring.UOM,
                                                TQty = _locSalesInvoiceMonitoring.TQty,
                                                SalesInvoiceMonitoring = _locSalesInvoiceMonitoring,
                                                SalesInvoice = _locSalesInvoiceMonitoring.SalesInvoice,
                                                ETD = _locSalesInvoiceMonitoring.ETD,
                                                ETA = _locSalesInvoiceMonitoring.ETA,
                                                DocDate = _locSalesInvoiceMonitoring.DocDate,
                                                JournalMonth = _locSalesInvoiceMonitoring.JournalMonth,
                                                JournalYear = _locSalesInvoiceMonitoring.JournalYear,
                                            };
                                            _saveDataTransferOutLine.Save();
                                            _saveDataTransferOutLine.Session.CommitTransaction();
                                            #endregion TransferOutLine

                                            TransferOutLine _locTransOutLine = _currSession.FindObject<TransferOutLine>
                                                                        (new GroupOperator(GroupOperatorType.And,
                                                                        new BinaryOperator("SignCode", _currSignCode)));
                                            if (_locTransOutLine != null)
                                            {
                                                SetRemainQty(_currSession, _locSalesInvoiceMonitoring);
                                                SetPostingQty(_currSession, _locSalesInvoiceMonitoring);
                                                SetProcessCount(_currSession, _locSalesInvoiceMonitoring);
                                                SetStatusSalesInvoiceMonitoring(_currSession, _locSalesInvoiceMonitoring);
                                                SetNormalQuantity(_currSession, _locSalesInvoiceMonitoring);
                                            }
                                        }
                                        _locSalesInvoiceMonitoring.PickingLine = _locPickingLine;
                                        _locSalesInvoiceMonitoring.Save();
                                        _locSalesInvoiceMonitoring.Session.CommitTransaction();
                                    }
                                }
                            }
                            #endregion SalesInvoiceMonitoring
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = TransferOut ", ex.ToString());
            }
        }

        private void SetRemainQty(Session _currSession, SalesInvoiceMonitoring _locSalesInvoiceMonitoringXPO)
        {
            try
            {
                if (_locSalesInvoiceMonitoringXPO != null)
                {
                    double _locRmDQty = 0;
                    double _locRmQty = 0;
                    double _locInvLineTotal = 0;
                    ItemUnitOfMeasure _locItemUOM = null;

                    #region ProcessCount=0
                    if (_locSalesInvoiceMonitoringXPO.PostedCount == 0)
                    {
                        #region MaxQuantity
                        if (_locSalesInvoiceMonitoringXPO.MxDQty > 0)
                        {
                            if (_locSalesInvoiceMonitoringXPO.DQty > 0 && _locSalesInvoiceMonitoringXPO.DQty <= _locSalesInvoiceMonitoringXPO.MxDQty)
                            {
                                _locRmDQty = _locSalesInvoiceMonitoringXPO.MxDQty - _locSalesInvoiceMonitoringXPO.DQty;
                            }

                            if (_locSalesInvoiceMonitoringXPO.Qty > 0 && _locSalesInvoiceMonitoringXPO.Qty <= _locSalesInvoiceMonitoringXPO.MxQty)
                            {
                                _locRmQty = _locSalesInvoiceMonitoringXPO.MxQty - _locSalesInvoiceMonitoringXPO.Qty;
                            }

                            _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                       (new GroupOperator(GroupOperatorType.And,
                                                        new BinaryOperator("Item", _locSalesInvoiceMonitoringXPO.Item),
                                                        new BinaryOperator("UOM", _locSalesInvoiceMonitoringXPO.MxUOM),
                                                        new BinaryOperator("DefaultUOM", _locSalesInvoiceMonitoringXPO.MxDUOM),
                                                        new BinaryOperator("Active", true)));
                            if (_locItemUOM != null)
                            {
                                if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                {
                                    _locInvLineTotal = _locRmQty * _locItemUOM.DefaultConversion + _locRmDQty;
                                }
                                else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                {
                                    _locInvLineTotal = _locRmQty / _locItemUOM.Conversion + _locRmDQty;
                                }
                                else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                {
                                    _locInvLineTotal = _locRmQty + _locRmDQty;
                                }
                            }
                            else
                            {
                                _locInvLineTotal = _locRmQty + _locRmDQty;
                            }
                        }
                        #endregion MaxQuantity

                        #region NonMaxQuantity
                        else
                        {
                            if (_locSalesInvoiceMonitoringXPO.DQty > 0)
                            {
                                _locRmDQty = 0;
                            }

                            if (_locSalesInvoiceMonitoringXPO.Qty > 0)
                            {
                                _locRmQty = 0;
                            }

                            _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                       (new GroupOperator(GroupOperatorType.And,
                                                        new BinaryOperator("Item", _locSalesInvoiceMonitoringXPO.Item),
                                                        new BinaryOperator("UOM", _locSalesInvoiceMonitoringXPO.UOM),
                                                        new BinaryOperator("DefaultUOM", _locSalesInvoiceMonitoringXPO.DUOM),
                                                        new BinaryOperator("Active", true)));
                            if (_locItemUOM != null)
                            {
                                if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                {
                                    _locInvLineTotal = _locRmQty * _locItemUOM.DefaultConversion + _locRmDQty;
                                }
                                else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                {
                                    _locInvLineTotal = _locRmQty / _locItemUOM.Conversion + _locRmDQty;
                                }
                                else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                {
                                    _locInvLineTotal = _locRmQty + _locRmDQty;
                                }
                            }
                            else
                            {
                                _locInvLineTotal = _locRmQty + _locRmDQty;
                            }
                        }
                        #endregion NonMaxQuantity
                    }
                    #endregion ProcessCount=0

                    #region ProcessCount>0
                    if (_locSalesInvoiceMonitoringXPO.PostedCount > 0)
                    {
                        if (_locSalesInvoiceMonitoringXPO.RmDQty > 0)
                        {
                            _locRmDQty = _locSalesInvoiceMonitoringXPO.RmDQty - _locSalesInvoiceMonitoringXPO.DQty;
                        }

                        if (_locSalesInvoiceMonitoringXPO.RmQty > 0)
                        {
                            _locRmQty = _locSalesInvoiceMonitoringXPO.RmQty - _locSalesInvoiceMonitoringXPO.Qty;
                        }

                        if (_locSalesInvoiceMonitoringXPO.MxDQty > 0 || _locSalesInvoiceMonitoringXPO.MxQty > 0)
                        {
                            _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                       (new GroupOperator(GroupOperatorType.And,
                                                        new BinaryOperator("Item", _locSalesInvoiceMonitoringXPO.Item),
                                                        new BinaryOperator("UOM", _locSalesInvoiceMonitoringXPO.MxUOM),
                                                        new BinaryOperator("DefaultUOM", _locSalesInvoiceMonitoringXPO.MxDUOM),
                                                        new BinaryOperator("Active", true)));
                        }
                        else
                        {
                            _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                       (new GroupOperator(GroupOperatorType.And,
                                                        new BinaryOperator("Item", _locSalesInvoiceMonitoringXPO.Item),
                                                        new BinaryOperator("UOM", _locSalesInvoiceMonitoringXPO.UOM),
                                                        new BinaryOperator("DefaultUOM", _locSalesInvoiceMonitoringXPO.DUOM),
                                                        new BinaryOperator("Active", true)));
                        }


                        if (_locItemUOM != null)
                        {
                            if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                            {
                                _locInvLineTotal = _locRmQty * _locItemUOM.DefaultConversion + _locRmDQty;
                            }
                            else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                            {
                                _locInvLineTotal = _locRmQty / _locItemUOM.Conversion + _locRmDQty;
                            }
                            else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                            {
                                _locInvLineTotal = _locRmQty + _locRmDQty;
                            }
                        }
                        else
                        {
                            _locInvLineTotal = _locRmQty + _locRmDQty;
                        }

                    }
                    #endregion ProcessCount>0

                    _locSalesInvoiceMonitoringXPO.RmDQty = _locRmDQty;
                    _locSalesInvoiceMonitoringXPO.RmQty = _locRmQty;
                    _locSalesInvoiceMonitoringXPO.RmTQty = _locInvLineTotal;
                    _locSalesInvoiceMonitoringXPO.Save();
                    _locSalesInvoiceMonitoringXPO.Session.CommitTransaction();

                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = TransferOut " + ex.ToString());
            }
        }

        private void SetPostingQty(Session _currSession, SalesInvoiceMonitoring _locSalesInvoiceMonitoringXPO)
        {
            try
            {
                if (_locSalesInvoiceMonitoringXPO != null)
                {
                    double _locPDQty = 0;
                    double _locPQty = 0;
                    double _locInvLineTotal = 0;
                    ItemUnitOfMeasure _locItemUOM = null;

                    #region ProcessCount=0
                    if (_locSalesInvoiceMonitoringXPO.PostedCount == 0)
                    {
                        #region MaxQuantity
                        if (_locSalesInvoiceMonitoringXPO.MxDQty > 0)
                        {
                            if (_locSalesInvoiceMonitoringXPO.DQty > 0 && _locSalesInvoiceMonitoringXPO.DQty <= _locSalesInvoiceMonitoringXPO.MxDQty)
                            {
                                _locPDQty = _locSalesInvoiceMonitoringXPO.DQty;
                            }

                            if (_locSalesInvoiceMonitoringXPO.Qty > 0 && _locSalesInvoiceMonitoringXPO.Qty <= _locSalesInvoiceMonitoringXPO.MxQty)
                            {
                                _locPQty = _locSalesInvoiceMonitoringXPO.Qty;
                            }

                            _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                       (new GroupOperator(GroupOperatorType.And,
                                                        new BinaryOperator("Item", _locSalesInvoiceMonitoringXPO.Item),
                                                        new BinaryOperator("UOM", _locSalesInvoiceMonitoringXPO.MxUOM),
                                                        new BinaryOperator("DefaultUOM", _locSalesInvoiceMonitoringXPO.MxDUOM),
                                                        new BinaryOperator("Active", true)));
                            if (_locItemUOM != null)
                            {
                                if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                {
                                    _locInvLineTotal = _locPQty * _locItemUOM.DefaultConversion + _locPDQty;
                                }
                                else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                {
                                    _locInvLineTotal = _locPQty / _locItemUOM.Conversion + _locPDQty;
                                }
                                else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                {
                                    _locInvLineTotal = _locPQty + _locPDQty;
                                }
                            }
                            else
                            {
                                _locInvLineTotal = _locPQty + _locPDQty;
                            }
                        }
                        #endregion MaxQuantity

                        #region NonMaxQuantity
                        else
                        {
                            if (_locSalesInvoiceMonitoringXPO.DQty > 0)
                            {
                                _locPDQty = _locSalesInvoiceMonitoringXPO.DQty;
                            }

                            if (_locSalesInvoiceMonitoringXPO.Qty > 0)
                            {
                                _locPQty = _locSalesInvoiceMonitoringXPO.Qty;
                            }

                            _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                       (new GroupOperator(GroupOperatorType.And,
                                                        new BinaryOperator("Item", _locSalesInvoiceMonitoringXPO.Item),
                                                        new BinaryOperator("UOM", _locSalesInvoiceMonitoringXPO.UOM),
                                                        new BinaryOperator("DefaultUOM", _locSalesInvoiceMonitoringXPO.DUOM),
                                                        new BinaryOperator("Active", true)));
                            if (_locItemUOM != null)
                            {
                                if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                {
                                    _locInvLineTotal = _locPQty * _locItemUOM.DefaultConversion + _locPDQty;
                                }
                                else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                {
                                    _locInvLineTotal = _locPQty / _locItemUOM.Conversion + _locPDQty;
                                }
                                else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                {
                                    _locInvLineTotal = _locPQty + _locPDQty;
                                }
                            }
                            else
                            {
                                _locInvLineTotal = _locPQty + _locPDQty;
                            }
                        }
                        #endregion NonMaxQuantity
                    }
                    #endregion ProcessCount=0

                    #region ProcessCount>0
                    if (_locSalesInvoiceMonitoringXPO.PostedCount > 0)
                    {
                        if (_locSalesInvoiceMonitoringXPO.PDQty > 0)
                        {
                            _locPDQty = _locSalesInvoiceMonitoringXPO.PDQty + _locSalesInvoiceMonitoringXPO.DQty;
                        }

                        if (_locSalesInvoiceMonitoringXPO.PQty > 0)
                        {
                            _locPQty = _locSalesInvoiceMonitoringXPO.PQty + _locSalesInvoiceMonitoringXPO.Qty;
                        }

                        if (_locSalesInvoiceMonitoringXPO.MxDQty > 0 || _locSalesInvoiceMonitoringXPO.MxQty > 0)
                        {
                            _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                   (new GroupOperator(GroupOperatorType.And,
                                                    new BinaryOperator("Item", _locSalesInvoiceMonitoringXPO.Item),
                                                    new BinaryOperator("UOM", _locSalesInvoiceMonitoringXPO.MxUOM),
                                                    new BinaryOperator("DefaultUOM", _locSalesInvoiceMonitoringXPO.MxDUOM),
                                                    new BinaryOperator("Active", true)));
                        }
                        else
                        {
                            _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                   (new GroupOperator(GroupOperatorType.And,
                                                    new BinaryOperator("Item", _locSalesInvoiceMonitoringXPO.Item),
                                                    new BinaryOperator("UOM", _locSalesInvoiceMonitoringXPO.UOM),
                                                    new BinaryOperator("DefaultUOM", _locSalesInvoiceMonitoringXPO.DUOM),
                                                    new BinaryOperator("Active", true)));
                        }

                        if (_locItemUOM != null)
                        {
                            if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                            {
                                _locInvLineTotal = _locPQty * _locItemUOM.DefaultConversion + _locPDQty;
                            }
                            else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                            {
                                _locInvLineTotal = _locPQty / _locItemUOM.Conversion + _locPDQty;
                            }
                            else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                            {
                                _locInvLineTotal = _locPQty + _locPDQty;
                            }
                        }
                        else
                        {
                            _locInvLineTotal = _locPQty + _locPDQty;
                        }
                    }
                    #endregion ProcessCount>0

                    _locSalesInvoiceMonitoringXPO.PDQty = _locPDQty;
                    _locSalesInvoiceMonitoringXPO.PDUOM = _locSalesInvoiceMonitoringXPO.DUOM;
                    _locSalesInvoiceMonitoringXPO.PQty = _locPQty;
                    _locSalesInvoiceMonitoringXPO.PUOM = _locSalesInvoiceMonitoringXPO.UOM;
                    _locSalesInvoiceMonitoringXPO.PTQty = _locInvLineTotal;
                    _locSalesInvoiceMonitoringXPO.Save();
                    _locSalesInvoiceMonitoringXPO.Session.CommitTransaction();

                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = TransferOut " + ex.ToString());
            }
        }

        private void SetProcessCount(Session _currSession, SalesInvoiceMonitoring _locSalesInvoiceMonitoringXPO)
        {
            try
            {
                if (_locSalesInvoiceMonitoringXPO != null)
                {
                    _locSalesInvoiceMonitoringXPO.PostedCount = _locSalesInvoiceMonitoringXPO.PostedCount + 1;
                    _locSalesInvoiceMonitoringXPO.Save();
                    _locSalesInvoiceMonitoringXPO.Session.CommitTransaction();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = TransferOut " + ex.ToString());
            }
        }

        private void SetStatusSalesInvoiceMonitoring(Session _currSession, SalesInvoiceMonitoring _locSalesInvoiceMonitoringXPO)
        {
            try
            {
                DateTime now = DateTime.Now;
                if (_locSalesInvoiceMonitoringXPO != null)
                {
                    if (_locSalesInvoiceMonitoringXPO.RmDQty == 0 && _locSalesInvoiceMonitoringXPO.RmQty == 0 && _locSalesInvoiceMonitoringXPO.RmTQty == 0)
                    {
                        _locSalesInvoiceMonitoringXPO.Status = Status.Close;
                        _locSalesInvoiceMonitoringXPO.ActivationPosting = true;
                        _locSalesInvoiceMonitoringXPO.StatusDate = now;
                    }
                    else
                    {
                        _locSalesInvoiceMonitoringXPO.Status = Status.Posted;
                        _locSalesInvoiceMonitoringXPO.StatusDate = now;
                    }
                    _locSalesInvoiceMonitoringXPO.Save();
                    _locSalesInvoiceMonitoringXPO.Session.CommitTransaction();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = TransferOut " + ex.ToString());
            }
        }

        private void SetNormalQuantity(Session _currSession, SalesInvoiceMonitoring _locSalesInvoiceMonitoringXPO)
        {
            try
            {
                if (_locSalesInvoiceMonitoringXPO != null)
                {
                    if (_locSalesInvoiceMonitoringXPO.DQty > 0 || _locSalesInvoiceMonitoringXPO.Qty > 0)
                    {
                        _locSalesInvoiceMonitoringXPO.Select = false;
                        _locSalesInvoiceMonitoringXPO.DQty = 0;
                        _locSalesInvoiceMonitoringXPO.Qty = 0;
                        _locSalesInvoiceMonitoringXPO.Save();
                        _locSalesInvoiceMonitoringXPO.Session.CommitTransaction();
                    }  
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = TransferOut " + ex.ToString());
            }
        }

        private Vehicle GetVehicle(Session _currSession, Employee _locPIC, TransferOut _locTransferOutXPO)
        {
            Vehicle _result = null;
            try
            {
                DateTime now = DateTime.Now;
                if (_locTransferOutXPO.Company != null && _locTransferOutXPO.WorkplaceFrom != null && _locPIC != null)
                {
                    SalesmanVehicleSetup _locSalesmanVehicleSetup = _currSession.FindObject<SalesmanVehicleSetup>
                                                        (new GroupOperator(GroupOperatorType.And,
                                                        new BinaryOperator("Company", _locTransferOutXPO.Company),
                                                        new BinaryOperator("Workplace", _locTransferOutXPO.WorkplaceFrom),
                                                        new BinaryOperator("Salesman", _locPIC),
                                                        new BinaryOperator("Active", true)));
                    if (_locSalesmanVehicleSetup != null)
                    {
                        if (_locSalesmanVehicleSetup.Vehicle != null)
                        {
                            _result = _locSalesmanVehicleSetup.Vehicle;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Business Object = TransferOut ", ex.ToString());
            }
            return _result;
        }

        #endregion GetSIM
        #region GetBegInv

        //Yg Login Salesman dgn begInv Vehicle
        private void GetBeginningInventory(Session _currSession, TransferOut _locTransferOutXPO)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                DateTime now = DateTime.Now;
                string _currSignCode = null;

                if (_locTransferOutXPO != null )
                {
                    if(_locTransferOutXPO != null)
                    {
                        if (_locTransferOutXPO.BegInvFrom != null)
                        {

                            XPCollection<BeginningInventoryLine> _locBegInventoryLines = new XPCollection<BeginningInventoryLine>(_currSession, 
                                                                                        new GroupOperator(GroupOperatorType.And,
                                                                                        new BinaryOperator("BeginningInventory", _locTransferOutXPO.BegInvFrom),
                                                                                        new BinaryOperator("QtyAvailable", 0, BinaryOperatorType.Greater),
                                                                                        new BinaryOperator("Lock", false),
                                                                                        new BinaryOperator("Active", true)));

                           
                            if (_locBegInventoryLines != null && _locBegInventoryLines.Count() > 0)
                            {
                                foreach (BeginningInventoryLine _locBegInventoryLine in _locBegInventoryLines)
                                {
                                    double _locCmbTQty = 0;
                                    double _locTQty = 0;
                                    double _locDQty = 0;
                                    double _locQty = 0;
                                    UnitOfMeasure _locDUOM = null;
                                    UnitOfMeasure _locUOM = null;
                                    if (_locTransferOutXPO.Company != null && _locBegInventoryLine.Item != null)
                                    {
                                        _locCmbTQty = _locBegInventoryLine.QtyAvailable;

                                        ItemUnitOfMeasure _locIUOM = _currSession.FindObject<ItemUnitOfMeasure>(new GroupOperator(GroupOperatorType.And,
                                                                                                new BinaryOperator("Company", _locTransferOutXPO.Company),
                                                                                                new BinaryOperator("Item", _locBegInventoryLine.Item),
                                                                                                new BinaryOperator("DefaultUOM", _locBegInventoryLine.DefaultUOM),
                                                                                                new BinaryOperator("Default", true),
                                                                                                new BinaryOperator("Active", true)));

                                        if (_locIUOM.Item != null && _locIUOM.DefaultUOM != null && _locIUOM.UOM != null)
                                        {
                                            _locDUOM = _locIUOM.DefaultUOM;
                                            _locUOM = _locIUOM.UOM;
                                            if (_locIUOM.Conversion < _locIUOM.DefaultConversion)
                                            {
                                                _locDQty = _locCmbTQty % _locIUOM.DefaultConversion;
                                                _locQty = System.Math.Floor(_locCmbTQty / _locIUOM.DefaultConversion);
                                                _locTQty = _locCmbTQty;
                                            }
                                            else if (_locIUOM.Conversion > _locIUOM.DefaultConversion)
                                            {
                                                _locDQty = System.Math.Floor(_locCmbTQty / _locIUOM.DefaultConversion);
                                                _locQty = (_locCmbTQty % _locIUOM.DefaultConversion) * _locIUOM.Conversion;
                                                _locTQty = _locCmbTQty;
                                            }
                                            else if (_locIUOM.Conversion == _locIUOM.DefaultConversion)
                                            {
                                                _locDQty = _locCmbTQty;
                                                _locTQty = _locCmbTQty;
                                            }
                                            
                                        }else
                                        {
                                            _locDUOM = _locBegInventoryLine.DefaultUOM;
                                            _locDQty = _locCmbTQty;
                                            _locTQty = _locCmbTQty;
                                        }

                                    }
                                    
                                    #region TransferOutLine
                                    TransferOutLine _saveDataTransferOutLine = new TransferOutLine(_currSession)
                                    {
                                        SignCode = _currSignCode,
                                        TransferOut = _locTransferOutXPO,
                                        Company = _locTransferOutXPO.Company,
                                        Division = _locTransferOutXPO.Division,
                                        Department = _locTransferOutXPO.Department,
                                        Section = _locTransferOutXPO.Section,
                                        Employee = _locTransferOutXPO.Employee,
                                        LocationTypeFrom = _locTransferOutXPO.LocationTypeFrom,
                                        StockTypeFrom = _locBegInventoryLine.StockType,
                                        WorkplaceFrom = _locTransferOutXPO.WorkplaceFrom,
                                        LocationFrom = _locTransferOutXPO.LocationFrom,
                                        StockGroupFrom = _locBegInventoryLine.StockGroup,
                                        BegInvFrom = _locTransferOutXPO.BegInvFrom,
                                        LocationTypeTo = _locTransferOutXPO.LocationTypeTo,
                                        StockTypeTo = _locTransferOutXPO.StockTypeTo,
                                        WorkplaceTo = _locTransferOutXPO.WorkplaceTo,
                                        LocationTo = _locTransferOutXPO.LocationTo,
                                        StockGroupTo = _locBegInventoryLine.StockGroup,
                                        BegInvTo = _locTransferOutXPO.BegInvTo,
                                        Item = _locBegInventoryLine.Item,
                                        Description = _locBegInventoryLine.Description,
                                        BegInvLineFrom = _locBegInventoryLine,
                                        DQty = _locDQty,
                                        DUOM = _locBegInventoryLine.DefaultUOM,
                                        Qty = _locQty,
                                        UOM = _locUOM,
                                        TQty = _locTQty,
                                        
                                    };
                                    _saveDataTransferOutLine.Save();
                                    _saveDataTransferOutLine.Session.CommitTransaction();
                                    #endregion TransferOutLine
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = TransferOut ", ex.ToString());
            }
        }

        #endregion GetBegInv

        #endregion GetStock

        #region Progress

        private void SetBegInvLineTo(Session _currSession, TransferOut _locTransferOutXPO)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                string _locSignCode = null;

                if (_locTransferOutXPO != null)
                {
                    XPCollection<TransferOutLine> _locTransferOutLines = new XPCollection<TransferOutLine>(_currSession,
                                                                        new GroupOperator(GroupOperatorType.And,
                                                                        new BinaryOperator("TransferOut", _locTransferOutXPO),
                                                                        new GroupOperator(GroupOperatorType.Or,
                                                                        new BinaryOperator("Status", Status.Open),
                                                                        new BinaryOperator("Status", Status.Progress))));

                    if (_locTransferOutLines != null && _locTransferOutLines.Count() > 0)
                    {
                        foreach (TransferOutLine _locTransferOutLine in _locTransferOutLines)
                        {
                            
                            if (_locTransferOutLine.Company != null && _locTransferOutLine.WorkplaceTo != null && _locTransferOutLine.BegInvTo != null 
                                && _locTransferOutLine.Item != null && _locTransferOutLine.BegInvLineTo == null)
                            {
                                BeginningInventoryLine _locBegInvLine = null;

                                if (_locTransferOutLine.DUOM != null)
                                {
                                    #region Update
                                    if (_locTransferOutLine.BinLocationTo != null)
                                    {
                                        //DefaultUOM
                                        _locBegInvLine = _currSession.FindObject<BeginningInventoryLine>(
                                                   new GroupOperator(GroupOperatorType.And,
                                                   new BinaryOperator("Company", _locTransferOutLine.Company),
                                                   new BinaryOperator("Workplace", _locTransferOutLine.WorkplaceTo),
                                                   new BinaryOperator("BeginningInventory", _locTransferOutLine.BegInvTo),
                                                   new BinaryOperator("Item", _locTransferOutLine.Item),
                                                   new BinaryOperator("DefaultUOM", _locTransferOutLine.DUOM),
                                                   new BinaryOperator("BinLocation", _locTransferOutLine.BinLocationTo),
                                                   new BinaryOperator("LocationType", _locTransferOutLine.LocationTypeTo),
                                                   new BinaryOperator("StockType", _locTransferOutLine.StockTypeTo),
                                                   new BinaryOperator("StockGroup", _locTransferOutLine.StockGroupTo),
                                                   new BinaryOperator("Lock", false),
                                                   new BinaryOperator("Active", true)));
                                    }
                                    else
                                    {
                                        _locBegInvLine = _currSession.FindObject<BeginningInventoryLine>(
                                                   new GroupOperator(GroupOperatorType.And,
                                                   new BinaryOperator("Company", _locTransferOutLine.Company),
                                                   new BinaryOperator("Workplace", _locTransferOutLine.WorkplaceTo),
                                                   new BinaryOperator("BeginningInventory", _locTransferOutLine.BegInvTo),
                                                   new BinaryOperator("Item", _locTransferOutLine.Item),
                                                   new BinaryOperator("DefaultUOM", _locTransferOutLine.DUOM),
                                                   new BinaryOperator("LocationType", _locTransferOutLine.LocationTypeTo),
                                                   new BinaryOperator("StockType", _locTransferOutLine.StockTypeTo),
                                                   new BinaryOperator("StockGroup", _locTransferOutLine.StockGroupTo),
                                                   new BinaryOperator("Lock", false),
                                                   new BinaryOperator("Active", true)));
                                    }

                                    if (_locBegInvLine != null)
                                    {
                                        _locTransferOutLine.BegInvLineTo = _locBegInvLine;
                                        _locTransferOutLine.Save();
                                        _locTransferOutLine.Session.CommitTransaction();
                                    }
                                    #endregion Update
                                    #region CreateNew
                                    else
                                    {
                                        _locSignCode = _globFunc.GetNumberingSignUnlockOptimisticRecordBasedOrganization(_currSession.DataLayer, ObjectList.BeginningInventoryLine, _locTransferOutLine.Company, _locTransferOutLine.WorkplaceTo);
                                        if (_locSignCode != null)
                                        {
                                            BeginningInventoryLine _locSaveDataBeginingInventoryLine = new BeginningInventoryLine(_currSession)
                                            {
                                                SignCode = _locSignCode,
                                                Company = _locTransferOutXPO.Company,
                                                Workplace = _locTransferOutXPO.WorkplaceTo,
                                                Item = _locTransferOutLine.Item,
                                                Location = _locTransferOutLine.LocationTo,
                                                Vehicle = _locTransferOutXPO.Vehicle,
                                                BinLocation = _locTransferOutLine.BinLocationTo,
                                                DefaultUOM = _locTransferOutLine.DUOM,
                                                LocationType = _locTransferOutLine.LocationTypeTo,
                                                StockType = _locTransferOutLine.StockTypeTo,
                                                StockGroup = _locTransferOutLine.StockGroupTo,
                                                Lock = false,
                                                Active = true,
                                                BeginningInventory = _locTransferOutLine.BegInvTo,
                                            };

                                            _locSaveDataBeginingInventoryLine.Save();
                                            _locSaveDataBeginingInventoryLine.Session.CommitTransaction();

                                            BeginningInventoryLine _locBegInvLine2 = _currSession.FindObject<BeginningInventoryLine>(new GroupOperator
                                                                                    (GroupOperatorType.And,
                                                                                    new BinaryOperator("SignCode", _locSignCode)));
                                            if (_locBegInvLine2 != null)
                                            {
                                                _locTransferOutLine.BegInvLineTo = _locBegInvLine2;
                                                _locTransferOutLine.Save();
                                                _locTransferOutLine.Session.CommitTransaction();
                                            }
                                        }
                                    }
                                    #endregion CreateNew
                                }
                                
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Module = TransferOut " + ex.ToString());
            }
        }

        #endregion Progress

        #region Posting

        private bool CheckLockStock(Session _currSession, TransferOut _locTransferOutXPO)
        {
            bool _result = false;
            try
            {
                bool _locLock = false;

                if (_locTransferOutXPO != null)
                {
                    XPCollection<TransferOutLine> _locTransferOutLines = new XPCollection<TransferOutLine>(_currSession,
                                                                        new GroupOperator(GroupOperatorType.And,
                                                                        new BinaryOperator("TransferIn", _locTransferOutXPO),
                                                                        new GroupOperator(GroupOperatorType.Or,
                                                                        new BinaryOperator("Status", Status.Progress),
                                                                        new BinaryOperator("Status", Status.Posted))));
                    if (_locTransferOutLines != null && _locTransferOutLines.Count() > 0)
                    {
                        foreach (TransferOutLine _locTransferOutLine in _locTransferOutLines)
                        {
                            if (_locTransferOutLine.BegInvLineFrom != null)
                            {
                                if (_locTransferOutLine.BegInvLineFrom.Lock == true)
                                {
                                    _locLock = true;
                                }
                            }
                        }

                        _result = _locLock;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Module = TransferOut " + ex.ToString());
            }
            return _result;
        }

        private bool CheckAvailableStock(Session _currSession, TransferOut _locTransferOutXPO)
        {
            bool _result = true;
            try
            {
                if (_locTransferOutXPO != null)
                {
                    XPCollection<TransferOutLine> _locTransOutLines = new XPCollection<TransferOutLine>(_currSession,
                                                                        new GroupOperator(GroupOperatorType.And,
                                                                        new BinaryOperator("TransferOut", _locTransferOutXPO),
                                                                        new BinaryOperator("Select", true),
                                                                        new GroupOperator(GroupOperatorType.Or,
                                                                        new BinaryOperator("Status", Status.Progress),
                                                                        new BinaryOperator("Status", Status.Posted))));

                    if (_locTransOutLines != null && _locTransOutLines.Count > 0)
                    {

                        foreach (TransferOutLine _locTransOutLine in _locTransOutLines)
                        {
                            XPCollection<TransferOutLot> _locTransferOutLots = new XPCollection<TransferOutLot>
                                                                                            (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                            new BinaryOperator("TransferOutLine", _locTransOutLine),
                                                                                            new BinaryOperator("Select", true),
                                                                                            new GroupOperator(GroupOperatorType.Or,
                                                                                            new BinaryOperator("Status", Status.Progress),
                                                                                            new BinaryOperator("Status", Status.Posted))));

                            #region LotNumber
                            if (_locTransferOutLots != null && _locTransferOutLots.Count() > 0)
                            {
                                foreach (TransferOutLot _locTransferOutLot in _locTransferOutLots)
                                {
                                    BeginningInventory _locBegInventory = _currSession.FindObject<BeginningInventory>
                                                        (new GroupOperator(GroupOperatorType.And,
                                                        new BinaryOperator("Item", _locTransferOutLot.Item)));
                                    if (_locBegInventory != null)
                                    {
                                        if (_locTransferOutLot.BegInvLine != null)
                                        {
                                            if (_locTransferOutLot.BegInvLine.Code != null)
                                            {
                                                BeginningInventoryLine _locBegInvLine = _currSession.FindObject<BeginningInventoryLine>(
                                                                                            new GroupOperator(GroupOperatorType.And,
                                                                                            new BinaryOperator("Code", _locTransferOutLot.BegInvLine.Code)));
                                                if (_locBegInvLine != null)
                                                {
                                                    if (_locTransferOutLot.TQty > _locBegInvLine.QtyAvailable)
                                                    {
                                                        _result = false;
                                                    }
                                                }
                                                else
                                                {
                                                    _result = false;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            #endregion LotNumber
                            #region NonLotNumber
                            else
                            {
                                if (_locTransOutLine.BegInvFrom != null)
                                {
                                    if (_locTransOutLine.BegInvLineFrom != null)
                                    {
                                        if (_locTransOutLine.TQty > _locTransOutLine.BegInvLineFrom.QtyAvailable)
                                        {
                                            _result = false;
                                        }
                                    }
                                }
                            }
                            #endregion NonLotNumber
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = TransferOut ", ex.ToString());
            }
            return _result;
        }

        private void SetDeliverBeginingInventory(Session _currSession, TransferOut _locTransferOutXPO)
        {
            try
            {
                XPCollection<TransferOutLine> _locTransferOutLines = new XPCollection<TransferOutLine>(_currSession,
                                                                        new GroupOperator(GroupOperatorType.And,
                                                                        new BinaryOperator("TransferOut", _locTransferOutXPO),
                                                                        new BinaryOperator("Select", true),
                                                                        new GroupOperator(GroupOperatorType.Or,
                                                                        new BinaryOperator("Status", Status.Progress),
                                                                        new BinaryOperator("Status", Status.Posted))));

                if (_locTransferOutLines != null && _locTransferOutLines.Count() > 0)
                {
                    GlobalFunction _globFunc = new GlobalFunction();

                    foreach (TransferOutLine _locTransferOutLine in _locTransferOutLines)
                    {
                        if (_locTransferOutLine.DQty > 0 || _locTransferOutLine.Qty > 0)
                        {
                            XPCollection<TransferOutLot> _locTransferOutLots = new XPCollection<TransferOutLot>
                                                                                    (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                    new BinaryOperator("TransferOutLine", _locTransferOutLine),
                                                                                    new BinaryOperator("Select", true),
                                                                                    new GroupOperator(GroupOperatorType.Or,
                                                                                    new BinaryOperator("Status", Status.Progress),
                                                                                    new BinaryOperator("Status", Status.Posted))));
                            #region LotNumber
                            if (_locTransferOutLots != null && _locTransferOutLots.Count() > 0)
                            {
                                foreach (TransferOutLot _locTransferOutLot in _locTransferOutLots)
                                {
                                    if (_locTransferOutLot.Select == true)
                                    {
                                        if (_locTransferOutLot.BegInvLine != null)
                                        {
                                            if (_locTransferOutLot.BegInvLine.Code != null)
                                            {
                                                BeginningInventoryLine _locBegInvLine = _currSession.FindObject<BeginningInventoryLine>
                                                                                (new GroupOperator(GroupOperatorType.And,
                                                                                 new BinaryOperator("Code", _locTransferOutLot.BegInvLine.Code)));
                                                if (_locBegInvLine != null)
                                                {
                                                    if (_locBegInvLine.QtyAvailable >= _locTransferOutLot.TQty)
                                                    {
                                                        _locBegInvLine.QtyAvailable = _locBegInvLine.QtyAvailable - _locTransferOutLot.TQty;
                                                        _locBegInvLine.Save();
                                                        _locBegInvLine.Session.CommitTransaction();
                                                    }

                                                    if (_locBegInvLine.BeginningInventory != null)
                                                    {
                                                        if (_locBegInvLine.BeginningInventory.Code != null)
                                                        {
                                                            BeginningInventory _locBegInv = _currSession.FindObject<BeginningInventory>
                                                                                (new GroupOperator(GroupOperatorType.And,
                                                                                 new BinaryOperator("Code", _locBegInvLine.BeginningInventory.Code)));
                                                            if (_locBegInv != null)
                                                            {
                                                                _locBegInv.QtyAvailable = _locBegInv.QtyAvailable - _locTransferOutLot.TQty;
                                                                _locBegInv.QtySale = _locBegInv.QtySale + _locTransferOutLot.TQty;
                                                                _locBegInv.Save();
                                                                _locBegInv.Session.CommitTransaction();
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            #endregion LotNumber

                            #region NonLotNumber
                            else
                            {
                                #region BeginningInventoryFrom
                                if (_locTransferOutLine.BegInvFrom != null && _locTransferOutLine.BegInvLineFrom != null)
                                {
                                    _locTransferOutLine.BegInvLineFrom.QtyAvailable = _locTransferOutLine.BegInvLineFrom.QtyAvailable - _locTransferOutLine.TQty;
                                    _locTransferOutLine.BegInvLineFrom.Save();
                                    _locTransferOutLine.BegInvLineFrom.Session.CommitTransaction();

                                    _locTransferOutLine.BegInvFrom.QtyAvailable = _locTransferOutLine.BegInvFrom.QtyAvailable - _locTransferOutLine.TQty;
                                    _locTransferOutLine.BegInvFrom.Save();
                                    _locTransferOutLine.BegInvFrom.Session.CommitTransaction();

                                }
                                #endregion BeginningInventoryFrom
                                #region BeginningInventoryTo
                                if (_locTransferOutLine.Company != null && _locTransferOutLine.WorkplaceTo != null && _locTransferOutLine.BegInvTo != null && _locTransferOutLine.Item != null)
                                {
                                    if (_locTransferOutLine.LocationTypeTo == LocationType.Transit )
                                    {
                                        #region Update
                                        if (_locTransferOutLine.BegInvLineTo != null)
                                        {
                                            _locTransferOutLine.BegInvLineTo.QtyAvailable = _locTransferOutLine.BegInvLineTo.QtyAvailable + _locTransferOutLine.TQty;
                                            _locTransferOutLine.BegInvLineTo.Save();
                                            _locTransferOutLine.BegInvLineTo.Session.CommitTransaction();

                                            _locTransferOutLine.BegInvTo.QtyAvailable = _locTransferOutLine.BegInvTo.QtyAvailable + _locTransferOutLine.TQty;
                                            _locTransferOutLine.BegInvTo.Save();
                                            _locTransferOutLine.BegInvTo.Session.CommitTransaction();
                                        }
                                        #endregion Update
                                    }
                                }

                                #endregion BeginningInventoryTo
                            }
                            #endregion NonLotNumber

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = TransferOut ", ex.ToString());
            }
        }

        private void SetDeliverInventoryJournal(Session _currSession, TransferOut _locTransferOutXPO)
        {
            try
            {
                XPCollection<TransferOutLine> _locTransferOutLines = new XPCollection<TransferOutLine>(_currSession,
                                                                        new GroupOperator(GroupOperatorType.And,
                                                                        new BinaryOperator("TransferOut", _locTransferOutXPO),
                                                                        new BinaryOperator("Select", true),
                                                                        new GroupOperator(GroupOperatorType.Or,
                                                                        new BinaryOperator("Status", Status.Progress),
                                                                        new BinaryOperator("Status", Status.Posted))));

                if (_locTransferOutLines != null && _locTransferOutLines.Count > 0)
                {
                    double _locInvLineTotal = 0;
                    DateTime now = DateTime.Now;

                    foreach (TransferOutLine _locTransferOutLine in _locTransferOutLines)
                    {
                        if (_locTransferOutLine.DQty > 0 || _locTransferOutLine.Qty > 0)
                        {
                            XPCollection<TransferOutLot> _locTransferOutLots = new XPCollection<TransferOutLot>
                                                                                    (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                    new BinaryOperator("TransferOutLine", _locTransferOutLine),
                                                                                    new BinaryOperator("Select", true),
                                                                                    new GroupOperator(GroupOperatorType.Or,
                                                                                    new BinaryOperator("Status", Status.Progress),
                                                                                    new BinaryOperator("Status", Status.Posted))));

                            #region LotNumber
                            if (_locTransferOutLots != null && _locTransferOutLots.Count() > 0)
                            {
                                foreach (TransferOutLot _locTransferOutLot in _locTransferOutLots)
                                {
                                    if (_locTransferOutLot.Select == true)
                                    {
                                        if (_locTransferOutLot.BegInvLine != null)
                                        {
                                            if (_locTransferOutLot.BegInvLine.Code != null)
                                            {
                                                BeginningInventoryLine _locBegInvLine = _currSession.FindObject<BeginningInventoryLine>
                                                                                (new GroupOperator(GroupOperatorType.And,
                                                                                 new BinaryOperator("Code", _locTransferOutLot.BegInvLine.Code)));
                                                if (_locBegInvLine != null)
                                                {
                                                    InventoryJournal _locNegatifInventoryJournal = new InventoryJournal(_currSession)
                                                    {
                                                        DocumentType = _locTransferOutXPO.DocumentType,
                                                        DocNo = _locTransferOutXPO.DocNo,
                                                        Location = _locTransferOutLot.LocationFrom,
                                                        BinLocation = _locTransferOutLot.BinLocationFrom,
                                                        StockType = _locTransferOutLot.StockTypeFrom,
                                                        Item = _locTransferOutLot.Item,
                                                        Company = _locTransferOutLot.Company,
                                                        QtyPos = 0,
                                                        QtyNeg = _locTransferOutLot.TQty,
                                                        DUOM = _locTransferOutLot.DUOM,
                                                        JournalDate = now,
                                                        LotNumber = _locTransferOutLot.LotNumber,
                                                        TransferOut = _locTransferOutXPO,
                                                    };
                                                    _locNegatifInventoryJournal.Save();
                                                    _locNegatifInventoryJournal.Session.CommitTransaction();
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            #endregion LotNumber
                            #region NonLotNumber
                            else
                            {
                                #region Negatif
                                if (_locTransferOutLine.Item != null && _locTransferOutLine.UOM != null && _locTransferOutLine.DUOM != null)
                                {
                                    ItemUnitOfMeasure _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                        (new GroupOperator(GroupOperatorType.And,
                                                         new BinaryOperator("Item", _locTransferOutLine.Item),
                                                         new BinaryOperator("UOM", _locTransferOutLine.UOM),
                                                         new BinaryOperator("DefaultUOM", _locTransferOutLine.DUOM),
                                                         new BinaryOperator("Active", true)));
                                    if (_locItemUOM != null)
                                    {
                                        #region Code
                                        if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locTransferOutLine.Qty * _locItemUOM.DefaultConversion + _locTransferOutLine.DQty;
                                        }
                                        else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locTransferOutLine.Qty / _locItemUOM.Conversion + _locTransferOutLine.DQty;
                                        }
                                        else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locTransferOutLine.Qty + _locTransferOutLine.DQty;
                                        }
                                        #endregion Code

                                        InventoryJournal _locNegatifInventoryJournal = new InventoryJournal(_currSession)
                                        {
                                            DocumentType = _locTransferOutXPO.DocumentType,
                                            DocNo = _locTransferOutXPO.DocNo,
                                            Location = _locTransferOutLine.LocationFrom,
                                            BinLocation = _locTransferOutLine.BinLocationFrom,
                                            LocationType = _locTransferOutLine.LocationTypeFrom,
                                            StockType = _locTransferOutLine.StockTypeFrom,
                                            StockGroup = _locTransferOutLine.StockGroupFrom,
                                            Item = _locTransferOutLine.Item,
                                            Company = _locTransferOutLine.Company,
                                            Workplace = _locTransferOutLine.WorkplaceFrom,
                                            QtyPos = 0,
                                            QtyNeg = _locInvLineTotal,
                                            DUOM = _locTransferOutLine.DUOM,
                                            JournalDate = now,
                                            TransferOut = _locTransferOutXPO,
                                        };
                                        _locNegatifInventoryJournal.Save();
                                        _locNegatifInventoryJournal.Session.CommitTransaction();

                                    }
                                }
                                else
                                {
                                    _locInvLineTotal = _locTransferOutLine.Qty + _locTransferOutLine.DQty;

                                    InventoryJournal _locNegatifInventoryJournal = new InventoryJournal(_currSession)
                                    {
                                        DocumentType = _locTransferOutXPO.DocumentType,
                                        DocNo = _locTransferOutXPO.DocNo,
                                        Location = _locTransferOutLine.LocationFrom,
                                        BinLocation = _locTransferOutLine.BinLocationFrom,
                                        LocationType = _locTransferOutLine.LocationTypeFrom,
                                        StockType = _locTransferOutLine.StockTypeFrom,
                                        StockGroup = _locTransferOutLine.StockGroupFrom,
                                        Item = _locTransferOutLine.Item,
                                        Company = _locTransferOutLine.Company,
                                        Workplace = _locTransferOutLine.WorkplaceFrom,
                                        QtyPos = 0,
                                        QtyNeg = _locInvLineTotal,
                                        DUOM = _locTransferOutLine.DUOM,
                                        JournalDate = now,
                                        TransferOut = _locTransferOutXPO,
                                    };
                                    _locNegatifInventoryJournal.Save();
                                    _locNegatifInventoryJournal.Session.CommitTransaction();
                                }
                                #endregion Negatif
                                #region Positif
                                if (_locTransferOutLine.LocationTypeTo == LocationType.Transit)
                                {
                                    if (_locTransferOutLine.Item != null && _locTransferOutLine.UOM != null && _locTransferOutLine.DUOM != null)
                                    {
                                        ItemUnitOfMeasure _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                            (new GroupOperator(GroupOperatorType.And,
                                                             new BinaryOperator("Item", _locTransferOutLine.Item),
                                                             new BinaryOperator("UOM", _locTransferOutLine.UOM),
                                                             new BinaryOperator("DefaultUOM", _locTransferOutLine.DUOM),
                                                             new BinaryOperator("Active", true)));
                                        if (_locItemUOM != null)
                                        {
                                            #region Code
                                            if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                            {
                                                _locInvLineTotal = _locTransferOutLine.Qty * _locItemUOM.DefaultConversion + _locTransferOutLine.DQty;
                                            }
                                            else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                            {
                                                _locInvLineTotal = _locTransferOutLine.Qty / _locItemUOM.Conversion + _locTransferOutLine.DQty;
                                            }
                                            else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                            {
                                                _locInvLineTotal = _locTransferOutLine.Qty + _locTransferOutLine.DQty;
                                            }
                                            #endregion Code

                                            InventoryJournal _locPositifInventoryJournal = new InventoryJournal(_currSession)
                                            {
                                                DocumentType = _locTransferOutXPO.DocumentType,
                                                DocNo = _locTransferOutXPO.DocNo,
                                                Location = _locTransferOutLine.LocationTo,
                                                BinLocation = _locTransferOutLine.BinLocationTo,
                                                LocationType = _locTransferOutLine.LocationTypeTo,
                                                StockType = _locTransferOutLine.StockTypeTo,
                                                StockGroup = _locTransferOutLine.StockGroupTo,
                                                Item = _locTransferOutLine.Item,
                                                Company = _locTransferOutLine.Company,
                                                Workplace = _locTransferOutLine.WorkplaceTo,
                                                QtyPos = _locInvLineTotal,
                                                QtyNeg = 0,
                                                DUOM = _locTransferOutLine.DUOM,
                                                JournalDate = now,
                                                TransferOut = _locTransferOutXPO,
                                            };
                                            _locPositifInventoryJournal.Save();
                                            _locPositifInventoryJournal.Session.CommitTransaction();

                                        }
                                    }
                                    else
                                    {
                                        _locInvLineTotal = _locTransferOutLine.Qty + _locTransferOutLine.DQty;

                                        InventoryJournal _locPositifInventoryJournal = new InventoryJournal(_currSession)
                                        {
                                            DocumentType = _locTransferOutXPO.DocumentType,
                                            DocNo = _locTransferOutXPO.DocNo,
                                            Location = _locTransferOutLine.LocationTo,
                                            BinLocation = _locTransferOutLine.BinLocationTo,
                                            LocationType = _locTransferOutLine.LocationTypeTo,
                                            StockType = _locTransferOutLine.StockTypeTo,
                                            StockGroup = _locTransferOutLine.StockGroupTo,
                                            Item = _locTransferOutLine.Item,
                                            Company = _locTransferOutLine.Company,
                                            Workplace = _locTransferOutLine.WorkplaceTo,
                                            QtyPos = _locInvLineTotal,
                                            QtyNeg = 0,
                                            DUOM = _locTransferOutLine.DUOM,
                                            JournalDate = now,
                                            TransferOut = _locTransferOutXPO,
                                        };
                                        _locPositifInventoryJournal.Save();
                                        _locPositifInventoryJournal.Session.CommitTransaction();
                                    }
                                }
                                #endregion Positif
                            }
                            #endregion NonLotNumber
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = TransferOut ", ex.ToString());
            }
        }

        private void SetTransferOutMonitoring(Session _currSession, TransferOut _locTransferOutXPO)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                DateTime now = DateTime.Now;
                PickingLine _locPickingLine = null;
                Employee _locPIC = null;

                if (_locTransferOutXPO != null)
                {
                    if (_locTransferOutXPO.Status == Status.Progress || _locTransferOutXPO.Status == Status.Posted)
                    {
                        XPCollection<TransferOutLine> _locTransferOutLines = new XPCollection<TransferOutLine>
                                                            (_currSession, new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("TransferOut", _locTransferOutXPO),
                                                            new BinaryOperator("Select", true),
                                                            new GroupOperator(GroupOperatorType.Or,
                                                            new BinaryOperator("Status", Status.Progress),
                                                            new BinaryOperator("Status", Status.Posted))));

                        if (_locTransferOutLines != null && _locTransferOutLines.Count() > 0)
                        {
                            foreach (TransferOutLine _locTransferOutLine in _locTransferOutLines)
                            {
                                if (_locTransferOutLine.SalesInvoiceMonitoring != null)
                                {
                                    if (_locTransferOutLine.SalesInvoiceMonitoring.PickingLine != null)
                                    {
                                        _locPickingLine = _locTransferOutLine.SalesInvoiceMonitoring.PickingLine;
                                    }
                                }

                                if (_locTransferOutLine.SalesInvoice != null)
                                {
                                    _locPIC = _locTransferOutLine.SalesInvoice.PIC;
                                }

                                TransferOutMonitoring _saveDataTransferOutMonitoring = new TransferOutMonitoring(_currSession)
                                {
                                    TransferOut = _locTransferOutXPO,
                                    TransferOutLine = _locTransferOutLine,
                                    #region Organization
                                    Company = _locTransferOutLine.Company,
                                    Workplace = _locTransferOutLine.Workplace,
                                    Division = _locTransferOutLine.Division,
                                    Department = _locTransferOutLine.Department,
                                    Section = _locTransferOutLine.Section,
                                    Employee = _locTransferOutLine.Employee,
                                    #endregion Organization
                                    #region From
                                    LocationTypeFrom = _locTransferOutLine.LocationTypeFrom,
                                    StockTypeFrom = _locTransferOutLine.StockTypeFrom,
                                    WorkplaceFrom = _locTransferOutLine.WorkplaceFrom,
                                    LocationFrom = _locTransferOutLine.LocationFrom,
                                    BinLocationFrom = _locTransferOutLine.BinLocationFrom,
                                    StockGroupFrom = _locTransferOutLine.StockGroupFrom,
                                    BegInvFrom = _locTransferOutLine.BegInvFrom,
                                    BegInvLineFrom = _locTransferOutLine.BegInvLineFrom,
                                    #endregion From
                                    #region To
                                    LocationTypeTo = _locTransferOutLine.LocationTypeTo,
                                    StockTypeTo = _locTransferOutLine.StockTypeTo,
                                    WorkplaceTo = _locTransferOutLine.WorkplaceTo,
                                    LocationTo = _locTransferOutLine.LocationTo,
                                    BinLocationTo = _locTransferOutLine.BinLocationTo,
                                    StockGroupTo = _locTransferOutLine.StockGroupTo,
                                    BegInvTo = _locTransferOutLine.BegInvTo,
                                    BegInvLineTo = _locTransferOutLine.BegInvLineTo,
                                    #endregion To
                                    DocumentRule = _locTransferOutXPO.DocumentRule,
                                    Picking = _locTransferOutXPO.Picking,
                                    PickingDate = _locTransferOutXPO.PickingDate,
                                    PickingLine = _locPickingLine,
                                    PIC = _locPIC,
                                    Vehicle = _locTransferOutXPO.Vehicle,
                                    #region Item
                                    Item = _locTransferOutLine.Item,
                                    Brand = _locTransferOutLine.Brand,
                                    Description = _locTransferOutLine.Description,
                                    MxDQty = _locTransferOutLine.DQty,
                                    MxDUOM = _locTransferOutLine.DUOM,
                                    MxQty = _locTransferOutLine.Qty,
                                    MxUOM = _locTransferOutLine.UOM,
                                    MxTQty = _locTransferOutLine.TQty,
                                    DQty = _locTransferOutLine.DQty,
                                    DUOM = _locTransferOutLine.DUOM,
                                    Qty = _locTransferOutLine.Qty,
                                    UOM = _locTransferOutLine.UOM,
                                    TQty = _locTransferOutLine.TQty,
                                    #endregion Item
                                    ETD = _locTransferOutLine.ETD,
                                    ETA = _locTransferOutLine.ETA,
                                    DocDate = _locTransferOutLine.DocDate,
                                    JournalMonth = _locTransferOutLine.JournalMonth,
                                    JournalYear = _locTransferOutLine.JournalYear,
                                    SalesInvoiceMonitoring = _locTransferOutLine.SalesInvoiceMonitoring,
                                    SalesInvoice = _locTransferOutLine.SalesInvoice,
                                };
                                _saveDataTransferOutMonitoring.Save();
                                _saveDataTransferOutMonitoring.Session.CommitTransaction();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = TransferOut ", ex.ToString());
            }
        }

        private void SetStatusDeliverTransferOutLine(Session _currSession, TransferOut _locTransferOutXPO)
        {
            try
            {
                DateTime now = DateTime.Now;
                if (_locTransferOutXPO != null)
                {
                    XPCollection<TransferOutLine> _locTransferOutLines = new XPCollection<TransferOutLine>(_currSession,
                                                            new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("TransferOut", _locTransferOutXPO),
                                                            new BinaryOperator("Select", true),
                                                            new GroupOperator(GroupOperatorType.Or,
                                                            new BinaryOperator("Status", Status.Progress),
                                                            new BinaryOperator("Status", Status.Posted))));

                    if (_locTransferOutLines != null && _locTransferOutLines.Count > 0)
                    {
                        foreach (TransferOutLine _locTransferOutLine in _locTransferOutLines)
                        {
                            _locTransferOutLine.Status = Status.Close;
                            _locTransferOutLine.ActivationPosting = true;
                            _locTransferOutLine.StatusDate = now;
                            _locTransferOutLine.Save();
                            _locTransferOutLine.Session.CommitTransaction();

                            XPCollection<TransferOutLot> _locTransferOutLots = new XPCollection<TransferOutLot>(_currSession,
                                                                                     new GroupOperator(GroupOperatorType.And,
                                                                                     new BinaryOperator("TransferOutLine", _locTransferOutLine),
                                                                                     new BinaryOperator("Select", true),
                                                                                     new GroupOperator(GroupOperatorType.Or,
                                                                                     new BinaryOperator("Status", Status.Progress),
                                                                                     new BinaryOperator("Status", Status.Posted))));

                            if (_locTransferOutLots != null && _locTransferOutLots.Count() > 0)
                            {
                                foreach (TransferOutLot _locTransferOutLot in _locTransferOutLots)
                                {
                                    if (_locTransferOutLot.Status == Status.Progress || _locTransferOutLot.Status == Status.Posted)
                                    {
                                        _locTransferOutLot.Status = Status.Close;
                                        _locTransferOutLot.ActivationPosting = true;
                                        _locTransferOutLot.StatusDate = now;
                                        _locTransferOutLot.Save();
                                        _locTransferOutLot.Session.CommitTransaction();
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = TransferOut " + ex.ToString());
            }
        }

        private void SetNormalDeliverQuantity(Session _currSession, TransferOut _locTransferOutXPO)
        {
            try
            {
                if (_locTransferOutXPO != null)
                {
                    XPCollection<TransferOutLine> _locTransferOutLines = new XPCollection<TransferOutLine>(_currSession,
                                                                                     new GroupOperator(GroupOperatorType.And,
                                                                                     new BinaryOperator("TransferOut", _locTransferOutXPO),
                                                                                     new BinaryOperator("Select", true),
                                                                                     new GroupOperator(GroupOperatorType.Or,
                                                                                     new BinaryOperator("Status", Status.Progress),
                                                                                     new BinaryOperator("Status", Status.Posted),
                                                                                     new BinaryOperator("Status", Status.Close))));

                    if (_locTransferOutLines != null && _locTransferOutLines.Count > 0)
                    {
                        foreach (TransferOutLine _locTransferOutLine in _locTransferOutLines)
                        {
                            if (_locTransferOutLine.DQty > 0 || _locTransferOutLine.Qty > 0)
                            {
                                _locTransferOutLine.Select = false;
                                _locTransferOutLine.Save();
                                _locTransferOutLine.Session.CommitTransaction();

                                XPCollection<TransferOutLot> _locTransferOutLots = new XPCollection<TransferOutLot>(_currSession,
                                                                                     new GroupOperator(GroupOperatorType.And,
                                                                                     new BinaryOperator("TransferOutLine", _locTransferOutLine),
                                                                                     new BinaryOperator("Select", true),
                                                                                     new GroupOperator(GroupOperatorType.Or,
                                                                                     new BinaryOperator("Status", Status.Progress),
                                                                                     new BinaryOperator("Status", Status.Posted))));

                                if (_locTransferOutLots != null && _locTransferOutLots.Count() > 0)
                                {
                                    foreach (TransferOutLot _locTransferOutLot in _locTransferOutLots)
                                    {
                                        if (_locTransferOutLot.Status == Status.Progress || _locTransferOutLot.Status == Status.Posted)
                                        {
                                            _locTransferOutLot.Select = false;
                                            _locTransferOutLot.Save();
                                            _locTransferOutLot.Session.CommitTransaction();
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = TransferOut " + ex.ToString());
            }
        }

        private void SetFinalStatusDeliverTransferOut(Session _currSession, TransferOut _locTransferOutXPO)
        {
            try
            {
                DateTime now = DateTime.Now;
                int _locStatusCount = 0;
                int _locTransOutLineCount = 0;

                if (_locTransferOutXPO != null)
                {
                    XPCollection<TransferOutLine> _locTransferOutLines = new XPCollection<TransferOutLine>(_currSession,
                                                                                     new GroupOperator(GroupOperatorType.And,
                                                                                     new BinaryOperator("TransferOut", _locTransferOutXPO)));

                    if (_locTransferOutLines != null && _locTransferOutLines.Count() > 0)
                    {
                        _locTransOutLineCount = _locTransferOutLines.Count();

                        foreach (TransferOutLine _locTransferOutLine in _locTransferOutLines)
                        {

                            if (_locTransferOutLine.Status == Status.Close)
                            {
                                _locStatusCount = _locStatusCount + 1;
                            }
                        }

                        if (_locTransOutLineCount == _locStatusCount)
                        {
                            _locTransferOutXPO.ActivationPosting = true;
                            _locTransferOutXPO.Status = Status.Close;
                            _locTransferOutXPO.StatusDate = now;
                            _locTransferOutXPO.PostedCount = _locTransferOutXPO.PostedCount + 1;
                            _locTransferOutXPO.Save();
                            _locTransferOutXPO.Session.CommitTransaction();
                        }
                        else
                        {
                            _locTransferOutXPO.Status = Status.Posted;
                            _locTransferOutXPO.StatusDate = now;
                            _locTransferOutXPO.PostedCount = _locTransferOutXPO.PostedCount + 1;
                            _locTransferOutXPO.Save();
                            _locTransferOutXPO.Session.CommitTransaction();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = TransferOut " + ex.ToString());
            }
        }

        #endregion Posting

        #region Global Method

        private void SuccessMessageShow(string _locMessage)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 4000;
            options.Message = string.Format("{0}", _locMessage);
            options.Type = InformationType.Success;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Success";
            options.Win.Type = WinMessageType.Flyout;
            Application.ShowViewStrategy.ShowMessage(options);
        }

        private void ErrorMessageShow(string _locMessage)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 2000;
            options.Message = string.Format(_locMessage);
            options.Type = InformationType.Warning;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Error";
            options.Win.Type = WinMessageType.Alert;
            Application.ShowViewStrategy.ShowMessage(options);
        }

        private int GetStockType(StockType objectName)
        {
            int _result = 0;
            try
            {
                if (objectName == StockType.Good)
                {
                    _result = 1;
                }
                else if (objectName == StockType.Bad)
                {
                    _result = 2;
                }
                else
                {
                    _result = 0;
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Module = TransferOut " + ex.ToString());
            }
            return _result;
        }

        private int GetStockGroup(StockGroup objectName)
        {
            int _result = 0;
            try
            {
                if (objectName == StockGroup.Normal)
                {
                    _result = 1;
                }
                else if (objectName == StockGroup.FreeItem)
                {
                    _result = 2;
                }
                else
                {
                    _result = 0;
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Module = TransferOut " + ex.ToString());
            }
            return _result;
        }

        private int GetActive(bool objectName)
        {
            int _result = 0;
            try
            {
                if (objectName == true)
                {
                    _result = 1;
                }
                else
                {
                    _result = 0;
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Module = TransferOut " + ex.ToString());
            }
            return _result;
        }

        #endregion Global Method

        
    }
}
