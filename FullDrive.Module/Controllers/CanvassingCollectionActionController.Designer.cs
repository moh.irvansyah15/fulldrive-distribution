﻿namespace FullDrive.Module.Controllers
{
    partial class CanvassingCollectionActionController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.CanvassingCollectionListviewFilterSelectionAction = new DevExpress.ExpressApp.Actions.SingleChoiceAction(this.components);
            this.CanvassingCollectionGetICAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.CanvassingCollectionProgressAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.CanvassingCollectionPostingAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            // 
            // CanvassingCollectionListviewFilterSelectionAction
            // 
            this.CanvassingCollectionListviewFilterSelectionAction.Caption = "Canvassing Collection Listview Filter Selection Action Id";
            this.CanvassingCollectionListviewFilterSelectionAction.ConfirmationMessage = null;
            this.CanvassingCollectionListviewFilterSelectionAction.Id = "CanvassingCollectionListviewFilterSelectionActionId";
            this.CanvassingCollectionListviewFilterSelectionAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.CanvassingCollection);
            this.CanvassingCollectionListviewFilterSelectionAction.ToolTip = null;
            this.CanvassingCollectionListviewFilterSelectionAction.Execute += new DevExpress.ExpressApp.Actions.SingleChoiceActionExecuteEventHandler(this.CanvassingCollectionListviewFilterSelectionAction_Execute);
            // 
            // CanvassingCollectionGetICAction
            // 
            this.CanvassingCollectionGetICAction.Caption = "Get IC";
            this.CanvassingCollectionGetICAction.ConfirmationMessage = null;
            this.CanvassingCollectionGetICAction.Id = "CanvassingCollectionGetICActionId";
            this.CanvassingCollectionGetICAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.CanvassingCollection);
            this.CanvassingCollectionGetICAction.ToolTip = null;
            this.CanvassingCollectionGetICAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.CanvassingCollectionGetICAction_Execute);
            // 
            // CanvassingCollectionProgressAction
            // 
            this.CanvassingCollectionProgressAction.Caption = "Progress";
            this.CanvassingCollectionProgressAction.ConfirmationMessage = null;
            this.CanvassingCollectionProgressAction.Id = "CanvassingCollectionProgressActionId";
            this.CanvassingCollectionProgressAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.CanvassingCollection);
            this.CanvassingCollectionProgressAction.ToolTip = null;
            this.CanvassingCollectionProgressAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.CanvassingCollectionProgressAction_Execute);
            // 
            // CanvassingCollectionPostingAction
            // 
            this.CanvassingCollectionPostingAction.Caption = "Posting";
            this.CanvassingCollectionPostingAction.ConfirmationMessage = null;
            this.CanvassingCollectionPostingAction.Id = "CanvassingCollectionPostingActionId";
            this.CanvassingCollectionPostingAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.CanvassingCollection);
            this.CanvassingCollectionPostingAction.ToolTip = null;
            this.CanvassingCollectionPostingAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.CanvassingCollectionPostingAction_Execute);
            // 
            // CanvassingCollectionActionController
            // 
            this.Actions.Add(this.CanvassingCollectionListviewFilterSelectionAction);
            this.Actions.Add(this.CanvassingCollectionGetICAction);
            this.Actions.Add(this.CanvassingCollectionProgressAction);
            this.Actions.Add(this.CanvassingCollectionPostingAction);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SingleChoiceAction CanvassingCollectionListviewFilterSelectionAction;
        private DevExpress.ExpressApp.Actions.SimpleAction CanvassingCollectionGetICAction;
        private DevExpress.ExpressApp.Actions.SimpleAction CanvassingCollectionProgressAction;
        private DevExpress.ExpressApp.Actions.SimpleAction CanvassingCollectionPostingAction;
    }
}
