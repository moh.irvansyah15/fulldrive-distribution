﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;

using FullDrive.Module.BusinessObjects;
using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.Xpo;
using System.Collections;
using DevExpress.Xpo;

namespace FullDrive.Module.Controllers
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class SalesInvoiceLineActionController : ViewController
    {
        public SalesInvoiceLineActionController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.
        }
        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }
        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        private void SalesInvoiceLineSelectAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;

                if (_objectToProcess != null)
                {
                    foreach (Object obj in _objectToProcess)
                    {
                        SalesInvoiceLine _locSalesInvoiceLineOS = (SalesInvoiceLine)_objectSpace.GetObject(obj);

                        if (_locSalesInvoiceLineOS != null)
                        {
                            if (_locSalesInvoiceLineOS.Session != null)
                            {
                                _currSession = _locSalesInvoiceLineOS.Session;
                            }

                            if (_locSalesInvoiceLineOS.Code != null && _currSession != null)
                            {
                                _currObjectId = _locSalesInvoiceLineOS.Code;

                                XPCollection<SalesInvoiceLine> _locSalesInvoiceLines = new XPCollection<SalesInvoiceLine>
                                                                                                     (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                      new BinaryOperator("Code", _currObjectId),
                                                                                                      new GroupOperator(GroupOperatorType.Or,
                                                                                                      new BinaryOperator("Status", Status.Progress),
                                                                                                      new BinaryOperator("Status", Status.Open)
                                                                                                      )));

                                if (_locSalesInvoiceLines != null && _locSalesInvoiceLines.Count > 0)
                                {
                                    foreach (SalesInvoiceLine _locSalesInvoiceLine in _locSalesInvoiceLines)
                                    {
                                        _locSalesInvoiceLine.Select = true;
                                        _locSalesInvoiceLine.Save();
                                        _locSalesInvoiceLine.Session.CommitTransaction();
                                    }

                                    SuccessMessageShow("SalesInvoiceLine has been Select");
                                }
                                else
                                {
                                    ErrorMessageShow("Data SalesInvoiceLine Not Available");
                                }
                            }
                        }
                    }
                    if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                    {
                        _objectSpace.CommitChanges();
                        _objectSpace.Refresh();
                    }
                    if (View is ListView)
                    {
                        _objectSpace.CommitChanges();
                        View.ObjectSpace.Refresh();
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("BusinessObject = SalesInvoiceLine" + ex.ToString());
            }
        }

        private void SalesInvoiceLineUnselectAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;

                if (_objectToProcess != null)
                {
                    foreach (Object obj in _objectToProcess)
                    {
                        SalesInvoiceLine _locSalesInvoiceLineOS = (SalesInvoiceLine)_objectSpace.GetObject(obj);

                        if (_locSalesInvoiceLineOS != null)
                        {
                            if (_locSalesInvoiceLineOS.Session != null)
                            {
                                _currSession = _locSalesInvoiceLineOS.Session;
                            }

                            if (_locSalesInvoiceLineOS.Code != null && _currSession != null)
                            {
                                _currObjectId = _locSalesInvoiceLineOS.Code;

                                XPCollection<SalesInvoiceLine> _locSalesInvoiceLines = new XPCollection<SalesInvoiceLine>
                                                                                                     (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                      new BinaryOperator("Code", _currObjectId),
                                                                                                      new GroupOperator(GroupOperatorType.Or,
                                                                                                      new BinaryOperator("Status", Status.Progress),
                                                                                                      new BinaryOperator("Status", Status.Open)
                                                                                                      )));

                                if (_locSalesInvoiceLines != null && _locSalesInvoiceLines.Count > 0)
                                {
                                    foreach (SalesInvoiceLine _locSalesInvoiceLine in _locSalesInvoiceLines)
                                    {
                                        _locSalesInvoiceLine.Select = false;
                                        _locSalesInvoiceLine.Save();
                                        _locSalesInvoiceLine.Session.CommitTransaction();
                                    }

                                    SuccessMessageShow("SalesInvoiceLine has been Unselect");
                                }
                                else
                                {
                                    ErrorMessageShow("Data SalesInvoiceLine Not Available");
                                }
                            }
                        }
                    }
                    if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                    {
                        _objectSpace.CommitChanges();
                        _objectSpace.Refresh();
                    }
                    if (View is ListView)
                    {
                        _objectSpace.CommitChanges();
                        View.ObjectSpace.Refresh();
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("BusinessObject = SalesInvoiceLine" + ex.ToString());
            }
        }

        private void SalesInvoiceLineTaxAndDiscountAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;

                if (_objectToProcess != null)
                {
                    foreach (Object obj in _objectToProcess)
                    {
                        SalesInvoiceLine _locSalesInvoiceLineOS = (SalesInvoiceLine)_objectSpace.GetObject(obj);

                        if (_locSalesInvoiceLineOS != null)
                        {
                            if (_locSalesInvoiceLineOS.Session != null)
                            {
                                _currSession = _locSalesInvoiceLineOS.Session;
                            }

                            if (_locSalesInvoiceLineOS.Code != null && _currSession != null)
                            {
                                _currObjectId = _locSalesInvoiceLineOS.Code;

                                XPCollection<SalesInvoiceLine> _locSalesInvoiceLines = new XPCollection<SalesInvoiceLine>
                                                                                                     (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                      new BinaryOperator("Code", _currObjectId),
                                                                                                      new GroupOperator(GroupOperatorType.Or,
                                                                                                      new BinaryOperator("Status", Status.Progress),
                                                                                                      new BinaryOperator("Status", Status.Open)
                                                                                                      )));

                                if (_locSalesInvoiceLines != null && _locSalesInvoiceLines.Count > 0)
                                {
                                    foreach (SalesInvoiceLine _locSalesInvoiceLine in _locSalesInvoiceLines)
                                    {
                                        SetTaxBeforeDiscount(_currSession, _locSalesInvoiceLine);
                                        SetDiscountGross(_currSession, _locSalesInvoiceLine);
                                        SetTaxAfterDiscount(_currSession, _locSalesInvoiceLine);
                                        SetDiscountNet(_currSession, _locSalesInvoiceLine);
                                    }

                                    SuccessMessageShow("SalesInvoiceLine Line has been Processed");
                                }
                                else
                                {
                                    ErrorMessageShow("Data SalesInvoiceLine Line Not Available");
                                }
                            }
                        }
                    }
                    if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                    {
                        _objectSpace.CommitChanges();
                        _objectSpace.Refresh();
                    }
                    if (View is ListView)
                    {
                        _objectSpace.CommitChanges();
                        View.ObjectSpace.Refresh();
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("BusinessObject = SalesInvoiceLine " + ex.ToString());
            }
        }

        //======================================== Code In Here ========================================

        #region Tax

        private void SetTaxBeforeDiscount(Session _currSession, SalesInvoiceLine _locSalesInvoiceLineXPO)
        {
            try
            {
                double _locTxAmount = 0;
                double _locTAmount = 0;
                if (_locSalesInvoiceLineXPO != null)
                {
                    if (_locSalesInvoiceLineXPO.Tax != null && _locSalesInvoiceLineXPO.TaxChecked == false)
                    {
                        if (_locSalesInvoiceLineXPO.Tax.TaxRule == TaxRule.BeforeDiscount)
                        {
                            _locTxAmount = _locSalesInvoiceLineXPO.TxValue / 100 * _locSalesInvoiceLineXPO.TUAmount;
                            if (_locSalesInvoiceLineXPO.Tax.TaxNature == TaxNature.Increase)
                            {
                                _locTAmount = _locSalesInvoiceLineXPO.TAmount + _locTxAmount;
                            }
                            if (_locSalesInvoiceLineXPO.Tax.TaxNature == TaxNature.Decrease)
                            {
                                _locTAmount = _locSalesInvoiceLineXPO.TAmount - _locTxAmount;
                            }
                            _locSalesInvoiceLineXPO.TxAmount = _locTxAmount;
                            _locSalesInvoiceLineXPO.TAmount = _locTAmount;
                            _locSalesInvoiceLineXPO.TaxChecked = true;
                            _locSalesInvoiceLineXPO.Save();
                            _locSalesInvoiceLineXPO.Session.CommitTransaction();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("BusinessObject = SalesInvoiceLine" + ex.ToString());
            }
        }

        private void SetDiscountGross(Session _currSession, SalesInvoiceLine _locSalesInvoiceLineXPO)
        {
            try
            {
                if (_locSalesInvoiceLineXPO != null)
                {
                    if (_locSalesInvoiceLineXPO.DiscountRule != null)
                    {
                        double _locTUAmountDiscount = 0;
                        double _locTUAmountDiscount2 = 0;
                        AccountingPeriodicLine _locAcctPerLine = null;

                        #region Gross

                        if (_locSalesInvoiceLineXPO.DiscountRule.GrossAmountRule == true)
                        {
                            if (_locSalesInvoiceLineXPO.TUAmount > 0 && _locSalesInvoiceLineXPO.TUAmount >= _locSalesInvoiceLineXPO.DiscountRule.GrossTotalUAmount)
                            {
                                if (_locSalesInvoiceLineXPO.DiscountRule.DiscountType == DiscountType.Percentage)
                                {
                                    _locTUAmountDiscount = _locSalesInvoiceLineXPO.TUAmount * _locSalesInvoiceLineXPO.DiscountRule.Value / 100;
                                }
                                if (_locSalesInvoiceLineXPO.DiscountRule.DiscountType == DiscountType.Value)
                                {
                                    _locTUAmountDiscount = _locSalesInvoiceLineXPO.DiscountRule.Value;
                                }

                                #region ValBasedAccount
                                if (_locSalesInvoiceLineXPO.DiscountRule.ValBasedAccount == true)
                                {
                                    if(_locSalesInvoiceLineXPO.AccountingPeriodicLine != null)
                                    {
                                        _locAcctPerLine = _locSalesInvoiceLineXPO.AccountingPeriodicLine;
                                        if (_locAcctPerLine.BalHold < _locTUAmountDiscount)
                                        {
                                            _locTUAmountDiscount = _locAcctPerLine.BalHold;
                                        }
         
                                        _locAcctPerLine.BalHold = _locAcctPerLine.BalHold + _locTUAmountDiscount;
                                        _locAcctPerLine.Save();
                                        _locAcctPerLine.Session.CommitTransaction();
                                    }
                                    else
                                    {
                                        if (_locSalesInvoiceLineXPO.DiscountRule.AccountNo != null)
                                        {
                                            AccountingPeriodic _locAccountPeriodic = _currSession.FindObject<AccountingPeriodic>
                                                                            (new GroupOperator(GroupOperatorType.And,
                                                                            new BinaryOperator("Company", _locSalesInvoiceLineXPO.Company),
                                                                            new BinaryOperator("Workplace", _locSalesInvoiceLineXPO.Workplace),
                                                                            new BinaryOperator("Active", true)));
                                            if (_locAccountPeriodic != null)
                                            {
                                                AccountingPeriodicLine _locAccountingPeriodicLine = _currSession.FindObject<AccountingPeriodicLine>
                                                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                                                    new BinaryOperator("AccountingPeriodic", _locAccountPeriodic),
                                                                                                    new BinaryOperator("AccountNo", _locSalesInvoiceLineXPO.DiscountRule.AccountNo)));
                                                if (_locAccountingPeriodicLine != null)
                                                {
                                                    _locAcctPerLine = _locAccountingPeriodicLine;
                                                    if (_locAccountingPeriodicLine.BalHold < _locAccountingPeriodicLine.Balance)
                                                    {
                                                        _locTUAmountDiscount2 = _locAccountingPeriodicLine.Balance - _locAccountingPeriodicLine.BalHold;
                                                        if(_locTUAmountDiscount2 < _locTUAmountDiscount)
                                                        {
                                                            _locTUAmountDiscount = _locTUAmountDiscount2;
                                                        }
                                                        
                                                    }else
                                                    {
                                                        _locTUAmountDiscount = 0;
                                                    }
                                                    _locAcctPerLine = _locAccountingPeriodicLine;
                                                    _locAccountingPeriodicLine.BalHold = _locAccountingPeriodicLine.BalHold + _locTUAmountDiscount;
                                                    _locAccountingPeriodicLine.Save();
                                                    _locAccountingPeriodicLine.Session.CommitTransaction();
                                                }
                                            }
                                        }
                                    }
                                    

                                }
                                #endregion ValBasedAccount

                                _locSalesInvoiceLineXPO.AccountingPeriodicLine = _locAcctPerLine;
                                _locSalesInvoiceLineXPO.DiscountChecked = true;
                                _locSalesInvoiceLineXPO.DiscAmount = _locTUAmountDiscount;
                                _locSalesInvoiceLineXPO.TAmount = _locSalesInvoiceLineXPO.TAmount - _locTUAmountDiscount;
                                _locSalesInvoiceLineXPO.Save();
                                _locSalesInvoiceLineXPO.Session.CommitTransaction();
                            }

                        }

                        #endregion Gross
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("BusinessObject = SalesInvoiceLine" + ex.ToString());
            }
        }

        private void SetTaxAfterDiscount(Session _currSession, SalesInvoiceLine _locSalesInvoiceLineXPO)
        {
            try
            {
                double _locTxAmount = 0;
                double _locTAmount = 0;
                if (_locSalesInvoiceLineXPO != null)
                {
                    if (_locSalesInvoiceLineXPO.Tax != null )
                    {
                        if(_locSalesInvoiceLineXPO.DiscountRule != null)
                        {
                            if (_locSalesInvoiceLineXPO.Tax.TaxRule == TaxRule.AfterDiscount && _locSalesInvoiceLineXPO.DiscountRule.GrossAmountRule == true)
                            {
                                _locTxAmount = _locSalesInvoiceLineXPO.TxValue / 100 * (_locSalesInvoiceLineXPO.TUAmount - _locSalesInvoiceLineXPO.DiscAmount);

                                if (_locSalesInvoiceLineXPO.Tax.TaxNature == TaxNature.Increase)
                                {
                                    _locTAmount = _locSalesInvoiceLineXPO.TAmount + _locTxAmount;
                                }
                                if (_locSalesInvoiceLineXPO.Tax.TaxNature == TaxNature.Decrease)
                                {
                                    _locTAmount = _locSalesInvoiceLineXPO.TAmount - _locTxAmount;
                                }
                                _locSalesInvoiceLineXPO.TxAmount = _locTxAmount;
                                _locSalesInvoiceLineXPO.TAmount = _locTAmount;
                                _locSalesInvoiceLineXPO.TaxChecked = true;
                                _locSalesInvoiceLineXPO.Save();
                                _locSalesInvoiceLineXPO.Session.CommitTransaction();
                            }
                        }
                        else
                        {
                            if (_locSalesInvoiceLineXPO.Tax.TaxRule == TaxRule.AfterDiscount )
                            {
                                _locTxAmount = _locSalesInvoiceLineXPO.TxValue / 100 * (_locSalesInvoiceLineXPO.TUAmount);

                                if (_locSalesInvoiceLineXPO.Tax.TaxNature == TaxNature.Increase)
                                {
                                    _locTAmount = _locSalesInvoiceLineXPO.TAmount + _locTxAmount;
                                }
                                if (_locSalesInvoiceLineXPO.Tax.TaxNature == TaxNature.Decrease)
                                {
                                    _locTAmount = _locSalesInvoiceLineXPO.TAmount - _locTxAmount;
                                }
                                _locSalesInvoiceLineXPO.TxAmount = _locTxAmount;
                                _locSalesInvoiceLineXPO.TAmount = _locTAmount;
                                _locSalesInvoiceLineXPO.TaxChecked = true;
                                _locSalesInvoiceLineXPO.Save();
                                _locSalesInvoiceLineXPO.Session.CommitTransaction();
                            }
                        }
                        
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("BusinessObject = SalesInvoiceLine" + ex.ToString());
            }
        }

        private void SetDiscountNet(Session _currSession, SalesInvoiceLine _locSalesInvoiceLineXPO)
        {
            try
            {
                if (_locSalesInvoiceLineXPO != null)
                {
                    if (_locSalesInvoiceLineXPO.DiscountRule != null && _locSalesInvoiceLineXPO.Tax != null)
                    {
                        double _locTUAmountDiscount = 0;
                        double _locTUAmountDiscount2 = 0;
                        AccountingPeriodicLine _locAcctPerLine = null;

                        #region Net
                        if (_locSalesInvoiceLineXPO.DiscountRule.NetAmountRule == true && _locSalesInvoiceLineXPO.Tax.TaxRule == TaxRule.BeforeDiscount)
                        {
                            if (_locSalesInvoiceLineXPO.TAmount > 0 && _locSalesInvoiceLineXPO.TAmount >= _locSalesInvoiceLineXPO.DiscountRule.NetTotalUAmount)
                            {
                                if (_locSalesInvoiceLineXPO.DiscountRule.DiscountType == DiscountType.Percentage)
                                {
                                    _locTUAmountDiscount = _locSalesInvoiceLineXPO.TAmount * _locSalesInvoiceLineXPO.DiscountRule.Value / 100;
                                }
                                if (_locSalesInvoiceLineXPO.DiscountRule.DiscountType == DiscountType.Value)
                                {
                                    _locTUAmountDiscount = _locSalesInvoiceLineXPO.DiscountRule.Value;
                                }

                                #region ValBasedAccount
                                if (_locSalesInvoiceLineXPO.DiscountRule.ValBasedAccount == true)
                                {
                                    if (_locSalesInvoiceLineXPO.AccountingPeriodicLine != null)
                                    {
                                        _locAcctPerLine = _locSalesInvoiceLineXPO.AccountingPeriodicLine;
                                        if (_locAcctPerLine.BalHold < _locTUAmountDiscount)
                                        {
                                            _locTUAmountDiscount = _locAcctPerLine.BalHold;
                                        }

                                        _locAcctPerLine.BalHold = _locAcctPerLine.BalHold + _locTUAmountDiscount;
                                        _locAcctPerLine.Save();
                                        _locAcctPerLine.Session.CommitTransaction();
                                    }
                                    else
                                    {
                                        if (_locSalesInvoiceLineXPO.DiscountRule.AccountNo != null)
                                        {
                                            AccountingPeriodic _locAccountPeriodic = _currSession.FindObject<AccountingPeriodic>
                                                                            (new GroupOperator(GroupOperatorType.And,
                                                                            new BinaryOperator("Company", _locSalesInvoiceLineXPO.Company),
                                                                            new BinaryOperator("Workplace", _locSalesInvoiceLineXPO.Workplace),
                                                                            new BinaryOperator("Active", true)));
                                            if (_locAccountPeriodic != null)
                                            {
                                                AccountingPeriodicLine _locAccountingPeriodicLine = _currSession.FindObject<AccountingPeriodicLine>
                                                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                                                    new BinaryOperator("AccountingPeriodic", _locAccountPeriodic),
                                                                                                    new BinaryOperator("AccountNo", _locSalesInvoiceLineXPO.DiscountRule.AccountNo)));
                                                if (_locAccountingPeriodicLine != null)
                                                {
                                                    _locAcctPerLine = _locAccountingPeriodicLine;
                                                    if (_locAccountingPeriodicLine.BalHold < _locAccountingPeriodicLine.Balance)
                                                    {
                                                        _locTUAmountDiscount2 = _locAccountingPeriodicLine.Balance - _locAccountingPeriodicLine.BalHold;
                                                        if (_locTUAmountDiscount2 < _locTUAmountDiscount)
                                                        {
                                                            _locTUAmountDiscount = _locTUAmountDiscount2;
                                                        }

                                                    }
                                                    else
                                                    {
                                                        _locTUAmountDiscount = 0;
                                                    }

                                                    _locAccountingPeriodicLine.BalHold = _locAccountingPeriodicLine.BalHold + _locTUAmountDiscount;
                                                    _locAccountingPeriodicLine.Save();
                                                    _locAccountingPeriodicLine.Session.CommitTransaction();
                                                }
                                            }
                                        }
                                    }


                                }
                                #endregion ValBasedAccount

                                _locSalesInvoiceLineXPO.AccountingPeriodicLine = _locAcctPerLine;
                                _locSalesInvoiceLineXPO.DiscountChecked = true;
                                _locSalesInvoiceLineXPO.DiscAmount = _locTUAmountDiscount;
                                _locSalesInvoiceLineXPO.TAmount = _locSalesInvoiceLineXPO.TAmount - _locTUAmountDiscount;
                                _locSalesInvoiceLineXPO.Save();
                                _locSalesInvoiceLineXPO.Session.CommitTransaction();
                            }

                        }
                        #endregion Net
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("BusinessObject = SalesInvoiceLine" + ex.ToString());
            }
        }

        #endregion Tax

        #region Global Method

        private void SuccessMessageShow(string _locActionName)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 2000;
            options.Message = string.Format("{0}", _locActionName);
            options.Type = InformationType.Success;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Success";
            options.Win.Type = WinMessageType.Flyout;
            Application.ShowViewStrategy.ShowMessage(options);
        }

        private void ErrorMessageShow(string _locMessage)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 2000;
            options.Message = string.Format(_locMessage);
            options.Type = InformationType.Warning;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Error";
            options.Win.Type = WinMessageType.Alert;
            Application.ShowViewStrategy.ShowMessage(options);
        }


        #endregion Global Method

        
    }
}
