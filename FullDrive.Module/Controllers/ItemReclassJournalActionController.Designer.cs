﻿namespace FullDrive.Module.Controllers
{
    partial class ItemReclassJournalActionController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.ItemReclassJournalProgressAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.ItemReclassJournalPostingAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.ItemReclassJournalListviewFilterSelectionAction = new DevExpress.ExpressApp.Actions.SingleChoiceAction(this.components);
            this.ItemReclassJournalGetStockAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            // 
            // ItemReclassJournalProgressAction
            // 
            this.ItemReclassJournalProgressAction.Caption = "Progress";
            this.ItemReclassJournalProgressAction.ConfirmationMessage = null;
            this.ItemReclassJournalProgressAction.Id = "ItemReclassJournalActionControllerId";
            this.ItemReclassJournalProgressAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.ItemReclassJournal);
            this.ItemReclassJournalProgressAction.ToolTip = null;
            this.ItemReclassJournalProgressAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.ItemReclassJournalProgressAction_Execute);
            // 
            // ItemReclassJournalPostingAction
            // 
            this.ItemReclassJournalPostingAction.Caption = "Posting";
            this.ItemReclassJournalPostingAction.ConfirmationMessage = null;
            this.ItemReclassJournalPostingAction.Id = "ItemReclassJournalPostingActionId";
            this.ItemReclassJournalPostingAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.ItemReclassJournal);
            this.ItemReclassJournalPostingAction.ToolTip = null;
            this.ItemReclassJournalPostingAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.ItemReclassJournalPostingAction_Execute);
            // 
            // ItemReclassJournalListviewFilterSelectionAction
            // 
            this.ItemReclassJournalListviewFilterSelectionAction.Caption = "Filter";
            this.ItemReclassJournalListviewFilterSelectionAction.ConfirmationMessage = null;
            this.ItemReclassJournalListviewFilterSelectionAction.Id = "ItemReclassJournalListviewFilterSelectionActionId";
            this.ItemReclassJournalListviewFilterSelectionAction.ItemType = DevExpress.ExpressApp.Actions.SingleChoiceActionItemType.ItemIsOperation;
            this.ItemReclassJournalListviewFilterSelectionAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.ItemReclassJournal);
            this.ItemReclassJournalListviewFilterSelectionAction.ToolTip = null;
            this.ItemReclassJournalListviewFilterSelectionAction.Execute += new DevExpress.ExpressApp.Actions.SingleChoiceActionExecuteEventHandler(this.ItemReclassJournalListviewFilterSelectionAction_Execute);
            // 
            // ItemReclassJournalGetStockAction
            // 
            this.ItemReclassJournalGetStockAction.Caption = "Get Stock";
            this.ItemReclassJournalGetStockAction.ConfirmationMessage = null;
            this.ItemReclassJournalGetStockAction.Id = "ItemReclassJournalGetStockActionId";
            this.ItemReclassJournalGetStockAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.ItemReclassJournal);
            this.ItemReclassJournalGetStockAction.ToolTip = null;
            this.ItemReclassJournalGetStockAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.ItemReclassJournalGetStockAction_Execute);
            // 
            // ItemReclassJournalActionController
            // 
            this.Actions.Add(this.ItemReclassJournalProgressAction);
            this.Actions.Add(this.ItemReclassJournalPostingAction);
            this.Actions.Add(this.ItemReclassJournalListviewFilterSelectionAction);
            this.Actions.Add(this.ItemReclassJournalGetStockAction);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SimpleAction ItemReclassJournalProgressAction;
        private DevExpress.ExpressApp.Actions.SimpleAction ItemReclassJournalPostingAction;
        private DevExpress.ExpressApp.Actions.SingleChoiceAction ItemReclassJournalListviewFilterSelectionAction;
        private DevExpress.ExpressApp.Actions.SimpleAction ItemReclassJournalGetStockAction;
    }
}
