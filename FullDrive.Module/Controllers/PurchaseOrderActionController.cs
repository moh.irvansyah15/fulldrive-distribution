﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;

using FullDrive.Module.BusinessObjects;
using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.Xpo;
using System.Collections;
using DevExpress.Xpo;
using System.IO;
using System.Web;

namespace FullDrive.Module.Controllers
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class PurchaseOrderActionController : ViewController
    {
        private ChoiceActionItem _selectionListviewFilter;
        private ChoiceActionItem _selectionApprovalListviewFilter;
        private ChoiceActionItem _setApprovalLevel;

        public PurchaseOrderActionController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.
            #region FilterStatus
            PurchaseOrderListviewFilterSelectionAction.Items.Clear();
            foreach (object _currApproval in Enum.GetValues(typeof(CustomProcess.Status)))
            {
                EnumDescriptor _ed = new EnumDescriptor(typeof(CustomProcess.Status));
                _selectionListviewFilter = new ChoiceActionItem(_ed.GetCaption(_currApproval), _currApproval);
                PurchaseOrderListviewFilterSelectionAction.Items.Add(_selectionListviewFilter);
            }
            #endregion FilterStatus

            #region FilterApproval
            PurchaseOrderListviewFilterApprovalSelectionAction.Items.Clear();
            foreach (object _currApprovalFilter in Enum.GetValues(typeof(CustomProcess.ApprovalFilter)))
            {
                EnumDescriptor _ed = new EnumDescriptor(typeof(CustomProcess.ApprovalFilter));
                _selectionApprovalListviewFilter = new ChoiceActionItem(_ed.GetCaption(_currApprovalFilter), _currApprovalFilter);
                PurchaseOrderListviewFilterApprovalSelectionAction.Items.Add(_selectionApprovalListviewFilter);
            }
            #endregion FilterApproval
        }

        protected override void OnActivated()
        {
            TypeOfView = typeof(ObjectView);
            base.OnActivated();
            // Perform various tasks depending on the target View.
            #region Approval
            var _locUser = SecuritySystem.CurrentUserName;
            
            if (this.ObjectSpace != null)
            {
                UserAccess _locUserAccess = this.ObjectSpace.FindObject<UserAccess>(new BinaryOperator("UserName", _locUser));

                if (_locUserAccess != null)
                {
                    PurchaseOrderApprovalAction.Items.Clear();

                    XPCollection<ApplicationSetupDetail> _locAppSetupDetails = new XPCollection<ApplicationSetupDetail>
                                                                                 (_locUserAccess.Session, new GroupOperator(GroupOperatorType.And,
                                                                                  new BinaryOperator("UserAccess", _locUserAccess),
                                                                                  new BinaryOperator("ObjectList", CustomProcess.ObjectList.PurchaseOrder),
                                                                                  new BinaryOperator("FunctionList", CustomProcess.FunctionList.Approval),
                                                                                  new BinaryOperator("Active", true)));

                    if (_locAppSetupDetails != null && _locAppSetupDetails.Count > 0)
                    {

                        foreach (ApplicationSetupDetail _locAppSetupDetail in _locAppSetupDetails)
                        {
                            foreach (object _currApproval in Enum.GetValues(typeof(CustomProcess.ApprovalLevel)))
                            {
                                if (_locAppSetupDetail.ApprovalLevel != CustomProcess.ApprovalLevel.None)
                                {
                                    EnumDescriptor _ed = new EnumDescriptor(typeof(CustomProcess.ApprovalLevel));
                                    _setApprovalLevel = new ChoiceActionItem(_ed.GetCaption(_locAppSetupDetail.ApprovalLevel), _locAppSetupDetail.ApprovalLevel);
                                }
                            }
                            PurchaseOrderApprovalAction.Items.Add(_setApprovalLevel);
                        }
                    }
                }
            }
            #endregion Approval
        }

        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }

        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        private void PurchaseOrderGetPRAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;

                if (_objectsToProcess != null)
                {
                    foreach (Object obj in _objectsToProcess)
                    {
                        PurchaseOrder _locPurchaseOrderOS = (PurchaseOrder)_objectSpace.GetObject(obj);

                        if (_locPurchaseOrderOS != null)
                        {
                            if (_locPurchaseOrderOS.Session != null)
                            {
                                _currSession = _locPurchaseOrderOS.Session;
                            }

                            if (_locPurchaseOrderOS.Code != null && _currSession != null)
                            {
                                _currObjectId = _locPurchaseOrderOS.Code;

                                PurchaseOrder _locPurchaseOrderXPO = _currSession.FindObject<PurchaseOrder>
                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                     new BinaryOperator("Code", _currObjectId),
                                                                     new BinaryOperator("Status", Status.Open)));

                                if (_locPurchaseOrderXPO != null)
                                {
                                    XPCollection<PurchaseRequisitionCollection> _locPurchaseRequisitionCollections = new XPCollection<PurchaseRequisitionCollection>
                                                                                        (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                        new BinaryOperator("PurchaseOrder", _locPurchaseOrderXPO),
                                                                                        new GroupOperator(GroupOperatorType.Or,
                                                                                        new BinaryOperator("Status", Status.Open),
                                                                                        new BinaryOperator("Status", Status.Progress))));

                                    if (_locPurchaseRequisitionCollections != null && _locPurchaseRequisitionCollections.Count() > 0)
                                    {
                                        foreach (PurchaseRequisitionCollection _locPurchaseRequisitionCollection in _locPurchaseRequisitionCollections)
                                        {
                                            if (_locPurchaseRequisitionCollection.PurchaseRequisition != null)
                                            {
                                                ApprovalLine _locApprovalLineXPO = _currSession.FindObject<ApprovalLine>
                                                                   (new GroupOperator(GroupOperatorType.And,
                                                                    new BinaryOperator("EndApproval", true),
                                                                    new BinaryOperator("PurchaseRequisition", _locPurchaseRequisitionCollection.PurchaseRequisition)));

                                                if (_locApprovalLineXPO != null)
                                                {
                                                    GetPurchaseRequisitionMonitoring(_currSession, _locPurchaseRequisitionCollection.PurchaseRequisition, _locPurchaseOrderXPO);
                                                }
                                            }
                                        }
                                        SuccessMessageShow("Purchase Requisition Has Been Successfully Getting into Purchase Order");
                                    }
                                }
                                else
                                {
                                    ErrorMessageShow("Data Purchase Order Not Available");
                                }
                            }
                            else
                            {
                                ErrorMessageShow("Data Purchase Order Not Available");
                            }
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PurchaseOrder " + ex.ToString());
            }
        }

        private void PurchaseOrderApprovalAction_Execute(object sender, SingleChoiceActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                ApplicationSetupDetail _locAppSetDetail = null;
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                var User = SecuritySystem.CurrentUserName;
                Session _currentSession = null;
                string _currObjectId = null;

                foreach (Object obj in _objectsToProcess)
                {
                    PurchaseOrder _objInNewObjectSpace = (PurchaseOrder)_objectSpace.GetObject(obj);

                    if (_objInNewObjectSpace != null)
                    {
                        if (_objInNewObjectSpace.Code != null)
                        {
                            _currObjectId = _objInNewObjectSpace.Code;
                        }
                        if (_objInNewObjectSpace.Session != null)
                        {
                            _currentSession = _objInNewObjectSpace.Session;
                        }
                    }

                    if (_currObjectId != null && _currentSession != null)
                    {
                        UserAccess _locUserAccess = _currentSession.FindObject<UserAccess>(new BinaryOperator("UserName", User));

                        PurchaseOrder _locPurchaseOrderXPO = _currentSession.FindObject<PurchaseOrder>
                                                                        (new GroupOperator(GroupOperatorType.And,
                                                                         new BinaryOperator("Code", _currObjectId),
                                                                         new BinaryOperator("Status", Status.Open)));

                        if (_locPurchaseOrderXPO != null)
                        {
                            ApprovalLine _locApprovalLine = _currentSession.FindObject<ApprovalLine>
                                                                            (new GroupOperator(GroupOperatorType.And,
                                                                             new BinaryOperator("PurchaseOrder", _locPurchaseOrderXPO),
                                                                             new BinaryOperator("EndApproval", true)));
                            if (_locApprovalLine == null)
                            {
                                #region Approval Level 1
                                if ((ApprovalLevel)e.SelectedChoiceActionItem.Data == ApprovalLevel.Level1)
                                {
                                    _locAppSetDetail = _globFunc.GetApplicationSetupDetailByApprovalLevel(_currentSession, _locUserAccess, ApprovalLevel.Level1, ObjectList.PurchaseOrder);
                                    if (_locAppSetDetail != null)
                                    {
                                        //Buat bs input langsung ke approvalline
                                        ApprovalLine _locApprovalLineXPO = _currentSession.FindObject<ApprovalLine>
                                                                            (new GroupOperator(GroupOperatorType.And,
                                                                             new BinaryOperator("PurchaseOrder", _locPurchaseOrderXPO),
                                                                             new BinaryOperator("ApprovalLevel", ApprovalLevel.Level1)));
                                        if (_locApprovalLineXPO == null)
                                        {

                                            if (_locAppSetDetail.EndApproval == true)
                                            {
                                                ApprovalLine _saveDataAL = new ApprovalLine(_currentSession)
                                                {
                                                    ApprovalDate = now,
                                                    ApprovalStatus = Status.Approved,
                                                    ApprovalLevel = ApprovalLevel.Level1,
                                                    EndApproval = true,
                                                    PurchaseOrder = _locPurchaseOrderXPO,
                                                };
                                                _saveDataAL.Save();
                                                _saveDataAL.Session.CommitTransaction();

                                                _locPurchaseOrderXPO.ActivationPosting = true;
                                                _locPurchaseOrderXPO.ActiveApproved1 = false;
                                                _locPurchaseOrderXPO.ActiveApproved2 = false;
                                                _locPurchaseOrderXPO.ActiveApproved3 = true;
                                                _locPurchaseOrderXPO.Save();
                                                _locPurchaseOrderXPO.Session.CommitTransaction();
                                            }
                                            else
                                            {
                                                ApprovalLine _saveDataAL = new ApprovalLine(_currentSession)
                                                {
                                                    ApprovalDate = now,
                                                    ApprovalStatus = Status.Approved,
                                                    ApprovalLevel = ApprovalLevel.Level1,
                                                    PurchaseOrder = _locPurchaseOrderXPO,
                                                };
                                                _saveDataAL.Save();
                                                _saveDataAL.Session.CommitTransaction();

                                                _locPurchaseOrderXPO.ActiveApproved1 = true;
                                                _locPurchaseOrderXPO.ActiveApproved2 = false;
                                                _locPurchaseOrderXPO.ActiveApproved3 = false;
                                                _locPurchaseOrderXPO.Save();
                                                _locPurchaseOrderXPO.Session.CommitTransaction();
                                            }

                                            //Send Email
                                            ApprovalLine _locApprovalLineXPO2 = _currentSession.FindObject<ApprovalLine>
                                                                                (new GroupOperator(GroupOperatorType.And,
                                                                                 new BinaryOperator("PurchaseOrder", _locPurchaseOrderXPO)));

                                            if (_locApprovalLineXPO2 != null)
                                            {
                                                SendEmail(_currentSession, _locPurchaseOrderXPO, Status.Progress, _locApprovalLineXPO2);
                                            }

                                            SuccessMessageShow("Purchase Order has successfully Approve");
                                        }
                                    }
                                }
                                #endregion Approval Level 1

                                #region Approval Level 2
                                if ((ApprovalLevel)e.SelectedChoiceActionItem.Data == ApprovalLevel.Level2)
                                {
                                    _locAppSetDetail = _globFunc.GetApplicationSetupDetailByApprovalLevel(_currentSession, _locUserAccess, ApprovalLevel.Level2, ObjectList.PurchaseOrder);

                                    if (_locAppSetDetail != null)
                                    {
                                        ApprovalLine _locApprovalLineXPO = _currentSession.FindObject<ApprovalLine>
                                                                            (new GroupOperator(GroupOperatorType.And,
                                                                             new BinaryOperator("PurchaseOrder", _locPurchaseOrderXPO),
                                                                             new BinaryOperator("ApprovalLevel", ApprovalLevel.Level2)));

                                        if (_locApprovalLineXPO == null)
                                        {
                                            if (_locAppSetDetail.EndApproval == true)
                                            {
                                                ApprovalLine _saveDataAL = new ApprovalLine(_currentSession)
                                                {
                                                    ApprovalDate = now,
                                                    ApprovalStatus = Status.Approved,
                                                    ApprovalLevel = ApprovalLevel.Level2,
                                                    EndApproval = true,
                                                    PurchaseOrder = _locPurchaseOrderXPO,
                                                };
                                                _saveDataAL.Save();
                                                _saveDataAL.Session.CommitTransaction();

                                                _locPurchaseOrderXPO.ActivationPosting = true;
                                                _locPurchaseOrderXPO.ActiveApproved1 = false;
                                                _locPurchaseOrderXPO.ActiveApproved2 = false;
                                                _locPurchaseOrderXPO.ActiveApproved3 = true;
                                                _locPurchaseOrderXPO.Save();
                                                _locPurchaseOrderXPO.Session.CommitTransaction();
                                            }
                                            else
                                            {
                                                ApprovalLine _saveDataAL = new ApprovalLine(_currentSession)
                                                {
                                                    ApprovalDate = now,
                                                    ApprovalStatus = Status.Approved,
                                                    ApprovalLevel = ApprovalLevel.Level2,
                                                    PurchaseOrder = _locPurchaseOrderXPO,
                                                };
                                                _saveDataAL.Save();
                                                _saveDataAL.Session.CommitTransaction();

                                                SetApprovalLine(_currentSession, _locPurchaseOrderXPO, ApprovalLevel.Level1);

                                                _locPurchaseOrderXPO.ActiveApproved1 = false;
                                                _locPurchaseOrderXPO.ActiveApproved2 = true;
                                                _locPurchaseOrderXPO.ActiveApproved3 = false;
                                                _locPurchaseOrderXPO.Save();
                                                _locPurchaseOrderXPO.Session.CommitTransaction();
                                            }

                                            //Send Email
                                            ApprovalLine _locApprovalLineXPO2 = _currentSession.FindObject<ApprovalLine>
                                                                                (new GroupOperator(GroupOperatorType.And,
                                                                                 new BinaryOperator("PurchaseOrder", _locPurchaseOrderXPO)));

                                            if (_locApprovalLineXPO2 != null)
                                            {
                                                SendEmail(_currentSession, _locPurchaseOrderXPO, Status.Progress, _locApprovalLineXPO2);
                                            }

                                            SuccessMessageShow("Purchase Order has successfully Approve");
                                        }
                                    }
                                }
                                #endregion Approval Level 2

                                #region Approval Level 3
                                if ((ApprovalLevel)e.SelectedChoiceActionItem.Data == ApprovalLevel.Level3)
                                {
                                    _locAppSetDetail = _globFunc.GetApplicationSetupDetailByApprovalLevel(_currentSession, _locUserAccess, ApprovalLevel.Level3, ObjectList.PurchaseOrder);

                                    if (_locAppSetDetail != null)
                                    {
                                        ApprovalLine _locApprovalLineXPO = _currentSession.FindObject<ApprovalLine>
                                                                            (new GroupOperator(GroupOperatorType.And,
                                                                             new BinaryOperator("PurchaseOrder", _locPurchaseOrderXPO),
                                                                             new BinaryOperator("ApprovalLevel", ApprovalLevel.Level3)));

                                        if (_locApprovalLineXPO == null)
                                        {
                                            if (_locAppSetDetail.EndApproval == true)
                                            {
                                                ApprovalLine _saveDataAL = new ApprovalLine(_currentSession)
                                                {
                                                    ApprovalDate = now,
                                                    ApprovalStatus = Status.Approved,
                                                    ApprovalLevel = ApprovalLevel.Level3,
                                                    EndApproval = true,
                                                    PurchaseOrder = _locPurchaseOrderXPO,
                                                };
                                                _saveDataAL.Save();
                                                _saveDataAL.Session.CommitTransaction();

                                                _locPurchaseOrderXPO.ActivationPosting = true;
                                                _locPurchaseOrderXPO.ActiveApproved1 = false;
                                                _locPurchaseOrderXPO.ActiveApproved2 = false;
                                                _locPurchaseOrderXPO.ActiveApproved3 = true;
                                                _locPurchaseOrderXPO.Save();
                                                _locPurchaseOrderXPO.Session.CommitTransaction();
                                            }
                                            else
                                            {
                                                ApprovalLine _saveDataAL = new ApprovalLine(_currentSession)
                                                {
                                                    ApprovalDate = now,
                                                    ApprovalStatus = Status.Approved,
                                                    ApprovalLevel = ApprovalLevel.Level3,
                                                    PurchaseOrder = _locPurchaseOrderXPO,
                                                };
                                                _saveDataAL.Save();
                                                _saveDataAL.Session.CommitTransaction();

                                                SetApprovalLine(_currentSession, _locPurchaseOrderXPO, ApprovalLevel.Level2);
                                                SetApprovalLine(_currentSession, _locPurchaseOrderXPO, ApprovalLevel.Level1);

                                                _locPurchaseOrderXPO.ActiveApproved1 = false;
                                                _locPurchaseOrderXPO.ActiveApproved2 = false;
                                                _locPurchaseOrderXPO.ActiveApproved3 = true;
                                                _locPurchaseOrderXPO.Save();
                                                _locPurchaseOrderXPO.Session.CommitTransaction();
                                            }

                                            //Send Email
                                            ApprovalLine _locApprovalLineXPO2 = _currentSession.FindObject<ApprovalLine>
                                                                                (new GroupOperator(GroupOperatorType.And,
                                                                                 new BinaryOperator("PurchaseOrder", _locPurchaseOrderXPO)));

                                            if (_locApprovalLineXPO2 != null)
                                            {
                                                SendEmail(_currentSession, _locPurchaseOrderXPO, Status.Progress, _locApprovalLineXPO2);
                                            }

                                            SuccessMessageShow("Purchase Order has successfully Approve");
                                        }
                                    }
                                }
                                #endregion Approval Level 3
                            }
                        }
                        else
                        {
                            ErrorMessageShow("Purchase Order Not Available");
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PurchaseOrder " + ex.ToString());
            }
        }

        private void PurchaseOrderProgressAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;
                Status _locStatus = Status.Open;

                if (_objectsToProcess != null)
                {
                    foreach (Object obj in _objectsToProcess)
                    {
                        PurchaseOrder _locPurchaseOrderOS = (PurchaseOrder)_objectSpace.GetObject(obj);

                        if (_locPurchaseOrderOS != null)
                        {
                            if (_locPurchaseOrderOS.Session != null)
                            {
                                _currSession = _locPurchaseOrderOS.Session;
                            }

                            if (_locPurchaseOrderOS.Code != null && _currSession != null)
                            {
                                _currObjectId = _locPurchaseOrderOS.Code;

                                PurchaseOrder _locPurchaseOrderXPO = _currSession.FindObject<PurchaseOrder>
                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                     new BinaryOperator("Code", _currObjectId),
                                                                     new GroupOperator(GroupOperatorType.Or,
                                                                     new BinaryOperator("Status", Status.Open),
                                                                     new BinaryOperator("Status", Status.Progress))));

                                if (_locPurchaseOrderXPO != null)
                                {
                                    ApprovalLine _locApprovalLine = _currSession.FindObject<ApprovalLine>
                                                                            (new GroupOperator(GroupOperatorType.And,
                                                                             new BinaryOperator("PurchaseOrder", _locPurchaseOrderXPO),
                                                                             new BinaryOperator("EndApproval", true)));

                                    if(_locApprovalLine != null)
                                    {
                                        if (_locPurchaseOrderXPO.Status == Status.Open)
                                        {
                                            _locStatus = Status.Progress;
                                        }
                                        else
                                        {
                                            _locStatus = _locPurchaseOrderXPO.Status;
                                        }

                                        _locPurchaseOrderXPO.Status = _locStatus;
                                        _locPurchaseOrderXPO.StatusDate = now;
                                        _locPurchaseOrderXPO.Save();
                                        _locPurchaseOrderXPO.Session.CommitTransaction();

                                        #region StatusPurchaseOrderLine
                                        XPCollection<PurchaseOrderLine> _locPurchaseOrderLines = new XPCollection<PurchaseOrderLine>
                                                               (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("PurchaseOrder", _locPurchaseOrderXPO),
                                                                new BinaryOperator("Status", Status.Open)));

                                        if (_locPurchaseOrderLines != null && _locPurchaseOrderLines.Count > 0)
                                        {
                                            foreach (PurchaseOrderLine _locPurchaseOrderLine in _locPurchaseOrderLines)
                                            {
                                                _locPurchaseOrderLine.Status = Status.Progress;
                                                _locPurchaseOrderLine.StatusDate = now;
                                                _locPurchaseOrderLine.Save();
                                                _locPurchaseOrderLine.Session.CommitTransaction();
                                            }
                                        }
                                        #endregion StatusPurchaseOrderLine

                                        #region StatusPurchaseRequisitionCollection
                                        XPCollection<PurchaseRequisitionCollection> _locPurReqCollections = new XPCollection<PurchaseRequisitionCollection>
                                                               (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("PurchaseOrder", _locPurchaseOrderXPO),
                                                                new BinaryOperator("Status", Status.Open)));

                                        if (_locPurReqCollections != null && _locPurReqCollections.Count > 0)
                                        {
                                            foreach (PurchaseRequisitionCollection _locPurReqCollection in _locPurReqCollections)
                                            {
                                                _locPurReqCollection.Status = Status.Progress;
                                                _locPurReqCollection.StatusDate = now;
                                                _locPurReqCollection.Save();
                                                _locPurReqCollection.Session.CommitTransaction();
                                            }
                                        }
                                        #endregion StatusPurchaseRequisitionCollection

                                        SetTotalAmountInPO(_currSession, _locPurchaseOrderXPO);
                                        SuccessMessageShow(_locPurchaseOrderXPO.Code + "has been successfully updated to progress");
                                    }                                    
                                }
                                else
                                {
                                    ErrorMessageShow("Data Purchase Order Not Available");
                                }
                            }
                            else
                            {
                                ErrorMessageShow("Data Purchase Order Not Available");
                            }
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PurchaseOrder " + ex.ToString());
            }
        }

        private void PurchaseOrderPostingAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;

                if (_objectsToProcess != null)
                {
                    foreach (Object obj in _objectsToProcess)
                    {
                        PurchaseOrder _locPurchaseOrderOS = (PurchaseOrder)_objectSpace.GetObject(obj);

                        if (_locPurchaseOrderOS != null)
                        {
                            if (_locPurchaseOrderOS.Session != null)
                            {
                                _currSession = _locPurchaseOrderOS.Session;
                            }

                            if (_locPurchaseOrderOS.Code != null && _currSession != null)
                            {
                                _currObjectId = _locPurchaseOrderOS.Code;

                                PurchaseOrder _locPurchaseOrderXPO = _currSession.FindObject<PurchaseOrder>
                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                     new BinaryOperator("Code", _currObjectId),
                                                                     new GroupOperator(GroupOperatorType.Or,
                                                                     new BinaryOperator("Status", Status.Progress),
                                                                     new BinaryOperator("Status", Status.Posted))));

                                if (_locPurchaseOrderXPO != null)
                                {
                                    ApprovalLine _locApprovalLineXPO = _currSession.FindObject<ApprovalLine>
                                                                           (new GroupOperator(GroupOperatorType.And,
                                                                            new BinaryOperator("EndApproval", true),
                                                                            new BinaryOperator("PurchaseOrder", _locPurchaseOrderXPO)));

                                    if (_locApprovalLineXPO != null)
                                    {
                                        SetPurchaseOrderMonitoring(_currSession, _locPurchaseOrderXPO);
                                        SetCostingMethod(_currSession, _locPurchaseOrderXPO);
                                        SetStatusPurchaseOrderLine(_currSession, _locPurchaseOrderXPO);
                                        SetNormal(_currSession, _locPurchaseOrderXPO);
                                        SetFinalStatusPurchaseOrder(_currSession, _locPurchaseOrderXPO);
                                        SuccessMessageShow("Purchase Order has been successfully post");
                                    }
                                }
                                else
                                {
                                    ErrorMessageShow("Data Purchase Order Not Available");
                                }
                            }
                            else
                            {
                                ErrorMessageShow("Data Purchase Order Not Available");
                            }
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PurchaseOrder " + ex.ToString());
            }
        }  

        private void PurchaseOrderListviewFilterApprovalSelectionAction_Execute(object sender, SingleChoiceActionExecuteEventArgs e)
        {
            try
            {
                if ((View is ListView) & (View.ObjectTypeInfo.Type == typeof(PurchaseOrder)))
                {
                    if ((ApprovalFilter)e.SelectedChoiceActionItem.Data == ApprovalFilter.Approval)
                    {
                        ((ListView)View).CollectionSource.Criteria["FilterApproval1"] = new GroupOperator(GroupOperatorType.And,
                                                                                        new BinaryOperator("ActiveApproved3", false, BinaryOperatorType.Equal),
                                                                                        new GroupOperator(GroupOperatorType.Or,
                                                                                        new BinaryOperator("ActiveApproved1", true, BinaryOperatorType.Equal),
                                                                                        new BinaryOperator("ActiveApproved2", true, BinaryOperatorType.Equal)));
                    }
                    else if ((ApprovalFilter)e.SelectedChoiceActionItem.Data == ApprovalFilter.EndApproval)
                    {
                        ((ListView)View).CollectionSource.Criteria["FilterApproval1"] = new BinaryOperator("ActiveApproved3", true, BinaryOperatorType.Equal);
                    }
                    else if ((ApprovalFilter)e.SelectedChoiceActionItem.Data == ApprovalFilter.AllData)
                    {
                        ((ListView)View).CollectionSource.Criteria["FilterApproval1"] = new GroupOperator(GroupOperatorType.Or,
                                                                                        new BinaryOperator("ActiveApproved1", true, BinaryOperatorType.Equal),
                                                                                        new BinaryOperator("ActiveApproved2", true, BinaryOperatorType.Equal),
                                                                                        new BinaryOperator("ActiveApproved3", true, BinaryOperatorType.Equal),
                                                                                        new BinaryOperator("ActiveApproved1", false, BinaryOperatorType.Equal),
                                                                                        new BinaryOperator("ActiveApproved2", false, BinaryOperatorType.Equal),
                                                                                        new BinaryOperator("ActiveApproved3", false, BinaryOperatorType.Equal));
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PurchaseOrder " + ex.ToString());
            }
        }

        private void PurchaseOrderListviewFilterSelectionAction_Execute(object sender, SingleChoiceActionExecuteEventArgs e)
        {
            try
            {
                if ((View is ListView) & (View.ObjectTypeInfo.Type == typeof(PurchaseOrder)))
                {
                    if ((Status)e.SelectedChoiceActionItem.Data == Status.None)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.None, BinaryOperatorType.NotEqual);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Open)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Open, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Progress)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Progress, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Posted)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Posted, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Lock)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Lock, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Close)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Close, BinaryOperatorType.Equal);
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PurchaseOrder " + ex.ToString());
            }
        }

        //=========================================== Code In Here ===========================================

        #region GetPR

        private void GetPurchaseRequisitionMonitoring(Session _currSession, PurchaseRequisition _locPurchaseRequisitionXPO, PurchaseOrder _locPurchaseOrderXPO)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                DateTime now = DateTime.Now;
                string _currSignCode = null;

                if (_locPurchaseRequisitionXPO != null && _locPurchaseOrderXPO != null)
                {
                    //Hanya memindahkan PurchaseRequisitionMonitoring ke PurchaseOrderLine
                    XPCollection<PurchaseRequisitionMonitoring> _locPurchaseRequisitionMonitorings = new XPCollection<PurchaseRequisitionMonitoring>
                                                                                        (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                        new BinaryOperator("PurchaseRequisition", _locPurchaseRequisitionXPO),
                                                                                        new BinaryOperator("PurchaseOrder", _locPurchaseOrderXPO),
                                                                                        new BinaryOperator("Select", true),
                                                                                        new GroupOperator(GroupOperatorType.Or,
                                                                                        new BinaryOperator("Status", Status.Open),
                                                                                        new BinaryOperator("Status", Status.Posted))));

                    if (_locPurchaseRequisitionMonitorings != null && _locPurchaseRequisitionMonitorings.Count() > 0)
                    {
                        foreach (PurchaseRequisitionMonitoring _locPurchaseRequisitionMonitoring in _locPurchaseRequisitionMonitorings)
                        {
                            if (_locPurchaseRequisitionMonitoring.PurchaseRequisitionLine != null)
                            {
                                _currSignCode = _globFunc.GetNumberingSignUnlockOptimisticRecordBasedOrganization(_currSession.DataLayer, ObjectList.PurchaseOrderLine, _locPurchaseOrderXPO.Company, _locPurchaseOrderXPO.Workplace);

                                if (_currSignCode != null)
                                {
                                    #region SavePurchaseOrderLine
                                    PurchaseOrderLine _saveDataPurchaseOrderLine = new PurchaseOrderLine(_currSession)
                                    {
                                        SignCode = _currSignCode,
                                        PurchaseType = _locPurchaseRequisitionMonitoring.PurchaseRequisitionLine.PurchaseType,
                                        Item = _locPurchaseRequisitionMonitoring.Item,
                                        Brand = _locPurchaseRequisitionMonitoring.PurchaseRequisitionLine.Brand,
                                        Description = _locPurchaseRequisitionMonitoring.PurchaseRequisitionLine.Description,
                                        DQty = _locPurchaseRequisitionMonitoring.DQty,
                                        DUOM = _locPurchaseRequisitionMonitoring.DUOM,
                                        Qty = _locPurchaseRequisitionMonitoring.Qty,
                                        UOM = _locPurchaseRequisitionMonitoring.UOM,
                                        TQty = _locPurchaseRequisitionMonitoring.TQty,
                                        Company = _locPurchaseRequisitionMonitoring.Company,
                                        Workplace = _locPurchaseRequisitionMonitoring.Workplace,
                                        Div = _locPurchaseRequisitionMonitoring.Div,
                                        Dept = _locPurchaseRequisitionMonitoring.Dept,
                                        Sect = _locPurchaseRequisitionMonitoring.Sect,
                                        PurchaseRequisitionMonitoring = _locPurchaseRequisitionMonitoring,
                                        PurchaseOrder = _locPurchaseOrderXPO,
                                    };
                                    _saveDataPurchaseOrderLine.Save();
                                    _saveDataPurchaseOrderLine.Session.CommitTransaction();
                                    #endregion SavePurchaseOrderLine

                                    PurchaseOrderLine _locPurchaseOrderLine = _currSession.FindObject<PurchaseOrderLine>
                                                                (new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("SignCode", _currSignCode)));
                                    if (_locPurchaseOrderLine != null)
                                    {
                                        SetRemainQty(_currSession, _locPurchaseRequisitionMonitoring);
                                        SetPostingQty(_currSession, _locPurchaseRequisitionMonitoring);
                                        SetProcessCount(_currSession, _locPurchaseRequisitionMonitoring);
                                        SetStatusPurchaseRequisitionMonitoring(_currSession, _locPurchaseRequisitionMonitoring);
                                        SetNormalQuantity(_currSession, _locPurchaseRequisitionMonitoring);
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = PurchaseOrder ", ex.ToString());
            }
        }

        private void SetRemainQty(Session _currSession, PurchaseRequisitionMonitoring _locPurchaseRequisitionMonitoringXPO)
        {
            try
            {
                if (_locPurchaseRequisitionMonitoringXPO != null)
                {
                    double _locRmDQty = 0;
                    double _locRmQty = 0;
                    double _locInvLineTotal = 0;
                    ItemUnitOfMeasure _locItemUOM = null;

                    #region ProcessCount=0
                    if (_locPurchaseRequisitionMonitoringXPO.PostedCount == 0)
                    {
                        #region MaxQuantity
                        if (_locPurchaseRequisitionMonitoringXPO.MxDQty > 0)
                        {
                            if (_locPurchaseRequisitionMonitoringXPO.DQty > 0 && _locPurchaseRequisitionMonitoringXPO.DQty <= _locPurchaseRequisitionMonitoringXPO.MxDQty)
                            {
                                _locRmDQty = _locPurchaseRequisitionMonitoringXPO.MxDQty - _locPurchaseRequisitionMonitoringXPO.DQty;
                            }

                            if (_locPurchaseRequisitionMonitoringXPO.Qty > 0 && _locPurchaseRequisitionMonitoringXPO.Qty <= _locPurchaseRequisitionMonitoringXPO.MxQty)
                            {
                                _locRmQty = _locPurchaseRequisitionMonitoringXPO.MxQty - _locPurchaseRequisitionMonitoringXPO.Qty;
                            }


                            _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                       (new GroupOperator(GroupOperatorType.And,
                                                        new BinaryOperator("Item", _locPurchaseRequisitionMonitoringXPO.Item),
                                                        new BinaryOperator("UOM", _locPurchaseRequisitionMonitoringXPO.MxUOM),
                                                        new BinaryOperator("DefaultUOM", _locPurchaseRequisitionMonitoringXPO.MxDUOM),
                                                        new BinaryOperator("Active", true)));
                            if (_locItemUOM != null)
                            {
                                if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                {
                                    _locInvLineTotal = _locRmQty * _locItemUOM.DefaultConversion + _locRmDQty;
                                }
                                else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                {
                                    _locInvLineTotal = _locRmQty / _locItemUOM.Conversion + _locRmDQty;
                                }
                                else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                {
                                    _locInvLineTotal = _locRmQty + _locRmDQty;
                                }
                            }
                            else
                            {
                                _locInvLineTotal = _locRmQty + _locRmDQty;
                            }

                        }
                        #endregion MaxQuantity
                        #region NonMaxQuantity
                        else
                        {
                            if (_locPurchaseRequisitionMonitoringXPO.DQty > 0)
                            {
                                _locRmDQty = 0;
                            }

                            if (_locPurchaseRequisitionMonitoringXPO.Qty > 0)
                            {
                                _locRmQty = 0;
                            }


                            _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                       (new GroupOperator(GroupOperatorType.And,
                                                        new BinaryOperator("Item", _locPurchaseRequisitionMonitoringXPO.Item),
                                                        new BinaryOperator("UOM", _locPurchaseRequisitionMonitoringXPO.UOM),
                                                        new BinaryOperator("DefaultUOM", _locPurchaseRequisitionMonitoringXPO.DUOM),
                                                        new BinaryOperator("Active", true)));
                            if (_locItemUOM != null)
                            {
                                if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                {
                                    _locInvLineTotal = _locRmQty * _locItemUOM.DefaultConversion + _locRmDQty;
                                }
                                else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                {
                                    _locInvLineTotal = _locRmQty / _locItemUOM.Conversion + _locRmDQty;
                                }
                                else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                {
                                    _locInvLineTotal = _locRmQty + _locRmDQty;
                                }
                            }
                            else
                            {
                                _locInvLineTotal = _locRmQty + _locRmDQty;
                            }
                        }
                        #endregion NonMaxQuantity
                    }
                    #endregion ProcessCount=0

                    #region ProcessCount>0
                    if (_locPurchaseRequisitionMonitoringXPO.PostedCount > 0)
                    {
                        if (_locPurchaseRequisitionMonitoringXPO.RmDQty > 0)
                        {
                            _locRmDQty = _locPurchaseRequisitionMonitoringXPO.RmDQty - _locPurchaseRequisitionMonitoringXPO.DQty;
                        }

                        if (_locPurchaseRequisitionMonitoringXPO.RmQty > 0)
                        {
                            _locRmQty = _locPurchaseRequisitionMonitoringXPO.RmQty - _locPurchaseRequisitionMonitoringXPO.Qty;
                        }

                        if (_locPurchaseRequisitionMonitoringXPO.MxDQty > 0 || _locPurchaseRequisitionMonitoringXPO.MxQty > 0)
                        {
                            _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                       (new GroupOperator(GroupOperatorType.And,
                                                        new BinaryOperator("Item", _locPurchaseRequisitionMonitoringXPO.Item),
                                                        new BinaryOperator("UOM", _locPurchaseRequisitionMonitoringXPO.MxUOM),
                                                        new BinaryOperator("DefaultUOM", _locPurchaseRequisitionMonitoringXPO.MxDUOM),
                                                        new BinaryOperator("Active", true)));
                        }
                        else
                        {
                            _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                       (new GroupOperator(GroupOperatorType.And,
                                                        new BinaryOperator("Item", _locPurchaseRequisitionMonitoringXPO.Item),
                                                        new BinaryOperator("UOM", _locPurchaseRequisitionMonitoringXPO.UOM),
                                                        new BinaryOperator("DefaultUOM", _locPurchaseRequisitionMonitoringXPO.DUOM),
                                                        new BinaryOperator("Active", true)));
                        }


                        if (_locItemUOM != null)
                        {
                            if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                            {
                                _locInvLineTotal = _locRmQty * _locItemUOM.DefaultConversion + _locRmDQty;
                            }
                            else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                            {
                                _locInvLineTotal = _locRmQty / _locItemUOM.Conversion + _locRmDQty;
                            }
                            else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                            {
                                _locInvLineTotal = _locRmQty + _locRmDQty;
                            }
                        }
                        else
                        {
                            _locInvLineTotal = _locRmQty + _locRmDQty;
                        }

                    }
                    #endregion ProcessCount>0

                    _locPurchaseRequisitionMonitoringXPO.RmDQty = _locRmDQty;
                    _locPurchaseRequisitionMonitoringXPO.RmQty = _locRmQty;
                    _locPurchaseRequisitionMonitoringXPO.RmTQty = _locInvLineTotal;
                    _locPurchaseRequisitionMonitoringXPO.Save();
                    _locPurchaseRequisitionMonitoringXPO.Session.CommitTransaction();

                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PurchaseOrder " + ex.ToString());
            }
        }

        private void SetPostingQty(Session _currSession, PurchaseRequisitionMonitoring _locPurchaseRequisitionMonitoringXPO)
        {
            try
            {
                if (_locPurchaseRequisitionMonitoringXPO != null)
                {
                    double _locPDQty = 0;
                    double _locPQty = 0;
                    double _locInvLineTotal = 0;
                    ItemUnitOfMeasure _locItemUOM = null;

                    #region ProcessCount=0
                    if (_locPurchaseRequisitionMonitoringXPO.PostedCount == 0)
                    {
                        #region MaxQuantity
                        if (_locPurchaseRequisitionMonitoringXPO.MxDQty > 0)
                        {
                            if (_locPurchaseRequisitionMonitoringXPO.DQty > 0 && _locPurchaseRequisitionMonitoringXPO.DQty <= _locPurchaseRequisitionMonitoringXPO.MxDQty)
                            {
                                _locPDQty = _locPurchaseRequisitionMonitoringXPO.DQty;
                            }

                            if (_locPurchaseRequisitionMonitoringXPO.Qty > 0 && _locPurchaseRequisitionMonitoringXPO.Qty <= _locPurchaseRequisitionMonitoringXPO.MxQty)
                            {
                                _locPQty = _locPurchaseRequisitionMonitoringXPO.Qty;
                            }


                            _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                       (new GroupOperator(GroupOperatorType.And,
                                                        new BinaryOperator("Item", _locPurchaseRequisitionMonitoringXPO.Item),
                                                        new BinaryOperator("UOM", _locPurchaseRequisitionMonitoringXPO.MxUOM),
                                                        new BinaryOperator("DefaultUOM", _locPurchaseRequisitionMonitoringXPO.MxDUOM),
                                                        new BinaryOperator("Active", true)));
                            if (_locItemUOM != null)
                            {
                                if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                {
                                    _locInvLineTotal = _locPQty * _locItemUOM.DefaultConversion + _locPDQty;
                                }
                                else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                {
                                    _locInvLineTotal = _locPQty / _locItemUOM.Conversion + _locPDQty;
                                }
                                else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                {
                                    _locInvLineTotal = _locPQty + _locPDQty;
                                }
                            }
                            else
                            {
                                _locInvLineTotal = _locPQty + _locPDQty;
                            }
                        }
                        #endregion MaxQuantity
                        #region NonMaxQuantity
                        else
                        {
                            if (_locPurchaseRequisitionMonitoringXPO.DQty > 0)
                            {
                                _locPDQty = _locPurchaseRequisitionMonitoringXPO.DQty;
                            }

                            if (_locPurchaseRequisitionMonitoringXPO.Qty > 0)
                            {
                                _locPQty = _locPurchaseRequisitionMonitoringXPO.Qty;
                            }


                            _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                       (new GroupOperator(GroupOperatorType.And,
                                                        new BinaryOperator("Item", _locPurchaseRequisitionMonitoringXPO.Item),
                                                        new BinaryOperator("UOM", _locPurchaseRequisitionMonitoringXPO.UOM),
                                                        new BinaryOperator("DefaultUOM", _locPurchaseRequisitionMonitoringXPO.DUOM),
                                                        new BinaryOperator("Active", true)));
                            if (_locItemUOM != null)
                            {
                                if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                {
                                    _locInvLineTotal = _locPQty * _locItemUOM.DefaultConversion + _locPDQty;
                                }
                                else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                {
                                    _locInvLineTotal = _locPQty / _locItemUOM.Conversion + _locPDQty;
                                }
                                else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                {
                                    _locInvLineTotal = _locPQty + _locPDQty;
                                }
                            }
                            else
                            {
                                _locInvLineTotal = _locPQty + _locPDQty;
                            }
                        }
                        #endregion NonMaxQuantity
                    }
                    #endregion ProcessCount=0

                    #region ProcessCount>0
                    if (_locPurchaseRequisitionMonitoringXPO.PostedCount > 0)
                    {
                        if (_locPurchaseRequisitionMonitoringXPO.PDQty > 0)
                        {
                            _locPDQty = _locPurchaseRequisitionMonitoringXPO.PDQty + _locPurchaseRequisitionMonitoringXPO.DQty;
                        }

                        if (_locPurchaseRequisitionMonitoringXPO.PQty > 0)
                        {
                            _locPQty = _locPurchaseRequisitionMonitoringXPO.PQty + _locPurchaseRequisitionMonitoringXPO.Qty;
                        }

                        if (_locPurchaseRequisitionMonitoringXPO.MxDQty > 0 || _locPurchaseRequisitionMonitoringXPO.MxQty > 0)
                        {
                            _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                   (new GroupOperator(GroupOperatorType.And,
                                                    new BinaryOperator("Item", _locPurchaseRequisitionMonitoringXPO.Item),
                                                    new BinaryOperator("UOM", _locPurchaseRequisitionMonitoringXPO.MxUOM),
                                                    new BinaryOperator("DefaultUOM", _locPurchaseRequisitionMonitoringXPO.MxDUOM),
                                                    new BinaryOperator("Active", true)));
                        }
                        else
                        {
                            _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                   (new GroupOperator(GroupOperatorType.And,
                                                    new BinaryOperator("Item", _locPurchaseRequisitionMonitoringXPO.Item),
                                                    new BinaryOperator("UOM", _locPurchaseRequisitionMonitoringXPO.UOM),
                                                    new BinaryOperator("DefaultUOM", _locPurchaseRequisitionMonitoringXPO.DUOM),
                                                    new BinaryOperator("Active", true)));
                        }

                        if (_locItemUOM != null)
                        {
                            if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                            {
                                _locInvLineTotal = _locPQty * _locItemUOM.DefaultConversion + _locPDQty;
                            }
                            else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                            {
                                _locInvLineTotal = _locPQty / _locItemUOM.Conversion + _locPDQty;
                            }
                            else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                            {
                                _locInvLineTotal = _locPQty + _locPDQty;
                            }
                        }
                        else
                        {
                            _locInvLineTotal = _locPQty + _locPDQty;
                        }

                    }
                    #endregion ProcessCount>0

                    _locPurchaseRequisitionMonitoringXPO.PDQty = _locPDQty;
                    _locPurchaseRequisitionMonitoringXPO.PDUOM = _locPurchaseRequisitionMonitoringXPO.DUOM;
                    _locPurchaseRequisitionMonitoringXPO.PQty = _locPQty;
                    _locPurchaseRequisitionMonitoringXPO.PUOM = _locPurchaseRequisitionMonitoringXPO.UOM;
                    _locPurchaseRequisitionMonitoringXPO.PTQty = _locInvLineTotal;
                    _locPurchaseRequisitionMonitoringXPO.Save();
                    _locPurchaseRequisitionMonitoringXPO.Session.CommitTransaction();

                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PurchaseOrder " + ex.ToString());
            }
        }

        private void SetProcessCount(Session _currSession, PurchaseRequisitionMonitoring _locPurchaseRequisitionMonitoringXPO)
        {
            try
            {
                if (_locPurchaseRequisitionMonitoringXPO != null)
                {
                    if (_locPurchaseRequisitionMonitoringXPO.Status == Status.Open ||_locPurchaseRequisitionMonitoringXPO.Status == Status.Progress || _locPurchaseRequisitionMonitoringXPO.Status == Status.Posted)
                    {
                        _locPurchaseRequisitionMonitoringXPO.PostedCount = _locPurchaseRequisitionMonitoringXPO.PostedCount + 1;
                        _locPurchaseRequisitionMonitoringXPO.Save();
                        _locPurchaseRequisitionMonitoringXPO.Session.CommitTransaction();
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PurchaseOrder " + ex.ToString());
            }
        }

        private void SetStatusPurchaseRequisitionMonitoring(Session _currSession, PurchaseRequisitionMonitoring _locPurchaseRequisitionMonitoringXPO)
        {
            try
            {
                DateTime now = DateTime.Now;
                if (_locPurchaseRequisitionMonitoringXPO != null)
                {
                    if (_locPurchaseRequisitionMonitoringXPO.Status == Status.Open || _locPurchaseRequisitionMonitoringXPO.Status == Status.Progress || _locPurchaseRequisitionMonitoringXPO.Status == Status.Posted)
                    {
                        if (_locPurchaseRequisitionMonitoringXPO.RmDQty == 0 && _locPurchaseRequisitionMonitoringXPO.RmQty == 0 && _locPurchaseRequisitionMonitoringXPO.RmTQty == 0)
                        {
                            _locPurchaseRequisitionMonitoringXPO.Status = Status.Close;
                            _locPurchaseRequisitionMonitoringXPO.ActivationPosting = true;
                            _locPurchaseRequisitionMonitoringXPO.StatusDate = now;
                        }
                        else
                        {
                            _locPurchaseRequisitionMonitoringXPO.Status = Status.Posted;
                            _locPurchaseRequisitionMonitoringXPO.StatusDate = now;
                        }
                        _locPurchaseRequisitionMonitoringXPO.Save();
                        _locPurchaseRequisitionMonitoringXPO.Session.CommitTransaction();
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PurchaseOrder " + ex.ToString());
            }
        }

        private void SetNormalQuantity(Session _currSession, PurchaseRequisitionMonitoring _locPurchaseRequisitionMonitoringXPO)
        {
            try
            {
                if (_locPurchaseRequisitionMonitoringXPO != null)
                {
                    if (_locPurchaseRequisitionMonitoringXPO.Status == Status.Open || _locPurchaseRequisitionMonitoringXPO.Status == Status.Progress || _locPurchaseRequisitionMonitoringXPO.Status == Status.Posted || _locPurchaseRequisitionMonitoringXPO.Status == Status.Close)
                    {
                        if (_locPurchaseRequisitionMonitoringXPO.DQty > 0 || _locPurchaseRequisitionMonitoringXPO.Qty > 0)
                        {
                            _locPurchaseRequisitionMonitoringXPO.Select = false;
                            _locPurchaseRequisitionMonitoringXPO.DQty = 0;
                            _locPurchaseRequisitionMonitoringXPO.Qty = 0;
                            _locPurchaseRequisitionMonitoringXPO.PurchaseOrder = null;
                            _locPurchaseRequisitionMonitoringXPO.Save();
                            _locPurchaseRequisitionMonitoringXPO.Session.CommitTransaction();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PurchaseOrder " + ex.ToString());
            }
        }

        #endregion GetPR

        #region GetTotalAmount

        private void SetTotalAmountInPO(Session _currSession, PurchaseOrder _locPurchaseOrderXPO)
        {
            try
            {
                double _locTAmount = 0;
                double _locTUAmount = 0;
                double _locTxAmount = 0;
                double _locDiscAmount = 0;

                if (_locPurchaseOrderXPO != null)
                {
                    XPCollection<PurchaseOrderLine> _locPurchaseOrderLines = new XPCollection<BusinessObjects.PurchaseOrderLine>
                                                                                (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                new BinaryOperator("PurchaseOrder", _locPurchaseOrderXPO),
                                                                                new GroupOperator(GroupOperatorType.Or,
                                                                                new BinaryOperator("Status", Status.Open),
                                                                                new BinaryOperator("Status", Status.Progress))));
                    if (_locPurchaseOrderLines != null && _locPurchaseOrderLines.Count() > 0)
                    {
                        foreach (PurchaseOrderLine _locPurchaseOrderLine in _locPurchaseOrderLines)
                        {
                            #region Tax=nullDiscount=null
                            if (_locPurchaseOrderLine.TaxRule == TaxRule.AfterDiscount && _locPurchaseOrderLine.MultiTax == false
                                && _locPurchaseOrderLine.Tax == null && _locPurchaseOrderLine.Discount == null)
                            {
                                _locTAmount = _locTAmount + _locPurchaseOrderLine.TAmount;
                                _locTUAmount = _locTUAmount + _locPurchaseOrderLine.TUAmount;
                            }
                            else if ((_locPurchaseOrderLine.TaxRule == TaxRule.BeforeDiscount || _locPurchaseOrderLine.TaxRule == TaxRule.None)
                                && _locPurchaseOrderLine.MultiTax == false && _locPurchaseOrderLine.Tax == null && _locPurchaseOrderLine.Discount == null)
                            {
                                _locTAmount = _locTAmount + _locPurchaseOrderLine.TAmount;
                                _locTUAmount = _locTUAmount + _locPurchaseOrderLine.TUAmount;
                            }
                            #endregion Tax=nullDiscount=null
                            #region AfterDiscount
                            else if (_locPurchaseOrderLine.TaxRule == TaxRule.AfterDiscount && _locPurchaseOrderLine.MultiTax == false
                                && _locPurchaseOrderLine.Tax != null && _locPurchaseOrderLine.Discount != null)
                            {
                                if (_locPurchaseOrderLine.Tax.TaxNature == TaxNature.Increase)
                                {
                                    _locTxAmount = _locTxAmount + _locPurchaseOrderLine.TxAmount;
                                }
                                if (_locPurchaseOrderLine.Tax.TaxNature == TaxNature.Decrease)
                                {
                                    _locTxAmount = _locTxAmount - _locPurchaseOrderLine.TxAmount;
                                }
                                if (_locPurchaseOrderLine.Disc > 0)
                                {
                                    _locDiscAmount = _locDiscAmount + _locPurchaseOrderLine.DiscAmount;
                                }
                                _locTAmount = _locTAmount + _locPurchaseOrderLine.TAmount;
                                _locTUAmount = _locTUAmount + _locPurchaseOrderLine.TUAmount;
                            }
                            else if (_locPurchaseOrderLine.TaxRule == TaxRule.AfterDiscount && _locPurchaseOrderLine.MultiTax == false
                                && _locPurchaseOrderLine.Tax != null && _locPurchaseOrderLine.Discount == null)
                            {
                                if (_locPurchaseOrderLine.Tax.TaxNature == TaxNature.Increase)
                                {
                                    _locTxAmount = _locTxAmount + _locPurchaseOrderLine.TxAmount;
                                }
                                if (_locPurchaseOrderLine.Tax.TaxNature == TaxNature.Decrease)
                                {
                                    _locTxAmount = _locTxAmount - _locPurchaseOrderLine.TxAmount;
                                }
                                _locTAmount = _locTAmount + _locPurchaseOrderLine.TAmount;
                                _locTUAmount = _locTUAmount + _locPurchaseOrderLine.TUAmount;
                            }
                            else if (_locPurchaseOrderLine.TaxRule == TaxRule.AfterDiscount && _locPurchaseOrderLine.MultiTax == false
                                && _locPurchaseOrderLine.Tax == null && _locPurchaseOrderLine.Discount != null)
                            {
                                if (_locPurchaseOrderLine.Disc > 0)
                                {
                                    _locDiscAmount = _locDiscAmount + _locPurchaseOrderLine.DiscAmount;
                                }
                                _locTAmount = _locTAmount + _locPurchaseOrderLine.TAmount;
                                _locTUAmount = _locTUAmount + _locPurchaseOrderLine.TUAmount;
                            }
                            else if (_locPurchaseOrderLine.TaxRule == TaxRule.AfterDiscount && _locPurchaseOrderLine.MultiTax == true
                                && _locPurchaseOrderLine.Tax == null && _locPurchaseOrderLine.Discount != null)
                            {
                                if (_locPurchaseOrderLine.TxValue > 0)
                                {
                                    _locTxAmount = _locTxAmount + _locPurchaseOrderLine.TxAmount;
                                }
                                if (_locPurchaseOrderLine.Disc > 0)
                                {
                                    _locDiscAmount = _locDiscAmount + _locPurchaseOrderLine.DiscAmount;
                                }
                                _locTAmount = _locTAmount + _locPurchaseOrderLine.TAmount;
                                _locTUAmount = _locTUAmount + _locPurchaseOrderLine.TUAmount;
                            }
                            else if (_locPurchaseOrderLine.TaxRule == TaxRule.AfterDiscount && _locPurchaseOrderLine.MultiTax == true
                                && _locPurchaseOrderLine.Tax == null && _locPurchaseOrderLine.Discount == null)
                            {
                                if (_locPurchaseOrderLine.TxValue > 0)
                                {
                                    _locTxAmount = _locTxAmount + _locPurchaseOrderLine.TxAmount;
                                }
                                _locTAmount = _locTAmount + _locPurchaseOrderLine.TAmount;
                                _locTUAmount = _locTUAmount + _locPurchaseOrderLine.TUAmount;
                            }
                            #endregion AfterDiscount
                            #region BeforeDiscount
                            else if ((_locPurchaseOrderLine.TaxRule == TaxRule.BeforeDiscount || _locPurchaseOrderLine.TaxRule == TaxRule.None)
                                && _locPurchaseOrderLine.MultiTax == false && _locPurchaseOrderLine.Tax != null && _locPurchaseOrderLine.Discount != null)
                            {
                                if (_locPurchaseOrderLine.Tax.TaxNature == TaxNature.Increase)
                                {
                                    _locTxAmount = _locTxAmount + _locPurchaseOrderLine.TxAmount;
                                }
                                if (_locPurchaseOrderLine.Tax.TaxNature == TaxNature.Decrease)
                                {
                                    _locTxAmount = _locTxAmount - _locPurchaseOrderLine.TxAmount;
                                }
                                if (_locPurchaseOrderLine.Disc > 0)
                                {
                                    _locDiscAmount = _locDiscAmount + _locPurchaseOrderLine.DiscAmount;
                                }
                                _locTAmount = _locTAmount + _locPurchaseOrderLine.TAmount;
                                _locTUAmount = _locTUAmount + _locPurchaseOrderLine.TUAmount;
                            }
                            else if ((_locPurchaseOrderLine.TaxRule == TaxRule.BeforeDiscount || _locPurchaseOrderLine.TaxRule == TaxRule.None)
                                && _locPurchaseOrderLine.MultiTax == false && _locPurchaseOrderLine.Tax != null && _locPurchaseOrderLine.Discount == null)
                            {
                                if (_locPurchaseOrderLine.Tax.TaxNature == TaxNature.Increase)
                                {
                                    _locTxAmount = _locTxAmount + _locPurchaseOrderLine.TxAmount;
                                }
                                if (_locPurchaseOrderLine.Tax.TaxNature == TaxNature.Decrease)
                                {
                                    _locTxAmount = _locTxAmount - _locPurchaseOrderLine.TxAmount;
                                }
                                _locTAmount = _locTAmount + _locPurchaseOrderLine.TAmount;
                                _locTUAmount = _locTUAmount + _locPurchaseOrderLine.TUAmount;
                            }
                            else if ((_locPurchaseOrderLine.TaxRule == TaxRule.BeforeDiscount || _locPurchaseOrderLine.TaxRule == TaxRule.None)
                                && _locPurchaseOrderLine.MultiTax == false && _locPurchaseOrderLine.Tax == null && _locPurchaseOrderLine.Discount != null)
                            {
                                if (_locPurchaseOrderLine.Disc > 0)
                                {
                                    _locDiscAmount = _locDiscAmount + _locPurchaseOrderLine.DiscAmount;
                                }
                                _locTAmount = _locTAmount + _locPurchaseOrderLine.TAmount;
                                _locTUAmount = _locTUAmount + _locPurchaseOrderLine.TUAmount;
                            }
                            else if ((_locPurchaseOrderLine.TaxRule == TaxRule.BeforeDiscount || _locPurchaseOrderLine.TaxRule == TaxRule.None)
                                && _locPurchaseOrderLine.MultiTax == true && _locPurchaseOrderLine.Tax == null && _locPurchaseOrderLine.Discount != null)
                            {
                                if (_locPurchaseOrderLine.TxValue > 0)
                                {
                                    _locTxAmount = _locTxAmount + _locPurchaseOrderLine.TxAmount;
                                }
                                if (_locPurchaseOrderLine.Disc > 0)
                                {
                                    _locDiscAmount = _locDiscAmount + _locPurchaseOrderLine.DiscAmount;
                                }
                                _locTAmount = _locTAmount + _locPurchaseOrderLine.TAmount;
                                _locTUAmount = _locTUAmount + _locPurchaseOrderLine.TUAmount;
                            }
                            else if ((_locPurchaseOrderLine.TaxRule == TaxRule.BeforeDiscount || _locPurchaseOrderLine.TaxRule == TaxRule.None)
                                && _locPurchaseOrderLine.MultiTax == true && _locPurchaseOrderLine.Tax == null && _locPurchaseOrderLine.Discount == null)
                            {
                                if (_locPurchaseOrderLine.TxValue > 0)
                                {
                                    _locTxAmount = _locTxAmount + _locPurchaseOrderLine.TxAmount;
                                }
                                _locTAmount = _locTAmount + _locPurchaseOrderLine.TAmount;
                                _locTUAmount = _locTUAmount + _locPurchaseOrderLine.TUAmount;
                            }
                            #endregion BeforeDiscount

                        }
                    }

                    _locPurchaseOrderXPO.TotUnitAmount = _locTUAmount;
                    _locPurchaseOrderXPO.TotAmount = _locTAmount;
                    _locPurchaseOrderXPO.TotDiscAmount = _locDiscAmount;
                    _locPurchaseOrderXPO.TotTaxAmount = _locTxAmount;
                    _locPurchaseOrderXPO.Amount = _locTAmount;
                    _locPurchaseOrderXPO.Save();
                    _locPurchaseOrderXPO.Session.CommitTransaction();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = PurchaseOrder ", ex.ToString());
            }
        }

        #endregion GetTotalAmount

        #region PostingMethod

        private void SetPurchaseOrderMonitoring(Session _currSession, PurchaseOrder _locPurchaseOrderXPO)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                DateTime now = DateTime.Now;

                if (_locPurchaseOrderXPO != null)
                {
                    if (_locPurchaseOrderXPO.Status == Status.Progress || _locPurchaseOrderXPO.Status == Status.Posted)
                    {
                        XPCollection<PurchaseOrderLine> _locPurchaseOrderLines = new XPCollection<PurchaseOrderLine>
                                                            (_currSession, new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("PurchaseOrder", _locPurchaseOrderXPO),
                                                            new BinaryOperator("Select", true),
                                                            new GroupOperator(GroupOperatorType.Or,
                                                            new BinaryOperator("Status", Status.Progress),
                                                            new BinaryOperator("Status", Status.Posted))));

                        if (_locPurchaseOrderLines != null && _locPurchaseOrderLines.Count() > 0)
                        {
                            foreach (PurchaseOrderLine _locPurchaseOrderLine in _locPurchaseOrderLines)
                            {
                                if(_locPurchaseOrderLine.TAmount > 0)
                                {
                                    #region SavePurchaseOrderMonitoring
                                    PurchaseOrderMonitoring _saveDataPurchaseOrderMonitoring = new PurchaseOrderMonitoring(_currSession)
                                    {
                                        PurchaseOrder = _locPurchaseOrderXPO,
                                        PurchaseOrderLine = _locPurchaseOrderLine,
                                        PurchaseType = _locPurchaseOrderLine.PurchaseType,
                                        Item = _locPurchaseOrderLine.Item,
                                        Brand = _locPurchaseOrderLine.Brand,
                                        Description = _locPurchaseOrderLine.Description,
                                        MxDQty = _locPurchaseOrderLine.DQty,
                                        MxDUOM = _locPurchaseOrderLine.DUOM,
                                        MxQty = _locPurchaseOrderLine.Qty,
                                        MxUOM = _locPurchaseOrderLine.UOM,
                                        MxTQty = _locPurchaseOrderLine.TQty,
                                        DQty = _locPurchaseOrderLine.DQty,
                                        DUOM = _locPurchaseOrderLine.DUOM,
                                        Qty = _locPurchaseOrderLine.Qty,
                                        UOM = _locPurchaseOrderLine.UOM,
                                        TQty = _locPurchaseOrderLine.TQty,
                                        Currency = _locPurchaseOrderLine.Currency,
                                        UAmount = _locPurchaseOrderLine.UAmount,
                                        TUAmount = _locPurchaseOrderLine.TUAmount,
                                        TaxRule = _locPurchaseOrderLine.TaxRule,
                                        MultiTax = _locPurchaseOrderLine.MultiTax,
                                        Tax = _locPurchaseOrderLine.Tax,
                                        TxValue = _locPurchaseOrderLine.TxValue,
                                        TxAmount = _locPurchaseOrderLine.TxAmount,
                                        Discount = _locPurchaseOrderLine.Discount,
                                        Disc = _locPurchaseOrderLine.Disc,
                                        DiscAmount = _locPurchaseOrderLine.DiscAmount,
                                        TAmount = _locPurchaseOrderLine.TAmount,
                                        Company = _locPurchaseOrderLine.Company,
                                        Workplace = _locPurchaseOrderLine.Workplace,
                                        Division = _locPurchaseOrderLine.Division,
                                        Department = _locPurchaseOrderLine.Department,
                                        Section = _locPurchaseOrderLine.Section,
                                        Employee = _locPurchaseOrderLine.Employee,
                                        Div = _locPurchaseOrderLine.Div,
                                        Dept = _locPurchaseOrderLine.Dept,
                                        Sect = _locPurchaseOrderLine.Sect,
                                        DocDate = _locPurchaseOrderLine.DocDate,
                                        JournalMonth = _locPurchaseOrderLine.JournalMonth,
                                        JournalYear = _locPurchaseOrderLine.JournalYear,
                                    };
                                    _saveDataPurchaseOrderMonitoring.Save();
                                    _saveDataPurchaseOrderMonitoring.Session.CommitTransaction();
                                    #endregion SavePurchaseOrderMonitoring
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = PurchaseOrder ", ex.ToString());
            }
        }

        private void SetCostingMethod(Session _currSession, PurchaseOrder _locPurchaseOrderXPO)
        {
            try
            {
                if (_locPurchaseOrderXPO != null)
                {
                    if (_locPurchaseOrderXPO.Company != null && _locPurchaseOrderXPO.Workplace != null)
                    {
                        XPCollection<PurchaseOrderLine> _locPurchaseOrderLines = new XPCollection<PurchaseOrderLine>
                                                            (_currSession, new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("PurchaseOrder", _locPurchaseOrderXPO),
                                                            new BinaryOperator("Select", true),
                                                            new GroupOperator(GroupOperatorType.Or,
                                                            new BinaryOperator("Status", Status.Progress),
                                                            new BinaryOperator("Status", Status.Posted))));

                        JournalSetupDetail _locJournalSetupDetail = _currSession.FindObject<JournalSetupDetail>
                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                    new BinaryOperator("Company", _locPurchaseOrderXPO.Company),
                                                                    new BinaryOperator("Workplace", _locPurchaseOrderXPO.Workplace),
                                                                    new BinaryOperator("JournalSetupType", JournalSetupType.Costing),
                                                                    new BinaryOperator("ObjectList", ObjectList.PurchaseOrder),
                                                                    new BinaryOperator("Active", true)));

                        if (_locJournalSetupDetail != null)
                        {
                            #region Average
                            if( _locJournalSetupDetail.CostingMethod == CostingMethod.Average )
                            {
                                if(_locPurchaseOrderLines != null && _locPurchaseOrderLines.Count() > 0)
                                {
                                    foreach(PurchaseOrderLine _locPurchaseOrderLine in _locPurchaseOrderLines)
                                    {
                                        PriceCostMethod _locPriceCostMethod = _currSession.FindObject<PriceCostMethod>(new GroupOperator(GroupOperatorType.And,
                                                                                                        new BinaryOperator("Company", _locPurchaseOrderLine.Company),
                                                                                                        new BinaryOperator("Workplace", _locPurchaseOrderLine.Workplace),
                                                                                                        new BinaryOperator("Item", _locPurchaseOrderLine.Item),
                                                                                                        new BinaryOperator("Active", true)));
                                        if(_locPriceCostMethod != null)
                                        {
                                            if(_locPriceCostMethod.Amount1a > 0)
                                            {
                                                _locPriceCostMethod.Amount1a = (_locPriceCostMethod.Amount1a + _locPurchaseOrderLine.UAmount) / 2;
                                            }
                                            else
                                            {
                                                _locPriceCostMethod.Amount1a = _locPurchaseOrderLine.UAmount;
                                            }
                                            _locPriceCostMethod.Save();
                                            _locPriceCostMethod.Session.CommitTransaction();
                                        }
                                    }
                                }
                            }
                            #endregion Average
                            #region LastInFirstOut
                            if (_locJournalSetupDetail.CostingMethod == CostingMethod.LastInFirstOut)
                            {
                                if (_locPurchaseOrderLines != null && _locPurchaseOrderLines.Count() > 0)
                                {
                                    foreach (PurchaseOrderLine _locPurchaseOrderLine in _locPurchaseOrderLines)
                                    {
                                        PriceCostMethod _locPriceCostMethod = _currSession.FindObject<PriceCostMethod>(new GroupOperator(GroupOperatorType.And,
                                                                                                        new BinaryOperator("Company", _locPurchaseOrderLine.Company),
                                                                                                        new BinaryOperator("Workplace", _locPurchaseOrderLine.Workplace),
                                                                                                        new BinaryOperator("Item", _locPurchaseOrderLine.Item),
                                                                                                        new BinaryOperator("Active", true)));
                                        if (_locPriceCostMethod != null)
                                        {
                                            _locPriceCostMethod.Amount1a = _locPurchaseOrderLine.UAmount;
                                            _locPriceCostMethod.Save();
                                            _locPriceCostMethod.Session.CommitTransaction();
                                        }
                                    }
                                }
                            }
                            #endregion LastInFirstOut
                            #region FirstInFirstOut
                            if (_locJournalSetupDetail.CostingMethod == CostingMethod.FirstInFirstOut)
                            {
                                if (_locPurchaseOrderLines != null && _locPurchaseOrderLines.Count() > 0)
                                {
                                    foreach (PurchaseOrderLine _locPurchaseOrderLine in _locPurchaseOrderLines)
                                    {
                                        PriceCostMethod _locPriceCostMethod = _currSession.FindObject<PriceCostMethod>(new GroupOperator(GroupOperatorType.And,
                                                                                                        new BinaryOperator("Company", _locPurchaseOrderLine.Company),
                                                                                                        new BinaryOperator("Workplace", _locPurchaseOrderLine.Workplace),
                                                                                                        new BinaryOperator("Item", _locPurchaseOrderLine.Item),
                                                                                                        new BinaryOperator("Active", true)));
                                        if (_locPriceCostMethod != null)
                                        {
                                            if(_locPriceCostMethod.Amount1a == 0)
                                            {
                                                _locPriceCostMethod.Amount1a = _locPurchaseOrderLine.UAmount;
                                                _locPriceCostMethod.Save();
                                                _locPriceCostMethod.Session.CommitTransaction();
                                            }        
                                        }
                                    }
                                }
                            }
                            #endregion FirstInFirstOut
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = PurchaseOrder ", ex.ToString());
            }
        }

        private void SetStatusPurchaseOrderLine(Session _currSession, PurchaseOrder _locPurchaseOrderXPO)
        {
            try
            {
                DateTime now = DateTime.Now;
                if (_locPurchaseOrderXPO != null)
                {
                    XPCollection<PurchaseOrderLine> _locPurchaseOrderLines = new XPCollection<PurchaseOrderLine>(_currSession,
                                                            new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("PurchaseOrder", _locPurchaseOrderXPO),
                                                            new BinaryOperator("Select", true),
                                                            new GroupOperator(GroupOperatorType.Or,
                                                            new BinaryOperator("Status", Status.Progress),
                                                            new BinaryOperator("Status", Status.Posted))));

                    if (_locPurchaseOrderLines != null && _locPurchaseOrderLines.Count > 0)
                    {
                        foreach (PurchaseOrderLine _locPurchaseOrderLine in _locPurchaseOrderLines)
                        {
                            _locPurchaseOrderLine.Status = Status.Close;
                            _locPurchaseOrderLine.ActivationPosting = true;
                            _locPurchaseOrderLine.StatusDate = now;
                            _locPurchaseOrderLine.Save();
                            _locPurchaseOrderLine.Session.CommitTransaction();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PurchaseOrder " + ex.ToString());
            }
        }

        private void SetNormal(Session _currSession, PurchaseOrder _locPurchaseOrderXPO)
        {
            try
            {
                if (_locPurchaseOrderXPO != null)
                {
                    XPCollection<PurchaseOrderLine> _locPurchaseOrderLines = new XPCollection<PurchaseOrderLine>(_currSession,
                                                                                     new GroupOperator(GroupOperatorType.And,
                                                                                     new BinaryOperator("PurchaseOrder", _locPurchaseOrderXPO),
                                                                                     new BinaryOperator("Select", true),
                                                                                     new GroupOperator(GroupOperatorType.Or,
                                                                                     new BinaryOperator("Status", Status.Progress),
                                                                                     new BinaryOperator("Status", Status.Posted),
                                                                                     new BinaryOperator("Status", Status.Close))));

                    if (_locPurchaseOrderLines != null && _locPurchaseOrderLines.Count > 0)
                    {
                        foreach (PurchaseOrderLine _locPurchaseOrderLine in _locPurchaseOrderLines)
                        {
                            _locPurchaseOrderLine.Select = false;
                            _locPurchaseOrderLine.Save();
                            _locPurchaseOrderLine.Session.CommitTransaction();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PurchaseOrder " + ex.ToString());
            }
        }

        private void SetFinalStatusPurchaseOrder(Session _currSession, PurchaseOrder _locPurchaseOrderXPO)
        {
            try
            {
                DateTime now = DateTime.Now;

                if (_locPurchaseOrderXPO != null)
                {
                    XPCollection<PurchaseOrderLine> _locPurchaseOrderLines = new XPCollection<PurchaseOrderLine>(_currSession,
                                                            new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("PurchaseOrder", _locPurchaseOrderXPO)));

                    if (_locPurchaseOrderLines != null && _locPurchaseOrderLines.Count() > 0)
                    {
                        foreach (PurchaseOrderLine _locPurchaseOrderLine in _locPurchaseOrderLines)
                        {
                            if (_locPurchaseOrderLine.Status == Status.Close)
                            {
                                _locPurchaseOrderXPO.ActivationPosting = true;
                                _locPurchaseOrderXPO.Status = Status.Posted;
                                _locPurchaseOrderXPO.StatusDate = now;
                                _locPurchaseOrderXPO.Save();
                                _locPurchaseOrderXPO.Session.CommitTransaction();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PurchaseOrder " + ex.ToString());
            }
        }

        #endregion PostingMethod       

        #region Global Method

        private void SuccessMessageShow(string _locActionName)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 2000;
            options.Message = string.Format("{0} !", _locActionName);
            options.Type = InformationType.Success;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Success";
            options.Win.Type = WinMessageType.Flyout;
            Application.ShowViewStrategy.ShowMessage(options);
        }

        private void ErrorMessageShow(string _locMessage)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 2000;
            options.Message = string.Format(_locMessage);
            options.Type = InformationType.Warning;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Error";
            options.Win.Type = WinMessageType.Alert;
            Application.ShowViewStrategy.ShowMessage(options);
        }

        #endregion Global Method

        #region Approval Process

        private void SetApprovalLine(Session _currentSession, PurchaseOrder _locPurchaseOrderXPO, ApprovalLevel _locApprovalLevel)
        {
            try
            {
                DateTime now = DateTime.Now;

                ApprovalLine _locApprovalLineXPO = _currentSession.FindObject<ApprovalLine>
                                                                        (new GroupOperator(GroupOperatorType.And,
                                                                         new BinaryOperator("PurchaseOrder", _locPurchaseOrderXPO),
                                                                         new BinaryOperator("ApprovalLevel", _locApprovalLevel)));

                if (_locApprovalLineXPO == null)
                {
                    ApprovalLine _saveDataAL2 = new ApprovalLine(_currentSession)
                    {
                        ApprovalDate = now,
                        ApprovalStatus = Status.Approved,
                        ApprovalLevel = _locApprovalLevel,
                        PurchaseOrder = _locPurchaseOrderXPO,
                    };
                    _saveDataAL2.Save();
                    _saveDataAL2.Session.CommitTransaction();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PurchaseOrder " + ex.ToString());
            }
        }

        #endregion Approval Process

        #region Email

        private string BackgroundBody(Session _currentSession, PurchaseOrder _locPurchaseOrderXPO, MailSetupDetail _locMailSetDetail)
        {
            string body = string.Empty;

            PurchaseOrder _locPurchaseOrders = _currentSession.FindObject<PurchaseOrder>
                                           (new GroupOperator(GroupOperatorType.And,
                                            new BinaryOperator("Code", _locPurchaseOrderXPO.Code)));

            ApprovalLine _locApprovalLineXPO = _currentSession.FindObject<ApprovalLine>
                                               (new GroupOperator(GroupOperatorType.And,
                                                new BinaryOperator("PurchaseOrder", _locPurchaseOrders)));

            string Status = _locApprovalLineXPO.ApprovalStatus.ToString();

            using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/Email/EmailTemplate.html")))
            {
                body = reader.ReadToEnd();
            }
            body = body.Replace("{Code}", _locPurchaseOrderXPO.Code);

            #region Level
            if (_locPurchaseOrders.ActiveApproved1 == true)
            {
                string Active = "Level 1";
                body = body.Replace("{Level}", Active);
            }
            else if (_locPurchaseOrders.ActiveApproved2 == true)
            {
                string Active = "Level 2";
                body = body.Replace("{Level}", Active);
            }
            else if (_locPurchaseOrders.ActiveApproved3 == true)
            {
                string Active = "Level 3";
                body = body.Replace("{Level}", Active);
            }
            else
            {
                string Active = "Not Available";
                body = body.Replace("{Level}", Active);
            }
            #endregion Level

            body = body.Replace("{Status}", Status);
            body = body.Replace("{Message}", _locMailSetDetail.MessageBody);
            return body;
        }

        protected void SendEmail(Session _currentSession, PurchaseOrder _locPurchaseOrderXPO, Status _locStatus, ApprovalLine _locApprovalLineXPO2)
        {
            DateTime now = DateTime.Now;
            var User = SecuritySystem.CurrentUserName;
            GlobalFunction _globFunc = new GlobalFunction();
            UserAccess _numLineUserAccess = _currentSession.FindObject<UserAccess>(new BinaryOperator("UserName", User));
            string _locMessageBody = null;

            try
            {
                if (_locPurchaseOrderXPO != null)
                {
                    XPCollection<MailSetupDetail> _locMailSetDetails = new XPCollection<MailSetupDetail>
                                                                       (_currentSession, new GroupOperator(GroupOperatorType.And,
                                                                        new BinaryOperator("ObjectList", ObjectList.PurchaseOrder),
                                                                        new BinaryOperator("UserAccess", _numLineUserAccess),
                                                                        new BinaryOperator("Active", true)));

                    if (_locMailSetDetails != null && _locMailSetDetails.Count > 0)
                    {
                        foreach (MailSetupDetail _locMailSetDetail in _locMailSetDetails)
                        {
                            if (_locMailSetDetail.MessageBody != null)
                            {
                                if (_locApprovalLineXPO2.EndApproval == true)
                                {
                                    _locMessageBody = this.BackgroundBody(_currentSession, _locPurchaseOrderXPO, _locMailSetDetail);
                                }
                                else
                                {
                                    _locMessageBody = this.BackgroundBody(_currentSession, _locPurchaseOrderXPO, _locMailSetDetail);
                                }
                            }

                            _globFunc.SetAndSendMail(_locMailSetDetail.SmtpHost, _locMailSetDetail.SmtpPort,
                            _locMailSetDetail.MailFrom, _locMailSetDetail.MailFromPassword, _locMailSetDetail.MailTo, _locMailSetDetail.MailSubject, _locMessageBody);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PurchaseOrder " + ex.ToString());
            }
        }

        #endregion Email

    }
}
