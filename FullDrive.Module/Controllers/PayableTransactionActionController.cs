﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;

using FullDrive.Module.BusinessObjects;
using FullDrive.Module.CustomProcess;
using System.Collections;
using DevExpress.Xpo;
using DevExpress.ExpressApp.Xpo;

namespace FullDrive.Module.Controllers
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class PayableTransactionActionController : ViewController
    {
        private ChoiceActionItem _selectionListviewFilter;

        public PayableTransactionActionController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.
            #region Filter Status
            PayableTransactionListviewFilterSelectionAction.Items.Clear();
            foreach (object _currApproval in Enum.GetValues(typeof(CustomProcess.Status)))
            {
                EnumDescriptor _ed = new EnumDescriptor(typeof(CustomProcess.Status));
                _selectionListviewFilter = new ChoiceActionItem(_ed.GetCaption(_currApproval), _currApproval);
                PayableTransactionListviewFilterSelectionAction.Items.Add(_selectionListviewFilter);
            }
            #endregion Filter Status
        }

        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
        }

        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }

        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        private void PayableTransactionGetPIAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;

                if (_objectsToProcess != null)
                {
                    foreach (Object obj in _objectsToProcess)
                    {
                        PayableTransaction _locPayableTransactionOS = (PayableTransaction)_objectSpace.GetObject(obj);

                        if (_locPayableTransactionOS != null)
                        {
                            if(_locPayableTransactionOS.Session != null)
                            {
                                _currSession = _locPayableTransactionOS.Session;
                            }
                            if (_locPayableTransactionOS.Code != null && _currSession != null)
                            {
                                _currObjectId = _locPayableTransactionOS.Code;

                                PayableTransaction _locPayableTransactionXPO = _currSession.FindObject<PayableTransaction>
                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                     new BinaryOperator("Code", _currObjectId),
                                                                     new GroupOperator(GroupOperatorType.Or,
                                                                     new BinaryOperator("Status", Status.Open),
                                                                     new BinaryOperator("Status", Status.Progress))));

                                if (_locPayableTransactionXPO != null)
                                {
                                    XPCollection<PayablePurchaseCollection> _locPayablePurchaseCollections = new XPCollection<PayablePurchaseCollection>
                                                                                        (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                        new BinaryOperator("PayableTransaction", _locPayableTransactionXPO),
                                                                                        new GroupOperator(GroupOperatorType.Or,
                                                                                        new BinaryOperator("Status", Status.Open),
                                                                                        new BinaryOperator("Status", Status.Progress))));

                                    if (_locPayablePurchaseCollections != null && _locPayablePurchaseCollections.Count() > 0)
                                    {
                                        foreach (PayablePurchaseCollection _locPayablePurchaseCollection in _locPayablePurchaseCollections)
                                        {
                                            if (_locPayablePurchaseCollection.PayableTransaction != null && _locPayablePurchaseCollection.PurchaseInvoice != null)
                                            {
                                                GetPurchaseInvoiceMonitoring(_currSession, _locPayablePurchaseCollection.PurchaseInvoice, _locPayableTransactionXPO);
                                            }
                                        }
                                        SuccessMessageShow("Purchase Invoice Has Been Successfully Getting into Payable Transaction");
                                    }
                                }
                                else
                                {
                                    ErrorMessageShow("Purchase Invoice Not Available");
                                }
                            }
                            else
                            {
                                ErrorMessageShow("Purchase Invoice Not Available");
                            }
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PayableTransaction " + ex.ToString());
            }
        }

        private void PayableTransactionGetPPIAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;

                if (_objectsToProcess != null)
                {
                    foreach (Object obj in _objectsToProcess)
                    {
                        PayableTransaction _locPayableTransactionOS = (PayableTransaction)_objectSpace.GetObject(obj);

                        if (_locPayableTransactionOS != null)
                        {
                            if(_locPayableTransactionOS.Session != null)
                            {
                                _currSession = _locPayableTransactionOS.Session; 
                            }

                            if (_locPayableTransactionOS.Code != null && _currSession != null)
                            {
                                _currObjectId = _locPayableTransactionOS.Code;

                                PayableTransaction _locPayableTransactionXPO = _currSession.FindObject<PayableTransaction>
                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                     new BinaryOperator("Code", _currObjectId),
                                                                     new GroupOperator(GroupOperatorType.Or,
                                                                     new BinaryOperator("Status", Status.Open),
                                                                     new BinaryOperator("Status", Status.Progress))));

                                if (_locPayableTransactionXPO != null)
                                {
                                    XPCollection<PayablePurchaseCollection> _locPayablePurchaseCollections = new XPCollection<PayablePurchaseCollection>
                                                                                        (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                        new BinaryOperator("PayableTransaction", _locPayableTransactionXPO),
                                                                                        new GroupOperator(GroupOperatorType.Or,
                                                                                        new BinaryOperator("Status", Status.Open),
                                                                                        new BinaryOperator("Status", Status.Progress))));

                                    if (_locPayablePurchaseCollections != null && _locPayablePurchaseCollections.Count() > 0)
                                    {
                                        foreach (PayablePurchaseCollection _locPayablePurchaseCollection in _locPayablePurchaseCollections)
                                        {
                                            if (_locPayablePurchaseCollection.PayableTransaction != null && _locPayablePurchaseCollection.PurchasePrePaymentInvoice != null)
                                            {
                                                GetPurchasePrePaymentInvoiceMonitoring(_currSession, _locPayablePurchaseCollection.PurchasePrePaymentInvoice, _locPayableTransactionXPO);
                                            }
                                        }
                                        SuccessMessageShow("Purchase Pre Payment Invoice Has Been Successfully Getting into Payable Transaction");
                                    }
                                }
                                else
                                {
                                    ErrorMessageShow("Purchase Pre Payment Invoice Not Available");
                                }
                            }
                            else
                            {
                                ErrorMessageShow("Purchase Pre Payment Invoice Not Available");
                            }
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PayableTransaction " + ex.ToString());
            }
        }

        private void PayableTransactionProgressAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;

                if (_objectToProcess != null)
                {
                    foreach (Object obj in _objectToProcess)
                    {
                        PayableTransaction _locPayableTransactionOS = (PayableTransaction)_objectSpace.GetObject(obj);

                        if (_locPayableTransactionOS != null)
                        {
                            if(_locPayableTransactionOS.Session != null)
                            {
                                _currSession = _locPayableTransactionOS.Session;
                            }

                            if (_locPayableTransactionOS.Code != null && _currSession != null)
                            {
                                _currObjectId = _locPayableTransactionOS.Code;

                                PayableTransaction _locPayableTransactionXPO = _currSession.FindObject<PayableTransaction>
                                                                                 (new GroupOperator(GroupOperatorType.And,
                                                                                  new BinaryOperator("Code", _currObjectId),
                                                                                  new BinaryOperator("Status", Status.Open)));

                                if (_locPayableTransactionXPO != null)
                                {
                                    _locPayableTransactionXPO.Status = Status.Progress;
                                    _locPayableTransactionXPO.StatusDate = now;
                                    _locPayableTransactionXPO.Save();
                                    _locPayableTransactionXPO.Session.CommitTransaction();

                                    #region StatusPayableTransactionLine
                                    XPCollection<PayableTransactionLine> _locPayableTransactionLines = new XPCollection<PayableTransactionLine>
                                                                                                             (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                              new BinaryOperator("PayableTransaction", _locPayableTransactionXPO),
                                                                                                              new BinaryOperator("Status", Status.Open)));

                                    if (_locPayableTransactionLines != null && _locPayableTransactionLines.Count > 0)
                                    {
                                        foreach (PayableTransactionLine _locPayableTransactionLine in _locPayableTransactionLines)
                                        {
                                            _locPayableTransactionLine.Status = Status.Progress;
                                            _locPayableTransactionLine.StatusDate = now;
                                            _locPayableTransactionLine.Save();
                                            _locPayableTransactionLine.Session.CommitTransaction();
                                        }
                                    }
                                    #endregion StatusPayableTransactionLine

                                    #region StatusPayablePurchaseCollection
                                    XPCollection<PayablePurchaseCollection> _locPayableTransactionCollections = new XPCollection<PayablePurchaseCollection>
                                                                                                        (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                        new BinaryOperator("PayableTransaction", _locPayableTransactionXPO),
                                                                                                        new BinaryOperator("Status", Status.Open)));
                                    if (_locPayableTransactionCollections != null && _locPayableTransactionCollections.Count() > 0)
                                    {
                                        foreach (PayablePurchaseCollection _locPayableTransactionCollection in _locPayableTransactionCollections)
                                        {
                                            _locPayableTransactionCollection.Status = Status.Progress;
                                            _locPayableTransactionCollection.StatusDate = now;
                                            _locPayableTransactionCollection.Save();
                                            _locPayableTransactionCollection.Session.CommitTransaction();
                                        }
                                    }
                                    #endregion StatusPayablePurchaseCollection

                                    SuccessMessageShow(_locPayableTransactionXPO.Code + "Has been change successfully to Progress");
                                }
                                else
                                {
                                    ErrorMessageShow("Data Payable Transaction not available");
                                }
                            }
                            else
                            {
                                ErrorMessageShow("Data Payable Transaction not available");
                            }
                        }
                    }
                }
                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Business = PayableTransaction " + ex.ToString());
            }
        }

        private void PayableTransactionPostingAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;

                if (_objectsToProcess != null)
                {
                    foreach (Object obj in _objectsToProcess)
                    {
                        PayableTransaction _locPayableTransactionOS = (PayableTransaction)_objectSpace.GetObject(obj);

                        if (_locPayableTransactionOS != null)
                        {
                            if(_locPayableTransactionOS.Session != null)
                            {
                                _currSession = _locPayableTransactionOS.Session;
                            }

                            if (_locPayableTransactionOS.Code != null)
                            {
                                _currObjectId = _locPayableTransactionOS.Code;

                                PayableTransaction _locPayableTransactionXPO = _currSession.FindObject<PayableTransaction>
                                                                                     (new GroupOperator(GroupOperatorType.And,
                                                                                      new BinaryOperator("Code", _currObjectId),
                                                                                      new GroupOperator(GroupOperatorType.Or,
                                                                                      new BinaryOperator("Status", Status.Progress),
                                                                                      new BinaryOperator("Status", Status.Posted))));
                                if (_locPayableTransactionXPO != null)
                                {
                                    if (_locPayableTransactionXPO.PaymentMethodType == PaymentMethodType.Normal)
                                    {
                                        if (CheckMaksAmountDebitBasedPurchaseInvoiceMonitoring(_currSession, _locPayableTransactionXPO) == true)
                                        {
                                            if (CheckAmountDebitCreditBasedPurchaseInvoiceMonitoring(_currSession, _locPayableTransactionXPO) == true)
                                            {
                                                SetPayableTransactionMonitoringBasedPurchaseInvoiceMonitoring(_currSession, _locPayableTransactionXPO);
                                                if (CheckJournalSetup(_currSession, _locPayableTransactionXPO) == true)
                                                {
                                                    SetPayableJournal(_currSession, _locPayableTransactionXPO);
                                                }
                                                SetNormalPurchaseInvoiceMonitoring(_currSession, _locPayableTransactionXPO);
                                                SetStatusPayableTransactionLine(_currSession, _locPayableTransactionXPO);
                                                SetFinalStatusPayableTransaction(_currSession, _locPayableTransactionXPO);
                                                SetCloseAllPurchaseProcess(_currSession, _locPayableTransactionXPO);
                                                SetNormal(_currSession, _locPayableTransactionXPO);
                                                SuccessMessageShow(_locPayableTransactionXPO.Code + " has been change successfully to Posting");
                                            }
                                            else
                                            {
                                                ErrorMessageShow("Please Check Amount Transaction");
                                            }
                                        }
                                        else
                                        {
                                            ErrorMessageShow("Please Check Amount Transaction");
                                        }
                                    }
                                    else if (_locPayableTransactionXPO.PaymentMethodType == PaymentMethodType.DownPayment || _locPayableTransactionXPO.PaymentMethodType == PaymentMethodType.CashBeforeDelivery)
                                    {
                                        if (CheckMaksAmountDebitBasedPurchasePrePaymentInvoiceMonitoring(_currSession, _locPayableTransactionXPO) == true)
                                        {
                                            if (CheckAmountDebitCreditBasedPurchasePrePaymentInvoiceMonitoring(_currSession, _locPayableTransactionXPO) == true)
                                            {
                                                SetPayableTransactionMonitoringBasedPurchasePrePaymentInvoiceMonitoring(_currSession, _locPayableTransactionXPO);
                                                if (CheckJournalSetupPrePayment(_currSession, _locPayableTransactionXPO) == true)
                                                {
                                                    SetPayableJournalBasedPurchasePrePaymentInvoice(_currSession, _locPayableTransactionXPO);
                                                }
                                                SetNormalPurchasePrePaymentInvoiceMonitoring(_currSession, _locPayableTransactionXPO);
                                                SetStatusPayableTransactionLine(_currSession, _locPayableTransactionXPO);
                                                SetFinalStatusPayableTransaction(_currSession, _locPayableTransactionXPO);
                                                SetCloseAllPurchaseProcess(_currSession, _locPayableTransactionXPO);
                                                SetNormal(_currSession, _locPayableTransactionXPO);
                                                SuccessMessageShow(_locPayableTransactionXPO.Code + " has been change successfully to Posting");
                                            }
                                            else
                                            {
                                                ErrorMessageShow("Please Check Amount Transaction");
                                            }
                                        }
                                        else
                                        {
                                            ErrorMessageShow("Please Check Amount Transaction");
                                        }
                                    }
                                }
                                else
                                {
                                    ErrorMessageShow("Data Payable Transaction Not Available");
                                }
                            }
                            else
                            {
                                ErrorMessageShow("Data Payable Transaction Not Available");
                            }
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PayableTransaction " + ex.ToString());
            }
        }

        private void PayableTransactionListviewFilterSelectionAction_Execute(object sender, SingleChoiceActionExecuteEventArgs e)
        {
            try
            {
                if ((View is ListView) & (View.ObjectTypeInfo.Type == typeof(PayableTransaction)))
                {
                    if ((Status)e.SelectedChoiceActionItem.Data == Status.None)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.None, BinaryOperatorType.NotEqual);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Open)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Open, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Progress)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Progress, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Posted)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Posted, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Lock)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Lock, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Close)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Close, BinaryOperatorType.Equal);
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PayableTransaction " + ex.ToString());
            }
        }

        //========================================= Code In Here =========================================

        #region GetPI

        private void GetPurchaseInvoiceMonitoring(Session _currSession, PurchaseInvoice _locPurchaseInvoiceXPO, PayableTransaction _locPayableTransactionXPO)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                DateTime now = DateTime.Now;
                ChartOfAccount _locAccountForCompany = null;
                ChartOfAccount _locAccountForVendor = null;
                DateTime _locDocDate = now;
                int _locJournalMonth = 0;
                int _locJournalYear = 0;

                if (_locPurchaseInvoiceXPO != null && _locPayableTransactionXPO != null)
                {
                    XPCollection<PurchaseInvoiceMonitoring> _locPurchaseInvoiceMonitorings = new XPCollection<PurchaseInvoiceMonitoring>
                                                                                        (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                        new BinaryOperator("PurchaseInvoice", _locPurchaseInvoiceXPO),
                                                                                        new BinaryOperator("PayableTransaction", _locPayableTransactionXPO),
                                                                                        new BinaryOperator("Select", true),
                                                                                        new GroupOperator(GroupOperatorType.Or,
                                                                                        new BinaryOperator("Status", Status.Open),
                                                                                        new BinaryOperator("Status", Status.Posted))));

                    if (_locPurchaseInvoiceMonitorings != null && _locPurchaseInvoiceMonitorings.Count() > 0)
                    {
                        //Cek Total PIP dan Purchase Order Maks Pay
                        foreach (PurchaseInvoiceMonitoring _locPurchaseInvoiceMonitoring in _locPurchaseInvoiceMonitorings)
                        {
                            if (_locPurchaseInvoiceMonitoring.PurchaseOrderMonitoring != null
                                && _locPurchaseInvoiceMonitoring.PurchaseOrderMonitoring.PurchaseOrder != null)
                            {
                                #region Company
                                if (_locPurchaseInvoiceMonitoring.PurchaseInvoice.BankAccountCompany != null && _locPurchaseInvoiceMonitoring.PurchaseInvoice.Company != null)
                                {
                                    _locAccountForCompany = GetAccountFromBankAccountForCompany(_currSession, _locPurchaseInvoiceMonitoring.PurchaseInvoice.BankAccountCompany, _locPurchaseInvoiceMonitoring.PurchaseInvoice.Company);
                                }

                                PayableTransactionLine _saveDataPayableTransactionLine1 = new PayableTransactionLine(_currSession)
                                {
                                    PostingDate = now,
                                    PurchaseType = PostingType.Purchase,
                                    PostingMethod = PostingMethod.Payment,
                                    OpenCompany = true,
                                    Company = _locPurchaseInvoiceMonitoring.PurchaseInvoice.Company,
                                    BankAccount = _locPurchaseInvoiceMonitoring.PurchaseInvoice.BankAccountCompany,
                                    AccountNo = _locPurchaseInvoiceMonitoring.PurchaseInvoice.CompanyAccountNo,
                                    AccountName = _locPurchaseInvoiceMonitoring.PurchaseInvoice.CompanyAccountName,
                                    Account = _locAccountForCompany,
                                    Debit = _locPurchaseInvoiceMonitoring.Amount,
                                    CloseCredit = true,
                                    PurchaseInvoice = _locPurchaseInvoiceMonitoring.PurchaseInvoice,
                                    PurchaseOrder = _locPurchaseInvoiceMonitoring.PurchaseOrderMonitoring.PurchaseOrder,
                                    PurchaseInvoiceMonitoring = _locPurchaseInvoiceMonitoring,
                                    CompanyDefault = _locPurchaseInvoiceMonitoring.Company,
                                    Workplace = _locPurchaseInvoiceMonitoring.Workplace,
                                    PayableTransaction = _locPayableTransactionXPO,
                                    DocDate = _locPurchaseInvoiceMonitoring.DocDate,
                                    JournalMonth = _locPurchaseInvoiceMonitoring.JournalMonth,
                                    JournalYear = _locPurchaseInvoiceMonitoring.JournalYear,
                                };
                                _saveDataPayableTransactionLine1.Save();
                                _saveDataPayableTransactionLine1.Session.CommitTransaction();

                                #endregion Company

                                #region Vendor 
                                if (_locPurchaseInvoiceMonitoring.PurchaseInvoice.BankAccount != null && _locPurchaseInvoiceMonitoring.PurchaseInvoice.BuyFromVendor != null)
                                {
                                    _locAccountForVendor = GetAccountFromBankAccountForVendor(_currSession, _locPurchaseInvoiceMonitoring.PurchaseInvoice.BankAccount, _locPurchaseInvoiceMonitoring.PurchaseInvoice.BuyFromVendor);
                                }
                                PayableTransactionLine _saveDataPayableTransactionLine2 = new PayableTransactionLine(_currSession)
                                {
                                    PostingDate = now,
                                    PurchaseType = PostingType.Purchase,
                                    PostingMethod = PostingMethod.Payment,
                                    OpenVendor = true,
                                    Vendor = _locPurchaseInvoiceMonitoring.PurchaseInvoice.PayToVendor,
                                    BankAccount = _locPurchaseInvoiceMonitoring.PurchaseInvoice.BankAccount,
                                    AccountNo = _locPurchaseInvoiceMonitoring.PurchaseInvoice.AccountNo,
                                    AccountName = _locPurchaseInvoiceMonitoring.PurchaseInvoice.AccountName,
                                    Account = _locAccountForVendor,
                                    Credit = _locPurchaseInvoiceMonitoring.Amount,
                                    CloseDebit = true,
                                    PurchaseInvoice = _locPurchaseInvoiceMonitoring.PurchaseInvoice,
                                    PurchaseOrder = _locPurchaseInvoiceMonitoring.PurchaseOrderMonitoring.PurchaseOrder,
                                    PurchaseInvoiceMonitoring = _locPurchaseInvoiceMonitoring,
                                    CompanyDefault = _locPurchaseInvoiceMonitoring.Company,
                                    Workplace = _locPurchaseInvoiceMonitoring.Workplace,
                                    PayableTransaction = _locPayableTransactionXPO,
                                    DocDate = _locPurchaseInvoiceMonitoring.DocDate,
                                    JournalMonth = _locPurchaseInvoiceMonitoring.JournalMonth,
                                    JournalYear = _locPurchaseInvoiceMonitoring.JournalYear,
                                };
                                _saveDataPayableTransactionLine2.Save();
                                _saveDataPayableTransactionLine2.Session.CommitTransaction();
                                #endregion Vendor

                                _locDocDate = _locPurchaseInvoiceMonitoring.DocDate;
                                _locJournalMonth = _locPurchaseInvoiceMonitoring.JournalMonth;
                                _locJournalYear = _locPurchaseInvoiceMonitoring.JournalYear;
                                
                                SetProcessCount(_currSession, _locPurchaseInvoiceMonitoring);
                                SetStatusPurchaseInvoiceMonitoring(_currSession, _locPurchaseInvoiceMonitoring);
                                SetNormalQuantity(_currSession, _locPurchaseInvoiceMonitoring);
                            }
                        }
                        _locPayableTransactionXPO.DocDate = _locDocDate;
                        _locPayableTransactionXPO.JournalMonth = _locJournalMonth;
                        _locPayableTransactionXPO.JournalYear = _locJournalYear;
                        _locPayableTransactionXPO.Save();
                        _locPayableTransactionXPO.Session.CommitTransaction();
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = PayableTransaction ", ex.ToString());
            }
        }

        private ChartOfAccount GetAccountFromBankAccountForCompany(Session _currSession, BankAccount _locBankAccountXPO, Company _locCompanyXPO)
        {
            ChartOfAccount _result = null;

            try
            {
                if (_locBankAccountXPO != null && _locCompanyXPO != null)
                {
                    #region JournalMapBankAccountGroupWithCompany
                    if (_locBankAccountXPO.BankAccountGroup != null)
                    {
                        XPCollection<JournalMap> _locJournalMaps = new XPCollection<JournalMap>
                                                               (_currSession, new GroupOperator(GroupOperatorType.And,
                                                               new BinaryOperator("BankAccountGroup", _locBankAccountXPO.BankAccountGroup)));

                        if (_locJournalMaps != null && _locJournalMaps.Count() > 0)
                        {
                            foreach (JournalMap _locJournalMap in _locJournalMaps)
                            {
                                XPCollection<JournalMapLine> _locJournalMapLines = new XPCollection<JournalMapLine>
                                                                                (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                new BinaryOperator("JournalMap", _locJournalMap)));

                                if (_locJournalMapLines != null && _locJournalMapLines.Count() > 0)
                                {
                                    foreach (JournalMapLine _locJournalMapLine in _locJournalMapLines)
                                    {
                                        AccountMap _locAccountMap = _currSession.FindObject<AccountMap>
                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                     new BinaryOperator("Code", _locJournalMapLine.AccountMap.Code),
                                                                     new BinaryOperator("PostingType", PostingType.Purchase),
                                                                     new BinaryOperator("PostingMethod", PostingMethod.Payment),
                                                                     new BinaryOperator("PostingMethodType", PostingMethodType.Normal)));

                                        if (_locAccountMap != null)
                                        {
                                            XPCollection<AccountMapLine> _locAccountMapLines = new XPCollection<AccountMapLine>
                                                                                               (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                new BinaryOperator("AccountMap", _locAccountMap),
                                                                                                new BinaryOperator("Active", true)));

                                            if (_locAccountMapLines != null && _locAccountMapLines.Count() > 0)
                                            {
                                                foreach (AccountMapLine _locAccountMapLine in _locAccountMapLines)
                                                {
                                                    if (_locAccountMapLine.AccountCharge == AccountCharge.Debit)
                                                    {
                                                        _result = _locAccountMapLine.Account;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }

                    #endregion JournalMapBankAccountGroupWithCompany
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = PayableTransaction ", ex.ToString());
            }

            return _result;
        }

        private ChartOfAccount GetAccountFromBankAccountForVendor(Session _currSession, BankAccount _locBankAccountXPO, BusinessPartner _locVendorXPO)
        {
            ChartOfAccount _result = null;

            try
            {
                if (_locBankAccountXPO != null && _locVendorXPO != null)
                {
                    #region JournalMapBankAccountGroupWithVendor
                    if (_locBankAccountXPO.BankAccountGroup != null)
                    {
                        XPCollection<JournalMap> _locJournalMaps = new XPCollection<JournalMap>
                                                               (_currSession, new GroupOperator(GroupOperatorType.And,
                                                               new BinaryOperator("BankAccountGroup", _locBankAccountXPO.BankAccountGroup)));

                        if (_locJournalMaps != null && _locJournalMaps.Count() > 0)
                        {
                            foreach (JournalMap _locJournalMap in _locJournalMaps)
                            {
                                XPCollection<JournalMapLine> _locJournalMapLines = new XPCollection<JournalMapLine>
                                                                                (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                new BinaryOperator("JournalMap", _locJournalMap)));

                                if (_locJournalMapLines != null && _locJournalMapLines.Count() > 0)
                                {
                                    foreach (JournalMapLine _locJournalMapLine in _locJournalMapLines)
                                    {
                                        AccountMap _locAccountMap = _currSession.FindObject<AccountMap>
                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                     new BinaryOperator("Code", _locJournalMapLine.AccountMap.Code),
                                                                     new BinaryOperator("PostingType", PostingType.Purchase),
                                                                     new BinaryOperator("PostingMethod", PostingMethod.Payment),
                                                                     new BinaryOperator("PostingMethodType", PostingMethodType.Normal)));

                                        if (_locAccountMap != null)
                                        {
                                            XPCollection<AccountMapLine> _locAccountMapLines = new XPCollection<AccountMapLine>
                                                                                               (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                new BinaryOperator("AccountMap", _locAccountMap),
                                                                                                new BinaryOperator("Active", true)));

                                            if (_locAccountMapLines != null && _locAccountMapLines.Count() > 0)
                                            {
                                                foreach (AccountMapLine _locAccountMapLine in _locAccountMapLines)
                                                {
                                                    if (_locAccountMapLine.AccountCharge == AccountCharge.Credit)
                                                    {
                                                        _result = _locAccountMapLine.Account;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }

                    #endregion JournalMapBankAccountGroupWithVendor
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = PayableTransaction ", ex.ToString());
            }

            return _result;
        }

        #region Set

        private void SetProcessCount(Session _currSession, PurchaseInvoiceMonitoring _locPurchaseInvoiceMonitoringXPO)
        {
            try
            {
                if (_locPurchaseInvoiceMonitoringXPO != null)
                {
                    if (_locPurchaseInvoiceMonitoringXPO.Status == Status.Open || _locPurchaseInvoiceMonitoringXPO.Status == Status.Progress || _locPurchaseInvoiceMonitoringXPO.Status == Status.Posted)
                    {
                        _locPurchaseInvoiceMonitoringXPO.PostedCount = _locPurchaseInvoiceMonitoringXPO.PostedCount + 1;
                        _locPurchaseInvoiceMonitoringXPO.Save();
                        _locPurchaseInvoiceMonitoringXPO.Session.CommitTransaction();
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PayableTransaction " + ex.ToString());
            }
        }

        private void SetStatusPurchaseInvoiceMonitoring(Session _currSession, PurchaseInvoiceMonitoring _locPurchaseInvoiceMonitoringXPO)
        {
            try
            {
                DateTime now = DateTime.Now;
                if (_locPurchaseInvoiceMonitoringXPO != null)
                {
                    if (_locPurchaseInvoiceMonitoringXPO.Status == Status.Open || _locPurchaseInvoiceMonitoringXPO.Status == Status.Progress || _locPurchaseInvoiceMonitoringXPO.Status == Status.Posted)
                    {
                        _locPurchaseInvoiceMonitoringXPO.Status = Status.Close;
                        _locPurchaseInvoiceMonitoringXPO.ActivationPosting = true;
                        _locPurchaseInvoiceMonitoringXPO.StatusDate = now;
                        _locPurchaseInvoiceMonitoringXPO.Save();
                        _locPurchaseInvoiceMonitoringXPO.Session.CommitTransaction();
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PayableTransaction " + ex.ToString());
            }
        }

        private void SetNormalQuantity(Session _currSession, PurchaseInvoiceMonitoring _locPurchaseInvoiceMonitoringXPO)
        {
            try
            {
                if (_locPurchaseInvoiceMonitoringXPO != null)
                {
                    if (_locPurchaseInvoiceMonitoringXPO.Status == Status.Open || _locPurchaseInvoiceMonitoringXPO.Status == Status.Progress || _locPurchaseInvoiceMonitoringXPO.Status == Status.Posted || _locPurchaseInvoiceMonitoringXPO.Status == Status.Close)
                    {
                        _locPurchaseInvoiceMonitoringXPO.Select = false;
                        _locPurchaseInvoiceMonitoringXPO.Save();
                        _locPurchaseInvoiceMonitoringXPO.Session.CommitTransaction();
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PayableTransaction " + ex.ToString());
            }
        }

        #endregion Set

        #endregion GetPI

        #region GetPPI

        private void GetPurchasePrePaymentInvoiceMonitoring(Session _currSession, PurchasePrePaymentInvoice _locPurchasePrePaymentInvoiceXPO, PayableTransaction _locPayableTransactionXPO)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                DateTime now = DateTime.Now;
                ChartOfAccount _locAccountForCompany = null;
                ChartOfAccount _locAccountForVendor = null;
                DateTime _locDocDate = now;
                int _locJournalMonth = 0;
                int _locJournalYear = 0;

                if (_locPurchasePrePaymentInvoiceXPO != null && _locPayableTransactionXPO != null)
                {
                    XPCollection<PurchasePrePaymentInvoiceMonitoring> _locPurchasePrePaymentInvoiceMonitorings = new XPCollection<PurchasePrePaymentInvoiceMonitoring>
                                                                                        (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                        new BinaryOperator("PurchasePrePaymentInvoice", _locPurchasePrePaymentInvoiceXPO),
                                                                                        new BinaryOperator("PayableTransaction", _locPayableTransactionXPO),
                                                                                        new BinaryOperator("Select", true),
                                                                                        new GroupOperator(GroupOperatorType.Or,
                                                                                        new BinaryOperator("Status", Status.Open),
                                                                                        new BinaryOperator("Status", Status.Posted))));

                    if (_locPurchasePrePaymentInvoiceMonitorings != null && _locPurchasePrePaymentInvoiceMonitorings.Count() > 0)
                    {
                        //Cek Total PIP dan Purchase Order Maks Pay
                        foreach (PurchasePrePaymentInvoiceMonitoring _locPurchasePrePaymentInvoiceMonitoring in _locPurchasePrePaymentInvoiceMonitorings)
                        {
                            if (_locPurchasePrePaymentInvoiceMonitoring.PurchaseOrder != null)
                            {
                                #region Company
                                if (_locPurchasePrePaymentInvoiceMonitoring.PurchasePrePaymentInvoice.BankAccountCompany != null && _locPurchasePrePaymentInvoiceMonitoring.PurchasePrePaymentInvoice.Company != null)
                                {
                                    _locAccountForCompany = GetAccountFromBankAccountForCompanyPrePayment(_currSession, _locPurchasePrePaymentInvoiceMonitoring.PurchasePrePaymentInvoice.BankAccountCompany, _locPurchasePrePaymentInvoiceMonitoring.PurchasePrePaymentInvoice.Company);
                                }

                                PayableTransactionLine _saveDataPayableTransactionLine1 = new PayableTransactionLine(_currSession)
                                {
                                    PostingDate = now,
                                    PurchaseType = PostingType.Purchase,
                                    PostingMethod = PostingMethod.Payment,
                                    OpenCompany = true,
                                    Company = _locPurchasePrePaymentInvoiceMonitoring.PurchasePrePaymentInvoice.Company,
                                    BankAccount = _locPurchasePrePaymentInvoiceMonitoring.PurchasePrePaymentInvoice.BankAccountCompany,
                                    AccountNo = _locPurchasePrePaymentInvoiceMonitoring.PurchasePrePaymentInvoice.CompanyAccountNo,
                                    AccountName = _locPurchasePrePaymentInvoiceMonitoring.PurchasePrePaymentInvoice.CompanyAccountName,
                                    Account = _locAccountForCompany,
                                    Debit = _locPurchasePrePaymentInvoiceMonitoring.DP_Amount,
                                    CloseCredit = true,
                                    PurchasePrePaymentInvoice = _locPurchasePrePaymentInvoiceMonitoring.PurchasePrePaymentInvoice,
                                    PurchaseOrder = _locPurchasePrePaymentInvoiceMonitoring.PurchaseOrder,
                                    PurchasePrePaymentInvoiceMonitoring = _locPurchasePrePaymentInvoiceMonitoring,
                                    CompanyDefault = _locPurchasePrePaymentInvoiceMonitoring.Company,
                                    Workplace = _locPurchasePrePaymentInvoiceMonitoring.Workplace,
                                    PayableTransaction = _locPayableTransactionXPO,
                                    DocDate = _locPurchasePrePaymentInvoiceMonitoring.DocDate,
                                    JournalMonth = _locPurchasePrePaymentInvoiceMonitoring.JournalMonth,
                                    JournalYear = _locPurchasePrePaymentInvoiceMonitoring.JournalYear,
                                };
                                _saveDataPayableTransactionLine1.Save();
                                _saveDataPayableTransactionLine1.Session.CommitTransaction();

                                #endregion Company

                                #region Vendor 
                                if (_locPurchasePrePaymentInvoiceMonitoring.PurchasePrePaymentInvoice.BankAccount != null && _locPurchasePrePaymentInvoiceMonitoring.PurchasePrePaymentInvoice.BuyFromVendor != null)
                                {
                                    _locAccountForVendor = GetAccountFromBankAccountForVendorPrePayment(_currSession, _locPurchasePrePaymentInvoiceMonitoring.PurchasePrePaymentInvoice.BankAccount, _locPurchasePrePaymentInvoiceMonitoring.PurchasePrePaymentInvoice.BuyFromVendor);
                                }
                                PayableTransactionLine _saveDataPayableTransactionLine2 = new PayableTransactionLine(_currSession)
                                {
                                    PostingDate = now,
                                    PurchaseType = PostingType.Purchase,
                                    PostingMethod = PostingMethod.Payment,
                                    OpenVendor = true,
                                    Vendor = _locPurchasePrePaymentInvoiceMonitoring.PurchasePrePaymentInvoice.PayToVendor,
                                    BankAccount = _locPurchasePrePaymentInvoiceMonitoring.PurchasePrePaymentInvoice.BankAccount,
                                    AccountNo = _locPurchasePrePaymentInvoiceMonitoring.PurchasePrePaymentInvoice.AccountNo,
                                    AccountName = _locPurchasePrePaymentInvoiceMonitoring.PurchasePrePaymentInvoice.AccountName,
                                    Account = _locAccountForVendor,
                                    Credit = _locPurchasePrePaymentInvoiceMonitoring.DP_Amount,
                                    CloseDebit = true,
                                    PurchasePrePaymentInvoice = _locPurchasePrePaymentInvoiceMonitoring.PurchasePrePaymentInvoice,
                                    PurchaseOrder = _locPurchasePrePaymentInvoiceMonitoring.PurchaseOrder,
                                    PurchasePrePaymentInvoiceMonitoring = _locPurchasePrePaymentInvoiceMonitoring,
                                    CompanyDefault = _locPurchasePrePaymentInvoiceMonitoring.Company,
                                    Workplace = _locPurchasePrePaymentInvoiceMonitoring.Workplace,
                                    PayableTransaction = _locPayableTransactionXPO,
                                    DocDate = _locPurchasePrePaymentInvoiceMonitoring.DocDate,
                                    JournalMonth = _locPurchasePrePaymentInvoiceMonitoring.JournalMonth,
                                    JournalYear = _locPurchasePrePaymentInvoiceMonitoring.JournalYear,
                                };
                                _saveDataPayableTransactionLine2.Save();
                                _saveDataPayableTransactionLine2.Session.CommitTransaction();
                                #endregion Vendor

                                _locDocDate = _locPurchasePrePaymentInvoiceMonitoring.DocDate;
                                _locJournalMonth = _locPurchasePrePaymentInvoiceMonitoring.JournalMonth;
                                _locJournalYear = _locPurchasePrePaymentInvoiceMonitoring.JournalYear;

                                SetProcessCountPPIM(_currSession, _locPurchasePrePaymentInvoiceMonitoring);
                                SetStatusPurchasePrePaymentInvoiceMonitoring(_currSession, _locPurchasePrePaymentInvoiceMonitoring);
                                SetNormalQuantityPPIM(_currSession, _locPurchasePrePaymentInvoiceMonitoring);
                            }
                        }
                        _locPayableTransactionXPO.DocDate = _locDocDate;
                        _locPayableTransactionXPO.JournalMonth = _locJournalMonth;
                        _locPayableTransactionXPO.JournalYear = _locJournalYear;
                        _locPayableTransactionXPO.Save();
                        _locPayableTransactionXPO.Session.CommitTransaction();
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = PayableTransaction ", ex.ToString());
            }
        }

        private ChartOfAccount GetAccountFromBankAccountForCompanyPrePayment(Session _currSession, BankAccount _locBankAccountXPO, Company _locCompanyXPO)
        {
            ChartOfAccount _result = null;

            try
            {
                if (_locBankAccountXPO != null && _locCompanyXPO != null)
                {
                    #region JournalMapBankAccountGroupWithCompany
                    if (_locBankAccountXPO.BankAccountGroup != null)
                    {
                        XPCollection<JournalMap> _locJournalMaps = new XPCollection<JournalMap>
                                                               (_currSession, new GroupOperator(GroupOperatorType.And,
                                                               new BinaryOperator("BankAccountGroup", _locBankAccountXPO.BankAccountGroup)));

                        if (_locJournalMaps != null && _locJournalMaps.Count() > 0)
                        {
                            foreach (JournalMap _locJournalMap in _locJournalMaps)
                            {
                                XPCollection<JournalMapLine> _locJournalMapLines = new XPCollection<JournalMapLine>
                                                                                (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                new BinaryOperator("JournalMap", _locJournalMap)));

                                if (_locJournalMapLines != null && _locJournalMapLines.Count() > 0)
                                {
                                    foreach (JournalMapLine _locJournalMapLine in _locJournalMapLines)
                                    {
                                        AccountMap _locAccountMap = _currSession.FindObject<AccountMap>
                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                     new BinaryOperator("Code", _locJournalMapLine.AccountMap.Code),
                                                                     new BinaryOperator("PostingType", PostingType.Purchase),
                                                                     new BinaryOperator("PostingMethod", PostingMethod.Payment),
                                                                     new BinaryOperator("PostingMethodType", PostingMethodType.DownPayment)));

                                        if (_locAccountMap != null)
                                        {
                                            XPCollection<AccountMapLine> _locAccountMapLines = new XPCollection<AccountMapLine>
                                                                                               (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                new BinaryOperator("AccountMap", _locAccountMap),
                                                                                                new BinaryOperator("Active", true)));

                                            if (_locAccountMapLines != null && _locAccountMapLines.Count() > 0)
                                            {
                                                foreach (AccountMapLine _locAccountMapLine in _locAccountMapLines)
                                                {
                                                    if (_locAccountMapLine.AccountCharge == AccountCharge.Debit)
                                                    {
                                                        _result = _locAccountMapLine.Account;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }

                    #endregion JournalMapBankAccountGroupWithCompany
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = PayableTransaction ", ex.ToString());
            }

            return _result;
        }

        private ChartOfAccount GetAccountFromBankAccountForVendorPrePayment(Session _currSession, BankAccount _locBankAccountXPO, BusinessPartner _locVendorXPO)
        {
            ChartOfAccount _result = null;

            try
            {
                if (_locBankAccountXPO != null && _locVendorXPO != null)
                {
                    #region JournalMapBankAccountGroupWithVendor
                    if (_locBankAccountXPO.BankAccountGroup != null)
                    {
                        XPCollection<JournalMap> _locJournalMaps = new XPCollection<JournalMap>
                                                               (_currSession, new GroupOperator(GroupOperatorType.And,
                                                               new BinaryOperator("BankAccountGroup", _locBankAccountXPO.BankAccountGroup)));

                        if (_locJournalMaps != null && _locJournalMaps.Count() > 0)
                        {
                            foreach (JournalMap _locJournalMap in _locJournalMaps)
                            {
                                XPCollection<JournalMapLine> _locJournalMapLines = new XPCollection<JournalMapLine>
                                                                                (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                new BinaryOperator("JournalMap", _locJournalMap)));

                                if (_locJournalMapLines != null && _locJournalMapLines.Count() > 0)
                                {
                                    foreach (JournalMapLine _locJournalMapLine in _locJournalMapLines)
                                    {
                                        AccountMap _locAccountMap = _currSession.FindObject<AccountMap>
                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                     new BinaryOperator("Code", _locJournalMapLine.AccountMap.Code),
                                                                     new BinaryOperator("PostingType", PostingType.Purchase),
                                                                     new BinaryOperator("PostingMethod", PostingMethod.Payment),
                                                                     new BinaryOperator("PostingMethodType", PostingMethodType.DownPayment)));

                                        if (_locAccountMap != null)
                                        {
                                            XPCollection<AccountMapLine> _locAccountMapLines = new XPCollection<AccountMapLine>
                                                                                               (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                new BinaryOperator("AccountMap", _locAccountMap),
                                                                                                new BinaryOperator("Active", true)));

                                            if (_locAccountMapLines != null && _locAccountMapLines.Count() > 0)
                                            {
                                                foreach (AccountMapLine _locAccountMapLine in _locAccountMapLines)
                                                {
                                                    if (_locAccountMapLine.AccountCharge == AccountCharge.Credit)
                                                    {
                                                        _result = _locAccountMapLine.Account;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }

                    #endregion JournalMapBankAccountGroupWithVendor
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = PayableTransaction ", ex.ToString());
            }

            return _result;
        }

        #region Set

        private void SetProcessCountPPIM(Session _currSession, PurchasePrePaymentInvoiceMonitoring _locPurchasePrePaymentInvoiceMonitoringXPO)
        {
            try
            {
                if (_locPurchasePrePaymentInvoiceMonitoringXPO != null)
                {
                    if (_locPurchasePrePaymentInvoiceMonitoringXPO.Status == Status.Open || _locPurchasePrePaymentInvoiceMonitoringXPO.Status == Status.Progress || _locPurchasePrePaymentInvoiceMonitoringXPO.Status == Status.Posted)
                    {
                        _locPurchasePrePaymentInvoiceMonitoringXPO.PostedCount = _locPurchasePrePaymentInvoiceMonitoringXPO.PostedCount + 1;
                        _locPurchasePrePaymentInvoiceMonitoringXPO.Save();
                        _locPurchasePrePaymentInvoiceMonitoringXPO.Session.CommitTransaction();
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PayableTransaction " + ex.ToString());
            }
        }

        private void SetStatusPurchasePrePaymentInvoiceMonitoring(Session _currSession, PurchasePrePaymentInvoiceMonitoring _locPurchasePrePaymentInvoiceMonitoringXPO)
        {
            try
            {
                DateTime now = DateTime.Now;
                if (_locPurchasePrePaymentInvoiceMonitoringXPO != null)
                {
                    if (_locPurchasePrePaymentInvoiceMonitoringXPO.Status == Status.Open || _locPurchasePrePaymentInvoiceMonitoringXPO.Status == Status.Progress || _locPurchasePrePaymentInvoiceMonitoringXPO.Status == Status.Posted)
                    {
                        _locPurchasePrePaymentInvoiceMonitoringXPO.Status = Status.Close;
                        _locPurchasePrePaymentInvoiceMonitoringXPO.ActivationPosting = true;
                        _locPurchasePrePaymentInvoiceMonitoringXPO.StatusDate = now;
                        _locPurchasePrePaymentInvoiceMonitoringXPO.Save();
                        _locPurchasePrePaymentInvoiceMonitoringXPO.Session.CommitTransaction();
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PayableTransaction " + ex.ToString());
            }
        }

        private void SetNormalQuantityPPIM(Session _currSession, PurchasePrePaymentInvoiceMonitoring _locPurchasePrePaymentInvoiceMonitoringXPO)
        {
            try
            {
                if (_locPurchasePrePaymentInvoiceMonitoringXPO != null)
                {
                    if (_locPurchasePrePaymentInvoiceMonitoringXPO.Status == Status.Open || _locPurchasePrePaymentInvoiceMonitoringXPO.Status == Status.Progress || _locPurchasePrePaymentInvoiceMonitoringXPO.Status == Status.Posted || _locPurchasePrePaymentInvoiceMonitoringXPO.Status == Status.Close)
                    {
                        _locPurchasePrePaymentInvoiceMonitoringXPO.Select = false;
                        _locPurchasePrePaymentInvoiceMonitoringXPO.Save();
                        _locPurchasePrePaymentInvoiceMonitoringXPO.Session.CommitTransaction();
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PayableTransaction " + ex.ToString());
            }
        }

        #endregion Set

        #endregion GetPPI

        #region Posting

        #region PostingPurchaseInvoice

        private bool CheckMaksAmountDebitBasedPurchaseInvoiceMonitoring(Session _currSession, PayableTransaction _locPayableTransactionXPO)
        {
            bool _result = true;

            try
            {
                double _totDebit = 0;

                XPQuery<PayableTransactionLine> _payableTransactionLineQuerys = new XPQuery<PayableTransactionLine>(_currSession);

                var _payableTransactionLines = from ptl in _payableTransactionLineQuerys
                                               where (ptl.PayableTransaction == _locPayableTransactionXPO
                                               && ptl.Select == true
                                               && (ptl.Status == Status.Progress || ptl.Status == Status.Posted))
                                               group ptl by ptl.PurchaseInvoiceMonitoring into g
                                               select new { PurchaseInvoiceMonitoring = g.Key };

                if (_payableTransactionLines != null && _payableTransactionLines.Count() > 0)
                {
                    foreach (var _payableTransactionLine in _payableTransactionLines)
                    {
                        if (_payableTransactionLine.PurchaseInvoiceMonitoring != null)
                        {
                            XPCollection<PayableTransactionLine> _locPayableTransactionLines = new XPCollection<PayableTransactionLine>
                                                                                            (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                            new BinaryOperator("PayableTransaction", _locPayableTransactionXPO),
                                                                                            new BinaryOperator("PurchaseInvoiceMonitoring", _payableTransactionLine.PurchaseInvoiceMonitoring),
                                                                                            new GroupOperator(GroupOperatorType.Or,
                                                                                            new BinaryOperator("Status", Status.Progress),
                                                                                            new BinaryOperator("Status", Status.Posted))));
                            if (_locPayableTransactionLines != null && _locPayableTransactionLines.Count() > 0)
                            {
                                foreach (PayableTransactionLine _locPayableTransactionLine in _locPayableTransactionLines)
                                {
                                    if (_locPayableTransactionLine.CloseCredit == true && _locPayableTransactionLine.Debit > 0)
                                    {
                                        _totDebit = _totDebit + _locPayableTransactionLine.Debit;
                                    }
                                }

                                if (_totDebit != _payableTransactionLine.PurchaseInvoiceMonitoring.TAmount)
                                {
                                    _result = false;
                                }

                                _totDebit = 0;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = PayableTransaction ", ex.ToString());
            }

            return _result;
        }

        private bool CheckAmountDebitCreditBasedPurchaseInvoiceMonitoring(Session _currSession, PayableTransaction _locPayableTransactionXPO)
        {
            bool _result = true;

            try
            {
                double _totDebit = 0;
                double _totCredit = 0;

                XPQuery<PayableTransactionLine> _payableTransactionLineQuerys = new XPQuery<PayableTransactionLine>(_currSession);

                var _payableTransactionLines = from ptl in _payableTransactionLineQuerys
                                               where (ptl.PayableTransaction == _locPayableTransactionXPO
                                               && ptl.Select == true
                                               && (ptl.Status == Status.Progress || ptl.Status == Status.Posted))
                                               group ptl by ptl.PurchaseInvoiceMonitoring into g
                                               select new { PurchaseInvoiceMonitoring = g.Key };

                if (_payableTransactionLines != null && _payableTransactionLines.Count() > 0)
                {
                    foreach (var _payableTransactionLine in _payableTransactionLines)
                    {
                        if (_payableTransactionLine.PurchaseInvoiceMonitoring != null)
                        {
                            XPCollection<PayableTransactionLine> _locPayableTransactionLines = new XPCollection<PayableTransactionLine>
                                                                                            (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                            new BinaryOperator("PayableTransaction", _locPayableTransactionXPO),
                                                                                            new BinaryOperator("PurchaseInvoiceMonitoring", _payableTransactionLine.PurchaseInvoiceMonitoring),
                                                                                            new GroupOperator(GroupOperatorType.Or,
                                                                                            new BinaryOperator("Status", Status.Progress),
                                                                                            new BinaryOperator("Status", Status.Posted))));
                            if (_locPayableTransactionLines != null && _locPayableTransactionLines.Count() > 0)
                            {
                                foreach (PayableTransactionLine _locPayableTransactionLine in _locPayableTransactionLines)
                                {
                                    if (_locPayableTransactionLine.CloseCredit == true && _locPayableTransactionLine.Debit > 0)
                                    {
                                        _totDebit = _totDebit + _locPayableTransactionLine.Debit;
                                    }
                                    else if (_locPayableTransactionLine.CloseDebit == true && _locPayableTransactionLine.Credit > 0)
                                    {
                                        _totCredit = _totCredit + _locPayableTransactionLine.Credit;
                                    }
                                }

                                if (_totDebit != _totCredit)
                                {
                                    _result = false;
                                }

                                _totDebit = 0;
                                _totCredit = 0;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = PayableTransaction ", ex.ToString());
            }

            return _result;
        }

        private void SetPayableTransactionMonitoringBasedPurchaseInvoiceMonitoring(Session _currSession, PayableTransaction _locPayableTransactionXPO)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                double _totDebit = 0;
                double _totCredit = 0;
                string _currSignCode = null;
                DateTime now = DateTime.Now;

                XPQuery<PayableTransactionLine> _payableTransactionLineQuerys = new XPQuery<PayableTransactionLine>(_currSession);

                var _payableTransactionLines = from ptl in _payableTransactionLineQuerys
                                               where (ptl.PayableTransaction == _locPayableTransactionXPO
                                               && ptl.Select == true
                                               && (ptl.Status == Status.Progress || ptl.Status == Status.Posted))
                                               group ptl by ptl.PurchaseInvoiceMonitoring into g
                                               select new { PurchaseInvoiceMonitoring = g.Key };

                if (_payableTransactionLines != null && _payableTransactionLines.Count() > 0)
                {
                    foreach (var _payableTransactionLine in _payableTransactionLines)
                    {
                        if (_payableTransactionLine.PurchaseInvoiceMonitoring != null)
                        {
                            XPCollection<PayableTransactionLine> _locPayableTransactionLines = new XPCollection<PayableTransactionLine>
                                                                                            (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                            new BinaryOperator("PayableTransaction", _locPayableTransactionXPO),
                                                                                            new BinaryOperator("PurchaseInvoiceMonitoring", _payableTransactionLine.PurchaseInvoiceMonitoring),
                                                                                            new GroupOperator(GroupOperatorType.Or,
                                                                                            new BinaryOperator("Status", Status.Progress),
                                                                                            new BinaryOperator("Status", Status.Posted))));

                            if (_locPayableTransactionLines != null && _locPayableTransactionLines.Count() > 0)
                            {
                                foreach (PayableTransactionLine _locPayableTransactionLine in _locPayableTransactionLines)
                                {
                                    if (_locPayableTransactionLine.CloseCredit == true && _locPayableTransactionLine.Debit > 0)
                                    {
                                        _totDebit = _totDebit + _locPayableTransactionLine.Debit;
                                    }
                                    else if (_locPayableTransactionLine.CloseDebit == true && _locPayableTransactionLine.Credit > 0)
                                    {
                                        _totCredit = _totCredit + _locPayableTransactionLine.Credit;
                                    }
                                }

                                _currSignCode = _globFunc.GetNumberingSignUnlockOptimisticRecordBasedOrganization(_currSession.DataLayer, ObjectList.PayableTransactionMonitoring, _locPayableTransactionXPO.CompanyDefault, _locPayableTransactionXPO.Workplace);
                                
                                if (_currSignCode != null)
                                {
                                    PayableTransactionMonitoring _saveDataPayableTransactionMonitoring = new PayableTransactionMonitoring(_currSession)
                                    {
                                        SignCode = _currSignCode,
                                        PayableTransaction = _locPayableTransactionXPO,
                                        PurchaseInvoiceMonitoring = _payableTransactionLine.PurchaseInvoiceMonitoring,
                                        PurchaseInvoice = _payableTransactionLine.PurchaseInvoiceMonitoring.PurchaseInvoice,
                                        PurchaseOrder = _payableTransactionLine.PurchaseInvoiceMonitoring.PurchaseOrderMonitoring.PurchaseOrder,
                                        Currency = _locPayableTransactionXPO.Currency,
                                        TotAmountDebit = _totDebit,
                                        TotAmountCredit = _totCredit,
                                        Company = _payableTransactionLine.PurchaseInvoiceMonitoring.Company,
                                        Workplace = _payableTransactionLine.PurchaseInvoiceMonitoring.Workplace,
                                        DocDate = _payableTransactionLine.PurchaseInvoiceMonitoring.DocDate,
                                        JournalMonth = _payableTransactionLine.PurchaseInvoiceMonitoring.JournalMonth,
                                        JournalYear = _payableTransactionLine.PurchaseInvoiceMonitoring.JournalYear,
                                    };
                                    _saveDataPayableTransactionMonitoring.Save();
                                    _saveDataPayableTransactionMonitoring.Session.CommitTransaction();

                                    PayableTransactionMonitoring _locPayableTransactionMonitoring = _currSession.FindObject<PayableTransactionMonitoring>
                                                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                                                    new BinaryOperator("SignCode", _currSignCode)));
                                    if (_locPayableTransactionMonitoring != null)
                                    {
                                        #region PaymentOutPlan

                                        double _locPaid = 0;
                                        double _locOutstanding = 0;
                                        Status _locStatus = Status.Open;

                                        PaymentOutPlan _locPaymentOutPlan = _currSession.FindObject<PaymentOutPlan>
                                                                        (new GroupOperator(GroupOperatorType.And,
                                                                        new BinaryOperator("PurchaseInvoiceMonitoring", _locPayableTransactionMonitoring.PurchaseInvoiceMonitoring)));
                                        if (_locPaymentOutPlan != null)
                                        {
                                            _locPaid = _locPayableTransactionMonitoring.TotAmountCredit;
                                            _locOutstanding = _locPaymentOutPlan.Plan - _locPayableTransactionMonitoring.TotAmountCredit;
                                            if (_locOutstanding == 0)
                                            {
                                                _locStatus = Status.Close;
                                            }
                                            else
                                            {
                                                _locStatus = Status.Posted;
                                            }

                                            _locPaymentOutPlan.Paid = _locPaymentOutPlan.Paid + _locPaid;
                                            _locPaymentOutPlan.Outstanding = _locOutstanding;
                                            _locPaymentOutPlan.Status = _locStatus;
                                            _locPaymentOutPlan.StatusDate = now;
                                            _locPaymentOutPlan.PayableTransaction = _locPayableTransactionXPO;
                                            _locPaymentOutPlan.Save();
                                            _locPaymentOutPlan.Session.CommitTransaction();
                                        }

                                        #endregion PaymentOutPlan

                                        #region PurchaseInvoiceMonitoring

                                        if (_locPayableTransactionMonitoring.PurchaseInvoiceMonitoring != null)
                                        {
                                            if (_locPayableTransactionMonitoring.PurchaseInvoiceMonitoring.Code != null)
                                            {
                                                PurchaseInvoiceMonitoring _locPurchaseInvoiceMonitoring = _currSession.FindObject<PurchaseInvoiceMonitoring>
                                                                                        (new GroupOperator(GroupOperatorType.And,
                                                                                        new BinaryOperator("Code", _locPayableTransactionMonitoring.PurchaseInvoiceMonitoring.Code)));
                                                if (_locPurchaseInvoiceMonitoring != null)
                                                {
                                                    if (_locPurchaseInvoiceMonitoring.Amount == _locPayableTransactionMonitoring.TotAmountCredit)
                                                    {
                                                        _locPurchaseInvoiceMonitoring.Status = Status.Close;
                                                        _locPurchaseInvoiceMonitoring.StatusDate = now;
                                                        _locPurchaseInvoiceMonitoring.Save();
                                                        _locPurchaseInvoiceMonitoring.Session.CommitTransaction();
                                                    }
                                                    else
                                                    {
                                                        _locPurchaseInvoiceMonitoring.Status = Status.Posted;
                                                        _locPurchaseInvoiceMonitoring.StatusDate = now;
                                                        _locPurchaseInvoiceMonitoring.Save();
                                                        _locPurchaseInvoiceMonitoring.Session.CommitTransaction();
                                                    }
                                                }
                                            }
                                        }

                                        #endregion PurchaseInvoiceMonitoring
                                    }
                                }
                                _totDebit = 0;
                                _totCredit = 0;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = PayableTransaction ", ex.ToString());
            }
        }

        #region JournalPayable

        private bool CheckJournalSetup(Session _currSession, PayableTransaction _locPayableTransactionXPO)
        {
            bool _result = false;
            try
            {
                if (_locPayableTransactionXPO != null)
                {
                    if (_locPayableTransactionXPO.CompanyDefault != null && _locPayableTransactionXPO.Workplace != null)
                    {
                        JournalSetupDetail _locJournalSetupDetail = _currSession.FindObject<JournalSetupDetail>
                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                    new BinaryOperator("Company", _locPayableTransactionXPO.CompanyDefault),
                                                                    new BinaryOperator("Workplace", _locPayableTransactionXPO.Workplace),
                                                                    new BinaryOperator("JournalSetupType", JournalSetupType.Journal),
                                                                    new BinaryOperator("ObjectList", ObjectList.PayableTransaction),
                                                                    new BinaryOperator("JournalProcess", JournalProcess.JournalPayable),
                                                                    new BinaryOperator("Active", true)));

                        if (_locJournalSetupDetail != null)
                        {
                            _result = true;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = PayableTransaction ", ex.ToString());
            }

            return _result;
        }

        private void SetPayableJournal(Session _currSession, PayableTransaction _locPayableTransactionXPO)
        {
            try
            {
                DateTime now = DateTime.Now;
                double _locTotalAmountBP = 0;
                double _locTotalAmountC = 0;
                BusinessPartnerAccountGroup _locBusinessPartnerAccountGroup = null;
                CompanyAccountGroup _locCompanyAccountGroup = null;
                BankAccountGroup _locBankAccountGroup = null;
                BankAccountGroup _locCompanyBankAccountGroup = null;

                if (_locPayableTransactionXPO != null)
                {
                    XPCollection<PayableTransactionLine> _locPayableTransactionLines = new XPCollection<PayableTransactionLine>(_currSession,
                                                                        new GroupOperator(GroupOperatorType.And,
                                                                        new BinaryOperator("PayableTransaction", _locPayableTransactionXPO),
                                                                        new BinaryOperator("Select", true),
                                                                        new GroupOperator(GroupOperatorType.Or,
                                                                        new BinaryOperator("Status", Status.Progress),
                                                                        new BinaryOperator("Status", Status.Posted))));

                    if (_locPayableTransactionLines != null && _locPayableTransactionLines.Count() > 0)
                    {
                        foreach (PayableTransactionLine _locPayableTransactionLine in _locPayableTransactionLines)
                        {
                            #region NonBankAccount

                            #region JournalMapBusinessPartnerAccountGroup

                            if (_locPayableTransactionLine.PurchaseInvoice.PayToVendor != null)
                            {
                                BusinessPartnerAccount _locBusinessPartnerAccount = _currSession.FindObject<BusinessPartnerAccount>(new GroupOperator(GroupOperatorType.And,
                                                                    new BinaryOperator("Company", _locPayableTransactionLine.CompanyDefault),
                                                                    new BinaryOperator("Workplace", _locPayableTransactionLine.Workplace),
                                                                    new BinaryOperator("BusinessPartner", _locPayableTransactionLine.PurchaseInvoice.PayToVendor),
                                                                    new BinaryOperator("Active", true)));

                                if(_locBusinessPartnerAccount != null)
                                {
                                    if(_locBusinessPartnerAccount.BusinessPartnerAccountGroup != null)
                                    {
                                        _locBusinessPartnerAccountGroup = _locBusinessPartnerAccount.BusinessPartnerAccountGroup;

                                        _locTotalAmountBP = _locPayableTransactionLine.Credit;

                                        XPCollection<JournalMap> _locJournalMapItems = new XPCollection<JournalMap>
                                                                           (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                            new BinaryOperator("Company", _locPayableTransactionLine.CompanyDefault),
                                                                            new BinaryOperator("Workplace", _locPayableTransactionLine.Workplace),
                                                                            new BinaryOperator("BusinessPartnerAccountGroup", _locBusinessPartnerAccountGroup),
                                                                            new BinaryOperator("Active", true)));

                                        if (_locJournalMapItems != null && _locJournalMapItems.Count() > 0)
                                        {
                                            foreach (JournalMap _locJournalMapItem in _locJournalMapItems)
                                            {
                                                XPCollection<JournalMapLine> _locJournalMapLines = new XPCollection<JournalMapLine>
                                                                                                   (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                    new BinaryOperator("Company", _locPayableTransactionLine.CompanyDefault),
                                                                                                    new BinaryOperator("Workplace", _locPayableTransactionLine.Workplace),
                                                                                                    new BinaryOperator("JournalMap", _locJournalMapItem),
                                                                                                    new BinaryOperator("Active", true)));

                                                if (_locJournalMapLines != null && _locJournalMapLines.Count() > 0)
                                                {
                                                    foreach (JournalMapLine _locJournalMapLine in _locJournalMapLines)
                                                    {
                                                        if (_locJournalMapLine.AccountMap != null)
                                                        {
                                                            #region NormalAmount
                                                            if (_locJournalMapLine.AccountMap.PostingType == PostingType.Purchase && _locJournalMapLine.AccountMap.OrderType == OrderType.Item
                                                                && _locJournalMapLine.AccountMap.PostingMethod == PostingMethod.Payment && _locJournalMapLine.AccountMap.PostingMethodType == PostingMethodType.Normal)
                                                            {
                                                                XPCollection<AccountMapLine> _locAccountMapLines = new XPCollection<AccountMapLine>
                                                                                                               (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                                new BinaryOperator("Company", _locPayableTransactionLine.CompanyDefault),
                                                                                                                new BinaryOperator("Workplace", _locPayableTransactionLine.Workplace),
                                                                                                                new BinaryOperator("AccountMap", _locJournalMapLine.AccountMap),
                                                                                                                new BinaryOperator("Active", true)));

                                                                if (_locAccountMapLines != null && _locAccountMapLines.Count() > 0)
                                                                {
                                                                    double _locTotalAmountDebit = 0;
                                                                    double _locTotalAmountCredit = 0;
                                                                    double _locTotalBalance = 0;

                                                                    foreach (AccountMapLine _locAccountMapLine in _locAccountMapLines)
                                                                    {
                                                                        if (_locAccountMapLine.AccountCharge == AccountCharge.Debit)
                                                                        {
                                                                            _locTotalAmountDebit = _locTotalAmountBP;
                                                                        }
                                                                        if (_locAccountMapLine.AccountCharge == AccountCharge.Credit)
                                                                        {
                                                                            _locTotalAmountCredit = _locTotalAmountBP;
                                                                        }

                                                                        GeneralJournal _saveGeneralJournal = new GeneralJournal(_currSession)
                                                                        {
                                                                            Company = _locPayableTransactionLine.CompanyDefault,
                                                                            Workplace = _locPayableTransactionLine.Workplace,
                                                                            PostingDate = now,
                                                                            PostingType = PostingType.Purchase,
                                                                            OrderType = OrderType.Item,
                                                                            PostingMethod = PostingMethod.Payment,
                                                                            PostingMethodType = PostingMethodType.Normal,
                                                                            Account = _locAccountMapLine.Account,
                                                                            Debit = _locTotalAmountDebit,
                                                                            Credit = _locTotalAmountCredit,
                                                                            JournalMonth = _locPayableTransactionLine.JournalMonth,
                                                                            JournalYear = _locPayableTransactionLine.JournalYear,
                                                                            PayableTransaction = _locPayableTransactionXPO,
                                                                        };

                                                                        _saveGeneralJournal.Save();
                                                                        _saveGeneralJournal.Session.CommitTransaction();

                                                                        #region AccountingPeriodicLine

                                                                        if (_locAccountMapLine.Account.Code != null)
                                                                        {
                                                                            //Cari Account Periodic Line
                                                                            AccountingPeriodicLine _locAPL = _currSession.FindObject<AccountingPeriodicLine>
                                                                                                            (new GroupOperator(GroupOperatorType.And,
                                                                                                            new BinaryOperator("Company", _locPayableTransactionLine.CompanyDefault),
                                                                                                            new BinaryOperator("Workplace", _locPayableTransactionLine.Workplace),
                                                                                                            new BinaryOperator("AccountNo", _locAccountMapLine.Account),
                                                                                                            new BinaryOperator("Month", _locPayableTransactionLine.JournalMonth),
                                                                                                            new BinaryOperator("Year", _locPayableTransactionLine.JournalYear)));
                                                                            if (_locAPL != null)
                                                                            {
                                                                                if (_locAPL.AccountNo != null)
                                                                                {
                                                                                    if (_locAPL.AccountNo.BalanceType == BalanceType.Change)
                                                                                    {
                                                                                        if (_locAPL.AccountNo.AccountCharge == AccountCharge.Debit)
                                                                                        {
                                                                                            if (_locTotalAmountDebit > 0)
                                                                                            {
                                                                                                _locTotalBalance = _locAPL.Balance + _locTotalAmountDebit;
                                                                                            }
                                                                                            if (_locTotalAmountCredit > 0)
                                                                                            {
                                                                                                _locTotalBalance = _locAPL.Balance - _locTotalAmountCredit;
                                                                                            }
                                                                                        }
                                                                                        if (_locAPL.AccountNo.AccountCharge == AccountCharge.Credit)
                                                                                        {
                                                                                            if (_locTotalAmountDebit > 0)
                                                                                            {
                                                                                                _locTotalBalance = _locAPL.Balance - _locTotalAmountDebit;
                                                                                            }
                                                                                            if (_locTotalAmountCredit > 0)
                                                                                            {
                                                                                                _locTotalBalance = _locAPL.Balance + _locTotalAmountCredit;
                                                                                            }
                                                                                        }
                                                                                    }

                                                                                    _locAPL.Balance = _locTotalBalance;
                                                                                    _locAPL.Debit = _locAPL.Debit + _locTotalAmountDebit;
                                                                                    _locAPL.Credit = _locAPL.Credit + _locTotalAmountCredit;
                                                                                    _locAPL.Save();
                                                                                    _locAPL.Session.CommitTransaction();

                                                                                }
                                                                            }
                                                                        }
                                                                        #endregion AccountingPeriodicLine
                                                                    }
                                                                }
                                                            }
                                                            #endregion NormalAmount   
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            #endregion JournalMapBusinessPartnerAccountGroup

                            #region JournalMapCompanyAccountGroup
                            if (_locPayableTransactionLine.CompanyDefault != null)
                            {
                                _locTotalAmountC = _locPayableTransactionLine.Debit;


                                CompanyAccount _locCompanyAccount = _currSession.FindObject<CompanyAccount>(new GroupOperator(GroupOperatorType.And,
                                                        new BinaryOperator("Company", _locPayableTransactionLine.CompanyDefault),
                                                        new BinaryOperator("Workplace", _locPayableTransactionLine.Workplace),
                                                        new BinaryOperator("Active", true)));

                                if (_locCompanyAccount != null)
                                {
                                    if (_locCompanyAccount.CompanyAccountGroup != null)
                                    {
                                        _locCompanyAccountGroup = _locCompanyAccount.CompanyAccountGroup;

                                        XPCollection<JournalMap> _locJournalMapItems = new XPCollection<JournalMap>
                                                                       (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                        new BinaryOperator("Company", _locPayableTransactionLine.CompanyDefault),
                                                                        new BinaryOperator("Workplace", _locPayableTransactionLine.Workplace),
                                                                        new BinaryOperator("CompanyAccountGroup", _locCompanyAccountGroup),
                                                                        new BinaryOperator("Active", true)));

                                        if (_locJournalMapItems != null && _locJournalMapItems.Count() > 0)
                                        {
                                            foreach (JournalMap _locJournalMapItem in _locJournalMapItems)
                                            {
                                                XPCollection<JournalMapLine> _locJournalMapLines = new XPCollection<JournalMapLine>
                                                                                                   (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                    new BinaryOperator("Company", _locPayableTransactionLine.CompanyDefault),
                                                                                                    new BinaryOperator("Workplace", _locPayableTransactionLine.Workplace),
                                                                                                    new BinaryOperator("JournalMap", _locJournalMapItem),
                                                                                                    new BinaryOperator("Active", true)));

                                                if (_locJournalMapLines != null && _locJournalMapLines.Count() > 0)
                                                {
                                                    foreach (JournalMapLine _locJournalMapLine in _locJournalMapLines)
                                                    {
                                                        if (_locJournalMapLine.AccountMap != null)
                                                        {
                                                            #region NormalAmount
                                                            if (_locJournalMapLine.AccountMap.PostingType == PostingType.Purchase && _locJournalMapLine.AccountMap.OrderType == OrderType.Item
                                                                && _locJournalMapLine.AccountMap.PostingMethod == PostingMethod.Payment && _locJournalMapLine.AccountMap.PostingMethodType == PostingMethodType.Normal)
                                                            {
                                                                XPCollection<AccountMapLine> _locAccountMapLines = new XPCollection<AccountMapLine>
                                                                                                               (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                                new BinaryOperator("Company", _locPayableTransactionLine.CompanyDefault),
                                                                                                                new BinaryOperator("Workplace", _locPayableTransactionLine.Workplace),
                                                                                                                new BinaryOperator("AccountMap", _locJournalMapLine.AccountMap),
                                                                                                                new BinaryOperator("Active", true)));

                                                                if (_locAccountMapLines != null && _locAccountMapLines.Count() > 0)
                                                                {
                                                                    double _locTotalAmountDebit = 0;
                                                                    double _locTotalAmountCredit = 0;
                                                                    double _locTotalBalance = 0;

                                                                    foreach (AccountMapLine _locAccountMapLine in _locAccountMapLines)
                                                                    {
                                                                        if (_locAccountMapLine.AccountCharge == AccountCharge.Debit)
                                                                        {
                                                                            _locTotalAmountDebit = _locTotalAmountC;
                                                                        }
                                                                        if (_locAccountMapLine.AccountCharge == AccountCharge.Credit)
                                                                        {
                                                                            _locTotalAmountCredit = _locTotalAmountC;
                                                                        }

                                                                        GeneralJournal _saveGeneralJournal = new GeneralJournal(_currSession)
                                                                        {
                                                                            Company = _locPayableTransactionLine.CompanyDefault,
                                                                            Workplace = _locPayableTransactionLine.Workplace,
                                                                            PostingDate = now,
                                                                            PostingType = PostingType.Purchase,
                                                                            OrderType = OrderType.Item,
                                                                            PostingMethod = PostingMethod.Payment,
                                                                            PostingMethodType = PostingMethodType.Normal,
                                                                            Account = _locAccountMapLine.Account,
                                                                            Debit = _locTotalAmountDebit,
                                                                            Credit = _locTotalAmountCredit,
                                                                            JournalMonth = _locPayableTransactionLine.JournalMonth,
                                                                            JournalYear = _locPayableTransactionLine.JournalYear,
                                                                            PayableTransaction = _locPayableTransactionXPO,
                                                                        };

                                                                        _saveGeneralJournal.Save();
                                                                        _saveGeneralJournal.Session.CommitTransaction();

                                                                        #region AccountingPeriodicLine

                                                                        if (_locAccountMapLine.Account.Code != null)
                                                                        {
                                                                            //Cari Account Periodic Line
                                                                            AccountingPeriodicLine _locAPL = _currSession.FindObject<AccountingPeriodicLine>
                                                                                                            (new GroupOperator(GroupOperatorType.And,
                                                                                                            new BinaryOperator("Company", _locPayableTransactionLine.CompanyDefault),
                                                                                                            new BinaryOperator("Workplace", _locPayableTransactionLine.Workplace),
                                                                                                            new BinaryOperator("AccountNo", _locAccountMapLine.Account),
                                                                                                            new BinaryOperator("Month", _locPayableTransactionLine.JournalMonth),
                                                                                                            new BinaryOperator("Year", _locPayableTransactionLine.JournalYear)));
                                                                            if (_locAPL != null)
                                                                            {
                                                                                if (_locAPL.AccountNo != null)
                                                                                {
                                                                                    if (_locAPL.AccountNo.BalanceType == BalanceType.Change)
                                                                                    {
                                                                                        if (_locAPL.AccountNo.AccountCharge == AccountCharge.Debit)
                                                                                        {
                                                                                            if (_locTotalAmountDebit > 0)
                                                                                            {
                                                                                                _locTotalBalance = _locAPL.Balance + _locTotalAmountDebit;
                                                                                            }
                                                                                            if (_locTotalAmountCredit > 0)
                                                                                            {
                                                                                                _locTotalBalance = _locAPL.Balance - _locTotalAmountCredit;
                                                                                            }
                                                                                        }
                                                                                        if (_locAPL.AccountNo.AccountCharge == AccountCharge.Credit)
                                                                                        {
                                                                                            if (_locTotalAmountDebit > 0)
                                                                                            {
                                                                                                _locTotalBalance = _locAPL.Balance - _locTotalAmountDebit;
                                                                                            }
                                                                                            if (_locTotalAmountCredit > 0)
                                                                                            {
                                                                                                _locTotalBalance = _locAPL.Balance + _locTotalAmountCredit;
                                                                                            }
                                                                                        }
                                                                                    }

                                                                                    _locAPL.Balance = _locTotalBalance;
                                                                                    _locAPL.Debit = _locAPL.Debit + _locTotalAmountDebit;
                                                                                    _locAPL.Credit = _locAPL.Credit + _locTotalAmountCredit;
                                                                                    _locAPL.Save();
                                                                                    _locAPL.Session.CommitTransaction();

                                                                                }
                                                                            }
                                                                        }
                                                                        #endregion AccountingPeriodicLine
                                                                    }
                                                                }
                                                            }
                                                            #endregion NormalAmount 
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            #endregion JournalMapCompanyAccountGroup

                            #endregion NonBankAccount

                            #region BankAccount
                            if (_locPayableTransactionLine.MultiBank == true)
                            {
                                XPCollection<PayableTransactionBank> _locPayableTransactionBanks = new XPCollection<PayableTransactionBank>(_currSession,
                                                                                                         new GroupOperator(GroupOperatorType.And,
                                                                                                         new BinaryOperator("PayableTransactionLine", _locPayableTransactionLine)));
                                if (_locPayableTransactionBanks != null && _locPayableTransactionBanks.Count() > 0)
                                {
                                    foreach (PayableTransactionBank _locPayableTransactionBank in _locPayableTransactionBanks)
                                    {
                                        #region JournalMapBusinessPartnerAccountGroup
                                        if (_locPayableTransactionBank.BankAccount != null)
                                        {
                                            BankAccountMap _locBankAccountMap = _currSession.FindObject<BankAccountMap>(new GroupOperator(GroupOperatorType.And,
                                                                                            new BinaryOperator("Company", _locPayableTransactionLine.CompanyDefault),
                                                                                            new BinaryOperator("Workplace", _locPayableTransactionLine.Workplace),
                                                                                            new BinaryOperator("BankAccount", _locPayableTransactionBank.BankAccount),
                                                                                            new BinaryOperator("Active", true)));
                                            if (_locBankAccountMap != null)
                                            {
                                                if (_locBankAccountMap.BankAccountGroup != null)
                                                {
                                                    _locBankAccountGroup = _locPayableTransactionBank.BankAccount.BankAccountGroup;

                                                    _locTotalAmountBP = _locPayableTransactionBank.Amount;


                                                    XPCollection<JournalMap> _locJournalMapItems = new XPCollection<JournalMap>
                                                                                       (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                        new BinaryOperator("Company", _locPayableTransactionBank.CompanyDefault),
                                                                                        new BinaryOperator("Workplace", _locPayableTransactionBank.Workplace),
                                                                                        new BinaryOperator("BankAccountGroup", _locBankAccountGroup),
                                                                                        new BinaryOperator("Active", true)));

                                                    if (_locJournalMapItems != null && _locJournalMapItems.Count() > 0)
                                                    {
                                                        foreach (JournalMap _locJournalMapItem in _locJournalMapItems)
                                                        {
                                                            XPCollection<JournalMapLine> _locJournalMapLines = new XPCollection<JournalMapLine>
                                                                                                               (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                                new BinaryOperator("Company", _locPayableTransactionBank.CompanyDefault),
                                                                                                                new BinaryOperator("Workplace", _locPayableTransactionBank.Workplace),
                                                                                                                new BinaryOperator("JournalMap", _locJournalMapItem),
                                                                                                                new BinaryOperator("Active", true)));

                                                            if (_locJournalMapLines != null && _locJournalMapLines.Count() > 0)
                                                            {
                                                                foreach (JournalMapLine _locJournalMapLine in _locJournalMapLines)
                                                                {
                                                                    if (_locJournalMapLine.AccountMap != null)
                                                                    {
                                                                        #region NormalAmount
                                                                        if (_locJournalMapLine.AccountMap.PostingType == PostingType.Purchase && _locJournalMapLine.AccountMap.OrderType == OrderType.Item
                                                                            && _locJournalMapLine.AccountMap.PostingMethod == PostingMethod.Payment && _locJournalMapLine.AccountMap.PostingMethodType == PostingMethodType.Normal)
                                                                        {
                                                                            XPCollection<AccountMapLine> _locAccountMapLines = new XPCollection<AccountMapLine>
                                                                                                                           (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                                            new BinaryOperator("Company", _locPayableTransactionBank.CompanyDefault),
                                                                                                                            new BinaryOperator("Workplace", _locPayableTransactionBank.Workplace),
                                                                                                                            new BinaryOperator("AccountMap", _locJournalMapLine.AccountMap),
                                                                                                                            new BinaryOperator("Active", true)));

                                                                            if (_locAccountMapLines != null && _locAccountMapLines.Count() > 0)
                                                                            {
                                                                                double _locTotalAmountDebit = 0;
                                                                                double _locTotalAmountCredit = 0;
                                                                                double _locTotalBalance = 0;

                                                                                foreach (AccountMapLine _locAccountMapLine in _locAccountMapLines)
                                                                                {
                                                                                    if (_locAccountMapLine.AccountCharge == AccountCharge.Debit)
                                                                                    {
                                                                                        _locTotalAmountDebit = _locTotalAmountBP;
                                                                                    }
                                                                                    if (_locAccountMapLine.AccountCharge == AccountCharge.Credit)
                                                                                    {
                                                                                        _locTotalAmountCredit = _locTotalAmountBP;
                                                                                    }

                                                                                    GeneralJournal _saveGeneralJournal = new GeneralJournal(_currSession)
                                                                                    {
                                                                                        Company = _locPayableTransactionBank.CompanyDefault,
                                                                                        Workplace = _locPayableTransactionBank.Workplace,
                                                                                        PostingDate = now,
                                                                                        PostingType = PostingType.Purchase,
                                                                                        OrderType = OrderType.Item,
                                                                                        PostingMethod = PostingMethod.Payment,
                                                                                        PostingMethodType = PostingMethodType.Normal,
                                                                                        Account = _locAccountMapLine.Account,
                                                                                        Debit = _locTotalAmountDebit,
                                                                                        Credit = _locTotalAmountCredit,
                                                                                        JournalMonth = _locPayableTransactionLine.JournalMonth,
                                                                                        JournalYear = _locPayableTransactionLine.JournalYear,
                                                                                        PayableTransaction = _locPayableTransactionXPO,
                                                                                    };

                                                                                    _saveGeneralJournal.Save();
                                                                                    _saveGeneralJournal.Session.CommitTransaction();

                                                                                    #region AccountingPeriodicLine

                                                                                    if (_locAccountMapLine.Account.Code != null)
                                                                                    {

                                                                                        //Cari Account Periodic Line
                                                                                        AccountingPeriodicLine _locAPL = _currSession.FindObject<AccountingPeriodicLine>
                                                                                                                        (new GroupOperator(GroupOperatorType.And,
                                                                                                                        new BinaryOperator("Company", _locPayableTransactionBank.CompanyDefault),
                                                                                                                        new BinaryOperator("Workplace", _locPayableTransactionBank.Workplace),
                                                                                                                        new BinaryOperator("AccountNo", _locAccountMapLine.Account),
                                                                                                                        new BinaryOperator("Month", _locPayableTransactionLine.JournalMonth),
                                                                                                                        new BinaryOperator("Year", _locPayableTransactionLine.JournalYear)));
                                                                                        if (_locAPL != null)
                                                                                        {
                                                                                            if (_locAPL.AccountNo != null)
                                                                                            {
                                                                                                if (_locAPL.AccountNo.BalanceType == BalanceType.Change)
                                                                                                {
                                                                                                    if (_locAPL.AccountNo.AccountCharge == AccountCharge.Debit)
                                                                                                    {
                                                                                                        if (_locTotalAmountDebit > 0)
                                                                                                        {
                                                                                                            _locTotalBalance = _locAPL.Balance + _locTotalAmountDebit;
                                                                                                        }
                                                                                                        if (_locTotalAmountCredit > 0)
                                                                                                        {
                                                                                                            _locTotalBalance = _locAPL.Balance - _locTotalAmountCredit;
                                                                                                        }
                                                                                                    }
                                                                                                    if (_locAPL.AccountNo.AccountCharge == AccountCharge.Credit)
                                                                                                    {
                                                                                                        if (_locTotalAmountDebit > 0)
                                                                                                        {
                                                                                                            _locTotalBalance = _locAPL.Balance - _locTotalAmountDebit;
                                                                                                        }
                                                                                                        if (_locTotalAmountCredit > 0)
                                                                                                        {
                                                                                                            _locTotalBalance = _locAPL.Balance + _locTotalAmountCredit;
                                                                                                        }
                                                                                                    }
                                                                                                }

                                                                                                _locAPL.Balance = _locTotalBalance;
                                                                                                _locAPL.Debit = _locAPL.Debit + _locTotalAmountDebit;
                                                                                                _locAPL.Credit = _locAPL.Credit + _locTotalAmountCredit;
                                                                                                _locAPL.Save();
                                                                                                _locAPL.Session.CommitTransaction();

                                                                                            }
                                                                                        }
                                                                                    }
                                                                                    #endregion AccountingPeriodicLine
                                                                                }
                                                                            }
                                                                        }
                                                                        #endregion NormalAmount   
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        #endregion JournalMapBusinessPartnerAccountGroup

                                        #region JournalMapCompanyAccountGroup
                                        if (_locPayableTransactionBank.BankAccount != null)
                                        {
                                            BankAccountMap _locBankAccountMap = _currSession.FindObject<BankAccountMap>(new GroupOperator(GroupOperatorType.And,
                                                                                            new BinaryOperator("Company", _locPayableTransactionLine.CompanyDefault),
                                                                                            new BinaryOperator("Workplace", _locPayableTransactionLine.Workplace),
                                                                                            new BinaryOperator("BankAccount", _locPayableTransactionBank.BankAccount),
                                                                                            new BinaryOperator("Active", true)));
                                            if (_locBankAccountMap != null)
                                            {
                                                if (_locBankAccountMap.BankAccountGroup != null)
                                                {
                                                    _locTotalAmountC = _locPayableTransactionBank.Amount;
                                                    _locCompanyBankAccountGroup = _locBankAccountMap.BankAccountGroup;

                                                    XPCollection<JournalMap> _locJournalMapItems = new XPCollection<JournalMap>
                                                                                   (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                    new BinaryOperator("Company", _locPayableTransactionBank.CompanyDefault),
                                                                                    new BinaryOperator("Workplace", _locPayableTransactionBank.Workplace),
                                                                                    new BinaryOperator("BankAccountGroup", _locCompanyBankAccountGroup),
                                                                                    new BinaryOperator("Active", true)));

                                                    if (_locJournalMapItems != null && _locJournalMapItems.Count() > 0)
                                                    {
                                                        foreach (JournalMap _locJournalMapItem in _locJournalMapItems)
                                                        {
                                                            XPCollection<JournalMapLine> _locJournalMapLines = new XPCollection<JournalMapLine>
                                                                                                               (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                                new BinaryOperator("Company", _locPayableTransactionBank.CompanyDefault),
                                                                                                                new BinaryOperator("Workplace", _locPayableTransactionBank.Workplace),
                                                                                                                new BinaryOperator("JournalMap", _locJournalMapItem),
                                                                                                                new BinaryOperator("Active", true)));

                                                            if (_locJournalMapLines != null && _locJournalMapLines.Count() > 0)
                                                            {
                                                                foreach (JournalMapLine _locJournalMapLine in _locJournalMapLines)
                                                                {
                                                                    if (_locJournalMapLine.AccountMap != null)
                                                                    {
                                                                        #region NormalAmount
                                                                        if (_locJournalMapLine.AccountMap.PostingType == PostingType.Purchase && _locJournalMapLine.AccountMap.OrderType == OrderType.Item
                                                                            && _locJournalMapLine.AccountMap.PostingMethod == PostingMethod.Payment && _locJournalMapLine.AccountMap.PostingMethodType == PostingMethodType.Normal)
                                                                        {
                                                                            XPCollection<AccountMapLine> _locAccountMapLines = new XPCollection<AccountMapLine>
                                                                                                                           (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                                            new BinaryOperator("Company", _locPayableTransactionBank.CompanyDefault),
                                                                                                                            new BinaryOperator("Workplace", _locPayableTransactionBank.Workplace),
                                                                                                                            new BinaryOperator("AccountMap", _locJournalMapLine.AccountMap),
                                                                                                                            new BinaryOperator("Active", true)));

                                                                            if (_locAccountMapLines != null && _locAccountMapLines.Count() > 0)
                                                                            {
                                                                                double _locTotalAmountDebit = 0;
                                                                                double _locTotalAmountCredit = 0;
                                                                                double _locTotalBalance = 0;

                                                                                foreach (AccountMapLine _locAccountMapLine in _locAccountMapLines)
                                                                                {
                                                                                    if (_locAccountMapLine.AccountCharge == AccountCharge.Debit)
                                                                                    {
                                                                                        _locTotalAmountDebit = _locTotalAmountC;
                                                                                    }
                                                                                    if (_locAccountMapLine.AccountCharge == AccountCharge.Credit)
                                                                                    {
                                                                                        _locTotalAmountCredit = _locTotalAmountC;
                                                                                    }

                                                                                    GeneralJournal _saveGeneralJournal = new GeneralJournal(_currSession)
                                                                                    {
                                                                                        Company = _locPayableTransactionBank.CompanyDefault,
                                                                                        Workplace = _locPayableTransactionBank.Workplace,
                                                                                        PostingDate = now,
                                                                                        PostingType = PostingType.Purchase,
                                                                                        OrderType = OrderType.Item,
                                                                                        PostingMethod = PostingMethod.Payment,
                                                                                        PostingMethodType = PostingMethodType.Normal,
                                                                                        Account = _locAccountMapLine.Account,
                                                                                        Debit = _locTotalAmountDebit,
                                                                                        Credit = _locTotalAmountCredit,
                                                                                        JournalMonth = _locPayableTransactionLine.JournalMonth,
                                                                                        JournalYear = _locPayableTransactionLine.JournalYear,
                                                                                        PayableTransaction = _locPayableTransactionXPO,
                                                                                    };

                                                                                    _saveGeneralJournal.Save();
                                                                                    _saveGeneralJournal.Session.CommitTransaction();

                                                                                    #region AccountingPeriodicLine

                                                                                    if (_locAccountMapLine.Account.Code != null)
                                                                                    {

                                                                                        //Cari Account Periodic Line
                                                                                        AccountingPeriodicLine _locAPL = _currSession.FindObject<AccountingPeriodicLine>
                                                                                                                        (new GroupOperator(GroupOperatorType.And,
                                                                                                                        new BinaryOperator("Company", _locPayableTransactionBank.CompanyDefault),
                                                                                                                        new BinaryOperator("Workplace", _locPayableTransactionBank.Workplace),
                                                                                                                        new BinaryOperator("AccountNo", _locAccountMapLine.Account),
                                                                                                                        new BinaryOperator("Month", _locPayableTransactionLine.JournalMonth),
                                                                                                                        new BinaryOperator("Year", _locPayableTransactionLine.JournalYear)));
                                                                                        if (_locAPL != null)
                                                                                        {
                                                                                            if (_locAPL.AccountNo != null)
                                                                                            {
                                                                                                if (_locAPL.AccountNo.BalanceType == BalanceType.Change)
                                                                                                {
                                                                                                    if (_locAPL.AccountNo.AccountCharge == AccountCharge.Debit)
                                                                                                    {
                                                                                                        if (_locTotalAmountDebit > 0)
                                                                                                        {
                                                                                                            _locTotalBalance = _locAPL.Balance + _locTotalAmountDebit;
                                                                                                        }
                                                                                                        if (_locTotalAmountCredit > 0)
                                                                                                        {
                                                                                                            _locTotalBalance = _locAPL.Balance - _locTotalAmountCredit;
                                                                                                        }
                                                                                                    }
                                                                                                    if (_locAPL.AccountNo.AccountCharge == AccountCharge.Credit)
                                                                                                    {
                                                                                                        if (_locTotalAmountDebit > 0)
                                                                                                        {
                                                                                                            _locTotalBalance = _locAPL.Balance - _locTotalAmountDebit;
                                                                                                        }
                                                                                                        if (_locTotalAmountCredit > 0)
                                                                                                        {
                                                                                                            _locTotalBalance = _locAPL.Balance + _locTotalAmountCredit;
                                                                                                        }
                                                                                                    }
                                                                                                }

                                                                                                _locAPL.Balance = _locTotalBalance;
                                                                                                _locAPL.Debit = _locAPL.Debit + _locTotalAmountDebit;
                                                                                                _locAPL.Credit = _locAPL.Credit + _locTotalAmountCredit;
                                                                                                _locAPL.Save();
                                                                                                _locAPL.Session.CommitTransaction();

                                                                                            }
                                                                                        }
                                                                                    }
                                                                                    #endregion AccountingPeriodicLine
                                                                                }
                                                                            }
                                                                        }
                                                                        #endregion NormalAmount 
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }

                                        #endregion JournalMapCompanyAccountGroup
                                    }
                                }
                            }
                            else
                            {
                                #region JournalMapBusinessPartnerAccountGroup
                                if (_locPayableTransactionLine.BankAccount != null)
                                {
                                    BankAccountMap _locBankAccountMap = _currSession.FindObject<BankAccountMap>(new GroupOperator(GroupOperatorType.And,
                                                                                            new BinaryOperator("Company", _locPayableTransactionLine.CompanyDefault),
                                                                                            new BinaryOperator("Workplace", _locPayableTransactionLine.Workplace),
                                                                                            new BinaryOperator("BankAccount", _locPayableTransactionLine.BankAccount),
                                                                                            new BinaryOperator("Active", true)));

                                    if (_locBankAccountMap != null)
                                    {
                                        if (_locBankAccountMap.BankAccountGroup != null)
                                        {
                                            _locBankAccountGroup = _locBankAccountMap.BankAccountGroup;

                                            _locTotalAmountBP = _locPayableTransactionLine.Credit;


                                            XPCollection<JournalMap> _locJournalMapItems = new XPCollection<JournalMap>
                                                                               (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                new BinaryOperator("Company", _locPayableTransactionLine.CompanyDefault),
                                                                                new BinaryOperator("Workplace", _locPayableTransactionLine.Workplace),
                                                                                new BinaryOperator("BankAccountGroup", _locBankAccountGroup),
                                                                                new BinaryOperator("Active", true)));

                                            if (_locJournalMapItems != null && _locJournalMapItems.Count() > 0)
                                            {
                                                foreach (JournalMap _locJournalMapItem in _locJournalMapItems)
                                                {
                                                    XPCollection<JournalMapLine> _locJournalMapLines = new XPCollection<JournalMapLine>
                                                                                                       (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                        new BinaryOperator("Company", _locPayableTransactionLine.CompanyDefault),
                                                                                                        new BinaryOperator("Workplace", _locPayableTransactionLine.Workplace),
                                                                                                        new BinaryOperator("JournalMap", _locJournalMapItem),
                                                                                                        new BinaryOperator("Active", true)));

                                                    if (_locJournalMapLines != null && _locJournalMapLines.Count() > 0)
                                                    {
                                                        foreach (JournalMapLine _locJournalMapLine in _locJournalMapLines)
                                                        {
                                                            if (_locJournalMapLine.AccountMap != null)
                                                            {
                                                                #region NormalAmount
                                                                if (_locJournalMapLine.AccountMap.PostingType == PostingType.Purchase && _locJournalMapLine.AccountMap.OrderType == OrderType.Item
                                                                    && _locJournalMapLine.AccountMap.PostingMethod == PostingMethod.Payment && _locJournalMapLine.AccountMap.PostingMethodType == PostingMethodType.Normal)
                                                                {
                                                                    XPCollection<AccountMapLine> _locAccountMapLines = new XPCollection<AccountMapLine>
                                                                                                                   (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                                    new BinaryOperator("Company", _locPayableTransactionLine.CompanyDefault),
                                                                                                                    new BinaryOperator("Workplace", _locPayableTransactionLine.Workplace),
                                                                                                                    new BinaryOperator("AccountMap", _locJournalMapLine.AccountMap),
                                                                                                                    new BinaryOperator("Active", true)));

                                                                    if (_locAccountMapLines != null && _locAccountMapLines.Count() > 0)
                                                                    {
                                                                        double _locTotalAmountDebit = 0;
                                                                        double _locTotalAmountCredit = 0;
                                                                        double _locTotalBalance = 0;

                                                                        foreach (AccountMapLine _locAccountMapLine in _locAccountMapLines)
                                                                        {
                                                                            if (_locAccountMapLine.AccountCharge == AccountCharge.Debit)
                                                                            {
                                                                                _locTotalAmountDebit = _locTotalAmountBP;
                                                                            }
                                                                            if (_locAccountMapLine.AccountCharge == AccountCharge.Credit)
                                                                            {
                                                                                _locTotalAmountCredit = _locTotalAmountBP;
                                                                            }

                                                                            GeneralJournal _saveGeneralJournal = new GeneralJournal(_currSession)
                                                                            {
                                                                                Company = _locPayableTransactionLine.CompanyDefault,
                                                                                Workplace = _locPayableTransactionLine.Workplace,
                                                                                PostingDate = now,
                                                                                PostingType = PostingType.Purchase,
                                                                                OrderType = OrderType.Item,
                                                                                PostingMethod = PostingMethod.Payment,
                                                                                PostingMethodType = PostingMethodType.Normal,
                                                                                Account = _locAccountMapLine.Account,
                                                                                Debit = _locTotalAmountDebit,
                                                                                Credit = _locTotalAmountCredit,
                                                                                JournalMonth = _locPayableTransactionLine.JournalMonth,
                                                                                JournalYear = _locPayableTransactionLine.JournalYear,
                                                                                PayableTransaction = _locPayableTransactionXPO,
                                                                            };

                                                                            _saveGeneralJournal.Save();
                                                                            _saveGeneralJournal.Session.CommitTransaction();

                                                                            #region AccountingPeriodicLine

                                                                            if (_locAccountMapLine.Account.Code != null)
                                                                            {

                                                                                //Cari Account Periodic Line
                                                                                AccountingPeriodicLine _locAPL = _currSession.FindObject<AccountingPeriodicLine>
                                                                                                                (new GroupOperator(GroupOperatorType.And,
                                                                                                                new BinaryOperator("Company", _locPayableTransactionLine.CompanyDefault),
                                                                                                                new BinaryOperator("Workplace", _locPayableTransactionLine.Workplace),
                                                                                                                new BinaryOperator("AccountNo", _locAccountMapLine.Account),
                                                                                                                new BinaryOperator("Month", _locPayableTransactionLine.JournalMonth),
                                                                                                                new BinaryOperator("Year", _locPayableTransactionLine.JournalYear)));
                                                                                if (_locAPL != null)
                                                                                {
                                                                                    if (_locAPL.AccountNo != null)
                                                                                    {
                                                                                        if (_locAPL.AccountNo.BalanceType == BalanceType.Change)
                                                                                        {
                                                                                            if (_locAPL.AccountNo.AccountCharge == AccountCharge.Debit)
                                                                                            {
                                                                                                if (_locTotalAmountDebit > 0)
                                                                                                {
                                                                                                    _locTotalBalance = _locAPL.Balance + _locTotalAmountDebit;
                                                                                                }
                                                                                                if (_locTotalAmountCredit > 0)
                                                                                                {
                                                                                                    _locTotalBalance = _locAPL.Balance - _locTotalAmountCredit;
                                                                                                }
                                                                                            }
                                                                                            if (_locAPL.AccountNo.AccountCharge == AccountCharge.Credit)
                                                                                            {
                                                                                                if (_locTotalAmountDebit > 0)
                                                                                                {
                                                                                                    _locTotalBalance = _locAPL.Balance - _locTotalAmountDebit;
                                                                                                }
                                                                                                if (_locTotalAmountCredit > 0)
                                                                                                {
                                                                                                    _locTotalBalance = _locAPL.Balance + _locTotalAmountCredit;
                                                                                                }
                                                                                            }
                                                                                        }

                                                                                        _locAPL.Balance = _locTotalBalance;
                                                                                        _locAPL.Debit = _locAPL.Debit + _locTotalAmountDebit;
                                                                                        _locAPL.Credit = _locAPL.Credit + _locTotalAmountCredit;
                                                                                        _locAPL.Save();
                                                                                        _locAPL.Session.CommitTransaction();

                                                                                    }
                                                                                }
                                                                            }
                                                                            #endregion AccountingPeriodicLine
                                                                        }
                                                                    }
                                                                }
                                                                #endregion NormalAmount   
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                                #endregion JournalMapBusinessPartnerAccountGroup

                                #region JournalMapCompanyAccountGroup
                                if (_locPayableTransactionLine.BankAccount != null)
                                {
                                    BankAccountMap _locBankAccountMap = _currSession.FindObject<BankAccountMap>(new GroupOperator(GroupOperatorType.And,
                                                                                            new BinaryOperator("Company", _locPayableTransactionLine.CompanyDefault),
                                                                                            new BinaryOperator("Workplace", _locPayableTransactionLine.Workplace),
                                                                                            new BinaryOperator("BankAccount", _locPayableTransactionLine.BankAccount),
                                                                                            new BinaryOperator("Active", true)));
                                    if (_locBankAccountMap != null)
                                    {
                                        if (_locBankAccountMap.BankAccountGroup != null)
                                        {
                                            _locTotalAmountC = _locPayableTransactionLine.Debit;
                                            _locCompanyBankAccountGroup = _locBankAccountMap.BankAccountGroup;

                                            XPCollection<JournalMap> _locJournalMapItems = new XPCollection<JournalMap>
                                                                           (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                            new BinaryOperator("Company", _locPayableTransactionLine.CompanyDefault),
                                                                            new BinaryOperator("Workplace", _locPayableTransactionLine.Workplace),
                                                                            new BinaryOperator("BankAccountGroup", _locCompanyBankAccountGroup),
                                                                            new BinaryOperator("Active", true)));

                                            if (_locJournalMapItems != null && _locJournalMapItems.Count() > 0)
                                            {
                                                foreach (JournalMap _locJournalMapItem in _locJournalMapItems)
                                                {
                                                    XPCollection<JournalMapLine> _locJournalMapLines = new XPCollection<JournalMapLine>
                                                                                                       (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                        new BinaryOperator("Company", _locPayableTransactionLine.CompanyDefault),
                                                                                                        new BinaryOperator("Workplace", _locPayableTransactionLine.Workplace),
                                                                                                        new BinaryOperator("JournalMap", _locJournalMapItem),
                                                                                                        new BinaryOperator("Active", true)));

                                                    if (_locJournalMapLines != null && _locJournalMapLines.Count() > 0)
                                                    {
                                                        foreach (JournalMapLine _locJournalMapLine in _locJournalMapLines)
                                                        {
                                                            if (_locJournalMapLine.AccountMap != null)
                                                            {
                                                                #region NormalAmount
                                                                if (_locJournalMapLine.AccountMap.PostingType == PostingType.Purchase && _locJournalMapLine.AccountMap.OrderType == OrderType.Item
                                                                    && _locJournalMapLine.AccountMap.PostingMethod == PostingMethod.Payment && _locJournalMapLine.AccountMap.PostingMethodType == PostingMethodType.Normal)
                                                                {
                                                                    XPCollection<AccountMapLine> _locAccountMapLines = new XPCollection<AccountMapLine>
                                                                                                                   (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                                    new BinaryOperator("Company", _locPayableTransactionLine.CompanyDefault),
                                                                                                                    new BinaryOperator("Workplace", _locPayableTransactionLine.Workplace),
                                                                                                                    new BinaryOperator("AccountMap", _locJournalMapLine.AccountMap),
                                                                                                                    new BinaryOperator("Active", true)));

                                                                    if (_locAccountMapLines != null && _locAccountMapLines.Count() > 0)
                                                                    {
                                                                        double _locTotalAmountDebit = 0;
                                                                        double _locTotalAmountCredit = 0;
                                                                        double _locTotalBalance = 0;

                                                                        foreach (AccountMapLine _locAccountMapLine in _locAccountMapLines)
                                                                        {
                                                                            if (_locAccountMapLine.AccountCharge == AccountCharge.Debit)
                                                                            {
                                                                                _locTotalAmountDebit = _locTotalAmountC;
                                                                            }
                                                                            if (_locAccountMapLine.AccountCharge == AccountCharge.Credit)
                                                                            {
                                                                                _locTotalAmountCredit = _locTotalAmountC;
                                                                            }

                                                                            GeneralJournal _saveGeneralJournal = new GeneralJournal(_currSession)
                                                                            {
                                                                                Company = _locPayableTransactionLine.CompanyDefault,
                                                                                Workplace = _locPayableTransactionLine.Workplace,
                                                                                PostingDate = now,
                                                                                PostingType = PostingType.Purchase,
                                                                                OrderType = OrderType.Item,
                                                                                PostingMethod = PostingMethod.Payment,
                                                                                PostingMethodType = PostingMethodType.Normal,
                                                                                Account = _locAccountMapLine.Account,
                                                                                Debit = _locTotalAmountDebit,
                                                                                Credit = _locTotalAmountCredit,
                                                                                JournalMonth = _locPayableTransactionLine.JournalMonth,
                                                                                JournalYear = _locPayableTransactionLine.JournalYear,
                                                                                PayableTransaction = _locPayableTransactionXPO,
                                                                            };

                                                                            _saveGeneralJournal.Save();
                                                                            _saveGeneralJournal.Session.CommitTransaction();

                                                                            #region AccountingPeriodicLine

                                                                            if (_locAccountMapLine.Account.Code != null)
                                                                            {

                                                                                //Cari Account Periodic Line
                                                                                AccountingPeriodicLine _locAPL = _currSession.FindObject<AccountingPeriodicLine>
                                                                                                                (new GroupOperator(GroupOperatorType.And,
                                                                                                                new BinaryOperator("Company", _locPayableTransactionLine.CompanyDefault),
                                                                                                                new BinaryOperator("Workplace", _locPayableTransactionLine.Workplace),
                                                                                                                new BinaryOperator("AccountNo", _locAccountMapLine.Account),
                                                                                                                new BinaryOperator("Month", _locPayableTransactionLine.JournalMonth),
                                                                                                                new BinaryOperator("Year", _locPayableTransactionLine.JournalYear)));
                                                                                if (_locAPL != null)
                                                                                {
                                                                                    if (_locAPL.AccountNo != null)
                                                                                    {
                                                                                        if (_locAPL.AccountNo.BalanceType == BalanceType.Change)
                                                                                        {
                                                                                            if (_locAPL.AccountNo.AccountCharge == AccountCharge.Debit)
                                                                                            {
                                                                                                if (_locTotalAmountDebit > 0)
                                                                                                {
                                                                                                    _locTotalBalance = _locAPL.Balance + _locTotalAmountDebit;
                                                                                                }
                                                                                                if (_locTotalAmountCredit > 0)
                                                                                                {
                                                                                                    _locTotalBalance = _locAPL.Balance - _locTotalAmountCredit;
                                                                                                }
                                                                                            }
                                                                                            if (_locAPL.AccountNo.AccountCharge == AccountCharge.Credit)
                                                                                            {
                                                                                                if (_locTotalAmountDebit > 0)
                                                                                                {
                                                                                                    _locTotalBalance = _locAPL.Balance - _locTotalAmountDebit;
                                                                                                }
                                                                                                if (_locTotalAmountCredit > 0)
                                                                                                {
                                                                                                    _locTotalBalance = _locAPL.Balance + _locTotalAmountCredit;
                                                                                                }
                                                                                            }
                                                                                        }

                                                                                        _locAPL.Balance = _locTotalBalance;
                                                                                        _locAPL.Debit = _locAPL.Debit + _locTotalAmountDebit;
                                                                                        _locAPL.Credit = _locAPL.Credit + _locTotalAmountCredit;
                                                                                        _locAPL.Save();
                                                                                        _locAPL.Session.CommitTransaction();

                                                                                    }
                                                                                }
                                                                            }
                                                                            #endregion AccountingPeriodicLine
                                                                        }
                                                                    }
                                                                }
                                                                #endregion NormalAmount 
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }

                                #endregion JournalMapCompanyAccountGroup
                            }
                            #endregion BankAccount
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = PayableTransaction ", ex.ToString());
            }
        }

        #endregion JournalPayable

        private void SetNormalPurchaseInvoiceMonitoring(Session _currSession, PayableTransaction _locPayableTransactionXPO)
        {
            try
            {
                XPQuery<PayableTransactionLine> _payableTransactionLineQuery1 = new XPQuery<PayableTransactionLine>(_currSession);

                var _payableTransactionLine1s = from ptl in _payableTransactionLineQuery1
                                                where (ptl.PayableTransaction == _locPayableTransactionXPO
                                                && ptl.Select == true
                                                && (ptl.Status == Status.Progress || ptl.Status == Status.Posted))
                                                group ptl by ptl.PurchaseInvoiceMonitoring into g
                                                select new { PurchaseInvoiceMonitoring = g.Key };
                if (_payableTransactionLine1s != null && _payableTransactionLine1s.Count() > 0)
                {
                    foreach (var _payableTransactionLine1 in _payableTransactionLine1s)
                    {
                        if (_payableTransactionLine1.PurchaseInvoiceMonitoring != null)
                        {
                            if (_payableTransactionLine1.PurchaseInvoiceMonitoring.Code != null)
                            {
                                PurchaseInvoiceMonitoring _locPurchaseInvoiceMonitoring = _currSession.FindObject<PurchaseInvoiceMonitoring>
                                                                        (new GroupOperator(GroupOperatorType.And,
                                                                        new BinaryOperator("Code", _payableTransactionLine1.PurchaseInvoiceMonitoring.Code)));
                                if (_locPurchaseInvoiceMonitoring != null)
                                {
                                    _locPurchaseInvoiceMonitoring.PayableTransaction = null;
                                    _locPurchaseInvoiceMonitoring.Save();
                                    _locPurchaseInvoiceMonitoring.Session.CommitTransaction();
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = PayableTransaction ", ex.ToString());
            }
        }

        #endregion PostingPurchaseInvoice

        #region PostingPurchasePrePaymentInvoice

        private bool CheckMaksAmountDebitBasedPurchasePrePaymentInvoiceMonitoring(Session _currSession, PayableTransaction _locPayableTransactionXPO)
        {
            bool _result = true;

            try
            {
                double _totDebit = 0;

                XPQuery<PayableTransactionLine> _payableTransactionLineQuerys = new XPQuery<PayableTransactionLine>(_currSession);

                var _payableTransactionLines = from ptl in _payableTransactionLineQuerys
                                               where (ptl.PayableTransaction == _locPayableTransactionXPO
                                               && ptl.Select == true
                                               && (ptl.Status == Status.Progress || ptl.Status == Status.Posted))
                                               group ptl by ptl.PurchasePrePaymentInvoiceMonitoring into g
                                               select new { PurchasePrePaymentInvoiceMonitoring = g.Key };

                if (_payableTransactionLines != null && _payableTransactionLines.Count() > 0)
                {
                    foreach (var _payableTransactionLine in _payableTransactionLines)
                    {
                        if (_payableTransactionLine.PurchasePrePaymentInvoiceMonitoring != null)
                        {
                            XPCollection<PayableTransactionLine> _locPayableTransactionLines = new XPCollection<PayableTransactionLine>
                                                                                            (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                            new BinaryOperator("PayableTransaction", _locPayableTransactionXPO),
                                                                                            new BinaryOperator("PurchasePrePaymentInvoiceMonitoring", _payableTransactionLine.PurchasePrePaymentInvoiceMonitoring),
                                                                                            new GroupOperator(GroupOperatorType.Or,
                                                                                            new BinaryOperator("Status", Status.Progress),
                                                                                            new BinaryOperator("Status", Status.Posted))));
                            if (_locPayableTransactionLines != null && _locPayableTransactionLines.Count() > 0)
                            {
                                foreach (PayableTransactionLine _locPayableTransactionLine in _locPayableTransactionLines)
                                {
                                    if (_locPayableTransactionLine.CloseCredit == true && _locPayableTransactionLine.Debit > 0)
                                    {
                                        _totDebit = _totDebit + _locPayableTransactionLine.Debit;
                                    }
                                }

                                if (_totDebit != _payableTransactionLine.PurchasePrePaymentInvoiceMonitoring.DP_Amount)
                                {
                                    _result = false;
                                }

                                _totDebit = 0;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = PayableTransaction ", ex.ToString());
            }

            return _result;
        }

        private bool CheckAmountDebitCreditBasedPurchasePrePaymentInvoiceMonitoring(Session _currSession, PayableTransaction _locPayableTransactionXPO)
        {
            bool _result = true;

            try
            {
                double _totDebit = 0;
                double _totCredit = 0;
                XPQuery<PayableTransactionLine> _payableTransactionLineQuerys = new XPQuery<PayableTransactionLine>(_currSession);

                var _payableTransactionLines = from ptl in _payableTransactionLineQuerys
                                               where (ptl.PayableTransaction == _locPayableTransactionXPO
                                               && ptl.Select == true
                                               && (ptl.Status == Status.Progress || ptl.Status == Status.Posted))
                                               group ptl by ptl.PurchasePrePaymentInvoiceMonitoring into g
                                               select new { PurchasePrePaymentInvoiceMonitoring = g.Key };

                if (_payableTransactionLines != null && _payableTransactionLines.Count() > 0)
                {
                    foreach (var _payableTransactionLine in _payableTransactionLines)
                    {
                        if (_payableTransactionLine.PurchasePrePaymentInvoiceMonitoring != null)
                        {
                            XPCollection<PayableTransactionLine> _locPayableTransactionLines = new XPCollection<PayableTransactionLine>
                                                                                            (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                            new BinaryOperator("PayableTransaction", _locPayableTransactionXPO),
                                                                                            new BinaryOperator("PurchasePrePaymentInvoiceMonitoring", _payableTransactionLine.PurchasePrePaymentInvoiceMonitoring),
                                                                                            new GroupOperator(GroupOperatorType.Or,
                                                                                            new BinaryOperator("Status", Status.Progress),
                                                                                            new BinaryOperator("Status", Status.Posted))));
                            if (_locPayableTransactionLines != null && _locPayableTransactionLines.Count() > 0)
                            {
                                foreach (PayableTransactionLine _locPayableTransactionLine in _locPayableTransactionLines)
                                {
                                    if (_locPayableTransactionLine.CloseCredit == true && _locPayableTransactionLine.Debit > 0)
                                    {
                                        _totDebit = _totDebit + _locPayableTransactionLine.Debit;
                                    }
                                    else if (_locPayableTransactionLine.CloseDebit == true && _locPayableTransactionLine.Credit > 0)
                                    {
                                        _totCredit = _totCredit + _locPayableTransactionLine.Credit;
                                    }
                                }

                                if (_totDebit != _totCredit)
                                {
                                    _result = false;
                                }
                                _totDebit = 0;
                                _totCredit = 0;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = PayableTransaction ", ex.ToString());
            }

            return _result;
        }     

        private void SetPayableTransactionMonitoringBasedPurchasePrePaymentInvoiceMonitoring(Session _currSession, PayableTransaction _locPayableTransactionXPO)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                double _totDebit = 0;
                double _totCredit = 0;
                string _currSignCode = null;
                DateTime now = DateTime.Now;

                XPQuery<PayableTransactionLine> _payableTransactionLineQuerys = new XPQuery<PayableTransactionLine>(_currSession);

                var _payableTransactionLines = from ptl in _payableTransactionLineQuerys
                                               where (ptl.PayableTransaction == _locPayableTransactionXPO
                                               && ptl.Select == true
                                               && (ptl.Status == Status.Progress || ptl.Status == Status.Posted))
                                               group ptl by ptl.PurchasePrePaymentInvoiceMonitoring into g
                                               select new { PurchasePrePaymentInvoiceMonitoring = g.Key };

                if (_payableTransactionLines != null && _payableTransactionLines.Count() > 0)
                {
                    foreach (var _payableTransactionLine in _payableTransactionLines)
                    {
                        if (_payableTransactionLine.PurchasePrePaymentInvoiceMonitoring != null)
                        {
                            XPCollection<PayableTransactionLine> _locPayableTransactionLines = new XPCollection<PayableTransactionLine>
                                                                                            (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                            new BinaryOperator("PayableTransaction", _locPayableTransactionXPO),
                                                                                            new BinaryOperator("PurchasePrePaymentInvoiceMonitoring", _payableTransactionLine.PurchasePrePaymentInvoiceMonitoring),
                                                                                            new GroupOperator(GroupOperatorType.Or,
                                                                                            new BinaryOperator("Status", Status.Progress),
                                                                                            new BinaryOperator("Status", Status.Posted))));

                            if (_locPayableTransactionLines != null && _locPayableTransactionLines.Count() > 0)
                            {
                                foreach (PayableTransactionLine _locPayableTransactionLine in _locPayableTransactionLines)
                                {
                                    if (_locPayableTransactionLine.CloseCredit == true && _locPayableTransactionLine.Debit > 0)
                                    {
                                        _totDebit = _totDebit + _locPayableTransactionLine.Debit;
                                    }
                                    else if (_locPayableTransactionLine.CloseDebit == true && _locPayableTransactionLine.Credit > 0)
                                    {
                                        _totCredit = _totCredit + _locPayableTransactionLine.Credit;
                                    }
                                }

                                _currSignCode = _globFunc.GetNumberingSignUnlockOptimisticRecordBasedOrganization(_currSession.DataLayer, ObjectList.PayableTransactionMonitoring, _locPayableTransactionXPO.CompanyDefault, _locPayableTransactionXPO.Workplace);
                                
                                if (_currSignCode != null)
                                {
                                    #region SavePayableTransactionMonitoring
                                    PayableTransactionMonitoring _saveDataPayableTransactionMonitoring = new PayableTransactionMonitoring(_currSession)
                                    {
                                        SignCode = _currSignCode,
                                        PayableTransaction = _locPayableTransactionXPO,
                                        PurchasePrePaymentInvoiceMonitoring = _payableTransactionLine.PurchasePrePaymentInvoiceMonitoring,
                                        PurchasePrePaymentInvoice = _payableTransactionLine.PurchasePrePaymentInvoiceMonitoring.PurchasePrePaymentInvoice,
                                        PurchaseOrder = _payableTransactionLine.PurchasePrePaymentInvoiceMonitoring.PurchaseOrder,
                                        Currency = _locPayableTransactionXPO.Currency,
                                        TotAmountDebit = _totDebit,
                                        TotAmountCredit = _totCredit,
                                        Company = _payableTransactionLine.PurchasePrePaymentInvoiceMonitoring.Company,
                                        Workplace = _payableTransactionLine.PurchasePrePaymentInvoiceMonitoring.Workplace,
                                        DocDate = _payableTransactionLine.PurchasePrePaymentInvoiceMonitoring.DocDate,
                                        JournalMonth = _payableTransactionLine.PurchasePrePaymentInvoiceMonitoring.JournalMonth,
                                        JournalYear = _payableTransactionLine.PurchasePrePaymentInvoiceMonitoring.JournalYear,
                                    };
                                    _saveDataPayableTransactionMonitoring.Save();
                                    _saveDataPayableTransactionMonitoring.Session.CommitTransaction();
                                    #endregion SavePayableTransactionMonitoring

                                    PayableTransactionMonitoring _locPayableTransactionMonitoring = _currSession.FindObject<PayableTransactionMonitoring>
                                                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                                                    new BinaryOperator("SignCode", _currSignCode)));
                                    if (_locPayableTransactionMonitoring != null)
                                    {
                                        #region PaymentOutPlan

                                        double _locPaid = 0;
                                        double _locOutstanding = 0;
                                        Status _locStatus = Status.Open;

                                        PaymentOutPlan _locPaymentOutPlan = _currSession.FindObject<PaymentOutPlan>
                                                                        (new GroupOperator(GroupOperatorType.And,
                                                                        new BinaryOperator("PurchasePrePaymentInvoiceMonitoring", _locPayableTransactionMonitoring.PurchasePrePaymentInvoiceMonitoring)));
                                        if (_locPaymentOutPlan != null)
                                        {
                                            _locPaid = _locPayableTransactionMonitoring.TotAmountCredit;
                                            _locOutstanding = _locPaymentOutPlan.Plan - _locPayableTransactionMonitoring.TotAmountCredit;
                                            if (_locOutstanding == 0)
                                            {
                                                _locStatus = Status.Close;
                                            }
                                            else
                                            {
                                                _locStatus = Status.Posted;
                                            }

                                            _locPaymentOutPlan.Paid = _locPaymentOutPlan.Paid + _locPaid;
                                            _locPaymentOutPlan.Outstanding = _locOutstanding;
                                            _locPaymentOutPlan.Status = _locStatus;
                                            _locPaymentOutPlan.StatusDate = now;
                                            _locPaymentOutPlan.PayableTransaction = _locPayableTransactionXPO;
                                            _locPaymentOutPlan.Save();
                                            _locPaymentOutPlan.Session.CommitTransaction();
                                        }

                                        #endregion PaymentOutPlan

                                        #region PurchasePrePaymentInvoiceMonitoring

                                        if (_locPayableTransactionMonitoring.PurchasePrePaymentInvoiceMonitoring != null)
                                        {
                                            if (_locPayableTransactionMonitoring.PurchasePrePaymentInvoiceMonitoring.Code != null)
                                            {
                                                PurchasePrePaymentInvoiceMonitoring _locPurchasePrePaymentInvoiceMonitoring = _currSession.FindObject<PurchasePrePaymentInvoiceMonitoring>
                                                                                        (new GroupOperator(GroupOperatorType.And,
                                                                                        new BinaryOperator("Code", _locPayableTransactionMonitoring.PurchasePrePaymentInvoiceMonitoring.Code)));
                                                if (_locPurchasePrePaymentInvoiceMonitoring != null)
                                                {
                                                    if (_locPurchasePrePaymentInvoiceMonitoring.DP_Amount == _locPayableTransactionMonitoring.TotAmountCredit)
                                                    {
                                                        _locPurchasePrePaymentInvoiceMonitoring.Status = Status.Close;
                                                        _locPurchasePrePaymentInvoiceMonitoring.StatusDate = now;
                                                        _locPurchasePrePaymentInvoiceMonitoring.Save();
                                                        _locPurchasePrePaymentInvoiceMonitoring.Session.CommitTransaction();
                                                    }
                                                    else
                                                    {
                                                        _locPurchasePrePaymentInvoiceMonitoring.Status = Status.Posted;
                                                        _locPurchasePrePaymentInvoiceMonitoring.StatusDate = now;
                                                        _locPurchasePrePaymentInvoiceMonitoring.Save();
                                                        _locPurchasePrePaymentInvoiceMonitoring.Session.CommitTransaction();
                                                    }
                                                }
                                            }
                                        }

                                        #endregion PurchasePrePaymentInvoiceMonitoring
                                    }
                                }
                                _totDebit = 0;
                                _totCredit = 0;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = PayableTransaction ", ex.ToString());
            }
        }

        #region JournalPayablePrePayment

        private bool CheckJournalSetupPrePayment(Session _currSession, PayableTransaction _locPayableTransactionXPO)
        {
            bool _result = false;
            try
            {
                if (_locPayableTransactionXPO != null)
                {
                    if (_locPayableTransactionXPO.CompanyDefault != null && _locPayableTransactionXPO.Workplace != null)
                    {
                        JournalSetupDetail _locJournalSetupDetail = _currSession.FindObject<JournalSetupDetail>
                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                    new BinaryOperator("Company", _locPayableTransactionXPO.CompanyDefault),
                                                                    new BinaryOperator("Workplace", _locPayableTransactionXPO.Workplace),
                                                                    new BinaryOperator("JournalSetupType", JournalSetupType.Journal),
                                                                    new BinaryOperator("ObjectList", ObjectList.PayableTransaction),
                                                                    new BinaryOperator("JournalProcess", JournalProcess.JournalPrePayment),
                                                                    new BinaryOperator("Active", true)));

                        if (_locJournalSetupDetail != null)
                        {
                            _result = true;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = PayableTransaction ", ex.ToString());
            }

            return _result;
        }

        private void SetPayableJournalBasedPurchasePrePaymentInvoice(Session _currSession, PayableTransaction _locPayableTransactionXPO)
        {
            try
            {
                DateTime now = DateTime.Now;
                double _locTotalAmountBP = 0;
                double _locTotalAmountC = 0;
                BusinessPartnerAccountGroup _locBusinessPartnerAccountGroup = null;
                CompanyAccountGroup _locCompanyAccountGroup = null;

                if (_locPayableTransactionXPO != null)
                {
                    XPCollection<PayableTransactionLine> _locPayableTransactionLines = new XPCollection<PayableTransactionLine>(_currSession,
                                                                        new GroupOperator(GroupOperatorType.And,
                                                                        new BinaryOperator("PayableTransaction", _locPayableTransactionXPO),
                                                                        new BinaryOperator("Select", true),
                                                                        new GroupOperator(GroupOperatorType.Or,
                                                                        new BinaryOperator("Status", Status.Progress),
                                                                        new BinaryOperator("Status", Status.Posted))));

                    if (_locPayableTransactionLines != null && _locPayableTransactionLines.Count() > 0)
                    {
                        foreach (PayableTransactionLine _locPayableTransactionLine in _locPayableTransactionLines)
                        {
                            #region JournalMapBusinessPartnerAccountGroup

                            if (_locPayableTransactionLine.PurchasePrePaymentInvoice.PayToVendor != null)
                            {
                                if (_locPayableTransactionLine.PurchasePrePaymentInvoice.PayToVendor.BusinessPartnerAccountGroup != null)
                                {
                                    _locBusinessPartnerAccountGroup = _locPayableTransactionLine.PurchasePrePaymentInvoice.PayToVendor.BusinessPartnerAccountGroup;

                                    _locTotalAmountBP = _locPayableTransactionLine.Credit;

                                    XPCollection<JournalMap> _locJournalMapItems = new XPCollection<JournalMap>
                                                                       (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                        new BinaryOperator("Company", _locPayableTransactionLine.Company),
                                                                        new BinaryOperator("Workplace", _locPayableTransactionLine.Workplace),
                                                                        new BinaryOperator("BusinessPartnerAccountGroup", _locBusinessPartnerAccountGroup),
                                                                        new BinaryOperator("Active", true)));

                                    if (_locJournalMapItems != null && _locJournalMapItems.Count() > 0)
                                    {
                                        foreach (JournalMap _locJournalMapItem in _locJournalMapItems)
                                        {
                                            XPCollection<JournalMapLine> _locJournalMapLines = new XPCollection<JournalMapLine>
                                                                                               (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                new BinaryOperator("Company", _locPayableTransactionLine.Company),
                                                                                                new BinaryOperator("Workplace", _locPayableTransactionLine.Workplace),
                                                                                                new BinaryOperator("JournalMap", _locJournalMapItem),
                                                                                                new BinaryOperator("Active", true)));

                                            if (_locJournalMapLines != null && _locJournalMapLines.Count() > 0)
                                            {
                                                foreach (JournalMapLine _locJournalMapLine in _locJournalMapLines)
                                                {
                                                    if (_locJournalMapLine.AccountMap != null)
                                                    {
                                                        #region NormalAmount
                                                        if (_locJournalMapLine.AccountMap.PostingType == PostingType.Purchase && _locJournalMapLine.AccountMap.OrderType == OrderType.Item
                                                            && _locJournalMapLine.AccountMap.PostingMethod == PostingMethod.PrePayment && _locJournalMapLine.AccountMap.PostingMethodType == PostingMethodType.Normal)
                                                        {
                                                            XPCollection<AccountMapLine> _locAccountMapLines = new XPCollection<AccountMapLine>
                                                                                                           (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                            new BinaryOperator("Company", _locPayableTransactionLine.Company),
                                                                                                            new BinaryOperator("Workplace", _locPayableTransactionLine.Workplace),
                                                                                                            new BinaryOperator("AccountMap", _locJournalMapLine.AccountMap),
                                                                                                            new BinaryOperator("Active", true)));

                                                            if (_locAccountMapLines != null && _locAccountMapLines.Count() > 0)
                                                            {
                                                                double _locTotalAmountDebit = 0;
                                                                double _locTotalAmountCredit = 0;
                                                                double _locTotalBalance = 0;

                                                                foreach (AccountMapLine _locAccountMapLine in _locAccountMapLines)
                                                                {
                                                                    if (_locAccountMapLine.AccountCharge == AccountCharge.Debit)
                                                                    {
                                                                        _locTotalAmountDebit = _locTotalAmountBP;
                                                                    }
                                                                    if (_locAccountMapLine.AccountCharge == AccountCharge.Credit)
                                                                    {
                                                                        _locTotalAmountCredit = _locTotalAmountBP;
                                                                    }

                                                                    GeneralJournal _saveGeneralJournal = new GeneralJournal(_currSession)
                                                                    {
                                                                        Company = _locPayableTransactionLine.CompanyDefault,
                                                                        Workplace = _locPayableTransactionLine.Workplace,
                                                                        PostingDate = now,
                                                                        PostingType = PostingType.Purchase,
                                                                        OrderType = OrderType.Item,
                                                                        PostingMethod = PostingMethod.PrePayment,
                                                                        PostingMethodType = PostingMethodType.Normal,
                                                                        Account = _locAccountMapLine.Account,
                                                                        Debit = _locTotalAmountDebit,
                                                                        Credit = _locTotalAmountCredit,
                                                                        JournalMonth = _locPayableTransactionLine.JournalMonth,
                                                                        JournalYear = _locPayableTransactionLine.JournalYear,
                                                                        PayableTransaction = _locPayableTransactionXPO,
                                                                    };

                                                                    _saveGeneralJournal.Save();
                                                                    _saveGeneralJournal.Session.CommitTransaction();

                                                                    #region AccountingPeriodicLine

                                                                    if (_locAccountMapLine.Account.Code != null)
                                                                    {
                                                                        //Cari Account Periodic Line
                                                                        AccountingPeriodicLine _locAPL = _currSession.FindObject<AccountingPeriodicLine>
                                                                                                        (new GroupOperator(GroupOperatorType.And,
                                                                                                        new BinaryOperator("Company", _locPayableTransactionLine.Company),
                                                                                                        new BinaryOperator("Workplace", _locPayableTransactionLine.Workplace),
                                                                                                        new BinaryOperator("AccountNo", _locAccountMapLine.Account),
                                                                                                        new BinaryOperator("Month", _locPayableTransactionLine.JournalMonth),
                                                                                                        new BinaryOperator("Year", _locPayableTransactionLine.JournalYear)));
                                                                        if (_locAPL != null)
                                                                        {
                                                                            if (_locAPL.AccountNo != null)
                                                                            {
                                                                                if (_locAPL.AccountNo.BalanceType == BalanceType.Change)
                                                                                {
                                                                                    if (_locAPL.AccountNo.AccountCharge == AccountCharge.Debit)
                                                                                    {
                                                                                        if (_locTotalAmountDebit > 0)
                                                                                        {
                                                                                            _locTotalBalance = _locAPL.Balance + _locTotalAmountDebit;
                                                                                        }
                                                                                        if (_locTotalAmountCredit > 0)
                                                                                        {
                                                                                            _locTotalBalance = _locAPL.Balance - _locTotalAmountCredit;
                                                                                        }
                                                                                    }
                                                                                    if (_locAPL.AccountNo.AccountCharge == AccountCharge.Credit)
                                                                                    {
                                                                                        if (_locTotalAmountDebit > 0)
                                                                                        {
                                                                                            _locTotalBalance = _locAPL.Balance - _locTotalAmountDebit;
                                                                                        }
                                                                                        if (_locTotalAmountCredit > 0)
                                                                                        {
                                                                                            _locTotalBalance = _locAPL.Balance + _locTotalAmountCredit;
                                                                                        }
                                                                                    }
                                                                                }

                                                                                _locAPL.Balance = _locTotalBalance;
                                                                                _locAPL.Debit = _locAPL.Debit + _locTotalAmountDebit;
                                                                                _locAPL.Credit = _locAPL.Credit + _locTotalAmountCredit;
                                                                                _locAPL.Save();
                                                                                _locAPL.Session.CommitTransaction();

                                                                            }
                                                                        }
                                                                    }
                                                                    #endregion AccountingPeriodicLine
                                                                }
                                                            }
                                                        }
                                                        #endregion NormalAmount   
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            #endregion JournalMapBusinessPartnerAccountGroup

                            #region JournalMapCompanyAccountGroup
                            if (_locPayableTransactionLine.Company != null)
                            {
                                _locTotalAmountC = _locPayableTransactionLine.Debit;

                                CompanyAccount _locCompanyAccount = _currSession.FindObject<CompanyAccount>(new GroupOperator(GroupOperatorType.And,
                                                        new BinaryOperator("Company", _locPayableTransactionLine.Company),
                                                        new BinaryOperator("Workplace", _locPayableTransactionLine.Workplace),
                                                        new BinaryOperator("Active", true)));

                                if (_locCompanyAccount != null)
                                {
                                    if (_locCompanyAccount.CompanyAccountGroup != null)
                                    {
                                        _locCompanyAccountGroup = _locCompanyAccount.CompanyAccountGroup;

                                        XPCollection<JournalMap> _locJournalMapItems = new XPCollection<JournalMap>
                                                                       (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                        new BinaryOperator("Company", _locPayableTransactionLine.Company),
                                                                        new BinaryOperator("Workplace", _locPayableTransactionLine.Workplace),
                                                                        new BinaryOperator("CompanyAccountGroup", _locCompanyAccountGroup),
                                                                        new BinaryOperator("Active", true)));

                                        if (_locJournalMapItems != null && _locJournalMapItems.Count() > 0)
                                        {
                                            foreach (JournalMap _locJournalMapItem in _locJournalMapItems)
                                            {
                                                XPCollection<JournalMapLine> _locJournalMapLines = new XPCollection<JournalMapLine>
                                                                                                   (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                    new BinaryOperator("Company", _locPayableTransactionLine.Company),
                                                                                                    new BinaryOperator("Workplace", _locPayableTransactionLine.Workplace),
                                                                                                    new BinaryOperator("JournalMap", _locJournalMapItem),
                                                                                                    new BinaryOperator("Active", true)));

                                                if (_locJournalMapLines != null && _locJournalMapLines.Count() > 0)
                                                {
                                                    foreach (JournalMapLine _locJournalMapLine in _locJournalMapLines)
                                                    {
                                                        if (_locJournalMapLine.AccountMap != null)
                                                        {
                                                            #region NormalAmount
                                                            if (_locJournalMapLine.AccountMap.PostingType == PostingType.Purchase && _locJournalMapLine.AccountMap.OrderType == OrderType.Item
                                                                && _locJournalMapLine.AccountMap.PostingMethod == PostingMethod.PrePayment && _locJournalMapLine.AccountMap.PostingMethodType == PostingMethodType.Normal)
                                                            {
                                                                XPCollection<AccountMapLine> _locAccountMapLines = new XPCollection<AccountMapLine>
                                                                                                               (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                                new BinaryOperator("Company", _locPayableTransactionLine.Company),
                                                                                                                new BinaryOperator("Workplace", _locPayableTransactionLine.Workplace),
                                                                                                                new BinaryOperator("AccountMap", _locJournalMapLine.AccountMap),
                                                                                                                new BinaryOperator("Active", true)));

                                                                if (_locAccountMapLines != null && _locAccountMapLines.Count() > 0)
                                                                {
                                                                    double _locTotalAmountDebit = 0;
                                                                    double _locTotalAmountCredit = 0;
                                                                    double _locTotalBalance = 0;

                                                                    foreach (AccountMapLine _locAccountMapLine in _locAccountMapLines)
                                                                    {
                                                                        if (_locAccountMapLine.AccountCharge == AccountCharge.Debit)
                                                                        {
                                                                            _locTotalAmountDebit = _locTotalAmountC;
                                                                        }
                                                                        if (_locAccountMapLine.AccountCharge == AccountCharge.Credit)
                                                                        {
                                                                            _locTotalAmountCredit = _locTotalAmountC;
                                                                        }

                                                                        GeneralJournal _saveGeneralJournal = new GeneralJournal(_currSession)
                                                                        {
                                                                            Company = _locPayableTransactionLine.CompanyDefault,
                                                                            Workplace = _locPayableTransactionLine.Workplace,
                                                                            PostingDate = now,
                                                                            PostingType = PostingType.Purchase,
                                                                            OrderType = OrderType.Item,
                                                                            PostingMethod = PostingMethod.PrePayment,
                                                                            PostingMethodType = PostingMethodType.Normal,
                                                                            Account = _locAccountMapLine.Account,
                                                                            Debit = _locTotalAmountDebit,
                                                                            Credit = _locTotalAmountCredit,
                                                                            JournalMonth = _locPayableTransactionLine.JournalMonth,
                                                                            JournalYear = _locPayableTransactionLine.JournalYear,
                                                                            PayableTransaction = _locPayableTransactionXPO,
                                                                        };

                                                                        _saveGeneralJournal.Save();
                                                                        _saveGeneralJournal.Session.CommitTransaction();

                                                                        #region AccountingPeriodicLine

                                                                        if (_locAccountMapLine.Account.Code != null)
                                                                        {
                                                                            //Cari Account Periodic Line
                                                                            AccountingPeriodicLine _locAPL = _currSession.FindObject<AccountingPeriodicLine>
                                                                                                            (new GroupOperator(GroupOperatorType.And,
                                                                                                            new BinaryOperator("Company", _locPayableTransactionLine.Company),
                                                                                                            new BinaryOperator("Workplace", _locPayableTransactionLine.Workplace),
                                                                                                            new BinaryOperator("AccountNo", _locAccountMapLine.Account),
                                                                                                            new BinaryOperator("Month", _locPayableTransactionLine.JournalMonth),
                                                                                                            new BinaryOperator("Year", _locPayableTransactionLine.JournalYear)));
                                                                            if (_locAPL != null)
                                                                            {
                                                                                if (_locAPL.AccountNo != null)
                                                                                {
                                                                                    if (_locAPL.AccountNo.BalanceType == BalanceType.Change)
                                                                                    {
                                                                                        if (_locAPL.AccountNo.AccountCharge == AccountCharge.Debit)
                                                                                        {
                                                                                            if (_locTotalAmountDebit > 0)
                                                                                            {
                                                                                                _locTotalBalance = _locAPL.Balance + _locTotalAmountDebit;
                                                                                            }
                                                                                            if (_locTotalAmountCredit > 0)
                                                                                            {
                                                                                                _locTotalBalance = _locAPL.Balance - _locTotalAmountCredit;
                                                                                            }
                                                                                        }
                                                                                        if (_locAPL.AccountNo.AccountCharge == AccountCharge.Credit)
                                                                                        {
                                                                                            if (_locTotalAmountDebit > 0)
                                                                                            {
                                                                                                _locTotalBalance = _locAPL.Balance - _locTotalAmountDebit;
                                                                                            }
                                                                                            if (_locTotalAmountCredit > 0)
                                                                                            {
                                                                                                _locTotalBalance = _locAPL.Balance + _locTotalAmountCredit;
                                                                                            }
                                                                                        }
                                                                                    }

                                                                                    _locAPL.Balance = _locTotalBalance;
                                                                                    _locAPL.Debit = _locAPL.Debit + _locTotalAmountDebit;
                                                                                    _locAPL.Credit = _locAPL.Credit + _locTotalAmountCredit;
                                                                                    _locAPL.Save();
                                                                                    _locAPL.Session.CommitTransaction();

                                                                                }
                                                                            }
                                                                        }
                                                                        #endregion AccountingPeriodicLine
                                                                    }
                                                                }
                                                            }
                                                            #endregion NormalAmount 
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            #endregion JournalMapCompanyAccountGroup
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = PayableTransaction ", ex.ToString());
            }
        }

        #endregion JournalPayablePrePayment

        private void SetNormalPurchasePrePaymentInvoiceMonitoring(Session _currSession, PayableTransaction _locPayableTransactionXPO)
        {
            try
            {
                XPQuery<PayableTransactionLine> _payableTransactionLineQuery1 = new XPQuery<PayableTransactionLine>(_currSession);

                var _payableTransactionLine1s = from ptl in _payableTransactionLineQuery1
                                                where (ptl.PayableTransaction == _locPayableTransactionXPO
                                                && ptl.Select == true
                                                && (ptl.Status == Status.Progress || ptl.Status == Status.Posted))
                                                group ptl by ptl.PurchasePrePaymentInvoiceMonitoring into g
                                                select new { PurchasePrePaymentInvoiceMonitoring = g.Key };

                if (_payableTransactionLine1s != null && _payableTransactionLine1s.Count() > 0)
                {
                    foreach (var _payableTransactionLine1 in _payableTransactionLine1s)
                    {
                        if (_payableTransactionLine1.PurchasePrePaymentInvoiceMonitoring != null)
                        {
                            if (_payableTransactionLine1.PurchasePrePaymentInvoiceMonitoring.Code != null)
                            {
                                PurchasePrePaymentInvoiceMonitoring _locPurchasePrePaymentInvoiceMonitoring = _currSession.FindObject<PurchasePrePaymentInvoiceMonitoring>
                                                                        (new GroupOperator(GroupOperatorType.And,
                                                                        new BinaryOperator("Code", _payableTransactionLine1.PurchasePrePaymentInvoiceMonitoring.Code)));
                                if (_locPurchasePrePaymentInvoiceMonitoring != null)
                                {
                                    _locPurchasePrePaymentInvoiceMonitoring.PayableTransaction = null;
                                    _locPurchasePrePaymentInvoiceMonitoring.Save();
                                    _locPurchasePrePaymentInvoiceMonitoring.Session.CommitTransaction();
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = PayableTransaction ", ex.ToString());
            }
        }

        #endregion PostingPurchasePrePaymentInvoice

        private void SetStatusPayableTransactionLine(Session _currSession, PayableTransaction _locPayableTransactionXPO)
        {
            try
            {
                DateTime now = DateTime.Now;
                if (_locPayableTransactionXPO != null)
                {
                    XPCollection<PayableTransactionLine> _locPayableTransactionLineLines = new XPCollection<PayableTransactionLine>(_currSession,
                                                            new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("PayableTransaction", _locPayableTransactionXPO),
                                                            new BinaryOperator("Select", true),
                                                            new GroupOperator(GroupOperatorType.Or,
                                                            new BinaryOperator("Status", Status.Progress),
                                                            new BinaryOperator("Status", Status.Posted))));

                    if (_locPayableTransactionLineLines != null && _locPayableTransactionLineLines.Count > 0)
                    {
                        foreach (PayableTransactionLine _locPayableTransactionLine in _locPayableTransactionLineLines)
                        {
                            _locPayableTransactionLine.Status = Status.Close;
                            _locPayableTransactionLine.ActivationPosting = true;
                            _locPayableTransactionLine.StatusDate = now;
                            _locPayableTransactionLine.Save();
                            _locPayableTransactionLine.Session.CommitTransaction();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PayableTransaction " + ex.ToString());
            }
        }

        private void SetFinalStatusPayableTransaction(Session _currSession, PayableTransaction _locPayableTransactionXPO)
        {
            try
            {
                DateTime now = DateTime.Now;
                int _locCount = 0;

                if (_locPayableTransactionXPO != null)
                {
                    XPCollection<PayableTransactionLine> _locPayableTransactionLines = new XPCollection<PayableTransactionLine>(_currSession,
                                                                                             new GroupOperator(GroupOperatorType.And,
                                                                                             new BinaryOperator("PayableTransaction", _locPayableTransactionXPO),
                                                                                             new GroupOperator(GroupOperatorType.Or,
                                                                                             new BinaryOperator("Status", Status.Progress),
                                                                                             new BinaryOperator("Status", Status.Posted),
                                                                                             new BinaryOperator("Status", Status.Close))));

                    if (_locPayableTransactionLines != null && _locPayableTransactionLines.Count() > 0)
                    {
                        foreach (PayableTransactionLine _locPayableTransactionLine in _locPayableTransactionLines)
                        {
                            if (_locPayableTransactionLine.Status == Status.Close)
                            {
                                _locCount = _locCount + 1;
                            }
                        }

                        if (_locCount == _locPayableTransactionLines.Count())
                        {
                            _locPayableTransactionXPO.ActivationPosting = true;
                            _locPayableTransactionXPO.Status = Status.Close;
                            _locPayableTransactionXPO.StatusDate = now;
                            _locPayableTransactionXPO.Save();
                            _locPayableTransactionXPO.Session.CommitTransaction();
                        }
                        else
                        {
                            _locPayableTransactionXPO.Status = Status.Posted;
                            _locPayableTransactionXPO.StatusDate = now;
                            _locPayableTransactionXPO.Save();
                            _locPayableTransactionXPO.Session.CommitTransaction();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PayableTransaction " + ex.ToString());
            }
        }

        private void SetCloseAllPurchaseProcess(Session _currSession, PayableTransaction _locPayableTransactionXPO)
        {
            //GroupByPaymentOutPlanBasedOnPurchaseInvoice
            //Cari Payable Transaction Monitoring 
            DateTime now = DateTime.Now;
            double _locTotPayPO = 0;
            double _locTotPayPI = 0;
            double _locTotPayPPI = 0;

            if (_locPayableTransactionXPO != null)
            {
                XPCollection<PayableTransactionMonitoring> _locPayableTransactionMonitorings = new XPCollection<PayableTransactionMonitoring>
                                                                                               (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                               new BinaryOperator("PayableTransaction", _locPayableTransactionXPO)));

                if (_locPayableTransactionMonitorings != null && _locPayableTransactionMonitorings.Count() > 0)
                {
                    foreach (PayableTransactionMonitoring _locPayableTransactionMonitoring in _locPayableTransactionMonitorings)
                    {
                        #region PurchaseOrderClose
                        if (_locPayableTransactionMonitoring.PurchaseOrder != null)
                        {
                            XPCollection<PaymentOutPlan> _locPaymentOutPlans = new XPCollection<PaymentOutPlan>
                                                                            (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                            new BinaryOperator("PurchaseOrder", _locPayableTransactionMonitoring.PurchaseOrder)));

                            if (_locPaymentOutPlans != null && _locPaymentOutPlans.Count() > 0)
                            {
                                foreach (PaymentOutPlan _locPaymentOutPlan in _locPaymentOutPlans)
                                {
                                    _locTotPayPO = _locTotPayPO + _locPaymentOutPlan.Paid;
                                }

                                if (_locTotPayPO > 0)
                                {
                                    if (_locTotPayPO == _locPayableTransactionMonitoring.PurchaseOrder.Amount)
                                    {
                                        _locPayableTransactionMonitoring.PurchaseOrder.Status = Status.Close;
                                        _locPayableTransactionMonitoring.PurchaseOrder.StatusDate = now;
                                        _locPayableTransactionMonitoring.PurchaseOrder.Save();
                                        _locPayableTransactionMonitoring.PurchaseOrder.Session.CommitTransaction();
                                    }
                                }
                            }
                        }
                        #endregion PurchaseOrderClose

                        #region PurchaseInvoiceClose
                        if (_locPayableTransactionMonitoring.PurchaseInvoice != null)
                        {
                            XPCollection<PaymentOutPlan> _locPaymentOutPlans = new XPCollection<PaymentOutPlan>
                                                                            (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                            new BinaryOperator("PurchaseInvoice", _locPayableTransactionMonitoring.PurchaseInvoice)));

                            if (_locPaymentOutPlans != null && _locPaymentOutPlans.Count() > 0)
                            {
                                foreach (PaymentOutPlan _locPaymentOutPlan in _locPaymentOutPlans)
                                {
                                    _locTotPayPI = _locTotPayPI + _locPaymentOutPlan.Paid;
                                }

                                if (_locTotPayPI > 0)
                                {
                                    if (_locTotPayPI == _locPayableTransactionMonitoring.PurchaseInvoice.Amount)
                                    {
                                        _locPayableTransactionMonitoring.PurchaseInvoice.Status = Status.Close;
                                        _locPayableTransactionMonitoring.PurchaseInvoice.StatusDate = now;
                                        _locPayableTransactionMonitoring.PurchaseInvoice.Save();
                                        _locPayableTransactionMonitoring.PurchaseInvoice.Session.CommitTransaction();
                                    }
                                }
                            }
                        }
                        #endregion PurchaseInvoiceClose

                        #region PurchasePrePaymentInvoiceClose
                        if (_locPayableTransactionMonitoring.PurchasePrePaymentInvoice != null)
                        {
                            XPCollection<PaymentOutPlan> _locPaymentOutPlans = new XPCollection<PaymentOutPlan>
                                                                            (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                            new BinaryOperator("PurchasePrePaymentInvoice", _locPayableTransactionMonitoring.PurchasePrePaymentInvoice)));

                            if (_locPaymentOutPlans != null && _locPaymentOutPlans.Count() > 0)
                            {
                                foreach (PaymentOutPlan _locPaymentOutPlan in _locPaymentOutPlans)
                                {
                                    _locTotPayPPI = _locTotPayPPI + _locPaymentOutPlan.Paid;
                                }

                                if (_locTotPayPPI > 0)
                                {
                                    if (_locTotPayPPI == _locPayableTransactionMonitoring.PurchasePrePaymentInvoice.DP_Amount)
                                    {
                                        _locPayableTransactionMonitoring.PurchasePrePaymentInvoice.Status = Status.Close;
                                        _locPayableTransactionMonitoring.PurchasePrePaymentInvoice.StatusDate = now;
                                        _locPayableTransactionMonitoring.PurchasePrePaymentInvoice.Save();
                                        _locPayableTransactionMonitoring.PurchasePrePaymentInvoice.Session.CommitTransaction();
                                    }
                                }
                            }
                        }
                        #endregion PurchasePrePaymentInvoiceClose
                    }
                }
            }
        }

        private void SetNormal(Session _currSession, PayableTransaction _locPayableTransactionXPO)
        {
            try
            {
                if (_locPayableTransactionXPO != null)
                {
                    XPCollection<PayableTransactionLine> _locPayableTransactionLines = new XPCollection<PayableTransactionLine>(_currSession,
                                                                                     new GroupOperator(GroupOperatorType.And,
                                                                                     new BinaryOperator("PayableTransaction", _locPayableTransactionXPO),
                                                                                     new BinaryOperator("Select", true),
                                                                                     new GroupOperator(GroupOperatorType.Or,
                                                                                     new BinaryOperator("Status", Status.Progress),
                                                                                     new BinaryOperator("Status", Status.Posted),
                                                                                     new BinaryOperator("Status", Status.Close))));

                    if (_locPayableTransactionLines != null && _locPayableTransactionLines.Count > 0)
                    {
                        foreach (PayableTransactionLine _locPayableTransactionLine in _locPayableTransactionLines)
                        {
                            _locPayableTransactionLine.Select = false;
                            _locPayableTransactionLine.Save();
                            _locPayableTransactionLine.Session.CommitTransaction();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PayableTransaction " + ex.ToString());
            }
        }

        #endregion Posting

        #region Global Method

        private void SuccessMessageShow(string _locMessage)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 4000;
            options.Message = string.Format("{0}", _locMessage);
            options.Type = InformationType.Success;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Success";
            options.Win.Type = WinMessageType.Flyout;
            Application.ShowViewStrategy.ShowMessage(options);
        }

        private void ErrorMessageShow(string _locMessage)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 2000;
            options.Message = string.Format(_locMessage);
            options.Type = InformationType.Warning;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Error";
            options.Win.Type = WinMessageType.Alert;
            Application.ShowViewStrategy.ShowMessage(options);
        }

        #endregion Global Method
        
    }
}
