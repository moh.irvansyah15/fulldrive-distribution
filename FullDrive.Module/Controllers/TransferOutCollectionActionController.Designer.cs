﻿namespace FullDrive.Module.Controllers
{
    partial class TransferOutCollectionActionController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.TransferOutCollectionShowTOMAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            // 
            // TransferOutCollectionShowTOMAction
            // 
            this.TransferOutCollectionShowTOMAction.Caption = "Show TOM";
            this.TransferOutCollectionShowTOMAction.ConfirmationMessage = null;
            this.TransferOutCollectionShowTOMAction.Id = "TransferOutCollectionShowTOMActionId";
            this.TransferOutCollectionShowTOMAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.TransferOutCollection);
            this.TransferOutCollectionShowTOMAction.ToolTip = null;
            this.TransferOutCollectionShowTOMAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.TransferOutCollectionShowTOMAction_Execute);
            // 
            // TransferOutCollectionActionController
            // 
            this.Actions.Add(this.TransferOutCollectionShowTOMAction);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SimpleAction TransferOutCollectionShowTOMAction;
    }
}
