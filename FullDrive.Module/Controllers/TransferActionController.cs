﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;

using FullDrive.Module.BusinessObjects;
using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.Xpo;
using System.Collections;
using DevExpress.Xpo;

namespace FullDrive.Module.Controllers
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class TransferActionController : ViewController
    {
        private ChoiceActionItem _selectionListviewFilter;

        public TransferActionController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.
            #region Filter Status
            TransferListviewFilterSelectionAction.Items.Clear();
            foreach (object _currApproval in Enum.GetValues(typeof(CustomProcess.Status)))
            {
                EnumDescriptor _ed = new EnumDescriptor(typeof(CustomProcess.Status));
                _selectionListviewFilter = new ChoiceActionItem(_ed.GetCaption(_currApproval), _currApproval);
                TransferListviewFilterSelectionAction.Items.Add(_selectionListviewFilter);
            }
            #endregion Filter Status
        }
        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }
        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        private void TransferProgressAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;
                Status _locStatus = Status.None;
                DateTime _locNow;
                Status _locStatus2 = Status.None;
                DateTime _locNow2;

                if (_objectsToProcess != null)
                {
                    foreach (Object obj in _objectsToProcess)
                    {
                        Transfer _locTransferOS = (Transfer)_objectSpace.GetObject(obj);

                        if (_locTransferOS != null)
                        {
                            if (_locTransferOS.Session != null)
                            {
                                _currSession = _locTransferOS.Session;
                            }
                            if (_locTransferOS.Code != null && _currSession != null)
                            {
                                _currObjectId = _locTransferOS.Code;

                                Transfer _locTransferXPO = _currSession.FindObject<Transfer>
                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                     new BinaryOperator("Code", _currObjectId),
                                                                     new GroupOperator(GroupOperatorType.Or,
                                                                     new BinaryOperator("Status", Status.Open),
                                                                     new BinaryOperator("Status", Status.Progress))));

                                if (_locTransferXPO != null)
                                {
                                    if (_locTransferXPO.Status == Status.Open)
                                    {
                                        _locStatus = Status.Progress;
                                        _locNow = now;
                                    }
                                    else
                                    {
                                        _locStatus = _locTransferXPO.Status;
                                        _locNow = _locTransferXPO.StatusDate;
                                    }
                                    _locTransferXPO.Status = _locStatus;
                                    _locTransferXPO.StatusDate = _locNow;
                                    _locTransferXPO.Save();
                                    _locTransferXPO.Session.CommitTransaction();

                                    #region TransferOutLine and TransferOutLot
                                    XPCollection<TransferLine> _locTransferLines = new XPCollection<TransferLine>
                                                           (_currSession, new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("Transfer", _locTransferXPO),
                                                            new GroupOperator(GroupOperatorType.Or,
                                                            new BinaryOperator("Status", Status.Open),
                                                            new BinaryOperator("Status", Status.Progress))));

                                    if (_locTransferLines != null && _locTransferLines.Count > 0)
                                    {
                                        foreach (TransferLine _locTransferLine in _locTransferLines)
                                        {
                                            if (_locTransferLine.Status == Status.Open)
                                            {
                                                _locStatus2 = Status.Progress;
                                                _locNow2 = now;
                                            }
                                            else
                                            {
                                                _locStatus2 = _locTransferLine.Status;
                                                _locNow2 = _locTransferLine.StatusDate;
                                            }

                                            _locTransferLine.Status = _locStatus2;
                                            _locTransferLine.StatusDate = _locNow2;
                                            _locTransferLine.Save();
                                            _locTransferLine.Session.CommitTransaction();
                                            

                                            
                                        }
                                    }
                                    #endregion TransferOutLine and TransferOutLot

                                    SetBegInvLineTo(_currSession, _locTransferXPO);

                                    SuccessMessageShow(_locTransferXPO.Code + " has been change successfully to Progress");
                                    
                                }
                                else
                                {
                                    ErrorMessageShow("Data Transfer Out Not Available");
                                }
                            }
                            else
                            {
                                ErrorMessageShow("Data Transfer Out Not Available");
                            }
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = Transfer " + ex.ToString());
            }
        }

        private void TransferPostingAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;

                if (_objectsToProcess != null)
                {
                    foreach (Object obj in _objectsToProcess)
                    {
                        Transfer _locTransferOS = (Transfer)_objectSpace.GetObject(obj);

                        if (_locTransferOS != null)
                        {
                            if (_locTransferOS.Status == Status.Progress || _locTransferOS.Status == Status.Posted)
                            {
                                if (_locTransferOS.Session != null)
                                {
                                    _currSession = _locTransferOS.Session;
                                }
                                if (_locTransferOS.Code != null && _currSession != null)
                                {
                                    _currObjectId = _locTransferOS.Code;

                                    Transfer _locTransferXPO = _currSession.FindObject<Transfer>
                                                                        (new GroupOperator(GroupOperatorType.And,
                                                                         new BinaryOperator("Code", _currObjectId),
                                                                         new GroupOperator(GroupOperatorType.Or,
                                                                         new BinaryOperator("Status", Status.Progress),
                                                                         new BinaryOperator("Status", Status.Posted))));

                                    if (_locTransferXPO != null)
                                    {
                                        if (CheckAvailableStock(_currSession, _locTransferXPO) == true && CheckLockStock(_currSession, _locTransferXPO) == false)
                                        {
                                            SetTransferBeginingInventory(_currSession, _locTransferXPO);
                                            SetTransferInventoryJournal(_currSession, _locTransferXPO);
                                            SetTransferMonitoring(_currSession, _locTransferXPO);
                                            SetStatusTransferLine(_currSession, _locTransferXPO);
                                            SetNormalQuantity(_currSession, _locTransferXPO);
                                            SetFinalStatusTransfer(_currSession, _locTransferXPO);

                                            SuccessMessageShow(_locTransferXPO.Code + " has been change successfully to Deliver");
                                        }
                                        else
                                        {
                                            ErrorMessageShow("Please check stock quantity");
                                        }
                                    }
                                    else
                                    {
                                        ErrorMessageShow("Transfer Out Data Not Available");
                                    }
                                }
                            }
                            else
                            {
                                ErrorMessageShow("Transfer Out Data Not Available");
                            }
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = Transfer " + ex.ToString());
            }
        }

        private void TransferListviewFilterSelectionAction_Execute(object sender, SingleChoiceActionExecuteEventArgs e)
        {
            try
            {
                if ((View is ListView) & (View.ObjectTypeInfo.Type == typeof(Transfer)))
                {
                    if ((Status)e.SelectedChoiceActionItem.Data == Status.None)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.None, BinaryOperatorType.NotEqual);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Open)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Open, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Progress)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Progress, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Posted)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Posted, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Lock)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Lock, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Close)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Close, BinaryOperatorType.Equal);
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = Transfer " + ex.ToString());
            }
        }

        #region Progress

        private void SetBegInvLineTo(Session _currSession, Transfer _locTransferXPO)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                string _locSignCode = null;

                if (_locTransferXPO != null)
                {
                    XPCollection<TransferLine> _locTransferLines = new XPCollection<TransferLine>(_currSession,
                                                                        new GroupOperator(GroupOperatorType.And,
                                                                        new BinaryOperator("Transfer", _locTransferXPO),
                                                                        new GroupOperator(GroupOperatorType.Or,
                                                                        new BinaryOperator("Status", Status.Open),
                                                                        new BinaryOperator("Status", Status.Progress))));

                    if (_locTransferLines != null && _locTransferLines.Count() > 0)
                    {
                        foreach (TransferLine _locTransferLine in _locTransferLines)
                        {

                            if (_locTransferLine.Company != null && _locTransferLine.WorkplaceTo != null && _locTransferLine.BegInvTo != null
                                && _locTransferLine.Item != null && _locTransferLine.BegInvLineTo == null)
                            {
                                BeginningInventoryLine _locBegInvLine = null;

                                if (_locTransferLine.DUOM != null)
                                {
                                    #region Update
                                    if (_locTransferLine.BinLocationTo != null)
                                    {
                                        //DefaultUOM
                                        _locBegInvLine = _currSession.FindObject<BeginningInventoryLine>(
                                                   new GroupOperator(GroupOperatorType.And,
                                                   new BinaryOperator("Company", _locTransferLine.Company),
                                                   new BinaryOperator("Workplace", _locTransferLine.WorkplaceTo),
                                                   new BinaryOperator("BeginningInventory", _locTransferLine.BegInvTo),
                                                   new BinaryOperator("Item", _locTransferLine.Item),
                                                   new BinaryOperator("DefaultUOM", _locTransferLine.DUOM),
                                                   new BinaryOperator("BinLocation", _locTransferLine.BinLocationTo),
                                                   new BinaryOperator("LocationType", _locTransferLine.LocationTypeTo),
                                                   new BinaryOperator("StockType", _locTransferLine.StockTypeTo),
                                                   new BinaryOperator("StockGroup", _locTransferLine.StockGroupTo),
                                                   new BinaryOperator("Lock", false),
                                                   new BinaryOperator("Active", true)));
                                    }
                                    else
                                    {
                                        _locBegInvLine = _currSession.FindObject<BeginningInventoryLine>(
                                                   new GroupOperator(GroupOperatorType.And,
                                                   new BinaryOperator("Company", _locTransferLine.Company),
                                                   new BinaryOperator("Workplace", _locTransferLine.WorkplaceTo),
                                                   new BinaryOperator("BeginningInventory", _locTransferLine.BegInvTo),
                                                   new BinaryOperator("Item", _locTransferLine.Item),
                                                   new BinaryOperator("DefaultUOM", _locTransferLine.DUOM),
                                                   new BinaryOperator("LocationType", _locTransferLine.LocationTypeTo),
                                                   new BinaryOperator("StockType", _locTransferLine.StockTypeTo),
                                                   new BinaryOperator("StockGroup", _locTransferLine.StockGroupTo),
                                                   new BinaryOperator("Lock", false),
                                                   new BinaryOperator("Active", true)));
                                    }

                                    if (_locBegInvLine != null)
                                    {
                                        _locTransferLine.BegInvLineTo = _locBegInvLine;
                                        _locTransferLine.Save();
                                        _locTransferLine.Session.CommitTransaction();
                                    }
                                    #endregion Update
                                    #region CreateNew
                                    else
                                    {
                                        _locSignCode = _globFunc.GetNumberingSignUnlockOptimisticRecordBasedOrganization(_currSession.DataLayer, ObjectList.BeginningInventoryLine, _locTransferLine.Company, _locTransferLine.WorkplaceFrom);
                                        if (_locSignCode != null)
                                        {
                                            BeginningInventoryLine _locSaveDataBeginingInventoryLine = new BeginningInventoryLine(_currSession)
                                            {
                                                SignCode = _locSignCode,
                                                Company = _locTransferXPO.Company,
                                                Workplace = _locTransferXPO.WorkplaceTo,
                                                Item = _locTransferLine.Item,
                                                Location = _locTransferLine.LocationTo,
                                                BinLocation = _locTransferLine.BinLocationTo,
                                                DefaultUOM = _locTransferLine.DUOM,
                                                LocationType = _locTransferLine.LocationTypeTo,
                                                StockType = _locTransferLine.StockTypeTo,
                                                StockGroup = _locTransferLine.StockGroupTo,
                                                Lock = false,
                                                Active = true,
                                                BeginningInventory = _locTransferLine.BegInvTo,
                                            };

                                            _locSaveDataBeginingInventoryLine.Save();
                                            _locSaveDataBeginingInventoryLine.Session.CommitTransaction();

                                            BeginningInventoryLine _locBegInvLine2 = _currSession.FindObject<BeginningInventoryLine>(new GroupOperator
                                                                                    (GroupOperatorType.And,
                                                                                    new BinaryOperator("SignCode", _locSignCode)));
                                            if (_locBegInvLine2 != null)
                                            {
                                                _locTransferLine.BegInvLineTo = _locBegInvLine2;
                                                _locTransferLine.Save();
                                                _locTransferLine.Session.CommitTransaction();
                                            }
                                        }
                                    }
                                    #endregion CreateNew
                                }

                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Module = TransferOut " + ex.ToString());
            }
        }

        #endregion Progress

        #region Posting

        private bool CheckLockStock(Session _currSession, Transfer _locTransferXPO)
        {
            bool _result = false;
            try
            {
                bool _locLock = false;

                if (_locTransferXPO != null)
                {
                    XPCollection<TransferLine> _locTransferLines = new XPCollection<TransferLine>(_currSession,
                                                                        new GroupOperator(GroupOperatorType.And,
                                                                        new BinaryOperator("Transfer", _locTransferXPO),
                                                                        new GroupOperator(GroupOperatorType.Or,
                                                                        new BinaryOperator("Status", Status.Progress),
                                                                        new BinaryOperator("Status", Status.Posted))));
                    if (_locTransferLines != null && _locTransferLines.Count() > 0)
                    {
                        foreach (TransferLine _locTransferLine in _locTransferLines)
                        {
                            if (_locTransferLine.BegInvLineFrom != null)
                            {
                                if (_locTransferLine.BegInvLineFrom.Lock == true)
                                {
                                    _locLock = true;
                                }
                            }
                        }

                        _result = _locLock;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Module = Transfer " + ex.ToString());
            }
            return _result;
        }

        private bool CheckAvailableStock(Session _currSession, Transfer _locTransferXPO)
        {
            bool _result = true;
            try
            {
                if (_locTransferXPO != null)
                {
                    XPCollection<TransferLine> _locTransferLines = new XPCollection<TransferLine>(_currSession,
                                                                        new GroupOperator(GroupOperatorType.And,
                                                                        new BinaryOperator("Transfer", _locTransferXPO),
                                                                        new BinaryOperator("Select", true),
                                                                        new GroupOperator(GroupOperatorType.Or,
                                                                        new BinaryOperator("Status", Status.Progress),
                                                                        new BinaryOperator("Status", Status.Posted))));

                    if (_locTransferLines != null && _locTransferLines.Count > 0)
                    {

                        foreach (TransferLine _locTransferLine in _locTransferLines)
                        {
                            if (_locTransferLine.BegInvFrom != null)
                            {
                                if (_locTransferLine.BegInvLineFrom != null)
                                {
                                    if (_locTransferLine.TQty > _locTransferLine.BegInvLineFrom.QtyAvailable)
                                    {
                                        _result = false;
                                    }
                                }
                            }
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = Transfer ", ex.ToString());
            }
            return _result;
        }

        private void SetTransferBeginingInventory(Session _currSession, Transfer _locTransferXPO)
        {
            try
            {
                XPCollection<TransferLine> _locTransferLines = new XPCollection<TransferLine>(_currSession,
                                                                        new GroupOperator(GroupOperatorType.And,
                                                                        new BinaryOperator("Transfer", _locTransferXPO),
                                                                        new BinaryOperator("Select", true),
                                                                        new GroupOperator(GroupOperatorType.Or,
                                                                        new BinaryOperator("Status", Status.Progress),
                                                                        new BinaryOperator("Status", Status.Posted))));

                if (_locTransferLines != null && _locTransferLines.Count() > 0)
                {
                    GlobalFunction _globFunc = new GlobalFunction();

                    foreach (TransferLine _locTransferLine in _locTransferLines)
                    {
                        if (_locTransferLine.DQty > 0 || _locTransferLine.Qty > 0)
                        {
                            #region BeginningInventoryFrom
                            if (_locTransferLine.BegInvFrom != null && _locTransferLine.BegInvLineFrom != null)
                            {
                                _locTransferLine.BegInvLineFrom.QtyAvailable = _locTransferLine.BegInvLineFrom.QtyAvailable - _locTransferLine.TQty;
                                _locTransferLine.BegInvLineFrom.Save();
                                _locTransferLine.BegInvLineFrom.Session.CommitTransaction();

                                _locTransferLine.BegInvFrom.QtyAvailable = _locTransferLine.BegInvFrom.QtyAvailable - _locTransferLine.TQty;
                                _locTransferLine.BegInvFrom.Save();
                                _locTransferLine.BegInvFrom.Session.CommitTransaction();

                            }
                            #endregion BeginningInventoryFrom
                            #region BeginningInventoryTo
                            if (_locTransferLine.BegInvTo != null && _locTransferLine.BegInvLineTo != null)
                            {
                                _locTransferLine.BegInvLineTo.QtyAvailable = _locTransferLine.BegInvLineTo.QtyAvailable + _locTransferLine.TQty;
                                _locTransferLine.BegInvLineTo.Save();
                                _locTransferLine.BegInvLineTo.Session.CommitTransaction();

                                _locTransferLine.BegInvTo.QtyAvailable = _locTransferLine.BegInvTo.QtyAvailable + _locTransferLine.TQty;
                                _locTransferLine.BegInvTo.Save();
                                _locTransferLine.BegInvTo.Session.CommitTransaction();
                            }
                            #endregion BeginningInventoryTo
                            

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = Transfer ", ex.ToString());
            }
        }

        private void SetTransferInventoryJournal(Session _currSession, Transfer _locTransferXPO)
        {
            try
            {
                XPCollection<TransferLine> _locTransferLines = new XPCollection<TransferLine>(_currSession,
                                                                        new GroupOperator(GroupOperatorType.And,
                                                                        new BinaryOperator("Transfer", _locTransferXPO),
                                                                        new BinaryOperator("Select", true),
                                                                        new GroupOperator(GroupOperatorType.Or,
                                                                        new BinaryOperator("Status", Status.Progress),
                                                                        new BinaryOperator("Status", Status.Posted))));

                if (_locTransferLines != null && _locTransferLines.Count > 0)
                {
                    double _locInvLineTotal = 0;
                    DateTime now = DateTime.Now;

                    foreach (TransferLine _locTransferLine in _locTransferLines)
                    {
                        if (_locTransferLine.DQty > 0 || _locTransferLine.Qty > 0)
                        {
                            #region Negatif
                            if (_locTransferLine.Item != null && _locTransferLine.UOM != null && _locTransferLine.DUOM != null)
                            {
                                ItemUnitOfMeasure _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                    (new GroupOperator(GroupOperatorType.And,
                                                     new BinaryOperator("Item", _locTransferLine.Item),
                                                     new BinaryOperator("UOM", _locTransferLine.UOM),
                                                     new BinaryOperator("DefaultUOM", _locTransferLine.DUOM),
                                                     new BinaryOperator("Active", true)));
                                if (_locItemUOM != null)
                                {
                                    #region Code
                                    if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                    {
                                        _locInvLineTotal = _locTransferLine.Qty * _locItemUOM.DefaultConversion + _locTransferLine.DQty;
                                    }
                                    else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                    {
                                        _locInvLineTotal = _locTransferLine.Qty / _locItemUOM.Conversion + _locTransferLine.DQty;
                                    }
                                    else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                    {
                                        _locInvLineTotal = _locTransferLine.Qty + _locTransferLine.DQty;
                                    }
                                    #endregion Code

                                    InventoryJournal _locNegatifInventoryJournal = new InventoryJournal(_currSession)
                                    {
                                        DocumentType = _locTransferXPO.DocumentType,
                                        DocNo = _locTransferXPO.DocNo,
                                        Location = _locTransferLine.LocationFrom,
                                        BinLocation = _locTransferLine.BinLocationFrom,
                                        LocationType = _locTransferLine.LocationTypeFrom,
                                        StockType = _locTransferLine.StockTypeFrom,
                                        StockGroup = _locTransferLine.StockGroupFrom,
                                        Item = _locTransferLine.Item,
                                        Company = _locTransferLine.Company,
                                        Workplace = _locTransferLine.WorkplaceFrom,
                                        QtyPos = 0,
                                        QtyNeg = _locInvLineTotal,
                                        DUOM = _locTransferLine.DUOM,
                                        JournalDate = now,
                                        Transfer = _locTransferXPO,
                                    };
                                    _locNegatifInventoryJournal.Save();
                                    _locNegatifInventoryJournal.Session.CommitTransaction();

                                }
                            }
                            else
                            {
                                _locInvLineTotal = _locTransferLine.Qty + _locTransferLine.DQty;

                                InventoryJournal _locNegatifInventoryJournal = new InventoryJournal(_currSession)
                                {
                                    DocumentType = _locTransferXPO.DocumentType,
                                    DocNo = _locTransferXPO.DocNo,
                                    Location = _locTransferLine.LocationFrom,
                                    BinLocation = _locTransferLine.BinLocationFrom,
                                    LocationType = _locTransferLine.LocationTypeFrom,
                                    StockType = _locTransferLine.StockTypeFrom,
                                    StockGroup = _locTransferLine.StockGroupFrom,
                                    Item = _locTransferLine.Item,
                                    Company = _locTransferLine.Company,
                                    Workplace = _locTransferLine.WorkplaceFrom,
                                    QtyPos = 0,
                                    QtyNeg = _locInvLineTotal,
                                    DUOM = _locTransferLine.DUOM,
                                    JournalDate = now,
                                    Transfer = _locTransferXPO,
                                };
                                _locNegatifInventoryJournal.Save();
                                _locNegatifInventoryJournal.Session.CommitTransaction();
                            }
                            #endregion Negatif
                            #region Positif
                            if (_locTransferLine.Item != null && _locTransferLine.UOM != null && _locTransferLine.DUOM != null)
                            {
                                ItemUnitOfMeasure _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                    (new GroupOperator(GroupOperatorType.And,
                                                     new BinaryOperator("Item", _locTransferLine.Item),
                                                     new BinaryOperator("UOM", _locTransferLine.UOM),
                                                     new BinaryOperator("DefaultUOM", _locTransferLine.DUOM),
                                                     new BinaryOperator("Active", true)));
                                if (_locItemUOM != null)
                                {
                                    #region Code
                                    if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                    {
                                        _locInvLineTotal = _locTransferLine.Qty * _locItemUOM.DefaultConversion + _locTransferLine.DQty;
                                    }
                                    else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                    {
                                        _locInvLineTotal = _locTransferLine.Qty / _locItemUOM.Conversion + _locTransferLine.DQty;
                                    }
                                    else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                    {
                                        _locInvLineTotal = _locTransferLine.Qty + _locTransferLine.DQty;
                                    }
                                    #endregion Code

                                    InventoryJournal _locPositifInventoryJournal = new InventoryJournal(_currSession)
                                    {
                                        DocumentType = _locTransferXPO.DocumentType,
                                        DocNo = _locTransferXPO.DocNo,
                                        Location = _locTransferLine.LocationTo,
                                        BinLocation = _locTransferLine.BinLocationTo,
                                        LocationType = _locTransferLine.LocationTypeTo,
                                        StockType = _locTransferLine.StockTypeTo,
                                        StockGroup = _locTransferLine.StockGroupTo,
                                        Item = _locTransferLine.Item,
                                        Company = _locTransferLine.Company,
                                        Workplace = _locTransferLine.WorkplaceTo,
                                        QtyPos = _locInvLineTotal,
                                        QtyNeg = 0,
                                        DUOM = _locTransferLine.DUOM,
                                        JournalDate = now,
                                        Transfer = _locTransferXPO,
                                    };
                                    _locPositifInventoryJournal.Save();
                                    _locPositifInventoryJournal.Session.CommitTransaction();

                                }
                            }
                            else
                            {
                                _locInvLineTotal = _locTransferLine.Qty + _locTransferLine.DQty;

                                InventoryJournal _locPositifInventoryJournal = new InventoryJournal(_currSession)
                                {
                                    DocumentType = _locTransferXPO.DocumentType,
                                    DocNo = _locTransferXPO.DocNo,
                                    Location = _locTransferLine.LocationTo,
                                    BinLocation = _locTransferLine.BinLocationTo,
                                    LocationType = _locTransferLine.LocationTypeTo,
                                    StockType = _locTransferLine.StockTypeTo,
                                    StockGroup = _locTransferLine.StockGroupTo,
                                    Item = _locTransferLine.Item,
                                    Company = _locTransferLine.Company,
                                    Workplace = _locTransferLine.WorkplaceTo,
                                    QtyPos = _locInvLineTotal,
                                    QtyNeg = 0,
                                    DUOM = _locTransferLine.DUOM,
                                    JournalDate = now,
                                    Transfer = _locTransferXPO,
                                };
                                _locPositifInventoryJournal.Save();
                                _locPositifInventoryJournal.Session.CommitTransaction();
                            }
                            
                            #endregion Positif
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = Transfer ", ex.ToString());
            }
        }

        private void SetTransferMonitoring(Session _currSession, Transfer _locTransferXPO)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                DateTime now = DateTime.Now;

                if (_locTransferXPO != null)
                {
                    XPCollection<TransferLine> _locTransferLines = new XPCollection<TransferLine>
                                                            (_currSession, new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("Transfer", _locTransferXPO),
                                                            new BinaryOperator("Select", true),
                                                            new GroupOperator(GroupOperatorType.Or,
                                                            new BinaryOperator("Status", Status.Progress),
                                                            new BinaryOperator("Status", Status.Posted))));

                    if (_locTransferLines != null && _locTransferLines.Count() > 0)
                    {
                        foreach (TransferLine _locTransferLine in _locTransferLines)
                        {
                            TransferMonitoring _saveDataTransferMonitoring = new TransferMonitoring(_currSession)
                            {
                                Transfer = _locTransferXPO,
                                TransferLine = _locTransferLine,
                                Company = _locTransferLine.Company,
                                Division = _locTransferLine.Division,
                                Department = _locTransferLine.Department,
                                Section = _locTransferLine.Section,
                                Employee = _locTransferLine.Employee,
                                LocationTypeFrom = _locTransferLine.LocationTypeFrom,
                                StockTypeFrom = _locTransferLine.StockTypeFrom,
                                WorkplaceFrom = _locTransferLine.WorkplaceFrom,
                                LocationFrom = _locTransferLine.LocationFrom,
                                BinLocationFrom = _locTransferLine.BinLocationFrom,
                                StockGroupFrom = _locTransferLine.StockGroupFrom,
                                BegInvFrom = _locTransferLine.BegInvFrom,
                                LocationTypeTo = _locTransferLine.LocationTypeTo,
                                WorkplaceTo = _locTransferLine.WorkplaceTo,
                                LocationTo = _locTransferLine.LocationTo,
                                BinLocationTo = _locTransferLine.BinLocationTo,
                                StockTypeTo = _locTransferLine.StockTypeTo,
                                StockGroupTo = _locTransferLine.StockGroupTo,
                                BegInvTo = _locTransferLine.BegInvTo,
                                Item = _locTransferLine.Item,
                                Brand = _locTransferLine.Brand,
                                Description = _locTransferLine.Description,
                                BegInvLineFrom = _locTransferLine.BegInvLineFrom,
                                BegInvLineTo = _locTransferLine.BegInvLineTo,
                                DQty = _locTransferLine.DQty,
                                DUOM = _locTransferLine.DUOM,
                                Qty = _locTransferLine.Qty,
                                UOM = _locTransferLine.UOM,
                                TQty = _locTransferLine.TQty,
                            };
                            _saveDataTransferMonitoring.Save();
                            _saveDataTransferMonitoring.Session.CommitTransaction();
                        }
                    }
                    
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = Transfer ", ex.ToString());
            }
        }

        private void SetStatusTransferLine(Session _currSession, Transfer _locTransferXPO)
        {
            try
            {
                DateTime now = DateTime.Now;
                if (_locTransferXPO != null)
                {
                    XPCollection<TransferLine> _locTransferLines = new XPCollection<TransferLine>(_currSession,
                                                            new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("Transfer", _locTransferXPO),
                                                            new BinaryOperator("Select", true),
                                                            new GroupOperator(GroupOperatorType.Or,
                                                            new BinaryOperator("Status", Status.Progress),
                                                            new BinaryOperator("Status", Status.Posted))));

                    if (_locTransferLines != null && _locTransferLines.Count > 0)
                    {
                        foreach (TransferLine _locTransferLine in _locTransferLines)
                        {
                            _locTransferLine.Status = Status.Close;
                            _locTransferLine.ActivationPosting = true;
                            _locTransferLine.StatusDate = now;
                            _locTransferLine.Save();
                            _locTransferLine.Session.CommitTransaction();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = Transfer " + ex.ToString());
            }
        }

        private void SetNormalQuantity(Session _currSession, Transfer _locTransferXPO)
        {
            try
            {
                if (_locTransferXPO != null)
                {
                    XPCollection<TransferLine> _locTransferLines = new XPCollection<TransferLine>(_currSession,
                                                                                     new GroupOperator(GroupOperatorType.And,
                                                                                     new BinaryOperator("Transfer", _locTransferXPO),
                                                                                     new BinaryOperator("Select", true),
                                                                                     new GroupOperator(GroupOperatorType.Or,
                                                                                     new BinaryOperator("Status", Status.Progress),
                                                                                     new BinaryOperator("Status", Status.Posted),
                                                                                     new BinaryOperator("Status", Status.Close))));

                    if (_locTransferLines != null && _locTransferLines.Count > 0)
                    {
                        foreach (TransferLine _locTransferLine in _locTransferLines)
                        {
                            if (_locTransferLine.DQty > 0 || _locTransferLine.Qty > 0)
                            {
                                _locTransferLine.Select = false;
                                _locTransferLine.Save();
                                _locTransferLine.Session.CommitTransaction();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = Transfer " + ex.ToString());
            }
        }

        private void SetFinalStatusTransfer(Session _currSession, Transfer _locTransferXPO)
        {
            try
            {
                DateTime now = DateTime.Now;
                int _locStatusCount = 0;
                int _locTransLineCount = 0;

                if (_locTransferXPO != null)
                {
                    XPCollection<TransferLine> _locTransferLines = new XPCollection<TransferLine>(_currSession,
                                                                                     new GroupOperator(GroupOperatorType.And,
                                                                                     new BinaryOperator("Transfer", _locTransferXPO)));

                    if (_locTransferLines != null && _locTransferLines.Count() > 0)
                    {
                        _locTransLineCount = _locTransferLines.Count();

                        foreach (TransferLine _locTransferLine in _locTransferLines)
                        {

                            if (_locTransferLine.Status == Status.Close)
                            {
                                _locStatusCount = _locStatusCount + 1;
                            }
                        }

                        if (_locTransLineCount == _locStatusCount)
                        {
                            _locTransferXPO.ActivationPosting = true;
                            _locTransferXPO.Status = Status.Close;
                            _locTransferXPO.StatusDate = now;
                            _locTransferXPO.PostedCount = _locTransferXPO.PostedCount + 1;
                            _locTransferXPO.Save();
                            _locTransferXPO.Session.CommitTransaction();
                        }
                        else
                        {
                            _locTransferXPO.Status = Status.Posted;
                            _locTransferXPO.StatusDate = now;
                            _locTransferXPO.PostedCount = _locTransferXPO.PostedCount + 1;
                            _locTransferXPO.Save();
                            _locTransferXPO.Session.CommitTransaction();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = TransferOut " + ex.ToString());
            }
        }

        #endregion Posting

        #region Global Method

        private void SuccessMessageShow(string _locMessage)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 4000;
            options.Message = string.Format("{0}", _locMessage);
            options.Type = InformationType.Success;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Success";
            options.Win.Type = WinMessageType.Flyout;
            Application.ShowViewStrategy.ShowMessage(options);
        }

        private void ErrorMessageShow(string _locMessage)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 2000;
            options.Message = string.Format(_locMessage);
            options.Type = InformationType.Warning;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Error";
            options.Win.Type = WinMessageType.Alert;
            Application.ShowViewStrategy.ShowMessage(options);
        }

        private int GetStockType(StockType objectName)
        {
            int _result = 0;
            try
            {
                if (objectName == StockType.Good)
                {
                    _result = 1;
                }
                else if (objectName == StockType.Bad)
                {
                    _result = 2;
                }
                else
                {
                    _result = 0;
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Module = TransferOut " + ex.ToString());
            }
            return _result;
        }

        private int GetStockGroup(StockGroup objectName)
        {
            int _result = 0;
            try
            {
                if (objectName == StockGroup.Normal)
                {
                    _result = 1;
                }
                else if (objectName == StockGroup.FreeItem)
                {
                    _result = 2;
                }
                else
                {
                    _result = 0;
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Module = TransferOut " + ex.ToString());
            }
            return _result;
        }

        private int GetActive(bool objectName)
        {
            int _result = 0;
            try
            {
                if (objectName == true)
                {
                    _result = 1;
                }
                else
                {
                    _result = 0;
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Module = TransferOut " + ex.ToString());
            }
            return _result;
        }

        #endregion Global Method
    }
}
