﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;

using FullDrive.Module.BusinessObjects;
using FullDrive.Module.CustomProcess;
using System.Collections;
using DevExpress.Xpo;
using DevExpress.ExpressApp.Xpo;

namespace FullDrive.Module.Controllers
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class TransferInActionController : ViewController
    {
        private ChoiceActionItem _selectionListviewFilter;

        public TransferInActionController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.
            #region Filter Status
            TransferInListviewFilterSelectionAction.Items.Clear();
            foreach (object _currApproval in Enum.GetValues(typeof(CustomProcess.Status)))
            {

                EnumDescriptor _ed = new EnumDescriptor(typeof(CustomProcess.Status));
                _selectionListviewFilter = new ChoiceActionItem(_ed.GetCaption(_currApproval), _currApproval);
                TransferInListviewFilterSelectionAction.Items.Add(_selectionListviewFilter);
            }
            #endregion Filter Status
        }

        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
        }

        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }

        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        private void TransferInGetStockAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;

                if (_objectsToProcess != null)
                {
                    foreach (Object obj in _objectsToProcess)
                    {
                        TransferIn _locTransferInOS = (TransferIn)_objectSpace.GetObject(obj);

                        if (_locTransferInOS != null)
                        {
                            if (_locTransferInOS.Session != null)
                            {
                                _currSession = _locTransferInOS.Session;
                            }

                            if (_locTransferInOS.Code != null && _currSession != null)
                            {
                                _currObjectId = _locTransferInOS.Code;

                                TransferIn _locTransferInXPO = _currSession.FindObject<TransferIn>
                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                     new BinaryOperator("Code", _currObjectId)));

                                if (_locTransferInXPO != null)
                                {
                                    if (_locTransferInXPO.Status == Status.Open || _locTransferInXPO.Status == Status.Progress)
                                    {
                                        XPCollection<TransferOutCollection> _locTransferOutCollections = new XPCollection<TransferOutCollection>
                                                                                        (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                        new BinaryOperator("TransferIn", _locTransferInXPO),
                                                                                        new GroupOperator(GroupOperatorType.Or,
                                                                                        new BinaryOperator("Status", Status.Open),
                                                                                        new BinaryOperator("Status", Status.Progress))));

                                        if (_locTransferOutCollections != null && _locTransferOutCollections.Count() > 0)
                                        {
                                            foreach (TransferOutCollection _locTransferOutCollection in _locTransferOutCollections)
                                            {
                                                if (_locTransferOutCollection.TransferIn != null)
                                                {
                                                    GetTransferOutMonitoring(_currSession, _locTransferOutCollection.TransferOut, _locTransferInXPO);
                                                }
                                            }
                                            SuccessMessageShow("Transfer Out Monitoring Has Been Successfully Getting into Transfer In");
                                        }
                                    }
                                }
                                else
                                {
                                    ErrorMessageShow("Data Transfer In Not Available");
                                }
                            }
                            else
                            {
                                ErrorMessageShow("Data Transfer In Not Available");
                            }
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = TransferIn " + ex.ToString());
            }
        }

        private void TransferInProgressAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;
                Status _locStatus = Status.None;
                DateTime _locNow;
                Status _locStatus2 = Status.None;
                DateTime _locNow2;

                if (_objectsToProcess != null)
                {
                    foreach (Object obj in _objectsToProcess)
                    {
                        TransferIn _locTransferInOS = (TransferIn)_objectSpace.GetObject(obj);

                        if (_locTransferInOS != null)
                        {
                            if (_locTransferInOS.Session != null)
                            {
                                _currSession = _locTransferInOS.Session;
                            }

                            if(_locTransferInOS.Code != null && _currSession != null)
                            {
                                _currObjectId = _locTransferInOS.Code;

                                TransferIn _locTransferInXPO = _currSession.FindObject<TransferIn>
                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                     new BinaryOperator("Code", _currObjectId)));

                                if (_locTransferInXPO != null)
                                {
                                    if (_locTransferInXPO.Status == Status.Open || _locTransferInXPO.Status == Status.Progress)
                                    {
                                        if (_locTransferInXPO.Status == Status.Open)
                                        {
                                            _locStatus = Status.Progress;
                                            _locNow = now;
                                        }
                                        else
                                        {
                                            _locStatus = _locTransferInXPO.Status;
                                            _locNow = _locTransferInXPO.StatusDate;
                                        }
                                        _locTransferInXPO.Status = _locStatus;
                                        _locTransferInXPO.StatusDate = _locNow;
                                        _locTransferInXPO.Save();
                                        _locTransferInXPO.Session.CommitTransaction();

                                        #region TransferInLine and TransferInLot
                                        XPCollection<TransferInLine> _locTransferInLines = new XPCollection<TransferInLine>
                                                               (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("TransferIn", _locTransferInXPO)));

                                        if (_locTransferInLines != null && _locTransferInLines.Count > 0)
                                        {
                                            foreach (TransferInLine _locTransferInLine in _locTransferInLines)
                                            {
                                                if (_locTransferInLine.Status == Status.Open || _locTransferInLine.Status == Status.Progress)
                                                {
                                                    if (_locTransferInLine.Status == Status.Open)
                                                    {
                                                        _locStatus2 = Status.Progress;
                                                        _locNow2 = now;
                                                    }
                                                    else
                                                    {
                                                        _locStatus2 = _locTransferInLine.Status;
                                                        _locNow2 = _locTransferInLine.StatusDate;
                                                    }

                                                    _locTransferInLine.Status = _locStatus2;
                                                    _locTransferInLine.StatusDate = _locNow2;
                                                    _locTransferInLine.Save();
                                                    _locTransferInLine.Session.CommitTransaction();
                                                }

                                                XPCollection<TransferInLot> _locTransferInLots = new XPCollection<TransferInLot>
                                                                                                        (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                        new BinaryOperator("TransferInLine", _locTransferInLine),
                                                                                                        new BinaryOperator("Status", Status.Open)));

                                                if (_locTransferInLots != null && _locTransferInLots.Count() > 0)
                                                {
                                                    foreach (TransferInLot _locTransferInLot in _locTransferInLots)
                                                    {
                                                        _locTransferInLot.Status = Status.Progress;
                                                        _locTransferInLot.StatusDate = now;
                                                        _locTransferInLot.Save();
                                                        _locTransferInLot.Session.CommitTransaction();
                                                    }
                                                }
                                            }
                                        }
                                        #endregion TransferInLine and TransferInLot

                                        #region TransferOutCollection
                                        XPCollection<TransferOutCollection> _locTransferOutCollections = new XPCollection<TransferOutCollection>
                                                               (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("TransferIn", _locTransferInXPO),
                                                                new BinaryOperator("Status", Status.Open)));

                                        if (_locTransferOutCollections != null && _locTransferOutCollections.Count > 0)
                                        {
                                            foreach (TransferOutCollection _locTransferOutCollection in _locTransferOutCollections)
                                            {
                                                _locTransferOutCollection.Status = Status.Progress;
                                                _locTransferOutCollection.StatusDate = now;
                                                _locTransferOutCollection.Save();
                                                _locTransferOutCollection.Session.CommitTransaction();
                                            }
                                        }
                                        #endregion TransferOutCollection

                                        SetBegInvLineTo(_currSession, _locTransferInXPO);

                                        SuccessMessageShow(_locTransferInXPO.Code + " has been change successfully to Progress");
                                    }
                                }
                                else
                                {
                                    ErrorMessageShow("Data Transfer In Not Available");
                                }
                            }
                            else
                            {
                                ErrorMessageShow("Data Transfer In Not Available");
                            }
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = TransferIn " + ex.ToString());
            }
        }

        private void TransferInPostingAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;

                if (_objectsToProcess != null)
                {
                    foreach (Object obj in _objectsToProcess)
                    {
                        TransferIn _locTransferInOS = (TransferIn)_objectSpace.GetObject(obj);

                        if (_locTransferInOS != null)
                        {
                            if (_locTransferInOS.Status == Status.Progress || _locTransferInOS.Status == Status.Posted)
                            {
                                if (_locTransferInOS.Session != null)
                                {
                                    _currSession = _locTransferInOS.Session;
                                }

                                if(_locTransferInOS.Code != null && _currSession != null)
                                {
                                    _currObjectId = _locTransferInOS.Code;

                                    TransferIn _locTransferInXPO = _currSession.FindObject<TransferIn>
                                                                        (new GroupOperator(GroupOperatorType.And,
                                                                         new BinaryOperator("Code", _currObjectId),
                                                                         new GroupOperator(GroupOperatorType.Or,
                                                                         new BinaryOperator("Status", Status.Progress),
                                                                         new BinaryOperator("Status", Status.Posted))));

                                    if (_locTransferInXPO != null)
                                    {
                                        if(CheckLockStock(_currSession, _locTransferInXPO) == false)
                                        {
                                            if (_locTransferInXPO.InventoryMovingType == InventoryMovingType.TransferIn)
                                            {
                                                SetTransferInventoryMonitoring(_currSession, _locTransferInXPO);
                                                SetReceiveBeginingInventory(_currSession, _locTransferInXPO);
                                                SetReceiveInventoryJournal(_currSession, _locTransferInXPO);
                                                if (_locTransferInXPO.DocumentType != null)
                                                {
                                                    if (_locTransferInXPO.DocumentType.DocumentRule == DocumentRule.Customer)
                                                    {
                                                        SetDeliveryStatusInSalesInvoiceMonitoring(_currSession, _locTransferInXPO);
                                                    }
                                                }
                                                SetNullTransferInOnTransferOutMonitoring(_currSession, _locTransferInXPO);
                                                SetStatusReceiveTransferInLine(_currSession, _locTransferInXPO);
                                                SetNormalReceiveQuantity(_currSession, _locTransferInXPO);
                                                SetFinalStatusReceiveTransferIn(_currSession, _locTransferInXPO);
                                                SuccessMessageShow(_locTransferInXPO.Code + " has been change successfully to Receive");
                                            }
                                            
                                        }else
                                        {
                                            ErrorMessageShow("Please Release Lock In Beginning Inventory Line");
                                        }
                                    }
                                    else
                                    {
                                        ErrorMessageShow("Data Transfer In Not Available");
                                    }
                                }
                            }
                            else
                            {
                                ErrorMessageShow("Data Transfer In Not Available");
                            }
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = TransferIn " + ex.ToString());
            }
        }

        private void TransferInListviewFilterSelectionAction_Execute(object sender, SingleChoiceActionExecuteEventArgs e)
        {
            try
            {
                if ((View is ListView) & (View.ObjectTypeInfo.Type == typeof(TransferIn)))
                {
                    if ((Status)e.SelectedChoiceActionItem.Data == Status.None)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.None, BinaryOperatorType.NotEqual);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Open)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Open, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Progress)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Progress, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Posted)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Posted, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Lock)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Lock, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Close)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Close, BinaryOperatorType.Equal);
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = TransferIn " + ex.ToString());
            }
        }

        //=========================================== Code In Here ===========================================

        #region GetTOM

        private void GetTransferOutMonitoring(Session _currSession, TransferOut _locTransferOutXPO, TransferIn _locTransferInXPO)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                DateTime now = DateTime.Now;
                string _currSignCode = null;
                DateTime _locDocDate = now;
                int _locJournalMonth = 0;

                if (_locTransferOutXPO != null && _locTransferInXPO != null)
                {
                    //Hanya memindahkan TransferOutMonitoring ke TransferInLine
                    XPCollection<TransferOutMonitoring> _locTransferOutMonitorings = new XPCollection<TransferOutMonitoring>
                                                                                        (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                        new BinaryOperator("TransferOut", _locTransferOutXPO),
                                                                                        new BinaryOperator("TransferIn", _locTransferInXPO),
                                                                                        new BinaryOperator("Select", true),
                                                                                        new GroupOperator(GroupOperatorType.Or,
                                                                                        new BinaryOperator("Status", Status.Open),
                                                                                        new BinaryOperator("Status", Status.Posted))));

                    if (_locTransferOutMonitorings != null && _locTransferOutMonitorings.Count() > 0)
                    {
                        foreach (TransferOutMonitoring _locTransferOutMonitoring in _locTransferOutMonitorings)
                        {
                            if (_locTransferOutMonitoring.TransferOutLine != null && _locTransferOutMonitoring.BegInvTo != null &&
                                _locTransferOutMonitoring.BegInvLineTo != null)
                            {
                                _currSignCode = _globFunc.GetNumberingSignUnlockOptimisticRecordBasedOrganization(_currSession.DataLayer, ObjectList.TransferInLine, _locTransferInXPO.Company, _locTransferInXPO.Workplace);

                                if (_currSignCode != null)
                                {
                                    #region SaveTransferInLine
                                    TransferInLine _saveDataTransferInLine = new TransferInLine(_currSession)
                                    {
                                        SignCode = _currSignCode,
                                        Company = _locTransferInXPO.Company,
                                        Division = _locTransferInXPO.Division,
                                        Workplace = _locTransferInXPO.Workplace,
                                        Department = _locTransferInXPO.Department,
                                        Section = _locTransferInXPO.Section,
                                        Employee = _locTransferInXPO.Employee,
                                        LocationTypeFrom = _locTransferOutMonitoring.LocationTypeTo,
                                        StockTypeFrom = _locTransferOutMonitoring.StockTypeTo,
                                        WorkplaceFrom = _locTransferOutMonitoring.WorkplaceTo,
                                        LocationFrom = _locTransferOutMonitoring.LocationTo,
                                        StockGroupFrom = _locTransferOutMonitoring.StockGroupTo,
                                        BegInvFrom = _locTransferOutMonitoring.BegInvTo,
                                        BegInvLineFrom = _locTransferOutMonitoring.BegInvLineTo,
                                        LocationTypeTo = _locTransferInXPO.LocationTypeTo,
                                        StockTypeTo = _locTransferInXPO.StockTypeTo,
                                        WorkplaceTo = _locTransferInXPO.WorkplaceTo,
                                        LocationTo = _locTransferInXPO.LocationTo,
                                        StockGroupTo = _locTransferOutMonitoring.StockGroupTo,
                                        BegInvTo = _locTransferInXPO.BegInvTo,
                                        Item = _locTransferOutMonitoring.Item,
                                        Brand = _locTransferOutMonitoring.Brand,
                                        Description = _locTransferOutMonitoring.Description,
                                        DQty = _locTransferOutMonitoring.DQty,
                                        DUOM = _locTransferOutMonitoring.DUOM,
                                        Qty = _locTransferOutMonitoring.Qty,
                                        UOM = _locTransferOutMonitoring.UOM,
                                        TQty = _locTransferOutMonitoring.TQty,
                                        TransferOutMonitoring = _locTransferOutMonitoring,
                                        SalesInvoiceMonitoring = _locTransferOutMonitoring.SalesInvoiceMonitoring,
                                        SalesInvoice = _locTransferOutMonitoring.SalesInvoice,
                                        ETD = _locTransferOutMonitoring.ETD,
                                        ETA = _locTransferOutMonitoring.ETA,
                                        DocDate = _locTransferOutMonitoring.DocDate,
                                        JournalMonth = _locTransferOutMonitoring.JournalMonth,
                                        JournalYear = _locTransferOutMonitoring.JournalYear,
                                        TransferIn = _locTransferInXPO,
                                    };
                                    _saveDataTransferInLine.Save();
                                    _saveDataTransferInLine.Session.CommitTransaction();

                                    #endregion SaveTransferInLine

                                    _locDocDate = _locTransferOutMonitoring.DocDate;
                                    _locJournalMonth = _locTransferOutMonitoring.JournalMonth;

                                    TransferInLine _locTransferInLine = _currSession.FindObject<TransferInLine>
                                                                (new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("SignCode", _currSignCode)));
                                    if (_locTransferInLine != null)
                                    {
                                        SetRemainQty(_currSession, _locTransferOutMonitoring);
                                        SetPostingQty(_currSession, _locTransferOutMonitoring);
                                        SetProcessCount(_currSession, _locTransferOutMonitoring);
                                        SetStatusTransferOutMonitoring(_currSession, _locTransferOutMonitoring);
                                        SetNormalQuantity(_currSession, _locTransferOutMonitoring);
                                    }
                                }
                            }
                        }

                        _locTransferInXPO.DocDate = _locDocDate;
                        _locTransferInXPO.JournalMonth = _locJournalMonth;
                        _locTransferInXPO.Save();
                        _locTransferInXPO.Session.CommitTransaction();
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = TransferIn ", ex.ToString());
            }
        }

        private void SetRemainQty(Session _currSession, TransferOutMonitoring _locTransferOutMonitoringXPO)
        {
            try
            {
                if (_locTransferOutMonitoringXPO != null)
                {
                    double _locRmDQty = 0;
                    double _locRmQty = 0;
                    double _locInvLineTotal = 0;
                    ItemUnitOfMeasure _locItemUOM = null;

                    #region ProcessCount=0
                    if (_locTransferOutMonitoringXPO.PostedCount == 0)
                    {
                        #region MaxQuantity
                        if (_locTransferOutMonitoringXPO.MxDQty > 0)
                        {
                            if (_locTransferOutMonitoringXPO.DQty > 0 && _locTransferOutMonitoringXPO.DQty <= _locTransferOutMonitoringXPO.MxDQty)
                            {
                                _locRmDQty = _locTransferOutMonitoringXPO.MxDQty - _locTransferOutMonitoringXPO.DQty;
                            }

                            if (_locTransferOutMonitoringXPO.Qty > 0 && _locTransferOutMonitoringXPO.Qty <= _locTransferOutMonitoringXPO.MxQty)
                            {
                                _locRmQty = _locTransferOutMonitoringXPO.MxQty - _locTransferOutMonitoringXPO.Qty;
                            }

                            _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                       (new GroupOperator(GroupOperatorType.And,
                                                        new BinaryOperator("Item", _locTransferOutMonitoringXPO.Item),
                                                        new BinaryOperator("UOM", _locTransferOutMonitoringXPO.MxUOM),
                                                        new BinaryOperator("DefaultUOM", _locTransferOutMonitoringXPO.MxDUOM),
                                                        new BinaryOperator("Active", true)));
                            if (_locItemUOM != null)
                            {
                                if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                {
                                    _locInvLineTotal = _locRmQty * _locItemUOM.DefaultConversion + _locRmDQty;
                                }
                                else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                {
                                    _locInvLineTotal = _locRmQty / _locItemUOM.Conversion + _locRmDQty;
                                }
                                else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                {
                                    _locInvLineTotal = _locRmQty + _locRmDQty;
                                }
                            }
                            else
                            {
                                _locInvLineTotal = _locRmQty + _locRmDQty;
                            }
                        }
                        #endregion MaxQuantity

                        #region NonMaxQuantity
                        else
                        {
                            if (_locTransferOutMonitoringXPO.DQty > 0)
                            {
                                _locRmDQty = 0;
                            }

                            if (_locTransferOutMonitoringXPO.Qty > 0)
                            {
                                _locRmQty = 0;
                            }

                            _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                       (new GroupOperator(GroupOperatorType.And,
                                                        new BinaryOperator("Item", _locTransferOutMonitoringXPO.Item),
                                                        new BinaryOperator("UOM", _locTransferOutMonitoringXPO.UOM),
                                                        new BinaryOperator("DefaultUOM", _locTransferOutMonitoringXPO.DUOM),
                                                        new BinaryOperator("Active", true)));
                            if (_locItemUOM != null)
                            {
                                if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                {
                                    _locInvLineTotal = _locRmQty * _locItemUOM.DefaultConversion + _locRmDQty;
                                }
                                else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                {
                                    _locInvLineTotal = _locRmQty / _locItemUOM.Conversion + _locRmDQty;
                                }
                                else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                {
                                    _locInvLineTotal = _locRmQty + _locRmDQty;
                                }
                            }
                            else
                            {
                                _locInvLineTotal = _locRmQty + _locRmDQty;
                            }
                        }
                        #endregion NonMaxQuantity
                    }
                    #endregion ProcessCount=0

                    #region ProcessCount>0
                    if (_locTransferOutMonitoringXPO.PostedCount > 0)
                    {
                        if (_locTransferOutMonitoringXPO.RmDQty > 0)
                        {
                            _locRmDQty = _locTransferOutMonitoringXPO.RmDQty - _locTransferOutMonitoringXPO.DQty;
                        }

                        if (_locTransferOutMonitoringXPO.RmQty > 0)
                        {
                            _locRmQty = _locTransferOutMonitoringXPO.RmQty - _locTransferOutMonitoringXPO.Qty;
                        }

                        if (_locTransferOutMonitoringXPO.MxDQty > 0 || _locTransferOutMonitoringXPO.MxQty > 0)
                        {
                            _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                       (new GroupOperator(GroupOperatorType.And,
                                                        new BinaryOperator("Item", _locTransferOutMonitoringXPO.Item),
                                                        new BinaryOperator("UOM", _locTransferOutMonitoringXPO.MxUOM),
                                                        new BinaryOperator("DefaultUOM", _locTransferOutMonitoringXPO.MxDUOM),
                                                        new BinaryOperator("Active", true)));
                        }
                        else
                        {
                            _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                       (new GroupOperator(GroupOperatorType.And,
                                                        new BinaryOperator("Item", _locTransferOutMonitoringXPO.Item),
                                                        new BinaryOperator("UOM", _locTransferOutMonitoringXPO.UOM),
                                                        new BinaryOperator("DefaultUOM", _locTransferOutMonitoringXPO.DUOM),
                                                        new BinaryOperator("Active", true)));
                        }


                        if (_locItemUOM != null)
                        {
                            if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                            {
                                _locInvLineTotal = _locRmQty * _locItemUOM.DefaultConversion + _locRmDQty;
                            }
                            else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                            {
                                _locInvLineTotal = _locRmQty / _locItemUOM.Conversion + _locRmDQty;
                            }
                            else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                            {
                                _locInvLineTotal = _locRmQty + _locRmDQty;
                            }
                        }
                        else
                        {
                            _locInvLineTotal = _locRmQty + _locRmDQty;
                        }

                    }
                    #endregion ProcessCount>0

                    _locTransferOutMonitoringXPO.RmDQty = _locRmDQty;
                    _locTransferOutMonitoringXPO.RmQty = _locRmQty;
                    _locTransferOutMonitoringXPO.RmTQty = _locInvLineTotal;
                    _locTransferOutMonitoringXPO.Save();
                    _locTransferOutMonitoringXPO.Session.CommitTransaction();

                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = TransferIn " + ex.ToString());
            }
        }

        private void SetPostingQty(Session _currSession, TransferOutMonitoring _locTransferOutMonitoringXPO)
        {
            try
            {
                if (_locTransferOutMonitoringXPO != null)
                {
                    double _locPDQty = 0;
                    double _locPQty = 0;
                    double _locInvLineTotal = 0;
                    ItemUnitOfMeasure _locItemUOM = null;

                    #region ProcessCount=0
                    if (_locTransferOutMonitoringXPO.PostedCount == 0)
                    {
                        #region MaxQuantity
                        if (_locTransferOutMonitoringXPO.MxDQty > 0)
                        {
                            if (_locTransferOutMonitoringXPO.DQty > 0 && _locTransferOutMonitoringXPO.DQty <= _locTransferOutMonitoringXPO.MxDQty)
                            {
                                _locPDQty = _locTransferOutMonitoringXPO.DQty;
                            }

                            if (_locTransferOutMonitoringXPO.Qty > 0 && _locTransferOutMonitoringXPO.Qty <= _locTransferOutMonitoringXPO.MxQty)
                            {
                                _locPQty = _locTransferOutMonitoringXPO.Qty;
                            }

                            _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                       (new GroupOperator(GroupOperatorType.And,
                                                        new BinaryOperator("Item", _locTransferOutMonitoringXPO.Item),
                                                        new BinaryOperator("UOM", _locTransferOutMonitoringXPO.MxUOM),
                                                        new BinaryOperator("DefaultUOM", _locTransferOutMonitoringXPO.MxDUOM),
                                                        new BinaryOperator("Active", true)));
                            if (_locItemUOM != null)
                            {
                                if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                {
                                    _locInvLineTotal = _locPQty * _locItemUOM.DefaultConversion + _locPDQty;
                                }
                                else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                {
                                    _locInvLineTotal = _locPQty / _locItemUOM.Conversion + _locPDQty;
                                }
                                else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                {
                                    _locInvLineTotal = _locPQty + _locPDQty;
                                }
                            }
                            else
                            {
                                _locInvLineTotal = _locPQty + _locPDQty;
                            }
                        }
                        #endregion MaxQuantity

                        #region NonMaxQuantity
                        else
                        {
                            if (_locTransferOutMonitoringXPO.DQty > 0)
                            {
                                _locPDQty = _locTransferOutMonitoringXPO.DQty;
                            }

                            if (_locTransferOutMonitoringXPO.Qty > 0)
                            {
                                _locPQty = _locTransferOutMonitoringXPO.Qty;
                            }

                            _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                       (new GroupOperator(GroupOperatorType.And,
                                                        new BinaryOperator("Item", _locTransferOutMonitoringXPO.Item),
                                                        new BinaryOperator("UOM", _locTransferOutMonitoringXPO.UOM),
                                                        new BinaryOperator("DefaultUOM", _locTransferOutMonitoringXPO.DUOM),
                                                        new BinaryOperator("Active", true)));
                            if (_locItemUOM != null)
                            {
                                if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                {
                                    _locInvLineTotal = _locPQty * _locItemUOM.DefaultConversion + _locPDQty;
                                }
                                else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                {
                                    _locInvLineTotal = _locPQty / _locItemUOM.Conversion + _locPDQty;
                                }
                                else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                {
                                    _locInvLineTotal = _locPQty + _locPDQty;
                                }
                            }
                            else
                            {
                                _locInvLineTotal = _locPQty + _locPDQty;
                            }
                        }
                        #endregion NonMaxQuantity
                    }
                    #endregion ProcessCount=0

                    #region ProcessCount>0
                    if (_locTransferOutMonitoringXPO.PostedCount > 0)
                    {
                        if (_locTransferOutMonitoringXPO.PDQty > 0)
                        {
                            _locPDQty = _locTransferOutMonitoringXPO.PDQty + _locTransferOutMonitoringXPO.DQty;
                        }

                        if (_locTransferOutMonitoringXPO.PQty > 0)
                        {
                            _locPQty = _locTransferOutMonitoringXPO.PQty + _locTransferOutMonitoringXPO.Qty;
                        }

                        if (_locTransferOutMonitoringXPO.MxDQty > 0 || _locTransferOutMonitoringXPO.MxQty > 0)
                        {
                            _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                   (new GroupOperator(GroupOperatorType.And,
                                                    new BinaryOperator("Item", _locTransferOutMonitoringXPO.Item),
                                                    new BinaryOperator("UOM", _locTransferOutMonitoringXPO.MxUOM),
                                                    new BinaryOperator("DefaultUOM", _locTransferOutMonitoringXPO.MxDUOM),
                                                    new BinaryOperator("Active", true)));
                        }
                        else
                        {
                            _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                   (new GroupOperator(GroupOperatorType.And,
                                                    new BinaryOperator("Item", _locTransferOutMonitoringXPO.Item),
                                                    new BinaryOperator("UOM", _locTransferOutMonitoringXPO.UOM),
                                                    new BinaryOperator("DefaultUOM", _locTransferOutMonitoringXPO.DUOM),
                                                    new BinaryOperator("Active", true)));
                        }

                        if (_locItemUOM != null)
                        {
                            if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                            {
                                _locInvLineTotal = _locPQty * _locItemUOM.DefaultConversion + _locPDQty;
                            }
                            else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                            {
                                _locInvLineTotal = _locPQty / _locItemUOM.Conversion + _locPDQty;
                            }
                            else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                            {
                                _locInvLineTotal = _locPQty + _locPDQty;
                            }
                        }
                        else
                        {
                            _locInvLineTotal = _locPQty + _locPDQty;
                        }
                    }
                    #endregion ProcessCount>0

                    _locTransferOutMonitoringXPO.PDQty = _locPDQty;
                    _locTransferOutMonitoringXPO.PDUOM = _locTransferOutMonitoringXPO.DUOM;
                    _locTransferOutMonitoringXPO.PQty = _locPQty;
                    _locTransferOutMonitoringXPO.PUOM = _locTransferOutMonitoringXPO.UOM;
                    _locTransferOutMonitoringXPO.PTQty = _locInvLineTotal;
                    _locTransferOutMonitoringXPO.Save();
                    _locTransferOutMonitoringXPO.Session.CommitTransaction();

                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = TransferIn " + ex.ToString());
            }
        }

        private void SetProcessCount(Session _currSession, TransferOutMonitoring _locTransferOutMonitoringXPO)
        {
            try
            {
                if (_locTransferOutMonitoringXPO != null)
                {
                    _locTransferOutMonitoringXPO.PostedCount = _locTransferOutMonitoringXPO.PostedCount + 1;
                    _locTransferOutMonitoringXPO.Save();
                    _locTransferOutMonitoringXPO.Session.CommitTransaction();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = TransferIn " + ex.ToString());
            }
        }

        private void SetStatusTransferOutMonitoring(Session _currSession, TransferOutMonitoring _locTransferOutMonitoringXPO)
        {
            try
            {
                DateTime now = DateTime.Now;
                if (_locTransferOutMonitoringXPO != null)
                {
                    if (_locTransferOutMonitoringXPO.RmDQty == 0 && _locTransferOutMonitoringXPO.RmQty == 0 && _locTransferOutMonitoringXPO.RmTQty == 0)
                    {
                        _locTransferOutMonitoringXPO.Status = Status.Close;
                        _locTransferOutMonitoringXPO.ActivationPosting = true;
                        _locTransferOutMonitoringXPO.StatusDate = now;
                    }
                    else
                    {
                        _locTransferOutMonitoringXPO.Status = Status.Posted;
                        _locTransferOutMonitoringXPO.StatusDate = now;
                    }
                    _locTransferOutMonitoringXPO.Save();
                    _locTransferOutMonitoringXPO.Session.CommitTransaction();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = TransferIn " + ex.ToString());
            }
        }

        private void SetNormalQuantity(Session _currSession, TransferOutMonitoring _locTransferOutMonitoringXPO)
        {
            try
            {
                if (_locTransferOutMonitoringXPO != null)
                {
                    if (_locTransferOutMonitoringXPO.DQty > 0 || _locTransferOutMonitoringXPO.Qty > 0)
                    {
                        _locTransferOutMonitoringXPO.Select = false;
                        _locTransferOutMonitoringXPO.DQty = 0;
                        _locTransferOutMonitoringXPO.Qty = 0;
                        _locTransferOutMonitoringXPO.Save();
                        _locTransferOutMonitoringXPO.Session.CommitTransaction();
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = TransferIn " + ex.ToString());
            }
        }

        #endregion GetTOM

        #region Progress

        private void SetBegInvLineTo(Session _currSession, TransferIn _locTransferInXPO)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                string _locSignCode = null;

                if (_locTransferInXPO != null)
                {
                    XPCollection<TransferInLine> _locTransferInLines = new XPCollection<TransferInLine>(_currSession,
                                                                        new GroupOperator(GroupOperatorType.And,
                                                                        new BinaryOperator("TransferIn", _locTransferInXPO),
                                                                        new GroupOperator(GroupOperatorType.Or,
                                                                        new BinaryOperator("Status", Status.Open),
                                                                        new BinaryOperator("Status", Status.Progress))));
                    if (_locTransferInLines != null && _locTransferInLines.Count() > 0)
                    {
                        foreach (TransferInLine _locTransferInLine in _locTransferInLines)
                        {
                            if (_locTransferInLine.Company != null && _locTransferInLine.WorkplaceTo != null && _locTransferInLine.BegInvTo != null
                                && _locTransferInLine.Item != null && _locTransferInLine.BegInvLineTo == null)
                            {
                                BeginningInventoryLine _locBegInvLine = null;

                                if (_locTransferInLine.DUOM != null)
                                {
                                    #region Update
                                    if (_locTransferInLine.BinLocationTo != null)
                                    {
                                        //DefaultUOM
                                        _locBegInvLine = _currSession.FindObject<BeginningInventoryLine>(
                                                   new GroupOperator(GroupOperatorType.And,
                                                   new BinaryOperator("Company", _locTransferInLine.Company),
                                                   new BinaryOperator("Workplace", _locTransferInLine.WorkplaceTo),
                                                   new BinaryOperator("BeginningInventory", _locTransferInLine.BegInvTo),
                                                   new BinaryOperator("Item", _locTransferInLine.Item),
                                                   new BinaryOperator("DefaultUOM", _locTransferInLine.DUOM),
                                                   new BinaryOperator("BinLocation", _locTransferInLine.BinLocationTo),
                                                   new BinaryOperator("LocationType", _locTransferInLine.LocationTypeTo),
                                                   new BinaryOperator("StockType", _locTransferInLine.StockTypeTo),
                                                   new BinaryOperator("StockGroup", _locTransferInLine.StockGroupTo),
                                                   new BinaryOperator("Active", true)));
                                    }
                                    else
                                    {
                                        _locBegInvLine = _currSession.FindObject<BeginningInventoryLine>(
                                                   new GroupOperator(GroupOperatorType.And,
                                                   new BinaryOperator("Company", _locTransferInLine.Company),
                                                   new BinaryOperator("Workplace", _locTransferInLine.WorkplaceTo),
                                                   new BinaryOperator("BeginningInventory", _locTransferInLine.BegInvTo),
                                                   new BinaryOperator("Item", _locTransferInLine.Item),
                                                   new BinaryOperator("DefaultUOM", _locTransferInLine.DUOM),
                                                   new BinaryOperator("LocationType", _locTransferInLine.LocationTypeTo),
                                                   new BinaryOperator("StockType", _locTransferInLine.StockTypeTo),
                                                   new BinaryOperator("StockGroup", _locTransferInLine.StockGroupTo),
                                                   new BinaryOperator("Active", true)));
                                    }



                                    if (_locBegInvLine != null)
                                    {
                                        _locTransferInLine.BegInvLineTo = _locBegInvLine;
                                        _locTransferInLine.Save();
                                        _locTransferInLine.Session.CommitTransaction();
                                    }
                                    #endregion Update
                                    #region CreateNew
                                    else
                                    {
                                        _locSignCode = _globFunc.GetNumberingSignUnlockOptimisticRecordBasedOrganization(_currSession.DataLayer, ObjectList.BeginningInventoryLine, _locTransferInLine.Company, _locTransferInLine.WorkplaceTo);

                                        if (_locSignCode != null)
                                        {
                                            BeginningInventoryLine _locSaveDataBeginingInventoryLine = new BeginningInventoryLine(_currSession)
                                            {
                                                SignCode = _locSignCode,
                                                Company = _locTransferInXPO.Company,
                                                Workplace = _locTransferInXPO.WorkplaceTo,
                                                Item = _locTransferInLine.Item,
                                                Location = _locTransferInLine.LocationTo,
                                                Vehicle = _locTransferInXPO.Vehicle,
                                                BinLocation = _locTransferInLine.BinLocationTo,
                                                DefaultUOM = _locTransferInLine.DUOM,
                                                LocationType = _locTransferInLine.LocationTypeTo,
                                                StockType = _locTransferInLine.StockTypeTo,
                                                StockGroup = _locTransferInLine.StockGroupTo,
                                                Lock = false,
                                                Active = true,
                                                BeginningInventory = _locTransferInLine.BegInvTo,
                                            };

                                            _locSaveDataBeginingInventoryLine.Save();
                                            _locSaveDataBeginingInventoryLine.Session.CommitTransaction();

                                            BeginningInventoryLine _locBegInvLine2 = _currSession.FindObject<BeginningInventoryLine>(new GroupOperator
                                                                                    (GroupOperatorType.And,
                                                                                    new BinaryOperator("SignCode", _locSignCode)));
                                            if (_locBegInvLine2 != null)
                                            {
                                                _locTransferInLine.BegInvLineTo = _locBegInvLine2;
                                                _locTransferInLine.Save();
                                                _locTransferInLine.Session.CommitTransaction();
                                            }
                                        }
                                    }
                                    #endregion CreateNew
                                }

                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Module = TransferIn " + ex.ToString());
            }
        }

        #endregion Progress

        #region Posting

        #region IMT=TransferIn

        private bool CheckLockStock(Session _currSession, TransferIn _locTransferInXPO)
        {
            bool _result = false;
            try
            {
                bool _locLock = false;

                if(_locTransferInXPO != null)
                {
                    XPCollection<TransferInLine> _locTransferInLines = new XPCollection<TransferInLine>(_currSession,
                                                                        new GroupOperator(GroupOperatorType.And,
                                                                        new BinaryOperator("TransferIn", _locTransferInXPO),
                                                                        new GroupOperator(GroupOperatorType.Or,
                                                                        new BinaryOperator("Status", Status.Progress),
                                                                        new BinaryOperator("Status", Status.Posted))));
                    if (_locTransferInLines != null && _locTransferInLines.Count() > 0)
                    {
                        foreach(TransferInLine _locTransferInLine in _locTransferInLines)
                        {
                            if(_locTransferInLine.BegInvLineTo != null)
                            {
                                if(_locTransferInLine.BegInvLineTo.Lock == true)
                                {
                                    _locLock = true;
                                }
                            }
                        }

                        _result = _locLock;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Module = TransferIn " + ex.ToString());
            }
            return _result;
        }

        private void SetTransferInventoryMonitoring(Session _currSession, TransferIn _locTransferInXPO)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                DateTime now = DateTime.Now;
                PickingLine _locPickingLine = null;
                Employee _locPIC = null;

                if (_locTransferInXPO != null)
                {
                    XPCollection<TransferInLine> _locTransferInLines = new XPCollection<TransferInLine>
                                                            (_currSession, new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("TransferIn", _locTransferInXPO),
                                                            new BinaryOperator("Select", true),
                                                            new GroupOperator(GroupOperatorType.Or,
                                                            new BinaryOperator("Status", Status.Progress),
                                                            new BinaryOperator("Status", Status.Posted))));

                    if (_locTransferInLines != null && _locTransferInLines.Count() > 0)
                    {
                        foreach (TransferInLine _locTransferInLine in _locTransferInLines)
                        {
                            if(_locTransferInLine.SalesInvoiceMonitoring != null)
                            {
                                if(_locTransferInLine.SalesInvoiceMonitoring.PickingLine != null)
                                {
                                    _locPickingLine = _locTransferInLine.SalesInvoiceMonitoring.PickingLine;
                                }
                            }

                            if(_locTransferInLine.SalesInvoice != null)
                            {
                                _locPIC = _locTransferInLine.SalesInvoice.PIC;
                            }

                            TransferInventoryMonitoring _saveDataTransferInventoryMonitoring = new TransferInventoryMonitoring(_currSession)
                            {
                                TransferIn = _locTransferInXPO,
                                TransferInLine = _locTransferInLine,
                                #region Organization
                                Company = _locTransferInLine.Company,
                                Workplace = _locTransferInLine.Workplace,
                                Division = _locTransferInLine.Division,
                                Department = _locTransferInLine.Department,
                                Section = _locTransferInLine.Section,
                                Employee = _locTransferInLine.Employee,
                                #endregion Organization
                                #region From
                                LocationTypeFrom = _locTransferInLine.LocationTypeFrom,
                                StockTypeFrom = _locTransferInLine.StockTypeFrom,
                                WorkplaceFrom = _locTransferInLine.WorkplaceFrom,
                                LocationFrom = _locTransferInLine.LocationFrom,
                                BinLocationFrom = _locTransferInLine.BinLocationFrom,
                                StockGroupFrom = _locTransferInLine.StockGroupFrom,
                                BegInvFrom = _locTransferInLine.BegInvFrom,
                                BegInvLineFrom = _locTransferInLine.BegInvLineFrom,
                                #endregion From
                                #region To
                                LocationTypeTo = _locTransferInLine.LocationTypeTo,
                                StockTypeTo = _locTransferInLine.StockTypeTo,
                                WorkplaceTo = _locTransferInLine.WorkplaceTo,
                                LocationTo = _locTransferInLine.LocationTo,
                                BinLocationTo = _locTransferInLine.BinLocationTo,
                                StockGroupTo = _locTransferInLine.StockGroupTo,
                                BegInvTo = _locTransferInLine.BegInvTo,
                                BegInvLineTo = _locTransferInLine.BegInvLineTo,
                                #endregion To
                                PickingDate = _locTransferInXPO.PickingDate,
                                Picking = _locTransferInXPO.Picking,
                                PickingLine = _locPickingLine,
                                PIC = _locPIC,
                                Vehicle = _locTransferInXPO.Vehicle,
                                #region Item
                                Item = _locTransferInLine.Item,
                                Brand = _locTransferInLine.Brand,
                                Description = _locTransferInLine.Description,
                                MxDQty = _locTransferInLine.DQty,
                                MxDUOM = _locTransferInLine.DUOM,
                                MxQty = _locTransferInLine.Qty,
                                MxUOM = _locTransferInLine.UOM,
                                MxTQty = _locTransferInLine.TQty,
                                DQty = _locTransferInLine.DQty,
                                DUOM = _locTransferInLine.DUOM,
                                Qty = _locTransferInLine.Qty,
                                UOM = _locTransferInLine.UOM,
                                TQty = _locTransferInLine.TQty,
                                #endregion Item
                                ETD = _locTransferInLine.ETD,
                                ETA = _locTransferInLine.ETA,
                                DocDate = _locTransferInLine.DocDate,
                                JournalMonth = _locTransferInLine.JournalMonth,
                                JournalYear = _locTransferInLine.JournalYear,
                                SalesInvoiceMonitoring = _locTransferInLine.SalesInvoiceMonitoring,
                                SalesInvoice = _locTransferInLine.SalesInvoice,
                            };
                            _saveDataTransferInventoryMonitoring.Save();
                            _saveDataTransferInventoryMonitoring.Session.CommitTransaction();
                        }
                    }
                    
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = TransferIn ", ex.ToString());
            }
        }

        private void SetReceiveBeginingInventory(Session _currSession, TransferIn _locTransferInXPO)
        {
            try
            {
                XPCollection<TransferInLine> _locTransferInLines = new XPCollection<TransferInLine>(_currSession,
                                                                        new GroupOperator(GroupOperatorType.And,
                                                                        new BinaryOperator("TransferIn", _locTransferInXPO),
                                                                        new BinaryOperator("Select", true),
                                                                        new GroupOperator(GroupOperatorType.Or,
                                                                        new BinaryOperator("Status", Status.Progress),
                                                                        new BinaryOperator("Status", Status.Posted))));

                if (_locTransferInLines != null && _locTransferInLines.Count() > 0)
                {
                    GlobalFunction _globFunc = new GlobalFunction();
                    
                    foreach (TransferInLine _locTransferInLine in _locTransferInLines)
                    {
                        if (_locTransferInLine.DQty > 0 || _locTransferInLine.Qty > 0)
                        {
                            XPCollection<TransferInLot> _locTransferInLots = new XPCollection<TransferInLot>
                                                                                    (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                    new BinaryOperator("TransferInLine", _locTransferInLine),
                                                                                    new BinaryOperator("Select", true),
                                                                                    new GroupOperator(GroupOperatorType.Or,
                                                                                    new BinaryOperator("Status", Status.Progress),
                                                                                    new BinaryOperator("Status", Status.Posted))));
                            #region LotNumber
                            if (_locTransferInLots != null && _locTransferInLots.Count() > 0)
                            {
                                foreach (TransferInLot _locTransferInLot in _locTransferInLots)
                                {
                                    if (_locTransferInLot.Select == true)
                                    {
                                        if (_locTransferInLot.BegInvLine != null)
                                        {
                                            if (_locTransferInLot.BegInvLine.Code != null)
                                            {
                                                BeginningInventoryLine _locBegInvLine = _currSession.FindObject<BeginningInventoryLine>
                                                                                (new GroupOperator(GroupOperatorType.And,
                                                                                 new BinaryOperator("Code", _locTransferInLot.BegInvLine.Code)));
                                                if (_locBegInvLine != null)
                                                {
                                                    if (_locBegInvLine.QtyAvailable >= _locTransferInLot.TQty)
                                                    {
                                                        _locBegInvLine.QtyAvailable = _locBegInvLine.QtyAvailable + _locTransferInLot.TQty;
                                                        _locBegInvLine.Save();
                                                        _locBegInvLine.Session.CommitTransaction();
                                                    }

                                                    if (_locBegInvLine.BeginningInventory != null)
                                                    {
                                                        if (_locBegInvLine.BeginningInventory.Code != null)
                                                        {
                                                            BeginningInventory _locBegInv = _currSession.FindObject<BeginningInventory>
                                                                                (new GroupOperator(GroupOperatorType.And,
                                                                                 new BinaryOperator("Code", _locBegInvLine.BeginningInventory.Code)));
                                                            if (_locBegInv != null)
                                                            {
                                                                _locBegInv.QtyAvailable = _locBegInv.QtyAvailable + _locTransferInLot.TQty;
                                                                _locBegInv.Save();
                                                                _locBegInv.Session.CommitTransaction();
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            #endregion LotNumber
                            #region NonLotNumber
                            else
                            {
                                #region BeginningInventoryFrom
                                if (_locTransferInLine.BegInvFrom != null && _locTransferInLine.BegInvLineFrom != null)
                                {
                                    if(_locTransferInLine.LocationTypeFrom == LocationType.Transit )
                                    {
                                        _locTransferInLine.BegInvLineFrom.QtyAvailable = _locTransferInLine.BegInvLineFrom.QtyAvailable - _locTransferInLine.TQty;
                                        _locTransferInLine.BegInvLineFrom.Save();
                                        _locTransferInLine.BegInvLineFrom.Session.CommitTransaction();

                                        _locTransferInLine.BegInvFrom.QtyAvailable = _locTransferInLine.BegInvFrom.QtyAvailable - _locTransferInLine.TQty;
                                        _locTransferInLine.BegInvFrom.Save();
                                        _locTransferInLine.BegInvFrom.Session.CommitTransaction();
                                    }
                                }
                                #endregion BeginningInventoryFrom
                                #region BeginningInventoryTo
                                if (_locTransferInLine.Company != null && _locTransferInLine.WorkplaceTo != null && _locTransferInLine.BegInvTo != null && _locTransferInLine.Item != null)
                                {
                                    #region Update
                                    if (_locTransferInLine.BegInvLineTo != null)
                                    {
                                        _locTransferInLine.BegInvLineTo.QtyAvailable = _locTransferInLine.BegInvLineTo.QtyAvailable + _locTransferInLine.TQty;
                                        _locTransferInLine.BegInvLineTo.Save();
                                        _locTransferInLine.BegInvLineTo.Session.CommitTransaction();

                                        _locTransferInLine.BegInvTo.QtyAvailable = _locTransferInLine.BegInvTo.QtyAvailable + _locTransferInLine.TQty;
                                        _locTransferInLine.BegInvTo.Save();
                                        _locTransferInLine.BegInvTo.Session.CommitTransaction();

                                        if (_locTransferInLine.TransferOutMonitoring != null)
                                        {
                                            double _locQtyOnHand = 0;
                                            if (_locTransferInLine.TransferOutMonitoring.DocumentRule == DocumentRule.Salesman)
                                            {
                                                _locQtyOnHand = _locTransferInLine.BegInvLineTo.QtyOnHand - _locTransferInLine.TQty;
                                                if(_locQtyOnHand < 0)
                                                {
                                                    _locQtyOnHand = 0;
                                                }
                                                _locTransferInLine.BegInvLineTo.QtyOnHand = _locQtyOnHand;
                                                _locTransferInLine.BegInvLineTo.Save();
                                                _locTransferInLine.BegInvLineTo.Session.CommitTransaction();
                                            }
                                        }
                                    }
                                    
                                    #endregion Update
                                }
                                #endregion BeginningInventoryTo
                            }
                            #endregion NonLotNumber
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = TransferIn ", ex.ToString());
            }
        }

        private void SetReceiveInventoryJournal(Session _currSession, TransferIn _locTransferInXPO)
        {
            try
            {
                XPCollection<TransferInLine> _locTransferInLines = new XPCollection<TransferInLine>(_currSession,
                                                                        new GroupOperator(GroupOperatorType.And,
                                                                        new BinaryOperator("TransferIn", _locTransferInXPO),
                                                                        new BinaryOperator("Select", true),
                                                                        new GroupOperator(GroupOperatorType.Or,
                                                                        new BinaryOperator("Status", Status.Progress),
                                                                        new BinaryOperator("Status", Status.Posted))));

                if (_locTransferInLines != null && _locTransferInLines.Count > 0)
                {
                    double _locInvLineTotal = 0;
                    DateTime now = DateTime.Now;

                    foreach (TransferInLine _locTransferInLine in _locTransferInLines)
                    {
                        if (_locTransferInLine.DQty > 0 || _locTransferInLine.Qty > 0)
                        {
                            XPCollection<TransferInLot> _locTransferInLots = new XPCollection<TransferInLot>
                                                                                    (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                    new BinaryOperator("TransferInLine", _locTransferInLine),
                                                                                    new BinaryOperator("Select", true),
                                                                                    new GroupOperator(GroupOperatorType.Or,
                                                                                    new BinaryOperator("Status", Status.Progress),
                                                                                    new BinaryOperator("Status", Status.Posted))));

                            #region LotNumber
                            if (_locTransferInLots != null && _locTransferInLots.Count() > 0)
                            {
                                foreach (TransferInLot _locTransferInLot in _locTransferInLots)
                                {
                                    if (_locTransferInLot.Select == true)
                                    {
                                        if (_locTransferInLot.BegInvLine != null)
                                        {
                                            if (_locTransferInLot.BegInvLine.Code != null)
                                            {
                                                BeginningInventoryLine _locBegInvLine = _currSession.FindObject<BeginningInventoryLine>
                                                                                (new GroupOperator(GroupOperatorType.And,
                                                                                 new BinaryOperator("Code", _locTransferInLot.BegInvLine.Code)));
                                                if (_locBegInvLine != null)
                                                {
                                                    InventoryJournal _locPositiveInventoryJournal = new InventoryJournal(_currSession)
                                                    {
                                                        DocumentType = _locTransferInXPO.DocumentType,
                                                        DocNo = _locTransferInXPO.DocNo,
                                                        Location = _locTransferInLot.LocationFrom,
                                                        BinLocation = _locTransferInLot.BinLocationFrom,
                                                        StockType = _locTransferInLot.StockTypeFrom,
                                                        Item = _locTransferInLot.Item,
                                                        Company = _locTransferInLot.Company,
                                                        QtyPos = _locTransferInLot.TQty,
                                                        QtyNeg = 0,
                                                        DUOM = _locTransferInLot.DUOM,
                                                        JournalDate = now,
                                                        LotNumber = _locTransferInLot.LotNumber,
                                                        TransferIn = _locTransferInXPO,
                                                    };
                                                    _locPositiveInventoryJournal.Save();
                                                    _locPositiveInventoryJournal.Session.CommitTransaction();
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            #endregion LotNumber

                            #region NonLotNumber
                            else
                            {
                                #region Negatif
                                if (_locTransferInLine.LocationTypeFrom == LocationType.Transit)
                                {
                                    if (_locTransferInLine.Item != null && _locTransferInLine.UOM != null && _locTransferInLine.DUOM != null)
                                    {
                                        ItemUnitOfMeasure _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                            (new GroupOperator(GroupOperatorType.And,
                                                             new BinaryOperator("Item", _locTransferInLine.Item),
                                                             new BinaryOperator("UOM", _locTransferInLine.UOM),
                                                             new BinaryOperator("DefaultUOM", _locTransferInLine.DUOM),
                                                             new BinaryOperator("Active", true)));
                                        if (_locItemUOM != null)
                                        {
                                            #region Code
                                            if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                            {
                                                _locInvLineTotal = _locTransferInLine.Qty * _locItemUOM.DefaultConversion + _locTransferInLine.DQty;
                                            }
                                            else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                            {
                                                _locInvLineTotal = _locTransferInLine.Qty / _locItemUOM.Conversion + _locTransferInLine.DQty;
                                            }
                                            else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                            {
                                                _locInvLineTotal = _locTransferInLine.Qty + _locTransferInLine.DQty;
                                            }
                                            #endregion Code

                                            InventoryJournal _locNegatifInventoryJournal = new InventoryJournal(_currSession)
                                            {
                                                DocumentType = _locTransferInXPO.DocumentType,
                                                DocNo = _locTransferInXPO.DocNo,
                                                Location = _locTransferInLine.LocationFrom,
                                                BinLocation = _locTransferInLine.BinLocationFrom,
                                                LocationType = _locTransferInLine.LocationTypeFrom,
                                                StockType = _locTransferInLine.StockTypeFrom,
                                                StockGroup = _locTransferInLine.StockGroupFrom,
                                                Item = _locTransferInLine.Item,
                                                Company = _locTransferInLine.Company,
                                                Workplace = _locTransferInLine.WorkplaceFrom,
                                                QtyPos = 0,
                                                QtyNeg = _locInvLineTotal,
                                                DUOM = _locTransferInLine.DUOM,
                                                JournalDate = now,
                                                TransferIn = _locTransferInXPO,
                                            };
                                            _locNegatifInventoryJournal.Save();
                                            _locNegatifInventoryJournal.Session.CommitTransaction();

                                        }
                                    }
                                    else
                                    {
                                        _locInvLineTotal = _locTransferInLine.Qty + _locTransferInLine.DQty;

                                        InventoryJournal _locNegatifInventoryJournal = new InventoryJournal(_currSession)
                                        {
                                            DocumentType = _locTransferInXPO.DocumentType,
                                            DocNo = _locTransferInXPO.DocNo,
                                            Location = _locTransferInLine.LocationFrom,
                                            BinLocation = _locTransferInLine.BinLocationFrom,
                                            LocationType = _locTransferInLine.LocationTypeFrom,
                                            StockType = _locTransferInLine.StockTypeFrom,
                                            StockGroup = _locTransferInLine.StockGroupFrom,
                                            Item = _locTransferInLine.Item,
                                            Company = _locTransferInLine.Company,
                                            Workplace = _locTransferInLine.WorkplaceFrom,
                                            QtyPos = 0,
                                            QtyNeg = _locInvLineTotal,
                                            DUOM = _locTransferInLine.DUOM,
                                            JournalDate = now,
                                            TransferIn = _locTransferInXPO,
                                        };
                                        _locNegatifInventoryJournal.Save();
                                        _locNegatifInventoryJournal.Session.CommitTransaction();
                                    }
                                }

                                #endregion Negatif
                                #region Positif
                                if (_locTransferInLine.Item != null && _locTransferInLine.UOM != null && _locTransferInLine.DUOM != null)
                                {
                                    ItemUnitOfMeasure _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                        (new GroupOperator(GroupOperatorType.And,
                                                         new BinaryOperator("Item", _locTransferInLine.Item),
                                                         new BinaryOperator("UOM", _locTransferInLine.UOM),
                                                         new BinaryOperator("DefaultUOM", _locTransferInLine.DUOM),
                                                         new BinaryOperator("Active", true)));
                                    if (_locItemUOM != null)
                                    {
                                        #region Code
                                        if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locTransferInLine.Qty * _locItemUOM.DefaultConversion + _locTransferInLine.DQty;
                                        }
                                        else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locTransferInLine.Qty / _locItemUOM.Conversion + _locTransferInLine.DQty;
                                        }
                                        else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locTransferInLine.Qty + _locTransferInLine.DQty;
                                        }
                                        #endregion Code

                                        InventoryJournal _locPositifInventoryJournal = new InventoryJournal(_currSession)
                                        {
                                            DocumentType = _locTransferInXPO.DocumentType,
                                            DocNo = _locTransferInXPO.DocNo,
                                            Location = _locTransferInLine.LocationTo,
                                            BinLocation = _locTransferInLine.BinLocationTo,
                                            LocationType = _locTransferInLine.LocationTypeTo,
                                            StockType = _locTransferInLine.StockTypeTo,
                                            StockGroup = _locTransferInLine.StockGroupTo,
                                            Item = _locTransferInLine.Item,
                                            Company = _locTransferInLine.Company,
                                            Workplace = _locTransferInLine.WorkplaceTo,
                                            QtyPos = _locInvLineTotal,
                                            QtyNeg = 0,
                                            DUOM = _locTransferInLine.DUOM,
                                            JournalDate = now,
                                            TransferIn = _locTransferInXPO,
                                        };
                                        _locPositifInventoryJournal.Save();
                                        _locPositifInventoryJournal.Session.CommitTransaction();

                                    }
                                }
                                else
                                {
                                    _locInvLineTotal = _locTransferInLine.Qty + _locTransferInLine.DQty;

                                    InventoryJournal _locPositifInventoryJournal = new InventoryJournal(_currSession)
                                    {
                                        DocumentType = _locTransferInXPO.DocumentType,
                                        DocNo = _locTransferInXPO.DocNo,
                                        Location = _locTransferInLine.LocationTo,
                                        BinLocation = _locTransferInLine.BinLocationTo,
                                        LocationType = _locTransferInLine.LocationTypeTo,
                                        StockType = _locTransferInLine.StockTypeTo,
                                        StockGroup = _locTransferInLine.StockGroupTo,
                                        Item = _locTransferInLine.Item,
                                        Company = _locTransferInLine.Company,
                                        Workplace = _locTransferInLine.WorkplaceTo,
                                        QtyPos = _locInvLineTotal,
                                        QtyNeg = 0,
                                        DUOM = _locTransferInLine.DUOM,
                                        JournalDate = now,
                                        TransferIn = _locTransferInXPO,
                                    };
                                    _locPositifInventoryJournal.Save();
                                    _locPositifInventoryJournal.Session.CommitTransaction();
                                }
                                
                                #endregion Positif
                            }
                            #endregion NonLotNumber
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = TransferIn ", ex.ToString());
            }
        }

        private void SetDeliveryStatusInSalesInvoiceMonitoring(Session _currSession, TransferIn _locTransferInXPO)
        {
            try
            {
                if (_locTransferInXPO != null)
                {
                    DateTime now = DateTime.Now;
                    XPCollection<TransferInLine> _locTransferInLines = new XPCollection<TransferInLine>
                                                                                    (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                    new BinaryOperator("TransferIn", _locTransferInXPO),
                                                                                    new BinaryOperator("Select", true),
                                                                                    new GroupOperator(GroupOperatorType.Or,
                                                                                    new BinaryOperator("Status", Status.Progress),
                                                                                    new BinaryOperator("Status", Status.Posted))));
                    if (_locTransferInLines != null && _locTransferInLines.Count() > 0)
                    {
                        foreach (TransferInLine _locTransferInLine in _locTransferInLines)
                        {
                            if (_locTransferInLine.SalesInvoiceMonitoring != null)
                            {
                                _locTransferInLine.SalesInvoiceMonitoring.ATD = now;
                                _locTransferInLine.SalesInvoiceMonitoring.DeliveryStatus = DeliveryStatus.Loading;
                                _locTransferInLine.SalesInvoiceMonitoring.Save();
                                _locTransferInLine.SalesInvoiceMonitoring.Session.CommitTransaction();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = TransferIn " + ex.ToString());
            }
        }

        private void SetNullTransferInOnTransferOutMonitoring(Session _currSession, TransferIn _locTransferInXPO)
        {
            try
            {
                if (_locTransferInXPO != null)
                {
                    XPCollection<TransferInLine> _locTransferInLines = new XPCollection<TransferInLine>
                                                                                    (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                    new BinaryOperator("TransferIn", _locTransferInXPO),
                                                                                    new BinaryOperator("Select", true),
                                                                                    new GroupOperator(GroupOperatorType.Or,
                                                                                    new BinaryOperator("Status", Status.Progress),
                                                                                    new BinaryOperator("Status", Status.Posted))));
                    if (_locTransferInLines != null && _locTransferInLines.Count() > 0)
                    {
                        foreach (TransferInLine _locTransferInLine in _locTransferInLines)
                        {
                            if (_locTransferInLine.TransferOutMonitoring != null)
                            {
                                _locTransferInLine.TransferOutMonitoring.TransferIn = null;
                                _locTransferInLine.TransferOutMonitoring.Save();
                                _locTransferInLine.TransferOutMonitoring.Session.CommitTransaction();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = TransferIn " + ex.ToString());
            }
            
        }

        private void SetStatusReceiveTransferInLine(Session _currSession, TransferIn _locTransferInXPO)
        {
            try
            {
                DateTime now = DateTime.Now;
                if (_locTransferInXPO != null)
                {
                    XPCollection<TransferInLine> _locTransferInLines = new XPCollection<TransferInLine>(_currSession,
                                                            new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("TransferIn", _locTransferInXPO),
                                                            new BinaryOperator("Select", true),
                                                            new GroupOperator(GroupOperatorType.Or,
                                                            new BinaryOperator("Status", Status.Progress),
                                                            new BinaryOperator("Status", Status.Posted))));

                    if (_locTransferInLines != null && _locTransferInLines.Count > 0)
                    {
                        foreach (TransferInLine _locTransferInLine in _locTransferInLines)
                        {
                            _locTransferInLine.Status = Status.Close;
                            _locTransferInLine.ActivationPosting = true;
                            _locTransferInLine.StatusDate = now;
                            _locTransferInLine.Save();
                            _locTransferInLine.Session.CommitTransaction();

                            XPCollection<TransferInLot> _locTransferInLots = new XPCollection<TransferInLot>(_currSession,
                                                                                     new GroupOperator(GroupOperatorType.And,
                                                                                     new BinaryOperator("TransferInLine", _locTransferInLine),
                                                                                     new BinaryOperator("Select", true),
                                                                                     new GroupOperator(GroupOperatorType.Or,
                                                                                     new BinaryOperator("Status", Status.Progress),
                                                                                     new BinaryOperator("Status", Status.Posted))));

                            if (_locTransferInLots != null && _locTransferInLots.Count() > 0)
                            {
                                foreach (TransferInLot _locTransferInLot in _locTransferInLots)
                                {
                                    _locTransferInLot.Status = Status.Close;
                                    _locTransferInLot.ActivationPosting = true;
                                    _locTransferInLot.StatusDate = now;
                                    _locTransferInLot.Save();
                                    _locTransferInLot.Session.CommitTransaction();
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = TransferIn " + ex.ToString());
            }
        }

        private void SetNormalReceiveQuantity(Session _currSession, TransferIn _locTransferInXPO)
        {
            try
            {
                if (_locTransferInXPO != null)
                {
                    XPCollection<TransferInLine> _locTransferInLines = new XPCollection<TransferInLine>(_currSession,
                                                                                     new GroupOperator(GroupOperatorType.And,
                                                                                     new BinaryOperator("TransferIn", _locTransferInXPO),
                                                                                     new BinaryOperator("Select", true),
                                                                                     new GroupOperator(GroupOperatorType.Or,
                                                                                     new BinaryOperator("Status", Status.Progress),
                                                                                     new BinaryOperator("Status", Status.Posted))));

                    if (_locTransferInLines != null && _locTransferInLines.Count > 0)
                    {
                        foreach (TransferInLine _locTransferInLine in _locTransferInLines)
                        {
                            if (_locTransferInLine.DQty > 0 || _locTransferInLine.Qty > 0)
                            {
                                _locTransferInLine.Select = false;
                                _locTransferInLine.Save();
                                _locTransferInLine.Session.CommitTransaction();

                                XPCollection<TransferInLot> _locTransferInLots = new XPCollection<TransferInLot>(_currSession,
                                                                                     new GroupOperator(GroupOperatorType.And,
                                                                                     new BinaryOperator("TransferInLine", _locTransferInLine),
                                                                                     new BinaryOperator("Select", true),
                                                                                     new GroupOperator(GroupOperatorType.Or,
                                                                                     new BinaryOperator("Status", Status.Progress),
                                                                                     new BinaryOperator("Status", Status.Posted))));

                                if (_locTransferInLots != null && _locTransferInLots.Count() > 0)
                                {
                                    foreach (TransferInLot _locTransferInLot in _locTransferInLots)
                                    {
                                        if (_locTransferInLot.Status == Status.Progress || _locTransferInLot.Status == Status.Posted)
                                        {
                                            _locTransferInLot.Select = false;
                                            _locTransferInLot.Save();
                                            _locTransferInLot.Session.CommitTransaction();
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = TransferIn " + ex.ToString());
            }
        }

        private void SetFinalStatusReceiveTransferIn(Session _currSession, TransferIn _locTransferInXPO)
        {
            try
            {
                DateTime now = DateTime.Now;
                int _locStatusCount = 0;
                int _locTransOutLineCount = 0;

                if (_locTransferInXPO != null)
                {
                    XPCollection<TransferInLine> _locTransferInLines = new XPCollection<TransferInLine>(_currSession,
                                                                                     new GroupOperator(GroupOperatorType.And,
                                                                                     new BinaryOperator("TransferIn", _locTransferInXPO)));

                    if (_locTransferInLines != null && _locTransferInLines.Count() > 0)
                    {
                        _locTransOutLineCount = _locTransferInLines.Count();

                        foreach (TransferInLine _locTransferInLine in _locTransferInLines)
                        {
                            if (_locTransferInLine.Status == Status.Close)
                            {
                                _locStatusCount = _locStatusCount + 1;
                            }
                        }
                        if (_locTransOutLineCount == _locStatusCount)
                        {
                            _locTransferInXPO.ActivationPosting = true;
                            _locTransferInXPO.Status = Status.Close;
                            _locTransferInXPO.StatusDate = now;
                            _locTransferInXPO.PostedCount = _locTransferInXPO.PostedCount + 1;
                            _locTransferInXPO.Save();
                            _locTransferInXPO.Session.CommitTransaction();
                        }
                        else
                        {
                            _locTransferInXPO.Status = Status.Posted;
                            _locTransferInXPO.StatusDate = now;
                            _locTransferInXPO.PostedCount = _locTransferInXPO.PostedCount + 1;
                            _locTransferInXPO.Save();
                            _locTransferInXPO.Session.CommitTransaction();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = TransferIn " + ex.ToString());
            }
        }
        #endregion IMT=TransferIn

        #region IMT=ReturnAndOther

        private void SetTransferInMonitoringForReturn(Session _currSession, TransferIn _locTransferInXPO)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                DateTime now = DateTime.Now;

                if (_locTransferInXPO != null)
                {
                    if (_locTransferInXPO.Status == Status.Progress || _locTransferInXPO.Status == Status.Posted)
                    {
                        XPCollection<TransferInLine> _locTransferInLines = new XPCollection<TransferInLine>
                                                            (_currSession, new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("TransferIn", _locTransferInXPO),
                                                            new BinaryOperator("Select", true),
                                                            new GroupOperator(GroupOperatorType.Or,
                                                            new BinaryOperator("Status", Status.Progress),
                                                            new BinaryOperator("Status", Status.Posted))));

                        if (_locTransferInLines != null && _locTransferInLines.Count() > 0)
                        {
                            foreach (TransferInLine _locTransferInLine in _locTransferInLines)
                            {
                                TransferInMonitoring _saveDataTransferInMonitoring = new TransferInMonitoring(_currSession)
                                {
                                    TransferIn = _locTransferInXPO,
                                    TransferInLine = _locTransferInLine,
                                    TransferOutMonitoring = _locTransferInLine.TransferOutMonitoring,
                                    SalesInvoiceMonitoring = _locTransferInLine.SalesInvoiceMonitoring,
                                    SalesInvoice = _locTransferInLine.SalesInvoice,
                                    Company = _locTransferInLine.Company,
                                    Division = _locTransferInLine.Division,
                                    Department = _locTransferInLine.Department,
                                    Section = _locTransferInLine.Section,
                                    Employee = _locTransferInLine.Employee,
                                    WorkplaceFrom = _locTransferInLine.WorkplaceFrom,
                                    WorkplaceTo = _locTransferInLine.WorkplaceTo,
                                    LocationFrom = _locTransferInLine.LocationFrom,
                                    BinLocationFrom = _locTransferInLine.BinLocationFrom,
                                    StockTypeFrom = _locTransferInLine.StockTypeFrom,
                                    StockGroupFrom = _locTransferInLine.StockGroupFrom,
                                    LocationTo = _locTransferInLine.LocationTo,
                                    BinLocationTo = _locTransferInLine.BinLocationTo,
                                    StockTypeTo = _locTransferInLine.StockTypeTo,
                                    StockGroupTo = _locTransferInLine.StockGroupTo,
                                    Vehicle = _locTransferInXPO.Vehicle,
                                    Item = _locTransferInLine.Item,
                                    Brand = _locTransferInLine.Brand,
                                    Description = _locTransferInLine.Description,
                                    MxDQty = _locTransferInLine.DQty,
                                    MxDUOM = _locTransferInLine.DUOM,
                                    MxQty = _locTransferInLine.Qty,
                                    MxUOM = _locTransferInLine.UOM,
                                    MxTQty = _locTransferInLine.TQty,
                                    DQty = _locTransferInLine.DQty,
                                    DUOM = _locTransferInLine.DUOM,
                                    Qty = _locTransferInLine.Qty,
                                    UOM = _locTransferInLine.UOM,
                                    TQty = _locTransferInLine.TQty,
                                    ETD = _locTransferInLine.ETD,
                                    ETA = _locTransferInLine.ETA,
                                    MSBegInv = _locTransferInLine.MSBegInv,
                                    MSBegInvLine = _locTransferInLine.MSBegInvLine,
                                };
                                _saveDataTransferInMonitoring.Save();
                                _saveDataTransferInMonitoring.Session.CommitTransaction();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = TransferIn ", ex.ToString());
            }
        }

        private void SetReceiveBeginingInventoryForReturn(Session _currSession, TransferIn _locTransferInXPO)
        {
            try
            {
                XPCollection<TransferInLine> _locTransferInLines = new XPCollection<TransferInLine>(_currSession,
                                                                        new GroupOperator(GroupOperatorType.And,
                                                                        new BinaryOperator("TransferIn", _locTransferInXPO),
                                                                        new BinaryOperator("Select", true),
                                                                        new GroupOperator(GroupOperatorType.Or,
                                                                        new BinaryOperator("Status", Status.Progress),
                                                                        new BinaryOperator("Status", Status.Posted))));

                if (_locTransferInLines != null && _locTransferInLines.Count() > 0)
                {
                    GlobalFunction _globFunc = new GlobalFunction();
                    double _locInvLineTotal = 0;
                    XPCollection<BeginningInventoryLine> _locBegInventoryLines = null;
                    ItemUnitOfMeasure _locItemUOM = null;


                    foreach (TransferInLine _locTransferInLine in _locTransferInLines)
                    {
                        if (_locTransferInLine.DQty > 0 || _locTransferInLine.Qty > 0)
                        {
                            XPCollection<TransferInLot> _locTransferInLots = new XPCollection<TransferInLot>
                                                                                    (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                    new BinaryOperator("TransferInLine", _locTransferInLine),
                                                                                    new BinaryOperator("Select", true),
                                                                                    new GroupOperator(GroupOperatorType.Or,
                                                                                    new BinaryOperator("Status", Status.Progress),
                                                                                    new BinaryOperator("Status", Status.Posted))));
                            #region LotNumber
                            if (_locTransferInLots != null && _locTransferInLots.Count() > 0)
                            {
                                foreach (TransferInLot _locTransferInLot in _locTransferInLots)
                                {
                                    if (_locTransferInLot.Select == true)
                                    {
                                        if (_locTransferInLot.BegInvLine != null)
                                        {
                                            if (_locTransferInLot.BegInvLine.Code != null)
                                            {
                                                BeginningInventoryLine _locBegInvLine = _currSession.FindObject<BeginningInventoryLine>
                                                                                (new GroupOperator(GroupOperatorType.And,
                                                                                 new BinaryOperator("Code", _locTransferInLot.BegInvLine.Code)));
                                                if (_locBegInvLine != null)
                                                {
                                                    if (_locBegInvLine.QtyAvailable >= _locTransferInLot.TQty)
                                                    {
                                                        _locBegInvLine.QtyAvailable = _locBegInvLine.QtyAvailable + _locTransferInLot.TQty;
                                                        _locBegInvLine.Save();
                                                        _locBegInvLine.Session.CommitTransaction();
                                                    }

                                                    if (_locBegInvLine.BeginningInventory != null)
                                                    {
                                                        if (_locBegInvLine.BeginningInventory.Code != null)
                                                        {
                                                            BeginningInventory _locBegInv = _currSession.FindObject<BeginningInventory>
                                                                                (new GroupOperator(GroupOperatorType.And,
                                                                                 new BinaryOperator("Code", _locBegInvLine.BeginningInventory.Code)));
                                                            if (_locBegInv != null)
                                                            {
                                                                _locBegInv.QtyAvailable = _locBegInv.QtyAvailable + _locTransferInLot.TQty;
                                                                _locBegInv.Save();
                                                                _locBegInv.Session.CommitTransaction();
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            #endregion LotNumber
                            #region NonLotNumber
                            else
                            {
                                if (_locTransferInLine.Item != null && _locTransferInLine.UOM != null && _locTransferInLine.DUOM != null)
                                {
                                    //for conversion
                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                        (new GroupOperator(GroupOperatorType.And,
                                                         new BinaryOperator("Item", _locTransferInLine.Item),
                                                         new BinaryOperator("UOM", _locTransferInLine.UOM),
                                                         new BinaryOperator("DefaultUOM", _locTransferInLine.DUOM),
                                                         new BinaryOperator("Active", true)));
                                }

                                #region BeginningInventoryTo
                                BeginningInventory _locBeginningInventory = _currSession.FindObject<BeginningInventory>(new GroupOperator(GroupOperatorType.And,
                                                                          new BinaryOperator("Location", _locTransferInLine.LocationTo),
                                                                          new BinaryOperator("Active", true)));

                                #region UpdateBeginingInventory
                                if (_locBeginningInventory != null)
                                {
                                    string _fullString = null;
                                    string _locLocationParse = null;
                                    string _locBinLocationParse = null;
                                    string _locItemParse = null;
                                    string _locStockTypeParse = null;
                                    string _locStockGroupParse = null;
                                    string _locActiveParse = null;

                                    #region code
                                    if (_locTransferInLine.LocationTo != null) { _locLocationParse = "[Location.Code]=='" + _locTransferInLine.LocationTo.Code + "'"; }
                                    else { _locLocationParse = ""; }

                                    if (_locTransferInLine.BinLocationTo != null && _locTransferInLine.LocationTo != null)
                                    { _locBinLocationParse = " AND [BinLocation.Code]=='" + _locTransferInLine.BinLocationTo.Code + "'"; }
                                    else if (_locTransferInLine.BinLocationTo != null && _locTransferInLine.LocationTo == null)
                                    { _locBinLocationParse = " [BinLocation.Code]=='" + _locTransferInLine.BinLocationTo.Code + "'"; }
                                    else { _locBinLocationParse = ""; }

                                    if (_locTransferInLine.Item != null && (_locTransferInLine.LocationFrom != null || _locTransferInLine.BinLocationFrom != null))
                                    { _locItemParse = " AND [Item.Code]=='" + _locTransferInLine.Item.Code + "'"; }
                                    else { _locItemParse = ""; }

                                    if (_locTransferInLine.StockTypeTo != StockType.None && ((_locTransferInLine.LocationTo != null && _locTransferInLine.Item != null) || _locTransferInLine.BinLocationTo != null))
                                    { _locStockTypeParse = " AND [StockType]=='" + GetStockType(_locTransferInLine.StockTypeTo).ToString() + "'"; }
                                    else { _locStockTypeParse = ""; }

                                    if (_locTransferInLine.StockGroupTo != StockGroup.None && ((_locTransferInLine.LocationTo != null && _locTransferInLine.Item != null) || _locTransferInLine.BinLocationTo != null))
                                    { _locStockGroupParse = " AND [StockGroup]=='" + GetStockGroup(_locTransferInLine.StockGroupTo).ToString() + "'"; }
                                    else { _locStockGroupParse = ""; }

                                    if (_locTransferInLine.LocationTo != null || _locTransferInLine.BinLocationTo != null)
                                    { _locActiveParse = " AND [Active]=='" + GetActive(true).ToString() + "'"; }
                                    else { _locActiveParse = " AND [Active]=='" + GetActive(true).ToString() + "'"; }

                                    if (_locLocationParse != null || _locBinLocationParse != null || _locItemParse != null || _locStockTypeParse != null || _locStockGroupParse != null)
                                    {
                                        _fullString = _locLocationParse + _locBinLocationParse + _locItemParse + _locStockTypeParse + _locStockGroupParse + _locActiveParse;
                                    }

                                    #endregion code

                                    _locBegInventoryLines = new XPCollection<BeginningInventoryLine>(_currSession, CriteriaOperator.Parse(_fullString));

                                    #region UpdateBeginingInventoryLine
                                    if (_locBegInventoryLines != null && _locBegInventoryLines.Count() > 0)
                                    {
                                        foreach (BeginningInventoryLine _locBegInventoryLine in _locBegInventoryLines)
                                        {
                                            if (_locItemUOM != null)
                                            {
                                                if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                                {
                                                    _locInvLineTotal = _locTransferInLine.Qty * _locItemUOM.DefaultConversion + _locTransferInLine.DQty;
                                                }
                                                else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                                {
                                                    _locInvLineTotal = _locTransferInLine.Qty / _locItemUOM.Conversion + _locTransferInLine.DQty;
                                                }
                                                else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                                {
                                                    _locInvLineTotal = _locTransferInLine.Qty + _locTransferInLine.DQty;
                                                }
                                            }

                                            else
                                            {
                                                _locInvLineTotal = _locTransferInLine.Qty + _locTransferInLine.DQty;
                                            }

                                            _locBegInventoryLine.QtyAvailable = _locBegInventoryLine.QtyAvailable + _locInvLineTotal;
                                            _locBegInventoryLine.Save();
                                            _locBegInventoryLine.Session.CommitTransaction();

                                            _locBeginningInventory.QtyAvailable = _locBeginningInventory.QtyAvailable + _locInvLineTotal;
                                            _locBeginningInventory.Save();
                                            _locBeginningInventory.Session.CommitTransaction();
                                        }
                                    }
                                    #endregion UpdateBeginingInventoryLine
                                    #region CreateNewBeginingInventoryLine
                                    else
                                    {
                                        if (_locItemUOM != null)
                                        {
                                            if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                            {
                                                _locInvLineTotal = _locTransferInLine.Qty * _locItemUOM.DefaultConversion + _locTransferInLine.DQty;
                                            }
                                            else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                            {
                                                _locInvLineTotal = _locTransferInLine.Qty / _locItemUOM.Conversion + _locTransferInLine.DQty;
                                            }
                                            else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                            {
                                                _locInvLineTotal = _locTransferInLine.Qty + _locTransferInLine.DQty;
                                            }
                                        }
                                        else
                                        {
                                            _locInvLineTotal = _locTransferInLine.Qty + _locTransferInLine.DQty;
                                        }

                                        BeginningInventoryLine _locSaveDataBeginingInventoryLine = new BeginningInventoryLine(_currSession)
                                        {
                                            Company = _locTransferInXPO.Company,
                                            Workplace = _locTransferInXPO.WorkplaceTo,
                                            Item = _locTransferInLine.Item,
                                            Location = _locTransferInLine.LocationTo,
                                            Vehicle = _locTransferInXPO.Vehicle,
                                            BinLocation = _locTransferInLine.BinLocationTo,
                                            QtyAvailable = _locInvLineTotal,
                                            DefaultUOM = _locTransferInLine.DUOM,
                                            StockType = _locTransferInLine.StockTypeTo,
                                            StockGroup = _locTransferInLine.StockGroupTo,
                                            Active = true,
                                            BeginningInventory = _locBeginningInventory,
                                        };
                                        _locSaveDataBeginingInventoryLine.Save();
                                        _locSaveDataBeginingInventoryLine.Session.CommitTransaction();

                                        _locBeginningInventory.QtyAvailable = _locBeginningInventory.QtyAvailable + _locInvLineTotal;
                                        _locBeginningInventory.Save();
                                        _locBeginningInventory.Session.CommitTransaction();
                                    }
                                    #endregion CreateNewBeginingInventoryLine

                                }
                                #endregion UpdateBeginingInventory
                                #endregion BeginningInventoryTo
                            }
                            #endregion NonLotNumber
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = TransferIn ", ex.ToString());
            }
        }

        private void SetReceiveInventoryJournalForReturn(Session _currSession, TransferIn _locTransferInXPO)
        {
            try
            {
                XPCollection<TransferInLine> _locTransferInLines = new XPCollection<TransferInLine>(_currSession,
                                                                        new GroupOperator(GroupOperatorType.And,
                                                                        new BinaryOperator("TransferIn", _locTransferInXPO),
                                                                        new BinaryOperator("Select", true),
                                                                        new GroupOperator(GroupOperatorType.Or,
                                                                        new BinaryOperator("Status", Status.Progress),
                                                                        new BinaryOperator("Status", Status.Posted))));

                if (_locTransferInLines != null && _locTransferInLines.Count > 0)
                {
                    double _locInvLineTotal = 0;
                    DateTime now = DateTime.Now;

                    foreach (TransferInLine _locTransferInLine in _locTransferInLines)
                    {
                        if (_locTransferInLine.DQty > 0 || _locTransferInLine.Qty > 0)
                        {
                            XPCollection<TransferInLot> _locTransferInLots = new XPCollection<TransferInLot>
                                                                                    (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                    new BinaryOperator("TransferInLine", _locTransferInLine),
                                                                                    new BinaryOperator("Select", true),
                                                                                    new GroupOperator(GroupOperatorType.Or,
                                                                                    new BinaryOperator("Status", Status.Progress),
                                                                                    new BinaryOperator("Status", Status.Posted))));

                            #region LotNumber
                            if (_locTransferInLots != null && _locTransferInLots.Count() > 0)
                            {
                                foreach (TransferInLot _locTransferInLot in _locTransferInLots)
                                {
                                    if (_locTransferInLot.Select == true)
                                    {
                                        if (_locTransferInLot.BegInvLine != null)
                                        {
                                            if (_locTransferInLot.BegInvLine.Code != null)
                                            {
                                                BeginningInventoryLine _locBegInvLine = _currSession.FindObject<BeginningInventoryLine>
                                                                                (new GroupOperator(GroupOperatorType.And,
                                                                                 new BinaryOperator("Code", _locTransferInLot.BegInvLine.Code)));
                                                if (_locBegInvLine != null)
                                                {
                                                    InventoryJournal _locPositiveInventoryJournal = new InventoryJournal(_currSession)
                                                    {
                                                        DocumentType = _locTransferInXPO.DocumentType,
                                                        DocNo = _locTransferInXPO.DocNo,
                                                        Location = _locTransferInLot.LocationFrom,
                                                        BinLocation = _locTransferInLot.BinLocationFrom,
                                                        StockType = _locTransferInLot.StockTypeFrom,
                                                        Item = _locTransferInLot.Item,
                                                        Company = _locTransferInLot.Company,
                                                        QtyPos = _locTransferInLot.TQty,
                                                        QtyNeg = 0,
                                                        DUOM = _locTransferInLot.DUOM,
                                                        JournalDate = now,
                                                        LotNumber = _locTransferInLot.LotNumber,
                                                        TransferIn = _locTransferInXPO,
                                                    };
                                                    _locPositiveInventoryJournal.Save();
                                                    _locPositiveInventoryJournal.Session.CommitTransaction();
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            #endregion LotNumber

                            #region NonLotNumber
                            else
                            {
                                if (_locTransferInLine.Item != null && _locTransferInLine.UOM != null && _locTransferInLine.DUOM != null)
                                {
                                    ItemUnitOfMeasure _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                        (new GroupOperator(GroupOperatorType.And,
                                                         new BinaryOperator("Item", _locTransferInLine.Item),
                                                         new BinaryOperator("UOM", _locTransferInLine.UOM),
                                                         new BinaryOperator("DefaultUOM", _locTransferInLine.DUOM),
                                                         new BinaryOperator("Active", true)));
                                    if (_locItemUOM != null)
                                    {
                                        #region Code
                                        if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locTransferInLine.Qty * _locItemUOM.DefaultConversion + _locTransferInLine.DQty;
                                        }
                                        else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locTransferInLine.Qty / _locItemUOM.Conversion + _locTransferInLine.DQty;
                                        }
                                        else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locTransferInLine.Qty + _locTransferInLine.DQty;
                                        }
                                        #endregion Code

                                        InventoryJournal _locPositiveInventoryJournal = new InventoryJournal(_currSession)
                                        {
                                            DocumentType = _locTransferInXPO.DocumentType,
                                            DocNo = _locTransferInXPO.DocNo,
                                            Location = _locTransferInLine.LocationFrom,
                                            BinLocation = _locTransferInLine.BinLocationFrom,
                                            StockType = _locTransferInLine.StockTypeFrom,
                                            StockGroup = _locTransferInLine.StockGroupFrom,
                                            Item = _locTransferInLine.Item,
                                            Company = _locTransferInLine.Company,
                                            Workplace = _locTransferInLine.WorkplaceTo,
                                            QtyPos = _locInvLineTotal,
                                            QtyNeg = 0,
                                            DUOM = _locTransferInLine.DUOM,
                                            JournalDate = now,
                                            TransferIn = _locTransferInXPO,
                                        };
                                        _locPositiveInventoryJournal.Save();
                                        _locPositiveInventoryJournal.Session.CommitTransaction();

                                    }
                                }
                                else
                                {
                                    _locInvLineTotal = _locTransferInLine.Qty + _locTransferInLine.DQty;

                                    InventoryJournal _locPositiveInventoryJournal = new InventoryJournal(_currSession)
                                    {
                                        DocumentType = _locTransferInXPO.DocumentType,
                                        DocNo = _locTransferInXPO.DocNo,
                                        Location = _locTransferInLine.LocationFrom,
                                        BinLocation = _locTransferInLine.BinLocationFrom,
                                        StockType = _locTransferInLine.StockTypeFrom,
                                        StockGroup = _locTransferInLine.StockGroupFrom,
                                        Item = _locTransferInLine.Item,
                                        Company = _locTransferInLine.Company,
                                        Workplace = _locTransferInLine.WorkplaceTo,
                                        QtyPos = _locInvLineTotal,
                                        QtyNeg = 0,
                                        DUOM = _locTransferInLine.DUOM,
                                        JournalDate = now,
                                        TransferIn = _locTransferInXPO,
                                    };
                                    _locPositiveInventoryJournal.Save();
                                    _locPositiveInventoryJournal.Session.CommitTransaction();
                                }
                            }
                            #endregion NonLotNumber
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = TransferIn ", ex.ToString());
            }
        }

        private void SetNormalReceiveQuantityForReturn(Session _currSession, TransferIn _locTransferInXPO)
        {
            try
            {
                if (_locTransferInXPO != null)
                {
                    XPCollection<TransferInLine> _locTransferInLines = new XPCollection<TransferInLine>(_currSession,
                                                                                     new GroupOperator(GroupOperatorType.And,
                                                                                     new BinaryOperator("TransferIn", _locTransferInXPO),
                                                                                     new BinaryOperator("Select", true),
                                                                                     new GroupOperator(GroupOperatorType.Or,
                                                                                     new BinaryOperator("Status", Status.Progress),
                                                                                     new BinaryOperator("Status", Status.Posted))));

                    if (_locTransferInLines != null && _locTransferInLines.Count > 0)
                    {
                        foreach (TransferInLine _locTransferInLine in _locTransferInLines)
                        {
                            if (_locTransferInLine.DQty > 0 || _locTransferInLine.Qty > 0)
                            {
                                _locTransferInLine.Select = false;
                                _locTransferInLine.Save();
                                _locTransferInLine.Session.CommitTransaction();

                                XPCollection<TransferInLot> _locTransferInLots = new XPCollection<TransferInLot>(_currSession,
                                                                                     new GroupOperator(GroupOperatorType.And,
                                                                                     new BinaryOperator("TransferInLine", _locTransferInLine),
                                                                                     new BinaryOperator("Select", true),
                                                                                     new GroupOperator(GroupOperatorType.Or,
                                                                                     new BinaryOperator("Status", Status.Progress),
                                                                                     new BinaryOperator("Status", Status.Posted))));

                                if (_locTransferInLots != null && _locTransferInLots.Count() > 0)
                                {
                                    foreach (TransferInLot _locTransferInLot in _locTransferInLots)
                                    {
                                        if (_locTransferInLot.Status == Status.Progress || _locTransferInLot.Status == Status.Posted)
                                        {
                                            _locTransferInLot.Select = false;
                                            _locTransferInLot.Save();
                                            _locTransferInLot.Session.CommitTransaction();
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = TransferIn " + ex.ToString());
            }
        }

        private void SetFinalStatusReceiveTransferInForReturn(Session _currSession, TransferIn _locTransferInXPO)
        {
            try
            {
                DateTime now = DateTime.Now;
                int _locStatusCount = 0;
                int _locTransOutLineCount = 0;

                if (_locTransferInXPO != null)
                {
                    XPCollection<TransferInLine> _locTransferInLines = new XPCollection<TransferInLine>(_currSession,
                                                                                     new GroupOperator(GroupOperatorType.And,
                                                                                     new BinaryOperator("TransferIn", _locTransferInXPO)));

                    if (_locTransferInLines != null && _locTransferInLines.Count() > 0)
                    {
                        _locTransOutLineCount = _locTransferInLines.Count();

                        foreach (TransferInLine _locTransferInLine in _locTransferInLines)
                        {
                            if (_locTransferInLine.Status == Status.Close)
                            {
                                _locStatusCount = _locStatusCount + 1;
                            }
                        }
                        if (_locTransOutLineCount == _locStatusCount)
                        {
                            _locTransferInXPO.ActivationPosting = true;
                            _locTransferInXPO.Status = Status.Close;
                            _locTransferInXPO.StatusDate = now;
                            _locTransferInXPO.PostedCount = _locTransferInXPO.PostedCount + 1;
                            _locTransferInXPO.Save();
                            _locTransferInXPO.Session.CommitTransaction();
                        }
                        else
                        {
                            _locTransferInXPO.Status = Status.Posted;
                            _locTransferInXPO.StatusDate = now;
                            _locTransferInXPO.PostedCount = _locTransferInXPO.PostedCount + 1;
                            _locTransferInXPO.Save();
                            _locTransferInXPO.Session.CommitTransaction();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = TransferIn " + ex.ToString());
            }
        }
        #endregion IMT=ReturnAndOther

        #endregion Posting

        #region Global Method

        private void SuccessMessageShow(string _locActionName)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 2000;
            options.Message = string.Format("{0} !", _locActionName);
            options.Type = InformationType.Success;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Success";
            options.Win.Type = WinMessageType.Flyout;
            Application.ShowViewStrategy.ShowMessage(options);
        }

        private void ErrorMessageShow(string _locMessage)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 2000;
            options.Message = string.Format(_locMessage);
            options.Type = InformationType.Warning;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Error";
            options.Win.Type = WinMessageType.Alert;
            Application.ShowViewStrategy.ShowMessage(options);
        }

        private int GetStockType(StockType objectName)
        {
            int _result = 0;
            try
            {
                if (objectName == StockType.Good)
                {
                    _result = 1;
                }
                else if (objectName == StockType.Bad)
                {
                    _result = 2;
                }
                else
                {
                    _result = 0;
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Module = TransferIn " + ex.ToString());
            }
            return _result;
        }

        private int GetStockGroup(StockGroup objectName)
        {
            int _result = 0;
            try
            {
                if (objectName == StockGroup.Normal)
                {
                    _result = 1;
                }
                else if (objectName == StockGroup.FreeItem)
                {
                    _result = 2;
                }
                else
                {
                    _result = 0;
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Module = TransferIn " + ex.ToString());
            }
            return _result;
        }

        private int GetActive(bool objectName)
        {
            int _result = 0;
            try
            {
                if (objectName == true)
                {
                    _result = 1;
                }
                else
                {
                    _result = 0;
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Module = TransferIn " + ex.ToString());
            }
            return _result;
        }

        #endregion Global Method

        
    }
}
