﻿namespace FullDrive.Module.Controllers
{
    partial class InventoryTransferInMonitoringActionController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.InventoryTransferInMonitoringSelectAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.InventoryTransferInMonitoringUnselectAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.InventoryTransferInMonitoringCancelPIAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            // 
            // InventoryTransferInMonitoringSelectAction
            // 
            this.InventoryTransferInMonitoringSelectAction.Caption = "Select";
            this.InventoryTransferInMonitoringSelectAction.ConfirmationMessage = null;
            this.InventoryTransferInMonitoringSelectAction.Id = "InventoryTransferInMonitoringSelectActionId";
            this.InventoryTransferInMonitoringSelectAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.InventoryTransferInMonitoring);
            this.InventoryTransferInMonitoringSelectAction.ToolTip = null;
            this.InventoryTransferInMonitoringSelectAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.InventoryTransferInMonitoringSelectAction_Execute);
            // 
            // InventoryTransferInMonitoringUnselectAction
            // 
            this.InventoryTransferInMonitoringUnselectAction.Caption = "Unselect";
            this.InventoryTransferInMonitoringUnselectAction.ConfirmationMessage = null;
            this.InventoryTransferInMonitoringUnselectAction.Id = "InventoryTransferInMonitoringUnselectActionId";
            this.InventoryTransferInMonitoringUnselectAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.InventoryTransferInMonitoring);
            this.InventoryTransferInMonitoringUnselectAction.ToolTip = null;
            this.InventoryTransferInMonitoringUnselectAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.InventoryTransferInMonitoringUnselectAction_Execute);
            // 
            // InventoryTransferInMonitoringCancelPIAction
            // 
            this.InventoryTransferInMonitoringCancelPIAction.Caption = "Cancel PI";
            this.InventoryTransferInMonitoringCancelPIAction.ConfirmationMessage = null;
            this.InventoryTransferInMonitoringCancelPIAction.Id = "InventoryTransferInMonitoringCancelPIActionId";
            this.InventoryTransferInMonitoringCancelPIAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.InventoryTransferInMonitoring);
            this.InventoryTransferInMonitoringCancelPIAction.ToolTip = null;
            this.InventoryTransferInMonitoringCancelPIAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.InventoryTransferInMonitoringCancelPIAction_Execute);
            // 
            // InventoryTransferInMonitoringActionController
            // 
            this.Actions.Add(this.InventoryTransferInMonitoringSelectAction);
            this.Actions.Add(this.InventoryTransferInMonitoringUnselectAction);
            this.Actions.Add(this.InventoryTransferInMonitoringCancelPIAction);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SimpleAction InventoryTransferInMonitoringSelectAction;
        private DevExpress.ExpressApp.Actions.SimpleAction InventoryTransferInMonitoringUnselectAction;
        private DevExpress.ExpressApp.Actions.SimpleAction InventoryTransferInMonitoringCancelPIAction;
    }
}
