﻿namespace FullDrive.Module.Controllers
{
    partial class TransferOutLineActionController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.TransferOutLineSelectAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.TransferOutLineUnselectAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            // 
            // TransferOutLineSelectAction
            // 
            this.TransferOutLineSelectAction.Caption = "Select";
            this.TransferOutLineSelectAction.ConfirmationMessage = null;
            this.TransferOutLineSelectAction.Id = "TransferOutLineSelectActionId";
            this.TransferOutLineSelectAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.TransferOutLine);
            this.TransferOutLineSelectAction.ToolTip = null;
            this.TransferOutLineSelectAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.TransferOutLineSelectAction_Execute);
            // 
            // TransferOutLineUnselectAction
            // 
            this.TransferOutLineUnselectAction.Caption = "Unselect";
            this.TransferOutLineUnselectAction.ConfirmationMessage = null;
            this.TransferOutLineUnselectAction.Id = "TransferOutLineUnselectActionId";
            this.TransferOutLineUnselectAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.TransferOutLine);
            this.TransferOutLineUnselectAction.ToolTip = null;
            this.TransferOutLineUnselectAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.TransferOutLineUnselectAction_Execute);
            // 
            // TransferOutLineActionController
            // 
            this.Actions.Add(this.TransferOutLineSelectAction);
            this.Actions.Add(this.TransferOutLineUnselectAction);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SimpleAction TransferOutLineSelectAction;
        private DevExpress.ExpressApp.Actions.SimpleAction TransferOutLineUnselectAction;
    }
}
