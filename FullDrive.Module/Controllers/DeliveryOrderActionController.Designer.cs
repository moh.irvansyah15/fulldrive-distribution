﻿namespace FullDrive.Module.Controllers
{
    partial class DeliveryOrderActionController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.DeliveryOrderProgressAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.DeliveryOrderPostingAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.DeliveryOrderListviewFilterSelectionAction = new DevExpress.ExpressApp.Actions.SingleChoiceAction(this.components);
            this.DeliveryOrderGetSIAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            // 
            // DeliveryOrderProgressAction
            // 
            this.DeliveryOrderProgressAction.Caption = "Progress";
            this.DeliveryOrderProgressAction.ConfirmationMessage = null;
            this.DeliveryOrderProgressAction.Id = "DeliveryOrderProgressActionId";
            this.DeliveryOrderProgressAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.DeliveryOrder);
            this.DeliveryOrderProgressAction.ToolTip = null;
            this.DeliveryOrderProgressAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.DeliveryOrderProgressAction_Execute);
            // 
            // DeliveryOrderPostingAction
            // 
            this.DeliveryOrderPostingAction.Caption = "Posting";
            this.DeliveryOrderPostingAction.ConfirmationMessage = null;
            this.DeliveryOrderPostingAction.Id = "DeliveryOrderPostingActionId";
            this.DeliveryOrderPostingAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.DeliveryOrder);
            this.DeliveryOrderPostingAction.ToolTip = null;
            this.DeliveryOrderPostingAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.DeliveryOrderPostingAction_Execute);
            // 
            // DeliveryOrderListviewFilterSelectionAction
            // 
            this.DeliveryOrderListviewFilterSelectionAction.Caption = "Filter";
            this.DeliveryOrderListviewFilterSelectionAction.ConfirmationMessage = null;
            this.DeliveryOrderListviewFilterSelectionAction.Id = "DeliveryOrderListviewFilterSelectionActionId";
            this.DeliveryOrderListviewFilterSelectionAction.ItemType = DevExpress.ExpressApp.Actions.SingleChoiceActionItemType.ItemIsOperation;
            this.DeliveryOrderListviewFilterSelectionAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.DeliveryOrder);
            this.DeliveryOrderListviewFilterSelectionAction.ToolTip = null;
            this.DeliveryOrderListviewFilterSelectionAction.Execute += new DevExpress.ExpressApp.Actions.SingleChoiceActionExecuteEventHandler(this.DeliveryOrderListviewFilterSelectionAction_Execute);
            // 
            // DeliveryOrderGetSIAction
            // 
            this.DeliveryOrderGetSIAction.Caption = "Get SI";
            this.DeliveryOrderGetSIAction.ConfirmationMessage = null;
            this.DeliveryOrderGetSIAction.Id = "DeliveryOrderGetSIActionId";
            this.DeliveryOrderGetSIAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.DeliveryOrder);
            this.DeliveryOrderGetSIAction.ToolTip = null;
            this.DeliveryOrderGetSIAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.DeliveryOrderGetSIAction_Execute);
            // 
            // DeliveryOrderActionController
            // 
            this.Actions.Add(this.DeliveryOrderProgressAction);
            this.Actions.Add(this.DeliveryOrderPostingAction);
            this.Actions.Add(this.DeliveryOrderListviewFilterSelectionAction);
            this.Actions.Add(this.DeliveryOrderGetSIAction);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SimpleAction DeliveryOrderProgressAction;
        private DevExpress.ExpressApp.Actions.SimpleAction DeliveryOrderPostingAction;
        private DevExpress.ExpressApp.Actions.SingleChoiceAction DeliveryOrderListviewFilterSelectionAction;
        private DevExpress.ExpressApp.Actions.SimpleAction DeliveryOrderGetSIAction;
    }
}
