﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;

using DevExpress.Xpo;
using DevExpress.ExpressApp.Xpo;
using FullDrive.Module.BusinessObjects;

namespace FullDrive.Module.Controllers
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class CashAdvanceLineListviewFilterController2 : ViewController
    {
        public CashAdvanceLineListviewFilterController2()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.
            TargetViewId = "CashAdvance_CashAdvanceLines_ListView";
        }
        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
            #region FilterBaseOnOrganization
            XPCollection<OrganizationSetupDetail> _locOrgSetDetail1s = null;
            XPCollection<OrganizationSetupDetail> _locOrgSetDetail2s = null;
            XPCollection<OrganizationSetupDetail> _locOrganizationSetupDetails = null;
            var User = SecuritySystem.CurrentUserName;
            string _beginString1 = null;
            string _endString1 = null;
            string _fullString1 = null;
            string _beginString2 = null;
            string _endString2 = null;
            string _fullString2 = null;
            string _fullString = null;
            var ListView = View as ListView;
            Session currentSession = null;

            if (this.ObjectSpace != null)
            {
                UserAccess _locUserAccess = this.ObjectSpace.FindObject<UserAccess>(new BinaryOperator("UserName", User));
                if (_locUserAccess.Session != null)
                {
                    currentSession = _locUserAccess.Session;
                }
                if (_locUserAccess != null && currentSession != null)
                {
                    if (_locUserAccess.Employee != null)
                    {
                        List<string> _stringCompany = new List<string>();
                        List<string> _stringWorkplace = new List<string>();


                        _locOrgSetDetail1s = new XPCollection<OrganizationSetupDetail>
                                            (currentSession, new GroupOperator(GroupOperatorType.And,
                                            new BinaryOperator("UserAccess", _locUserAccess),
                                            new BinaryOperator("Process", CustomProcess.Process.CashAdvance),
                                            new BinaryOperator("PageType", CustomProcess.PageType.ListView),
                                            new BinaryOperator("Active", true)));

                        if (_locOrgSetDetail1s != null && _locOrgSetDetail1s.Count() > 0)
                        {
                            _locOrganizationSetupDetails = _locOrgSetDetail1s;
                        }
                        else
                        {
                            _locOrgSetDetail2s = new XPCollection<OrganizationSetupDetail>
                                                (currentSession, new GroupOperator(GroupOperatorType.And,
                                                new BinaryOperator("UserAccess", _locUserAccess),
                                                new BinaryOperator("ObjectList", CustomProcess.ObjectList.CashAdvanceLine),
                                                new BinaryOperator("PageType", CustomProcess.PageType.ListView),
                                                new BinaryOperator("Active", true)));
                            if (_locOrgSetDetail2s != null && _locOrgSetDetail2s.Count() > 0)
                            {
                                _locOrganizationSetupDetails = _locOrgSetDetail2s;
                            }
                        }

                        #region TakeDataFromOrganizationSetupDetail
                        if (_locOrganizationSetupDetails != null && _locOrganizationSetupDetails.Count > 0)
                        {
                            foreach (OrganizationSetupDetail _locOrganizationSetupDetail in _locOrganizationSetupDetails)
                            {
                                if (_locOrganizationSetupDetail.Company != null)
                                {
                                    if (_locOrganizationSetupDetail.Company.Code != null) { _stringCompany.Add(_locOrganizationSetupDetail.Company.Code); }
                                }
                                if (_locOrganizationSetupDetail.Workplace != null)
                                {
                                    if (_locOrganizationSetupDetail.Workplace.Code != null) { _stringWorkplace.Add(_locOrganizationSetupDetail.Workplace.Code); }
                                }
                            }
                        }
                        #endregion TakeDataFromOrganizationSetupDetail
                        #region TakeDataDirectFromUser
                        else
                        {
                            if (_locUserAccess.Employee.Company != null)
                            {
                                if (_locUserAccess.Employee.Company.Code != null) { _stringCompany.Add(_locUserAccess.Employee.Company.Code); }
                            }
                            if (_locUserAccess.Employee.Workplace != null)
                            {
                                if (_locUserAccess.Employee.Workplace.Code != null) { _stringWorkplace.Add(_locUserAccess.Employee.Workplace.Code); }
                            }
                        }
                        #endregion TakeDataDirectFromUser

                        #region Company
                        IEnumerable<string> _stringArrayCompanyDistinct = _stringCompany.Distinct();
                        string[] _stringArrayCompanyList = _stringArrayCompanyDistinct.ToArray();
                        if (_stringArrayCompanyList.Length == 1)
                        {
                            for (int i = 0; i < _stringArrayCompanyList.Length; i++)
                            {
                                Company _locCompany = currentSession.FindObject<Company>(new BinaryOperator("Code", _stringArrayCompanyList[i]));
                                if (_locCompany != null)
                                {
                                    if (i == 0)
                                    {
                                        _beginString1 = "[Company.Code]=='" + _locCompany.Code + "'";
                                    }
                                }
                            }
                        }
                        else if (_stringArrayCompanyList.Length > 1)
                        {
                            for (int i = 0; i < _stringArrayCompanyList.Length; i++)
                            {
                                Company _locCompany = currentSession.FindObject<Company>(new BinaryOperator("Code", _stringArrayCompanyList[i]));
                                if (_locCompany != null)
                                {
                                    if (i == 0)
                                    {
                                        _beginString1 = "[Company.Code]=='" + _locCompany.Code + "'";
                                    }
                                    else
                                    {
                                        _endString1 = _endString1 + " OR [Company.Code]=='" + _locCompany.Code + "'";
                                    }
                                }
                            }
                        }
                        _fullString1 = _beginString1 + _endString1;

                        #endregion Company

                        #region Workplace
                        IEnumerable<string> _stringArrayWorkplaceDistinct = _stringWorkplace.Distinct();
                        string[] _stringArrayWorkplaceList = _stringArrayWorkplaceDistinct.ToArray();
                        if (_stringArrayWorkplaceList.Length == 1)
                        {
                            for (int i = 0; i < _stringArrayWorkplaceList.Length; i++)
                            {
                                Workplace _locWorkplace = currentSession.FindObject<Workplace>(new BinaryOperator("Code", _stringArrayWorkplaceList[i]));
                                if (_locWorkplace != null)
                                {
                                    if (i == 0)
                                    {
                                        _beginString2 = "[Workplace.Code]=='" + _locWorkplace.Code + "'";
                                    }
                                }
                            }
                        }
                        else if (_stringArrayWorkplaceList.Length > 1)
                        {
                            for (int i = 0; i < _stringArrayWorkplaceList.Length; i++)
                            {
                                Workplace _locWorkplace = currentSession.FindObject<Workplace>(new BinaryOperator("Code", _stringArrayWorkplaceList[i]));
                                if (_locWorkplace != null)
                                {
                                    if (i == 0)
                                    {
                                        _beginString2 = "[Workplace.Code]=='" + _locWorkplace.Code + "'";
                                    }
                                    else
                                    {
                                        _endString2 = _endString2 + " OR [Workplace.Code]=='" + _locWorkplace.Code + "'";
                                    }
                                }

                            }
                        }
                        _fullString2 = _beginString2 + _endString2;
                        #endregion Workplace

                        //Part1
                        if (_stringArrayCompanyList.Length >= 1 && _stringArrayWorkplaceList.Length >= 1)
                        {
                            _fullString = " ( " + _fullString1 + " ) AND ( " + _fullString2 + " ) ";
                        }
                        else if (_stringArrayCompanyList.Length >= 1 && _stringArrayWorkplaceList.Length == 0)
                        {
                            _fullString = _fullString1;
                        }

                        if (_fullString != null)
                        {
                            ListView.CollectionSource.Criteria["CashAdvanceLineFilter2"] = CriteriaOperator.Parse(_fullString);
                        }

                    }
                }
            }
            #endregion FilterBaseOnOrganization
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }
        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }
    }
}
