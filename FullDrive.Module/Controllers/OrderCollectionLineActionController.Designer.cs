﻿namespace FullDrive.Module.Controllers
{
    partial class OrderCollectionLineActionController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.OrderCollectionLineSelectAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.OrderCollectionLineUnselectAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.OrderCollectionLineCollectStatusAction = new DevExpress.ExpressApp.Actions.SingleChoiceAction(this.components);
            // 
            // OrderCollectionLineSelectAction
            // 
            this.OrderCollectionLineSelectAction.Caption = "Select";
            this.OrderCollectionLineSelectAction.ConfirmationMessage = null;
            this.OrderCollectionLineSelectAction.Id = "OrderCollectionLineSelectActionId";
            this.OrderCollectionLineSelectAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.OrderCollectionLine);
            this.OrderCollectionLineSelectAction.ToolTip = null;
            this.OrderCollectionLineSelectAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.OrderCollectionLineSelectAction_Execute);
            // 
            // OrderCollectionLineUnselectAction
            // 
            this.OrderCollectionLineUnselectAction.Caption = "Unselect";
            this.OrderCollectionLineUnselectAction.ConfirmationMessage = null;
            this.OrderCollectionLineUnselectAction.Id = "OrderCollectionLineUnselectActionId";
            this.OrderCollectionLineUnselectAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.OrderCollectionLine);
            this.OrderCollectionLineUnselectAction.ToolTip = null;
            this.OrderCollectionLineUnselectAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.OrderCollectionLineUnselectAction_Execute);
            // 
            // OrderCollectionLineCollectStatusAction
            // 
            this.OrderCollectionLineCollectStatusAction.Caption = "Collect Status";
            this.OrderCollectionLineCollectStatusAction.ConfirmationMessage = null;
            this.OrderCollectionLineCollectStatusAction.Id = "OrderCollectionLineCollectStatusActionId";
            this.OrderCollectionLineCollectStatusAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.OrderCollectionLine);
            this.OrderCollectionLineCollectStatusAction.ToolTip = null;
            this.OrderCollectionLineCollectStatusAction.Execute += new DevExpress.ExpressApp.Actions.SingleChoiceActionExecuteEventHandler(this.OrderCollectionLineCollectStatusAction_Execute);
            // 
            // OrderCollectionLineActionController
            // 
            this.Actions.Add(this.OrderCollectionLineSelectAction);
            this.Actions.Add(this.OrderCollectionLineUnselectAction);
            this.Actions.Add(this.OrderCollectionLineCollectStatusAction);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SimpleAction OrderCollectionLineSelectAction;
        private DevExpress.ExpressApp.Actions.SimpleAction OrderCollectionLineUnselectAction;
        private DevExpress.ExpressApp.Actions.SingleChoiceAction OrderCollectionLineCollectStatusAction;
    }
}
