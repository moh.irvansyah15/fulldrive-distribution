﻿namespace FullDrive.Module.Controllers
{
    partial class PayablePurchaseCollectionActionController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.PayablePurchaseCollectionShowPIAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.PayablePurchaseCollectionShowPPIAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            // 
            // PayablePurchaseCollectionShowPIAction
            // 
            this.PayablePurchaseCollectionShowPIAction.Caption = "Show PI";
            this.PayablePurchaseCollectionShowPIAction.ConfirmationMessage = null;
            this.PayablePurchaseCollectionShowPIAction.Id = "PayablePurchaseCollectionShowPIActionId";
            this.PayablePurchaseCollectionShowPIAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.PayablePurchaseCollection);
            this.PayablePurchaseCollectionShowPIAction.ToolTip = null;
            this.PayablePurchaseCollectionShowPIAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.PayablePurchaseCollectionShowPIAction_Execute);
            // 
            // PayablePurchaseCollectionShowPPIAction
            // 
            this.PayablePurchaseCollectionShowPPIAction.Caption = "Show PPI";
            this.PayablePurchaseCollectionShowPPIAction.ConfirmationMessage = null;
            this.PayablePurchaseCollectionShowPPIAction.Id = "PayablePurchaseCollectionShowPPIActionId";
            this.PayablePurchaseCollectionShowPPIAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.PayablePurchaseCollection);
            this.PayablePurchaseCollectionShowPPIAction.ToolTip = null;
            this.PayablePurchaseCollectionShowPPIAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.PayablePurchaseCollectionShowPPIAction_Execute);
            // 
            // PayablePurchaseCollectionActionController
            // 
            this.Actions.Add(this.PayablePurchaseCollectionShowPIAction);
            this.Actions.Add(this.PayablePurchaseCollectionShowPPIAction);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SimpleAction PayablePurchaseCollectionShowPIAction;
        private DevExpress.ExpressApp.Actions.SimpleAction PayablePurchaseCollectionShowPPIAction;
    }
}
