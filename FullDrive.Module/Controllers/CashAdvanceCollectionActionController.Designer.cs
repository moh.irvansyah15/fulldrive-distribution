﻿namespace FullDrive.Module.Controllers
{
    partial class CashAdvanceCollectionActionController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.CashAdvanceCollectionShowCAMAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            // 
            // CashAdvanceCollectionShowCAMAction
            // 
            this.CashAdvanceCollectionShowCAMAction.Caption = "Show CAM";
            this.CashAdvanceCollectionShowCAMAction.ConfirmationMessage = null;
            this.CashAdvanceCollectionShowCAMAction.Id = "CashAdvanceCollectionShowCAMActionId";
            this.CashAdvanceCollectionShowCAMAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.CashAdvanceCollection);
            this.CashAdvanceCollectionShowCAMAction.ToolTip = null;
            this.CashAdvanceCollectionShowCAMAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.CashAdvanceCollectionShowCAMAction_Execute);
            // 
            // CashAdvanceCollectionActionController
            // 
            this.Actions.Add(this.CashAdvanceCollectionShowCAMAction);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SimpleAction CashAdvanceCollectionShowCAMAction;
    }
}
