﻿namespace FullDrive.Module.Controllers
{
    partial class PayableTransactionActionController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.PayableTransactionGetPIAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.PayableTransactionGetPPIAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.PayableTransactionProgressAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.PayableTransactionPostingAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.PayableTransactionListviewFilterSelectionAction = new DevExpress.ExpressApp.Actions.SingleChoiceAction(this.components);
            // 
            // PayableTransactionGetPIAction
            // 
            this.PayableTransactionGetPIAction.Caption = "Get PI";
            this.PayableTransactionGetPIAction.ConfirmationMessage = null;
            this.PayableTransactionGetPIAction.Id = "PayableTransactionGetPIActionId";
            this.PayableTransactionGetPIAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.PayableTransaction);
            this.PayableTransactionGetPIAction.ToolTip = null;
            this.PayableTransactionGetPIAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.PayableTransactionGetPIAction_Execute);
            // 
            // PayableTransactionGetPPIAction
            // 
            this.PayableTransactionGetPPIAction.Caption = "Get PPI";
            this.PayableTransactionGetPPIAction.ConfirmationMessage = null;
            this.PayableTransactionGetPPIAction.Id = "PayableTransactionGetPPIActionId";
            this.PayableTransactionGetPPIAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.PayableTransaction);
            this.PayableTransactionGetPPIAction.ToolTip = null;
            this.PayableTransactionGetPPIAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.PayableTransactionGetPPIAction_Execute);
            // 
            // PayableTransactionProgressAction
            // 
            this.PayableTransactionProgressAction.Caption = "Progress";
            this.PayableTransactionProgressAction.ConfirmationMessage = null;
            this.PayableTransactionProgressAction.Id = "PayableTransactionProgressActionId";
            this.PayableTransactionProgressAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.PayableTransaction);
            this.PayableTransactionProgressAction.ToolTip = null;
            this.PayableTransactionProgressAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.PayableTransactionProgressAction_Execute);
            // 
            // PayableTransactionPostingAction
            // 
            this.PayableTransactionPostingAction.Caption = "Posting";
            this.PayableTransactionPostingAction.ConfirmationMessage = null;
            this.PayableTransactionPostingAction.Id = "PayableTransactionPostingActionId";
            this.PayableTransactionPostingAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.PayableTransaction);
            this.PayableTransactionPostingAction.ToolTip = null;
            this.PayableTransactionPostingAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.PayableTransactionPostingAction_Execute);
            // 
            // PayableTransactionListviewFilterSelectionAction
            // 
            this.PayableTransactionListviewFilterSelectionAction.Caption = "Filter";
            this.PayableTransactionListviewFilterSelectionAction.ConfirmationMessage = null;
            this.PayableTransactionListviewFilterSelectionAction.Id = "PayableTransactionListviewFilterSelectionActionId";
            this.PayableTransactionListviewFilterSelectionAction.ItemType = DevExpress.ExpressApp.Actions.SingleChoiceActionItemType.ItemIsOperation;
            this.PayableTransactionListviewFilterSelectionAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.PayableTransaction);
            this.PayableTransactionListviewFilterSelectionAction.ToolTip = null;
            this.PayableTransactionListviewFilterSelectionAction.Execute += new DevExpress.ExpressApp.Actions.SingleChoiceActionExecuteEventHandler(this.PayableTransactionListviewFilterSelectionAction_Execute);
            // 
            // PayableTransactionActionController
            // 
            this.Actions.Add(this.PayableTransactionGetPIAction);
            this.Actions.Add(this.PayableTransactionGetPPIAction);
            this.Actions.Add(this.PayableTransactionProgressAction);
            this.Actions.Add(this.PayableTransactionPostingAction);
            this.Actions.Add(this.PayableTransactionListviewFilterSelectionAction);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SimpleAction PayableTransactionGetPIAction;
        private DevExpress.ExpressApp.Actions.SimpleAction PayableTransactionGetPPIAction;
        private DevExpress.ExpressApp.Actions.SimpleAction PayableTransactionProgressAction;
        private DevExpress.ExpressApp.Actions.SimpleAction PayableTransactionPostingAction;
        private DevExpress.ExpressApp.Actions.SingleChoiceAction PayableTransactionListviewFilterSelectionAction;
    }
}
