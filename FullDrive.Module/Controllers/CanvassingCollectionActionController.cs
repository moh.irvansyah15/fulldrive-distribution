﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;

using FullDrive.Module.BusinessObjects;
using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.Xpo;
using System.Collections;
using DevExpress.Xpo;
using System.IO;
using System.Web;

namespace FullDrive.Module.Controllers
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class CanvassingCollectionActionController : ViewController
    {
        private ChoiceActionItem _selectionListviewFilter;

        public CanvassingCollectionActionController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.
            #region FilterStatus
            CanvassingCollectionListviewFilterSelectionAction.Items.Clear();
            foreach (object _currApproval in Enum.GetValues(typeof(CustomProcess.Status)))
            {
                EnumDescriptor _ed = new EnumDescriptor(typeof(CustomProcess.Status));
                _selectionListviewFilter = new ChoiceActionItem(_ed.GetCaption(_currApproval), _currApproval);
                CanvassingCollectionListviewFilterSelectionAction.Items.Add(_selectionListviewFilter);
            }
            #endregion FilterStatus
        }
        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }
        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        private void CanvassingCollectionListviewFilterSelectionAction_Execute(object sender, SingleChoiceActionExecuteEventArgs e)
        {
            try
            {
                if ((View is ListView) & (View.ObjectTypeInfo.Type == typeof(CanvassingCollection)))
                {
                    if ((Status)e.SelectedChoiceActionItem.Data == Status.None)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.None, BinaryOperatorType.NotEqual);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Open)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Open, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Progress)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Progress, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Posted)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Posted, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Close)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Close, BinaryOperatorType.Equal);
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = CanvassingCollection " + ex.ToString());
            }
        }

        private void CanvassingCollectionGetICAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;

                if (_objectsToProcess != null)
                {
                    foreach (Object obj in _objectsToProcess)
                    {
                        CanvassingCollection _locCanvassingCollectionOS = (CanvassingCollection)_objectSpace.GetObject(obj);

                        if (_locCanvassingCollectionOS != null)
                        {
                            if (_locCanvassingCollectionOS.Session != null)
                            {
                                _currSession = _locCanvassingCollectionOS.Session;
                            }

                            if (_locCanvassingCollectionOS.Code != null && _currSession != null)
                            {
                                _currObjectId = _locCanvassingCollectionOS.Code;

                                CanvassingCollection _locCanvassingCollectionXPO = _currSession.FindObject<CanvassingCollection>
                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                     new BinaryOperator("Code", _currObjectId)));

                                if (_locCanvassingCollectionXPO != null)
                                {
                                    if (_locCanvassingCollectionXPO.Status == Status.Open || _locCanvassingCollectionXPO.Status == Status.Progress)
                                    {
                                        XPCollection<InvoiceCanvassingCollection> _locInvoiceCanvassingCollections = new XPCollection<InvoiceCanvassingCollection>
                                                                                        (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                        new BinaryOperator("CanvassingCollection", _locCanvassingCollectionXPO),
                                                                                        new GroupOperator(GroupOperatorType.Or,
                                                                                        new BinaryOperator("Status", Status.Open),
                                                                                        new BinaryOperator("Status", Status.Progress))));

                                        if (_locInvoiceCanvassingCollections != null && _locInvoiceCanvassingCollections.Count() > 0)
                                        {
                                            foreach (InvoiceCanvassingCollection _locInvoiceCanvassingCollection in _locInvoiceCanvassingCollections)
                                            {
                                                if (_locInvoiceCanvassingCollection.InvoiceCanvassing != null)
                                                {
                                                    GetInvoiceCanvassingMonitoring(_currSession, _locInvoiceCanvassingCollection.InvoiceCanvassing, _locCanvassingCollectionXPO);
                                                }
                                            }
                                            SuccessMessageShow("InvoiceCanvassing Has Been Successfully Getting into CanvassingCollection");
                                        }
                                    }
                                }
                                else
                                {
                                    ErrorMessageShow("Data CanvassingCollection Not Available");
                                }
                            }
                            else
                            {
                                ErrorMessageShow("Data CanvassingCollection Not Available");
                            }
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = CanvassingCollection " + ex.ToString());
            }
        }

        private void CanvassingCollectionProgressAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;

                if (_objectsToProcess != null)
                {
                    foreach (Object obj in _objectsToProcess)
                    {
                        CanvassingCollection _locCanvassingCollectionOS = (CanvassingCollection)_objectSpace.GetObject(obj);

                        if (_locCanvassingCollectionOS != null)
                        {
                            if (_locCanvassingCollectionOS.Session != null)
                            {
                                _currSession = _locCanvassingCollectionOS.Session;
                            }

                            if (_locCanvassingCollectionOS.Code != null && _currSession != null)
                            {
                                _currObjectId = _locCanvassingCollectionOS.Code;

                                CanvassingCollection _locCanvassingCollectionXPO = _currSession.FindObject<CanvassingCollection>
                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                     new BinaryOperator("Code", _currObjectId)));

                                if (_locCanvassingCollectionXPO != null)
                                {
                                    if (_locCanvassingCollectionXPO.Status == Status.Open || _locCanvassingCollectionXPO.Status == Status.Progress 
                                        || _locCanvassingCollectionXPO.Status == Status.Posted)
                                    {
                                        if (_locCanvassingCollectionXPO.Status == Status.Open)
                                        {
                                            _locCanvassingCollectionXPO.Status = Status.Progress;
                                            _locCanvassingCollectionXPO.StatusDate = now;
                                            _locCanvassingCollectionXPO.Save();
                                            _locCanvassingCollectionXPO.Session.CommitTransaction();
                                        }
                                        #region CanvassingCollectionLine
                                        XPCollection<CanvassingCollectionLine> _locCanvassingCollectionLines = new XPCollection<CanvassingCollectionLine>
                                                               (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("CanvassingCollection", _locCanvassingCollectionXPO),
                                                                new BinaryOperator("Status", Status.Open)));

                                        if (_locCanvassingCollectionLines != null && _locCanvassingCollectionLines.Count > 0)
                                        {
                                            foreach (CanvassingCollectionLine _locCanvassingCollectionLine in _locCanvassingCollectionLines)
                                            {
                                                _locCanvassingCollectionLine.Status = Status.Progress;
                                                _locCanvassingCollectionLine.StatusDate = now;
                                                _locCanvassingCollectionLine.Save();
                                                _locCanvassingCollectionLine.Session.CommitTransaction();
                                            }
                                        }
                                        #endregion CanvassingCollectionLine
                                        #region CanvassingCollectionFreeItem
                                        XPCollection<CanvassingCollectionFreeItem> _locCanvassingCollectionFreeItems = new XPCollection<CanvassingCollectionFreeItem>
                                                               (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("CanvassingCollection", _locCanvassingCollectionXPO),
                                                                new BinaryOperator("Status", Status.Open)));

                                        if (_locCanvassingCollectionFreeItems != null && _locCanvassingCollectionFreeItems.Count > 0)
                                        {
                                            foreach (CanvassingCollectionFreeItem _locCanvassingCollectionFreeItem in _locCanvassingCollectionFreeItems)
                                            {
                                                _locCanvassingCollectionFreeItem.Status = Status.Progress;
                                                _locCanvassingCollectionFreeItem.StatusDate = now;
                                                _locCanvassingCollectionFreeItem.Save();
                                                _locCanvassingCollectionFreeItem.Session.CommitTransaction();
                                            }
                                        }
                                        #endregion CanvassingCollectionFreeItem

                                        SetTotalAmountAndTaxAmountAndDiscountAmount(_currSession, _locCanvassingCollectionXPO);

                                    }
                                    SuccessMessageShow("CanvassingCollection has been successfully updated to progress");
                                }
                                else
                                {
                                    ErrorMessageShow("Data CanvassingCollection Not Available");
                                }

                            }
                            else
                            {
                                ErrorMessageShow("Data CanvassingCollection Not Available");
                            }
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PurchaseOrder " + ex.ToString());
            }
        }

        private void CanvassingCollectionPostingAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {

                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;

                if (_objectsToProcess != null)
                {
                    foreach (Object obj in _objectsToProcess)
                    {
                        CanvassingCollection _locCanvassingCollectionOS = (CanvassingCollection)_objectSpace.GetObject(obj);

                        if (_locCanvassingCollectionOS != null)
                        {
                            if (_locCanvassingCollectionOS.Session != null)
                            {
                                _currSession = _locCanvassingCollectionOS.Session;
                            }

                            if (_locCanvassingCollectionOS.Code != null && _currSession != null)
                            {
                                _currObjectId = _locCanvassingCollectionOS.Code;

                                CanvassingCollection _locCanvassingCollectionXPO = _currSession.FindObject<CanvassingCollection>
                                                                   (new GroupOperator(GroupOperatorType.And,
                                                                    new BinaryOperator("Code", _currObjectId)));

                                if (_locCanvassingCollectionXPO != null)
                                {
                                    if (_locCanvassingCollectionXPO.Status == Status.Progress || _locCanvassingCollectionXPO.Status == Status.Posted)
                                    {
                                        if (_locCanvassingCollectionXPO.TotAmount > 0)
                                        {
                                            //Berdasarkan Outstanding dari PaymentOutPlan yg mana di hitung berdasarkan jumlah mak Outstanding 
                                            if (_locCanvassingCollectionXPO.TotAmount == GetCanvassingCollectionLineTotalAmount(_currSession, _locCanvassingCollectionXPO))
                                            {
                                                SetCanvassingCollectionMonitoring(_currSession, _locCanvassingCollectionXPO);
                                                SetCanvasPaymentIn(_currSession, _locCanvassingCollectionXPO);
                                                SetStatusCanvassingCollectionLine(_currSession, _locCanvassingCollectionXPO);
                                                SetStatusCanvassingCollectionFreeItem(_currSession, _locCanvassingCollectionXPO);
                                                SetFinalStatusCanvassingCollection(_currSession, _locCanvassingCollectionXPO);
                                                SetFinalStatusInvoiceCanvassing(_currSession, _locCanvassingCollectionXPO);
                                                SuccessMessageShow(_locCanvassingCollectionXPO.Code + " has been change successfully to Posted");
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    ErrorMessageShow("Data CanvassingCollection Not Available");
                                }
                            }
                            else
                            {
                                ErrorMessageShow("Data CanvassingCollection Not Available");
                            }
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = InvoiceCanvassing " + ex.ToString());
            }
        }

        //====================================================================== Code Only ===========================================================================

        #region GetIC

        private void GetInvoiceCanvassingMonitoring(Session _currSession, InvoiceCanvassing _locInvoiceCanvassingXPO, CanvassingCollection _locCanvassingCollectionXPO)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                DateTime now = DateTime.Now;
                string _currSignCode = null;

                if (_locInvoiceCanvassingXPO != null && _locCanvassingCollectionXPO != null)
                {
                    #region InvoiceCanvassingMonitoring
                    //Hanya memindahkan PurchaseRequisitionMonitoring ke PurchaseOrderLine
                    XPCollection<InvoiceCanvassingMonitoring> _locInvoiceCanvassingMonitorings = new XPCollection<InvoiceCanvassingMonitoring>
                                                                                        (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                        new BinaryOperator("InvoiceCanvassing", _locInvoiceCanvassingXPO),
                                                                                        new BinaryOperator("CanvassingCollection", _locCanvassingCollectionXPO),
                                                                                        new BinaryOperator("Select", true),
                                                                                        new BinaryOperator("Status", Status.Close, BinaryOperatorType.NotEqual)));

                    if (_locInvoiceCanvassingMonitorings != null && _locInvoiceCanvassingMonitorings.Count() > 0)
                    {
                        foreach (InvoiceCanvassingMonitoring _locInvoiceCanvassingMonitoring in _locInvoiceCanvassingMonitorings)
                        {
                            if (_locInvoiceCanvassingMonitoring.InvoiceCanvassingLine != null)
                            {
                                _currSignCode = _globFunc.GetNumberingSignUnlockOptimisticRecordBasedOrganization(_currSession.DataLayer, ObjectList.CanvassingCollectionLine, _locCanvassingCollectionXPO.Company, _locCanvassingCollectionXPO.Workplace);
                                
                                if (_currSignCode != null)
                                {
                                    #region CanvassingCollectionLine
                                    CanvassingCollectionLine _saveDataCanvassingCollectionLine = new CanvassingCollectionLine(_currSession)
                                    {
                                        SignCode = _currSignCode,
                                        FreeItemChecked = _locInvoiceCanvassingMonitoring.FreeItemChecked,
                                        Company = _locInvoiceCanvassingMonitoring.Company,
                                        Workplace = _locInvoiceCanvassingMonitoring.Workplace,
                                        Division = _locInvoiceCanvassingMonitoring.Division,
                                        Department = _locInvoiceCanvassingMonitoring.Department,
                                        Section = _locInvoiceCanvassingMonitoring.Section,
                                        Employee = _locInvoiceCanvassingMonitoring.Employee,
                                        Salesman = _locInvoiceCanvassingMonitoring.Salesman,
                                        Vehicle = _locInvoiceCanvassingMonitoring.Vehicle,
                                        BeginningInventory = _locInvoiceCanvassingMonitoring.BeginningInventory,
                                        Item = _locInvoiceCanvassingMonitoring.Item,
                                        DQty = _locInvoiceCanvassingMonitoring.DQty,
                                        DUOM = _locInvoiceCanvassingMonitoring.DUOM,
                                        Qty = _locInvoiceCanvassingMonitoring.Qty,
                                        UOM = _locInvoiceCanvassingMonitoring.UOM,
                                        TQty = _locInvoiceCanvassingMonitoring.TQty,
                                        StockType = _locInvoiceCanvassingMonitoring.StockType,
                                        Currency = _locInvoiceCanvassingMonitoring.Currency,
                                        PriceGroup = _locInvoiceCanvassingMonitoring.PriceGroup,
                                        PriceLine = _locInvoiceCanvassingMonitoring.PriceLine,
                                        UAmount = _locInvoiceCanvassingMonitoring.UAmount,
                                        TUAmount = _locInvoiceCanvassingMonitoring.TUAmount,
                                        Tax = _locInvoiceCanvassingMonitoring.Tax,
                                        TxValue = _locInvoiceCanvassingMonitoring.TxValue,
                                        TxAmount = _locInvoiceCanvassingMonitoring.TxAmount,
                                        DiscountRule = _locInvoiceCanvassingMonitoring.DiscountRule,
                                        DiscAmount = _locInvoiceCanvassingMonitoring.DiscAmount,
                                        TAmount = _locInvoiceCanvassingMonitoring.TAmount,
                                        InvoiceCanvassingMonitoring = _locInvoiceCanvassingMonitoring,
                                        CanvassingCollection = _locCanvassingCollectionXPO,
                                    };
                                    _saveDataCanvassingCollectionLine.Save();
                                    _saveDataCanvassingCollectionLine.Session.CommitTransaction();
                                    #endregion CanvassingCollectionLine

                                    CanvassingCollectionLine _locCanvassingCollectionLine = _currSession.FindObject<CanvassingCollectionLine>
                                                                (new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("SignCode", _currSignCode)));
                                    if (_locCanvassingCollectionLine != null)
                                    {
                                        SetStatusInvoiceCanvassingMonitoring(_currSession, _locInvoiceCanvassingMonitoring, _locCanvassingCollectionLine);
                                    }
                                }
                            }
                        }
                    }
                    #endregion InvoiceCanvassingMonitoring
                    #region InvoiceCanvassingFreeItem
                    XPCollection<InvoiceCanvassingFreeItem> _locInvoiceCanvassingFreeItems = new XPCollection<InvoiceCanvassingFreeItem>
                                                                                        (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                        new BinaryOperator("InvoiceCanvassing", _locInvoiceCanvassingXPO)));
                    if(_locInvoiceCanvassingFreeItems != null && _locInvoiceCanvassingFreeItems.Count() > 0)
                    {
                        foreach(InvoiceCanvassingFreeItem _locInvoiceCanvassingFreeItem in _locInvoiceCanvassingFreeItems)
                        {
                            CanvassingCollectionFreeItem _saveDataCanvassingCollectionFreeItem = new CanvassingCollectionFreeItem(_currSession)
                            {
                                Company = _locInvoiceCanvassingFreeItem.Company,
                                Workplace = _locInvoiceCanvassingFreeItem.Workplace,
                                Division = _locInvoiceCanvassingFreeItem.Division,
                                Department = _locInvoiceCanvassingFreeItem.Department,
                                Section = _locInvoiceCanvassingFreeItem.Section,
                                Employee = _locInvoiceCanvassingFreeItem.Employee,
                                InvoiceCanvassingLine = _locInvoiceCanvassingFreeItem.InvoiceCanvassingLine,
                                FpItem = _locInvoiceCanvassingFreeItem.FpItem,
                                FpDQty = _locInvoiceCanvassingFreeItem.FpDQty,
                                FpDUOM = _locInvoiceCanvassingFreeItem.FpDUOM,
                                FpQty = _locInvoiceCanvassingFreeItem.FpQty,
                                FpUOM = _locInvoiceCanvassingFreeItem.FpUOM,
                                FpTQty = _locInvoiceCanvassingFreeItem.FpTQty,
                                FpStockType = _locInvoiceCanvassingFreeItem.FpStockType,
                                CanvassingCollection = _locCanvassingCollectionXPO,
                            };
                            _saveDataCanvassingCollectionFreeItem.Save();
                            _saveDataCanvassingCollectionFreeItem.Session.CommitTransaction();
                        }
                    }
                    #endregion InvoiceCanvassingFreeItem
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = CanvassingCollection ", ex.ToString());
            }
        }

        private void SetStatusInvoiceCanvassingMonitoring(Session _currSession, InvoiceCanvassingMonitoring _locInvoiceCanvassingMonitoringXPO, CanvassingCollectionLine _locCanvassingCollectionLineXPO)
        {
            try
            {
                DateTime now = DateTime.Now;
                if (_locInvoiceCanvassingMonitoringXPO != null)
                {
                    if (_locInvoiceCanvassingMonitoringXPO.Status == Status.Open || _locInvoiceCanvassingMonitoringXPO.Status == Status.Progress)
                    {
                        _locInvoiceCanvassingMonitoringXPO.ActivationPosting = true;
                        _locInvoiceCanvassingMonitoringXPO.Select = false;
                        _locInvoiceCanvassingMonitoringXPO.CanvassingCollectionLine = _locCanvassingCollectionLineXPO;
                        _locInvoiceCanvassingMonitoringXPO.PostedCount = _locInvoiceCanvassingMonitoringXPO.PostedCount + 1;
                        _locInvoiceCanvassingMonitoringXPO.Status = Status.Posted;
                        _locInvoiceCanvassingMonitoringXPO.StatusDate = now;
                        _locInvoiceCanvassingMonitoringXPO.Save();
                        _locInvoiceCanvassingMonitoringXPO.Session.CommitTransaction();
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = CanvassingCollection " + ex.ToString());
            }
        }

        #endregion GetIC

        #region GetAmount

        private void SetTotalAmountAndTaxAmountAndDiscountAmount(Session _currSession, CanvassingCollection _locCanvassingCollectionXPO)
        {
            try
            {
                double _locTAmount = 0;
                double _locDiscAmountRule2 = 0;
                double _locTxAmount = 0;
                double _locDiscAmountRule1 = 0;
                if (_locCanvassingCollectionXPO != null)
                {
                    XPCollection<CanvassingCollectionLine> _locCanvassingCollectionLines = new XPCollection<BusinessObjects.CanvassingCollectionLine>
                                                                                (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                new BinaryOperator("CanvassingCollection", _locCanvassingCollectionXPO),
                                                                                new GroupOperator(GroupOperatorType.Or,
                                                                                new BinaryOperator("Status", Status.Open),
                                                                                new BinaryOperator("Status", Status.Progress))));
                    if (_locCanvassingCollectionLines != null && _locCanvassingCollectionLines.Count() > 0)
                    {
                        foreach (CanvassingCollectionLine _locCanvassingCollectionLine in _locCanvassingCollectionLines)
                        {
                            _locTAmount = _locTAmount + _locCanvassingCollectionLine.TAmount;
                            _locDiscAmountRule2 = _locDiscAmountRule2 + _locCanvassingCollectionLine.DiscAmount;
                            if (_locCanvassingCollectionLine.Tax != null)
                            {
                                if (_locCanvassingCollectionLine.Tax.TaxNature == TaxNature.Increase)
                                {
                                    _locTxAmount = _locTxAmount + _locCanvassingCollectionLine.TxAmount;
                                }
                                if (_locCanvassingCollectionLine.Tax.TaxNature == TaxNature.Decrease)
                                {
                                    _locTxAmount = _locTxAmount - _locCanvassingCollectionLine.TxAmount;
                                }
                            }
                        }

                        _locDiscAmountRule1 = GetDiscountRule1(_currSession, _locCanvassingCollectionXPO);
                    }

                    _locCanvassingCollectionXPO.TotAmount = _locTAmount;
                    _locCanvassingCollectionXPO.TotDiscAmount = _locDiscAmountRule2 + _locDiscAmountRule1;
                    _locCanvassingCollectionXPO.TotTaxAmount = _locTxAmount;
                    _locCanvassingCollectionXPO.Amount = _locTAmount - _locDiscAmountRule1;
                    _locCanvassingCollectionXPO.TotCollectedAmount = _locTAmount - _locDiscAmountRule1;
                    _locCanvassingCollectionXPO.Save();
                    _locCanvassingCollectionXPO.Session.CommitTransaction();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = CanvassingCollection ", ex.ToString());
            }

        }

        #endregion GetAmount

        #region Discount

        private double GetDiscountRule1(Session _currSession, CanvassingCollection _locCanvassingCollectionXPO)
        {
            double _result = 0;
            try
            {
                if (_locCanvassingCollectionXPO != null)
                {
                    //Biking Group By InvoiceCanvassing

                    XPQuery<CanvassingCollectionLine> _canvassingCollectionLinesQuery = new XPQuery<CanvassingCollectionLine>(_currSession);

                    var _canvassingCollectionLines = from ccl in _canvassingCollectionLinesQuery
                                                  where (ccl.Company == _locCanvassingCollectionXPO.Company
                                                  && ccl.Workplace == _locCanvassingCollectionXPO.Workplace
                                                  && ccl.Salesman == _locCanvassingCollectionXPO.Salesman
                                                  && ccl.CanvassingCollection == _locCanvassingCollectionXPO
                                                  )
                                                  group ccl by ccl.InvoiceCanvassingMonitoring.InvoiceCanvassing into g
                                                  select new { InvoiceCanvassing = g.Key };

                    if(_canvassingCollectionLines != null && _canvassingCollectionLines.Count() > 0)
                    {
                        double _locTUAmount = 0;
                        double _locTAmount = 0;
                        double _locTUAmountDiscount = 0;
                        double _totResult = 0;

                        foreach (var _canvassingCollectionLine in _canvassingCollectionLines)
                        {
                            if(_canvassingCollectionLine.InvoiceCanvassing != null)
                            {
                                DiscountRule _locDiscountRule1 = _currSession.FindObject<DiscountRule>
                                                            (new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("Company", _locCanvassingCollectionXPO.Company),
                                                            new BinaryOperator("Workplace", _locCanvassingCollectionXPO.Workplace),
                                                            new BinaryOperator("PriceGroup", _locCanvassingCollectionXPO.PriceGroup),
                                                            new BinaryOperator("RuleType", RuleType.Rule1),
                                                            new BinaryOperator("Active", true)));

                                #region Rule1
                                //Grand Total Amount Per Invoice
                                if (_locDiscountRule1 != null)
                                {
                                    #region Gross
                                    if (_locDiscountRule1.GrossAmountRule == true)
                                    {
                                        XPCollection<CanvassingCollectionLine> _locMainCanvassingCollectionLines = new XPCollection<CanvassingCollectionLine>
                                                                                                       (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                       new BinaryOperator("CanvassingCollection", _locCanvassingCollectionXPO),
                                                                                                       new BinaryOperator("InvoiceCanvassingMonitoring.InvoiceCanvassing", _canvassingCollectionLine.InvoiceCanvassing),
                                                                                                       new GroupOperator(GroupOperatorType.Or,
                                                                                                       new BinaryOperator("Status", Status.Open),
                                                                                                       new BinaryOperator("Status", Status.Progress))));


                                        if (_locMainCanvassingCollectionLines != null && _locMainCanvassingCollectionLines.Count() > 0)
                                        {
                                            foreach (CanvassingCollectionLine _locMainCanvassingCollectionLine in _locMainCanvassingCollectionLines)
                                            {
                                                if (_locMainCanvassingCollectionLine.TUAmount > 0)
                                                {
                                                    _locTUAmount = _locTUAmount + _locMainCanvassingCollectionLine.TUAmount;
                                                    _locMainCanvassingCollectionLine.DiscountChecked = true;
                                                    _locMainCanvassingCollectionLine.Save();
                                                    _locMainCanvassingCollectionLine.Session.CommitTransaction();
                                                }
                                            }

                                            if (_locTUAmount > 0 && _locTUAmount >= _locDiscountRule1.GrossTotalUAmount)
                                            {
                                                if (_locDiscountRule1.DiscountType == DiscountType.Percentage)
                                                {
                                                    _locTUAmountDiscount = _locTUAmount * _locDiscountRule1.Value / 100;
                                                }
                                                if (_locDiscountRule1.DiscountType == DiscountType.Value)
                                                {
                                                    _locTUAmountDiscount = _locDiscountRule1.Value;
                                                }
                                            }
                                        }

                                    }
                                    #endregion Gross
                                    #region Net
                                    if (_locDiscountRule1.NetAmountRule == true)
                                    {
                                        XPCollection<CanvassingCollectionLine> _locMainCanvassingCollectionLines = new XPCollection<CanvassingCollectionLine>
                                                                                                       (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                       new BinaryOperator("CanvassingCollection", _locCanvassingCollectionXPO),
                                                                                                       new BinaryOperator("InvoiceCanvassingMonitoring.InvoiceCanvassing", _canvassingCollectionLine.InvoiceCanvassing),
                                                                                                       new GroupOperator(GroupOperatorType.Or,
                                                                                                       new BinaryOperator("Status", Status.Open),
                                                                                                       new BinaryOperator("Status", Status.Progress))));

                                        if (_locMainCanvassingCollectionLines != null && _locMainCanvassingCollectionLines.Count() > 0)
                                        {
                                            foreach (CanvassingCollectionLine _locMainCanvassingCollectionLine in _locMainCanvassingCollectionLines)
                                            {
                                                if (_locMainCanvassingCollectionLine.TAmount > 0)
                                                {
                                                    _locTAmount = _locTAmount + _locMainCanvassingCollectionLine.TAmount;
                                                    _locMainCanvassingCollectionLine.DiscountChecked = true;
                                                    _locMainCanvassingCollectionLine.Save();
                                                    _locMainCanvassingCollectionLine.Session.CommitTransaction();
                                                }
                                            }

                                            if (_locTAmount > 0 && _locTAmount >= _locDiscountRule1.NetTotalUAmount)
                                            {
                                                if (_locDiscountRule1.DiscountType == DiscountType.Percentage)
                                                {
                                                    _locTUAmountDiscount = _locTAmount * _locDiscountRule1.Value / 100;
                                                }
                                                if (_locDiscountRule1.DiscountType == DiscountType.Value)
                                                {
                                                    _locTUAmountDiscount = _locDiscountRule1.Value;
                                                }
                                            }
                                        }
                                    }
                                    #endregion Net
                                }
                                #endregion Rule1

                                _totResult = _totResult + _locTUAmountDiscount;
                                _locTUAmountDiscount = 0;
                                _locTUAmount = 0;
                                _locTAmount = 0;
                            }

                            _result = _totResult;
                        }
                    } 
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = CanvassingCollection ", ex.ToString());
            }
            return _result;
        }

        #endregion Discount

        #region Posting Method

        private double GetCanvassingCollectionLineTotalAmount(Session _currSession, CanvassingCollection _locCanvassingCollectionXPO)
        {
            double _return = 0;
            try
            {
                if (_locCanvassingCollectionXPO != null)
                {
                    double _locTAmount = 0;
                    XPCollection<CanvassingCollectionLine> _locCanvassingCollectionLines = new XPCollection<CanvassingCollectionLine>
                                                                                               (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                new BinaryOperator("CanvassingCollection", _locCanvassingCollectionXPO),
                                                                                                new GroupOperator(GroupOperatorType.Or,
                                                                                                new BinaryOperator("Status", Status.Progress),
                                                                                                new BinaryOperator("Status", Status.Posted))));

                    if (_locCanvassingCollectionLines != null && _locCanvassingCollectionLines.Count > 0)
                    {
                        foreach (CanvassingCollectionLine _locCanvassingCollectionLine in _locCanvassingCollectionLines)
                        {
                            _locTAmount = _locTAmount + _locCanvassingCollectionLine.TAmount;
                        }
                        _return = _locTAmount;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("BusinessObject = CanvassingCollection" + ex.ToString());
            }
            return _return;
        }

        private void SetCanvassingCollectionMonitoring(Session _currSession, CanvassingCollection _locCanvassingCollectionXPO)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                DateTime now = DateTime.Now;

                if (_locCanvassingCollectionXPO != null)
                {
                    if (_locCanvassingCollectionXPO.Status == Status.Progress || _locCanvassingCollectionXPO.Status == Status.Posted)
                    {
                        XPCollection<CanvassingCollectionLine> _locCanvassingCollectionLines = new XPCollection<CanvassingCollectionLine>
                                                            (_currSession, new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("CanvassingCollection", _locCanvassingCollectionXPO),
                                                            new GroupOperator(GroupOperatorType.Or,
                                                            new BinaryOperator("Status", Status.Progress),
                                                            new BinaryOperator("Status", Status.Posted))));

                        if (_locCanvassingCollectionLines != null && _locCanvassingCollectionLines.Count() > 0)
                        {
                            foreach (CanvassingCollectionLine _locCanvassingCollectionLine in _locCanvassingCollectionLines)
                            {
                                if (_locCanvassingCollectionLine.TAmount > 0)
                                {
                                    CanvassingCollectionMonitoring _saveDataCanvassingCollectionMonitoring = new CanvassingCollectionMonitoring(_currSession)
                                    {
                                        FreeItemChecked = _locCanvassingCollectionLine.FreeItemChecked,
                                        DiscountChecked = _locCanvassingCollectionLine.DiscountChecked,
                                        Company = _locCanvassingCollectionLine.Company,
                                        Workplace = _locCanvassingCollectionLine.Workplace,
                                        Division = _locCanvassingCollectionLine.Division,
                                        Department = _locCanvassingCollectionLine.Department,
                                        Section = _locCanvassingCollectionLine.Section,
                                        Employee = _locCanvassingCollectionLine.Employee,
                                        Salesman = _locCanvassingCollectionLine.Salesman,
                                        Vehicle = _locCanvassingCollectionLine.Vehicle,
                                        BeginningInventory = _locCanvassingCollectionLine.BeginningInventory,
                                        Item = _locCanvassingCollectionLine.Item,
                                        DQty = _locCanvassingCollectionLine.DQty,
                                        DUOM = _locCanvassingCollectionLine.DUOM,
                                        Qty = _locCanvassingCollectionLine.Qty,
                                        UOM = _locCanvassingCollectionLine.UOM,
                                        TQty = _locCanvassingCollectionLine.TQty,
                                        StockType = _locCanvassingCollectionLine.StockType,
                                        Currency = _locCanvassingCollectionLine.Currency,
                                        PriceGroup = _locCanvassingCollectionLine.PriceGroup,
                                        PriceLine = _locCanvassingCollectionLine.PriceLine,
                                        UAmount = _locCanvassingCollectionLine.UAmount,
                                        TUAmount = _locCanvassingCollectionLine.TUAmount,
                                        Tax = _locCanvassingCollectionLine.Tax,
                                        TxValue = _locCanvassingCollectionLine.TxValue,
                                        TxAmount = _locCanvassingCollectionLine.TxAmount,
                                        DiscountRule = _locCanvassingCollectionLine.DiscountRule,
                                        DiscAmount = _locCanvassingCollectionLine.DiscAmount,
                                        TAmount = _locCanvassingCollectionLine.TAmount,
                                        CanvassingCollection = _locCanvassingCollectionXPO,
                                        CanvassingCollectionLine = _locCanvassingCollectionLine,
                                    };
                                    _saveDataCanvassingCollectionMonitoring.Save();
                                    _saveDataCanvassingCollectionMonitoring.Session.CommitTransaction();
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = CanvassingCollection ", ex.ToString());
            }
        }

        private void SetCanvasPaymentIn(Session _currSession, CanvassingCollection _locCanvassingCollectionXPO)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                DateTime now = DateTime.Now;
                double _locTotCollectionAmount = 0;

                if (_locCanvassingCollectionXPO != null)
                {
                    _locTotCollectionAmount = _locCanvassingCollectionXPO.TotCollectedAmount;

                    XPCollection<InvoiceCanvassingCollection> _locInvoiceCanvassingCollections = new XPCollection<InvoiceCanvassingCollection>
                                                                                (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                new BinaryOperator("CanvassingCollection", _locCanvassingCollectionXPO)));
                    if(_locInvoiceCanvassingCollections != null && _locInvoiceCanvassingCollections.Count() > 0)
                    {
                        double _locReceived = 0;
                        
                        foreach(InvoiceCanvassingCollection _locInvoiceCanvassingCollection in _locInvoiceCanvassingCollections)
                        {
                            if(_locInvoiceCanvassingCollection.InvoiceCanvassing != null)
                            {
                                CanvasPaymentIn _locCanvasPaymentIn = _currSession.FindObject<CanvasPaymentIn>
                                                    (new GroupOperator(GroupOperatorType.And,
                                                    new BinaryOperator("Company", _locCanvassingCollectionXPO.Company),
                                                    new BinaryOperator("Workplace", _locCanvassingCollectionXPO.Workplace),
                                                    new BinaryOperator("Salesman", _locCanvassingCollectionXPO.Salesman),
                                                    new BinaryOperator("InvoiceCanvassing", _locInvoiceCanvassingCollection.InvoiceCanvassing)));

                                if (_locCanvasPaymentIn != null)
                                {
                                    
                                    if (_locCanvasPaymentIn.Outstanding == 0)
                                    {
                                        if(_locTotCollectionAmount >= _locCanvasPaymentIn.Plan)
                                        {
                                            _locReceived = _locCanvasPaymentIn.Plan;
                                            _locTotCollectionAmount = _locTotCollectionAmount - _locReceived;
                                        }
                                        else
                                        {
                                            _locReceived = _locTotCollectionAmount;
                                        }

                                        _locCanvasPaymentIn.Received = _locReceived;
                                        _locCanvasPaymentIn.Outstanding = _locCanvasPaymentIn.Plan - _locReceived;
                                        _locCanvasPaymentIn.CanvassingCollection = _locCanvassingCollectionXPO;
                                        _locCanvasPaymentIn.Save();
                                        _locCanvasPaymentIn.Session.CommitTransaction();
                                    }else
                                    {
                                        if(_locTotCollectionAmount >= _locCanvasPaymentIn.Outstanding)
                                        {
                                            _locReceived = _locCanvasPaymentIn.Outstanding;
                                            _locTotCollectionAmount = _locTotCollectionAmount - _locCanvasPaymentIn.Outstanding;  
                                        }
                                        else
                                        {
                                            if(_locTotCollectionAmount < _locCanvasPaymentIn.Outstanding)
                                            {
                                                if(_locTotCollectionAmount + _locCanvasPaymentIn.Received < _locCanvasPaymentIn.Outstanding)
                                                {
                                                    _locReceived = _locTotCollectionAmount + _locCanvasPaymentIn.Received;
                                                    _locTotCollectionAmount = _locCanvasPaymentIn.Outstanding - (_locTotCollectionAmount + _locCanvasPaymentIn.Received);

                                                }
                                                else if (_locTotCollectionAmount + _locCanvasPaymentIn.Received >= _locCanvasPaymentIn.Outstanding)
                                                {
                                                    _locReceived = _locCanvasPaymentIn.Outstanding + _locCanvasPaymentIn.Received;
                                                    _locTotCollectionAmount = _locTotCollectionAmount + _locCanvasPaymentIn.Received - _locCanvasPaymentIn.Outstanding;
                                                }
                                            } 
                                        }
                                        _locCanvasPaymentIn.Received = _locReceived;
                                        _locCanvasPaymentIn.Outstanding = _locCanvasPaymentIn.Outstanding - _locReceived;
                                        _locCanvasPaymentIn.CanvassingCollection = _locCanvassingCollectionXPO;
                                        _locCanvasPaymentIn.Save();
                                        _locCanvasPaymentIn.Session.CommitTransaction();
                                    }

                                    if(_locCanvasPaymentIn.Plan == _locCanvasPaymentIn.Received)
                                    {
                                        _locCanvasPaymentIn.Status = Status.Close;
                                        _locCanvasPaymentIn.StatusDate = now;
                                        _locCanvasPaymentIn.Save();
                                        _locCanvasPaymentIn.Session.CommitTransaction();
                                    }
                                }

                                
                            }
                            _locReceived = 0;
                        }
                        _locTotCollectionAmount = 0;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = CanvassingCollection ", ex.ToString());
            }
        }

        private void SetStatusCanvassingCollectionLine(Session _currSession, CanvassingCollection _locCanvassingCollectionXPO)
        {
            try
            {
                DateTime now = DateTime.Now;
                if (_locCanvassingCollectionXPO != null)
                {
                    XPCollection<CanvassingCollectionLine> _locCanvassingCollectionLines = new XPCollection<CanvassingCollectionLine>(_currSession,
                                                            new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("CanvassingCollection", _locCanvassingCollectionXPO),
                                                            new GroupOperator(GroupOperatorType.Or,
                                                            new BinaryOperator("Status", Status.Progress),
                                                            new BinaryOperator("Status", Status.Posted))));

                    if (_locCanvassingCollectionLines != null && _locCanvassingCollectionLines.Count > 0)
                    {

                        foreach (CanvassingCollectionLine _locCanvassingCollectionLine in _locCanvassingCollectionLines)
                        {
                            _locCanvassingCollectionLine.Status = Status.Close;
                            _locCanvassingCollectionLine.ActivationPosting = true;
                            _locCanvassingCollectionLine.StatusDate = now;
                            _locCanvassingCollectionLine.Save();
                            _locCanvassingCollectionLine.Session.CommitTransaction();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = CanvassingCollection " + ex.ToString());
            }
        }

        private void SetStatusCanvassingCollectionFreeItem(Session _currSession, CanvassingCollection _locCanvassingCollectionXPO)
        {
            try
            {
                DateTime now = DateTime.Now;
                if (_locCanvassingCollectionXPO != null)
                {
                    XPCollection<CanvassingCollectionFreeItem> _locCanvassingCollectionFreeItems = new XPCollection<CanvassingCollectionFreeItem>(_currSession,
                                                            new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("CanvassingCollection", _locCanvassingCollectionXPO),
                                                            new GroupOperator(GroupOperatorType.Or,
                                                            new BinaryOperator("Status", Status.Open),
                                                            new BinaryOperator("Status", Status.Progress),
                                                            new BinaryOperator("Status", Status.Posted))));

                    if (_locCanvassingCollectionFreeItems != null && _locCanvassingCollectionFreeItems.Count > 0)
                    {

                        foreach (CanvassingCollectionFreeItem _locCanvassingCollectionFreeItem in _locCanvassingCollectionFreeItems)
                        {
                            _locCanvassingCollectionFreeItem.Status = Status.Close;
                            _locCanvassingCollectionFreeItem.StatusDate = now;
                            _locCanvassingCollectionFreeItem.Save();
                            _locCanvassingCollectionFreeItem.Session.CommitTransaction();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = CanvassingCollection " + ex.ToString());
            }
        }

        private void SetFinalStatusCanvassingCollection(Session _currSession, CanvassingCollection _locCanvassingCollectionXPO)
        {
            try
            {
                DateTime now = DateTime.Now;
                int _locStatusCount = 0;
                int _locInvCanvLineCount = 0;

                if (_locCanvassingCollectionXPO != null)
                {

                    XPCollection<CanvassingCollectionLine> _locCanvassingCollectionLines = new XPCollection<CanvassingCollectionLine>(_currSession,
                                                            new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("CanvassingCollection", _locCanvassingCollectionXPO)));

                    if (_locCanvassingCollectionLines != null && _locCanvassingCollectionLines.Count() > 0)
                    {
                        _locInvCanvLineCount = _locCanvassingCollectionLines.Count();

                        foreach (CanvassingCollectionLine _locCanvassingCollectionLine in _locCanvassingCollectionLines)
                        {
                            if (_locCanvassingCollectionLine.Status == Status.Close)
                            {
                                _locStatusCount = _locStatusCount + 1;
                            }
                        }
                    }

                    if (_locStatusCount == _locInvCanvLineCount)
                    {
                        _locCanvassingCollectionXPO.ActivationPosting = true;
                        _locCanvassingCollectionXPO.Status = Status.Close;
                        _locCanvassingCollectionXPO.StatusDate = now;
                        _locCanvassingCollectionXPO.Save();
                        _locCanvassingCollectionXPO.Session.CommitTransaction();
                    }
                    else
                    {
                        _locCanvassingCollectionXPO.Status = Status.Posted;
                        _locCanvassingCollectionXPO.StatusDate = now;
                        _locCanvassingCollectionXPO.Save();
                        _locCanvassingCollectionXPO.Session.CommitTransaction();
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = CanvassingCollection " + ex.ToString());
            }
        }

        private void SetFinalStatusInvoiceCanvassing(Session _currSession, CanvassingCollection _locCanvassingCollectionXPO)
        {
            try
            {
                DateTime now = DateTime.Now;

                if (_locCanvassingCollectionXPO != null)
                {
                    XPQuery<CanvassingCollectionLine> _canvassingCollectionLinesQuery = new XPQuery<CanvassingCollectionLine>(_currSession);

                    var _canvassingCollectionLines = from ccl in _canvassingCollectionLinesQuery
                                                     where (ccl.Company == _locCanvassingCollectionXPO.Company
                                                     && ccl.Workplace == _locCanvassingCollectionXPO.Workplace
                                                     && ccl.Salesman == _locCanvassingCollectionXPO.Salesman
                                                     && ccl.CanvassingCollection == _locCanvassingCollectionXPO
                                                     )
                                                     group ccl by ccl.InvoiceCanvassingMonitoring.InvoiceCanvassing into g
                                                     select new { InvoiceCanvassing = g.Key };

                    if (_canvassingCollectionLines != null && _canvassingCollectionLines.Count() > 0)
                    {
                        foreach (var _canvassingCollectionLine in _canvassingCollectionLines)
                        {
                            if (_canvassingCollectionLine.InvoiceCanvassing != null)
                            {
                                _canvassingCollectionLine.InvoiceCanvassing.Status = Status.Close;
                                _canvassingCollectionLine.InvoiceCanvassing.StatusDate = now;
                                _canvassingCollectionLine.InvoiceCanvassing.Save();
                                _canvassingCollectionLine.InvoiceCanvassing.Session.CommitTransaction();
                            }
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = CanvassingCollection " + ex.ToString());
            }
        }

        #endregion Posting Method

        #region Global Method

        private void SuccessMessageShow(string _locActionName)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 2000;
            options.Message = string.Format("{0} !", _locActionName);
            options.Type = InformationType.Success;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Success";
            options.Win.Type = WinMessageType.Flyout;
            Application.ShowViewStrategy.ShowMessage(options);
        }

        private void ErrorMessageShow(string _locMessage)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 2000;
            options.Message = string.Format(_locMessage);
            options.Type = InformationType.Warning;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Error";
            options.Win.Type = WinMessageType.Alert;
            Application.ShowViewStrategy.ShowMessage(options);
        }



        #endregion Global Method

        
    }
}
