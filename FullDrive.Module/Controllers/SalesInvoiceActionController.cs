﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;

using FullDrive.Module.BusinessObjects;
using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.Xpo;
using System.Collections;
using DevExpress.Xpo;
using System.IO;
using System.Web;

namespace FullDrive.Module.Controllers
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class SalesInvoiceActionController : ViewController
    {
        private ChoiceActionItem _selectionListviewFilter;
        private ChoiceActionItem _selectionApprovalListviewFilter;
        private ChoiceActionItem _setApprovalLevel;

        public SalesInvoiceActionController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.
            #region FilterStatus
            SalesInvoiceListviewFilterSelectionAction.Items.Clear();
            foreach (object _currApproval in Enum.GetValues(typeof(CustomProcess.Status)))
            {
                EnumDescriptor _ed = new EnumDescriptor(typeof(CustomProcess.Status));
                _selectionListviewFilter = new ChoiceActionItem(_ed.GetCaption(_currApproval), _currApproval);
                SalesInvoiceListviewFilterSelectionAction.Items.Add(_selectionListviewFilter);
            }
            #endregion FilterStatus

            #region FilterApproval
            SalesInvoiceListviewFilterApprovalSelectionAction.Items.Clear();
            foreach (object _currApprovalFilter in Enum.GetValues(typeof(CustomProcess.ApprovalFilter)))
            {
                EnumDescriptor _ed = new EnumDescriptor(typeof(CustomProcess.ApprovalFilter));
                _selectionApprovalListviewFilter = new ChoiceActionItem(_ed.GetCaption(_currApprovalFilter), _currApprovalFilter);
                SalesInvoiceListviewFilterApprovalSelectionAction.Items.Add(_selectionApprovalListviewFilter);
            }
            #endregion FilterApproval
        }
        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
            #region Approval
            var _locUser = SecuritySystem.CurrentUserName;

            if (this.ObjectSpace != null)
            {
                UserAccess _locUserAccess = this.ObjectSpace.FindObject<UserAccess>(new BinaryOperator("UserName", _locUser));

                if (_locUserAccess != null)
                {
                    SalesInvoiceApprovalAction.Items.Clear();

                    XPCollection<ApplicationSetupDetail> _locAppSetupDetails = new XPCollection<ApplicationSetupDetail>
                                                                                 (_locUserAccess.Session, new GroupOperator(GroupOperatorType.And,
                                                                                  new BinaryOperator("UserAccess", _locUserAccess),
                                                                                  new BinaryOperator("ObjectList", CustomProcess.ObjectList.SalesInvoice),
                                                                                  new BinaryOperator("FunctionList", CustomProcess.FunctionList.Approval),
                                                                                  new BinaryOperator("Active", true)));

                    if (_locAppSetupDetails != null && _locAppSetupDetails.Count > 0)
                    {

                        foreach (ApplicationSetupDetail _locAppSetupDetail in _locAppSetupDetails)
                        {
                            foreach (object _currApproval in Enum.GetValues(typeof(CustomProcess.ApprovalLevel)))
                            {
                                if (_locAppSetupDetail.ApprovalLevel != CustomProcess.ApprovalLevel.None)
                                {
                                    EnumDescriptor _ed = new EnumDescriptor(typeof(CustomProcess.ApprovalLevel));
                                    _setApprovalLevel = new ChoiceActionItem(_ed.GetCaption(_locAppSetupDetail.ApprovalLevel), _locAppSetupDetail.ApprovalLevel);
                                }
                            }
                            SalesInvoiceApprovalAction.Items.Add(_setApprovalLevel);
                        }
                    }
                }
            }
            #endregion Approval
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }
        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        private void SalesInvoiceListviewFilterSelectionAction_Execute(object sender, SingleChoiceActionExecuteEventArgs e)
        {
            try
            {
                if ((View is ListView) & (View.ObjectTypeInfo.Type == typeof(SalesInvoice)))
                {
                    if ((Status)e.SelectedChoiceActionItem.Data == Status.None)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.None, BinaryOperatorType.NotEqual);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Open)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Open, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Progress)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Progress, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Posted)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Posted, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Lock)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Lock, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Close)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Close, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Cancel)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Cancel, BinaryOperatorType.Equal);
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = SalesInvoice " + ex.ToString());
            }
        }

        private void SalesInvoiceListviewFilterApprovalSelectionAction_Execute(object sender, SingleChoiceActionExecuteEventArgs e)
        {
            try
            {
                if ((View is ListView) & (View.ObjectTypeInfo.Type == typeof(SalesInvoice)))
                {
                    if ((ApprovalFilter)e.SelectedChoiceActionItem.Data == ApprovalFilter.Approval)
                    {
                        ((ListView)View).CollectionSource.Criteria["FilterApproval1"] = new GroupOperator(GroupOperatorType.And,
                                                                                        new BinaryOperator("ActiveApproved3", false, BinaryOperatorType.Equal),
                                                                                        new GroupOperator(GroupOperatorType.Or,
                                                                                        new BinaryOperator("ActiveApproved1", true, BinaryOperatorType.Equal),
                                                                                        new BinaryOperator("ActiveApproved2", true, BinaryOperatorType.Equal)));
                    }
                    else if ((ApprovalFilter)e.SelectedChoiceActionItem.Data == ApprovalFilter.EndApproval)
                    {
                        ((ListView)View).CollectionSource.Criteria["FilterApproval1"] = new BinaryOperator("ActiveApproved3", true, BinaryOperatorType.Equal);
                    }
                    else if ((ApprovalFilter)e.SelectedChoiceActionItem.Data == ApprovalFilter.AllData)
                    {
                        ((ListView)View).CollectionSource.Criteria["FilterApproval1"] = new GroupOperator(GroupOperatorType.Or,
                                                                                        new BinaryOperator("ActiveApproved1", true, BinaryOperatorType.Equal),
                                                                                        new BinaryOperator("ActiveApproved2", true, BinaryOperatorType.Equal),
                                                                                        new BinaryOperator("ActiveApproved3", true, BinaryOperatorType.Equal),
                                                                                        new BinaryOperator("ActiveApproved1", false, BinaryOperatorType.Equal),
                                                                                        new BinaryOperator("ActiveApproved2", false, BinaryOperatorType.Equal),
                                                                                        new BinaryOperator("ActiveApproved3", false, BinaryOperatorType.Equal));
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = SalesInvoice " + ex.ToString());
            }
        }

        private void SalesInvoiceGetSOAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;
                int _locCount = 0;
                DiscountRule _locDiscountRule = null;
                AccountingPeriodicLine _locAcctPerLine = null;

                if (_objectsToProcess != null)
                {
                    foreach (Object obj in _objectsToProcess)
                    {
                        SalesInvoice _locSalesInvoiceOS = (SalesInvoice)_objectSpace.GetObject(obj);

                        if (_locSalesInvoiceOS != null)
                        {
                            if (_locSalesInvoiceOS.Session != null)
                            {
                                _currSession = _locSalesInvoiceOS.Session;
                            }

                            if (_locSalesInvoiceOS.Code != null && _currSession != null)
                            {
                                _currObjectId = _locSalesInvoiceOS.Code;

                                SalesInvoice _locSalesInvoiceXPO = _currSession.FindObject<SalesInvoice>
                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                     new BinaryOperator("Code", _currObjectId)));

                                if (_locSalesInvoiceXPO != null)
                                {
                                    if (_locSalesInvoiceXPO.Status == Status.Open || _locSalesInvoiceXPO.Status == Status.Progress)
                                    {
                                        XPCollection<SalesOrderCollection> _locSalesOrderCollections = new XPCollection<SalesOrderCollection>
                                                                                        (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                        new BinaryOperator("SalesInvoice", _locSalesInvoiceXPO),
                                                                                        new GroupOperator(GroupOperatorType.Or,
                                                                                        new BinaryOperator("Status", Status.Open),
                                                                                        new BinaryOperator("Status", Status.Progress))));

                                        if (_locSalesOrderCollections != null && _locSalesOrderCollections.Count() > 0)
                                        {
                                            _locCount = _locSalesOrderCollections.Count();
                                            foreach (SalesOrderCollection _locSalesOrderCollection in _locSalesOrderCollections)
                                            {
                                                if (_locSalesOrderCollection.SalesOrder != null)
                                                {
                                                    if(_locSalesOrderCollection.SalesOrder.DiscountRule != null)
                                                    {
                                                        _locDiscountRule = _locSalesOrderCollection.SalesOrder.DiscountRule;
                                                    }
                                                    if(_locSalesOrderCollection.SalesOrder.AccountingPeriodicLine != null)
                                                    {
                                                        _locAcctPerLine = _locSalesOrderCollection.SalesOrder.AccountingPeriodicLine;
                                                    }
                                                    GetSalesOrderMonitoring(_currSession, _locSalesOrderCollection.SalesOrder, _locSalesInvoiceXPO);
                                                    if (_locCount == 1)
                                                    {
                                                        _locSalesInvoiceXPO.OverCredit = _locSalesOrderCollection.SalesOrder.OverCredit;
                                                        _locSalesInvoiceXPO.Message = _locSalesOrderCollection.SalesOrder.Message;
                                                        _locSalesInvoiceXPO.CreditLimitStatus = _locSalesOrderCollection.SalesOrder.CreditLimitStatus;
                                                        _locSalesInvoiceXPO.CreditLimitStatusDate = now;
                                                        _locSalesInvoiceXPO.DiscountRule = _locDiscountRule;
                                                        _locSalesInvoiceXPO.AccountingPeriodicLine = _locAcctPerLine;
                                                        _locSalesInvoiceXPO.Save();
                                                        _locSalesInvoiceXPO.Session.CommitTransaction();
                                                    } 
                                                }
                                            }
                                            
                                            SuccessMessageShow("SalesOrder Has Been Successfully Getting into SalesInvoice");
                                        }
                                    }
                                }
                                else
                                {
                                    ErrorMessageShow("Data SalesInvoice Not Available");
                                }
                            }
                            else
                            {
                                ErrorMessageShow("Data SalesInvoice Not Available");
                            }
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = SalesInvoice " + ex.ToString());
            }
        }

        private void SalesInvoiceApprovalAction_Execute(object sender, SingleChoiceActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                ApplicationSetupDetail _locAppSetDetail = null;
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                var User = SecuritySystem.CurrentUserName;
                Session _currentSession = null;
                string _currObjectId = null;

                foreach (Object obj in _objectsToProcess)
                {
                    SalesInvoice _objInNewObjectSpace = (SalesInvoice)_objectSpace.GetObject(obj);

                    if (_objInNewObjectSpace != null)
                    {
                        if (_objInNewObjectSpace.Code != null)
                        {
                            _currObjectId = _objInNewObjectSpace.Code;
                        }
                        if (_objInNewObjectSpace.Session != null)
                        {
                            _currentSession = _objInNewObjectSpace.Session;
                        }
                    }

                    if (_currObjectId != null && _currentSession != null)
                    {
                        UserAccess _locUserAccess = _currentSession.FindObject<UserAccess>(new BinaryOperator("UserName", User));

                        SalesInvoice _locSalesInvoiceXPO = _currentSession.FindObject<SalesInvoice>
                                                                        (new GroupOperator(GroupOperatorType.And,
                                                                         new BinaryOperator("Code", _currObjectId)));

                        if (_locSalesInvoiceXPO != null)
                        {
                            if (_locSalesInvoiceXPO.Status == Status.Open)
                            {
                                ApprovalLine _locApprovalLine = _currentSession.FindObject<ApprovalLine>
                                                                            (new GroupOperator(GroupOperatorType.And,
                                                                             new BinaryOperator("SalesInvoice", _locSalesInvoiceXPO),
                                                                             new BinaryOperator("EndApproval", true)));
                                if (_locApprovalLine == null)
                                {
                                    #region Approval Level 1
                                    if ((ApprovalLevel)e.SelectedChoiceActionItem.Data == ApprovalLevel.Level1)
                                    {
                                        _locAppSetDetail = _globFunc.GetApplicationSetupDetailByApprovalLevel(_currentSession, _locUserAccess, ApprovalLevel.Level1, ObjectList.SalesInvoice);
                                        if (_locAppSetDetail != null)
                                        {
                                            //Buat bs input langsung ke approvalline
                                            ApprovalLine _locApprovalLineXPO = _currentSession.FindObject<ApprovalLine>
                                                                                (new GroupOperator(GroupOperatorType.And,
                                                                                 new BinaryOperator("SalesInvoice", _locSalesInvoiceXPO),
                                                                                 new BinaryOperator("ApprovalLevel", ApprovalLevel.Level1)));
                                            if (_locApprovalLineXPO == null)
                                            {

                                                if (_locAppSetDetail.EndApproval == true)
                                                {
                                                    ApprovalLine _saveDataAL = new ApprovalLine(_currentSession)
                                                    {
                                                        ApprovalDate = now,
                                                        ApprovalStatus = Status.Approved,
                                                        ApprovalLevel = ApprovalLevel.Level1,
                                                        EndApproval = true,
                                                        SalesInvoice = _locSalesInvoiceXPO,
                                                    };
                                                    _saveDataAL.Save();
                                                    _saveDataAL.Session.CommitTransaction();

                                                    _locSalesInvoiceXPO.ActivationPosting = true;
                                                    _locSalesInvoiceXPO.ActiveApproved1 = false;
                                                    _locSalesInvoiceXPO.ActiveApproved2 = false;
                                                    _locSalesInvoiceXPO.ActiveApproved3 = true;
                                                    _locSalesInvoiceXPO.Save();
                                                    _locSalesInvoiceXPO.Session.CommitTransaction();
                                                }
                                                else
                                                {
                                                    ApprovalLine _saveDataAL = new ApprovalLine(_currentSession)
                                                    {
                                                        ApprovalDate = now,
                                                        ApprovalStatus = Status.Approved,
                                                        ApprovalLevel = ApprovalLevel.Level1,
                                                        SalesInvoice = _locSalesInvoiceXPO,
                                                    };
                                                    _saveDataAL.Save();
                                                    _saveDataAL.Session.CommitTransaction();

                                                    _locSalesInvoiceXPO.ActiveApproved1 = true;
                                                    _locSalesInvoiceXPO.ActiveApproved2 = false;
                                                    _locSalesInvoiceXPO.ActiveApproved3 = false;
                                                    _locSalesInvoiceXPO.Save();
                                                    _locSalesInvoiceXPO.Session.CommitTransaction();
                                                }

                                                //Send Email
                                                ApprovalLine _locApprovalLineXPO2 = _currentSession.FindObject<ApprovalLine>
                                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                                     new BinaryOperator("SalesInvoice", _locSalesInvoiceXPO)));

                                                if (_locApprovalLineXPO2 != null)
                                                {
                                                    SendEmail(_currentSession, _locSalesInvoiceXPO, Status.Progress, _locApprovalLineXPO2);
                                                }

                                                SuccessMessageShow("SalesInvoice has successfully Approve");
                                            }
                                        }
                                    }
                                    #endregion Approval Level 1

                                    #region Approval Level 2
                                    if ((ApprovalLevel)e.SelectedChoiceActionItem.Data == ApprovalLevel.Level2)
                                    {
                                        _locAppSetDetail = _globFunc.GetApplicationSetupDetailByApprovalLevel(_currentSession, _locUserAccess, ApprovalLevel.Level2, ObjectList.SalesInvoice);

                                        if (_locAppSetDetail != null)
                                        {
                                            ApprovalLine _locApprovalLineXPO = _currentSession.FindObject<ApprovalLine>
                                                                                (new GroupOperator(GroupOperatorType.And,
                                                                                 new BinaryOperator("SalesInvoice", _locSalesInvoiceXPO),
                                                                                 new BinaryOperator("ApprovalLevel", ApprovalLevel.Level2)));

                                            if (_locApprovalLineXPO == null)
                                            {
                                                if (_locAppSetDetail.EndApproval == true)
                                                {
                                                    ApprovalLine _saveDataAL = new ApprovalLine(_currentSession)
                                                    {
                                                        ApprovalDate = now,
                                                        ApprovalStatus = Status.Approved,
                                                        ApprovalLevel = ApprovalLevel.Level2,
                                                        EndApproval = true,
                                                        SalesInvoice = _locSalesInvoiceXPO,
                                                    };
                                                    _saveDataAL.Save();
                                                    _saveDataAL.Session.CommitTransaction();

                                                    _locSalesInvoiceXPO.ActivationPosting = true;
                                                    _locSalesInvoiceXPO.ActiveApproved1 = false;
                                                    _locSalesInvoiceXPO.ActiveApproved2 = false;
                                                    _locSalesInvoiceXPO.ActiveApproved3 = true;
                                                    _locSalesInvoiceXPO.Save();
                                                    _locSalesInvoiceXPO.Session.CommitTransaction();
                                                }
                                                else
                                                {
                                                    ApprovalLine _saveDataAL = new ApprovalLine(_currentSession)
                                                    {
                                                        ApprovalDate = now,
                                                        ApprovalStatus = Status.Approved,
                                                        ApprovalLevel = ApprovalLevel.Level2,
                                                        SalesInvoice = _locSalesInvoiceXPO,
                                                    };
                                                    _saveDataAL.Save();
                                                    _saveDataAL.Session.CommitTransaction();

                                                    SetApprovalLine(_currentSession, _locSalesInvoiceXPO, ApprovalLevel.Level1);

                                                    _locSalesInvoiceXPO.ActiveApproved1 = false;
                                                    _locSalesInvoiceXPO.ActiveApproved2 = true;
                                                    _locSalesInvoiceXPO.ActiveApproved3 = false;
                                                    _locSalesInvoiceXPO.Save();
                                                    _locSalesInvoiceXPO.Session.CommitTransaction();
                                                }

                                                //Send Email
                                                ApprovalLine _locApprovalLineXPO2 = _currentSession.FindObject<ApprovalLine>
                                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                                     new BinaryOperator("SalesInvoice", _locSalesInvoiceXPO)));

                                                if (_locApprovalLineXPO2 != null)
                                                {
                                                    SendEmail(_currentSession, _locSalesInvoiceXPO, Status.Progress, _locApprovalLineXPO2);
                                                }

                                                SuccessMessageShow("SalesInvoice has successfully Approve");
                                            }
                                        }
                                    }
                                    #endregion Approval Level 2

                                    #region Approval Level 3
                                    if ((ApprovalLevel)e.SelectedChoiceActionItem.Data == ApprovalLevel.Level3)
                                    {
                                        _locAppSetDetail = _globFunc.GetApplicationSetupDetailByApprovalLevel(_currentSession, _locUserAccess, ApprovalLevel.Level3, ObjectList.SalesInvoice);

                                        if (_locAppSetDetail != null)
                                        {
                                            ApprovalLine _locApprovalLineXPO = _currentSession.FindObject<ApprovalLine>
                                                                                (new GroupOperator(GroupOperatorType.And,
                                                                                 new BinaryOperator("SalesInvoice", _locSalesInvoiceXPO),
                                                                                 new BinaryOperator("ApprovalLevel", ApprovalLevel.Level3)));

                                            if (_locApprovalLineXPO == null)
                                            {
                                                if (_locAppSetDetail.EndApproval == true)
                                                {
                                                    ApprovalLine _saveDataAL = new ApprovalLine(_currentSession)
                                                    {
                                                        ApprovalDate = now,
                                                        ApprovalStatus = Status.Approved,
                                                        ApprovalLevel = ApprovalLevel.Level3,
                                                        EndApproval = true,
                                                        SalesInvoice = _locSalesInvoiceXPO,
                                                    };
                                                    _saveDataAL.Save();
                                                    _saveDataAL.Session.CommitTransaction();

                                                    _locSalesInvoiceXPO.ActivationPosting = true;
                                                    _locSalesInvoiceXPO.ActiveApproved1 = false;
                                                    _locSalesInvoiceXPO.ActiveApproved2 = false;
                                                    _locSalesInvoiceXPO.ActiveApproved3 = true;
                                                    _locSalesInvoiceXPO.Save();
                                                    _locSalesInvoiceXPO.Session.CommitTransaction();
                                                }
                                                else
                                                {
                                                    ApprovalLine _saveDataAL = new ApprovalLine(_currentSession)
                                                    {
                                                        ApprovalDate = now,
                                                        ApprovalStatus = Status.Approved,
                                                        ApprovalLevel = ApprovalLevel.Level3,
                                                        SalesInvoice = _locSalesInvoiceXPO,
                                                    };
                                                    _saveDataAL.Save();
                                                    _saveDataAL.Session.CommitTransaction();

                                                    SetApprovalLine(_currentSession, _locSalesInvoiceXPO, ApprovalLevel.Level2);
                                                    SetApprovalLine(_currentSession, _locSalesInvoiceXPO, ApprovalLevel.Level1);

                                                    _locSalesInvoiceXPO.ActiveApproved1 = false;
                                                    _locSalesInvoiceXPO.ActiveApproved2 = false;
                                                    _locSalesInvoiceXPO.ActiveApproved3 = true;
                                                    _locSalesInvoiceXPO.Save();
                                                    _locSalesInvoiceXPO.Session.CommitTransaction();
                                                }

                                                //Send Email
                                                ApprovalLine _locApprovalLineXPO2 = _currentSession.FindObject<ApprovalLine>
                                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                                     new BinaryOperator("SalesInvoice", _locSalesInvoiceXPO)));

                                                if (_locApprovalLineXPO2 != null)
                                                {
                                                    SendEmail(_currentSession, _locSalesInvoiceXPO, Status.Progress, _locApprovalLineXPO2);
                                                }

                                                SuccessMessageShow("SalesInvoice has successfully Approve");
                                            }
                                        }
                                    }
                                    #endregion Approval Level 3
                                }
                            }
                            else
                            {
                                ErrorMessageShow("SalesInvoice Status Not Available");
                            }
                        }
                        else
                        {
                            ErrorMessageShow("SalesInvoice Not Available");
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = SalesInvoice " + ex.ToString());
            }
        }

        private void SalesInvoiceProgressAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;
                Status _locStatus = Status.Open;

                if (_objectsToProcess != null)
                {
                    foreach (Object obj in _objectsToProcess)
                    {
                        SalesInvoice _locSalesInvoiceOS = (SalesInvoice)_objectSpace.GetObject(obj);

                        if (_locSalesInvoiceOS != null)
                        {
                            if (_locSalesInvoiceOS.Session != null)
                            {
                                _currSession = _locSalesInvoiceOS.Session;
                            }

                            if (_locSalesInvoiceOS.Code != null && _currSession != null)
                            {
                                _currObjectId = _locSalesInvoiceOS.Code;

                                SalesInvoice _locSalesInvoiceXPO = _currSession.FindObject<SalesInvoice>
                                                                   (new GroupOperator(GroupOperatorType.And,
                                                                    new BinaryOperator("Code", _currObjectId),
                                                                    new GroupOperator(GroupOperatorType.Or,
                                                                    new BinaryOperator("Status", Status.Open),
                                                                    new BinaryOperator("Status", Status.Progress))));

                                if (_locSalesInvoiceXPO != null && _locSalesInvoiceXPO.CreditLimitStatus != CreditLimitStatus.Hold)
                                {
                                    ApprovalLine _locApprovalLine = _currSession.FindObject<ApprovalLine>
                                                                            (new GroupOperator(GroupOperatorType.And,
                                                                             new BinaryOperator("SalesInvoice", _locSalesInvoiceXPO),
                                                                             new BinaryOperator("EndApproval", true)));

                                    if (_locApprovalLine != null )
                                    {
                                        SetTotalAmountAndTaxAmountAndDiscountAmount(_currSession, _locSalesInvoiceXPO);

                                        if (_locSalesInvoiceXPO.Status == Status.Open)
                                        {
                                            _locStatus = Status.Progress;

                                        }
                                        else
                                        {
                                            _locStatus = _locSalesInvoiceXPO.Status;
                                        }

                                        _locSalesInvoiceXPO.Status = _locStatus;
                                        _locSalesInvoiceXPO.StatusDate = now;
                                        _locSalesInvoiceXPO.Save();
                                        _locSalesInvoiceXPO.Session.CommitTransaction();

                                        #region SalesInvoiceLine
                                        XPCollection<SalesInvoiceLine> _locSalesInvoiceLines = new XPCollection<SalesInvoiceLine>
                                                                                               (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                new BinaryOperator("SalesInvoice", _locSalesInvoiceXPO)));

                                        if (_locSalesInvoiceLines != null && _locSalesInvoiceLines.Count > 0)
                                        {
                                            foreach (SalesInvoiceLine _locSalesInvoiceLine in _locSalesInvoiceLines)
                                            {
                                                if (_locSalesInvoiceLine.Status == Status.Open)
                                                {
                                                    _locSalesInvoiceLine.Status = Status.Progress;
                                                    _locSalesInvoiceLine.StatusDate = now;
                                                    _locSalesInvoiceLine.ActivationPosting = true;
                                                    _locSalesInvoiceLine.Save();
                                                    _locSalesInvoiceLine.Session.CommitTransaction();
                                                }
                                            }
                                        }
                                        #endregion SalesInvoiceLine

                                        #region SalesInvoiceFreeItem
                                        XPCollection<SalesInvoiceFreeItem> _locSalesInvoiceFreeItems = new XPCollection<SalesInvoiceFreeItem>
                                                                                               (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                new BinaryOperator("SalesInvoice", _locSalesInvoiceXPO)));

                                        if (_locSalesInvoiceFreeItems != null && _locSalesInvoiceFreeItems.Count > 0)
                                        {
                                            foreach (SalesInvoiceFreeItem _locSalesInvoiceFreeItem in _locSalesInvoiceFreeItems)
                                            {
                                                if (_locSalesInvoiceFreeItem.Status == Status.Open)
                                                {
                                                    _locSalesInvoiceFreeItem.Status = Status.Progress;
                                                    _locSalesInvoiceFreeItem.StatusDate = now;
                                                    _locSalesInvoiceFreeItem.ActivationPosting = true;
                                                    _locSalesInvoiceFreeItem.Save();
                                                    _locSalesInvoiceFreeItem.Session.CommitTransaction();
                                                }
                                            }
                                        }
                                        #endregion SalesInvoiceFreeItem

                                        SetOverCreditStatus(_currSession, _locSalesInvoiceXPO);

                                        SuccessMessageShow(_locSalesInvoiceXPO.Code + " has been change successfully to Progress ");
                                    }
                                }
                                else
                                {
                                    ErrorMessageShow("Data SalesInvoice Not Available");
                                }
                            }
                            else
                            {
                                ErrorMessageShow("Data SalesInvoice Not Available");
                            }
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = SalesOrder " + ex.ToString());
            }
        }

        private void SalesInvoiceSetFreeItemAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;

                if (_objectsToProcess != null)
                {
                    foreach (Object obj in _objectsToProcess)
                    {
                        SalesInvoice _locSalesInvoiceOS = (SalesInvoice)_objectSpace.GetObject(obj);

                        if (_locSalesInvoiceOS != null)
                        {
                            if (_locSalesInvoiceOS.Session != null)
                            {
                                _currSession = _locSalesInvoiceOS.Session;
                            }

                            if (_locSalesInvoiceOS.Code != null && _currSession != null)
                            {
                                _currObjectId = _locSalesInvoiceOS.Code;

                                SalesInvoice _locSalesInvoiceXPO = _currSession.FindObject<SalesInvoice>
                                                                   (new GroupOperator(GroupOperatorType.And,
                                                                    new BinaryOperator("Code", _currObjectId),
                                                                    new BinaryOperator("Status", Status.Progress)));

                                if (_locSalesInvoiceXPO != null)
                                {
                                    ApprovalLine _locApprovalLine = _currSession.FindObject<ApprovalLine>
                                                                            (new GroupOperator(GroupOperatorType.And,
                                                                             new BinaryOperator("SalesInvoice", _locSalesInvoiceXPO),
                                                                             new BinaryOperator("EndApproval", true)));
                                    if (_locApprovalLine != null)
                                    {
                                        SetFreeItem(_currSession, _locSalesInvoiceXPO);
                                        SetTotalFreeItemAmount(_currSession, _locSalesInvoiceXPO);
                                        SuccessMessageShow(_locSalesInvoiceXPO.Code + " has been successfully create Free Item  ");
                                    }
                                }
                                else
                                {
                                    ErrorMessageShow("Data SalesInvoice Not Available");
                                }
                            }
                            else
                            {
                                ErrorMessageShow("Data SalesInvoice Not Available");
                            }
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = SalesInvoice " + ex.ToString());
            }
        }

        private void SalesInvoiceETDAndETAAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;

                if (_objectsToProcess != null)
                {
                    foreach (Object obj in _objectsToProcess)
                    {
                        SalesInvoice _locSalesInvoiceOS = (SalesInvoice)_objectSpace.GetObject(obj);

                        if (_locSalesInvoiceOS != null)
                        {
                            if (_locSalesInvoiceOS.Session != null)
                            {
                                _currSession = _locSalesInvoiceOS.Session;
                            }

                            if (_locSalesInvoiceOS.Code != null && _currSession != null)
                            {
                                _currObjectId = _locSalesInvoiceOS.Code;

                                SalesInvoice _locSalesInvoiceXPO = _currSession.FindObject<SalesInvoice>
                                                                   (new GroupOperator(GroupOperatorType.And,
                                                                    new BinaryOperator("Code", _currObjectId),
                                                                    new GroupOperator(GroupOperatorType.Or,
                                                                    new BinaryOperator("Status", Status.Progress),
                                                                    new BinaryOperator("Status", Status.Open))));

                                if (_locSalesInvoiceXPO != null)
                                {
                                    XPCollection<SalesInvoiceLine> _locSalesInvoiceLines = new XPCollection<SalesInvoiceLine>
                                                                                    (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                    new BinaryOperator("SalesInvoice", _locSalesInvoiceXPO),
                                                                                    new GroupOperator(GroupOperatorType.Or,
                                                                                    new BinaryOperator("Status", Status.Open),
                                                                                    new BinaryOperator("Status", Status.Progress))));
                                    if(_locSalesInvoiceLines != null && _locSalesInvoiceLines.Count() > 0)
                                    {
                                        foreach(SalesInvoiceLine _locSalesInvoiceLine in _locSalesInvoiceLines)
                                        {
                                            _locSalesInvoiceLine.ETD = _locSalesInvoiceXPO.ETD;
                                            _locSalesInvoiceLine.ETA = _locSalesInvoiceXPO.ETA;
                                            _locSalesInvoiceLine.Save();
                                            _locSalesInvoiceLine.Session.CommitTransaction();
                                        }
                                    }                                  
                                }
                                else
                                {
                                    ErrorMessageShow("Data SalesInvoice Not Available");
                                }
                            }
                            else
                            {
                                ErrorMessageShow("Data SalesInvoice Not Available");
                            }
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = SalesInvoice " + ex.ToString());
            }
        }

        private void SalesInvoicePostingAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {

                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;

                if (_objectsToProcess != null)
                {
                    foreach (Object obj in _objectsToProcess)
                    {
                        SalesInvoice _locSalesInvoiceOS = (SalesInvoice)_objectSpace.GetObject(obj);

                        if (_locSalesInvoiceOS != null)
                        {
                            if (_locSalesInvoiceOS.Session != null)
                            {
                                _currSession = _locSalesInvoiceOS.Session;
                            }

                            if (_locSalesInvoiceOS.Code != null && _currSession != null)
                            {
                                _currObjectId = _locSalesInvoiceOS.Code;

                                SalesInvoice _locSalesInvoiceXPO = _currSession.FindObject<SalesInvoice>
                                                                   (new GroupOperator(GroupOperatorType.And,
                                                                    new BinaryOperator("Code", _currObjectId),
                                                                    new GroupOperator(GroupOperatorType.Or,
                                                                    new BinaryOperator("Status", Status.Progress),
                                                                    new BinaryOperator("Status", Status.Posted))));

                                if (_locSalesInvoiceXPO != null)
                                {
                                    if (_locSalesInvoiceXPO.TotAmount > 0)
                                    {
                                        //Berdasarkan Outstanding dari PaymentOutPlan yg mana di hitung berdasarkan jumlah mak Outstanding 
                                        if (_locSalesInvoiceXPO.TotAmount == GetSalesInvoiceLineTotalAmount(_currSession, _locSalesInvoiceXPO) && _locSalesInvoiceXPO.CreditLimitStatus != CreditLimitStatus.Hold )
                                        {
                                            SetSalesInvoiceMonitoring(_currSession, _locSalesInvoiceXPO);
                                            SetStatusSalesInvoiceLine(_currSession, _locSalesInvoiceXPO);
                                            SetStatusSalesInvoiceFreeItem(_currSession, _locSalesInvoiceXPO);
                                            SetFinalSalesInvoice(_currSession, _locSalesInvoiceXPO);
                                            SuccessMessageShow(_locSalesInvoiceXPO.Code + " has been change successfully to Posted");
                                        }
                                    }
                                }
                                else
                                {
                                    ErrorMessageShow("Data SalesInvoice Not Available");
                                }
                            }
                            else
                            {
                                ErrorMessageShow("Data SalesInvoice Not Available");
                            }
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = SalesInvoice " + ex.ToString());
            }
        }

        private void SalesInvoiceReleaseCLAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;
                var User = SecuritySystem.CurrentUserName;
                string _locMessage = null;

                if (_objectsToProcess != null)
                {
                    foreach (Object obj in _objectsToProcess)
                    {
                        SalesInvoice _locSalesInvoiceOS = (SalesInvoice)_objectSpace.GetObject(obj);

                        if (_locSalesInvoiceOS != null)
                        {
                            if (_locSalesInvoiceOS.Session != null)
                            {
                                _currSession = _locSalesInvoiceOS.Session;
                            }

                            if (_locSalesInvoiceOS.Code != null && _currSession != null)
                            {
                                _currObjectId = _locSalesInvoiceOS.Code;

                                SalesInvoice _locSalesInvoiceXPO = _currSession.FindObject<SalesInvoice>
                                                                   (new GroupOperator(GroupOperatorType.And,
                                                                    new BinaryOperator("Code", _currObjectId),
                                                                    new GroupOperator(GroupOperatorType.Or,
                                                                    new BinaryOperator("Status", Status.Open),
                                                                    new BinaryOperator("Status", Status.Progress),
                                                                    new BinaryOperator("Status", Status.Posted))));

                                if (_locSalesInvoiceXPO != null)
                                {
                                    UserAccess _locUserAccess = _currSession.FindObject<UserAccess>(new BinaryOperator("UserName", User));
                                    if (_locUserAccess != null)
                                    {
                                        _locMessage = "Released By " + _locUserAccess.UserName;
                                    }
                                    else
                                    {
                                        _locMessage = "Released";
                                    }

                                    _locSalesInvoiceXPO.OverCredit = false;
                                    _locSalesInvoiceXPO.Message = _locMessage;
                                    _locSalesInvoiceXPO.CreditLimitStatus = CreditLimitStatus.Released;
                                    _locSalesInvoiceXPO.CreditLimitStatusDate = now;
                                    _locSalesInvoiceXPO.Save();
                                    _locSalesInvoiceXPO.Session.CommitTransaction();
                                    SuccessMessageShow(_locSalesInvoiceXPO.Code + " has been change successfully to Released Credit Limit ");
                                }
                                else
                                {
                                    ErrorMessageShow("Data SalesInvoice Not Available");
                                }
                            }
                            else
                            {
                                ErrorMessageShow("Data SalesInvoice Not Available");
                            }
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = SalesOrder " + ex.ToString());
            }
        }

        //=========================================== Code In Here ===========================================

        #region GetSI

        private void GetSalesOrderMonitoring(Session _currSession, SalesOrder _locSalesOrderXPO, SalesInvoice _locSalesInvoiceXPO)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                DateTime now = DateTime.Now;
                string _currSignCode = null;
                string _currFpSignCode = null;
                if (_locSalesOrderXPO != null && _locSalesInvoiceXPO != null)
                {
                    #region SalesOrderMonitoring
                    //Hanya memindahkan PurchaseRequisitionMonitoring ke PurchaseOrderLine
                    XPCollection<SalesOrderMonitoring> _locSalesOrderMonitorings = new XPCollection<SalesOrderMonitoring>
                                                                                        (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                        new BinaryOperator("SalesOrder", _locSalesOrderXPO),
                                                                                        new BinaryOperator("SalesInvoice", _locSalesInvoiceXPO),
                                                                                        new BinaryOperator("Select", true),
                                                                                        new BinaryOperator("Status", Status.Close, BinaryOperatorType.NotEqual)));

                    if (_locSalesOrderMonitorings != null && _locSalesOrderMonitorings.Count() > 0)
                    {
                        foreach (SalesOrderMonitoring _locSalesOrderMonitoring in _locSalesOrderMonitorings)
                        {
                            double _locCmbTQty = 0;
                            double _locTQty = 0;
                            double _locDQty = 0;
                            double _locQty = 0;
                            double _locCmbFpTQty = 0;
                            double _locFpTQty = 0;
                            double _locFpDQty = 0;
                            double _locFpQty = 0;
                            double _locTUAmount = 0;
                            double _locTxAmount = 0;
                            double _locDiscAmount = 0;

                            if (_locSalesOrderMonitoring.StockGroup == StockGroup.Normal)
                            {
                                if(_locSalesOrderMonitoring.SalesOrderLine != null && _locSalesOrderMonitoring.Item != null)
                                {
                                    SalesInvoiceLine _locSalesInvoiceLine = _currSession.FindObject<SalesInvoiceLine>
                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                    new BinaryOperator("Item", _locSalesOrderMonitoring.Item),
                                                                    new BinaryOperator("SalesInvoice", _locSalesInvoiceXPO)));
                                    #region SalesInvoiceLineAvailable
                                    if (_locSalesInvoiceLine != null)
                                    {
                                        if(_locSalesInvoiceLine.Tax == _locSalesOrderMonitoring.Tax && _locSalesInvoiceLine.DiscountRule == _locSalesOrderMonitoring.DiscountRule)
                                        {
                                            _locCmbTQty = _locSalesInvoiceLine.TQty + _locSalesOrderMonitoring.TQty;
                                            _locTUAmount = _locSalesInvoiceLine.TUAmount + _locSalesOrderMonitoring.TUAmount;
                                            _locTxAmount = _locSalesInvoiceLine.TxAmount + _locSalesOrderMonitoring.TxAmount;
                                            _locDiscAmount = _locSalesInvoiceLine.DiscAmount + _locSalesOrderMonitoring.DiscAmount;

                                            if (_locSalesInvoiceLine.Item != null && _locSalesInvoiceLine.DUOM != null && _locSalesInvoiceLine.UOM != null)
                                            {
                                                ItemUnitOfMeasure _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                                        (new GroupOperator(GroupOperatorType.And,
                                                                         new BinaryOperator("Item", _locSalesInvoiceLine.Item),
                                                                         new BinaryOperator("UOM", _locSalesInvoiceLine.UOM),
                                                                         new BinaryOperator("DefaultUOM", _locSalesInvoiceLine.DUOM),
                                                                         new BinaryOperator("Active", true)));
                                                if (_locItemUOM != null)
                                                {
                                                    if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                                    {
                                                        _locDQty = _locCmbTQty % _locItemUOM.DefaultConversion;
                                                        _locQty = System.Math.Floor(_locCmbTQty / _locItemUOM.DefaultConversion);
                                                        _locTQty = _locCmbTQty;
                                                    }
                                                    else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                                    {
                                                        _locDQty = System.Math.Floor(_locCmbTQty / _locItemUOM.DefaultConversion);
                                                        _locQty = (_locCmbTQty % _locItemUOM.DefaultConversion) * _locItemUOM.Conversion;
                                                        _locTQty = _locCmbTQty;
                                                    }
                                                    else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                                    {
                                                        _locDQty = _locCmbTQty;
                                                        _locTQty = _locCmbTQty;
                                                    }
                                                }
                                                else
                                                {
                                                    _locDQty = _locCmbTQty;
                                                    _locTQty = _locCmbTQty;
                                                }
                                            }
                                            else
                                            {
                                                _locDQty = _locCmbTQty;
                                                _locTQty = _locCmbTQty;
                                            }

                                            _locSalesInvoiceLine.DQty = _locDQty;
                                            _locSalesInvoiceLine.Qty = _locQty;
                                            _locSalesInvoiceLine.TQty = _locTQty;
                                            _locSalesInvoiceLine.TUAmount = _locTUAmount;
                                            _locSalesInvoiceLine.TxAmount = _locTxAmount;
                                            _locSalesInvoiceLine.DiscAmount = _locDiscAmount;
                                            _locSalesInvoiceLine.Save();
                                            _locSalesInvoiceLine.Session.CommitTransaction();

                                            SetStatusSalesOrderMonitoringNormalItem(_currSession, _locSalesOrderMonitoring, _locSalesInvoiceLine);
                                        }
                                        
                                    }
                                    #endregion SalesInvoiceLineAvailable
                                    #region SalesInvoiceLineNotAvailable
                                    else
                                    {
                                        _currSignCode = _globFunc.GetNumberingSignUnlockOptimisticRecordBasedOrganization(_currSession.DataLayer, ObjectList.SalesInvoiceLine, _locSalesInvoiceXPO.Company, _locSalesInvoiceXPO.Workplace);
                                        if (_currSignCode != null)
                                        {
                                            #region SalesInvoiceLine
                                            SalesInvoiceLine _saveDataSalesInvoiceLine = new SalesInvoiceLine(_currSession)
                                            {
                                                SignCode = _currSignCode,
                                                FreeItemChecked = _locSalesOrderMonitoring.FreeItemChecked,
                                                Company = _locSalesOrderMonitoring.Company,
                                                Workplace = _locSalesOrderMonitoring.Workplace,
                                                Division = _locSalesInvoiceXPO.Division,
                                                Department = _locSalesInvoiceXPO.Department,
                                                Section = _locSalesInvoiceXPO.Section,
                                                Employee = _locSalesInvoiceXPO.Employee,
                                                Div = _locSalesOrderMonitoring.Div,
                                                Dept = _locSalesOrderMonitoring.Dept,
                                                Sect = _locSalesOrderMonitoring.Sect,
                                                PIC = _locSalesOrderMonitoring.PIC,
                                                SalesType = _locSalesOrderMonitoring.SalesType,
                                                BegInv = _locSalesOrderMonitoring.BegInv,
                                                Item = _locSalesOrderMonitoring.Item,
                                                BegInvLine = _locSalesOrderMonitoring.BegInvLine,
                                                Description = _locSalesOrderMonitoring.Description,
                                                DQty = _locSalesOrderMonitoring.DQty,
                                                DUOM = _locSalesOrderMonitoring.DUOM,
                                                Qty = _locSalesOrderMonitoring.Qty,
                                                UOM = _locSalesOrderMonitoring.UOM,
                                                TQty = _locSalesOrderMonitoring.TQty,
                                                StockType = _locSalesOrderMonitoring.StockType,
                                                Currency = _locSalesOrderMonitoring.Currency,
                                                PriceGroup = _locSalesOrderMonitoring.PriceGroup,
                                                PriceLine = _locSalesOrderMonitoring.PriceLine,
                                                UAmount = _locSalesOrderMonitoring.UAmount,
                                                TUAmount = _locSalesOrderMonitoring.TUAmount,
                                                Tax = _locSalesOrderMonitoring.Tax,
                                                TxValue = _locSalesOrderMonitoring.TxValue,
                                                TxAmount = _locSalesOrderMonitoring.TxAmount,
                                                DiscountRule = _locSalesOrderMonitoring.DiscountRule,
                                                AccountingPeriodicLine = _locSalesOrderMonitoring.AccountingPeriodicLine,
                                                DiscAmount = _locSalesOrderMonitoring.DiscAmount,
                                                TAmount = _locSalesOrderMonitoring.TAmount,
                                                ETD = _locSalesOrderMonitoring.ETD,
                                                ETA = _locSalesOrderMonitoring.ETA,
                                                DocDate = _locSalesInvoiceXPO.DocDate,
                                                JournalMonth = _locSalesInvoiceXPO.JournalMonth,
                                                JournalYear = _locSalesInvoiceXPO.JournalYear,
                                                SalesInvoice = _locSalesInvoiceXPO,
                                            };
                                            _saveDataSalesInvoiceLine.Save();
                                            _saveDataSalesInvoiceLine.Session.CommitTransaction();
                                            #endregion SalesInvoiceLine

                                            SalesInvoiceLine _locSalInvLine = _currSession.FindObject<SalesInvoiceLine>
                                                                        (new GroupOperator(GroupOperatorType.And,
                                                                        new BinaryOperator("SignCode", _currSignCode)));
                                            if (_locSalInvLine != null)
                                            {
                                                SetStatusSalesOrderMonitoringNormalItem(_currSession, _locSalesOrderMonitoring, _locSalInvLine);
                                            }
                                        }
                                    }
                                    #endregion SalesInvoiceLineNotAvailable
                                }
                            }
                            else
                            {
                                if(_locSalesOrderMonitoring.Item != null)
                                {
                                    SalesInvoiceFreeItem _locSalesInvoiceFreeItem = _currSession.FindObject<SalesInvoiceFreeItem>
                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                    new BinaryOperator("FpItem", _locSalesOrderMonitoring.Item),
                                                                    new BinaryOperator("SalesInvoice", _locSalesInvoiceXPO)));
                                    #region SalesInvoiceFreeItemAvailable
                                    if (_locSalesInvoiceFreeItem != null)
                                    {
                                        _locCmbFpTQty = _locSalesInvoiceFreeItem.FpTQty + _locSalesOrderMonitoring.TQty;

                                        if (_locSalesInvoiceFreeItem.FpItem != null && _locSalesInvoiceFreeItem.FpDUOM != null && _locSalesInvoiceFreeItem.FpUOM != null)
                                        {
                                            ItemUnitOfMeasure _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                     new BinaryOperator("Item", _locSalesInvoiceFreeItem.FpItem),
                                                                     new BinaryOperator("UOM", _locSalesInvoiceFreeItem.FpUOM),
                                                                     new BinaryOperator("DefaultUOM", _locSalesInvoiceFreeItem.FpDUOM),
                                                                     new BinaryOperator("Active", true)));
                                            if (_locItemUOM != null)
                                            {
                                                if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                                {
                                                    _locFpDQty = _locCmbFpTQty % _locItemUOM.DefaultConversion;
                                                    _locFpQty = System.Math.Floor(_locCmbFpTQty / _locItemUOM.DefaultConversion);
                                                    _locFpTQty = _locCmbFpTQty;
                                                }
                                                else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                                {
                                                    _locFpDQty = System.Math.Floor(_locCmbFpTQty / _locItemUOM.DefaultConversion);
                                                    _locFpQty = (_locCmbFpTQty % _locItemUOM.DefaultConversion) * _locItemUOM.Conversion;
                                                    _locFpTQty = _locCmbFpTQty;
                                                }
                                                else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                                {
                                                    _locFpDQty = _locCmbFpTQty;
                                                    _locFpTQty = _locCmbFpTQty;
                                                }
                                            }
                                            else
                                            {
                                                _locFpDQty = _locCmbFpTQty;
                                                _locFpTQty = _locCmbFpTQty;
                                            }
                                        }
                                        else
                                        {
                                            _locFpDQty = _locCmbFpTQty;
                                            _locFpTQty = _locCmbFpTQty;
                                        }

                                        _locSalesInvoiceFreeItem.FpDQty = _locFpDQty;
                                        _locSalesInvoiceFreeItem.FpQty = _locFpQty;
                                        _locSalesInvoiceFreeItem.FpTQty = _locFpTQty;
                                        _locSalesInvoiceFreeItem.Save();
                                        _locSalesInvoiceFreeItem.Session.CommitTransaction();

                                        SetStatusSalesOrderMonitoringFreeItem(_currSession, _locSalesOrderMonitoring, _locSalesInvoiceFreeItem);
                                    }
                                    #endregion SalesInvoiceFreeItemAvailable
                                    #region SalesInvoiceFreeItemNotAvailable
                                    else
                                    {
                                        _currFpSignCode = _globFunc.GetNumberingSignUnlockOptimisticRecordBasedOrganization(_currSession.DataLayer, ObjectList.SalesInvoiceFreeItem, _locSalesInvoiceXPO.Company, _locSalesInvoiceXPO.Workplace);
                                        if (_currFpSignCode != null)
                                        {
                                            SalesInvoiceFreeItem _saveDataSalesInvoiceFreeItem = new SalesInvoiceFreeItem(_currSession)
                                            {
                                                SignCode = _currFpSignCode,
                                                Company = _locSalesOrderMonitoring.Company,
                                                Workplace = _locSalesOrderMonitoring.Workplace,
                                                Division = _locSalesInvoiceXPO.Division,
                                                Department = _locSalesInvoiceXPO.Department,
                                                Section = _locSalesInvoiceXPO.Section,
                                                Employee = _locSalesInvoiceXPO.Employee,
                                                Div = _locSalesOrderMonitoring.Div,
                                                Dept = _locSalesOrderMonitoring.Dept,
                                                Sect = _locSalesOrderMonitoring.Sect,
                                                PIC = _locSalesOrderMonitoring.PIC,
                                                Location = _locSalesOrderMonitoring.Location,
                                                BegInv = _locSalesOrderMonitoring.BegInv,
                                                FreeItemRule = _locSalesOrderMonitoring.FreeItemRule,
                                                FpItem = _locSalesOrderMonitoring.Item,
                                                BegInvLine = _locSalesOrderMonitoring.BegInvLine,
                                                FpDQty = _locSalesOrderMonitoring.DQty,
                                                FpDUOM = _locSalesOrderMonitoring.DUOM,
                                                FpQty = _locSalesOrderMonitoring.Qty,
                                                FpUOM = _locSalesOrderMonitoring.UOM,
                                                FpTQty = _locSalesOrderMonitoring.TQty,
                                                Currency = _locSalesOrderMonitoring.Currency,
                                                PriceGroup = _locSalesOrderMonitoring.PriceGroup,
                                                PriceLine = _locSalesOrderMonitoring.PriceLine,
                                                UAmount = _locSalesOrderMonitoring.UAmount,
                                                TUAmount = _locSalesOrderMonitoring.TUAmount,
                                                ETD = _locSalesOrderMonitoring.ETD,
                                                ETA = _locSalesOrderMonitoring.ETA,
                                                DocDate = _locSalesInvoiceXPO.DocDate,
                                                JournalMonth = _locSalesInvoiceXPO.JournalMonth,
                                                JournalYear = _locSalesInvoiceXPO.JournalYear,
                                                FpStockType = _locSalesOrderMonitoring.StockType,
                                                FpStockGroup = _locSalesOrderMonitoring.StockGroup,
                                                SalesInvoice = _locSalesInvoiceXPO,
                                            };
                                            _saveDataSalesInvoiceFreeItem.Save();
                                            _saveDataSalesInvoiceFreeItem.Session.CommitTransaction();

                                            SalesInvoiceFreeItem _locSalInvFreeItem = _currSession.FindObject<SalesInvoiceFreeItem>
                                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                                    new BinaryOperator("SignCode", _currFpSignCode)));
                                            if (_locSalInvFreeItem != null)
                                            {
                                                SetStatusSalesOrderMonitoringFreeItem(_currSession, _locSalesOrderMonitoring, _locSalInvFreeItem);
                                            }

                                            _locSalesInvoiceXPO.TotFIAmount = _locSalesOrderMonitoring.SalesOrder.TotFIAmount;
                                            _locSalesInvoiceXPO.Save();
                                            _locSalesInvoiceXPO.Session.CommitTransaction();
                                        }

                                    }
                                    #endregion SalesInvoiceFreeItemNotAvailable
                                }
                            }
                        }
                    }
                    #endregion SalesOrderMonitoring
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = SalesInvoice ", ex.ToString());
            }
        }

        private void SetStatusSalesOrderMonitoringNormalItem(Session _currSession, SalesOrderMonitoring _locSalesOrderMonitoringXPO, SalesInvoiceLine _locSalesInvoiceLineXPO)
        {
            try
            {
                DateTime now = DateTime.Now;
                if (_locSalesOrderMonitoringXPO != null)
                {
                    if (_locSalesOrderMonitoringXPO.Status == Status.Open || _locSalesOrderMonitoringXPO.Status == Status.Progress)
                    {
                        _locSalesOrderMonitoringXPO.ActivationPosting = true;
                        _locSalesOrderMonitoringXPO.Select = false;
                        _locSalesOrderMonitoringXPO.SalesInvoiceLine = _locSalesInvoiceLineXPO;
                        _locSalesOrderMonitoringXPO.PostedCount = _locSalesOrderMonitoringXPO.PostedCount + 1;
                        _locSalesOrderMonitoringXPO.Status = Status.Posted;
                        _locSalesOrderMonitoringXPO.StatusDate = now;
                        _locSalesOrderMonitoringXPO.Save();
                        _locSalesOrderMonitoringXPO.Session.CommitTransaction();
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = SalesInvoice " + ex.ToString());
            }
        }

        private void SetStatusSalesOrderMonitoringFreeItem(Session _currSession, SalesOrderMonitoring _locSalesOrderMonitoringXPO, SalesInvoiceFreeItem _locSalesInvoiceFreeItemXPO)
        {
            try
            {
                DateTime now = DateTime.Now;
                if (_locSalesOrderMonitoringXPO != null)
                {
                    if (_locSalesOrderMonitoringXPO.Status == Status.Open || _locSalesOrderMonitoringXPO.Status == Status.Progress)
                    {
                        _locSalesOrderMonitoringXPO.ActivationPosting = true;
                        _locSalesOrderMonitoringXPO.Select = false;
                        _locSalesOrderMonitoringXPO.SalesInvoiceFreeItem = _locSalesInvoiceFreeItemXPO;
                        _locSalesOrderMonitoringXPO.PostedCount = _locSalesOrderMonitoringXPO.PostedCount + 1;
                        _locSalesOrderMonitoringXPO.Status = Status.Posted;
                        _locSalesOrderMonitoringXPO.StatusDate = now;
                        _locSalesOrderMonitoringXPO.Save();
                        _locSalesOrderMonitoringXPO.Session.CommitTransaction();
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = SalesInvoice " + ex.ToString());
            }
        }

        #endregion GetSI

        #region GetAmount

        private void SetTotalAmountAndTaxAmountAndDiscountAmount(Session _currSession, SalesInvoice _locSalesInvoiceXPO)
        {
            try
            {
                double _locTAmount = 0;
                double _locTUAmount = 0;
                double _locDiscAmountRule2 = 0;
                double _locTxAmount = 0;
                double _locDiscAmountRule1 = 0;
                if (_locSalesInvoiceXPO != null)
                {
                    XPCollection<SalesInvoiceLine> _locSalesInvoiceLines = new XPCollection<SalesInvoiceLine>
                                                                                (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                new BinaryOperator("SalesInvoice", _locSalesInvoiceXPO),
                                                                                new BinaryOperator("Select", true),
                                                                                new GroupOperator(GroupOperatorType.Or,
                                                                                new BinaryOperator("Status", Status.Open),
                                                                                new BinaryOperator("Status", Status.Progress))));
                    if (_locSalesInvoiceLines != null && _locSalesInvoiceLines.Count() > 0)
                    {
                        foreach (SalesInvoiceLine _locSalesInvoiceLine in _locSalesInvoiceLines)
                        {
                            _locTAmount = _locTAmount + _locSalesInvoiceLine.TAmount;
                            _locTUAmount = _locTUAmount + _locSalesInvoiceLine.TUAmount;
                            _locDiscAmountRule2 = _locDiscAmountRule2 + _locSalesInvoiceLine.DiscAmount;
                            if (_locSalesInvoiceLine.Tax != null)
                            {
                                if (_locSalesInvoiceLine.Tax.TaxNature == TaxNature.Increase)
                                {
                                    _locTxAmount = _locTxAmount + _locSalesInvoiceLine.TxAmount;
                                }
                                if (_locSalesInvoiceLine.Tax.TaxNature == TaxNature.Decrease)
                                {
                                    _locTxAmount = _locTxAmount - _locSalesInvoiceLine.TxAmount;
                                }
                            }
                        }

                        _locDiscAmountRule1 = GetDiscountRule1(_currSession, _locSalesInvoiceXPO);
                    }

                    _locSalesInvoiceXPO.TotUnitAmount = _locTUAmount;
                    _locSalesInvoiceXPO.TotAmount = _locTAmount;
                    _locSalesInvoiceXPO.AmountDisc = _locDiscAmountRule1;
                    _locSalesInvoiceXPO.TotDiscAmount = (_locDiscAmountRule2 + _locDiscAmountRule1);
                    _locSalesInvoiceXPO.TotTaxAmount = _locTxAmount;
                    _locSalesInvoiceXPO.Amount = _locTAmount - _locDiscAmountRule1;
                    _locSalesInvoiceXPO.Save();
                    _locSalesInvoiceXPO.Session.CommitTransaction();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = SalesInvoice ", ex.ToString());
            }

        }

        private void SetOverCreditStatus(Session _currSession, SalesInvoice _locSalesInvoiceXPO)
        {
    
            try
            {
                DateTime now = DateTime.Now;
                if (_locSalesInvoiceXPO != null)
                {
                    if (_locSalesInvoiceXPO.CreditLimitList != null)
                    {
                        if (_locSalesInvoiceXPO.CreditLimitStatus == CreditLimitStatus.None)
                        {
                            if ((_locSalesInvoiceXPO.CreditLimitList.CurrCredit + _locSalesInvoiceXPO.Amount) > _locSalesInvoiceXPO.CreditLimitList.MaxCredit)
                            {
                                _locSalesInvoiceXPO.OverCredit = true;
                                _locSalesInvoiceXPO.Message = "Over Credit";
                                _locSalesInvoiceXPO.CreditLimitStatus = CreditLimitStatus.Hold;
                                _locSalesInvoiceXPO.CreditLimitStatusDate = now;
                                _locSalesInvoiceXPO.Save();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("BusinessObject = SalesInvoice " + ex.ToString());
            }
            
        }

        #endregion GetAmount

        #region FreeItem

        private void SetFreeItem(Session _currSession, SalesInvoice _locSalesInvoiceXPO)
        {
            try
            {
                if (_locSalesInvoiceXPO != null)
                {
                    XPCollection<FreeItemRule> _locFreeItemRules = new XPCollection<FreeItemRule>
                                                            (_currSession, new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("Company", _locSalesInvoiceXPO.Company),
                                                            new BinaryOperator("Workplace", _locSalesInvoiceXPO.Workplace),
                                                            new BinaryOperator("Location", _locSalesInvoiceXPO.Location),
                                                            new BinaryOperator("PriceGroup", _locSalesInvoiceXPO.PriceGroup),
                                                            new BinaryOperator("Active", true)));
                    if (_locFreeItemRules != null && _locFreeItemRules.Count() > 0)
                    {
                        foreach(FreeItemRule _locFreeItemRule in _locFreeItemRules)
                        {
                            #region Header
                            if(_locFreeItemRule.FreeItemType1 == FreeItemType1.Header)
                            {
                                if(_locFreeItemRule.OpenAmount == true)
                                {
                                    //1+1+1+1+1
                                    #region Rule1
                                    if (_locFreeItemRule.RuleType == RuleType.Rule1)
                                    {
                                        double _locQtyFI = 0;
                                        double _locTotQtyFI = 0;
                                        double _locFpDqty = 0;
                                        double _locFpQty = 0;
                                        int _locMaxLoop = 0;
                                        int _locTotMaxLoopVal = 0;
                                        #region Amount
                                        if (_locFreeItemRule.NetAmountRule == true)
                                        {
                                            if (_locSalesInvoiceXPO.TotAmount >= _locFreeItemRule.NetTotalUAmount)
                                            {
                                                _locQtyFI = System.Math.Floor(_locSalesInvoiceXPO.TotAmount / _locFreeItemRule.NetTotalUAmount);
                                            }
                                        }
                                        if (_locFreeItemRule.GrossAmountRule == true)
                                        {
                                            if (_locSalesInvoiceXPO.TotUnitAmount >= _locFreeItemRule.GrossTotalUAmount)
                                            {
                                                _locQtyFI = System.Math.Floor(_locSalesInvoiceXPO.TotUnitAmount / _locFreeItemRule.GrossTotalUAmount);
                                            }
                                        }
                                        #endregion Amount

                                        if (_locFreeItemRule.MaxLoop == 0)
                                        {
                                            _locMaxLoop = (int)_locQtyFI;
                                        }
                                        else
                                        {
                                            if (_locFreeItemRule.MaxLoop > (int)_locQtyFI)
                                            {
                                                _locMaxLoop = (int)_locQtyFI;
                                            }
                                            else
                                            {
                                                _locMaxLoop = _locFreeItemRule.MaxLoop;
                                            }
                                        }

                                        for (int i = 1; i <= _locMaxLoop; i++)
                                        {
                                            _locTotMaxLoopVal = _locTotMaxLoopVal + 1;
                                        }

                                        _locTotQtyFI = _locTotMaxLoopVal * _locFreeItemRule.FpTQty;

                                        if(_locFreeItemRule.FpBegInvLine != null)
                                        {
                                            if((_locFreeItemRule.FpBegInvLine.QtyAvailable - _locFreeItemRule.FpBegInvLine.QtyOnHand) > 0)
                                            {
                                                if ((_locFreeItemRule.FpBegInvLine.QtyAvailable - _locFreeItemRule.FpBegInvLine.QtyOnHand) < _locTotQtyFI)
                                                {
                                                    _locTotQtyFI = _locFreeItemRule.FpBegInvLine.QtyAvailable - _locFreeItemRule.FpBegInvLine.QtyOnHand;
                                                }

                                                if (_locFreeItemRule.FpItem != null && _locFreeItemRule.FpDUOM != null && _locFreeItemRule.FpUOM != null)
                                                {
                                                    ItemUnitOfMeasure _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                                            (new GroupOperator(GroupOperatorType.And,
                                                                             new BinaryOperator("Item", _locFreeItemRule.FpItem),
                                                                             new BinaryOperator("UOM", _locFreeItemRule.FpUOM),
                                                                             new BinaryOperator("DefaultUOM", _locFreeItemRule.FpDUOM),
                                                                             new BinaryOperator("Active", true)));
                                                    if (_locItemUOM != null)
                                                    {
                                                        if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                                        {
                                                            _locFpDqty = _locTotQtyFI % _locItemUOM.DefaultConversion;
                                                            _locFpQty = System.Math.Floor(_locTotQtyFI / _locItemUOM.DefaultConversion);
                                                        }
                                                        else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                                        {
                                                            _locFpDqty = System.Math.Floor(_locTotQtyFI / _locItemUOM.DefaultConversion);
                                                            _locFpQty = (_locTotQtyFI % _locItemUOM.DefaultConversion) * _locItemUOM.Conversion;
                                                        }
                                                        else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                                        {
                                                            _locFpDqty = _locTotQtyFI;
                                                        }
                                                    }
                                                }
                                                else
                                                {
                                                    _locFpDqty = _locTotQtyFI;
                                                }

                                                SalesInvoiceFreeItem _saveDataSalesInvoiceFreeItem = new SalesInvoiceFreeItem(_currSession)
                                                {
                                                    //Organization
                                                    Company = _locSalesInvoiceXPO.Company,
                                                    Workplace = _locSalesInvoiceXPO.Workplace,
                                                    Division = _locSalesInvoiceXPO.Division,
                                                    Department = _locSalesInvoiceXPO.Department,
                                                    Section = _locSalesInvoiceXPO.Section,
                                                    Employee = _locSalesInvoiceXPO.Employee,
                                                    //Item
                                                    SalesInvoice = _locSalesInvoiceXPO,
                                                    Location = _locSalesInvoiceXPO.Location,
                                                    BegInv = _locFreeItemRule.FpBegInv,
                                                    FpLocationType = _locFreeItemRule.LocationType,
                                                    FpStockType = _locFreeItemRule.StockType,
                                                    FpStockGroup = _locFreeItemRule.FpStockGroup,
                                                    FreeItemRule = _locFreeItemRule,
                                                    FpItem = _locFreeItemRule.FpItem,
                                                    FpDQty = _locFpDqty,
                                                    FpDUOM = _locFreeItemRule.FpDUOM,
                                                    FpQty = _locFpQty,
                                                    FpUOM = _locFreeItemRule.FpUOM,
                                                    FpTQty = _locTotQtyFI,
                                                    BegInvLine = _locFreeItemRule.FpBegInvLine,
                                                    //Amount
                                                    Currency = _locSalesInvoiceXPO.Currency,
                                                    PriceGroup = _locSalesInvoiceXPO.PriceGroup,
                                                    PriceLine = GetPriceLine(_currSession, _locSalesInvoiceXPO, _locFreeItemRule.FpItem),
                                                    UAmount = GetPriceLine(_currSession, _locSalesInvoiceXPO, _locFreeItemRule.FpItem).Amount2a,
                                                };
                                                _saveDataSalesInvoiceFreeItem.Save();
                                                _saveDataSalesInvoiceFreeItem.Session.CommitTransaction();

                                                if (_locFreeItemRule.FpBegInv != null && _locFreeItemRule.FpBegInvLine != null)
                                                {
                                                    _locFreeItemRule.FpBegInvLine.QtyOnHand = _locFreeItemRule.FpBegInvLine.QtyOnHand + _locTotQtyFI;
                                                    _locFreeItemRule.FpBegInvLine.Save();
                                                    _locFreeItemRule.FpBegInvLine.Session.CommitTransaction();
                                                }
                                            }
                                        }   
                                    }

                                    #endregion Rule1

                                    //1, 2+1, 3+1, 4+1
                                    #region Rule2
                                    if (_locFreeItemRule.RuleType == RuleType.Rule2)
                                    {
                                        double _locQtyFI = 0;
                                        double _locTotQtyFI = 0;
                                        double _locFpDqty = 0;
                                        double _locFpQty = 0;
                                        int _locMaxLoop = 0;

                                        #region Amount
                                        if (_locFreeItemRule.NetAmountRule == true)
                                        {
                                            if (_locSalesInvoiceXPO.TotAmount >= _locFreeItemRule.NetTotalUAmount)
                                            {
                                                _locQtyFI = System.Math.Floor(_locSalesInvoiceXPO.TotAmount / _locFreeItemRule.NetTotalUAmount);
                                            }
                                        }
                                        if (_locFreeItemRule.GrossAmountRule == true)
                                        {
                                            if (_locSalesInvoiceXPO.TotUnitAmount >= _locFreeItemRule.GrossTotalUAmount)
                                            {
                                                _locQtyFI = System.Math.Floor(_locSalesInvoiceXPO.TotUnitAmount / _locFreeItemRule.GrossTotalUAmount);
                                            }
                                        }
                                        #endregion Amount

                                        if (_locFreeItemRule.MaxLoop == 0)
                                        {
                                            _locMaxLoop = (int)_locQtyFI;
                                        }
                                        else
                                        {
                                            if (_locFreeItemRule.MaxLoop > (int)_locQtyFI)
                                            {
                                                _locMaxLoop = (int)_locQtyFI;
                                            }
                                            else
                                            {
                                                _locMaxLoop = _locFreeItemRule.MaxLoop;
                                            }
                                        }

                                        for (int i = 1; i <= _locMaxLoop; i++)
                                        {
                                            if (i == 1)
                                            {
                                                _locTotQtyFI = _locFreeItemRule.FpTQty;
                                            }
                                            else
                                            {
                                                _locTotQtyFI = _locTotQtyFI + (i + _locFreeItemRule.FpTQty);
                                            }

                                        }

                                        if (_locFreeItemRule.FpBegInvLine != null)
                                        {
                                            if ((_locFreeItemRule.FpBegInvLine.QtyAvailable - _locFreeItemRule.FpBegInvLine.QtyOnHand) > 0)
                                            {
                                                if ((_locFreeItemRule.FpBegInvLine.QtyAvailable - _locFreeItemRule.FpBegInvLine.QtyOnHand) < _locTotQtyFI)
                                                {
                                                    _locTotQtyFI = _locFreeItemRule.FpBegInvLine.QtyAvailable - _locFreeItemRule.FpBegInvLine.QtyOnHand;
                                                }

                                                if (_locFreeItemRule.FpItem != null && _locFreeItemRule.FpDUOM != null && _locFreeItemRule.FpUOM != null)
                                                {
                                                    ItemUnitOfMeasure _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                                            (new GroupOperator(GroupOperatorType.And,
                                                                             new BinaryOperator("Item", _locFreeItemRule.FpItem),
                                                                             new BinaryOperator("UOM", _locFreeItemRule.FpUOM),
                                                                             new BinaryOperator("DefaultUOM", _locFreeItemRule.FpDUOM),
                                                                             new BinaryOperator("Active", true)));
                                                    if (_locItemUOM != null)
                                                    {
                                                        if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                                        {
                                                            _locFpDqty = _locTotQtyFI % _locItemUOM.DefaultConversion;
                                                            _locFpQty = System.Math.Floor(_locTotQtyFI / _locItemUOM.DefaultConversion);
                                                        }
                                                        else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                                        {
                                                            _locFpDqty = System.Math.Floor(_locTotQtyFI / _locItemUOM.DefaultConversion);
                                                            _locFpQty = (_locTotQtyFI % _locItemUOM.DefaultConversion) * _locItemUOM.Conversion;
                                                        }
                                                        else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                                        {
                                                            _locFpDqty = _locTotQtyFI;
                                                        }
                                                    }
                                                }
                                                else
                                                {
                                                    _locFpDqty = _locTotQtyFI;
                                                }

                                                SalesInvoiceFreeItem _saveDataSalesInvoiceFreeItem = new SalesInvoiceFreeItem(_currSession)
                                                {
                                                    //Organization
                                                    Company = _locSalesInvoiceXPO.Company,
                                                    Workplace = _locSalesInvoiceXPO.Workplace,
                                                    Division = _locSalesInvoiceXPO.Division,
                                                    Department = _locSalesInvoiceXPO.Department,
                                                    Section = _locSalesInvoiceXPO.Section,
                                                    Employee = _locSalesInvoiceXPO.Employee,
                                                    //Item
                                                    SalesInvoice = _locSalesInvoiceXPO,
                                                    Location = _locSalesInvoiceXPO.Location,
                                                    BegInv = _locFreeItemRule.FpBegInv,
                                                    FpLocationType = _locFreeItemRule.LocationType,
                                                    FpStockType = _locFreeItemRule.StockType,
                                                    FpStockGroup = _locFreeItemRule.FpStockGroup,
                                                    FreeItemRule = _locFreeItemRule,
                                                    FpItem = _locFreeItemRule.FpItem,
                                                    FpDQty = _locFpDqty,
                                                    FpDUOM = _locFreeItemRule.FpDUOM,
                                                    FpQty = _locFpQty,
                                                    FpUOM = _locFreeItemRule.FpUOM,
                                                    FpTQty = _locTotQtyFI,
                                                    BegInvLine = _locFreeItemRule.FpBegInvLine,
                                                    //Amount
                                                    Currency = _locSalesInvoiceXPO.Currency,
                                                    PriceGroup = _locSalesInvoiceXPO.PriceGroup,
                                                    PriceLine = GetPriceLine(_currSession, _locSalesInvoiceXPO, _locFreeItemRule.FpItem),
                                                    UAmount = GetPriceLine(_currSession, _locSalesInvoiceXPO, _locFreeItemRule.FpItem).Amount2a,
                                                };
                                                _saveDataSalesInvoiceFreeItem.Save();
                                                _saveDataSalesInvoiceFreeItem.Session.CommitTransaction();

                                                if (_locFreeItemRule.FpBegInv != null && _locFreeItemRule.FpBegInvLine != null)
                                                {
                                                    _locFreeItemRule.FpBegInvLine.QtyOnHand = _locFreeItemRule.FpBegInvLine.QtyOnHand + _locTotQtyFI;
                                                    _locFreeItemRule.FpBegInvLine.Save();
                                                    _locFreeItemRule.FpBegInvLine.Session.CommitTransaction();
                                                }
                                            }
                                        }
                                    }
                                    #endregion Rule2

                                    //2 * 2 * 2 * 2
                                    #region Rule3
                                    if (_locFreeItemRule.RuleType == RuleType.Rule3)
                                    {
                                        double _locQtyFI = 0;
                                        double _locTotQtyFI = 0;
                                        double _locFpDqty = 0;
                                        double _locFpQty = 0;
                                        int _locMaxLoop = 0;

                                        #region Amount
                                        if (_locFreeItemRule.NetAmountRule == true)
                                        {
                                            if (_locSalesInvoiceXPO.TotAmount >= _locFreeItemRule.NetTotalUAmount)
                                            {
                                                _locQtyFI = System.Math.Floor(_locSalesInvoiceXPO.TotAmount / _locFreeItemRule.NetTotalUAmount);
                                            }
                                        }
                                        if (_locFreeItemRule.GrossAmountRule == true)
                                        {
                                            if (_locSalesInvoiceXPO.TotUnitAmount >= _locFreeItemRule.GrossTotalUAmount)
                                            {
                                                _locQtyFI = System.Math.Floor(_locSalesInvoiceXPO.TotUnitAmount / _locFreeItemRule.GrossTotalUAmount);
                                            }
                                        }
                                        #endregion Amount

                                        if (_locFreeItemRule.MaxLoop == 0)
                                        {
                                            _locMaxLoop = (int)_locQtyFI;
                                        }
                                        else
                                        {
                                            if (_locFreeItemRule.MaxLoop > (int)_locQtyFI)
                                            {
                                                _locMaxLoop = (int)_locQtyFI;
                                            }
                                            else
                                            {
                                                _locMaxLoop = _locFreeItemRule.MaxLoop;
                                            }
                                        }

                                        for (int i = 1; i <= _locMaxLoop; i++)
                                        {
                                            _locTotQtyFI = _locTotQtyFI * _locFreeItemRule.FpTQty;
                                        }

                                        if (_locFreeItemRule.FpBegInvLine != null)
                                        {
                                            if ((_locFreeItemRule.FpBegInvLine.QtyAvailable - _locFreeItemRule.FpBegInvLine.QtyOnHand) > 0)
                                            {
                                                if ((_locFreeItemRule.FpBegInvLine.QtyAvailable - _locFreeItemRule.FpBegInvLine.QtyOnHand) < _locTotQtyFI)
                                                {
                                                    _locTotQtyFI = _locFreeItemRule.FpBegInvLine.QtyAvailable - _locFreeItemRule.FpBegInvLine.QtyOnHand;
                                                }

                                                if (_locFreeItemRule.FpItem != null && _locFreeItemRule.FpDUOM != null && _locFreeItemRule.FpUOM != null)
                                                {
                                                    ItemUnitOfMeasure _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                                            (new GroupOperator(GroupOperatorType.And,
                                                                             new BinaryOperator("Item", _locFreeItemRule.FpItem),
                                                                             new BinaryOperator("UOM", _locFreeItemRule.FpUOM),
                                                                             new BinaryOperator("DefaultUOM", _locFreeItemRule.FpDUOM),
                                                                             new BinaryOperator("Active", true)));
                                                    if (_locItemUOM != null)
                                                    {
                                                        if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                                        {
                                                            _locFpDqty = _locTotQtyFI % _locItemUOM.DefaultConversion;
                                                            _locFpQty = System.Math.Floor(_locTotQtyFI / _locItemUOM.DefaultConversion);
                                                        }
                                                        else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                                        {
                                                            _locFpDqty = System.Math.Floor(_locTotQtyFI / _locItemUOM.DefaultConversion);
                                                            _locFpQty = (_locTotQtyFI % _locItemUOM.DefaultConversion) * _locItemUOM.Conversion;
                                                        }
                                                        else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                                        {
                                                            _locFpDqty = _locTotQtyFI;
                                                        }
                                                    }
                                                }
                                                else
                                                {
                                                    _locFpDqty = _locTotQtyFI;
                                                }

                                                SalesInvoiceFreeItem _saveDataSalesInvoiceFreeItem = new SalesInvoiceFreeItem(_currSession)
                                                {
                                                    //Organization
                                                    Company = _locSalesInvoiceXPO.Company,
                                                    Workplace = _locSalesInvoiceXPO.Workplace,
                                                    Division = _locSalesInvoiceXPO.Division,
                                                    Department = _locSalesInvoiceXPO.Department,
                                                    Section = _locSalesInvoiceXPO.Section,
                                                    Employee = _locSalesInvoiceXPO.Employee,
                                                    //Item
                                                    SalesInvoice = _locSalesInvoiceXPO,
                                                    Location = _locSalesInvoiceXPO.Location,
                                                    BegInv = _locFreeItemRule.FpBegInv,
                                                    FpLocationType = _locFreeItemRule.LocationType,
                                                    FpStockType = _locFreeItemRule.StockType,
                                                    FpStockGroup = _locFreeItemRule.FpStockGroup,
                                                    FreeItemRule = _locFreeItemRule,
                                                    FpItem = _locFreeItemRule.FpItem,
                                                    FpDQty = _locFpDqty,
                                                    FpDUOM = _locFreeItemRule.FpDUOM,
                                                    FpQty = _locFpQty,
                                                    FpUOM = _locFreeItemRule.FpUOM,
                                                    FpTQty = _locTotQtyFI,
                                                    BegInvLine = _locFreeItemRule.FpBegInvLine,
                                                    //Amount
                                                    Currency = _locSalesInvoiceXPO.Currency,
                                                    PriceGroup = _locSalesInvoiceXPO.PriceGroup,
                                                    PriceLine = GetPriceLine(_currSession, _locSalesInvoiceXPO, _locFreeItemRule.FpItem),
                                                    UAmount = GetPriceLine(_currSession, _locSalesInvoiceXPO, _locFreeItemRule.FpItem).Amount2a,
                                                };
                                                _saveDataSalesInvoiceFreeItem.Save();
                                                _saveDataSalesInvoiceFreeItem.Session.CommitTransaction();

                                                if (_locFreeItemRule.FpBegInv != null && _locFreeItemRule.FpBegInvLine != null)
                                                {
                                                    _locFreeItemRule.FpBegInvLine.QtyOnHand = _locFreeItemRule.FpBegInvLine.QtyOnHand + _locTotQtyFI;
                                                    _locFreeItemRule.FpBegInvLine.Save();
                                                    _locFreeItemRule.FpBegInvLine.Session.CommitTransaction();
                                                }
                                            }
                                        } 
                                    }
                                    #endregion Rule3

                                    //2^(1-1) + 2^(2-1) + 2^(3-1) + 2^(4-1)
                                    #region Rule4
                                    if (_locFreeItemRule.RuleType == RuleType.Rule4)
                                    {
                                        double _locQtyFI = 0;
                                        double _locTotQtyFI = 0;
                                        double _locFpDqty = 0;
                                        double _locFpQty = 0;
                                        int _locMaxLoop = 0;
                                        #region Amount
                                        if (_locFreeItemRule.NetAmountRule == true)
                                        {
                                            if (_locSalesInvoiceXPO.TotAmount >= _locFreeItemRule.NetTotalUAmount)
                                            {
                                                _locQtyFI = System.Math.Floor(_locSalesInvoiceXPO.TotAmount / _locFreeItemRule.NetTotalUAmount);
                                            }
                                        }
                                        if (_locFreeItemRule.GrossAmountRule == true)
                                        {
                                            if (_locSalesInvoiceXPO.TotUnitAmount >= _locFreeItemRule.GrossTotalUAmount)
                                            {
                                                _locQtyFI = System.Math.Floor(_locSalesInvoiceXPO.TotUnitAmount / _locFreeItemRule.GrossTotalUAmount);
                                            }
                                        }
                                        #endregion Amount


                                        if (_locFreeItemRule.MaxLoop == 0)
                                        {
                                            _locMaxLoop = (int)_locQtyFI;
                                        }
                                        else
                                        {
                                            if (_locFreeItemRule.MaxLoop > (int)_locQtyFI)
                                            {
                                                _locMaxLoop = (int)_locQtyFI;
                                            }
                                            else
                                            {
                                                _locMaxLoop = _locFreeItemRule.MaxLoop;
                                            }
                                        }

                                        for (int i = 1; i <= _locMaxLoop; i++)
                                        {

                                            _locTotQtyFI = _locTotQtyFI + System.Math.Pow(_locFreeItemRule.FpTQty, i - 1);
                                        }

                                        if (_locFreeItemRule.FpBegInvLine != null)
                                        {
                                            if ((_locFreeItemRule.FpBegInvLine.QtyAvailable - _locFreeItemRule.FpBegInvLine.QtyOnHand) > 0)
                                            {
                                                if ((_locFreeItemRule.FpBegInvLine.QtyAvailable - _locFreeItemRule.FpBegInvLine.QtyOnHand) < _locTotQtyFI)
                                                {
                                                    _locTotQtyFI = _locFreeItemRule.FpBegInvLine.QtyAvailable - _locFreeItemRule.FpBegInvLine.QtyOnHand;
                                                }

                                                if (_locFreeItemRule.FpItem != null && _locFreeItemRule.FpDUOM != null && _locFreeItemRule.FpUOM != null)
                                                {
                                                    ItemUnitOfMeasure _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                                            (new GroupOperator(GroupOperatorType.And,
                                                                             new BinaryOperator("Item", _locFreeItemRule.FpItem),
                                                                             new BinaryOperator("UOM", _locFreeItemRule.FpUOM),
                                                                             new BinaryOperator("DefaultUOM", _locFreeItemRule.FpDUOM),
                                                                             new BinaryOperator("Active", true)));
                                                    if (_locItemUOM != null)
                                                    {
                                                        if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                                        {
                                                            _locFpDqty = _locTotQtyFI % _locItemUOM.DefaultConversion;
                                                            _locFpQty = System.Math.Floor(_locTotQtyFI / _locItemUOM.DefaultConversion);
                                                        }
                                                        else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                                        {
                                                            _locFpDqty = System.Math.Floor(_locTotQtyFI / _locItemUOM.DefaultConversion);
                                                            _locFpQty = (_locTotQtyFI % _locItemUOM.DefaultConversion) * _locItemUOM.Conversion;
                                                        }
                                                        else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                                        {
                                                            _locFpDqty = _locTotQtyFI;
                                                        }
                                                    }
                                                }
                                                else
                                                {
                                                    _locFpDqty = _locTotQtyFI;
                                                }

                                                SalesInvoiceFreeItem _saveDataSalesInvoiceFreeItem = new SalesInvoiceFreeItem(_currSession)
                                                {
                                                    //Organization
                                                    Company = _locSalesInvoiceXPO.Company,
                                                    Workplace = _locSalesInvoiceXPO.Workplace,
                                                    Division = _locSalesInvoiceXPO.Division,
                                                    Department = _locSalesInvoiceXPO.Department,
                                                    Section = _locSalesInvoiceXPO.Section,
                                                    Employee = _locSalesInvoiceXPO.Employee,
                                                    //Item
                                                    SalesInvoice = _locSalesInvoiceXPO,
                                                    Location = _locSalesInvoiceXPO.Location,
                                                    BegInv = _locFreeItemRule.FpBegInv,
                                                    FpLocationType = _locFreeItemRule.LocationType,
                                                    FpStockType = _locFreeItemRule.StockType,
                                                    FpStockGroup = _locFreeItemRule.FpStockGroup,
                                                    FreeItemRule = _locFreeItemRule,
                                                    FpItem = _locFreeItemRule.FpItem,
                                                    FpDQty = _locFpDqty,
                                                    FpDUOM = _locFreeItemRule.FpDUOM,
                                                    FpQty = _locFpQty,
                                                    FpUOM = _locFreeItemRule.FpUOM,
                                                    FpTQty = _locTotQtyFI,
                                                    BegInvLine = _locFreeItemRule.FpBegInvLine,
                                                    //Amount
                                                    Currency = _locSalesInvoiceXPO.Currency,
                                                    PriceGroup = _locSalesInvoiceXPO.PriceGroup,
                                                    PriceLine = GetPriceLine(_currSession, _locSalesInvoiceXPO, _locFreeItemRule.FpItem),
                                                    UAmount = GetPriceLine(_currSession, _locSalesInvoiceXPO, _locFreeItemRule.FpItem).Amount2a,
                                                };
                                                _saveDataSalesInvoiceFreeItem.Save();
                                                _saveDataSalesInvoiceFreeItem.Session.CommitTransaction();

                                                if (_locFreeItemRule.FpBegInv != null && _locFreeItemRule.FpBegInvLine != null)
                                                {
                                                    _locFreeItemRule.FpBegInvLine.QtyOnHand = _locFreeItemRule.FpBegInvLine.QtyOnHand + _locTotQtyFI;
                                                    _locFreeItemRule.FpBegInvLine.Save();
                                                    _locFreeItemRule.FpBegInvLine.Session.CommitTransaction();
                                                }
                                            }
                                        }  
                                    }
                                    #endregion Rule4
                                }
                            }
                            #endregion Header
                            #region Line
                            if(_locFreeItemRule.FreeItemType1 == FreeItemType1.Line)
                            {
                                XPQuery<SalesInvoiceLine> _salesInvoiceLinesQuery = new XPQuery<SalesInvoiceLine>(_currSession);

                                var _salesInvoiceLines = from sil in _salesInvoiceLinesQuery
                                                         where (sil.SalesInvoice == _locSalesInvoiceXPO
                                                         && sil.Status == Status.Progress
                                                         && sil.Select == true
                                                         )
                                                         group sil by sil.Item into g
                                                         select new { Item = g.Key };

                                if (_salesInvoiceLines != null && _salesInvoiceLines.Count() > 0)
                                {
                                    foreach (var _salesInvoiceLine in _salesInvoiceLines)
                                    {
                                        #region MultiItem=false
                                        if (_locFreeItemRule.MultiItem == false)
                                        {
                                            if (_locFreeItemRule.MpItem == _salesInvoiceLine.Item)
                                            {
                                                //1+1+1+1+1
                                                #region Rule1
                                                if (_locFreeItemRule.RuleType == RuleType.Rule1)
                                                {
                                                    XPCollection<SalesInvoiceLine> _locSalesInvoiceLines = new XPCollection<SalesInvoiceLine>
                                                                                                           (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                           new BinaryOperator("SalesInvoice", _locSalesInvoiceXPO),
                                                                                                           new BinaryOperator("Item", _salesInvoiceLine.Item),
                                                                                                           new BinaryOperator("Status", Status.Progress),
                                                                                                           new BinaryOperator("FreeItemChecked", false),
                                                                                                           new BinaryOperator("Select", true)));

                                                    if (_locSalesInvoiceLines != null && _locSalesInvoiceLines.Count() > 0)
                                                    {
                                                        #region Foreach
                                                        foreach (SalesInvoiceLine _locSalesInvoiceLine in _locSalesInvoiceLines)
                                                        {
                                                            double _locQtyFI = 0;
                                                            double _locTotQtyFI = 0;
                                                            double _locFpDqty = 0;
                                                            double _locFpQty = 0;
                                                            int _locMaxLoop = 0;
                                                            int _locTotMaxLoopVal = 0;

                                                            if (_locSalesInvoiceLine.TQty >= _locFreeItemRule.MpTQty)
                                                            {
                                                                #region Amount
                                                                if (_locFreeItemRule.OpenAmount == true)
                                                                {
                                                                    if (_locFreeItemRule.NetAmountRule == true)
                                                                    {
                                                                        if (_locSalesInvoiceLine.TAmount >= _locFreeItemRule.NetTotalUAmount)
                                                                        {
                                                                            _locQtyFI = System.Math.Floor(_locSalesInvoiceLine.TAmount / _locFreeItemRule.NetTotalUAmount);
                                                                        }
                                                                    }
                                                                    if (_locFreeItemRule.GrossAmountRule == true)
                                                                    {
                                                                        if (_locSalesInvoiceLine.TUAmount >= _locFreeItemRule.GrossTotalUAmount)
                                                                        {
                                                                            _locQtyFI = System.Math.Floor(_locSalesInvoiceLine.TUAmount / _locFreeItemRule.GrossTotalUAmount);
                                                                        }
                                                                    }

                                                                }
                                                                #endregion Amount
                                                                #region Item
                                                                else
                                                                {
                                                                    if (_locSalesInvoiceLine.TQty >= _locFreeItemRule.MpTQty)
                                                                    {
                                                                        _locQtyFI = System.Math.Floor(_locSalesInvoiceLine.TQty / _locFreeItemRule.MpTQty);
                                                                    }
                                                                }
                                                                #endregion Item

                                                                if (_locFreeItemRule.MaxLoop == 0)
                                                                {
                                                                    _locMaxLoop = (int)_locQtyFI;
                                                                }
                                                                else
                                                                {
                                                                    if (_locFreeItemRule.MaxLoop > (int)_locQtyFI)
                                                                    {
                                                                        _locMaxLoop = (int)_locQtyFI;
                                                                    }
                                                                    else
                                                                    {
                                                                        _locMaxLoop = _locFreeItemRule.MaxLoop;
                                                                    }
                                                                }

                                                                for (int i = 1; i <= _locMaxLoop; i++)
                                                                {
                                                                    _locTotMaxLoopVal = _locTotMaxLoopVal + 1;
                                                                }

                                                                _locTotQtyFI = _locTotMaxLoopVal * _locFreeItemRule.FpTQty;

                                                                if(_locFreeItemRule.FpBegInvLine != null)
                                                                {
                                                                    if((_locFreeItemRule.FpBegInvLine.QtyAvailable - _locFreeItemRule.FpBegInvLine.QtyOnHand) > 0)
                                                                    {
                                                                        if ((_locFreeItemRule.FpBegInvLine.QtyAvailable - _locFreeItemRule.FpBegInvLine.QtyOnHand) < _locTotQtyFI)
                                                                        {
                                                                            _locTotQtyFI = _locFreeItemRule.FpBegInvLine.QtyAvailable - _locFreeItemRule.FpBegInvLine.QtyOnHand;
                                                                        }

                                                                        if (_locFreeItemRule.FpItem != null && _locFreeItemRule.FpDUOM != null && _locFreeItemRule.FpUOM != null)
                                                                        {
                                                                            ItemUnitOfMeasure _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                                                     new BinaryOperator("Item", _locFreeItemRule.FpItem),
                                                                                                     new BinaryOperator("UOM", _locFreeItemRule.FpUOM),
                                                                                                     new BinaryOperator("DefaultUOM", _locFreeItemRule.FpDUOM),
                                                                                                     new BinaryOperator("Active", true)));
                                                                            if (_locItemUOM != null)
                                                                            {
                                                                                if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                                                                {
                                                                                    _locFpDqty = _locTotQtyFI % _locItemUOM.DefaultConversion;
                                                                                    _locFpQty = System.Math.Floor(_locTotQtyFI / _locItemUOM.DefaultConversion);
                                                                                }
                                                                                else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                                                                {
                                                                                    _locFpDqty = System.Math.Floor(_locTotQtyFI / _locItemUOM.DefaultConversion);
                                                                                    _locFpQty = (_locTotQtyFI % _locItemUOM.DefaultConversion) * _locItemUOM.Conversion;
                                                                                }
                                                                                else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                                                                {
                                                                                    _locFpDqty = _locTotQtyFI;
                                                                                }
                                                                            }
                                                                        }
                                                                        else
                                                                        {
                                                                            _locFpDqty = _locTotQtyFI;
                                                                        }

                                                                        SalesInvoiceFreeItem _saveDataSalesInvoiceFreeItem = new SalesInvoiceFreeItem(_currSession)
                                                                        {
                                                                            //Organization
                                                                            Company = _locSalesInvoiceXPO.Company,
                                                                            Workplace = _locSalesInvoiceXPO.Workplace,
                                                                            Division = _locSalesInvoiceXPO.Division,
                                                                            Department = _locSalesInvoiceXPO.Department,
                                                                            Section = _locSalesInvoiceXPO.Section,
                                                                            Employee = _locSalesInvoiceXPO.Employee,
                                                                            //Item
                                                                            SalesInvoice = _locSalesInvoiceXPO,
                                                                            SalesInvoiceLine = _locSalesInvoiceLine,
                                                                            Location = _locSalesInvoiceXPO.Location,
                                                                            BegInv = _locFreeItemRule.FpBegInv,
                                                                            FpLocationType = _locFreeItemRule.LocationType,
                                                                            FpStockType = _locFreeItemRule.StockType,
                                                                            FpStockGroup = _locFreeItemRule.FpStockGroup,
                                                                            FreeItemRule = _locFreeItemRule,
                                                                            FpItem = _locFreeItemRule.FpItem,
                                                                            FpDQty = _locFpDqty,
                                                                            FpDUOM = _locFreeItemRule.FpDUOM,
                                                                            FpQty = _locFpQty,
                                                                            FpUOM = _locFreeItemRule.FpUOM,
                                                                            FpTQty = _locTotQtyFI,
                                                                            BegInvLine = _locFreeItemRule.FpBegInvLine,
                                                                            //Amount
                                                                            Currency = _locSalesInvoiceXPO.Currency,
                                                                            PriceGroup = _locSalesInvoiceXPO.PriceGroup,
                                                                            PriceLine = GetPriceLine(_currSession, _locSalesInvoiceXPO, _locFreeItemRule.FpItem),
                                                                            UAmount = GetPriceLine(_currSession, _locSalesInvoiceXPO, _locFreeItemRule.FpItem).Amount2a,
                                                                        };
                                                                        _saveDataSalesInvoiceFreeItem.Save();
                                                                        _saveDataSalesInvoiceFreeItem.Session.CommitTransaction();

                                                                        _locSalesInvoiceLine.FreeItemChecked = true;
                                                                        _locSalesInvoiceLine.Save();
                                                                        _locSalesInvoiceLine.Session.CommitTransaction();

                                                                        if (_locFreeItemRule.FpBegInv != null && _locFreeItemRule.FpBegInvLine != null)
                                                                        {
                                                                            _locFreeItemRule.FpBegInvLine.QtyOnHand = _locFreeItemRule.FpBegInvLine.QtyOnHand + _locTotQtyFI;
                                                                            _locFreeItemRule.FpBegInvLine.Save();
                                                                            _locFreeItemRule.FpBegInvLine.Session.CommitTransaction();
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                        #endregion Foreach
                                                    }
                                                }

                                                #endregion Rule1

                                                //1, 2+1, 3+1, 4+1
                                                #region Rule2
                                                if (_locFreeItemRule.RuleType == RuleType.Rule2)
                                                {
                                                    XPCollection<SalesInvoiceLine> _locSalesInvoiceLines = new XPCollection<SalesInvoiceLine>
                                                                                                           (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                           new BinaryOperator("SalesInvoice", _locSalesInvoiceXPO),
                                                                                                           new BinaryOperator("Item", _salesInvoiceLine.Item),
                                                                                                           new BinaryOperator("Status", Status.Progress),
                                                                                                           new BinaryOperator("FreeItemChecked", false),
                                                                                                           new BinaryOperator("Select", true)));

                                                    if (_locSalesInvoiceLines != null && _locSalesInvoiceLines.Count() > 0)
                                                    {

                                                        foreach (SalesInvoiceLine _locSalesInvoiceLine in _locSalesInvoiceLines)
                                                        {
                                                            double _locQtyFI = 0;
                                                            double _locTotQtyFI = 0;
                                                            double _locFpDqty = 0;
                                                            double _locFpQty = 0;
                                                            int _locMaxLoop = 0;

                                                            if (_locSalesInvoiceLine.TQty >= _locFreeItemRule.MpTQty)
                                                            {
                                                                #region Amount
                                                                if (_locFreeItemRule.OpenAmount == true)
                                                                {
                                                                    if (_locFreeItemRule.NetAmountRule == true)
                                                                    {
                                                                        if (_locSalesInvoiceLine.TAmount >= _locFreeItemRule.NetTotalUAmount)
                                                                        {
                                                                            _locQtyFI = System.Math.Floor(_locSalesInvoiceLine.TAmount / _locFreeItemRule.NetTotalUAmount);
                                                                        }
                                                                    }
                                                                    if (_locFreeItemRule.GrossAmountRule == true)
                                                                    {
                                                                        if (_locSalesInvoiceLine.TUAmount >= _locFreeItemRule.GrossTotalUAmount)
                                                                        {
                                                                            _locQtyFI = System.Math.Floor(_locSalesInvoiceLine.TUAmount / _locFreeItemRule.GrossTotalUAmount);
                                                                        }
                                                                    }

                                                                }
                                                                #endregion Amount
                                                                #region Item
                                                                else
                                                                {
                                                                    if (_locSalesInvoiceLine.TQty >= _locFreeItemRule.MpTQty)
                                                                    {
                                                                        _locQtyFI = System.Math.Floor(_locSalesInvoiceLine.TQty / _locFreeItemRule.MpTQty);
                                                                    }
                                                                }
                                                                #endregion Item

                                                                if (_locFreeItemRule.MaxLoop == 0)
                                                                {
                                                                    _locMaxLoop = (int)_locQtyFI;
                                                                }
                                                                else
                                                                {
                                                                    if (_locFreeItemRule.MaxLoop > (int)_locQtyFI)
                                                                    {
                                                                        _locMaxLoop = (int)_locQtyFI;
                                                                    }
                                                                    else
                                                                    {
                                                                        _locMaxLoop = _locFreeItemRule.MaxLoop;
                                                                    }
                                                                }

                                                                for (int i = 1; i <= _locMaxLoop; i++)
                                                                {
                                                                    if (i == 1)
                                                                    {
                                                                        _locTotQtyFI = _locFreeItemRule.FpTQty;
                                                                    }
                                                                    else
                                                                    {
                                                                        _locTotQtyFI = _locTotQtyFI + (i + _locFreeItemRule.FpTQty);
                                                                    }

                                                                }

                                                                if (_locFreeItemRule.FpBegInvLine != null)
                                                                {
                                                                    if ((_locFreeItemRule.FpBegInvLine.QtyAvailable - _locFreeItemRule.FpBegInvLine.QtyOnHand) > 0)
                                                                    {
                                                                        if ((_locFreeItemRule.FpBegInvLine.QtyAvailable - _locFreeItemRule.FpBegInvLine.QtyOnHand) < _locTotQtyFI)
                                                                        {
                                                                            _locTotQtyFI = _locFreeItemRule.FpBegInvLine.QtyAvailable - _locFreeItemRule.FpBegInvLine.QtyOnHand;
                                                                        }

                                                                        if (_locFreeItemRule.FpItem != null && _locFreeItemRule.FpDUOM != null && _locFreeItemRule.FpUOM != null)
                                                                        {
                                                                            ItemUnitOfMeasure _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                                                     new BinaryOperator("Item", _locFreeItemRule.FpItem),
                                                                                                     new BinaryOperator("UOM", _locFreeItemRule.FpUOM),
                                                                                                     new BinaryOperator("DefaultUOM", _locFreeItemRule.FpDUOM),
                                                                                                     new BinaryOperator("Active", true)));
                                                                            if (_locItemUOM != null)
                                                                            {
                                                                                if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                                                                {
                                                                                    _locFpDqty = _locTotQtyFI % _locItemUOM.DefaultConversion;
                                                                                    _locFpQty = System.Math.Floor(_locTotQtyFI / _locItemUOM.DefaultConversion);
                                                                                }
                                                                                else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                                                                {
                                                                                    _locFpDqty = System.Math.Floor(_locTotQtyFI / _locItemUOM.DefaultConversion);
                                                                                    _locFpQty = (_locTotQtyFI % _locItemUOM.DefaultConversion) * _locItemUOM.Conversion;
                                                                                }
                                                                                else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                                                                {
                                                                                    _locFpDqty = _locTotQtyFI;
                                                                                }
                                                                            }
                                                                        }
                                                                        else
                                                                        {
                                                                            _locFpDqty = _locTotQtyFI;
                                                                        }

                                                                        SalesInvoiceFreeItem _saveDataSalesInvoiceFreeItem = new SalesInvoiceFreeItem(_currSession)
                                                                        {
                                                                            //Organization
                                                                            Company = _locSalesInvoiceXPO.Company,
                                                                            Workplace = _locSalesInvoiceXPO.Workplace,
                                                                            Division = _locSalesInvoiceXPO.Division,
                                                                            Department = _locSalesInvoiceXPO.Department,
                                                                            Section = _locSalesInvoiceXPO.Section,
                                                                            Employee = _locSalesInvoiceXPO.Employee,
                                                                            //Item
                                                                            SalesInvoice = _locSalesInvoiceXPO,
                                                                            SalesInvoiceLine = _locSalesInvoiceLine,
                                                                            Location = _locSalesInvoiceXPO.Location,
                                                                            BegInv = _locFreeItemRule.FpBegInv,
                                                                            FpLocationType = _locFreeItemRule.LocationType,
                                                                            FpStockType = _locFreeItemRule.StockType,
                                                                            FpStockGroup = _locFreeItemRule.FpStockGroup,
                                                                            FreeItemRule = _locFreeItemRule,
                                                                            FpItem = _locFreeItemRule.FpItem,
                                                                            FpDQty = _locFpDqty,
                                                                            FpDUOM = _locFreeItemRule.FpDUOM,
                                                                            FpQty = _locFpQty,
                                                                            FpUOM = _locFreeItemRule.FpUOM,
                                                                            FpTQty = _locTotQtyFI,
                                                                            BegInvLine = _locFreeItemRule.FpBegInvLine,
                                                                            //Amount
                                                                            Currency = _locSalesInvoiceXPO.Currency,
                                                                            PriceGroup = _locSalesInvoiceXPO.PriceGroup,
                                                                            PriceLine = GetPriceLine(_currSession, _locSalesInvoiceXPO, _locFreeItemRule.FpItem),
                                                                            UAmount = GetPriceLine(_currSession, _locSalesInvoiceXPO, _locFreeItemRule.FpItem).Amount2a,
                                                                        };
                                                                        _saveDataSalesInvoiceFreeItem.Save();
                                                                        _saveDataSalesInvoiceFreeItem.Session.CommitTransaction();

                                                                        _locSalesInvoiceLine.FreeItemChecked = true;
                                                                        _locSalesInvoiceLine.Save();
                                                                        _locSalesInvoiceLine.Session.CommitTransaction();

                                                                        if (_locFreeItemRule.FpBegInv != null && _locFreeItemRule.FpBegInvLine != null)
                                                                        {
                                                                            _locFreeItemRule.FpBegInvLine.QtyOnHand = _locFreeItemRule.FpBegInvLine.QtyOnHand + _locTotQtyFI;
                                                                            _locFreeItemRule.FpBegInvLine.Save();
                                                                            _locFreeItemRule.FpBegInvLine.Session.CommitTransaction();
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                                #endregion Rule2

                                                //2 * 2 * 2 * 2
                                                #region Rule3
                                                if (_locFreeItemRule.RuleType == RuleType.Rule3)
                                                {
                                                    XPCollection<SalesInvoiceLine> _locSalesInvoiceLines = new XPCollection<SalesInvoiceLine>
                                                                                                           (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                           new BinaryOperator("SalesInvoice", _locSalesInvoiceXPO),
                                                                                                           new BinaryOperator("Item", _salesInvoiceLine.Item),
                                                                                                           new BinaryOperator("Status", Status.Progress),
                                                                                                           new BinaryOperator("FreeItemChecked", false),
                                                                                                           new BinaryOperator("Select", true)));

                                                    if (_locSalesInvoiceLines != null && _locSalesInvoiceLines.Count() > 0)
                                                    {

                                                        foreach (SalesInvoiceLine _locSalesInvoiceLine in _locSalesInvoiceLines)
                                                        {
                                                            double _locQtyFI = 0;
                                                            double _locTotQtyFI = 0;
                                                            double _locFpDqty = 0;
                                                            double _locFpQty = 0;
                                                            int _locMaxLoop = 0;

                                                            if (_locSalesInvoiceLine.TQty >= _locFreeItemRule.MpTQty)
                                                            {
                                                                #region Amount
                                                                if (_locFreeItemRule.OpenAmount == true)
                                                                {
                                                                    if (_locFreeItemRule.NetAmountRule == true)
                                                                    {
                                                                        if (_locSalesInvoiceLine.TAmount >= _locFreeItemRule.NetTotalUAmount)
                                                                        {
                                                                            _locQtyFI = System.Math.Floor(_locSalesInvoiceLine.TAmount / _locFreeItemRule.NetTotalUAmount);
                                                                        }
                                                                    }
                                                                    if (_locFreeItemRule.GrossAmountRule == true)
                                                                    {
                                                                        if (_locSalesInvoiceLine.TUAmount >= _locFreeItemRule.GrossTotalUAmount)
                                                                        {
                                                                            _locQtyFI = System.Math.Floor(_locSalesInvoiceLine.TUAmount / _locFreeItemRule.GrossTotalUAmount);
                                                                        }
                                                                    }

                                                                }
                                                                #endregion Amount
                                                                #region Item
                                                                else
                                                                {
                                                                    if (_locSalesInvoiceLine.TQty >= _locFreeItemRule.MpTQty)
                                                                    {
                                                                        _locQtyFI = System.Math.Floor(_locSalesInvoiceLine.TQty / _locFreeItemRule.MpTQty);
                                                                    }
                                                                }
                                                                #endregion Item

                                                                if (_locFreeItemRule.MaxLoop == 0)
                                                                {
                                                                    _locMaxLoop = (int)_locQtyFI;
                                                                }
                                                                else
                                                                {
                                                                    if (_locFreeItemRule.MaxLoop > (int)_locQtyFI)
                                                                    {
                                                                        _locMaxLoop = (int)_locQtyFI;
                                                                    }
                                                                    else
                                                                    {
                                                                        _locMaxLoop = _locFreeItemRule.MaxLoop;
                                                                    }
                                                                }

                                                                for (int i = 1; i <= _locMaxLoop; i++)
                                                                {
                                                                    _locTotQtyFI = _locTotQtyFI * _locFreeItemRule.FpTQty;
                                                                }

                                                                if (_locFreeItemRule.FpBegInvLine != null)
                                                                {
                                                                    if ((_locFreeItemRule.FpBegInvLine.QtyAvailable - _locFreeItemRule.FpBegInvLine.QtyOnHand) > 0)
                                                                    {
                                                                        if ((_locFreeItemRule.FpBegInvLine.QtyAvailable - _locFreeItemRule.FpBegInvLine.QtyOnHand) < _locTotQtyFI)
                                                                        {
                                                                            _locTotQtyFI = _locFreeItemRule.FpBegInvLine.QtyAvailable - _locFreeItemRule.FpBegInvLine.QtyOnHand;
                                                                        }

                                                                        if (_locFreeItemRule.FpItem != null && _locFreeItemRule.FpDUOM != null && _locFreeItemRule.FpUOM != null)
                                                                        {
                                                                            ItemUnitOfMeasure _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                                                     new BinaryOperator("Item", _locFreeItemRule.FpItem),
                                                                                                     new BinaryOperator("UOM", _locFreeItemRule.FpUOM),
                                                                                                     new BinaryOperator("DefaultUOM", _locFreeItemRule.FpDUOM),
                                                                                                     new BinaryOperator("Active", true)));
                                                                            if (_locItemUOM != null)
                                                                            {
                                                                                if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                                                                {
                                                                                    _locFpDqty = _locTotQtyFI % _locItemUOM.DefaultConversion;
                                                                                    _locFpQty = System.Math.Floor(_locTotQtyFI / _locItemUOM.DefaultConversion);
                                                                                }
                                                                                else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                                                                {
                                                                                    _locFpDqty = System.Math.Floor(_locTotQtyFI / _locItemUOM.DefaultConversion);
                                                                                    _locFpQty = (_locTotQtyFI % _locItemUOM.DefaultConversion) * _locItemUOM.Conversion;
                                                                                }
                                                                                else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                                                                {
                                                                                    _locFpDqty = _locTotQtyFI;
                                                                                }
                                                                            }
                                                                        }
                                                                        else
                                                                        {
                                                                            _locFpDqty = _locTotQtyFI;
                                                                        }

                                                                        SalesInvoiceFreeItem _saveDataSalesInvoiceFreeItem = new SalesInvoiceFreeItem(_currSession)
                                                                        {
                                                                            //Organization
                                                                            Company = _locSalesInvoiceXPO.Company,
                                                                            Workplace = _locSalesInvoiceXPO.Workplace,
                                                                            Division = _locSalesInvoiceXPO.Division,
                                                                            Department = _locSalesInvoiceXPO.Department,
                                                                            Section = _locSalesInvoiceXPO.Section,
                                                                            Employee = _locSalesInvoiceXPO.Employee,
                                                                            //Item
                                                                            SalesInvoice = _locSalesInvoiceXPO,
                                                                            SalesInvoiceLine = _locSalesInvoiceLine,
                                                                            Location = _locSalesInvoiceXPO.Location,
                                                                            BegInv = _locFreeItemRule.FpBegInv,
                                                                            FpLocationType = _locFreeItemRule.LocationType,
                                                                            FpStockType = _locFreeItemRule.StockType,
                                                                            FpStockGroup = _locFreeItemRule.FpStockGroup,
                                                                            FreeItemRule = _locFreeItemRule,
                                                                            FpItem = _locFreeItemRule.FpItem,
                                                                            FpDQty = _locFpDqty,
                                                                            FpDUOM = _locFreeItemRule.FpDUOM,
                                                                            FpQty = _locFpQty,
                                                                            FpUOM = _locFreeItemRule.FpUOM,
                                                                            FpTQty = _locTotQtyFI,
                                                                            BegInvLine = _locFreeItemRule.FpBegInvLine,
                                                                            //Amount
                                                                            Currency = _locSalesInvoiceXPO.Currency,
                                                                            PriceGroup = _locSalesInvoiceXPO.PriceGroup,
                                                                            PriceLine = GetPriceLine(_currSession, _locSalesInvoiceXPO, _locFreeItemRule.FpItem),
                                                                            UAmount = GetPriceLine(_currSession, _locSalesInvoiceXPO, _locFreeItemRule.FpItem).Amount2a,
                                                                        };
                                                                        _saveDataSalesInvoiceFreeItem.Save();
                                                                        _saveDataSalesInvoiceFreeItem.Session.CommitTransaction();

                                                                        _locSalesInvoiceLine.FreeItemChecked = true;
                                                                        _locSalesInvoiceLine.Save();
                                                                        _locSalesInvoiceLine.Session.CommitTransaction();

                                                                        if (_locFreeItemRule.FpBegInv != null && _locFreeItemRule.FpBegInvLine != null)
                                                                        {
                                                                            _locFreeItemRule.FpBegInvLine.QtyOnHand = _locFreeItemRule.FpBegInvLine.QtyOnHand + _locTotQtyFI;
                                                                            _locFreeItemRule.FpBegInvLine.Save();
                                                                            _locFreeItemRule.FpBegInvLine.Session.CommitTransaction();
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                                #endregion Rule3

                                                //2^(1-1) + 2^(2-1) + 2^(3-1) + 2^(4-1)
                                                #region Rule4
                                                if (_locFreeItemRule.RuleType == RuleType.Rule4)
                                                {
                                                    XPCollection<SalesInvoiceLine> _locSalesInvoiceLines = new XPCollection<SalesInvoiceLine>
                                                                                                           (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                           new BinaryOperator("SalesInvoice", _locSalesInvoiceXPO),
                                                                                                           new BinaryOperator("Item", _salesInvoiceLine.Item),
                                                                                                           new BinaryOperator("Status", Status.Progress),
                                                                                                           new BinaryOperator("FreeItemChecked", false),
                                                                                                           new BinaryOperator("Select", true)));

                                                    if (_locSalesInvoiceLines != null && _locSalesInvoiceLines.Count() > 0)
                                                    {

                                                        foreach (SalesInvoiceLine _locSalesInvoiceLine in _locSalesInvoiceLines)
                                                        {
                                                            double _locQtyFI = 0;
                                                            double _locTotQtyFI = 0;
                                                            double _locFpDqty = 0;
                                                            double _locFpQty = 0;
                                                            int _locMaxLoop = 0;

                                                            if (_locSalesInvoiceLine.TQty >= _locFreeItemRule.MpTQty)
                                                            {
                                                                #region Amount
                                                                if (_locFreeItemRule.OpenAmount == true)
                                                                {
                                                                    if (_locFreeItemRule.NetAmountRule == true)
                                                                    {
                                                                        if (_locSalesInvoiceLine.TAmount >= _locFreeItemRule.NetTotalUAmount)
                                                                        {
                                                                            _locQtyFI = System.Math.Floor(_locSalesInvoiceLine.TAmount / _locFreeItemRule.NetTotalUAmount);
                                                                        }
                                                                    }
                                                                    if (_locFreeItemRule.GrossAmountRule == true)
                                                                    {
                                                                        if (_locSalesInvoiceLine.TUAmount >= _locFreeItemRule.GrossTotalUAmount)
                                                                        {
                                                                            _locQtyFI = System.Math.Floor(_locSalesInvoiceLine.TUAmount / _locFreeItemRule.GrossTotalUAmount);
                                                                        }
                                                                    }

                                                                }
                                                                #endregion Amount
                                                                #region Item
                                                                else
                                                                {
                                                                    if (_locSalesInvoiceLine.TQty >= _locFreeItemRule.MpTQty)
                                                                    {
                                                                        _locQtyFI = System.Math.Floor(_locSalesInvoiceLine.TQty / _locFreeItemRule.MpTQty);
                                                                    }
                                                                }
                                                                #endregion Item

                                                                if (_locFreeItemRule.MaxLoop == 0)
                                                                {
                                                                    _locMaxLoop = (int)_locQtyFI;
                                                                }
                                                                else
                                                                {
                                                                    if (_locFreeItemRule.MaxLoop > (int)_locQtyFI)
                                                                    {
                                                                        _locMaxLoop = (int)_locQtyFI;
                                                                    }
                                                                    else
                                                                    {
                                                                        _locMaxLoop = _locFreeItemRule.MaxLoop;
                                                                    }
                                                                }

                                                                for (int i = 1; i <= _locMaxLoop; i++)
                                                                {

                                                                    _locTotQtyFI = _locTotQtyFI + System.Math.Pow(_locFreeItemRule.FpTQty, i - 1);
                                                                }

                                                                if (_locFreeItemRule.FpBegInvLine != null)
                                                                {
                                                                    if ((_locFreeItemRule.FpBegInvLine.QtyAvailable - _locFreeItemRule.FpBegInvLine.QtyOnHand) > 0)
                                                                    {
                                                                        if ((_locFreeItemRule.FpBegInvLine.QtyAvailable - _locFreeItemRule.FpBegInvLine.QtyOnHand) < _locTotQtyFI)
                                                                        {
                                                                            _locTotQtyFI = _locFreeItemRule.FpBegInvLine.QtyAvailable - _locFreeItemRule.FpBegInvLine.QtyOnHand;
                                                                        }

                                                                        if (_locFreeItemRule.FpItem != null && _locFreeItemRule.FpDUOM != null && _locFreeItemRule.FpUOM != null)
                                                                        {
                                                                            ItemUnitOfMeasure _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                                                     new BinaryOperator("Item", _locFreeItemRule.FpItem),
                                                                                                     new BinaryOperator("UOM", _locFreeItemRule.FpUOM),
                                                                                                     new BinaryOperator("DefaultUOM", _locFreeItemRule.FpDUOM),
                                                                                                     new BinaryOperator("Active", true)));
                                                                            if (_locItemUOM != null)
                                                                            {
                                                                                if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                                                                {
                                                                                    _locFpDqty = _locTotQtyFI % _locItemUOM.DefaultConversion;
                                                                                    _locFpQty = System.Math.Floor(_locTotQtyFI / _locItemUOM.DefaultConversion);
                                                                                }
                                                                                else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                                                                {
                                                                                    _locFpDqty = System.Math.Floor(_locTotQtyFI / _locItemUOM.DefaultConversion);
                                                                                    _locFpQty = (_locTotQtyFI % _locItemUOM.DefaultConversion) * _locItemUOM.Conversion;
                                                                                }
                                                                                else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                                                                {
                                                                                    _locFpDqty = _locTotQtyFI;
                                                                                }
                                                                            }
                                                                        }
                                                                        else
                                                                        {
                                                                            _locFpDqty = _locTotQtyFI;
                                                                        }

                                                                        SalesInvoiceFreeItem _saveDataSalesInvoiceFreeItem = new SalesInvoiceFreeItem(_currSession)
                                                                        {
                                                                            //Organization
                                                                            Company = _locSalesInvoiceXPO.Company,
                                                                            Workplace = _locSalesInvoiceXPO.Workplace,
                                                                            Division = _locSalesInvoiceXPO.Division,
                                                                            Department = _locSalesInvoiceXPO.Department,
                                                                            Section = _locSalesInvoiceXPO.Section,
                                                                            Employee = _locSalesInvoiceXPO.Employee,
                                                                            //Item
                                                                            SalesInvoice = _locSalesInvoiceXPO,
                                                                            SalesInvoiceLine = _locSalesInvoiceLine,
                                                                            Location = _locSalesInvoiceXPO.Location,
                                                                            BegInv = _locFreeItemRule.FpBegInv,
                                                                            FpLocationType = _locFreeItemRule.LocationType,
                                                                            FpStockType = _locFreeItemRule.StockType,
                                                                            FpStockGroup = _locFreeItemRule.FpStockGroup,
                                                                            FreeItemRule = _locFreeItemRule,
                                                                            FpItem = _locFreeItemRule.FpItem,
                                                                            FpDQty = _locFpDqty,
                                                                            FpDUOM = _locFreeItemRule.FpDUOM,
                                                                            FpQty = _locFpQty,
                                                                            FpUOM = _locFreeItemRule.FpUOM,
                                                                            FpTQty = _locTotQtyFI,
                                                                            BegInvLine = _locFreeItemRule.FpBegInvLine,
                                                                            //Amount
                                                                            Currency = _locSalesInvoiceXPO.Currency,
                                                                            PriceGroup = _locSalesInvoiceXPO.PriceGroup,
                                                                            PriceLine = GetPriceLine(_currSession, _locSalesInvoiceXPO, _locFreeItemRule.FpItem),
                                                                            UAmount = GetPriceLine(_currSession, _locSalesInvoiceXPO, _locFreeItemRule.FpItem).Amount2a,
                                                                        };
                                                                        _saveDataSalesInvoiceFreeItem.Save();
                                                                        _saveDataSalesInvoiceFreeItem.Session.CommitTransaction();

                                                                        _locSalesInvoiceLine.FreeItemChecked = true;
                                                                        _locSalesInvoiceLine.Save();
                                                                        _locSalesInvoiceLine.Session.CommitTransaction();

                                                                        if (_locFreeItemRule.FpBegInv != null && _locFreeItemRule.FpBegInvLine != null)
                                                                        {
                                                                            _locFreeItemRule.FpBegInvLine.QtyOnHand = _locFreeItemRule.FpBegInvLine.QtyOnHand + _locTotQtyFI;
                                                                            _locFreeItemRule.FpBegInvLine.Save();
                                                                            _locFreeItemRule.FpBegInvLine.Session.CommitTransaction();
                                                                        }
                                                                    }
                                                                }  
                                                            }
                                                        }
                                                    }
                                                }
                                                #endregion Rule4
                                            }
                                        }
                                        #endregion MultiItem=false
                                        #region MultiItem=true
                                        else
                                        {
                                            SalesInvoiceLine _locComboSalesInvoiceLine = _currSession.FindObject<SalesInvoiceLine>
                                                                                                        (new GroupOperator(GroupOperatorType.And,
                                                                                                        new BinaryOperator("SalesInvoice", _locSalesInvoiceXPO),
                                                                                                        new BinaryOperator("Item", _locFreeItemRule.CpItem),
                                                                                                        new BinaryOperator("Status", Status.Progress),
                                                                                                        new BinaryOperator("FreeItemChecked", false),
                                                                                                        new BinaryOperator("Select", true)));

                                            SalesInvoiceLine _locMainSalesInvoiceLine = _currSession.FindObject<SalesInvoiceLine>
                                                                                                       (new GroupOperator(GroupOperatorType.And,
                                                                                                       new BinaryOperator("SalesInvoice", _locSalesInvoiceXPO),
                                                                                                       new BinaryOperator("Item", _locFreeItemRule.MpItem),
                                                                                                       new BinaryOperator("Status", Status.Progress),
                                                                                                       new BinaryOperator("FreeItemChecked", false),
                                                                                                       new BinaryOperator("Select", true)));

                                            if (_locMainSalesInvoiceLine != null && _locComboSalesInvoiceLine != null)
                                            {
                                                double _locComboQtyCI = 0;
                                                double _locComboTotQtyFI = 0;
                                                double _locComboCpDqty = 0;
                                                double _locComboCpQty = 0;
                                                int _locComboMaxLoop = 0;
                                                int _locComboTotMaxLoopVal = 0;

                                                double _locMainQtyFI = 0;
                                                double _locTotQtyFI = 0;
                                                double _locFpDqty = 0;
                                                double _locFpQty = 0;
                                                int _locMaxLoop = 0;
                                                int _locTotMaxLoopVal = 0;

                                                if (_locMainSalesInvoiceLine.TQty >= _locFreeItemRule.MpTQty && _locComboSalesInvoiceLine.TQty >= _locFreeItemRule.CpTQty)
                                                {
                                                    _locMainQtyFI = System.Math.Floor(_locMainSalesInvoiceLine.TQty / _locFreeItemRule.MpTQty);
                                                    _locComboQtyCI = System.Math.Floor(_locComboSalesInvoiceLine.TQty / _locFreeItemRule.CpTQty);

                                                    if (_locComboQtyCI < _locMainQtyFI)
                                                    {
                                                        #region ComboPromoItem

                                                        //1+1+1+1+1
                                                        #region Rule1
                                                        if (_locFreeItemRule.RuleType == RuleType.Rule1)
                                                        {

                                                            if (_locFreeItemRule.MaxLoop == 0)
                                                            {
                                                                _locComboMaxLoop = (int)_locComboQtyCI;
                                                            }
                                                            else
                                                            {
                                                                if (_locFreeItemRule.MaxLoop > (int)_locComboQtyCI)
                                                                {
                                                                    _locComboMaxLoop = (int)_locComboQtyCI;
                                                                }
                                                                else
                                                                {
                                                                    _locComboMaxLoop = _locFreeItemRule.MaxLoop;
                                                                }
                                                            }

                                                            for (int i = 1; i <= _locComboMaxLoop; i++)
                                                            {
                                                                _locComboTotMaxLoopVal = _locComboTotMaxLoopVal + 1;
                                                            }

                                                            _locComboTotQtyFI = _locComboTotMaxLoopVal * _locFreeItemRule.FpTQty;

                                                            if(_locFreeItemRule.FpBegInvLine != null)
                                                            {
                                                                if((_locFreeItemRule.FpBegInvLine.QtyAvailable - _locFreeItemRule.FpBegInvLine.QtyOnHand) > 0)
                                                                {
                                                                    if ((_locFreeItemRule.FpBegInvLine.QtyAvailable - _locFreeItemRule.FpBegInvLine.QtyOnHand) < _locComboTotQtyFI)
                                                                    {
                                                                        _locComboTotQtyFI = _locFreeItemRule.FpBegInvLine.QtyAvailable - _locFreeItemRule.FpBegInvLine.QtyOnHand;
                                                                    }

                                                                    if (_locFreeItemRule.FpItem != null && _locFreeItemRule.FpDUOM != null && _locFreeItemRule.FpUOM != null)
                                                                    {
                                                                        ItemUnitOfMeasure _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                                                                (new GroupOperator(GroupOperatorType.And,
                                                                                                 new BinaryOperator("Item", _locFreeItemRule.FpItem),
                                                                                                 new BinaryOperator("UOM", _locFreeItemRule.FpUOM),
                                                                                                 new BinaryOperator("DefaultUOM", _locFreeItemRule.FpDUOM),
                                                                                                 new BinaryOperator("Active", true)));
                                                                        if (_locItemUOM != null)
                                                                        {
                                                                            if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                                                            {
                                                                                _locComboCpDqty = _locComboTotQtyFI % _locItemUOM.DefaultConversion;
                                                                                _locComboCpQty = System.Math.Floor(_locComboTotQtyFI / _locItemUOM.DefaultConversion);
                                                                            }
                                                                            else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                                                            {
                                                                                _locComboCpDqty = System.Math.Floor(_locComboTotQtyFI / _locItemUOM.DefaultConversion);
                                                                                _locComboCpQty = (_locComboTotQtyFI % _locItemUOM.DefaultConversion) * _locItemUOM.Conversion;
                                                                            }
                                                                            else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                                                            {
                                                                                _locComboCpDqty = _locComboTotQtyFI;
                                                                            }
                                                                        }
                                                                    }
                                                                    else
                                                                    {
                                                                        _locComboCpDqty = _locComboTotQtyFI;
                                                                    }

                                                                    SalesInvoiceFreeItem _saveDataSalesInvoiceFreeItem = new SalesInvoiceFreeItem(_currSession)
                                                                    {
                                                                        //Organization
                                                                        Company = _locSalesInvoiceXPO.Company,
                                                                        Workplace = _locSalesInvoiceXPO.Workplace,
                                                                        Division = _locSalesInvoiceXPO.Division,
                                                                        Department = _locSalesInvoiceXPO.Department,
                                                                        Section = _locSalesInvoiceXPO.Section,
                                                                        Employee = _locSalesInvoiceXPO.Employee,
                                                                        //Item
                                                                        SalesInvoice = _locSalesInvoiceXPO,
                                                                        Location = _locFreeItemRule.Location,
                                                                        FreeItemRule = _locFreeItemRule,
                                                                        BegInv = _locFreeItemRule.FpBegInv,
                                                                        FpLocationType = _locFreeItemRule.LocationType,
                                                                        FpStockType = _locFreeItemRule.StockType,
                                                                        FpItem = _locFreeItemRule.FpItem,
                                                                        FpDQty = _locComboCpDqty,
                                                                        FpDUOM = _locFreeItemRule.FpDUOM,
                                                                        FpQty = _locComboCpQty,
                                                                        FpUOM = _locFreeItemRule.FpUOM,
                                                                        FpTQty = _locComboTotQtyFI,
                                                                        FpStockGroup = _locFreeItemRule.FpStockGroup,
                                                                        BegInvLine = _locFreeItemRule.FpBegInvLine,
                                                                        //Amount
                                                                        Currency = _locSalesInvoiceXPO.Currency,
                                                                        PriceGroup = _locSalesInvoiceXPO.PriceGroup,
                                                                        PriceLine = GetPriceLine(_currSession, _locSalesInvoiceXPO, _locFreeItemRule.FpItem),
                                                                        UAmount = GetPriceLine(_currSession, _locSalesInvoiceXPO, _locFreeItemRule.FpItem).Amount2a,
                                                                    };
                                                                    _saveDataSalesInvoiceFreeItem.Save();
                                                                    _saveDataSalesInvoiceFreeItem.Session.CommitTransaction();

                                                                    _locComboSalesInvoiceLine.FreeItemChecked = true;
                                                                    _locComboSalesInvoiceLine.Save();
                                                                    _locComboSalesInvoiceLine.Session.CommitTransaction();

                                                                    _locMainSalesInvoiceLine.FreeItemChecked = true;
                                                                    _locMainSalesInvoiceLine.Save();
                                                                    _locMainSalesInvoiceLine.Session.CommitTransaction();

                                                                    if (_locFreeItemRule.FpBegInv != null && _locFreeItemRule.FpBegInvLine != null)
                                                                    {
                                                                        _locFreeItemRule.FpBegInvLine.QtyOnHand = _locFreeItemRule.FpBegInvLine.QtyOnHand + _locComboTotQtyFI;
                                                                        _locFreeItemRule.FpBegInvLine.Save();
                                                                        _locFreeItemRule.FpBegInvLine.Session.CommitTransaction();
                                                                    }
                                                                }
                                                            }
                                                        }

                                                        #endregion Rule1

                                                        //1, 2+1, 3+1, 4+1
                                                        #region Rule2

                                                        if (_locFreeItemRule.RuleType == RuleType.Rule2)
                                                        {
                                                            if (_locFreeItemRule.MaxLoop == 0)
                                                            {
                                                                _locComboMaxLoop = (int)_locComboQtyCI;
                                                            }
                                                            else
                                                            {
                                                                if (_locFreeItemRule.MaxLoop > (int)_locComboQtyCI)
                                                                {
                                                                    _locComboMaxLoop = (int)_locComboQtyCI;
                                                                }
                                                                else
                                                                {
                                                                    _locComboMaxLoop = _locFreeItemRule.MaxLoop;
                                                                }
                                                            }

                                                            for (int i = 1; i <= _locComboMaxLoop; i++)
                                                            {
                                                                if (i == 1)
                                                                {
                                                                    _locComboTotQtyFI = _locFreeItemRule.FpTQty;
                                                                }
                                                                else
                                                                {
                                                                    _locComboTotQtyFI = _locComboTotQtyFI + (i + _locFreeItemRule.FpTQty);
                                                                }
                                                            }

                                                            if (_locFreeItemRule.FpBegInvLine != null)
                                                            {
                                                                if ((_locFreeItemRule.FpBegInvLine.QtyAvailable - _locFreeItemRule.FpBegInvLine.QtyOnHand) > 0)
                                                                {
                                                                    if ((_locFreeItemRule.FpBegInvLine.QtyAvailable - _locFreeItemRule.FpBegInvLine.QtyOnHand) < _locComboTotQtyFI)
                                                                    {
                                                                        _locComboTotQtyFI = _locFreeItemRule.FpBegInvLine.QtyAvailable - _locFreeItemRule.FpBegInvLine.QtyOnHand;
                                                                    }

                                                                    if (_locFreeItemRule.FpItem != null && _locFreeItemRule.FpDUOM != null && _locFreeItemRule.FpUOM != null)
                                                                    {
                                                                        ItemUnitOfMeasure _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                                                                (new GroupOperator(GroupOperatorType.And,
                                                                                                 new BinaryOperator("Item", _locFreeItemRule.FpItem),
                                                                                                 new BinaryOperator("UOM", _locFreeItemRule.FpUOM),
                                                                                                 new BinaryOperator("DefaultUOM", _locFreeItemRule.FpDUOM),
                                                                                                 new BinaryOperator("Active", true)));
                                                                        if (_locItemUOM != null)
                                                                        {
                                                                            if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                                                            {
                                                                                _locComboCpDqty = _locComboTotQtyFI % _locItemUOM.DefaultConversion;
                                                                                _locComboCpQty = System.Math.Floor(_locComboTotQtyFI / _locItemUOM.DefaultConversion);
                                                                            }
                                                                            else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                                                            {
                                                                                _locComboCpDqty = System.Math.Floor(_locComboTotQtyFI / _locItemUOM.DefaultConversion);
                                                                                _locComboCpQty = (_locComboTotQtyFI % _locItemUOM.DefaultConversion) * _locItemUOM.Conversion;
                                                                            }
                                                                            else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                                                            {
                                                                                _locComboCpDqty = _locComboTotQtyFI;
                                                                            }
                                                                        }
                                                                    }
                                                                    else
                                                                    {
                                                                        _locComboCpDqty = _locComboTotQtyFI;
                                                                    }

                                                                    SalesInvoiceFreeItem _saveDataSalesInvoiceFreeItem = new SalesInvoiceFreeItem(_currSession)
                                                                    {
                                                                        //Organization
                                                                        Company = _locSalesInvoiceXPO.Company,
                                                                        Workplace = _locSalesInvoiceXPO.Workplace,
                                                                        Division = _locSalesInvoiceXPO.Division,
                                                                        Department = _locSalesInvoiceXPO.Department,
                                                                        Section = _locSalesInvoiceXPO.Section,
                                                                        Employee = _locSalesInvoiceXPO.Employee,
                                                                        //Item
                                                                        SalesInvoice = _locSalesInvoiceXPO,
                                                                        Location = _locFreeItemRule.Location,
                                                                        FreeItemRule = _locFreeItemRule,
                                                                        BegInv = _locFreeItemRule.FpBegInv,
                                                                        FpLocationType = _locFreeItemRule.LocationType,
                                                                        FpStockType = _locFreeItemRule.StockType,
                                                                        FpItem = _locFreeItemRule.FpItem,
                                                                        FpDQty = _locComboCpDqty,
                                                                        FpDUOM = _locFreeItemRule.FpDUOM,
                                                                        FpQty = _locComboCpQty,
                                                                        FpUOM = _locFreeItemRule.FpUOM,
                                                                        FpTQty = _locComboTotQtyFI,
                                                                        FpStockGroup = _locFreeItemRule.FpStockGroup,
                                                                        BegInvLine = _locFreeItemRule.FpBegInvLine,
                                                                        //Amount
                                                                        Currency = _locSalesInvoiceXPO.Currency,
                                                                        PriceGroup = _locSalesInvoiceXPO.PriceGroup,
                                                                        PriceLine = GetPriceLine(_currSession, _locSalesInvoiceXPO, _locFreeItemRule.FpItem),
                                                                        UAmount = GetPriceLine(_currSession, _locSalesInvoiceXPO, _locFreeItemRule.FpItem).Amount2a,
                                                                    };
                                                                    _saveDataSalesInvoiceFreeItem.Save();
                                                                    _saveDataSalesInvoiceFreeItem.Session.CommitTransaction();

                                                                    _locComboSalesInvoiceLine.FreeItemChecked = true;
                                                                    _locComboSalesInvoiceLine.Save();
                                                                    _locComboSalesInvoiceLine.Session.CommitTransaction();

                                                                    _locMainSalesInvoiceLine.FreeItemChecked = true;
                                                                    _locMainSalesInvoiceLine.Save();
                                                                    _locMainSalesInvoiceLine.Session.CommitTransaction();

                                                                    if (_locFreeItemRule.FpBegInv != null && _locFreeItemRule.FpBegInvLine != null)
                                                                    {
                                                                        _locFreeItemRule.FpBegInvLine.QtyOnHand = _locFreeItemRule.FpBegInvLine.QtyOnHand + _locComboTotQtyFI;
                                                                        _locFreeItemRule.FpBegInvLine.Save();
                                                                        _locFreeItemRule.FpBegInvLine.Session.CommitTransaction();
                                                                    }
                                                                }
                                                            }
                                                        }
                                                        #endregion Rule2

                                                        //2 * 2 * 2 * 2
                                                        #region Rule3

                                                        if (_locFreeItemRule.RuleType == RuleType.Rule3)
                                                        {

                                                            if (_locFreeItemRule.MaxLoop == 0)
                                                            {
                                                                _locComboMaxLoop = (int)_locComboQtyCI;
                                                            }
                                                            else
                                                            {
                                                                if (_locFreeItemRule.MaxLoop > (int)_locComboQtyCI)
                                                                {
                                                                    _locComboMaxLoop = (int)_locComboQtyCI;
                                                                }
                                                                else
                                                                {
                                                                    _locComboMaxLoop = _locFreeItemRule.MaxLoop;
                                                                }
                                                            }

                                                            for (int i = 1; i <= _locComboMaxLoop; i++)
                                                            {
                                                                _locComboTotQtyFI = _locComboTotQtyFI * _locFreeItemRule.FpTQty;
                                                            }

                                                            if (_locFreeItemRule.FpBegInvLine != null)
                                                            {
                                                                if ((_locFreeItemRule.FpBegInvLine.QtyAvailable - _locFreeItemRule.FpBegInvLine.QtyOnHand) > 0)
                                                                {
                                                                    if ((_locFreeItemRule.FpBegInvLine.QtyAvailable - _locFreeItemRule.FpBegInvLine.QtyOnHand) < _locComboTotQtyFI)
                                                                    {
                                                                        _locComboTotQtyFI = _locFreeItemRule.FpBegInvLine.QtyAvailable - _locFreeItemRule.FpBegInvLine.QtyOnHand;
                                                                    }

                                                                    if (_locFreeItemRule.FpItem != null && _locFreeItemRule.FpDUOM != null && _locFreeItemRule.FpUOM != null)
                                                                    {
                                                                        ItemUnitOfMeasure _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                                                                (new GroupOperator(GroupOperatorType.And,
                                                                                                 new BinaryOperator("Item", _locFreeItemRule.FpItem),
                                                                                                 new BinaryOperator("UOM", _locFreeItemRule.FpUOM),
                                                                                                 new BinaryOperator("DefaultUOM", _locFreeItemRule.FpDUOM),
                                                                                                 new BinaryOperator("Active", true)));
                                                                        if (_locItemUOM != null)
                                                                        {
                                                                            if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                                                            {
                                                                                _locComboCpDqty = _locComboTotQtyFI % _locItemUOM.DefaultConversion;
                                                                                _locComboCpQty = System.Math.Floor(_locComboTotQtyFI / _locItemUOM.DefaultConversion);
                                                                            }
                                                                            else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                                                            {
                                                                                _locComboCpDqty = System.Math.Floor(_locComboTotQtyFI / _locItemUOM.DefaultConversion);
                                                                                _locComboCpQty = (_locComboTotQtyFI % _locItemUOM.DefaultConversion) * _locItemUOM.Conversion;
                                                                            }
                                                                            else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                                                            {
                                                                                _locComboCpDqty = _locComboTotQtyFI;
                                                                            }
                                                                        }
                                                                    }
                                                                    else
                                                                    {
                                                                        _locComboCpDqty = _locComboTotQtyFI;
                                                                    }

                                                                    SalesInvoiceFreeItem _saveDataSalesInvoiceFreeItem = new SalesInvoiceFreeItem(_currSession)
                                                                    {
                                                                        //Organization
                                                                        Company = _locSalesInvoiceXPO.Company,
                                                                        Workplace = _locSalesInvoiceXPO.Workplace,
                                                                        Division = _locSalesInvoiceXPO.Division,
                                                                        Department = _locSalesInvoiceXPO.Department,
                                                                        Section = _locSalesInvoiceXPO.Section,
                                                                        Employee = _locSalesInvoiceXPO.Employee,
                                                                        //Item
                                                                        SalesInvoice = _locSalesInvoiceXPO,
                                                                        Location = _locFreeItemRule.Location,
                                                                        FreeItemRule = _locFreeItemRule,
                                                                        BegInv = _locFreeItemRule.FpBegInv,
                                                                        FpLocationType = _locFreeItemRule.LocationType,
                                                                        FpStockType = _locFreeItemRule.StockType,
                                                                        FpItem = _locFreeItemRule.FpItem,
                                                                        FpDQty = _locComboCpDqty,
                                                                        FpDUOM = _locFreeItemRule.FpDUOM,
                                                                        FpQty = _locComboCpQty,
                                                                        FpUOM = _locFreeItemRule.FpUOM,
                                                                        FpTQty = _locComboTotQtyFI,
                                                                        FpStockGroup = _locFreeItemRule.FpStockGroup,
                                                                        BegInvLine = _locFreeItemRule.FpBegInvLine,
                                                                        //Amount
                                                                        Currency = _locSalesInvoiceXPO.Currency,
                                                                        PriceGroup = _locSalesInvoiceXPO.PriceGroup,
                                                                        PriceLine = GetPriceLine(_currSession, _locSalesInvoiceXPO, _locFreeItemRule.FpItem),
                                                                        UAmount = GetPriceLine(_currSession, _locSalesInvoiceXPO, _locFreeItemRule.FpItem).Amount2a,
                                                                    };
                                                                    _saveDataSalesInvoiceFreeItem.Save();
                                                                    _saveDataSalesInvoiceFreeItem.Session.CommitTransaction();

                                                                    _locComboSalesInvoiceLine.FreeItemChecked = true;
                                                                    _locComboSalesInvoiceLine.Save();
                                                                    _locComboSalesInvoiceLine.Session.CommitTransaction();

                                                                    _locMainSalesInvoiceLine.FreeItemChecked = true;
                                                                    _locMainSalesInvoiceLine.Save();
                                                                    _locMainSalesInvoiceLine.Session.CommitTransaction();

                                                                    if (_locFreeItemRule.FpBegInv != null && _locFreeItemRule.FpBegInvLine != null)
                                                                    {
                                                                        _locFreeItemRule.FpBegInvLine.QtyOnHand = _locFreeItemRule.FpBegInvLine.QtyOnHand + _locComboTotQtyFI;
                                                                        _locFreeItemRule.FpBegInvLine.Save();
                                                                        _locFreeItemRule.FpBegInvLine.Session.CommitTransaction();
                                                                    }
                                                                }
                                                            }
                                                        }
                                                        #endregion Rule3

                                                        //2^(1-1) + 2^(2-1) + 2^(3-1) + 2^(4-1)
                                                        #region Rule4

                                                        if (_locFreeItemRule.RuleType == RuleType.Rule4)
                                                        {
                                                            if (_locFreeItemRule.MaxLoop == 0)
                                                            {
                                                                _locComboMaxLoop = (int)_locComboQtyCI;
                                                            }
                                                            else
                                                            {
                                                                if (_locFreeItemRule.MaxLoop > (int)_locComboQtyCI)
                                                                {
                                                                    _locComboMaxLoop = (int)_locComboQtyCI;
                                                                }
                                                                else
                                                                {
                                                                    _locComboMaxLoop = _locFreeItemRule.MaxLoop;
                                                                }
                                                            }

                                                            for (int i = 1; i <= _locComboMaxLoop; i++)
                                                            {
                                                                _locComboTotQtyFI = _locComboTotQtyFI + System.Math.Pow(_locFreeItemRule.FpTQty, i - 1);
                                                            }

                                                            if (_locFreeItemRule.FpBegInvLine != null)
                                                            {
                                                                if ((_locFreeItemRule.FpBegInvLine.QtyAvailable - _locFreeItemRule.FpBegInvLine.QtyOnHand) > 0)
                                                                {
                                                                    if ((_locFreeItemRule.FpBegInvLine.QtyAvailable - _locFreeItemRule.FpBegInvLine.QtyOnHand) < _locComboTotQtyFI)
                                                                    {
                                                                        _locComboTotQtyFI = _locFreeItemRule.FpBegInvLine.QtyAvailable - _locFreeItemRule.FpBegInvLine.QtyOnHand;
                                                                    }

                                                                    if (_locFreeItemRule.FpItem != null && _locFreeItemRule.FpDUOM != null && _locFreeItemRule.FpUOM != null)
                                                                    {
                                                                        ItemUnitOfMeasure _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                                                                (new GroupOperator(GroupOperatorType.And,
                                                                                                 new BinaryOperator("Item", _locFreeItemRule.FpItem),
                                                                                                 new BinaryOperator("UOM", _locFreeItemRule.FpUOM),
                                                                                                 new BinaryOperator("DefaultUOM", _locFreeItemRule.FpDUOM),
                                                                                                 new BinaryOperator("Active", true)));
                                                                        if (_locItemUOM != null)
                                                                        {
                                                                            if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                                                            {
                                                                                _locComboCpDqty = _locComboTotQtyFI % _locItemUOM.DefaultConversion;
                                                                                _locComboCpQty = System.Math.Floor(_locComboTotQtyFI / _locItemUOM.DefaultConversion);
                                                                            }
                                                                            else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                                                            {
                                                                                _locComboCpDqty = System.Math.Floor(_locComboTotQtyFI / _locItemUOM.DefaultConversion);
                                                                                _locComboCpQty = (_locComboTotQtyFI % _locItemUOM.DefaultConversion) * _locItemUOM.Conversion;
                                                                            }
                                                                            else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                                                            {
                                                                                _locComboCpDqty = _locComboTotQtyFI;
                                                                            }
                                                                        }
                                                                    }
                                                                    else
                                                                    {
                                                                        _locComboCpDqty = _locComboTotQtyFI;
                                                                    }

                                                                    SalesInvoiceFreeItem _saveDataSalesInvoiceFreeItem = new SalesInvoiceFreeItem(_currSession)
                                                                    {
                                                                        //Organization
                                                                        Company = _locSalesInvoiceXPO.Company,
                                                                        Workplace = _locSalesInvoiceXPO.Workplace,
                                                                        Division = _locSalesInvoiceXPO.Division,
                                                                        Department = _locSalesInvoiceXPO.Department,
                                                                        Section = _locSalesInvoiceXPO.Section,
                                                                        Employee = _locSalesInvoiceXPO.Employee,
                                                                        //Item
                                                                        SalesInvoice = _locSalesInvoiceXPO,
                                                                        Location = _locFreeItemRule.Location,
                                                                        FreeItemRule = _locFreeItemRule,
                                                                        BegInv = _locFreeItemRule.FpBegInv,
                                                                        FpLocationType = _locFreeItemRule.LocationType,
                                                                        FpStockType = _locFreeItemRule.StockType,
                                                                        FpItem = _locFreeItemRule.FpItem,
                                                                        FpDQty = _locComboCpDqty,
                                                                        FpDUOM = _locFreeItemRule.FpDUOM,
                                                                        FpQty = _locComboCpQty,
                                                                        FpUOM = _locFreeItemRule.FpUOM,
                                                                        FpTQty = _locComboTotQtyFI,
                                                                        FpStockGroup = _locFreeItemRule.FpStockGroup,
                                                                        BegInvLine = _locFreeItemRule.FpBegInvLine,
                                                                        //Amount
                                                                        Currency = _locSalesInvoiceXPO.Currency,
                                                                        PriceGroup = _locSalesInvoiceXPO.PriceGroup,
                                                                        PriceLine = GetPriceLine(_currSession, _locSalesInvoiceXPO, _locFreeItemRule.FpItem),
                                                                        UAmount = GetPriceLine(_currSession, _locSalesInvoiceXPO, _locFreeItemRule.FpItem).Amount2a,
                                                                    };
                                                                    _saveDataSalesInvoiceFreeItem.Save();
                                                                    _saveDataSalesInvoiceFreeItem.Session.CommitTransaction();

                                                                    _locComboSalesInvoiceLine.FreeItemChecked = true;
                                                                    _locComboSalesInvoiceLine.Save();
                                                                    _locComboSalesInvoiceLine.Session.CommitTransaction();

                                                                    _locMainSalesInvoiceLine.FreeItemChecked = true;
                                                                    _locMainSalesInvoiceLine.Save();
                                                                    _locMainSalesInvoiceLine.Session.CommitTransaction();

                                                                    if (_locFreeItemRule.FpBegInv != null && _locFreeItemRule.FpBegInvLine != null)
                                                                    {
                                                                        _locFreeItemRule.FpBegInvLine.QtyOnHand = _locFreeItemRule.FpBegInvLine.QtyOnHand + _locComboTotQtyFI;
                                                                        _locFreeItemRule.FpBegInvLine.Save();
                                                                        _locFreeItemRule.FpBegInvLine.Session.CommitTransaction();
                                                                    }
                                                                }
                                                            }
                                                        }
                                                        #endregion Rule4

                                                        #endregion ComboPromoItem
                                                    }
                                                    else
                                                    {
                                                        #region MainPromoItem

                                                        #region Rule1

                                                        if (_locFreeItemRule.RuleType == RuleType.Rule1)
                                                        {

                                                            if (_locFreeItemRule.MaxLoop == 0)
                                                            {
                                                                _locMaxLoop = (int)_locMainQtyFI;
                                                            }
                                                            else
                                                            {
                                                                if (_locFreeItemRule.MaxLoop > (int)_locMainQtyFI)
                                                                {
                                                                    _locMaxLoop = (int)_locMainQtyFI;
                                                                }
                                                                else
                                                                {
                                                                    _locMaxLoop = _locFreeItemRule.MaxLoop;
                                                                }
                                                            }

                                                            for (int i = 1; i <= _locMaxLoop; i++)
                                                            {
                                                                _locTotMaxLoopVal = _locTotMaxLoopVal + 1;
                                                            }

                                                            _locTotQtyFI = _locTotMaxLoopVal * _locFreeItemRule.FpTQty;

                                                            if(_locFreeItemRule.FpBegInvLine != null)
                                                            {
                                                                if((_locFreeItemRule.FpBegInvLine.QtyAvailable - _locFreeItemRule.FpBegInvLine.QtyOnHand) > 0)
                                                                {
                                                                    if ((_locFreeItemRule.FpBegInvLine.QtyAvailable - _locFreeItemRule.FpBegInvLine.QtyOnHand) < _locTotQtyFI)
                                                                    {
                                                                        _locTotQtyFI = _locFreeItemRule.FpBegInvLine.QtyAvailable - _locFreeItemRule.FpBegInvLine.QtyOnHand;
                                                                    }

                                                                    if (_locFreeItemRule.FpItem != null && _locFreeItemRule.FpDUOM != null && _locFreeItemRule.FpUOM != null)
                                                                    {
                                                                        ItemUnitOfMeasure _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                                                                (new GroupOperator(GroupOperatorType.And,
                                                                                                 new BinaryOperator("Item", _locFreeItemRule.FpItem),
                                                                                                 new BinaryOperator("UOM", _locFreeItemRule.FpUOM),
                                                                                                 new BinaryOperator("DefaultUOM", _locFreeItemRule.FpDUOM),
                                                                                                 new BinaryOperator("Active", true)));
                                                                        if (_locItemUOM != null)
                                                                        {
                                                                            if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                                                            {
                                                                                _locFpDqty = _locTotQtyFI % _locItemUOM.DefaultConversion;
                                                                                _locFpQty = System.Math.Floor(_locTotQtyFI / _locItemUOM.DefaultConversion);
                                                                            }
                                                                            else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                                                            {
                                                                                _locFpDqty = System.Math.Floor(_locTotQtyFI / _locItemUOM.DefaultConversion);
                                                                                _locFpQty = (_locTotQtyFI % _locItemUOM.DefaultConversion) * _locItemUOM.Conversion;
                                                                            }
                                                                            else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                                                            {
                                                                                _locFpDqty = _locTotQtyFI;
                                                                            }
                                                                        }
                                                                    }
                                                                    else
                                                                    {
                                                                        _locFpDqty = _locTotQtyFI;
                                                                    }


                                                                    SalesInvoiceFreeItem _saveDataSalesInvoiceFreeItem = new SalesInvoiceFreeItem(_currSession)
                                                                    {
                                                                        //Organization
                                                                        Company = _locSalesInvoiceXPO.Company,
                                                                        Workplace = _locSalesInvoiceXPO.Workplace,
                                                                        Division = _locSalesInvoiceXPO.Division,
                                                                        Department = _locSalesInvoiceXPO.Department,
                                                                        Section = _locSalesInvoiceXPO.Section,
                                                                        Employee = _locSalesInvoiceXPO.Employee,
                                                                        //Item
                                                                        SalesInvoice = _locSalesInvoiceXPO,
                                                                        Location = _locFreeItemRule.Location,
                                                                        FreeItemRule = _locFreeItemRule,
                                                                        FpLocationType = _locFreeItemRule.LocationType,
                                                                        FpStockType = _locFreeItemRule.StockType,
                                                                        BegInv = _locFreeItemRule.FpBegInv,
                                                                        FpItem = _locFreeItemRule.FpItem,
                                                                        FpDQty = _locFpDqty,
                                                                        FpDUOM = _locFreeItemRule.FpDUOM,
                                                                        FpQty = _locFpQty,
                                                                        FpUOM = _locFreeItemRule.FpUOM,
                                                                        FpTQty = _locTotQtyFI,
                                                                        FpStockGroup = _locFreeItemRule.FpStockGroup,
                                                                        BegInvLine = _locFreeItemRule.FpBegInvLine,
                                                                        //Amout
                                                                        Currency = _locSalesInvoiceXPO.Currency,
                                                                        PriceGroup = _locSalesInvoiceXPO.PriceGroup,
                                                                        PriceLine = GetPriceLine(_currSession, _locSalesInvoiceXPO, _locFreeItemRule.FpItem),
                                                                        UAmount = GetPriceLine(_currSession, _locSalesInvoiceXPO, _locFreeItemRule.FpItem).Amount2a,
                                                                    };
                                                                    _saveDataSalesInvoiceFreeItem.Save();
                                                                    _saveDataSalesInvoiceFreeItem.Session.CommitTransaction();

                                                                    _locComboSalesInvoiceLine.FreeItemChecked = true;
                                                                    _locComboSalesInvoiceLine.Save();
                                                                    _locComboSalesInvoiceLine.Session.CommitTransaction();

                                                                    _locMainSalesInvoiceLine.FreeItemChecked = true;
                                                                    _locMainSalesInvoiceLine.Save();
                                                                    _locMainSalesInvoiceLine.Session.CommitTransaction();

                                                                    if (_locFreeItemRule.FpBegInv != null && _locFreeItemRule.FpBegInvLine != null)
                                                                    {
                                                                        _locFreeItemRule.FpBegInvLine.QtyOnHand = _locFreeItemRule.FpBegInvLine.QtyOnHand + _locTotQtyFI;
                                                                        _locFreeItemRule.FpBegInvLine.Save();
                                                                        _locFreeItemRule.FpBegInvLine.Session.CommitTransaction();
                                                                    }
                                                                }
                                                            }
                                                        }

                                                        #endregion Rule1

                                                        //1, 2+1, 3+1, 4+1
                                                        #region Rule2
                                                        if (_locFreeItemRule.RuleType == RuleType.Rule2)
                                                        {

                                                            if (_locFreeItemRule.MaxLoop == 0)
                                                            {
                                                                _locMaxLoop = (int)_locMainQtyFI;
                                                            }
                                                            else
                                                            {
                                                                if (_locFreeItemRule.MaxLoop > (int)_locMainQtyFI)
                                                                {
                                                                    _locMaxLoop = (int)_locMainQtyFI;
                                                                }
                                                                else
                                                                {
                                                                    _locMaxLoop = _locFreeItemRule.MaxLoop;
                                                                }
                                                            }

                                                            for (int i = 1; i <= _locMaxLoop; i++)
                                                            {

                                                                if (i == 1)
                                                                {
                                                                    _locTotQtyFI = _locFreeItemRule.FpTQty;
                                                                }
                                                                else
                                                                {
                                                                    _locTotQtyFI = _locTotQtyFI + (i + _locFreeItemRule.FpTQty);
                                                                }

                                                            }

                                                            if (_locFreeItemRule.FpBegInvLine != null)
                                                            {
                                                                if ((_locFreeItemRule.FpBegInvLine.QtyAvailable - _locFreeItemRule.FpBegInvLine.QtyOnHand) > 0)
                                                                {
                                                                    if ((_locFreeItemRule.FpBegInvLine.QtyAvailable - _locFreeItemRule.FpBegInvLine.QtyOnHand) < _locTotQtyFI)
                                                                    {
                                                                        _locTotQtyFI = _locFreeItemRule.FpBegInvLine.QtyAvailable - _locFreeItemRule.FpBegInvLine.QtyOnHand;
                                                                    }

                                                                    if (_locFreeItemRule.FpItem != null && _locFreeItemRule.FpDUOM != null && _locFreeItemRule.FpUOM != null)
                                                                    {
                                                                        ItemUnitOfMeasure _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                                                                (new GroupOperator(GroupOperatorType.And,
                                                                                                 new BinaryOperator("Item", _locFreeItemRule.FpItem),
                                                                                                 new BinaryOperator("UOM", _locFreeItemRule.FpUOM),
                                                                                                 new BinaryOperator("DefaultUOM", _locFreeItemRule.FpDUOM),
                                                                                                 new BinaryOperator("Active", true)));
                                                                        if (_locItemUOM != null)
                                                                        {
                                                                            if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                                                            {
                                                                                _locFpDqty = _locTotQtyFI % _locItemUOM.DefaultConversion;
                                                                                _locFpQty = System.Math.Floor(_locTotQtyFI / _locItemUOM.DefaultConversion);
                                                                            }
                                                                            else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                                                            {
                                                                                _locFpDqty = System.Math.Floor(_locTotQtyFI / _locItemUOM.DefaultConversion);
                                                                                _locFpQty = (_locTotQtyFI % _locItemUOM.DefaultConversion) * _locItemUOM.Conversion;
                                                                            }
                                                                            else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                                                            {
                                                                                _locFpDqty = _locTotQtyFI;
                                                                            }
                                                                        }
                                                                    }
                                                                    else
                                                                    {
                                                                        _locFpDqty = _locTotQtyFI;
                                                                    }


                                                                    SalesInvoiceFreeItem _saveDataSalesInvoiceFreeItem = new SalesInvoiceFreeItem(_currSession)
                                                                    {
                                                                        //Organization
                                                                        Company = _locSalesInvoiceXPO.Company,
                                                                        Workplace = _locSalesInvoiceXPO.Workplace,
                                                                        Division = _locSalesInvoiceXPO.Division,
                                                                        Department = _locSalesInvoiceXPO.Department,
                                                                        Section = _locSalesInvoiceXPO.Section,
                                                                        Employee = _locSalesInvoiceXPO.Employee,
                                                                        //Item
                                                                        SalesInvoice = _locSalesInvoiceXPO,
                                                                        Location = _locFreeItemRule.Location,
                                                                        FreeItemRule = _locFreeItemRule,
                                                                        FpLocationType = _locFreeItemRule.LocationType,
                                                                        FpStockType = _locFreeItemRule.StockType,
                                                                        BegInv = _locFreeItemRule.FpBegInv,
                                                                        FpItem = _locFreeItemRule.FpItem,
                                                                        FpDQty = _locFpDqty,
                                                                        FpDUOM = _locFreeItemRule.FpDUOM,
                                                                        FpQty = _locFpQty,
                                                                        FpUOM = _locFreeItemRule.FpUOM,
                                                                        FpTQty = _locTotQtyFI,
                                                                        FpStockGroup = _locFreeItemRule.FpStockGroup,
                                                                        BegInvLine = _locFreeItemRule.FpBegInvLine,
                                                                        //Amout
                                                                        Currency = _locSalesInvoiceXPO.Currency,
                                                                        PriceGroup = _locSalesInvoiceXPO.PriceGroup,
                                                                        PriceLine = GetPriceLine(_currSession, _locSalesInvoiceXPO, _locFreeItemRule.FpItem),
                                                                        UAmount = GetPriceLine(_currSession, _locSalesInvoiceXPO, _locFreeItemRule.FpItem).Amount2a,
                                                                    };
                                                                    _saveDataSalesInvoiceFreeItem.Save();
                                                                    _saveDataSalesInvoiceFreeItem.Session.CommitTransaction();

                                                                    _locComboSalesInvoiceLine.FreeItemChecked = true;
                                                                    _locComboSalesInvoiceLine.Save();
                                                                    _locComboSalesInvoiceLine.Session.CommitTransaction();

                                                                    _locMainSalesInvoiceLine.FreeItemChecked = true;
                                                                    _locMainSalesInvoiceLine.Save();
                                                                    _locMainSalesInvoiceLine.Session.CommitTransaction();

                                                                    if (_locFreeItemRule.FpBegInv != null && _locFreeItemRule.FpBegInvLine != null)
                                                                    {
                                                                        _locFreeItemRule.FpBegInvLine.QtyOnHand = _locFreeItemRule.FpBegInvLine.QtyOnHand + _locTotQtyFI;
                                                                        _locFreeItemRule.FpBegInvLine.Save();
                                                                        _locFreeItemRule.FpBegInvLine.Session.CommitTransaction();
                                                                    }
                                                                }
                                                            }
                                                        }
                                                        #endregion Rule2

                                                        //2 * 2 * 2 * 2
                                                        #region Rule3
                                                        if (_locFreeItemRule.RuleType == RuleType.Rule3)
                                                        {

                                                            if (_locFreeItemRule.MaxLoop == 0)
                                                            {
                                                                _locMaxLoop = (int)_locMainQtyFI;
                                                            }
                                                            else
                                                            {
                                                                if (_locFreeItemRule.MaxLoop > (int)_locMainQtyFI)
                                                                {
                                                                    _locMaxLoop = (int)_locMainQtyFI;
                                                                }
                                                                else
                                                                {
                                                                    _locMaxLoop = _locFreeItemRule.MaxLoop;
                                                                }
                                                            }

                                                            for (int i = 1; i <= _locMaxLoop; i++)
                                                            {
                                                                _locTotQtyFI = _locTotQtyFI * _locFreeItemRule.FpTQty;
                                                            }

                                                            if (_locFreeItemRule.FpBegInvLine != null)
                                                            {
                                                                if ((_locFreeItemRule.FpBegInvLine.QtyAvailable - _locFreeItemRule.FpBegInvLine.QtyOnHand) > 0)
                                                                {
                                                                    if ((_locFreeItemRule.FpBegInvLine.QtyAvailable - _locFreeItemRule.FpBegInvLine.QtyOnHand) < _locTotQtyFI)
                                                                    {
                                                                        _locTotQtyFI = _locFreeItemRule.FpBegInvLine.QtyAvailable - _locFreeItemRule.FpBegInvLine.QtyOnHand;
                                                                    }

                                                                    if (_locFreeItemRule.FpItem != null && _locFreeItemRule.FpDUOM != null && _locFreeItemRule.FpUOM != null)
                                                                    {
                                                                        ItemUnitOfMeasure _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                                                                (new GroupOperator(GroupOperatorType.And,
                                                                                                 new BinaryOperator("Item", _locFreeItemRule.FpItem),
                                                                                                 new BinaryOperator("UOM", _locFreeItemRule.FpUOM),
                                                                                                 new BinaryOperator("DefaultUOM", _locFreeItemRule.FpDUOM),
                                                                                                 new BinaryOperator("Active", true)));
                                                                        if (_locItemUOM != null)
                                                                        {
                                                                            if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                                                            {
                                                                                _locFpDqty = _locTotQtyFI % _locItemUOM.DefaultConversion;
                                                                                _locFpQty = System.Math.Floor(_locTotQtyFI / _locItemUOM.DefaultConversion);
                                                                            }
                                                                            else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                                                            {
                                                                                _locFpDqty = System.Math.Floor(_locTotQtyFI / _locItemUOM.DefaultConversion);
                                                                                _locFpQty = (_locTotQtyFI % _locItemUOM.DefaultConversion) * _locItemUOM.Conversion;
                                                                            }
                                                                            else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                                                            {
                                                                                _locFpDqty = _locTotQtyFI;
                                                                            }
                                                                        }
                                                                    }
                                                                    else
                                                                    {
                                                                        _locFpDqty = _locTotQtyFI;
                                                                    }


                                                                    SalesInvoiceFreeItem _saveDataSalesInvoiceFreeItem = new SalesInvoiceFreeItem(_currSession)
                                                                    {
                                                                        //Organization
                                                                        Company = _locSalesInvoiceXPO.Company,
                                                                        Workplace = _locSalesInvoiceXPO.Workplace,
                                                                        Division = _locSalesInvoiceXPO.Division,
                                                                        Department = _locSalesInvoiceXPO.Department,
                                                                        Section = _locSalesInvoiceXPO.Section,
                                                                        Employee = _locSalesInvoiceXPO.Employee,
                                                                        //Item
                                                                        SalesInvoice = _locSalesInvoiceXPO,
                                                                        Location = _locFreeItemRule.Location,
                                                                        FreeItemRule = _locFreeItemRule,
                                                                        FpLocationType = _locFreeItemRule.LocationType,
                                                                        FpStockType = _locFreeItemRule.StockType,
                                                                        BegInv = _locFreeItemRule.FpBegInv,
                                                                        FpItem = _locFreeItemRule.FpItem,
                                                                        FpDQty = _locFpDqty,
                                                                        FpDUOM = _locFreeItemRule.FpDUOM,
                                                                        FpQty = _locFpQty,
                                                                        FpUOM = _locFreeItemRule.FpUOM,
                                                                        FpTQty = _locTotQtyFI,
                                                                        FpStockGroup = _locFreeItemRule.FpStockGroup,
                                                                        BegInvLine = _locFreeItemRule.FpBegInvLine,
                                                                        //Amout
                                                                        Currency = _locSalesInvoiceXPO.Currency,
                                                                        PriceGroup = _locSalesInvoiceXPO.PriceGroup,
                                                                        PriceLine = GetPriceLine(_currSession, _locSalesInvoiceXPO, _locFreeItemRule.FpItem),
                                                                        UAmount = GetPriceLine(_currSession, _locSalesInvoiceXPO, _locFreeItemRule.FpItem).Amount2a,
                                                                    };
                                                                    _saveDataSalesInvoiceFreeItem.Save();
                                                                    _saveDataSalesInvoiceFreeItem.Session.CommitTransaction();

                                                                    _locComboSalesInvoiceLine.FreeItemChecked = true;
                                                                    _locComboSalesInvoiceLine.Save();
                                                                    _locComboSalesInvoiceLine.Session.CommitTransaction();

                                                                    _locMainSalesInvoiceLine.FreeItemChecked = true;
                                                                    _locMainSalesInvoiceLine.Save();
                                                                    _locMainSalesInvoiceLine.Session.CommitTransaction();

                                                                    if (_locFreeItemRule.FpBegInv != null && _locFreeItemRule.FpBegInvLine != null)
                                                                    {
                                                                        _locFreeItemRule.FpBegInvLine.QtyOnHand = _locFreeItemRule.FpBegInvLine.QtyOnHand + _locTotQtyFI;
                                                                        _locFreeItemRule.FpBegInvLine.Save();
                                                                        _locFreeItemRule.FpBegInvLine.Session.CommitTransaction();
                                                                    }
                                                                }
                                                            } 
                                                        }
                                                        #endregion Rule3

                                                        //2^(1-1) + 2^(2-1) + 2^(3-1) + 2^(4-1)
                                                        #region Rule4

                                                        if (_locFreeItemRule.RuleType == RuleType.Rule4)
                                                        {
                                                            if (_locFreeItemRule.MaxLoop == 0)
                                                            {
                                                                _locMaxLoop = (int)_locMainQtyFI;
                                                            }
                                                            else
                                                            {
                                                                if (_locFreeItemRule.MaxLoop > (int)_locMainQtyFI)
                                                                {
                                                                    _locMaxLoop = (int)_locMainQtyFI;
                                                                }
                                                                else
                                                                {
                                                                    _locMaxLoop = _locFreeItemRule.MaxLoop;
                                                                }
                                                            }

                                                            for (int i = 1; i <= _locMaxLoop; i++)
                                                            {
                                                                _locTotQtyFI = _locTotQtyFI + System.Math.Pow(_locFreeItemRule.FpTQty, i - 1);
                                                            }

                                                            if (_locFreeItemRule.FpBegInvLine != null)
                                                            {
                                                                if ((_locFreeItemRule.FpBegInvLine.QtyAvailable - _locFreeItemRule.FpBegInvLine.QtyOnHand) > 0)
                                                                {
                                                                    if ((_locFreeItemRule.FpBegInvLine.QtyAvailable - _locFreeItemRule.FpBegInvLine.QtyOnHand) < _locTotQtyFI)
                                                                    {
                                                                        _locTotQtyFI = _locFreeItemRule.FpBegInvLine.QtyAvailable - _locFreeItemRule.FpBegInvLine.QtyOnHand;
                                                                    }

                                                                    if (_locFreeItemRule.FpItem != null && _locFreeItemRule.FpDUOM != null && _locFreeItemRule.FpUOM != null)
                                                                    {
                                                                        ItemUnitOfMeasure _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                                                                (new GroupOperator(GroupOperatorType.And,
                                                                                                 new BinaryOperator("Item", _locFreeItemRule.FpItem),
                                                                                                 new BinaryOperator("UOM", _locFreeItemRule.FpUOM),
                                                                                                 new BinaryOperator("DefaultUOM", _locFreeItemRule.FpDUOM),
                                                                                                 new BinaryOperator("Active", true)));
                                                                        if (_locItemUOM != null)
                                                                        {
                                                                            if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                                                            {
                                                                                _locFpDqty = _locTotQtyFI % _locItemUOM.DefaultConversion;
                                                                                _locFpQty = System.Math.Floor(_locTotQtyFI / _locItemUOM.DefaultConversion);
                                                                            }
                                                                            else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                                                            {
                                                                                _locFpDqty = System.Math.Floor(_locTotQtyFI / _locItemUOM.DefaultConversion);
                                                                                _locFpQty = (_locTotQtyFI % _locItemUOM.DefaultConversion) * _locItemUOM.Conversion;
                                                                            }
                                                                            else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                                                            {
                                                                                _locFpDqty = _locTotQtyFI;
                                                                            }
                                                                        }
                                                                    }
                                                                    else
                                                                    {
                                                                        _locFpDqty = _locTotQtyFI;
                                                                    }


                                                                    SalesInvoiceFreeItem _saveDataSalesInvoiceFreeItem = new SalesInvoiceFreeItem(_currSession)
                                                                    {
                                                                        //Organization
                                                                        Company = _locSalesInvoiceXPO.Company,
                                                                        Workplace = _locSalesInvoiceXPO.Workplace,
                                                                        Division = _locSalesInvoiceXPO.Division,
                                                                        Department = _locSalesInvoiceXPO.Department,
                                                                        Section = _locSalesInvoiceXPO.Section,
                                                                        Employee = _locSalesInvoiceXPO.Employee,
                                                                        //Item
                                                                        SalesInvoice = _locSalesInvoiceXPO,
                                                                        Location = _locFreeItemRule.Location,
                                                                        FreeItemRule = _locFreeItemRule,
                                                                        FpLocationType = _locFreeItemRule.LocationType,
                                                                        FpStockType = _locFreeItemRule.StockType,
                                                                        BegInv = _locFreeItemRule.FpBegInv,
                                                                        FpItem = _locFreeItemRule.FpItem,
                                                                        FpDQty = _locFpDqty,
                                                                        FpDUOM = _locFreeItemRule.FpDUOM,
                                                                        FpQty = _locFpQty,
                                                                        FpUOM = _locFreeItemRule.FpUOM,
                                                                        FpTQty = _locTotQtyFI,
                                                                        FpStockGroup = _locFreeItemRule.FpStockGroup,
                                                                        BegInvLine = _locFreeItemRule.FpBegInvLine,
                                                                        //Amout
                                                                        Currency = _locSalesInvoiceXPO.Currency,
                                                                        PriceGroup = _locSalesInvoiceXPO.PriceGroup,
                                                                        PriceLine = GetPriceLine(_currSession, _locSalesInvoiceXPO, _locFreeItemRule.FpItem),
                                                                        UAmount = GetPriceLine(_currSession, _locSalesInvoiceXPO, _locFreeItemRule.FpItem).Amount2a,
                                                                    };
                                                                    _saveDataSalesInvoiceFreeItem.Save();
                                                                    _saveDataSalesInvoiceFreeItem.Session.CommitTransaction();

                                                                    _locComboSalesInvoiceLine.FreeItemChecked = true;
                                                                    _locComboSalesInvoiceLine.Save();
                                                                    _locComboSalesInvoiceLine.Session.CommitTransaction();

                                                                    _locMainSalesInvoiceLine.FreeItemChecked = true;
                                                                    _locMainSalesInvoiceLine.Save();
                                                                    _locMainSalesInvoiceLine.Session.CommitTransaction();

                                                                    if (_locFreeItemRule.FpBegInv != null && _locFreeItemRule.FpBegInvLine != null)
                                                                    {
                                                                        _locFreeItemRule.FpBegInvLine.QtyOnHand = _locFreeItemRule.FpBegInvLine.QtyOnHand + _locTotQtyFI;
                                                                        _locFreeItemRule.FpBegInvLine.Save();
                                                                        _locFreeItemRule.FpBegInvLine.Session.CommitTransaction();
                                                                    }
                                                                }
                                                            } 
                                                        }
                                                        #endregion Rule4

                                                        #endregion MainPromoItem
                                                    }
                                                }

                                            }
                                        }
                                        #endregion MultiItem=true
                                        
                                    }
                                }
                            }
                            #endregion Line
                        }
                    }
                    
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = SalesInvoice ", ex.ToString());
            }
        }

        private void SetTotalFreeItemAmount(Session _currSession, SalesInvoice _locSalesInvoiceXPO)
        {
            try
            {
                double _locTUAmount = 0;
                double _locGetTUAmount = 0;
                if (_locSalesInvoiceXPO != null)
                {
                    XPCollection<SalesInvoiceFreeItem> _locSalesInvoiceFreeItems = new XPCollection<SalesInvoiceFreeItem>
                                                                                (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                new BinaryOperator("SalesInvoice", _locSalesInvoiceXPO),
                                                                                new GroupOperator(GroupOperatorType.Or,
                                                                                new BinaryOperator("Status", Status.Open),
                                                                                new BinaryOperator("Status", Status.Progress))));
                    if (_locSalesInvoiceFreeItems != null && _locSalesInvoiceFreeItems.Count() > 0)
                    {
                        foreach (SalesInvoiceFreeItem _locSalesInvoiceFreeItem in _locSalesInvoiceFreeItems)
                        {
                            _locGetTUAmount = GetTotalUnitAmount(_currSession, _locSalesInvoiceFreeItem.FpTQty, _locSalesInvoiceFreeItem.UAmount);
                            _locTUAmount = _locTUAmount + _locGetTUAmount;

                            _locSalesInvoiceFreeItem.TUAmount = _locGetTUAmount;
                            _locSalesInvoiceFreeItem.Save();
                            _locSalesInvoiceFreeItem.Session.CommitTransaction();
                        }
                        _locSalesInvoiceXPO.TotFIAmount = _locTUAmount;
                        _locSalesInvoiceXPO.Save();
                        _locSalesInvoiceXPO.Session.CommitTransaction();
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = SalesInvoice ", ex.ToString());
            }

        }

        private PriceLine GetPriceLine(Session _currSession, SalesInvoice _locSalesInvoiceXPO, Item _locFpItem)
        {
            PriceLine _result = null;
            try
            {

                if (_locSalesInvoiceXPO.Company != null && _locSalesInvoiceXPO.Workplace != null && _locFpItem != null && _locSalesInvoiceXPO.PriceGroup != null)
                {
                    PriceLine _locPriceLine = _currSession.FindObject<PriceLine>(new GroupOperator(GroupOperatorType.And,
                                                                            new BinaryOperator("Company", _locSalesInvoiceXPO.Company),
                                                                            new BinaryOperator("Workplace", _locSalesInvoiceXPO.Workplace),
                                                                            new BinaryOperator("Item", _locFpItem),
                                                                            new BinaryOperator("PriceGroup", _locSalesInvoiceXPO.PriceGroup),
                                                                            new BinaryOperator("Active", true)));
                    if (_locPriceLine != null)
                    {
                        _result = _locPriceLine;
                    }
                }

            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = SalesInvoice " + ex.ToString());
            }
            return _result;
        }

        private double GetTotalUnitAmount(Session _currSession, double _locFpTQty, double _locUAmount)
        {
            double _result = 0;
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();

                if (_locFpTQty >= 0 & _locUAmount >= 0)
                {
                    if (_globFunc.GetRoundingList(_currSession, ObjectList.SalesInvoiceFreeItem, FieldName.TUAmount) == true)
                    {
                        _result = _globFunc.GetRoundUp(_currSession, (_locFpTQty * _locUAmount), ObjectList.SalesInvoiceFreeItem, FieldName.TUAmount);
                    }
                    else
                    {
                        _result = _locFpTQty * _locUAmount;
                    }

                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = SalesInvoice " + ex.ToString());
            }
            return _result;
        }

        #endregion FreeItem

        #region Discount

        private double GetDiscountRule1(Session _currSession, SalesInvoice _locSalesInvoiceXPO)
        {
            double _result = 0;
            try
            {
                DiscountRule _locDiscountRule1 = null;
                DiscountRule _locDiscountRule1b = null;
                AccountingPeriodicLine _locAcctPerLine = null;

                int _locCount = 0;
                if (_locSalesInvoiceXPO != null)
                {
                    if(_locSalesInvoiceXPO.Company != null && _locSalesInvoiceXPO.Workplace != null)
                    {
                        #region SearchDiscountRule
                        if(_locSalesInvoiceXPO.DiscountRule != null)
                        {
                            _locDiscountRule1 = _locSalesInvoiceXPO.DiscountRule;
                            if(_locSalesInvoiceXPO.AccountingPeriodicLine != null)
                            {
                                _locAcctPerLine = _locSalesInvoiceXPO.AccountingPeriodicLine;
                            }
                        }
                        else
                        {
                            XPQuery<SalesOrderCollection> _salesOrderCollectionsQuery = new XPQuery<SalesOrderCollection>(_currSession);

                            var _salesOrderCollections = from soc in _salesOrderCollectionsQuery
                                                         where (soc.Company == _locSalesInvoiceXPO.Company
                                                         && soc.Workplace == _locSalesInvoiceXPO.Workplace
                                                         && soc.SalesInvoice == _locSalesInvoiceXPO)
                                                         group soc by soc.DiscountRule into g
                                                         select new { DiscountRule = g.Key };
                            if (_salesOrderCollections != null && _salesOrderCollections.Count() > 0)
                            {
                                foreach (var _salesOrderCollection in _salesOrderCollections)
                                {
                                    if (_salesOrderCollection.DiscountRule != null)
                                    {
                                        _locCount = _locCount + 1;
                                        _locDiscountRule1b = _salesOrderCollection.DiscountRule;
                                    }
                                }
                                if (_locCount == 0)
                                {
                                    _locDiscountRule1 = _locDiscountRule1b;
                                }
                                else
                                {
                                    DiscountRule _locDiscountRule1a = _currSession.FindObject<DiscountRule>
                                                                (new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("Company", _locSalesInvoiceXPO.Company),
                                                                new BinaryOperator("Workplace", _locSalesInvoiceXPO.Workplace),
                                                                new BinaryOperator("PriceGroup", _locSalesInvoiceXPO.PriceGroup),
                                                                new BinaryOperator("RuleType", RuleType.Rule1),
                                                                new BinaryOperator("Active", true)));
                                    if (_locDiscountRule1a != null)
                                    {
                                        _locDiscountRule1 = _locDiscountRule1a;
                                    }
                                }
                            }
                            else
                            {
                                DiscountRule _locDiscountRule1a = _currSession.FindObject<DiscountRule>
                                                                (new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("Company", _locSalesInvoiceXPO.Company),
                                                                new BinaryOperator("Workplace", _locSalesInvoiceXPO.Workplace),
                                                                new BinaryOperator("PriceGroup", _locSalesInvoiceXPO.PriceGroup),
                                                                new BinaryOperator("RuleType", RuleType.Rule1),
                                                                new BinaryOperator("Active", true)));
                                if (_locDiscountRule1a != null)
                                {
                                    _locDiscountRule1 = _locDiscountRule1a;
                                }
                            }
                        }
                        #endregion SearchDiscountRule

                        #region Rule1

                        //Grand Total Amount Per Invoice
                        if (_locDiscountRule1 != null)
                        {
                            double _locTUAmount = 0;
                            double _locTAmount = 0;
                            double _locTUAmountDiscount = 0;
                            double _locTUAmountDiscount2 = 0;

                            #region Gross
                            if (_locDiscountRule1.GrossAmountRule == true)
                            {
                                XPCollection<SalesInvoiceLine> _locMainSalesInvoiceLines = new XPCollection<SalesInvoiceLine>
                                                                                               (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                               new BinaryOperator("SalesInvoice", _locSalesInvoiceXPO),
                                                                                               new BinaryOperator("Select", true),
                                                                                               new GroupOperator(GroupOperatorType.Or,
                                                                                               new BinaryOperator("Status", Status.Open))));


                                if (_locMainSalesInvoiceLines != null && _locMainSalesInvoiceLines.Count() > 0)
                                {
                                    foreach (SalesInvoiceLine _locMainSalesInvoiceLine in _locMainSalesInvoiceLines)
                                    {
                                        if (_locMainSalesInvoiceLine.TUAmount > 0)
                                        {
                                            _locTUAmount = _locTUAmount + _locMainSalesInvoiceLine.TUAmount;
                                            _locMainSalesInvoiceLine.DiscountChecked = true;
                                            _locMainSalesInvoiceLine.Save();
                                            _locMainSalesInvoiceLine.Session.CommitTransaction();
                                        }
                                    }

                                    if (_locTUAmount > 0 && _locTUAmount >= _locDiscountRule1.GrossTotalUAmount)
                                    {
                                        if (_locDiscountRule1.DiscountType == DiscountType.Percentage)
                                        {
                                            _locTUAmountDiscount = _locTUAmount * _locDiscountRule1.Value / 100;
                                        }
                                        if (_locDiscountRule1.DiscountType == DiscountType.Value)
                                        {
                                            _locTUAmountDiscount = _locDiscountRule1.Value;
                                        }
                                    }
                                }

                            }
                            #endregion Gross
                            #region Net
                            if (_locDiscountRule1.NetAmountRule == true)
                            {
                                XPCollection<SalesInvoiceLine> _locMainSalesInvoiceLines = new XPCollection<SalesInvoiceLine>
                                                                                               (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                               new BinaryOperator("SalesInvoice", _locSalesInvoiceXPO),
                                                                                               new BinaryOperator("Select", true),
                                                                                               new GroupOperator(GroupOperatorType.Or,
                                                                                               new BinaryOperator("Status", Status.Open))));

                                if (_locMainSalesInvoiceLines != null && _locMainSalesInvoiceLines.Count() > 0)
                                {
                                    foreach (SalesInvoiceLine _locMainSalesInvoiceLine in _locMainSalesInvoiceLines)
                                    {
                                        if (_locMainSalesInvoiceLine.TAmount > 0)
                                        {
                                            _locTAmount = _locTAmount + _locMainSalesInvoiceLine.TAmount;
                                            _locMainSalesInvoiceLine.DiscountChecked = true;
                                            _locMainSalesInvoiceLine.Save();
                                            _locMainSalesInvoiceLine.Session.CommitTransaction();
                                        }
                                    }

                                    if (_locTAmount > 0 && _locTAmount >= _locDiscountRule1.NetTotalUAmount)
                                    {
                                        if (_locDiscountRule1.DiscountType == DiscountType.Percentage)
                                        {
                                            _locTUAmountDiscount = _locTAmount * _locDiscountRule1.Value / 100;
                                        }
                                        if (_locDiscountRule1.DiscountType == DiscountType.Value)
                                        {
                                            _locTUAmountDiscount = _locDiscountRule1.Value;
                                        }
                                    }
                                }
                            }
                            #endregion Net

                            #region ValBasedAccount
                            if (_locDiscountRule1.ValBasedAccount == true)
                            {
                                if(_locAcctPerLine != null)
                                {
                                    if (_locAcctPerLine.BalHold < _locAcctPerLine.Balance)
                                    {
                                        _locTUAmountDiscount2 = _locAcctPerLine.Balance - _locAcctPerLine.BalHold;
                                        if(_locTUAmountDiscount2 < _locTUAmountDiscount)
                                        {
                                            _locTUAmountDiscount = _locTUAmountDiscount2;
                                        }
                                        
                                    }else
                                    {
                                        _locTUAmountDiscount = 0;
                                    }
                                    _locAcctPerLine.BalHold = _locAcctPerLine.BalHold + _locTUAmountDiscount;
                                    _locAcctPerLine.Save();
                                    _locAcctPerLine.Session.CommitTransaction();

                                    _locTUAmountDiscount = 0;
                                    _locTUAmountDiscount2 = 0;
                                }
                                else
                                {
                                    if (_locDiscountRule1.AccountNo != null)
                                    {
                                        AccountingPeriodic _locAccountPeriodic = _currSession.FindObject<AccountingPeriodic>
                                                                        (new GroupOperator(GroupOperatorType.And,
                                                                        new BinaryOperator("Company", _locSalesInvoiceXPO.Company),
                                                                        new BinaryOperator("Workplace", _locSalesInvoiceXPO.Workplace),
                                                                        new BinaryOperator("Active", true)));
                                        if (_locAccountPeriodic != null)
                                        {
                                            AccountingPeriodicLine _locAccountingPeriodicLine = _currSession.FindObject<AccountingPeriodicLine>
                                                                                                (new GroupOperator(GroupOperatorType.And,
                                                                                                new BinaryOperator("AccountingPeriodic", _locAccountPeriodic),
                                                                                                new BinaryOperator("AccountNo", _locDiscountRule1.AccountNo)));
                                            if (_locAccountingPeriodicLine != null)
                                            {
                                                _locAcctPerLine = _locAccountingPeriodicLine;
                                                if (_locAccountingPeriodicLine.BalHold < _locAccountingPeriodicLine.Balance)
                                                {
                                                    _locTUAmountDiscount2 = _locAccountingPeriodicLine.Balance - _locAccountingPeriodicLine.BalHold;
                                                    if(_locTUAmountDiscount2 < _locTUAmountDiscount)
                                                    {
                                                        _locTUAmountDiscount = _locTUAmountDiscount2;
                                                    }
                                                }else
                                                {
                                                    _locTUAmountDiscount = 0;
                                                }

                                                _locAccountingPeriodicLine.BalHold = _locAccountingPeriodicLine.BalHold + _locTUAmountDiscount;
                                                _locAccountingPeriodicLine.Save();
                                                _locAccountingPeriodicLine.Session.CommitTransaction();
                                            }
                                        }
                                    }
                                }
                            }
                            #endregion ValBasedAccount

                            _locSalesInvoiceXPO.DiscountRule = _locDiscountRule1;
                            _locSalesInvoiceXPO.AccountingPeriodicLine = _locAcctPerLine;
                            _locSalesInvoiceXPO.Save();
                            _locSalesInvoiceXPO.Session.CommitTransaction();

                            _result = _locTUAmountDiscount;
                        }
                        #endregion Rule1
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = SalesInvoice ", ex.ToString());
            }
            return _result;
        }

        #endregion Discount

        #region Posting Method

        private double GetSalesInvoiceLineTotalAmount(Session _currSession, SalesInvoice _locSalesInvoiceXPO)
        {
            double _return = 0;
            try
            {
                if (_locSalesInvoiceXPO != null)
                {
                    double _locTAmount = 0;
                    XPCollection<SalesInvoiceLine> _locSalesInvoiceLines = new XPCollection<SalesInvoiceLine>
                                                                        (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                        new BinaryOperator("SalesInvoice", _locSalesInvoiceXPO),
                                                                        new BinaryOperator("Select", true),
                                                                        new GroupOperator(GroupOperatorType.Or,
                                                                        new BinaryOperator("Status", Status.Progress),
                                                                        new BinaryOperator("Status", Status.Posted))));

                    if (_locSalesInvoiceLines != null && _locSalesInvoiceLines.Count > 0)
                    {
                        foreach (SalesInvoiceLine _locSalesInvoiceLine in _locSalesInvoiceLines)
                        {
                            _locTAmount = _locTAmount + _locSalesInvoiceLine.TAmount;
                        }
                        _return = _locTAmount;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("BusinessObject = SalesInvoice" + ex.ToString());
            }
            return _return;
        }

        private void SetSalesInvoiceMonitoring(Session _currSession, SalesInvoice _locSalesInvoiceXPO)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                DateTime now = DateTime.Now;

                if (_locSalesInvoiceXPO != null)
                {
                    #region SalesInvoiceLine
                    XPCollection<SalesInvoiceLine> _locSalesInvoiceLines = new XPCollection<SalesInvoiceLine>
                                                        (_currSession, new GroupOperator(GroupOperatorType.And,
                                                        new BinaryOperator("SalesInvoice", _locSalesInvoiceXPO),
                                                        new BinaryOperator("Select", true),
                                                        new GroupOperator(GroupOperatorType.Or,
                                                        new BinaryOperator("Status", Status.Progress),
                                                        new BinaryOperator("Status", Status.Posted))));

                    if (_locSalesInvoiceLines != null && _locSalesInvoiceLines.Count() > 0)
                    {
                        foreach (SalesInvoiceLine _locSalesInvoiceLine in _locSalesInvoiceLines)
                        {
                            if (_locSalesInvoiceLine.TAmount > 0)
                            {
                                SalesInvoiceMonitoring _saveDataSalesInvoiceMonitoring = new SalesInvoiceMonitoring(_currSession)
                                {
                                    FreeItemChecked = _locSalesInvoiceLine.FreeItemChecked,
                                    DiscountChecked = _locSalesInvoiceLine.DiscountChecked,
                                    Company = _locSalesInvoiceLine.Company,
                                    Workplace = _locSalesInvoiceLine.Workplace,
                                    Division = _locSalesInvoiceLine.Division,
                                    Department = _locSalesInvoiceLine.Department,
                                    Section = _locSalesInvoiceLine.Section,
                                    Employee = _locSalesInvoiceLine.Employee,
                                    Div = _locSalesInvoiceLine.Div,
                                    Dept = _locSalesInvoiceLine.Dept,
                                    Sect = _locSalesInvoiceLine.Sect,
                                    PIC = _locSalesInvoiceXPO.PIC,
                                    Location = _locSalesInvoiceXPO.Location,
                                    BegInv = _locSalesInvoiceXPO.BegInv,
                                    Item = _locSalesInvoiceLine.Item,
                                    BegInvLine = _locSalesInvoiceLine.BegInvLine,
                                    Description = _locSalesInvoiceLine.Description,
                                    MxDQty = _locSalesInvoiceLine.DQty,
                                    MxDUOM = _locSalesInvoiceLine.DUOM,
                                    MxQty = _locSalesInvoiceLine.Qty,
                                    MxUOM = _locSalesInvoiceLine.UOM,
                                    MxTQty = _locSalesInvoiceLine.TQty,
                                    DQty = _locSalesInvoiceLine.DQty,
                                    DUOM = _locSalesInvoiceLine.DUOM,
                                    Qty = _locSalesInvoiceLine.Qty,
                                    UOM = _locSalesInvoiceLine.UOM,
                                    TQty = _locSalesInvoiceLine.TQty,
                                    StockType = _locSalesInvoiceLine.StockType,
                                    StockGroup = StockGroup.Normal,
                                    ETD = _locSalesInvoiceLine.ETD,
                                    ETA = _locSalesInvoiceLine.ETA,
                                    DocDate = _locSalesInvoiceLine.DocDate,
                                    JournalMonth = _locSalesInvoiceLine.JournalMonth,
                                    JournalYear = _locSalesInvoiceLine.JournalYear,
                                    SalesInvoice = _locSalesInvoiceXPO,
                                    SalesInvoiceLine = _locSalesInvoiceLine,
                                };
                                _saveDataSalesInvoiceMonitoring.Save();
                                _saveDataSalesInvoiceMonitoring.Session.CommitTransaction();
                            }
                        }
                    }
                    #endregion SalesInvoiceLine
                    #region SalesInvoiceFreeItem
                    XPCollection<SalesInvoiceFreeItem> _locSalesInvoiceFreeItems = new XPCollection<SalesInvoiceFreeItem>(_currSession,
                                                                            new GroupOperator(GroupOperatorType.And,
                                                                            new BinaryOperator("SalesInvoice", _locSalesInvoiceXPO)));
                    if (_locSalesInvoiceFreeItems != null && _locSalesInvoiceFreeItems.Count() > 0)
                    {
                        foreach (SalesInvoiceFreeItem _locSalesInvoiceFreeItem in _locSalesInvoiceFreeItems)
                        {
                            SalesInvoiceMonitoring _saveDataSalesInvoiceMonitoring = new SalesInvoiceMonitoring(_currSession)
                            {
                                Company = _locSalesInvoiceFreeItem.Company,
                                Workplace = _locSalesInvoiceFreeItem.Workplace,
                                Division = _locSalesInvoiceFreeItem.Division,
                                Department = _locSalesInvoiceFreeItem.Department,
                                Section = _locSalesInvoiceFreeItem.Section,
                                Employee = _locSalesInvoiceFreeItem.Employee,
                                Div = _locSalesInvoiceFreeItem.Div,
                                Dept = _locSalesInvoiceFreeItem.Dept,
                                Sect = _locSalesInvoiceFreeItem.Sect,
                                PIC = _locSalesInvoiceFreeItem.PIC,
                                Location = _locSalesInvoiceXPO.Location,
                                BegInv = _locSalesInvoiceXPO.BegInv,
                                Item = _locSalesInvoiceFreeItem.FpItem,
                                BegInvLine = _locSalesInvoiceFreeItem.BegInvLine,
                                MxDQty = _locSalesInvoiceFreeItem.FpDQty,
                                MxDUOM = _locSalesInvoiceFreeItem.FpDUOM,
                                MxQty = _locSalesInvoiceFreeItem.FpQty,
                                MxUOM = _locSalesInvoiceFreeItem.FpUOM,
                                MxTQty = _locSalesInvoiceFreeItem.FpTQty,
                                DQty = _locSalesInvoiceFreeItem.FpDQty,
                                DUOM = _locSalesInvoiceFreeItem.FpDUOM,
                                Qty = _locSalesInvoiceFreeItem.FpQty,
                                UOM = _locSalesInvoiceFreeItem.FpUOM,
                                TQty = _locSalesInvoiceFreeItem.FpTQty,
                                StockType = _locSalesInvoiceFreeItem.FpStockType,
                                StockGroup = _locSalesInvoiceFreeItem.FpStockGroup,
                                ETA = _locSalesInvoiceFreeItem.ETA,
                                ETD = _locSalesInvoiceFreeItem.ETD,
                                DocDate = _locSalesInvoiceFreeItem.DocDate,
                                JournalMonth = _locSalesInvoiceFreeItem.JournalMonth,
                                JournalYear = _locSalesInvoiceFreeItem.JournalYear,
                                SalesInvoice = _locSalesInvoiceXPO,
                                SalesInvoiceFreeItem = _locSalesInvoiceFreeItem,
                            };
                            _saveDataSalesInvoiceMonitoring.Save();
                            _saveDataSalesInvoiceMonitoring.Session.CommitTransaction();
                        }
                    }
                    #endregion SalesInvoiceFreeItem
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = SalesInvoice ", ex.ToString());
            }
        }

        private void SetStatusSalesInvoiceLine(Session _currSession, SalesInvoice _locSalesInvoiceXPO)
        {
            try
            {
                DateTime now = DateTime.Now;
                if (_locSalesInvoiceXPO != null)
                {
                    XPCollection<SalesInvoiceLine> _locSalesInvoiceLines = new XPCollection<SalesInvoiceLine>(_currSession,
                                                            new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("SalesInvoice", _locSalesInvoiceXPO),
                                                            new BinaryOperator("Select", true),
                                                            new GroupOperator(GroupOperatorType.Or,
                                                            new BinaryOperator("Status", Status.Progress),
                                                            new BinaryOperator("Status", Status.Posted))));

                    if (_locSalesInvoiceLines != null && _locSalesInvoiceLines.Count > 0)
                    {

                        foreach (SalesInvoiceLine _locSalesInvoiceLine in _locSalesInvoiceLines)
                        {
                            _locSalesInvoiceLine.Status = Status.Close;
                            _locSalesInvoiceLine.ActivationPosting = true;
                            _locSalesInvoiceLine.StatusDate = now;
                            _locSalesInvoiceLine.Save();
                            _locSalesInvoiceLine.Session.CommitTransaction();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = SalesInvoice " + ex.ToString());
            }
        }

        private void SetStatusSalesInvoiceFreeItem(Session _currSession, SalesInvoice _locSalesInvoiceXPO)
        {
            try
            {
                DateTime now = DateTime.Now;
                if (_locSalesInvoiceXPO != null)
                {
                    XPCollection<SalesOrderFreeItem> _locSalesInvoiceFreeItems = new XPCollection<SalesOrderFreeItem>(_currSession,
                                                            new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("SalesOrder", _locSalesInvoiceXPO),
                                                            new GroupOperator(GroupOperatorType.Or,
                                                            new BinaryOperator("Status", Status.Open),
                                                            new BinaryOperator("Status", Status.Progress),
                                                            new BinaryOperator("Status", Status.Posted))));

                    if (_locSalesInvoiceFreeItems != null && _locSalesInvoiceFreeItems.Count > 0)
                    {

                        foreach (SalesOrderFreeItem _locSalesInvoiceFreeItem in _locSalesInvoiceFreeItems)
                        {
                            _locSalesInvoiceFreeItem.Status = Status.Close;
                            _locSalesInvoiceFreeItem.StatusDate = now;
                            _locSalesInvoiceFreeItem.Save();
                            _locSalesInvoiceFreeItem.Session.CommitTransaction();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = SalesInvoice " + ex.ToString());
            }
        }

        private void SetFinalSalesInvoice(Session _currSession, SalesInvoice _locSalesInvoiceXPO)
        {
            try
            {
                DateTime now = DateTime.Now;
                int _locStatusCount = 0;
                int _locInvCanvLineCount = 0;

                if (_locSalesInvoiceXPO != null)
                {

                    XPCollection<SalesInvoiceLine> _locSalesInvoiceLines = new XPCollection<SalesInvoiceLine>(_currSession,
                                                            new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("SalesInvoice", _locSalesInvoiceXPO)));

                    if (_locSalesInvoiceLines != null && _locSalesInvoiceLines.Count() > 0)
                    {
                        _locInvCanvLineCount = _locSalesInvoiceLines.Count();

                        foreach (SalesInvoiceLine _locSalesInvoiceLine in _locSalesInvoiceLines)
                        {
                            if (_locSalesInvoiceLine.Status == Status.Close)
                            {
                                _locStatusCount = _locStatusCount + 1;
                            }
                        }
                    }

                    if (_locSalesInvoiceXPO.PostedCount == 0)
                    {
                        double _locTotCurrCredit = 0;
                        bool _locOverCredit = false;
                        if (_locSalesInvoiceXPO.CreditLimitList != null)
                        {
                            _locTotCurrCredit = _locSalesInvoiceXPO.CreditLimitList.CurrCredit + _locSalesInvoiceXPO.Amount;
                            if(_locTotCurrCredit > _locSalesInvoiceXPO.CreditLimitList.MaxCredit)
                            {
                                _locOverCredit = true;
                            }
                            _locSalesInvoiceXPO.CreditLimitList.OverCredit = _locOverCredit;
                            _locSalesInvoiceXPO.CreditLimitList.CurrCredit = _locTotCurrCredit;
                            _locSalesInvoiceXPO.CreditLimitList.Save();
                            _locSalesInvoiceXPO.CreditLimitList.Session.CommitTransaction();
                        }
                    }

                    if (_locStatusCount == _locInvCanvLineCount)
                    {
                        _locSalesInvoiceXPO.ActivationPosting = true;
                        _locSalesInvoiceXPO.Status = Status.Posted;
                        _locSalesInvoiceXPO.StatusDate = now;
                        _locSalesInvoiceXPO.PostedCount = _locSalesInvoiceXPO.PostedCount + 1;
                        _locSalesInvoiceXPO.Save();
                        _locSalesInvoiceXPO.Session.CommitTransaction();
                    }
                    else
                    {
                        _locSalesInvoiceXPO.Status = Status.Posted;
                        _locSalesInvoiceXPO.StatusDate = now;
                        _locSalesInvoiceXPO.PostedCount = _locSalesInvoiceXPO.PostedCount + 1;
                        _locSalesInvoiceXPO.Save();
                        _locSalesInvoiceXPO.Session.CommitTransaction();
                    }


                    
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = SalesInvoice " + ex.ToString());
            }
        }

        #endregion Posting Method

        #region Global Method

        private void SuccessMessageShow(string _locActionName)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 2000;
            options.Message = string.Format("{0} !", _locActionName);
            options.Type = InformationType.Success;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Success";
            options.Win.Type = WinMessageType.Flyout;
            Application.ShowViewStrategy.ShowMessage(options);
        }

        private void ErrorMessageShow(string _locMessage)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 2000;
            options.Message = string.Format(_locMessage);
            options.Type = InformationType.Warning;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Error";
            options.Win.Type = WinMessageType.Alert;
            Application.ShowViewStrategy.ShowMessage(options);
        }

        #endregion Global Method

        #region Approval Process

        private void SetApprovalLine(Session _currentSession, SalesInvoice _locSalesInvoiceXPO, ApprovalLevel _locApprovalLevel)
        {
            try
            {
                DateTime now = DateTime.Now;

                ApprovalLine _locApprovalLineXPO = _currentSession.FindObject<ApprovalLine>
                                                                        (new GroupOperator(GroupOperatorType.And,
                                                                         new BinaryOperator("SalesInvoice", _locSalesInvoiceXPO),
                                                                         new BinaryOperator("ApprovalLevel", _locApprovalLevel)));

                if (_locApprovalLineXPO == null)
                {
                    ApprovalLine _saveDataAL2 = new ApprovalLine(_currentSession)
                    {
                        ApprovalDate = now,
                        ApprovalStatus = Status.Approved,
                        ApprovalLevel = _locApprovalLevel,
                        SalesInvoice = _locSalesInvoiceXPO,
                    };
                    _saveDataAL2.Save();
                    _saveDataAL2.Session.CommitTransaction();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = SalesInvoice " + ex.ToString());
            }
        }

        #endregion Approval Process

        #region Email

        private string BackgroundBody(Session _currentSession, SalesInvoice _locSalesInvoiceXPO, MailSetupDetail _locMailSetDetail)
        {
            string body = string.Empty;
            if (_locSalesInvoiceXPO != null)
            {
                ApprovalLine _locApprovalLineXPO = _currentSession.FindObject<ApprovalLine>
                                               (new GroupOperator(GroupOperatorType.And,
                                                new BinaryOperator("SalesInvoice", _locSalesInvoiceXPO)));

                string Status = _locApprovalLineXPO.ApprovalStatus.ToString();

                using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/Email/EmailTemplate.html")))
                {
                    body = reader.ReadToEnd();
                }
                body = body.Replace("{Code}", _locSalesInvoiceXPO.Code);

                #region Level
                if (_locSalesInvoiceXPO.ActiveApproved1 == true)
                {
                    string Active = "Level 1";
                    body = body.Replace("{Level}", Active);
                }
                else if (_locSalesInvoiceXPO.ActiveApproved2 == true)
                {
                    string Active = "Level 2";
                    body = body.Replace("{Level}", Active);
                }
                else if (_locSalesInvoiceXPO.ActiveApproved3 == true)
                {
                    string Active = "Level 3";
                    body = body.Replace("{Level}", Active);
                }
                else
                {
                    string Active = "Not Available";
                    body = body.Replace("{Level}", Active);
                }
                #endregion Level

                body = body.Replace("{Status}", Status);
                body = body.Replace("{Message}", _locMailSetDetail.MessageBody);
            }
            return body;
        }

        protected void SendEmail(Session _currentSession, SalesInvoice _locSalesInvoiceXPO, Status _locStatus, ApprovalLine _locApprovalLineXPO2)
        {
            DateTime now = DateTime.Now;
            var User = SecuritySystem.CurrentUserName;
            GlobalFunction _globFunc = new GlobalFunction();
            UserAccess _numLineUserAccess = _currentSession.FindObject<UserAccess>(new BinaryOperator("UserName", User));
            string _locMessageBody = null;

            try
            {
                if (_locSalesInvoiceXPO != null)
                {
                    XPCollection<MailSetupDetail> _locMailSetDetails = new XPCollection<MailSetupDetail>
                                                                       (_currentSession, new GroupOperator(GroupOperatorType.And,
                                                                        new BinaryOperator("ObjectList", ObjectList.SalesOrder),
                                                                        new BinaryOperator("UserAccess", _numLineUserAccess),
                                                                        new BinaryOperator("Active", true)));

                    if (_locMailSetDetails != null && _locMailSetDetails.Count > 0)
                    {
                        foreach (MailSetupDetail _locMailSetDetail in _locMailSetDetails)
                        {
                            if (_locMailSetDetail.MessageBody != null)
                            {
                                if (_locApprovalLineXPO2.EndApproval == true)
                                {
                                    _locMessageBody = this.BackgroundBody(_currentSession, _locSalesInvoiceXPO, _locMailSetDetail);
                                }
                                else
                                {
                                    _locMessageBody = this.BackgroundBody(_currentSession, _locSalesInvoiceXPO, _locMailSetDetail);
                                }
                            }

                            _globFunc.SetAndSendMail(_locMailSetDetail.SmtpHost, _locMailSetDetail.SmtpPort,
                            _locMailSetDetail.MailFrom, _locMailSetDetail.MailFromPassword, _locMailSetDetail.MailTo, _locMailSetDetail.MailSubject, _locMessageBody);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = SalesInvoice " + ex.ToString());
            }
        }



        #endregion Email

        
    }
}
