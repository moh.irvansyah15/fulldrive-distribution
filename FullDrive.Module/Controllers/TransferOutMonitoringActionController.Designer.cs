﻿namespace FullDrive.Module.Controllers
{
    partial class TransferOutMonitoringActionController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.TransferOutMonitoringSelectAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.TransferOutMonitoringUnselectAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.TransferOutMonitoringCancelTIAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            // 
            // TransferOutMonitoringSelectAction
            // 
            this.TransferOutMonitoringSelectAction.Caption = "Select";
            this.TransferOutMonitoringSelectAction.ConfirmationMessage = null;
            this.TransferOutMonitoringSelectAction.Id = "TransferOutMonitoringSelectActionId";
            this.TransferOutMonitoringSelectAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.TransferOutMonitoring);
            this.TransferOutMonitoringSelectAction.ToolTip = null;
            this.TransferOutMonitoringSelectAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.TransferOutMonitoringSelectAction_Execute);
            // 
            // TransferOutMonitoringUnselectAction
            // 
            this.TransferOutMonitoringUnselectAction.Caption = "Unselect";
            this.TransferOutMonitoringUnselectAction.ConfirmationMessage = null;
            this.TransferOutMonitoringUnselectAction.Id = "TransferOutMonitoringUnselectActionId";
            this.TransferOutMonitoringUnselectAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.TransferOutMonitoring);
            this.TransferOutMonitoringUnselectAction.ToolTip = null;
            this.TransferOutMonitoringUnselectAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.TransferOutMonitoringUnselectAction_Execute);
            // 
            // TransferOutMonitoringCancelTIAction
            // 
            this.TransferOutMonitoringCancelTIAction.Caption = "Cancel TI";
            this.TransferOutMonitoringCancelTIAction.ConfirmationMessage = null;
            this.TransferOutMonitoringCancelTIAction.Id = "TransferOutMonitoringCancelTIActionId";
            this.TransferOutMonitoringCancelTIAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.TransferOutMonitoring);
            this.TransferOutMonitoringCancelTIAction.ToolTip = null;
            this.TransferOutMonitoringCancelTIAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.TransferOutMonitoringCancelTIAction_Execute);
            // 
            // TransferOutMonitoringActionController
            // 
            this.Actions.Add(this.TransferOutMonitoringSelectAction);
            this.Actions.Add(this.TransferOutMonitoringUnselectAction);
            this.Actions.Add(this.TransferOutMonitoringCancelTIAction);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SimpleAction TransferOutMonitoringSelectAction;
        private DevExpress.ExpressApp.Actions.SimpleAction TransferOutMonitoringUnselectAction;
        private DevExpress.ExpressApp.Actions.SimpleAction TransferOutMonitoringCancelTIAction;
    }
}
