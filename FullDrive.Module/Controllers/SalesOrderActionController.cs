﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;

using FullDrive.Module.BusinessObjects;
using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.Xpo;
using System.Collections;
using DevExpress.Xpo;
using System.IO;
using System.Web;

namespace FullDrive.Module.Controllers
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class SalesOrderActionController : ViewController
    {
        private ChoiceActionItem _selectionListviewFilter;
        private ChoiceActionItem _selectionApprovalListviewFilter;
        private ChoiceActionItem _setApprovalLevel;

        public SalesOrderActionController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.
            #region FilterStatus
            SalesOrderListviewFilterSelectionAction.Items.Clear();
            foreach (object _currApproval in Enum.GetValues(typeof(CustomProcess.Status)))
            {
                EnumDescriptor _ed = new EnumDescriptor(typeof(CustomProcess.Status));
                _selectionListviewFilter = new ChoiceActionItem(_ed.GetCaption(_currApproval), _currApproval);
                SalesOrderListviewFilterSelectionAction.Items.Add(_selectionListviewFilter);
            }
            #endregion FilterStatus

            #region FilterApproval
            SalesOrderListviewFilterApprovalSelectionAction.Items.Clear();
            foreach (object _currApprovalFilter in Enum.GetValues(typeof(CustomProcess.ApprovalFilter)))
            {
                EnumDescriptor _ed = new EnumDescriptor(typeof(CustomProcess.ApprovalFilter));
                _selectionApprovalListviewFilter = new ChoiceActionItem(_ed.GetCaption(_currApprovalFilter), _currApprovalFilter);
                SalesOrderListviewFilterApprovalSelectionAction.Items.Add(_selectionApprovalListviewFilter);
            }
            #endregion FilterApproval
        }
        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
            #region Approval
            var _locUser = SecuritySystem.CurrentUserName;

            if (this.ObjectSpace != null)
            {
                UserAccess _locUserAccess = this.ObjectSpace.FindObject<UserAccess>(new BinaryOperator("UserName", _locUser));

                if (_locUserAccess != null)
                {
                    SalesOrderApprovalAction.Items.Clear();

                    XPCollection<ApplicationSetupDetail> _locAppSetupDetails = new XPCollection<ApplicationSetupDetail>
                                                                                 (_locUserAccess.Session, new GroupOperator(GroupOperatorType.And,
                                                                                  new BinaryOperator("UserAccess", _locUserAccess),
                                                                                  new BinaryOperator("ObjectList", CustomProcess.ObjectList.SalesOrder),
                                                                                  new BinaryOperator("FunctionList", CustomProcess.FunctionList.Approval),
                                                                                  new BinaryOperator("Active", true)));

                    if (_locAppSetupDetails != null && _locAppSetupDetails.Count > 0)
                    {

                        foreach (ApplicationSetupDetail _locAppSetupDetail in _locAppSetupDetails)
                        {
                            foreach (object _currApproval in Enum.GetValues(typeof(CustomProcess.ApprovalLevel)))
                            {
                                if (_locAppSetupDetail.ApprovalLevel != CustomProcess.ApprovalLevel.None)
                                {
                                    EnumDescriptor _ed = new EnumDescriptor(typeof(CustomProcess.ApprovalLevel));
                                    _setApprovalLevel = new ChoiceActionItem(_ed.GetCaption(_locAppSetupDetail.ApprovalLevel), _locAppSetupDetail.ApprovalLevel);
                                }
                            }
                            SalesOrderApprovalAction.Items.Add(_setApprovalLevel);
                        }
                    }
                }
            }
            #endregion Approval
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }
        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        private void SalesOrderListviewFilterSelectionAction_Execute(object sender, SingleChoiceActionExecuteEventArgs e)
        {
            try
            {
                if ((View is ListView) & (View.ObjectTypeInfo.Type == typeof(SalesOrder)))
                {
                    if ((Status)e.SelectedChoiceActionItem.Data == Status.None)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.None, BinaryOperatorType.NotEqual);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Open)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Open, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Progress)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Progress, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Posted)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Posted, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Lock)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Lock, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Close)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Close, BinaryOperatorType.Equal);
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = SalesOrder " + ex.ToString());
            }
        }

        private void SalesOrderListviewFilterApprovalSelectionAction_Execute(object sender, SingleChoiceActionExecuteEventArgs e)
        {
            try
            {
                if ((View is ListView) & (View.ObjectTypeInfo.Type == typeof(SalesOrder)))
                {
                    if ((ApprovalFilter)e.SelectedChoiceActionItem.Data == ApprovalFilter.Approval)
                    {
                        ((ListView)View).CollectionSource.Criteria["FilterApproval1"] = new GroupOperator(GroupOperatorType.And,
                                                                                        new BinaryOperator("ActiveApproved3", false, BinaryOperatorType.Equal),
                                                                                        new GroupOperator(GroupOperatorType.Or,
                                                                                        new BinaryOperator("ActiveApproved1", true, BinaryOperatorType.Equal),
                                                                                        new BinaryOperator("ActiveApproved2", true, BinaryOperatorType.Equal)));
                    }
                    else if ((ApprovalFilter)e.SelectedChoiceActionItem.Data == ApprovalFilter.EndApproval)
                    {
                        ((ListView)View).CollectionSource.Criteria["FilterApproval1"] = new BinaryOperator("ActiveApproved3", true, BinaryOperatorType.Equal);
                    }
                    else if ((ApprovalFilter)e.SelectedChoiceActionItem.Data == ApprovalFilter.AllData)
                    {
                        ((ListView)View).CollectionSource.Criteria["FilterApproval1"] = new GroupOperator(GroupOperatorType.Or,
                                                                                        new BinaryOperator("ActiveApproved1", true, BinaryOperatorType.Equal),
                                                                                        new BinaryOperator("ActiveApproved2", true, BinaryOperatorType.Equal),
                                                                                        new BinaryOperator("ActiveApproved3", true, BinaryOperatorType.Equal),
                                                                                        new BinaryOperator("ActiveApproved1", false, BinaryOperatorType.Equal),
                                                                                        new BinaryOperator("ActiveApproved2", false, BinaryOperatorType.Equal),
                                                                                        new BinaryOperator("ActiveApproved3", false, BinaryOperatorType.Equal));
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = SalesOrder " + ex.ToString());
            }
        }

        private void SalesOrderApprovalAction_Execute(object sender, SingleChoiceActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                ApplicationSetupDetail _locAppSetDetail = null;
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                var User = SecuritySystem.CurrentUserName;
                Session _currentSession = null;
                string _currObjectId = null;

                foreach (Object obj in _objectsToProcess)
                {
                    SalesOrder _objInNewObjectSpace = (SalesOrder)_objectSpace.GetObject(obj);

                    if (_objInNewObjectSpace != null)
                    {
                        if (_objInNewObjectSpace.Code != null)
                        {
                            _currObjectId = _objInNewObjectSpace.Code;
                        }
                        if (_objInNewObjectSpace.Session != null)
                        {
                            _currentSession = _objInNewObjectSpace.Session;
                        }
                    }

                    if (_currObjectId != null && _currentSession != null)
                    {
                        UserAccess _locUserAccess = _currentSession.FindObject<UserAccess>(new BinaryOperator("UserName", User));

                        SalesOrder _locSalesOrderXPO = _currentSession.FindObject<SalesOrder>
                                                                        (new GroupOperator(GroupOperatorType.And,
                                                                         new BinaryOperator("Code", _currObjectId)));

                        if (_locSalesOrderXPO != null)
                        {
                            if (_locSalesOrderXPO.Status == Status.Open)
                            {
                                ApprovalLine _locApprovalLine = _currentSession.FindObject<ApprovalLine>
                                                                            (new GroupOperator(GroupOperatorType.And,
                                                                             new BinaryOperator("SalesOrder", _locSalesOrderXPO),
                                                                             new BinaryOperator("EndApproval", true)));
                                if (_locApprovalLine == null)
                                {
                                    #region Approval Level 1
                                    if ((ApprovalLevel)e.SelectedChoiceActionItem.Data == ApprovalLevel.Level1)
                                    {
                                        _locAppSetDetail = _globFunc.GetApplicationSetupDetailByApprovalLevel(_currentSession, _locUserAccess, ApprovalLevel.Level1, ObjectList.SalesOrder);
                                        if (_locAppSetDetail != null)
                                        {
                                            //Buat bs input langsung ke approvalline
                                            ApprovalLine _locApprovalLineXPO = _currentSession.FindObject<ApprovalLine>
                                                                                (new GroupOperator(GroupOperatorType.And,
                                                                                 new BinaryOperator("SalesOrder", _locSalesOrderXPO),
                                                                                 new BinaryOperator("ApprovalLevel", ApprovalLevel.Level1)));
                                            if (_locApprovalLineXPO == null)
                                            {

                                                if (_locAppSetDetail.EndApproval == true)
                                                {
                                                    ApprovalLine _saveDataAL = new ApprovalLine(_currentSession)
                                                    {
                                                        ApprovalDate = now,
                                                        ApprovalStatus = Status.Approved,
                                                        ApprovalLevel = ApprovalLevel.Level1,
                                                        EndApproval = true,
                                                        SalesOrder = _locSalesOrderXPO,
                                                    };
                                                    _saveDataAL.Save();
                                                    _saveDataAL.Session.CommitTransaction();

                                                    _locSalesOrderXPO.ActivationPosting = true;
                                                    _locSalesOrderXPO.ActiveApproved1 = false;
                                                    _locSalesOrderXPO.ActiveApproved2 = false;
                                                    _locSalesOrderXPO.ActiveApproved3 = true;
                                                    _locSalesOrderXPO.Save();
                                                    _locSalesOrderXPO.Session.CommitTransaction();
                                                }
                                                else
                                                {
                                                    ApprovalLine _saveDataAL = new ApprovalLine(_currentSession)
                                                    {
                                                        ApprovalDate = now,
                                                        ApprovalStatus = Status.Approved,
                                                        ApprovalLevel = ApprovalLevel.Level1,
                                                        SalesOrder = _locSalesOrderXPO,
                                                    };
                                                    _saveDataAL.Save();
                                                    _saveDataAL.Session.CommitTransaction();

                                                    _locSalesOrderXPO.ActiveApproved1 = true;
                                                    _locSalesOrderXPO.ActiveApproved2 = false;
                                                    _locSalesOrderXPO.ActiveApproved3 = false;
                                                    _locSalesOrderXPO.Save();
                                                    _locSalesOrderXPO.Session.CommitTransaction();
                                                }

                                                //Send Email
                                                ApprovalLine _locApprovalLineXPO2 = _currentSession.FindObject<ApprovalLine>
                                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                                     new BinaryOperator("SalesOrder", _locSalesOrderXPO)));

                                                if (_locApprovalLineXPO2 != null)
                                                {
                                                    SendEmail(_currentSession, _locSalesOrderXPO, Status.Progress, _locApprovalLineXPO2);
                                                }

                                                SuccessMessageShow("SalesOrder has successfully Approve");
                                            }
                                        }
                                    }
                                    #endregion Approval Level 1

                                    #region Approval Level 2
                                    if ((ApprovalLevel)e.SelectedChoiceActionItem.Data == ApprovalLevel.Level2)
                                    {
                                        _locAppSetDetail = _globFunc.GetApplicationSetupDetailByApprovalLevel(_currentSession, _locUserAccess, ApprovalLevel.Level2, ObjectList.SalesOrder);

                                        if (_locAppSetDetail != null)
                                        {
                                            ApprovalLine _locApprovalLineXPO = _currentSession.FindObject<ApprovalLine>
                                                                                (new GroupOperator(GroupOperatorType.And,
                                                                                 new BinaryOperator("SalesOrder", _locSalesOrderXPO),
                                                                                 new BinaryOperator("ApprovalLevel", ApprovalLevel.Level2)));

                                            if (_locApprovalLineXPO == null)
                                            {
                                                if (_locAppSetDetail.EndApproval == true)
                                                {
                                                    ApprovalLine _saveDataAL = new ApprovalLine(_currentSession)
                                                    {
                                                        ApprovalDate = now,
                                                        ApprovalStatus = Status.Approved,
                                                        ApprovalLevel = ApprovalLevel.Level2,
                                                        EndApproval = true,
                                                        SalesOrder = _locSalesOrderXPO,
                                                    };
                                                    _saveDataAL.Save();
                                                    _saveDataAL.Session.CommitTransaction();

                                                    _locSalesOrderXPO.ActivationPosting = true;
                                                    _locSalesOrderXPO.ActiveApproved1 = false;
                                                    _locSalesOrderXPO.ActiveApproved2 = false;
                                                    _locSalesOrderXPO.ActiveApproved3 = true;
                                                    _locSalesOrderXPO.Save();
                                                    _locSalesOrderXPO.Session.CommitTransaction();
                                                }
                                                else
                                                {
                                                    ApprovalLine _saveDataAL = new ApprovalLine(_currentSession)
                                                    {
                                                        ApprovalDate = now,
                                                        ApprovalStatus = Status.Approved,
                                                        ApprovalLevel = ApprovalLevel.Level2,
                                                        SalesOrder = _locSalesOrderXPO,
                                                    };
                                                    _saveDataAL.Save();
                                                    _saveDataAL.Session.CommitTransaction();

                                                    SetApprovalLine(_currentSession, _locSalesOrderXPO, ApprovalLevel.Level1);

                                                    _locSalesOrderXPO.ActiveApproved1 = false;
                                                    _locSalesOrderXPO.ActiveApproved2 = true;
                                                    _locSalesOrderXPO.ActiveApproved3 = false;
                                                    _locSalesOrderXPO.Save();
                                                    _locSalesOrderXPO.Session.CommitTransaction();
                                                }

                                                //Send Email
                                                ApprovalLine _locApprovalLineXPO2 = _currentSession.FindObject<ApprovalLine>
                                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                                     new BinaryOperator("SalesOrder", _locSalesOrderXPO)));

                                                if (_locApprovalLineXPO2 != null)
                                                {
                                                    SendEmail(_currentSession, _locSalesOrderXPO, Status.Progress, _locApprovalLineXPO2);
                                                }

                                                SuccessMessageShow("SalesOrder has successfully Approve");
                                            }
                                        }
                                    }
                                    #endregion Approval Level 2

                                    #region Approval Level 3
                                    if ((ApprovalLevel)e.SelectedChoiceActionItem.Data == ApprovalLevel.Level3)
                                    {
                                        _locAppSetDetail = _globFunc.GetApplicationSetupDetailByApprovalLevel(_currentSession, _locUserAccess, ApprovalLevel.Level3, ObjectList.SalesOrder);

                                        if (_locAppSetDetail != null)
                                        {
                                            ApprovalLine _locApprovalLineXPO = _currentSession.FindObject<ApprovalLine>
                                                                                (new GroupOperator(GroupOperatorType.And,
                                                                                 new BinaryOperator("SalesOrder", _locSalesOrderXPO),
                                                                                 new BinaryOperator("ApprovalLevel", ApprovalLevel.Level3)));

                                            if (_locApprovalLineXPO == null)
                                            {
                                                if (_locAppSetDetail.EndApproval == true)
                                                {
                                                    ApprovalLine _saveDataAL = new ApprovalLine(_currentSession)
                                                    {
                                                        ApprovalDate = now,
                                                        ApprovalStatus = Status.Approved,
                                                        ApprovalLevel = ApprovalLevel.Level3,
                                                        EndApproval = true,
                                                        SalesOrder = _locSalesOrderXPO,
                                                    };
                                                    _saveDataAL.Save();
                                                    _saveDataAL.Session.CommitTransaction();

                                                    _locSalesOrderXPO.ActivationPosting = true;
                                                    _locSalesOrderXPO.ActiveApproved1 = false;
                                                    _locSalesOrderXPO.ActiveApproved2 = false;
                                                    _locSalesOrderXPO.ActiveApproved3 = true;
                                                    _locSalesOrderXPO.Save();
                                                    _locSalesOrderXPO.Session.CommitTransaction();
                                                }
                                                else
                                                {
                                                    ApprovalLine _saveDataAL = new ApprovalLine(_currentSession)
                                                    {
                                                        ApprovalDate = now,
                                                        ApprovalStatus = Status.Approved,
                                                        ApprovalLevel = ApprovalLevel.Level3,
                                                        SalesOrder = _locSalesOrderXPO,
                                                    };
                                                    _saveDataAL.Save();
                                                    _saveDataAL.Session.CommitTransaction();

                                                    SetApprovalLine(_currentSession, _locSalesOrderXPO, ApprovalLevel.Level2);
                                                    SetApprovalLine(_currentSession, _locSalesOrderXPO, ApprovalLevel.Level1);

                                                    _locSalesOrderXPO.ActiveApproved1 = false;
                                                    _locSalesOrderXPO.ActiveApproved2 = false;
                                                    _locSalesOrderXPO.ActiveApproved3 = true;
                                                    _locSalesOrderXPO.Save();
                                                    _locSalesOrderXPO.Session.CommitTransaction();
                                                }

                                                //Send Email
                                                ApprovalLine _locApprovalLineXPO2 = _currentSession.FindObject<ApprovalLine>
                                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                                     new BinaryOperator("SalesOrder", _locSalesOrderXPO)));

                                                if (_locApprovalLineXPO2 != null)
                                                {
                                                    SendEmail(_currentSession, _locSalesOrderXPO, Status.Progress, _locApprovalLineXPO2);
                                                }

                                                SuccessMessageShow("SalesOrder has successfully Approve");
                                            }
                                        }
                                    }
                                    #endregion Approval Level 3
                                }
                            }
                            else
                            {
                                ErrorMessageShow("SalesOrder Status Not Available");
                            }
                        }
                        else
                        {
                            ErrorMessageShow("SalesOrder Not Available");
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = SalesOrder " + ex.ToString());
            }
        }

        private void SalesOrderProgressAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;
                Status _locStatus = Status.Open;

                if (_objectsToProcess != null)
                {
                    foreach (Object obj in _objectsToProcess)
                    {
                        SalesOrder _locSalesOrderOS = (SalesOrder)_objectSpace.GetObject(obj);

                        if (_locSalesOrderOS != null)
                        {
                            if (_locSalesOrderOS.Session != null)
                            {
                                _currSession = _locSalesOrderOS.Session;
                            }

                            if (_locSalesOrderOS.Code != null && _currSession != null)
                            {
                                _currObjectId = _locSalesOrderOS.Code;

                                SalesOrder _locSalesOrderXPO = _currSession.FindObject<SalesOrder>
                                                                   (new GroupOperator(GroupOperatorType.And,
                                                                    new BinaryOperator("Code", _currObjectId),
                                                                    new GroupOperator(GroupOperatorType.Or,
                                                                    new BinaryOperator("Status", Status.Open),
                                                                    new BinaryOperator("Status", Status.Progress))));

                                if (_locSalesOrderXPO != null && _locSalesOrderXPO.CreditLimitStatus != CreditLimitStatus.Hold)
                                {
                                    ApprovalLine _locApprovalLine = _currSession.FindObject<ApprovalLine>
                                                                            (new GroupOperator(GroupOperatorType.And,
                                                                             new BinaryOperator("SalesOrder", _locSalesOrderXPO),
                                                                             new BinaryOperator("EndApproval", true)));
                                    if (_locApprovalLine != null)
                                    {

                                        //Pengecekan
                                        if (_locSalesOrderXPO.Status == Status.Open)
                                        {
                                            _locStatus = Status.Progress;

                                        }
                                        else
                                        {
                                            _locStatus = _locSalesOrderXPO.Status;
                                        }

                                        _locSalesOrderXPO.Status = _locStatus;
                                        _locSalesOrderXPO.StatusDate = now;
                                        _locSalesOrderXPO.Save();
                                        _locSalesOrderXPO.Session.CommitTransaction();

                                        #region SalesOrderLine
                                        if(GetAndSetForCheckQtyOnHandAlsoChangeStatusToProgress(_currSession, _locSalesOrderXPO) == false )
                                        {
                                            SetTotalAmountAndTaxAmountAndDiscountAmount(_currSession, _locSalesOrderXPO);
                                        }
                                        #endregion SalesOrderLine
                                        SetOverCreditStatus(_currSession, _locSalesOrderXPO);
                                        SuccessMessageShow(_locSalesOrderXPO.Code + " has been change successfully to Progress ");
                                    }
                                   
                                }
                                else
                                {
                                    ErrorMessageShow("Data SalesOrder Not Available");
                                }
                            }
                            else
                            {
                                ErrorMessageShow("Data SalesOrder Not Available");
                            }
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = SalesOrder " + ex.ToString());
            }
        }

        private void SalesOrderSetFreeItemAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;

                if (_objectsToProcess != null)
                {
                    foreach (Object obj in _objectsToProcess)
                    {
                        SalesOrder _locSalesOrderOS = (SalesOrder)_objectSpace.GetObject(obj);

                        if (_locSalesOrderOS != null)
                        {
                            if (_locSalesOrderOS.Session != null)
                            {
                                _currSession = _locSalesOrderOS.Session;
                            }

                            if (_locSalesOrderOS.Code != null && _currSession != null)
                            {
                                _currObjectId = _locSalesOrderOS.Code;

                                SalesOrder _locSalesOrderXPO = _currSession.FindObject<SalesOrder>
                                                                   (new GroupOperator(GroupOperatorType.And,
                                                                    new BinaryOperator("Code", _currObjectId),
                                                                    new BinaryOperator("Status", Status.Progress)));

                                if (_locSalesOrderXPO != null)
                                {
                                    ApprovalLine _locApprovalLine = _currSession.FindObject<ApprovalLine>
                                                                            (new GroupOperator(GroupOperatorType.And,
                                                                             new BinaryOperator("SalesOrder", _locSalesOrderXPO),
                                                                             new BinaryOperator("EndApproval", true)));
                                    if (_locApprovalLine != null)
                                    {
                                        SetFreeItem(_currSession, _locSalesOrderXPO);
                                        SetTotalFreeItemAmount(_currSession, _locSalesOrderXPO);
                                        SuccessMessageShow(_locSalesOrderXPO.Code + " has been successfully create Free Item  ");
                                    }      
                                }
                                else
                                {
                                    ErrorMessageShow("Data SalesOrder Not Available");
                                }
                            }
                            else
                            {
                                ErrorMessageShow("Data SalesOrder Not Available");
                            }
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = SalesOrder " + ex.ToString());
            }
        }

        private void SalesOrderPostingAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {

                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;

                if (_objectsToProcess != null)
                {
                    foreach (Object obj in _objectsToProcess)
                    {
                        SalesOrder _locSalesOrderOS = (SalesOrder)_objectSpace.GetObject(obj);

                        if (_locSalesOrderOS != null)
                        {
                            if (_locSalesOrderOS.Session != null)
                            {
                                _currSession = _locSalesOrderOS.Session;
                            }

                            if (_locSalesOrderOS.Code != null && _currSession != null)
                            {
                                _currObjectId = _locSalesOrderOS.Code;

                                SalesOrder _locSalesOrderXPO = _currSession.FindObject<SalesOrder>
                                                                   (new GroupOperator(GroupOperatorType.And,
                                                                    new BinaryOperator("Code", _currObjectId)));

                                if (_locSalesOrderXPO != null)
                                {
                                    if (_locSalesOrderXPO.Status == Status.Progress || _locSalesOrderXPO.Status == Status.Posted)
                                    {
                                        if (_locSalesOrderXPO.TotAmount > 0)
                                        {
                                            //Berdasarkan Outstanding dari PaymentOutPlan yg mana di hitung berdasarkan jumlah mak Outstanding 
                                            if (_locSalesOrderXPO.TotAmount == GetSalesOrderLineTotalAmount(_currSession, _locSalesOrderXPO) && _locSalesOrderXPO.CreditLimitStatus != CreditLimitStatus.Hold)
                                            {
                                                SetSalesOrderMonitoring(_currSession, _locSalesOrderXPO);
                                                SetStatusSalesOrderLine(_currSession, _locSalesOrderXPO);
                                                SetStatusSalesOrderFreeItem(_currSession, _locSalesOrderXPO);
                                                SetFinalSalesOrder(_currSession, _locSalesOrderXPO);
                                                SuccessMessageShow(_locSalesOrderXPO.Code + " has been change successfully to Posted");
                                            }
                                            else
                                            {
                                                ErrorMessageShow("Data SalesOrder Not Available");
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    ErrorMessageShow("Data SalesOrder Not Available");
                                }
                            }
                            else
                            {
                                ErrorMessageShow("Data SalesOrder Not Available");
                            }
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = SalesOrder " + ex.ToString());
            }
        }

        private void SalesOrderReleaseCLAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;
                var User = SecuritySystem.CurrentUserName;
                string _locMessage = null;

                if (_objectsToProcess != null)
                {
                    foreach (Object obj in _objectsToProcess)
                    {
                        SalesOrder _locSalesOrderOS = (SalesOrder)_objectSpace.GetObject(obj);

                        if (_locSalesOrderOS != null)
                        {
                            if (_locSalesOrderOS.Session != null)
                            {
                                _currSession = _locSalesOrderOS.Session;
                            }

                            if (_locSalesOrderOS.Code != null && _currSession != null)
                            {
                                _currObjectId = _locSalesOrderOS.Code;

                                SalesOrder _locSalesOrderXPO = _currSession.FindObject<SalesOrder>
                                                                   (new GroupOperator(GroupOperatorType.And,
                                                                    new BinaryOperator("Code", _currObjectId),
                                                                    new GroupOperator(GroupOperatorType.Or,
                                                                    new BinaryOperator("Status", Status.Open),
                                                                    new BinaryOperator("Status", Status.Progress),
                                                                    new BinaryOperator("Status", Status.Posted))));

                                if (_locSalesOrderXPO != null)
                                {
                                    UserAccess _locUserAccess = _currSession.FindObject<UserAccess>(new BinaryOperator("UserName", User));
                                    if(_locUserAccess != null)
                                    {
                                        _locMessage = "Released By " + _locUserAccess.UserName;
                                    }else
                                    {
                                        _locMessage = "Released";
                                    }

                                    _locSalesOrderXPO.OverCredit = false;
                                    _locSalesOrderXPO.Message = _locMessage;
                                    _locSalesOrderXPO.CreditLimitStatus = CreditLimitStatus.Released;
                                    _locSalesOrderXPO.CreditLimitStatusDate = now;
                                    _locSalesOrderXPO.Save();
                                    _locSalesOrderXPO.Session.CommitTransaction();
                                    SuccessMessageShow(_locSalesOrderXPO.Code + " has been change successfully to Release Credit Limit ");
                                }
                                else
                                {
                                    ErrorMessageShow("Data SalesOrder Not Available");
                                }
                            }
                            else
                            {
                                ErrorMessageShow("Data SalesOrder Not Available");
                            }
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = SalesOrder " + ex.ToString());
            }
        }

        //Masih Home Work Buat yg Free Item
        private void SalesOrderCancelOrderAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;
                var User = SecuritySystem.CurrentUserName;

                if (_objectsToProcess != null)
                {
                    foreach (Object obj in _objectsToProcess)
                    {
                        SalesOrder _locSalesOrderOS = (SalesOrder)_objectSpace.GetObject(obj);

                        if (_locSalesOrderOS != null)
                        {
                            if (_locSalesOrderOS.Session != null)
                            {
                                _currSession = _locSalesOrderOS.Session;
                            }

                            if (_locSalesOrderOS.Code != null && _currSession != null)
                            {
                                _currObjectId = _locSalesOrderOS.Code;

                                SalesOrder _locSalesOrderXPO = _currSession.FindObject<SalesOrder>
                                                                   (new GroupOperator(GroupOperatorType.And,
                                                                    new BinaryOperator("Code", _currObjectId)));

                                if (_locSalesOrderXPO != null)
                                {
                                    if(_locSalesOrderXPO.Status == Status.Open)
                                    {
                                        XPCollection<SalesOrderLine> _locSalesOrderLines = new XPCollection<SalesOrderLine>
                                                                                               (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                new BinaryOperator("SalesOrder", _locSalesOrderXPO),
                                                                                                new BinaryOperator("Status", Status.Open)));
                                        if(_locSalesOrderLines != null && _locSalesOrderLines.Count() > 0)
                                        {
                                            foreach(SalesOrderLine _locSalesOrderLine in _locSalesOrderLines)
                                            {
                                                _locSalesOrderLine.Status = Status.Cancel;
                                                _locSalesOrderLine.StatusDate = now;
                                                _locSalesOrderLine.Save();
                                                _locSalesOrderLine.Session.CommitTransaction();
                                            }
                                        }
                                    }
                                    else if (_locSalesOrderXPO.Status == Status.Progress || _locSalesOrderXPO.Status == Status.Posted || _locSalesOrderXPO.Status == Status.Close)
                                    {
                                        double _locQtyOnHand = 0;
                                        double _locBalHold = 0;
                                        double _locQtyFreeItemOnHand = 0;
                                        #region SalesOrderLine
                                        XPCollection<SalesOrderLine> _locSalesOrderLines = new XPCollection<SalesOrderLine>
                                                                                               (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                new BinaryOperator("SalesOrder", _locSalesOrderXPO),
                                                                                                new GroupOperator(GroupOperatorType.Or,
                                                                                                new BinaryOperator("Status", Status.Progress),
                                                                                                new BinaryOperator("Status", Status.Posted),
                                                                                                new BinaryOperator("Status", Status.Close))));
                                        if (_locSalesOrderLines != null && _locSalesOrderLines.Count() > 0)
                                        {
                                            foreach (SalesOrderLine _locSalesOrderLine in _locSalesOrderLines)
                                            {
                                                if(_locSalesOrderLine.BegInvLine != null)
                                                {
                                                    _locQtyOnHand = _locSalesOrderLine.BegInvLine.QtyOnHand - _locSalesOrderLine.TQty;
                                                    if(_locQtyOnHand < 0)
                                                    {
                                                        _locQtyOnHand = 0;
                                                    }
                                                    _locSalesOrderLine.BegInvLine.QtyOnHand = _locQtyOnHand;
                                                    _locSalesOrderLine.BegInvLine.Save();
                                                    _locSalesOrderLine.BegInvLine.Session.CommitTransaction();

                                                }
                                                if(_locSalesOrderLine.AccountingPeriodicLine != null)
                                                {
                                                    _locBalHold = _locSalesOrderLine.AccountingPeriodicLine.BalHold - _locSalesOrderLine.DiscAmount;
                                                    if(_locBalHold < 0)
                                                    {
                                                        _locBalHold = 0;
                                                    }
                                                    _locSalesOrderLine.AccountingPeriodicLine.BalHold = _locBalHold;
                                                    _locSalesOrderLine.AccountingPeriodicLine.Save();
                                                    _locSalesOrderLine.AccountingPeriodicLine.Session.CommitTransaction();
                                                }

                                                _locSalesOrderLine.Status = Status.Cancel;
                                                _locSalesOrderLine.StatusDate = now;
                                                _locSalesOrderLine.Save();
                                                _locSalesOrderLine.Session.CommitTransaction();
                                            }
                                        }
                                        #endregion SalesOrderLine
                                        #region SalesOrderMonitoring
                                        XPCollection<SalesOrderMonitoring> _locSalesOrderMonitorings = new XPCollection<SalesOrderMonitoring>
                                                                                               (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                new BinaryOperator("SalesOrder", _locSalesOrderXPO)
                                                                                                ));
                                        if (_locSalesOrderMonitorings != null && _locSalesOrderMonitorings.Count() > 0)
                                        {
                                            foreach (SalesOrderMonitoring _locSalesOrderMonitoring in _locSalesOrderMonitorings)
                                            {
                                                _locSalesOrderMonitoring.Status = Status.Cancel;
                                                _locSalesOrderMonitoring.StatusDate = now;
                                                _locSalesOrderMonitoring.Save();
                                                _locSalesOrderMonitoring.Session.CommitTransaction();
                                            }
                                        }
                                        #endregion SalesOrderMonitoring
                                        #region SalesOrderFreeItem
                                        XPCollection<SalesOrderFreeItem> _locSalesOrderFreeItems = new XPCollection<SalesOrderFreeItem>
                                                                                               (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                new BinaryOperator("SalesOrder", _locSalesOrderXPO)
                                                                                                ));
                                        if (_locSalesOrderFreeItems != null && _locSalesOrderFreeItems.Count() > 0)
                                        {
                                            foreach (SalesOrderFreeItem _locSalesOrderFreeItem in _locSalesOrderFreeItems)
                                            {
                                                if(_locSalesOrderFreeItem.BegInvLine != null)
                                                {
                                                    _locQtyFreeItemOnHand = _locSalesOrderFreeItem.BegInvLine.QtyOnHand - _locSalesOrderFreeItem.FpTQty;
                                                    if (_locQtyFreeItemOnHand < 0)
                                                    {
                                                        _locQtyFreeItemOnHand = 0;
                                                    }
                                                    _locSalesOrderFreeItem.BegInvLine.QtyOnHand = _locQtyFreeItemOnHand;
                                                    _locSalesOrderFreeItem.BegInvLine.Save();
                                                    _locSalesOrderFreeItem.BegInvLine.Session.CommitTransaction();
                                                }
                                                _locSalesOrderFreeItem.Status = Status.Cancel;
                                                _locSalesOrderFreeItem.StatusDate = now;
                                                _locSalesOrderFreeItem.Save();
                                                _locSalesOrderFreeItem.Session.CommitTransaction();
                                            }
                                        }
                                        #endregion SalesOrderFreeItem
                                    }

                                    _locSalesOrderXPO.Status = Status.Cancel;
                                    _locSalesOrderXPO.StatusDate = now;
                                    _locSalesOrderXPO.Save();
                                    _locSalesOrderXPO.Session.CommitTransaction();
                                    SuccessMessageShow(_locSalesOrderXPO.Code + " has been change successfully to Cancel ");
                                }
                                else
                                {
                                    ErrorMessageShow("Data SalesOrder Not Available");
                                }
                            }
                            else
                            {
                                ErrorMessageShow("Data SalesOrder Not Available");
                            }
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = SalesOrder " + ex.ToString());
            }
        }

        //=========================================== Code In Here ===========================================

        #region Progress

        private bool GetAndSetForCheckQtyOnHandAlsoChangeStatusToProgress(Session _currSession, SalesOrder _locSalesOrderXPO)
        {
            bool _return = false;
            try
            {
                DateTime now = DateTime.Now;
                bool _locRedColor = false;
                if (_locSalesOrderXPO != null)
                {
                    XPCollection<SalesOrderLine> _locSalesOrderLines = new XPCollection<SalesOrderLine>
                                                                                               (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                new BinaryOperator("SalesOrder", _locSalesOrderXPO),
                                                                                                new BinaryOperator("Select", true),
                                                                                                new BinaryOperator("Status", Status.Open)));

                    if (_locSalesOrderLines != null && _locSalesOrderLines.Count > 0)
                    {
                        foreach(SalesOrderLine _locSalesOrderLine in _locSalesOrderLines)
                        {
                            if(_locSalesOrderLine.BegInvLine != null)
                            {
                                if((_locSalesOrderLine.BegInvLine.QtyAvailable - _locSalesOrderLine.BegInvLine.QtyOnHand) >= _locSalesOrderLine.TQty)
                                {
                                    _locSalesOrderLine.Status = Status.Progress;
                                    _locSalesOrderLine.StatusDate = now;
                                    _locSalesOrderLine.RecColor = false;
                                    _locSalesOrderLine.Message = null;
                                    _locSalesOrderLine.Save();
                                    _locSalesOrderLine.Session.CommitTransaction();

                                    _locSalesOrderLine.BegInvLine.QtyOnHand = _locSalesOrderLine.BegInvLine.QtyOnHand + _locSalesOrderLine.TQty;
                                    _locSalesOrderLine.BegInvLine.Save();
                                    _locSalesOrderLine.BegInvLine.Session.CommitTransaction();
                                }
                                else
                                {
                                    _locRedColor = true;
                                    _locSalesOrderLine.RecColor = true;
                                    _locSalesOrderLine.Message = "Please delete the records and create new one also update stock in warehouse";
                                    _locSalesOrderLine.Save();
                                    _locSalesOrderLine.Session.CommitTransaction();
                                }
                            }
                        }
                    }
                    _return = _locRedColor;
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("BusinessObject = SalesOrder" + ex.ToString());
            }
            return _return;
        }

        private void SetOverCreditStatus(Session _currSession, SalesOrder _locSalesOrderXPO)
        {
            
            try
            {
                DateTime now = DateTime.Now;
                if (_locSalesOrderXPO != null)
                {
                    if (_locSalesOrderXPO.CreditLimitList != null)
                    {
                        if (_locSalesOrderXPO.CreditLimitStatus == CreditLimitStatus.None)
                        {
                            if ((_locSalesOrderXPO.CreditLimitList.CurrCredit + _locSalesOrderXPO.Amount) > _locSalesOrderXPO.CreditLimitList.MaxCredit)
                            {
                                _locSalesOrderXPO.OverCredit = true;
                                _locSalesOrderXPO.Message = "Over Credit";
                                _locSalesOrderXPO.CreditLimitStatus = CreditLimitStatus.Hold;
                                _locSalesOrderXPO.CreditLimitStatusDate = now;
                                _locSalesOrderXPO.Save();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("BusinessObject = SalesOrder" + ex.ToString());
            }
            
        }

        #region GetAmount

        private void SetTotalAmountAndTaxAmountAndDiscountAmount(Session _currSession, SalesOrder _locSalesOrderXPO)
        {
            try
            {
                double _locTAmount = 0;
                double _locTUAmount = 0;
                double _locDiscAmountRule2 = 0;
                double _locTxAmount = 0;
                double _locDiscAmountRule1 = 0;
                if (_locSalesOrderXPO != null)
                {
                    XPCollection<SalesOrderLine> _locSalesOrderLines = new XPCollection<SalesOrderLine>
                                                                                (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                new BinaryOperator("SalesOrder", _locSalesOrderXPO),
                                                                                new BinaryOperator("Select", true),
                                                                                new GroupOperator(GroupOperatorType.Or,
                                                                                new BinaryOperator("Status", Status.Progress))));
                    if (_locSalesOrderLines != null && _locSalesOrderLines.Count() > 0)
                    {
                        foreach (SalesOrderLine _locSalesOrderLine in _locSalesOrderLines)
                        {
                            _locTAmount = _locTAmount + _locSalesOrderLine.TAmount;
                            _locTUAmount = _locTUAmount + _locSalesOrderLine.TUAmount;
                            _locDiscAmountRule2 = _locDiscAmountRule2 + _locSalesOrderLine.DiscAmount;
                            if (_locSalesOrderLine.Tax != null)
                            {
                                if (_locSalesOrderLine.Tax.TaxNature == TaxNature.Increase)
                                {
                                    _locTxAmount = _locTxAmount + _locSalesOrderLine.TxAmount;
                                }
                                if (_locSalesOrderLine.Tax.TaxNature == TaxNature.Decrease)
                                {
                                    _locTxAmount = _locTxAmount - _locSalesOrderLine.TxAmount;
                                }
                            }
                        }

                        _locDiscAmountRule1 = GetDiscountRule1(_currSession, _locSalesOrderXPO);
                    }

                    _locSalesOrderXPO.TotUnitAmount = _locTUAmount;
                    _locSalesOrderXPO.TotAmount = _locTAmount;
                    _locSalesOrderXPO.AmountDisc = _locDiscAmountRule1;
                    _locSalesOrderXPO.TotDiscAmount = (_locDiscAmountRule2 + _locDiscAmountRule1);
                    _locSalesOrderXPO.TotTaxAmount = _locTxAmount;
                    _locSalesOrderXPO.Amount = _locTAmount - _locDiscAmountRule1;
                    _locSalesOrderXPO.Save();
                    _locSalesOrderXPO.Session.CommitTransaction();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = SalesOrder ", ex.ToString());
            }

        }

        #endregion GetAmount

        #endregion Progress

        #region FreeItem

        private void SetFreeItem(Session _currSession, SalesOrder _locSalesOrderXPO)
        {
            try
            {
                //Harus di perhatikan QtyOnHand
                if (_locSalesOrderXPO != null)
                {
                    XPCollection<FreeItemRule> _locFreeItemRules = new XPCollection<FreeItemRule>
                                                            (_currSession, new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("Company", _locSalesOrderXPO.Company),
                                                            new BinaryOperator("Workplace", _locSalesOrderXPO.Workplace),
                                                            new BinaryOperator("Location", _locSalesOrderXPO.Location),
                                                            new BinaryOperator("PriceGroup", _locSalesOrderXPO.PriceGroup),
                                                            new BinaryOperator("Active", true)));

                    if(_locFreeItemRules != null && _locFreeItemRules.Count() > 0)
                    {
                        foreach (FreeItemRule _locFreeItemRule in _locFreeItemRules)
                        {
                            #region Header
                            if (_locFreeItemRule.FreeItemType1 == FreeItemType1.Header)
                            {
                                if (_locFreeItemRule.OpenAmount == true)
                                {  
                                    #region Rule1
                                    if (_locFreeItemRule.RuleType == RuleType.Rule1)
                                    {
                                        double _locQtyFI = 0;
                                        double _locTotQtyFI = 0;
                                        double _locFpDqty = 0;
                                        double _locFpQty = 0;
                                        int _locMaxLoop = 0;
                                        int _locTotMaxLoopVal = 0;

                                        #region Amount
                                        if (_locFreeItemRule.NetAmountRule == true)
                                        {
                                            if (_locSalesOrderXPO.TotAmount >= _locFreeItemRule.NetTotalUAmount)
                                            {
                                                _locQtyFI = System.Math.Floor(_locSalesOrderXPO.TotAmount / _locFreeItemRule.NetTotalUAmount);
                                            }
                                        }
                                        if (_locFreeItemRule.GrossAmountRule == true)
                                        {
                                            if (_locSalesOrderXPO.TotUnitAmount >= _locFreeItemRule.GrossTotalUAmount)
                                            {
                                                _locQtyFI = System.Math.Floor(_locSalesOrderXPO.TotUnitAmount / _locFreeItemRule.GrossTotalUAmount);
                                            }
                                        }
                                        #endregion Amount

                                        if (_locFreeItemRule.MaxLoop == 0)
                                        {
                                            _locMaxLoop = (int)_locQtyFI;
                                        }
                                        else
                                        {
                                            if (_locFreeItemRule.MaxLoop > (int)_locQtyFI)
                                            {
                                                _locMaxLoop = (int)_locQtyFI;
                                            }
                                            else
                                            {
                                                _locMaxLoop = _locFreeItemRule.MaxLoop;
                                            }
                                        }

                                        for (int i = 1; i <= _locMaxLoop; i++)
                                        {
                                            _locTotMaxLoopVal = _locTotMaxLoopVal + 1;
                                        }

                                        _locTotQtyFI = _locTotMaxLoopVal * _locFreeItemRule.FpTQty;

                                        if (_locFreeItemRule.FpBegInvLine != null)
                                        {
                                            if ((_locFreeItemRule.FpBegInvLine.QtyAvailable - _locFreeItemRule.FpBegInvLine.QtyOnHand) > 0)
                                            {
                                                if((_locFreeItemRule.FpBegInvLine.QtyAvailable - _locFreeItemRule.FpBegInvLine.QtyOnHand) < _locTotQtyFI)
                                                {
                                                    _locTotQtyFI = _locFreeItemRule.FpBegInvLine.QtyAvailable - _locFreeItemRule.FpBegInvLine.QtyOnHand;
                                                }

                                                if (_locFreeItemRule.FpItem != null && _locFreeItemRule.FpDUOM != null && _locFreeItemRule.FpUOM != null)
                                                {
                                                    ItemUnitOfMeasure _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                                            (new GroupOperator(GroupOperatorType.And,
                                                                             new BinaryOperator("Item", _locFreeItemRule.FpItem),
                                                                             new BinaryOperator("UOM", _locFreeItemRule.FpUOM),
                                                                             new BinaryOperator("DefaultUOM", _locFreeItemRule.FpDUOM),
                                                                             new BinaryOperator("Active", true)));
                                                    if (_locItemUOM != null)
                                                    {
                                                        if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                                        {
                                                            _locFpDqty = _locTotQtyFI % _locItemUOM.DefaultConversion;
                                                            _locFpQty = System.Math.Floor(_locTotQtyFI / _locItemUOM.DefaultConversion);
                                                        }
                                                        else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                                        {
                                                            _locFpDqty = System.Math.Floor(_locTotQtyFI / _locItemUOM.DefaultConversion);
                                                            _locFpQty = (_locTotQtyFI % _locItemUOM.DefaultConversion) * _locItemUOM.Conversion;
                                                        }
                                                        else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                                        {
                                                            _locFpDqty = _locTotQtyFI;
                                                        }
                                                    }
                                                }
                                                else
                                                {
                                                    _locFpDqty = _locTotQtyFI;
                                                }

                                                SalesOrderFreeItem _saveDataSalesOrderFreeItem = new SalesOrderFreeItem(_currSession)
                                                {
                                                    //Organization
                                                    Company = _locSalesOrderXPO.Company,
                                                    Workplace = _locSalesOrderXPO.Workplace,
                                                    Division = _locSalesOrderXPO.Division,
                                                    Department = _locSalesOrderXPO.Department,
                                                    Section = _locSalesOrderXPO.Section,
                                                    Employee = _locSalesOrderXPO.Employee,
                                                    //Item
                                                    SalesOrder = _locSalesOrderXPO,
                                                    Location = _locSalesOrderXPO.Location,
                                                    BegInv = _locFreeItemRule.FpBegInv,
                                                    FpLocationType = _locFreeItemRule.LocationType,
                                                    FpStockType = _locFreeItemRule.StockType,
                                                    FpStockGroup = _locFreeItemRule.FpStockGroup,
                                                    FreeItemRule = _locFreeItemRule,
                                                    FpItem = _locFreeItemRule.FpItem,
                                                    FpDQty = _locFpDqty,
                                                    FpDUOM = _locFreeItemRule.FpDUOM,
                                                    FpQty = _locFpQty,
                                                    FpUOM = _locFreeItemRule.FpUOM,
                                                    FpTQty = _locTotQtyFI,
                                                    BegInvLine = _locFreeItemRule.FpBegInvLine,
                                                    //Amount
                                                    Currency = _locSalesOrderXPO.Currency,
                                                    PriceGroup = _locSalesOrderXPO.PriceGroup,
                                                    PriceLine = GetPriceLine(_currSession, _locSalesOrderXPO, _locFreeItemRule.FpItem),
                                                    UAmount = GetPriceLine(_currSession, _locSalesOrderXPO, _locFreeItemRule.FpItem).Amount2a,
                                                };
                                                _saveDataSalesOrderFreeItem.Save();
                                                _saveDataSalesOrderFreeItem.Session.CommitTransaction();

                                                if (_locFreeItemRule.FpBegInv != null && _locFreeItemRule.FpBegInvLine != null)
                                                {
                                                    _locFreeItemRule.FpBegInvLine.QtyOnHand = _locFreeItemRule.FpBegInvLine.QtyOnHand + _locTotQtyFI;
                                                    _locFreeItemRule.FpBegInvLine.Save();
                                                    _locFreeItemRule.FpBegInvLine.Session.CommitTransaction();
                                                }
                                            }
                                        }
                                    }

                                    #endregion Rule1

                                    //1, 2+1, 3+1, 4+1
                                    #region Rule2
                                    if (_locFreeItemRule.RuleType == RuleType.Rule2)
                                    {
                                        double _locQtyFI = 0;
                                        double _locTotQtyFI = 0;
                                        double _locFpDqty = 0;
                                        double _locFpQty = 0;
                                        int _locMaxLoop = 0;

                                        #region Amount
                                        if (_locFreeItemRule.NetAmountRule == true)
                                        {
                                            if (_locSalesOrderXPO.TotAmount >= _locFreeItemRule.NetTotalUAmount)
                                            {
                                                _locQtyFI = System.Math.Floor(_locSalesOrderXPO.TotAmount / _locFreeItemRule.NetTotalUAmount);
                                            }
                                        }
                                        if (_locFreeItemRule.GrossAmountRule == true)
                                        {
                                            if (_locSalesOrderXPO.TotUnitAmount >= _locFreeItemRule.GrossTotalUAmount)
                                            {
                                                _locQtyFI = System.Math.Floor(_locSalesOrderXPO.TotUnitAmount / _locFreeItemRule.GrossTotalUAmount);
                                            }
                                        }
                                        #endregion Amount

                                        if (_locFreeItemRule.MaxLoop == 0)
                                        {
                                            _locMaxLoop = (int)_locQtyFI;
                                        }
                                        else
                                        {
                                            if (_locFreeItemRule.MaxLoop > (int)_locQtyFI)
                                            {
                                                _locMaxLoop = (int)_locQtyFI;
                                            }
                                            else
                                            {
                                                _locMaxLoop = _locFreeItemRule.MaxLoop;
                                            }
                                        }

                                        for (int i = 1; i <= _locMaxLoop; i++)
                                        {
                                            if (i == 1)
                                            {
                                                _locTotQtyFI = _locFreeItemRule.FpTQty;
                                            }
                                            else
                                            {
                                                _locTotQtyFI = _locTotQtyFI + (i + _locFreeItemRule.FpTQty);
                                            }
                                        }

                                        if(_locFreeItemRule.FpBegInvLine != null)
                                        {
                                            if((_locFreeItemRule.FpBegInvLine.QtyAvailable - _locFreeItemRule.FpBegInvLine.QtyOnHand) > 0)
                                            {
                                                if((_locFreeItemRule.FpBegInvLine.QtyAvailable - _locFreeItemRule.FpBegInvLine.QtyOnHand) < _locTotQtyFI)
                                                {
                                                    _locTotQtyFI = _locFreeItemRule.FpBegInvLine.QtyAvailable - _locFreeItemRule.FpBegInvLine.QtyOnHand;
                                                }

                                                if (_locFreeItemRule.FpItem != null && _locFreeItemRule.FpDUOM != null && _locFreeItemRule.FpUOM != null)
                                                {
                                                    ItemUnitOfMeasure _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                                            (new GroupOperator(GroupOperatorType.And,
                                                                             new BinaryOperator("Item", _locFreeItemRule.FpItem),
                                                                             new BinaryOperator("UOM", _locFreeItemRule.FpUOM),
                                                                             new BinaryOperator("DefaultUOM", _locFreeItemRule.FpDUOM),
                                                                             new BinaryOperator("Active", true)));
                                                    if (_locItemUOM != null)
                                                    {
                                                        if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                                        {
                                                            _locFpDqty = _locTotQtyFI % _locItemUOM.DefaultConversion;
                                                            _locFpQty = System.Math.Floor(_locTotQtyFI / _locItemUOM.DefaultConversion);
                                                        }
                                                        else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                                        {
                                                            _locFpDqty = System.Math.Floor(_locTotQtyFI / _locItemUOM.DefaultConversion);
                                                            _locFpQty = (_locTotQtyFI % _locItemUOM.DefaultConversion) * _locItemUOM.Conversion;
                                                        }
                                                        else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                                        {
                                                            _locFpDqty = _locTotQtyFI;
                                                        }
                                                    }
                                                }
                                                else
                                                {
                                                    _locFpDqty = _locTotQtyFI;
                                                }

                                                SalesOrderFreeItem _saveDataSalesOrderFreeItem = new SalesOrderFreeItem(_currSession)
                                                {
                                                    //Organization
                                                    Company = _locSalesOrderXPO.Company,
                                                    Workplace = _locSalesOrderXPO.Workplace,
                                                    Division = _locSalesOrderXPO.Division,
                                                    Department = _locSalesOrderXPO.Department,
                                                    Section = _locSalesOrderXPO.Section,
                                                    Employee = _locSalesOrderXPO.Employee,
                                                    //Item
                                                    SalesOrder = _locSalesOrderXPO,
                                                    Location = _locSalesOrderXPO.Location,
                                                    BegInv = _locFreeItemRule.FpBegInv,
                                                    FpLocationType = _locFreeItemRule.LocationType,
                                                    FpStockType = _locFreeItemRule.StockType,
                                                    FpStockGroup = _locFreeItemRule.FpStockGroup,
                                                    FreeItemRule = _locFreeItemRule,
                                                    FpItem = _locFreeItemRule.FpItem,
                                                    FpDQty = _locFpDqty,
                                                    FpDUOM = _locFreeItemRule.FpDUOM,
                                                    FpQty = _locFpQty,
                                                    FpUOM = _locFreeItemRule.FpUOM,
                                                    FpTQty = _locTotQtyFI,
                                                    BegInvLine = _locFreeItemRule.FpBegInvLine,
                                                    //Amount
                                                    Currency = _locSalesOrderXPO.Currency,
                                                    PriceGroup = _locSalesOrderXPO.PriceGroup,
                                                    PriceLine = GetPriceLine(_currSession, _locSalesOrderXPO, _locFreeItemRule.FpItem),
                                                    UAmount = GetPriceLine(_currSession, _locSalesOrderXPO, _locFreeItemRule.FpItem).Amount2a,
                                                };
                                                _saveDataSalesOrderFreeItem.Save();
                                                _saveDataSalesOrderFreeItem.Session.CommitTransaction();

                                                if (_locFreeItemRule.FpBegInv != null && _locFreeItemRule.FpBegInvLine != null)
                                                {
                                                    _locFreeItemRule.FpBegInvLine.QtyOnHand = _locFreeItemRule.FpBegInvLine.QtyOnHand + _locTotQtyFI;
                                                    _locFreeItemRule.FpBegInvLine.Save();
                                                    _locFreeItemRule.FpBegInvLine.Session.CommitTransaction();
                                                }
                                            }
                                        }
                                    }
                                    #endregion Rule2

                                    //2 * 2 * 2 * 2
                                    #region Rule3
                                    if (_locFreeItemRule.RuleType == RuleType.Rule3)
                                    {
                                        double _locQtyFI = 0;
                                        double _locTotQtyFI = 0;
                                        double _locFpDqty = 0;
                                        double _locFpQty = 0;
                                        int _locMaxLoop = 0;

                                        #region Amount
                                        if (_locFreeItemRule.NetAmountRule == true)
                                        {
                                            if (_locSalesOrderXPO.TotAmount >= _locFreeItemRule.NetTotalUAmount)
                                            {
                                                _locQtyFI = System.Math.Floor(_locSalesOrderXPO.TotAmount / _locFreeItemRule.NetTotalUAmount);
                                            }
                                        }
                                        if (_locFreeItemRule.GrossAmountRule == true)
                                        {
                                            if (_locSalesOrderXPO.TotUnitAmount >= _locFreeItemRule.GrossTotalUAmount)
                                            {
                                                _locQtyFI = System.Math.Floor(_locSalesOrderXPO.TotUnitAmount / _locFreeItemRule.GrossTotalUAmount);
                                            }
                                        }
                                        #endregion Amount


                                        if (_locFreeItemRule.MaxLoop == 0)
                                        {
                                            _locMaxLoop = (int)_locQtyFI;
                                        }
                                        else
                                        {
                                            if (_locFreeItemRule.MaxLoop > (int)_locQtyFI)
                                            {
                                                _locMaxLoop = (int)_locQtyFI;
                                            }
                                            else
                                            {
                                                _locMaxLoop = _locFreeItemRule.MaxLoop;
                                            }
                                        }

                                        for (int i = 1; i <= _locMaxLoop; i++)
                                        {
                                            _locTotQtyFI = _locTotQtyFI * _locFreeItemRule.FpTQty;
                                        }

                                        if(_locFreeItemRule.FpBegInvLine != null)
                                        {
                                            if((_locFreeItemRule.FpBegInvLine.QtyAvailable - _locFreeItemRule.FpBegInvLine.QtyOnHand) > 0)
                                            {
                                                if ((_locFreeItemRule.FpBegInvLine.QtyAvailable - _locFreeItemRule.FpBegInvLine.QtyOnHand) < _locTotQtyFI)
                                                {
                                                    _locTotQtyFI = _locFreeItemRule.FpBegInvLine.QtyAvailable - _locFreeItemRule.FpBegInvLine.QtyOnHand;
                                                }

                                                if (_locFreeItemRule.FpItem != null && _locFreeItemRule.FpDUOM != null && _locFreeItemRule.FpUOM != null)
                                                {
                                                    ItemUnitOfMeasure _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                                            (new GroupOperator(GroupOperatorType.And,
                                                                             new BinaryOperator("Item", _locFreeItemRule.FpItem),
                                                                             new BinaryOperator("UOM", _locFreeItemRule.FpUOM),
                                                                             new BinaryOperator("DefaultUOM", _locFreeItemRule.FpDUOM),
                                                                             new BinaryOperator("Active", true)));
                                                    if (_locItemUOM != null)
                                                    {
                                                        if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                                        {
                                                            _locFpDqty = _locTotQtyFI % _locItemUOM.DefaultConversion;
                                                            _locFpQty = System.Math.Floor(_locTotQtyFI / _locItemUOM.DefaultConversion);
                                                        }
                                                        else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                                        {
                                                            _locFpDqty = System.Math.Floor(_locTotQtyFI / _locItemUOM.DefaultConversion);
                                                            _locFpQty = (_locTotQtyFI % _locItemUOM.DefaultConversion) * _locItemUOM.Conversion;
                                                        }
                                                        else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                                        {
                                                            _locFpDqty = _locTotQtyFI;
                                                        }
                                                    }
                                                }
                                                else
                                                {
                                                    _locFpDqty = _locTotQtyFI;
                                                }

                                                SalesOrderFreeItem _saveDataSalesOrderFreeItem = new SalesOrderFreeItem(_currSession)
                                                {
                                                    //Organization
                                                    Company = _locSalesOrderXPO.Company,
                                                    Workplace = _locSalesOrderXPO.Workplace,
                                                    Division = _locSalesOrderXPO.Division,
                                                    Department = _locSalesOrderXPO.Department,
                                                    Section = _locSalesOrderXPO.Section,
                                                    Employee = _locSalesOrderXPO.Employee,
                                                    //Item
                                                    SalesOrder = _locSalesOrderXPO,
                                                    Location = _locSalesOrderXPO.Location,
                                                    BegInv = _locFreeItemRule.FpBegInv,
                                                    FpLocationType = _locFreeItemRule.LocationType,
                                                    FpStockType = _locFreeItemRule.StockType,
                                                    FpStockGroup = _locFreeItemRule.FpStockGroup,
                                                    FreeItemRule = _locFreeItemRule,
                                                    FpItem = _locFreeItemRule.FpItem,
                                                    FpDQty = _locFpDqty,
                                                    FpDUOM = _locFreeItemRule.FpDUOM,
                                                    FpQty = _locFpQty,
                                                    FpUOM = _locFreeItemRule.FpUOM,
                                                    FpTQty = _locTotQtyFI,
                                                    BegInvLine = _locFreeItemRule.FpBegInvLine,
                                                    //Amount
                                                    Currency = _locSalesOrderXPO.Currency,
                                                    PriceGroup = _locSalesOrderXPO.PriceGroup,
                                                    PriceLine = GetPriceLine(_currSession, _locSalesOrderXPO, _locFreeItemRule.FpItem),
                                                    UAmount = GetPriceLine(_currSession, _locSalesOrderXPO, _locFreeItemRule.FpItem).Amount2a,
                                                };
                                                _saveDataSalesOrderFreeItem.Save();
                                                _saveDataSalesOrderFreeItem.Session.CommitTransaction();

                                                if (_locFreeItemRule.FpBegInv != null && _locFreeItemRule.FpBegInvLine != null)
                                                {
                                                    _locFreeItemRule.FpBegInvLine.QtyOnHand = _locFreeItemRule.FpBegInvLine.QtyOnHand + _locTotQtyFI;
                                                    _locFreeItemRule.FpBegInvLine.Save();
                                                    _locFreeItemRule.FpBegInvLine.Session.CommitTransaction();
                                                }
                                            }
                                        } 
                                    }
                                    #endregion Rule3

                                    //2^(1-1) + 2^(2-1) + 2^(3-1) + 2^(4-1)
                                    #region Rule4
                                    if (_locFreeItemRule.RuleType == RuleType.Rule4)
                                    {
                                        double _locQtyFI = 0;
                                        double _locTotQtyFI = 0;
                                        double _locFpDqty = 0;
                                        double _locFpQty = 0;
                                        int _locMaxLoop = 0;

                                        #region Amount
                                        if (_locFreeItemRule.NetAmountRule == true)
                                        {
                                            if (_locSalesOrderXPO.TotAmount >= _locFreeItemRule.NetTotalUAmount)
                                            {
                                                _locQtyFI = System.Math.Floor(_locSalesOrderXPO.TotAmount / _locFreeItemRule.NetTotalUAmount);
                                            }
                                        }
                                        if (_locFreeItemRule.GrossAmountRule == true)
                                        {
                                            if (_locSalesOrderXPO.TotUnitAmount >= _locFreeItemRule.GrossTotalUAmount)
                                            {
                                                _locQtyFI = System.Math.Floor(_locSalesOrderXPO.TotUnitAmount / _locFreeItemRule.GrossTotalUAmount);
                                            }
                                        }
                                        #endregion Amount      

                                        if (_locFreeItemRule.MaxLoop == 0)
                                        {
                                            _locMaxLoop = (int)_locQtyFI;
                                        }
                                        else
                                        {
                                            if (_locFreeItemRule.MaxLoop > (int)_locQtyFI)
                                            {
                                                _locMaxLoop = (int)_locQtyFI;
                                            }
                                            else
                                            {
                                                _locMaxLoop = _locFreeItemRule.MaxLoop;
                                            }
                                        }

                                        for (int i = 1; i <= _locMaxLoop; i++)
                                        {

                                            _locTotQtyFI = _locTotQtyFI + System.Math.Pow(_locFreeItemRule.FpTQty, i - 1);
                                        }

                                        if(_locFreeItemRule.FpBegInvLine != null)
                                        {
                                            if ((_locFreeItemRule.FpBegInvLine.QtyAvailable - _locFreeItemRule.FpBegInvLine.QtyOnHand) > 0)
                                            {
                                                if ((_locFreeItemRule.FpBegInvLine.QtyAvailable - _locFreeItemRule.FpBegInvLine.QtyOnHand) < _locTotQtyFI)
                                                {
                                                    _locTotQtyFI = _locFreeItemRule.FpBegInvLine.QtyAvailable - _locFreeItemRule.FpBegInvLine.QtyOnHand;
                                                }

                                                if (_locFreeItemRule.FpItem != null && _locFreeItemRule.FpDUOM != null && _locFreeItemRule.FpUOM != null)
                                                {
                                                    ItemUnitOfMeasure _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                                            (new GroupOperator(GroupOperatorType.And,
                                                                             new BinaryOperator("Item", _locFreeItemRule.FpItem),
                                                                             new BinaryOperator("UOM", _locFreeItemRule.FpUOM),
                                                                             new BinaryOperator("DefaultUOM", _locFreeItemRule.FpDUOM),
                                                                             new BinaryOperator("Active", true)));
                                                    if (_locItemUOM != null)
                                                    {
                                                        if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                                        {
                                                            _locFpDqty = _locTotQtyFI % _locItemUOM.DefaultConversion;
                                                            _locFpQty = System.Math.Floor(_locTotQtyFI / _locItemUOM.DefaultConversion);
                                                        }
                                                        else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                                        {
                                                            _locFpDqty = System.Math.Floor(_locTotQtyFI / _locItemUOM.DefaultConversion);
                                                            _locFpQty = (_locTotQtyFI % _locItemUOM.DefaultConversion) * _locItemUOM.Conversion;
                                                        }
                                                        else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                                        {
                                                            _locFpDqty = _locTotQtyFI;
                                                        }
                                                    }
                                                }
                                                else
                                                {
                                                    _locFpDqty = _locTotQtyFI;
                                                }

                                                SalesOrderFreeItem _saveDataSalesOrderFreeItem = new SalesOrderFreeItem(_currSession)
                                                {
                                                    //Organization
                                                    Company = _locSalesOrderXPO.Company,
                                                    Workplace = _locSalesOrderXPO.Workplace,
                                                    Division = _locSalesOrderXPO.Division,
                                                    Department = _locSalesOrderXPO.Department,
                                                    Section = _locSalesOrderXPO.Section,
                                                    Employee = _locSalesOrderXPO.Employee,
                                                    //Item
                                                    SalesOrder = _locSalesOrderXPO,
                                                    Location = _locSalesOrderXPO.Location,
                                                    BegInv = _locFreeItemRule.FpBegInv,
                                                    FpLocationType = _locFreeItemRule.LocationType,
                                                    FpStockType = _locFreeItemRule.StockType,
                                                    FpStockGroup = _locFreeItemRule.FpStockGroup,
                                                    FreeItemRule = _locFreeItemRule,
                                                    FpItem = _locFreeItemRule.FpItem,
                                                    FpDQty = _locFpDqty,
                                                    FpDUOM = _locFreeItemRule.FpDUOM,
                                                    FpQty = _locFpQty,
                                                    FpUOM = _locFreeItemRule.FpUOM,
                                                    FpTQty = _locTotQtyFI,
                                                    BegInvLine = _locFreeItemRule.FpBegInvLine,
                                                    //Amount
                                                    Currency = _locSalesOrderXPO.Currency,
                                                    PriceGroup = _locSalesOrderXPO.PriceGroup,
                                                    PriceLine = GetPriceLine(_currSession, _locSalesOrderXPO, _locFreeItemRule.FpItem),
                                                    UAmount = GetPriceLine(_currSession, _locSalesOrderXPO, _locFreeItemRule.FpItem).Amount2a,
                                                };
                                                _saveDataSalesOrderFreeItem.Save();
                                                _saveDataSalesOrderFreeItem.Session.CommitTransaction();

                                                if (_locFreeItemRule.FpBegInv != null && _locFreeItemRule.FpBegInvLine != null)
                                                {
                                                    _locFreeItemRule.FpBegInvLine.QtyOnHand = _locFreeItemRule.FpBegInvLine.QtyOnHand + _locTotQtyFI;
                                                    _locFreeItemRule.FpBegInvLine.Save();
                                                    _locFreeItemRule.FpBegInvLine.Session.CommitTransaction();
                                                }
                                            }
                                        } 
                                    }
                                    #endregion Rule4
                                }
                            }
                            #endregion Header
                            #region Line
                            if (_locFreeItemRule.FreeItemType1 == FreeItemType1.Line)
                            {
                                XPQuery<SalesOrderLine> _salesOrderLinesQuery = new XPQuery<SalesOrderLine>(_currSession);

                                var _salesOrderLines = from icl in _salesOrderLinesQuery
                                                       where (icl.SalesOrder == _locSalesOrderXPO
                                                       && icl.Status == Status.Progress
                                                       && icl.Select == true
                                                       )
                                                       group icl by icl.Item into g
                                                       select new { Item = g.Key };

                                if (_salesOrderLines != null && _salesOrderLines.Count() > 0)
                                {
                                    foreach (var _salesOrderLine in _salesOrderLines)
                                    {
                                        #region MultiItem=false
                                        if (_locFreeItemRule.MultiItem == false)
                                        {
                                            if (_locFreeItemRule.MpItem == _salesOrderLine.Item)
                                            {
                                                //1+1+1+1+1
                                                #region Rule1
                                                if (_locFreeItemRule.RuleType == RuleType.Rule1)
                                                {
                                                    XPCollection<SalesOrderLine> _locSalesOrderLines = new XPCollection<SalesOrderLine>
                                                                                                           (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                           new BinaryOperator("SalesOrder", _locSalesOrderXPO),
                                                                                                           new BinaryOperator("Item", _salesOrderLine.Item),
                                                                                                           new BinaryOperator("Status", Status.Progress),
                                                                                                           new BinaryOperator("FreeItemChecked", false),
                                                                                                           new BinaryOperator("Select", true)));

                                                    if (_locSalesOrderLines != null && _locSalesOrderLines.Count() > 0)
                                                    {
                                                        #region Foreach
                                                        foreach (SalesOrderLine _locSalesOrderLine in _locSalesOrderLines)
                                                        {
                                                            double _locQtyFI = 0;
                                                            double _locTotQtyFI = 0;
                                                            double _locFpDqty = 0;
                                                            double _locFpQty = 0;
                                                            int _locMaxLoop = 0;
                                                            int _locTotMaxLoopVal = 0;

                                                            #region Amount
                                                            if (_locFreeItemRule.OpenAmount == true)
                                                            {
                                                                if (_locFreeItemRule.NetAmountRule == true)
                                                                {
                                                                    if (_locSalesOrderLine.TAmount >= _locFreeItemRule.NetTotalUAmount)
                                                                    {
                                                                        _locQtyFI = System.Math.Floor(_locSalesOrderLine.TAmount / _locFreeItemRule.NetTotalUAmount);
                                                                    }
                                                                }
                                                                if (_locFreeItemRule.GrossAmountRule == true)
                                                                {
                                                                    if (_locSalesOrderLine.TUAmount >= _locFreeItemRule.GrossTotalUAmount)
                                                                    {
                                                                        _locQtyFI = System.Math.Floor(_locSalesOrderLine.TUAmount / _locFreeItemRule.GrossTotalUAmount);
                                                                    }
                                                                }

                                                            }
                                                            #endregion Amount
                                                            #region Item
                                                            else
                                                            {
                                                                if (_locSalesOrderLine.TQty >= _locFreeItemRule.MpTQty)
                                                                {
                                                                    _locQtyFI = System.Math.Floor(_locSalesOrderLine.TQty / _locFreeItemRule.MpTQty);
                                                                }
                                                            }
                                                            #endregion Item

                                                            if (_locFreeItemRule.MaxLoop == 0)
                                                            {
                                                                _locMaxLoop = (int)_locQtyFI;
                                                            }
                                                            else
                                                            {
                                                                if (_locFreeItemRule.MaxLoop > (int)_locQtyFI)
                                                                {
                                                                    _locMaxLoop = (int)_locQtyFI;
                                                                }
                                                                else
                                                                {
                                                                    _locMaxLoop = _locFreeItemRule.MaxLoop;
                                                                }
                                                            }

                                                            for (int i = 1; i <= _locMaxLoop; i++)
                                                            {
                                                                _locTotMaxLoopVal = _locTotMaxLoopVal + 1;
                                                            }

                                                            _locTotQtyFI = _locTotMaxLoopVal * _locFreeItemRule.FpTQty;

                                                            if(_locFreeItemRule.FpBegInvLine != null)
                                                            {
                                                                if((_locFreeItemRule.FpBegInvLine.QtyAvailable - _locFreeItemRule.FpBegInvLine.QtyOnHand) > 0)
                                                                {
                                                                    if ((_locFreeItemRule.FpBegInvLine.QtyAvailable - _locFreeItemRule.FpBegInvLine.QtyOnHand) < _locTotQtyFI)
                                                                    {
                                                                        _locTotQtyFI = _locFreeItemRule.FpBegInvLine.QtyAvailable - _locFreeItemRule.FpBegInvLine.QtyOnHand;
                                                                    }

                                                                    if (_locFreeItemRule.FpItem != null && _locFreeItemRule.FpDUOM != null && _locFreeItemRule.FpUOM != null)
                                                                    {
                                                                        ItemUnitOfMeasure _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                                                                (new GroupOperator(GroupOperatorType.And,
                                                                                                 new BinaryOperator("Item", _locFreeItemRule.FpItem),
                                                                                                 new BinaryOperator("UOM", _locFreeItemRule.FpUOM),
                                                                                                 new BinaryOperator("DefaultUOM", _locFreeItemRule.FpDUOM),
                                                                                                 new BinaryOperator("Active", true)));
                                                                        if (_locItemUOM != null)
                                                                        {
                                                                            if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                                                            {
                                                                                _locFpDqty = _locTotQtyFI % _locItemUOM.DefaultConversion;
                                                                                _locFpQty = System.Math.Floor(_locTotQtyFI / _locItemUOM.DefaultConversion);
                                                                            }
                                                                            else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                                                            {
                                                                                _locFpDqty = System.Math.Floor(_locTotQtyFI / _locItemUOM.DefaultConversion);
                                                                                _locFpQty = (_locTotQtyFI % _locItemUOM.DefaultConversion) * _locItemUOM.Conversion;
                                                                            }
                                                                            else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                                                            {
                                                                                _locFpDqty = _locTotQtyFI;
                                                                            }
                                                                        }
                                                                    }
                                                                    else
                                                                    {
                                                                        _locFpDqty = _locTotQtyFI;
                                                                    }

                                                                    SalesOrderFreeItem _saveDataSalesOrderFreeItem = new SalesOrderFreeItem(_currSession)
                                                                    {
                                                                        //Organization
                                                                        Company = _locSalesOrderXPO.Company,
                                                                        Workplace = _locSalesOrderXPO.Workplace,
                                                                        Division = _locSalesOrderXPO.Division,
                                                                        Department = _locSalesOrderXPO.Department,
                                                                        Section = _locSalesOrderXPO.Section,
                                                                        Employee = _locSalesOrderXPO.Employee,
                                                                        //Item
                                                                        SalesOrder = _locSalesOrderXPO,
                                                                        SalesOrderLine = _locSalesOrderLine,
                                                                        Location = _locSalesOrderXPO.Location,
                                                                        BegInv = _locFreeItemRule.FpBegInv,
                                                                        FpLocationType = _locFreeItemRule.LocationType,
                                                                        FpStockType = _locFreeItemRule.StockType,
                                                                        FpStockGroup = _locFreeItemRule.FpStockGroup,
                                                                        FreeItemRule = _locFreeItemRule,
                                                                        FpItem = _locFreeItemRule.FpItem,
                                                                        FpDQty = _locFpDqty,
                                                                        FpDUOM = _locFreeItemRule.FpDUOM,
                                                                        FpQty = _locFpQty,
                                                                        FpUOM = _locFreeItemRule.FpUOM,
                                                                        FpTQty = _locTotQtyFI,
                                                                        BegInvLine = _locFreeItemRule.FpBegInvLine,
                                                                        //Amount
                                                                        Currency = _locSalesOrderXPO.Currency,
                                                                        PriceGroup = _locSalesOrderXPO.PriceGroup,
                                                                        PriceLine = GetPriceLine(_currSession, _locSalesOrderXPO, _locFreeItemRule.FpItem),
                                                                        UAmount = GetPriceLine(_currSession, _locSalesOrderXPO, _locFreeItemRule.FpItem).Amount2a,
                                                                    };
                                                                    _saveDataSalesOrderFreeItem.Save();
                                                                    _saveDataSalesOrderFreeItem.Session.CommitTransaction();

                                                                    _locSalesOrderLine.FreeItemChecked = true;
                                                                    _locSalesOrderLine.Save();
                                                                    _locSalesOrderLine.Session.CommitTransaction();

                                                                    if (_locFreeItemRule.FpBegInv != null && _locFreeItemRule.FpBegInvLine != null)
                                                                    {
                                                                        _locFreeItemRule.FpBegInvLine.QtyOnHand = _locFreeItemRule.FpBegInvLine.QtyOnHand + _locTotQtyFI;
                                                                        _locFreeItemRule.FpBegInvLine.Save();
                                                                        _locFreeItemRule.FpBegInvLine.Session.CommitTransaction();
                                                                    }

                                                                }
                                                            }
                                                        }
                                                        #endregion Foreach
                                                    }
                                                }

                                                #endregion Rule1

                                                //1, 2+1, 3+1, 4+1
                                                #region Rule2
                                                if (_locFreeItemRule.RuleType == RuleType.Rule2)
                                                {
                                                    XPCollection<SalesOrderLine> _locSalesOrderLines = new XPCollection<SalesOrderLine>
                                                                                                           (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                           new BinaryOperator("SalesOrder", _locSalesOrderXPO),
                                                                                                           new BinaryOperator("Item", _salesOrderLine.Item),
                                                                                                           new BinaryOperator("Status", Status.Progress),
                                                                                                           new BinaryOperator("FreeItemChecked", false),
                                                                                                           new BinaryOperator("Select", true)));

                                                    if (_locSalesOrderLines != null && _locSalesOrderLines.Count() > 0)
                                                    {

                                                        foreach (SalesOrderLine _locSalesOrderLine in _locSalesOrderLines)
                                                        {
                                                            double _locQtyFI = 0;
                                                            double _locTotQtyFI = 0;
                                                            double _locFpDqty = 0;
                                                            double _locFpQty = 0;
                                                            int _locMaxLoop = 0;

                                                            #region Amount
                                                            if (_locFreeItemRule.OpenAmount == true)
                                                            {
                                                                if (_locFreeItemRule.NetAmountRule == true)
                                                                {
                                                                    if (_locSalesOrderLine.TAmount >= _locFreeItemRule.NetTotalUAmount)
                                                                    {
                                                                        _locQtyFI = System.Math.Floor(_locSalesOrderLine.TAmount / _locFreeItemRule.NetTotalUAmount);
                                                                    }
                                                                }
                                                                if (_locFreeItemRule.GrossAmountRule == true)
                                                                {
                                                                    if (_locSalesOrderLine.TUAmount >= _locFreeItemRule.GrossTotalUAmount)
                                                                    {
                                                                        _locQtyFI = System.Math.Floor(_locSalesOrderLine.TUAmount / _locFreeItemRule.GrossTotalUAmount);
                                                                    }
                                                                }
                                                            }
                                                            #endregion Amount
                                                            #region Item
                                                            else
                                                            {
                                                                if (_locSalesOrderLine.TQty >= _locFreeItemRule.MpTQty)
                                                                {
                                                                    _locQtyFI = System.Math.Floor(_locSalesOrderLine.TQty / _locFreeItemRule.MpTQty);
                                                                }
                                                            }
                                                            #endregion Item

                                                            if (_locFreeItemRule.MaxLoop == 0)
                                                            {
                                                                _locMaxLoop = (int)_locQtyFI;
                                                            }
                                                            else
                                                            {
                                                                if (_locFreeItemRule.MaxLoop > (int)_locQtyFI)
                                                                {
                                                                    _locMaxLoop = (int)_locQtyFI;
                                                                }
                                                                else
                                                                {
                                                                    _locMaxLoop = _locFreeItemRule.MaxLoop;
                                                                }
                                                            }

                                                            for (int i = 1; i <= _locMaxLoop; i++)
                                                            {
                                                                if (i == 1)
                                                                {
                                                                    _locTotQtyFI = _locFreeItemRule.FpTQty;
                                                                }
                                                                else
                                                                {
                                                                    _locTotQtyFI = _locTotQtyFI + (i + _locFreeItemRule.FpTQty);
                                                                }

                                                            }

                                                            if (_locFreeItemRule.FpBegInvLine != null)
                                                            {
                                                                if ((_locFreeItemRule.FpBegInvLine.QtyAvailable - _locFreeItemRule.FpBegInvLine.QtyOnHand) > 0)
                                                                {
                                                                    if ((_locFreeItemRule.FpBegInvLine.QtyAvailable - _locFreeItemRule.FpBegInvLine.QtyOnHand) < _locTotQtyFI)
                                                                    {
                                                                        _locTotQtyFI = _locFreeItemRule.FpBegInvLine.QtyAvailable - _locFreeItemRule.FpBegInvLine.QtyOnHand;
                                                                    }

                                                                    if (_locFreeItemRule.FpItem != null && _locFreeItemRule.FpDUOM != null && _locFreeItemRule.FpUOM != null)
                                                                    {
                                                                        ItemUnitOfMeasure _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                                                                (new GroupOperator(GroupOperatorType.And,
                                                                                                 new BinaryOperator("Item", _locFreeItemRule.FpItem),
                                                                                                 new BinaryOperator("UOM", _locFreeItemRule.FpUOM),
                                                                                                 new BinaryOperator("DefaultUOM", _locFreeItemRule.FpDUOM),
                                                                                                 new BinaryOperator("Active", true)));
                                                                        if (_locItemUOM != null)
                                                                        {
                                                                            if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                                                            {
                                                                                _locFpDqty = _locTotQtyFI % _locItemUOM.DefaultConversion;
                                                                                _locFpQty = System.Math.Floor(_locTotQtyFI / _locItemUOM.DefaultConversion);
                                                                            }
                                                                            else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                                                            {
                                                                                _locFpDqty = System.Math.Floor(_locTotQtyFI / _locItemUOM.DefaultConversion);
                                                                                _locFpQty = (_locTotQtyFI % _locItemUOM.DefaultConversion) * _locItemUOM.Conversion;
                                                                            }
                                                                            else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                                                            {
                                                                                _locFpDqty = _locTotQtyFI;
                                                                            }
                                                                        }
                                                                    }
                                                                    else
                                                                    {
                                                                        _locFpDqty = _locTotQtyFI;
                                                                    }

                                                                    SalesOrderFreeItem _saveDataSalesOrderFreeItem = new SalesOrderFreeItem(_currSession)
                                                                    {
                                                                        //Organization
                                                                        Company = _locSalesOrderXPO.Company,
                                                                        Workplace = _locSalesOrderXPO.Workplace,
                                                                        Division = _locSalesOrderXPO.Division,
                                                                        Department = _locSalesOrderXPO.Department,
                                                                        Section = _locSalesOrderXPO.Section,
                                                                        Employee = _locSalesOrderXPO.Employee,
                                                                        //Item
                                                                        SalesOrder = _locSalesOrderXPO,
                                                                        SalesOrderLine = _locSalesOrderLine,
                                                                        Location = _locSalesOrderXPO.Location,
                                                                        BegInv = _locFreeItemRule.FpBegInv,
                                                                        FpLocationType = _locFreeItemRule.LocationType,
                                                                        FpStockType = _locFreeItemRule.StockType,
                                                                        FpStockGroup = _locFreeItemRule.FpStockGroup,
                                                                        FreeItemRule = _locFreeItemRule,
                                                                        FpItem = _locFreeItemRule.FpItem,
                                                                        FpDQty = _locFpDqty,
                                                                        FpDUOM = _locFreeItemRule.FpDUOM,
                                                                        FpQty = _locFpQty,
                                                                        FpUOM = _locFreeItemRule.FpUOM,
                                                                        FpTQty = _locTotQtyFI,
                                                                        BegInvLine = _locFreeItemRule.FpBegInvLine,
                                                                        //Amount
                                                                        Currency = _locSalesOrderXPO.Currency,
                                                                        PriceGroup = _locSalesOrderXPO.PriceGroup,
                                                                        PriceLine = GetPriceLine(_currSession, _locSalesOrderXPO, _locFreeItemRule.FpItem),
                                                                        UAmount = GetPriceLine(_currSession, _locSalesOrderXPO, _locFreeItemRule.FpItem).Amount2a,
                                                                    };
                                                                    _saveDataSalesOrderFreeItem.Save();
                                                                    _saveDataSalesOrderFreeItem.Session.CommitTransaction();

                                                                    _locSalesOrderLine.FreeItemChecked = true;
                                                                    _locSalesOrderLine.Save();
                                                                    _locSalesOrderLine.Session.CommitTransaction();

                                                                    if (_locFreeItemRule.FpBegInv != null && _locFreeItemRule.FpBegInvLine != null)
                                                                    {
                                                                        _locFreeItemRule.FpBegInvLine.QtyOnHand = _locFreeItemRule.FpBegInvLine.QtyOnHand + _locTotQtyFI;
                                                                        _locFreeItemRule.FpBegInvLine.Save();
                                                                        _locFreeItemRule.FpBegInvLine.Session.CommitTransaction();
                                                                    }

                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                                #endregion Rule2

                                                //2 * 2 * 2 * 2
                                                #region Rule3
                                                if (_locFreeItemRule.RuleType == RuleType.Rule3)
                                                {
                                                    XPCollection<SalesOrderLine> _locSalesOrderLines = new XPCollection<SalesOrderLine>
                                                                                                           (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                           new BinaryOperator("SalesOrder", _locSalesOrderXPO),
                                                                                                           new BinaryOperator("Item", _salesOrderLine.Item),
                                                                                                           new BinaryOperator("Status", Status.Progress),
                                                                                                           new BinaryOperator("FreeItemChecked", false),
                                                                                                           new BinaryOperator("Select", true)));

                                                    if (_locSalesOrderLines != null && _locSalesOrderLines.Count() > 0)
                                                    {

                                                        foreach (SalesOrderLine _locSalesOrderLine in _locSalesOrderLines)
                                                        {
                                                            double _locQtyFI = 0;
                                                            double _locTotQtyFI = 0;
                                                            double _locFpDqty = 0;
                                                            double _locFpQty = 0;
                                                            int _locMaxLoop = 0;

                                                            #region Amount
                                                            if (_locFreeItemRule.OpenAmount == true)
                                                            {
                                                                if (_locFreeItemRule.NetAmountRule == true)
                                                                {
                                                                    if (_locSalesOrderLine.TAmount >= _locFreeItemRule.NetTotalUAmount)
                                                                    {
                                                                        _locQtyFI = System.Math.Floor(_locSalesOrderLine.TAmount / _locFreeItemRule.NetTotalUAmount);
                                                                    }
                                                                }
                                                                if (_locFreeItemRule.GrossAmountRule == true)
                                                                {
                                                                    if (_locSalesOrderLine.TUAmount >= _locFreeItemRule.GrossTotalUAmount)
                                                                    {
                                                                        _locQtyFI = System.Math.Floor(_locSalesOrderLine.TUAmount / _locFreeItemRule.GrossTotalUAmount);
                                                                    }
                                                                }

                                                            }
                                                            #endregion Amount
                                                            #region Item
                                                            else
                                                            {
                                                                if (_locSalesOrderLine.TQty >= _locFreeItemRule.MpTQty)
                                                                {
                                                                    _locQtyFI = System.Math.Floor(_locSalesOrderLine.TQty / _locFreeItemRule.MpTQty);
                                                                }
                                                            }
                                                            #endregion Item

                                                            if (_locFreeItemRule.MaxLoop == 0)
                                                            {
                                                                _locMaxLoop = (int)_locQtyFI;
                                                            }
                                                            else
                                                            {
                                                                if (_locFreeItemRule.MaxLoop > (int)_locQtyFI)
                                                                {
                                                                    _locMaxLoop = (int)_locQtyFI;
                                                                }
                                                                else
                                                                {
                                                                    _locMaxLoop = _locFreeItemRule.MaxLoop;
                                                                }
                                                            }

                                                            for (int i = 1; i <= _locMaxLoop; i++)
                                                            {
                                                                _locTotQtyFI = _locTotQtyFI * _locFreeItemRule.FpTQty;
                                                            }

                                                            if (_locFreeItemRule.FpBegInvLine != null)
                                                            {
                                                                if ((_locFreeItemRule.FpBegInvLine.QtyAvailable - _locFreeItemRule.FpBegInvLine.QtyOnHand) > 0)
                                                                {
                                                                    if ((_locFreeItemRule.FpBegInvLine.QtyAvailable - _locFreeItemRule.FpBegInvLine.QtyOnHand) < _locTotQtyFI)
                                                                    {
                                                                        _locTotQtyFI = _locFreeItemRule.FpBegInvLine.QtyAvailable - _locFreeItemRule.FpBegInvLine.QtyOnHand;
                                                                    }

                                                                    if (_locFreeItemRule.FpItem != null && _locFreeItemRule.FpDUOM != null && _locFreeItemRule.FpUOM != null)
                                                                    {
                                                                        ItemUnitOfMeasure _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                                                                (new GroupOperator(GroupOperatorType.And,
                                                                                                 new BinaryOperator("Item", _locFreeItemRule.FpItem),
                                                                                                 new BinaryOperator("UOM", _locFreeItemRule.FpUOM),
                                                                                                 new BinaryOperator("DefaultUOM", _locFreeItemRule.FpDUOM),
                                                                                                 new BinaryOperator("Active", true)));
                                                                        if (_locItemUOM != null)
                                                                        {
                                                                            if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                                                            {
                                                                                _locFpDqty = _locTotQtyFI % _locItemUOM.DefaultConversion;
                                                                                _locFpQty = System.Math.Floor(_locTotQtyFI / _locItemUOM.DefaultConversion);
                                                                            }
                                                                            else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                                                            {
                                                                                _locFpDqty = System.Math.Floor(_locTotQtyFI / _locItemUOM.DefaultConversion);
                                                                                _locFpQty = (_locTotQtyFI % _locItemUOM.DefaultConversion) * _locItemUOM.Conversion;
                                                                            }
                                                                            else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                                                            {
                                                                                _locFpDqty = _locTotQtyFI;
                                                                            }
                                                                        }
                                                                    }
                                                                    else
                                                                    {
                                                                        _locFpDqty = _locTotQtyFI;
                                                                    }

                                                                    SalesOrderFreeItem _saveDataSalesOrderFreeItem = new SalesOrderFreeItem(_currSession)
                                                                    {
                                                                        //Organization
                                                                        Company = _locSalesOrderXPO.Company,
                                                                        Workplace = _locSalesOrderXPO.Workplace,
                                                                        Division = _locSalesOrderXPO.Division,
                                                                        Department = _locSalesOrderXPO.Department,
                                                                        Section = _locSalesOrderXPO.Section,
                                                                        Employee = _locSalesOrderXPO.Employee,
                                                                        //Item
                                                                        SalesOrder = _locSalesOrderXPO,
                                                                        SalesOrderLine = _locSalesOrderLine,
                                                                        Location = _locSalesOrderXPO.Location,
                                                                        BegInv = _locFreeItemRule.FpBegInv,
                                                                        FpLocationType = _locFreeItemRule.LocationType,
                                                                        FpStockType = _locFreeItemRule.StockType,
                                                                        FpStockGroup = _locFreeItemRule.FpStockGroup,
                                                                        FreeItemRule = _locFreeItemRule,
                                                                        FpItem = _locFreeItemRule.FpItem,
                                                                        FpDQty = _locFpDqty,
                                                                        FpDUOM = _locFreeItemRule.FpDUOM,
                                                                        FpQty = _locFpQty,
                                                                        FpUOM = _locFreeItemRule.FpUOM,
                                                                        FpTQty = _locTotQtyFI,
                                                                        BegInvLine = _locFreeItemRule.FpBegInvLine,
                                                                        //Amount
                                                                        Currency = _locSalesOrderXPO.Currency,
                                                                        PriceGroup = _locSalesOrderXPO.PriceGroup,
                                                                        PriceLine = GetPriceLine(_currSession, _locSalesOrderXPO, _locFreeItemRule.FpItem),
                                                                        UAmount = GetPriceLine(_currSession, _locSalesOrderXPO, _locFreeItemRule.FpItem).Amount2a,
                                                                    };
                                                                    _saveDataSalesOrderFreeItem.Save();
                                                                    _saveDataSalesOrderFreeItem.Session.CommitTransaction();

                                                                    _locSalesOrderLine.FreeItemChecked = true;
                                                                    _locSalesOrderLine.Save();
                                                                    _locSalesOrderLine.Session.CommitTransaction();

                                                                    if (_locFreeItemRule.FpBegInv != null && _locFreeItemRule.FpBegInvLine != null)
                                                                    {
                                                                        _locFreeItemRule.FpBegInvLine.QtyOnHand = _locFreeItemRule.FpBegInvLine.QtyOnHand + _locTotQtyFI;
                                                                        _locFreeItemRule.FpBegInvLine.Save();
                                                                        _locFreeItemRule.FpBegInvLine.Session.CommitTransaction();
                                                                    }

                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                                #endregion Rule3

                                                //2^(1-1) + 2^(2-1) + 2^(3-1) + 2^(4-1)
                                                #region Rule4
                                                if (_locFreeItemRule.RuleType == RuleType.Rule4)
                                                {
                                                    XPCollection<SalesOrderLine> _locSalesOrderLines = new XPCollection<SalesOrderLine>
                                                                                                           (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                           new BinaryOperator("SalesOrder", _locSalesOrderXPO),
                                                                                                           new BinaryOperator("Item", _salesOrderLine.Item),
                                                                                                           new BinaryOperator("Status", Status.Progress),
                                                                                                           new BinaryOperator("FreeItemChecked", false),
                                                                                                           new BinaryOperator("Select", true)));

                                                    if (_locSalesOrderLines != null && _locSalesOrderLines.Count() > 0)
                                                    {

                                                        foreach (SalesOrderLine _locSalesOrderLine in _locSalesOrderLines)
                                                        {
                                                            double _locQtyFI = 0;
                                                            double _locTotQtyFI = 0;
                                                            double _locFpDqty = 0;
                                                            double _locFpQty = 0;
                                                            int _locMaxLoop = 0;

                                                            #region Amount
                                                            if (_locFreeItemRule.OpenAmount == true)
                                                            {
                                                                if (_locFreeItemRule.NetAmountRule == true)
                                                                {
                                                                    if (_locSalesOrderLine.TAmount >= _locFreeItemRule.NetTotalUAmount)
                                                                    {
                                                                        _locQtyFI = System.Math.Floor(_locSalesOrderLine.TAmount / _locFreeItemRule.NetTotalUAmount);
                                                                    }
                                                                }
                                                                if (_locFreeItemRule.GrossAmountRule == true)
                                                                {
                                                                    if (_locSalesOrderLine.TUAmount >= _locFreeItemRule.GrossTotalUAmount)
                                                                    {
                                                                        _locQtyFI = System.Math.Floor(_locSalesOrderLine.TUAmount / _locFreeItemRule.GrossTotalUAmount);
                                                                    }
                                                                }

                                                            }
                                                            #endregion Amount
                                                            #region Item
                                                            else
                                                            {
                                                                if (_locSalesOrderLine.TQty >= _locFreeItemRule.MpTQty)
                                                                {
                                                                    _locQtyFI = System.Math.Floor(_locSalesOrderLine.TQty / _locFreeItemRule.MpTQty);
                                                                }
                                                            }
                                                            #endregion Item

                                                            if (_locFreeItemRule.MaxLoop == 0)
                                                            {
                                                                _locMaxLoop = (int)_locQtyFI;
                                                            }
                                                            else
                                                            {
                                                                if (_locFreeItemRule.MaxLoop > (int)_locQtyFI)
                                                                {
                                                                    _locMaxLoop = (int)_locQtyFI;
                                                                }
                                                                else
                                                                {
                                                                    _locMaxLoop = _locFreeItemRule.MaxLoop;
                                                                }
                                                            }

                                                            for (int i = 1; i <= _locMaxLoop; i++)
                                                            {

                                                                _locTotQtyFI = _locTotQtyFI + System.Math.Pow(_locFreeItemRule.FpTQty, i - 1);
                                                            }

                                                            if (_locFreeItemRule.FpBegInvLine != null)
                                                            {
                                                                if ((_locFreeItemRule.FpBegInvLine.QtyAvailable - _locFreeItemRule.FpBegInvLine.QtyOnHand) > 0)
                                                                {
                                                                    if ((_locFreeItemRule.FpBegInvLine.QtyAvailable - _locFreeItemRule.FpBegInvLine.QtyOnHand) < _locTotQtyFI)
                                                                    {
                                                                        _locTotQtyFI = _locFreeItemRule.FpBegInvLine.QtyAvailable - _locFreeItemRule.FpBegInvLine.QtyOnHand;
                                                                    }

                                                                    if (_locFreeItemRule.FpItem != null && _locFreeItemRule.FpDUOM != null && _locFreeItemRule.FpUOM != null)
                                                                    {
                                                                        ItemUnitOfMeasure _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                                                                (new GroupOperator(GroupOperatorType.And,
                                                                                                 new BinaryOperator("Item", _locFreeItemRule.FpItem),
                                                                                                 new BinaryOperator("UOM", _locFreeItemRule.FpUOM),
                                                                                                 new BinaryOperator("DefaultUOM", _locFreeItemRule.FpDUOM),
                                                                                                 new BinaryOperator("Active", true)));
                                                                        if (_locItemUOM != null)
                                                                        {
                                                                            if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                                                            {
                                                                                _locFpDqty = _locTotQtyFI % _locItemUOM.DefaultConversion;
                                                                                _locFpQty = System.Math.Floor(_locTotQtyFI / _locItemUOM.DefaultConversion);
                                                                            }
                                                                            else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                                                            {
                                                                                _locFpDqty = System.Math.Floor(_locTotQtyFI / _locItemUOM.DefaultConversion);
                                                                                _locFpQty = (_locTotQtyFI % _locItemUOM.DefaultConversion) * _locItemUOM.Conversion;
                                                                            }
                                                                            else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                                                            {
                                                                                _locFpDqty = _locTotQtyFI;
                                                                            }
                                                                        }
                                                                    }
                                                                    else
                                                                    {
                                                                        _locFpDqty = _locTotQtyFI;
                                                                    }

                                                                    SalesOrderFreeItem _saveDataSalesOrderFreeItem = new SalesOrderFreeItem(_currSession)
                                                                    {
                                                                        //Organization
                                                                        Company = _locSalesOrderXPO.Company,
                                                                        Workplace = _locSalesOrderXPO.Workplace,
                                                                        Division = _locSalesOrderXPO.Division,
                                                                        Department = _locSalesOrderXPO.Department,
                                                                        Section = _locSalesOrderXPO.Section,
                                                                        Employee = _locSalesOrderXPO.Employee,
                                                                        //Item
                                                                        SalesOrder = _locSalesOrderXPO,
                                                                        SalesOrderLine = _locSalesOrderLine,
                                                                        Location = _locSalesOrderXPO.Location,
                                                                        BegInv = _locFreeItemRule.FpBegInv,
                                                                        FpLocationType = _locFreeItemRule.LocationType,
                                                                        FpStockType = _locFreeItemRule.StockType,
                                                                        FpStockGroup = _locFreeItemRule.FpStockGroup,
                                                                        FreeItemRule = _locFreeItemRule,
                                                                        FpItem = _locFreeItemRule.FpItem,
                                                                        FpDQty = _locFpDqty,
                                                                        FpDUOM = _locFreeItemRule.FpDUOM,
                                                                        FpQty = _locFpQty,
                                                                        FpUOM = _locFreeItemRule.FpUOM,
                                                                        FpTQty = _locTotQtyFI,
                                                                        BegInvLine = _locFreeItemRule.FpBegInvLine,
                                                                        //Amount
                                                                        Currency = _locSalesOrderXPO.Currency,
                                                                        PriceGroup = _locSalesOrderXPO.PriceGroup,
                                                                        PriceLine = GetPriceLine(_currSession, _locSalesOrderXPO, _locFreeItemRule.FpItem),
                                                                        UAmount = GetPriceLine(_currSession, _locSalesOrderXPO, _locFreeItemRule.FpItem).Amount2a,
                                                                    };
                                                                    _saveDataSalesOrderFreeItem.Save();
                                                                    _saveDataSalesOrderFreeItem.Session.CommitTransaction();

                                                                    _locSalesOrderLine.FreeItemChecked = true;
                                                                    _locSalesOrderLine.Save();
                                                                    _locSalesOrderLine.Session.CommitTransaction();

                                                                    if (_locFreeItemRule.FpBegInv != null && _locFreeItemRule.FpBegInvLine != null)
                                                                    {
                                                                        _locFreeItemRule.FpBegInvLine.QtyOnHand = _locFreeItemRule.FpBegInvLine.QtyOnHand + _locTotQtyFI;
                                                                        _locFreeItemRule.FpBegInvLine.Save();
                                                                        _locFreeItemRule.FpBegInvLine.Session.CommitTransaction();
                                                                    }

                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                                #endregion Rule4
                                            }

                                        }
                                        #endregion MultiItem=false
                                        #region MultiItem=true
                                        else
                                        {
                                            SalesOrderLine _locComboSalesOrderLine = _currSession.FindObject<SalesOrderLine>
                                                                                                        (new GroupOperator(GroupOperatorType.And,
                                                                                                        new BinaryOperator("SalesOrder", _locSalesOrderXPO),
                                                                                                        new BinaryOperator("Item", _locFreeItemRule.CpItem),
                                                                                                        new BinaryOperator("Status", Status.Progress),
                                                                                                        new BinaryOperator("FreeItemChecked", false),
                                                                                                        new BinaryOperator("Select", true)));

                                            SalesOrderLine _locMainSalesOrderLine = _currSession.FindObject<SalesOrderLine>
                                                                                                       (new GroupOperator(GroupOperatorType.And,
                                                                                                       new BinaryOperator("SalesOrder", _locSalesOrderXPO),
                                                                                                       new BinaryOperator("Item", _locFreeItemRule.MpItem),
                                                                                                       new BinaryOperator("Status", Status.Progress),
                                                                                                       new BinaryOperator("FreeItemChecked", false),
                                                                                                       new BinaryOperator("Select", true)));

                                            if (_locMainSalesOrderLine != null && _locComboSalesOrderLine != null)
                                            {
                                                double _locComboQtyCI = 0;
                                                double _locComboTotQtyFI = 0;
                                                double _locComboCpDqty = 0;
                                                double _locComboCpQty = 0;
                                                int _locComboMaxLoop = 0;
                                                int _locComboTotMaxLoopVal = 0;

                                                double _locMainQtyFI = 0;
                                                double _locTotQtyFI = 0;
                                                double _locFpDqty = 0;
                                                double _locFpQty = 0;
                                                int _locMaxLoop = 0;
                                                int _locTotMaxLoopVal = 0;

                                                if (_locMainSalesOrderLine.TQty >= _locFreeItemRule.MpTQty && _locComboSalesOrderLine.TQty >= _locFreeItemRule.CpTQty)
                                                {
                                                    _locMainQtyFI = System.Math.Floor(_locMainSalesOrderLine.TQty / _locFreeItemRule.MpTQty);
                                                    _locComboQtyCI = System.Math.Floor(_locComboSalesOrderLine.TQty / _locFreeItemRule.CpTQty);

                                                    if (_locComboQtyCI < _locMainQtyFI)
                                                    {
                                                        #region ComboPromoItem

                                                        //1+1+1+1+1
                                                        #region Rule1
                                                        if (_locFreeItemRule.RuleType == RuleType.Rule1)
                                                        {

                                                            if (_locFreeItemRule.MaxLoop == 0)
                                                            {
                                                                _locComboMaxLoop = (int)_locComboQtyCI;
                                                            }
                                                            else
                                                            {
                                                                if (_locFreeItemRule.MaxLoop > (int)_locComboQtyCI)
                                                                {
                                                                    _locComboMaxLoop = (int)_locComboQtyCI;
                                                                }
                                                                else
                                                                {
                                                                    _locComboMaxLoop = _locFreeItemRule.MaxLoop;
                                                                }

                                                            }

                                                            for (int i = 1; i <= _locComboMaxLoop; i++)
                                                            {
                                                                _locComboTotMaxLoopVal = _locComboTotMaxLoopVal + 1;
                                                            }

                                                            _locComboTotQtyFI = _locComboTotMaxLoopVal * _locFreeItemRule.FpTQty;

                                                            if(_locFreeItemRule.FpBegInvLine != null)
                                                            {
                                                                if((_locFreeItemRule.FpBegInvLine.QtyAvailable - _locFreeItemRule.FpBegInvLine.QtyOnHand) > 0)
                                                                {
                                                                    if ((_locFreeItemRule.FpBegInvLine.QtyAvailable - _locFreeItemRule.FpBegInvLine.QtyOnHand) < _locComboTotQtyFI)
                                                                    {
                                                                        _locComboTotQtyFI = _locFreeItemRule.FpBegInvLine.QtyAvailable - _locFreeItemRule.FpBegInvLine.QtyOnHand;
                                                                    }

                                                                    if (_locFreeItemRule.FpItem != null && _locFreeItemRule.FpDUOM != null && _locFreeItemRule.FpUOM != null)
                                                                    {
                                                                        ItemUnitOfMeasure _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                                                                (new GroupOperator(GroupOperatorType.And,
                                                                                                 new BinaryOperator("Item", _locFreeItemRule.FpItem),
                                                                                                 new BinaryOperator("UOM", _locFreeItemRule.FpUOM),
                                                                                                 new BinaryOperator("DefaultUOM", _locFreeItemRule.FpDUOM),
                                                                                                 new BinaryOperator("Active", true)));
                                                                        if (_locItemUOM != null)
                                                                        {
                                                                            if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                                                            {
                                                                                _locComboCpDqty = _locComboTotQtyFI % _locItemUOM.DefaultConversion;
                                                                                _locComboCpQty = System.Math.Floor(_locComboTotQtyFI / _locItemUOM.DefaultConversion);
                                                                            }
                                                                            else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                                                            {
                                                                                _locComboCpDqty = System.Math.Floor(_locComboTotQtyFI / _locItemUOM.DefaultConversion);
                                                                                _locComboCpQty = (_locComboTotQtyFI % _locItemUOM.DefaultConversion) * _locItemUOM.Conversion;
                                                                            }
                                                                            else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                                                            {
                                                                                _locComboCpDqty = _locComboTotQtyFI;
                                                                            }
                                                                        }
                                                                    }
                                                                    else
                                                                    {
                                                                        _locComboCpDqty = _locComboTotQtyFI;
                                                                    }

                                                                    SalesOrderFreeItem _saveDataSalesOrderFreeItem = new SalesOrderFreeItem(_currSession)
                                                                    {
                                                                        //Organization
                                                                        Company = _locSalesOrderXPO.Company,
                                                                        Workplace = _locSalesOrderXPO.Workplace,
                                                                        Division = _locSalesOrderXPO.Division,
                                                                        Department = _locSalesOrderXPO.Department,
                                                                        Section = _locSalesOrderXPO.Section,
                                                                        Employee = _locSalesOrderXPO.Employee,
                                                                        //Item
                                                                        SalesOrder = _locSalesOrderXPO,
                                                                        Location = _locFreeItemRule.Location,
                                                                        FreeItemRule = _locFreeItemRule,
                                                                        BegInv = _locFreeItemRule.FpBegInv,
                                                                        FpLocationType = _locFreeItemRule.LocationType,
                                                                        FpStockType = _locFreeItemRule.StockType,
                                                                        FpItem = _locFreeItemRule.FpItem,
                                                                        FpDQty = _locComboCpDqty,
                                                                        FpDUOM = _locFreeItemRule.FpDUOM,
                                                                        FpQty = _locComboCpQty,
                                                                        FpUOM = _locFreeItemRule.FpUOM,
                                                                        FpTQty = _locComboTotQtyFI,
                                                                        FpStockGroup = _locFreeItemRule.FpStockGroup,
                                                                        BegInvLine = _locFreeItemRule.FpBegInvLine,
                                                                        //Amount
                                                                        Currency = _locSalesOrderXPO.Currency,
                                                                        PriceGroup = _locSalesOrderXPO.PriceGroup,
                                                                        PriceLine = GetPriceLine(_currSession, _locSalesOrderXPO, _locFreeItemRule.FpItem),
                                                                        UAmount = GetPriceLine(_currSession, _locSalesOrderXPO, _locFreeItemRule.FpItem).Amount2a,
                                                                    };
                                                                    _saveDataSalesOrderFreeItem.Save();
                                                                    _saveDataSalesOrderFreeItem.Session.CommitTransaction();

                                                                    _locComboSalesOrderLine.FreeItemChecked = true;
                                                                    _locComboSalesOrderLine.Save();
                                                                    _locComboSalesOrderLine.Session.CommitTransaction();

                                                                    _locMainSalesOrderLine.FreeItemChecked = true;
                                                                    _locMainSalesOrderLine.Save();
                                                                    _locMainSalesOrderLine.Session.CommitTransaction();

                                                                    if (_locFreeItemRule.FpBegInv != null && _locFreeItemRule.FpBegInvLine != null)
                                                                    {
                                                                        _locFreeItemRule.FpBegInvLine.QtyOnHand = _locFreeItemRule.FpBegInvLine.QtyOnHand + _locComboTotQtyFI;
                                                                        _locFreeItemRule.FpBegInvLine.Save();
                                                                        _locFreeItemRule.FpBegInvLine.Session.CommitTransaction();
                                                                    }

                                                                }
                                                            }
                                                        }

                                                        #endregion Rule1

                                                        //1, 2+1, 3+1, 4+1
                                                        #region Rule2

                                                        if (_locFreeItemRule.RuleType == RuleType.Rule2)
                                                        {
                                                            if (_locFreeItemRule.MaxLoop == 0)
                                                            {
                                                                _locComboMaxLoop = (int)_locComboQtyCI;
                                                            }
                                                            else
                                                            {
                                                                if (_locFreeItemRule.MaxLoop > (int)_locComboQtyCI)
                                                                {
                                                                    _locComboMaxLoop = (int)_locComboQtyCI;
                                                                }
                                                                else
                                                                {
                                                                    _locComboMaxLoop = _locFreeItemRule.MaxLoop;
                                                                }
                                                            }

                                                            for (int i = 1; i <= _locComboMaxLoop; i++)
                                                            {
                                                                if (i == 1)
                                                                {
                                                                    _locComboTotQtyFI = _locFreeItemRule.FpTQty;
                                                                }
                                                                else
                                                                {
                                                                    _locComboTotQtyFI = _locComboTotQtyFI + (i + _locFreeItemRule.FpTQty);
                                                                }
                                                            }

                                                            if (_locFreeItemRule.FpBegInvLine != null)
                                                            {
                                                                if ((_locFreeItemRule.FpBegInvLine.QtyAvailable - _locFreeItemRule.FpBegInvLine.QtyOnHand) > 0)
                                                                {
                                                                    if ((_locFreeItemRule.FpBegInvLine.QtyAvailable - _locFreeItemRule.FpBegInvLine.QtyOnHand) < _locComboTotQtyFI)
                                                                    {
                                                                        _locComboTotQtyFI = _locFreeItemRule.FpBegInvLine.QtyAvailable - _locFreeItemRule.FpBegInvLine.QtyOnHand;
                                                                    }

                                                                    if (_locFreeItemRule.FpItem != null && _locFreeItemRule.FpDUOM != null && _locFreeItemRule.FpUOM != null)
                                                                    {
                                                                        ItemUnitOfMeasure _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                                                                (new GroupOperator(GroupOperatorType.And,
                                                                                                 new BinaryOperator("Item", _locFreeItemRule.FpItem),
                                                                                                 new BinaryOperator("UOM", _locFreeItemRule.FpUOM),
                                                                                                 new BinaryOperator("DefaultUOM", _locFreeItemRule.FpDUOM),
                                                                                                 new BinaryOperator("Active", true)));
                                                                        if (_locItemUOM != null)
                                                                        {
                                                                            if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                                                            {
                                                                                _locComboCpDqty = _locComboTotQtyFI % _locItemUOM.DefaultConversion;
                                                                                _locComboCpQty = System.Math.Floor(_locComboTotQtyFI / _locItemUOM.DefaultConversion);
                                                                            }
                                                                            else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                                                            {
                                                                                _locComboCpDqty = System.Math.Floor(_locComboTotQtyFI / _locItemUOM.DefaultConversion);
                                                                                _locComboCpQty = (_locComboTotQtyFI % _locItemUOM.DefaultConversion) * _locItemUOM.Conversion;
                                                                            }
                                                                            else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                                                            {
                                                                                _locComboCpDqty = _locComboTotQtyFI;
                                                                            }
                                                                        }
                                                                    }
                                                                    else
                                                                    {
                                                                        _locComboCpDqty = _locComboTotQtyFI;
                                                                    }

                                                                    SalesOrderFreeItem _saveDataSalesOrderFreeItem = new SalesOrderFreeItem(_currSession)
                                                                    {
                                                                        //Organization
                                                                        Company = _locSalesOrderXPO.Company,
                                                                        Workplace = _locSalesOrderXPO.Workplace,
                                                                        Division = _locSalesOrderXPO.Division,
                                                                        Department = _locSalesOrderXPO.Department,
                                                                        Section = _locSalesOrderXPO.Section,
                                                                        Employee = _locSalesOrderXPO.Employee,
                                                                        //Item
                                                                        SalesOrder = _locSalesOrderXPO,
                                                                        Location = _locFreeItemRule.Location,
                                                                        FreeItemRule = _locFreeItemRule,
                                                                        BegInv = _locFreeItemRule.FpBegInv,
                                                                        FpLocationType = _locFreeItemRule.LocationType,
                                                                        FpStockType = _locFreeItemRule.StockType,
                                                                        FpItem = _locFreeItemRule.FpItem,
                                                                        FpDQty = _locComboCpDqty,
                                                                        FpDUOM = _locFreeItemRule.FpDUOM,
                                                                        FpQty = _locComboCpQty,
                                                                        FpUOM = _locFreeItemRule.FpUOM,
                                                                        FpTQty = _locComboTotQtyFI,
                                                                        FpStockGroup = _locFreeItemRule.FpStockGroup,
                                                                        BegInvLine = _locFreeItemRule.FpBegInvLine,
                                                                        //Amount
                                                                        Currency = _locSalesOrderXPO.Currency,
                                                                        PriceGroup = _locSalesOrderXPO.PriceGroup,
                                                                        PriceLine = GetPriceLine(_currSession, _locSalesOrderXPO, _locFreeItemRule.FpItem),
                                                                        UAmount = GetPriceLine(_currSession, _locSalesOrderXPO, _locFreeItemRule.FpItem).Amount2a,
                                                                    };
                                                                    _saveDataSalesOrderFreeItem.Save();
                                                                    _saveDataSalesOrderFreeItem.Session.CommitTransaction();

                                                                    _locComboSalesOrderLine.FreeItemChecked = true;
                                                                    _locComboSalesOrderLine.Save();
                                                                    _locComboSalesOrderLine.Session.CommitTransaction();

                                                                    _locMainSalesOrderLine.FreeItemChecked = true;
                                                                    _locMainSalesOrderLine.Save();
                                                                    _locMainSalesOrderLine.Session.CommitTransaction();

                                                                    if (_locFreeItemRule.FpBegInv != null && _locFreeItemRule.FpBegInvLine != null)
                                                                    {
                                                                        _locFreeItemRule.FpBegInvLine.QtyOnHand = _locFreeItemRule.FpBegInvLine.QtyOnHand + _locComboTotQtyFI;
                                                                        _locFreeItemRule.FpBegInvLine.Save();
                                                                        _locFreeItemRule.FpBegInvLine.Session.CommitTransaction();
                                                                    }

                                                                }
                                                            }

                                                        }
                                                        #endregion Rule2

                                                        //2 * 2 * 2 * 2
                                                        #region Rule3

                                                        if (_locFreeItemRule.RuleType == RuleType.Rule3)
                                                        {

                                                            if (_locFreeItemRule.MaxLoop == 0)
                                                            {
                                                                _locComboMaxLoop = (int)_locComboQtyCI;
                                                            }
                                                            else
                                                            {
                                                                if (_locFreeItemRule.MaxLoop > (int)_locComboQtyCI)
                                                                {
                                                                    _locComboMaxLoop = (int)_locComboQtyCI;
                                                                }
                                                                else
                                                                {
                                                                    _locComboMaxLoop = _locFreeItemRule.MaxLoop;
                                                                }
                                                            }

                                                            for (int i = 1; i <= _locComboMaxLoop; i++)
                                                            {
                                                                _locComboTotQtyFI = _locComboTotQtyFI * _locFreeItemRule.FpTQty;
                                                            }

                                                            if (_locFreeItemRule.FpBegInvLine != null)
                                                            {
                                                                if ((_locFreeItemRule.FpBegInvLine.QtyAvailable - _locFreeItemRule.FpBegInvLine.QtyOnHand) > 0)
                                                                {
                                                                    if ((_locFreeItemRule.FpBegInvLine.QtyAvailable - _locFreeItemRule.FpBegInvLine.QtyOnHand) < _locComboTotQtyFI)
                                                                    {
                                                                        _locComboTotQtyFI = _locFreeItemRule.FpBegInvLine.QtyAvailable - _locFreeItemRule.FpBegInvLine.QtyOnHand;
                                                                    }

                                                                    if (_locFreeItemRule.FpItem != null && _locFreeItemRule.FpDUOM != null && _locFreeItemRule.FpUOM != null)
                                                                    {
                                                                        ItemUnitOfMeasure _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                                                                (new GroupOperator(GroupOperatorType.And,
                                                                                                 new BinaryOperator("Item", _locFreeItemRule.FpItem),
                                                                                                 new BinaryOperator("UOM", _locFreeItemRule.FpUOM),
                                                                                                 new BinaryOperator("DefaultUOM", _locFreeItemRule.FpDUOM),
                                                                                                 new BinaryOperator("Active", true)));
                                                                        if (_locItemUOM != null)
                                                                        {
                                                                            if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                                                            {
                                                                                _locComboCpDqty = _locComboTotQtyFI % _locItemUOM.DefaultConversion;
                                                                                _locComboCpQty = System.Math.Floor(_locComboTotQtyFI / _locItemUOM.DefaultConversion);
                                                                            }
                                                                            else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                                                            {
                                                                                _locComboCpDqty = System.Math.Floor(_locComboTotQtyFI / _locItemUOM.DefaultConversion);
                                                                                _locComboCpQty = (_locComboTotQtyFI % _locItemUOM.DefaultConversion) * _locItemUOM.Conversion;
                                                                            }
                                                                            else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                                                            {
                                                                                _locComboCpDqty = _locComboTotQtyFI;
                                                                            }
                                                                        }
                                                                    }
                                                                    else
                                                                    {
                                                                        _locComboCpDqty = _locComboTotQtyFI;
                                                                    }

                                                                    SalesOrderFreeItem _saveDataSalesOrderFreeItem = new SalesOrderFreeItem(_currSession)
                                                                    {
                                                                        //Organization
                                                                        Company = _locSalesOrderXPO.Company,
                                                                        Workplace = _locSalesOrderXPO.Workplace,
                                                                        Division = _locSalesOrderXPO.Division,
                                                                        Department = _locSalesOrderXPO.Department,
                                                                        Section = _locSalesOrderXPO.Section,
                                                                        Employee = _locSalesOrderXPO.Employee,
                                                                        //Item
                                                                        SalesOrder = _locSalesOrderXPO,
                                                                        Location = _locFreeItemRule.Location,
                                                                        FreeItemRule = _locFreeItemRule,
                                                                        BegInv = _locFreeItemRule.FpBegInv,
                                                                        FpLocationType = _locFreeItemRule.LocationType,
                                                                        FpStockType = _locFreeItemRule.StockType,
                                                                        FpItem = _locFreeItemRule.FpItem,
                                                                        FpDQty = _locComboCpDqty,
                                                                        FpDUOM = _locFreeItemRule.FpDUOM,
                                                                        FpQty = _locComboCpQty,
                                                                        FpUOM = _locFreeItemRule.FpUOM,
                                                                        FpTQty = _locComboTotQtyFI,
                                                                        FpStockGroup = _locFreeItemRule.FpStockGroup,
                                                                        BegInvLine = _locFreeItemRule.FpBegInvLine,
                                                                        //Amount
                                                                        Currency = _locSalesOrderXPO.Currency,
                                                                        PriceGroup = _locSalesOrderXPO.PriceGroup,
                                                                        PriceLine = GetPriceLine(_currSession, _locSalesOrderXPO, _locFreeItemRule.FpItem),
                                                                        UAmount = GetPriceLine(_currSession, _locSalesOrderXPO, _locFreeItemRule.FpItem).Amount2a,
                                                                    };
                                                                    _saveDataSalesOrderFreeItem.Save();
                                                                    _saveDataSalesOrderFreeItem.Session.CommitTransaction();

                                                                    _locComboSalesOrderLine.FreeItemChecked = true;
                                                                    _locComboSalesOrderLine.Save();
                                                                    _locComboSalesOrderLine.Session.CommitTransaction();

                                                                    _locMainSalesOrderLine.FreeItemChecked = true;
                                                                    _locMainSalesOrderLine.Save();
                                                                    _locMainSalesOrderLine.Session.CommitTransaction();

                                                                    if (_locFreeItemRule.FpBegInv != null && _locFreeItemRule.FpBegInvLine != null)
                                                                    {
                                                                        _locFreeItemRule.FpBegInvLine.QtyOnHand = _locFreeItemRule.FpBegInvLine.QtyOnHand + _locComboTotQtyFI;
                                                                        _locFreeItemRule.FpBegInvLine.Save();
                                                                        _locFreeItemRule.FpBegInvLine.Session.CommitTransaction();
                                                                    }

                                                                }
                                                            }

                                                        }
                                                        #endregion Rule3

                                                        //2^(1-1) + 2^(2-1) + 2^(3-1) + 2^(4-1)
                                                        #region Rule4

                                                        if (_locFreeItemRule.RuleType == RuleType.Rule4)
                                                        {
                                                            if (_locFreeItemRule.MaxLoop == 0)
                                                            {
                                                                _locComboMaxLoop = (int)_locComboQtyCI;
                                                            }
                                                            else
                                                            {
                                                                if (_locFreeItemRule.MaxLoop > (int)_locComboQtyCI)
                                                                {
                                                                    _locComboMaxLoop = (int)_locComboQtyCI;
                                                                }
                                                                else
                                                                {
                                                                    _locComboMaxLoop = _locFreeItemRule.MaxLoop;
                                                                }
                                                            }

                                                            for (int i = 1; i <= _locComboMaxLoop; i++)
                                                            {
                                                                _locComboTotQtyFI = _locComboTotQtyFI + System.Math.Pow(_locFreeItemRule.FpTQty, i - 1);
                                                            }

                                                            if (_locFreeItemRule.FpBegInvLine != null)
                                                            {
                                                                if ((_locFreeItemRule.FpBegInvLine.QtyAvailable - _locFreeItemRule.FpBegInvLine.QtyOnHand) > 0)
                                                                {
                                                                    if ((_locFreeItemRule.FpBegInvLine.QtyAvailable - _locFreeItemRule.FpBegInvLine.QtyOnHand) < _locComboTotQtyFI)
                                                                    {
                                                                        _locComboTotQtyFI = _locFreeItemRule.FpBegInvLine.QtyAvailable - _locFreeItemRule.FpBegInvLine.QtyOnHand;
                                                                    }

                                                                    if (_locFreeItemRule.FpItem != null && _locFreeItemRule.FpDUOM != null && _locFreeItemRule.FpUOM != null)
                                                                    {
                                                                        ItemUnitOfMeasure _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                                                                (new GroupOperator(GroupOperatorType.And,
                                                                                                 new BinaryOperator("Item", _locFreeItemRule.FpItem),
                                                                                                 new BinaryOperator("UOM", _locFreeItemRule.FpUOM),
                                                                                                 new BinaryOperator("DefaultUOM", _locFreeItemRule.FpDUOM),
                                                                                                 new BinaryOperator("Active", true)));
                                                                        if (_locItemUOM != null)
                                                                        {
                                                                            if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                                                            {
                                                                                _locComboCpDqty = _locComboTotQtyFI % _locItemUOM.DefaultConversion;
                                                                                _locComboCpQty = System.Math.Floor(_locComboTotQtyFI / _locItemUOM.DefaultConversion);
                                                                            }
                                                                            else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                                                            {
                                                                                _locComboCpDqty = System.Math.Floor(_locComboTotQtyFI / _locItemUOM.DefaultConversion);
                                                                                _locComboCpQty = (_locComboTotQtyFI % _locItemUOM.DefaultConversion) * _locItemUOM.Conversion;
                                                                            }
                                                                            else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                                                            {
                                                                                _locComboCpDqty = _locComboTotQtyFI;
                                                                            }
                                                                        }
                                                                    }
                                                                    else
                                                                    {
                                                                        _locComboCpDqty = _locComboTotQtyFI;
                                                                    }

                                                                    SalesOrderFreeItem _saveDataSalesOrderFreeItem = new SalesOrderFreeItem(_currSession)
                                                                    {
                                                                        //Organization
                                                                        Company = _locSalesOrderXPO.Company,
                                                                        Workplace = _locSalesOrderXPO.Workplace,
                                                                        Division = _locSalesOrderXPO.Division,
                                                                        Department = _locSalesOrderXPO.Department,
                                                                        Section = _locSalesOrderXPO.Section,
                                                                        Employee = _locSalesOrderXPO.Employee,
                                                                        //Item
                                                                        SalesOrder = _locSalesOrderXPO,
                                                                        Location = _locFreeItemRule.Location,
                                                                        FreeItemRule = _locFreeItemRule,
                                                                        BegInv = _locFreeItemRule.FpBegInv,
                                                                        FpLocationType = _locFreeItemRule.LocationType,
                                                                        FpStockType = _locFreeItemRule.StockType,
                                                                        FpItem = _locFreeItemRule.FpItem,
                                                                        FpDQty = _locComboCpDqty,
                                                                        FpDUOM = _locFreeItemRule.FpDUOM,
                                                                        FpQty = _locComboCpQty,
                                                                        FpUOM = _locFreeItemRule.FpUOM,
                                                                        FpTQty = _locComboTotQtyFI,
                                                                        FpStockGroup = _locFreeItemRule.FpStockGroup,
                                                                        BegInvLine = _locFreeItemRule.FpBegInvLine,
                                                                        //Amount
                                                                        Currency = _locSalesOrderXPO.Currency,
                                                                        PriceGroup = _locSalesOrderXPO.PriceGroup,
                                                                        PriceLine = GetPriceLine(_currSession, _locSalesOrderXPO, _locFreeItemRule.FpItem),
                                                                        UAmount = GetPriceLine(_currSession, _locSalesOrderXPO, _locFreeItemRule.FpItem).Amount2a,
                                                                    };
                                                                    _saveDataSalesOrderFreeItem.Save();
                                                                    _saveDataSalesOrderFreeItem.Session.CommitTransaction();

                                                                    _locComboSalesOrderLine.FreeItemChecked = true;
                                                                    _locComboSalesOrderLine.Save();
                                                                    _locComboSalesOrderLine.Session.CommitTransaction();

                                                                    _locMainSalesOrderLine.FreeItemChecked = true;
                                                                    _locMainSalesOrderLine.Save();
                                                                    _locMainSalesOrderLine.Session.CommitTransaction();

                                                                    if (_locFreeItemRule.FpBegInv != null && _locFreeItemRule.FpBegInvLine != null)
                                                                    {
                                                                        _locFreeItemRule.FpBegInvLine.QtyOnHand = _locFreeItemRule.FpBegInvLine.QtyOnHand + _locComboTotQtyFI;
                                                                        _locFreeItemRule.FpBegInvLine.Save();
                                                                        _locFreeItemRule.FpBegInvLine.Session.CommitTransaction();
                                                                    }

                                                                }
                                                            }

                                                        }
                                                        #endregion Rule4

                                                        #endregion ComboPromoItem
                                                    }
                                                    else
                                                    {
                                                        #region MainPromoItem

                                                        #region Rule1

                                                        if (_locFreeItemRule.RuleType == RuleType.Rule1)
                                                        {

                                                            if (_locFreeItemRule.MaxLoop == 0)
                                                            {
                                                                _locMaxLoop = (int)_locMainQtyFI;
                                                            }
                                                            else
                                                            {
                                                                if (_locFreeItemRule.MaxLoop > (int)_locMainQtyFI)
                                                                {
                                                                    _locMaxLoop = (int)_locMainQtyFI;
                                                                }
                                                                else
                                                                {
                                                                    _locMaxLoop = _locFreeItemRule.MaxLoop;
                                                                }

                                                            }

                                                            for (int i = 1; i <= _locMaxLoop; i++)
                                                            {
                                                                _locTotMaxLoopVal = _locTotMaxLoopVal + 1;
                                                            }

                                                            _locTotQtyFI = _locTotMaxLoopVal * _locFreeItemRule.FpTQty;

                                                            if(_locFreeItemRule.FpBegInvLine != null)
                                                            {
                                                                if((_locFreeItemRule.FpBegInvLine.QtyAvailable - _locFreeItemRule.FpBegInvLine.QtyOnHand) > 0)
                                                                {
                                                                    if ((_locFreeItemRule.FpBegInvLine.QtyAvailable - _locFreeItemRule.FpBegInvLine.QtyOnHand) < _locTotQtyFI)
                                                                    {
                                                                        _locTotQtyFI = _locFreeItemRule.FpBegInvLine.QtyAvailable - _locFreeItemRule.FpBegInvLine.QtyOnHand;
                                                                    }

                                                                    if (_locFreeItemRule.FpItem != null && _locFreeItemRule.FpDUOM != null && _locFreeItemRule.FpUOM != null)
                                                                    {
                                                                        ItemUnitOfMeasure _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                                                                (new GroupOperator(GroupOperatorType.And,
                                                                                                 new BinaryOperator("Item", _locFreeItemRule.FpItem),
                                                                                                 new BinaryOperator("UOM", _locFreeItemRule.FpUOM),
                                                                                                 new BinaryOperator("DefaultUOM", _locFreeItemRule.FpDUOM),
                                                                                                 new BinaryOperator("Active", true)));
                                                                        if (_locItemUOM != null)
                                                                        {
                                                                            if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                                                            {
                                                                                _locFpDqty = _locTotQtyFI % _locItemUOM.DefaultConversion;
                                                                                _locFpQty = System.Math.Floor(_locTotQtyFI / _locItemUOM.DefaultConversion);
                                                                            }
                                                                            else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                                                            {
                                                                                _locFpDqty = System.Math.Floor(_locTotQtyFI / _locItemUOM.DefaultConversion);
                                                                                _locFpQty = (_locTotQtyFI % _locItemUOM.DefaultConversion) * _locItemUOM.Conversion;
                                                                            }
                                                                            else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                                                            {
                                                                                _locFpDqty = _locTotQtyFI;
                                                                            }
                                                                        }
                                                                    }
                                                                    else
                                                                    {
                                                                        _locFpDqty = _locTotQtyFI;
                                                                    }

                                                                    SalesOrderFreeItem _saveDataSalesOrderFreeItem = new SalesOrderFreeItem(_currSession)
                                                                    {
                                                                        //Organization
                                                                        Company = _locSalesOrderXPO.Company,
                                                                        Workplace = _locSalesOrderXPO.Workplace,
                                                                        Division = _locSalesOrderXPO.Division,
                                                                        Department = _locSalesOrderXPO.Department,
                                                                        Section = _locSalesOrderXPO.Section,
                                                                        Employee = _locSalesOrderXPO.Employee,
                                                                        //Item
                                                                        SalesOrder = _locSalesOrderXPO,
                                                                        Location = _locFreeItemRule.Location,
                                                                        FreeItemRule = _locFreeItemRule,
                                                                        FpLocationType = _locFreeItemRule.LocationType,
                                                                        FpStockType = _locFreeItemRule.StockType,
                                                                        BegInv = _locFreeItemRule.FpBegInv,
                                                                        FpItem = _locFreeItemRule.FpItem,
                                                                        FpDQty = _locFpDqty,
                                                                        FpDUOM = _locFreeItemRule.FpDUOM,
                                                                        FpQty = _locFpQty,
                                                                        FpUOM = _locFreeItemRule.FpUOM,
                                                                        FpTQty = _locTotQtyFI,
                                                                        FpStockGroup = _locFreeItemRule.FpStockGroup,
                                                                        BegInvLine = _locFreeItemRule.FpBegInvLine,
                                                                        //Amount
                                                                        Currency = _locSalesOrderXPO.Currency,
                                                                        PriceGroup = _locSalesOrderXPO.PriceGroup,
                                                                        PriceLine = GetPriceLine(_currSession, _locSalesOrderXPO, _locFreeItemRule.FpItem),
                                                                        UAmount = GetPriceLine(_currSession, _locSalesOrderXPO, _locFreeItemRule.FpItem).Amount2a,
                                                                    };
                                                                    _saveDataSalesOrderFreeItem.Save();
                                                                    _saveDataSalesOrderFreeItem.Session.CommitTransaction();

                                                                    _locComboSalesOrderLine.FreeItemChecked = true;
                                                                    _locComboSalesOrderLine.Save();
                                                                    _locComboSalesOrderLine.Session.CommitTransaction();
                                                                    _locMainSalesOrderLine.FreeItemChecked = true;
                                                                    _locMainSalesOrderLine.Save();
                                                                    _locMainSalesOrderLine.Session.CommitTransaction();

                                                                    if (_locFreeItemRule.FpBegInv != null && _locFreeItemRule.FpBegInvLine != null)
                                                                    {
                                                                        _locFreeItemRule.FpBegInvLine.QtyOnHand = _locFreeItemRule.FpBegInvLine.QtyOnHand + _locTotQtyFI;
                                                                        _locFreeItemRule.FpBegInvLine.Save();
                                                                        _locFreeItemRule.FpBegInvLine.Session.CommitTransaction();
                                                                    }
                                                                }
                                                            }
                                                        }

                                                        #endregion Rule1

                                                        //1, 2+1, 3+1, 4+1
                                                        #region Rule2
                                                        if (_locFreeItemRule.RuleType == RuleType.Rule2)
                                                        {

                                                            if (_locFreeItemRule.MaxLoop == 0)
                                                            {
                                                                _locMaxLoop = (int)_locMainQtyFI;
                                                            }
                                                            else
                                                            {
                                                                if (_locFreeItemRule.MaxLoop > (int)_locMainQtyFI)
                                                                {
                                                                    _locMaxLoop = (int)_locMainQtyFI;
                                                                }
                                                                else
                                                                {
                                                                    _locMaxLoop = _locFreeItemRule.MaxLoop;
                                                                }
                                                            }

                                                            for (int i = 1; i <= _locMaxLoop; i++)
                                                            {

                                                                if (i == 1)
                                                                {
                                                                    _locTotQtyFI = _locFreeItemRule.FpTQty;
                                                                }
                                                                else
                                                                {
                                                                    _locTotQtyFI = _locTotQtyFI + (i + _locFreeItemRule.FpTQty);
                                                                }

                                                            }

                                                            if (_locFreeItemRule.FpBegInvLine != null)
                                                            {
                                                                if ((_locFreeItemRule.FpBegInvLine.QtyAvailable - _locFreeItemRule.FpBegInvLine.QtyOnHand) > 0)
                                                                {
                                                                    if ((_locFreeItemRule.FpBegInvLine.QtyAvailable - _locFreeItemRule.FpBegInvLine.QtyOnHand) < _locTotQtyFI)
                                                                    {
                                                                        _locTotQtyFI = _locFreeItemRule.FpBegInvLine.QtyAvailable - _locFreeItemRule.FpBegInvLine.QtyOnHand;
                                                                    }

                                                                    if (_locFreeItemRule.FpItem != null && _locFreeItemRule.FpDUOM != null && _locFreeItemRule.FpUOM != null)
                                                                    {
                                                                        ItemUnitOfMeasure _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                                                                (new GroupOperator(GroupOperatorType.And,
                                                                                                 new BinaryOperator("Item", _locFreeItemRule.FpItem),
                                                                                                 new BinaryOperator("UOM", _locFreeItemRule.FpUOM),
                                                                                                 new BinaryOperator("DefaultUOM", _locFreeItemRule.FpDUOM),
                                                                                                 new BinaryOperator("Active", true)));
                                                                        if (_locItemUOM != null)
                                                                        {
                                                                            if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                                                            {
                                                                                _locFpDqty = _locTotQtyFI % _locItemUOM.DefaultConversion;
                                                                                _locFpQty = System.Math.Floor(_locTotQtyFI / _locItemUOM.DefaultConversion);
                                                                            }
                                                                            else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                                                            {
                                                                                _locFpDqty = System.Math.Floor(_locTotQtyFI / _locItemUOM.DefaultConversion);
                                                                                _locFpQty = (_locTotQtyFI % _locItemUOM.DefaultConversion) * _locItemUOM.Conversion;
                                                                            }
                                                                            else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                                                            {
                                                                                _locFpDqty = _locTotQtyFI;
                                                                            }
                                                                        }
                                                                    }
                                                                    else
                                                                    {
                                                                        _locFpDqty = _locTotQtyFI;
                                                                    }

                                                                    SalesOrderFreeItem _saveDataSalesOrderFreeItem = new SalesOrderFreeItem(_currSession)
                                                                    {
                                                                        //Organization
                                                                        Company = _locSalesOrderXPO.Company,
                                                                        Workplace = _locSalesOrderXPO.Workplace,
                                                                        Division = _locSalesOrderXPO.Division,
                                                                        Department = _locSalesOrderXPO.Department,
                                                                        Section = _locSalesOrderXPO.Section,
                                                                        Employee = _locSalesOrderXPO.Employee,
                                                                        //Item
                                                                        SalesOrder = _locSalesOrderXPO,
                                                                        Location = _locFreeItemRule.Location,
                                                                        FreeItemRule = _locFreeItemRule,
                                                                        FpLocationType = _locFreeItemRule.LocationType,
                                                                        FpStockType = _locFreeItemRule.StockType,
                                                                        BegInv = _locFreeItemRule.FpBegInv,
                                                                        FpItem = _locFreeItemRule.FpItem,
                                                                        FpDQty = _locFpDqty,
                                                                        FpDUOM = _locFreeItemRule.FpDUOM,
                                                                        FpQty = _locFpQty,
                                                                        FpUOM = _locFreeItemRule.FpUOM,
                                                                        FpTQty = _locTotQtyFI,
                                                                        FpStockGroup = _locFreeItemRule.FpStockGroup,
                                                                        BegInvLine = _locFreeItemRule.FpBegInvLine,
                                                                        //Amount
                                                                        Currency = _locSalesOrderXPO.Currency,
                                                                        PriceGroup = _locSalesOrderXPO.PriceGroup,
                                                                        PriceLine = GetPriceLine(_currSession, _locSalesOrderXPO, _locFreeItemRule.FpItem),
                                                                        UAmount = GetPriceLine(_currSession, _locSalesOrderXPO, _locFreeItemRule.FpItem).Amount2a,
                                                                    };
                                                                    _saveDataSalesOrderFreeItem.Save();
                                                                    _saveDataSalesOrderFreeItem.Session.CommitTransaction();

                                                                    _locComboSalesOrderLine.FreeItemChecked = true;
                                                                    _locComboSalesOrderLine.Save();
                                                                    _locComboSalesOrderLine.Session.CommitTransaction();
                                                                    _locMainSalesOrderLine.FreeItemChecked = true;
                                                                    _locMainSalesOrderLine.Save();
                                                                    _locMainSalesOrderLine.Session.CommitTransaction();

                                                                    if (_locFreeItemRule.FpBegInv != null && _locFreeItemRule.FpBegInvLine != null)
                                                                    {
                                                                        _locFreeItemRule.FpBegInvLine.QtyOnHand = _locFreeItemRule.FpBegInvLine.QtyOnHand + _locTotQtyFI;
                                                                        _locFreeItemRule.FpBegInvLine.Save();
                                                                        _locFreeItemRule.FpBegInvLine.Session.CommitTransaction();
                                                                    }
                                                                }
                                                            }
                                                        }
                                                        #endregion Rule2

                                                        //2 * 2 * 2 * 2
                                                        #region Rule3
                                                        if (_locFreeItemRule.RuleType == RuleType.Rule3)
                                                        {

                                                            if (_locFreeItemRule.MaxLoop == 0)
                                                            {
                                                                _locMaxLoop = (int)_locMainQtyFI;
                                                            }
                                                            else
                                                            {
                                                                if (_locFreeItemRule.MaxLoop > (int)_locMainQtyFI)
                                                                {
                                                                    _locMaxLoop = (int)_locMainQtyFI;
                                                                }
                                                                else
                                                                {
                                                                    _locMaxLoop = _locFreeItemRule.MaxLoop;
                                                                }
                                                            }

                                                            for (int i = 1; i <= _locMaxLoop; i++)
                                                            {
                                                                _locTotQtyFI = _locTotQtyFI * _locFreeItemRule.FpTQty;
                                                            }

                                                            if (_locFreeItemRule.FpBegInvLine != null)
                                                            {
                                                                if ((_locFreeItemRule.FpBegInvLine.QtyAvailable - _locFreeItemRule.FpBegInvLine.QtyOnHand) > 0)
                                                                {
                                                                    if ((_locFreeItemRule.FpBegInvLine.QtyAvailable - _locFreeItemRule.FpBegInvLine.QtyOnHand) < _locTotQtyFI)
                                                                    {
                                                                        _locTotQtyFI = _locFreeItemRule.FpBegInvLine.QtyAvailable - _locFreeItemRule.FpBegInvLine.QtyOnHand;
                                                                    }

                                                                    if (_locFreeItemRule.FpItem != null && _locFreeItemRule.FpDUOM != null && _locFreeItemRule.FpUOM != null)
                                                                    {
                                                                        ItemUnitOfMeasure _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                                                                (new GroupOperator(GroupOperatorType.And,
                                                                                                 new BinaryOperator("Item", _locFreeItemRule.FpItem),
                                                                                                 new BinaryOperator("UOM", _locFreeItemRule.FpUOM),
                                                                                                 new BinaryOperator("DefaultUOM", _locFreeItemRule.FpDUOM),
                                                                                                 new BinaryOperator("Active", true)));
                                                                        if (_locItemUOM != null)
                                                                        {
                                                                            if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                                                            {
                                                                                _locFpDqty = _locTotQtyFI % _locItemUOM.DefaultConversion;
                                                                                _locFpQty = System.Math.Floor(_locTotQtyFI / _locItemUOM.DefaultConversion);
                                                                            }
                                                                            else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                                                            {
                                                                                _locFpDqty = System.Math.Floor(_locTotQtyFI / _locItemUOM.DefaultConversion);
                                                                                _locFpQty = (_locTotQtyFI % _locItemUOM.DefaultConversion) * _locItemUOM.Conversion;
                                                                            }
                                                                            else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                                                            {
                                                                                _locFpDqty = _locTotQtyFI;
                                                                            }
                                                                        }
                                                                    }
                                                                    else
                                                                    {
                                                                        _locFpDqty = _locTotQtyFI;
                                                                    }

                                                                    SalesOrderFreeItem _saveDataSalesOrderFreeItem = new SalesOrderFreeItem(_currSession)
                                                                    {
                                                                        //Organization
                                                                        Company = _locSalesOrderXPO.Company,
                                                                        Workplace = _locSalesOrderXPO.Workplace,
                                                                        Division = _locSalesOrderXPO.Division,
                                                                        Department = _locSalesOrderXPO.Department,
                                                                        Section = _locSalesOrderXPO.Section,
                                                                        Employee = _locSalesOrderXPO.Employee,
                                                                        //Item
                                                                        SalesOrder = _locSalesOrderXPO,
                                                                        Location = _locFreeItemRule.Location,
                                                                        FreeItemRule = _locFreeItemRule,
                                                                        FpLocationType = _locFreeItemRule.LocationType,
                                                                        FpStockType = _locFreeItemRule.StockType,
                                                                        BegInv = _locFreeItemRule.FpBegInv,
                                                                        FpItem = _locFreeItemRule.FpItem,
                                                                        FpDQty = _locFpDqty,
                                                                        FpDUOM = _locFreeItemRule.FpDUOM,
                                                                        FpQty = _locFpQty,
                                                                        FpUOM = _locFreeItemRule.FpUOM,
                                                                        FpTQty = _locTotQtyFI,
                                                                        FpStockGroup = _locFreeItemRule.FpStockGroup,
                                                                        BegInvLine = _locFreeItemRule.FpBegInvLine,
                                                                        //Amount
                                                                        Currency = _locSalesOrderXPO.Currency,
                                                                        PriceGroup = _locSalesOrderXPO.PriceGroup,
                                                                        PriceLine = GetPriceLine(_currSession, _locSalesOrderXPO, _locFreeItemRule.FpItem),
                                                                        UAmount = GetPriceLine(_currSession, _locSalesOrderXPO, _locFreeItemRule.FpItem).Amount2a,
                                                                    };
                                                                    _saveDataSalesOrderFreeItem.Save();
                                                                    _saveDataSalesOrderFreeItem.Session.CommitTransaction();

                                                                    _locComboSalesOrderLine.FreeItemChecked = true;
                                                                    _locComboSalesOrderLine.Save();
                                                                    _locComboSalesOrderLine.Session.CommitTransaction();
                                                                    _locMainSalesOrderLine.FreeItemChecked = true;
                                                                    _locMainSalesOrderLine.Save();
                                                                    _locMainSalesOrderLine.Session.CommitTransaction();

                                                                    if (_locFreeItemRule.FpBegInv != null && _locFreeItemRule.FpBegInvLine != null)
                                                                    {
                                                                        _locFreeItemRule.FpBegInvLine.QtyOnHand = _locFreeItemRule.FpBegInvLine.QtyOnHand + _locTotQtyFI;
                                                                        _locFreeItemRule.FpBegInvLine.Save();
                                                                        _locFreeItemRule.FpBegInvLine.Session.CommitTransaction();
                                                                    }
                                                                }
                                                            }
                                                        }
                                                        #endregion Rule3

                                                        //2^(1-1) + 2^(2-1) + 2^(3-1) + 2^(4-1)
                                                        #region Rule4

                                                        if (_locFreeItemRule.RuleType == RuleType.Rule4)
                                                        {
                                                            if (_locFreeItemRule.MaxLoop == 0)
                                                            {
                                                                _locMaxLoop = (int)_locMainQtyFI;
                                                            }
                                                            else
                                                            {
                                                                if (_locFreeItemRule.MaxLoop > (int)_locMainQtyFI)
                                                                {
                                                                    _locMaxLoop = (int)_locMainQtyFI;
                                                                }
                                                                else
                                                                {
                                                                    _locMaxLoop = _locFreeItemRule.MaxLoop;
                                                                }
                                                            }

                                                            for (int i = 1; i <= _locMaxLoop; i++)
                                                            {
                                                                _locTotQtyFI = _locTotQtyFI + System.Math.Pow(_locFreeItemRule.FpTQty, i - 1);
                                                            }

                                                            if (_locFreeItemRule.FpBegInvLine != null)
                                                            {
                                                                if ((_locFreeItemRule.FpBegInvLine.QtyAvailable - _locFreeItemRule.FpBegInvLine.QtyOnHand) > 0)
                                                                {
                                                                    if ((_locFreeItemRule.FpBegInvLine.QtyAvailable - _locFreeItemRule.FpBegInvLine.QtyOnHand) < _locTotQtyFI)
                                                                    {
                                                                        _locTotQtyFI = _locFreeItemRule.FpBegInvLine.QtyAvailable - _locFreeItemRule.FpBegInvLine.QtyOnHand;
                                                                    }

                                                                    if (_locFreeItemRule.FpItem != null && _locFreeItemRule.FpDUOM != null && _locFreeItemRule.FpUOM != null)
                                                                    {
                                                                        ItemUnitOfMeasure _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                                                                (new GroupOperator(GroupOperatorType.And,
                                                                                                 new BinaryOperator("Item", _locFreeItemRule.FpItem),
                                                                                                 new BinaryOperator("UOM", _locFreeItemRule.FpUOM),
                                                                                                 new BinaryOperator("DefaultUOM", _locFreeItemRule.FpDUOM),
                                                                                                 new BinaryOperator("Active", true)));
                                                                        if (_locItemUOM != null)
                                                                        {
                                                                            if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                                                            {
                                                                                _locFpDqty = _locTotQtyFI % _locItemUOM.DefaultConversion;
                                                                                _locFpQty = System.Math.Floor(_locTotQtyFI / _locItemUOM.DefaultConversion);
                                                                            }
                                                                            else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                                                            {
                                                                                _locFpDqty = System.Math.Floor(_locTotQtyFI / _locItemUOM.DefaultConversion);
                                                                                _locFpQty = (_locTotQtyFI % _locItemUOM.DefaultConversion) * _locItemUOM.Conversion;
                                                                            }
                                                                            else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                                                            {
                                                                                _locFpDqty = _locTotQtyFI;
                                                                            }
                                                                        }
                                                                    }
                                                                    else
                                                                    {
                                                                        _locFpDqty = _locTotQtyFI;
                                                                    }

                                                                    SalesOrderFreeItem _saveDataSalesOrderFreeItem = new SalesOrderFreeItem(_currSession)
                                                                    {
                                                                        //Organization
                                                                        Company = _locSalesOrderXPO.Company,
                                                                        Workplace = _locSalesOrderXPO.Workplace,
                                                                        Division = _locSalesOrderXPO.Division,
                                                                        Department = _locSalesOrderXPO.Department,
                                                                        Section = _locSalesOrderXPO.Section,
                                                                        Employee = _locSalesOrderXPO.Employee,
                                                                        //Item
                                                                        SalesOrder = _locSalesOrderXPO,
                                                                        Location = _locFreeItemRule.Location,
                                                                        FreeItemRule = _locFreeItemRule,
                                                                        FpLocationType = _locFreeItemRule.LocationType,
                                                                        FpStockType = _locFreeItemRule.StockType,
                                                                        BegInv = _locFreeItemRule.FpBegInv,
                                                                        FpItem = _locFreeItemRule.FpItem,
                                                                        FpDQty = _locFpDqty,
                                                                        FpDUOM = _locFreeItemRule.FpDUOM,
                                                                        FpQty = _locFpQty,
                                                                        FpUOM = _locFreeItemRule.FpUOM,
                                                                        FpTQty = _locTotQtyFI,
                                                                        FpStockGroup = _locFreeItemRule.FpStockGroup,
                                                                        BegInvLine = _locFreeItemRule.FpBegInvLine,
                                                                        //Amount
                                                                        Currency = _locSalesOrderXPO.Currency,
                                                                        PriceGroup = _locSalesOrderXPO.PriceGroup,
                                                                        PriceLine = GetPriceLine(_currSession, _locSalesOrderXPO, _locFreeItemRule.FpItem),
                                                                        UAmount = GetPriceLine(_currSession, _locSalesOrderXPO, _locFreeItemRule.FpItem).Amount2a,
                                                                    };
                                                                    _saveDataSalesOrderFreeItem.Save();
                                                                    _saveDataSalesOrderFreeItem.Session.CommitTransaction();

                                                                    _locComboSalesOrderLine.FreeItemChecked = true;
                                                                    _locComboSalesOrderLine.Save();
                                                                    _locComboSalesOrderLine.Session.CommitTransaction();
                                                                    _locMainSalesOrderLine.FreeItemChecked = true;
                                                                    _locMainSalesOrderLine.Save();
                                                                    _locMainSalesOrderLine.Session.CommitTransaction();

                                                                    if (_locFreeItemRule.FpBegInv != null && _locFreeItemRule.FpBegInvLine != null)
                                                                    {
                                                                        _locFreeItemRule.FpBegInvLine.QtyOnHand = _locFreeItemRule.FpBegInvLine.QtyOnHand + _locTotQtyFI;
                                                                        _locFreeItemRule.FpBegInvLine.Save();
                                                                        _locFreeItemRule.FpBegInvLine.Session.CommitTransaction();
                                                                    }
                                                                }
                                                            }
                                                        }
                                                        #endregion Rule4

                                                        #endregion MainPromoItem
                                                    }
                                                }

                                            }
                                        }
                                        #endregion MultiItem=true  
                                    }
                                }
                            }
                            #endregion Line
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = SalesOrder ", ex.ToString());
            }
        }

        private void SetTotalFreeItemAmount(Session _currSession, SalesOrder _locSalesOrderXPO)
        {
            try
            {
                double _locTUAmount = 0;
                double _locGetTUAmount = 0;
                if (_locSalesOrderXPO != null)
                {
                    XPCollection<SalesOrderFreeItem> _locSalesOrderFreeItems = new XPCollection<SalesOrderFreeItem>
                                                                                (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                new BinaryOperator("SalesOrder", _locSalesOrderXPO),
                                                                                new GroupOperator(GroupOperatorType.Or,
                                                                                new BinaryOperator("Status", Status.Open))));
                    if (_locSalesOrderFreeItems != null && _locSalesOrderFreeItems.Count() > 0)
                    {
                        foreach (SalesOrderFreeItem _locSalesOrderFreeItem in _locSalesOrderFreeItems)
                        {
                            _locGetTUAmount = GetTotalUnitAmount(_currSession, _locSalesOrderFreeItem.FpTQty, _locSalesOrderFreeItem.UAmount);
                            _locTUAmount = _locTUAmount + _locGetTUAmount;

                            _locSalesOrderFreeItem.TUAmount = _locGetTUAmount;
                            _locSalesOrderFreeItem.Save();
                            _locSalesOrderFreeItem.Session.CommitTransaction();
                        }
                        _locSalesOrderXPO.TotFIAmount = _locTUAmount;
                        _locSalesOrderXPO.Save();
                        _locSalesOrderXPO.Session.CommitTransaction();
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = SalesOrder ", ex.ToString());
            }

        }

        private PriceLine GetPriceLine(Session _currSession, SalesOrder _locSalesOrderXPO, Item _locFpItem)
        {
            PriceLine _result = null;
            try
            {
                
                if (_locSalesOrderXPO.Company != null && _locSalesOrderXPO.Workplace != null && _locFpItem != null && _locSalesOrderXPO.PriceGroup != null)
                {
                    PriceLine _locPriceLine = _currSession.FindObject<PriceLine>(new GroupOperator(GroupOperatorType.And,
                                                                            new BinaryOperator("Company", _locSalesOrderXPO.Company),
                                                                            new BinaryOperator("Workplace", _locSalesOrderXPO.Workplace),
                                                                            new BinaryOperator("Item", _locFpItem),
                                                                            new BinaryOperator("PriceGroup", _locSalesOrderXPO.PriceGroup),
                                                                            new BinaryOperator("Active", true)));
                    if(_locPriceLine != null)
                    {
                        _result = _locPriceLine;
                    }  
                }
                
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = SalesOrder " + ex.ToString());
            }
            return _result;
        }

        private double GetTotalUnitAmount(Session _currSession, double _locFpTQty, double _locUAmount)
        {
            double _result = 0;
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();

                if (_locFpTQty >= 0 & _locUAmount >= 0)
                {
                    if (_globFunc.GetRoundingList(_currSession, ObjectList.SalesOrderFreeItem, FieldName.TUAmount) == true)
                    {
                        _result = _globFunc.GetRoundUp(_currSession, (_locFpTQty * _locUAmount), ObjectList.SalesOrderFreeItem, FieldName.TUAmount);
                    }
                    else
                    {
                        _result = _locFpTQty * _locUAmount;
                    }

                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = SalesOrder " + ex.ToString());
            }
            return _result;
        }

        #endregion FreeItem

        #region Discount

        private double GetDiscountRule1(Session _currSession, SalesOrder _locSalesOrderXPO)
        {
            double _result = 0;
            try
            {
                if (_locSalesOrderXPO != null)
                {
                    if(_locSalesOrderXPO.Company != null && _locSalesOrderXPO.Workplace != null)
                    {
                        DiscountRule _locDiscountRule1 = _currSession.FindObject<DiscountRule>
                                                            (new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("Company", _locSalesOrderXPO.Company),
                                                            new BinaryOperator("Workplace", _locSalesOrderXPO.Workplace),
                                                            new BinaryOperator("PriceGroup", _locSalesOrderXPO.PriceGroup),
                                                            new BinaryOperator("RuleType", RuleType.Rule1),
                                                            new BinaryOperator("Active", true)));

                        #region Rule1

                        //Grand Total Amount Per Invoice
                        if (_locDiscountRule1 != null)
                        {
                            double _locTUAmount = 0;
                            double _locTAmount = 0;
                            double _locTUAmountDiscount = 0;
                            double _locTUAmountDiscount2 = 0;
                            AccountingPeriodicLine _locAcctPerLine = null;
           
                            #region Gross
                            if (_locDiscountRule1.GrossAmountRule == true)
                            {
                                XPCollection<SalesOrderLine> _locMainSalesOrderLines = new XPCollection<SalesOrderLine>
                                                                                               (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                               new BinaryOperator("SalesOrder", _locSalesOrderXPO),
                                                                                               new BinaryOperator("Select", true),
                                                                                               new GroupOperator(GroupOperatorType.Or,
                                                                                               new BinaryOperator("Status", Status.Open),
                                                                                               new BinaryOperator("Status", Status.Progress))));


                                if (_locMainSalesOrderLines != null && _locMainSalesOrderLines.Count() > 0)
                                {
                                    foreach (SalesOrderLine _locMainSalesOrderLine in _locMainSalesOrderLines)
                                    {
                                        if (_locMainSalesOrderLine.TUAmount > 0)
                                        {
                                            _locTUAmount = _locTUAmount + _locMainSalesOrderLine.TUAmount;
                                            _locMainSalesOrderLine.DiscountChecked = true;
                                            _locMainSalesOrderLine.Save();
                                            _locMainSalesOrderLine.Session.CommitTransaction();
                                        }
                                    }

                                    if (_locTUAmount > 0 && _locTUAmount >= _locDiscountRule1.GrossTotalUAmount)
                                    {
                                        if (_locDiscountRule1.DiscountType == DiscountType.Percentage)
                                        {
                                            _locTUAmountDiscount = _locTUAmount * _locDiscountRule1.Value / 100;
                                        }
                                        if (_locDiscountRule1.DiscountType == DiscountType.Value)
                                        {
                                            _locTUAmountDiscount = _locDiscountRule1.Value;
                                        }
                                    }
                                }

                            }
                            #endregion Gross
                            #region Net
                            if (_locDiscountRule1.NetAmountRule == true)
                            {
                                XPCollection<SalesOrderLine> _locMainSalesOrderLines = new XPCollection<SalesOrderLine>
                                                                                               (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                               new BinaryOperator("SalesOrder", _locSalesOrderXPO),
                                                                                               new BinaryOperator("Select", true),
                                                                                               new GroupOperator(GroupOperatorType.Or,
                                                                                               new BinaryOperator("Status", Status.Open),
                                                                                               new BinaryOperator("Status", Status.Progress))));

                                if (_locMainSalesOrderLines != null && _locMainSalesOrderLines.Count() > 0)
                                {
                                    foreach (SalesOrderLine _locMainSalesOrderLine in _locMainSalesOrderLines)
                                    {
                                        if (_locMainSalesOrderLine.TAmount > 0)
                                        {
                                            _locTAmount = _locTAmount + _locMainSalesOrderLine.TAmount;
                                            _locMainSalesOrderLine.DiscountChecked = true;
                                            _locMainSalesOrderLine.Save();
                                            _locMainSalesOrderLine.Session.CommitTransaction();
                                        }
                                    }

                                    if (_locTAmount > 0 && _locTAmount >= _locDiscountRule1.NetTotalUAmount)
                                    {
                                        if (_locDiscountRule1.DiscountType == DiscountType.Percentage)
                                        {
                                            _locTUAmountDiscount = _locTAmount * _locDiscountRule1.Value / 100;
                                        }
                                        if (_locDiscountRule1.DiscountType == DiscountType.Value)
                                        {
                                            _locTUAmountDiscount = _locDiscountRule1.Value;
                                        }
                                    }
                                }
                            }
                            #endregion Net

                            #region ValBasedAccount
                            if (_locDiscountRule1.ValBasedAccount == true)
                            {
                                if(_locDiscountRule1.AccountNo != null)
                                {
                                    AccountingPeriodic _locAccountPeriodic = _currSession.FindObject<AccountingPeriodic>
                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                    new BinaryOperator("Company", _locSalesOrderXPO.Company),
                                                                    new BinaryOperator("Workplace", _locSalesOrderXPO.Workplace),
                                                                    new BinaryOperator("Active", true)));
                                    if (_locAccountPeriodic != null)
                                    {
                                        AccountingPeriodicLine _locAccountingPeriodicLine = _currSession.FindObject<AccountingPeriodicLine>
                                                                                            (new GroupOperator(GroupOperatorType.And,
                                                                                            new BinaryOperator("AccountingPeriodic", _locAccountPeriodic),
                                                                                            new BinaryOperator("AccountNo", _locDiscountRule1.AccountNo)));
                                        if (_locAccountingPeriodicLine != null)
                                        {
                                            _locAcctPerLine = _locAccountingPeriodicLine;
                                            if(_locAccountingPeriodicLine.BalHold < _locAccountingPeriodicLine.Balance)
                                            {
                                                _locTUAmountDiscount2 = _locAccountingPeriodicLine.Balance - _locAccountingPeriodicLine.BalHold;
                                                if(_locTUAmountDiscount2 < _locTUAmountDiscount)
                                                {
                                                    _locTUAmountDiscount = _locTUAmountDiscount2;
                                                }
                                                
                                            }else
                                            {
                                                _locTUAmountDiscount = 0;
                                            }
                                            _locAccountingPeriodicLine.BalHold = _locAccountingPeriodicLine.BalHold + _locTUAmountDiscount;
                                            _locAccountingPeriodicLine.Save();
                                            _locAccountingPeriodicLine.Session.CommitTransaction();
                                        }
                                    }
                                }
                                
                            }
                            #endregion ValBasedAccount

                            _locSalesOrderXPO.AccountingPeriodicLine = _locAcctPerLine;
                            _locSalesOrderXPO.DiscountRule = _locDiscountRule1;
                            _locSalesOrderXPO.Save();
                            _locSalesOrderXPO.Session.CommitTransaction();

                            _result = _locTUAmountDiscount;


                        }
                        #endregion Rule1
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = SalesOrder ", ex.ToString());
            }
            return _result;
        }

        #endregion Discount

        #region Posting Method

        private double GetSalesOrderLineTotalAmount(Session _currSession, SalesOrder _locSalesOrderXPO)
        {
            double _return = 0;
            try
            {
                if (_locSalesOrderXPO != null)
                {
                    double _locTAmount = 0;
                    XPCollection<SalesOrderLine> _locSalesOrderLines = new XPCollection<SalesOrderLine>
                                                                        (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                        new BinaryOperator("SalesOrder", _locSalesOrderXPO),
                                                                        new BinaryOperator("Select", true),
                                                                        new GroupOperator(GroupOperatorType.Or,
                                                                        new BinaryOperator("Status", Status.Progress),
                                                                        new BinaryOperator("Status", Status.Posted))));

                    if (_locSalesOrderLines != null && _locSalesOrderLines.Count > 0)
                    {
                        foreach (SalesOrderLine _locSalesOrderLine in _locSalesOrderLines)
                        {
                            _locTAmount = _locTAmount + _locSalesOrderLine.TAmount;
                        }
                        _return = _locTAmount;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("BusinessObject = SalesOrder" + ex.ToString());
            }
            return _return;
        }

        private void SetSalesOrderMonitoring(Session _currSession, SalesOrder _locSalesOrderXPO)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                DateTime now = DateTime.Now;

                if (_locSalesOrderXPO != null)
                {
                    if (_locSalesOrderXPO.Status == Status.Progress || _locSalesOrderXPO.Status == Status.Posted)
                    {
                        #region SalesOrderLine
                        XPCollection<SalesOrderLine> _locSalesOrderLines = new XPCollection<SalesOrderLine>
                                                            (_currSession, new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("SalesOrder", _locSalesOrderXPO),
                                                            new BinaryOperator("Select", true),
                                                            new GroupOperator(GroupOperatorType.Or,
                                                            new BinaryOperator("Status", Status.Progress),
                                                            new BinaryOperator("Status", Status.Posted))));

                        if (_locSalesOrderLines != null && _locSalesOrderLines.Count() > 0)
                        {
                            foreach (SalesOrderLine _locSalesOrderLine in _locSalesOrderLines)
                            {
                                if (_locSalesOrderLine.TAmount > 0)
                                {
                                    SalesOrderMonitoring _saveDataSalesOrderMonitoring = new SalesOrderMonitoring(_currSession)
                                    {
                                        FreeItemChecked = _locSalesOrderLine.FreeItemChecked,
                                        DiscountChecked = _locSalesOrderLine.DiscountChecked,
                                        Company = _locSalesOrderLine.Company,
                                        Workplace = _locSalesOrderLine.Workplace,
                                        Division = _locSalesOrderLine.Division,
                                        Department = _locSalesOrderLine.Department,
                                        Section = _locSalesOrderLine.Section,
                                        Employee = _locSalesOrderLine.Employee,
                                        Div = _locSalesOrderLine.Div,
                                        Dept = _locSalesOrderLine.Dept,
                                        Sect = _locSalesOrderLine.Sect,
                                        PIC = _locSalesOrderLine.PIC,
                                        Location = _locSalesOrderLine.Location,
                                        BegInv = _locSalesOrderLine.BegInv,
                                        Item = _locSalesOrderLine.Item,
                                        BegInvLine = _locSalesOrderLine.BegInvLine,
                                        Description = _locSalesOrderLine.Description,
                                        DQty = _locSalesOrderLine.DQty,
                                        DUOM = _locSalesOrderLine.DUOM,
                                        Qty = _locSalesOrderLine.Qty,
                                        UOM = _locSalesOrderLine.UOM,
                                        TQty = _locSalesOrderLine.TQty,
                                        StockType = _locSalesOrderLine.StockType,
                                        StockGroup = StockGroup.Normal,
                                        Currency = _locSalesOrderLine.Currency,
                                        PriceGroup = _locSalesOrderLine.PriceGroup,
                                        PriceLine = _locSalesOrderLine.PriceLine,
                                        UAmount = _locSalesOrderLine.UAmount,
                                        TUAmount = _locSalesOrderLine.TUAmount,
                                        Tax = _locSalesOrderLine.Tax,
                                        TxValue = _locSalesOrderLine.TxValue,
                                        TxAmount = _locSalesOrderLine.TxAmount,
                                        DiscountRule = _locSalesOrderLine.DiscountRule,
                                        AccountingPeriodicLine = _locSalesOrderLine.AccountingPeriodicLine,
                                        DiscAmount = _locSalesOrderLine.DiscAmount,
                                        TAmount = _locSalesOrderLine.TAmount,
                                        ETD = _locSalesOrderLine.ETD,
                                        ETA = _locSalesOrderLine.ETA,
                                        SalesOrder = _locSalesOrderXPO,
                                        SalesOrderLine = _locSalesOrderLine,
                                    };
                                    _saveDataSalesOrderMonitoring.Save();
                                    _saveDataSalesOrderMonitoring.Session.CommitTransaction();
                                }
                            }
                        }
                        #endregion SalesOrderLine
                        #region SalesOrderFreeItem
                        XPCollection<SalesOrderFreeItem> _locSalesOrderFreeItems = new XPCollection<SalesOrderFreeItem>(_currSession,
                                                                            new GroupOperator(GroupOperatorType.And,
                                                                            new BinaryOperator("SalesOrder", _locSalesOrderXPO)));
                        if(_locSalesOrderFreeItems != null && _locSalesOrderFreeItems.Count() > 0)
                        {
                            foreach(SalesOrderFreeItem _locSalesOrderFreeItem in _locSalesOrderFreeItems)
                            {
                                SalesOrderMonitoring _saveDataSalesOrderMonitoring = new SalesOrderMonitoring(_currSession)
                                {
                                    Company = _locSalesOrderXPO.Company,
                                    Workplace = _locSalesOrderXPO.Workplace,
                                    Division = _locSalesOrderFreeItem.Division,
                                    Department = _locSalesOrderFreeItem.Department,
                                    Section = _locSalesOrderFreeItem.Section,
                                    Employee = _locSalesOrderFreeItem.Employee,
                                    Div = _locSalesOrderFreeItem.Div,
                                    Dept = _locSalesOrderFreeItem.Dept,
                                    Sect = _locSalesOrderFreeItem.Sect,
                                    PIC = _locSalesOrderFreeItem.PIC,
                                    Location = _locSalesOrderFreeItem.Location,
                                    BegInv = _locSalesOrderFreeItem.BegInv,
                                    Item = _locSalesOrderFreeItem.FpItem,
                                    BegInvLine = _locSalesOrderFreeItem.BegInvLine,
                                    Description = _locSalesOrderFreeItem.FpItem.Description,
                                    DQty = _locSalesOrderFreeItem.FpDQty,
                                    DUOM = _locSalesOrderFreeItem.FpDUOM,
                                    Qty = _locSalesOrderFreeItem.FpQty,
                                    UOM = _locSalesOrderFreeItem.FpUOM,
                                    TQty = _locSalesOrderFreeItem.FpTQty,
                                    StockType = _locSalesOrderFreeItem.FpStockType,
                                    StockGroup = _locSalesOrderFreeItem.FpStockGroup,
                                    Currency = _locSalesOrderFreeItem.Currency,
                                    PriceGroup = _locSalesOrderFreeItem.PriceGroup,
                                    PriceLine = _locSalesOrderFreeItem.PriceLine,
                                    UAmount = _locSalesOrderFreeItem.UAmount,
                                    TUAmount = _locSalesOrderFreeItem.TUAmount,
                                    ETD = _locSalesOrderXPO.ETD,
                                    ETA = _locSalesOrderXPO.ETA,
                                    SalesOrder = _locSalesOrderXPO,
                                    SalesOrderFreeItem = _locSalesOrderFreeItem,
                                    FreeItemRule = _locSalesOrderFreeItem.FreeItemRule,
                                };
                                _saveDataSalesOrderMonitoring.Save();
                                _saveDataSalesOrderMonitoring.Session.CommitTransaction();
                            }
                        }
                        #endregion SalesOrderFreeItem
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = SalesOrder ", ex.ToString());
            }
        }

        private void SetStatusSalesOrderLine(Session _currSession, SalesOrder _locSalesOrderXPO)
        {
            try
            {
                DateTime now = DateTime.Now;
                if (_locSalesOrderXPO != null)
                {
                    XPCollection<SalesOrderLine> _locSalesOrderLines = new XPCollection<SalesOrderLine>(_currSession,
                                                            new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("SalesOrder", _locSalesOrderXPO),
                                                            new BinaryOperator("Select", true),
                                                            new GroupOperator(GroupOperatorType.Or,
                                                            new BinaryOperator("Status", Status.Progress),
                                                            new BinaryOperator("Status", Status.Posted))));

                    if (_locSalesOrderLines != null && _locSalesOrderLines.Count > 0)
                    {

                        foreach (SalesOrderLine _locSalesOrderLine in _locSalesOrderLines)
                        {
                            _locSalesOrderLine.Status = Status.Close;
                            _locSalesOrderLine.ActivationPosting = true;
                            _locSalesOrderLine.StatusDate = now;
                            _locSalesOrderLine.Save();
                            _locSalesOrderLine.Session.CommitTransaction();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = SalesOrder " + ex.ToString());
            }
        }

        private void SetStatusSalesOrderFreeItem(Session _currSession, SalesOrder _locSalesOrderXPO)
        {
            try
            {
                DateTime now = DateTime.Now;
                if (_locSalesOrderXPO != null)
                {
                    XPCollection<SalesOrderFreeItem> _locSalesOrderFreeItems = new XPCollection<SalesOrderFreeItem>(_currSession,
                                                            new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("SalesOrder", _locSalesOrderXPO),
                                                            new GroupOperator(GroupOperatorType.Or,
                                                            new BinaryOperator("Status", Status.Open),
                                                            new BinaryOperator("Status", Status.Progress),
                                                            new BinaryOperator("Status", Status.Posted))));

                    if (_locSalesOrderFreeItems != null && _locSalesOrderFreeItems.Count > 0)
                    {

                        foreach (SalesOrderFreeItem _locSalesOrderFreeItem in _locSalesOrderFreeItems)
                        {
                            _locSalesOrderFreeItem.Status = Status.Close;
                            _locSalesOrderFreeItem.StatusDate = now;
                            _locSalesOrderFreeItem.Save();
                            _locSalesOrderFreeItem.Session.CommitTransaction();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = SalesOrder " + ex.ToString());
            }
        }

        private void SetFinalSalesOrder(Session _currSession, SalesOrder _locSalesOrderXPO)
        {
            try
            {
                DateTime now = DateTime.Now;
                int _locStatusCount = 0;
                int _locInvCanvLineCount = 0;

                if (_locSalesOrderXPO != null)
                {

                    XPCollection<SalesOrderLine> _locSalesOrderLines = new XPCollection<SalesOrderLine>(_currSession,
                                                            new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("SalesOrder", _locSalesOrderXPO)));

                    if (_locSalesOrderLines != null && _locSalesOrderLines.Count() > 0)
                    {
                        _locInvCanvLineCount = _locSalesOrderLines.Count();

                        foreach (SalesOrderLine _locSalesOrderLine in _locSalesOrderLines)
                        {
                            if (_locSalesOrderLine.Status == Status.Close)
                            {
                                _locStatusCount = _locStatusCount + 1;
                            }
                        }
                    }

                    if (_locStatusCount == _locInvCanvLineCount)
                    {
                        _locSalesOrderXPO.ActivationPosting = true;
                        _locSalesOrderXPO.Status = Status.Posted;
                        _locSalesOrderXPO.StatusDate = now;
                        _locSalesOrderXPO.PostedCount = _locSalesOrderXPO.PostedCount + 1;
                        _locSalesOrderXPO.Save();
                        _locSalesOrderXPO.Session.CommitTransaction();
                    }
                    else
                    {
                        _locSalesOrderXPO.Status = Status.Posted;
                        _locSalesOrderXPO.StatusDate = now;
                        _locSalesOrderXPO.PostedCount = _locSalesOrderXPO.PostedCount + 1;
                        _locSalesOrderXPO.Save();
                        _locSalesOrderXPO.Session.CommitTransaction();
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = InvoiceCanvassing " + ex.ToString());
            }
        }

        #endregion Posting Method

        #region Global Method

        private void SuccessMessageShow(string _locActionName)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 2000;
            options.Message = string.Format("{0} !", _locActionName);
            options.Type = InformationType.Success;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Success";
            options.Win.Type = WinMessageType.Flyout;
            Application.ShowViewStrategy.ShowMessage(options);
        }

        private void ErrorMessageShow(string _locMessage)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 2000;
            options.Message = string.Format(_locMessage);
            options.Type = InformationType.Warning;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Error";
            options.Win.Type = WinMessageType.Alert;
            Application.ShowViewStrategy.ShowMessage(options);
        }

        #endregion Global Method

        #region Approval Process

        private void SetApprovalLine(Session _currentSession, SalesOrder _locSalesOrderXPO, ApprovalLevel _locApprovalLevel)
        {
            try
            {
                DateTime now = DateTime.Now;

                ApprovalLine _locApprovalLineXPO = _currentSession.FindObject<ApprovalLine>
                                                                        (new GroupOperator(GroupOperatorType.And,
                                                                         new BinaryOperator("SalesOrder", _locSalesOrderXPO),
                                                                         new BinaryOperator("ApprovalLevel", _locApprovalLevel)));

                if (_locApprovalLineXPO == null)
                {
                    ApprovalLine _saveDataAL2 = new ApprovalLine(_currentSession)
                    {
                        ApprovalDate = now,
                        ApprovalStatus = Status.Approved,
                        ApprovalLevel = _locApprovalLevel,
                        SalesOrder = _locSalesOrderXPO,
                    };
                    _saveDataAL2.Save();
                    _saveDataAL2.Session.CommitTransaction();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = SalesOrder " + ex.ToString());
            }
        }

        #endregion Approval Process

        #region Email

        private string BackgroundBody(Session _currentSession, SalesOrder _locSalesOrderXPO, MailSetupDetail _locMailSetDetail)
        {
            string body = string.Empty;
            if(_locSalesOrderXPO != null)
            {
                ApprovalLine _locApprovalLineXPO = _currentSession.FindObject<ApprovalLine>
                                               (new GroupOperator(GroupOperatorType.And,
                                                new BinaryOperator("SalesOrder", _locSalesOrderXPO)));

                string Status = _locApprovalLineXPO.ApprovalStatus.ToString();

                using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/Email/EmailTemplate.html")))
                {
                    body = reader.ReadToEnd();
                }
                body = body.Replace("{Code}", _locSalesOrderXPO.Code);

                #region Level
                if (_locSalesOrderXPO.ActiveApproved1 == true)
                {
                    string Active = "Level 1";
                    body = body.Replace("{Level}", Active);
                }
                else if (_locSalesOrderXPO.ActiveApproved2 == true)
                {
                    string Active = "Level 2";
                    body = body.Replace("{Level}", Active);
                }
                else if (_locSalesOrderXPO.ActiveApproved3 == true)
                {
                    string Active = "Level 3";
                    body = body.Replace("{Level}", Active);
                }
                else
                {
                    string Active = "Not Available";
                    body = body.Replace("{Level}", Active);
                }
                #endregion Level

                body = body.Replace("{Status}", Status);
                body = body.Replace("{Message}", _locMailSetDetail.MessageBody);  
            }
            return body;
        }

        protected void SendEmail(Session _currentSession, SalesOrder _locSalesOrderXPO, Status _locStatus, ApprovalLine _locApprovalLineXPO2)
        {
            DateTime now = DateTime.Now;
            var User = SecuritySystem.CurrentUserName;
            GlobalFunction _globFunc = new GlobalFunction();
            UserAccess _numLineUserAccess = _currentSession.FindObject<UserAccess>(new BinaryOperator("UserName", User));
            string _locMessageBody = null;

            try
            {
                if (_locSalesOrderXPO != null)
                {
                    XPCollection<MailSetupDetail> _locMailSetDetails = new XPCollection<MailSetupDetail>
                                                                       (_currentSession, new GroupOperator(GroupOperatorType.And,
                                                                        new BinaryOperator("ObjectList", ObjectList.SalesOrder),
                                                                        new BinaryOperator("UserAccess", _numLineUserAccess),
                                                                        new BinaryOperator("Active", true)));

                    if (_locMailSetDetails != null && _locMailSetDetails.Count > 0)
                    {
                        foreach (MailSetupDetail _locMailSetDetail in _locMailSetDetails)
                        {
                            if (_locMailSetDetail.MessageBody != null)
                            {
                                if (_locApprovalLineXPO2.EndApproval == true)
                                {
                                    _locMessageBody = this.BackgroundBody(_currentSession, _locSalesOrderXPO, _locMailSetDetail);
                                }
                                else
                                {
                                    _locMessageBody = this.BackgroundBody(_currentSession, _locSalesOrderXPO, _locMailSetDetail);
                                }
                            }

                            _globFunc.SetAndSendMail(_locMailSetDetail.SmtpHost, _locMailSetDetail.SmtpPort,
                            _locMailSetDetail.MailFrom, _locMailSetDetail.MailFromPassword, _locMailSetDetail.MailTo, _locMailSetDetail.MailSubject, _locMessageBody);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = SalesOrder " + ex.ToString());
            }
        }


        #endregion Email

        
    }
}
