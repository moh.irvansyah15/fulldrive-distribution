﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;

using FullDrive.Module.BusinessObjects;
using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.Xpo;
using System.Collections;
using DevExpress.Xpo;

namespace FullDrive.Module.Controllers
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class PurchasePrePaymentInvoiceMonitoringActionController : ViewController
    {
        public PurchasePrePaymentInvoiceMonitoringActionController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.
        }

        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
        }

        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }

        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        private void PurchasePrePaymentInvoiceMonitoringSelectAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;

                if (_objectToProcess != null)
                {
                    foreach (Object obj in _objectToProcess)
                    {
                        PurchasePrePaymentInvoiceMonitoring _locPurchasePrePaymentInvoiceMonitoringOS = (PurchasePrePaymentInvoiceMonitoring)_objectSpace.GetObject(obj);

                        if (_locPurchasePrePaymentInvoiceMonitoringOS != null)
                        {
                            if (_locPurchasePrePaymentInvoiceMonitoringOS.Session != null)
                            {
                                _currSession = _locPurchasePrePaymentInvoiceMonitoringOS.Session;
                            }

                            if (_locPurchasePrePaymentInvoiceMonitoringOS.Code != null && _currSession != null)
                            {
                                _currObjectId = _locPurchasePrePaymentInvoiceMonitoringOS.Code;

                                XPCollection<PurchasePrePaymentInvoiceMonitoring> _locPurchasePrePaymentInvoiceMonitorings = new XPCollection<PurchasePrePaymentInvoiceMonitoring>
                                                                                                     (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                      new BinaryOperator("Code", _currObjectId),
                                                                                                      new GroupOperator(GroupOperatorType.Or,
                                                                                                      new BinaryOperator("Status", Status.Progress),
                                                                                                      new BinaryOperator("Status", Status.Open)
                                                                                                      )));

                                if (_locPurchasePrePaymentInvoiceMonitorings != null && _locPurchasePrePaymentInvoiceMonitorings.Count > 0)
                                {
                                    foreach (PurchasePrePaymentInvoiceMonitoring _locPurchasePrePaymentInvoiceMonitoring in _locPurchasePrePaymentInvoiceMonitorings)
                                    {
                                        _locPurchasePrePaymentInvoiceMonitoring.Select = true;
                                        _locPurchasePrePaymentInvoiceMonitoring.Save();
                                        _locPurchasePrePaymentInvoiceMonitoring.Session.CommitTransaction();
                                    }

                                    SuccessMessageShow("Purchase Pre Payment Invoice Monitoring has been Select");
                                }
                                else
                                {
                                    ErrorMessageShow("Data Purchase Pre Payment Invoice Monitoring Not Available");
                                }
                            }
                        }
                    }
                    if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                    {
                        _objectSpace.CommitChanges();
                        _objectSpace.Refresh();
                    }
                    if (View is ListView)
                    {
                        _objectSpace.CommitChanges();
                        View.ObjectSpace.Refresh();
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("BusinessObject = PurchasePrePaymentInvoiceMonitoring" + ex.ToString());
            }
        }

        private void PurchasePrePaymentInvoiceMonitoringUnselectAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;

                if (_objectToProcess != null)
                {
                    foreach (Object obj in _objectToProcess)
                    {
                        PurchasePrePaymentInvoiceMonitoring _locPurchasePrePaymentInvoiceMonitoringOS = (PurchasePrePaymentInvoiceMonitoring)_objectSpace.GetObject(obj);

                        if (_locPurchasePrePaymentInvoiceMonitoringOS != null)
                        {
                            if (_locPurchasePrePaymentInvoiceMonitoringOS.Session != null)
                            {
                                _currSession = _locPurchasePrePaymentInvoiceMonitoringOS.Session;
                            }

                            if (_locPurchasePrePaymentInvoiceMonitoringOS.Code != null && _currSession != null)
                            {
                                _currObjectId = _locPurchasePrePaymentInvoiceMonitoringOS.Code;

                                XPCollection<PurchasePrePaymentInvoiceMonitoring> _locPurchasePrePaymentInvoiceMonitorings = new XPCollection<PurchasePrePaymentInvoiceMonitoring>
                                                                                                     (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                      new BinaryOperator("Code", _currObjectId),
                                                                                                      new GroupOperator(GroupOperatorType.Or,
                                                                                                      new BinaryOperator("Status", Status.Progress),
                                                                                                      new BinaryOperator("Status", Status.Open)
                                                                                                      )));

                                if (_locPurchasePrePaymentInvoiceMonitorings != null && _locPurchasePrePaymentInvoiceMonitorings.Count > 0)
                                {
                                    foreach (PurchasePrePaymentInvoiceMonitoring _locPurchasePrePaymentInvoiceMonitoring in _locPurchasePrePaymentInvoiceMonitorings)
                                    {
                                        _locPurchasePrePaymentInvoiceMonitoring.Select = false;
                                        _locPurchasePrePaymentInvoiceMonitoring.Save();
                                        _locPurchasePrePaymentInvoiceMonitoring.Session.CommitTransaction();
                                    }

                                    SuccessMessageShow("Purchase Pre Payment Invoice Monitoring has been Select");
                                }
                                else
                                {
                                    ErrorMessageShow("Data Purchase Pre Payment Invoice Monitoring Not Available");
                                }
                            }
                        }
                    }
                    if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                    {
                        _objectSpace.CommitChanges();
                        _objectSpace.Refresh();
                    }
                    if (View is ListView)
                    {
                        _objectSpace.CommitChanges();
                        View.ObjectSpace.Refresh();
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("BusinessObject = PurchasePrePaymentInvoiceMonitoring" + ex.ToString());
            }
        }

        private void PurchasePrePaymentInvoiceMonitoringCancelPayableAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;

                if (_objectToProcess != null)
                {
                    foreach (Object obj in _objectToProcess)
                    {
                        PurchasePrePaymentInvoiceMonitoring _locPurchasePrePaymentInvoiceMonitoringOS = (PurchasePrePaymentInvoiceMonitoring)_objectSpace.GetObject(obj);

                        if (_locPurchasePrePaymentInvoiceMonitoringOS != null)
                        {
                            if (_locPurchasePrePaymentInvoiceMonitoringOS.Session != null)
                            {
                                _currSession = _locPurchasePrePaymentInvoiceMonitoringOS.Session;
                            }

                            if (_locPurchasePrePaymentInvoiceMonitoringOS.Code != null && _currSession != null)
                            {
                                _currObjectId = _locPurchasePrePaymentInvoiceMonitoringOS.Code;

                                XPCollection<PurchasePrePaymentInvoiceMonitoring> _locPurchasePrePaymentInvoiceMonitorings = new XPCollection<PurchasePrePaymentInvoiceMonitoring>
                                                                                                     (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                      new BinaryOperator("Code", _currObjectId),
                                                                                                      new GroupOperator(GroupOperatorType.Or,
                                                                                                      new BinaryOperator("Status", Status.Progress),
                                                                                                      new BinaryOperator("Status", Status.Open)
                                                                                                      )));

                                if (_locPurchasePrePaymentInvoiceMonitorings != null && _locPurchasePrePaymentInvoiceMonitorings.Count > 0)
                                {
                                    foreach (PurchasePrePaymentInvoiceMonitoring _locPurchasePrePaymentInvoiceMonitoring in _locPurchasePrePaymentInvoiceMonitorings)
                                    {
                                        _locPurchasePrePaymentInvoiceMonitoring.PayableTransaction = null;
                                        _locPurchasePrePaymentInvoiceMonitoring.Save();
                                        _locPurchasePrePaymentInvoiceMonitoring.Session.CommitTransaction();
                                    }

                                    SuccessMessageShow("Purchase Pre Payment Invoice Monitoring has been Select");
                                }
                                else
                                {
                                    ErrorMessageShow("Data Purchase Pre Payment Invoice Monitoring Not Available");
                                }
                            }
                        }
                    }
                    if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                    {
                        _objectSpace.CommitChanges();
                        _objectSpace.Refresh();
                    }
                    if (View is ListView)
                    {
                        _objectSpace.CommitChanges();
                        View.ObjectSpace.Refresh();
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("BusinessObject = PurchasePrePaymentInvoiceMonitoring" + ex.ToString());
            }
        }

        //=============================================== Code In Here ===============================================

        #region Global Method

        private void SuccessMessageShow(string _locActionName)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 2000;
            options.Message = string.Format("{0}", _locActionName);
            options.Type = InformationType.Success;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Success";
            options.Win.Type = WinMessageType.Flyout;
            Application.ShowViewStrategy.ShowMessage(options);
        }

        private void ErrorMessageShow(string _locMessage)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 2000;
            options.Message = string.Format(_locMessage);
            options.Type = InformationType.Warning;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Error";
            options.Win.Type = WinMessageType.Alert;
            Application.ShowViewStrategy.ShowMessage(options);
        }

        #endregion Global Method

    }
}
