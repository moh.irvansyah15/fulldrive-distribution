﻿namespace FullDrive.Module.Controllers
{
    partial class FreeItemRuleActionController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.FreeItemRuleActiveAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.FreeItemRuleNotActiveAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            // 
            // FreeItemRuleActiveAction
            // 
            this.FreeItemRuleActiveAction.Caption = "Active";
            this.FreeItemRuleActiveAction.ConfirmationMessage = null;
            this.FreeItemRuleActiveAction.Id = "FreeItemRuleActiveActionId";
            this.FreeItemRuleActiveAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.FreeItemRule);
            this.FreeItemRuleActiveAction.ToolTip = null;
            this.FreeItemRuleActiveAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.FreeItemRuleActiveAction_Execute);
            // 
            // FreeItemRuleNotActiveAction
            // 
            this.FreeItemRuleNotActiveAction.Caption = "Not Active";
            this.FreeItemRuleNotActiveAction.ConfirmationMessage = null;
            this.FreeItemRuleNotActiveAction.Id = "FreeItemRuleNotActiveActionId";
            this.FreeItemRuleNotActiveAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.FreeItemRule);
            this.FreeItemRuleNotActiveAction.ToolTip = null;
            this.FreeItemRuleNotActiveAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.FreeItemRuleNotActiveAction_Execute);
            // 
            // FreeItemRuleActionController
            // 
            this.Actions.Add(this.FreeItemRuleActiveAction);
            this.Actions.Add(this.FreeItemRuleNotActiveAction);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SimpleAction FreeItemRuleActiveAction;
        private DevExpress.ExpressApp.Actions.SimpleAction FreeItemRuleNotActiveAction;
    }
}
