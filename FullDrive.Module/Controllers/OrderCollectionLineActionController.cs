﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;

using FullDrive.Module.BusinessObjects;
using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.Xpo;
using System.Collections;
using DevExpress.Xpo;

namespace FullDrive.Module.Controllers
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class OrderCollectionLineActionController : ViewController
    {
        private ChoiceActionItem _selectionCollectStatus;

        public OrderCollectionLineActionController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.
            #region DeliveryStatus
            OrderCollectionLineCollectStatusAction.Items.Clear();
            foreach (object _currApproval in Enum.GetValues(typeof(CustomProcess.CollectStatus)))
            {
                EnumDescriptor _ed = new EnumDescriptor(typeof(CustomProcess.CollectStatus));
                _selectionCollectStatus = new ChoiceActionItem(_ed.GetCaption(_currApproval), _currApproval);
                OrderCollectionLineCollectStatusAction.Items.Add(_selectionCollectStatus);
            }
            #endregion DeliveryStatus
        }
        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }
        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        private void OrderCollectionLineSelectAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;

                if (_objectToProcess != null)
                {
                    foreach (Object obj in _objectToProcess)
                    {
                        OrderCollectionLine _locOrderCollectionLineOS = (OrderCollectionLine)_objectSpace.GetObject(obj);

                        if (_locOrderCollectionLineOS != null)
                        {
                            if (_locOrderCollectionLineOS.Session != null)
                            {
                                _currSession = _locOrderCollectionLineOS.Session;
                            }

                            if (_locOrderCollectionLineOS.Code != null && _currSession != null)
                            {
                                _currObjectId = _locOrderCollectionLineOS.Code;

                                XPCollection<OrderCollectionLine> _locOrderCollectionLines = new XPCollection<OrderCollectionLine>
                                                                                                     (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                      new BinaryOperator("Code", _currObjectId),
                                                                                                      new GroupOperator(GroupOperatorType.Or,
                                                                                                      new BinaryOperator("Status", Status.Progress),
                                                                                                      new BinaryOperator("Status", Status.Open)
                                                                                                      )));

                                if (_locOrderCollectionLines != null && _locOrderCollectionLines.Count > 0)
                                {
                                    foreach (OrderCollectionLine _locOrderCollectionLine in _locOrderCollectionLines)
                                    {
                                        _locOrderCollectionLine.Select = true;
                                        _locOrderCollectionLine.Save();
                                        _locOrderCollectionLine.Session.CommitTransaction();
                                    }

                                    SuccessMessageShow("OrderCollectionLine has been Select");
                                }
                                else
                                {
                                    ErrorMessageShow("Data OrderCollectionLine Not Available");
                                }
                            }
                        }
                    }
                    if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                    {
                        _objectSpace.CommitChanges();
                        _objectSpace.Refresh();
                    }
                    if (View is ListView)
                    {
                        _objectSpace.CommitChanges();
                        View.ObjectSpace.Refresh();
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("BusinessObject = OrderCollectionLine" + ex.ToString());
            }
        }

        private void OrderCollectionLineUnselectAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;

                if (_objectToProcess != null)
                {
                    foreach (Object obj in _objectToProcess)
                    {
                        OrderCollectionLine _locOrderCollectionLineOS = (OrderCollectionLine)_objectSpace.GetObject(obj);

                        if (_locOrderCollectionLineOS != null)
                        {
                            if (_locOrderCollectionLineOS.Session != null)
                            {
                                _currSession = _locOrderCollectionLineOS.Session;
                            }

                            if (_locOrderCollectionLineOS.Code != null && _currSession != null)
                            {
                                _currObjectId = _locOrderCollectionLineOS.Code;

                                XPCollection<OrderCollectionLine> _locOrderCollectionLines = new XPCollection<OrderCollectionLine>
                                                                                                     (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                      new BinaryOperator("Code", _currObjectId),
                                                                                                      new GroupOperator(GroupOperatorType.Or,
                                                                                                      new BinaryOperator("Status", Status.Progress),
                                                                                                      new BinaryOperator("Status", Status.Open)
                                                                                                      )));

                                if (_locOrderCollectionLines != null && _locOrderCollectionLines.Count > 0)
                                {
                                    foreach (OrderCollectionLine _locOrderCollectionLine in _locOrderCollectionLines)
                                    {
                                        _locOrderCollectionLine.Select = false;
                                        _locOrderCollectionLine.Save();
                                        _locOrderCollectionLine.Session.CommitTransaction();
                                    }

                                    SuccessMessageShow("OrderCollectionLine has been Unselect");
                                }
                                else
                                {
                                    ErrorMessageShow("Data OrderCollectionLine Not Available");
                                }
                            }
                        }
                    }
                    if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                    {
                        _objectSpace.CommitChanges();
                        _objectSpace.Refresh();
                    }
                    if (View is ListView)
                    {
                        _objectSpace.CommitChanges();
                        View.ObjectSpace.Refresh();
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("BusinessObject = OrderCollectionLine" + ex.ToString());
            }
        }

        private void OrderCollectionLineCollectStatusAction_Execute(object sender, SingleChoiceActionExecuteEventArgs e)
        {
            try
            {
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currentSession = null;
                string _currObjectId = null;

                foreach (Object obj in _objectsToProcess)
                {
                    OrderCollectionLine _objInNewObjectSpace = (OrderCollectionLine)_objectSpace.GetObject(obj);

                    if (_objInNewObjectSpace != null)
                    {
                        if (_objInNewObjectSpace.Code != null)
                        {
                            _currObjectId = _objInNewObjectSpace.Code;
                        }
                        if (_objInNewObjectSpace.Session != null)
                        {
                            _currentSession = _objInNewObjectSpace.Session;
                        }
                    }

                    if (_currObjectId != null && _currentSession != null)
                    {
                        CollectStatus _locCollectStatus = CollectStatus.None;

                        OrderCollectionLine _locOrderCollectionLineXPO = _currentSession.FindObject<OrderCollectionLine>
                                                                        (new GroupOperator(GroupOperatorType.And,
                                                                         new BinaryOperator("Code", _currObjectId),
                                                                         new GroupOperator(GroupOperatorType.Or,
                                                                         new BinaryOperator("Status", Status.Open),
                                                                         new BinaryOperator("Status", Status.Progress),
                                                                         new BinaryOperator("Status", Status.Posted))));

                        if (_locOrderCollectionLineXPO != null)
                        {
                            _locCollectStatus = (CollectStatus)e.SelectedChoiceActionItem.Data;
                            
                            _locOrderCollectionLineXPO.CollectStatus = _locCollectStatus;
                            _locOrderCollectionLineXPO.CollectStatusDate = now;                            
                            _locOrderCollectionLineXPO.Save();
                            _locOrderCollectionLineXPO.Session.CommitTransaction();
                            SuccessMessageShow("OrderCollectionLine has been Change CollectStatus");
                        }
                        else
                        {
                            ErrorMessageShow("OrderCollectionLine Not Available");
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = OrderCollectionLine " + ex.ToString());
            }
        }

        //======================================== Code In Here ========================================

        #region Global Method

        private void SuccessMessageShow(string _locActionName)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 2000;
            options.Message = string.Format("{0}", _locActionName);
            options.Type = InformationType.Success;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Success";
            options.Win.Type = WinMessageType.Flyout;
            Application.ShowViewStrategy.ShowMessage(options);
        }

        private void ErrorMessageShow(string _locMessage)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 2000;
            options.Message = string.Format(_locMessage);
            options.Type = InformationType.Warning;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Error";
            options.Win.Type = WinMessageType.Alert;
            Application.ShowViewStrategy.ShowMessage(options);
        }


        #endregion Global Method

        
    }
}
