﻿namespace FullDrive.Module.Controllers
{
    partial class RoutePlanActionController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.RoutePlanSelectAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.RoutePlanUnselectAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            // 
            // RoutePlanSelectAction
            // 
            this.RoutePlanSelectAction.Caption = "Select";
            this.RoutePlanSelectAction.ConfirmationMessage = null;
            this.RoutePlanSelectAction.Id = "RoutePlanSelectActionId";
            this.RoutePlanSelectAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.RoutePlan);
            this.RoutePlanSelectAction.ToolTip = null;
            this.RoutePlanSelectAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.RoutePlanSelectAction_Execute);
            // 
            // RoutePlanUnselectAction
            // 
            this.RoutePlanUnselectAction.Caption = "Unselect";
            this.RoutePlanUnselectAction.ConfirmationMessage = null;
            this.RoutePlanUnselectAction.Id = "RoutePlanUnselectActionId";
            this.RoutePlanUnselectAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.RoutePlan);
            this.RoutePlanUnselectAction.ToolTip = null;
            this.RoutePlanUnselectAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.RoutePlanUnselectAction_Execute);
            // 
            // RoutePlanActionController
            // 
            this.Actions.Add(this.RoutePlanSelectAction);
            this.Actions.Add(this.RoutePlanUnselectAction);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SimpleAction RoutePlanSelectAction;
        private DevExpress.ExpressApp.Actions.SimpleAction RoutePlanUnselectAction;
    }
}
