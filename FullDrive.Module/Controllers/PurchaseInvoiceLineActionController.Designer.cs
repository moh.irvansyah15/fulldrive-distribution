﻿namespace FullDrive.Module.Controllers
{
    partial class PurchaseInvoiceLineActionController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.PurchaseInvoiceLineListviewFilterSelectionAction = new DevExpress.ExpressApp.Actions.SingleChoiceAction(this.components);
            this.PurchaseInvoiceLineSelectAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.PurchaseInvoiceLineUnselectAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.PurchaseInvoiceLineTaxAndTotalAmountAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            // 
            // PurchaseInvoiceLineListviewFilterSelectionAction
            // 
            this.PurchaseInvoiceLineListviewFilterSelectionAction.Caption = "Filter";
            this.PurchaseInvoiceLineListviewFilterSelectionAction.ConfirmationMessage = null;
            this.PurchaseInvoiceLineListviewFilterSelectionAction.Id = "PurchaseInvoiceLineListviewFilterSelectionActionId";
            this.PurchaseInvoiceLineListviewFilterSelectionAction.ItemType = DevExpress.ExpressApp.Actions.SingleChoiceActionItemType.ItemIsOperation;
            this.PurchaseInvoiceLineListviewFilterSelectionAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.PurchaseInvoiceLine);
            this.PurchaseInvoiceLineListviewFilterSelectionAction.ToolTip = null;
            this.PurchaseInvoiceLineListviewFilterSelectionAction.Execute += new DevExpress.ExpressApp.Actions.SingleChoiceActionExecuteEventHandler(this.PurchaseInvoiceLineListviewFilterSelectionAction_Execute);
            // 
            // PurchaseInvoiceLineSelectAction
            // 
            this.PurchaseInvoiceLineSelectAction.Caption = "Select";
            this.PurchaseInvoiceLineSelectAction.ConfirmationMessage = null;
            this.PurchaseInvoiceLineSelectAction.Id = "PurchaseInvoiceLineSelectActionId";
            this.PurchaseInvoiceLineSelectAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.PurchaseInvoiceLine);
            this.PurchaseInvoiceLineSelectAction.ToolTip = null;
            this.PurchaseInvoiceLineSelectAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.PurchaseInvoiceLineSelectAction_Execute);
            // 
            // PurchaseInvoiceLineUnselectAction
            // 
            this.PurchaseInvoiceLineUnselectAction.Caption = "Unselect";
            this.PurchaseInvoiceLineUnselectAction.ConfirmationMessage = null;
            this.PurchaseInvoiceLineUnselectAction.Id = "PurchaseInvoiceLineUnselectActionId";
            this.PurchaseInvoiceLineUnselectAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.PurchaseInvoiceLine);
            this.PurchaseInvoiceLineUnselectAction.ToolTip = null;
            this.PurchaseInvoiceLineUnselectAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.PurchaseInvoiceLineUnselectAction_Execute);
            // 
            // PurchaseInvoiceLineTaxAndTotalAmountAction
            // 
            this.PurchaseInvoiceLineTaxAndTotalAmountAction.Caption = "Tax And Total Amount";
            this.PurchaseInvoiceLineTaxAndTotalAmountAction.ConfirmationMessage = null;
            this.PurchaseInvoiceLineTaxAndTotalAmountAction.Id = "PurchaseInvoiceLineTaxAndTotalAmountActionId";
            this.PurchaseInvoiceLineTaxAndTotalAmountAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.PurchaseInvoiceLine);
            this.PurchaseInvoiceLineTaxAndTotalAmountAction.ToolTip = null;
            this.PurchaseInvoiceLineTaxAndTotalAmountAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.PurchaseInvoiceLineTaxAndTotalAmountAction_Execute);
            // 
            // PurchaseInvoiceLineActionController
            // 
            this.Actions.Add(this.PurchaseInvoiceLineListviewFilterSelectionAction);
            this.Actions.Add(this.PurchaseInvoiceLineSelectAction);
            this.Actions.Add(this.PurchaseInvoiceLineUnselectAction);
            this.Actions.Add(this.PurchaseInvoiceLineTaxAndTotalAmountAction);

        }

        #endregion
        private DevExpress.ExpressApp.Actions.SingleChoiceAction PurchaseInvoiceLineListviewFilterSelectionAction;
        private DevExpress.ExpressApp.Actions.SimpleAction PurchaseInvoiceLineSelectAction;
        private DevExpress.ExpressApp.Actions.SimpleAction PurchaseInvoiceLineUnselectAction;
        private DevExpress.ExpressApp.Actions.SimpleAction PurchaseInvoiceLineTaxAndTotalAmountAction;
    }
}
