﻿namespace FullDrive.Module.Controllers
{
    partial class CustomerOrderLineActionController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.CustomerOrderLineSelectAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.CustomerOrderLineUnselectAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.CustomerOrderLineTaxAndDiscountAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.CustomerOrderLineListviewFilterSelectionAction = new DevExpress.ExpressApp.Actions.SingleChoiceAction(this.components);
            // 
            // CustomerOrderLineSelectAction
            // 
            this.CustomerOrderLineSelectAction.Caption = "Select";
            this.CustomerOrderLineSelectAction.ConfirmationMessage = null;
            this.CustomerOrderLineSelectAction.Id = "CustomerOrderLineSelectActionId";
            this.CustomerOrderLineSelectAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.CustomerOrderLine);
            this.CustomerOrderLineSelectAction.ToolTip = null;
            this.CustomerOrderLineSelectAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.CustomerOrderLineSelectAction_Execute);
            // 
            // CustomerOrderLineUnselectAction
            // 
            this.CustomerOrderLineUnselectAction.Caption = "Unselect";
            this.CustomerOrderLineUnselectAction.ConfirmationMessage = null;
            this.CustomerOrderLineUnselectAction.Id = "CustomerOrderLineUnselectActionId";
            this.CustomerOrderLineUnselectAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.CustomerOrderLine);
            this.CustomerOrderLineUnselectAction.ToolTip = null;
            this.CustomerOrderLineUnselectAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.CustomerOrderLineUnselectAction_Execute);
            // 
            // CustomerOrderLineTaxAndDiscountAction
            // 
            this.CustomerOrderLineTaxAndDiscountAction.Caption = "Tax And Discount";
            this.CustomerOrderLineTaxAndDiscountAction.ConfirmationMessage = null;
            this.CustomerOrderLineTaxAndDiscountAction.Id = "CustomerOrderLineTaxAndDiscountActionId";
            this.CustomerOrderLineTaxAndDiscountAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.CustomerOrderLine);
            this.CustomerOrderLineTaxAndDiscountAction.ToolTip = null;
            this.CustomerOrderLineTaxAndDiscountAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.CustomerOrderLineTaxAndDiscountAction_Execute);
            // 
            // CustomerOrderLineListviewFilterSelectionAction
            // 
            this.CustomerOrderLineListviewFilterSelectionAction.Caption = "Filter";
            this.CustomerOrderLineListviewFilterSelectionAction.ConfirmationMessage = null;
            this.CustomerOrderLineListviewFilterSelectionAction.Id = "CustomerOrderLineListviewFilterSelectionActionId";
            this.CustomerOrderLineListviewFilterSelectionAction.ItemType = DevExpress.ExpressApp.Actions.SingleChoiceActionItemType.ItemIsOperation;
            this.CustomerOrderLineListviewFilterSelectionAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.CustomerOrderLine);
            this.CustomerOrderLineListviewFilterSelectionAction.ToolTip = null;
            this.CustomerOrderLineListviewFilterSelectionAction.Execute += new DevExpress.ExpressApp.Actions.SingleChoiceActionExecuteEventHandler(this.CustomerOrderLineListviewFilterSelectionAction_Execute);
            // 
            // CustomerOrderLineActionController
            // 
            this.Actions.Add(this.CustomerOrderLineSelectAction);
            this.Actions.Add(this.CustomerOrderLineUnselectAction);
            this.Actions.Add(this.CustomerOrderLineTaxAndDiscountAction);
            this.Actions.Add(this.CustomerOrderLineListviewFilterSelectionAction);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SimpleAction CustomerOrderLineSelectAction;
        private DevExpress.ExpressApp.Actions.SimpleAction CustomerOrderLineUnselectAction;
        private DevExpress.ExpressApp.Actions.SimpleAction CustomerOrderLineTaxAndDiscountAction;
        private DevExpress.ExpressApp.Actions.SingleChoiceAction CustomerOrderLineListviewFilterSelectionAction;
    }
}
