﻿namespace FullDrive.Module.Controllers
{
    partial class CustomerOrderActionController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.CustomerOrderProgressAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.CustomerOrderSetFreeItemAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.CustomerOrderPostingAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.CustomerOrderListviewFilterSelectionAction = new DevExpress.ExpressApp.Actions.SingleChoiceAction(this.components);
            this.CustomerOrderCancelOrderAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            // 
            // CustomerOrderProgressAction
            // 
            this.CustomerOrderProgressAction.Caption = "Progress";
            this.CustomerOrderProgressAction.ConfirmationMessage = null;
            this.CustomerOrderProgressAction.Id = "CustomerOrderProgressActionId";
            this.CustomerOrderProgressAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.CustomerOrder);
            this.CustomerOrderProgressAction.ToolTip = null;
            this.CustomerOrderProgressAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.CustomerOrderProgressAction_Execute);
            // 
            // CustomerOrderSetFreeItemAction
            // 
            this.CustomerOrderSetFreeItemAction.Caption = "Set Free Item";
            this.CustomerOrderSetFreeItemAction.ConfirmationMessage = null;
            this.CustomerOrderSetFreeItemAction.Id = "CustomerOrderSetFreeItemActionId";
            this.CustomerOrderSetFreeItemAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.CustomerOrder);
            this.CustomerOrderSetFreeItemAction.ToolTip = null;
            this.CustomerOrderSetFreeItemAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.CustomerOrderSetFreeItemAction_Execute);
            // 
            // CustomerOrderPostingAction
            // 
            this.CustomerOrderPostingAction.Caption = "Posting";
            this.CustomerOrderPostingAction.ConfirmationMessage = null;
            this.CustomerOrderPostingAction.Id = "CustomerOrderPostingActionId";
            this.CustomerOrderPostingAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.CustomerOrder);
            this.CustomerOrderPostingAction.ToolTip = null;
            this.CustomerOrderPostingAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.CustomerOrderPostingAction_Execute);
            // 
            // CustomerOrderListviewFilterSelectionAction
            // 
            this.CustomerOrderListviewFilterSelectionAction.Caption = "Filter";
            this.CustomerOrderListviewFilterSelectionAction.ConfirmationMessage = null;
            this.CustomerOrderListviewFilterSelectionAction.Id = "CustomerOrderListviewFilterSelectionActionId";
            this.CustomerOrderListviewFilterSelectionAction.ItemType = DevExpress.ExpressApp.Actions.SingleChoiceActionItemType.ItemIsOperation;
            this.CustomerOrderListviewFilterSelectionAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.CustomerOrder);
            this.CustomerOrderListviewFilterSelectionAction.ToolTip = null;
            this.CustomerOrderListviewFilterSelectionAction.Execute += new DevExpress.ExpressApp.Actions.SingleChoiceActionExecuteEventHandler(this.CustomerOrderListviewFilterSelectionAction_Execute);
            // 
            // CustomerOrderCancelOrderAction
            // 
            this.CustomerOrderCancelOrderAction.Caption = "Cancel Order";
            this.CustomerOrderCancelOrderAction.ConfirmationMessage = null;
            this.CustomerOrderCancelOrderAction.Id = "CustomerOrderCancelOrderActionId";
            this.CustomerOrderCancelOrderAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.CustomerOrder);
            this.CustomerOrderCancelOrderAction.ToolTip = null;
            this.CustomerOrderCancelOrderAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.CustomerOrderCancelOrderAction_Execute);
            // 
            // CustomerOrderActionController
            // 
            this.Actions.Add(this.CustomerOrderProgressAction);
            this.Actions.Add(this.CustomerOrderSetFreeItemAction);
            this.Actions.Add(this.CustomerOrderPostingAction);
            this.Actions.Add(this.CustomerOrderListviewFilterSelectionAction);
            this.Actions.Add(this.CustomerOrderCancelOrderAction);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SimpleAction CustomerOrderProgressAction;
        private DevExpress.ExpressApp.Actions.SimpleAction CustomerOrderSetFreeItemAction;
        private DevExpress.ExpressApp.Actions.SimpleAction CustomerOrderPostingAction;
        private DevExpress.ExpressApp.Actions.SingleChoiceAction CustomerOrderListviewFilterSelectionAction;
        private DevExpress.ExpressApp.Actions.SimpleAction CustomerOrderCancelOrderAction;
    }
}
