﻿namespace FullDrive.Module.Controllers
{
    partial class InvoiceOutMonitoringActionController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.InvoiceOutMonitoringImportAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.InvoiceOutMonitoringGetDataAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            // 
            // InvoiceOutMonitoringImportAction
            // 
            this.InvoiceOutMonitoringImportAction.Caption = "Invoice Out Monitoring Import Action Id";
            this.InvoiceOutMonitoringImportAction.ConfirmationMessage = null;
            this.InvoiceOutMonitoringImportAction.Id = "InvoiceOutMonitoringImportActionId";
            this.InvoiceOutMonitoringImportAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.InvoiceOutMonitoring);
            this.InvoiceOutMonitoringImportAction.ToolTip = null;
            this.InvoiceOutMonitoringImportAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.InvoiceOutMonitoringImportAction_Execute);
            // 
            // InvoiceOutMonitoringGetDataAction
            // 
            this.InvoiceOutMonitoringGetDataAction.Caption = "Get Data";
            this.InvoiceOutMonitoringGetDataAction.ConfirmationMessage = null;
            this.InvoiceOutMonitoringGetDataAction.Id = "InvoiceOutMonitoringGetDataActionId";
            this.InvoiceOutMonitoringGetDataAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.InvoiceOutMonitoring);
            this.InvoiceOutMonitoringGetDataAction.TargetViewType = DevExpress.ExpressApp.ViewType.ListView;
            this.InvoiceOutMonitoringGetDataAction.ToolTip = null;
            this.InvoiceOutMonitoringGetDataAction.TypeOfView = typeof(DevExpress.ExpressApp.ListView);
            this.InvoiceOutMonitoringGetDataAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.InvoiceOutMonitoringGetDataAction_Execute);
            // 
            // InvoiceOutMonitoringActionController
            // 
            this.Actions.Add(this.InvoiceOutMonitoringImportAction);
            this.Actions.Add(this.InvoiceOutMonitoringGetDataAction);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SimpleAction InvoiceOutMonitoringImportAction;
        private DevExpress.ExpressApp.Actions.SimpleAction InvoiceOutMonitoringGetDataAction;
    }
}
