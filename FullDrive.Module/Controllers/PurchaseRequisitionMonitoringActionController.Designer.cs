﻿namespace FullDrive.Module.Controllers
{
    partial class PurchaseRequisitionMonitoringActionController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.PurchaseRequisitionMonitoringSelectAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.PurchaseRequisitionMonitoringUnselectAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.PurchaseRequisitionMonitoringCancelPOAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            // 
            // PurchaseRequisitionMonitoringSelectAction
            // 
            this.PurchaseRequisitionMonitoringSelectAction.Caption = "Select";
            this.PurchaseRequisitionMonitoringSelectAction.ConfirmationMessage = null;
            this.PurchaseRequisitionMonitoringSelectAction.Id = "PurchaseRequisitionMonitoringSelectActionId";
            this.PurchaseRequisitionMonitoringSelectAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.PurchaseRequisitionMonitoring);
            this.PurchaseRequisitionMonitoringSelectAction.ToolTip = null;
            this.PurchaseRequisitionMonitoringSelectAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.PurchaseRequisitionMonitoringSelectAction_Execute);
            // 
            // PurchaseRequisitionMonitoringUnselectAction
            // 
            this.PurchaseRequisitionMonitoringUnselectAction.Caption = "Unselect";
            this.PurchaseRequisitionMonitoringUnselectAction.ConfirmationMessage = null;
            this.PurchaseRequisitionMonitoringUnselectAction.Id = "PurchaseRequisitionMonitoringUnselectActionId";
            this.PurchaseRequisitionMonitoringUnselectAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.PurchaseRequisitionMonitoring);
            this.PurchaseRequisitionMonitoringUnselectAction.ToolTip = null;
            this.PurchaseRequisitionMonitoringUnselectAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.PurchaseRequisitionMonitoringUnselectAction_Execute);
            // 
            // PurchaseRequisitionMonitoringCancelPOAction
            // 
            this.PurchaseRequisitionMonitoringCancelPOAction.Caption = "Cancel PO";
            this.PurchaseRequisitionMonitoringCancelPOAction.ConfirmationMessage = null;
            this.PurchaseRequisitionMonitoringCancelPOAction.Id = "PurchaseRequisitionMonitoringCancelPOActionId";
            this.PurchaseRequisitionMonitoringCancelPOAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.PurchaseRequisitionMonitoring);
            this.PurchaseRequisitionMonitoringCancelPOAction.ToolTip = null;
            this.PurchaseRequisitionMonitoringCancelPOAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.PurchaseRequisitionMonitoringCancelPOAction_Execute);
            // 
            // PurchaseRequisitionMonitoringActionController
            // 
            this.Actions.Add(this.PurchaseRequisitionMonitoringSelectAction);
            this.Actions.Add(this.PurchaseRequisitionMonitoringUnselectAction);
            this.Actions.Add(this.PurchaseRequisitionMonitoringCancelPOAction);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SimpleAction PurchaseRequisitionMonitoringSelectAction;
        private DevExpress.ExpressApp.Actions.SimpleAction PurchaseRequisitionMonitoringUnselectAction;
        private DevExpress.ExpressApp.Actions.SimpleAction PurchaseRequisitionMonitoringCancelPOAction;
    }
}
