﻿namespace FullDrive.Module.Controllers
{
    partial class PaymentRealizationLineActionController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.PaymentRealizationLineSelectAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.PaymentRealizationLineUnselectAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.PaymentRealizationLinePaymentStatusAction = new DevExpress.ExpressApp.Actions.SingleChoiceAction(this.components);
            // 
            // PaymentRealizationLineSelectAction
            // 
            this.PaymentRealizationLineSelectAction.Caption = "Select";
            this.PaymentRealizationLineSelectAction.ConfirmationMessage = null;
            this.PaymentRealizationLineSelectAction.Id = "PaymentRealizationLineSelectActionId";
            this.PaymentRealizationLineSelectAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.PaymentRealizationLine);
            this.PaymentRealizationLineSelectAction.ToolTip = null;
            this.PaymentRealizationLineSelectAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.PaymentRealizationLineSelectAction_Execute);
            // 
            // PaymentRealizationLineUnselectAction
            // 
            this.PaymentRealizationLineUnselectAction.Caption = "Unselect";
            this.PaymentRealizationLineUnselectAction.ConfirmationMessage = null;
            this.PaymentRealizationLineUnselectAction.Id = "PaymentRealizationLineUnselectActionId";
            this.PaymentRealizationLineUnselectAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.PaymentRealizationLine);
            this.PaymentRealizationLineUnselectAction.ToolTip = null;
            this.PaymentRealizationLineUnselectAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.PaymentRealizationLineUnselectAction_Execute);
            // 
            // PaymentRealizationLinePaymentStatusAction
            // 
            this.PaymentRealizationLinePaymentStatusAction.Caption = "Payment Status";
            this.PaymentRealizationLinePaymentStatusAction.ConfirmationMessage = null;
            this.PaymentRealizationLinePaymentStatusAction.Id = "PaymentRealizationLinePaymentStatusActionId";
            this.PaymentRealizationLinePaymentStatusAction.ItemType = DevExpress.ExpressApp.Actions.SingleChoiceActionItemType.ItemIsOperation;
            this.PaymentRealizationLinePaymentStatusAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.PaymentRealizationLine);
            this.PaymentRealizationLinePaymentStatusAction.ToolTip = null;
            this.PaymentRealizationLinePaymentStatusAction.Execute += new DevExpress.ExpressApp.Actions.SingleChoiceActionExecuteEventHandler(this.PaymentRealizationLinePaymentStatusAction_Execute);
            // 
            // PaymentRealizationLineActionController
            // 
            this.Actions.Add(this.PaymentRealizationLineSelectAction);
            this.Actions.Add(this.PaymentRealizationLineUnselectAction);
            this.Actions.Add(this.PaymentRealizationLinePaymentStatusAction);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SimpleAction PaymentRealizationLineSelectAction;
        private DevExpress.ExpressApp.Actions.SimpleAction PaymentRealizationLineUnselectAction;
        private DevExpress.ExpressApp.Actions.SingleChoiceAction PaymentRealizationLinePaymentStatusAction;
    }
}
