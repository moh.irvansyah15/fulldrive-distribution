﻿using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;

using DevExpress.ExpressApp.Editors;
using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.ConditionalAppearance;

namespace FullDrive.Module.BusinessObjects
{
    [DefaultClassOptions]
    [DefaultProperty("Code")]
    [NavigationItem("Sales")]
    [RuleCombinationOfPropertiesIsUnique("SalesInvoiceLineCmpRuleUnique", DefaultContexts.Save, "Code")]
    //[ImageName("BO_Contact")]
    //[DefaultProperty("DisplayMemberNameForLookupEditorsOfThisType")]
    //[DefaultListViewOptions(MasterDetailMode.ListViewOnly, false, NewItemRowPosition.None)]
    //[Persistent("DatabaseTableName")]
    // Specify more UI options using a declarative approach (https://documentation.devexpress.com/#eXpressAppFramework/CustomDocument112701).
    public class SalesInvoiceLineCmp : BaseObject
    { // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).

        #region InitialSystem
        private bool _activationPosting;
        private bool _freeItemChecked;
        private bool _discountChecked;
        private bool _taxChecked;
        private int _no;
        private bool _select;
        private string _code;
        #endregion InitialSystem
        #region InitialOrganization
        private Company _company;
        private Workplace _workplace;
        private Division _division;
        private Department _department;
        private Section _section;
        private Employee _employee;
        private XPCollection<Employee> _availablePIC;
        private Employee _pic;
        private XPCollection<Location> _availableLocation;
        private Location _location;
        private BeginningInventory _begInv;
        #endregion InitialOrganization 
        private OrderType _salesType;
        private XPCollection<Item> _availableItem;
        private Item _item;
        private BeginningInventoryLine _begInvLine;
        private string _description;
        private XPCollection<UnitOfMeasure> _availableUnitOfMeasure;
        #region inisialisasiDefaultQty
        private double _dQty;
        private UnitOfMeasure _dUom;
        private double _qty;
        private UnitOfMeasure _uom;
        private double _tQty;
        private StockType _stockType;
        #endregion inisialisasiDefaultQty
        #region InisialisasiAmount
        private Currency _currency;
        private PriceGroup _priceGroup;
        private PriceLine _priceLine;
        private double _uAmount;
        private double _tUAmount;
        private DiscountRule _discountRule;
        private AccountingPeriodicLine _accountingPeriodicLine;
        private double _discAmount;
        private double _oldDiscAmount;
        private Tax _tax;
        private double _txValue;
        private double _txAmount;
        private double _tAmount;
        #endregion InisialisasiAmount
        private DateTime _etd;
        private DateTime _eta;
        private DateTime _docDate;
        private int _journalMonth;
        private int _journalYear;
        private Status _status;
        private DateTime _statusDate;
        private int _postedCount;
        private string _signCode;
        private SalesInvoiceCmp _salesInvoiceCmp;
        private SalesInvoiceLine _salesInvoiceLine;
        private string _localUserAccess;
        private GlobalFunction _globFunc;
        //AuditTrail
        private XPCollection<AuditDataItemPersistent> changeHistory;

        public SalesInvoiceLineCmp(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if (!IsLoading)
            {
                _globFunc = new GlobalFunction();
                DateTime now = DateTime.Now;
                #region UserAccess
                _localUserAccess = SecuritySystem.CurrentUserName;
                UserAccess _locUserAccess = Session.FindObject<UserAccess>(new BinaryOperator("UserName", _localUserAccess));
                if (_locUserAccess != null)
                {
                    if (_locUserAccess.Employee != null)
                    {
                        if (_locUserAccess.Employee.Company != null && _locUserAccess.Employee.Workplace != null)
                        {
                            this.Code = _globFunc.GetNumberingUnlockOptimisticRecordBasedOrganization(this.Session.DataLayer, ObjectList.SalesInvoiceLineCmp, _locUserAccess.Employee.Company, _locUserAccess.Employee.Workplace);
                        }
                        else
                        {
                            this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.SalesInvoiceLineCmp);
                        }
                    }
                }
                #endregion UserAccess
                this.Select = true;
                this.SalesType = OrderType.Item;
                this.Status = CustomProcess.Status.Open;
                this.StatusDate = now;
                this.DiscountChecked = false;
                this.StockType = StockType.Good;
            }
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).
        }

        protected override void OnSaving()
        {
            base.OnSaving();
            if (!IsLoading && IsSaving)
            {
                UpdateNo();
            }
        }

        protected override void OnDeleting()
        {
            base.OnDeleting();
            if (!IsLoading)
            {
                RecoveryDeleteNo();
            }
        }

        #region Field

        #region System

        [Browsable(false)]
        [ImmediatePostData()]
        public bool ActivationPosting
        {
            get { return _activationPosting; }
            set { SetPropertyValue("ActivationPosting", ref _activationPosting, value); }
        }

        [Browsable(false)]
        public bool FreeItemChecked
        {
            get { return _freeItemChecked; }
            set { SetPropertyValue("FreeItemChecked", ref _freeItemChecked, value); }
        }

        [Browsable(false)]
        public bool DiscountChecked
        {
            get { return _discountChecked; }
            set { SetPropertyValue("DiscountChecked", ref _discountChecked, value); }
        }

        [Browsable(false)]
        public bool TaxChecked
        {
            get { return _taxChecked; }
            set { SetPropertyValue("TaxChecked", ref _taxChecked, value); }
        }

        [Appearance("SalesInvoiceLineCmpNoClose", Enabled = false)]
        public int No
        {
            get { return _no; }
            set { SetPropertyValue("No", ref _no, value); }
        }

        [Appearance("SalesInvoiceLineCmpSelectClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public bool Select
        {
            get { return _select; }
            set { SetPropertyValue("Select", ref _select, value); }
        }

        [VisibleInListView(false)]
        [RuleRequiredField(DefaultContexts.Save)]
        [Appearance("SalesInvoiceLineCmpCodeClose", Enabled = false)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue("Code", ref _code, value); }
        }

        #endregion System
        #region Organization

        [ImmediatePostData()]
        [VisibleInListView(false)]
        [RuleRequiredField(DefaultContexts.Save)]
        [DataSourceCriteria("Active = true")]
        [Appearance("SalesInvoiceLineCmpCompanyClose", Enabled = false)]
        public Company Company
        {
            get { return _company; }
            set { SetPropertyValue("Company", ref _company, value); }
        }

        [ImmediatePostData()]
        [VisibleInListView(false)]
        [RuleRequiredField(DefaultContexts.Save)]
        [DataSourceCriteria("Active = true")]
        [Appearance("SalesInvoiceLineCmpWorkplaceClose", Enabled = false)]
        public Workplace Workplace
        {
            get { return _workplace; }
            set { SetPropertyValue("Workplace", ref _workplace, value); }
        }

        [Browsable(false)]
        public Division Division
        {
            get { return _division; }
            set { SetPropertyValue("Division", ref _division, value); }
        }

        [Browsable(false)]
        public Department Department
        {
            get { return _department; }
            set { SetPropertyValue("Department", ref _department, value); }
        }

        [Browsable(false)]
        public Section Section
        {
            get { return _section; }
            set { SetPropertyValue("Section", ref _section, value); }
        }

        [Browsable(false)]
        public Employee Employee
        {
            get { return _employee; }
            set { SetPropertyValue("Employee", ref _employee, value); }
        }

        [Browsable(false)]
        public XPCollection<Employee> AvailablePIC
        {
            get
            {

                if (this.Company != null && this.Workplace != null)
                {
                    XPCollection<Employee> _locPICs = new XPCollection<Employee>
                                                            (Session, new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("Company", this.Company),
                                                            new BinaryOperator("Workplace", this.Workplace),
                                                            new BinaryOperator("Active", true),
                                                            new GroupOperator(GroupOperatorType.Or,
                                                            new BinaryOperator("SalesRole", SalesRole.TakingOrder),
                                                            new BinaryOperator("SalesRole", SalesRole.All))));
                    if (_locPICs != null && _locPICs.Count() > 0)
                    {
                        _availablePIC = _locPICs;
                    }
                }
                else
                {
                    _availablePIC = new XPCollection<Employee>(Session, new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("Active", true),
                                                            new GroupOperator(GroupOperatorType.Or,
                                                            new BinaryOperator("SalesRole", SalesRole.TakingOrder),
                                                            new BinaryOperator("SalesRole", SalesRole.All))));
                }

                return _availablePIC;

            }
        }

        [Browsable(false)]
        [ImmediatePostData()]
        [DataSourceProperty("AvailablePIC", DataSourcePropertyIsNullMode.SelectAll)]
        [Appearance("SalesInvoiceLinePICClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Employee PIC
        {
            get { return _pic; }
            set
            {
                SetPropertyValue("PIC", ref _pic, value);
                if (!IsLoading)
                {
                    if (this._pic != null)
                    {
                        SetLocation(_pic);
                    }
                    else
                    {
                        this.Location = null;
                    }
                }
            }
        }

        [Browsable(false)]
        public XPCollection<Location> AvailableLocation
        {
            get
            {
                string _beginString = null;
                string _endString = null;
                string _fullString = null;

                if (this.Company != null && this.Workplace != null && this.PIC != null)
                {
                    List<string> _stringSLS = new List<string>();

                    XPCollection<SalesmanLocationSetup> _locSalesmanLocationSetups = new XPCollection<SalesmanLocationSetup>
                                                                              (Session, new GroupOperator(GroupOperatorType.And,
                                                                               new BinaryOperator("Company", this.Company),
                                                                               new BinaryOperator("Workplace", this.Workplace),
                                                                               new BinaryOperator("Salesman", this.PIC),
                                                                               new BinaryOperator("Active", true)));

                    if (_locSalesmanLocationSetups != null && _locSalesmanLocationSetups.Count() > 0)
                    {
                        foreach (SalesmanLocationSetup _locSalesmanLocationSetup in _locSalesmanLocationSetups)
                        {
                            if (_locSalesmanLocationSetup.Location != null)
                            {
                                if (_locSalesmanLocationSetup.Location.Code != null)
                                {
                                    _stringSLS.Add(_locSalesmanLocationSetup.Location.Code);
                                }
                            }
                        }
                    }

                    IEnumerable<string> _stringArraySLSDistinct = _stringSLS.Distinct();
                    string[] _stringArraySLSList = _stringArraySLSDistinct.ToArray();
                    if (_stringArraySLSList.Length == 1)
                    {
                        for (int i = 0; i < _stringArraySLSList.Length; i++)
                        {
                            Location _locLoc = Session.FindObject<Location>(new BinaryOperator("Code", _stringArraySLSList[i]));
                            if (_locLoc != null)
                            {
                                if (i == 0)
                                {
                                    _beginString = "[Code]=='" + _locLoc.Code + "'";
                                }
                            }
                        }
                    }
                    else if (_stringArraySLSList.Length > 1)
                    {
                        for (int i = 0; i < _stringArraySLSList.Length; i++)
                        {
                            Location _locLoc = Session.FindObject<Location>(new BinaryOperator("Code", _stringArraySLSList[i]));
                            if (_locLoc != null)
                            {
                                if (i == 0)
                                {
                                    _beginString = "[Code]=='" + _locLoc.Code + "'";
                                }
                                else
                                {
                                    _endString = _endString + " OR [Code]=='" + _locLoc.Code + "'";
                                }
                            }
                        }
                    }
                    _fullString = _beginString + _endString;

                    if (_fullString != null)
                    {
                        _availableLocation = new XPCollection<Location>(Session, CriteriaOperator.Parse(_fullString));
                    }

                }
                else
                {
                    if (this.Company != null && this.Workplace != null)
                    {
                        _availableLocation = new XPCollection<Location>(Session, new GroupOperator(GroupOperatorType.And,
                                                                            new BinaryOperator("Company", this.Company),
                                                                            new BinaryOperator("Workplace", this.Workplace),
                                                                            new BinaryOperator("Active", true)));
                    }
                }

                return _availableLocation;

            }
        }

        [VisibleInListView(false)]
        [ImmediatePostData()]
        [DataSourceProperty("AvailableLocation")]
        [Appearance("SalesInvoiceLineLocationClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Location Location
        {
            get { return _location; }
            set
            {
                SetPropertyValue("Location", ref _location, value);
                if (!IsLoading)
                {
                    if (this._location != null)
                    {
                        SetBegInv(_location);
                    }
                    else
                    {
                        BegInv = null;
                    }
                }
            }

        }

        [Browsable(false)]
        [ImmediatePostData()]
        [Appearance("SalesInvoiceLineBegInvClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public BeginningInventory BegInv
        {
            get { return _begInv; }
            set { SetPropertyValue("BegInv", ref _begInv, value); }
        }
        #endregion Organization

        [ImmediatePostData()]
        [VisibleInListView(false)]
        [Appearance("SalesInvoiceLineCmpSalesTypeClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public OrderType SalesType
        {
            get { return _salesType; }
            set { SetPropertyValue("SalesType", ref _salesType, value); }
        }

        [Browsable(false)]
        public XPCollection<Item> AvailableItem
        {
            get
            {
                string _beginString = null;
                string _endString = null;
                string _fullString = null;
                if (this.Company != null && this.Workplace != null)
                {
                    List<string> _stringITM = new List<string>();

                    XPQuery<BeginningInventoryLine> _beginningInventoryLinesQuery = new XPQuery<BeginningInventoryLine>(Session);

                    var _beginningInventoryLines = from bil in _beginningInventoryLinesQuery
                                                   where (bil.Company == this.Company
                                                   && bil.Workplace == this.Workplace
                                                   && bil.Item.ItemType == this.SalesType
                                                   && bil.QtyAvailable > 0
                                                   && bil.Active == true
                                                   && bil.Vehicle == null
                                                   )
                                                   group bil by bil.Item into g
                                                   select new { Item = g.Key };
                    if (_beginningInventoryLines != null && _beginningInventoryLines.Count() > 0)
                    {
                        foreach (var _beginningInventoryLine in _beginningInventoryLines)
                        {
                            _stringITM.Add(_beginningInventoryLine.Item.Code);
                        }
                    }

                    IEnumerable<string> _stringArrayITMDistinct = _stringITM.Distinct();
                    string[] _stringArrayITMList = _stringArrayITMDistinct.ToArray();
                    if (_stringArrayITMList.Length == 1)
                    {
                        for (int i = 0; i < _stringArrayITMList.Length; i++)
                        {
                            Item _locITM = Session.FindObject<Item>(new BinaryOperator("Code", _stringArrayITMList[i]));
                            if (_locITM != null)
                            {
                                if (i == 0)
                                {
                                    _beginString = "[Code]=='" + _locITM.Code + "'";
                                }
                            }
                        }
                    }
                    else if (_stringArrayITMList.Length > 1)
                    {
                        for (int i = 0; i < _stringArrayITMList.Length; i++)
                        {
                            Item _locITM = Session.FindObject<Item>(new BinaryOperator("Code", _stringArrayITMList[i]));
                            if (_locITM != null)
                            {
                                if (i == 0)
                                {
                                    _beginString = "[Code]=='" + _locITM.Code + "'";
                                }
                                else
                                {
                                    _endString = _endString + " OR [Code]=='" + _locITM.Code + "'";
                                }
                            }
                        }
                    }
                    _fullString = _beginString + _endString;

                    if (_fullString != null)
                    {
                        _availableItem = new XPCollection<Item>(Session, CriteriaOperator.Parse(_fullString));
                    }
                }
                else
                {
                    _availableItem = new XPCollection<Item>(Session, new GroupOperator(GroupOperatorType.And,
                                                    new BinaryOperator("Active", true)));
                }

                return _availableItem;

            }
        }

        [DataSourceProperty("AvailableItem", DataSourcePropertyIsNullMode.SelectAll)]
        [Appearance("SalesInvoiceLineCmpItemClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Item Item
        {
            get { return _item; }
            set
            {
                SetPropertyValue("Item", ref _item, value);
                if (!IsLoading)
                {
                    if (this._item != null)
                    {
                        if (this._item.Description != null)
                        {
                            this.Description = this._item.Description;
                        }
                        if (this._item.BasedUOM != null)
                        {
                            this.DUOM = this._item.BasedUOM;
                        }
                        SetUOM();
                        SetPriceLine();
                        SetDiscountRule();
                        SetBegInvLine(_item);
                    }
                    else
                    {
                        this.Description = null;
                        this.DUOM = null;
                        this.BegInvLine = null;
                    }
                }
            }
        }

        [Browsable(false)]
        [ImmediatePostData()]
        [Appearance("SalesInvoiceLineCmpBegInvLineClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public BeginningInventoryLine BegInvLine
        {
            get { return _begInvLine; }
            set { SetPropertyValue("BegInvLine", ref _begInvLine, value); }
        }

        [Appearance("SalesInvoiceLineCmpDescriptionClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [Size(512)]
        public string Description
        {
            get { return _description; }
            set { SetPropertyValue("Description", ref _description, value); }
        }

        #region DefaultQty

        [Appearance("SalesInvoiceLineCmpDQtyClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [ImmediatePostData()]
        public double DQty
        {
            get { return _dQty; }
            set
            {
                SetPropertyValue("DQty", ref _dQty, value);
                if (!IsLoading)
                {
                    SetTotalQty();
                    SetTotalUnitAmount();
                    SetTotalAmount();
                }
            }
        }

        [Appearance("SalesInvoiceLineCmpDUOMClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [ImmediatePostData()]
        [DataSourceCriteria("Active = true")]
        public UnitOfMeasure DUOM
        {
            get { return _dUom; }
            set
            {
                SetPropertyValue("DUOM", ref _dUom, value);
                if (!IsLoading)
                {
                    SetTotalQty();
                    SetTotalUnitAmount();
                    SetTotalAmount();
                }
            }
        }

        [Appearance("SalesInvoiceLineCmpQtyClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [ImmediatePostData()]
        public double Qty
        {
            get { return _qty; }
            set
            {
                SetPropertyValue("Qty", ref _qty, value);
                if (!IsLoading)
                {
                    SetTotalQty();
                    SetTotalUnitAmount();
                    SetTotalAmount();
                }
            }
        }

        [Browsable(false)]
        public XPCollection<UnitOfMeasure> AvailableUnitOfMeasure
        {
            get
            {
                string _beginString = null;
                string _endString = null;
                string _fullString = null;
                if (Item == null)
                {
                    _availableUnitOfMeasure = new XPCollection<UnitOfMeasure>(Session, new GroupOperator(GroupOperatorType.And,
                                                                            new BinaryOperator("Active", true)));
                }
                else
                {
                    List<string> _stringUOM = new List<string>();

                    XPCollection<ItemUnitOfMeasure> _locItemUnitOfMeasures = new XPCollection<ItemUnitOfMeasure>
                                                                              (Session, new GroupOperator(GroupOperatorType.And,
                                                                               new BinaryOperator("Item", this.Item),
                                                                               new BinaryOperator("Active", true)));

                    if (_locItemUnitOfMeasures != null && _locItemUnitOfMeasures.Count() > 0)
                    {
                        foreach (ItemUnitOfMeasure _locItemUnitOfMeasure in _locItemUnitOfMeasures)
                        {
                            _stringUOM.Add(_locItemUnitOfMeasure.UOM.Code);
                        }
                    }

                    IEnumerable<string> _stringArrayUOMDistinct = _stringUOM.Distinct();
                    string[] _stringArrayUOMList = _stringArrayUOMDistinct.ToArray();
                    if (_stringArrayUOMList.Length == 1)
                    {
                        for (int i = 0; i < _stringArrayUOMList.Length; i++)
                        {
                            UnitOfMeasure _locUOM = Session.FindObject<UnitOfMeasure>(new BinaryOperator("Code", _stringArrayUOMList[i]));
                            if (_locUOM != null)
                            {
                                if (i == 0)
                                {
                                    _beginString = "[Code]=='" + _locUOM.Code + "'";
                                }
                            }
                        }
                    }
                    else if (_stringArrayUOMList.Length > 1)
                    {
                        for (int i = 0; i < _stringArrayUOMList.Length; i++)
                        {
                            UnitOfMeasure _locUOM = Session.FindObject<UnitOfMeasure>(new BinaryOperator("Code", _stringArrayUOMList[i]));
                            if (_locUOM != null)
                            {
                                if (i == 0)
                                {
                                    _beginString = "[Code]=='" + _locUOM.Code + "'";
                                }
                                else
                                {
                                    _endString = _endString + " OR [Code]=='" + _locUOM.Code + "'";
                                }
                            }
                        }
                    }
                    _fullString = _beginString + _endString;

                    if (_fullString != null)
                    {
                        _availableUnitOfMeasure = new XPCollection<UnitOfMeasure>(Session, CriteriaOperator.Parse(_fullString));
                    }

                }
                return _availableUnitOfMeasure;

            }
        }

        [ImmediatePostData()]
        [DataSourceProperty("AvailableUnitOfMeasure", DataSourcePropertyIsNullMode.SelectAll)]
        [Appearance("SalesInvoiceLineCmpUOMClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public UnitOfMeasure UOM
        {
            get { return _uom; }
            set
            {
                SetPropertyValue("UOM", ref _uom, value);
                if (!IsLoading)
                {
                    SetTotalQty();
                    SetTotalUnitAmount();
                    SetTotalAmount();
                }
            }
        }

        [Appearance("SalesInvoiceLineCmpTotalQtyEnabled", Enabled = false)]
        public double TQty
        {
            get { return _tQty; }
            set { SetPropertyValue("TotalQty", ref _tQty, value); }
        }

        [Appearance("SalesInvoiceLineCmpStockTypeClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public StockType StockType
        {
            get { return _stockType; }
            set { SetPropertyValue("StockType", ref _stockType, value); }
        }

        #endregion DefaultQty

        #region Amount

        [VisibleInListView(false)]
        [Appearance("SalesInvoiceLineCmpCurrencyClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Currency Currency
        {
            get { return _currency; }
            set { SetPropertyValue("Currency", ref _currency, value); }
        }

        [VisibleInListView(false)]
        [ImmediatePostData()]
        [Appearance("SalesInvoiceLineCmpPriceGroupClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public PriceGroup PriceGroup
        {
            get { return _priceGroup; }
            set
            {
                SetPropertyValue("PriceGroup", ref _priceGroup, value);
                if (!IsLoading)
                {
                    if (this._priceGroup != null)
                    {
                        SetPriceLine();
                        SetDiscountRule();
                    }
                }
            }
        }

        [Browsable(false)]
        [DataSourceCriteria("Active = true"), ImmediatePostData()]
        [Appearance("SalesInvoiceLineCmpPriceLineClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public PriceLine PriceLine
        {
            get { return _priceLine; }
            set
            {
                SetPropertyValue("PriceLine", ref _priceLine, value);
                if (!IsLoading)
                {
                    if (this._priceLine != null)
                    {
                        this.UAmount = this._priceLine.Amount2a;
                    }
                }
            }
        }

        [Appearance("SalesInvoiceLineCmpUnitAmountEnabled", Enabled = false)]
        [ImmediatePostData()]
        public double UAmount
        {
            get { return _uAmount; }
            set
            {
                SetPropertyValue("UAmount", ref _uAmount, value);
                if (!IsLoading)
                {
                    _globFunc = new GlobalFunction();
                    if (this._uAmount > 0)
                    {
                        if (_globFunc.GetRoundingList(Session, ObjectList.SalesInvoiceLine, FieldName.UAmount) == true)
                        {
                            this._uAmount = _globFunc.GetRoundUp(Session, this._uAmount, ObjectList.SalesInvoiceLine, FieldName.UAmount);
                        }
                    }
                    SetTotalUnitAmount();
                    SetTotalAmount();
                }
            }
        }

        [Appearance("SalesInvoiceLineCmpTotalUnitAmountEnabled", Enabled = false)]
        public double TUAmount
        {
            get { return _tUAmount; }
            set { SetPropertyValue("TUAmount", ref _tUAmount, value); }
        }

        [Appearance("SalesInvoiceLineCmpTaxClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [ImmediatePostData]
        [DataSourceCriteria("Active = true")]
        public Tax Tax
        {
            get { return _tax; }
            set
            {
                SetPropertyValue("Tax", ref _tax, value);
                if (!IsLoading)
                {
                    if (this._tax != null)
                    {
                        this.TxValue = this._tax.Value;
                    }
                    else
                    {
                        SetNormalTax();
                    }
                }
            }
        }

        [Appearance("SalesInvoiceLineCmpTxValueClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [ImmediatePostData()]
        public double TxValue
        {
            get { return _txValue; }
            set { SetPropertyValue("TxValue", ref _txValue, value); }
        }

        [Appearance("SalesInvoiceLineCmpTaxAmountEnabled", Enabled = false)]
        public double TxAmount
        {
            get { return _txAmount; }
            set
            {
                SetPropertyValue("TxAmount", ref _txAmount, value);
                if (!IsLoading)
                {
                    _globFunc = new GlobalFunction();
                    if (this._txAmount > 0)
                    {
                        if (_globFunc.GetRoundingList(Session, ObjectList.SalesInvoiceLine, FieldName.TxAmount) == true)
                        {
                            this._txAmount = _globFunc.GetRoundUp(Session, this._txAmount, ObjectList.SalesInvoiceLine, FieldName.TxAmount);
                        }
                    }

                    SetTotalAmount();
                }
            }
        }

        [ImmediatePostData()]
        [DataSourceCriteria("Company = '@This.Company' AND Workplace = '@This.Workplace' AND MpItem = '@This.Item' AND PriceGroup = '@This.PriceGroup' AND Active = true")]
        [Appearance("SalesInvoiceLineCmpDiscountRuleClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public DiscountRule DiscountRule
        {
            get { return _discountRule; }
            set { SetPropertyValue("DiscountRule", ref _discountRule, value); }
        }

        [Browsable(false)]
        public AccountingPeriodicLine AccountingPeriodicLine
        {
            get { return _accountingPeriodicLine; }
            set { SetPropertyValue("AccountingPeriodicLine", ref _accountingPeriodicLine, value); }
        }

        [Appearance("SalesInvoiceLineCmpDiscAmountEnabled", Enabled = false)]
        public double DiscAmount
        {
            get { return _discAmount; }
            set
            {
                SetPropertyValue("DiscAmount", ref _discAmount, value);
                if (!IsLoading)
                {
                    _globFunc = new GlobalFunction();
                    if (this._discAmount > 0)
                    {
                        if (_globFunc.GetRoundingList(Session, ObjectList.SalesInvoiceLine, FieldName.DiscAmount) == true)
                        {
                            this._discAmount = _globFunc.GetRoundUp(Session, this._discAmount, ObjectList.SalesInvoiceLine, FieldName.DiscAmount);
                        }
                    }
                }
            }
        }

        [Browsable(false)]
        public double OldDiscAmount
        {
            get { return _oldDiscAmount; }
            set { SetPropertyValue("OldDiscAmount", ref _oldDiscAmount, value); }
        }

        [Appearance("SalesInvoiceLineCmpTotalAmountEnabled", Enabled = false)]
        public double TAmount
        {
            get { return _tAmount; }
            set
            {
                SetPropertyValue("TAmount", ref _tAmount, value);
                if (!IsLoading)
                {
                    _globFunc = new GlobalFunction();
                    if (this._tAmount > 0)
                    {
                        if (_globFunc.GetRoundingList(Session, ObjectList.SalesInvoiceLine, FieldName.TAmount) == true)
                        {
                            this._tAmount = _globFunc.GetRoundUp(Session, this._tAmount, ObjectList.SalesInvoiceLine, FieldName.TAmount);
                        }
                    }
                }
            }
        }

        #endregion Amount

        [VisibleInListView(false)]
        [ModelDefault("DisplayFormat", "{0:dd/MM/yyyy}")]
        [ModelDefault("EditMask", "dd/MM/yyyy")]
        [Appearance("SalesInvoiceLineCmpETDClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public DateTime ETD
        {
            get { return _etd; }
            set { SetPropertyValue("ETD", ref _etd, value); }
        }

        [VisibleInListView(false)]
        [ModelDefault("DisplayFormat", "{0:dd/MM/yyyy}")]
        [ModelDefault("EditMask", "dd/MM/yyyy")]
        [Appearance("SalesInvoiceLineCmpETAClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public DateTime ETA
        {
            get { return _eta; }
            set { SetPropertyValue("ETA", ref _eta, value); }
        }

        [ImmediatePostData()]
        [VisibleInListView(false)]
        [ModelDefault("DisplayFormat", "{0:dd/MM/yyyy}")]
        [ModelDefault("EditMask", "dd/MM/yyyy")]
        [Appearance("SalesInvoiceLineCmpDocDateClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public DateTime DocDate
        {
            get { return _docDate; }
            set
            {
                SetPropertyValue("DocDate", ref _docDate, value);
                if (!IsLoading)
                {
                    this.JournalMonth = this._docDate.Month;
                    this.JournalYear = this._docDate.Year;
                }
            }
        }

        [Browsable(false)]
        public int JournalMonth
        {
            get { return _journalMonth; }
            set { SetPropertyValue("JournalMonth", ref _journalMonth, value); }
        }

        [Browsable(false)]
        public int JournalYear
        {
            get { return _journalYear; }
            set { SetPropertyValue("JournalYear", ref _journalYear, value); }
        }

        [VisibleInListView(false)]
        [Appearance("SalesInvoiceLineCmpStatusClose", Enabled = false)]
        public Status Status
        {
            get { return _status; }
            set { SetPropertyValue("Status", ref _status, value); }
        }

        [VisibleInListView(false)]
        [ModelDefault("DisplayFormat", "{0:dd/MM/yyyy}")]
        [ModelDefault("EditMask", "dd/MM/yyyy")]
        [Appearance("SalesInvoiceLineCmpStatusDateClose", Enabled = false)]
        public DateTime StatusDate
        {
            get { return _statusDate; }
            set { SetPropertyValue("StatusDate", ref _statusDate, value); }
        }

        [VisibleInListView(false)]
        [Appearance("SalesInvoiceLineCmpPostedCountEnabled", Enabled = false)]
        public int PostedCount
        {
            get { return _postedCount; }
            set { SetPropertyValue("PostedCount", ref _postedCount, value); }
        }

        [VisibleInListView(false)]
        [Appearance("SalesInvoiceLineCmpSignCodeClose", Enabled = false)]
        public string SignCode
        {
            get { return _signCode; }
            set { SetPropertyValue("SignCode", ref _signCode, value); }
        }

        [ImmediatePostData()]
        [VisibleInListView(false)]
        [Association("SalesInvoiceCmp-SalesInvoiceLineCmps")]
        [Appearance("SalesInvoiceLineCmpSalesInvoiceCmpEnabled", Enabled = false)]
        [RuleRequiredField(DefaultContexts.Save)]
        public SalesInvoiceCmp SalesInvoiceCmp
        {
            get { return _salesInvoiceCmp; }
            set
            {
                SetPropertyValue("SalesInvoiceCmp", ref _salesInvoiceCmp, value);
                if (!IsLoading)
                {
                    if (this._salesInvoiceCmp != null)
                    {
                        if (this._salesInvoiceCmp.Company != null) { this.Company = this._salesInvoiceCmp.Company; }
                        if (this._salesInvoiceCmp.Workplace != null) { this.Workplace = this._salesInvoiceCmp.Workplace; }
                        if (this._salesInvoiceCmp.Division != null) { this.Division = this._salesInvoiceCmp.Division; }
                        if (this._salesInvoiceCmp.Department != null) { this.Department = this._salesInvoiceCmp.Department; }
                        if (this._salesInvoiceCmp.Section != null) { this.Section = this._salesInvoiceCmp.Section; }
                        if (this._salesInvoiceCmp.Employee != null) { this.Employee = this._salesInvoiceCmp.Employee; }
                        if (this._salesInvoiceCmp.PriceGroup != null) { this.PriceGroup = this._salesInvoiceCmp.PriceGroup; }
                        if (this._salesInvoiceCmp.Currency != null) { this.Currency = this._salesInvoiceCmp.Currency; }
                        this.ETD = this._salesInvoiceCmp.ETD;
                        this.ETA = this._salesInvoiceCmp.ETA;
                        this.DocDate = this._salesInvoiceCmp.DocDate;
                        this.JournalMonth = this._salesInvoiceCmp.JournalMonth;
                        this.JournalYear = this._salesInvoiceCmp.JournalYear;
                    }
                }
            }
        }

        [Browsable(false)]
        public SalesInvoiceLine SalesInvoiceLine
        {
            get { return _salesInvoiceLine; }
            set { SetPropertyValue("SalesInvoiceLine", ref _salesInvoiceLine, value); }
        }

        //AuditTrail
        [CollectionOperationSet(AllowAdd = false, AllowRemove = true)]
        public XPCollection<AuditDataItemPersistent> ChangeHistory
        {
            get
            {
                if (changeHistory == null)
                {
                    changeHistory = AuditedObjectWeakReference.GetAuditTrail(Session, this);
                }
                return changeHistory;
            }
        }

        #endregion Field

        //====================================== Code Only ======================================================

        #region No

        public void UpdateNo()
        {
            try
            {
                if (!IsLoading && Session.IsNewObject(this))
                {
                    if (this.SalesInvoiceCmp != null)
                    {
                        object _makRecord = Session.Evaluate<SalesInvoiceLineCmp>(CriteriaOperator.Parse("Max(No)"), CriteriaOperator.Parse("SalesInvoiceCmp=?", this.SalesInvoiceCmp));
                        this.No = Convert.ToInt32(_makRecord) + 1;
                        this.Save();
                        RecoveryUpdateNo();
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = SalesInvoiceLineCmp " + ex.ToString());
            }
        }

        public void RecoveryUpdateNo()
        {
            try
            {
                if (this.SalesInvoiceCmp != null)
                {
                    SalesInvoiceCmp _numHeader = Session.FindObject<SalesInvoiceCmp>
                                                (new BinaryOperator("Code", this.SalesInvoiceCmp.Code));

                    XPCollection<SalesInvoiceLineCmp> _numLines = new XPCollection<SalesInvoiceLineCmp>
                                                (Session, new BinaryOperator("SalesInvoiceCmp", _numHeader),
                                                new SortProperty("No", DevExpress.Xpo.DB.SortingDirection.Ascending));
                    if (_numLines != null)
                    {
                        int i1 = 0;
                        foreach (SalesInvoiceLineCmp _numLine in _numLines)
                        {
                            i1 += 1;
                            _numLine.No = i1;
                            _numLine.Save();
                        }
                        i1 = 1;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = SalesInvoiceLineCmp " + ex.ToString());
            }
        }

        public void RecoveryDeleteNo()
        {
            try
            {
                if (this.SalesInvoiceCmp != null)
                {
                    SalesInvoiceCmp _numHeader = Session.FindObject<SalesInvoiceCmp>
                                                (new BinaryOperator("Code", this.SalesInvoiceCmp.Code));

                    XPCollection<SalesInvoiceLineCmp> _numLines = new XPCollection<SalesInvoiceLineCmp>
                                                (Session, new GroupOperator(GroupOperatorType.And,
                                                 new BinaryOperator("This", this, BinaryOperatorType.NotEqual),
                                                 new BinaryOperator("SalesInvoiceCmp", _numHeader)),
                                                 new SortProperty("No", DevExpress.Xpo.DB.SortingDirection.Ascending));
                    if (_numLines != null)
                    {
                        int i = 0;
                        foreach (SalesInvoiceLineCmp _numLine in _numLines)
                        {
                            i += 1;
                            _numLine.No = i;
                            _numLine.Save();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = SalesInvoiceLineCmp " + ex.ToString());
            }
        }

        #endregion No

        #region Set

        private void SetTotalQty()
        {
            try
            {
                double _locInvLineTotal = 0;
                DateTime now = DateTime.Now;
                if (this.SalesInvoiceCmp != null)
                {
                    ItemUnitOfMeasure _locItemUOM = Session.FindObject<ItemUnitOfMeasure>
                                                        (new GroupOperator(GroupOperatorType.And,
                                                         new BinaryOperator("Item", this.Item),
                                                         new BinaryOperator("UOM", this.UOM),
                                                         new BinaryOperator("DefaultUOM", this.DUOM),
                                                         new BinaryOperator("Active", true)));
                    if (_locItemUOM != null)
                    {
                        if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                        {
                            _locInvLineTotal = this.Qty * _locItemUOM.DefaultConversion + this.DQty;
                        }
                        else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                        {
                            _locInvLineTotal = this.Qty / _locItemUOM.Conversion + this.DQty;
                        }
                        else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                        {
                            _locInvLineTotal = this.Qty + this.DQty;
                        }

                        this.TQty = _locInvLineTotal;
                    }
                    else
                    {
                        _locInvLineTotal = this.Qty + this.DQty;
                        this.TQty = _locInvLineTotal;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = SalesInvoiceLineCmp " + ex.ToString());
            }
        }

        private void SetTotalAmount()
        {
            try
            {
                _globFunc = new GlobalFunction();

                if (_uAmount >= 0 && _txAmount >= 0 && _discAmount >= 0)
                {
                    if (!IsLoading)
                    {
                        if (this.Tax != null)
                        {
                            if (this.Tax.TaxNature == TaxNature.Increase)
                            {
                                if (_globFunc.GetRoundingList(Session, ObjectList.SalesInvoiceLineCmp, FieldName.TAmount) == true)
                                {
                                    this.TAmount = _globFunc.GetRoundUp(Session, (this.TUAmount + this.TxAmount - this.DiscAmount), ObjectList.SalesInvoiceLineCmp, FieldName.TAmount);
                                }
                                else
                                {
                                    this.TAmount = this.TUAmount + this.TxAmount - this.DiscAmount;
                                }
                            }
                            else if (this.Tax.TaxNature == TaxNature.Decrease)
                            {
                                if (_globFunc.GetRoundingList(Session, ObjectList.SalesInvoiceLineCmp, FieldName.TAmount) == true)
                                {
                                    this.TAmount = _globFunc.GetRoundUp(Session, (this.TUAmount - this.TxAmount - this.DiscAmount), ObjectList.SalesInvoiceLineCmp, FieldName.TAmount);
                                }
                                else
                                {
                                    this.TAmount = this.TUAmount - this.TxAmount - this.DiscAmount;
                                }
                            }
                            else if (this.Tax.TaxNature == TaxNature.None)
                            {
                                if (_globFunc.GetRoundingList(Session, ObjectList.SalesInvoiceLineCmp, FieldName.TAmount) == true)
                                {
                                    this.TAmount = _globFunc.GetRoundUp(Session, (this.TUAmount - this.DiscAmount), ObjectList.SalesInvoiceLineCmp, FieldName.TAmount);
                                }
                                else
                                {
                                    this.TAmount = this.TUAmount - this.DiscAmount;
                                }
                            }
                            else
                            {
                                if (_globFunc.GetRoundingList(Session, ObjectList.SalesInvoiceLineCmp, FieldName.TAmount) == true)
                                {
                                    this.TAmount = _globFunc.GetRoundUp(Session, (this.TUAmount + this.TxAmount - this.DiscAmount), ObjectList.SalesInvoiceLineCmp, FieldName.TAmount);
                                }
                                else
                                {
                                    this.TAmount = this.TUAmount + this.TxAmount - this.DiscAmount;
                                }
                            }
                        }
                        else
                        {
                            this.TAmount = this.TUAmount + this.TxAmount - this.DiscAmount;
                        }
                    }
                }
                else
                {
                    this.TAmount = this.TUAmount + this.TxAmount - this.DiscAmount;
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = SalesInvoiceLineCmp " + ex.ToString());
            }
        }

        private void SetTotalUnitAmount()
        {
            try
            {
                _globFunc = new GlobalFunction();

                if (_tQty >= 0 & _uAmount >= 0)
                {
                    if (_globFunc.GetRoundingList(Session, ObjectList.SalesInvoiceLineCmp, FieldName.TUAmount) == true)
                    {
                        this.TUAmount = _globFunc.GetRoundUp(Session, (this.TQty * this.UAmount), ObjectList.SalesInvoiceLineCmp, FieldName.TUAmount);
                    }
                    else
                    {
                        this.TUAmount = this.TQty * this.UAmount;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = SalesInvoiceLineCmp " + ex.ToString());
            }
        }

        private void SetNormalTax()
        {
            try
            {
                if (_tax != null && _txValue > 0 && _txAmount > 0)
                {
                    this.Tax = this.Tax;
                    this.TxValue = this.TxValue;
                    this.TxAmount = this.TxAmount;
                }
                else
                {
                    this.TxValue = 0;
                    this.TxAmount = 0;
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = SalesInvoiceLineCmp " + ex.ToString());
            }
        }

        private void SetPriceLine()
        {
            try
            {
                if (this.Company != null && this.Workplace != null && this.Item != null && this.PriceGroup != null)
                {
                    PriceLine _locPriceLine = Session.FindObject<PriceLine>(new GroupOperator(GroupOperatorType.And,
                                                                            new BinaryOperator("Company", this.Company),
                                                                            new BinaryOperator("Workplace", this.Workplace),
                                                                            new BinaryOperator("Item", this.Item),
                                                                            new BinaryOperator("PriceGroup", this.PriceGroup),
                                                                            new BinaryOperator("Active", true)));
                    if (_locPriceLine != null)
                    {
                        this.PriceLine = _locPriceLine;
                        this.UAmount = _locPriceLine.Amount2a;
                    }
                    else
                    {
                        this.PriceLine = null;
                        this.UAmount = 0;
                    }
                }
                else
                {
                    this.PriceLine = null;
                    this.UAmount = 0;
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = SalesInvoiceLineCmp " + ex.ToString());
            }
        }

        private void SetUOM()
        {
            try
            {
                if (this.Company != null && this.Item != null && this.DUOM != null)
                {
                    ItemUnitOfMeasure _locIUOM = Session.FindObject<ItemUnitOfMeasure>(new GroupOperator(GroupOperatorType.And,
                                                                            new BinaryOperator("Company", this.Company),
                                                                            new BinaryOperator("Item", this.Item),
                                                                            new BinaryOperator("Default", true),
                                                                            new BinaryOperator("Active", true)));
                    if (_locIUOM != null)
                    {
                        if (_locIUOM.UOM != null)
                        {
                            this.UOM = _locIUOM.UOM;
                        }
                        else
                        {
                            this.UOM = null;
                        }

                    }
                    else
                    {
                        this.UOM = null;
                    }
                }
                else
                {
                    this.UOM = null;
                }

            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = SalesInvoiceLineCmp " + ex.ToString());
            }
        }

        private void SetDiscountRule()
        {
            try
            {
                if (this.Company != null && this.Workplace != null && this.Item != null && this.PriceGroup != null)
                {
                    DiscountRule _locDiscountRule2a = Session.FindObject<DiscountRule>
                                                            (new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("Company", this.Company),
                                                            new BinaryOperator("Workplace", this.Workplace),
                                                            new BinaryOperator("PriceGroup", this.PriceGroup),
                                                            new BinaryOperator("AllItem", true),
                                                            new BinaryOperator("RuleType", RuleType.Rule2),
                                                            new BinaryOperator("Active", true)));
                    if (_locDiscountRule2a != null)
                    {
                        this.DiscountRule = _locDiscountRule2a;
                    }
                    else
                    {
                        DiscountRule _locDiscountRule2b = Session.FindObject<DiscountRule>
                                                            (new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("Company", this.Company),
                                                            new BinaryOperator("Workplace", this.Workplace),
                                                            new BinaryOperator("PriceGroup", this.PriceGroup),
                                                            new BinaryOperator("MpItem", this.Item),
                                                            new BinaryOperator("RuleType", RuleType.Rule2),
                                                            new BinaryOperator("Active", true)));
                        if (_locDiscountRule2b != null)
                        {
                            this.DiscountRule = _locDiscountRule2b;
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = SalesInvoiceLineCmp " + ex.ToString());
            }
        }

        private void SetLocation(Employee _locPIC)
        {
            try
            {
                DateTime now = DateTime.Now;
                if (this.Company != null && this.Workplace != null)
                {
                    SalesmanLocationSetup _locSalesmanLocationSetup = Session.FindObject<SalesmanLocationSetup>
                                                        (new GroupOperator(GroupOperatorType.And,
                                                        new BinaryOperator("Company", this.Company),
                                                        new BinaryOperator("Workplace", this.Workplace),
                                                        new BinaryOperator("Salesman", _locPIC),
                                                        new BinaryOperator("Active", true),
                                                        new BinaryOperator("Default", true)));
                    if (_locSalesmanLocationSetup != null)
                    {
                        if (_locSalesmanLocationSetup.Location != null)
                        {
                            this.Location = _locSalesmanLocationSetup.Location;
                        }
                        else
                        {
                            this.Location = null;
                        }
                    }
                    else
                    {
                        this.Location = null;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Business Object = SalesInvoiceLineCmp ", ex.ToString());
            }

        }

        private void SetBegInv(Location _locLocation)
        {
            try
            {
                DateTime now = DateTime.Now;
                if (this.Company != null && this.Workplace != null)
                {
                    BeginningInventory _locBegInv = Session.FindObject<BeginningInventory>
                                                        (new GroupOperator(GroupOperatorType.And,
                                                        new BinaryOperator("Company", this.Company),
                                                        new BinaryOperator("Workplace", this.Workplace),
                                                        new BinaryOperator("Location", _locLocation),
                                                        new BinaryOperator("LocationType", LocationType.Main),
                                                        new BinaryOperator("StockType", StockType.Good),
                                                        new BinaryOperator("Active", true)));
                    if (_locBegInv != null)
                    {
                        this.BegInv = _locBegInv;
                    }
                    else
                    {
                        this.BegInv = null;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Business Object = SalesInvoiceLineCmp ", ex.ToString());
            }
        }

        private void SetBegInvLine(Item _locItem)
        {
            try
            {
                DateTime now = DateTime.Now;
                if (this.Company != null && this.Workplace != null && this.BegInv != null)
                {
                    BeginningInventoryLine _locBegInvLine = Session.FindObject<BeginningInventoryLine>(
                                                                            new GroupOperator(GroupOperatorType.And,
                                                                            new BinaryOperator("Company", this.Company),
                                                                            new BinaryOperator("Workplace", this.Workplace),
                                                                            new BinaryOperator("BeginningInventory", this.BegInv),
                                                                            new BinaryOperator("Item", _locItem),
                                                                            new BinaryOperator("StockType", StockType.Good),
                                                                            new BinaryOperator("StockGroup", StockGroup.Normal),
                                                                            new BinaryOperator("LocationType", LocationType.Main),
                                                                            new BinaryOperator("Lock", false),
                                                                            new BinaryOperator("Active", true)));
                    if (_locBegInvLine != null)
                    {
                        this.BegInvLine = _locBegInvLine;
                    }
                    else
                    {
                        this.BegInvLine = null;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Business Object = SalesInvoiceLineCmp ", ex.ToString());
            }
        }

        #endregion Set

    }
}