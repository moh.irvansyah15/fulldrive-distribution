﻿using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;

using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.ConditionalAppearance;

namespace FullDrive.Module.BusinessObjects
{
    [DefaultClassOptions]
    [DefaultProperty("Code")]
    [NavigationItem("Inventory")]
    [RuleCombinationOfPropertiesIsUnique("InventoryPurchaseCollectionRuleUnique", DefaultContexts.Save, "Code")]
    //[ImageName("BO_Contact")]
    //[DefaultProperty("DisplayMemberNameForLookupEditorsOfThisType")]
    //[DefaultListViewOptions(MasterDetailMode.ListViewOnly, false, NewItemRowPosition.None)]
    //[Persistent("DatabaseTableName")]
    // Specify more UI options using a declarative approach (https://documentation.devexpress.com/#eXpressAppFramework/CustomDocument112701).
    public class InventoryPurchaseCollection : BaseObject
    { // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).
        private bool _activationPosting;
        private string _code;
        private XPCollection<PurchaseOrder> _availablePurchaseOrder;
        private PurchaseOrder _purchaseOrder;
        private InventoryTransferIn _inventoryTransferIn;
        #region InitialOrganization
        private Company _company;
        private Workplace _workplace;
        private Division _division;
        private Department _department;
        private Section _section;
        private Employee _employee;
        #endregion InitialOrganization
        private Status _status;
        private DateTime _statusDate;        
        private int _postedCount;
        private string _localUserAccess;
        private GlobalFunction _globFunc;
        //AuditTrail
        private XPCollection<AuditDataItemPersistent> changeHistory;

        public InventoryPurchaseCollection(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if (!IsLoading)
            {
                if(Session.IsNewObject(this))
                {
                    _globFunc = new GlobalFunction();
                    DateTime now = DateTime.Now;
                    #region UserAccess
                    _localUserAccess = SecuritySystem.CurrentUserName;
                    UserAccess _locUserAccess = Session.FindObject<UserAccess>(new BinaryOperator("UserName", _localUserAccess));
                    if (_locUserAccess != null)
                    {
                        if (_locUserAccess.Employee != null)
                        {
                            if (_locUserAccess.Employee.Company != null && _locUserAccess.Employee.Workplace != null)
                            {
                                this.Code = _globFunc.GetNumberingUnlockOptimisticRecordBasedOrganization(this.Session.DataLayer, ObjectList.InventoryPurchaseCollection, _locUserAccess.Employee.Company, _locUserAccess.Employee.Workplace);
                            }
                            else
                            {
                                this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.InventoryPurchaseCollection);
                            }
                        }
                    }
                    #endregion UserAccess
                    this.Status = Status.Open;
                    this.StatusDate = now;
                }               
            }
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).
        }

        #region Field

        [Browsable(false)]
        public bool ActivationPosting
        {
            get { return _activationPosting; }
            set { SetPropertyValue("ActivationPosting", ref _activationPosting, value); }
        }

        [RuleRequiredField(DefaultContexts.Save)]
        [Appearance("InventoryPurchaseCollectionCodeClose", Enabled = false)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue("Code", ref _code, value); }
        }

        [Browsable(false)]
        public XPCollection<PurchaseOrder> AvailablePurchaseOrder
        {
            get
            {
                if(!IsLoading)
                {
                    string _beginString = null;
                    string _endString = null;
                    string _fullString = null;

                    if (this.InventoryTransferIn != null)
                    {
                        if (this.InventoryTransferIn.BusinessPartner != null)
                        {
                            XPQuery<PurchaseOrderMonitoring> _purchaseOrderMonitoringsQuery = new XPQuery<PurchaseOrderMonitoring>(Session);

                            var _purchaseOrderMonitorings = from pom in _purchaseOrderMonitoringsQuery
                                                            where ((pom.Status == Status.Open || pom.Status == Status.Posted)
                                                            && pom.PurchaseOrder.BuyFromVendor == this.InventoryTransferIn.BusinessPartner
                                                            && pom.InventoryTransferIn == null)
                                                            group pom by pom.PurchaseOrder into g
                                                            select new { PurchaseOrder = g.Key };

                            if (_purchaseOrderMonitorings != null && _purchaseOrderMonitorings.Count() > 0)
                            {
                                List<string> _stringPOM = new List<string>();

                                foreach (var _purchaseOrderMonitoring in _purchaseOrderMonitorings)
                                {
                                    if (_purchaseOrderMonitoring != null)
                                    {
                                        _stringPOM.Add(_purchaseOrderMonitoring.PurchaseOrder.Code);
                                    }
                                }

                                IEnumerable<string> _stringArrayPOMDistinct = _stringPOM.Distinct();
                                string[] _stringArrayPOMList = _stringArrayPOMDistinct.ToArray();
                                if (_stringArrayPOMList.Length == 1)
                                {
                                    for (int i = 0; i < _stringArrayPOMList.Length; i++)
                                    {
                                        if (i == 0)
                                        {
                                            _beginString = "[Code]=='" + _stringArrayPOMList[i] + "'";
                                        }
                                    }
                                }
                                else if (_stringArrayPOMList.Length > 1)
                                {
                                    for (int i = 0; i < _stringArrayPOMList.Length; i++)
                                    {
                                        if (i == 0)
                                        {
                                            _beginString = "[Code]=='" + _stringArrayPOMList[i] + "'";
                                        }
                                        else
                                        {
                                            _endString = _endString + " OR [Code]=='" + _stringArrayPOMList[i] + "'";
                                        }
                                    }
                                }
                                _fullString = _beginString + _endString;

                                if (_fullString != null)
                                {
                                    _availablePurchaseOrder = new XPCollection<PurchaseOrder>(Session, CriteriaOperator.Parse(_fullString));
                                }
                            }
                        }
                    }
                }
                return _availablePurchaseOrder;
            }
        }

        [RuleRequiredField(DefaultContexts.Save)]
        [ImmediatePostData()]
        [DataSourceProperty("AvailablePurchaseOrder", DataSourcePropertyIsNullMode.SelectNothing)]
        public PurchaseOrder PurchaseOrder
        {
            get { return _purchaseOrder; }
            set { SetPropertyValue("PurchaseOrder", ref _purchaseOrder, value); }
        }

        [ImmediatePostData()]
        [Appearance("InventoryPurchaseCollectionInventoryTransferInClose", Enabled = false)]
        [Association("InventoryTransferIn-InventoryPurchaseCollections")]
        public InventoryTransferIn InventoryTransferIn
        {
            get { return _inventoryTransferIn; }
            set
            {
                SetPropertyValue("InventoryTransferIn", ref _inventoryTransferIn, value);
                if(!IsLoading)
                {
                    if(this._inventoryTransferIn != null)
                    {
                        if(this._inventoryTransferIn.Company != null)
                        {
                            this.Company = this._inventoryTransferIn.Company;
                        }
                        if(this._inventoryTransferIn.Workplace != null)
                        {
                            this.Workplace = this._inventoryTransferIn.Workplace;
                        }
                        if (this._inventoryTransferIn.Division != null)
                        {
                            this.Division = this._inventoryTransferIn.Division;
                        }
                        if (this._inventoryTransferIn.Department != null)
                        {
                            this.Department = this._inventoryTransferIn.Department;
                        }
                        if (this._inventoryTransferIn.Section != null)
                        {
                            this.Section = this._inventoryTransferIn.Section;
                        }
                        if (this._inventoryTransferIn.Employee != null)
                        {
                            this.Employee = this._inventoryTransferIn.Employee;
                        }
                    }
                }
            }
        }

        #region Organization

        [Appearance("InventoryPurchaseCollectionCompanyClose", Enabled = false)]
        public Company Company
        {
            get { return _company; }
            set { SetPropertyValue("Company", ref _company, value); }
        }

        [Appearance("InventoryPurchaseCollectionWorkplaceClose", Enabled = false)]
        public Workplace Workplace
        {
            get { return _workplace; }
            set { SetPropertyValue("Workplace", ref _workplace, value); }
        }

        [Browsable(false)]
        public Division Division
        {
            get { return _division; }
            set { SetPropertyValue("Division", ref _division, value); }
        }

        [Browsable(false)]
        public Department Department
        {
            get { return _department; }
            set { SetPropertyValue("Department", ref _department, value); }
        }

        [Browsable(false)]
        public Section Section
        {
            get { return _section; }
            set { SetPropertyValue("Section", ref _section, value); }
        }

        [Browsable(false)]
        public Employee Employee
        {
            get { return _employee; }
            set { SetPropertyValue("Employee", ref _employee, value); }
        }

        #endregion Organization

        [VisibleInListView(false)]
        [Appearance("InventoryPurchaseCollectionStatusClose", Enabled = false)]
        public Status Status
        {
            get { return _status; }
            set { SetPropertyValue("Status", ref _status, value); }
        }

        [VisibleInListView(false)]
        [ModelDefault("DisplayFormat", "{0:dd/MM/yyyy}")]
        [ModelDefault("EditMask", "dd/MM/yyyy")]
        [Appearance("InventoryPurchaseCollectionStatusDateClose", Enabled = false)]
        public DateTime StatusDate
        {
            get { return _statusDate; }
            set { SetPropertyValue("StatusDate", ref _statusDate, value); }
        }

        [VisibleInListView(false)]
        [Appearance("InventoryPurchaseCollectionPostedCountEnabled", Enabled = false)]
        public int PostedCount
        {
            get { return _postedCount; }
            set { SetPropertyValue("PostedCount", ref _postedCount, value); }
        }

        //AuditTrail
        [CollectionOperationSet(AllowAdd = false, AllowRemove = true)]
        public XPCollection<AuditDataItemPersistent> ChangeHistory
        {
            get
            {
                if (changeHistory == null)
                {
                    changeHistory = AuditedObjectWeakReference.GetAuditTrail(Session, this);
                }
                return changeHistory;
            }
        }

        #endregion Field
        
    }
}