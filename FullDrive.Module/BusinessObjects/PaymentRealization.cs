﻿using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;

using DevExpress.ExpressApp.ConditionalAppearance;
using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.Xpo;
using System.Collections;
using System.IO;

namespace FullDrive.Module.BusinessObjects
{
    [DefaultClassOptions]
    [DefaultProperty("Code")]
    [NavigationItem("Cash Advance")]
    [RuleCombinationOfPropertiesIsUnique("PaymentRealizationRuleUnique", DefaultContexts.Save, "Code")]
    //[ImageName("BO_Contact")]
    //[DefaultProperty("DisplayMemberNameForLookupEditorsOfThisType")]
    //[DefaultListViewOptions(MasterDetailMode.ListViewOnly, false, NewItemRowPosition.None)]
    //[Persistent("DatabaseTableName")]
    // Specify more UI options using a declarative approach (https://documentation.devexpress.com/#eXpressAppFramework/CustomDocument112701).
    public class PaymentRealization : BaseObject
    { // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).
        #region InitialSystem
        private bool _activationPosting;
        private bool _activeApproved1;
        private bool _activeApproved2;
        private bool _activeApproved3;
        private string _code;
        #endregion InitialSystem
        private string _name;
        #region InitialOrganization
        private Company _company;
        private XPCollection<Workplace> _availableWorkplace;
        private Workplace _workplace;
        private Division _division;
        private Department _department;
        private Section _section;
        private Employee _employee;
        private Division _divisionCA;
        private Department _departmentCA;
        private Section _sectionCA;
        private Employee _employeeCA;
        private Position _position;
        #endregion InitialOrganization
        private double _totAmount;
        private double _totAmountRealized;
        private double _totAmountRefund;
        private DateTime _docDate;
        private int _journalMonth;
        private int _journalYear;
        private Status _status;
        private DateTime _statusDate;
        private string _localUserAccess;
        private GlobalFunction _globFunc;       
        //Active Approved
        
        //AuditTrail
        private XPCollection<AuditDataItemPersistent> changeHistory;

        public PaymentRealization(Session session)
            : base(session)
        {
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if(!IsLoading)
            {
                if (Session.IsNewObject(this))
                {
                    _globFunc = new GlobalFunction();
                    DateTime now = DateTime.Now;
                    #region UserAccess
                    _localUserAccess = SecuritySystem.CurrentUserName;
                    UserAccess _locUserAccess = Session.FindObject<UserAccess>(new BinaryOperator("UserName", _localUserAccess));
                    if (_locUserAccess != null)
                    {
                        if (_locUserAccess.Employee != null)
                        {
                            if (_locUserAccess.Employee.Company != null && _locUserAccess.Employee.Workplace != null)
                            {
                                this.Code = _globFunc.GetNumberingUnlockOptimisticRecordBasedOrganization(this.Session.DataLayer, ObjectList.PaymentRealization, _locUserAccess.Employee.Company, _locUserAccess.Employee.Workplace);
                            }
                            else
                            {
                                this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.PaymentRealization);
                            }
                            if (_locUserAccess.Employee.Company != null)
                            {
                                this.Company = _locUserAccess.Employee.Company;
                            }
                            if (_locUserAccess.Employee.Workplace != null)
                            {
                                this.Workplace = _locUserAccess.Employee.Workplace;
                            }
                            if (_locUserAccess.Employee.Division != null)
                            {
                                Division = _locUserAccess.Employee.Division;
                                DivisionCA = _locUserAccess.Employee.Division;
                            }
                            if (_locUserAccess.Employee.Department != null)
                            {
                                Department = _locUserAccess.Employee.Department;
                                DepartmentCA = _locUserAccess.Employee.Department;
                            }
                            if (_locUserAccess.Employee.Section != null)
                            {
                                Section = _locUserAccess.Employee.Section;
                                SectionCA = _locUserAccess.Employee.Section;
                            }
                            if (_locUserAccess.Employee.Position != null)
                            {
                                Position = _locUserAccess.Employee.Position;
                            }
                           
                            this.Employee = _locUserAccess.Employee;
                            this.EmployeeCA = _locUserAccess.Employee;
                        }
                    }
                    #endregion UserAccess
                    this.Status = Status.Open;
                    this.StatusDate = now;
                }
            }
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).
        }

        #region Field

        #region System

        [Browsable(false)]
        public bool ActivationPosting
        {
            get { return _activationPosting; }
            set { SetPropertyValue("ActivationPosting", ref _activationPosting, value); }
        }

        [Browsable(false)]
        [Appearance("PaymentRealizationActiveApproved1Close", Enabled = false)]
        public bool ActiveApproved1
        {
            get { return _activeApproved1; }
            set { SetPropertyValue("ActiveApproved1", ref _activeApproved1, value); }
        }

        [Browsable(false)]
        [Appearance("PaymentRealizationActiveApproved2Close", Enabled = false)]
        public bool ActiveApproved2
        {
            get { return _activeApproved2; }
            set { SetPropertyValue("ActiveApproved2", ref _activeApproved2, value); }
        }

        [Browsable(false)]
        [Appearance("PaymentRealizationActiveApproved3Close", Enabled = false)]
        public bool ActiveApproved3
        {
            get { return _activeApproved3; }
            set { SetPropertyValue("ActiveApproved3", ref _activeApproved3, value); }
        }

        [ImmediatePostData()]
        [Appearance("PaymentRealizationCodeClose", Enabled = false)]
        [Appearance("PaymentRealizationRedColor", Criteria = "ActiveApproved1 = true", BackColor = "#fd79a8")]
        [Appearance("PaymentRealizationYellowColor", Criteria = "ActiveApproved2 = true", BackColor = "#ffeaa7")]
        [Appearance("PaymentRealizationGreenOColor", Criteria = "ActiveApproved3 = true", BackColor = "#81ecec")]
        [RuleRequiredField(DefaultContexts.Save)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue("Code", ref _code, value); }
        }

        #endregion System

        #region Organization

        [RuleRequiredField(DefaultContexts.Save)]
        [DataSourceCriteria("Active = true"), ImmediatePostData()]
        [Appearance("PaymentRealizationCompanyClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Company Company
        {
            get { return _company; }
            set { SetPropertyValue("Company", ref _company, value); }
        }

        [Browsable(false)]
        public XPCollection<Workplace> AvailableWorkplace
        {
            get
            {
                if (!IsLoading)
                {
                    if (this.Company != null)
                    {
                        _localUserAccess = SecuritySystem.CurrentUserName;
                        UserAccess _locUserAccess = Session.FindObject<UserAccess>(new BinaryOperator("UserName", _localUserAccess));
                        if (_locUserAccess != null)
                        {
                            string _beginString = null;
                            string _endString = null;
                            string _fullString = null;
                            List<string> _stringWorkplace = new List<string>();

                            XPCollection<OrganizationSetupDetail> _locOrganizationSetupDetails = new XPCollection<OrganizationSetupDetail>
                                                                            (Session, new GroupOperator(GroupOperatorType.And,
                                                                            new BinaryOperator("Company", this.Company),
                                                                            new BinaryOperator("UserAccess", _locUserAccess),
                                                                            new BinaryOperator("OpenWorkplace", true),
                                                                            new BinaryOperator("Active", true)));

                            if (_locOrganizationSetupDetails != null && _locOrganizationSetupDetails.Count() > 0)
                            {
                                foreach (OrganizationSetupDetail _locOrganizationSetupDetail in _locOrganizationSetupDetails)
                                {
                                    if (_locOrganizationSetupDetail.Workplace != null)
                                    {
                                        if (_locOrganizationSetupDetail.Workplace.Code != null)
                                        {
                                            _stringWorkplace.Add(_locOrganizationSetupDetail.Workplace.Code);
                                        }
                                    }
                                }

                                #region Workplace
                                IEnumerable<string> _stringArrayWorkplaceDistinct = _stringWorkplace.Distinct();
                                string[] _stringArrayWorkplaceList = _stringArrayWorkplaceDistinct.ToArray();
                                if (_stringArrayWorkplaceList.Length == 1)
                                {
                                    for (int i = 0; i < _stringArrayWorkplaceList.Length; i++)
                                    {
                                        Workplace _locWorkplace = Session.FindObject<Workplace>(new BinaryOperator("Code", _stringArrayWorkplaceList[i]));
                                        if (_locWorkplace != null)
                                        {
                                            if (i == 0)
                                            {
                                                _beginString = "[Code]=='" + _locWorkplace.Code + "'";
                                            }
                                        }
                                    }
                                }
                                else if (_stringArrayWorkplaceList.Length > 1)
                                {
                                    for (int i = 0; i < _stringArrayWorkplaceList.Length; i++)
                                    {
                                        Workplace _locWorkplace = Session.FindObject<Workplace>(new BinaryOperator("Code", _stringArrayWorkplaceList[i]));
                                        if (_locWorkplace != null)
                                        {
                                            if (i == 0)
                                            {
                                                _beginString = "[Code]=='" + _locWorkplace.Code + "'";
                                            }
                                            else
                                            {
                                                _endString = _endString + " OR [Code]=='" + _locWorkplace.Code + "'";
                                            }
                                        }

                                    }
                                }
                                _fullString = _beginString + _endString;


                                if (_fullString != null)
                                {
                                    _availableWorkplace = new XPCollection<Workplace>(Session, CriteriaOperator.Parse(_fullString));
                                }

                                #endregion Workplace
                            }
                            else
                            {
                                OrganizationSetupDetail _locOrgSetDetail = Session.FindObject<OrganizationSetupDetail>
                                                                            (new GroupOperator(GroupOperatorType.And,
                                                                            new BinaryOperator("Company", this.Company),
                                                                            new BinaryOperator("UserAccess", _locUserAccess),
                                                                            new BinaryOperator("AllWorkplace", true),
                                                                            new BinaryOperator("Active", true)));
                                if (_locOrgSetDetail != null)
                                {
                                    _availableWorkplace = new XPCollection<Workplace>(Session,
                                                        new GroupOperator(GroupOperatorType.And,
                                                        new BinaryOperator("Company", this.Company),
                                                        new BinaryOperator("Active", true)));
                                }
                            }
                        }
                    }
                }

                return _availableWorkplace;

            }
        }

        [ImmediatePostData()]
        [DataSourceProperty("AvailableWorkplace")]
        [Appearance("PaymentRealizationWorkplaceClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Workplace Workplace
        {
            get { return _workplace; }
            set { SetPropertyValue("Workplace", ref _workplace, value); }
        }

        [Browsable(false)]
        public Division Division
        {
            get { return _division; }
            set { SetPropertyValue("Division", ref _division, value); }
        }

        [Browsable(false)]
        public Department Department
        {
            get { return _department; }
            set { SetPropertyValue("Department", ref _department, value); }
        }

        [Browsable(false)]
        public Section Section
        {
            get { return _section; }
            set { SetPropertyValue("Section", ref _section, value); }
        }

        [Browsable(false)]
        public Employee Employee
        {
            get { return _employee; }
            set
            {
                SetPropertyValue("Employee", ref _employee, value);
                if (!IsLoading)
                {
                    UpdateName();
                }
            }
        }

        [DataSourceCriteria("Company = '@This.Company' And Active = true "), ImmediatePostData()]
        [Appearance("PaymentRealizationDivisionCAClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Division DivisionCA
        {
            get { return _divisionCA; }
            set { SetPropertyValue("DivisionCA", ref _divisionCA, value); }
        }

        [DataSourceCriteria("Company = '@This.Company' And Division = '@This.DivisionCA' And Active = true"), ImmediatePostData()]
        [Appearance("PaymentRealizationDepartmentCAClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Department DepartmentCA
        {
            get { return _departmentCA; }
            set { SetPropertyValue("DepartmentCA", ref _departmentCA, value); }
        }

        [DataSourceCriteria("Company = '@This.Company' And Department = '@This.DepartmentCA' And Active = true "), ImmediatePostData()]
        [Appearance("PaymentRealizationSectionCAClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Section SectionCA
        {
            get { return _sectionCA; }
            set { SetPropertyValue("SectionCA", ref _sectionCA, value); }
        }

        [ImmediatePostData()]
        [DataSourceCriteria("Company = '@This.Company' And Workplace = '@This.Workplace' And Division = '@This.DivisionCA' And Department = '@This.DepartmentCA' And Section = '@This.SectionCA' And Active = true ")]
        [Appearance("PaymentRealizationEmployeeCAClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Employee EmployeeCA
        {
            get { return _employeeCA; }
            set
            {
                SetPropertyValue("EmployeeCA", ref _employeeCA, value);
                if (!IsLoading)
                {
                    if(this._employeeCA != null)
                    {
                        if(this._employeeCA.Position != null)
                        {
                            this.Position = this._employeeCA.Position;
                        }
                    }
                }
            }
        }

        #endregion Organization

        [Appearance("PaymentRealizationPositionClose", Enabled = false)]
        public Position Position
        {
            get { return _position; }
            set { SetPropertyValue("Position", ref _position, value); }
        }

        [ImmediatePostData()]
        [Appearance("PaymentRealizationNameClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public string Name
        {
            get { return _name; }
            set { SetPropertyValue("Name", ref _name, value); }
        }

        [Appearance("PaymentRealizationTotAmountClose", Enabled = false)]
        public double TotAmount
        {
            get { return _totAmount; }
            set { SetPropertyValue("TotAmount", ref _totAmount, value); }
            
        }

        [Appearance("PaymentRealizationTotAmountRealizedClose", Enabled = false)]
        public double TotAmountRealized
        {
            get { return _totAmountRealized;  }
            set { SetPropertyValue("TotAmountRealized", ref _totAmountRealized, value); }
        }

        [Appearance("PaymentRealizationTotAmountRefundClose", Enabled = false)]
        public double TotAmountRefund
        {
            get { return _totAmountRefund; }
            set { SetPropertyValue("TotAmountRefund", ref _totAmountRefund, value); }
        }

        [Browsable(false)]
        [ImmediatePostData()]
        [ModelDefault("DisplayFormat", "{0:dd/MM/yyyy}")]
        [ModelDefault("EditMask", "dd/MM/yyyy")]
        [Appearance("PaymentRealizationLineDocDateClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public DateTime DocDate
        {
            get { return _docDate; }
            set
            {
                SetPropertyValue("DocDate", ref _docDate, value);
                if (!IsLoading)
                {
                    SetAccountingPeriodic();
                }
            }
        }

        [Browsable(false)]
        public int JournalMonth
        {
            get { return _journalMonth; }
            set { SetPropertyValue("JournalMonth", ref _journalMonth, value); }
        }

        [Browsable(false)]
        public int JournalYear
        {
            get { return _journalYear; }
            set { SetPropertyValue("JournalYear", ref _journalYear, value); }
        }

        [Appearance("PaymentRealizationStatusClose", Enabled = false)]
        public Status Status
        {
            get { return _status; }
            set { SetPropertyValue("Status", ref _status, value); }
        }

        [ModelDefault("DisplayFormat", "{0:dd/MM/yyyy}")]
        [ModelDefault("EditMask", "dd/MM/yyyy")]
        [Appearance("PaymentRealizationStatusDateClose", Enabled = false)]
        public DateTime StatusDate
        {
            get { return _statusDate; }
            set { SetPropertyValue("StatusDate", ref _statusDate, value); }
        }

        [Association("PaymentRealization-PaymentRealizationLines")]
        public XPCollection<PaymentRealizationLine> PaymentRealizationLines
        {
            get { return GetCollection<PaymentRealizationLine>("PaymentRealizationLines"); }
        }

        [Association("PaymentRealization-ApprovalLines")]
        public XPCollection<ApprovalLine> ApprovalLines
        {
            get { return GetCollection<ApprovalLine>("ApprovalLines"); }
        }

        [Association("PaymentRealization-CashAdvanceCollections")]
        public XPCollection<CashAdvanceCollection> CashAdvanceCollections
        {
            get { return GetCollection<CashAdvanceCollection>("CashAdvanceCollections"); }
        }

        //AuditTrail
        [CollectionOperationSet(AllowAdd = false, AllowRemove = true)]
        public XPCollection<AuditDataItemPersistent> ChangeHistory
        {
            get
            {
                if (changeHistory == null)
                {
                    changeHistory = AuditedObjectWeakReference.GetAuditTrail(Session, this);
                }
                return changeHistory;
            }
        }

        #endregion Field

        //============================================= Code Only =============================================

        private void UpdateName()
        {
            try
            {
                this._name = string.Format("PaymentRealization {0} [ {1} ]", Code, Employee.Name);
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("BusinessObject = PaymentRealization" + ex.ToString());
            }
        }

        private void SetAccountingPeriodic()
        {
            try
            {
                DateTime now = DateTime.Now;
                if (this.Company != null && this.Workplace != null)
                {
                    AccountingPeriodic _locAcctPeriodic = Session.FindObject<AccountingPeriodic>
                                                        (new GroupOperator(GroupOperatorType.And,
                                                        new BinaryOperator("Company", this.Company),
                                                        new BinaryOperator("Workplace", this.Workplace),
                                                        new BinaryOperator("Active", true)));
                    if (_locAcctPeriodic != null)
                    {
                        this.JournalMonth = _locAcctPeriodic.Month;
                        this.JournalYear = _locAcctPeriodic.Year;
                    }
                    else
                    {
                        this.JournalMonth = 0;
                        this.JournalYear = 0;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Business Object = PaymentRealization ", ex.ToString());
            }
        }

    }
}