﻿using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;

using DevExpress.ExpressApp.Editors;
using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.ConditionalAppearance;

namespace FullDrive.Module.BusinessObjects
{
    [DefaultClassOptions]
    [DefaultProperty("Code")]
    [NavigationItem("Sales")]
    [RuleCombinationOfPropertiesIsUnique("InvoiceCanvassingMonitoringRuleUnique", DefaultContexts.Save, "Code")]
    //[ImageName("BO_Contact")]
    //[DefaultProperty("DisplayMemberNameForLookupEditorsOfThisType")]
    //[DefaultListViewOptions(MasterDetailMode.ListViewOnly, false, NewItemRowPosition.None)]
    //[Persistent("DatabaseTableName")]
    // Specify more UI options using a declarative approach (https://documentation.devexpress.com/#eXpressAppFramework/CustomDocument112701).
    public class InvoiceCanvassingMonitoring : BaseObject
    { // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).

        private bool _activationPosting;
        private bool _freeItemChecked;
        private bool _discountChecked;
        private string _code;
        private bool _select;
        private InvoiceCanvassing _invoiceCanvassing;
        private InvoiceCanvassingLine _invoiceCanvassingLine;
        private CanvassingCollection _canvassingCollection;
        private CanvassingCollectionLine _canvassingCollectionLine;
        #region InitialOrganization
        private Company _company;
        private Workplace _workplace;
        private Division _division;
        private Department _department;
        private Section _section;
        private Employee _employee;
        #endregion InitialOrganization
        private Employee _salesman;
        private Vehicle _vehicle;
        private BeginningInventory _beginningInventory;
        private XPCollection<Item> _availableItem;
        private Item _item;
        private string _description;
        private XPCollection<UnitOfMeasure> _availableUnitOfMeasure;
        #region InisialisasiDefaultQty
        private double _dQty;
        private UnitOfMeasure _dUom;
        private double _qty;
        private UnitOfMeasure _uom;
        private double _tQty;
        private StockType _stockType;
        #endregion InisialisasiDefaultQty
        #region InisialisasiAmount
        private Currency _currency;
        private PriceGroup _priceGroup;
        private PriceLine _priceLine;
        private double _uAmount;
        private double _tUAmount;
        private Tax _tax;
        private double _txValue;
        private double _txAmount;
        private DiscountRule _discountRule;
        private double _discAmount;
        private double _tAmount;
        #endregion InisialisasiAmount 
        private Status _status;
        private DateTime _statusDate;
        private int _postedCount;
        private string _userAccessIn;
        private GlobalFunction _globFunc;
        //AuditTrail
        private XPCollection<AuditDataItemPersistent> changeHistory;

        public InvoiceCanvassingMonitoring(Session session)
            : base(session)
        {
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if (!IsLoading)
            {
                if (Session.IsNewObject(this))
                {
                    _globFunc = new GlobalFunction();
                    DateTime now = DateTime.Now;
                    this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.InvoiceCanvassingMonitoring);
                    #region UserAccess
                    _userAccessIn = SecuritySystem.CurrentUserName;
                    UserAccess _locUserAccess = Session.FindObject<UserAccess>(new BinaryOperator("UserName", _userAccessIn));
                    if (_locUserAccess != null)
                    {
                        if (_locUserAccess.Employee != null)
                        {
                            if (_locUserAccess.Employee.Company != null && _locUserAccess.Employee.Workplace != null)
                            {
                                this.Code = _globFunc.GetNumberingUnlockOptimisticRecordBasedOrganization(this.Session.DataLayer, ObjectList.InvoiceCanvassingMonitoring, _locUserAccess.Employee.Company, _locUserAccess.Employee.Workplace);
                            }
                            else
                            {
                                this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.InvoiceCanvassingMonitoring);
                            }
                        }
                    }
                    #endregion UserAccess
                    
                    this.Status = CustomProcess.Status.Open;
                    this.StatusDate = now;
                }  
            }
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).
        }

        #region Field

        [Browsable(false)]
        [ImmediatePostData()]
        public bool ActivationPosting
        {
            get { return _activationPosting; }
            set { SetPropertyValue("ActivationPosting", ref _activationPosting, value); }
        }

        [Browsable(false)]
        public bool FreeItemChecked
        {
            get { return _freeItemChecked; }
            set { SetPropertyValue("FreeItemChecked", ref _freeItemChecked, value); }
        }

        [Browsable(false)]
        public bool DiscountChecked
        {
            get { return _discountChecked; }
            set { SetPropertyValue("DiscountChecked", ref _discountChecked, value); }
        }

        [Appearance("InvoiceCanvassingMonitoringCodeClose", Enabled = false)]
        [RuleRequiredField(DefaultContexts.Save)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue("Code", ref _code, value); }
        }

        [Appearance("InvoiceCanvassingMonitoringSelectClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public bool Select
        {
            get { return _select; }
            set { SetPropertyValue("Select", ref _select, value); }
        }

        [ImmediatePostData()]
        [Appearance("InvoiceCanvassingMonitoringInvoiceCanvassingClose", Enabled = false)]
        public InvoiceCanvassing InvoiceCanvassing
        {
            get { return _invoiceCanvassing; }
            set { SetPropertyValue("InvoiceCanvassing", ref _invoiceCanvassing, value); }
        }

        [Appearance("InvoiceCanvassingMonitoringInvoiceCanvassingLineClose", Enabled = false)]
        public InvoiceCanvassingLine InvoiceCanvassingLine
        {
            get { return _invoiceCanvassingLine; }
            set { SetPropertyValue("InvoiceCanvassingLine", ref _invoiceCanvassingLine, value); }
        }

        [Appearance("InvoiceCanvassingMonitoringCanvassingCollectionClose", Enabled = false)]
        public CanvassingCollection CanvassingCollection
        {
            get { return _canvassingCollection; }
            set { SetPropertyValue("CanvassingCollection", ref _canvassingCollection, value); }
        }

        [Appearance("InvoiceCanvassingMonitoringCanvassingCollectionLineClose", Enabled = false)]
        public CanvassingCollectionLine CanvassingCollectionLine
        {
            get { return _canvassingCollectionLine; }
            set { SetPropertyValue("CanvassingCollectionLine", ref _canvassingCollectionLine, value); }
        }

        #region Organization

        [ImmediatePostData()]
        [RuleRequiredField(DefaultContexts.Save)]
        [Appearance("InvoiceCanvassingMonitoringCompanyClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Company Company
        {
            get { return _company; }
            set { SetPropertyValue("Company", ref _company, value); }
        }

        [ImmediatePostData()]
        [RuleRequiredField(DefaultContexts.Save)]
        [Appearance("InvoiceCanvassingMonitoringWorkplaceClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Workplace Workplace
        {
            get { return _workplace; }
            set { SetPropertyValue("Workplace", ref _workplace, value); }
        }

        [Browsable(false)]
        public Division Division
        {
            get { return _division; }
            set { SetPropertyValue("Division", ref _division, value); }
        }

        [Browsable(false)]
        public Department Department
        {
            get { return _department; }
            set { SetPropertyValue("Department", ref _department, value); }
        }

        [Browsable(false)]
        public Section Section
        {
            get { return _section; }
            set { SetPropertyValue("Section", ref _section, value); }
        }

        [Browsable(false)]
        public Employee Employee
        {
            get { return _employee; }
            set { SetPropertyValue("Employee", ref _employee, value); }
        }

        #endregion Organization

        [ImmediatePostData()]
        [RuleRequiredField(DefaultContexts.Save)]
        [Appearance("InvoiceCanvassingMonitoringSalesmanClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Employee Salesman
        {
            get { return _salesman; }
            set { SetPropertyValue("Salesman", ref _salesman, value); }
        }

        [ImmediatePostData()]
        [Appearance("InvoiceCanvassingMonitoringVehicleClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Vehicle Vehicle
        {
            get { return _vehicle; }
            set { SetPropertyValue("Vehicle", ref _vehicle, value); }
        }

        [Browsable(false)]
        [ImmediatePostData()]
        [Appearance("InvoiceCanvassingMonitoringBeginningInventoryClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public BeginningInventory BeginningInventory
        {
            get { return _beginningInventory; }
            set { SetPropertyValue("BeginningInventory", ref _beginningInventory, value); }
        }

        [Browsable(false)]
        public XPCollection<Item> AvailableItem
        {
            get
            {
                string _beginString = null;
                string _endString = null;
                string _fullString = null;

                if (this.BeginningInventory != null)
                {
                    List<string> _stringBIL = new List<string>();

                    XPCollection<BeginningInventoryLine> _locBeginningInventoryLines = new XPCollection<BeginningInventoryLine>
                                                                        (Session, new GroupOperator(GroupOperatorType.And,
                                                                        new BinaryOperator("BeginningInventory", this.BeginningInventory),
                                                                        new BinaryOperator("Active", true)));

                    if (_locBeginningInventoryLines != null && _locBeginningInventoryLines.Count() > 0)
                    {
                        foreach (BeginningInventoryLine _locBeginningInventoryLine in _locBeginningInventoryLines)
                        {
                            if (_locBeginningInventoryLine.Item != null)
                            {
                                if (_locBeginningInventoryLine.Item.Code != null)
                                {
                                    _stringBIL.Add(_locBeginningInventoryLine.Item.Code);
                                }
                            }
                        }
                    }

                    IEnumerable<string> _stringArrayBILDistinct = _stringBIL.Distinct();
                    string[] _stringArrayBILList = _stringArrayBILDistinct.ToArray();
                    if (_stringArrayBILList.Length == 1)
                    {
                        for (int i = 0; i < _stringArrayBILList.Length; i++)
                        {
                            Item _locItm = Session.FindObject<Item>(new BinaryOperator("Code", _stringArrayBILList[i]));
                            if (_locItm != null)
                            {
                                if (i == 0)
                                {
                                    _beginString = "[Code]=='" + _locItm.Code + "'";
                                }
                            }
                        }
                    }
                    else if (_stringArrayBILList.Length > 1)
                    {
                        for (int i = 0; i < _stringArrayBILList.Length; i++)
                        {
                            Item _locItm = Session.FindObject<Item>(new BinaryOperator("Code", _stringArrayBILList[i]));
                            if (_locItm != null)
                            {
                                if (i == 0)
                                {
                                    _beginString = "[Code]=='" + _locItm.Code + "'";
                                }
                                else
                                {
                                    _endString = _endString + " OR [Code]=='" + _locItm.Code + "'";
                                }
                            }
                        }
                    }
                    _fullString = _beginString + _endString;

                    if (_fullString != null)
                    {
                        _availableItem = new XPCollection<Item>(Session, CriteriaOperator.Parse(_fullString));
                    }
                }

                return _availableItem;

            }
        }

        [ImmediatePostData()]
        [DataSourceProperty("AvailableItem", DataSourcePropertyIsNullMode.SelectAll)]
        [Appearance("InvoiceCanvassingMonitoringItemClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Item Item
        {
            get { return _item; }
            set
            {
                SetPropertyValue("Item", ref _item, value);
                if (!IsLoading)
                {
                    if (this._item != null)
                    {
                        if (this._item.Description != null)
                        {
                            this.Description = this._item.Description;
                        }
                    }
                }
            }
        }

        [Appearance("InvoiceCanvassingMonitoringDescriptionClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public string Description
        {
            get { return _description; }
            set { SetPropertyValue("Description", ref _description, value); }
        }

        [Browsable(false)]
        public XPCollection<UnitOfMeasure> AvailableUnitOfMeasure
        {
            get
            {
                string _beginString = null;
                string _endString = null;
                string _fullString = null;
                if (Item == null)
                {
                    _availableUnitOfMeasure = new XPCollection<UnitOfMeasure>(Session);
                }
                else
                {
                    List<string> _stringUOM = new List<string>();

                    XPCollection<ItemUnitOfMeasure> _locItemUnitOfMeasures = new XPCollection<ItemUnitOfMeasure>
                                                                              (Session, new GroupOperator(GroupOperatorType.And,
                                                                               new BinaryOperator("Item", this.Item),
                                                                               new BinaryOperator("Active", true)));

                    if (_locItemUnitOfMeasures != null && _locItemUnitOfMeasures.Count() > 0)
                    {
                        foreach (ItemUnitOfMeasure _locItemUnitOfMeasure in _locItemUnitOfMeasures)
                        {
                            _stringUOM.Add(_locItemUnitOfMeasure.UOM.Code);
                        }
                    }

                    IEnumerable<string> _stringArrayUOMDistinct = _stringUOM.Distinct();
                    string[] _stringArrayUOMList = _stringArrayUOMDistinct.ToArray();
                    if (_stringArrayUOMList.Length == 1)
                    {
                        for (int i = 0; i < _stringArrayUOMList.Length; i++)
                        {
                            UnitOfMeasure _locUOM = Session.FindObject<UnitOfMeasure>(new BinaryOperator("Code", _stringArrayUOMList[i]));
                            if (_locUOM != null)
                            {
                                if (i == 0)
                                {
                                    _beginString = "[Code]=='" + _locUOM.Code + "'";
                                }
                            }
                        }
                    }
                    else if (_stringArrayUOMList.Length > 1)
                    {
                        for (int i = 0; i < _stringArrayUOMList.Length; i++)
                        {
                            UnitOfMeasure _locUOM = Session.FindObject<UnitOfMeasure>(new BinaryOperator("Code", _stringArrayUOMList[i]));
                            if (_locUOM != null)
                            {
                                if (i == 0)
                                {
                                    _beginString = "[Code]=='" + _locUOM.Code + "'";
                                }
                                else
                                {
                                    _endString = _endString + " OR [Code]=='" + _locUOM.Code + "'";
                                }
                            }
                        }
                    }
                    _fullString = _beginString + _endString;

                    if (_fullString != null)
                    {
                        _availableUnitOfMeasure = new XPCollection<UnitOfMeasure>(Session, CriteriaOperator.Parse(_fullString));
                    }

                }
                return _availableUnitOfMeasure;

            }
        }


        #region DefaultQty

        [Appearance("InvoiceCanvassingMonitoringDQtyClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [ImmediatePostData()]
        public double DQty
        {
            get { return _dQty; }
            set {
                SetPropertyValue("DQty", ref _dQty, value);
                if (!IsLoading)
                {
                    SetTotalQty();
                    SetTotalUnitAmount();
                    SetTaxAndDiscountAmount();
                    SetTotalAmount();
                }
            }
        }

        [Appearance("InvoiceCanvassingMonitoringDUOMClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [ImmediatePostData()]
        [DataSourceCriteria("Active = true")]
        public UnitOfMeasure DUOM
        {
            get { return _dUom; }
            set {
                SetPropertyValue("DUOM", ref _dUom, value);
                if (!IsLoading)
                {
                    SetTotalQty();
                    SetTotalUnitAmount();
                    SetTaxAndDiscountAmount();
                    SetTotalAmount();
                }
            }
        }

        [Appearance("InvoiceCanvassingMonitoringQtyClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [ImmediatePostData()]
        public double Qty
        {
            get { return _qty; }
            set {
                SetPropertyValue("Qty", ref _qty, value);
                if (!IsLoading)
                {
                    SetTotalQty();
                    SetTotalUnitAmount();
                    SetTaxAndDiscountAmount();
                    SetTotalAmount();
                }
            }
        }

        [Appearance("InvoiceCanvassingMonitoringUOMClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [ImmediatePostData()]
        [DataSourceProperty("AvailableUnitOfMeasure", DataSourcePropertyIsNullMode.SelectAll)]
        [DataSourceCriteria("Active = true")]
        public UnitOfMeasure UOM
        {
            get { return _uom; }
            set {
                SetPropertyValue("UOM", ref _uom, value);
                if (!IsLoading)
                {
                    SetTotalQty();
                    SetTotalUnitAmount();
                    SetTaxAndDiscountAmount();
                    SetTotalAmount();
                }
            }
        }

        [Appearance("InvoiceCanvassingMonitoringTotalQtyEnabled", Enabled = false)]
        public double TQty
        {
            get { return _tQty; }
            set { SetPropertyValue("TotalQty", ref _tQty, value); }
        }

        [Appearance("InvoiceCanvassingLineStockTypeClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public StockType StockType
        {
            get { return _stockType; }
            set { SetPropertyValue("StockType", ref _stockType, value); }
        }

        #endregion DefaultQty

        #region Amount

        [Browsable(false)]
        [Appearance("InvoiceCanvassingMonitoringCurrencyClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Currency Currency
        {
            get { return _currency; }
            set { SetPropertyValue("Currency", ref _currency, value); }
        }

        [Browsable(false)]
        [Appearance("InvoiceCanvassingMonitoringPriceGroupClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public PriceGroup PriceGroup
        {
            get { return _priceGroup; }
            set {
                SetPropertyValue("PriceGroup", ref _priceGroup, value);
                if (!IsLoading)
                {
                    if (this._priceGroup != null)
                    {
                        SetPriceLine();
                    }
                }
            }
        }

        [Browsable(false)]
        [Appearance("InvoiceCanvassingMonitoringPriceLineClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public PriceLine PriceLine
        {
            get { return _priceLine; }
            set {
                SetPropertyValue("PriceLine", ref _priceLine, value);
                if (!IsLoading)
                {
                    if (this._priceLine != null)
                    {
                        this.UAmount = this._priceLine.Amount2a;
                    }
                }
            }
        }

        [Appearance("InvoiceCanvassingMonitoringUAmountClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [ImmediatePostData()]
        public double UAmount
        {
            get { return _uAmount; }
            set {
                SetPropertyValue("UAmount", ref _uAmount, value);
                if (!IsLoading)
                {
                    _globFunc = new GlobalFunction();
                    if (this._uAmount > 0)
                    {
                        if (_globFunc.GetRoundingList(Session, ObjectList.InvoiceCanvassingMonitoring, FieldName.UAmount) == true)
                        {
                            this._uAmount = _globFunc.GetRoundUp(Session, this._uAmount, ObjectList.InvoiceCanvassingMonitoring, FieldName.UAmount);
                        }
                    }
                    SetTotalUnitAmount();
                    SetTaxAndDiscountAmount();
                    SetTotalAmount();
                    
                }
            }
        }

        [Appearance("InvoiceCanvassingMonitoringTUAmountEnabled", Enabled = false)]
        [ImmediatePostData()]
        public double TUAmount
        {
            get { return _tUAmount; }
            set { SetPropertyValue("TUAmount", ref _tUAmount, value); }
        }

        [ImmediatePostData]
        [Appearance("InvoiceCanvassingMonitoringTaxClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [DataSourceCriteria("Active = true")]
        public Tax Tax
        {
            get { return _tax; }
            set {
                SetPropertyValue("Tax", ref _tax, value);
                if (!IsLoading)
                {
                    if (this._tax != null)
                    {
                        this.TxValue = this._tax.Value;
                        SetTaxAndDiscountAmount();
                        SetTotalAmount();
                    }
                    else
                    {
                        SetNormalTax();
                    }
                }
            }
        }

        [Browsable(false)]
        [ImmediatePostData()]
        [Appearance("InvoiceCanvassingMonitoringTxValueClose", Enabled = false)]
        public double TxValue
        {
            get { return _txValue; }
            set {
                SetPropertyValue("TxValue", ref _txValue, value);
                if (!IsLoading)
                {
                    SetTaxAndDiscountAmount();
                    SetTotalAmount();
                }
            }
        }

        [Appearance("InvoiceCanvassingMonitoringTxAmountEnabled", Enabled = false)]
        [ImmediatePostData()]
        public double TxAmount
        {
            get { return _txAmount; }
            set {
                SetPropertyValue("TxAmount", ref _txAmount, value);
                if (!IsLoading)
                {
                    _globFunc = new GlobalFunction();
                    if (this._txAmount > 0)
                    {
                        if (_globFunc.GetRoundingList(Session, ObjectList.InvoiceCanvassingMonitoring, FieldName.TxAmount) == true)
                        {
                            this._txAmount = _globFunc.GetRoundUp(Session, this._txAmount, ObjectList.InvoiceCanvassingMonitoring, FieldName.TxAmount);
                        }
                    }
                    SetTotalAmount();
                    
                }
            }
        }

        [Appearance("InvoiceCanvassingMonitorinDiscountRuleClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [ImmediatePostData()]
        public DiscountRule DiscountRule
        {
            get { return _discountRule; }
            set {
                SetPropertyValue("DiscountRule", ref _discountRule, value);
                if (!IsLoading)
                {
                    if (this._discountRule != null)
                    {
                        SetTaxAndDiscountAmount();
                        SetTotalAmount();
                    }  
                }
            }
        }

        [Appearance("InvoiceCanvassingMonitoringDiscAmountEnabled", Enabled = false)]
        [ImmediatePostData()]
        public double DiscAmount
        {
            get { return _discAmount; }
            set
            {
                SetPropertyValue("DiscAmount", ref _discAmount, value);
                if (!IsLoading)
                {
                    _globFunc = new GlobalFunction();
                    if (this._discAmount > 0)
                    {
                        if (_globFunc.GetRoundingList(Session, ObjectList.PurchaseOrderLine, FieldName.DiscAmount) == true)
                        {
                            this._discAmount = _globFunc.GetRoundUp(Session, this._discAmount, ObjectList.PurchaseOrderLine, FieldName.DiscAmount);
                        }
                    }
                }
            }
        }

        [Appearance("InvoiceCanvassingMonitoringTAmountEnabled", Enabled = false)]
        [ImmediatePostData()]
        public double TAmount
        {
            get { return _tAmount; }
            set {
                SetPropertyValue("TAmount", ref _tAmount, value);
                if (!IsLoading)
                {
                    _globFunc = new GlobalFunction();
                    if (this._tAmount > 0)
                    {
                        if (_globFunc.GetRoundingList(Session, ObjectList.InvoiceCanvassingMonitoring, FieldName.TAmount) == true)
                        {
                            this._tAmount = _globFunc.GetRoundUp(Session, this._tAmount, ObjectList.InvoiceCanvassingMonitoring, FieldName.TAmount);
                        }
                    }
                }
            }
        }

        #endregion Amount

        //[Appearance("InvoiceCanvassingMonitoringStatusClose", Enabled = false)]
        public Status Status
        {
            get { return _status; }
            set { SetPropertyValue("Status", ref _status, value); }
        }

        [Appearance("InvoiceCanvassingMonitoringStatusDateClose", Enabled = false)]
        public DateTime StatusDate
        {
            get { return _statusDate; }
            set { SetPropertyValue("StatusDate", ref _statusDate, value); }
        }

        [Appearance("InvoiceCanvassingMonitoringPostedCountEnabled", Enabled = false)]
        public int PostedCount
        {
            get { return _postedCount; }
            set { SetPropertyValue("PostedCount", ref _postedCount, value); }
        }

        //AuditTrail
        [CollectionOperationSet(AllowAdd = false, AllowRemove = true)]
        public XPCollection<AuditDataItemPersistent> ChangeHistory
        {
            get
            {
                if (changeHistory == null)
                {
                    changeHistory = AuditedObjectWeakReference.GetAuditTrail(Session, this);
                }
                return changeHistory;
            }
        }

        #endregion Field

        //=========================================== Code Only =========================================

        #region CodeOnly

        #region Set

        private void SetTotalQty()
        {
            try
            {
                double _locInvLineTotal = 0;
                DateTime now = DateTime.Now;
                if (this.InvoiceCanvassing != null)
                {
                    if (this.Item != null && this.UOM != null && this.DUOM != null)
                    {
                        ItemUnitOfMeasure _locItemUOM = Session.FindObject<ItemUnitOfMeasure>
                                                        (new GroupOperator(GroupOperatorType.And,
                                                         new BinaryOperator("Item", this.Item),
                                                         new BinaryOperator("UOM", this.UOM),
                                                         new BinaryOperator("DefaultUOM", this.DUOM),
                                                         new BinaryOperator("Active", true)));
                        if (_locItemUOM != null)
                        {
                            if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                            {
                                _locInvLineTotal = this.Qty * _locItemUOM.DefaultConversion + this.DQty;
                            }
                            else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                            {
                                _locInvLineTotal = this.Qty / _locItemUOM.Conversion + this.DQty;
                            }
                            else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                            {
                                _locInvLineTotal = this.Qty + this.DQty;
                            }

                            this.TQty = _locInvLineTotal;
                        }
                    }
                    else
                    {
                        _locInvLineTotal = this.Qty + this.DQty;
                        this.TQty = _locInvLineTotal;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = InvoiceCanvassingLine " + ex.ToString());
            }
        }

        private void SetTotalUnitAmount()
        {
            try
            {
                _globFunc = new GlobalFunction();

                if (_tQty >= 0 & _uAmount >= 0)
                {
                    if (_globFunc.GetRoundingList(Session, ObjectList.InvoiceCanvassingLine, FieldName.TUAmount) == true)
                    {
                        this.TUAmount = _globFunc.GetRoundUp(Session, (this.TQty * this.UAmount), ObjectList.InvoiceCanvassingLine, FieldName.TUAmount);
                    }
                    else
                    {
                        this.TUAmount = this.TQty * this.UAmount;
                    }

                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = InvoiceCanvassingLine " + ex.ToString());
            }
        }

        private void SetTotalAmount()
        {
            try
            {
                _globFunc = new GlobalFunction();

                if (_uAmount >= 0 && _txAmount >= 0)
                {
                    if (!IsLoading)
                    {
                        if (this.Tax != null)
                        {
                            if (this.Tax.TaxNature == TaxNature.Increase)
                            {
                                if (_globFunc.GetRoundingList(Session, ObjectList.InvoiceCanvassingLine, FieldName.TAmount) == true)
                                {
                                    this.TAmount = _globFunc.GetRoundUp(Session, (this.TUAmount + this.TxAmount), ObjectList.InvoiceCanvassingLine, FieldName.TAmount);
                                }
                                else
                                {
                                    this.TAmount = this.TUAmount + this.TxAmount;
                                }
                            }
                            else if (this.Tax.TaxNature == TaxNature.Decrease)
                            {
                                if (_globFunc.GetRoundingList(Session, ObjectList.InvoiceCanvassingLine, FieldName.TAmount) == true)
                                {
                                    this.TAmount = _globFunc.GetRoundUp(Session, (this.TUAmount - this.TxAmount), ObjectList.InvoiceCanvassingLine, FieldName.TAmount);
                                }
                                else
                                {
                                    this.TAmount = this.TUAmount - this.TxAmount;
                                }
                            }
                            else if (this.Tax.TaxNature == TaxNature.None)
                            {
                                if (_globFunc.GetRoundingList(Session, ObjectList.InvoiceCanvassingLine, FieldName.TAmount) == true)
                                {
                                    this.TAmount = _globFunc.GetRoundUp(Session, (this.TUAmount), ObjectList.InvoiceCanvassingLine, FieldName.TAmount);
                                }
                                else
                                {
                                    this.TAmount = this.TUAmount;
                                }
                            }
                            else
                            {
                                if (_globFunc.GetRoundingList(Session, ObjectList.InvoiceCanvassingLine, FieldName.TAmount) == true)
                                {
                                    this.TAmount = _globFunc.GetRoundUp(Session, (this.TUAmount + this.TxAmount), ObjectList.InvoiceCanvassingLine, FieldName.TAmount);
                                }
                                else
                                {
                                    this.TAmount = this.TUAmount + this.TxAmount;
                                }
                            }
                        }
                        //clm
                        else
                        {
                            this.TAmount = this.TUAmount + this.TxAmount;
                        }
                    }
                }
                //clm
                else
                {
                    this.TAmount = this.TUAmount + this.TxAmount;
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = InvoiceCanvassingLine " + ex.ToString());
            }
        }

        private void SetTaxAndDiscountAmount()
        {
            try
            {
                SetTaxBeforeDiscount();
                SetDiscountGross();
                SetTaxAfterDiscount();
                SetDiscountNet();
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = InvoiceCanvassingLine " + ex.ToString());
            }
        }

        private void SetNormalTax()
        {
            try
            {
                if (_tax != null && _txValue > 0 && _txAmount > 0)
                {
                    this.Tax = this.Tax;
                    this.TxValue = this.TxValue;
                    this.TxAmount = this.TxAmount;
                }
                else
                {
                    this.TxValue = 0;
                    this.TxAmount = 0;
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = InvoiceCanvassingLine " + ex.ToString());
            }
        }

        private void SetPriceLine()
        {
            try
            {
                if (this.Company != null && this.Workplace != null && this.Item != null && this.PriceGroup != null && this.Currency != null)
                {
                    PriceLine _locPriceLine = Session.FindObject<PriceLine>(new GroupOperator(GroupOperatorType.And,
                                                                            new BinaryOperator("Company", this.Company),
                                                                            new BinaryOperator("Workplace", this.Workplace),
                                                                            new BinaryOperator("Item", this.Item),
                                                                            new BinaryOperator("PriceGroup", this.PriceGroup),
                                                                            new BinaryOperator("Currency", this.Currency),
                                                                            new BinaryOperator("Active", true)));
                    if (_locPriceLine != null)
                    {
                        this.PriceLine = _locPriceLine;
                        this.UAmount = _locPriceLine.Amount2a;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = InvoiceCanvassingLine " + ex.ToString());
            }
        }

        #region Tax

        private void SetTaxBeforeDiscount()
        {
            try
            {
                double _locTxAmount = 0;
                double _locTAmount = 0;
                if (this.Tax != null)
                {
                    if (this.Tax.TaxRule == TaxRule.BeforeDiscount)
                    {
                        _locTxAmount = this.TxValue / 100 * this.TUAmount;
                        if (this.Tax.TaxNature == TaxNature.Increase)
                        {
                            _locTAmount = this.TAmount + _locTxAmount;
                        }
                        if (this.Tax.TaxNature == TaxNature.Decrease)
                        {
                            _locTAmount = this.TAmount - _locTxAmount;
                        }
                        this.TxAmount = _locTxAmount;
                        this.TAmount = _locTAmount;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("BusinessObject = InvoiceCanvassingMonitoring" + ex.ToString());
            }
        }

        private void SetDiscountGross()
        {
            try
            {
                if (this.DiscountRule != null)
                {
                    double _locTUAmountDiscount = 0;

                    #region Gross
                    if (this.DiscountRule.RuleType == RuleType.Rule2)
                    {
                        if (this.DiscountRule.GrossAmountRule == true)
                        {
                            if (this.TUAmount > 0 && this.TUAmount >= this.DiscountRule.GrossTotalUAmount)
                            {
                                if (this.DiscountRule.DiscountType == DiscountType.Percentage)
                                {
                                    _locTUAmountDiscount = this.TUAmount * this.DiscountRule.Value / 100;
                                }
                                if (this.DiscountRule.DiscountType == DiscountType.Value)
                                {
                                    _locTUAmountDiscount = this.DiscountRule.Value;
                                }

                                this.DiscountChecked = true;
                                this.DiscAmount = _locTUAmountDiscount;
                                this.TAmount = this.TAmount - _locTUAmountDiscount;
                                
                            }

                        }
                    }
                    #endregion Gross
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("BusinessObject = InvoiceCanvassingMonitoring" + ex.ToString());
            }
        }

        private void SetTaxAfterDiscount()
        {
            try
            {
                double _locTxAmount = 0;
                double _locTAmount = 0;
                if (this.Tax != null && this.DiscountRule != null)
                {
                    if (this.Tax.TaxRule == TaxRule.AfterDiscount && this.DiscountRule.GrossAmountRule == true)
                    {
                        _locTxAmount = this.TxValue / 100 * (this.TUAmount - this.DiscAmount);

                        if (this.Tax.TaxNature == TaxNature.Increase)
                        {
                            _locTAmount = this.TAmount + _locTxAmount;
                        }
                        if (this.Tax.TaxNature == TaxNature.Decrease)
                        {
                            _locTAmount = this.TAmount - _locTxAmount;
                        }
                        this.TxAmount = _locTxAmount;
                        this.TAmount = _locTAmount;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("BusinessObject = InvoiceCanvassingMonitoring" + ex.ToString());
            }
        }

        private void SetDiscountNet()
        {
            try
            {
                if (this.DiscountRule != null && this.Tax != null)
                {
                    double _locTUAmountDiscount = 0;

                    #region Net

                    if (this.DiscountRule.RuleType == RuleType.Rule2)
                    {
                        if (this.DiscountRule.NetAmountRule == true && this.Tax.TaxRule == TaxRule.BeforeDiscount)
                        {
                            if (this.TAmount > 0 && this.TAmount >= this.DiscountRule.NetTotalUAmount)
                            {
                                if (this.DiscountRule.DiscountType == DiscountType.Percentage)
                                {
                                    _locTUAmountDiscount = this.TAmount * this.DiscountRule.Value / 100;
                                }
                                if (this.DiscountRule.DiscountType == DiscountType.Value)
                                {
                                    _locTUAmountDiscount = this.DiscountRule.Value;
                                }

                                this.DiscountChecked = true;
                                this.DiscAmount = _locTUAmountDiscount;
                                this.TAmount = this.TAmount - _locTUAmountDiscount;
                                
                            }
                        }
                    }

                    #endregion Net
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("BusinessObject = InvoiceCanvassingMonitoring" + ex.ToString());
            }
        }

        #endregion Tax

        #endregion Set

        #endregion CodeOnly

    }
}