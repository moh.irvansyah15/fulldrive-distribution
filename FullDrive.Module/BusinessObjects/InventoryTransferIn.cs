﻿using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;

using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.ConditionalAppearance;
using DevExpress.ExpressApp.Editors;

namespace FullDrive.Module.BusinessObjects
{
    [DefaultClassOptions]
    [DefaultProperty("Code")]
    [NavigationItem("Inventory")]
    [RuleCombinationOfPropertiesIsUnique("InventoryTransferInRuleUnique", DefaultContexts.Save, "Code")]
    //[ImageName("BO_Contact")]
    //[DefaultProperty("DisplayMemberNameForLookupEditorsOfThisType")]
    //[DefaultListViewOptions(MasterDetailMode.ListViewOnly, false, NewItemRowPosition.None)]
    //[Persistent("DatabaseTableName")]
    // Specify more UI options using a declarative approach (https://documentation.devexpress.com/#eXpressAppFramework/CustomDocument112701).
    public class InventoryTransferIn : BaseObject
    { // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).
        private bool _activationPosting;
        private string _code;
        #region InitialOrganization
        private Company _company;
        private Workplace _workplace;
        private Division _division;
        private Department _department;
        private Section _section;
        private Employee _employee;
        #endregion InitialOrganization
        private DirectionType _transferType;
        private InventoryMovingType _inventoryMovingType;       
        private ObjectList _objectList;
        XPCollection<DocumentType> _availableDocumentType;
        private DocumentType _documentType;
        private string _docNo;
        private DocumentRule _documentRule;
        #region InitialLocation
        private LocationType _locationType;
        private StockType _stockType;
        private XPCollection<Location> _availableLocation;
        private Location _location;
        #endregion InitialLocation
        #region InitialBusinessPartner
        private bool _openVendor;
        private BusinessPartner _businessPartner;
        private string _businessPartnerContact;
        private Country _businessPartnerCountry;
        private City _businessPartnerCity;
        private string _businessPartnerAddress;
        #endregion InitialBusinessPartner        
        private DateTime _estimatedDate;
        private DateTime _docDate;
        private int _journalMonth;
        private int _journalYear;
        private Status _status;
        private DateTime _statusDate;               
        private string _localUserAccess;
        private GlobalFunction _globFunc;
        //AuditTrail
        private XPCollection<AuditDataItemPersistent> changeHistory;

        public InventoryTransferIn(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if (!IsLoading)
            {
                if (Session.IsNewObject(this))
                {
                    _globFunc = new GlobalFunction();
                    DateTime now = DateTime.Now;
                    #region UserAccess
                    _localUserAccess = SecuritySystem.CurrentUserName;
                    UserAccess _locUserAccess = Session.FindObject<UserAccess>(new BinaryOperator("UserName", _localUserAccess));
                    if (_locUserAccess != null)
                    {
                        if (_locUserAccess.Employee != null)
                        {
                            if (_locUserAccess.Employee.Company != null && _locUserAccess.Employee.Workplace != null)
                            {
                                this.Code = _globFunc.GetNumberingUnlockOptimisticRecordBasedOrganization(this.Session.DataLayer, ObjectList.InventoryTransferIn, _locUserAccess.Employee.Company, _locUserAccess.Employee.Workplace);
                            }
                            else
                            {
                                this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.InventoryTransferIn);
                            }
                            if (_locUserAccess.Employee.Company != null)
                            {
                                this.Company = _locUserAccess.Employee.Company;
                            }
                            if (_locUserAccess.Employee.Workplace != null)
                            {
                                this.Workplace = _locUserAccess.Employee.Workplace;
                            }
                            else
                            {
                                this.Workplace = _globFunc.GetDefaultWorkplace(this.Session, _locUserAccess);
                            }
                            if (_locUserAccess.Employee.Division != null)
                            {
                                Division = _locUserAccess.Employee.Division;
                            }
                            if (_locUserAccess.Employee.Department != null)
                            {
                                Department = _locUserAccess.Employee.Department;
                            }
                            if (_locUserAccess.Employee.Section != null)
                            {
                                Section = _locUserAccess.Employee.Section;
                            }
                            this.Employee = _locUserAccess.Employee;

                            #region DocumentTypeSetup
                            EmployeeDocumentTypeSetup _locEmpDocTypeSetup = Session.FindObject<EmployeeDocumentTypeSetup>
                                                                            (new GroupOperator(GroupOperatorType.And,
                                                                            new BinaryOperator("Employee", _locUserAccess.Employee),
                                                                            new BinaryOperator("ObjectList", ObjectList.InventoryTransferIn),
                                                                            new BinaryOperator("TransferType", DirectionType.None, BinaryOperatorType.NotEqual),
                                                                            new BinaryOperator("InventoryMovingType", InventoryMovingType.None, BinaryOperatorType.NotEqual),
                                                                            new BinaryOperator("Active", true),
                                                                            new BinaryOperator("Default", true)));


                            if (_locEmpDocTypeSetup != null)
                            {
                                this.TransferType = _locEmpDocTypeSetup.TransferType;
                                this.InventoryMovingType = _locEmpDocTypeSetup.InventoryMovingType;
                            }
                            #endregion DocumentTypeSetup

                        }
                    }
                    #endregion UserAccess
                    this.TransferType = DirectionType.External;
                    this.ObjectList = ObjectList.InventoryTransferIn;
                    this.Status = Status.Open;
                    this.StatusDate = now;
                }
            }
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).
        }

        #region Field

        [Browsable(false)]
        public bool ActivationPosting
        {
            get { return _activationPosting; }
            set { SetPropertyValue("ActivationPosting", ref _activationPosting, value); }
        }

        [RuleRequiredField(DefaultContexts.Save)]
        [Appearance("InventoryTransferInCodeEnabled", Enabled = false)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue("Code", ref _code, value); }
        }

        #region Organization

        [Appearance("InventoryTransferInCompanyEnabled", Enabled = false)]
        public Company Company
        {
            get { return _company; }
            set { SetPropertyValue("Company", ref _company, value); }
        }

        [Appearance("InventoryTransferInWorkplaceEnabled", Enabled = false)]
        public Workplace Workplace
        {
            get { return _workplace; }
            set { SetPropertyValue("Workplace", ref _workplace, value); }
        }

        [Browsable(false)]
        public Division Division
        {
            get { return _division; }
            set { SetPropertyValue("Division", ref _division, value); }
        }

        [Browsable(false)]
        public Department Department
        {
            get { return _department; }
            set { SetPropertyValue("Department", ref _department, value); }
        }

        [Browsable(false)]
        public Section Section
        {
            get { return _section; }
            set { SetPropertyValue("Section", ref _section, value); }
        }

        [Browsable(false)]
        public Employee Employee
        {
            get { return _employee; }
            set { SetPropertyValue("Employee", ref _employee, value); }
        }

        #endregion Organization

        [ImmediatePostData()]
        [Appearance("InventoryTransferInTransferTypeClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public DirectionType TransferType
        {
            get { return _transferType; }
            set { SetPropertyValue("TransferType", ref _transferType, value); }
        }

        [ImmediatePostData()]
        [Appearance("InventoryTransferInInventoryMovingTypeClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public InventoryMovingType InventoryMovingType
        {
            get { return _inventoryMovingType; }
            set { SetPropertyValue("InventoryMovingType", ref _inventoryMovingType, value); }
        }       

        [Browsable(false)]
        public ObjectList ObjectList
        {
            get { return _objectList; }
            set { SetPropertyValue("ObjectList", ref _objectList, value); }
        }

        [Browsable(false)]
        public XPCollection<DocumentType> AvailableDocumentType
        {
            get
            {
                if(!IsLoading)
                {
                    string _beginString = null;
                    string _endString = null;
                    string _fullString = null;

                    if (this.Company != null && this.Workplace != null && this.Employee != null)
                    {
                        if (ObjectList != ObjectList.None)
                        {
                            List<string> _stringEDTS = new List<string>();

                            XPCollection<EmployeeDocumentTypeSetup> _locEmployeeDocumentTypeSetups = new XPCollection<EmployeeDocumentTypeSetup>
                                                                                (Session, new GroupOperator(GroupOperatorType.And,
                                                                                new BinaryOperator("Company", this.Company),
                                                                                new BinaryOperator("Workplace", this.Workplace),
                                                                                new BinaryOperator("Employee", this.Employee),
                                                                                new BinaryOperator("ObjectList", this.ObjectList),
                                                                                new BinaryOperator("InventoryMovingType", this.InventoryMovingType),
                                                                                new BinaryOperator("TransferType", this.TransferType),
                                                                                new BinaryOperator("Active", true)));

                            if (_locEmployeeDocumentTypeSetups != null && _locEmployeeDocumentTypeSetups.Count() > 0)
                            {
                                foreach (EmployeeDocumentTypeSetup _locEmployeeDocumentTypeSetup in _locEmployeeDocumentTypeSetups)
                                {
                                    if (_locEmployeeDocumentTypeSetup.DocumentType != null)
                                    {
                                        if (_locEmployeeDocumentTypeSetup.DocumentType.Code != null)
                                        {
                                            _stringEDTS.Add(_locEmployeeDocumentTypeSetup.DocumentType.Code);
                                        }
                                    }
                                }
                            }

                            IEnumerable<string> _stringArrayEDTSDistinct = _stringEDTS.Distinct();
                            string[] _stringArrayEDTSList = _stringArrayEDTSDistinct.ToArray();
                            if (_stringArrayEDTSList.Length == 1)
                            {
                                for (int i = 0; i < _stringArrayEDTSList.Length; i++)
                                {
                                    DocumentType _locDT = Session.FindObject<DocumentType>(new BinaryOperator("Code", _stringArrayEDTSList[i]));
                                    if (_locDT != null)
                                    {
                                        if (i == 0)
                                        {
                                            _beginString = "[Code]=='" + _locDT.Code + "'";
                                        }
                                    }
                                }
                            }
                            else if (_stringArrayEDTSList.Length > 1)
                            {
                                for (int i = 0; i < _stringArrayEDTSList.Length; i++)
                                {
                                    DocumentType _locDT = Session.FindObject<DocumentType>(new BinaryOperator("Code", _stringArrayEDTSList[i]));
                                    if (_locDT != null)
                                    {
                                        if (i == 0)
                                        {
                                            _beginString = "[Code]=='" + _locDT.Code + "'";
                                        }
                                        else
                                        {
                                            _endString = _endString + " OR [Code]=='" + _locDT.Code + "'";
                                        }
                                    }
                                }
                            }
                            _fullString = _beginString + _endString;

                            if (_fullString != null)
                            {
                                _availableDocumentType = new XPCollection<DocumentType>(Session, CriteriaOperator.Parse(_fullString));
                            }
                        }
                        else
                        {
                            _availableDocumentType = new XPCollection<DocumentType>(Session, new GroupOperator(GroupOperatorType.And,
                                                                                    new BinaryOperator("Company", this.Company)
                                                                                    ));
                        }
                    }
                }
                return _availableDocumentType;
            }
        }

        [Appearance("InventoryTransferInDocumentTypeClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [ImmediatePostData()]
        [DataSourceProperty("AvailableDocumentType")]
        public DocumentType DocumentType
        {
            get { return _documentType; }
            set
            {
                SetPropertyValue("DocumentType", ref _documentType, value);
                if (!IsLoading)
                {
                    if (_documentType != null)
                    {
                        _globFunc = new GlobalFunction();
                        this.DocNo = _globFunc.GetDocumentNumberingUnlockOptimisticRecordBasedOrganization(this.Session.DataLayer, this._documentType, this.Company, this.Workplace);
                        //this.DocNo = _globFunc.GetDocumentNumberingUnlockOptimisticRecord(this.Session.DataLayer, this._documentType);
                        if (this._documentType.LocationTypeFrom != LocationType.None)
                        {
                            this.LocationType = this._documentType.LocationTypeFrom;
                        }
                        else
                        {
                            this.LocationType = LocationType.None;
                        }

                        if (this._documentType.StockTypeFrom != StockType.None)
                        {
                            this.StockType = this._documentType.StockTypeFrom;
                        }
                        else
                        {
                            this.StockType = StockType.None;
                        }

                        if (this._documentType.DocumentRule == DocumentRule.Vendor)
                        {
                            this.OpenVendor = true;
                            this.DocumentRule = DocumentRule.Vendor;
                        }
                        else
                        {
                            this.OpenVendor = false;
                            this.DocumentRule = DocumentRule.None;
                        }
                    }
                    else
                    {
                        this.LocationType = LocationType.None;
                        this.StockType = StockType.None;
                        this.DocNo = null;
                        this.OpenVendor = false;
                        this.DocumentRule = DocumentRule.None;
                    }
                }
            }
        }

        [RuleRequiredField(DefaultContexts.Save)]
        [Appearance("InventoryTransferInDocNoClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public string DocNo
        {
            get { return _docNo; }
            set { SetPropertyValue("DocNo", ref _docNo, value); }
        }

        [Browsable(false)]
        [Appearance("InventoryTransferInDocumentRuleClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public DocumentRule DocumentRule
        {
            get { return _documentRule; }
            set { SetPropertyValue("DocumentRule", ref _documentRule, value); }
        }

        #region Location

        [ImmediatePostData()]
        [Appearance("InventoryTransferInLocationTypeClose", Enabled = false)]
        public LocationType LocationType
        {
            get { return _locationType; }
            set { SetPropertyValue("LocationType", ref _locationType, value); }
        }

        [ImmediatePostData()]
        [Appearance("InventoryTransferInStockTypeClose", Enabled = false)]
        public StockType StockType
        {
            get { return _stockType; }
            set { SetPropertyValue("StockType", ref _stockType, value); }
        }

        [Browsable(false)]
        public XPCollection<Location> AvailableLocation
        {
            get
            {
                if(!IsLoading)
                {
                    _localUserAccess = SecuritySystem.CurrentUserName;
                    string _beginString = null;
                    string _endString = null;
                    string _fullString = null;
                    string _locLocationCode = null;
                    UserAccess _locUserAccess = Session.FindObject<UserAccess>(new BinaryOperator("UserName", _localUserAccess));
                    List<string> _stringLocation = new List<string>();
                    if (_locUserAccess != null)
                    {
                        #region InventoryTransferIn
                        if (this.TransferType == DirectionType.External && this.InventoryMovingType == InventoryMovingType.Receive)
                        {
                            XPCollection<WarehouseSetupDetail> _locWhsSetupDetails = null;

                            if (this.LocationType == LocationType.None)
                            {
                                _locWhsSetupDetails = new XPCollection<WarehouseSetupDetail>
                                                    (Session, new GroupOperator(GroupOperatorType.And,
                                                    new BinaryOperator("UserAccess", _locUserAccess),
                                                    new BinaryOperator("Owner", true),
                                                    new BinaryOperator("Active", true)));
                            }
                            else
                            {
                                _locWhsSetupDetails = new XPCollection<WarehouseSetupDetail>
                                                    (Session, new GroupOperator(GroupOperatorType.And,
                                                    new BinaryOperator("UserAccess", _locUserAccess),
                                                    new BinaryOperator("Owner", true),
                                                    new BinaryOperator("LocationType", this.LocationType),
                                                    new BinaryOperator("Active", true)));
                            }

                            if (_locWhsSetupDetails != null && _locWhsSetupDetails.Count() > 0)
                            {
                                foreach (WarehouseSetupDetail _locWhsSetupDetail in _locWhsSetupDetails)
                                {
                                    if (_locWhsSetupDetail.Location != null)
                                    {
                                        if (_locWhsSetupDetail.Location.Code != null)
                                        {
                                            _locLocationCode = _locWhsSetupDetail.Location.Code;
                                            _stringLocation.Add(_locLocationCode);
                                        }
                                    }
                                }
                            }

                            IEnumerable<string> _stringArrayLocationFromDistinct = _stringLocation.Distinct();
                            string[] _stringArrayLocationFromList = _stringArrayLocationFromDistinct.ToArray();
                            if (_stringArrayLocationFromList.Length == 1)
                            {
                                for (int i = 0; i < _stringArrayLocationFromList.Length; i++)
                                {
                                    Location _locLocationFrom = Session.FindObject<Location>(new BinaryOperator("Code", _stringArrayLocationFromList[i]));
                                    if (_locLocationFrom != null)
                                    {
                                        if (i == 0)
                                        {
                                            _beginString = "[Code]=='" + _locLocationFrom.Code + "'";
                                        }
                                    }
                                }
                            }
                            else if (_stringArrayLocationFromList.Length > 1)
                            {
                                for (int i = 0; i < _stringArrayLocationFromList.Length; i++)
                                {
                                    Location _locLocationFrom = Session.FindObject<Location>(new BinaryOperator("Code", _stringArrayLocationFromList[i]));
                                    if (_locLocationFrom != null)
                                    {
                                        if (i == 0)
                                        {
                                            _beginString = "[Code]=='" + _locLocationFrom.Code + "'";
                                        }
                                        else
                                        {
                                            _endString = _endString + " OR [Code]=='" + _locLocationFrom.Code + "'";
                                        }
                                    }
                                }
                            }
                            _fullString = _beginString + _endString;

                            if (_fullString != null)
                            {
                                _availableLocation = new XPCollection<Location>(Session, CriteriaOperator.Parse(_fullString));
                            }
                        }
                        #endregion InventoryTransferIn
                    }
                }
                return _availableLocation;
            }
        }

        [RuleRequiredField(DefaultContexts.Save)]
        [Appearance("InventoryTransferInLocationClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [DataSourceProperty("AvailableLocation")]
        [ImmediatePostData()]
        [DataSourceCriteria("Active = true")]
        public Location Location
        {
            get { return _location; }
            set { SetPropertyValue("Location", ref _location, value); }
        }

        #endregion Location

        #region BusinessPartner

        [Browsable(false)]
        [ImmediatePostData()]
        public bool OpenVendor
        {
            get { return _openVendor; }
            set { SetPropertyValue("OpenVendor", ref _openVendor, value); }
        }

        [Appearance("InventoryTransferInBusinessPartnerHide", Criteria = "OpenVendor = false", Visibility = ViewItemVisibility.Hide, Context = "DetailView")]
        [Appearance("InventoryTransferInBusinessPartnerShow", Criteria = "OpenVendor = true", Visibility = ViewItemVisibility.Show, Context = "DetailView")]
        [Appearance("InventoryTransferInBusinessPartnerClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [ImmediatePostData()]
        [DataSourceCriteria("Active = true")]
        public BusinessPartner BusinessPartner
        {
            get { return _businessPartner; }
            set
            {
                SetPropertyValue("BusinessPartner", ref _businessPartner, value);
                if (!IsLoading)
                {
                    if (_businessPartner != null)
                    {
                        if(_businessPartner.Contact != null)
                        {
                            this.BusinessPartnerContact = this._businessPartner.Contact;
                        }
                        if (_businessPartner.Country != null)
                        {
                            this.BusinessPartnerCountry = this._businessPartner.Country;
                        }
                        if (_businessPartner.City != null)
                        {
                            this.BusinessPartnerCity = this._businessPartner.City;
                        }
                        if (_businessPartner.Address != null)
                        {
                            this.BusinessPartnerAddress = this._businessPartner.Address;
                        }
                    }
                    else
                    {
                        this.BusinessPartnerContact = null;
                        this.BusinessPartnerCountry = null;
                        this.BusinessPartnerCity = null;
                        this.BusinessPartnerAddress = null;
                    }
                }
            }
        }

        [Appearance("InventoryTransferInBusinessPartnerContactHide", Criteria = "OpenVendor = false", Visibility = ViewItemVisibility.Hide, Context = "DetailView")]
        [Appearance("InventoryTransferInBusinessPartnerContactShow", Criteria = "OpenVendor = true", Visibility = ViewItemVisibility.Show, Context = "DetailView")]
        [Appearance("InventoryTransferInBusinessPartnerContactClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public string BusinessPartnerContact
        {
            get { return _businessPartnerContact; }
            set { SetPropertyValue("BusinessPartnerContact", ref _businessPartnerContact, value); }
        }

        [Appearance("InventoryTransferInBusinessPartnerCountrytHide", Criteria = "OpenVendor = false", Visibility = ViewItemVisibility.Hide, Context = "DetailView")]
        [Appearance("InventoryTransferInBusinessPartnerCountryShow", Criteria = "OpenVendor = true", Visibility = ViewItemVisibility.Show, Context = "DetailView")]
        [Appearance("InventoryTransferInBusinessPartnerCountryClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [DataSourceCriteria("Active = true")]
        public Country BusinessPartnerCountry
        {
            get { return _businessPartnerCountry; }
            set { SetPropertyValue("BusinessPartnerCountry", ref _businessPartnerCountry, value); }
        }

        [Appearance("InventoryTransferInBusinessPartnerCitytHide", Criteria = "OpenVendor = false", Visibility = ViewItemVisibility.Hide, Context = "DetailView")]
        [Appearance("InventoryTransferInBusinessPartnerCityShow", Criteria = "OpenVendor = true", Visibility = ViewItemVisibility.Show, Context = "DetailView")]
        [Appearance("InventoryTransferInBusinessPartnerCityClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [DataSourceCriteria("Active = true")]
        public City BusinessPartnerCity
        {
            get { return _businessPartnerCity; }
            set { SetPropertyValue("BusinessPartnerCity", ref _businessPartnerCity, value); }
        }

        [Appearance("InventoryTransferInBusinessPartnerAddresstHide", Criteria = "OpenVendor = false", Visibility = ViewItemVisibility.Hide, Context = "DetailView")]
        [Appearance("InventoryTransferInBusinessPartnerAddressShow", Criteria = "OpenVendor = true", Visibility = ViewItemVisibility.Show, Context = "DetailView")]
        [Size(512)]
        [Appearance("InventoryTransferInBusinessPartnerAddressClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public string BusinessPartnerAddress
        {
            get { return _businessPartnerAddress; }
            set { SetPropertyValue("BusinessPartnerAddress", ref _businessPartnerAddress, value); }
        }

        #endregion BusinessPartner

        [Browsable(false)]
        [ImmediatePostData()]
        [ModelDefault("DisplayFormat", "{0:dd/MM/yyyy}")]
        [ModelDefault("EditMask", "dd/MM/yyyy")]
        [Appearance("InventoryTransferInDocDateClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public DateTime DocDate
        {
            get { return _docDate; }
            set
            {
                SetPropertyValue("DocDate", ref _docDate, value);
                if (!IsLoading)
                {
                    this.JournalMonth = this._docDate.Month;
                    this.JournalYear = this._docDate.Year;
                }
            }
        }

        [Browsable(false)]
        public int JournalMonth
        {
            get { return _journalMonth; }
            set { SetPropertyValue("JournalMonth", ref _journalMonth, value); }
        }

        [Browsable(false)]
        public int JournalYear
        {
            get { return _journalYear; }
            set { SetPropertyValue("JournalYear", ref _journalYear, value); }
        }

        [ModelDefault("DisplayFormat", "{0:dd/MM/yyyy}")]
        [ModelDefault("EditMask", "dd/MM/yyyy")]
        [Appearance("InventoryTransferInEstimatedDateClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public DateTime EstimatedDate
        {
            get { return _estimatedDate; }
            set { SetPropertyValue("EstimatedDate", ref _estimatedDate, value); }
        }

        [Appearance("InventoryTransferInStatusEnabled", Enabled = false)]
        public Status Status
        {
            get { return _status; }
            set { SetPropertyValue("Status", ref _status, value); }
        }

        [ModelDefault("DisplayFormat", "{0:dd/MM/yyyy}")]
        [ModelDefault("EditMask", "dd/MM/yyyy")]
        [Appearance("InventoryTransferInStatusDateEnabled", Enabled = false)]
        public DateTime StatusDate
        {
            get { return _statusDate; }
            set { SetPropertyValue("StatusDate", ref _statusDate, value); }
        }

        [Association("InventoryTransferIn-InventoryTransferInLines")]
        public XPCollection<InventoryTransferInLine> InventoryTransferInLines
        {
            get { return GetCollection<InventoryTransferInLine>("InventoryTransferInLines"); }
        }

        [Association("InventoryTransferIn-InventoryPurchaseCollections")]
        public XPCollection<InventoryPurchaseCollection> InventoryPurchaseCollections
        {
            get { return GetCollection<InventoryPurchaseCollection>("InventoryPurchaseCollections"); }
        }

        //AuditTrail
        [CollectionOperationSet(AllowAdd = false, AllowRemove = true)]
        public XPCollection<AuditDataItemPersistent> ChangeHistory
        {
            get
            {
                if (changeHistory == null)
                {
                    changeHistory = AuditedObjectWeakReference.GetAuditTrail(Session, this);
                }
                return changeHistory;
            }
        }

        #endregion Field
        
    }
}