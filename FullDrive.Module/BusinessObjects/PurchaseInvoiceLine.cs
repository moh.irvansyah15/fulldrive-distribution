﻿using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;

using DevExpress.ExpressApp.Editors;
using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.ConditionalAppearance;

namespace FullDrive.Module.BusinessObjects
{
    [DefaultClassOptions]
    [DefaultProperty("Code")]
    [NavigationItem("Finance & Accounting")]
    [RuleCombinationOfPropertiesIsUnique("PurchaseInvoiceLineRuleUnique", DefaultContexts.Save, "Code")]
    //[ImageName("BO_Contact")]
    //[DefaultProperty("DisplayMemberNameForLookupEditorsOfThisType")]
    //[DefaultListViewOptions(MasterDetailMode.ListViewOnly, false, NewItemRowPosition.None)]
    //[Persistent("DatabaseTableName")]
    // Specify more UI options using a declarative approach (https://documentation.devexpress.com/#eXpressAppFramework/CustomDocument112701).
    public class PurchaseInvoiceLine : BaseObject
    { // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).
        private bool _activationPosting;
        private int _no;        
        private string _code;
        private bool _select;
        private OrderType _purchaseType;
        private XPCollection<Item> _availableItem;
        private Item _item;
        private Brand _brand;
        private string _description;
        private XPCollection<UnitOfMeasure> _availableUnitOfMeasure;
        #region InisialisasiDefaultQty
        private double _dQty;
        private UnitOfMeasure _dUom;
        private double _qty;
        private UnitOfMeasure _uom;
        private double _tQty;
        #endregion InisialisasiDefaultQty
        #region InisialisasiAmount
        private Currency _currency;
        private double _uAmount;
        private double _tUAmount;
        private TaxRule _taxRule;
        private bool _multiTax;
        private Tax _tax;
        private double _txValue;
        private double _txAmount;
        private Discount _discount;
        private double _disc;
        private double _discAmount;
        private double _tAmount;
        #endregion InisialisasiAmount
        private TermOfPayment _top;
        #region InitialOrganization
        private Company _company;
        private Workplace _workplace;
        private Division _division;
        private Department _department;
        private Section _section;
        private Employee _employee;
        #endregion InitialOrganization
        private DateTime _docDate;
        private int _journalMonth;
        private int _journalYear;
        private Status _status;
        private DateTime _statusDate;
        private string _signCode;
        private PurchaseInvoice _purchaseInvoice;
        private InventoryTransferInMonitoring _inventoryTransferInMonitoring;
        private PurchaseOrderMonitoring _purchaseOrderMonitoring;       
        private string _localUserAccess;
        private GlobalFunction _globFunc;
        //AuditTrail
        private XPCollection<AuditDataItemPersistent> changeHistory;

        public PurchaseInvoiceLine(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if (!IsLoading)
            {
                if(Session.IsNewObject(this))
                {
                    _globFunc = new GlobalFunction();
                    DateTime now = DateTime.Now;
                    #region UserAccess
                    _localUserAccess = SecuritySystem.CurrentUserName;
                    UserAccess _locUserAccess = Session.FindObject<UserAccess>(new BinaryOperator("UserName", _localUserAccess));
                    if (_locUserAccess != null)
                    {
                        if (_locUserAccess.Employee != null)
                        {
                            if (_locUserAccess.Employee.Company != null && _locUserAccess.Employee.Workplace != null)
                            {
                                this.Code = _globFunc.GetNumberingUnlockOptimisticRecordBasedOrganization(this.Session.DataLayer, ObjectList.PurchaseInvoiceLine, _locUserAccess.Employee.Company, _locUserAccess.Employee.Workplace);
                            }
                            else
                            {
                                this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.PurchaseInvoiceLine);
                            }
                        }
                    }
                    #endregion UserAccess
                    this.Status = CustomProcess.Status.Open;
                    this.StatusDate = now;
                    this.Select = true;
                    this.Tax = _globFunc.GetDefaultTax(this.Session, TaxMode.Purchase);
                    this.Discount = _globFunc.GetDefaultDiscount(this.Session, DiscountMode.Purchase);
                }                
            }
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).
        }

        protected override void OnSaving()
        {
            base.OnSaving();
            if (!IsLoading && IsSaving)
            {
                UpdateNo();
            }
        }

        protected override void OnDeleting()
        {
            base.OnDeleting();
            if (!IsLoading)
            {
                RecoveryDeleteNo();
            }
        }

        #region Field

        [ImmediatePostData()]
        [Browsable(false)]
        public bool ActivationPosting
        {
            get { return _activationPosting; }
            set { SetPropertyValue("ActivationPosting", ref _activationPosting, value); }
        }

        [Appearance("PurchaseInvoiceLineNoClose", Enabled = false)]
        public int No
        {
            get { return _no; }
            set { SetPropertyValue("No", ref _no, value); }
        }

        [VisibleInListView(false)]
        [RuleRequiredField(DefaultContexts.Save)]
        [Appearance("PurchaseInvoiceLineCodeClose", Enabled = false)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue("Code", ref _code, value); }
        }

        [Appearance("PurchaseInvoiceLineSelectClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public bool Select
        {
            get { return _select; }
            set { SetPropertyValue("Select", ref _select, value); }
        }

        [VisibleInListView(false)]
        [Appearance("PurchaseInvoiceLinePurchaseTypeClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public OrderType PurchaseType
        {
            get { return _purchaseType; }
            set { SetPropertyValue("PurchaseType", ref _purchaseType, value); }
        }

        [Browsable(false)]
        public XPCollection<Item> AvailableItem
        {
            get
            {
                if(!IsLoading)
                {
                    if (PurchaseType == OrderType.Item)
                    {
                        _availableItem = new XPCollection<Item>(Session,
                                        new GroupOperator(GroupOperatorType.And,
                                        new BinaryOperator("ItemType", OrderType.Item)));
                    }
                    else if (PurchaseType == OrderType.Service)
                    {
                        _availableItem = new XPCollection<Item>(Session,
                                        new GroupOperator(GroupOperatorType.And,
                                        new BinaryOperator("ItemType", OrderType.Service)));
                    }
                    else
                    {
                        _availableItem = new XPCollection<Item>(Session);
                    }
                }
                return _availableItem;
            }
        }

        [ImmediatePostData()]
        [DataSourceProperty("AvailableItem", DataSourcePropertyIsNullMode.SelectAll)]
        [Appearance("PurchaseInvoiceLineItemClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Item Item
        {
            get { return _item; }
            set
            {
                SetPropertyValue("Item", ref _item, value);
                if (!IsLoading)
                {
                    if (this._item != null)
                    {
                        this.DUOM = this._item.BasedUOM;
                        this.Brand = this._item.Brand;
                        this.Description = GetDescriptions();
                    }
                }
            }
        }

        [VisibleInListView(false)]
        [Appearance("PurchaseInvoiceLineBrandClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Brand Brand
        {
            get { return _brand; }
            set { SetPropertyValue("Brand", ref _brand, value); }
        }

        [Appearance("PurchaseInvoiceLineDescriptionClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [Size(512)]
        public string Description
        {
            get { return _description; }
            set { SetPropertyValue("Description", ref _description, value); }
        }

        [Browsable(false)]
        public XPCollection<UnitOfMeasure> AvailableUnitOfMeasure
        {
            get
            {
                if(!IsLoading)
                {
                    string _beginString = null;
                    string _endString = null;
                    string _fullString = null;
                    if (Item == null)
                    {
                        _availableUnitOfMeasure = new XPCollection<UnitOfMeasure>(Session);
                    }
                    else
                    {
                        List<string> _stringUOM = new List<string>();

                        XPCollection<ItemUnitOfMeasure> _locItemUnitOfMeasures = new XPCollection<ItemUnitOfMeasure>
                                                                                  (Session, new GroupOperator(GroupOperatorType.And,
                                                                                   new BinaryOperator("Item", this.Item),
                                                                                   new BinaryOperator("Active", true)));

                        if (_locItemUnitOfMeasures != null && _locItemUnitOfMeasures.Count() > 0)
                        {
                            foreach (ItemUnitOfMeasure _locItemUnitOfMeasure in _locItemUnitOfMeasures)
                            {
                                _stringUOM.Add(_locItemUnitOfMeasure.UOM.Code);
                            }
                        }

                        IEnumerable<string> _stringArrayUOMDistinct = _stringUOM.Distinct();
                        string[] _stringArrayUOMList = _stringArrayUOMDistinct.ToArray();
                        if (_stringArrayUOMList.Length == 1)
                        {
                            for (int i = 0; i < _stringArrayUOMList.Length; i++)
                            {
                                UnitOfMeasure _locUOM = Session.FindObject<UnitOfMeasure>(new BinaryOperator("Code", _stringArrayUOMList[i]));
                                if (_locUOM != null)
                                {
                                    if (i == 0)
                                    {
                                        _beginString = "[Code]=='" + _locUOM.Code + "'";
                                    }
                                }
                            }
                        }
                        else if (_stringArrayUOMList.Length > 1)
                        {
                            for (int i = 0; i < _stringArrayUOMList.Length; i++)
                            {
                                UnitOfMeasure _locUOM = Session.FindObject<UnitOfMeasure>(new BinaryOperator("Code", _stringArrayUOMList[i]));
                                if (_locUOM != null)
                                {
                                    if (i == 0)
                                    {
                                        _beginString = "[Code]=='" + _locUOM.Code + "'";
                                    }
                                    else
                                    {
                                        _endString = _endString + " OR [Code]=='" + _locUOM.Code + "'";
                                    }
                                }
                            }
                        }
                        _fullString = _beginString + _endString;

                        if (_fullString != null)
                        {
                            _availableUnitOfMeasure = new XPCollection<UnitOfMeasure>(Session, CriteriaOperator.Parse(_fullString));
                        }
                    }
                }               
                return _availableUnitOfMeasure;
            }
        }

        #region DefaultQty

        [Appearance("PurchaseInvoiceLineDQtyClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [ImmediatePostData()]
        public double DQty
        {
            get { return _dQty; }
            set
            {
                SetPropertyValue("DQty", ref _dQty, value);
                if (!IsLoading)
                {
                    SetTotalQty();
                    SetTotalUnitAmount();
                    SetTotalAmount();
                }
            }
        }

        [Appearance("PurchaseInvoiceLineDUOMClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [ImmediatePostData()]
        [DataSourceCriteria("Active = true")]
        public UnitOfMeasure DUOM
        {
            get { return _dUom; }
            set
            {
                SetPropertyValue("DUOM", ref _dUom, value);
                if (!IsLoading)
                {
                    SetUOM();
                    SetTotalQty();
                    SetTotalUnitAmount();
                    SetTotalAmount();
                }
            }
        }

        [Appearance("PurchaseInvoiceLineQtyClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [ImmediatePostData()]
        public double Qty
        {
            get { return _qty; }
            set
            {
                SetPropertyValue("Qty", ref _qty, value);
                if (!IsLoading)
                {
                    SetTotalQty();
                    SetTotalUnitAmount();
                    SetTotalAmount();
                }
            }
        }

        [Appearance("PurchaseInvoiceLineUOMClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [ImmediatePostData()]
        [DataSourceProperty("AvailableUnitOfMeasure", DataSourcePropertyIsNullMode.SelectAll)]
        [DataSourceCriteria("Active = true")]
        public UnitOfMeasure UOM
        {
            get { return _uom; }
            set
            {
                SetPropertyValue("UOM", ref _uom, value);
                if (!IsLoading)
                {
                    SetTotalQty();
                    SetTotalUnitAmount();
                    SetTotalAmount();
                }
            }
        }

        [Appearance("PurchaseInvoiceLineTotalQtyEnabled", Enabled = false)]
        public double TQty
        {
            get { return _tQty; }
            set { SetPropertyValue("TotalQty", ref _tQty, value); }
        }

        [VisibleInListView(false)]
        [Persistent]
        public string FullName
        {
            get
            {
                if (this.DQty > 0 && this.Qty > 0)
                {
                    return String.Format("{0} {1} {2} {3}", this.Qty, this.UOM.Code, this.DQty, this.DUOM.Code);
                }
                else if (this.DQty > 0 && this.Qty == 0)
                {
                    return String.Format("{0} {1}", this.DQty, this.DUOM.Code);
                }
                else if (this.DQty == 0 && this.Qty > 0)
                {
                    return String.Format("{0} {1}", this.Qty, this.UOM.Code);
                }
                return null;
            }
        }

        #endregion DefaultQty       

        #region Amount

        [VisibleInListView(false)]
        [Appearance("PurchaseInvoiceLineCurrencyClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Currency Currency
        {
            get { return _currency; }
            set { SetPropertyValue("Currency", ref _currency, value); }
        }

        [Appearance("PurchaseInvoiceLineUnitAmountClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [ImmediatePostData()]
        public double UAmount
        {
            get { return _uAmount; }
            set
            {
                SetPropertyValue("UAmount", ref _uAmount, value);
                if (!IsLoading)
                {
                    _globFunc = new GlobalFunction();
                    if (this._uAmount > 0)
                    {
                        if (_globFunc.GetRoundingList(Session, ObjectList.PurchaseInvoiceLine, FieldName.UAmount) == true)
                        {
                            this._uAmount = _globFunc.GetRoundUp(Session, this._uAmount, ObjectList.PurchaseInvoiceLine, FieldName.UAmount);
                        }
                    }
                    SetTotalUnitAmount();
                    SetTotalAmount();
                }
            }
        }

        [Appearance("PurchaseInvoiceLineTotalUnitAmountEnabled", Enabled = false)]
        public double TUAmount
        {
            get { return _tUAmount; }
            set { SetPropertyValue("TUAmount", ref _tUAmount, value); }
        }

        [Appearance("PurchaseInvoiceLineTaxRuleClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public TaxRule TaxRule
        {
            get { return _taxRule; }
            set { SetPropertyValue("TaxRule", ref _taxRule, value); }
        }

        [Appearance("PurchaseInvoiceLineMultiTaxClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [ImmediatePostData()]
        public bool MultiTax
        {
            get { return _multiTax; }
            set
            {
                SetPropertyValue("ActivationPosting", ref _multiTax, value);
                if (!IsLoading)
                {
                    SetNormalTax();
                }
            }
        }

        [Appearance("PurchaseInvoiceLineTaxClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [Appearance("PurchaseInvoiceLineTaxEnabled", Criteria = "MultiTax = true", Enabled = false)]
        [ImmediatePostData]
        [DataSourceCriteria("Active = true")]
        public Tax Tax
        {
            get { return _tax; }
            set
            {
                SetPropertyValue("Tax", ref _tax, value);
                if (!IsLoading)
                {
                    if (this._tax != null)
                    {
                        this.TxValue = this._tax.Value;
                    }
                    else
                    {
                        SetNormalTax();
                    }
                }
            }
        }
        
        [Appearance("PurchaseInvoiceLineTxValueClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [Appearance("PurchaseInvoiceLineTxValueEnabled", Criteria = "MultiTax = true", Enabled = false)]
        [ImmediatePostData()]
        public double TxValue
        {
            get { return _txValue; }
            set { SetPropertyValue("TxValue", ref _txValue, value); }
        }

        [Appearance("PurchaseInvoiceLineTaxAmountEnabled", Enabled = false)]
        public double TxAmount
        {
            get { return _txAmount; }
            set
            {
                SetPropertyValue("TxAmount", ref _txAmount, value);
                if (!IsLoading)
                {
                    _globFunc = new GlobalFunction();
                    if (this._txAmount > 0)
                    {
                        if (_globFunc.GetRoundingList(Session, ObjectList.PurchaseInvoiceLine, FieldName.TxAmount) == true)
                        {
                            this._txAmount = _globFunc.GetRoundUp(Session, this._txAmount, ObjectList.PurchaseInvoiceLine, FieldName.TxAmount);
                        }
                    }

                    SetTotalAmount();
                }
            }
        }

        [ImmediatePostData()]
        [Appearance("PurchaseInvoiceLineDiscountClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Discount Discount
        {
            get { return _discount; }
            set
            {
                SetPropertyValue("Discount", ref _discount, value);
                if (!IsLoading)
                {
                    if (this._discount != null)
                    {
                        this.Disc = this._discount.Value;
                    }
                    else
                    {
                        SetNormalDiscount();
                    }
                }
            }
        }

        [VisibleInListView(false)]
        [Appearance("PurchaseInvoiceLineDiscClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [ImmediatePostData()]
        public double Disc
        {
            get { return _disc; }
            set
            {
                SetPropertyValue("Disc", ref _disc, value);
                if (!IsLoading)
                {
                    SetDiscAmount();
                    SetTotalAmount();
                }
            }
        }

        [Appearance("PurchaseInvoiceLineDiscAmountEnabled", Enabled = false)]
        public double DiscAmount
        {
            get { return _discAmount; }
            set
            {
                SetPropertyValue("DiscAmount", ref _discAmount, value);
                if (!IsLoading)
                {
                    _globFunc = new GlobalFunction();
                    if (this._discAmount > 0)
                    {
                        if (_globFunc.GetRoundingList(Session, ObjectList.PurchaseInvoiceLine, FieldName.DiscAmount) == true)
                        {
                            this._discAmount = _globFunc.GetRoundUp(Session, this._discAmount, ObjectList.PurchaseInvoiceLine, FieldName.DiscAmount);
                        }
                    }
                }
            }
        }

        [Appearance("PurchaseInvoiceLineTotalPriceEnabled", Enabled = false)]
        public double TAmount
        {
            get { return _tAmount; }
            set
            {
                SetPropertyValue("TAmount", ref _tAmount, value);
                if (!IsLoading)
                {
                    _globFunc = new GlobalFunction();
                    if (this._tAmount > 0)
                    {
                        if (_globFunc.GetRoundingList(Session, ObjectList.PurchaseInvoiceLine, FieldName.TAmount) == true)
                        {
                            this._tAmount = _globFunc.GetRoundUp(Session, this._tAmount, ObjectList.PurchaseInvoiceLine, FieldName.TAmount);
                        }
                    }
                }
            }
        }

        #endregion Amount

        [VisibleInListView(false)]
        [Appearance("PurchaseInvoiceLineTOPClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public TermOfPayment TOP
        {
            get { return _top; }
            set { SetPropertyValue("TOP", ref _top, value); }
        }

        #region Organization

        [VisibleInListView(false)]
        [Appearance("PurchaseInvoiceLineCompanyClose", Enabled = false)]
        public Company Company
        {
            get { return _company; }
            set { SetPropertyValue("Company", ref _company, value); }
        }

        [VisibleInListView(false)]
        [Appearance("PurchaseInvoiceLineWorkplaceClose", Enabled = false)]
        public Workplace Workplace
        {
            get { return _workplace; }
            set { SetPropertyValue("Workplace", ref _workplace, value); }
        }

        [Browsable(false)]
        public Division Division
        {
            get { return _division; }
            set { SetPropertyValue("Division", ref _division, value); }
        }

        [Browsable(false)]
        public Department Department
        {
            get { return _department; }
            set { SetPropertyValue("Department", ref _department, value); }
        }

        [Browsable(false)]
        public Section Section
        {
            get { return _section; }
            set { SetPropertyValue("Section", ref _section, value); }
        }

        [Browsable(false)]
        public Employee Employee
        {
            get { return _employee; }
            set { SetPropertyValue("Employee", ref _employee, value); }
        }

        #endregion Organization

        [Browsable(false)]
        [ImmediatePostData()]
        [ModelDefault("DisplayFormat", "{0:dd/MM/yyyy}")]
        [ModelDefault("EditMask", "dd/MM/yyyy")]
        [Appearance("PurchaseInvoiceLineDocDateClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public DateTime DocDate
        {
            get { return _docDate; }
            set
            {
                SetPropertyValue("DocDate", ref _docDate, value);
                if (!IsLoading)
                {
                    this.JournalMonth = this._docDate.Month;
                    this.JournalYear = this._docDate.Year;
                }
            }
        }

        [Browsable(false)]
        public int JournalMonth
        {
            get { return _journalMonth; }
            set { SetPropertyValue("JournalMonth", ref _journalMonth, value); }
        }

        [Browsable(false)]
        public int JournalYear
        {
            get { return _journalYear; }
            set { SetPropertyValue("JournalYear", ref _journalYear, value); }
        }

        [VisibleInListView(false)]
        [Appearance("PurchaseInvoiceLineStatusClose", Enabled = false)]
        public Status Status
        {
            get { return _status; }
            set { SetPropertyValue("Status", ref _status, value); }
        }

        [VisibleInListView(false)]
        [ModelDefault("DisplayFormat", "{0:dd/MM/yyyy}")]
        [ModelDefault("EditMask", "dd/MM/yyyy")]
        [Appearance("PurchaseInvoiceLineStatusDateClose", Enabled = false)]
        public DateTime StatusDate
        {
            get { return _statusDate; }
            set { SetPropertyValue("StatusDate", ref _statusDate, value); }
        }

        [VisibleInListView(false)]
        [Appearance("PurchaseInvoiceLineSignCodeClose", Enabled = false)]
        public string SignCode
        {
            get { return _signCode; }
            set { SetPropertyValue("SignCode", ref _signCode, value); }
        }

        [VisibleInListView(false)]
        [Appearance("PurchaseInvoiceLineInventoryTransferInMonitoringClose", Enabled = false)]
        public InventoryTransferInMonitoring InventoryTransferInMonitoring
        {
            get { return _inventoryTransferInMonitoring; }
            set { SetPropertyValue("InventoryTransferInMonitoring", ref _inventoryTransferInMonitoring, value); }
        }

        [VisibleInListView(false)]
        [Appearance("PurchaseInvoiceLinePurchaseOrderMonitoringClose", Enabled = false)]
        public PurchaseOrderMonitoring PurchaseOrderMonitoring
        {
            get { return _purchaseOrderMonitoring; }
            set { SetPropertyValue("PurchaseOrderMonitoring", ref _purchaseOrderMonitoring, value); }
        }

        [VisibleInListView(false)]
        [Association("PurchaseInvoice-PurchaseInvoiceLines")]
        public PurchaseInvoice PurchaseInvoice
        {
            get { return _purchaseInvoice; }
            set
            {
                SetPropertyValue("PurchaseInvoice", ref _purchaseInvoice, value);
                if (!IsLoading)
                {
                    if(this._purchaseInvoice != null)
                    {
                        if (this._purchaseInvoice.Company != null)
                        {
                            this.Company = this._purchaseInvoice.Company;
                        }
                        if (this._purchaseInvoice.Workplace != null)
                        {
                            this.Workplace = this._purchaseInvoice.Workplace;
                        }
                        if (this._purchaseInvoice.Division != null)
                        {
                            this.Division = this._purchaseInvoice.Division;
                        }
                        if (this._purchaseInvoice.Department != null)
                        {
                            this.Department = this._purchaseInvoice.Department;
                        }
                        if (this._purchaseInvoice.Section != null)
                        {
                            this.Section = this._purchaseInvoice.Section;
                        }
                        if (this._purchaseInvoice.Employee != null)
                        {
                            this.Employee = this._purchaseInvoice.Employee;
                        }
                        if (this._purchaseInvoice.Currency != null)
                        {
                            this.Currency = this._purchaseInvoice.Currency;
                        }
                        if (this._purchaseInvoice.TOP != null)
                        {
                            this.TOP = this._purchaseInvoice.TOP;
                        }
                    }                    
                }
            }
        }

        [Association("PurchaseInvoiceLine-TaxLines")]
        [Appearance("PurchaseInvoiceLineTaxLineClose", Criteria = "MultiTax = false", Visibility = ViewItemVisibility.Hide, Context = "DetailView")]
        [Appearance("PurchaseInvoiceLineTaxLineEnabled", Criteria = "MultiTax = true", Visibility = ViewItemVisibility.Show, Context = "DetailView")]
        public XPCollection<TaxLine> TaxLines
        {
            get { return GetCollection<TaxLine>("TaxLines"); }
        }

        //AuditTrail
        [CollectionOperationSet(AllowAdd = false, AllowRemove = true)]
        public XPCollection<AuditDataItemPersistent> ChangeHistory
        {
            get
            {
                if (changeHistory == null)
                {
                    changeHistory = AuditedObjectWeakReference.GetAuditTrail(Session, this);
                }
                return changeHistory;
            }
        }

        #endregion Field

        //============================================= Code Only =============================================

        public string GetDescriptions()
        {
            string _result = "";
            try
            {
                if (this._item != null)
                {
                    string _result1 = null;
                    string _result2 = null;
                    if (this._item.Name != null)
                    {
                        _result1 = this._item.Name;
                    }
                    if (this._item.Description != null)
                    {
                        _result2 = this._item.Description;
                    }
                    _result = _result1 + " " + _result2;
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PurchaseInvoiceLine " + ex.ToString());
            }

            return _result;
        }

        #region No

        public void UpdateNo()
        {
            try
            {
                if (!IsLoading && Session.IsNewObject(this))
                {
                    if (this.PurchaseInvoice != null)
                    {
                        object _makRecord = Session.Evaluate<PurchaseInvoiceLine>(CriteriaOperator.Parse("Max(No)"), CriteriaOperator.Parse("PurchaseInvoice=?", this.PurchaseInvoice));
                        this.No = Convert.ToInt32(_makRecord) + 1;
                        this.Save();
                        RecoveryUpdateNo();
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PurchaseInvoiceLine " + ex.ToString());
            }
        }

        public void RecoveryUpdateNo()
        {
            try
            {
                if (this.PurchaseInvoice != null)
                {
                    PurchaseInvoice _numHeader = Session.FindObject<PurchaseInvoice>
                                                (new BinaryOperator("Code", this.PurchaseInvoice.Code));

                    XPCollection<PurchaseInvoiceLine> _numLines = new XPCollection<PurchaseInvoiceLine>
                                                (Session, new BinaryOperator("PurchaseInvoice", _numHeader),
                                                new SortProperty("No", DevExpress.Xpo.DB.SortingDirection.Ascending));
                    if (_numLines != null)
                    {
                        int i1 = 0;
                        foreach (PurchaseInvoiceLine _numLine in _numLines)
                        {
                            i1 += 1;
                            _numLine.No = i1;
                            _numLine.Save();
                        }
                        i1 = 1;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PurchaseInvoiceLine " + ex.ToString());
            }
        }

        public void RecoveryDeleteNo()
        {
            try
            {
                if (this.PurchaseInvoice != null)
                {
                    PurchaseInvoice _numHeader = Session.FindObject<PurchaseInvoice>
                                                (new BinaryOperator("Code", this.PurchaseInvoice.Code));

                    XPCollection<PurchaseInvoiceLine> _numLines = new XPCollection<PurchaseInvoiceLine>
                                                (Session, new GroupOperator(GroupOperatorType.And,
                                                 new BinaryOperator("This", this, BinaryOperatorType.NotEqual),
                                                 new BinaryOperator("PurchaseInvoice", _numHeader)),
                                                 new SortProperty("No", DevExpress.Xpo.DB.SortingDirection.Ascending));
                    if (_numLines != null)
                    {
                        int i = 0;
                        foreach (PurchaseInvoiceLine _numLine in _numLines)
                        {
                            i += 1;
                            _numLine.No = i;
                            _numLine.Save();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PurchaseInvoiceLine " + ex.ToString());
            }
        }

        #endregion No

        #region Set

        private void SetUOM()
        {
            try
            {
                if (this.Company != null && this.Item != null && this.DUOM != null)
                {
                    ItemUnitOfMeasure _locIUOM = Session.FindObject<ItemUnitOfMeasure>(new GroupOperator(GroupOperatorType.And,
                                                                            new BinaryOperator("Company", this.Company),
                                                                            new BinaryOperator("Item", this.Item),
                                                                            new BinaryOperator("DefaultUOM", this.DUOM),
                                                                            new BinaryOperator("Default", true),
                                                                            new BinaryOperator("Active", true)));
                    if (_locIUOM != null)
                    {
                        if (_locIUOM.UOM != null)
                        {
                            this.UOM = _locIUOM.UOM;
                        }
                        else
                        {
                            this.UOM = null;
                        }
                    }
                    else
                    {
                        this.UOM = null;
                    }
                }
                else
                {
                    this.UOM = null;
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PurchaseInvoiceLine " + ex.ToString());
            }
        }

        private void SetTotalUnitAmount()
        {
            try
            {
                _globFunc = new GlobalFunction();

                if (_tQty >= 0 & _uAmount >= 0)
                {
                    if (_globFunc.GetRoundingList(Session, ObjectList.PurchaseInvoiceLine, FieldName.TUAmount) == true)
                    {
                        this.TUAmount = _globFunc.GetRoundUp(Session, (this.TQty * this.UAmount), ObjectList.PurchaseInvoiceLine, FieldName.TUAmount);
                    }
                    else
                    {
                        this.TUAmount = this.TQty * this.UAmount;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PurchaseInvoiceLine " + ex.ToString());
            }
        }

        private void SetTotalAmount()
        {
            try
            {
                _globFunc = new GlobalFunction();

                if (_uAmount >= 0 && _txAmount >= 0 && _discAmount >= 0)
                {
                    if (!IsLoading)
                    {
                        if (this.Tax != null)
                        {
                            if (this.Tax.TaxNature == TaxNature.Increase)
                            {
                                if (_globFunc.GetRoundingList(Session, ObjectList.PurchaseInvoiceLine, FieldName.TAmount) == true)
                                {
                                    this.TAmount = _globFunc.GetRoundUp(Session, (this.TUAmount + this.TxAmount - this.DiscAmount), ObjectList.PurchaseInvoiceLine, FieldName.TAmount);
                                }
                                else
                                {
                                    this.TAmount = this.TUAmount + this.TxAmount - this.DiscAmount;
                                }
                            }
                            else if (this.Tax.TaxNature == TaxNature.Decrease)
                            {
                                if (_globFunc.GetRoundingList(Session, ObjectList.PurchaseInvoiceLine, FieldName.TAmount) == true)
                                {
                                    this.TAmount = _globFunc.GetRoundUp(Session, (this.TUAmount - this.TxAmount - this.DiscAmount), ObjectList.PurchaseInvoiceLine, FieldName.TAmount);
                                }
                                else
                                {
                                    this.TAmount = this.TUAmount - this.TxAmount - this.DiscAmount;
                                }
                            }
                            else if (this.Tax.TaxNature == TaxNature.None)
                            {
                                if (_globFunc.GetRoundingList(Session, ObjectList.PurchaseInvoiceLine, FieldName.TAmount) == true)
                                {
                                    this.TAmount = _globFunc.GetRoundUp(Session, (this.TUAmount - this.DiscAmount), ObjectList.PurchaseInvoiceLine, FieldName.TAmount);
                                }
                                else
                                {
                                    this.TAmount = this.TUAmount - this.DiscAmount;
                                }
                            }
                            else
                            {
                                if (_globFunc.GetRoundingList(Session, ObjectList.PurchaseInvoiceLine, FieldName.TAmount) == true)
                                {
                                    this.TAmount = _globFunc.GetRoundUp(Session, (this.TUAmount + this.TxAmount - this.DiscAmount), ObjectList.PurchaseInvoiceLine, FieldName.TAmount);
                                }
                                else
                                {
                                    this.TAmount = this.TUAmount + this.TxAmount - this.DiscAmount;
                                }
                            }
                        }
                        else
                        {
                            this.TAmount = this.TUAmount + this.TxAmount - this.DiscAmount;
                        }
                    }
                }
                else
                {
                    this.TAmount = this.TUAmount + this.TxAmount - this.DiscAmount;
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PurchaseInvoiceLine " + ex.ToString());
            }
        }

        private void SetTotalQty()
        {
            try
            {
                double _locInvLineTotal = 0;
                DateTime now = DateTime.Now;
                if (this.PurchaseInvoice != null)
                {
                    ItemUnitOfMeasure _locItemUOM = Session.FindObject<ItemUnitOfMeasure>
                                                        (new GroupOperator(GroupOperatorType.And,
                                                         new BinaryOperator("Item", this.Item),
                                                         new BinaryOperator("UOM", this.UOM),
                                                         new BinaryOperator("DefaultUOM", this.DUOM),
                                                         new BinaryOperator("Active", true)));
                    if (_locItemUOM != null)
                    {
                        if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                        {
                            _locInvLineTotal = this.Qty * _locItemUOM.DefaultConversion + this.DQty;
                        }
                        else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                        {
                            _locInvLineTotal = this.Qty / _locItemUOM.Conversion + this.DQty;
                        }
                        else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                        {
                            _locInvLineTotal = this.Qty + this.DQty;
                        }

                        this.TQty = _locInvLineTotal;
                    }
                    else
                    {
                        _locInvLineTotal = this.Qty + this.DQty;
                        this.TQty = _locInvLineTotal;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PurchaseInvoiceLine " + ex.ToString());
            }
        }

        private void SetDiscAmount()
        {
            try
            {
                _globFunc = new GlobalFunction();

                if (_uAmount >= 0 && _discAmount >= 0)
                {
                    if (_globFunc.GetRoundingList(Session, ObjectList.PurchaseInvoiceLine, FieldName.DiscAmount) == true)
                    {
                        this.DiscAmount = _globFunc.GetRoundUp(Session, (this.TUAmount * this.Disc / 100), ObjectList.PurchaseInvoiceLine, FieldName.DiscAmount);
                    }
                    else
                    {
                        this.DiscAmount = this.TUAmount * this.Disc / 100;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PurchaseInvoiceLine " + ex.ToString());
            }
        }

        #endregion Set

        #region Tax

        private void SetNormalTax()
        {
            try
            {
                if (_tax != null && _txValue > 0 && _txAmount > 0)
                {
                    this.Tax = this.Tax;
                    this.TxValue = this.TxValue;
                    this.TxAmount = this.TxAmount;
                }
                else
                {
                    this.TxValue = 0;
                    this.TxAmount = 0;
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PurchaseInvoiceLine " + ex.ToString());
            }
        }

        private Tax UpdateTaxLine(bool forceChangeEvents)
        {
            Tax _result = null;
            try
            {
                Tax _locTax = null;
                XPCollection<TaxLine> _locTaxLines = new XPCollection<TaxLine>(Session,
                                                     new GroupOperator(GroupOperatorType.And,
                                                     new BinaryOperator("PurchaseInvoiceLine", this)),
                                                     new SortProperty("No", DevExpress.Xpo.DB.SortingDirection.Ascending));

                if (_locTaxLines != null && _locTaxLines.Count > 0)
                {
                    if (_locTaxLines.Count == 1)
                    {
                        foreach (TaxLine _locTaxLinerLine in _locTaxLines)
                        {
                            if (forceChangeEvents)
                            {
                                //OnChanged("CurrencyLine", oldCurrencyLine, _locPurchaseOrderLine.Currency);
                                _result = _locTaxLinerLine.Tax;
                            }
                        }
                    }
                    else
                    {
                        foreach (TaxLine _locTaxLine in _locTaxLines)
                        {
                            if (_locTaxLine.No == 1)
                            {
                                _locTax = _locTaxLine.Tax;
                            }
                            else
                            {
                                if (_locTaxLine.Tax != _locTax)
                                {
                                    _globFunc = new GlobalFunction();
                                    //OnChanged("CurrencyLine", oldCurrencyLine, _globFunc.GetDefaultCurrency(Session));
                                    _result = _globFunc.GetDefaultTax(Session, TaxMode.Purchase);
                                    break;
                                }
                                else
                                {
                                    _result = _locTaxLine.Tax;
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = PurchaseInvoiceLine ", ex.ToString());
            }
            return _result;
        }

        #endregion Tax

        #region Discount

        private void SetNormalDiscount()
        {
            try
            {
                if (_discount != null)
                {
                    this.Disc = this.Disc;
                    this.DiscAmount = this.DiscAmount;
                }
                else
                {
                    this.Disc = 0;
                    this.DiscAmount = 0;
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PurchaseInvoiceLine " + ex.ToString());
            }
        }

        #endregion Discount

    }
}