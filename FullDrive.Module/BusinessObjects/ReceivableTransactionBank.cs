﻿using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;

using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.ConditionalAppearance;
using DevExpress.ExpressApp.SystemModule;

namespace FullDrive.Module.BusinessObjects
{
    [DefaultClassOptions]
    [DefaultProperty("Code")]
    [NavigationItem("Finance & Accounting")]
    [RuleCombinationOfPropertiesIsUnique("ReceivableTransactionBankRuleUnique", DefaultContexts.Save, "Code")]
    //[ImageName("BO_Contact")]
    //[DefaultProperty("DisplayMemberNameForLookupEditorsOfThisType")]
    //[DefaultListViewOptions(MasterDetailMode.ListViewOnly, false, NewItemRowPosition.None)]
    //[Persistent("DatabaseTableName")]
    // Specify more UI options using a declarative approach (https://documentation.devexpress.com/#eXpressAppFramework/CustomDocument112701).
    public class ReceivableTransactionBank : BaseObject
    { // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).

        private bool _activationPosting;
        private string _code;
        #region InitialOrganization
        private Company _company;
        private Workplace _workplace;
        private Division _division;
        private Department _department;
        private Section _section;
        private Employee _employee;
        #endregion InitialOrganization
        private SalesInvoice _salesInvoice;
        private double _amount;
        #region InitialBank
        private bool _bankBasedInvoice;
        private XPCollection<BankAccount> _availableCompanyBankAccount;
        private BankAccount _companyBankAccount;
        private string _companyAccountNo;
        private string _companyAccountName;
        private XPCollection<BankAccount> _availableBankAccount;
        private BankAccount _bankAccount;
        private string _accountNo;
        private string _accountName;
        #endregion InitialBank
        private Status _status;
        private DateTime _statusdate;
        private ReceivableTransactionLine _receivableTransactionLine;
        private ReceivableTransactionLine2 _receivableTransactionLine2;
        private string _userAccessIn;
        private GlobalFunction _globFunc;
        //AuditTrail
        private XPCollection<AuditDataItemPersistent> changeHistory;

        public ReceivableTransactionBank(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if (!IsLoading)
            {
                if (Session.IsNewObject(this))
                {
                    DateTime now = DateTime.Now;
                    _globFunc = new GlobalFunction();
                    #region UserAccess
                    _userAccessIn = SecuritySystem.CurrentUserName;
                    UserAccess _locUserAccess = Session.FindObject<UserAccess>(new BinaryOperator("UserName", _userAccessIn));
                    if (_locUserAccess != null)
                    {
                        if (_locUserAccess.Employee != null)
                        {
                            if (_locUserAccess.Employee.Company != null && _locUserAccess.Employee.Workplace != null)
                            {
                                this.Code = _globFunc.GetNumberingUnlockOptimisticRecordBasedOrganization(this.Session.DataLayer, ObjectList.ReceivableTransactionBank, _locUserAccess.Employee.Company, _locUserAccess.Employee.Workplace);
                            }
                            else
                            {
                                this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.ReceivableTransactionBank);
                            }
                        }
                    }
                    #endregion UserAccess
                    this.Status = Status.Open;
                    this.StatusDate = now;
                }
            }
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).
        }

        #region Field

        [Browsable(false)]
        [ImmediatePostData()]
        public bool ActivationPosting
        {
            get { return _activationPosting; }
            set { SetPropertyValue("ActivationPosting", ref _activationPosting, value); }
        }

        [Appearance("ReceivableTransactionBankClose", Enabled = false)]
        [RuleRequiredField(DefaultContexts.Save)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue("Code", ref _code, value); }
        }

        #region Organization

        [Appearance("ReceivableTransactionBankCompanyClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Company Company
        {
            get { return _company; }
            set { SetPropertyValue("Company", ref _company, value); }
        }

        [Appearance("ReceivableTransactionBankWorkplaceClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Workplace Workplace
        {
            get { return _workplace; }
            set { SetPropertyValue("Workplace", ref _workplace, value); }
        }

        [Browsable(false)]
        public Division Division
        {
            get { return _division; }
            set { SetPropertyValue("Division", ref _division, value); }
        }

        [Browsable(false)]
        public Department Department
        {
            get { return _department; }
            set { SetPropertyValue("Department", ref _department, value); }
        }

        [Browsable(false)]
        public Section Section
        {
            get { return _section; }
            set { SetPropertyValue("Section", ref _section, value); }
        }

        [Browsable(false)]
        public Employee Employee
        {
            get { return _employee; }
            set { SetPropertyValue("Employee", ref _employee, value); }
        }

        #endregion Organization

        [Appearance("ReceivableTransactionBankSalesInvoiceClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public SalesInvoice SalesInvoice
        {
            get { return _salesInvoice; }
            set { SetPropertyValue("SalesInvoice", ref _salesInvoice, value); }
        }

        [ImmediatePostData()]
        [Appearance("ReceivableTransactionBankAmountClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public double Amount
        {
            get { return _amount; }
            set { SetPropertyValue("Amount", ref _amount, value); }
        }

        #region Bank

        [ImmediatePostData()]
        public bool BankBasedInvoice
        {
            get { return _bankBasedInvoice; }
            set
            {
                SetPropertyValue("BankBasedInvoice", ref _bankBasedInvoice, value);
                if (!IsLoading)
                {
                    if (this._bankBasedInvoice == true)
                    {
                        if (this.SalesInvoice != null)
                        {
                            if (this.SalesInvoice.CompanyBankAccount != null) { this.CompanyBankAccount = this.SalesInvoice.CompanyBankAccount; }
                            if (this.SalesInvoice.CompanyAccountName != null) { this.CompanyAccountName = this.SalesInvoice.CompanyAccountName; }
                            if (this.SalesInvoice.CompanyAccountNo != null) { this.CompanyAccountNo = this.SalesInvoice.CompanyAccountNo; }
                            if (this.SalesInvoice.BankAccount != null) { this.BankAccount = this.SalesInvoice.BankAccount; }
                            if (this.SalesInvoice.AccountName != null) { this.AccountName = this.SalesInvoice.AccountName; }
                            if (this.SalesInvoice.AccountNo != null) { this.CompanyAccountNo = this.SalesInvoice.AccountNo; }
                        }
                    }
                    else
                    {
                        this.CompanyBankAccount = null;
                        this.CompanyAccountName = null;
                        this.CompanyAccountNo = null;
                        this.BankAccount = null;
                        this.AccountName = null;
                        this.AccountNo = null;
                    }
                }
            }
        }

        [Browsable(false)]
        public XPCollection<BankAccount> AvailableCompanyBankAccount
        {
            get
            {
                if (!IsLoading)
                {
                    if (this.Company != null)
                    {
                        if (this.Workplace != null)
                        {
                            _availableCompanyBankAccount = new XPCollection<BankAccount>(Session, new GroupOperator
                                                    (GroupOperatorType.And,
                                                    new BinaryOperator("Company", this.Company),
                                                    new BinaryOperator("Workplace", this.Workplace),
                                                    new BinaryOperator("Active", true)));
                        }
                        else
                        {
                            _availableCompanyBankAccount = new XPCollection<BankAccount>(Session, new GroupOperator
                                                    (GroupOperatorType.And,
                                                    new BinaryOperator("Company", this.Company),
                                                    new BinaryOperator("Active", true)));
                        }
                    }
                    else
                    {
                        _availableCompanyBankAccount = new XPCollection<BankAccount>(Session,
                            new GroupOperator(GroupOperatorType.And,
                            new BinaryOperator("Active", true)));
                    }
                }

                return _availableCompanyBankAccount;

            }
        }

        [ImmediatePostData()]
        [DataSourceProperty("AvailableCompanyBankAccount")]
        [Appearance("ReceivableTransactionLineCompanyBankAccountClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public BankAccount CompanyBankAccount
        {
            get { return _companyBankAccount; }
            set
            {
                SetPropertyValue("CompanyBankAccount", ref _companyBankAccount, value);
                if (!IsLoading)
                {
                    if (this._companyBankAccount != null)
                    {
                        this.CompanyAccountNo = this._companyBankAccount.AccountNo;
                        this.CompanyAccountName = this._companyBankAccount.AccountName;
                    }
                }
            }
        }

        [Appearance("ReceivableTransactionLineCompanyAccountNoClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public string CompanyAccountNo
        {
            get { return _companyAccountNo; }
            set { SetPropertyValue("CompanyAccountNo", ref _companyAccountNo, value); }
        }

        [Appearance("ReceivableTransactionLineCompanyAccountNameClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public string CompanyAccountName
        {
            get { return _companyAccountName; }
            set { SetPropertyValue("CompanyAccountName", ref _companyAccountName, value); }
        }

        [Browsable(false)]
        public XPCollection<BankAccount> AvailableBankAccount
        {
            get
            {
                if (this.Company != null && this.Workplace != null)
                {
                    if (this.SalesInvoice != null)
                    {
                        if (this.SalesInvoice.BillToCustomer != null)
                        {
                            _availableBankAccount = new XPCollection<BankAccount>(Session,
                                               new GroupOperator(GroupOperatorType.And,
                                               new BinaryOperator("Company", this.Company),
                                               new BinaryOperator("Workplace", this.Workplace),
                                               new BinaryOperator("BusinessPartner", this.SalesInvoice.BillToCustomer),
                                               new BinaryOperator("Active", true)));
                        }
                    }
                }
                else
                {

                    _availableBankAccount = new XPCollection<BankAccount>(Session,
                                            new GroupOperator(GroupOperatorType.And,
                                            new BinaryOperator("Active", true)));

                }
                return _availableBankAccount;

            }
        }

        [Appearance("ReceivableTransactionLineBankAccountClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [DataSourceProperty("AvailableBankAccount")]
        [DataSourceCriteria("Active = true")]
        public BankAccount BankAccount
        {
            get { return _bankAccount; }
            set
            {
                SetPropertyValue("BankAccount", ref _bankAccount, value);
                if (!IsLoading)
                {
                    if (this._bankAccount != null)
                    {
                        this.AccountNo = this._bankAccount.AccountNo;
                        this.AccountName = this._bankAccount.AccountName;
                    }
                }
            }
        }

        [Appearance("ReceivableTransactionLineAccountNoClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public string AccountNo
        {
            get { return _accountNo; }
            set { SetPropertyValue("AccountNo", ref _accountNo, value); }
        }

        [Appearance("ReceivableTransactionLineAccountNameClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public string AccountName
        {
            get { return _accountName; }
            set { SetPropertyValue("AccountName", ref _accountName, value); }
        }

        #endregion Bank

        [Appearance("ReceivableTransactionLineStatusClose", Enabled = false)]
        public Status Status
        {
            get { return _status; }
            set { SetPropertyValue("Status", ref _status, value); }
        }

        [ModelDefault("DisplayFormat", "{0:dd/MM/yyyy}")]
        [ModelDefault("EditMask", "dd/MM/yyyy")]
        [Appearance("ReceivableTransactionLineStatusDateClose", Enabled = false)]
        public DateTime StatusDate
        {
            get { return _statusdate; }
            set { SetPropertyValue("StatusDate", ref _statusdate, value); }
        }

        [Association("ReceivableTransactionLine-ReceivableTransactionBanks")]
        [Appearance("ReceivableTransactionBankReceivableTransactionLineClose", Enabled = false)]
        public ReceivableTransactionLine ReceivableTransactionLine
        {
            get { return _receivableTransactionLine; }
            set {
                SetPropertyValue("ReceivableTransactionLine", ref _receivableTransactionLine, value);
                if (!IsLoading)
                {
                    if (this._receivableTransactionLine != null)
                    {
                        if (this._receivableTransactionLine.Company != null) { this.Company = this._receivableTransactionLine.Company; }
                        if (this._receivableTransactionLine.Workplace != null) { this.Workplace = this._receivableTransactionLine.Workplace; }
                        if (this._receivableTransactionLine.Division != null) { this.Division = this._receivableTransactionLine.Division; }
                        if (this._receivableTransactionLine.Department != null) { this.Department = this._receivableTransactionLine.Department; }
                        if (this._receivableTransactionLine.Section != null) { this.Section = this._receivableTransactionLine.Section; }
                        if (this._receivableTransactionLine.Employee != null) { this.Employee = this._receivableTransactionLine.Employee; }
                        if (this._receivableTransactionLine.SalesInvoice != null) { this.SalesInvoice = this._receivableTransactionLine.SalesInvoice; }
                        if (this._receivableTransactionLine.Amount > 0) { this.Amount = this._receivableTransactionLine.Amount; }
                    }
                }
            }
        }

        [Association("ReceivableTransactionLine2-ReceivableTransactionBanks")]
        [Appearance("ReceivableTransactionBankReceivableTransactionLine2Close", Enabled = false)]
        public ReceivableTransactionLine2 ReceivableTransactionLine2
        {
            get { return _receivableTransactionLine2; }
            set
            {
                SetPropertyValue("ReceivableTransactionLine2", ref _receivableTransactionLine2, value);
                if (!IsLoading)
                {
                    if (this._receivableTransactionLine2 != null)
                    {
                        if (this._receivableTransactionLine2.Company != null) { this.Company = this._receivableTransactionLine2.Company; }
                        if (this._receivableTransactionLine2.Workplace != null) { this.Workplace = this._receivableTransactionLine2.Workplace; }
                        if (this._receivableTransactionLine2.Division != null) { this.Division = this._receivableTransactionLine2.Division; }
                        if (this._receivableTransactionLine2.Department != null) { this.Department = this._receivableTransactionLine2.Department; }
                        if (this._receivableTransactionLine2.Section != null) { this.Section = this._receivableTransactionLine2.Section; }
                        if (this._receivableTransactionLine2.Employee != null) { this.Employee = this._receivableTransactionLine2.Employee; }
                        if (this._receivableTransactionLine2.SalesInvoice != null) { this.SalesInvoice = this._receivableTransactionLine2.SalesInvoice; }
                        if (this._receivableTransactionLine2.Amount > 0) { this.Amount = this._receivableTransactionLine2.Amount; }
                    }
                }
            }
        }

        //AuditTrail
        [CollectionOperationSet(AllowAdd = false, AllowRemove = true)]
        public XPCollection<AuditDataItemPersistent> ChangeHistory
        {
            get
            {
                if (changeHistory == null)
                {
                    changeHistory = AuditedObjectWeakReference.GetAuditTrail(Session, this);
                }
                return changeHistory;
            }
        }

        #endregion Field
    }
}