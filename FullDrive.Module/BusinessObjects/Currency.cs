﻿using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using FullDrive.Module.CustomProcess;

namespace FullDrive.Module.BusinessObjects
{
    [DefaultClassOptions]
    [DefaultProperty("ShortName")]
    [NavigationItem("Rate")]
    [RuleCombinationOfPropertiesIsUnique("CurrencyRuleUnique", DefaultContexts.Save, "Code")]
    //[ImageName("BO_Contact")]
    //[DefaultProperty("DisplayMemberNameForLookupEditorsOfThisType")]
    //[DefaultListViewOptions(MasterDetailMode.ListViewOnly, false, NewItemRowPosition.None)]
    //[Persistent("DatabaseTableName")]
    // Specify more UI options using a declarative approach (https://documentation.devexpress.com/#eXpressAppFramework/CustomDocument112701).
    public class Currency : BaseObject
    { // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).
        private string _code;
        private string _name;
        private string _shortName;
        private double _currentRate;
        private Country _country;
        private bool _active;
        private bool _default;
        private GlobalFunction _globFunc;
        //AuditTrail
        private XPCollection<AuditDataItemPersistent> changeHistory;

        public Currency(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if (!IsLoading)
            {
                _globFunc = new GlobalFunction();
                this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.Currency);
            }
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).
        }

        protected override void OnSaving()
        {
            base.OnSaving();
            if (this.Default == true)
            {
                CheckDefault();
            }
        }

        [RuleRequiredField(DefaultContexts.Save)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue("Code", ref _code, value); }
        }

        [RuleRequiredField(DefaultContexts.Save)]
        public string Name
        {
            get { return _name; }
            set { SetPropertyValue("Name", ref _name, value); }
        }

        public string ShortName
        {
            get { return _shortName; }
            set { SetPropertyValue("ShortName", ref _shortName, value); }
        }

        [DataSourceCriteria("Active = true")]
        public Country Country
        {
            get { return _country; }
            set { SetPropertyValue("Country", ref _country, value); }
        }

        //public double CurrentRate
        //{
        //    //get
        //    //{
        //    //    double result = 1;
        //    //    if (CurrencyRates.Count > 0 )
        //    //    {
        //    //        foreach (CurrencyRate currRate in CurrencyRates)
        //    //        {
        //    //            if (currRate.Active)
        //    //            {
        //    //                result = currRate.Rate1;
        //    //            }
        //    //        }
        //    //    }
        //    //    return result;
        //    //}
        //    get { return _currentRate; }
        //    set { SetPropertyValue("CurrencyRate", ref _currentRate, value); }
        //}

        public bool Active
        {
            get { return _active; }
            set { SetPropertyValue("Active", ref _active, value); }
        }

        public bool Default
        {
            get { return _default; }
            set { SetPropertyValue("Default", ref _default, value); }
        }

        [Association("Currency-CurrencyRates")]
        public XPCollection<CurrencyRate> CurrencyRates
        {
            get { return GetCollection<CurrencyRate>("CurrencyRates"); }
        }

        //AuditTrail
        [CollectionOperationSet(AllowAdd = false, AllowRemove = true)]
        public XPCollection<AuditDataItemPersistent> ChangeHistory
        {
            get
            {
                if (changeHistory == null)
                {
                    changeHistory = AuditedObjectWeakReference.GetAuditTrail(Session, this);
                }
                return changeHistory;
            }
        }

        //private string _PersistentProperty;
        //[XafDisplayName("My display name"), ToolTip("My hint message")]
        //[ModelDefault("EditMask", "(000)-00"), Index(0), VisibleInListView(false)]
        //[Persistent("DatabaseColumnName"), RuleRequiredField(DefaultContexts.Save)]
        //public string PersistentProperty {
        //    get { return _PersistentProperty; }
        //    set { SetPropertyValue("PersistentProperty", ref _PersistentProperty, value); }
        //}

        //[Action(Caption = "My UI Action", ConfirmationMessage = "Are you sure?", ImageName = "Attention", AutoCommit = true)]
        //public void ActionMethod() {
        //    // Trigger a custom business logic for the current record in the UI (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112619.aspx).
        //    this.PersistentProperty = "Paid";
        //}

        //=============================================== Code In Here ===============================================

        #region CheckDefault
        private void CheckDefault()
        {
            try
            {
                XPCollection<Currency> _currs = new XPCollection<Currency>(Session, new BinaryOperator("This", this, BinaryOperatorType.NotEqual));
                if (_currs == null)
                {
                    return;
                }
                else
                {
                    foreach (Currency _curr in _currs)
                    {
                        _curr.Default = false;
                        _curr.Save();
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Business Object = Currency", ex.ToString());
            }
        }
        #endregion CheckDefault

        #region GetCurrencyRateByDate
        public double GetCurrencyRateByDate(DateTime CurrDate)
        {
            CurrencyRate Rec = Session.FindObject<CurrencyRate>(new BinaryOperator("StartDate", CurrDate, BinaryOperatorType.LessOrEqual));
            if (Rec != null)
                return Rec.Rate1;
            else
                return 1;
        }
        #endregion GetCurrencyRateByDate

    }
}