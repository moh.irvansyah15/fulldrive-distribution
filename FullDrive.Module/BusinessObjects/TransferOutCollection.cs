﻿using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;

using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.ConditionalAppearance;
using DevExpress.ExpressApp.Editors;

namespace FullDrive.Module.BusinessObjects
{
    [DefaultClassOptions]
    [DefaultProperty("Code")]
    [NavigationItem("Inventory")]
    [RuleCombinationOfPropertiesIsUnique("TransferOutCollectionRuleUnique", DefaultContexts.Save, "Code")]
    //[ImageName("BO_Contact")]
    //[DefaultProperty("DisplayMemberNameForLookupEditorsOfThisType")]
    //[DefaultListViewOptions(MasterDetailMode.ListViewOnly, false, NewItemRowPosition.None)]
    //[Persistent("DatabaseTableName")]
    // Specify more UI options using a declarative approach (https://documentation.devexpress.com/#eXpressAppFramework/CustomDocument112701).
    public class TransferOutCollection : BaseObject
    { // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).
        private bool _activationPosting;
        private string _code;
        #region InitialOrganization
        private Company _company;
        private Workplace _workplace;
        private Division _division;
        private Department _department;
        private Section _section;
        private Employee _employee;
        #endregion InitialOrganization
        #region From
        private LocationType _locationTypeFrom;
        private StockType _stockTypeFrom;
        private XPCollection<Workplace> _availableWorkplaceFrom;
        private Workplace _workplaceFrom;
        private XPCollection<Location> _availableLocationFrom;
        private Location _locationFrom;
        private BeginningInventory _begInvFrom;
        #endregion From
        #region To
        private bool _openPIC;
        private LocationType _locationTypeTo;
        private StockType _stockTypeTo;
        private XPCollection<Workplace> _availableWorkplaceTo;
        private Workplace _workplaceTo;
        private DateTime _pickingDate;
        private XPCollection<Picking> _availablePicking;
        private Picking _picking;
        private Location _locationTo;
        private XPCollection<Vehicle> _availableVehicle;
        private Vehicle _vehicle;
        private BeginningInventory _begInvTo;
        #endregion To
        private Employee _pic;
        private XPCollection<TransferOut> _availableTransferOut;
        private TransferOut _transferOut;
        private TransferIn _transferIn;
        private Status _status;
        private DateTime _statusDate;
        private string _localUserAccess;
        private string _userAccess;
        private GlobalFunction _globFunc;

        public TransferOutCollection(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if (!IsLoading)
            {
                if(Session.IsNewObject(this))
                {
                    _globFunc = new GlobalFunction();
                    DateTime now = DateTime.Now;
                    #region UserAccess
                    _localUserAccess = SecuritySystem.CurrentUserName;
                    UserAccess _locUserAccess = Session.FindObject<UserAccess>(new BinaryOperator("UserName", _localUserAccess));
                    if (_locUserAccess != null)
                    {
                        if (_locUserAccess.Employee != null)
                        {
                            if (_locUserAccess.Employee.Company != null && _locUserAccess.Employee.Workplace != null)
                            {
                                this.Code = _globFunc.GetNumberingUnlockOptimisticRecordBasedOrganization(this.Session.DataLayer, ObjectList.TransferOutCollection, _locUserAccess.Employee.Company, _locUserAccess.Employee.Workplace);
                            }
                            else
                            {
                                this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.TransferOutCollection);
                            }
                        }
                    }
                    #endregion UserAccess
                    this.OpenPIC = false;
                    this.PickingDate = now;
                    this.Status = Status.Open;
                    this.StatusDate = now;
                }                
            }
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).
        }

        #region Field

        [Browsable(false)]
        public bool ActivationPosting
        {
            get { return _activationPosting; }
            set { SetPropertyValue("ActivationPosting", ref _activationPosting, value); }
        }

        [RuleRequiredField(DefaultContexts.Save)]
        [Appearance("PurchaseRequisitionCollectionCodeClose", Enabled = false)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue("Code", ref _code, value); }
        }

        #region Organization

        [ImmediatePostData()]
        [RuleRequiredField(DefaultContexts.Save)]
        [DataSourceCriteria("Active = true")]
        [Appearance("TransferOutCollectionCompanyClose", Enabled = false)]
        public Company Company
        {
            get { return _company; }
            set { SetPropertyValue("Company", ref _company, value); }
        }

        [ImmediatePostData()]
        [RuleRequiredField(DefaultContexts.Save)]
        [DataSourceCriteria("Active = true")]
        [Appearance("TransferOutCollectionWorkplaceClose", Enabled = false)]
        public Workplace Workplace
        {
            get { return _workplace; }
            set { SetPropertyValue("Workplace", ref _workplace, value); }
        }

        [Browsable(false)]
        public Division Division
        {
            get { return _division; }
            set { SetPropertyValue("Division", ref _division, value); }
        }

        [Browsable(false)]
        public Department Department
        {
            get { return _department; }
            set { SetPropertyValue("Department", ref _department, value); }
        }

        [Browsable(false)]
        public Section Section
        {
            get { return _section; }
            set { SetPropertyValue("Section", ref _section, value); }
        }

        [Browsable(false)]
        public Employee Employee
        {
            get { return _employee; }
            set { SetPropertyValue("Employee", ref _employee, value); }
        }

        #endregion Organization

        #region From
        [VisibleInListView(false)]
        [ImmediatePostData()]
        [Appearance("TransferOutCollectionLocationTypeFromClose", Enabled = false)]
        public LocationType LocationTypeFrom
        {
            get { return _locationTypeFrom; }
            set { SetPropertyValue("LocationTypeFrom", ref _locationTypeFrom, value); }
        }

        [VisibleInListView(false)]
        [ImmediatePostData()]
        [Appearance("TransferOutCollectionStockTypeFromClose", Enabled = false)]
        public StockType StockTypeFrom
        {
            get { return _stockTypeFrom; }
            set { SetPropertyValue("StockTypeFrom", ref _stockTypeFrom, value); }
        }

        [Browsable(false)]
        public XPCollection<Workplace> AvailableWorkplaceFrom
        {
            get
            {
                if (!IsLoading)
                {
                    string _beginString = null;
                    string _endString = null;
                    string _fullString = null;

                    if (this.Company != null)
                    {
                        _userAccess = SecuritySystem.CurrentUserName;
                        UserAccess _locUserAccess = Session.FindObject<UserAccess>(new BinaryOperator("UserName", _userAccess));
                        if (_locUserAccess != null)
                        {
                            List<string> _stringOSD = new List<string>();

                            XPCollection<OrganizationSetupDetail> _locOrganizationSetupDetails = new XPCollection<OrganizationSetupDetail>
                                                                                (Session, new GroupOperator(GroupOperatorType.And,
                                                                                new BinaryOperator("Company", this.Company),
                                                                                new BinaryOperator("UserAccess", _locUserAccess),
                                                                                new BinaryOperator("AllWorkplace", false),
                                                                                new BinaryOperator("OpenWorkplace", true),
                                                                                new BinaryOperator("Active", true)));

                            if (_locOrganizationSetupDetails != null && _locOrganizationSetupDetails.Count() > 0)
                            {
                                foreach (OrganizationSetupDetail _locOrganizationSetupDetail in _locOrganizationSetupDetails)
                                {
                                    if (_locOrganizationSetupDetail.Workplace != null)
                                    {
                                        if (_locOrganizationSetupDetail.Workplace.Code != null)
                                        {
                                            _stringOSD.Add(_locOrganizationSetupDetail.Workplace.Code);
                                        }
                                    }
                                }
                            }

                            IEnumerable<string> _stringArrayOSDDistinct = _stringOSD.Distinct();
                            string[] _stringArrayOSDList = _stringArrayOSDDistinct.ToArray();
                            if (_stringArrayOSDList.Length == 1)
                            {
                                for (int i = 0; i < _stringArrayOSDList.Length; i++)
                                {
                                    Workplace _locW = Session.FindObject<Workplace>(new BinaryOperator("Code", _stringArrayOSDList[i]));
                                    if (_locW != null)
                                    {
                                        if (i == 0)
                                        {
                                            _beginString = "[Code]=='" + _locW.Code + "'";
                                        }
                                    }
                                }
                            }
                            else if (_stringArrayOSDList.Length > 1)
                            {
                                for (int i = 0; i < _stringArrayOSDList.Length; i++)
                                {
                                    Workplace _locW = Session.FindObject<Workplace>(new BinaryOperator("Code", _stringArrayOSDList[i]));
                                    if (_locW != null)
                                    {
                                        if (i == 0)
                                        {
                                            _beginString = "[Code]=='" + _locW.Code + "'";
                                        }
                                        else
                                        {
                                            _endString = _endString + " OR [Code]=='" + _locW.Code + "'";
                                        }
                                    }
                                }
                            }
                            _fullString = _beginString + _endString;

                            if (_fullString != null)
                            {
                                _availableWorkplaceFrom = new XPCollection<Workplace>(Session, CriteriaOperator.Parse(_fullString));
                            }
                            else
                            {
                                OrganizationSetupDetail _locWhsSetupDetail = Session.FindObject<OrganizationSetupDetail>
                                                                        (new GroupOperator(GroupOperatorType.And,
                                                                        new BinaryOperator("Company", this.Company),
                                                                        new BinaryOperator("UserAccess", _locUserAccess),
                                                                        new BinaryOperator("AllWorkplace", true),
                                                                        new BinaryOperator("Active", true)));
                                if (_locWhsSetupDetail != null)
                                {
                                    _availableWorkplaceFrom = new XPCollection<Workplace>(Session,
                                                        new GroupOperator(GroupOperatorType.And,
                                                        new BinaryOperator("Company", this.Company),
                                                        new BinaryOperator("Active", true)));
                                }
                            }
                        }
                    }
                }

                return _availableWorkplaceFrom;

            }
        }

        [ImmediatePostData()]
        [Appearance("TransferInWorkplaceFromClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [DataSourceProperty("AvailableWorkplaceFrom", DataSourcePropertyIsNullMode.SelectAll)]
        public Workplace WorkplaceFrom
        {
            get { return _workplaceFrom; }
            set { SetPropertyValue("WorkplaceFrom", ref _workplaceFrom, value); }
        }

        [ImmediatePostData()]
        [Appearance("TransferInLocationFromClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [DataSourceCriteria("Company = '@This.Company' And Workplace = '@this.WorkplaceFrom' And Active = true")]
        public Location LocationFrom
        {
            get { return _locationFrom; }
            set
            {
                SetPropertyValue("LocationFrom", ref _locationFrom, value);
                if (!IsLoading)
                {
                    if (this._locationFrom != null)
                    {
                        SetBegInvFrom(_locationFrom);
                    }
                    else
                    {
                        BegInvFrom = null;
                    }
                }
            }
        }

        [Browsable(false)]
        [ImmediatePostData()]
        [Appearance("TransferInBegInvFromClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public BeginningInventory BegInvFrom
        {
            get { return _begInvFrom; }
            set { SetPropertyValue("BegInvFrom", ref _begInvFrom, value); }
        }

        #endregion From

        #region To

        

        [Browsable(false)]
        [ImmediatePostData()]
        public bool OpenPIC
        {
            get { return _openPIC; }
            set { SetPropertyValue("OpenPIC", ref _openPIC, value); }
        }

        [VisibleInListView(false)]
        [ImmediatePostData()]
        [Appearance("TransferOutCollectionLocationTypeClose", Enabled = false)]
        public LocationType LocationTypeTo
        {
            get { return _locationTypeTo; }
            set { SetPropertyValue("LocationTypeTo", ref _locationTypeTo, value); }
        }

        [VisibleInListView(false)]
        [ImmediatePostData()]
        [Appearance("TransferOutCollectionStockTypeClose", Enabled = false)]
        public StockType StockTypeTo
        {
            get { return _stockTypeTo; }
            set { SetPropertyValue("StockTypeTo", ref _stockTypeTo, value); }
        }

        [Browsable(false)]
        public XPCollection<Workplace> AvailableWorkplaceTo
        {
            get
            {
                if (!IsLoading)
                {
                    string _beginString = null;
                    string _endString = null;
                    string _fullString = null;

                    if (this.Company != null)
                    {
                        _localUserAccess = SecuritySystem.CurrentUserName;
                        UserAccess _locUserAccess = Session.FindObject<UserAccess>(new BinaryOperator("UserName", _localUserAccess));
                        if (_locUserAccess != null)
                        {
                            List<string> _stringOSD = new List<string>();

                            XPCollection<OrganizationSetupDetail> _locOrganizationSetupDetails = new XPCollection<OrganizationSetupDetail>
                                                                                (Session, new GroupOperator(GroupOperatorType.And,
                                                                                new BinaryOperator("Company", this.Company),
                                                                                new BinaryOperator("UserAccess", _locUserAccess),
                                                                                new BinaryOperator("AllWorkplace", false),
                                                                                new BinaryOperator("OpenWorkplace", true),
                                                                                new BinaryOperator("Active", true)));

                            if (_locOrganizationSetupDetails != null && _locOrganizationSetupDetails.Count() > 0)
                            {
                                foreach (OrganizationSetupDetail _locOrganizationSetupDetail in _locOrganizationSetupDetails)
                                {
                                    if (_locOrganizationSetupDetail.Workplace != null)
                                    {
                                        if (_locOrganizationSetupDetail.Workplace.Code != null)
                                        {
                                            _stringOSD.Add(_locOrganizationSetupDetail.Workplace.Code);
                                        }
                                    }
                                }
                            }

                            IEnumerable<string> _stringArrayOSDDistinct = _stringOSD.Distinct();
                            string[] _stringArrayOSDList = _stringArrayOSDDistinct.ToArray();
                            if (_stringArrayOSDList.Length == 1)
                            {
                                for (int i = 0; i < _stringArrayOSDList.Length; i++)
                                {
                                    Workplace _locW = Session.FindObject<Workplace>(new BinaryOperator("Code", _stringArrayOSDList[i]));
                                    if (_locW != null)
                                    {
                                        if (i == 0)
                                        {
                                            _beginString = "[Code]=='" + _locW.Code + "'";
                                        }
                                    }
                                }
                            }
                            else if (_stringArrayOSDList.Length > 1)
                            {
                                for (int i = 0; i < _stringArrayOSDList.Length; i++)
                                {
                                    Workplace _locW = Session.FindObject<Workplace>(new BinaryOperator("Code", _stringArrayOSDList[i]));
                                    if (_locW != null)
                                    {
                                        if (i == 0)
                                        {
                                            _beginString = "[Code]=='" + _locW.Code + "'";
                                        }
                                        else
                                        {
                                            _endString = _endString + " OR [Code]=='" + _locW.Code + "'";
                                        }
                                    }
                                }
                            }
                            _fullString = _beginString + _endString;

                            if (_fullString != null)
                            {
                                _availableWorkplaceTo = new XPCollection<Workplace>(Session, CriteriaOperator.Parse(_fullString));
                            }
                            else
                            {
                                OrganizationSetupDetail _locWhsSetupDetail = Session.FindObject<OrganizationSetupDetail>
                                                                        (new GroupOperator(GroupOperatorType.And,
                                                                        new BinaryOperator("Company", this.Company),
                                                                        new BinaryOperator("UserAccess", _locUserAccess),
                                                                        new BinaryOperator("AllWorkplace", true),
                                                                        new BinaryOperator("Active", true)));
                                if (_locWhsSetupDetail != null)
                                {
                                    _availableWorkplaceTo = new XPCollection<Workplace>(Session,
                                                        new GroupOperator(GroupOperatorType.And,
                                                        new BinaryOperator("Company", this.Company),
                                                        new BinaryOperator("Active", true)));
                                }
                            }
                        }
                    }
                }

                return _availableWorkplaceTo;

            }
        }

        [VisibleInListView(false)]
        [ImmediatePostData()]
        [DataSourceProperty("AvailableWorkplaceTo", DataSourcePropertyIsNullMode.SelectAll)]
        [Appearance("TransferOutCollectionWorkplaceToClose", Enabled = false)]
        public Workplace WorkplaceTo
        {
            get { return _workplaceTo; }
            set { SetPropertyValue("WorkplaceTo", ref _workplaceTo, value); }
        }

        [ImmediatePostData()]
        [ModelDefault("DisplayFormat", "{0:dd/MM/yyyy}")]
        [ModelDefault("EditMask", "dd/MM/yyyy")]
        [Appearance("TransferOutCollectionPickingDateClose1", Criteria = "OpenPIC = false", Enabled = false)]
        [Appearance("TransferOutCollectionPickingDateClose2", Criteria = "OpenPIC = true", Enabled = true)]
        [Appearance("TransferOutCollectionPickingDateClose3", Criteria = "ActivationPosting = true", Enabled = false)]
        public DateTime PickingDate
        {
            get { return _pickingDate; }
            set { SetPropertyValue("PickingDate", ref _pickingDate, value); }

        }

        [Browsable(false)]
        public XPCollection<Picking> AvailablePicking
        {
            get
            {

                if (!IsLoading)
                {
                    if (this.Company != null && this.WorkplaceFrom != null)
                    {
                        XPQuery<TransferOutMonitoring> _transferOutMonitoringsQuery = new XPQuery<TransferOutMonitoring>(Session);

                        var _transferOutMonitorings = from sim in _transferOutMonitoringsQuery
                                                      where ((sim.Status == Status.Open || sim.Status == Status.Posted)
                                                      && sim.Company == this.Company
                                                      && sim.Workplace == this.WorkplaceFrom
                                                      && sim.TransferOut != null
                                                      && sim.TransferIn == null
                                                      && sim.Picking != null
                                                      )
                                                      group sim by sim.Picking into g
                                                      select new { Picking = g.Key };

                        if (_transferOutMonitorings != null && _transferOutMonitorings.Count() > 0)
                        {
                            string _beginString = null;
                            string _endString = null;
                            string _fullString = null;

                            List<string> _stringTOM = new List<string>();

                            foreach (var _pickingMonitoring in _transferOutMonitorings)
                            {
                                if (_pickingMonitoring != null)
                                {
                                    if (_pickingMonitoring.Picking != null)
                                    {
                                        if (_pickingMonitoring.Picking.Code != null && _pickingMonitoring.Picking.PickingDate.ToShortDateString() == this.PickingDate.ToShortDateString())
                                        {
                                            _stringTOM.Add(_pickingMonitoring.Picking.Code);
                                        }
                                    }
                                }
                            }

                            IEnumerable<string> _stringArrayTOMDistinct = _stringTOM.Distinct();
                            string[] _stringArrayTOMList = _stringArrayTOMDistinct.ToArray();
                            if (_stringArrayTOMList.Length == 1)
                            {
                                for (int i = 0; i < _stringArrayTOMList.Length; i++)
                                {
                                    if (i == 0)
                                    {
                                        _beginString = "[Code]=='" + _stringArrayTOMList[i] + "'";
                                    }
                                }
                            }
                            else if (_stringArrayTOMList.Length > 1)
                            {
                                for (int i = 0; i < _stringArrayTOMList.Length; i++)
                                {
                                    if (i == 0)
                                    {
                                        _beginString = "[Code]=='" + _stringArrayTOMList[i] + "'";
                                    }
                                    else
                                    {
                                        _endString = _endString + " OR [Code]=='" + _stringArrayTOMList[i] + "'";
                                    }
                                }
                            }
                            _fullString = _beginString + _endString;

                            if (_fullString != null && _fullString != "")
                            {
                                _availablePicking = new XPCollection<Picking>(Session, CriteriaOperator.Parse(_fullString));
                            }
                        }
                    }
                }

                return _availablePicking;
            }
        }

        [ImmediatePostData()]
        [DataSourceProperty("AvailablePicking")]
        [Appearance("TransferOutCollectionPickingClose1", Criteria = "OpenPIC = false", Enabled = false)]
        [Appearance("TransferOutCollectionPickingClose2", Criteria = "OpenPIC = true", Enabled = true)]
        [Appearance("TransferOutCollectionPickingClose3", Criteria = "ActivationPosting = true", Enabled = false)]
        public Picking Picking
        {
            get { return _picking; }
            set
            {
                SetPropertyValue("Picking", ref _picking, value);
                if (!IsLoading)
                {
                    if (this._picking != null)
                    {
                        if (this._picking.Vehicle != null)
                        {
                            this.Vehicle = this._picking.Vehicle;
                        }
                        else
                        {
                            this.Vehicle = null;
                        }
                    }
                    else
                    {
                        this.Vehicle = null;
                    }
                }
            }
        }

        [Browsable(false)]
        public XPCollection<Vehicle> AvailableVehicle
        {
            get
            {
                if (!IsLoading)
                {
                    if (this.Company != null && this.WorkplaceTo != null)
                    {
                        _availableVehicle = new XPCollection<Vehicle>(Session, new GroupOperator(GroupOperatorType.And,
                                                                            new BinaryOperator("Company", this.Company),
                                                                            new BinaryOperator("Workplace", this.WorkplaceTo),
                                                                            new BinaryOperator("Active", true)));
                    }
                    else
                    {
                        _availableVehicle = new XPCollection<Vehicle>(Session, new GroupOperator(GroupOperatorType.And,
                                                                            new BinaryOperator("Active", true)));
                    }

                }

                return _availableVehicle;

            }
        }

        [ImmediatePostData()]
        [DataSourceProperty("AvailableVehicle")]
        [Appearance("TransferOutCollectionVehicleClose1", Criteria = "OpenPIC = false", Enabled = false)]
        [Appearance("TransferOutCollectionVehicleClose1", Criteria = "OpenPIC = true", Enabled = true)]
        [Appearance("TransferOutCollectionVehicleClose3", Criteria = "ActivationPosting = true", Enabled = false)]
        public Vehicle Vehicle
        {
            get { return _vehicle; }
            set {
                SetPropertyValue("Vehicle", ref _vehicle, value);
                if (!IsLoading)
                {
                    if (this._vehicle != null)
                    {
                        SetLocationTo(this._vehicle);
                    }
                    else
                    {
                        this.LocationTo = null;
                    }
                }
            }
        }

        [ImmediatePostData()]
        [DataSourceProperty("AvailableLocation")]
        [Appearance("TransferOutCollectionLocationToClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Location LocationTo
        {
            get { return _locationTo; }
            set
            {
                SetPropertyValue("LocationTo", ref _locationTo, value);
                if (!IsLoading)
                {
                    if (this._locationTo != null)
                    {
                        SetBegInvTo(_locationTo);
                    }
                    else
                    {
                        BegInvTo = null;
                    }
                }
            }

        }

        [Browsable(false)]
        [ImmediatePostData()]
        [Appearance("TransferOutCollectionBegInvToClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public BeginningInventory BegInvTo
        {
            get { return _begInvTo; }
            set { SetPropertyValue("BegInvTo", ref _begInvTo, value); }
        }

        #endregion To

        [VisibleInListView(false)]
        [ImmediatePostData()]
        [Appearance("TransferOutCollectionPICClose2", Criteria = "ActivationPosting = true", Enabled = false)]
        public Employee PIC
        {
            get { return _pic; }
            set { SetPropertyValue("InternalPIC", ref _pic, value); }
        }

        [Browsable(false)]
        public XPCollection<TransferOut> AvailableTransferOut
        {
            get
            {
                if (!IsLoading)
                {
                    if (this.Company != null && this.Workplace != null && this.TransferIn != null)
                    {
                        #region Picking
                        if(this.Picking != null && this.Vehicle != null)
                        {
                            string _fullString = null;
                            
                            XPQuery<TransferOutMonitoring> _transferOutMonitoringsQuery = new XPQuery<TransferOutMonitoring>(Session);

                            var _transferOutMonitorings = from tom in _transferOutMonitoringsQuery
                                                          where ((tom.Status == Status.Open || tom.Status == Status.Posted)
                                                          && tom.Company == this.Company
                                                          && tom.LocationTypeTo == this.LocationTypeFrom
                                                          && tom.StockTypeTo == this.StockTypeFrom
                                                          && tom.WorkplaceTo == this.WorkplaceFrom
                                                          && tom.LocationTo == this.LocationFrom
                                                          && tom.BegInvTo == this.BegInvFrom
                                                          && tom.Picking == this.Picking
                                                          && tom.TransferIn == null
                                                          && tom.Vehicle == this.Vehicle)
                                                          group tom by tom.TransferOut into g
                                                          select new { TransferOut = g.Key };

                            if (_transferOutMonitorings != null && _transferOutMonitorings.Count() > 0)
                            {
                                string _beginString = null;
                                string _endString = null;
                                

                                List<string> _stringTOM = new List<string>();

                                foreach (var _transferOutMonitoring in _transferOutMonitorings)
                                {
                                    if (_transferOutMonitoring != null)
                                    {
                                        _stringTOM.Add(_transferOutMonitoring.TransferOut.Code);
                                    }
                                }

                                IEnumerable<string> _stringArrayTOMDistinct = _stringTOM.Distinct();
                                string[] _stringArrayTOMList = _stringArrayTOMDistinct.ToArray();
                                if (_stringArrayTOMList.Length == 1)
                                {
                                    for (int i = 0; i < _stringArrayTOMList.Length; i++)
                                    {
                                        if (i == 0)
                                        {
                                            _beginString = "[Code]=='" + _stringArrayTOMList[i] + "'";
                                        }
                                    }
                                }
                                else if (_stringArrayTOMList.Length > 1)
                                {
                                    for (int i = 0; i < _stringArrayTOMList.Length; i++)
                                    {
                                        if (i == 0)
                                        {
                                            _beginString = "[Code]=='" + _stringArrayTOMList[i] + "'";
                                        }
                                        else
                                        {
                                            _endString = _endString + " OR [Code]=='" + _stringArrayTOMList[i] + "'";
                                        }
                                    }
                                }
                                _fullString = _beginString + _endString;

                                if (_fullString != null && _fullString != "")
                                {
                                    _availableTransferOut = new XPCollection<TransferOut>(Session, CriteriaOperator.Parse(_fullString));
                                }
                            }
                        }
                        #endregion Picking
                        #region NonPicking
                        else
                        {
                            string _beginString = null;
                            string _endString = null;
                            string _fullString = null;

                            XPQuery<TransferOutMonitoring> _transfOutMonsQuery = new XPQuery<TransferOutMonitoring>(Session);

                            var _transfOutMons = from tom in _transfOutMonsQuery
                                                 where ((tom.Status == Status.Open || tom.Status == Status.Posted)
                                                    && tom.Company == this.Company
                                                    && tom.LocationTypeTo == this.LocationTypeFrom
                                                    && tom.StockTypeTo == this.StockTypeFrom
                                                    && tom.WorkplaceTo == this.WorkplaceFrom
                                                    && tom.LocationTo == this.LocationFrom
                                                    && tom.BegInvTo == this.BegInvFrom
                                                    && tom.TransferIn == null)
                                                 group tom by tom.TransferOut into g
                                                 select new { TransferOut = g.Key };

                            if (_transfOutMons != null && _transfOutMons.Count() > 0)
                            {
                                List<string> _stringTOM = new List<string>();

                                foreach (var _transfOutMon in _transfOutMons)
                                {
                                    if (_transfOutMon != null)
                                    {
                                        _stringTOM.Add(_transfOutMon.TransferOut.Code);
                                    }
                                }

                                IEnumerable<string> _stringArrayTOMDistinct = _stringTOM.Distinct();
                                string[] _stringArrayTOMList = _stringArrayTOMDistinct.ToArray();
                                if (_stringArrayTOMList.Length == 1)
                                {
                                    for (int i = 0; i < _stringArrayTOMList.Length; i++)
                                    {
                                        if (i == 0)
                                        {
                                            _beginString = "[Code]=='" + _stringArrayTOMList[i] + "'";
                                        }
                                    }
                                }
                                else if (_stringArrayTOMList.Length > 1)
                                {
                                    for (int i = 0; i < _stringArrayTOMList.Length; i++)
                                    {
                                        if (i == 0)
                                        {
                                            _beginString = "[Code]=='" + _stringArrayTOMList[i] + "'";
                                        }
                                        else
                                        {
                                            _endString = _endString + " OR [Code]=='" + _stringArrayTOMList[i] + "'";
                                        }
                                    }
                                }
                                _fullString = _beginString + _endString;

                                if (_fullString != null && _fullString != "")
                                {
                                    _availableTransferOut = new XPCollection<TransferOut>(Session, CriteriaOperator.Parse(_fullString));
                                }
                            }
                        }
                        #endregion NonPicking
                    }
                }
                return _availableTransferOut;
            }
        }

        [ImmediatePostData()]
        [DataSourceProperty("AvailableTransferOut", DataSourcePropertyIsNullMode.SelectNothing)]
        public TransferOut TransferOut
        {
            get { return _transferOut; }
            set { SetPropertyValue("TransferOut", ref _transferOut, value); }
        }

        [ImmediatePostData()]
        [Appearance("TransferOutCollectionTransferInClose", Enabled = false)]
        [Association("TransferIn-TransferOutCollections")]
        public TransferIn TransferIn
        {
            get { return _transferIn; }
            set
            {
                SetPropertyValue("TransferIn", ref _transferIn, value);
                if (!IsLoading)
                {
                    if (this._transferIn != null)
                    {
                        if (Session.IsNewObject(this))
                        {
                            if (this._transferIn.Company != null) { this.Company = this._transferIn.Company; }
                            if (this._transferIn.Workplace != null) { this.Workplace = this._transferIn.Workplace; }
                            if (this._transferIn.Division != null) { this.Division = this._transferIn.Division; }
                            if (this._transferIn.Department != null) { this.Department = this._transferIn.Department; }
                            if (this._transferIn.Section != null) { this.Section = this._transferIn.Section; }
                            if (this._transferIn.Employee != null) { this.Employee = this._transferIn.Employee; }
                            #region From
                            if (this._transferIn.LocationTypeFrom != LocationType.None) { this.LocationTypeFrom = this._transferIn.LocationTypeFrom; }
                            if (this._transferIn.StockTypeFrom != StockType.None) { this.StockTypeFrom = this._transferIn.StockTypeFrom; }
                            if (this._transferIn.WorkplaceFrom != null) { this.WorkplaceFrom = this._transferIn.WorkplaceFrom; }
                            if (this._transferIn.LocationFrom != null) { this.LocationFrom = this._transferIn.LocationFrom; }
                            if (this._transferIn.BegInvFrom != null) { this.BegInvFrom = this._transferIn.BegInvFrom; }
                            #endregion From
                            #region To
                            this.OpenPIC = this._transferIn.OpenPIC;
                            if (this._transferIn.LocationTypeTo != LocationType.None) { this.LocationTypeTo = this._transferIn.LocationTypeTo; }
                            if (this._transferIn.StockTypeTo != StockType.None) { this.StockTypeTo = this._transferIn.StockTypeTo; }
                            if (this._transferIn.WorkplaceTo != null) { this.WorkplaceTo = this._transferIn.WorkplaceTo; }
                            this.PickingDate = this._transferIn.PickingDate;
                            if (this._transferIn.Picking != null) { this.Picking = this._transferIn.Picking; }
                            if (this._transferIn.Vehicle != null) { this.Vehicle = this._transferIn.Vehicle; }
                            if (this._transferIn.LocationTo != null) { this.LocationTo = this._transferIn.LocationTo; }
                            if (this._transferIn.BegInvTo != null) { this.BegInvTo = this._transferIn.BegInvTo; }
                            #endregion To
                        }
                    }
                    else
                    {
                        this.Company = null;
                        this.Workplace = null;
                        this.Division = null;
                        this.Department = null;
                        this.Section = null;
                        this.Employee = null;
                        this.LocationTypeFrom = LocationType.None;
                        this.StockTypeFrom = StockType.None;
                        this.WorkplaceFrom = null;
                        this.LocationFrom = null;
                        this.BegInvFrom = null;
                        this.LocationTypeTo = LocationType.None;
                        this.StockTypeTo = StockType.None;
                        this.WorkplaceTo = null;
                        this.Picking = null;
                        this.Vehicle = null;
                        this.LocationTo = null;
                        this.BegInvTo = null;
                        this.OpenPIC = false;
                    }
                }
            }
        }

        [Appearance("TransferOutCollectionStatusClose", Enabled = false)]
        public Status Status
        {
            get { return _status; }
            set { SetPropertyValue("Status", ref _status, value); }
        }

        [ModelDefault("DisplayFormat", "{0:dd/MM/yyyy}")]
        [ModelDefault("EditMask", "dd/MM/yyyy")]
        [Appearance("TransferOutCollectionStatusDateClose", Enabled = false)]
        public DateTime StatusDate
        {
            get { return _statusDate; }
            set { SetPropertyValue("StatusDate", ref _statusDate, value); }
        }

        #endregion Field

        //=================================== Code Only ===================================

        private void SetBegInvFrom(Location _locLocation)
        {
            try
            {
                if (this.Company != null && this.WorkplaceFrom != null)
                {
                    BeginningInventory _locBegInvFrom = Session.FindObject<BeginningInventory>
                                                        (new GroupOperator(GroupOperatorType.And,
                                                        new BinaryOperator("Company", this.Company),
                                                        new BinaryOperator("Workplace", this.WorkplaceFrom),
                                                        new BinaryOperator("Location", _locLocation),
                                                        new BinaryOperator("LocationType", this.LocationTypeFrom),
                                                        new BinaryOperator("StockType", this.StockTypeFrom),
                                                        new BinaryOperator("Active", true)));
                    if (_locBegInvFrom != null)
                    {
                        this.BegInvFrom = _locBegInvFrom;
                    }
                    else
                    {
                        this.BegInvFrom = null;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Business Object = TransferOutCollection ", ex.ToString());
            }
        }

        private void SetBegInvTo(Location _locLocation)
        {
            try
            {
                DateTime now = DateTime.Now;
                if (this.Company != null && this.WorkplaceTo != null)
                {
                    BeginningInventory _locBegInv = Session.FindObject<BeginningInventory>
                                                        (new GroupOperator(GroupOperatorType.And,
                                                        new BinaryOperator("Company", this.Company),
                                                        new BinaryOperator("Workplace", this.WorkplaceTo),
                                                        new BinaryOperator("Location", _locLocation),
                                                        new BinaryOperator("LocationType", this.LocationTypeTo),
                                                        new BinaryOperator("StockType", this.StockTypeTo),
                                                        new BinaryOperator("Active", true)));
                    if (_locBegInv != null)
                    {
                        this.BegInvTo = _locBegInv;
                    }
                    else
                    {
                        this.BegInvTo = null;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Business Object = TransferOutCollection ", ex.ToString());
            }
        }

        private void SetLocationTo(Vehicle _locVehicle)
        {
            try
            {
                DateTime now = DateTime.Now;
                if (this.Company != null && this.WorkplaceTo != null)
                {
                    Location _locLocation = Session.FindObject<Location>
                                                        (new GroupOperator(GroupOperatorType.And,
                                                        new BinaryOperator("Company", this.Company),
                                                        new BinaryOperator("Workplace", this.WorkplaceTo),
                                                        new BinaryOperator("OpenVehicle", true),
                                                        new BinaryOperator("Vehicle", _locVehicle),
                                                        new BinaryOperator("Active", true)));
                    if (_locLocation != null)
                    {
                        if (_locLocation != null)
                        {
                            this.LocationTo = _locLocation;
                        }
                        else
                        {
                            this.LocationTo = null;
                        }
                    }
                    else
                    {
                        this.LocationTo = null;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Business Object = TransferOutCollection ", ex.ToString());
            }
        }

    }
}