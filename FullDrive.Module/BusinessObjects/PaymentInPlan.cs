﻿using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;

using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.ConditionalAppearance;

namespace FullDrive.Module.BusinessObjects
{
    [DefaultClassOptions]
    [DefaultProperty("Code")]
    [NavigationItem("Finance & Accounting")]
    [RuleCombinationOfPropertiesIsUnique("PaymentInPlanRuleUnique", DefaultContexts.Save, "Code")]
    //[ImageName("BO_Contact")]
    //[DefaultProperty("DisplayMemberNameForLookupEditorsOfThisType")]
    //[DefaultListViewOptions(MasterDetailMode.ListViewOnly, false, NewItemRowPosition.None)]
    //[Persistent("DatabaseTableName")]
    // Specify more UI options using a declarative approach (https://documentation.devexpress.com/#eXpressAppFramework/CustomDocument112701).
    public class PaymentInPlan : BaseObject
    { // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).

        private string _code;
        private Company _company;
        private Workplace _workplace;
        private Employee _pic;
        private PaymentMethod _paymentMethod;
        private Currency _currency;
        private double _plan;
        private double _received;
        private double _outstanding;
        private Status _status;
        private DateTime _statusDate;
        private SalesInvoice _salesInvoice;
        private SalesOrder _salesOrder;
        //private ReceivableTransaction _receivableTransaction;
        private GlobalFunction _globFunc;
        //AuditTrail
        private XPCollection<AuditDataItemPersistent> changeHistory;

        public PaymentInPlan(Session session)
            : base(session)
        {
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if (!IsLoading)
            {
                if(Session.IsNewObject(this))
                {
                    DateTime now = DateTime.Now;
                    _globFunc = new GlobalFunction();
                    this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.PaymentInPlan);
                    this.Status = Status.Open;
                    this.StatusDate = now;
                }                
            }
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).
        }

        #region Field

        [Appearance("PaymentInPlanCodeEnabled", Enabled = false)]
        [RuleRequiredField(DefaultContexts.Save)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue("Code", ref _code, value); }
        }

        [Appearance("PaymentInPlanCompanyEnabled", Enabled = false)]
        public Company Company
        {
            get { return _company; }
            set { SetPropertyValue("Company", ref _company, value); }
        }

        [Appearance("PaymentInPlanWorkplaceEnabled", Enabled = false)]
        public Workplace Workplace
        {
            get { return _workplace; }
            set { SetPropertyValue("Workplace", ref _workplace, value); }
        }

        [Browsable(false)]
        [ImmediatePostData()]
        [Appearance("PaymentInPlanSalesmanClose", Enabled = false)]
        public Employee PIC
        {
            get { return _pic; }
            set { SetPropertyValue("PIC", ref _pic, value); }
        }

        [Appearance("PaymentInPlanPaymentMethodEnabled", Enabled = false)]
        public PaymentMethod PaymentMethod
        {
            get { return _paymentMethod; }
            set { SetPropertyValue("PaymentMethod", ref _paymentMethod, value); }
        }

        [Appearance("PaymentInPlanCurrencyClose", Enabled = false)]
        public Currency Currency
        {
            get { return _currency; }
            set { SetPropertyValue("Currency", ref _currency, value); }
        }

        [Appearance("PaymentInPlanPlanEnabled", Enabled = false)]
        public double Plan
        {
            get { return _plan; }
            set { SetPropertyValue("Plan", ref _plan, value); }
        }

        [Appearance("PaymentInPlanReceivedEnabled", Enabled = false)]
        public double Received
        {
            get { return _received; }
            set { SetPropertyValue("Received", ref _received, value); }
        }

        [Appearance("PaymentInPlanOutStandingEnabled", Enabled = false)]
        public double Outstanding
        {
            get { return _outstanding; }
            set { SetPropertyValue("Outstanding", ref _outstanding, value); }
        }

        [Appearance("PaymentInPlanStatusEnabled", Enabled = false)]
        public Status Status
        {
            get { return _status; }
            set { SetPropertyValue("Status", ref _status, value); }
        }

        [ModelDefault("DisplayFormat", "{0:dd/MM/yyyy}")]
        [ModelDefault("EditMask", "dd/MM/yyyy")]
        [Appearance("PaymentInPlanStatusDateEnabled", Enabled = false)]
        public DateTime StatusDate
        {
            get { return _statusDate; }
            set { SetPropertyValue("StatusDate", ref _statusDate, value); }
        }

        [Appearance("PaymentInPlanSalesOrderEnabled", Enabled = false)]
        public SalesOrder SalesOrder
        {
            get { return _salesOrder; }
            set { SetPropertyValue("SalesOrder", ref _salesOrder, value); }
        }

        [Appearance("PaymentInPlanSalesInvoiceEnabled", Enabled = false)]
        public SalesInvoice SalesInvoice
        {
            get { return _salesInvoice; }
            set { SetPropertyValue("SalesInvoice", ref _salesInvoice, value); }
        }

        //[Appearance("PaymentInPlanReceivableTransactionEnabled", Enabled = false)]
        //[Association("ReceivableTransaction-PaymentInPlans")]
        //public ReceivableTransaction ReceivableTransaction
        //{
        //    get { return _receivableTransaction; }
        //    set { SetPropertyValue("ReceivableTransaction", ref _receivableTransaction, value); }
        //}

        //AuditTrail
        [CollectionOperationSet(AllowAdd = false, AllowRemove = true)]
        public XPCollection<AuditDataItemPersistent> ChangeHistory
        {
            get
            {
                if (changeHistory == null)
                {
                    changeHistory = AuditedObjectWeakReference.GetAuditTrail(Session, this);
                }
                return changeHistory;
            }
        }

        #endregion Field

    }
}