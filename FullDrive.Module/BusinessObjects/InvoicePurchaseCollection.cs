﻿using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;

using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.ConditionalAppearance;

namespace FullDrive.Module.BusinessObjects
{
    [DefaultClassOptions]
    [DefaultProperty("Code")]
    [NavigationItem("Finance & Accounting")]
    [RuleCombinationOfPropertiesIsUnique("InvoicePurchaseCollectionRuleUnique", DefaultContexts.Save, "Code")]
    //[ImageName("BO_Contact")]
    //[DefaultProperty("DisplayMemberNameForLookupEditorsOfThisType")]
    //[DefaultListViewOptions(MasterDetailMode.ListViewOnly, false, NewItemRowPosition.None)]
    //[Persistent("DatabaseTableName")]
    // Specify more UI options using a declarative approach (https://documentation.devexpress.com/#eXpressAppFramework/CustomDocument112701).
    public class InvoicePurchaseCollection : BaseObject
    { // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).
        private bool _activationPosting;
        private string _code;
        private XPCollection<InventoryTransferIn> _availableInventoryTransferIn;
        private InventoryTransferIn _inventoryTransferIn;
        private PurchaseInvoice _purchaseInvoice;
        #region InitialOrganization
        private Company _company;
        private Workplace _workplace;
        private Division _division;
        private Department _department;
        private Section _section;
        private Employee _employee;
        #endregion InitialOrganization
        private Status _status;
        private DateTime _statusDate;        
        private int _postedCount;
        private string _localUserAccess;
        private GlobalFunction _globFunc;
        //AuditTrail
        private XPCollection<AuditDataItemPersistent> changeHistory;

        public InvoicePurchaseCollection(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if (!IsLoading)
            {
                if(Session.IsNewObject(this))
                {
                    _globFunc = new GlobalFunction();
                    DateTime now = DateTime.Now;
                    #region UserAccess
                    _localUserAccess = SecuritySystem.CurrentUserName;
                    UserAccess _locUserAccess = Session.FindObject<UserAccess>(new BinaryOperator("UserName", _localUserAccess));
                    if (_locUserAccess != null)
                    {
                        if (_locUserAccess.Employee != null)
                        {
                            if (_locUserAccess.Employee.Company != null && _locUserAccess.Employee.Workplace != null)
                            {
                                this.Code = _globFunc.GetNumberingUnlockOptimisticRecordBasedOrganization(this.Session.DataLayer, ObjectList.InvoicePurchaseCollection, _locUserAccess.Employee.Company, _locUserAccess.Employee.Workplace);
                            }
                            else
                            {
                                this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.InvoicePurchaseCollection);
                            }
                        }
                    }
                    #endregion UserAccess
                    this.Status = Status.Open;
                    this.StatusDate = now;
                }                
            }
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).
        }

        #region Field

        [Browsable(false)]
        public bool ActivationPosting
        {
            get { return _activationPosting; }
            set { SetPropertyValue("ActivationPosting", ref _activationPosting, value); }
        }

        [RuleRequiredField(DefaultContexts.Save)]
        [Appearance("InvoicePurchaseCollectionCodeClose", Enabled = false)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue("Code", ref _code, value); }
        }

        public XPCollection<InventoryTransferIn> AvailableInventoryTransferIn
        {
            get
            {
                if(!IsLoading)
                {
                    string _beginString = null;
                    string _endString = null;
                    string _fullString = null;

                    if (this.PurchaseInvoice != null)
                    {
                        if (this.PurchaseInvoice.BuyFromVendor != null)
                        {
                            XPQuery<InventoryTransferInMonitoring> _inventoryTransferInMonitoringsQuery = new XPQuery<InventoryTransferInMonitoring>(Session);

                            var _inventoryTransferInMonitorings = from itim in _inventoryTransferInMonitoringsQuery
                                                                  where ((itim.Status == Status.Open || itim.Status == Status.Posted)
                                                                  && itim.InventoryTransferIn.BusinessPartner == this.PurchaseInvoice.BuyFromVendor
                                                                  && itim.PurchaseInvoice == null)
                                                                  group itim by itim.InventoryTransferIn into g
                                                                  select new { InventoryTransferIn = g.Key };

                            if (_inventoryTransferInMonitorings != null && _inventoryTransferInMonitorings.Count() > 0)
                            {
                                List<string> _stringITIM = new List<string>();

                                foreach (var _inventoryTransferInMonitoring in _inventoryTransferInMonitorings)
                                {
                                    if (_inventoryTransferInMonitoring.InventoryTransferIn != null)
                                    {
                                        if (_inventoryTransferInMonitoring.InventoryTransferIn.Code != null)
                                        {
                                            _stringITIM.Add(_inventoryTransferInMonitoring.InventoryTransferIn.Code);
                                        }
                                    }
                                }

                                IEnumerable<string> _stringArrayITIDistinct = _stringITIM.Distinct();
                                string[] _stringArrayITIMList = _stringArrayITIDistinct.ToArray();
                                if (_stringArrayITIMList.Length == 1)
                                {
                                    for (int i = 0; i < _stringArrayITIMList.Length; i++)
                                    {
                                        if (i == 0)
                                        {
                                            _beginString = "[Code]=='" + _stringArrayITIMList[i] + "'";
                                        }
                                    }
                                }
                                else if (_stringArrayITIMList.Length > 1)
                                {
                                    for (int i = 0; i < _stringArrayITIMList.Length; i++)
                                    {
                                        if (i == 0)
                                        {
                                            _beginString = "[Code]=='" + _stringArrayITIMList[i] + "'";
                                        }
                                        else
                                        {
                                            _endString = _endString + " OR [Code]=='" + _stringArrayITIMList[i] + "'";
                                        }
                                    }
                                }
                                _fullString = _beginString + _endString;

                                if (_fullString != null)
                                {
                                    _availableInventoryTransferIn = new XPCollection<InventoryTransferIn>(Session, CriteriaOperator.Parse(_fullString));
                                }
                            }
                        }
                    }
                }                
                return _availableInventoryTransferIn;
            }
        }

        [ImmediatePostData()]
        [DataSourceProperty("AvailableInventoryTransferIn", DataSourcePropertyIsNullMode.SelectNothing)]
        public InventoryTransferIn InventoryTransferIn
        {
            get { return _inventoryTransferIn; }
            set { SetPropertyValue("InventoryTransferIn", ref _inventoryTransferIn, value); }
        }

        [VisibleInListView(false)]
        [ImmediatePostData()]
        [Appearance("InvoicePurchaseCollectionPurchaseInvoiceClose", Enabled = false)]
        [Association("PurchaseInvoice-InvoicePurchaseCollections")]
        public PurchaseInvoice PurchaseInvoice
        {
            get { return _purchaseInvoice; }
            set
            {
                SetPropertyValue("PurchaseInvoice", ref _purchaseInvoice, value);
                if(!IsLoading)
                {
                    if(_purchaseInvoice != null)
                    {
                        if(_purchaseInvoice.Company != null)
                        {
                            this.Company = this._purchaseInvoice.Company;
                        }
                        if (_purchaseInvoice.Workplace != null)
                        {
                            this.Workplace = this._purchaseInvoice.Workplace;
                        }
                        if (_purchaseInvoice.Division != null)
                        {
                            this.Division = this._purchaseInvoice.Division;
                        }
                        if (_purchaseInvoice.Department != null)
                        {
                            this.Department = this._purchaseInvoice.Department;
                        }
                        if (_purchaseInvoice.Section != null)
                        {
                            this.Section = this._purchaseInvoice.Section;
                        }
                        if (_purchaseInvoice.Employee != null)
                        {
                            this.Employee = this._purchaseInvoice.Employee;
                        }
                    }
                }
            }
        }

        #region Organization

        [Appearance("InvoicePurchaseCollectionCompanyClose", Enabled = false)]
        public Company Company
        {
            get { return _company; }
            set { SetPropertyValue("Company", ref _company, value); }
        }

        [Appearance("InvoicePurchaseCollectionWorkplaceClose", Enabled = false)]
        public Workplace Workplace
        {
            get { return _workplace; }
            set { SetPropertyValue("Workplace", ref _workplace, value); }
        }

        [Browsable(false)]
        public Division Division
        {
            get { return _division; }
            set { SetPropertyValue("Division", ref _division, value); }
        }

        [Browsable(false)]
        public Department Department
        {
            get { return _department; }
            set { SetPropertyValue("Department", ref _department, value); }
        }

        [Browsable(false)]
        public Section Section
        {
            get { return _section; }
            set { SetPropertyValue("Section", ref _section, value); }
        }

        [Browsable(false)]
        public Employee Employee
        {
            get { return _employee; }
            set { SetPropertyValue("Employee", ref _employee, value); }
        }

        #endregion Organization

        [VisibleInListView(false)]
        [Appearance("InvoicePurchaseCollectionStatusClose", Enabled = false)]
        public Status Status
        {
            get { return _status; }
            set { SetPropertyValue("Status", ref _status, value); }
        }

        [VisibleInListView(false)]
        [ModelDefault("DisplayFormat", "{0:dd/MM/yyyy}")]
        [ModelDefault("EditMask", "dd/MM/yyyy")]
        [Appearance("InvoicePurchaseCollectionStatusDateClose", Enabled = false)]
        public DateTime StatusDate
        {
            get { return _statusDate; }
            set { SetPropertyValue("StatusDate", ref _statusDate, value); }
        }

        [VisibleInListView(false)]
        [Appearance("InvoicePurchaseCollectionPostedCountEnabled", Enabled = false)]
        public int PostedCount
        {
            get { return _postedCount; }
            set { SetPropertyValue("PostedCount", ref _postedCount, value); }
        }

        //AuditTrail
        [CollectionOperationSet(AllowAdd = false, AllowRemove = true)]
        public XPCollection<AuditDataItemPersistent> ChangeHistory
        {
            get
            {
                if (changeHistory == null)
                {
                    changeHistory = AuditedObjectWeakReference.GetAuditTrail(Session, this);
                }
                return changeHistory;
            }
        }

        #endregion Field

    }
}