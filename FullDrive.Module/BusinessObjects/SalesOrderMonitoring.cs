﻿using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;

using DevExpress.ExpressApp.ConditionalAppearance;
using DevExpress.ExpressApp.SystemModule;
using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.Editors;

namespace FullDrive.Module.BusinessObjects
{
    [DefaultClassOptions]
    [DefaultProperty("Code")]
    [NavigationItem("Sales")]
    [RuleCombinationOfPropertiesIsUnique("SalesOrderMonitoringRuleUnique", DefaultContexts.Save, "Code")]
    //[ImageName("BO_Contact")]
    //[DefaultProperty("DisplayMemberNameForLookupEditorsOfThisType")]
    //[DefaultListViewOptions(MasterDetailMode.ListViewOnly, false, NewItemRowPosition.None)]
    //[Persistent("DatabaseTableName")]
    // Specify more UI options using a declarative approach (https://documentation.devexpress.com/#eXpressAppFramework/CustomDocument112701).
    public class SalesOrderMonitoring : BaseObject
    { // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).

        private bool _activationPosting;
        private bool _freeItemChecked;
        private bool _discountChecked;
        private string _code;
        private bool _select;
        private SalesOrder _salesOrder;
        private SalesOrderLine _salesOrderLine;
        private SalesOrderFreeItem _salesOrderFreeItem;
        private SalesInvoice _salesInvoice;
        private SalesInvoiceLine _salesInvoiceLine;
        private SalesInvoiceFreeItem _salesInvoiceFreeItem;
        private FreeItemRule _freeItemRule;
        #region InitialOrganization
        private Company _company;
        private Workplace _workplace;
        private Division _division;
        private Department _department;
        private Section _section;
        private Employee _employee;
        private Division _div;
        private Department _dept;
        private Section _sect;
        private XPCollection<Employee> _availablePIC;
        private Employee _pic;
        private XPCollection<Location> _availableLocation;
        private Location _location;
        private BeginningInventory _begInv;
        #endregion InitialOrganization 
        private OrderType _salesType;
        private XPCollection<Item> _availableItem;
        private Item _item;
        private BeginningInventoryLine _begInvLine;
        private string _description;
        #region InisialisasiDefaultQty
        private double _dQty;
        private UnitOfMeasure _dUom;
        private double _qty;
        private XPCollection<UnitOfMeasure> _availableUnitOfMeasure;
        private UnitOfMeasure _uom;
        private double _tQty;
        private StockType _stockType;
        private StockGroup _stockGroup;
        #endregion InisialisasiDefaultQty
        #region InisialisasiAmount
        private Currency _currency;
        private PriceGroup _priceGroup;
        private PriceLine _priceLine;
        private double _uAmount;
        private double _tUAmount;
        private DiscountRule _discountRule;
        private AccountingPeriodicLine _accountingPeriodicLine;
        private double _discAmount;
        private Tax _tax;
        private double _txValue;
        private double _txAmount;
        private double _tAmount;
        #endregion InisialisasiAmount
        private DateTime _etd;
        private DateTime _eta;
        private Status _status;
        private DateTime _statusDate;
        private int _postedCount;
        private string _localUserAccess;
        private GlobalFunction _globFunc;
        //AuditTrail
        private XPCollection<AuditDataItemPersistent> changeHistory;


        public SalesOrderMonitoring(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).
            if (!IsLoading)
            {
                _globFunc = new GlobalFunction();
                DateTime now = DateTime.Now;
                #region UserAccess
                _localUserAccess = SecuritySystem.CurrentUserName;
                UserAccess _locUserAccess = Session.FindObject<UserAccess>(new BinaryOperator("UserName", _localUserAccess));
                if (_locUserAccess != null)
                {
                    if (_locUserAccess.Employee != null)
                    {
                        if (_locUserAccess.Employee.Company != null && _locUserAccess.Employee.Workplace != null)
                        {
                            this.Code = _globFunc.GetNumberingUnlockOptimisticRecordBasedOrganization(this.Session.DataLayer, ObjectList.SalesOrderMonitoring, _locUserAccess.Employee.Company, _locUserAccess.Employee.Workplace);
                        }
                        else
                        {
                            this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.SalesOrderMonitoring);
                        }
                    }
                }
                #endregion UserAccess
                this.SalesType = CustomProcess.OrderType.Item;
                this.Status = CustomProcess.Status.Open;
                this.StatusDate = now;
                this.Select = true;
                this.DiscountChecked = false;
                this.StockType = StockType.Good;
            }
        }

        #region Field

        [Browsable(false)]
        [ImmediatePostData()]
        public bool ActivationPosting
        {
            get { return _activationPosting; }
            set { SetPropertyValue("ActivationPosting", ref _activationPosting, value); }
        }

        [Browsable(false)]
        public bool FreeItemChecked
        {
            get { return _freeItemChecked; }
            set { SetPropertyValue("FreeItemChecked", ref _freeItemChecked, value); }
        }

        [Browsable(false)]
        public bool DiscountChecked
        {
            get { return _discountChecked; }
            set { SetPropertyValue("DiscountChecked", ref _discountChecked, value); }
        }

        [RuleRequiredField(DefaultContexts.Save)]
        [Appearance("SalesOrderMonitoringCodeEnabled", Enabled = false)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue("Code", ref _code, value); }
        }

        //[Appearance("SalesOrderLineSelectClose", Criteria = "SelectEnabled = true", Enabled = false)]
        public bool Select
        {
            get { return _select; }
            set { SetPropertyValue("Select", ref _select, value); }
        }

        [Appearance("SalesOrderMonitoringSalesOrderEnabled", Enabled = false)]
        [RuleRequiredField(DefaultContexts.Save)]
        public SalesOrder SalesOrder
        {
            get { return _salesOrder; }
            set { SetPropertyValue("SalesOrder", ref _salesOrder, value); }
        }

        [Appearance("SalesOrderMonitoringSalesOrderLineEnabled", Enabled = false)]
        public SalesOrderLine SalesOrderLine
        {
            get { return _salesOrderLine; }
            set { SetPropertyValue("SalesOrderLine", ref _salesOrderLine, value); }
        }

        [Appearance("SalesOrderMonitoringSalesOrderFreeItemEnabled", Enabled = false)]
        public SalesOrderFreeItem SalesOrderFreeItem
        {
            get { return _salesOrderFreeItem; }
            set { SetPropertyValue("SalesOrderFreeItem", ref _salesOrderFreeItem, value); }
        }

        [Appearance("SalesOrderMonitoringSalesInvoiceEnabled", Enabled = false)]
        public SalesInvoice SalesInvoice
        {
            get { return _salesInvoice; }
            set { SetPropertyValue("SalesInvoice", ref _salesInvoice, value); }
        }

        [Appearance("SalesOrderMonitoringSalesInvoiceLineEnabled", Enabled = false)]
        public SalesInvoiceLine SalesInvoiceLine
        {
            get { return _salesInvoiceLine; }
            set { SetPropertyValue("SalesInvoiceLine", ref _salesInvoiceLine, value); }
        }

        [Appearance("SalesOrderMonitoringSalesInvoiceFreeItemEnabled", Enabled = false)]
        public SalesInvoiceFreeItem SalesInvoiceFreeItem
        {
            get { return _salesInvoiceFreeItem; }
            set { SetPropertyValue("SalesInvoiceFreeItem", ref _salesInvoiceFreeItem, value); }
        }

        [Appearance("SalesOrderMonitoringFreeItemRuleClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public FreeItemRule FreeItemRule
        {
            get { return _freeItemRule; }
            set { SetPropertyValue("FreeItemRule", ref _freeItemRule, value); }
        }

        #region Organization

        [ImmediatePostData()]
        [RuleRequiredField(DefaultContexts.Save)]
        [DataSourceCriteria("Active = true")]
        [Appearance("SalesOrderMonitoringCompanyClose", Enabled = false)]
        public Company Company
        {
            get { return _company; }
            set { SetPropertyValue("Company", ref _company, value); }
        }

        [ImmediatePostData()]
        [RuleRequiredField(DefaultContexts.Save)]
        [DataSourceCriteria("Active = true")]
        [Appearance("SalesOrderMonitoringWorkplaceClose", Enabled = false)]
        public Workplace Workplace
        {
            get { return _workplace; }
            set { SetPropertyValue("Workplace", ref _workplace, value); }
        }

        [Browsable(false)]
        public Division Division
        {
            get { return _division; }
            set { SetPropertyValue("Division", ref _division, value); }
        }

        [Browsable(false)]
        public Department Department
        {
            get { return _department; }
            set { SetPropertyValue("Department", ref _department, value); }
        }

        [Browsable(false)]
        public Section Section
        {
            get { return _section; }
            set { SetPropertyValue("Section", ref _section, value); }
        }

        [Browsable(false)]
        public Employee Employee
        {
            get { return _employee; }
            set { SetPropertyValue("Employee", ref _employee, value); }
        }

        [ImmediatePostData()]
        [DataSourceCriteria("Company = '@This.Company' And Active = true")]
        [Appearance("SalesOrderMonitoringDivClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Division Div
        {
            get { return _div; }
            set { SetPropertyValue("Div", ref _div, value); }
        }

        [ImmediatePostData()]
        [DataSourceCriteria("Company = '@This.Company' And Division = '@This.Div' And Active = true")]
        [Appearance("SalesOrderMonitoringDeptClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Department Dept
        {
            get { return _dept; }
            set { SetPropertyValue("Dept", ref _dept, value); }
        }

        [ImmediatePostData()]
        [DataSourceCriteria("Company = '@This.Company' And Department = '@This.Dept' And Active = true")]
        [Appearance("SalesOrderMonitoringSectClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Section Sect
        {
            get { return _sect; }
            set { SetPropertyValue("Sect", ref _sect, value); }
        }

        [Browsable(false)]
        public XPCollection<Employee> AvailablePIC
        {
            get
            {
                XPCollection<Employee> _locPICs = null;
                if (this.Company != null && this.Workplace != null)
                {
                    #region DivDeptSect
                    if (this.Div != null && this.Dept != null && this.Sect != null)
                    {
                        _locPICs = new XPCollection<Employee>
                                  (Session, new GroupOperator(GroupOperatorType.And,
                                    new BinaryOperator("Company", this.Company),
                                    new BinaryOperator("Workplace", this.Workplace),
                                    new BinaryOperator("Division", this.Div),
                                    new BinaryOperator("Department", this.Dept),
                                    new BinaryOperator("Section", this.Sect),
                                    new BinaryOperator("Active", true),
                                    new GroupOperator(GroupOperatorType.Or,
                                    new BinaryOperator("SalesRole", SalesRole.TakingOrder),
                                    new BinaryOperator("SalesRole", SalesRole.All))));
                    }
                    else if (this.Div != null && this.Dept != null && this.Sect == null)
                    {
                        _locPICs = new XPCollection<Employee>
                                  (Session, new GroupOperator(GroupOperatorType.And,
                                    new BinaryOperator("Company", this.Company),
                                    new BinaryOperator("Workplace", this.Workplace),
                                    new BinaryOperator("Division", this.Div),
                                    new BinaryOperator("Department", this.Dept),
                                    new BinaryOperator("Active", true),
                                    new GroupOperator(GroupOperatorType.Or,
                                    new BinaryOperator("SalesRole", SalesRole.TakingOrder),
                                    new BinaryOperator("SalesRole", SalesRole.All))));

                    }
                    else if (this.Div != null && this.Dept == null && this.Sect == null)
                    {
                        _locPICs = new XPCollection<Employee>
                                  (Session, new GroupOperator(GroupOperatorType.And,
                                    new BinaryOperator("Company", this.Company),
                                    new BinaryOperator("Workplace", this.Workplace),
                                    new BinaryOperator("Division", this.Div),
                                    new BinaryOperator("Active", true),
                                    new GroupOperator(GroupOperatorType.Or,
                                    new BinaryOperator("SalesRole", SalesRole.TakingOrder),
                                    new BinaryOperator("SalesRole", SalesRole.All))));

                    }
                    else if (this.Div == null && this.Dept == null && this.Sect == null)
                    {
                        _locPICs = new XPCollection<Employee>
                                  (Session, new GroupOperator(GroupOperatorType.And,
                                    new BinaryOperator("Company", this.Company),
                                    new BinaryOperator("Workplace", this.Workplace),
                                    new BinaryOperator("Active", true),
                                    new GroupOperator(GroupOperatorType.Or,
                                    new BinaryOperator("SalesRole", SalesRole.TakingOrder),
                                    new BinaryOperator("SalesRole", SalesRole.All))));

                    }
                    #endregion DivDeptSect
                    if (_locPICs != null && _locPICs.Count() > 0)
                    {
                        _availablePIC = _locPICs;
                    }
                }
                else
                {
                    _availablePIC = new XPCollection<Employee>(Session, new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("Active", true),
                                                            new GroupOperator(GroupOperatorType.Or,
                                                            new BinaryOperator("SalesRole", SalesRole.TakingOrder),
                                                            new BinaryOperator("SalesRole", SalesRole.All))));
                }

                return _availablePIC;

            }
        }

        [Browsable(false)]
        [ImmediatePostData()]
        [DataSourceProperty("AvailablePIC")]
        [Appearance("SalesOrderMonitoringPICClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Employee PIC
        {
            get { return _pic; }
            set
            {
                SetPropertyValue("PIC", ref _pic, value);
                if (!IsLoading)
                {
                    if (this._pic != null)
                    {
                        SetLocation(_pic);
                    }
                    else
                    {
                        this.Location = null;
                    }
                }
            }
        }

        [Browsable(false)]
        public XPCollection<Location> AvailableLocation
        {
            get
            {
                string _beginString = null;
                string _endString = null;
                string _fullString = null;

                if (this.Company != null && this.Workplace != null)
                {
                    #region PIC
                    if(this.PIC != null)
                    {
                        List<string> _stringSLS = new List<string>();

                        XPCollection<SalesmanLocationSetup> _locSalesmanLocationSetups = new XPCollection<SalesmanLocationSetup>
                                                                                  (Session, new GroupOperator(GroupOperatorType.And,
                                                                                   new BinaryOperator("Company", this.Company),
                                                                                   new BinaryOperator("Workplace", this.Workplace),
                                                                                   new BinaryOperator("Salesman", this.PIC),
                                                                                   new BinaryOperator("Active", true)));

                        if (_locSalesmanLocationSetups != null && _locSalesmanLocationSetups.Count() > 0)
                        {
                            foreach (SalesmanLocationSetup _locSalesmanLocationSetup in _locSalesmanLocationSetups)
                            {
                                if (_locSalesmanLocationSetup.Location != null)
                                {
                                    if (_locSalesmanLocationSetup.Location.Code != null)
                                    {
                                        _stringSLS.Add(_locSalesmanLocationSetup.Location.Code);
                                    }
                                }
                            }
                        }

                        IEnumerable<string> _stringArraySLSDistinct = _stringSLS.Distinct();
                        string[] _stringArraySLSList = _stringArraySLSDistinct.ToArray();
                        if (_stringArraySLSList.Length == 1)
                        {
                            for (int i = 0; i < _stringArraySLSList.Length; i++)
                            {
                                Location _locLoc = Session.FindObject<Location>(new BinaryOperator("Code", _stringArraySLSList[i]));
                                if (_locLoc != null)
                                {
                                    if (i == 0)
                                    {
                                        _beginString = "[Code]=='" + _locLoc.Code + "'";
                                    }
                                }
                            }
                        }
                        else if (_stringArraySLSList.Length > 1)
                        {
                            for (int i = 0; i < _stringArraySLSList.Length; i++)
                            {
                                Location _locLoc = Session.FindObject<Location>(new BinaryOperator("Code", _stringArraySLSList[i]));
                                if (_locLoc != null)
                                {
                                    if (i == 0)
                                    {
                                        _beginString = "[Code]=='" + _locLoc.Code + "'";
                                    }
                                    else
                                    {
                                        _endString = _endString + " OR [Code]=='" + _locLoc.Code + "'";
                                    }
                                }
                            }
                        }
                        _fullString = _beginString + _endString;

                        if (_fullString != null)
                        {
                            _availableLocation = new XPCollection<Location>(Session, CriteriaOperator.Parse(_fullString));
                        }
                    }
                    #endregion PIC
                    #region NonPIC
                    else
                    {
                        _availableLocation = new XPCollection<Location>(Session, new GroupOperator(GroupOperatorType.And,
                                                                            new BinaryOperator("Company", this.Company),
                                                                            new BinaryOperator("Workplace", this.Workplace),
                                                                            new BinaryOperator("OpenVehicle", false),
                                                                            new BinaryOperator("Active", true)));
                    }
                    #endregion NonPIC

                }

                return _availableLocation;

            }
        }

        [VisibleInListView(false)]
        [ImmediatePostData()]
        [DataSourceProperty("AvailableLocation")]
        [Appearance("SalesOrderMonitoringLocationClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Location Location
        {
            get { return _location; }
            set
            {
                SetPropertyValue("Location", ref _location, value);
                if (!IsLoading)
                {
                    if (this._location != null)
                    {
                        SetBegInv(_location);
                    }
                    else
                    {
                        BegInv = null;
                    }
                }
            }

        }

        [Browsable(false)]
        [ImmediatePostData()]
        [Appearance("SalesOrderMonitoringBegInvClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public BeginningInventory BegInv
        {
            get { return _begInv; }
            set { SetPropertyValue("BegInv", ref _begInv, value); }
        }

        #endregion Organization

        [ImmediatePostData()]
        [Appearance("SalesOrderMonitoringSalesTypeClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public OrderType SalesType
        {
            get { return _salesType; }
            set { SetPropertyValue("SalesType", ref _salesType, value); }
        }

        [Browsable(false)]
        public XPCollection<Item> AvailableItem
        {
            get
            {
                string _beginString = null;
                string _endString = null;
                string _fullString = null;
                if (this.Company != null && this.Workplace != null)
                {
                    List<string> _stringITM = new List<string>();

                    XPQuery<BeginningInventoryLine> _beginningInventoryLinesQuery = new XPQuery<BeginningInventoryLine>(Session);

                    var _beginningInventoryLines = from bil in _beginningInventoryLinesQuery
                                                   where (bil.Company == this.Company
                                                   && bil.Workplace == this.Workplace
                                                   && bil.Item.ItemType == this.SalesType
                                                   && bil.QtyAvailable > 0
                                                   && bil.Active == true
                                                   )
                                                   group bil by bil.Item into g
                                                   select new { Item = g.Key };
                    if (_beginningInventoryLines != null && _beginningInventoryLines.Count() > 0)
                    {
                        foreach (var _beginningInventoryLine in _beginningInventoryLines)
                        {
                            _stringITM.Add(_beginningInventoryLine.Item.Code);
                        }
                    }

                    IEnumerable<string> _stringArrayITMDistinct = _stringITM.Distinct();
                    string[] _stringArrayITMList = _stringArrayITMDistinct.ToArray();
                    if (_stringArrayITMList.Length == 1)
                    {
                        for (int i = 0; i < _stringArrayITMList.Length; i++)
                        {
                            Item _locITM = Session.FindObject<Item>(new BinaryOperator("Code", _stringArrayITMList[i]));
                            if (_locITM != null)
                            {
                                if (i == 0)
                                {
                                    _beginString = "[Code]=='" + _locITM.Code + "'";
                                }
                            }
                        }
                    }
                    else if (_stringArrayITMList.Length > 1)
                    {
                        for (int i = 0; i < _stringArrayITMList.Length; i++)
                        {
                            Item _locITM = Session.FindObject<Item>(new BinaryOperator("Code", _stringArrayITMList[i]));
                            if (_locITM != null)
                            {
                                if (i == 0)
                                {
                                    _beginString = "[Code]=='" + _locITM.Code + "'";
                                }
                                else
                                {
                                    _endString = _endString + " OR [Code]=='" + _locITM.Code + "'";
                                }
                            }
                        }
                    }
                    _fullString = _beginString + _endString;

                    if (_fullString != null)
                    {
                        _availableItem = new XPCollection<Item>(Session, CriteriaOperator.Parse(_fullString));
                    }
                }
                else
                {
                    _availableItem = new XPCollection<Item>(Session, new GroupOperator(GroupOperatorType.And,
                                                    new BinaryOperator("Active", true)));
                }

                return _availableItem;

            }
        }

        [DataSourceCriteria("Active = true"), ImmediatePostData()]
        [DataSourceProperty("AvailableItem", DataSourcePropertyIsNullMode.SelectAll)]
        [Appearance("SalesOrderMonitoringItemClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Item Item
        {
            get { return _item; }
            set
            {
                SetPropertyValue("Item", ref _item, value);

                if (!IsLoading)
                {
                    if (this._item != null)
                    {
                        if (this._item.Description != null)
                        {
                            this.Description = this._item.Description;
                        }
                        if (this._item.BasedUOM != null)
                        {
                            this.DUOM = this._item.BasedUOM;
                        }
                        SetUOM();
                        SetPriceLine();
                        SetDiscountRule();
                        SetBegInvLine(_item);
                    }
                    else
                    {
                        this.Description = null;
                        this.DUOM = null;
                        this.BegInvLine = null;
                    }
                }
            }
        }

        [Browsable(false)]
        [ImmediatePostData()]
        [Appearance("SalesOrderMonitoringBegInvLineClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public BeginningInventoryLine BegInvLine
        {
            get { return _begInvLine; }
            set { SetPropertyValue("BegInvLine", ref _begInvLine, value); }
        }

        [Appearance("SalesOrderMonitoringDescriptionClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [Size(512)]
        public string Description
        {
            get { return _description; }
            set { SetPropertyValue("Description", ref _description, value); }
        }

        public string Stock
        {
            get
            {
                double _locTotAvailableQty = 0;
                string _locBasedUOM = null;

                if (this.BegInvLine != null)
                {
                    _locTotAvailableQty = (BegInvLine.QtyAvailable - BegInvLine.QtyOnHand);

                    if (this.Item.BasedUOM != null)
                    {
                        if (this.Item.BasedUOM != null)
                        {
                            if (this.Item.BasedUOM.Name != null)
                            {
                                _locBasedUOM = this.Item.BasedUOM.Name;
                            }
                        }
                    }
                    return String.Format("{0} ({1})", _locTotAvailableQty.ToString(), _locBasedUOM);
                }
                return null;
            }
        }

        #region DefaultQty

        [Appearance("SalesOrderMonitoringDQtyClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [ImmediatePostData()]
        public double DQty
        {
            get { return _dQty; }
            set
            {
                SetPropertyValue("DQty", ref _dQty, value);
                if (!IsLoading)
                {
                    SetTotalQty();
                    SetTotalUnitAmount();
                    SetTaxAndDiscountAmount();
                    SetTotalAmount();
                }
            }
        }

        [Appearance("SalesOrderMonitoringDUOMClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [ImmediatePostData()]
        [DataSourceCriteria("Active = true")]
        public UnitOfMeasure DUOM
        {
            get { return _dUom; }
            set
            {
                SetPropertyValue("DUOM", ref _dUom, value);
                if (!IsLoading)
                {
                    SetTotalQty();
                    SetTotalUnitAmount();
                    SetTaxAndDiscountAmount();
                    SetTotalAmount();
                }
            }
        }

        [Appearance("SalesOrderMonitoringQtyClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [ImmediatePostData()]
        public double Qty
        {
            get { return _qty; }
            set
            {
                SetPropertyValue("Qty", ref _qty, value);
                if (!IsLoading)
                {
                    SetTotalQty();
                    SetTotalUnitAmount();
                    SetTaxAndDiscountAmount();
                    SetTotalAmount();
                }
            }
        }

        [Browsable(false)]
        public XPCollection<UnitOfMeasure> AvailableUnitOfMeasure
        {
            get
            {
                string _beginString = null;
                string _endString = null;
                string _fullString = null;
                if (Item == null)
                {
                    _availableUnitOfMeasure = new XPCollection<UnitOfMeasure>(Session, new GroupOperator(GroupOperatorType.And,
                                                                            new BinaryOperator("Active", true)));
                }
                else
                {
                    List<string> _stringUOM = new List<string>();

                    XPCollection<ItemUnitOfMeasure> _locItemUnitOfMeasures = new XPCollection<ItemUnitOfMeasure>
                                                                              (Session, new GroupOperator(GroupOperatorType.And,
                                                                               new BinaryOperator("Item", this.Item),
                                                                               new BinaryOperator("Active", true)));

                    if (_locItemUnitOfMeasures != null && _locItemUnitOfMeasures.Count() > 0)
                    {
                        foreach (ItemUnitOfMeasure _locItemUnitOfMeasure in _locItemUnitOfMeasures)
                        {
                            _stringUOM.Add(_locItemUnitOfMeasure.UOM.Code);
                        }
                    }

                    IEnumerable<string> _stringArrayUOMDistinct = _stringUOM.Distinct();
                    string[] _stringArrayUOMList = _stringArrayUOMDistinct.ToArray();
                    if (_stringArrayUOMList.Length == 1)
                    {
                        for (int i = 0; i < _stringArrayUOMList.Length; i++)
                        {
                            UnitOfMeasure _locUOM = Session.FindObject<UnitOfMeasure>(new BinaryOperator("Code", _stringArrayUOMList[i]));
                            if (_locUOM != null)
                            {
                                if (i == 0)
                                {
                                    _beginString = "[Code]=='" + _locUOM.Code + "'";
                                }
                            }
                        }
                    }
                    else if (_stringArrayUOMList.Length > 1)
                    {
                        for (int i = 0; i < _stringArrayUOMList.Length; i++)
                        {
                            UnitOfMeasure _locUOM = Session.FindObject<UnitOfMeasure>(new BinaryOperator("Code", _stringArrayUOMList[i]));
                            if (_locUOM != null)
                            {
                                if (i == 0)
                                {
                                    _beginString = "[Code]=='" + _locUOM.Code + "'";
                                }
                                else
                                {
                                    _endString = _endString + " OR [Code]=='" + _locUOM.Code + "'";
                                }
                            }
                        }
                    }
                    _fullString = _beginString + _endString;

                    if (_fullString != null)
                    {
                        _availableUnitOfMeasure = new XPCollection<UnitOfMeasure>(Session, CriteriaOperator.Parse(_fullString));
                    }

                }
                return _availableUnitOfMeasure;

            }
        }

        [Appearance("SalesOrderMonitoringUOMClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [ImmediatePostData()]
        [DataSourceProperty("AvailableUnitOfMeasure", DataSourcePropertyIsNullMode.SelectAll)]
        [DataSourceCriteria("Active = true")]
        public UnitOfMeasure UOM
        {
            get { return _uom; }
            set
            {
                SetPropertyValue("UOM", ref _uom, value);
                if (!IsLoading)
                {
                    SetTotalQty();
                    SetTotalUnitAmount();
                    SetTaxAndDiscountAmount();
                    SetTotalAmount();
                }
            }
        }

        [Appearance("SalesOrderMonitoringTotalQtyEnabled", Enabled = false)]
        public double TQty
        {
            get { return _tQty; }
            set { SetPropertyValue("TotalQty", ref _tQty, value); }
        }

        [Appearance("SalesOrderMonitoringStockTypeClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public StockType StockType
        {
            get { return _stockType; }
            set { SetPropertyValue("StockType", ref _stockType, value); }
        }

        [Appearance("SalesOrderMonitoringStockGroupClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public StockGroup StockGroup
        {
            get { return _stockGroup; }
            set { SetPropertyValue("StockGroup", ref _stockGroup, value); }
        }

        #endregion DefaultQty

        #region Amount

        [Appearance("SalesOrderMonitoringCurrencyClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Currency Currency
        {
            get { return _currency; }
            set { SetPropertyValue("Currency", ref _currency, value); }
        }

        [ImmediatePostData()]
        [Appearance("SalesOrderMonitoringPriceGroupClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public PriceGroup PriceGroup
        {
            get { return _priceGroup; }
            set
            {
                SetPropertyValue("PriceGroup", ref _priceGroup, value);
                if (!IsLoading)
                {
                    if (this._priceGroup != null)
                    {
                        SetPriceLine();
                        SetTaxAndDiscountAmount();
                        SetDiscountRule();
                    }
                }
            }
        }

        [Browsable(false)]
        [DataSourceCriteria("Active = true"), ImmediatePostData()]
        [Appearance("SalesOrderMonitoringPriceLineClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public PriceLine PriceLine
        {
            get { return _priceLine; }
            set
            {
                SetPropertyValue("PriceLine", ref _priceLine, value);
                if (!IsLoading)
                {
                    if (this._priceLine != null)
                    {
                        this.UAmount = this._priceLine.Amount2a;
                    }
                }
            }
        }

        [Appearance("SalesOrderMonitoringUAmountClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [ImmediatePostData()]
        public double UAmount
        {
            get { return _uAmount; }
            set
            {
                SetPropertyValue("UAmount", ref _uAmount, value);
                if (!IsLoading)
                {
                    _globFunc = new GlobalFunction();
                    if (this._uAmount > 0)
                    {
                        if (_globFunc.GetRoundingList(Session, ObjectList.SalesOrderLine, FieldName.UAmount) == true)
                        {
                            this._uAmount = _globFunc.GetRoundUp(Session, this._uAmount, ObjectList.SalesOrderLine, FieldName.UAmount);
                        }
                    }
                    SetTotalUnitAmount();
                    SetTaxAndDiscountAmount();
                    SetTotalAmount();
                }
            }
        }

        [Appearance("SalesOrderMonitoringTUAmountEnabled", Enabled = false)]
        [ImmediatePostData()]
        public double TUAmount
        {
            get { return _tUAmount; }
            set { SetPropertyValue("TUAmount", ref _tUAmount, value); }
        }

        [Appearance("SalesOrderMonitoringTaxClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [ImmediatePostData]
        [DataSourceCriteria("Active = true")]
        public Tax Tax
        {
            get { return _tax; }
            set
            {
                SetPropertyValue("Tax", ref _tax, value);
                if (!IsLoading)
                {
                    if (this._tax != null)
                    {
                        this.TxValue = this._tax.Value;
                        SetTaxAndDiscountAmount();
                        SetTotalAmount();
                    }
                    else
                    {
                        SetNormalTax();
                    }
                }
            }
        }

        [Browsable(false)]
        [ImmediatePostData()]
        [Appearance("SalesOrderMonitoringTxValueClose", Enabled = false)]
        public double TxValue
        {
            get { return _txValue; }
            set {
                SetPropertyValue("TxValue", ref _txValue, value);
                if (!IsLoading)
                {
                    SetTaxAndDiscountAmount();
                    SetTotalAmount();
                }
            }
        }

        [Appearance("SalesOrderMonitoringTxAmountEnabled", Enabled = false)]
        [ImmediatePostData()]
        public double TxAmount
        {
            get { return _txAmount; }
            set
            {
                SetPropertyValue("TxAmount", ref _txAmount, value);
                if (!IsLoading)
                {
                    _globFunc = new GlobalFunction();
                    if (this._txAmount > 0)
                    {
                        if (_globFunc.GetRoundingList(Session, ObjectList.SalesOrderLine, FieldName.TxAmount) == true)
                        {
                            this._txAmount = _globFunc.GetRoundUp(Session, this._txAmount, ObjectList.SalesOrderLine, FieldName.TxAmount);
                        }
                    }

                    SetTotalAmount();
                }
            }
        }

        [ImmediatePostData()]
        [DataSourceCriteria("Company = '@This.Company' AND Workplace = '@This.Workplace' AND MpItem = '@This.Item' AND PriceGroup = '@This.PriceGroup' AND Active = true")]
        [Appearance("SalesOrderMonitoringDiscountRuleClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public DiscountRule DiscountRule
        {
            get { return _discountRule; }
            set { SetPropertyValue("DiscountRule", ref _discountRule, value); }
        }

        [Browsable(false)]
        public AccountingPeriodicLine AccountingPeriodicLine
        {
            get { return _accountingPeriodicLine; }
            set { SetPropertyValue("AccountingPeriodicLine", ref _accountingPeriodicLine, value); }
        }

        [Appearance("SalesOrderMonitoringDiscAmountEnabled", Enabled = false)]
        [ImmediatePostData()]
        public double DiscAmount
        {
            get { return _discAmount; }
            set
            {
                SetPropertyValue("DiscAmount", ref _discAmount, value);
                if (!IsLoading)
                {
                    _globFunc = new GlobalFunction();
                    if (this._discAmount > 0)
                    {
                        if (_globFunc.GetRoundingList(Session, ObjectList.SalesOrderLine, FieldName.DiscAmount) == true)
                        {
                            this._discAmount = _globFunc.GetRoundUp(Session, this._discAmount, ObjectList.SalesOrderLine, FieldName.DiscAmount);
                        }
                    }
                }
            }
        }

        [Appearance("SalesOrderMonitoringTPriceEnabled", Enabled = false)]
        [ImmediatePostData()]
        public double TAmount
        {
            get { return _tAmount; }
            set
            {
                SetPropertyValue("TAmount", ref _tAmount, value);
                if (!IsLoading)
                {
                    _globFunc = new GlobalFunction();
                    if (this._tAmount > 0)
                    {
                        if (_globFunc.GetRoundingList(Session, ObjectList.SalesOrderLine, FieldName.TAmount) == true)
                        {
                            this._tAmount = _globFunc.GetRoundUp(Session, this._tAmount, ObjectList.SalesOrderLine, FieldName.TAmount);
                        }
                    }
                }
            }
        }

        #endregion Amount

        [ModelDefault("DisplayFormat", "{0:dd/MM/yyyy}")]
        [ModelDefault("EditMask", "dd/MM/yyyy")]
        [Appearance("SalesOrderMonitoringETDClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public DateTime ETD
        {
            get { return _etd; }
            set { SetPropertyValue("ETD", ref _etd, value); }
        }

        [ModelDefault("DisplayFormat", "{0:dd/MM/yyyy}")]
        [ModelDefault("EditMask", "dd/MM/yyyy")]
        [Appearance("SalesOrderMonitoringETAClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public DateTime ETA
        {
            get { return _eta; }
            set { SetPropertyValue("ETA", ref _eta, value); }
        }

        //[Appearance("SalesOrderMonitoringStatusEnabled", Enabled = false)]
        public Status Status
        {
            get { return _status; }
            set { SetPropertyValue("Status", ref _status, value); }
        }

        [ModelDefault("DisplayFormat", "{0:dd/MM/yyyy}")]
        [ModelDefault("EditMask", "dd/MM/yyyy")]
        [Appearance("SalesOrderMonitoringStatusDateEnabled", Enabled = false)]
        public DateTime StatusDate
        {
            get { return _statusDate; }
            set { SetPropertyValue("StatusDate", ref _statusDate, value); }
        }

        //[Appearance("SalesOrderMonitoringPostedCountEnabled", Enabled = false)]
        public int PostedCount
        {
            get { return _postedCount; }
            set { SetPropertyValue("PostedCount", ref _postedCount, value); }
        }

        //AuditTrail
        [CollectionOperationSet(AllowAdd = false, AllowRemove = true)]
        public XPCollection<AuditDataItemPersistent> ChangeHistory
        {
            get
            {
                if (changeHistory == null)
                {
                    changeHistory = AuditedObjectWeakReference.GetAuditTrail(Session, this);
                }
                return changeHistory;
            }
        }

        #endregion Field

        //================================================== Code Only ===============================================

        #region CodeOnly

        #region Set

        private void SetTotalQty()
        {
            try
            {
                double _locInvLineTotal = 0;
                DateTime now = DateTime.Now;
                if (this.SalesOrder != null)
                {
                    if (this.Item != null && this.UOM != null && this.DUOM != null)
                    {
                        ItemUnitOfMeasure _locItemUOM = Session.FindObject<ItemUnitOfMeasure>
                                                        (new GroupOperator(GroupOperatorType.And,
                                                         new BinaryOperator("Item", this.Item),
                                                         new BinaryOperator("UOM", this.UOM),
                                                         new BinaryOperator("DefaultUOM", this.DUOM),
                                                         new BinaryOperator("Active", true)));
                        if (_locItemUOM != null)
                        {
                            if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                            {
                                _locInvLineTotal = this.Qty * _locItemUOM.DefaultConversion + this.DQty;
                            }
                            else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                            {
                                _locInvLineTotal = this.Qty / _locItemUOM.Conversion + this.DQty;
                            }
                            else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                            {
                                _locInvLineTotal = this.Qty + this.DQty;
                            }

                            this.TQty = _locInvLineTotal;
                        }
                    }
                    else
                    {
                        _locInvLineTotal = this.Qty + this.DQty;
                        this.TQty = _locInvLineTotal;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = SalesOrderMonitoring " + ex.ToString());
            }
        }

        private void SetTotalUnitAmount()
        {
            try
            {
                _globFunc = new GlobalFunction();

                if (_tQty >= 0 & _uAmount >= 0)
                {
                    if (_globFunc.GetRoundingList(Session, ObjectList.SalesOrderLine, FieldName.TUAmount) == true)
                    {
                        this.TUAmount = _globFunc.GetRoundUp(Session, (this.TQty * this.UAmount), ObjectList.SalesOrderLine, FieldName.TUAmount);
                    }
                    else
                    {
                        this.TUAmount = this.TQty * this.UAmount;
                    }

                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = SalesOrderMonitoring " + ex.ToString());
            }
        }

        private void SetTotalAmount()
        {
            try
            {
                _globFunc = new GlobalFunction();

                if (_uAmount >= 0 && _txAmount >= 0 && _discAmount >= 0)
                {
                    if (!IsLoading)
                    {
                        if (this.Tax != null)
                        {
                            if (this.Tax.TaxNature == TaxNature.Increase)
                            {
                                if (_globFunc.GetRoundingList(Session, ObjectList.SalesOrderLine, FieldName.TAmount) == true)
                                {
                                    this.TAmount = _globFunc.GetRoundUp(Session, (this.TUAmount + this.TxAmount - this.DiscAmount), ObjectList.SalesOrderLine, FieldName.TAmount);
                                }
                                else
                                {
                                    this.TAmount = this.TUAmount + this.TxAmount - this.DiscAmount;
                                }
                            }
                            else if (this.Tax.TaxNature == TaxNature.Decrease)
                            {
                                if (_globFunc.GetRoundingList(Session, ObjectList.SalesOrderLine, FieldName.TAmount) == true)
                                {
                                    this.TAmount = _globFunc.GetRoundUp(Session, (this.TUAmount - this.TxAmount - this.DiscAmount), ObjectList.SalesOrderLine, FieldName.TAmount);
                                }
                                else
                                {
                                    this.TAmount = this.TUAmount - this.TxAmount - this.DiscAmount;
                                }
                            }
                            else if (this.Tax.TaxNature == TaxNature.None)
                            {
                                if (_globFunc.GetRoundingList(Session, ObjectList.SalesOrderLine, FieldName.TAmount) == true)
                                {
                                    this.TAmount = _globFunc.GetRoundUp(Session, (this.TUAmount - this.DiscAmount), ObjectList.SalesOrderLine, FieldName.TAmount);
                                }
                                else
                                {
                                    this.TAmount = this.TUAmount - this.DiscAmount;
                                }
                            }
                            else
                            {
                                if (_globFunc.GetRoundingList(Session, ObjectList.SalesOrderLine, FieldName.TAmount) == true)
                                {
                                    this.TAmount = _globFunc.GetRoundUp(Session, (this.TUAmount + this.TxAmount - this.DiscAmount), ObjectList.SalesOrderLine, FieldName.TAmount);
                                }
                                else
                                {
                                    this.TAmount = this.TUAmount + this.TxAmount - this.DiscAmount;
                                }
                            }
                        }
                        //clm
                        else
                        {
                            this.TAmount = this.TUAmount + this.TxAmount - this.DiscAmount;
                        }
                    }
                }
                //clm
                else
                {
                    this.TAmount = this.TUAmount + this.TxAmount - this.DiscAmount;
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = SalesOrderMonitoring " + ex.ToString());
            }
        }

        private void SetTaxAndDiscountAmount()
        {
            try
            {
                SetTaxBeforeDiscount();
                SetDiscountGross();
                SetTaxAfterDiscount();
                SetDiscountNet();
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = SalesOrderMonitoring " + ex.ToString());
            }
        }

        private void SetNormalTax()
        {
            try
            {
                if (_tax != null && _txValue > 0 && _txAmount > 0)
                {
                    this.Tax = this.Tax;
                    this.TxValue = this.TxValue;
                    this.TxAmount = this.TxAmount;
                }
                else
                {
                    this.TxValue = 0;
                    this.TxAmount = 0;
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = SalesOrderMonitoring " + ex.ToString());
            }
        }

        private void SetPriceLine()
        {
            try
            {
                if (this.Company != null && this.Workplace != null && this.Item != null && this.PriceGroup != null)
                {
                    PriceLine _locPriceLine = Session.FindObject<PriceLine>(new GroupOperator(GroupOperatorType.And,
                                                                            new BinaryOperator("Company", this.Company),
                                                                            new BinaryOperator("Workplace", this.Workplace),
                                                                            new BinaryOperator("Item", this.Item),
                                                                            new BinaryOperator("PriceGroup", this.PriceGroup),
                                                                            new BinaryOperator("Active", true)));
                    if (_locPriceLine != null)
                    {
                        this.PriceLine = _locPriceLine;
                        this.UAmount = _locPriceLine.Amount2a;
                    }
                    else
                    {
                        this.PriceLine = null;
                        this.UAmount = 0;
                    }
                }
                else
                {
                    this.PriceLine = null;
                    this.UAmount = 0;
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = SalesOrderMonitoring " + ex.ToString());
            }
        }

        private void SetUOM()
        {
            try
            {
                if (this.Company != null && this.Item != null && this.DUOM != null)
                {
                    ItemUnitOfMeasure _locIUOM = Session.FindObject<ItemUnitOfMeasure>(new GroupOperator(GroupOperatorType.And,
                                                                            new BinaryOperator("Company", this.Company),
                                                                            new BinaryOperator("Item", this.Item),
                                                                            new BinaryOperator("Default", true),
                                                                            new BinaryOperator("Active", true)));
                    if (_locIUOM != null)
                    {
                        if (_locIUOM.UOM != null)
                        {
                            this.UOM = _locIUOM.UOM;
                        }
                        else
                        {
                            this.UOM = null;
                        }

                    }
                    else
                    {
                        this.UOM = null;
                    }
                }
                else
                {
                    this.UOM = null;
                }

            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = SalesOrderMonitoring " + ex.ToString());
            }
        }

        private void SetDiscountRule()
        {
            try
            {
                if (this.Company != null && this.Workplace != null && this.Item != null && this.PriceGroup != null)
                {
                    DiscountRule _locDiscountRule2a = Session.FindObject<DiscountRule>
                                                            (new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("Company", this.Company),
                                                            new BinaryOperator("Workplace", this.Workplace),
                                                            new BinaryOperator("PriceGroup", this.PriceGroup),
                                                            new BinaryOperator("AllItem", true),
                                                            new BinaryOperator("RuleType", RuleType.Rule2),
                                                            new BinaryOperator("Active", true)));
                    if (_locDiscountRule2a != null)
                    {
                        this.DiscountRule = _locDiscountRule2a;
                    }
                    else
                    {
                        DiscountRule _locDiscountRule2b = Session.FindObject<DiscountRule>
                                                            (new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("Company", this.Company),
                                                            new BinaryOperator("Workplace", this.Workplace),
                                                            new BinaryOperator("PriceGroup", this.PriceGroup),
                                                            new BinaryOperator("MpItem", this.Item),
                                                            new BinaryOperator("RuleType", RuleType.Rule2),
                                                            new BinaryOperator("Active", true)));
                        if (_locDiscountRule2b != null)
                        {
                            this.DiscountRule = _locDiscountRule2b;
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = SalesOrderMonitoring " + ex.ToString());
            }
        }

        #region Tax

        private void SetTaxBeforeDiscount()
        {
            try
            {
                double _locTxAmount = 0;
                double _locTAmount = 0;
                if (this.Tax != null)
                {
                    if (this.Tax.TaxRule == TaxRule.BeforeDiscount)
                    {
                        _locTxAmount = this.TxValue / 100 * this.TUAmount;
                        if (this.Tax.TaxNature == TaxNature.Increase)
                        {
                            _locTAmount = this.TAmount + _locTxAmount;
                        }
                        if (this.Tax.TaxNature == TaxNature.Decrease)
                        {
                            _locTAmount = this.TAmount - _locTxAmount;
                        }
                        this.TxAmount = _locTxAmount;
                        this.TAmount = _locTAmount;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("BusinessObject = SalesOrderMonitoring" + ex.ToString());
            }
        }

        private void SetDiscountGross()
        {
            try
            {
                if (this.DiscountRule != null)
                {
                    double _locTUAmountDiscount = 0;

                    #region Gross
                    if (this.DiscountRule.RuleType == RuleType.Rule2)
                    {
                        if (this.DiscountRule.GrossAmountRule == true)
                        {
                            if (this.TUAmount > 0 && this.TUAmount >= this.DiscountRule.GrossTotalUAmount)
                            {
                                if (this.DiscountRule.DiscountType == DiscountType.Percentage)
                                {
                                    _locTUAmountDiscount = this.TUAmount * this.DiscountRule.Value / 100;
                                }
                                if (this.DiscountRule.DiscountType == DiscountType.Value)
                                {
                                    _locTUAmountDiscount = this.DiscountRule.Value;
                                }

                                this.DiscountChecked = true;
                                this.DiscAmount = _locTUAmountDiscount;
                                this.TAmount = this.TAmount - _locTUAmountDiscount;

                            }

                        }
                    }
                    #endregion Gross
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("BusinessObject = SalesOrderMonitoring" + ex.ToString());
            }
        }

        private void SetTaxAfterDiscount()
        {
            try
            {
                double _locTxAmount = 0;
                double _locTAmount = 0;
                if (this.Tax != null)
                {
                    if(this.DiscountRule != null)
                    {
                        if (this.Tax.TaxRule == TaxRule.AfterDiscount && this.DiscountRule.GrossAmountRule == true)
                        {
                            _locTxAmount = this.TxValue / 100 * (this.TUAmount - this.DiscAmount);

                            if (this.Tax.TaxNature == TaxNature.Increase)
                            {
                                _locTAmount = this.TAmount + _locTxAmount;
                            }
                            if (this.Tax.TaxNature == TaxNature.Decrease)
                            {
                                _locTAmount = this.TAmount - _locTxAmount;
                            }
                            this.TxAmount = _locTxAmount;
                            this.TAmount = _locTAmount;
                        }
                    }
                    else
                    {
                        if (this.Tax.TaxRule == TaxRule.AfterDiscount)
                        {
                            _locTxAmount = this.TxValue / 100 * (this.TUAmount);

                            if (this.Tax.TaxNature == TaxNature.Increase)
                            {
                                _locTAmount = this.TAmount + _locTxAmount;
                            }
                            if (this.Tax.TaxNature == TaxNature.Decrease)
                            {
                                _locTAmount = this.TAmount - _locTxAmount;
                            }
                            this.TxAmount = _locTxAmount;
                            this.TAmount = _locTAmount;
                        }
                    }
                    
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("BusinessObject = SalesOrderMonitoring" + ex.ToString());
            }
        }

        private void SetDiscountNet()
        {
            try
            {
                if (this.DiscountRule != null && this.Tax != null)
                {
                    double _locTUAmountDiscount = 0;

                    #region Net

                    if (this.DiscountRule.RuleType == RuleType.Rule2)
                    {
                        if (this.DiscountRule.NetAmountRule == true && this.Tax.TaxRule == TaxRule.BeforeDiscount)
                        {
                            if (this.TAmount > 0 && this.TAmount >= this.DiscountRule.NetTotalUAmount)
                            {
                                if (this.DiscountRule.DiscountType == DiscountType.Percentage)
                                {
                                    _locTUAmountDiscount = this.TAmount * this.DiscountRule.Value / 100;
                                }
                                if (this.DiscountRule.DiscountType == DiscountType.Value)
                                {
                                    _locTUAmountDiscount = this.DiscountRule.Value;
                                }

                                this.DiscountChecked = true;
                                this.DiscAmount = _locTUAmountDiscount;
                                this.TAmount = this.TAmount - _locTUAmountDiscount;

                            }
                        }
                    }

                    #endregion Net
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("BusinessObject = SalesOrderMonitoring" + ex.ToString());
            }
        }

        #endregion Tax

        private void SetLocation(Employee _locPIC)
        {
            try
            {
                DateTime now = DateTime.Now;
                if (this.Company != null && this.Workplace != null)
                {
                    SalesmanLocationSetup _locSalesmanLocationSetup = Session.FindObject<SalesmanLocationSetup>
                                                        (new GroupOperator(GroupOperatorType.And,
                                                        new BinaryOperator("Company", this.Company),
                                                        new BinaryOperator("Workplace", this.Workplace),
                                                        new BinaryOperator("Salesman", _locPIC),
                                                        new BinaryOperator("Active", true),
                                                        new BinaryOperator("Default", true)));
                    if (_locSalesmanLocationSetup != null)
                    {
                        if (_locSalesmanLocationSetup.Location != null)
                        {
                            this.Location = _locSalesmanLocationSetup.Location;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Business Object = SalesOrder ", ex.ToString());
            }

        }

        private void SetBegInv(Location _locLocation)
        {
            try
            {
                DateTime now = DateTime.Now;
                if (this.Company != null && this.Workplace != null)
                {
                    BeginningInventory _locBegInv = Session.FindObject<BeginningInventory>
                                                        (new GroupOperator(GroupOperatorType.And,
                                                        new BinaryOperator("Company", this.Company),
                                                        new BinaryOperator("Workplace", this.Workplace),
                                                        new BinaryOperator("Location", _locLocation),
                                                        new BinaryOperator("Active", true)));
                    if (_locBegInv != null)
                    {
                        this.BegInv = _locBegInv;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Business Object = SalesOrder ", ex.ToString());
            }
        }

        private void SetBegInvLine(Item _locItem)
        {
            try
            {
                DateTime now = DateTime.Now;
                if (this.Company != null && this.Workplace != null && this.BegInv != null)
                {
                    BeginningInventoryLine _locBegInvLine = Session.FindObject<BeginningInventoryLine>(
                                                                            new GroupOperator(GroupOperatorType.And,
                                                                            new BinaryOperator("Company", this.Company),
                                                                            new BinaryOperator("Workplace", this.Workplace),
                                                                            new BinaryOperator("BeginningInventory", this.BegInv),
                                                                            new BinaryOperator("Item", _locItem),
                                                                            new BinaryOperator("StockType", StockType.Good),
                                                                            new BinaryOperator("StockGroup", StockGroup.Normal),
                                                                            new BinaryOperator("Lock", false),
                                                                            new BinaryOperator("Active", true)));
                    if (_locBegInvLine != null)
                    {
                        this.BegInvLine = _locBegInvLine;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Business Object = SalesOrder ", ex.ToString());
            }
        }

        #endregion Set

        #endregion CodeOnly
    }
}