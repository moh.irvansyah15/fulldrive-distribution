﻿using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;

using DevExpress.ExpressApp.Editors;
using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.ConditionalAppearance;

namespace FullDrive.Module.BusinessObjects
{
    [DefaultClassOptions]
    [DefaultProperty("Code")]
    [NavigationItem("Inventory")]
    [RuleCombinationOfPropertiesIsUnique("DeliveryOrderRuleUnique", DefaultContexts.Save, "Code")]
    //[ImageName("BO_Contact")]
    //[DefaultProperty("DisplayMemberNameForLookupEditorsOfThisType")]
    //[DefaultListViewOptions(MasterDetailMode.ListViewOnly, false, NewItemRowPosition.None)]
    //[Persistent("DatabaseTableName")]
    // Specify more UI options using a declarative approach (https://documentation.devexpress.com/#eXpressAppFramework/CustomDocument112701).
    public class DeliveryOrder : BaseObject
    { // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).

        private bool _activationPosting;
        private string _code;
        #region InitialOrganization
        private Company _company;
        private Workplace _workplace;
        private Division _division;
        private Department _department;
        private Section _section;
        private Employee _employee;
        #endregion InitialOrganization
        private Employee _salesman;
        private XPCollection<Vehicle> _availableVehicle;
        private Vehicle _vehicle;
        private BeginningInventory _beginningInventory;
        private bool _mobile;
        private bool _isRoute;
        private SalesmanCustomerSetup _selectCustomer;
        private DateTime _pickingDate;
        private XPCollection<Picking> _availablePicking;
        private Picking _picking;
        private XPCollection<PickingMonitoring> _availablePickingMonitoring;
        private PickingMonitoring _selectInvoice;
        private XPCollection<BusinessPartner> _availableCustomer;
        private BusinessPartner _customer;
        private string _contact;
        private string _address;
        private SalesInvoice _salesInvoice;
        private DateTime _docDate;
        private int _journalMonth;
        private int _journalYear;
        private Status _status;
        private DateTime _statusDate;
        private string _userAccessIn;
        private GlobalFunction _globFunc;
        //AuditTrail
        private XPCollection<AuditDataItemPersistent> changeHistory;

        public DeliveryOrder(Session session)
            : base(session)
        {
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if (!IsLoading && !IsSaving && !IsInvalidated)
            {
                if (Session.IsNewObject(this))
                {
                    _globFunc = new GlobalFunction();
                    DateTime now = DateTime.Now;
                    #region UserAccess
                    _userAccessIn = SecuritySystem.CurrentUserName;
                    UserAccess _locUserAccess = Session.FindObject<UserAccess>(new BinaryOperator("UserName", _userAccessIn));

                    if (_locUserAccess != null)
                    {
                        if (_locUserAccess.Employee != null)
                        {
                            if (_locUserAccess.Mobile == true)
                            {
                                this.Mobile = _locUserAccess.Mobile;
                            }
                            else
                            {
                                if (_locUserAccess.Employee.Company != null && _locUserAccess.Employee.Workplace != null)
                                {
                                    this.Code = _globFunc.GetNumberingUnlockOptimisticRecordBasedOrganization(this.Session.DataLayer, ObjectList.DeliveryOrder, _locUserAccess.Employee.Company, _locUserAccess.Employee.Workplace);
                                }
                                else
                                {
                                    this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.DeliveryOrder);
                                }
                            }

                            if (_locUserAccess.Employee.Company != null)
                            {
                                Company = _locUserAccess.Employee.Company;
                            }
                            if (_locUserAccess.Employee.Workplace != null)
                            {
                                this.Workplace = _locUserAccess.Employee.Workplace;
                            }
                            else
                            {
                                this.Workplace = _globFunc.GetDefaultWorkplace(this.Session, _locUserAccess);
                            }
                            if (_locUserAccess.Employee.Division != null)
                            {
                                Division = _locUserAccess.Employee.Division;
                            }
                            if (_locUserAccess.Employee.Department != null)
                            {
                                Department = _locUserAccess.Employee.Department;
                            }
                            if (_locUserAccess.Employee.Section != null)
                            {
                                Section = _locUserAccess.Employee.Section;
                            }

                            this.Employee = _locUserAccess.Employee;

                            #region SalesmanSetup

                            SalesmanVehicleSetup _locSalesmanVehicleSetup = Session.FindObject<SalesmanVehicleSetup>(
                                                                new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("UserAccess", _locUserAccess),
                                                                new BinaryOperator("Salesman", _locUserAccess.Employee),
                                                                new BinaryOperator("Active", true)));
                            if (_locSalesmanVehicleSetup != null)
                            {
                                
                                if (_locSalesmanVehicleSetup.Salesman != null)
                                {
                                    this.Salesman = _locSalesmanVehicleSetup.Salesman;
                                }
                                if (_locSalesmanVehicleSetup.Vehicle != null)
                                {
                                    this.Vehicle = _locSalesmanVehicleSetup.Vehicle;
                                    BeginningInventory _locBegInv = Session.FindObject<BeginningInventory>
                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                    new BinaryOperator("Company", this.Company),
                                                                    new BinaryOperator("Workplace", this.Workplace),
                                                                    new BinaryOperator("Vehicle", _locSalesmanVehicleSetup.Vehicle),
                                                                    new BinaryOperator("Active", true)));
                                    if (_locBegInv != null)
                                    {
                                        this.BeginningInventory = _locBegInv;
                                    }
                                }
                            }
                            #endregion SalesmanSetup
                        }

                        this.IsRoute = true;
                    }
                    #endregion UserAccess 
                    this.PickingDate = now;
                    this.Status = CustomProcess.Status.Open;
                    this.StatusDate = now;
                }
            }
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).
        }

        protected override void OnSaving()
        {
            base.OnSaving();
            if (!IsLoading)
            {
                if (this.Mobile == true)
                {
                    if (Session.IsNewObject(this))
                    {
                        if (!(Session is NestedUnitOfWork))
                        {
                            if (this.Code == null)
                            {
                                this.Code = "DONM - " + DistributedIdGeneratorHelper.Generate(this.Session.DataLayer, this.GetType().FullName, string.Empty);
                            }
                        }
                    }
                }
            }
        }

        #region Field

        [Browsable(false)]
        [ImmediatePostData()]
        public bool ActivationPosting
        {
            get { return _activationPosting; }
            set { SetPropertyValue("ActivationPosting", ref _activationPosting, value); }
        }

        [Appearance("DeliveryOrderCodeClose", Enabled = false)]
        //[RuleRequiredField(DefaultContexts.Save)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue("Code", ref _code, value); }
        }

        #region Organization

        [Browsable(false)]
        [ImmediatePostData()]
        [Appearance("DeliveryOrderCompanyClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Company Company
        {
            get { return _company; }
            set { SetPropertyValue("Company", ref _company, value); }
        }

        [Browsable(false)]
        [ImmediatePostData()]
        [Appearance("DeliveryOrderWorkplaceClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Workplace Workplace
        {
            get { return _workplace; }
            set { SetPropertyValue("Workplace", ref _workplace, value); }
        }

        [Browsable(false)]
        public Division Division
        {
            get { return _division; }
            set { SetPropertyValue("Division", ref _division, value); }
        }

        [Browsable(false)]
        public Department Department
        {
            get { return _department; }
            set { SetPropertyValue("Department", ref _department, value); }
        }

        [Browsable(false)]
        public Section Section
        {
            get { return _section; }
            set { SetPropertyValue("Section", ref _section, value); }
        }

        [Browsable(false)]
        public Employee Employee
        {
            get { return _employee; }
            set { SetPropertyValue("Employee", ref _employee, value); }
        }

        #endregion Organization

        [Browsable(false)]
        [ImmediatePostData()]
        [Appearance("DeliveryOrderSalesmanClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Employee Salesman
        {
            get { return _salesman; }
            set { SetPropertyValue("Salesman", ref _salesman, value); }
        }

        [Browsable(false)]
        public XPCollection<Vehicle> AvailableVehicle
        {
            get
            {
                if (!IsLoading)
                {
                    if (this.Company != null && this.Workplace != null)
                    {
                        XPQuery<TransferInventoryMonitoring> _transferInventoryMonitoringsQuery = new XPQuery<TransferInventoryMonitoring>(Session);

                        var _transferInventoryMonitorings = from tim in _transferInventoryMonitoringsQuery
                                                  where ((tim.Status == Status.Open || tim.Status == Status.Posted)
                                                  && tim.Company == this.Company
                                                  && tim.Workplace == this.Workplace
                                                  && tim.Vehicle != null
                                                  && (tim.TransferOrder != null || tim.TransferIn != null))
                                                  group tim by tim.Vehicle into g
                                                  select new { Vehicle = g.Key };

                        if (_transferInventoryMonitorings != null && _transferInventoryMonitorings.Count() > 0)
                        {
                            string _beginString = null;
                            string _endString = null;
                            string _fullString = null;

                            List<string> _stringTIM = new List<string>();

                            foreach (var _transferInventoryMonitoring in _transferInventoryMonitorings)
                            {
                                if (_transferInventoryMonitoring != null)
                                {
                                    if (_transferInventoryMonitoring.Vehicle != null)
                                    {
                                        if (_transferInventoryMonitoring.Vehicle.Code != null)
                                        {
                                            _stringTIM.Add(_transferInventoryMonitoring.Vehicle.Code);
                                        }
                                    }
                                }
                            }

                            IEnumerable<string> _stringArrayTIMDistinct = _stringTIM.Distinct();
                            string[] _stringArrayTIMList = _stringArrayTIMDistinct.ToArray();
                            if (_stringArrayTIMList.Length == 1)
                            {
                                for (int i = 0; i < _stringArrayTIMList.Length; i++)
                                {
                                    if (i == 0)
                                    {
                                        _beginString = "[Code]=='" + _stringArrayTIMList[i] + "'";
                                    }
                                }
                            }
                            else if (_stringArrayTIMList.Length > 1)
                            {
                                for (int i = 0; i < _stringArrayTIMList.Length; i++)
                                {
                                    if (i == 0)
                                    {
                                        _beginString = "[Code]=='" + _stringArrayTIMList[i] + "'";
                                    }
                                    else
                                    {
                                        _endString = _endString + " OR [Code]=='" + _stringArrayTIMList[i] + "'";
                                    }
                                }
                            }
                            _fullString = _beginString + _endString;

                            if (_fullString != null && _fullString != "")
                            {
                                _availableVehicle = new XPCollection<Vehicle>(Session, CriteriaOperator.Parse(_fullString));
                            }
                            else
                            {
                                _availableVehicle = new XPCollection<Vehicle>(Session, new GroupOperator(GroupOperatorType.And,
                                                                            new BinaryOperator("Company", this.Company),
                                                                            new BinaryOperator("Workplace", this.Workplace),
                                                                            new BinaryOperator("Active", true)));
                            }
                        }
                        else
                        {
                            _availableVehicle = new XPCollection<Vehicle>(Session, new GroupOperator(GroupOperatorType.And,
                                                                            new BinaryOperator("Company", this.Company),
                                                                            new BinaryOperator("Workplace", this.Workplace),
                                                                            new BinaryOperator("Active", true)));
                        }
                    }
                    else
                    {
                        _availableVehicle = new XPCollection<Vehicle>(Session, new GroupOperator(GroupOperatorType.And,
                                                                            new BinaryOperator("Active", true)));
                    }

                }

                return _availableVehicle;

            }
        }

        [ImmediatePostData()]
        [DataSourceProperty("AvailableVehicle")]
        [Appearance("DeliveryOrderVehicleClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Vehicle Vehicle
        {
            get { return _vehicle; }
            set {
                SetPropertyValue("Vehicle", ref _vehicle, value);
                if(!IsLoading)
                {
                    if(this._vehicle != null)
                    {
                        SetBegInv(this._vehicle);
                    }
                    else
                    {
                        this.BeginningInventory = null;
                    }
                }
            }
        }

        [Browsable(false)]
        [ImmediatePostData()]
        [Appearance("DeliveryOrderBeginningInventoryClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public BeginningInventory BeginningInventory
        {
            get { return _beginningInventory; }
            set { SetPropertyValue("BeginningInventory", ref _beginningInventory, value); }
        }

        [Browsable(false)]
        [ImmediatePostData()]
        public bool Mobile
        {
            get { return _mobile; }
            set { SetPropertyValue("Mobile", ref _mobile, value); }
        }

        [Browsable(false)]
        [ImmediatePostData()]
        public bool IsRoute
        {
            get { return _isRoute; }
            set { SetPropertyValue("IsRoute", ref _isRoute, value); }
        }

        //Ganti pake Route untuk customernya soalnya biar gak rancu
        [NonPersistent]
        [ImmediatePostData()]
        [DataSourceCriteria("Company = '@This.Company' AND Workplace = '@This.Workplace' AND Salesman = '@This.Salesman' AND Active = true")]
        [Appearance("DeliveryOrderSelectCustomerClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [Appearance("DeliveryOrderSelectCustomerClose2", Criteria = "Mobile = false", Enabled = false)]
        [Appearance("DeliveryOrderSelectCustomerClose3", Criteria = "IsRoute = true", Enabled = false)]
        public SalesmanCustomerSetup SelectCustomer
        {
            get { return _selectCustomer; }
            set
            {
                SetPropertyValue("CustomerChoose", ref _selectCustomer, value);
                if (!IsLoading)
                {
                    if (this._selectCustomer != null)
                    {
                        if (this.SelectCustomer.Customer != null)
                        {
                            this.Customer = this._selectCustomer.Customer;
                            if (this._selectCustomer.Customer.Contact != null)
                            {
                                this.Contact = this._selectCustomer.Customer.Contact;
                            }
                            if (this._selectCustomer.Customer.Address != null)
                            {
                                this.Address = this._selectCustomer.Customer.Address;
                            }
                        }
                        else
                        {
                            this.Customer = this._selectCustomer.Customer;
                            if (this._selectCustomer.Customer.Contact != null)
                            {
                                this.Contact = null;
                            }
                            if (this._selectCustomer.Customer.Address != null)
                            {
                                this.Address = null;
                            }
                        }

                    }
                    else
                    {
                        this.Customer = null;
                        this.Contact = null;
                        this.Address = null;
                    }
                }
            }
        }

        [ImmediatePostData()]
        [ModelDefault("DisplayFormat", "{0:dd/MM/yyyy}")]
        [ModelDefault("EditMask", "dd/MM/yyyy")]
        [Appearance("DeliveryOrderPickingDateClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [Appearance("DeliveryOrderPickingDateClose2", Criteria = "IsRoute = true", Enabled = true)]
        [Appearance("DeliveryOrderPickingDateClose3", Criteria = "IsRoute = false", Enabled = false)]
        public DateTime PickingDate
        {
            get { return _pickingDate; }
            set { SetPropertyValue("PickingDate", ref _pickingDate, value); }

        }

        [Browsable(false)]
        public XPCollection<Picking> AvailablePicking
        {
            get
            {
                if (!IsLoading)
                {
                    if (this.Company != null && this.Workplace != null)
                    {
                        XPQuery<TransferInventoryMonitoring> _transferInventoryMonitoringsQuery = new XPQuery<TransferInventoryMonitoring>(Session);

                        var _transferInventoryMonitorings = from tim in _transferInventoryMonitoringsQuery
                                                  where ((tim.Status == Status.Open || tim.Status == Status.Posted)
                                                  && tim.Company == this.Company
                                                  && tim.Workplace == this.Workplace
                                                  && tim.Picking != null
                                                  && (tim.TransferOrder != null || tim.TransferIn != null))
                                                  group tim by tim.Picking into g
                                                  select new { Picking = g.Key };

                        if (_transferInventoryMonitorings != null && _transferInventoryMonitorings.Count() > 0)
                        {
                            string _beginString = null;
                            string _endString = null;
                            string _fullString = null;

                            List<string> _stringPM = new List<string>();

                            foreach (var _transferInventoryMonitoring in _transferInventoryMonitorings)
                            {
                                if (_transferInventoryMonitoring != null)
                                {
                                    if (_transferInventoryMonitoring.Picking != null)
                                    {
                                        if (_transferInventoryMonitoring.Picking.Code != null && _transferInventoryMonitoring.Picking.PickingDate.ToShortDateString() == this.PickingDate.ToShortDateString())
                                        {
                                            _stringPM.Add(_transferInventoryMonitoring.Picking.Code);
                                        }
                                    }
                                }
                            }

                            IEnumerable<string> _stringArrayPMDistinct = _stringPM.Distinct();
                            string[] _stringArrayPMList = _stringArrayPMDistinct.ToArray();
                            if (_stringArrayPMList.Length == 1)
                            {
                                for (int i = 0; i < _stringArrayPMList.Length; i++)
                                {
                                    if (i == 0)
                                    {
                                        _beginString = "[Code]=='" + _stringArrayPMList[i] + "'";
                                    }
                                }
                            }
                            else if (_stringArrayPMList.Length > 1)
                            {
                                for (int i = 0; i < _stringArrayPMList.Length; i++)
                                {
                                    if (i == 0)
                                    {
                                        _beginString = "[Code]=='" + _stringArrayPMList[i] + "'";
                                    }
                                    else
                                    {
                                        _endString = _endString + " OR [Code]=='" + _stringArrayPMList[i] + "'";
                                    }
                                }
                            }
                            _fullString = _beginString + _endString;

                            if (_fullString != null && _fullString != "")
                            {
                                _availablePicking = new XPCollection<Picking>(Session, CriteriaOperator.Parse(_fullString));
                            }
                        }
                    }
                }

                return _availablePicking;
            }
        }

        [ImmediatePostData()]
        [DataSourceProperty("AvailablePicking")]
        [Appearance("DeliveryOrderPickingClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [Appearance("DeliveryOrderPickingClose2", Criteria = "IsRoute = true", Enabled = true)]
        [Appearance("DeliveryOrderPickingClose3", Criteria = "IsRoute = false", Enabled = false)]
        public Picking Picking
        {
            get { return _picking; }
            set
            {
                SetPropertyValue("Picking", ref _picking, value);
                if (!IsLoading)
                {
                    if (this._picking != null)
                    {
                        if (this._picking.Vehicle != null)
                        {
                            this.Vehicle = this._picking.Vehicle;
                        }
                        else
                        {
                            this.Vehicle = null;
                        }
                    }
                    else
                    {
                        this.Vehicle = null;
                    }
                }
            }
        }

        [Browsable(false)]
        public XPCollection<PickingMonitoring> AvailablePickingMonitoring
        {
            get
            {
                if (!IsLoading)
                {
                    if (this.Company != null && this.Workplace != null && this.Picking != null)
                    {
                        XPQuery<TransferInventoryMonitoring> _transferInventoryMonitoringsQuery = new XPQuery<TransferInventoryMonitoring>(Session);

                        var _transferInventoryMonitorings = from tim in _transferInventoryMonitoringsQuery
                                                            where ((tim.Status == Status.Open || tim.Status == Status.Posted)
                                                            && tim.Company == this.Company
                                                            && tim.Workplace == this.Workplace
                                                            && tim.Picking == this.Picking
                                                            && tim.PickingLine != null
                                                            && (tim.TransferOrder != null || tim.TransferIn != null))
                                                            group tim by tim.PickingLine into g
                                                            select new { PickingLine = g.Key };

                        if (_transferInventoryMonitorings != null && _transferInventoryMonitorings.Count() > 0)
                        {
                            string _beginString = null;
                            string _endString = null;
                            string _fullString = null;

                            List<string> _stringTIM = new List<string>();

                            foreach (var _transferInventoryMonitoring in _transferInventoryMonitorings)
                            {
                                if (_transferInventoryMonitoring != null)
                                {
                                    if (_transferInventoryMonitoring.PickingLine != null)
                                    {
                                        PickingMonitoring _locPickingMonitoring = Session.FindObject<PickingMonitoring>(new GroupOperator(GroupOperatorType.And,
                                                                                    new BinaryOperator("PickingLine", _transferInventoryMonitoring.PickingLine)));

                                        if (_locPickingMonitoring != null )
                                        {
                                            if(_locPickingMonitoring.Code != null)
                                            {
                                                _stringTIM.Add(_locPickingMonitoring.Code);
                                            }
                                        }
                                    }
                                }
                            }

                            IEnumerable<string> _stringArrayTIMDistinct = _stringTIM.Distinct();
                            string[] _stringArrayTIMList = _stringArrayTIMDistinct.ToArray();
                            if (_stringArrayTIMList.Length == 1)
                            {
                                for (int i = 0; i < _stringArrayTIMList.Length; i++)
                                {
                                    if (i == 0)
                                    {
                                        _beginString = "[Code]=='" + _stringArrayTIMList[i] + "'";
                                    }
                                }
                            }
                            else if (_stringArrayTIMList.Length > 1)
                            {
                                for (int i = 0; i < _stringArrayTIMList.Length; i++)
                                {
                                    if (i == 0)
                                    {
                                        _beginString = "[Code]=='" + _stringArrayTIMList[i] + "'";
                                    }
                                    else
                                    {
                                        _endString = _endString + " OR [Code]=='" + _stringArrayTIMList[i] + "'";
                                    }
                                }
                            }
                            _fullString = _beginString + _endString;

                            if (_fullString != null && _fullString != "")
                            {
                                _availablePickingMonitoring = new XPCollection<PickingMonitoring>(Session, CriteriaOperator.Parse(_fullString));
                            }
                        }
                    }
                }

                return _availablePickingMonitoring;
            }
        }

        [ImmediatePostData()]
        [DataSourceProperty("AvailablePickingMonitoring")]
        [ModelDefault("LookupProperty", "SalesInvoice")]
        [Appearance("DeliveryOrderSelectInvoiceClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [Appearance("DeliveryOrderSelectInvoiceClose2", Criteria = "IsRoute = true", Enabled = true)]
        [Appearance("DeliveryOrderSelectInvoiceClose3", Criteria = "IsRoute = false", Enabled = false)]
        public PickingMonitoring SelectInvoice
        {
            get { return _selectInvoice; }
            set {
                SetPropertyValue("SelectInvoice", ref _selectInvoice, value);
                if (!IsLoading)
                {
                    if (this._selectInvoice != null)
                    {
                        if (this._selectInvoice.Customer != null)
                        {
                            this.Customer = this._selectInvoice.Customer;
                        }
                        else
                        {
                            this.Customer = null;
                        }
                        if (this._selectInvoice.SalesInvoice != null)
                        {
                            this.SalesInvoice = this._selectInvoice.SalesInvoice;
                            this.DocDate = this._selectInvoice.SalesInvoice.DocDate;
                            this.JournalMonth = this._selectInvoice.SalesInvoice.JournalMonth;
                            this.JournalYear = this._selectInvoice.SalesInvoice.JournalYear;
                        }
                        else
                        {
                            this.SalesInvoice = null;
                            this.DocDate = DateTime.Now;
                            this.JournalMonth = 0;
                        }

                    }
                }
            }
        }

        [Browsable(false)]
        public XPCollection<BusinessPartner> AvailableCustomer
        {
            get
            {
                string _beginString = null;
                string _endString = null;
                string _fullString = null;

                if (this.Company != null && this.Workplace != null && this.Salesman != null)
                {
                    List<string> _stringSCS = new List<string>();

                    XPCollection<SalesmanCustomerSetup> _locSalesmanCustomerSetups = new XPCollection<SalesmanCustomerSetup>
                                                                              (Session, new GroupOperator(GroupOperatorType.And,
                                                                               new BinaryOperator("Company", this.Company),
                                                                               new BinaryOperator("Workplace", this.Workplace),
                                                                               new BinaryOperator("Salesman", this.Salesman),
                                                                               new BinaryOperator("Active", true)));

                    if (_locSalesmanCustomerSetups != null && _locSalesmanCustomerSetups.Count() > 0)
                    {
                        foreach (SalesmanCustomerSetup _locSalesmanCustomerSetup in _locSalesmanCustomerSetups)
                        {
                            if (_locSalesmanCustomerSetup.Customer != null)
                            {
                                if (_locSalesmanCustomerSetup.Customer.Code != null)
                                {
                                    _stringSCS.Add(_locSalesmanCustomerSetup.Customer.Code);
                                }
                            }
                        }
                    }

                    IEnumerable<string> _stringArraySCSDistinct = _stringSCS.Distinct();
                    string[] _stringArraySCSList = _stringArraySCSDistinct.ToArray();
                    if (_stringArraySCSList.Length == 1)
                    {
                        for (int i = 0; i < _stringArraySCSList.Length; i++)
                        {
                            BusinessPartner _locBP = Session.FindObject<BusinessPartner>(new BinaryOperator("Code", _stringArraySCSList[i]));
                            if (_locBP != null)
                            {
                                if (i == 0)
                                {
                                    _beginString = "[Code]=='" + _locBP.Code + "'";
                                }
                            }
                        }
                    }
                    else if (_stringArraySCSList.Length > 1)
                    {
                        for (int i = 0; i < _stringArraySCSList.Length; i++)
                        {
                            BusinessPartner _locBP = Session.FindObject<BusinessPartner>(new BinaryOperator("Code", _stringArraySCSList[i]));
                            if (_locBP != null)
                            {
                                if (i == 0)
                                {
                                    _beginString = "[Code]=='" + _locBP.Code + "'";
                                }
                                else
                                {
                                    _endString = _endString + " OR [Code]=='" + _locBP.Code + "'";
                                }
                            }
                        }
                    }
                    _fullString = _beginString + _endString;

                    if (_fullString != null)
                    {
                        _availableCustomer = new XPCollection<BusinessPartner>(Session, CriteriaOperator.Parse(_fullString));
                    }
                }
                else
                {
                    _availableCustomer = new XPCollection<BusinessPartner>(Session, new GroupOperator(GroupOperatorType.And,
                                                                            new BinaryOperator("Active", true)));
                }

                return _availableCustomer;

            }
        }

        [ImmediatePostData()]
        [DataSourceProperty("AvailableCustomer", DataSourcePropertyIsNullMode.SelectAll)]
        [Appearance("DeliveryOrderCustomerClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [Appearance("DeliveryOrderCustomerClose2", Criteria = "Mobile = true", Enabled = false)]
        public BusinessPartner Customer
        {
            get { return _customer; }
            set
            {
                SetPropertyValue("Customer", ref _customer, value);
                if (!IsLoading)
                {
                    if (this._customer != null)
                    {
                        if (this._customer.Contact != null)
                        {
                            this.Contact = this._customer.Contact;
                        }
                        if (this._customer.Address != null)
                        {
                            this.Address = this._customer.Address;
                        } 
                    }
                    else
                    {
                        this.Contact = null;
                        this.Address = null;
                    }
                }
            }
        }

        [Appearance("DeliveryOrderContactClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public string Contact
        {
            get { return _contact; }
            set { SetPropertyValue("Contact", ref _contact, value); }
        }

        [Appearance("DeliveryOrderAddressClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public string Address
        {
            get { return _address; }
            set { SetPropertyValue("Address", ref _address, value); }
        }

        [ImmediatePostData()]
        [DataSourceCriteria("Company = '@This.Company' AND Workplace = '@This.Workplace' AND SalesToCustomer = '@This.Customer' AND Status == 3")]
        [Appearance("DeliveryOrderSalesInvoiceClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public SalesInvoice SalesInvoice
        {
            get { return _salesInvoice; }
            set { SetPropertyValue("SalesInvoice", ref _salesInvoice, value); }
        }

        [Browsable(false)]
        [ImmediatePostData()]
        [VisibleInListView(false)]
        [ModelDefault("DisplayFormat", "{0:dd/MM/yyyy}")]
        [ModelDefault("EditMask", "dd/MM/yyyy")]
        [Appearance("DeliveryOrderDocDateClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public DateTime DocDate
        {
            get { return _docDate; }
            set
            {
                SetPropertyValue("DocDate", ref _docDate, value);
                if (!IsLoading)
                {
                    this.JournalMonth = this._docDate.Month;
                    this.JournalYear = this._docDate.Year;
                }
            }
        }

        [Browsable(false)]
        public int JournalMonth
        {
            get { return _journalMonth; }
            set { SetPropertyValue("JournalMonth", ref _journalMonth, value); }
        }

        [Browsable(false)]
        public int JournalYear
        {
            get { return _journalYear; }
            set { SetPropertyValue("JournalYear", ref _journalYear, value); }
        }

        [Appearance("DeliveryOrderStatusClose", Enabled = false)]
        public Status Status
        {
            get { return _status; }
            set { SetPropertyValue("Status", ref _status, value); }
        }

        [ModelDefault("DisplayFormat", "{0:dd/MM/yyyy}")]
        [ModelDefault("EditMask", "dd/MM/yyyy")]
        [Appearance("DeliveryOrderStatusDateClose", Enabled = false)]
        public DateTime StatusDate
        {
            get { return _statusDate; }
            set { SetPropertyValue("StatusDate", ref _statusDate, value); }
        }

        [Association("DeliveryOrder-DeliveryOrderLines")]
        public XPCollection<DeliveryOrderLine> DeliveryOrderLines
        {
            get { return GetCollection<DeliveryOrderLine>("DeliveryOrderLines"); }
        }

        //AuditTrail
        [CollectionOperationSet(AllowAdd = false, AllowRemove = true)]
        public XPCollection<AuditDataItemPersistent> ChangeHistory
        {
            get
            {
                if (changeHistory == null)
                {
                    changeHistory = AuditedObjectWeakReference.GetAuditTrail(Session, this);
                }
                return changeHistory;
            }
        }

        #endregion Field

        //=============================================== Code Only ====================================================

        private void SetBegInv(Vehicle _locVehicle)
        {
            try
            {
                if (this.Company != null && this.Workplace != null)
                {
                    BeginningInventory _locBegInv = Session.FindObject<BeginningInventory>
                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                    new BinaryOperator("Company", this.Company),
                                                                    new BinaryOperator("Workplace", this.Workplace),
                                                                    new BinaryOperator("Vehicle", _locVehicle),
                                                                    new BinaryOperator("Active", true)));
                    if (_locBegInv != null)
                    {
                        this.BeginningInventory = _locBegInv;
                    }
                    else
                    {
                        this.BeginningInventory = null;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Business Object = DeliveryOrder ", ex.ToString());
            }
        }

    }
}