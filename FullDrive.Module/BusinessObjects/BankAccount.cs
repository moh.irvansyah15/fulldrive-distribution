﻿using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.ConditionalAppearance;

namespace FullDrive.Module.BusinessObjects
{
    [DefaultClassOptions]
    [DefaultProperty("FullName")]
    [NavigationItem("Finance & Accounting")]
    [RuleCombinationOfPropertiesIsUnique("BankAccountRuleUnique", DefaultContexts.Save, "Code")]
    //[ImageName("BO_Contact")]
    //[DefaultProperty("DisplayMemberNameForLookupEditorsOfThisType")]
    //[DefaultListViewOptions(MasterDetailMode.ListViewOnly, false, NewItemRowPosition.None)]
    //[Persistent("DatabaseTableName")]
    // Specify more UI options using a declarative approach (https://documentation.devexpress.com/#eXpressAppFramework/CustomDocument112701).
    public class BankAccount : BaseObject
    { // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).
        private string _code;
        private Company _company;
        private XPCollection<Workplace> _availableWorkplace;
        private Workplace _workplace;
        private string _name;
        private string _fullName;
        private string _address;
        private Country _country;
        private City _city;
        private string _accountNo;
        private BankAccountGroup _bankAccountGroup;
        private BusinessPartner _businessPartner;
        private string _accountName;
        private bool _default;
        private bool _active;
        private string _userAccessIn;
        private GlobalFunction _globFunc;
        //AuditTrail
        private XPCollection<AuditDataItemPersistent> changeHistory;

        public BankAccount(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if (!IsLoading)
            {
                _globFunc = new GlobalFunction();
                #region UserAccess
                _userAccessIn = SecuritySystem.CurrentUserName;
                UserAccess _locUserAccess = Session.FindObject<UserAccess>(new BinaryOperator("UserName", _userAccessIn));
                if (Session.IsNewObject(this))
                {
                    if (_locUserAccess != null)
                    {
                        if (_locUserAccess.Employee != null)
                        {
                            if (_locUserAccess.Employee.Company != null && _locUserAccess.Employee.Workplace != null)
                            {
                                this.Code = _globFunc.GetNumberingUnlockOptimisticRecordBasedOrganization(this.Session.DataLayer, ObjectList.BankAccount, _locUserAccess.Employee.Company, _locUserAccess.Employee.Workplace);
                            }
                            else
                            {
                                this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.BankAccount);
                            }
                            if (_locUserAccess.Employee.Company != null)
                            {
                                Company = _locUserAccess.Employee.Company;
                            }
                            if (_locUserAccess.Employee.Workplace != null)
                            {
                                Workplace = _locUserAccess.Employee.Workplace;
                            }
                            else
                            {
                                this.Workplace = _globFunc.GetDefaultWorkplace(this.Session, _locUserAccess);
                            }
                        }
                    }
                }
                #endregion UserAccess
            }
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).
        }

        [RuleRequiredField(DefaultContexts.Save)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue("Code", ref _code, value); }
        }

        [ImmediatePostData()]
        [Association("Company-BankAccounts")]
        [RuleRequiredField(DefaultContexts.Save)]
        [DataSourceCriteria("Active = true")]
        public Company Company
        {
            get { return _company; }
            set { SetPropertyValue("Company", ref _company, value); }
        }

        [Browsable(false)]
        public XPCollection<Workplace> AvailableWorkplace
        {
            get
            {
                if (!IsLoading)
                {
                    if (this.Company != null)
                    {
                        _userAccessIn = SecuritySystem.CurrentUserName;
                        UserAccess _locUserAccess = Session.FindObject<UserAccess>(new BinaryOperator("UserName", _userAccessIn));
                        if (_locUserAccess != null)
                        {
                            string _beginString = null;
                            string _endString = null;
                            string _fullString = null;
                            List<string> _stringWorkplace = new List<string>();

                            XPCollection<OrganizationSetupDetail> _locOrganizationSetupDetails = new XPCollection<OrganizationSetupDetail>
                                                                            (Session, new GroupOperator(GroupOperatorType.And,
                                                                            new BinaryOperator("Company", this.Company),
                                                                            new BinaryOperator("UserAccess", _locUserAccess),
                                                                            new BinaryOperator("OpenWorkplace", true),
                                                                            new BinaryOperator("Active", true)));

                            if (_locOrganizationSetupDetails != null && _locOrganizationSetupDetails.Count() > 0)
                            {
                                foreach (OrganizationSetupDetail _locOrganizationSetupDetail in _locOrganizationSetupDetails)
                                {
                                    if (_locOrganizationSetupDetail.Workplace != null)
                                    {
                                        if (_locOrganizationSetupDetail.Workplace.Code != null)
                                        {
                                            _stringWorkplace.Add(_locOrganizationSetupDetail.Workplace.Code);
                                        }
                                    }
                                }

                                #region Workplace
                                IEnumerable<string> _stringArrayWorkplaceDistinct = _stringWorkplace.Distinct();
                                string[] _stringArrayWorkplaceList = _stringArrayWorkplaceDistinct.ToArray();
                                if (_stringArrayWorkplaceList.Length == 1)
                                {
                                    for (int i = 0; i < _stringArrayWorkplaceList.Length; i++)
                                    {
                                        Workplace _locWorkplace = Session.FindObject<Workplace>(new BinaryOperator("Code", _stringArrayWorkplaceList[i]));
                                        if (_locWorkplace != null)
                                        {
                                            if (i == 0)
                                            {
                                                _beginString = "[Code]=='" + _locWorkplace.Code + "'";
                                            }
                                        }
                                    }
                                }
                                else if (_stringArrayWorkplaceList.Length > 1)
                                {
                                    for (int i = 0; i < _stringArrayWorkplaceList.Length; i++)
                                    {
                                        Workplace _locWorkplace = Session.FindObject<Workplace>(new BinaryOperator("Code", _stringArrayWorkplaceList[i]));
                                        if (_locWorkplace != null)
                                        {
                                            if (i == 0)
                                            {
                                                _beginString = "[Code]=='" + _locWorkplace.Code + "'";
                                            }
                                            else
                                            {
                                                _endString = _endString + " OR [Code]=='" + _locWorkplace.Code + "'";
                                            }
                                        }

                                    }
                                }
                                _fullString = _beginString + _endString;


                                if (_fullString != null)
                                {
                                    _availableWorkplace = new XPCollection<Workplace>(Session, CriteriaOperator.Parse(_fullString));
                                }

                                #endregion Workplace
                            }
                            else
                            {
                                OrganizationSetupDetail _locOrgSetDetail = Session.FindObject<OrganizationSetupDetail>
                                                                            (new GroupOperator(GroupOperatorType.And,
                                                                            new BinaryOperator("Company", this.Company),
                                                                            new BinaryOperator("UserAccess", _locUserAccess),
                                                                            new BinaryOperator("AllWorkplace", true),
                                                                            new BinaryOperator("Active", true)));
                                if (_locOrgSetDetail != null)
                                {
                                    _availableWorkplace = new XPCollection<Workplace>(Session,
                                                        new GroupOperator(GroupOperatorType.And,
                                                        new BinaryOperator("Company", this.Company),
                                                        new BinaryOperator("Active", true)));
                                }
                            }
                        }
                    }
                }

                return _availableWorkplace;

            }
        }

        [ImmediatePostData()]
        [DataSourceProperty("AvailableWorkplace")]
        public Workplace Workplace
        {
            get { return _workplace; }
            set { SetPropertyValue("Workplace", ref _workplace, value); }
        }

        [RuleRequiredField(DefaultContexts.Save)]
        [ImmediatePostData]
        public string Name
        {
            get { return _name; }
            set
            {
                SetPropertyValue("Name", ref _name, value);
                if (!IsLoading)
                {
                    if (this._name != null)
                    {
                        SetFullName();
                    }
                }
            }
        }

        [Appearance("BankAccountFullNameEnabled", Enabled = false)]
        public string FullName
        {
            get { return _fullName; }
            set { SetPropertyValue("FullName", ref _fullName, value); }
        }

        [Size(512)]
        public string Address
        {
            get { return _address; }
            set { SetPropertyValue("Address", ref _address, value); }
        }

        [DataSourceCriteria("Active = true")]
        public Country Country
        {
            get { return _country; }
            set { SetPropertyValue("Country", ref _country, value); }
        }

        [DataSourceCriteria("Active = true")]
        public City City
        {
            get { return _city; }
            set { SetPropertyValue("City", ref _city, value); }
        }

        [ImmediatePostData()]
        public string AccountNo
        {
            get { return _accountNo; }
            set
            {
                SetPropertyValue("AccountNo", ref _accountNo, value);
                if (!IsLoading)
                {
                    if (this._accountNo != null)
                    {
                        SetFullName();
                    }
                }
            }
        }

        [Association("BankAccountGroup-BankAccounts")]
        [DataSourceCriteria("Company = '@This.Company' AND Workplace = '@This.Workplace' AND Active = true")]
        public BankAccountGroup BankAccountGroup
        {
            get { return _bankAccountGroup; }
            set { SetPropertyValue("BankAccountGroup", ref _bankAccountGroup, value); }
        }

        public string AccountName
        {
            get { return _accountName; }
            set { SetPropertyValue("AccountName", ref _accountName, value); }
        }

        [Association("BusinessPartner-BankAccounts")]
        [DataSourceCriteria("Active = true")]
        public BusinessPartner BusinessPartner
        {
            get { return _businessPartner; }
            set { SetPropertyValue("BusinessPartner", ref _businessPartner, value); }
        }

        public bool Default
        {
            get { return _default; }
            set { SetPropertyValue("Default", ref _default, value); }
        }

        public bool Active
        {
            get { return _active; }
            set { SetPropertyValue("Active", ref _active, value); }
        }

        [Association("BankAccount-BankAccountMaps")]
        public XPCollection<BankAccountMap> BankAccountMaps
        {
            get { return GetCollection<BankAccountMap>("BankAccountMaps"); }
        }

        //AuditTrail
        [CollectionOperationSet(AllowAdd = false, AllowRemove = true)]
        public XPCollection<AuditDataItemPersistent> ChangeHistory
        {
            get
            {
                if (changeHistory == null)
                {
                    changeHistory = AuditedObjectWeakReference.GetAuditTrail(Session, this);
                }
                return changeHistory;
            }
        }

        //========================================= Coce Only =========================================

        private void SetFullName()
        {
            try
            {
                if (this._name != null && this._accountNo != null)
                {
                    this.FullName = String.Format("{0} ({1})", this.Name, this.AccountNo);
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = BankAccount ", ex.ToString());
            }
        }

    }
}