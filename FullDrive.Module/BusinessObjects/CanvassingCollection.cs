﻿using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;

using DevExpress.ExpressApp.Editors;
using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.ConditionalAppearance;

namespace FullDrive.Module.BusinessObjects
{
    [DefaultClassOptions]
    [DefaultProperty("Code")]
    [NavigationItem("Finance & Accounting")]
    [RuleCombinationOfPropertiesIsUnique("CanvassingCollectionRuleUnique", DefaultContexts.Save, "Code")]
    //[ImageName("BO_Contact")]
    //[DefaultProperty("DisplayMemberNameForLookupEditorsOfThisType")]
    //[DefaultListViewOptions(MasterDetailMode.ListViewOnly, false, NewItemRowPosition.None)]
    //[Persistent("DatabaseTableName")]
    // Specify more UI options using a declarative approach (https://documentation.devexpress.com/#eXpressAppFramework/CustomDocument112701).
    public class CanvassingCollection : BaseObject
    { // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).

        private bool _activationPosting;
        private string _code;
        #region InitialOrganization
        private Company _company;
        private Workplace _workplace;
        private Division _division;
        private Department _department;
        private Section _section;
        private Employee _employee;
        #endregion InitialOrganization
        private XPCollection<Employee> _availableSalesman;
        private Employee _salesman;
        private XPCollection<Vehicle> _availableVehicle;
        private Vehicle _vehicle;
        private BeginningInventory _beginningInventory;
        private Currency _currency;
        private XPCollection<PriceGroup> _availablePriceGroup;
        private PriceGroup _priceGroup;
        private double _totAmount;
        private double _totTaxAmount;
        private double _totDiscAmount;
        private double _amount;
        private double _totCollectedAmount;
        private PaymentType _paymentType;
        private Status _status;
        private DateTime _statusDate;
        private string _userAccessIn;
        private GlobalFunction _globFunc;
        //AuditTrail
        private XPCollection<AuditDataItemPersistent> changeHistory;

        public CanvassingCollection(Session session)
            : base(session)
        {
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if (!IsLoading)
            {
                if (Session.IsNewObject(this))
                {
                    _globFunc = new GlobalFunction();
                    DateTime now = DateTime.Now;
                    #region UserAccess
                    _userAccessIn = SecuritySystem.CurrentUserName;
                    UserAccess _locUserAccess = Session.FindObject<UserAccess>(new BinaryOperator("UserName", _userAccessIn));
                
                    if (_locUserAccess != null)
                    {
                        if (_locUserAccess.Employee != null)
                        {
                            if (_locUserAccess.Employee.Company != null && _locUserAccess.Employee.Workplace != null)
                            {
                                this.Code = _globFunc.GetNumberingUnlockOptimisticRecordBasedOrganization(this.Session.DataLayer, ObjectList.CanvassingCollection, _locUserAccess.Employee.Company, _locUserAccess.Employee.Workplace);
                            }
                            else
                            {
                                this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.CanvassingCollection);
                            }

                            if (_locUserAccess.Employee.Company != null)
                            {
                                this.Company = _locUserAccess.Employee.Company;
                            }
                            if (_locUserAccess.Employee.Workplace != null)
                            {
                                this.Workplace = _locUserAccess.Employee.Workplace;
                            }
                            else
                            {
                                this.Workplace = _globFunc.GetDefaultWorkplace(this.Session, _locUserAccess);
                            }
                            if (_locUserAccess.Employee.Division != null)
                            {
                                Division = _locUserAccess.Employee.Division;
                            }
                            if (_locUserAccess.Employee.Department != null)
                            {
                                Department = _locUserAccess.Employee.Department;
                            }
                            if (_locUserAccess.Employee.Section != null)
                            {
                                Section = _locUserAccess.Employee.Section;
                            }

                            this.Employee = _locUserAccess.Employee;
                        }

                        
                    }
                    #endregion UserAccess
                    this.Status = CustomProcess.Status.Open;
                    this.StatusDate = now;
                    this.Currency = _globFunc.GetDefaultCurrency(this.Session);
                    this.PaymentType = PaymentType.Cash;
                }
            }
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).
        }

        #region Field

        [Browsable(false)]
        [ImmediatePostData()]
        public bool ActivationPosting
        {
            get { return _activationPosting; }
            set { SetPropertyValue("ActivationPosting", ref _activationPosting, value); }
        }

        [Appearance("CanvassingCollectionCodeClose", Enabled = false)]
        [RuleRequiredField(DefaultContexts.Save)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue("Code", ref _code, value); }
        }

        #region Organization

        //[Browsable(false)]
        [ImmediatePostData()]
        [RuleRequiredField(DefaultContexts.Save)]
        [Appearance("CanvassingCollectionCompanyClose", Enabled = false)]
        public Company Company
        {
            get { return _company; }
            set { SetPropertyValue("Company", ref _company, value); }
        }

        //[Browsable(false)]
        [ImmediatePostData()]
        [RuleRequiredField(DefaultContexts.Save)]
        [Appearance("CanvassingCollectionWorkplaceClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Workplace Workplace
        {
            get { return _workplace; }
            set { SetPropertyValue("Workplace", ref _workplace, value); }
        }

        [Browsable(false)]
        public Division Division
        {
            get { return _division; }
            set { SetPropertyValue("Division", ref _division, value); }
        }

        [Browsable(false)]
        public Department Department
        {
            get { return _department; }
            set { SetPropertyValue("Department", ref _department, value); }
        }

        [Browsable(false)]
        public Section Section
        {
            get { return _section; }
            set { SetPropertyValue("Section", ref _section, value); }
        }

        [Browsable(false)]
        public Employee Employee
        {
            get { return _employee; }
            set { SetPropertyValue("Employee", ref _employee, value); }
        }

        #endregion Organization

        [Browsable(false)]
        public XPCollection<Employee> AvailableSalesman
        {
            get
            {

                if (this.Company != null && this.Workplace != null )
                {
                    XPCollection<Employee> _locSalesmans = new XPCollection<Employee>
                                                            (Session, new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("Company", this.Company),
                                                            new BinaryOperator("Workplace", this.Workplace),
                                                            new BinaryOperator("Active", true),
                                                            new GroupOperator(GroupOperatorType.Or,
                                                            new BinaryOperator("SalesRole", SalesRole.Canvassing),
                                                            new BinaryOperator("SalesRole", SalesRole.All))));
                    if (_locSalesmans != null && _locSalesmans.Count() > 0)
                    {
                        _availableSalesman = _locSalesmans;
                    }
                }
                else
                {
                    _availableSalesman = new XPCollection<Employee>(Session, new GroupOperator(GroupOperatorType.And,
                                                                            new BinaryOperator("Active", true),
                                                                            new GroupOperator(GroupOperatorType.Or,
                                                                            new BinaryOperator("SalesRole", SalesRole.Canvassing),
                                                                            new BinaryOperator("SalesRole", SalesRole.All))));
                }

                return _availableSalesman;

            }
        }

        [ImmediatePostData()]
        [RuleRequiredField(DefaultContexts.Save)]
        [DataSourceProperty("AvailableSalesman", DataSourcePropertyIsNullMode.SelectAll)]
        [Appearance("CanvassingCollectionSalesmanClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Employee Salesman
        {
            get { return _salesman; }
            set { SetPropertyValue("Salesman", ref _salesman, value); }
        }

        [Browsable(false)]
        public XPCollection<Vehicle> AvailableVehicle
        {
            get
            {
                string _beginString = null;
                string _endString = null;
                string _fullString = null;


                if (this.Company != null && this.Workplace != null && this.Salesman != null)
                {

                    List<string> _stringSVS = new List<string>();

                    XPCollection<SalesmanVehicleSetup> _locSalesmanVehicleSetups = new XPCollection<SalesmanVehicleSetup>
                                                                              (Session, new GroupOperator(GroupOperatorType.And,
                                                                               new BinaryOperator("Company", this.Company),
                                                                               new BinaryOperator("Workplace", this.Workplace),
                                                                               new BinaryOperator("Salesman", this.Salesman),
                                                                               new BinaryOperator("Active", true)));

                    if (_locSalesmanVehicleSetups != null && _locSalesmanVehicleSetups.Count() > 0)
                    {
                        foreach (SalesmanVehicleSetup _locSalesmanVehicleSetup in _locSalesmanVehicleSetups)
                        {
                            if (_locSalesmanVehicleSetup.Vehicle != null)
                            {
                                if (_locSalesmanVehicleSetup.Vehicle.Code != null)
                                {
                                    _stringSVS.Add(_locSalesmanVehicleSetup.Vehicle.Code);
                                }
                            }
                        }
                    }

                    IEnumerable<string> _stringArraySVSDistinct = _stringSVS.Distinct();
                    string[] _stringArraySVSList = _stringArraySVSDistinct.ToArray();
                    if (_stringArraySVSList.Length == 1)
                    {
                        for (int i = 0; i < _stringArraySVSList.Length; i++)
                        {
                            Vehicle _locVHC = Session.FindObject<Vehicle>(new BinaryOperator("Code", _stringArraySVSList[i]));
                            if (_locVHC != null)
                            {
                                if (i == 0)
                                {
                                    _beginString = "[Code]=='" + _locVHC.Code + "'";
                                }
                            }
                        }
                    }
                    else if (_stringArraySVSList.Length > 1)
                    {
                        for (int i = 0; i < _stringArraySVSList.Length; i++)
                        {
                            Vehicle _locVHC = Session.FindObject<Vehicle>(new BinaryOperator("Code", _stringArraySVSList[i]));
                            if (_locVHC != null)
                            {
                                if (i == 0)
                                {
                                    _beginString = "[Code]=='" + _locVHC.Code + "'";
                                }
                                else
                                {
                                    _endString = _endString + " OR [Code]=='" + _locVHC.Code + "'";
                                }
                            }
                        }
                    }
                    _fullString = _beginString + _endString;

                    if (_fullString != null)
                    {
                        _availableVehicle = new XPCollection<Vehicle>(Session, CriteriaOperator.Parse(_fullString));
                    }

                }
                else
                {
                    _availableVehicle = new XPCollection<Vehicle>(Session, new GroupOperator(GroupOperatorType.And,
                                                                            new BinaryOperator("Active", true)));
                }

                return _availableVehicle;

            }
        }

        [ImmediatePostData()]
        [RuleRequiredField(DefaultContexts.Save)]
        [DataSourceProperty("AvailableVehicle", DataSourcePropertyIsNullMode.SelectAll)]
        [Appearance("CanvassingCollectionVehicleClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Vehicle Vehicle
        {
            get { return _vehicle; }
            set {
                SetPropertyValue("Vehicle", ref _vehicle, value);
                if(!IsLoading)
                {
                    if(this._vehicle != null)
                    {
                        BeginningInventory _locBegInv = Session.FindObject<BeginningInventory>
                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                    new BinaryOperator("Company", this.Company),
                                                                    new BinaryOperator("Workplace", this.Workplace),
                                                                    new BinaryOperator("Vehicle", this.Vehicle),
                                                                    new BinaryOperator("Active", true)));
                        if (_locBegInv != null)
                        {
                            this.BeginningInventory = _locBegInv;
                        }
                    }
                }
            }
        }

        [Browsable(false)]
        [ImmediatePostData()]
        [Appearance("CanvassingCollectionBeginningInventoryClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public BeginningInventory BeginningInventory
        {
            get { return _beginningInventory; }
            set { SetPropertyValue("BeginningInventory", ref _beginningInventory, value); }
        }

        [Browsable(false)]
        [Appearance("CanvassingCollectionCurrencyClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Currency Currency
        {
            get { return _currency; }
            set { SetPropertyValue("Currency", ref _currency, value); }
        }

        [Browsable(false)]
        public XPCollection<PriceGroup> AvailablePriceGroup
        {
            get
            {
                string _beginString = null;
                string _endString = null;
                string _fullString = null;


                if (this.Company != null && this.Workplace != null && this.Salesman != null)
                {
                    List<string> _stringSCS = new List<string>();

                    XPQuery<SalesmanCustomerSetup> _salesmenCustomerSetupssQuery = new XPQuery<SalesmanCustomerSetup>(Session);

                    var _salesmanCustomerSetups = from scs in _salesmenCustomerSetupssQuery
                                                  where (scs.Company == this.Company
                                                  && scs.Workplace == this.Workplace
                                                  && scs.Salesman  == this.Salesman 
                                                  )
                                                  group scs by scs.PriceGroup into g
                                                  select new { PriceGroup = g.Key };

                    
                    if (_salesmanCustomerSetups != null && _salesmanCustomerSetups.Count() > 0)
                    {
                        foreach (var _salesmanCustomerSetup in _salesmanCustomerSetups)
                        {
                            if (_salesmanCustomerSetup.PriceGroup != null)
                            {
                                if (_salesmanCustomerSetup.PriceGroup.Code != null)
                                {
                                    _stringSCS.Add(_salesmanCustomerSetup.PriceGroup.Code);
                                }
                            }
                        }
                    }

                    IEnumerable<string> _stringArraySCSDistinct = _stringSCS.Distinct();
                    string[] _stringArraySCSList = _stringArraySCSDistinct.ToArray();
                    if (_stringArraySCSList.Length == 1)
                    {
                        for (int i = 0; i < _stringArraySCSList.Length; i++)
                        {
                            PriceGroup _locPG = Session.FindObject<PriceGroup>(new BinaryOperator("Code", _stringArraySCSList[i]));
                            if (_locPG != null)
                            {
                                if (i == 0)
                                {
                                    _beginString = "[Code]=='" + _locPG.Code + "'";
                                }
                            }
                        }
                    }
                    else if (_stringArraySCSList.Length > 1)
                    {
                        for (int i = 0; i < _stringArraySCSList.Length; i++)
                        {
                            PriceGroup _locPG = Session.FindObject<PriceGroup>(new BinaryOperator("Code", _stringArraySCSList[i]));
                            if (_locPG != null)
                            {
                                if (i == 0)
                                {
                                    _beginString = "[Code]=='" + _locPG.Code + "'";
                                }
                                else
                                {
                                    _endString = _endString + " OR [Code]=='" + _locPG.Code + "'";
                                }
                            }
                        }
                    }
                    _fullString = _beginString + _endString;

                    if (_fullString != null)
                    {
                        _availablePriceGroup = new XPCollection<PriceGroup>(Session, CriteriaOperator.Parse(_fullString));
                    }

                }
                else
                {
                    _availablePriceGroup = new XPCollection<PriceGroup>(Session, new GroupOperator(GroupOperatorType.And,
                                                                            new BinaryOperator("Active", true)));
                }

                return _availablePriceGroup;

            }
        }

        [ImmediatePostData()]
        [DataSourceProperty("AvailablePriceGroup", DataSourcePropertyIsNullMode.SelectAll)]
        [Appearance("CanvassingCollectionPriceGroupClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public PriceGroup PriceGroup
        {
            get { return _priceGroup; }
            set { SetPropertyValue("PriceGroup", ref _priceGroup, value); }
        }

        [Appearance("CanvassingCollectionTotAmountClose", Enabled = false)]
        public double TotAmount
        {
            get { return _totAmount; }
            set { SetPropertyValue("TotAmount", ref _totAmount, value); }
        }

        [Appearance("CanvassingCollectionTotTaxAmountClose", Enabled = false)]
        public double TotTaxAmount
        {
            get { return _totTaxAmount; }
            set { SetPropertyValue("TotTaxAmount", ref _totTaxAmount, value); }
        }

        [Appearance("InvoiceCanvassingCollectionTotDiscAmountClose", Enabled = false)]
        public double TotDiscAmount
        {
            get { return _totDiscAmount; }
            set { SetPropertyValue("TotDiscAmount", ref _totDiscAmount, value); }
        }

        [Appearance("CanvassingCollectionAmountClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public double Amount
        {
            get { return _amount; }
            set { SetPropertyValue("Amount", ref _amount, value); }
        }

        [Appearance("CanvassingCollectionTotCollectedAmountClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public double TotCollectedAmount
        {
            get { return _totCollectedAmount; }
            set { SetPropertyValue("TotCollectedAmount", ref _totCollectedAmount, value); }
        }

        [Appearance("CanvassingCollectionPaymentTypeClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public PaymentType PaymentType
        {
            get { return _paymentType; }
            set { SetPropertyValue("PaymentType", ref _paymentType, value); }
        }

        [Appearance("CanvassingCollectionStatusClose", Enabled = false)]
        public Status Status
        {
            get { return _status; }
            set { SetPropertyValue("Status", ref _status, value); }
        }

        [Appearance("CanvassingCollectionStatusDateClose", Enabled = false)]
        public DateTime StatusDate
        {
            get { return _statusDate; }
            set { SetPropertyValue("StatusDate", ref _statusDate, value); }
        }

        [Association("CanvassingCollection-CanvassingCollectionLines")]
        public XPCollection<CanvassingCollectionLine> CanvassingCollectionLines
        {
            get { return GetCollection<CanvassingCollectionLine>("CanvassingCollectionLines"); }
        }

        [Association("CanvassingCollection-CanvassingCollectionFreeItems")]
        public XPCollection<CanvassingCollectionFreeItem> CanvassingCollectionFreeItems
        {
            get { return GetCollection<CanvassingCollectionFreeItem>("CanvassingCollectionFreeItems"); }
        }

        [Association("CanvassingCollection-InvoiceCanvassingCollections")]
        public XPCollection<InvoiceCanvassingCollection> InvoiceCanvassingCollections
        {
            get { return GetCollection<InvoiceCanvassingCollection>("InvoiceCanvassingCollections"); }
        }

        //AuditTrail
        [CollectionOperationSet(AllowAdd = false, AllowRemove = true)]
        public XPCollection<AuditDataItemPersistent> ChangeHistory
        {
            get
            {
                if (changeHistory == null)
                {
                    changeHistory = AuditedObjectWeakReference.GetAuditTrail(Session, this);
                }
                return changeHistory;
            }
        }

        #endregion Field


    }
}