﻿using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;

using DevExpress.ExpressApp.ConditionalAppearance;
using DevExpress.ExpressApp.SystemModule;
using FullDrive.Module.CustomProcess;

namespace FullDrive.Module.BusinessObjects
{
    [DefaultClassOptions]
    [DefaultProperty("Code")]
    [NavigationItem("Inventory")]
    [RuleCombinationOfPropertiesIsUnique("InventoryTransferInLineRuleUnique", DefaultContexts.Save, "Code")]
    //[ImageName("BO_Contact")]
    //[DefaultProperty("DisplayMemberNameForLookupEditorsOfThisType")]
    //[DefaultListViewOptions(MasterDetailMode.ListViewOnly, false, NewItemRowPosition.None)]
    //[Persistent("DatabaseTableName")]
    // Specify more UI options using a declarative approach (https://documentation.devexpress.com/#eXpressAppFramework/CustomDocument112701).
    public class InventoryTransferInLine : BaseObject
    { // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).
        private bool _activationPosting;
        private int _no;        
        private string _code;
        private bool _select;
        #region InitialOrganization
        private Company _company;
        private Workplace _workplace;
        private Division _division;
        private Department _department;
        private Section _section;
        private Employee _employee;
        #endregion InitialOrganization
        #region InitialLocation
        private LocationType _locationType;
        private StockType _stockType;
        private XPCollection<Location> _availableLocation;
        private Location _location;
        private XPCollection<BinLocation> _availableBinLocation;
        private BinLocation _binLocation;       
        private StockGroup _stockGroup;
        #endregion InitialLocation
        private OrderType _purchaseType;
        private XPCollection<Item> _availableItem;
        private Item _item;
        private Brand _brand;
        private string _description;
        private XPCollection<UnitOfMeasure> _availableUnitOfMeasure;
        #region InisialisasiDefaultQty
        private double _dQty;
        private UnitOfMeasure _dUom;
        private double _qty;
        private UnitOfMeasure _uom;
        private double _tQty;
        #endregion InisialisasiDefaultQty        
        private DateTime _estimatedDate;
        private DateTime _actualDate;
        private DateTime _docDate;
        private int _journalMonth;
        private int _journalYear;
        private Status _status;
        private DateTime _statusDate;
        private string _signCode;        
        private InventoryTransferIn _inventoryTransferIn;
        private PurchaseOrderMonitoring _purchaseOrderMonitoring;
        private string _localUserAccess;
        private GlobalFunction _globFunc;
        //AuditTrail
        private XPCollection<AuditDataItemPersistent> changeHistory;

        public InventoryTransferInLine(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if (!IsLoading)
            {
                if(Session.IsNewObject(this))
                {
                    _globFunc = new GlobalFunction();
                    DateTime now = DateTime.Now;
                    #region UserAccess
                    _localUserAccess = SecuritySystem.CurrentUserName;
                    UserAccess _locUserAccess = Session.FindObject<UserAccess>(new BinaryOperator("UserName", _localUserAccess));
                    if (_locUserAccess != null)
                    {
                        if (_locUserAccess.Employee != null)
                        {
                            if (_locUserAccess.Employee.Company != null && _locUserAccess.Employee.Workplace != null)
                            {
                                this.Code = _globFunc.GetNumberingUnlockOptimisticRecordBasedOrganization(this.Session.DataLayer, ObjectList.InventoryTransferInLine, _locUserAccess.Employee.Company, _locUserAccess.Employee.Workplace);
                            }
                            else
                            {
                                this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.InventoryTransferInLine);
                            }
                        }
                    }
                    #endregion UserAccess
                    this.Status = Status.Open;
                    this.StatusDate = now;
                    this.StockType = StockType.Good;
                    this.StockGroup = StockGroup.Normal;
                    this.Select = true;
                }                
            }
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).
        }       

        protected override void OnSaving()
        {
            base.OnSaving();
            if (!IsLoading && IsSaving)
            {
                UpdateNo();
            }
        }

        protected override void OnDeleting()
        {
            base.OnDeleting();
            if (!IsLoading)
            {
                RecoveryDeleteNo();
            }
        }

        #region Field

        [Browsable(false)]
        public bool ActivationPosting
        {
            get { return _activationPosting; }
            set { SetPropertyValue("ActivationPosting", ref _activationPosting, value); }
        }

        [Appearance("InventoryTransferInLineNoEnabled", Enabled = false)]
        public int No
        {
            get { return _no; }
            set { SetPropertyValue("No", ref _no, value); }
        }

        [VisibleInListView(false)]
        [RuleRequiredField(DefaultContexts.Save)]
        [Appearance("InventoryTransferInLineCodeEnabled", Enabled = false)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue("Code", ref _code, value); }
        }

        [Appearance("InventoryTransferInLineSelectClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public bool Select
        {
            get { return _select; }
            set { SetPropertyValue("Select", ref _select, value); }
        }

        #region Organization

        [VisibleInListView(false)]
        [Appearance("InventoryTransferInLineCompanyClose", Enabled = false)]
        public Company Company
        {
            get { return _company; }
            set { SetPropertyValue("Company", ref _company, value); }
        }

        [VisibleInListView(false)]
        [Appearance("InventoryTransferInLineWorkplaceClose", Enabled = false)]
        public Workplace Workplace
        {
            get { return _workplace; }
            set { SetPropertyValue("Workplace", ref _workplace, value); }
        }

        [Browsable(false)]
        public Division Division
        {
            get { return _division; }
            set { SetPropertyValue("Division", ref _division, value); }
        }

        [Browsable(false)]
        public Department Department
        {
            get { return _department; }
            set { SetPropertyValue("Department", ref _department, value); }
        }

        [Browsable(false)]
        public Section Section
        {
            get { return _section; }
            set { SetPropertyValue("Section", ref _section, value); }
        }

        [Browsable(false)]
        public Employee Employee
        {
            get { return _employee; }
            set { SetPropertyValue("Employee", ref _employee, value); }
        }

        #endregion Organization

        #region Location

        [ImmediatePostData()]
        [Appearance("InventoryTransferInLineLocationTypeClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public LocationType LocationType
        {
            get { return _locationType; }
            set { SetPropertyValue("LocationType", ref _locationType, value); }
        }

        [Appearance("InventoryTransferInLineStockTypeClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public StockType StockType
        {
            get { return _stockType; }
            set { SetPropertyValue("StockType", ref _stockType, value); }
        }

        [Browsable(false)]
        public XPCollection<Location> AvailableLocation
        {
            get
            {
                if(!IsLoading)
                {
                    _localUserAccess = SecuritySystem.CurrentUserName;
                    string _beginString = null;
                    string _endString = null;
                    string _fullString = null;
                    string _locLocationCode = null;
                    UserAccess _locUserAccess = Session.FindObject<UserAccess>(new BinaryOperator("UserName", _localUserAccess));
                    List<string> _stringLocation = new List<string>();
                    if (_locUserAccess != null)
                    {
                        if (this.InventoryTransferIn != null)
                        {
                            #region InventoryTransferIn
                            if (this.InventoryTransferIn.InventoryMovingType == InventoryMovingType.Receive)
                            {
                                XPCollection<WarehouseSetupDetail> _locWhsSetupDetails = null;

                                if (this.LocationType == LocationType.None)
                                {
                                    _locWhsSetupDetails = new XPCollection<WarehouseSetupDetail>
                                                        (Session, new GroupOperator(GroupOperatorType.And,
                                                        new BinaryOperator("UserAccess", _locUserAccess),
                                                        new BinaryOperator("Owner", true),
                                                        new BinaryOperator("Active", true)));
                                }
                                else
                                {
                                    _locWhsSetupDetails = new XPCollection<WarehouseSetupDetail>
                                                        (Session, new GroupOperator(GroupOperatorType.And,
                                                        new BinaryOperator("UserAccess", _locUserAccess),
                                                        new BinaryOperator("Owner", true),
                                                        new BinaryOperator("LocationType", this.LocationType),
                                                        new BinaryOperator("Active", true)));
                                }

                                if (_locWhsSetupDetails != null && _locWhsSetupDetails.Count() > 0)
                                {
                                    foreach (WarehouseSetupDetail _locWhsSetupDetail in _locWhsSetupDetails)
                                    {
                                        if (_locWhsSetupDetail.Location != null)
                                        {
                                            if (_locWhsSetupDetail.Location.Code != null)
                                            {
                                                _locLocationCode = _locWhsSetupDetail.Location.Code;
                                                _stringLocation.Add(_locLocationCode);
                                            }
                                        }
                                    }
                                }

                                IEnumerable<string> _stringArrayLocationFromDistinct = _stringLocation.Distinct();
                                string[] _stringArrayLocationFromList = _stringArrayLocationFromDistinct.ToArray();
                                if (_stringArrayLocationFromList.Length == 1)
                                {
                                    for (int i = 0; i < _stringArrayLocationFromList.Length; i++)
                                    {
                                        Location _locLocationFrom = Session.FindObject<Location>(new BinaryOperator("Code", _stringArrayLocationFromList[i]));
                                        if (_locLocationFrom != null)
                                        {
                                            if (i == 0)
                                            {
                                                _beginString = "[Code]=='" + _locLocationFrom.Code + "'";
                                            }
                                        }
                                    }
                                }
                                else if (_stringArrayLocationFromList.Length > 1)
                                {
                                    for (int i = 0; i < _stringArrayLocationFromList.Length; i++)
                                    {
                                        Location _locLocationFrom = Session.FindObject<Location>(new BinaryOperator("Code", _stringArrayLocationFromList[i]));
                                        if (_locLocationFrom != null)
                                        {
                                            if (i == 0)
                                            {
                                                _beginString = "[Code]=='" + _locLocationFrom.Code + "'";
                                            }
                                            else
                                            {
                                                _endString = _endString + " OR [Code]=='" + _locLocationFrom.Code + "'";
                                            }
                                        }
                                    }
                                }
                                _fullString = _beginString + _endString;

                                if (_fullString != null)
                                {
                                    _availableLocation = new XPCollection<Location>(Session, CriteriaOperator.Parse(_fullString));
                                }
                            }
                            #endregion InventoryTransferIn
                        }
                    }
                }
                return _availableLocation;
            }
        }

        [Appearance("InventoryTransferInLineLocationClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [DataSourceProperty("AvailableLocation")]
        [ImmediatePostData()]
        [DataSourceCriteria("Active = true")]
        public Location Location
        {
            get { return _location; }
            set { SetPropertyValue("Location", ref _location, value); }
        }

        [Browsable(false)]
        public XPCollection<BinLocation> AvailableBinLocation
        {
            get
            {
                if(!IsLoading)
                {
                    _localUserAccess = SecuritySystem.CurrentUserName;
                    string _beginString = null;
                    string _endString = null;
                    string _fullString = null;
                    string _locBinLocationCode = null;
                    UserAccess _locUserAccess = Session.FindObject<UserAccess>(new BinaryOperator("UserName", _localUserAccess));
                    List<string> _stringBinLocation = new List<string>();
                    if (_locUserAccess != null)
                    {
                        if (this.Location != null)
                        {
                            if (this.InventoryTransferIn != null)
                            {
                                #region InventoryTransferIn
                                if (this.InventoryTransferIn.InventoryMovingType == InventoryMovingType.Receive)
                                {

                                    XPCollection<WarehouseSetupDetail> _locWhsSetupDetails = new XPCollection<WarehouseSetupDetail>
                                                                                      (Session, new GroupOperator(GroupOperatorType.And,
                                                                                       new BinaryOperator("UserAccess", _locUserAccess),
                                                                                       new BinaryOperator("Location", this.Location),
                                                                                       new BinaryOperator("Owner", true),
                                                                                       new BinaryOperator("Active", true)));

                                    if (_locWhsSetupDetails != null && _locWhsSetupDetails.Count() > 0)
                                    {
                                        foreach (WarehouseSetupDetail _locWhsSetupDetail in _locWhsSetupDetails)
                                        {
                                            if (_locWhsSetupDetail.BinLocation != null)
                                            {
                                                if (_locWhsSetupDetail.BinLocation.Code != null)
                                                {
                                                    _locBinLocationCode = _locWhsSetupDetail.BinLocation.Code;
                                                    _stringBinLocation.Add(_locBinLocationCode);
                                                }
                                            }
                                        }
                                    }

                                    IEnumerable<string> _stringArrayBinLocationFromDistinct = _stringBinLocation.Distinct();
                                    string[] _stringArrayBinLocationFromList = _stringArrayBinLocationFromDistinct.ToArray();
                                    if (_stringArrayBinLocationFromList.Length == 1)
                                    {
                                        for (int i = 0; i < _stringArrayBinLocationFromList.Length; i++)
                                        {
                                            BinLocation _locBinLocationFrom = Session.FindObject<BinLocation>(new BinaryOperator("Code", _stringArrayBinLocationFromList[i]));
                                            if (_locBinLocationFrom != null)
                                            {
                                                if (i == 0)
                                                {
                                                    _beginString = "[Code]=='" + _locBinLocationFrom.Code + "'";
                                                }
                                            }
                                        }
                                    }
                                    else if (_stringArrayBinLocationFromList.Length > 1)
                                    {
                                        for (int i = 0; i < _stringArrayBinLocationFromList.Length; i++)
                                        {
                                            BinLocation _locBinLocationFrom = Session.FindObject<BinLocation>(new BinaryOperator("Code", _stringArrayBinLocationFromList[i]));
                                            if (_locBinLocationFrom != null)
                                            {
                                                if (i == 0)
                                                {
                                                    _beginString = "[Code]=='" + _locBinLocationFrom.Code + "'";
                                                }
                                                else
                                                {
                                                    _endString = _endString + " OR [Code]=='" + _locBinLocationFrom.Code + "'";
                                                }
                                            }
                                        }
                                    }
                                    _fullString = _beginString + _endString;

                                    if (_fullString != null)
                                    {
                                        _availableBinLocation = new XPCollection<BinLocation>(Session, CriteriaOperator.Parse(_fullString));
                                    }
                                }
                                #endregion InventoryTransferIn
                            }
                        }
                    }
                }
                return _availableBinLocation;
            }
        }

        [Appearance("InventoryTransferInLineBinLocationClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [DataSourceProperty("AvailableBinLocation")]
        [DataSourceCriteria("Active = true")]
        public BinLocation BinLocation
        {
            get { return _binLocation; }
            set { SetPropertyValue("BinLocation", ref _binLocation, value); }
        }

        [Appearance("InventoryTransferInLineStockGroupClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public StockGroup StockGroup
        {
            get { return _stockGroup; }
            set { SetPropertyValue("StockGroup", ref _stockGroup, value); }
        }

        #endregion Location

        [VisibleInListView(false)]
        [ImmediatePostData()]
        [Appearance("InventoryTransferInLinePurchaseTypeClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public OrderType PurchaseType
        {
            get { return _purchaseType; }
            set { SetPropertyValue("PurchaseType", ref _purchaseType, value); }
        }

        [Browsable(false)]
        public XPCollection<Item> AvailableItem
        {
            get
            {
                if(!IsLoading)
                {
                    if (PurchaseType == OrderType.Item)
                    {
                        _availableItem = new XPCollection<Item>(Session,
                                        new GroupOperator(GroupOperatorType.And,
                                        new BinaryOperator("ItemType", OrderType.Item)));

                    }
                    else if (PurchaseType == OrderType.Service)
                    {
                        _availableItem = new XPCollection<Item>(Session,
                                        new GroupOperator(GroupOperatorType.And,
                                        new BinaryOperator("ItemType", OrderType.Service)));
                    }
                    else
                    {
                        _availableItem = new XPCollection<Item>(Session);
                    }
                }
                return _availableItem;
            }
        }

        [ImmediatePostData()]
        [DataSourceProperty("AvailableItem", DataSourcePropertyIsNullMode.SelectAll)]
        [Appearance("InventoryTransferInLineItemClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Item Item
        {
            get { return _item; }
            set
            {
                SetPropertyValue("Item", ref _item, value);
                if (!IsLoading)
                {
                    if (this._item != null)
                    {
                        this.DUOM = this._item.BasedUOM;
                        this.Brand = this._item.Brand;
                        this.Description = GetDescriptions();
                    }
                    else
                    {
                        this.DUOM = null;
                        this.Brand = null;
                        this.Description = null;
                    }
                }
            }
        }

        [VisibleInListView(false)]
        [Appearance("InventoryTransferInLineBrandClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Brand Brand
        {
            get { return _brand; }
            set { SetPropertyValue("Brand", ref _brand, value); }
        }

        [Appearance("InventoryTransferInLineDescriptionClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [Size(512)]
        public string Description
        {
            get { return _description; }
            set { SetPropertyValue("Description", ref _description, value); }
        }

        [Browsable(false)]
        public XPCollection<UnitOfMeasure> AvailableUnitOfMeasure
        {
            get
            {
                if(!IsLoading)
                {
                    string _beginString = null;
                    string _endString = null;
                    string _fullString = null;
                    if (Item == null)
                    {
                        _availableUnitOfMeasure = new XPCollection<UnitOfMeasure>(Session);
                    }
                    else
                    {
                        List<string> _stringUOM = new List<string>();

                        XPCollection<ItemUnitOfMeasure> _locItemUnitOfMeasures = new XPCollection<ItemUnitOfMeasure>
                                                                                  (Session, new GroupOperator(GroupOperatorType.And,
                                                                                   new BinaryOperator("Item", this.Item),
                                                                                   new BinaryOperator("Active", true)));

                        if (_locItemUnitOfMeasures != null && _locItemUnitOfMeasures.Count() > 0)
                        {
                            foreach (ItemUnitOfMeasure _locItemUnitOfMeasure in _locItemUnitOfMeasures)
                            {
                                _stringUOM.Add(_locItemUnitOfMeasure.UOM.Code);
                            }
                        }

                        IEnumerable<string> _stringArrayUOMDistinct = _stringUOM.Distinct();
                        string[] _stringArrayUOMList = _stringArrayUOMDistinct.ToArray();
                        if (_stringArrayUOMList.Length == 1)
                        {
                            for (int i = 0; i < _stringArrayUOMList.Length; i++)
                            {
                                UnitOfMeasure _locUOM = Session.FindObject<UnitOfMeasure>(new BinaryOperator("Code", _stringArrayUOMList[i]));
                                if (_locUOM != null)
                                {
                                    if (i == 0)
                                    {
                                        _beginString = "[Code]=='" + _locUOM.Code + "'";
                                    }
                                }
                            }
                        }
                        else if (_stringArrayUOMList.Length > 1)
                        {
                            for (int i = 0; i < _stringArrayUOMList.Length; i++)
                            {
                                UnitOfMeasure _locUOM = Session.FindObject<UnitOfMeasure>(new BinaryOperator("Code", _stringArrayUOMList[i]));
                                if (_locUOM != null)
                                {
                                    if (i == 0)
                                    {
                                        _beginString = "[Code]=='" + _locUOM.Code + "'";
                                    }
                                    else
                                    {
                                        _endString = _endString + " OR [Code]=='" + _locUOM.Code + "'";
                                    }
                                }
                            }
                        }
                        _fullString = _beginString + _endString;

                        if (_fullString != null)
                        {
                            _availableUnitOfMeasure = new XPCollection<UnitOfMeasure>(Session, CriteriaOperator.Parse(_fullString));
                        }
                    }
                }                
                return _availableUnitOfMeasure;
            }
        }

        #region DefaultQty

        [Appearance("InventoryTransferInLineDQtyClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [ImmediatePostData()]
        public double DQty
        {
            get { return _dQty; }
            set
            {
                SetPropertyValue("DQty", ref _dQty, value);
                if (!IsLoading)
                {
                    SetTotalQty();
                }
            }
        }

        [Appearance("InventoryTransferInLineDUOMClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [ImmediatePostData()]
        [DataSourceCriteria("Active = true")]
        public UnitOfMeasure DUOM
        {
            get { return _dUom; }
            set
            {
                SetPropertyValue("DUOM", ref _dUom, value);
                if (!IsLoading)
                {
                    SetTotalQty();
                }
            }
        }

        [Appearance("InventoryTransferInLineQtyClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [ImmediatePostData()]
        public double Qty
        {
            get { return _qty; }
            set
            {
                SetPropertyValue("Qty", ref _qty, value);
                if (!IsLoading)
                {
                    SetTotalQty();
                }
            }
        }

        [Appearance("InventoryTransferInLineUOMClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [ImmediatePostData()]
        [DataSourceProperty("AvailableUnitOfMeasure", DataSourcePropertyIsNullMode.SelectAll)]
        [DataSourceCriteria("Active = true")]
        public UnitOfMeasure UOM
        {
            get { return _uom; }
            set
            {
                SetPropertyValue("UOM", ref _uom, value);
                if (!IsLoading)
                {
                    SetTotalQty();
                }
            }
        }

        [Appearance("InventoryTransferInLineTotalQtyEnabled", Enabled = false)]
        public double TQty
        {
            get { return _tQty; }
            set { SetPropertyValue("TQty", ref _tQty, value); }
        }

        [VisibleInListView(false)]
        [Persistent]
        public string FullName
        {
            get
            {
                if (this.DQty > 0 && this.Qty > 0)
                {
                    return String.Format("{0} {1} {2} {3}", this.Qty, this.UOM.Code, this.DQty, this.DUOM.Code);
                }
                else if (this.DQty > 0 && this.Qty == 0)
                {
                    return String.Format("{0} {1}", this.DQty, this.DUOM.Code);
                }
                else if (this.DQty == 0 && this.Qty > 0)
                {
                    return String.Format("{0} {1}", this.Qty, this.UOM.Code);
                }
                return null;
            }
        }

        #endregion DefaultQty      

        [VisibleInListView(false)]
        [ModelDefault("DisplayFormat", "{0:dd/MM/yyyy}")]
        [ModelDefault("EditMask", "dd/MM/yyyy")]
        [Appearance("InventoryTransferInLineEstimatedDateClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public DateTime EstimatedDate
        {
            get { return _estimatedDate; }
            set { SetPropertyValue("EstimatedDate", ref _estimatedDate, value); }
        }

        [ModelDefault("DisplayFormat", "{0:dd/MM/yyyy}")]
        [ModelDefault("EditMask", "dd/MM/yyyy")]
        [Appearance("InventoryTransferInLineActualDateEnabled", Enabled = false)]
        public DateTime ActualDate
        {
            get { return _actualDate; }
            set { SetPropertyValue("ActualDate", ref _actualDate, value); }
        }

        [Browsable(false)]
        [ImmediatePostData()]
        [ModelDefault("DisplayFormat", "{0:dd/MM/yyyy}")]
        [ModelDefault("EditMask", "dd/MM/yyyy")]
        [Appearance("InventoryTransferInLineDocDateClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public DateTime DocDate
        {
            get { return _docDate; }
            set
            {
                SetPropertyValue("DocDate", ref _docDate, value);
                if (!IsLoading)
                {
                    this.JournalMonth = this._docDate.Month;
                    this.JournalYear = this._docDate.Year;
                }
            }
        }

        [Browsable(false)]
        public int JournalMonth
        {
            get { return _journalMonth; }
            set { SetPropertyValue("JournalMonth", ref _journalMonth, value); }
        }

        [Browsable(false)]
        public int JournalYear
        {
            get { return _journalYear; }
            set { SetPropertyValue("JournalYear", ref _journalYear, value); }
        }

        [VisibleInListView(false)]
        [Appearance("InventoryTransferInLineStatusEnabled", Enabled = false)]
        public Status Status
        {
            get { return _status; }
            set { SetPropertyValue("Status", ref _status, value); }
        }

        [VisibleInListView(false)]
        [ModelDefault("DisplayFormat", "{0:dd/MM/yyyy}")]
        [ModelDefault("EditMask", "dd/MM/yyyy")]
        [Appearance("InventoryTransferInLineStatusDateEnabled", Enabled = false)]
        public DateTime StatusDate
        {
            get { return _statusDate; }
            set { SetPropertyValue("StatusDate", ref _statusDate, value); }
        }

        [VisibleInListView(false)]
        [Appearance("InventoryTransferInLineSignCodeClose", Enabled = false)]
        public string SignCode
        {
            get { return _signCode; }
            set { SetPropertyValue("SignCode", ref _signCode, value); }
        }

        [VisibleInListView(false)]
        [Appearance("InventoryTransferInLinePurchaseOrderMonitoringClose", Enabled = false)]
        public PurchaseOrderMonitoring PurchaseOrderMonitoring
        {
            get { return _purchaseOrderMonitoring; }
            set { SetPropertyValue("PurchaseOrderMonitoring", ref _purchaseOrderMonitoring, value); }
        }

        [VisibleInListView(false)]
        [Appearance("InventoryTransferInLineInventoryTransferInEnabled", Enabled = false)]
        [RuleRequiredField(DefaultContexts.Save)]
        [ImmediatePostData()]
        [Association("InventoryTransferIn-InventoryTransferInLines")]
        public InventoryTransferIn InventoryTransferIn
        {
            get { return _inventoryTransferIn; }
            set
            {
                SetPropertyValue("InventoryTransferIn", ref _inventoryTransferIn, value);
                if (!IsLoading)
                {
                    if (_inventoryTransferIn != null)
                    {
                        if (this._inventoryTransferIn.Location != null)
                        {
                            this.Location = _inventoryTransferIn.Location;
                        }
                        if (this._inventoryTransferIn.LocationType != LocationType.None)
                        {
                            this.LocationType = this._inventoryTransferIn.LocationType;
                        }
                        if (this._inventoryTransferIn.StockType != StockType.None)
                        {
                            this.StockType = this._inventoryTransferIn.StockType;
                        }
                        if (this._inventoryTransferIn.Company != null)
                        {
                            this.Company = this._inventoryTransferIn.Company;
                        }
                        if (this._inventoryTransferIn.Workplace != null)
                        {
                            this.Workplace = this._inventoryTransferIn.Workplace;
                        }
                        if (this._inventoryTransferIn.Division != null)
                        {
                            this.Division = this._inventoryTransferIn.Division;
                        }
                        if (this._inventoryTransferIn.Department != null)
                        {
                            this.Department = this._inventoryTransferIn.Department;
                        }
                        if (this._inventoryTransferIn.Section != null)
                        {
                            this.Section = this._inventoryTransferIn.Section;
                        }
                        if (this._inventoryTransferIn.Employee != null)
                        {
                            this.Employee = this._inventoryTransferIn.Employee;
                        }
                        this.EstimatedDate = _inventoryTransferIn.EstimatedDate;
                    }
                }
            }
        }

        [Association("InventoryTransferInLine-InventoryTransferInLots")]
        public XPCollection<InventoryTransferInLot> InventoryTransferInLots
        {
            get { return GetCollection<InventoryTransferInLot>("InventoryTransferInLots"); }
        }

        //AuditTrail
        [CollectionOperationSet(AllowAdd = false, AllowRemove = true)]
        public XPCollection<AuditDataItemPersistent> ChangeHistory
        {
            get
            {
                if (changeHistory == null)
                {
                    changeHistory = AuditedObjectWeakReference.GetAuditTrail(Session, this);
                }
                return changeHistory;
            }
        }

        #endregion Field

        //=========================================== Code Only ===========================================

        public string GetDescriptions()
        {
            string _result = "";
            try
            {
                if (this._item != null)
                {
                    string _result1 = null;
                    string _result2 = null;
                    if (this._item.Name != null)
                    {
                        _result1 = this._item.Name;
                    }
                    if (this._item.Description != null)
                    {
                        _result2 = this._item.Description;
                    }
                    _result = _result1 + " " + _result2;
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = InventoryTransferInLine " + ex.ToString());
            }

            return _result;
        }

        #region No

        public void UpdateNo()
        {
            try
            {
                if (!IsLoading && Session.IsNewObject(this))
                {
                    if (this.InventoryTransferIn != null)
                    {
                        object _makRecord = Session.Evaluate<InventoryTransferInLine>(CriteriaOperator.Parse("Max(No)"), CriteriaOperator.Parse("InventoryTransferIn=?", this.InventoryTransferIn));
                        this.No = Convert.ToInt32(_makRecord) + 1;
                        this.Save();
                        RecoveryUpdateNo();
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = InventoryTransferInLine " + ex.ToString());
            }
        }

        public void RecoveryUpdateNo()
        {
            try
            {
                if (this.InventoryTransferIn != null)
                {
                    InventoryTransferIn _numHeader = Session.FindObject<InventoryTransferIn>
                                                (new BinaryOperator("Code", this.InventoryTransferIn.Code));

                    XPCollection<InventoryTransferInLine> _numLines = new XPCollection<InventoryTransferInLine>
                                                (Session, new BinaryOperator("InventoryTransferIn", _numHeader),
                                                new SortProperty("No", DevExpress.Xpo.DB.SortingDirection.Ascending));
                    if (_numLines != null)
                    {
                        int i1 = 0;
                        foreach (InventoryTransferInLine _numLine in _numLines)
                        {
                            i1 += 1;
                            _numLine.No = i1;
                            _numLine.Save();
                        }
                        i1 = 1;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = InventoryTransferInLine " + ex.ToString());
            }
        }

        public void RecoveryDeleteNo()
        {
            try
            {
                if (this.InventoryTransferIn != null)
                {
                    InventoryTransferIn _numHeader = Session.FindObject<InventoryTransferIn>
                                                (new BinaryOperator("Code", this.InventoryTransferIn.Code));

                    XPCollection<InventoryTransferInLine> _numLines = new XPCollection<InventoryTransferInLine>
                                                (Session, new GroupOperator(GroupOperatorType.And,
                                                 new BinaryOperator("This", this, BinaryOperatorType.NotEqual),
                                                 new BinaryOperator("InventoryTransferIn", _numHeader)),
                                                 new SortProperty("No", DevExpress.Xpo.DB.SortingDirection.Ascending));
                    if (_numLines != null)
                    {
                        int i = 0;
                        foreach (InventoryTransferInLine _numLine in _numLines)
                        {
                            i += 1;
                            _numLine.No = i;
                            _numLine.Save();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = InventoryTransferInLine " + ex.ToString());
            }
        }

        #endregion No

        #region Set

        private void SetTotalQty()
        {
            try
            {
                double _locInvLineTotal = 0;
                DateTime now = DateTime.Now;
                if (this.InventoryTransferIn != null)
                {
                    ItemUnitOfMeasure _locItemUOM = Session.FindObject<ItemUnitOfMeasure>
                                                        (new GroupOperator(GroupOperatorType.And,
                                                         new BinaryOperator("Item", this.Item),
                                                         new BinaryOperator("UOM", this.UOM),
                                                         new BinaryOperator("DefaultUOM", this.DUOM),
                                                         new BinaryOperator("Active", true)));
                    if (_locItemUOM != null)
                    {
                        if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                        {
                            _locInvLineTotal = this.Qty * _locItemUOM.DefaultConversion + this.DQty;
                        }
                        else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                        {
                            _locInvLineTotal = this.Qty / _locItemUOM.Conversion + this.DQty;
                        }
                        else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                        {
                            _locInvLineTotal = this.Qty + this.DQty;
                        }

                        this.TQty = _locInvLineTotal;
                    }
                    else
                    {
                        _locInvLineTotal = this.Qty + this.DQty;
                        this.TQty = _locInvLineTotal;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = InventoryTransferInLine " + ex.ToString());
            }
        }

        #endregion Set

    }
}