﻿using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;

using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.ConditionalAppearance;

namespace FullDrive.Module.BusinessObjects
{
    [DefaultClassOptions]
    [DefaultProperty("Code")]
    [NavigationItem("Inventory")]
    [RuleCombinationOfPropertiesIsUnique("BeginningInventoryRuleUnique", DefaultContexts.Save, "Code")]
    //[ImageName("BO_Contact")]
    //[DefaultProperty("DisplayMemberNameForLookupEditorsOfThisType")]
    //[DefaultListViewOptions(MasterDetailMode.ListViewOnly, false, NewItemRowPosition.None)]
    //[Persistent("DatabaseTableName")]
    // Specify more UI options using a declarative approach (https://documentation.devexpress.com/#eXpressAppFramework/CustomDocument112701).
    public class BeginningInventory : BaseObject
    { // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).

        private string _code;
        private Country _country;
        private City _city;
        private Company _company;
        private XPCollection<Workplace> _availableWorkplace;
        private Workplace _workplace;
        private XPCollection<Location> _availableLocation;
        private Location _location;
        private bool _openVehicle;
        private XPCollection<Vehicle> _availableVehicle;
        private Vehicle _vehicle;
        private double _qtyBegining;
        private double _qtyMinimal;
        private double _qtyAvailable;
        private double _qtySale;
        private StockType _stockType;
        private LocationType _locationType;
        private bool _active;
        private string _signCode;
        private string _userAccess;
        private GlobalFunction _globFunc;
        //AuditTrail
        private XPCollection<AuditDataItemPersistent> changeHistory;

        public BeginningInventory(Session session)
            : base(session)
        {
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if (!IsLoading)
            {
                _globFunc = new GlobalFunction();
                #region UserAccess
                _userAccess = SecuritySystem.CurrentUserName;
                UserAccess _locUserAccess = Session.FindObject<UserAccess>(new BinaryOperator("UserName", _userAccess));
                if (Session.IsNewObject(this))
                {
                    if (_locUserAccess != null)
                    {
                        if (_locUserAccess.Employee != null)
                        {
                            if (_locUserAccess.Employee.Company != null && _locUserAccess.Employee.Workplace != null)
                            {
                                this.Code = _globFunc.GetNumberingUnlockOptimisticRecordBasedOrganization(this.Session.DataLayer, ObjectList.BeginningInventory, _locUserAccess.Employee.Company, _locUserAccess.Employee.Workplace);
                            }
                            else
                            {
                                this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.BeginningInventory);
                            }
                            if (_locUserAccess.Employee.Company != null )
                            {
                                Company = _locUserAccess.Employee.Company;
                            }
                            if (_locUserAccess.Employee.Workplace != null)
                            {
                                Workplace = _locUserAccess.Employee.Workplace;
                            }
                            else
                            {
                                this.Workplace = _globFunc.GetDefaultWorkplace(this.Session, _locUserAccess);
                            }
                        }
                    }
                }
                #endregion UserAccess
            }
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).
        }

        #region Field

        [RuleRequiredField(DefaultContexts.Save)]
        [Appearance("BeginningInventoryCodeEnabled", Enabled = false)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue("Code", ref _code, value); }
        }

        public Country Country
        {
            get { return _country; }
            set { SetPropertyValue("Country", ref _country, value); }
        }

        public City City
        {
            get { return _city; }
            set { SetPropertyValue("City", ref _city, value); }
        }

        [ImmediatePostData()]
        [RuleRequiredField(DefaultContexts.Save)]
        [DataSourceCriteria("Active = true")]
        public Company Company
        {
            get { return _company; }
            set { SetPropertyValue("Company", ref _company, value); }
        }

        [Browsable(false)]
        public XPCollection<Workplace> AvailableWorkplace
        {
            get
            {
                if (!IsLoading)
                {
                    if (this.Company != null)
                    {
                        _userAccess = SecuritySystem.CurrentUserName;
                        UserAccess _locUserAccess = Session.FindObject<UserAccess>(new BinaryOperator("UserName", _userAccess));
                        if (_locUserAccess != null)
                        {
                            string _beginString = null;
                            string _endString = null;
                            string _fullString = null;
                            List<string> _stringWorkplace = new List<string>();

                            XPCollection<OrganizationSetupDetail> _locOrganizationSetupDetails = new XPCollection<OrganizationSetupDetail>
                                                                            (Session, new GroupOperator(GroupOperatorType.And,
                                                                            new BinaryOperator("Company", this.Company),
                                                                            new BinaryOperator("UserAccess", _locUserAccess),
                                                                            new BinaryOperator("OpenWorkplace", true),
                                                                            new BinaryOperator("Active", true)));

                            if (_locOrganizationSetupDetails != null && _locOrganizationSetupDetails.Count() > 0)
                            {
                                foreach (OrganizationSetupDetail _locOrganizationSetupDetail in _locOrganizationSetupDetails)
                                {
                                    if (_locOrganizationSetupDetail.Workplace != null)
                                    {
                                        if (_locOrganizationSetupDetail.Workplace.Code != null)
                                        {
                                            _stringWorkplace.Add(_locOrganizationSetupDetail.Workplace.Code);
                                        }
                                    }
                                }

                                #region Workplace
                                IEnumerable<string> _stringArrayWorkplaceDistinct = _stringWorkplace.Distinct();
                                string[] _stringArrayWorkplaceList = _stringArrayWorkplaceDistinct.ToArray();
                                if (_stringArrayWorkplaceList.Length == 1)
                                {
                                    for (int i = 0; i < _stringArrayWorkplaceList.Length; i++)
                                    {
                                        Workplace _locWorkplace = Session.FindObject<Workplace>(new BinaryOperator("Code", _stringArrayWorkplaceList[i]));
                                        if (_locWorkplace != null)
                                        {
                                            if (i == 0)
                                            {
                                                _beginString = "[Code]=='" + _locWorkplace.Code + "'";
                                            }
                                        }
                                    }
                                }
                                else if (_stringArrayWorkplaceList.Length > 1)
                                {
                                    for (int i = 0; i < _stringArrayWorkplaceList.Length; i++)
                                    {
                                        Workplace _locWorkplace = Session.FindObject<Workplace>(new BinaryOperator("Code", _stringArrayWorkplaceList[i]));
                                        if (_locWorkplace != null)
                                        {
                                            if (i == 0)
                                            {
                                                _beginString = "[Code]=='" + _locWorkplace.Code + "'";
                                            }
                                            else
                                            {
                                                _endString = _endString + " OR [Code]=='" + _locWorkplace.Code + "'";
                                            }
                                        }

                                    }
                                }
                                _fullString = _beginString + _endString;


                                if (_fullString != null)
                                {
                                    _availableWorkplace = new XPCollection<Workplace>(Session, CriteriaOperator.Parse(_fullString));
                                }

                                #endregion Workplace
                            }
                            else
                            {
                                OrganizationSetupDetail _locOrgSetDetail = Session.FindObject<OrganizationSetupDetail>
                                                                            (new GroupOperator(GroupOperatorType.And,
                                                                            new BinaryOperator("Company", this.Company),
                                                                            new BinaryOperator("UserAccess", _locUserAccess),
                                                                            new BinaryOperator("AllWorkplace", true),
                                                                            new BinaryOperator("Active", true)));
                                if (_locOrgSetDetail != null)
                                {
                                    _availableWorkplace = new XPCollection<Workplace>(Session,
                                                        new GroupOperator(GroupOperatorType.And,
                                                        new BinaryOperator("Company", this.Company),
                                                        new BinaryOperator("Active", true)));
                                }
                            }
                        }
                    }
                }

                return _availableWorkplace;

            }
        }

        [ImmediatePostData()]
        [RuleRequiredField(DefaultContexts.Save)]
        [DataSourceProperty("AvailableWorkplace")]
        public Workplace Workplace
        {
            get { return _workplace; }
            set { SetPropertyValue("Workplace", ref _workplace, value); }
        }

        [Browsable(false)]
        public XPCollection<Location> AvailableLocation
        {

            get
            {
                if (this.Workplace != null)
                {
                    _availableLocation = new XPCollection<Location>
                                        (Session, new GroupOperator(GroupOperatorType.And,
                                        new BinaryOperator("Workplace", this.Workplace),
                                        new BinaryOperator("Active", true)));


                }
                else
                {
                    _availableLocation = new XPCollection<Location>
                                        (Session, new GroupOperator(GroupOperatorType.And,
                                        new BinaryOperator("Active", true)));
                }

                return _availableLocation;

            }
        }

        [ImmediatePostData()]
        [DataSourceProperty("AvailableLocation", DataSourcePropertyIsNullMode.SelectAll)]
        public Location Location
        {
            get { return _location; }
            set
            {
                SetPropertyValue("Location", ref _location, value);
                if (this._location != null)
                {
                    if (this._location.Vehicle != null)
                    {
                        this.OpenVehicle = true;
                        this.Vehicle = this._location.Vehicle;
                        this.LocationType = LocationType.Vehicle;
                    }
                    else
                    {
                        this.OpenVehicle = false;
                        this.Vehicle = null;
                        this.LocationType = LocationType.None;
                    }
                }else
                {
                    this.OpenVehicle = false;
                    this.Vehicle = null;
                    this.LocationType = LocationType.None;
                }
            }
        }

        [Browsable(false)]
        [ImmediatePostData()]
        public bool OpenVehicle
        {
            get { return _openVehicle; }
            set { SetPropertyValue("OpenVehicle", ref _openVehicle, value); }
        }

        [Browsable(false)]
        public XPCollection<Vehicle> AvailableVehicle
        {

            get
            {
                if (this.Company != null && this.Workplace != null)
                {
                    XPCollection<Vehicle> _locVehicles = new XPCollection<Vehicle>(Session,
                                                        new GroupOperator(GroupOperatorType.And,
                                                        new BinaryOperator("Company", this.Company),
                                                        new BinaryOperator("Workplace", this.Workplace),
                                                        new BinaryOperator("Active", true)));

                    if (_locVehicles != null && _locVehicles.Count() > 0)
                    {
                        _availableVehicle = _locVehicles;
                    }
                }
                
                return _availableVehicle;

            }
        }

        [DataSourceProperty("AvailableVehicle")]
        [Appearance("BeginningInventoryVehicleClose", Criteria = "OpenVehicle = false", Enabled = false)]
        public Vehicle Vehicle
        {
            get { return _vehicle; }
            set { SetPropertyValue("Vehicle", ref _vehicle, value); }
        }

        public double QtyBegining
        {
            get { return _qtyBegining; }
            set { SetPropertyValue("QtyBegining", ref _qtyBegining, value); }
        }

        public double QtyMinimal
        {
            get { return _qtyMinimal; }
            set { SetPropertyValue("QtyMinimal", ref _qtyMinimal, value); }
        }

        public double QtyAvailable
        {
            get { return _qtyAvailable; }
            set { SetPropertyValue("QtyAvailable", ref _qtyAvailable, value); }
        }

        public double QtySale
        {
            get { return _qtySale; }
            set { SetPropertyValue("QtySale", ref _qtySale, value); }
        }

        public StockType StockType
        {
            get { return _stockType; }
            set { SetPropertyValue("StockType", ref _stockType, value); }
        }

        public LocationType LocationType
        {
            get { return _locationType; }
            set { SetPropertyValue("LocationType", ref _locationType, value); }
        }

        public bool Active
        {
            get { return _active; }
            set { SetPropertyValue("Active", ref _active, value); }
        }

        [Browsable(false)]
        public string SignCode
        {
            get { return _signCode; }
            set { SetPropertyValue("SignCode", ref _signCode, value); }
        }

        [Association("BeginningInventory-BeginningInventoryLines")]
        public XPCollection<BeginningInventoryLine> BeginningInventoryLines
        {
            get { return GetCollection<BeginningInventoryLine>("BeginningInventoryLines"); }
        }

        //AuditTrail
        [CollectionOperationSet(AllowAdd = false, AllowRemove = true)]
        public XPCollection<AuditDataItemPersistent> ChangeHistory
        {
            get
            {
                if (changeHistory == null)
                {
                    changeHistory = AuditedObjectWeakReference.GetAuditTrail(Session, this);
                }
                return changeHistory;
            }
        }

        #endregion Field


        //============================================= Code Only =============================================

        [Action(Caption = "ChangeQuantityAvailable", ConfirmationMessage = "Are you sure?", AutoCommit = true)]
        public void ChangeQuantityAvailable()
        {
            try
            {
                double _locQtyAvailable = 0;
                XPCollection<BeginningInventoryLine> _locBeginningInventoryLines = new XPCollection<BeginningInventoryLine>
                                                                (Session, new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("BeginningInventory", this)));
                if (_locBeginningInventoryLines != null && _locBeginningInventoryLines.Count() > 0)
                {
                    foreach (BeginningInventoryLine _locBeginningInventoryLine in _locBeginningInventoryLines)
                    {
                        _locQtyAvailable = _locQtyAvailable + _locBeginningInventoryLine.QtyAvailable;
                    }

                    if (_locQtyAvailable != this.QtyAvailable)
                    {
                        this.QtyAvailable = _locQtyAvailable;
                        this.Save();
                        this.Session.CommitTransaction();
                    }
                }

            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Business Object = BeginningInventory ", ex.ToString());
            }
        }
    }
}