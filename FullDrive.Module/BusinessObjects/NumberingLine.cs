﻿using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;

using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.ConditionalAppearance;

namespace FullDrive.Module.BusinessObjects
{
    [DefaultClassOptions]
    [DefaultProperty("Name")]
    [NavigationItem("Setup")]
    [OptimisticLocking(false)]
    //[ImageName("BO_Contact")]
    //[DefaultProperty("DisplayMemberNameForLookupEditorsOfThisType")]
    //[DefaultListViewOptions(MasterDetailMode.ListViewOnly, false, NewItemRowPosition.None)]
    //[Persistent("DatabaseTableName")]
    // Specify more UI options using a declarative approach (https://documentation.devexpress.com/#eXpressAppFramework/CustomDocument112701).
    public class NumberingLine : BaseObject
    { // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).
        private int _no;
        private Company _company;
        private Workplace _workplace;
        private string _name;
        private ObjectList _objectList;
        private string _prefix;
        private string _formatNumber;
        private string _suffix;
        private int _lastValue;
        private int _incrementValue;
        private int _minValue;
        private string _formatedValue;
        private DocumentType _documentType;
        private XPCollection<Employee> _availablePIC;
        private Employee _pic;
        private XPCollection<PriceGroup> _availablePriceGroup;
        private PriceGroup _priceGroup;
        private bool _active;
        private bool _default;
        private bool _selection;
        private bool _sign;
        private bool _date;
        private DateTime _startDate;
        private DateTime _endDate;
        private NumberingHeader _numberingHeader;
        //AuditTrail
        private XPCollection<AuditDataItemPersistent> changeHistory;

        public NumberingLine(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).
        }

        #region Field

        protected override void OnSaving()
        {
            base.OnSaving();
            if (!IsLoading)
            {
                if (this.Default == true)
                {
                    CheckDefaults();
                }
                UpdateNo();
            }
        }

        protected override void OnDeleting()
        {
            base.OnDeleting();
            if (!IsLoading)
            {
                RecoveryDeleteNo();
            }
        }

        [Appearance("NumberingLineNoEnabled", Enabled = false)]
        public int No
        {
            get { return _no; }
            set { SetPropertyValue("No", ref _no, value); }
        }

        public Company Company
        {
            get { return _company; }
            set { SetPropertyValue("Company", ref _company, value); }
        }

        public Workplace Workplace
        {
            get { return _workplace; }
            set { SetPropertyValue("Workplace", ref _workplace, value); }
        }

        public string Name
        {
            get { return _name; }
            set { SetPropertyValue("Name", ref _name, value); }
        }

        public ObjectList ObjectList
        {
            get { return _objectList; }
            set { SetPropertyValue("ObjectList", ref _objectList, value); }
        }

        public string Prefix
        {
            get { return _prefix; }
            set { SetPropertyValue("Prefix", ref _prefix, value); }
        }

        public string FormatNumber
        {
            get { return _formatNumber; }
            set { SetPropertyValue("FormatNumber", ref _formatNumber, value); }
        }

        public string Suffix
        {
            get { return _suffix; }
            set { SetPropertyValue("Suffix", ref _suffix, value); }
        }

        public int LastValue
        {
            get { return _lastValue; }
            set { SetPropertyValue("LastValue", ref _lastValue, value); }
        }

        public int IncrementValue
        {
            get { return _incrementValue; }
            set { SetPropertyValue("IncrementValue", ref _incrementValue, value); }
        }

        public int MinValue
        {
            get { return _minValue; }
            set { SetPropertyValue("MinValue", ref _minValue, value); }
        }

        public string FormatedValue
        {
            get
            {
                string strFormat;
                if (this.FormatNumber == null)
                {
                    strFormat = "{0:0}";
                }
                else
                {
                    strFormat = String.Format("{{0:{0}}}", this.FormatNumber);
                }
                _formatedValue = this.Prefix + string.Format(strFormat, this.LastValue) + this.Suffix;
                return _formatedValue;
            }
        }

        [ImmediatePostData()]
        public DocumentType DocumentType
        {
            get { return _documentType; }
            set {
                SetPropertyValue("DocumentType", ref _documentType, value);
                if(!IsLoading)
                {
                    if(this._documentType != null)
                    {
                        if(this._documentType.Code != null)
                        {
                            this.Prefix = this._documentType.Code + "-";
                        }
                        else
                        {
                            this.Prefix = null;
                        }
                    }
                    else
                    {
                        this.Prefix = null;
                    }
                }
            }
        }

        [Browsable(false)]
        public XPCollection<Employee> AvailablePIC
        {
            get
            {
                XPCollection<Employee> _locPICs = new XPCollection<Employee>
                                                            (Session, new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("SalesOrderCode", true),
                                                            new BinaryOperator("Active", true),
                                                            new GroupOperator(GroupOperatorType.Or,
                                                            new BinaryOperator("SalesRole", SalesRole.TakingOrder),
                                                            new BinaryOperator("SalesRole", SalesRole.Canvassing))));
                if(_locPICs != null && _locPICs.Count() > 0)
                {
                    _availablePIC = _locPICs;
                }
                else
                {
                    _availablePIC = new XPCollection<Employee>(Session, new GroupOperator(GroupOperatorType.And,
                                                                            new BinaryOperator("Active", true)));
                }

                return _availablePIC;

            }
        }

        [ImmediatePostData()]
        [DataSourceProperty("AvailablePIC", DataSourcePropertyIsNullMode.SelectAll)]
        public Employee PIC
        {
            get { return _pic; }
            set { SetPropertyValue("PIC", ref _pic, value); }
        }

        [Browsable(false)]
        public XPCollection<PriceGroup> AvailablePriceGroup
        {
            get
            {
                string _beginString = null;
                string _endString = null;
                string _fullString = null;


                if (this.PIC != null)
                {
                    List<string> _stringSCS = new List<string>();

                    XPQuery<SalesmanCustomerSetup> _salesmenCustomerSetupssQuery = new XPQuery<SalesmanCustomerSetup>(Session);

                    var _salesmanCustomerSetups = from scs in _salesmenCustomerSetupssQuery
                                                  where (scs.Salesman == this.PIC
                                                  )
                                                  group scs by scs.PriceGroup into g
                                                  select new { PriceGroup = g.Key };


                    if (_salesmanCustomerSetups != null && _salesmanCustomerSetups.Count() > 0)
                    {
                        foreach (var _salesmanCustomerSetup in _salesmanCustomerSetups)
                        {
                            if (_salesmanCustomerSetup.PriceGroup != null)
                            {
                                if (_salesmanCustomerSetup.PriceGroup.Code != null)
                                {
                                    _stringSCS.Add(_salesmanCustomerSetup.PriceGroup.Code);
                                }
                            }
                        }
                    }

                    IEnumerable<string> _stringArraySCSDistinct = _stringSCS.Distinct();
                    string[] _stringArraySCSList = _stringArraySCSDistinct.ToArray();
                    if (_stringArraySCSList.Length == 1)
                    {
                        for (int i = 0; i < _stringArraySCSList.Length; i++)
                        {
                            PriceGroup _locPG = Session.FindObject<PriceGroup>(new BinaryOperator("Code", _stringArraySCSList[i]));
                            if (_locPG != null)
                            {
                                if (i == 0)
                                {
                                    _beginString = "[Code]=='" + _locPG.Code + "'";
                                }
                            }
                        }
                    }
                    else if (_stringArraySCSList.Length > 1)
                    {
                        for (int i = 0; i < _stringArraySCSList.Length; i++)
                        {
                            PriceGroup _locPG = Session.FindObject<PriceGroup>(new BinaryOperator("Code", _stringArraySCSList[i]));
                            if (_locPG != null)
                            {
                                if (i == 0)
                                {
                                    _beginString = "[Code]=='" + _locPG.Code + "'";
                                }
                                else
                                {
                                    _endString = _endString + " OR [Code]=='" + _locPG.Code + "'";
                                }
                            }
                        }
                    }
                    _fullString = _beginString + _endString;

                    if (_fullString != null)
                    {
                        _availablePriceGroup = new XPCollection<PriceGroup>(Session, CriteriaOperator.Parse(_fullString));
                    }

                }
                else
                {
                    _availablePriceGroup = new XPCollection<PriceGroup>(Session, new GroupOperator(GroupOperatorType.And,
                                                                            new BinaryOperator("Active", true)));
                }

                return _availablePriceGroup;

            }
        }

        [DataSourceProperty("AvailablePriceGroup", DataSourcePropertyIsNullMode.SelectAll)]
        public PriceGroup PriceGroup
        {
            get { return _priceGroup; }
            set { SetPropertyValue("PriceGroup", ref _priceGroup, value); }
        }

        public bool Active
        {
            get { return _active; }
            set { SetPropertyValue("Active", ref _active, value); }
        }

        public bool Default
        {
            get { return _default; }
            set { SetPropertyValue("Default", ref _default, value); }
        }

        public bool Selection
        {
            get { return _selection; }
            set { SetPropertyValue("Selection", ref _selection, value); }
        }

        public bool Sign
        {
            get { return _sign; }
            set { SetPropertyValue("Sign", ref _sign, value); }
        }

        [ImmediatePostData()]
        public bool Date
        {
            get { return _date; }
            set { SetPropertyValue("Date", ref _date, value); }
        }

        [Appearance("NumberingLineStartDateClose1", Criteria = "Date = true", Enabled = true)]
        [Appearance("NumberingLineStartDateClose2", Criteria = "Date = false", Enabled = false)]
        public DateTime StartDate
        {
            get { return _startDate; }
            set { SetPropertyValue("StartDate", ref _startDate, value); }
        }

        [Appearance("NumberingLineEndDateClose1", Criteria = "Date = true", Enabled = true)]
        [Appearance("NumberingLineEndDateClose2", Criteria = "Date = false", Enabled = false)]
        public DateTime EndDate
        {
            get { return _endDate; }
            set { SetPropertyValue("EndDate", ref _endDate, value); }
        }

        [RuleRequiredField(DefaultContexts.Save)]
        [Association("NumberingHeader-NumberingLines")]
        public NumberingHeader NumberingHeader
        {
            get { return _numberingHeader; }
            set
            {
                NumberingHeader oldNumberingHeader = _numberingHeader;
                SetPropertyValue("NumberingHeader", ref _numberingHeader, value);
                if (!IsLoading && !IsSaving && !object.ReferenceEquals(oldNumberingHeader, _numberingHeader))
                {
                    oldNumberingHeader = oldNumberingHeader ?? _numberingHeader;
                    oldNumberingHeader.UpdateTotalLine(true);
                }
                if(!IsLoading)
                {
                    if(this._numberingHeader != null)
                    {
                        if (this._numberingHeader.Company != null)
                        {
                            this.Company = this._numberingHeader.Company;
                        }
                        if (this._numberingHeader.Workplace != null)
                        {
                            this.Workplace = this._numberingHeader.Workplace;
                        }
                    }else
                    {
                        this.Company = null;
                        this.Workplace = null;
                    }
                }
            }
        }

        //AuditTrail
        [CollectionOperationSet(AllowAdd = false, AllowRemove = true)]
        public XPCollection<AuditDataItemPersistent> ChangeHistory
        {
            get
            {
                if (changeHistory == null)
                {
                    changeHistory = AuditedObjectWeakReference.GetAuditTrail(Session, this);
                }
                return changeHistory;
            }
        }

        #endregion Field

        //=============================================== Code In Here ===============================================

        #region Numbering

        public void UpdateNo()
        {
            try
            {
                if (!IsLoading && Session.IsNewObject(this))
                {
                    if (this.NumberingHeader != null)
                    {
                        object _makRecord = Session.Evaluate<NumberingLine>(CriteriaOperator.Parse("Max(No)"), CriteriaOperator.Parse("NumberingHeader=?", this.NumberingHeader));
                        this.No = Convert.ToInt32(_makRecord) + 1;
                        this.Save();
                        RecoveryUpdateNo();
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = NumberingLine " + ex.ToString());
            }
        }

        public void RecoveryUpdateNo()
        {
            try
            {
                if (this.NumberingHeader != null)
                {
                    NumberingHeader _numHeader = Session.FindObject<NumberingHeader>
                                                (new BinaryOperator("Code", this.NumberingHeader.Code));

                    XPCollection<NumberingLine> _numLines = new XPCollection<NumberingLine>
                                                (Session, new BinaryOperator("NumberingHeader", _numHeader),
                                                new SortProperty("No", DevExpress.Xpo.DB.SortingDirection.Ascending));
                    if (_numLines != null)
                    {
                        int i1 = 0;
                        foreach (NumberingLine _numLine in _numLines)
                        {
                            i1 += 1;
                            _numLine.No = i1;
                            _numLine.Save();
                        }
                        i1 = 1;
                        Session.CommitTransaction();
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = NumberingLine " + ex.ToString());
            }
        }

        public void RecoveryDeleteNo()
        {
            try
            {
                if (this.NumberingHeader != null)
                {
                    NumberingHeader _numHeader = Session.FindObject<NumberingHeader>
                                                (new BinaryOperator("Code", this.NumberingHeader.Code));

                    XPCollection<NumberingLine> _numLines = new XPCollection<NumberingLine>
                                                (Session, new GroupOperator(GroupOperatorType.And,
                                                 new BinaryOperator("This", this, BinaryOperatorType.NotEqual),
                                                 new BinaryOperator("NumberingHeader", _numHeader)),
                                                 new SortProperty("No", DevExpress.Xpo.DB.SortingDirection.Ascending));
                    if (_numLines != null)
                    {
                        int i = 0;
                        foreach (NumberingLine _numLine in _numLines)
                        {
                            i += 1;
                            _numLine.No = i;
                            _numLine.Save();
                        }
                        Session.CommitTransaction();
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = NumberingLine " + ex.ToString());
            }
        }

        #endregion Numbering

        private void CheckDefaults()
        {
            try
            {
                XPCollection<NumberingLine> _numSetups = new XPCollection<NumberingLine>(Session,
                                                            new BinaryOperator("This", this, BinaryOperatorType.NotEqual));
                if (_numSetups != null && _numSetups.Count() > 0)
                {
                    foreach (NumberingLine _numSetup in _numSetups)
                    {
                        if(this.Company != null)
                        {
                            if (_numSetup.Company == this.Company)
                            {
                                if(_numSetup.Workplace != null)
                                {
                                    if (_numSetup.Workplace == this.Workplace && this.Default == true && _numSetup.ObjectList == this.ObjectList)
                                    {
                                        _numSetup.Default = false;
                                        _numSetup.Save();
                                    }
                                } 
                            }
                        }
                        else
                        {
                            if(_numSetup.ObjectList == this.ObjectList)
                            {
                                _numSetup.Default = false;
                                _numSetup.Save();
                            }
                        } 
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = NumberingLine ", ex.ToString());
            }
        }

    }
}