﻿using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;

using DevExpress.ExpressApp.ConditionalAppearance;
using DevExpress.ExpressApp.SystemModule;
using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.Editors;

namespace FullDrive.Module.BusinessObjects
{
    [DefaultClassOptions]
    [DefaultProperty("Code")]
    [NavigationItem("Finance & Accounting")]
    [RuleCombinationOfPropertiesIsUnique("AccountReclassJournalMonitoringRuleUnique", DefaultContexts.Save, "Code")]
    //[ImageName("BO_Contact")]
    //[DefaultProperty("DisplayMemberNameForLookupEditorsOfThisType")]
    //[DefaultListViewOptions(MasterDetailMode.ListViewOnly, false, NewItemRowPosition.None)]
    //[Persistent("DatabaseTableName")]
    // Specify more UI options using a declarative approach (https://documentation.devexpress.com/#eXpressAppFramework/CustomDocument112701).
    public class AccountReclassJournalMonitoring : BaseObject
    { // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).
        private string _code;
        private AccountReclassJournal _accountReclassJournal;
        private AccountReclassJournalLine _accountReclassJournalLine;
        #region InitialOrganization
        private Company _company;
        private Workplace _workplace;
        private Division _division;
        private Department _department;
        private Section _section;
        private Employee _employee;
        #endregion InitialOrganization
        private PostingType _postingType;
        private PostingMethod _postingMethod;
        private PostingMethodType _postingMethodType;
        private string _description;
        private AccountGroup _accountGroup;
        private XPCollection<ChartOfAccount> _availableChartOfAccount;
        private ChartOfAccount _account;
        private double _debit;
        private double _credit;
        private DateTime _docDate;
        private int _journalMonth;
        private int _journalYear;
        private Status _status;
        private DateTime _statusDate;
        private string _localUserAccess;
        private GlobalFunction _globFunc;
        //AuditTrail
        private XPCollection<AuditDataItemPersistent> changeHistory;

        public AccountReclassJournalMonitoring(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if(!IsLoading)
            {
                if(Session.IsNewObject(this))
                {
                    _globFunc = new GlobalFunction();
                    DateTime now = DateTime.Now;
                    #region UserAccess
                    _localUserAccess = SecuritySystem.CurrentUserName;
                    UserAccess _locUserAccess = Session.FindObject<UserAccess>(new BinaryOperator("UserName", _localUserAccess));
                    if (_locUserAccess != null)
                    {
                        if (_locUserAccess.Employee != null)
                        {
                            if (_locUserAccess.Employee.Company != null && _locUserAccess.Employee.Workplace != null)
                            {
                                this.Code = _globFunc.GetNumberingUnlockOptimisticRecordBasedOrganization(this.Session.DataLayer, ObjectList.AccountReclassJournalMonitoring, _locUserAccess.Employee.Company, _locUserAccess.Employee.Workplace);
                            }
                            else
                            {
                                this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.AccountReclassJournalMonitoring);
                            }
                        }
                    }
                    #endregion UserAccess
                    this.Status = Status.Open;
                    this.StatusDate = now;
                }               
            }
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).
        }

        #region Field

        [RuleRequiredField(DefaultContexts.Save)]
        [Appearance("AccountReclassJournalMonitoringCodeClose", Enabled = false)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue("Code", ref _code, value); }
        }

        [Appearance("AccountReclassJournalMonitoringAccountReclassJournalClose", Enabled = false)]
        public AccountReclassJournal AccountReclassJournal
        {
            get { return _accountReclassJournal; }
            set { SetPropertyValue("AccountReclassJournal", ref _accountReclassJournal, value); }
        }

        [Appearance("AccountReclassJournalMonitoringAccountReclassJournalLineClose", Enabled = false)]
        public AccountReclassJournalLine AccountReclassJournalLine
        {
            get { return _accountReclassJournalLine; }
            set { SetPropertyValue("AccountReclassJournalLine", ref _accountReclassJournalLine, value); }
        }

        #region Organization

        [Appearance("AccountReclassJournalMonitoringCompanyDefaultEnabled", Enabled = false)]
        public Company Company
        {
            get { return _company; }
            set { SetPropertyValue("Company", ref _company, value); }
        }

        [Appearance("AccountReclassJournalMonitoringWorkplaceEnabled", Enabled = false)]
        public Workplace Workplace
        {
            get { return _workplace; }
            set { SetPropertyValue("Workplace", ref _workplace, value); }
        }

        [Appearance("AccountReclassJournalMonitoringDivisionEnabled", Enabled = false)]
        public Division Division
        {
            get { return _division; }
            set { SetPropertyValue("Division", ref _division, value); }
        }

        [Appearance("AccountReclassJournalMonitoringDepartmentEnabled", Enabled = false)]
        public Department Department
        {
            get { return _department; }
            set { SetPropertyValue("Department", ref _department, value); }
        }

        [Appearance("AccountReclassJournalMonitoringSectionEnabled", Enabled = false)]
        public Section Section
        {
            get { return _section; }
            set { SetPropertyValue("Section", ref _section, value); }
        }

        [Appearance("AccountReclassJournalMonitoringEmployeeEnabled", Enabled = false)]
        public Employee Employee
        {
            get { return _employee; }
            set { SetPropertyValue("Employee", ref _employee, value); }
        }

        #endregion Organization

        [Appearance("AccountReclassJournalLinePostingTypeClose", Enabled = false)]
        public PostingType PostingType
        {
            get { return _postingType; }
            set { SetPropertyValue("PostingType", ref _postingType, value); }
        }

        [Appearance("AccountReclassJournalLinePostingMethodClose", Enabled = false)]
        public PostingMethod PostingMethod
        {
            get { return _postingMethod; }
            set { SetPropertyValue("PostingMethod", ref _postingMethod, value); }
        }

        [Appearance("AccountReclassJournalLinePostingMethodTypeClose", Enabled = false)]
        public PostingMethodType PostingMethodType
        {
            get { return _postingMethodType; }
            set { SetPropertyValue("PostingMethodType", ref _postingMethodType, value); }
        }

        [Size(512)]
        [Appearance("AccountReclassJournalLineDescriptionClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public string Description
        {
            get { return _description; }
            set { SetPropertyValue("Description", ref _description, value); }
        }

        [ImmediatePostData]
        [DataSourceCriteria("Company = '@This.Company' And Active = true")]
        [Appearance("AccountReclassJournalLineAccountGroupClose", Enabled = false)]
        public AccountGroup AccountGroup
        {
            get { return _accountGroup; }
            set { SetPropertyValue("AccountGroup", ref _accountGroup, value); }
        }

        [Browsable(false)]
        public XPCollection<ChartOfAccount> AvailableChartOfAccount
        {
            get
            {
                if (this.Company != null && this.Workplace != null && AccountGroup != null)
                {
                    XPCollection<ChartOfAccount> _availableChtOfAcct = new XPCollection<ChartOfAccount>
                                                               (Session, new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("Company", this.Company),
                                                                new BinaryOperator("Workplace", this.Workplace),
                                                                new BinaryOperator("AccountGroup", this.AccountGroup),
                                                                new BinaryOperator("Active", true)));
                    if (_availableChtOfAcct != null && _availableChtOfAcct.Count() > 0)
                    {
                        _availableChartOfAccount = _availableChtOfAcct;
                    }
                }
                else
                {
                    if (this.Company != null && this.Workplace != null)
                    {
                        XPCollection<ChartOfAccount> _availableChtOfAcct = new XPCollection<ChartOfAccount>
                                                               (Session, new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("Company", this.Company),
                                                                new BinaryOperator("Workplace", this.Workplace),
                                                                new BinaryOperator("Active", true)));
                        if (_availableChtOfAcct != null && _availableChtOfAcct.Count() > 0)
                        {
                            _availableChartOfAccount = _availableChtOfAcct;
                        }
                    }
                    else
                    {
                        _availableChartOfAccount = new XPCollection<ChartOfAccount>(Session);
                    }
                }
                return _availableChartOfAccount;

            }
        }

        [ImmediatePostData()]
        [DataSourceProperty("AvailableChartOfAccount")]
        [Appearance("AccountReclassJournalMonitoringAccountClose", Enabled = false)]
        public ChartOfAccount Account
        {
            get { return _account; }
            set { SetPropertyValue("Account", ref _account, value); }
        }

        [Appearance("AccountReclassJournalMonitoringTotAmountDebitClose", Enabled = false)]
        public double Debit
        {
            get { return _debit; }
            set { SetPropertyValue("Debit", ref _debit, value); }
        }

        [Appearance("AccountReclassJournalMonitoringTotAmountCreditClose", Enabled = false)]
        public double Credit
        {
            get { return _credit; }
            set { SetPropertyValue("Credit", ref _credit, value); }
        }

        [ImmediatePostData()]
        [ModelDefault("DisplayFormat", "{0:dd/MM/yyyy}")]
        [ModelDefault("EditMask", "dd/MM/yyyy")]
        [Appearance("AccountReclassJournalMonitoringDocDateClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public DateTime DocDate
        {
            get { return _docDate; }
            set
            {
                SetPropertyValue("DocDate", ref _docDate, value);
                if (!IsLoading)
                {
                    SetAccountingPeriodic();
                }
            }
        }

        [ImmediatePostData()]
        [Appearance("AccountReclassJournalMonitoringJournalMonthClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public int JournalMonth
        {
            get { return _journalMonth; }
            set
            {
                SetPropertyValue("JournalMonth", ref _journalMonth, value);
                if (!IsLoading)
                {
                    if (this._journalMonth < 0)
                    {
                        this._journalMonth = 0;
                    }
                    if (this._journalMonth > 12)
                    {
                        this._journalMonth = 12;
                    }
                }
            }
        }

        [ImmediatePostData()]
        [Appearance("AccountReclassJournalMonitoringJournalYearClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public int JournalYear
        {
            get { return _journalYear; }
            set
            {
                SetPropertyValue("JournalYear", ref _journalYear, value);
                if (!IsLoading)
                {
                    if (this._journalYear < 0)
                    {
                        this._journalYear = 0;
                    }
                    if (this._journalYear > 9999)
                    {
                        this._journalYear = 9999;
                    }
                }
            }
        }

        [Appearance("AccountReclassJournalMonitoringStatusEnabled", Enabled = false)]
        public Status Status
        {
            get { return _status; }
            set { SetPropertyValue("Status", ref _status, value); }
        }

        [ModelDefault("DisplayFormat", "{0:dd/MM/yyyy}")]
        [ModelDefault("EditMask", "dd/MM/yyyy")]
        [Appearance("AccountReclassJournalMonitoringStatusDateEnabled", Enabled = false)]
        public DateTime StatusDate
        {
            get { return _statusDate; }
            set { SetPropertyValue("StatusDate", ref _statusDate, value); }
        }

        //AuditTrail
        [CollectionOperationSet(AllowAdd = false, AllowRemove = true)]
        public XPCollection<AuditDataItemPersistent> ChangeHistory
        {
            get
            {
                if (changeHistory == null)
                {
                    changeHistory = AuditedObjectWeakReference.GetAuditTrail(Session, this);
                }
                return changeHistory;
            }
        }

        #endregion Field

        //========================================== Code Only ===============================================

        private void SetAccountingPeriodic()
        {
            try
            {
                DateTime now = DateTime.Now;
                if (this.Company != null && this.Workplace != null)
                {
                    AccountingPeriodic _locAcctPeriodic = Session.FindObject<AccountingPeriodic>
                                                        (new GroupOperator(GroupOperatorType.And,
                                                        new BinaryOperator("Company", this.Company),
                                                        new BinaryOperator("Workplace", this.Workplace),
                                                        new BinaryOperator("Active", true)));
                    if (_locAcctPeriodic != null)
                    {
                        this.JournalMonth = _locAcctPeriodic.Month;
                        this.JournalYear = _locAcctPeriodic.Year;
                    }
                    else
                    {
                        this.JournalMonth = 0;
                        this.JournalYear = 0;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Business Object = AccountReclassJournalMonitoring", ex.ToString());
            }
        }
    }
}