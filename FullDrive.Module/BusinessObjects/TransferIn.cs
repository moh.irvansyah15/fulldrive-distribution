﻿using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;

using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.ConditionalAppearance;
using DevExpress.ExpressApp.Editors;

namespace FullDrive.Module.BusinessObjects
{
    [DefaultClassOptions]
    [DefaultProperty("DocNo")]
    [NavigationItem("Inventory")]
    [RuleCombinationOfPropertiesIsUnique("TransferInRuleUnique", DefaultContexts.Save, "Code")]
    //[ImageName("BO_Contact")]
    //[DefaultProperty("DisplayMemberNameForLookupEditorsOfThisType")]
    //[DefaultListViewOptions(MasterDetailMode.ListViewOnly, false, NewItemRowPosition.None)]
    //[Persistent("DatabaseTableName")]
    // Specify more UI options using a declarative approach (https://documentation.devexpress.com/#eXpressAppFramework/CustomDocument112701).
    public class TransferIn : BaseObject
    { // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).
        private bool _activationPosting;
        private string _code;
        #region InitialOrganization
        private Company _company;
        private Workplace _workplace;
        private Division _division;
        private Department _department;
        private Section _section;
        private Employee _employee;
        #endregion InitialOrganization
        private DirectionType _transferType;
        private InventoryMovingType _inventoryMovingType;
        private ObjectList _objectList;
        XPCollection<DocumentType> _availableDocumentType;
        private DocumentType _documentType;
        private string _docNo;
        private DocumentRule _documentRule;
        #region From
        private LocationType _locationTypeFrom;
        private StockType _stockTypeFrom;
        private XPCollection<Workplace> _availableWorkplaceFrom;
        private Workplace _workplaceFrom;
        private XPCollection<Location> _availableLocationFrom;
        private Location _locationFrom;
        private BeginningInventory _begInvFrom;
        #endregion From
        #region To
        private LocationType _locationTypeTo;
        private StockType _stockTypeTo;
        private XPCollection<Workplace> _availableWorkplaceTo;
        private Workplace _workplaceTo;
        private XPCollection<Location> _availableLocationTo;
        private Location _locationTo;
        private BeginningInventory _begInvTo;
        #endregion To
        private bool _openPIC;
        private DateTime _pickingDate;
        private XPCollection<Picking> _availablePicking;
        private Picking _picking;
        private XPCollection<Employee> _availablePIC;
        private Employee _pic;
        private SalesRole _salesRole;
        private XPCollection<Vehicle> _availableVehicle;
        private Vehicle _vehicle;
        private DateTime _routeDate;
        private int _postedCount;
        private DateTime _docDate;
        private int _journalMonth;
        private int _journalYear;
        private Status _status;
        private DateTime _statusDate;
        private string _userAccess;
        private GlobalFunction _globFunc;
        //AuditTrail
        private XPCollection<AuditDataItemPersistent> changeHistory;

        public TransferIn(Session session)
            : base(session)
        {
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if (!IsLoading)
            {
                if (Session.IsNewObject(this))
                {
                    _globFunc = new GlobalFunction();
                    DateTime now = DateTime.Now;
                    #region UserAccess
                    _userAccess = SecuritySystem.CurrentUserName;
                    UserAccess _locUserAccess = Session.FindObject<UserAccess>(new BinaryOperator("UserName", _userAccess));
                    if (_locUserAccess != null)
                    {
                        if (_locUserAccess.Employee != null)
                        {
                            if (_locUserAccess.Employee.Company != null && _locUserAccess.Employee.Workplace != null)
                            {
                                this.Code = _globFunc.GetNumberingUnlockOptimisticRecordBasedOrganization(this.Session.DataLayer, ObjectList.TransferIn, _locUserAccess.Employee.Company, _locUserAccess.Employee.Workplace);

                                #region DocumentTypeSetup
                                EmployeeDocumentTypeSetup _locEmpDocTypeSetup = Session.FindObject<EmployeeDocumentTypeSetup>
                                                                                (new GroupOperator(GroupOperatorType.And,
                                                                                new BinaryOperator("Employee", _locUserAccess.Employee),
                                                                                new BinaryOperator("ObjectList", ObjectList.TransferIn),
                                                                                new BinaryOperator("TransferType", DirectionType.None, BinaryOperatorType.NotEqual),
                                                                                new BinaryOperator("InventoryMovingType", InventoryMovingType.None, BinaryOperatorType.NotEqual),
                                                                                new BinaryOperator("Active", true),
                                                                                new BinaryOperator("Default", true)));


                                if (_locEmpDocTypeSetup != null)
                                {
                                    this.TransferType = _locEmpDocTypeSetup.TransferType;
                                    this.InventoryMovingType = _locEmpDocTypeSetup.InventoryMovingType;
                                }
                                else
                                {
                                    this.TransferType = DirectionType.Internal;
                                    this.InventoryMovingType = InventoryMovingType.TransferIn;
                                }
                                #endregion DocumentTypeSetup
                            }
                            else
                            {
                                this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.TransferIn);
                            }
                            if (_locUserAccess.Employee.Company != null)
                            {
                                Company = _locUserAccess.Employee.Company;
                            }
                            if (_locUserAccess.Employee.Workplace != null)
                            {
                                this.Workplace = _locUserAccess.Employee.Workplace;
                                this.WorkplaceTo = _locUserAccess.Employee.Workplace;
                            }
                            else
                            {
                                this.Workplace = _globFunc.GetDefaultWorkplace(this.Session, _locUserAccess);
                                this.WorkplaceTo = _globFunc.GetDefaultWorkplace(this.Session, _locUserAccess);
                            }
                            if (_locUserAccess.Employee.Division != null)
                            {
                                Division = _locUserAccess.Employee.Division;
                            }
                            if (_locUserAccess.Employee.Department != null)
                            {
                                Department = _locUserAccess.Employee.Department;
                            }
                            if (_locUserAccess.Employee.Section != null)
                            {
                                Section = _locUserAccess.Employee.Section;
                            }
                            this.Employee = _locUserAccess.Employee;
                        }
                    }
                    #endregion UserAccess
                    this.ObjectList = ObjectList.TransferIn;
                    this.Status = Status.Open;
                    this.StatusDate = now;
                    this.PickingDate = now;
                    this.DocumentRule = DocumentRule.None;
                }                
            }
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).
        }

        #region Field

        [Browsable(false)]
        public bool ActivationPosting
        {
            get { return _activationPosting; }
            set { SetPropertyValue("ActivationPosting", ref _activationPosting, value); }
        }

        [RuleRequiredField(DefaultContexts.Save)]
        [Appearance("StockTransferCodeEnabled", Enabled = false)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue("Code", ref _code, value); }
        }

        #region Organization

        [Appearance("TransferInCompanyClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Company Company
        {
            get { return _company; }
            set { SetPropertyValue("Company", ref _company, value); }
        }

        [Appearance("TransferInWorkplaceClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Workplace Workplace
        {
            get { return _workplace; }
            set { SetPropertyValue("Workplace", ref _workplace, value); }
        }

        [Browsable(false)]
        public Division Division
        {
            get { return _division; }
            set { SetPropertyValue("Division", ref _division, value); }
        }

        [Browsable(false)]
        public Department Department
        {
            get { return _department; }
            set { SetPropertyValue("Department", ref _department, value); }
        }

        [Browsable(false)]
        public Section Section
        {
            get { return _section; }
            set { SetPropertyValue("Section", ref _section, value); }
        }

        [Browsable(false)]
        public Employee Employee
        {
            get { return _employee; }
            set { SetPropertyValue("Employee", ref _employee, value); }
        }

        #endregion Organization

        [Appearance("TransferInTransferTypeClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [ImmediatePostData()]
        public DirectionType TransferType
        {
            get { return _transferType; }
            set { SetPropertyValue("TransferType", ref _transferType, value); }
        }

        [Appearance("TransferInInventoryMovingTypeClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [ImmediatePostData()]
        public InventoryMovingType InventoryMovingType
        {
            get { return _inventoryMovingType; }
            set { SetPropertyValue("InventoryMovingType", ref _inventoryMovingType, value); }
        }

        [Browsable(false)]
        public ObjectList ObjectList
        {
            get { return _objectList; }
            set { SetPropertyValue("ObjectList", ref _objectList, value); }
        }

        [Browsable(false)]
        public XPCollection<DocumentType> AvailableDocumentType
        {
            get
            {
                if(!IsLoading)
                {
                    string _beginString = null;
                    string _endString = null;
                    string _fullString = null;

                    if (this.Company != null && this.Workplace != null && this.Employee != null)
                    {
                        if (ObjectList != ObjectList.None)
                        {
                            List<string> _stringEDTS = new List<string>();

                            XPCollection<EmployeeDocumentTypeSetup> _locEmployeeDocumentTypeSetups = new XPCollection<EmployeeDocumentTypeSetup>
                                                                                (Session, new GroupOperator(GroupOperatorType.And,
                                                                                new BinaryOperator("Company", this.Company),
                                                                                new BinaryOperator("Workplace", this.Workplace),
                                                                                new BinaryOperator("Employee", this.Employee),
                                                                                new BinaryOperator("ObjectList", this.ObjectList),
                                                                                new BinaryOperator("InventoryMovingType", this.InventoryMovingType),
                                                                                new BinaryOperator("TransferType", this.TransferType),
                                                                                new BinaryOperator("Active", true)));

                            if (_locEmployeeDocumentTypeSetups != null && _locEmployeeDocumentTypeSetups.Count() > 0)
                            {
                                foreach (EmployeeDocumentTypeSetup _locEmployeeDocumentTypeSetup in _locEmployeeDocumentTypeSetups)
                                {
                                    if (_locEmployeeDocumentTypeSetup.DocumentType != null)
                                    {
                                        if (_locEmployeeDocumentTypeSetup.DocumentType.Code != null)
                                        {
                                            _stringEDTS.Add(_locEmployeeDocumentTypeSetup.DocumentType.Code);
                                        }
                                    }
                                }
                            }

                            IEnumerable<string> _stringArrayEDTSDistinct = _stringEDTS.Distinct();
                            string[] _stringArrayEDTSList = _stringArrayEDTSDistinct.ToArray();
                            if (_stringArrayEDTSList.Length == 1)
                            {
                                for (int i = 0; i < _stringArrayEDTSList.Length; i++)
                                {
                                    DocumentType _locDT = Session.FindObject<DocumentType>(new BinaryOperator("Code", _stringArrayEDTSList[i]));
                                    if (_locDT != null)
                                    {
                                        if (i == 0)
                                        {
                                            _beginString = "[Code]=='" + _locDT.Code + "'";
                                        }
                                    }
                                }
                            }
                            else if (_stringArrayEDTSList.Length > 1)
                            {
                                for (int i = 0; i < _stringArrayEDTSList.Length; i++)
                                {
                                    DocumentType _locDT = Session.FindObject<DocumentType>(new BinaryOperator("Code", _stringArrayEDTSList[i]));
                                    if (_locDT != null)
                                    {
                                        if (i == 0)
                                        {
                                            _beginString = "[Code]=='" + _locDT.Code + "'";
                                        }
                                        else
                                        {
                                            _endString = _endString + " OR [Code]=='" + _locDT.Code + "'";
                                        }
                                    }
                                }
                            }
                            _fullString = _beginString + _endString;

                            if (_fullString != null)
                            {
                                _availableDocumentType = new XPCollection<DocumentType>(Session, CriteriaOperator.Parse(_fullString));
                            }

                        }

                    }
                }
                
                return _availableDocumentType;

            }
        }

        [Appearance("TransferInDocumentTypeClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [ImmediatePostData()]
        [DataSourceProperty("AvailableDocumentType", DataSourcePropertyIsNullMode.SelectNothing)]
        public DocumentType DocumentType
        {
            get { return _documentType; }
            set
            {
                SetPropertyValue("DocumentType", ref _documentType, value);
                if (!IsLoading)
                {
                    if (_documentType != null)
                    {
                        _globFunc = new GlobalFunction();
                        this.DocNo = _globFunc.GetDocumentNumberingUnlockOptimisticRecord(this.Session.DataLayer, this._documentType);
                        #region To
                        if (this._documentType.LocationTypeTo != LocationType.None)
                        {
                            this.LocationTypeTo = this._documentType.LocationTypeTo;
                        }
                        else
                        {
                            this.LocationTypeTo = LocationType.None;
                        }

                        if (this._documentType.StockTypeTo != StockType.None)
                        {
                            this.StockTypeTo = this._documentType.StockTypeTo;
                        }
                        else
                        {
                            this.StockTypeTo = StockType.None;
                        }
                        #endregion To
                        #region From
                        if (this._documentType.LocationTypeFrom != LocationType.None)
                        {
                            this.LocationTypeFrom = this._documentType.LocationTypeFrom;
                        }
                        else
                        {
                            this.LocationTypeFrom = LocationType.None;
                        }

                        if (this._documentType.StockTypeFrom != StockType.None)
                        {
                            this.StockTypeFrom = this._documentType.StockTypeFrom;
                        }
                        else
                        {
                            this.StockTypeFrom = StockType.None;
                        }
                        #endregion From
                        
                        if (this._documentType.SameWorkplace == true)
                        {
                            this.WorkplaceFrom = this.WorkplaceTo;
                        }
                        else
                        {
                            this.WorkplaceFrom = null;
                        }

                        if(this._documentType.DocumentRule == DocumentRule.None)
                        {
                            this.DocumentRule = this._documentType.DocumentRule;
                            if (this._documentType.DocumentRule == DocumentRule.Customer)
                            {
                                this.OpenPIC = true;
                            }
                        }
                        else
                        {
                            this.OpenPIC = false;
                            this.DocumentRule = DocumentRule.None;
                        }
                    }
                    else
                    {
                        this.LocationTypeTo = LocationType.None;
                        this.StockTypeTo = StockType.None;
                        this.LocationTypeFrom = LocationType.None;
                        this.StockTypeFrom = StockType.None;
                        this.DocNo = null;
                        this.OpenPIC = false;
                        this.DocumentRule = DocumentRule.None;
                    }
                }
            }
        }

        [RuleRequiredField(DefaultContexts.Save)]
        [Appearance("TransferInDocNoClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public string DocNo
        {
            get { return _docNo; }
            set { SetPropertyValue("DocNo", ref _docNo, value); }
        }

        [Browsable(false)]
        [Appearance("TransferOutDocumentRuleClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public DocumentRule DocumentRule
        {
            get { return _documentRule; }
            set { SetPropertyValue("DocumentRule", ref _documentRule, value); }
        }

        #region From

        [ImmediatePostData()]
        [Appearance("TransferInLocationTypeFromClose", Enabled = false)]
        public LocationType LocationTypeFrom
        {
            get { return _locationTypeFrom; }
            set { SetPropertyValue("LocationTypeFrom", ref _locationTypeFrom, value); }
        }

        [ImmediatePostData()]
        [Appearance("TransferInStockTypeFromClose", Enabled = false)]
        public StockType StockTypeFrom
        {
            get { return _stockTypeFrom; }
            set { SetPropertyValue("StockTypeFrom", ref _stockTypeFrom, value); }
        }

        [Browsable(false)]
        public XPCollection<Workplace> AvailableWorkplaceFrom
        {
            get
            {
                if(!IsLoading)
                {
                    string _beginString = null;
                    string _endString = null;
                    string _fullString = null;

                    if (this.Company != null)
                    {
                        _userAccess = SecuritySystem.CurrentUserName;
                        UserAccess _locUserAccess = Session.FindObject<UserAccess>(new BinaryOperator("UserName", _userAccess));
                        if (_locUserAccess != null)
                        {
                            List<string> _stringOSD = new List<string>();

                            XPCollection<OrganizationSetupDetail> _locOrganizationSetupDetails = new XPCollection<OrganizationSetupDetail>
                                                                                (Session, new GroupOperator(GroupOperatorType.And,
                                                                                new BinaryOperator("Company", this.Company),
                                                                                new BinaryOperator("UserAccess", _locUserAccess),
                                                                                new BinaryOperator("AllWorkplace", false),
                                                                                new BinaryOperator("OpenWorkplace", true),
                                                                                new BinaryOperator("Active", true)));

                            if (_locOrganizationSetupDetails != null && _locOrganizationSetupDetails.Count() > 0)
                            {
                                foreach (OrganizationSetupDetail _locOrganizationSetupDetail in _locOrganizationSetupDetails)
                                {
                                    if (_locOrganizationSetupDetail.Workplace != null)
                                    {
                                        if (_locOrganizationSetupDetail.Workplace.Code != null)
                                        {
                                            _stringOSD.Add(_locOrganizationSetupDetail.Workplace.Code);
                                        }
                                    }
                                }
                            }

                            IEnumerable<string> _stringArrayOSDDistinct = _stringOSD.Distinct();
                            string[] _stringArrayOSDList = _stringArrayOSDDistinct.ToArray();
                            if (_stringArrayOSDList.Length == 1)
                            {
                                for (int i = 0; i < _stringArrayOSDList.Length; i++)
                                {
                                    Workplace _locW = Session.FindObject<Workplace>(new BinaryOperator("Code", _stringArrayOSDList[i]));
                                    if (_locW != null)
                                    {
                                        if (i == 0)
                                        {
                                            _beginString = "[Code]=='" + _locW.Code + "'";
                                        }
                                    }
                                }
                            }
                            else if (_stringArrayOSDList.Length > 1)
                            {
                                for (int i = 0; i < _stringArrayOSDList.Length; i++)
                                {
                                    Workplace _locW = Session.FindObject<Workplace>(new BinaryOperator("Code", _stringArrayOSDList[i]));
                                    if (_locW != null)
                                    {
                                        if (i == 0)
                                        {
                                            _beginString = "[Code]=='" + _locW.Code + "'";
                                        }
                                        else
                                        {
                                            _endString = _endString + " OR [Code]=='" + _locW.Code + "'";
                                        }
                                    }
                                }
                            }
                            _fullString = _beginString + _endString;

                            if (_fullString != null)
                            {
                                _availableWorkplaceFrom = new XPCollection<Workplace>(Session, CriteriaOperator.Parse(_fullString));
                            }
                            else
                            {
                                OrganizationSetupDetail _locWhsSetupDetail = Session.FindObject<OrganizationSetupDetail>
                                                                        (new GroupOperator(GroupOperatorType.And,
                                                                        new BinaryOperator("Company", this.Company),
                                                                        new BinaryOperator("UserAccess", _locUserAccess),
                                                                        new BinaryOperator("AllWorkplace", true),
                                                                        new BinaryOperator("Active", true)));
                                if (_locWhsSetupDetail != null)
                                {
                                    _availableWorkplaceFrom = new XPCollection<Workplace>(Session,
                                                        new GroupOperator(GroupOperatorType.And,
                                                        new BinaryOperator("Company", this.Company),
                                                        new BinaryOperator("Active", true)));
                                }
                            }
                        }
                    }
                }

                return _availableWorkplaceFrom;

            }
        }

        [ImmediatePostData()]
        [Appearance("TransferInWorkplaceFromClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [DataSourceProperty("AvailableWorkplaceFrom", DataSourcePropertyIsNullMode.SelectAll)]
        public Workplace WorkplaceFrom
        {
            get { return _workplaceFrom; }
            set { SetPropertyValue("WorkplaceFrom", ref _workplaceFrom, value); }
        }

        [Browsable(false)]
        public XPCollection<Location> AvailableLocationFrom
        {
            get
            {
                if(!IsLoading)
                {
                    _userAccess = SecuritySystem.CurrentUserName;
                    string _beginString = null;
                    string _endString = null;
                    string _fullString = null;
                    string _locLocationCode = null;
                    UserAccess _locUserAccess = Session.FindObject<UserAccess>(new BinaryOperator("UserName", _userAccess));
                    List<string> _stringLocation = new List<string>();
                    if (_locUserAccess != null)
                    {
                        #region TransferIn
                        if (this.TransferType == DirectionType.Internal && this.InventoryMovingType == InventoryMovingType.TransferIn)
                        {
                            XPCollection<WarehouseSetupDetail> _locWhsSetupDetails = null;

                            if (this.LocationTypeFrom == LocationType.None)
                            {
                                _locWhsSetupDetails = new XPCollection<WarehouseSetupDetail>
                                                    (Session, new GroupOperator(GroupOperatorType.And,
                                                    new BinaryOperator("UserAccess", _locUserAccess),
                                                    new BinaryOperator("StockType", this.StockTypeFrom),
                                                    new BinaryOperator("Active", true)));
                            }
                            else
                            {
                                _locWhsSetupDetails = new XPCollection<WarehouseSetupDetail>
                                                    (Session, new GroupOperator(GroupOperatorType.And,
                                                    new BinaryOperator("UserAccess", _locUserAccess),
                                                    new BinaryOperator("LocationType", this.LocationTypeFrom),
                                                    new BinaryOperator("StockType", this.StockTypeFrom),
                                                    new BinaryOperator("Active", true)));
                            }


                            if (_locWhsSetupDetails != null && _locWhsSetupDetails.Count() > 0)
                            {
                                foreach (WarehouseSetupDetail _locWhsSetupDetail in _locWhsSetupDetails)
                                {
                                    if (_locWhsSetupDetail.Location != null)
                                    {
                                        if (_locWhsSetupDetail.Location.Code != null)
                                        {
                                            _locLocationCode = _locWhsSetupDetail.Location.Code;
                                            _stringLocation.Add(_locLocationCode);
                                        }
                                    }

                                }
                            }

                            IEnumerable<string> _stringArrayLocationFromDistinct = _stringLocation.Distinct();
                            string[] _stringArrayLocationFromList = _stringArrayLocationFromDistinct.ToArray();
                            if (_stringArrayLocationFromList.Length == 1)
                            {
                                for (int i = 0; i < _stringArrayLocationFromList.Length; i++)
                                {
                                    Location _locLocationFrom = Session.FindObject<Location>(new BinaryOperator("Code", _stringArrayLocationFromList[i]));
                                    if (_locLocationFrom != null)
                                    {
                                        if (i == 0)
                                        {
                                            _beginString = "[Code]=='" + _locLocationFrom.Code + "'";
                                        }
                                    }
                                }
                            }
                            else if (_stringArrayLocationFromList.Length > 1)
                            {
                                for (int i = 0; i < _stringArrayLocationFromList.Length; i++)
                                {
                                    Location _locLocationFrom = Session.FindObject<Location>(new BinaryOperator("Code", _stringArrayLocationFromList[i]));
                                    if (_locLocationFrom != null)
                                    {
                                        if (i == 0)
                                        {
                                            _beginString = "[Code]=='" + _locLocationFrom.Code + "'";
                                        }
                                        else
                                        {
                                            _endString = _endString + " OR [Code]=='" + _locLocationFrom.Code + "'";
                                        }
                                    }
                                }
                            }
                            _fullString = _beginString + _endString;

                            if (_fullString != null)
                            {
                                _availableLocationFrom = new XPCollection<Location>(Session, CriteriaOperator.Parse(_fullString));
                            }
                        }
                        #endregion TransferIn
                    }
                }

                return _availableLocationFrom;

            }
        }

        [ImmediatePostData()]
        [Appearance("TransferInLocationFromClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [DataSourceProperty("AvailableLocationFrom")]
        [DataSourceCriteria("Active = true")]
        public Location LocationFrom
        {
            get { return _locationFrom; }
            set {
                SetPropertyValue("LocationFrom", ref _locationFrom, value);
                if (!IsLoading)
                {
                    if (this._locationFrom != null)
                    {
                        SetBegInvFrom(_locationFrom);
                    }
                    else
                    {
                        BegInvFrom = null;
                    }
                }
            }
        }

        [Browsable(false)]
        [ImmediatePostData()]
        [Appearance("TransferInBegInvFromClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public BeginningInventory BegInvFrom
        {
            get { return _begInvFrom; }
            set { SetPropertyValue("BegInvFrom", ref _begInvFrom, value); }
        }

        #endregion From

        #region To

        [ImmediatePostData()]
        [Appearance("TransferInLocationTypeToClose", Enabled = false)]
        public LocationType LocationTypeTo
        {
            get { return _locationTypeTo; }
            set { SetPropertyValue("LocationTypeTo", ref _locationTypeTo, value); }
        }

        [ImmediatePostData()]
        [Appearance("TransferInStockTypeToClose", Enabled = false)]
        public StockType StockTypeTo
        {
            get { return _stockTypeTo; }
            set { SetPropertyValue("StockTypeTo", ref _stockTypeTo, value); }
        }

        [Browsable(false)]
        public XPCollection<Workplace> AvailableWorkplaceTo
        {
            get
            {
                if(!IsLoading)
                {
                    string _beginString = null;
                    string _endString = null;
                    string _fullString = null;

                    if (this.Company != null)
                    {
                        _userAccess = SecuritySystem.CurrentUserName;
                        UserAccess _locUserAccess = Session.FindObject<UserAccess>(new BinaryOperator("UserName", _userAccess));
                        if (_locUserAccess != null)
                        {
                            List<string> _stringOSD = new List<string>();

                            XPCollection<OrganizationSetupDetail> _locOrgSetupDetails = new XPCollection<OrganizationSetupDetail>
                                                                                (Session, new GroupOperator(GroupOperatorType.And,
                                                                                new BinaryOperator("Company", this.Company),
                                                                                new BinaryOperator("UserAccess", _locUserAccess),
                                                                                new BinaryOperator("AllWorkplace", false),
                                                                                new BinaryOperator("OpenWorkplace", true),
                                                                                new BinaryOperator("Active", true)));

                            if (_locOrgSetupDetails != null && _locOrgSetupDetails.Count() > 0)
                            {
                                foreach (OrganizationSetupDetail _locOrgSetupDetail in _locOrgSetupDetails)
                                {
                                    if (_locOrgSetupDetail.Workplace != null)
                                    {
                                        if (_locOrgSetupDetail.Workplace.Code != null)
                                        {
                                            _stringOSD.Add(_locOrgSetupDetail.Workplace.Code);
                                        }
                                    }
                                }
                            }

                            IEnumerable<string> _stringArrayOSDDistinct = _stringOSD.Distinct();
                            string[] _stringArrayOSDList = _stringArrayOSDDistinct.ToArray();
                            if (_stringArrayOSDList.Length == 1)
                            {
                                for (int i = 0; i < _stringArrayOSDList.Length; i++)
                                {
                                    Workplace _locW = Session.FindObject<Workplace>(new BinaryOperator("Code", _stringArrayOSDList[i]));
                                    if (_locW != null)
                                    {
                                        if (i == 0)
                                        {
                                            _beginString = "[Code]=='" + _locW.Code + "'";
                                        }
                                    }
                                }
                            }
                            else if (_stringArrayOSDList.Length > 1)
                            {
                                for (int i = 0; i < _stringArrayOSDList.Length; i++)
                                {
                                    Workplace _locW = Session.FindObject<Workplace>(new BinaryOperator("Code", _stringArrayOSDList[i]));
                                    if (_locW != null)
                                    {
                                        if (i == 0)
                                        {
                                            _beginString = "[Code]=='" + _locW.Code + "'";
                                        }
                                        else
                                        {
                                            _endString = _endString + " OR [Code]=='" + _locW.Code + "'";
                                        }
                                    }
                                }
                            }
                            _fullString = _beginString + _endString;

                            if (_fullString != null)
                            {
                                _availableWorkplaceTo = new XPCollection<Workplace>(Session, CriteriaOperator.Parse(_fullString));
                            }
                            else
                            {
                                OrganizationSetupDetail _locOrgniSetupDetails = Session.FindObject<OrganizationSetupDetail>
                                                                        (new GroupOperator(GroupOperatorType.And,
                                                                        new BinaryOperator("Company", this.Company),
                                                                        new BinaryOperator("UserAccess", _locUserAccess),
                                                                        new BinaryOperator("OpenWorkplace", false),
                                                                        new BinaryOperator("AllWorkplace", true),
                                                                        new BinaryOperator("Active", true),
                                                                        new BinaryOperator("Default", false)));
                                if (_locOrgniSetupDetails != null)
                                {
                                    _availableWorkplaceTo = new XPCollection<Workplace>(Session,
                                                        new GroupOperator(GroupOperatorType.And,
                                                        new BinaryOperator("Company", this.Company),
                                                        new BinaryOperator("Active", true)));
                                }
                            }

                        }
                        else
                        {
                            _availableWorkplaceTo = new XPCollection<Workplace>(Session,
                                                        new GroupOperator(GroupOperatorType.And,
                                                        new BinaryOperator("Company", this.Company),
                                                        new BinaryOperator("Active", true)));
                        }
                    }
                }
                
                return _availableWorkplaceTo;

            }

        }

        [ImmediatePostData()]
        [Appearance("TransferInWorkplaceToClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [DataSourceProperty("AvailableWorkplaceTo", DataSourcePropertyIsNullMode.SelectAll)]
        public Workplace WorkplaceTo
        {
            get { return _workplaceTo; }
            set
            {
                SetPropertyValue("WorkplaceTo", ref _workplaceTo, value);
                if (!IsLoading)
                {
                    if (this._workplaceTo != null)
                    {
                        if (this.DocumentType != null)
                        {
                            if (this.DocumentType.SameWorkplace == true)
                            {
                                this.WorkplaceFrom = this._workplaceTo;
                            }
                            else
                            {
                                this.WorkplaceFrom = null;
                            }
                        }
                        else
                        {
                            this.WorkplaceFrom = null;
                        }
                    }
                    else
                    {
                        this.WorkplaceFrom = null;
                    }
                }
            }
        }

        [Browsable(false)]
        public XPCollection<Location> AvailableLocationTo
        {
            get
            {
                if(!IsLoading)
                {
                    _userAccess = SecuritySystem.CurrentUserName;
                    string _beginString = null;
                    string _endString = null;
                    string _fullString = null;
                    string _locLocationCode = null;
                    UserAccess _locUserAccess = Session.FindObject<UserAccess>(new BinaryOperator("UserName", _userAccess));
                    List<string> _stringLocation = new List<string>();
                    if (_locUserAccess != null)
                    {
                        #region TransferIn
                        if (this.TransferType == DirectionType.Internal && this.InventoryMovingType == InventoryMovingType.TransferIn)
                        {
                            XPCollection<WarehouseSetupDetail> _locWhsSetupDetails = null;

                            if (this.LocationTypeTo == LocationType.None)
                            {
                                _locWhsSetupDetails = new XPCollection<WarehouseSetupDetail>
                                                    (Session, new GroupOperator(GroupOperatorType.And,
                                                    new BinaryOperator("UserAccess", _locUserAccess),
                                                    new BinaryOperator("StockType", this.StockTypeTo),
                                                    new BinaryOperator("Owner", true),
                                                    new BinaryOperator("Active", true)));
                            }
                            else
                            {
                                _locWhsSetupDetails = new XPCollection<WarehouseSetupDetail>
                                                    (Session, new GroupOperator(GroupOperatorType.And,
                                                    new BinaryOperator("UserAccess", _locUserAccess),
                                                    new BinaryOperator("LocationType", this.LocationTypeTo),
                                                    new BinaryOperator("StockType", this.StockTypeTo),
                                                    new BinaryOperator("Owner", true),
                                                    new BinaryOperator("Active", true)));
                            }



                            if (_locWhsSetupDetails != null && _locWhsSetupDetails.Count() > 0)
                            {
                                foreach (WarehouseSetupDetail _locWhsSetupDetail in _locWhsSetupDetails)
                                {
                                    if (_locWhsSetupDetail.Location != null)
                                    {
                                        if (_locWhsSetupDetail.Location.Code != null)
                                        {
                                            _locLocationCode = _locWhsSetupDetail.Location.Code;
                                            _stringLocation.Add(_locLocationCode);
                                        }
                                    }
                                }
                            }

                            IEnumerable<string> _stringArrayLocationToDistinct = _stringLocation.Distinct();
                            string[] _stringArrayLocationToList = _stringArrayLocationToDistinct.ToArray();
                            if (_stringArrayLocationToList.Length == 1)
                            {
                                for (int i = 0; i < _stringArrayLocationToList.Length; i++)
                                {
                                    Location _locLocationTo = Session.FindObject<Location>(new BinaryOperator("Code", _stringArrayLocationToList[i]));
                                    if (_locLocationTo != null)
                                    {
                                        if (i == 0)
                                        {
                                            _beginString = "[Code]=='" + _locLocationTo.Code + "'";
                                        }
                                    }
                                }
                            }
                            else if (_stringArrayLocationToList.Length > 1)
                            {
                                for (int i = 0; i < _stringArrayLocationToList.Length; i++)
                                {
                                    Location _locLocationTo = Session.FindObject<Location>(new BinaryOperator("Code", _stringArrayLocationToList[i]));
                                    if (_locLocationTo != null)
                                    {
                                        if (i == 0)
                                        {
                                            _beginString = "[Code]=='" + _locLocationTo.Code + "'";
                                        }
                                        else
                                        {
                                            _endString = _endString + " OR [Code]=='" + _locLocationTo.Code + "'";
                                        }
                                    }
                                }
                            }
                            _fullString = _beginString + _endString;

                            if (_fullString != null)
                            {
                                _availableLocationTo = new XPCollection<Location>(Session, CriteriaOperator.Parse(_fullString));
                            }
                        }
                        #endregion TransferIn
                    }
                }
                
                return _availableLocationTo;

            }
        }

        [ImmediatePostData]
        [Appearance("TransferInLineLocationToClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [DataSourceProperty("AvailableLocationTo")]
        [DataSourceCriteria("Active = true")]
        public Location LocationTo
        {
            get { return _locationTo; }
            set
            {
                SetPropertyValue("LocationTo", ref _locationTo, value);
                if (!IsLoading)
                {
                    if (this._locationTo != null)
                    {
                        SetBegInvTo(_locationTo);
                        if (this.DocumentType != null)
                        {
                            if (this.DocumentType.SameLocation == true)
                            {
                                this.LocationFrom = this.LocationTo;
                            }
                            else
                            {
                                this.LocationFrom = null;
                            }
                        }
                        else
                        {
                            this.LocationFrom = null;
                        }
                    }
                    else
                    {
                        BegInvTo = null;
                        this.LocationFrom = null;
                    }
                }
            }
        }

        [Browsable(false)]
        [ImmediatePostData()]
        [Appearance("TransferInBegInvToClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public BeginningInventory BegInvTo
        {
            get { return _begInvTo; }
            set { SetPropertyValue("BegInvTo", ref _begInvTo, value); }
        }

        #endregion To

        [Browsable(false)]
        [ImmediatePostData()]
        public bool OpenPIC
        {
            get { return _openPIC; }
            set { SetPropertyValue("OpenPIC", ref _openPIC, value); }
        }

        [ModelDefault("DisplayFormat", "{0:dd/MM/yyyy}")]
        [ModelDefault("EditMask", "dd/MM/yyyy")]
        [Appearance("TransferInPickingDateHide", Criteria = "OpenPIC = false", Visibility = ViewItemVisibility.Hide, Context = "DetailView")]
        [Appearance("TransferInPickingDatehow", Criteria = "OpenPIC = true", Visibility = ViewItemVisibility.Show, Context = "DetailView")]
        [Appearance("TransferInPickingDateClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public DateTime PickingDate
        {
            get { return _pickingDate; }
            set { SetPropertyValue("PickingDate", ref _pickingDate, value); }

        }

        [Browsable(false)]
        public XPCollection<Picking> AvailablePicking
        {
            get
            {

                if (!IsLoading)
                {
                    if (this.Company != null && this.WorkplaceFrom != null)
                    {
                        XPQuery<TransferOutMonitoring> _transferOutMonitoringsQuery = new XPQuery<TransferOutMonitoring>(Session);

                        var _transferOutMonitorings = from sim in _transferOutMonitoringsQuery
                                                      where ((sim.Status == Status.Open || sim.Status == Status.Posted)
                                                      && sim.Company == this.Company
                                                      && sim.Workplace == this.WorkplaceFrom
                                                      && sim.TransferOut != null
                                                      && sim.TransferIn == null
                                                      && sim.Picking != null
                                                      )
                                                      group sim by sim.Picking into g
                                                      select new { Picking = g.Key };

                        if (_transferOutMonitorings != null && _transferOutMonitorings.Count() > 0)
                        {
                            string _beginString = null;
                            string _endString = null;
                            string _fullString = null;

                            List<string> _stringTOM = new List<string>();

                            foreach (var _pickingMonitoring in _transferOutMonitorings)
                            {
                                if (_pickingMonitoring != null)
                                {
                                    if (_pickingMonitoring.Picking != null)
                                    {
                                        if (_pickingMonitoring.Picking.Code != null && _pickingMonitoring.Picking.PickingDate.ToShortDateString() == this.PickingDate.ToShortDateString())
                                        {
                                            _stringTOM.Add(_pickingMonitoring.Picking.Code);
                                        }
                                    }
                                }
                            }

                            IEnumerable<string> _stringArrayTOMDistinct = _stringTOM.Distinct();
                            string[] _stringArrayTOMList = _stringArrayTOMDistinct.ToArray();
                            if (_stringArrayTOMList.Length == 1)
                            {
                                for (int i = 0; i < _stringArrayTOMList.Length; i++)
                                {
                                    if (i == 0)
                                    {
                                        _beginString = "[Code]=='" + _stringArrayTOMList[i] + "'";
                                    }
                                }
                            }
                            else if (_stringArrayTOMList.Length > 1)
                            {
                                for (int i = 0; i < _stringArrayTOMList.Length; i++)
                                {
                                    if (i == 0)
                                    {
                                        _beginString = "[Code]=='" + _stringArrayTOMList[i] + "'";
                                    }
                                    else
                                    {
                                        _endString = _endString + " OR [Code]=='" + _stringArrayTOMList[i] + "'";
                                    }
                                }
                            }
                            _fullString = _beginString + _endString;

                            if (_fullString != null && _fullString != "")
                            {
                                _availablePicking = new XPCollection<Picking>(Session, CriteriaOperator.Parse(_fullString));
                            }
                        }
                    }
                }

                return _availablePicking;
            }
        }

        [DataSourceProperty("AvailablePicking")]
        [Appearance("TransferInPickingHide", Criteria = "OpenPIC = false", Visibility = ViewItemVisibility.Hide, Context = "DetailView")]
        [Appearance("TransferInPickingShow", Criteria = "OpenPIC = true", Visibility = ViewItemVisibility.Show, Context = "DetailView")]
        [Appearance("TransferInPickingClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Picking Picking
        {
            get { return _picking; }
            set
            {
                SetPropertyValue("Picking", ref _picking, value);
                if (!IsLoading)
                {
                    if (this._picking != null)
                    {
                        if (this._picking.Vehicle != null)
                        {
                            this.Vehicle = this._picking.Vehicle;
                        }
                        else
                        {
                            this.Vehicle = null;
                        }
                    }
                    else
                    {
                        this.Vehicle = null;
                    }
                }
            }
        }

        [Browsable(false)]
        public XPCollection<Vehicle> AvailableVehicle
        {
            get
            {
                if (!IsLoading)
                {
                    if (this.Company != null && this.WorkplaceFrom != null)
                    {
                        _availableVehicle = new XPCollection<Vehicle>(Session, new GroupOperator(GroupOperatorType.And,
                                                                            new BinaryOperator("Company", this.Company),
                                                                            new BinaryOperator("Workplace", this.WorkplaceFrom),
                                                                            new BinaryOperator("Active", true)));
                    }
                    else
                    {
                        _availableVehicle = new XPCollection<Vehicle>(Session, new GroupOperator(GroupOperatorType.And,
                                                                            new BinaryOperator("Active", true)));
                    }

                }

                return _availableVehicle;

            }
        }

        [ImmediatePostData()]
        [DataSourceProperty("AvailableVehicle")]
        [Appearance("TransferInVehicleHide", Criteria = "OpenPIC = false", Visibility = ViewItemVisibility.Hide, Context = "DetailView")]
        [Appearance("TransferInVehicleShow", Criteria = "OpenPIC = true", Visibility = ViewItemVisibility.Show, Context = "DetailView")]
        [Appearance("TransferInVehicleClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Vehicle Vehicle
        {
            get { return _vehicle; }
            set {
                SetPropertyValue("Vehicle", ref _vehicle, value);
                if (!IsLoading)
                {
                    if (this._vehicle != null)
                    {
                        SetLocationTo(this._vehicle);
                    }
                    else
                    {
                        this.LocationTo = null;
                    }
                }
            }
        }

        [Browsable(false)]
        [Appearance("TransferOutPostedCountClose", Enabled = false)]
        public int PostedCount
        {
            get { return _postedCount; }
            set { SetPropertyValue("PostedCount", ref _postedCount, value); }
        }

        [Browsable(false)]
        [ImmediatePostData()]
        [VisibleInListView(false)]
        [ModelDefault("DisplayFormat", "{0:dd/MM/yyyy}")]
        [ModelDefault("EditMask", "dd/MM/yyyy")]
        [Appearance("TransferInDocDateClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public DateTime DocDate
        {
            get { return _docDate; }
            set
            {
                SetPropertyValue("DocDate", ref _docDate, value);
                if (!IsLoading)
                {
                    this.JournalMonth = this._docDate.Month;
                    this.JournalYear = this._docDate.Year;
                }
            }
        }

        [Browsable(false)]
        public int JournalMonth
        {
            get { return _journalMonth; }
            set { SetPropertyValue("JournalMonth", ref _journalMonth, value); }
        }

        [Browsable(false)]
        public int JournalYear
        {
            get { return _journalYear; }
            set { SetPropertyValue("JournalYear", ref _journalYear, value); }
        }

        [Appearance("TransferInStatusEnabled", Enabled = false)]
        public Status Status
        {
            get { return _status; }
            set { SetPropertyValue("Status", ref _status, value); }
        }

        [ModelDefault("DisplayFormat", "{0:dd/MM/yyyy}")]
        [ModelDefault("EditMask", "dd/MM/yyyy")]
        [Appearance("TransferInStatusDateEnabled", Enabled = false)]
        public DateTime StatusDate
        {
            get { return _statusDate; }
            set { SetPropertyValue("StatusDate", ref _statusDate, value); }
        }

        [Association("TransferIn-TransferInLines")]
        public XPCollection<TransferInLine> TransferInLines
        {
            get { return GetCollection<TransferInLine>("TransferInLines"); }
        }

        [Association("TransferIn-TransferOutCollections")]
        public XPCollection<TransferOutCollection> TransferOutCollections
        {
            get { return GetCollection<TransferOutCollection>("TransferOutCollections"); }
        }

        //AuditTrail
        [CollectionOperationSet(AllowAdd = false, AllowRemove = true)]
        public XPCollection<AuditDataItemPersistent> ChangeHistory
        {
            get
            {
                if (changeHistory == null)
                {
                    changeHistory = AuditedObjectWeakReference.GetAuditTrail(Session, this);
                }
                return changeHistory;
            }
        }

        #endregion Field

        //=================================== Code Only ===================================

        private void SetBegInvTo(Location _locLocation)
        {
            try
            {
                DateTime now = DateTime.Now;
                if (this.Company != null && this.WorkplaceTo != null)
                {
                    BeginningInventory _locBegInvTo = Session.FindObject<BeginningInventory>
                                                        (new GroupOperator(GroupOperatorType.And,
                                                        new BinaryOperator("Company", this.Company),
                                                        new BinaryOperator("Workplace", this.WorkplaceTo),
                                                        new BinaryOperator("Location", _locLocation),
                                                        new BinaryOperator("LocationType", this.LocationTypeTo),
                                                        new BinaryOperator("StockType", this.StockTypeTo),
                                                        new BinaryOperator("Active", true)));
                    if (_locBegInvTo != null)
                    {
                        this.BegInvTo = _locBegInvTo;
                    }
                    else
                    {
                        this.BegInvTo = null;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Business Object = TransferIn ", ex.ToString());
            }
        }

        private void SetBegInvFrom(Location _locLocation)
        {
            try
            {
                DateTime now = DateTime.Now;
                if (this.Company != null && this.WorkplaceFrom != null)
                {
                    BeginningInventory _locBegInvFrom = Session.FindObject<BeginningInventory>
                                                        (new GroupOperator(GroupOperatorType.And,
                                                        new BinaryOperator("Company", this.Company),
                                                        new BinaryOperator("Workplace", this.WorkplaceFrom),
                                                        new BinaryOperator("Location", _locLocation),
                                                        new BinaryOperator("LocationType", this.LocationTypeFrom),
                                                        new BinaryOperator("StockType", this.StockTypeFrom),
                                                        new BinaryOperator("Active", true)));
                    if (_locBegInvFrom != null)
                    {
                        this.BegInvFrom = _locBegInvFrom;
                    }
                    else
                    {
                        this.BegInvFrom = null;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Business Object = TransferIn ", ex.ToString());
            }
        }

        private void SetLocationTo(Vehicle _locVehicle)
        {
            try
            {
                DateTime now = DateTime.Now;
                if (this.Company != null && this.Workplace != null)
                {
                    Location _locLocation = Session.FindObject<Location>
                                                        (new GroupOperator(GroupOperatorType.And,
                                                        new BinaryOperator("Company", this.Company),
                                                        new BinaryOperator("Workplace", this.Workplace),
                                                        new BinaryOperator("OpenVehicle", true),
                                                        new BinaryOperator("Vehicle", _locVehicle),
                                                        new BinaryOperator("Active", true)));
                    if (_locLocation != null)
                    {
                        if (_locLocation != null)
                        {
                            this.LocationTo = _locLocation;
                        }
                        else
                        {
                            this.LocationTo = null;
                        }
                    }
                    else
                    {
                        this.LocationTo = null;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Business Object = TransferOrder ", ex.ToString());
            }
        }
    }
}