﻿using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;

using DevExpress.ExpressApp.Editors;
using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.ConditionalAppearance;

namespace FullDrive.Module.BusinessObjects
{
    [DefaultClassOptions]
    [DefaultProperty("Code")]
    [NavigationItem("Finance & Accounting")]
    [RuleCombinationOfPropertiesIsUnique("ReceivableTransaction2RuleUnique", DefaultContexts.Save, "Code")]
    //[ImageName("BO_Contact")]
    //[DefaultProperty("DisplayMemberNameForLookupEditorsOfThisType")]
    //[DefaultListViewOptions(MasterDetailMode.ListViewOnly, false, NewItemRowPosition.None)]
    //[Persistent("DatabaseTableName")]
    // Specify more UI options using a declarative approach (https://documentation.devexpress.com/#eXpressAppFramework/CustomDocument112701).
    public class ReceivableTransaction2 : BaseObject
    { // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).

        private bool _activationPosting;
        private string _code;
        #region InitialOrganization
        private Company _company;
        private XPCollection<Workplace> _availableWorkplace;
        private Workplace _workplace;
        private Division _division;
        private Department _department;
        private Section _section;
        private Employee _employee;
        private Division _div;
        private Department _dept;
        private Section _sect;
        private XPCollection<Employee> _availablePIC;
        private Employee _pic;
        #endregion InitialOrganization
        private XPCollection<Vehicle> _availableVehicle;
        private Vehicle _vehicle;
        private DateTime _pickingDate;
        private XPCollection<Picking> _availablePicking;
        private Picking _picking;
        private double _grandTotalAmount;
        private double _grandTotalAmountDN;
        private double _grandTotalAmountCN;
        private double _grandTotalPlan;
        private Status _status;
        private DateTime _statusDate;
        private string _userAccessIn;
        private GlobalFunction _globFunc;
        //AuditTrail
        private XPCollection<AuditDataItemPersistent> changeHistory;

        public ReceivableTransaction2(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if (!IsLoading && !IsSaving && !IsInvalidated)
            {
                if (Session.IsNewObject(this))
                {
                    _globFunc = new GlobalFunction();
                    DateTime now = DateTime.Now;
                    #region UserAccess
                    _userAccessIn = SecuritySystem.CurrentUserName;
                    UserAccess _locUserAccess = Session.FindObject<UserAccess>(new BinaryOperator("UserName", _userAccessIn));

                    if (_locUserAccess != null)
                    {
                        if (_locUserAccess.Employee != null)
                        {
                            if (_locUserAccess.Employee.Company != null && _locUserAccess.Employee.Workplace != null)
                            {
                                this.Code = _globFunc.GetNumberingUnlockOptimisticRecordBasedOrganization(this.Session.DataLayer, ObjectList.ReceivableTransaction2, _locUserAccess.Employee.Company, _locUserAccess.Employee.Workplace);
                            }
                            else
                            {
                                this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.ReceivableTransaction2);
                            }

                            if (_locUserAccess.Employee.Company != null)
                            {
                                Company = _locUserAccess.Employee.Company;
                            }
                            if (_locUserAccess.Employee.Workplace != null)
                            {
                                this.Workplace = _locUserAccess.Employee.Workplace;
                            }
                            else
                            {
                                this.Workplace = _globFunc.GetDefaultWorkplace(this.Session, _locUserAccess);
                            }
                            if (_locUserAccess.Employee.Division != null)
                            {
                                this.Division = _locUserAccess.Employee.Division;
                                this.Div = _locUserAccess.Employee.Division;
                            }
                            if (_locUserAccess.Employee.Department != null)
                            {
                                this.Department = _locUserAccess.Employee.Department;
                                this.Dept = _locUserAccess.Employee.Department;
                            }
                            if (_locUserAccess.Employee.Section != null)
                            {
                                this.Section = _locUserAccess.Employee.Section;
                                this.Sect = _locUserAccess.Employee.Section;
                            }

                            this.Employee = _locUserAccess.Employee;
                        }
                    }
                    #endregion UserAccess
                    this.PickingDate = now;
                    this.Status = CustomProcess.Status.Open;
                    this.StatusDate = now;
                }
            }
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).
        }

        #region Field

        [Browsable(false)]
        [ImmediatePostData()]
        public bool ActivationPosting
        {
            get { return _activationPosting; }
            set { SetPropertyValue("ActivationPosting", ref _activationPosting, value); }
        }

        [Appearance("ReceivableTransaction2CodeClose", Enabled = false)]
        [RuleRequiredField(DefaultContexts.Save)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue("Code", ref _code, value); }
        }

        #region Organization

        [ImmediatePostData()]
        [RuleRequiredField(DefaultContexts.Save)]
        [Appearance("ReceivableTransaction2CompanyClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Company Company
        {
            get { return _company; }
            set { SetPropertyValue("Company", ref _company, value); }
        }

        [Browsable(false)]
        public XPCollection<Workplace> AvailableWorkplace
        {
            get
            {
                if (!IsLoading)
                {
                    if (this.Company != null)
                    {
                        _userAccessIn = SecuritySystem.CurrentUserName;
                        UserAccess _locUserAccess = Session.FindObject<UserAccess>(new BinaryOperator("UserName", _userAccessIn));
                        if (_locUserAccess != null)
                        {
                            string _beginString = null;
                            string _endString = null;
                            string _fullString = null;
                            List<string> _stringWorkplace = new List<string>();

                            XPCollection<OrganizationSetupDetail> _locOrganizationSetupDetails = new XPCollection<OrganizationSetupDetail>
                                                                            (Session, new GroupOperator(GroupOperatorType.And,
                                                                            new BinaryOperator("Company", this.Company),
                                                                            new BinaryOperator("UserAccess", _locUserAccess),
                                                                            new BinaryOperator("OpenWorkplace", true),
                                                                            new BinaryOperator("Active", true)));

                            if (_locOrganizationSetupDetails != null && _locOrganizationSetupDetails.Count() > 0)
                            {
                                foreach (OrganizationSetupDetail _locOrganizationSetupDetail in _locOrganizationSetupDetails)
                                {
                                    if (_locOrganizationSetupDetail.Workplace != null)
                                    {
                                        if (_locOrganizationSetupDetail.Workplace.Code != null)
                                        {
                                            _stringWorkplace.Add(_locOrganizationSetupDetail.Workplace.Code);
                                        }
                                    }
                                }

                                #region Workplace
                                IEnumerable<string> _stringArrayWorkplaceDistinct = _stringWorkplace.Distinct();
                                string[] _stringArrayWorkplaceList = _stringArrayWorkplaceDistinct.ToArray();
                                if (_stringArrayWorkplaceList.Length == 1)
                                {
                                    for (int i = 0; i < _stringArrayWorkplaceList.Length; i++)
                                    {
                                        Workplace _locWorkplace = Session.FindObject<Workplace>(new BinaryOperator("Code", _stringArrayWorkplaceList[i]));
                                        if (_locWorkplace != null)
                                        {
                                            if (i == 0)
                                            {
                                                _beginString = "[Code]=='" + _locWorkplace.Code + "'";
                                            }
                                        }
                                    }
                                }
                                else if (_stringArrayWorkplaceList.Length > 1)
                                {
                                    for (int i = 0; i < _stringArrayWorkplaceList.Length; i++)
                                    {
                                        Workplace _locWorkplace = Session.FindObject<Workplace>(new BinaryOperator("Code", _stringArrayWorkplaceList[i]));
                                        if (_locWorkplace != null)
                                        {
                                            if (i == 0)
                                            {
                                                _beginString = "[Code]=='" + _locWorkplace.Code + "'";
                                            }
                                            else
                                            {
                                                _endString = _endString + " OR [Code]=='" + _locWorkplace.Code + "'";
                                            }
                                        }

                                    }
                                }
                                _fullString = _beginString + _endString;


                                if (_fullString != null)
                                {
                                    _availableWorkplace = new XPCollection<Workplace>(Session, CriteriaOperator.Parse(_fullString));
                                }

                                #endregion Workplace
                            }
                            else
                            {
                                OrganizationSetupDetail _locOrgSetDetail = Session.FindObject<OrganizationSetupDetail>
                                                                            (new GroupOperator(GroupOperatorType.And,
                                                                            new BinaryOperator("Company", this.Company),
                                                                            new BinaryOperator("UserAccess", _locUserAccess),
                                                                            new BinaryOperator("AllWorkplace", true),
                                                                            new BinaryOperator("Active", true)));
                                if (_locOrgSetDetail != null)
                                {
                                    _availableWorkplace = new XPCollection<Workplace>(Session,
                                                        new GroupOperator(GroupOperatorType.And,
                                                        new BinaryOperator("Company", this.Company),
                                                        new BinaryOperator("Active", true)));
                                }
                            }
                        }
                    }
                }

                return _availableWorkplace;

            }
        }

        [ImmediatePostData()]
        [DataSourceProperty("AvailableWorkplace")]
        [Appearance("ReceivableTransaction2WorkplaceClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Workplace Workplace
        {
            get { return _workplace; }
            set { SetPropertyValue("Workplace", ref _workplace, value); }
        }

        [Browsable(false)]
        public Division Division
        {
            get { return _division; }
            set { SetPropertyValue("Division", ref _division, value); }
        }

        [Browsable(false)]
        public Department Department
        {
            get { return _department; }
            set { SetPropertyValue("Department", ref _department, value); }
        }

        [Browsable(false)]
        public Section Section
        {
            get { return _section; }
            set { SetPropertyValue("Section", ref _section, value); }
        }

        [Browsable(false)]
        public Employee Employee
        {
            get { return _employee; }
            set { SetPropertyValue("Employee", ref _employee, value); }
        }

        [ImmediatePostData()]
        [DataSourceCriteria("Company = '@This.Company' And Active = true")]
        [Appearance("ReceivableTransaction2DivClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Division Div
        {
            get { return _div; }
            set { SetPropertyValue("Div", ref _div, value); }
        }

        [ImmediatePostData()]
        [DataSourceCriteria("Company = '@This.Company' And Division = '@This.Div' And Active = true")]
        [Appearance("ReceivableTransaction2DeptClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Department Dept
        {
            get { return _dept; }
            set { SetPropertyValue("Dept", ref _dept, value); }
        }

        [ImmediatePostData()]
        [DataSourceCriteria("Company = '@This.Company' And Department = '@This.Dept' And Active = true")]
        [Appearance("ReceivableTransaction2SectClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Section Sect
        {
            get { return _sect; }
            set { SetPropertyValue("Sect", ref _sect, value); }
        }

        [Browsable(false)]
        public XPCollection<Employee> AvailablePIC
        {
            get
            {
                XPCollection<Employee> _locPICs = null;
                if (this.Company != null && this.Workplace != null)
                {
                    #region DivDeptSect
                    if (this.Div != null && this.Dept != null && this.Sect != null)
                    {
                        _locPICs = new XPCollection<Employee>
                                  (Session, new GroupOperator(GroupOperatorType.And,
                                    new BinaryOperator("Company", this.Company),
                                    new BinaryOperator("Workplace", this.Workplace),
                                    new BinaryOperator("Division", this.Div),
                                    new BinaryOperator("Department", this.Dept),
                                    new BinaryOperator("Section", this.Sect),
                                    new BinaryOperator("Active", true),
                                    new GroupOperator(GroupOperatorType.Or,
                                    new BinaryOperator("SalesRole", SalesRole.TakingOrder),
                                    new BinaryOperator("SalesRole", SalesRole.All))));
                    }
                    else if (this.Div != null && this.Dept != null && this.Sect == null)
                    {
                        _locPICs = new XPCollection<Employee>
                                  (Session, new GroupOperator(GroupOperatorType.And,
                                    new BinaryOperator("Company", this.Company),
                                    new BinaryOperator("Workplace", this.Workplace),
                                    new BinaryOperator("Division", this.Div),
                                    new BinaryOperator("Department", this.Dept),
                                    new BinaryOperator("Active", true),
                                    new GroupOperator(GroupOperatorType.Or,
                                    new BinaryOperator("SalesRole", SalesRole.TakingOrder),
                                    new BinaryOperator("SalesRole", SalesRole.All))));

                    }
                    else if (this.Div != null && this.Dept == null && this.Sect == null)
                    {
                        _locPICs = new XPCollection<Employee>
                                  (Session, new GroupOperator(GroupOperatorType.And,
                                    new BinaryOperator("Company", this.Company),
                                    new BinaryOperator("Workplace", this.Workplace),
                                    new BinaryOperator("Division", this.Div),
                                    new BinaryOperator("Active", true),
                                    new GroupOperator(GroupOperatorType.Or,
                                    new BinaryOperator("SalesRole", SalesRole.TakingOrder),
                                    new BinaryOperator("SalesRole", SalesRole.All))));

                    }
                    else if (this.Div == null && this.Dept == null && this.Sect == null)
                    {
                        _locPICs = new XPCollection<Employee>
                                  (Session, new GroupOperator(GroupOperatorType.And,
                                    new BinaryOperator("Company", this.Company),
                                    new BinaryOperator("Workplace", this.Workplace),
                                    new BinaryOperator("Active", true),
                                    new GroupOperator(GroupOperatorType.Or,
                                    new BinaryOperator("SalesRole", SalesRole.TakingOrder),
                                    new BinaryOperator("SalesRole", SalesRole.All))));

                    }
                    #endregion DivDeptSect
                    if (_locPICs != null && _locPICs.Count() > 0)
                    {
                        _availablePIC = _locPICs;
                    }
                }
                else
                {
                    _availablePIC = new XPCollection<Employee>(Session, new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("Active", true),
                                                            new GroupOperator(GroupOperatorType.Or,
                                                            new BinaryOperator("SalesRole", SalesRole.TakingOrder),
                                                            new BinaryOperator("SalesRole", SalesRole.All))));
                }

                return _availablePIC;

            }
        }

        [ImmediatePostData()]
        [DataSourceProperty("AvailablePIC")]
        [Appearance("ReceivableTransaction2PICClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Employee PIC
        {
            get { return _pic; }
            set
            {
                SetPropertyValue("Salesman", ref _pic, value);
                if (!IsLoading)
                {
                    if (this._pic != null)
                    {
                        SetVehicle(_pic);
                    }
                    else
                    {
                        this.Vehicle = null;
                    }
                }
            }
        }

        #endregion Organization

        [Browsable(false)]
        public XPCollection<Vehicle> AvailableVehicle
        {
            get
            {
                if (!IsLoading)
                {
                    if (this.Company != null && this.Workplace != null)
                    {
                        string _beginString = null;
                        string _endString = null;
                        string _fullString = null;

                        #region PIC
                        if (this.PIC != null)
                        {
                            List<string> _stringSVS = new List<string>();

                            XPCollection<SalesmanVehicleSetup> _locSalesmanVehicleSetups = new XPCollection<SalesmanVehicleSetup>
                                                                                      (Session, new GroupOperator(GroupOperatorType.And,
                                                                                       new BinaryOperator("Company", this.Company),
                                                                                       new BinaryOperator("Workplace", this.Workplace),
                                                                                       new BinaryOperator("Salesman", this.PIC),
                                                                                       new BinaryOperator("Active", true)));

                            if (_locSalesmanVehicleSetups != null && _locSalesmanVehicleSetups.Count() > 0)
                            {
                                foreach (SalesmanVehicleSetup _locSalesmanVehicleSetup in _locSalesmanVehicleSetups)
                                {
                                    if (_locSalesmanVehicleSetup.Vehicle != null)
                                    {
                                        if (_locSalesmanVehicleSetup.Vehicle.Code != null)
                                        {
                                            _stringSVS.Add(_locSalesmanVehicleSetup.Vehicle.Code);
                                        }
                                    }
                                }
                            }

                            IEnumerable<string> _stringArraySVSDistinct = _stringSVS.Distinct();
                            string[] _stringArraySVSList = _stringArraySVSDistinct.ToArray();
                            if (_stringArraySVSList.Length == 1)
                            {
                                for (int i = 0; i < _stringArraySVSList.Length; i++)
                                {
                                    Vehicle _locV = Session.FindObject<Vehicle>(new BinaryOperator("Code", _stringArraySVSList[i]));
                                    if (_locV != null)
                                    {
                                        if (i == 0)
                                        {
                                            _beginString = "[Code]=='" + _locV.Code + "'";
                                        }
                                    }
                                }
                            }
                            else if (_stringArraySVSList.Length > 1)
                            {
                                for (int i = 0; i < _stringArraySVSList.Length; i++)
                                {
                                    Vehicle _locV = Session.FindObject<Vehicle>(new BinaryOperator("Code", _stringArraySVSList[i]));
                                    if (_locV != null)
                                    {
                                        if (i == 0)
                                        {
                                            _beginString = "[Code]=='" + _locV.Code + "'";
                                        }
                                        else
                                        {
                                            _endString = _endString + " OR [Code]=='" + _locV.Code + "'";
                                        }
                                    }
                                }
                            }
                            _fullString = _beginString + _endString;

                            if (_fullString != null && _fullString != "")
                            {
                                _availableVehicle = new XPCollection<Vehicle>(Session, CriteriaOperator.Parse(_fullString));
                            }
                            else
                            {
                                _availableVehicle = new XPCollection<Vehicle>(Session, new GroupOperator(GroupOperatorType.And,
                                                                                    new BinaryOperator("Company", this.Company),
                                                                                    new BinaryOperator("Workplace", this.Workplace),
                                                                                    new BinaryOperator("Active", true)));
                            }
                        }
                        #endregion PIC
                        #region NonPIC
                        else
                        {
                            _availableVehicle = new XPCollection<Vehicle>(Session, new GroupOperator(GroupOperatorType.And,
                                                                            new BinaryOperator("Company", this.Company),
                                                                            new BinaryOperator("Workplace", this.Workplace),
                                                                            new BinaryOperator("Active", true)));
                        }
                        #endregion NonPIC
                    }
                    else
                    {
                        _availableVehicle = new XPCollection<Vehicle>(Session, new GroupOperator(GroupOperatorType.And,
                                                                            new BinaryOperator("Active", true)));
                    }

                }

                return _availableVehicle;

            }
        }

        [ImmediatePostData()]
        [DataSourceProperty("AvailableVehicle")]
        [Appearance("ReceivableTransaction2VehicleClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Vehicle Vehicle
        {
            get { return _vehicle; }
            set { SetPropertyValue("Vehicle", ref _vehicle, value); }
        }

        [ImmediatePostData()]
        [ModelDefault("DisplayFormat", "{0:dd/MM/yyyy}")]
        [ModelDefault("EditMask", "dd/MM/yyyy")]
        [Appearance("ReceivableTransaction2PickingDateClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public DateTime PickingDate
        {
            get { return _pickingDate; }
            set { SetPropertyValue("PickingDate", ref _pickingDate, value); }

        }

        [Browsable(false)]
        public XPCollection<Picking> AvailablePicking
        {
            get
            {
                if (!IsLoading)
                {
                    if (this.Company != null && this.Workplace != null)
                    {
                        if(this.PIC != null)
                        {
                            XPQuery<OrderCollectionMonitoring> _orderCollectionsQuery = new XPQuery<OrderCollectionMonitoring>(Session);

                            var _orderCollections = from tim in _orderCollectionsQuery
                                                    where ((tim.Status == Status.Open || tim.Status == Status.Posted)
                                                    && tim.CollectStatus == CollectStatus.Collect
                                                    && tim.Company == this.Company
                                                    && tim.Workplace == this.Workplace
                                                    && tim.PIC == this.PIC
                                                    && tim.Picking != null
                                                    && tim.ReceivableTransaction == null
                                                    && tim.ReceivableTransaction2 == null)
                                                    group tim by tim.Picking into g
                                                    select new { Picking = g.Key };

                            if (_orderCollections != null && _orderCollections.Count() > 0)
                            {
                                string _beginString = null;
                                string _endString = null;
                                string _fullString = null;

                                List<string> _stringOC = new List<string>();

                                foreach (var _orderCollection in _orderCollections)
                                {
                                    if (_orderCollection != null)
                                    {
                                        if (_orderCollection.Picking != null)
                                        {
                                            if (_orderCollection.Picking.Code != null && _orderCollection.Picking.PickingDate.ToShortDateString() == this.PickingDate.ToShortDateString())
                                            {
                                                _stringOC.Add(_orderCollection.Picking.Code);
                                            }
                                        }
                                    }
                                }

                                IEnumerable<string> _stringArrayOCDistinct = _stringOC.Distinct();
                                string[] _stringArrayOCList = _stringArrayOCDistinct.ToArray();
                                if (_stringArrayOCList.Length == 1)
                                {
                                    for (int i = 0; i < _stringArrayOCList.Length; i++)
                                    {
                                        if (i == 0)
                                        {
                                            _beginString = "[Code]=='" + _stringArrayOCList[i] + "'";
                                        }
                                    }
                                }
                                else if (_stringArrayOCList.Length > 1)
                                {
                                    for (int i = 0; i < _stringArrayOCList.Length; i++)
                                    {
                                        if (i == 0)
                                        {
                                            _beginString = "[Code]=='" + _stringArrayOCList[i] + "'";
                                        }
                                        else
                                        {
                                            _endString = _endString + " OR [Code]=='" + _stringArrayOCList[i] + "'";
                                        }
                                    }
                                }
                                _fullString = _beginString + _endString;

                                if (_fullString != null && _fullString != "")
                                {
                                    _availablePicking = new XPCollection<Picking>(Session, CriteriaOperator.Parse(_fullString));
                                }
                            }
                        }
                        else
                        {
                            XPQuery<OrderCollectionMonitoring> _orderCollectionsQuery = new XPQuery<OrderCollectionMonitoring>(Session);

                            var _orderCollections = from tim in _orderCollectionsQuery
                                                    where ((tim.Status == Status.Open || tim.Status == Status.Posted)
                                                    && tim.CollectStatus == CollectStatus.Collect
                                                    && tim.Company == this.Company
                                                    && tim.Workplace == this.Workplace
                                                    && tim.Picking != null
                                                    && tim.ReceivableTransaction == null
                                                    && tim.ReceivableTransaction2 == null)
                                                    group tim by tim.Picking into g
                                                    select new { Picking = g.Key };

                            if (_orderCollections != null && _orderCollections.Count() > 0)
                            {
                                string _beginString = null;
                                string _endString = null;
                                string _fullString = null;

                                List<string> _stringOC = new List<string>();

                                foreach (var _orderCollection in _orderCollections)
                                {
                                    if (_orderCollection != null)
                                    {
                                        if (_orderCollection.Picking != null)
                                        {
                                            if (_orderCollection.Picking.Code != null && _orderCollection.Picking.PickingDate.ToShortDateString() == this.PickingDate.ToShortDateString())
                                            {
                                                _stringOC.Add(_orderCollection.Picking.Code);
                                            }
                                        }
                                    }
                                }

                                IEnumerable<string> _stringArrayOCDistinct = _stringOC.Distinct();
                                string[] _stringArrayOCList = _stringArrayOCDistinct.ToArray();
                                if (_stringArrayOCList.Length == 1)
                                {
                                    for (int i = 0; i < _stringArrayOCList.Length; i++)
                                    {
                                        if (i == 0)
                                        {
                                            _beginString = "[Code]=='" + _stringArrayOCList[i] + "'";
                                        }
                                    }
                                }
                                else if (_stringArrayOCList.Length > 1)
                                {
                                    for (int i = 0; i < _stringArrayOCList.Length; i++)
                                    {
                                        if (i == 0)
                                        {
                                            _beginString = "[Code]=='" + _stringArrayOCList[i] + "'";
                                        }
                                        else
                                        {
                                            _endString = _endString + " OR [Code]=='" + _stringArrayOCList[i] + "'";
                                        }
                                    }
                                }
                                _fullString = _beginString + _endString;

                                if (_fullString != null && _fullString != "")
                                {
                                    _availablePicking = new XPCollection<Picking>(Session, CriteriaOperator.Parse(_fullString));
                                }
                            }
                        }
                        
                    }
                }

                return _availablePicking;
            }
        }

        [ImmediatePostData()]
        [DataSourceProperty("AvailablePicking")]
        [Appearance("ReceivableTransaction2PickingClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Picking Picking
        {
            get { return _picking; }
            set { SetPropertyValue("Picking", ref _picking, value); }
        }

        [Appearance("ReceivableTransaction2GrandTotalAmountClose", Enabled = false)]
        public double GrandTotalAmount
        {
            get { return _grandTotalAmount; }
            set { SetPropertyValue("GrandTotalAmount", ref _grandTotalAmount, value); }
        }

        [Appearance("ReceivableTransaction2GrandTotalAmountDNClose", Enabled = false)]
        public double GrandTotalAmountDN
        {
            get { return _grandTotalAmountDN; }
            set { SetPropertyValue("GrandTotalAmountDN", ref _grandTotalAmountDN, value); }
        }

        [Appearance("ReceivableTransaction2GrandTotalAmountCNClose", Enabled = false)]
        public double GrandTotalAmountCN
        {
            get { return _grandTotalAmountCN; }
            set { SetPropertyValue("GrandTotalAmountCN", ref _grandTotalAmountCN, value); }
        }

        [Appearance("ReceivableTransaction2GrandTotalPlanClose", Enabled = false)]
        public double GrandTotalPlan
        {
            get { return _grandTotalPlan; }
            set { SetPropertyValue("GrandTotalPlan", ref _grandTotalPlan, value); }
        }

        [Appearance("ReceivableTransaction2StatusClose", Enabled = false)]
        public Status Status
        {
            get { return _status; }
            set { SetPropertyValue("Status", ref _status, value); }
        }

        [ModelDefault("DisplayFormat", "{0:dd/MM/yyyy}")]
        [ModelDefault("EditMask", "dd/MM/yyyy")]
        [Appearance("ReceivableTransaction2StatusDateClose", Enabled = false)]
        public DateTime StatusDate
        {
            get { return _statusDate; }
            set { SetPropertyValue("StatusDate", ref _statusDate, value); }
        }

        [Association("ReceivableTransaction2-ReceivableTransactionLine2s")]
        public XPCollection<ReceivableTransactionLine2> ReceivableTransactionLine2s
        {
            get { return GetCollection<ReceivableTransactionLine2>("ReceivableTransactionLine2s"); }
        }

        //AuditTrail
        [CollectionOperationSet(AllowAdd = false, AllowRemove = true)]
        public XPCollection<AuditDataItemPersistent> ChangeHistory
        {
            get
            {
                if (changeHistory == null)
                {
                    changeHistory = AuditedObjectWeakReference.GetAuditTrail(Session, this);
                }
                return changeHistory;
            }
        }

        #endregion Field

        //======================================== Code Only ==========================================================

        private void SetVehicle(Employee _locPIC)
        {
            try
            {
                DateTime now = DateTime.Now;
                if (this.Company != null && this.Workplace != null)
                {
                    SalesmanVehicleSetup _locSalesmanVehicleSetup = Session.FindObject<SalesmanVehicleSetup>
                                                        (new GroupOperator(GroupOperatorType.And,
                                                        new BinaryOperator("Company", this.Company),
                                                        new BinaryOperator("Workplace", this.Workplace),
                                                        new BinaryOperator("Salesman", _locPIC),
                                                        new BinaryOperator("Active", true)));
                    if (_locSalesmanVehicleSetup != null)
                    {
                        if (_locSalesmanVehicleSetup.Vehicle != null)
                        {
                            this.Vehicle = _locSalesmanVehicleSetup.Vehicle;
                        }
                        else
                        {
                            this.Vehicle = null;
                        }
                    }
                    else
                    {
                        this.Vehicle = null;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Business Object = ReceivableTransaction2 ", ex.ToString());
            }
        }
    }
}