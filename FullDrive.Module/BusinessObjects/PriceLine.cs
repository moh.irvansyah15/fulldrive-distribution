﻿using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;

using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.ConditionalAppearance;

namespace FullDrive.Module.BusinessObjects
{
    [DefaultClassOptions]
    [DefaultProperty("Code")]
    [NavigationItem("Rate")]
    [RuleCombinationOfPropertiesIsUnique("PriceLineRuleUnique", DefaultContexts.Save, "Code")]
    //[ImageName("BO_Contact")]
    //[DefaultProperty("DisplayMemberNameForLookupEditorsOfThisType")]
    //[DefaultListViewOptions(MasterDetailMode.ListViewOnly, false, NewItemRowPosition.None)]
    //[Persistent("DatabaseTableName")]
    // Specify more UI options using a declarative approach (https://documentation.devexpress.com/#eXpressAppFramework/CustomDocument112701).
    public class PriceLine : BaseObject
    { // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).

        private string _code;
        private Company _company;
        private Workplace _workplace;
        private PriceGroup _priceGroup;
        private string _name;
        private Currency _currency;
        private Item _item;
        private OrderType _itemType;
        private BusinessPartner _principal;
        private Brand _brand;
        private XPCollection<UnitOfMeasure> _availableUnitOfMeasure;
        #region InitialVendor
        private double _qty1a;
        private UnitOfMeasure _dUom1;
        private double _amount1a;
        private double _qty1b;
        private UnitOfMeasure _uom1;
        private double _amount1b;
        #endregion InitialVendor
        #region InitialCustomer
        private double _qty2a;
        private UnitOfMeasure _dUom2;
        private double _amount2a;
        private double _qty2b;
        private UnitOfMeasure _uom2;
        private double _amount2b;
        #endregion InitialCustomer
        private DateTime _startDate;
        private DateTime _endDate;
        private bool _active;
        private Price _price;
        private string _userAccess;
        private GlobalFunction _globFunc;
        //AuditTrail
        private XPCollection<AuditDataItemPersistent> changeHistory;

        public PriceLine(Session session)
            : base(session)
        {
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if (!IsLoading)
            {
                _globFunc = new GlobalFunction();
                #region UserAccess
                _userAccess = SecuritySystem.CurrentUserName;
                UserAccess _locUserAccess = Session.FindObject<UserAccess>(new BinaryOperator("UserName", _userAccess));
                if (_locUserAccess != null)
                {
                    if (_locUserAccess.Employee != null)
                    {
                        if (_locUserAccess.Employee.Company != null && _locUserAccess.Employee.Workplace != null)
                        {
                            this.Code = _globFunc.GetNumberingUnlockOptimisticRecordBasedOrganization(this.Session.DataLayer, ObjectList.PriceLine, _locUserAccess.Employee.Company, _locUserAccess.Employee.Workplace);
                        }
                        else
                        {
                            this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.PriceLine);
                        }
                    }
                }
                #endregion UserAccess
                this.Qty1a = 1;
                this.Qty1b = 1;
                this.Qty2a = 1;
                this.Qty2b = 1;
            }
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).
        }

        protected override void OnSaving()
        {
            base.OnSaving();
            if (this.Active == true)
            {
                CheckActiveSystem();
            }
        }

        #region Field

        [RuleRequiredField(DefaultContexts.Save)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue("Code", ref _code, value); }
        }

        [RuleRequiredField(DefaultContexts.Save)]
        [DataSourceCriteria("Active = true")]
        [Appearance("PriceLineCompanyEnabled", Enabled = false)]
        public Company Company
        {
            get { return _company; }
            set { SetPropertyValue("Company", ref _company, value); }
        }

        [RuleRequiredField(DefaultContexts.Save)]
        [DataSourceCriteria("Active = true")]
        [Appearance("PriceLineWorkplaceEnabled", Enabled = false)]
        public Workplace Workplace
        {
            get { return _workplace; }
            set { SetPropertyValue("Workplace", ref _workplace, value); }
        }

        public PriceGroup PriceGroup
        {
            get { return _priceGroup; }
            set { SetPropertyValue("PriceGroup", ref _priceGroup, value); }
        }

        public string Name
        {
            get { return _name; }
            set { SetPropertyValue("Name", ref _name, value); }
        }

        public Currency Currency
        {
            get { return _currency; }
            set { SetPropertyValue("Currency", ref _currency, value); }
        }

        [ImmediatePostData()]
        [DataSourceCriteria("Active = true")]
        public Item Item
        {
            get { return _item; }
            set {
                SetPropertyValue("Item", ref _item, value);
                if(!IsLoading)
                {
                    if(this.Item != null)
                    {
                        this.ItemType = this._item.ItemType;
                        if (this._item.BasedUOM != null)
                        {
                            this.DUOM1 = this._item.BasedUOM;
                            this.DUOM2 = this._item.BasedUOM;
                        }
                        else
                        {
                            this.DUOM1 = null;
                            this.DUOM2 = null;
                        }
                        if (this._item.Brand != null)
                        {
                            this.Brand = this._item.Brand;
                        }
                        else
                        {
                            this.Brand = null;
                        }
                        if (this._item.Principal != null)
                        {
                            this.Principal = this._item.Principal;
                        }
                        else
                        {
                            this.Principal = null;
                        }
                    }
                    else
                    {
                        this.DUOM1 = null;
                        this.DUOM2 = null;
                        this.Brand = null;
                        this.Principal = null;
                    } 
                }
            }
        }

        public OrderType ItemType
        {
            get { return _itemType; }
            set { SetPropertyValue("ItemType", ref _itemType, value); }
        }

        public BusinessPartner Principal
        {
            get { return _principal; }
            set { SetPropertyValue("Principal", ref _principal, value); }
        }

        public Brand Brand
        {
            get { return _brand; }
            set { SetPropertyValue("Brand", ref _brand, value); }
        }

        [Browsable(false)]
        public XPCollection<UnitOfMeasure> AvailableUnitOfMeasure
        {
            get
            {
                string _beginString = null;
                string _endString = null;
                string _fullString = null;
                if (Item == null)
                {
                    _availableUnitOfMeasure = new XPCollection<UnitOfMeasure>(Session);
                }
                else
                {
                    List<string> _stringUOM = new List<string>();

                    XPCollection<ItemUnitOfMeasure> _locItemUnitOfMeasures = new XPCollection<ItemUnitOfMeasure>
                                                                              (Session, new GroupOperator(GroupOperatorType.And,
                                                                               new BinaryOperator("Item", this.Item),
                                                                               new BinaryOperator("Active", true)));

                    if (_locItemUnitOfMeasures != null && _locItemUnitOfMeasures.Count() > 0)
                    {
                        foreach (ItemUnitOfMeasure _locItemUnitOfMeasure in _locItemUnitOfMeasures)
                        {
                            _stringUOM.Add(_locItemUnitOfMeasure.UOM.Code);
                        }
                    }

                    IEnumerable<string> _stringArrayUOMDistinct = _stringUOM.Distinct();
                    string[] _stringArrayUOMList = _stringArrayUOMDistinct.ToArray();
                    if (_stringArrayUOMList.Length == 1)
                    {
                        for (int i = 0; i < _stringArrayUOMList.Length; i++)
                        {
                            UnitOfMeasure _locUOM = Session.FindObject<UnitOfMeasure>(new BinaryOperator("Code", _stringArrayUOMList[i]));
                            if (_locUOM != null)
                            {
                                if (i == 0)
                                {
                                    _beginString = "[Code]=='" + _locUOM.Code + "'";
                                }
                            }
                        }
                    }
                    else if (_stringArrayUOMList.Length > 1)
                    {
                        for (int i = 0; i < _stringArrayUOMList.Length; i++)
                        {
                            UnitOfMeasure _locUOM = Session.FindObject<UnitOfMeasure>(new BinaryOperator("Code", _stringArrayUOMList[i]));
                            if (_locUOM != null)
                            {
                                if (i == 0)
                                {
                                    _beginString = "[Code]=='" + _locUOM.Code + "'";
                                }
                                else
                                {
                                    _endString = _endString + " OR [Code]=='" + _locUOM.Code + "'";
                                }
                            }
                        }
                    }
                    _fullString = _beginString + _endString;

                    if (_fullString != null)
                    {
                        _availableUnitOfMeasure = new XPCollection<UnitOfMeasure>(Session, CriteriaOperator.Parse(_fullString));
                    }

                }
                return _availableUnitOfMeasure;

            }
        }

        #region FromVendor

        public double Qty1a
        {
            get { return _qty1a; }
            set { SetPropertyValue("Qty1a", ref _qty1a, value); }
        }

        [DataSourceCriteria("Active = true")]
        public UnitOfMeasure DUOM1
        {
            get { return _dUom1; }
            set { SetPropertyValue("DUOM1", ref _dUom1, value); }
        }

        [ImmediatePostData()]
        public double Amount1a
        {
            get { return _amount1a; }
            set
            {
                SetPropertyValue("Amount1a", ref _amount1a, value);
                if (!IsLoading)
                {
                    _globFunc = new GlobalFunction();

                    if (this._amount1a > 0)
                    {
                        if (_globFunc.GetRoundingList(Session, ObjectList.PriceLine, FieldName.Amount1a) == true)
                        {
                            this._amount1a = _globFunc.GetRoundUp(Session, this._amount1a, ObjectList.PriceLine, FieldName.Amount1a);
                        }
                    }
                }
            }
        }

        public double Qty1b
        {
            get { return _qty1b; }
            set { SetPropertyValue("Qty1b", ref _qty1b, value); }
        }

        [DataSourceProperty("AvailableUnitOfMeasure", DataSourcePropertyIsNullMode.SelectAll)]
        [DataSourceCriteria("Active = true")]
        public UnitOfMeasure UOM1
        {
            get { return _uom1; }
            set { SetPropertyValue("UOM1", ref _uom1, value); }
        }

        [ImmediatePostData()]
        public double Amount1b
        {
            get { return _amount1b; }
            set
            {
                SetPropertyValue("Amount1b", ref _amount1b, value);
                if (!IsLoading)
                {
                    _globFunc = new GlobalFunction();

                    if (this._amount1b > 0)
                    {
                        if (_globFunc.GetRoundingList(Session, ObjectList.PriceLine, FieldName.Amount1b) == true)
                        {
                            this._amount1b = _globFunc.GetRoundUp(Session, this._amount1b, ObjectList.PriceLine, FieldName.Amount1b);
                        }
                    }
                }
            }
        }

        #endregion FromVendor

        #region ForCustomer

        public double Qty2a
        {
            get { return _qty2a; }
            set { SetPropertyValue("Qty2a", ref _qty2a, value); }
        }

        [DataSourceCriteria("Active = true")]
        public UnitOfMeasure DUOM2
        {
            get { return _dUom2; }
            set { SetPropertyValue("DUOM2", ref _dUom2, value); }
        }

        [ImmediatePostData()]
        public double Amount2a
        {
            get { return _amount2a; }
            set
            {
                SetPropertyValue("Amount2a", ref _amount2a, value);
                if (!IsLoading)
                {
                    _globFunc = new GlobalFunction();

                    if (this._amount2a > 0)
                    {
                        if (_globFunc.GetRoundingList(Session, ObjectList.PriceLine, FieldName.Amount2a) == true)
                        {
                            this._amount2a = _globFunc.GetRoundUp(Session, this._amount2a, ObjectList.PriceLine, FieldName.Amount2a);
                        }
                    }
                }
            }
        }

        public double Qty2b
        {
            get { return _qty2b; }
            set { SetPropertyValue("Qty2b", ref _qty2b, value); }
        }

        [DataSourceProperty("AvailableUnitOfMeasure", DataSourcePropertyIsNullMode.SelectAll)]
        [DataSourceCriteria("Active = true")]
        public UnitOfMeasure UOM2
        {
            get { return _uom2; }
            set { SetPropertyValue("UOM2", ref _uom2, value); }
        }

        [ImmediatePostData()]
        public double Amount2b
        {
            get { return _amount2b; }
            set
            {
                SetPropertyValue("Amount2b", ref _amount2b, value);
                if (!IsLoading)
                {
                    _globFunc = new GlobalFunction();

                    if (this._amount2b > 0)
                    {
                        if (_globFunc.GetRoundingList(Session, ObjectList.PriceLine, FieldName.Amount2b) == true)
                        {
                            this._amount2b = _globFunc.GetRoundUp(Session, this._amount2b, ObjectList.PriceLine, FieldName.Amount2b);
                        }
                    }
                }
            }
        }

        #endregion ForCustomer

        public DateTime StartDate
        {
            get { return _startDate; }
            set { SetPropertyValue("StartDate", ref _startDate, value); }
        }

        public DateTime EndDate
        {
            get { return _endDate; }
            set { SetPropertyValue("EndDate", ref _endDate, value); }
        }

        public bool Active
        {
            get { return _active; }
            set { SetPropertyValue("Active", ref _active, value); }
        }

        [ImmediatePostData]
        [RuleRequiredField(DefaultContexts.Save)]
        [Association("Price-PriceLines")]
        public Price Price
        {
            get { return _price; }
            set
            {
                SetPropertyValue("Price", ref _price, value);
                if (!IsLoading)
                {
                    if (this._price != null)
                    {
                        if (this._price.Item != null)
                        {
                            this.Item = this._price.Item;
                        }
                        if (this._price.Company != null)
                        {
                            this.Company = this._price.Company;
                        }
                        if (this._price.Workplace != null)
                        {
                            this.Workplace = this._price.Workplace;
                        }
                        if (this._price.PriceGroup != null)
                        {
                            this.PriceGroup = this._price.PriceGroup;
                        }
                        if (this._price.Currency != null)
                        {
                            this.Currency = this._price.Currency;
                        }
                    }
                }
            }
        }

        //AuditTrail
        [CollectionOperationSet(AllowAdd = false, AllowRemove = true)]
        public XPCollection<AuditDataItemPersistent> ChangeHistory
        {
            get
            {
                if (changeHistory == null)
                {
                    changeHistory = AuditedObjectWeakReference.GetAuditTrail(Session, this);
                }
                return changeHistory;
            }
        }

        #endregion Field

        //========================================================================= Code Only =================================================================

        #region CodeOnly

        private void CheckActiveSystem()
        {
            try
            {
                if(this.Company != null && this.Workplace != null && this.Item != null)
                {
                    XPCollection<PriceLine> _locPriceLines = new XPCollection<PriceLine>(Session, new GroupOperator(GroupOperatorType.And,
                                                                            new BinaryOperator("This", this, BinaryOperatorType.NotEqual)));
                    if (_locPriceLines != null && _locPriceLines.Count() > 0)
                    {
                        foreach(PriceLine _locPriceLine in _locPriceLines)
                        {
                            if(_locPriceLine.Company == this.Company && _locPriceLine.Workplace == this.Workplace && _locPriceLine.Item == this.Item && _locPriceLine.PriceGroup == this.PriceGroup && this.Active == true)
                            {
                                _locPriceLine.Active = false;
                                _locPriceLine.Save();
                            }
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Business Object = PriceLine", ex.ToString());
            }
        }

        #endregion CodeOnly
    }
}