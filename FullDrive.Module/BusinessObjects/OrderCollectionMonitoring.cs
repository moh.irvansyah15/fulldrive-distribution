﻿using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;

using DevExpress.ExpressApp.Editors;
using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.ConditionalAppearance;

namespace FullDrive.Module.BusinessObjects
{
    [DefaultClassOptions]
    [DefaultProperty("Code")]
    [NavigationItem("Finance & Accounting")]
    [RuleCombinationOfPropertiesIsUnique("OrderCollectionMonitoringRuleUnique", DefaultContexts.Save, "Code")]
    //[ImageName("BO_Contact")]
    //[DefaultProperty("DisplayMemberNameForLookupEditorsOfThisType")]
    //[DefaultListViewOptions(MasterDetailMode.ListViewOnly, false, NewItemRowPosition.None)]
    //[Persistent("DatabaseTableName")]
    // Specify more UI options using a declarative approach (https://documentation.devexpress.com/#eXpressAppFramework/CustomDocument112701).
    public class OrderCollectionMonitoring : BaseObject
    { // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).
        private bool _activationPosting;
        private string _code;
        private bool _select;
        private OrderCollection _orderCollection;
        private OrderCollectionLine _orderCollectionLine;
        private ReceivableTransaction _receivableTransaction;
        private ReceivableTransaction2 _receivableTransaction2;
        #region InitialOrganization
        private Company _company;
        private Workplace _workplace;
        private Division _division;
        private Department _department;
        private Section _section;
        private Employee _employee;
        private Division _div;
        private Department _dept;
        private Section _sect;
        private XPCollection<Employee> _availablePIC;
        private Employee _pic;
        private Employee _collector;
        #endregion InitialOrganization
        private XPCollection<Vehicle> _availableVehicle;
        private Vehicle _vehicle;
        private BusinessPartner _customer;
        private XPCollection<SalesInvoice> _availableSalesInvoice;
        private SalesInvoice _salesInvoice;
        private Currency _currency;
        private PriceGroup _priceGroup;
        private PaymentMethodType _paymentMethodType;
        private PaymentMethod _paymentMethod;
        private PaymentType _paymentType;
        private bool _openDueDate;
        private string _chequeNo;
        private DateTime _estimatedDate;
        private DateTime _actualDate;
        private DateTime _dueDate;
        private double _totAmount;
        private double _totTaxAmount;
        private double _totDiscAmount;
        private double _amount;
        private double _amountDN;
        private double _amountCN;
        private double _amountColl;
        private TermOfPayment _top;
        private CollectStatus _collectStatus;
        private DateTime _collectStatusDate;
        private Status _status;
        private DateTime _statusDate;
        private int _postedCount;
        private DateTime _pickingDate;
        private Picking _picking;
        private PickingMonitoring _pickingMonitoring;
        private string _localUserAccess;
        private GlobalFunction _globFunc;
        //AuditTrail
        private XPCollection<AuditDataItemPersistent> changeHistory;

        public OrderCollectionMonitoring(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if (!IsLoading)
            {
                if (Session.IsNewObject(this))
                {
                    _globFunc = new GlobalFunction();
                    DateTime now = DateTime.Now;
                    #region UserAccess
                    _localUserAccess = SecuritySystem.CurrentUserName;
                    UserAccess _locUserAccess = Session.FindObject<UserAccess>(new BinaryOperator("UserName", _localUserAccess));
                    if (_locUserAccess != null)
                    {
                        if (_locUserAccess.Employee != null)
                        {
                            if (_locUserAccess.Employee.Company != null && _locUserAccess.Employee.Workplace != null)
                            {
                                this.Code = _globFunc.GetNumberingUnlockOptimisticRecordBasedOrganization(this.Session.DataLayer, ObjectList.OrderCollectionMonitoring, _locUserAccess.Employee.Company, _locUserAccess.Employee.Workplace);
                            }
                            else
                            {
                                this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.OrderCollectionMonitoring);
                            }
                        }
                    }
                    #endregion UserAccess
                    this.Status = CustomProcess.Status.Open;
                    this.StatusDate = now;
                    this.Select = true;
                }
            }
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).
        }

        #region Field

        [Browsable(false)]
        public bool ActivationPosting
        {
            get { return _activationPosting; }
            set { SetPropertyValue("ActivationPosting", ref _activationPosting, value); }
        }

        [RuleRequiredField(DefaultContexts.Save)]
        [Appearance("OrderCollectionMonitoringCodeClose", Enabled = false)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue("Code", ref _code, value); }
        }

        public bool Select
        {
            get { return _select; }
            set { SetPropertyValue("Select", ref _select, value); }
        }

        [Appearance("OrderCollectionMonitoringOrderCollectionClose", Enabled = false)]
        public OrderCollection OrderCollection
        {
            get { return _orderCollection; }
            set { SetPropertyValue("OrderCollection", ref _orderCollection, value); }
        }

        [Appearance("OrderCollectionMonitoringOrderCollectionLineClose", Enabled = false)]
        public OrderCollectionLine OrderCollectionLine
        {
            get { return _orderCollectionLine; }
            set { SetPropertyValue("OrderCollectionLine", ref _orderCollectionLine, value); }
        }

        [Appearance("OrderCollectionMonitoringReceivableTransactionClose", Enabled = false)]
        public ReceivableTransaction ReceivableTransaction
        {
            get { return _receivableTransaction; }
            set { SetPropertyValue("ReceivableTransaction", ref _receivableTransaction, value); }
        }

        [Appearance("OrderCollectionMonitoringReceivableTransaction2Close", Enabled = false)]
        public ReceivableTransaction2 ReceivableTransaction2
        {
            get { return _receivableTransaction2; }
            set { SetPropertyValue("ReceivableTransaction2", ref _receivableTransaction2, value); }
        }

        #region Organization

        [Appearance("OrderCollectionMonitoringCompanyClose", Enabled = false)]
        public Company Company
        {
            get { return _company; }
            set { SetPropertyValue("Company", ref _company, value); }
        }

        [Appearance("OrderCollectionMonitoringWorkplaceClose", Enabled = false)]
        public Workplace Workplace
        {
            get { return _workplace; }
            set { SetPropertyValue("Workplace", ref _workplace, value); }
        }

        [Browsable(false)]
        public Division Division
        {
            get { return _division; }
            set { SetPropertyValue("Division", ref _division, value); }
        }

        [Browsable(false)]
        public Department Department
        {
            get { return _department; }
            set { SetPropertyValue("Department", ref _department, value); }
        }

        [Browsable(false)]
        public Section Section
        {
            get { return _section; }
            set { SetPropertyValue("Section", ref _section, value); }
        }

        [Browsable(false)]
        public Employee Employee
        {
            get { return _employee; }
            set { SetPropertyValue("Employee", ref _employee, value); }
        }

        [ImmediatePostData()]
        [DataSourceCriteria("Company = '@This.Company' And Active = true")]
        [Appearance("OrderCollectionMonitoringDivClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Division Div
        {
            get { return _div; }
            set { SetPropertyValue("Div", ref _div, value); }
        }

        [ImmediatePostData()]
        [DataSourceCriteria("Company = '@This.Company' And Division = '@This.Div' And Active = true")]
        [Appearance("OrderCollectionMonitoringDeptClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Department Dept
        {
            get { return _dept; }
            set { SetPropertyValue("Dept", ref _dept, value); }
        }

        [ImmediatePostData()]
        [DataSourceCriteria("Company = '@This.Company' And Department = '@This.Dept' And Active = true")]
        [Appearance("OrderCollectionMonitoringSectClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Section Sect
        {
            get { return _sect; }
            set { SetPropertyValue("Sect", ref _sect, value); }
        }

        [Browsable(false)]
        public XPCollection<Employee> AvailablePIC
        {
            get
            {
                XPCollection<Employee> _locPICs = null;
                if (this.Company != null && this.Workplace != null)
                {
                    #region DivDeptSect
                    if (this.Div != null && this.Dept != null && this.Sect != null)
                    {
                        _locPICs = new XPCollection<Employee>
                                  (Session, new GroupOperator(GroupOperatorType.And,
                                    new BinaryOperator("Company", this.Company),
                                    new BinaryOperator("Workplace", this.Workplace),
                                    new BinaryOperator("Division", this.Div),
                                    new BinaryOperator("Department", this.Dept),
                                    new BinaryOperator("Section", this.Sect),
                                    new BinaryOperator("Active", true),
                                    new GroupOperator(GroupOperatorType.Or,
                                    new BinaryOperator("SalesRole", SalesRole.TakingOrder),
                                    new BinaryOperator("SalesRole", SalesRole.All))));
                    }
                    else if (this.Div != null && this.Dept != null && this.Sect == null)
                    {
                        _locPICs = new XPCollection<Employee>
                                  (Session, new GroupOperator(GroupOperatorType.And,
                                    new BinaryOperator("Company", this.Company),
                                    new BinaryOperator("Workplace", this.Workplace),
                                    new BinaryOperator("Division", this.Div),
                                    new BinaryOperator("Department", this.Dept),
                                    new BinaryOperator("Active", true),
                                    new GroupOperator(GroupOperatorType.Or,
                                    new BinaryOperator("SalesRole", SalesRole.TakingOrder),
                                    new BinaryOperator("SalesRole", SalesRole.All))));

                    }
                    else if (this.Div != null && this.Dept == null && this.Sect == null)
                    {
                        _locPICs = new XPCollection<Employee>
                                  (Session, new GroupOperator(GroupOperatorType.And,
                                    new BinaryOperator("Company", this.Company),
                                    new BinaryOperator("Workplace", this.Workplace),
                                    new BinaryOperator("Division", this.Div),
                                    new BinaryOperator("Active", true),
                                    new GroupOperator(GroupOperatorType.Or,
                                    new BinaryOperator("SalesRole", SalesRole.TakingOrder),
                                    new BinaryOperator("SalesRole", SalesRole.All))));

                    }
                    else if (this.Div == null && this.Dept == null && this.Sect == null)
                    {
                        _locPICs = new XPCollection<Employee>
                                  (Session, new GroupOperator(GroupOperatorType.And,
                                    new BinaryOperator("Company", this.Company),
                                    new BinaryOperator("Workplace", this.Workplace),
                                    new BinaryOperator("Active", true),
                                    new GroupOperator(GroupOperatorType.Or,
                                    new BinaryOperator("SalesRole", SalesRole.TakingOrder),
                                    new BinaryOperator("SalesRole", SalesRole.All))));

                    }
                    #endregion DivDeptSect
                    if (_locPICs != null && _locPICs.Count() > 0)
                    {
                        _availablePIC = _locPICs;
                    }
                }
                else
                {
                    _availablePIC = new XPCollection<Employee>(Session, new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("Active", true),
                                                            new GroupOperator(GroupOperatorType.Or,
                                                            new BinaryOperator("SalesRole", SalesRole.TakingOrder),
                                                            new BinaryOperator("SalesRole", SalesRole.All))));
                }

                return _availablePIC;

            }
        }

        [ImmediatePostData()]
        [DataSourceProperty("AvailablePIC")]
        [Appearance("OrderCollectionMonitoringPICClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Employee PIC
        {
            get { return _pic; }
            set
            {
                SetPropertyValue("Salesman", ref _pic, value);
                if (!IsLoading)
                {
                    if (this._pic != null)
                    {
                        SetVehicle(_pic);
                    }
                    else
                    {
                        this.Vehicle = null;
                    }
                }
            }
        }

        [ImmediatePostData()]
        [DataSourceCriteria("Company = '@This.Company' And Workplace = '@This.Workplace' And Active = true")]
        [Appearance("OrderCollectionMonitoringCollectorClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Employee Collector
        {
            get { return _collector; }
            set { SetPropertyValue("Collector", ref _collector, value); }
        }

        #endregion Organization

        [Browsable(false)]
        public XPCollection<Vehicle> AvailableVehicle
        {
            get
            {
                if (!IsLoading)
                {
                    if (this.Company != null && this.Workplace != null)
                    {
                        string _beginString = null;
                        string _endString = null;
                        string _fullString = null;

                        #region PIC
                        if (this.PIC != null)
                        {
                            List<string> _stringSVS = new List<string>();

                            XPCollection<SalesmanVehicleSetup> _locSalesmanVehicleSetups = new XPCollection<SalesmanVehicleSetup>
                                                                                      (Session, new GroupOperator(GroupOperatorType.And,
                                                                                       new BinaryOperator("Company", this.Company),
                                                                                       new BinaryOperator("Workplace", this.Workplace),
                                                                                       new BinaryOperator("Salesman", this.PIC),
                                                                                       new BinaryOperator("Active", true)));

                            if (_locSalesmanVehicleSetups != null && _locSalesmanVehicleSetups.Count() > 0)
                            {
                                foreach (SalesmanVehicleSetup _locSalesmanVehicleSetup in _locSalesmanVehicleSetups)
                                {
                                    if (_locSalesmanVehicleSetup.Vehicle != null)
                                    {
                                        if (_locSalesmanVehicleSetup.Vehicle.Code != null)
                                        {
                                            _stringSVS.Add(_locSalesmanVehicleSetup.Vehicle.Code);
                                        }
                                    }
                                }
                            }

                            IEnumerable<string> _stringArraySVSDistinct = _stringSVS.Distinct();
                            string[] _stringArraySVSList = _stringArraySVSDistinct.ToArray();
                            if (_stringArraySVSList.Length == 1)
                            {
                                for (int i = 0; i < _stringArraySVSList.Length; i++)
                                {
                                    Vehicle _locV = Session.FindObject<Vehicle>(new BinaryOperator("Code", _stringArraySVSList[i]));
                                    if (_locV != null)
                                    {
                                        if (i == 0)
                                        {
                                            _beginString = "[Code]=='" + _locV.Code + "'";
                                        }
                                    }
                                }
                            }
                            else if (_stringArraySVSList.Length > 1)
                            {
                                for (int i = 0; i < _stringArraySVSList.Length; i++)
                                {
                                    Vehicle _locV = Session.FindObject<Vehicle>(new BinaryOperator("Code", _stringArraySVSList[i]));
                                    if (_locV != null)
                                    {
                                        if (i == 0)
                                        {
                                            _beginString = "[Code]=='" + _locV.Code + "'";
                                        }
                                        else
                                        {
                                            _endString = _endString + " OR [Code]=='" + _locV.Code + "'";
                                        }
                                    }
                                }
                            }
                            _fullString = _beginString + _endString;

                            if (_fullString != null && _fullString != "")
                            {
                                _availableVehicle = new XPCollection<Vehicle>(Session, CriteriaOperator.Parse(_fullString));
                            }
                            else
                            {
                                _availableVehicle = new XPCollection<Vehicle>(Session, new GroupOperator(GroupOperatorType.And,
                                                                                    new BinaryOperator("Company", this.Company),
                                                                                    new BinaryOperator("Workplace", this.Workplace),
                                                                                    new BinaryOperator("Active", true)));
                            }
                        }
                        #endregion PIC
                        #region NonPIC
                        else
                        {
                            _availableVehicle = new XPCollection<Vehicle>(Session, new GroupOperator(GroupOperatorType.And,
                                                                            new BinaryOperator("Company", this.Company),
                                                                            new BinaryOperator("Workplace", this.Workplace),
                                                                            new BinaryOperator("Active", true)));
                        }
                        #endregion NonPIC
                    }
                    else
                    {
                        _availableVehicle = new XPCollection<Vehicle>(Session, new GroupOperator(GroupOperatorType.And,
                                                                            new BinaryOperator("Active", true)));
                    }

                }

                return _availableVehicle;

            }
        }

        [ImmediatePostData()]
        [DataSourceProperty("AvailableVehicle")]
        [Appearance("OrderCollectionMonitoringVehicleClose", Enabled = false)]
        public Vehicle Vehicle
        {
            get { return _vehicle; }
            set { SetPropertyValue("Vehicle", ref _vehicle, value); }
        }

        [Appearance("OrderCollectionMonitoringCustomerClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public BusinessPartner Customer
        {
            get { return _customer; }
            set { SetPropertyValue("Customer", ref _customer, value); }
        }

        [Browsable(false)]
        public XPCollection<SalesInvoice> AvailableSalesInvoice
        {
            get
            {
                if (!IsLoading)
                {
                    if (this.Company != null && this.Workplace != null && this.Picking != null)
                    {
                        #region PIC
                        if (this.PIC != null)
                        {
                            XPQuery<PickingMonitoring> _pickingMonitoringsQuery = new XPQuery<PickingMonitoring>(Session);

                            var _pickingMonitorings = from sim in _pickingMonitoringsQuery
                                                      where ((sim.Status == Status.Open || sim.Status == Status.Posted)
                                                      && sim.Company == this.Company
                                                      && sim.Workplace == this.Workplace
                                                      && sim.PIC == this.PIC
                                                      && sim.Picking == this.Picking)
                                                      group sim by sim.SalesInvoice into g
                                                      select new { SalesInvoice = g.Key };

                            if (_pickingMonitorings != null && _pickingMonitorings.Count() > 0)
                            {
                                string _beginString = null;
                                string _endString = null;
                                string _fullString = null;

                                List<string> _stringPM = new List<string>();

                                foreach (var _pickingMonitoring in _pickingMonitorings)
                                {
                                    if (_pickingMonitoring != null)
                                    {
                                        if (_pickingMonitoring.SalesInvoice.Code != null)
                                        {
                                            _stringPM.Add(_pickingMonitoring.SalesInvoice.Code);
                                        }
                                    }
                                }

                                IEnumerable<string> _stringArrayPMDistinct = _stringPM.Distinct();
                                string[] _stringArrayPMList = _stringArrayPMDistinct.ToArray();
                                if (_stringArrayPMList.Length == 1)
                                {
                                    for (int i = 0; i < _stringArrayPMList.Length; i++)
                                    {
                                        if (i == 0)
                                        {
                                            _beginString = "[Code]=='" + _stringArrayPMList[i] + "'";
                                        }
                                    }
                                }
                                else if (_stringArrayPMList.Length > 1)
                                {
                                    for (int i = 0; i < _stringArrayPMList.Length; i++)
                                    {
                                        if (i == 0)
                                        {
                                            _beginString = "[Code]=='" + _stringArrayPMList[i] + "'";
                                        }
                                        else
                                        {
                                            _endString = _endString + " OR [Code]=='" + _stringArrayPMList[i] + "'";
                                        }
                                    }
                                }
                                _fullString = _beginString + _endString;

                                if (_fullString != null)
                                {
                                    _availableSalesInvoice = new XPCollection<SalesInvoice>(Session, CriteriaOperator.Parse(_fullString));
                                }
                            }
                        }
                        #endregion PIC
                        #region NonPIC
                        else
                        {
                            XPQuery<PickingMonitoring> _pickingMonitoringsQuery = new XPQuery<PickingMonitoring>(Session);

                            var _pickingMonitorings = from sim in _pickingMonitoringsQuery
                                                      where ((sim.Status == Status.Open || sim.Status == Status.Posted)
                                                      && sim.Company == this.Company
                                                      && sim.Workplace == this.Workplace
                                                      && sim.Picking == this.Picking)
                                                      group sim by sim.SalesInvoice into g
                                                      select new { SalesInvoice = g.Key };

                            if (_pickingMonitorings != null && _pickingMonitorings.Count() > 0)
                            {
                                string _beginString = null;
                                string _endString = null;
                                string _fullString = null;

                                List<string> _stringPM = new List<string>();

                                foreach (var _pickingMonitoring in _pickingMonitorings)
                                {
                                    if (_pickingMonitoring != null)
                                    {
                                        if (_pickingMonitoring.SalesInvoice.Code != null)
                                        {
                                            _stringPM.Add(_pickingMonitoring.SalesInvoice.Code);
                                        }
                                    }
                                }

                                IEnumerable<string> _stringArrayPMDistinct = _stringPM.Distinct();
                                string[] _stringArrayPMList = _stringArrayPMDistinct.ToArray();
                                if (_stringArrayPMList.Length == 1)
                                {
                                    for (int i = 0; i < _stringArrayPMList.Length; i++)
                                    {
                                        if (i == 0)
                                        {
                                            _beginString = "[Code]=='" + _stringArrayPMList[i] + "'";
                                        }
                                    }
                                }
                                else if (_stringArrayPMList.Length > 1)
                                {
                                    for (int i = 0; i < _stringArrayPMList.Length; i++)
                                    {
                                        if (i == 0)
                                        {
                                            _beginString = "[Code]=='" + _stringArrayPMList[i] + "'";
                                        }
                                        else
                                        {
                                            _endString = _endString + " OR [Code]=='" + _stringArrayPMList[i] + "'";
                                        }
                                    }
                                }
                                _fullString = _beginString + _endString;

                                if (_fullString != null)
                                {
                                    _availableSalesInvoice = new XPCollection<SalesInvoice>(Session, CriteriaOperator.Parse(_fullString));
                                }
                            }
                        }
                        #endregion NonPIC
                    }
                }

                return _availableSalesInvoice;
            }
        }

        [DataSourceProperty("AvailableSalesInvoice")]
        [Appearance("OrderCollectionMonitoringSalesInvoiceClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public SalesInvoice SalesInvoice
        {
            get { return _salesInvoice; }
            set { SetPropertyValue("SalesInvoice", ref _salesInvoice, value); }
        }

        [Appearance("OrderCollectionMonitoringCurrencyClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Currency Currency
        {
            get { return _currency; }
            set { SetPropertyValue("Currency", ref _currency, value); }
        }

        [Appearance("OrderCollectionMonitoringPriceGroupClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public PriceGroup PriceGroup
        {
            get { return _priceGroup; }
            set { SetPropertyValue("PriceGroup", ref _priceGroup, value); }
        }

        [VisibleInListView(false)]
        [Appearance("OrderCollectionMonitoringPaymentMethodTypeClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public PaymentMethodType PaymentMethodType
        {
            get { return _paymentMethodType; }
            set { SetPropertyValue("PaymentMethodType", ref _paymentMethodType, value); }
        }

        [VisibleInListView(false)]
        [Appearance("OrderCollectionMonitoringPaymentMethodClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public PaymentMethod PaymentMethod
        {
            get { return _paymentMethod; }
            set { SetPropertyValue("PaymentMethod", ref _paymentMethod, value); }
        }

        [Appearance("OrderCollectionMonitoringPaymentTypeClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public PaymentType PaymentType
        {
            get { return _paymentType; }
            set
            {
                SetPropertyValue("PaymentType", ref _paymentType, value);
                if (!IsLoading)
                {
                    if (this._paymentType == PaymentType.Cheque)
                    {
                        this.OpenDueDate = true;
                    }
                    else
                    {
                        this.OpenDueDate = false;
                    }
                }
            }
        }

        [Browsable(false)]
        [ImmediatePostData()]
        public bool OpenDueDate
        {
            get { return _openDueDate; }
            set { SetPropertyValue("OpenDueDate", ref _openDueDate, value); }
        }

        [Appearance("OrderCollectionMonitoringChequeNoClose1", Criteria = "OpenDueDate = true", Enabled = true)]
        [Appearance("OrderCollectionMonitoringChequeNoClose2", Criteria = "OpenDueDate = false", Enabled = false)]
        [Appearance("OrderCollectionMonitoringChequeNoClose3", Criteria = "ActivationPosting = true", Enabled = false)]
        public string ChequeNo
        {
            get { return _chequeNo; }
            set { SetPropertyValue("ChequeNo", ref _chequeNo, value); }
        }

        [ModelDefault("DisplayFormat", "{0:dd/MM/yyyy}")]
        [ModelDefault("EditMask", "dd/MM/yyyy")]
        [Appearance("OrderCollectionMonitoringEstimatedDateClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public DateTime EstimatedDate
        {
            get { return _estimatedDate; }
            set { SetPropertyValue("EstimatedDate", ref _estimatedDate, value); }
        }

        [ModelDefault("DisplayFormat", "{0:dd/MM/yyyy}")]
        [ModelDefault("EditMask", "dd/MM/yyyy")]
        [Appearance("OrderCollectionMonitoringActualDateClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public DateTime ActualDate
        {
            get { return _actualDate; }
            set { SetPropertyValue("ActualDate", ref _actualDate, value); }
        }

        [ModelDefault("DisplayFormat", "{0:dd/MM/yyyy}")]
        [ModelDefault("EditMask", "dd/MM/yyyy")]
        [Appearance("OrderCollectionMonitoringDueDateClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public DateTime DueDate
        {
            get { return _dueDate; }
            set { SetPropertyValue("DueDate", ref _dueDate, value); }
        }

        [Appearance("OrderCollectionMonitoringTotAmountClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public double TotAmount
        {
            get { return _totAmount; }
            set { SetPropertyValue("TotAmount", ref _totAmount, value); }
        }

        [Appearance("OrderCollectionMonitoringTotTaxAmountClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public double TotTaxAmount
        {
            get { return _totTaxAmount; }
            set { SetPropertyValue("TotTaxAmount", ref _totTaxAmount, value); }
        }

        [Appearance("OrderCollectionMonitoringTotDiscAmountClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public double TotDiscAmount
        {
            get { return _totDiscAmount; }
            set { SetPropertyValue("TotDiscAmount", ref _totDiscAmount, value); }
        }

        [ImmediatePostData()]
        [Appearance("OrderCollectionMonitoringAmountClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public double Amount
        {
            get { return _amount; }
            set {
                SetPropertyValue("Amount", ref _amount, value);
                if (!IsLoading)
                {
                    SetTotalAmountCollection();
                }
            }
        }

        [ImmediatePostData()]
        [Appearance("OrderCollectionMonitoringAmountDNClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public double AmountDN
        {
            get { return _amountDN; }
            set
            {
                SetPropertyValue("AmountDN", ref _amountDN, value);
                if (!IsLoading)
                {
                    SetTotalAmountCollection();
                }
            }
        }

        [ImmediatePostData()]
        [Appearance("OrderCollectionMonitoringAmountCNClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public double AmountCN
        {
            get { return _amountCN; }
            set
            {
                SetPropertyValue("AmountCN", ref _amountCN, value);
                if (!IsLoading)
                {
                    SetTotalAmountCollection();
                }
            }
        }

        [Appearance("OrderCollectionMonitoringAmountCollClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public double AmountColl
        {
            get { return _amountColl; }
            set { SetPropertyValue("AmountColl", ref _amountColl, value); }
        }

        [Appearance("OrderCollectionMonitoringTOPClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public TermOfPayment TOP
        {
            get { return _top; }
            set { SetPropertyValue("TOP", ref _top, value); }
        }

        [Appearance("OrderCollectionMonitoringCollectStatusClose", Enabled = false)]
        public CollectStatus CollectStatus
        {
            get { return _collectStatus; }
            set { SetPropertyValue("CollectStatus", ref _collectStatus, value); }
        }

        [ModelDefault("DisplayFormat", "{0:dd/MM/yyyy}")]
        [ModelDefault("EditMask", "dd/MM/yyyy")]
        [Appearance("OrderCollectionMonitoringCollectStatusDateClose", Enabled = false)]
        public DateTime CollectStatusDate
        {
            get { return _collectStatusDate; }
            set { SetPropertyValue("CollectStatusDate", ref _collectStatusDate, value); }
        }

        [Appearance("OrderCollectionMonitoringStatusEnabled", Enabled = false)]
        public Status Status
        {
            get { return _status; }
            set { SetPropertyValue("Status", ref _status, value); }
        }

        [ModelDefault("DisplayFormat", "{0:dd/MM/yyyy}")]
        [ModelDefault("EditMask", "dd/MM/yyyy")]
        [Appearance("OrderCollectionMonitoringStatusDateEnabled", Enabled = false)]
        public DateTime StatusDate
        {
            get { return _statusDate; }
            set { SetPropertyValue("StatusDate", ref _statusDate, value); }
        }

        [Appearance("OrderCollectionMonitoringPostedCountEnabled", Enabled = false)]
        public int PostedCount
        {
            get { return _postedCount; }
            set { SetPropertyValue("PostedCount", ref _postedCount, value); }
        }

        [Appearance("OrderCollectionMonitoringPickingDateEnabled", Enabled = false)]
        public DateTime PickingDate
        {
            get { return _pickingDate; }
            set { SetPropertyValue("PickingDate", ref _pickingDate, value); }

        }

        [Appearance("OrderCollectionMonitoringPickingEnabled", Enabled = false)]
        public Picking Picking
        {
            get { return _picking; }
            set { SetPropertyValue("Picking", ref _picking, value); }
        }

        [Appearance("OrderCollectionMonitoringPickingMonitoringEnabled", Enabled = false)]
        public PickingMonitoring PickingMonitoring
        {
            get { return _pickingMonitoring; }
            set { SetPropertyValue("PickingMonitoring", ref _pickingMonitoring, value); }
        }

        //AuditTrail
        [CollectionOperationSet(AllowAdd = false, AllowRemove = true)]
        public XPCollection<AuditDataItemPersistent> ChangeHistory
        {
            get
            {
                if (changeHistory == null)
                {
                    changeHistory = AuditedObjectWeakReference.GetAuditTrail(Session, this);
                }
                return changeHistory;
            }
        }

        #endregion Field

        //============================================================== Code Only ===============================================

        #region CodeOnly

        private void SetVehicle(Employee _locPIC)
        {
            try
            {
                DateTime now = DateTime.Now;
                if (this.Company != null && this.Workplace != null)
                {
                    SalesmanVehicleSetup _locSalesmanVehicleSetup = Session.FindObject<SalesmanVehicleSetup>
                                                        (new GroupOperator(GroupOperatorType.And,
                                                        new BinaryOperator("Company", this.Company),
                                                        new BinaryOperator("Workplace", this.Workplace),
                                                        new BinaryOperator("Salesman", _locPIC),
                                                        new BinaryOperator("Active", true)));
                    if (_locSalesmanVehicleSetup != null)
                    {
                        if (_locSalesmanVehicleSetup.Vehicle != null)
                        {
                            this.Vehicle = _locSalesmanVehicleSetup.Vehicle;
                        }
                        else
                        {
                            this.Vehicle = null;
                        }
                    }
                    else
                    {
                        this.Vehicle = null;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Business Object = OrderCollection ", ex.ToString());
            }
        }

        private void SetTotalAmountCollection()
        {
            try
            {
                _globFunc = new GlobalFunction();

                if (_amount >= 0 || _amountCN >= 0 || _amountDN >= 0)
                {
                    if (_globFunc.GetRoundingList(Session, ObjectList.OrderCollectionLine, FieldName.TAmount) == true)
                    {
                        this.AmountColl = _globFunc.GetRoundUp(Session, ((this.Amount + _amountDN) - this.AmountCN), ObjectList.SalesInvoiceLine, FieldName.TAmount);
                    }
                    else
                    {
                        this.AmountColl = (this.Amount + _amountDN) - this.AmountCN;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = OrderCollectionMonitoring " + ex.ToString());
            }
        }

        #endregion CodeOnly

    }
}