﻿using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;

using DevExpress.ExpressApp.ConditionalAppearance;
using DevExpress.ExpressApp.SystemModule;
using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.Editors;

namespace FullDrive.Module.BusinessObjects
{
    [DefaultClassOptions]
    [DefaultProperty("Code")]
    [NavigationItem("Inventory")]
    [RuleCombinationOfPropertiesIsUnique("InventoryTransferOutLineRuleUnique", DefaultContexts.Save, "Code")]
    //[ImageName("BO_Contact")]
    //[DefaultProperty("DisplayMemberNameForLookupEditorsOfThisType")]
    //[DefaultListViewOptions(MasterDetailMode.ListViewOnly, false, NewItemRowPosition.None)]
    //[Persistent("DatabaseTableName")]
    // Specify more UI options using a declarative approach (https://documentation.devexpress.com/#eXpressAppFramework/CustomDocument112701).
    public class InventoryTransferOutLine : BaseObject
    { // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).
        private bool _activationPosting;
        private int _no;
        private string _code;
        private bool _select;       
        #region InitialOrganization
        private Company _company;
        private Workplace _workplace;
        private Division _division;
        private Department _department;
        private Section _section;
        private Employee _employee;
        #endregion InitialOrganization
        #region InitialLocation
        private LocationType _locationType;
        private StockType _stockType;
        private XPCollection<Location> _availableLocation;
        private Location _location;
        private XPCollection<BinLocation> _availableBinLocation;
        private BinLocation _binLocation;
        private StockGroup _stockGroup;
        private BeginningInventory _begInv;
        #endregion InitialLocation
        private OrderType _salesType;
        private XPCollection<Item> _availableItem;
        private Item _item;
        private Brand _brand;
        private string _description;
        private BeginningInventoryLine _begInvLine;
        private XPCollection<UnitOfMeasure> _availableUnitOfMeasure;
        #region InitialDefaultQty
        private double _dQty;
        private UnitOfMeasure _dUom;
        private double _qty;
        private UnitOfMeasure _uom;
        private double _tQty;
        #endregion InitialDefaultQty        
        private DateTime _estimatedDate;
        private DateTime _actualDate;
        private DateTime _docDate;
        private int _journalMonth;
        private int _journalYear;
        private Status _status;
        private DateTime _statusDate;
        private InventoryTransferOut _inventoryTransferOut;
        private CustomerInvoiceLine _customerInvoiceLine;
        private string _signCode;
        private string _userAccess;
        private GlobalFunction _globFunc;
        //AuditTrail
        private XPCollection<AuditDataItemPersistent> changeHistory;

        public InventoryTransferOutLine(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if (!IsLoading)
            {
                if (Session.IsNewObject(this))
                {
                    _globFunc = new GlobalFunction();
                    DateTime now = DateTime.Now;
                    #region UserAccess
                    _userAccess = SecuritySystem.CurrentUserName;
                    UserAccess _locUserAccess = Session.FindObject<UserAccess>(new BinaryOperator("UserName", _userAccess));
                    if (_locUserAccess != null)
                    {
                        if (_locUserAccess.Employee != null)
                        {
                            if (_locUserAccess.Employee.Company != null && _locUserAccess.Employee.Workplace != null)
                            {
                                this.Code = _globFunc.GetNumberingUnlockOptimisticRecordBasedOrganization(this.Session.DataLayer, ObjectList.InventoryTransferOutLine, _locUserAccess.Employee.Company, _locUserAccess.Employee.Workplace);
                            }
                            else
                            {
                                this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.InventoryTransferOutLine);
                            }
                        }
                    }
                    #endregion UserAccess
                    this.Status = CustomProcess.Status.Open;
                    this.StatusDate = now;
                    this.Select = true;
                    this.StockType = StockType.Good;
                }
            }
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).
        }

        protected override void OnSaving()
        {
            base.OnSaving();
            if (!IsLoading && IsSaving)
            {
                UpdateNo();
            }
        }

        protected override void OnDeleting()
        {
            base.OnDeleting();
            if (!IsLoading)
            {
                RecoveryDeleteNo();
            }
        }

        #region Field

        [Browsable(false)]
        public bool ActivationPosting
        {
            get { return _activationPosting; }
            set { SetPropertyValue("ActivationPosting", ref _activationPosting, value); }
        }

        [Appearance("InventoryTransferOutLineNoEnabled", Enabled = false)]
        public int No
        {
            get { return _no; }
            set { SetPropertyValue("No", ref _no, value); }
        }

        [VisibleInListView(false)]
        [RuleRequiredField(DefaultContexts.Save)]
        [Appearance("InventoryTransferOutLineCodeEnabled", Enabled = false)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue("Code", ref _code, value); }
        }

        [Appearance("InventoryTransferOutLineSelectClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public bool Select
        {
            get { return _select; }
            set { SetPropertyValue("Select", ref _select, value); }
        }

        #region Organization

        [VisibleInListView(false)]
        [Appearance("InventoryTransferOutLineCompanyClose", Enabled = false)]
        public Company Company
        {
            get { return _company; }
            set { SetPropertyValue("Company", ref _company, value); }
        }

        [VisibleInListView(false)]
        [Appearance("InventoryTransferOutLineWorkplaceClose", Enabled = false)]
        public Workplace Workplace
        {
            get { return _workplace; }
            set { SetPropertyValue("Workplace", ref _workplace, value); }
        }

        [Browsable(false)]
        public Division Division
        {
            get { return _division; }
            set { SetPropertyValue("Division", ref _division, value); }
        }

        [Browsable(false)]
        public Department Department
        {
            get { return _department; }
            set { SetPropertyValue("Department", ref _department, value); }
        }

        [Browsable(false)]
        public Section Section
        {
            get { return _section; }
            set { SetPropertyValue("Section", ref _section, value); }
        }

        [Browsable(false)]
        public Employee Employee
        {
            get { return _employee; }
            set { SetPropertyValue("Employee", ref _employee, value); }
        }

        #endregion Organization

        #region Location

        [ImmediatePostData()]
        [Appearance("InventoryTransferOutLineLocationTypeClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public LocationType LocationType
        {
            get { return _locationType; }
            set { SetPropertyValue("LocationType", ref _locationType, value); }
        }

        [Appearance("InventoryTransferOutLineStockTypeClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public StockType StockType
        {
            get { return _stockType; }
            set { SetPropertyValue("StockType", ref _stockType, value); }
        }

        [Browsable(false)]
        public XPCollection<Location> AvailableLocation
        {
            get
            {
                if(!IsLoading)
                {
                    _userAccess = SecuritySystem.CurrentUserName;
                    string _beginString = null;
                    string _endString = null;
                    string _fullString = null;
                    string _locLocationCode = null;
                    UserAccess _locUserAccess = Session.FindObject<UserAccess>(new BinaryOperator("UserName", _userAccess));
                    List<string> _stringLocation = new List<string>();
                    if (_locUserAccess != null)
                    {
                        if (this.InventoryTransferOut != null)
                        {
                            #region InventoryTransferOut
                            if (this.InventoryTransferOut.InventoryMovingType == InventoryMovingType.Deliver)
                            {
                                XPCollection<WarehouseSetupDetail> _locWhsSetupDetails = null;

                                if (this.LocationType == LocationType.None)
                                {
                                    _locWhsSetupDetails = new XPCollection<WarehouseSetupDetail>
                                                        (Session, new GroupOperator(GroupOperatorType.And,
                                                        new BinaryOperator("UserAccess", _locUserAccess),
                                                        new BinaryOperator("Owner", true),
                                                        new BinaryOperator("Active", true)));


                                }
                                else
                                {
                                    _locWhsSetupDetails = new XPCollection<WarehouseSetupDetail>
                                                        (Session, new GroupOperator(GroupOperatorType.And,
                                                        new BinaryOperator("UserAccess", _locUserAccess),
                                                        new BinaryOperator("Owner", true),
                                                        new BinaryOperator("LocationType", this.LocationType),
                                                        new BinaryOperator("Active", true)));

                                }

                                if (_locWhsSetupDetails != null && _locWhsSetupDetails.Count() > 0)
                                {
                                    foreach (WarehouseSetupDetail _locWhsSetupDetail in _locWhsSetupDetails)
                                    {
                                        if (_locWhsSetupDetail.Location != null)
                                        {
                                            if (_locWhsSetupDetail.Location.Code != null)
                                            {
                                                _locLocationCode = _locWhsSetupDetail.Location.Code;
                                                _stringLocation.Add(_locLocationCode);
                                            }
                                        }
                                    }
                                }

                                IEnumerable<string> _stringArrayLocationFromDistinct = _stringLocation.Distinct();
                                string[] _stringArrayLocationFromList = _stringArrayLocationFromDistinct.ToArray();
                                if (_stringArrayLocationFromList.Length == 1)
                                {
                                    for (int i = 0; i < _stringArrayLocationFromList.Length; i++)
                                    {
                                        Location _locLocationFrom = Session.FindObject<Location>(new BinaryOperator("Code", _stringArrayLocationFromList[i]));
                                        if (_locLocationFrom != null)
                                        {
                                            if (i == 0)
                                            {
                                                _beginString = "[Code]=='" + _locLocationFrom.Code + "'";
                                            }
                                        }
                                    }
                                }
                                else if (_stringArrayLocationFromList.Length > 1)
                                {
                                    for (int i = 0; i < _stringArrayLocationFromList.Length; i++)
                                    {
                                        Location _locLocationFrom = Session.FindObject<Location>(new BinaryOperator("Code", _stringArrayLocationFromList[i]));
                                        if (_locLocationFrom != null)
                                        {
                                            if (i == 0)
                                            {
                                                _beginString = "[Code]=='" + _locLocationFrom.Code + "'";
                                            }
                                            else
                                            {
                                                _endString = _endString + " OR [Code]=='" + _locLocationFrom.Code + "'";
                                            }
                                        }
                                    }
                                }
                                _fullString = _beginString + _endString;

                                if (_fullString != null)
                                {
                                    _availableLocation = new XPCollection<Location>(Session, CriteriaOperator.Parse(_fullString));
                                }

                            }
                            #endregion InventoryTransferOut
                        }
                    }
                }
                return _availableLocation;
            }
        }

        [Appearance("InventoryTransferOutLineLocationClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [DataSourceProperty("AvailableLocation")]
        [ImmediatePostData()]
        [DataSourceCriteria("Active = true")]
        public Location Location
        {
            get { return _location; }
            set
            {
                SetPropertyValue("Location", ref _location, value);
                if(!IsLoading)
                {
                    if (this._location != null)
                    {
                        SetBegInv(_location);
                    }
                    else
                    {
                        BegInv = null;
                    }
                }
            }
        }

        [Browsable(false)]
        public XPCollection<BinLocation> AvailableBinLocation
        {
            get
            {
                if(!IsLoading)
                {
                    _userAccess = SecuritySystem.CurrentUserName;
                    string _beginString = null;
                    string _endString = null;
                    string _fullString = null;
                    string _locBinLocationCode = null;
                    UserAccess _locUserAccess = Session.FindObject<UserAccess>(new BinaryOperator("UserName", _userAccess));
                    List<string> _stringBinLocation = new List<string>();
                    if (_locUserAccess != null)
                    {
                        if (this.Location != null)
                        {
                            if (this.InventoryTransferOut != null)
                            {
                                #region InventoryTransferOut
                                if (this.InventoryTransferOut.InventoryMovingType == InventoryMovingType.Deliver)
                                {

                                    XPCollection<WarehouseSetupDetail> _locWhsSetupDetails = new XPCollection<WarehouseSetupDetail>
                                                                                      (Session, new GroupOperator(GroupOperatorType.And,
                                                                                       new BinaryOperator("UserAccess", _locUserAccess),
                                                                                       new BinaryOperator("Location", this.Location),
                                                                                       new BinaryOperator("Owner", true),
                                                                                       new BinaryOperator("Active", true)));

                                    if (_locWhsSetupDetails != null && _locWhsSetupDetails.Count() > 0)
                                    {
                                        foreach (WarehouseSetupDetail _locWhsSetupDetail in _locWhsSetupDetails)
                                        {
                                            if (_locWhsSetupDetail.BinLocation != null)
                                            {
                                                if (_locWhsSetupDetail.BinLocation.Code != null)
                                                {
                                                    _locBinLocationCode = _locWhsSetupDetail.BinLocation.Code;
                                                    _stringBinLocation.Add(_locBinLocationCode);
                                                }
                                            }
                                        }
                                    }

                                    IEnumerable<string> _stringArrayBinLocationFromDistinct = _stringBinLocation.Distinct();
                                    string[] _stringArrayBinLocationFromList = _stringArrayBinLocationFromDistinct.ToArray();
                                    if (_stringArrayBinLocationFromList.Length == 1)
                                    {
                                        for (int i = 0; i < _stringArrayBinLocationFromList.Length; i++)
                                        {
                                            BinLocation _locBinLocationFrom = Session.FindObject<BinLocation>(new BinaryOperator("Code", _stringArrayBinLocationFromList[i]));
                                            if (_locBinLocationFrom != null)
                                            {
                                                if (i == 0)
                                                {
                                                    _beginString = "[Code]=='" + _locBinLocationFrom.Code + "'";
                                                }
                                            }
                                        }
                                    }
                                    else if (_stringArrayBinLocationFromList.Length > 1)
                                    {
                                        for (int i = 0; i < _stringArrayBinLocationFromList.Length; i++)
                                        {
                                            BinLocation _locBinLocationFrom = Session.FindObject<BinLocation>(new BinaryOperator("Code", _stringArrayBinLocationFromList[i]));
                                            if (_locBinLocationFrom != null)
                                            {
                                                if (i == 0)
                                                {
                                                    _beginString = "[Code]=='" + _locBinLocationFrom.Code + "'";
                                                }
                                                else
                                                {
                                                    _endString = _endString + " OR [Code]=='" + _locBinLocationFrom.Code + "'";
                                                }
                                            }
                                        }
                                    }
                                    _fullString = _beginString + _endString;

                                    if (_fullString != null)
                                    {
                                        _availableBinLocation = new XPCollection<BinLocation>(Session, CriteriaOperator.Parse(_fullString));
                                    }
                                }
                                #endregion InventoryTransferOut
                            }
                        }
                    }
                }
                return _availableBinLocation;
            }
        }

        [Appearance("InventoryTransferOutLineBinLocationClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [DataSourceProperty("AvailableBinLocation")]
        [DataSourceCriteria("Active = true")]
        public BinLocation BinLocation
        {
            get { return _binLocation; }
            set { SetPropertyValue("BinLocation", ref _binLocation, value); }
        }

        [Appearance("InventoryTransferOutLineStockGroupClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public StockGroup StockGroup
        {
            get { return _stockGroup; }
            set { SetPropertyValue("StockGroup", ref _stockGroup, value); }
        }

        //[Browsable(false)]
        [ImmediatePostData()]
        [Appearance("TransferOutLineBegInvClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public BeginningInventory BegInv
        {
            get { return _begInv; }
            set { SetPropertyValue("BegInv", ref _begInv, value); }
        }

        #endregion Location

        [VisibleInListView(false)]
        [ImmediatePostData()]
        [Appearance("InventoryTransferOutLineSalesTypeClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public OrderType SalesType
        {
            get { return _salesType; }
            set { SetPropertyValue("SalesType", ref _salesType, value); }
        }

        [Browsable(false)]
        public XPCollection<Item> AvailableItem
        {
            get
            {
                if(!IsLoading)
                {
                    string _beginString = null;
                    string _endString = null;
                    string _fullString = null;
                    if (this.Company != null && this.Workplace != null)
                    {
                        List<string> _stringITM = new List<string>();

                        XPQuery<BeginningInventoryLine> _beginningInventoryLinesQuery = new XPQuery<BeginningInventoryLine>(Session);

                        var _beginningInventoryLines = from bil in _beginningInventoryLinesQuery
                                                       where (bil.Company == this.Company
                                                       && bil.Workplace == this.Workplace
                                                       && bil.Item.ItemType == this.SalesType
                                                       && bil.BeginningInventory == this.BegInv
                                                       && bil.QtyAvailable > 0
                                                       && (bil.QtyAvailable - bil.QtyOnHand) > 0
                                                       && bil.Lock == false
                                                       && bil.Vehicle == null
                                                       && bil.Active == true
                                                       )
                                                       group bil by bil.Item into g
                                                       select new { Item = g.Key };
                        if (_beginningInventoryLines != null && _beginningInventoryLines.Count() > 0)
                        {
                            foreach (var _beginningInventoryLine in _beginningInventoryLines)
                            {
                                _stringITM.Add(_beginningInventoryLine.Item.Code);
                            }
                        }

                        IEnumerable<string> _stringArrayITMDistinct = _stringITM.Distinct();
                        string[] _stringArrayITMList = _stringArrayITMDistinct.ToArray();
                        if (_stringArrayITMList.Length == 1)
                        {
                            for (int i = 0; i < _stringArrayITMList.Length; i++)
                            {
                                Item _locITM = Session.FindObject<Item>(new BinaryOperator("Code", _stringArrayITMList[i]));
                                if (_locITM != null)
                                {
                                    if (i == 0)
                                    {
                                        _beginString = "[Code]=='" + _locITM.Code + "'";
                                    }
                                }
                            }
                        }
                        else if (_stringArrayITMList.Length > 1)
                        {
                            for (int i = 0; i < _stringArrayITMList.Length; i++)
                            {
                                Item _locITM = Session.FindObject<Item>(new BinaryOperator("Code", _stringArrayITMList[i]));
                                if (_locITM != null)
                                {
                                    if (i == 0)
                                    {
                                        _beginString = "[Code]=='" + _locITM.Code + "'";
                                    }
                                    else
                                    {
                                        _endString = _endString + " OR [Code]=='" + _locITM.Code + "'";
                                    }
                                }
                            }
                        }
                        _fullString = _beginString + _endString;

                        if (_fullString != null)
                        {
                            _availableItem = new XPCollection<Item>(Session, CriteriaOperator.Parse(_fullString));
                        }
                    }
                    else
                    {
                        _availableItem = new XPCollection<Item>(Session, new GroupOperator(GroupOperatorType.And,
                                                        new BinaryOperator("Company", this.Company),
                                                        new BinaryOperator("Active", true)));
                    }
                }
                return _availableItem;
            }
        }

        [ImmediatePostData()]
        [DataSourceProperty("AvailableItem", DataSourcePropertyIsNullMode.SelectAll)]
        [Appearance("InventoryTransferOutLineItemClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Item Item
        {
            get { return _item; }
            set
            {
                SetPropertyValue("Item", ref _item, value);
                if (!IsLoading)
                {
                    if (this._item != null)
                    {
                        this.DUOM = this._item.BasedUOM;
                        this.Brand = this._item.Brand;
                        this.Description = GetDescriptions();
                        SetBegInvLine(_item);
                    }
                    else
                    {
                        this.DUOM = null;
                        this.Brand = null;
                        this.Description = null;
                        this.BegInvLine = null;
                    }
                }
            }
        }

        [VisibleInListView(false)]
        [Appearance("InventoryTransferOutLineBrandClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Brand Brand
        {
            get { return _brand; }
            set { SetPropertyValue("Brand", ref _brand, value); }
        }

        [Appearance("InventoryTransferOutLineDescriptionClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [Size(512)]
        public string Description
        {
            get { return _description; }
            set { SetPropertyValue("Description", ref _description, value); }
        }

        //[Browsable(false)]
        [ImmediatePostData()]
        [Appearance("InventoryTransferOutLineBegInvLineClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public BeginningInventoryLine BegInvLine
        {
            get { return _begInvLine; }
            set { SetPropertyValue("BegInvLine", ref _begInvLine, value); }
        }

        [Browsable(false)]
        public XPCollection<UnitOfMeasure> AvailableUnitOfMeasure
        {
            get
            {
                string _beginString = null;
                string _endString = null;
                string _fullString = null;
                if (Item == null)
                {
                    _availableUnitOfMeasure = new XPCollection<UnitOfMeasure>(Session);
                }
                else
                {
                    List<string> _stringUOM = new List<string>();

                    XPCollection<ItemUnitOfMeasure> _locItemUnitOfMeasures = new XPCollection<ItemUnitOfMeasure>
                                                                              (Session, new GroupOperator(GroupOperatorType.And,
                                                                               new BinaryOperator("Item", this.Item),
                                                                               new BinaryOperator("Active", true)));

                    if (_locItemUnitOfMeasures != null && _locItemUnitOfMeasures.Count() > 0)
                    {
                        foreach (ItemUnitOfMeasure _locItemUnitOfMeasure in _locItemUnitOfMeasures)
                        {
                            _stringUOM.Add(_locItemUnitOfMeasure.UOM.Code);
                        }
                    }

                    IEnumerable<string> _stringArrayUOMDistinct = _stringUOM.Distinct();
                    string[] _stringArrayUOMList = _stringArrayUOMDistinct.ToArray();
                    if (_stringArrayUOMList.Length == 1)
                    {
                        for (int i = 0; i < _stringArrayUOMList.Length; i++)
                        {
                            UnitOfMeasure _locUOM = Session.FindObject<UnitOfMeasure>(new BinaryOperator("Code", _stringArrayUOMList[i]));
                            if (_locUOM != null)
                            {
                                if (i == 0)
                                {
                                    _beginString = "[Code]=='" + _locUOM.Code + "'";
                                }
                            }
                        }
                    }
                    else if (_stringArrayUOMList.Length > 1)
                    {
                        for (int i = 0; i < _stringArrayUOMList.Length; i++)
                        {
                            UnitOfMeasure _locUOM = Session.FindObject<UnitOfMeasure>(new BinaryOperator("Code", _stringArrayUOMList[i]));
                            if (_locUOM != null)
                            {
                                if (i == 0)
                                {
                                    _beginString = "[Code]=='" + _locUOM.Code + "'";
                                }
                                else
                                {
                                    _endString = " OR [Code]=='" + _locUOM.Code + "'";
                                }
                            }
                        }
                    }
                    _fullString = _beginString + _endString;

                    if (_fullString != null)
                    {
                        _availableUnitOfMeasure = new XPCollection<UnitOfMeasure>(Session, CriteriaOperator.Parse(_fullString));
                    }

                }
                return _availableUnitOfMeasure;

            }
        }

        #region DefaultQty

        [Appearance("InventoryTransferOutLineDQtyClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [ImmediatePostData()]
        public double DQty
        {
            get { return _dQty; }
            set
            {
                SetPropertyValue("DQty", ref _dQty, value);
                if (!IsLoading)
                {
                    SetTotalQty();
                }
            }
        }

        [Appearance("InventoryTransferOutLineDUOMClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [ImmediatePostData()]
        [DataSourceCriteria("Active = true")]
        public UnitOfMeasure DUOM
        {
            get { return _dUom; }
            set
            {
                SetPropertyValue("DUOM", ref _dUom, value);
                if (!IsLoading)
                {
                    SetTotalQty();
                }
            }
        }

        [Appearance("InventoryTransferOutLineQtyClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [ImmediatePostData()]
        public double Qty
        {
            get { return _qty; }
            set
            {
                SetPropertyValue("Qty", ref _qty, value);
                if (!IsLoading)
                {
                    SetTotalQty();
                }
            }
        }

        [Appearance("InventoryTransferOutLineUOMClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [ImmediatePostData()]
        [DataSourceProperty("AvailableUnitOfMeasure", DataSourcePropertyIsNullMode.SelectAll)]
        [DataSourceCriteria("Active = true")]
        public UnitOfMeasure UOM
        {
            get { return _uom; }
            set
            {
                SetPropertyValue("UOM", ref _uom, value);
                if (!IsLoading)
                {
                    SetTotalQty();
                }
            }
        }

        [Appearance("InventoryTransferOutLineTotalQtyEnabled", Enabled = false)]
        public double TQty
        {
            get { return _tQty; }
            set { SetPropertyValue("TQty", ref _tQty, value); }
        }

        #endregion DefaultQty

        [VisibleInListView(false)]
        [ModelDefault("DisplayFormat", "{0:dd/MM/yyyy}")]
        [ModelDefault("EditMask", "dd/MM/yyyy")]
        [Appearance("InventoryTransferOutLineEstimatedDateEnabled", Enabled = false)]
        public DateTime EstimatedDate
        {
            get { return _estimatedDate; }
            set { SetPropertyValue("EstimatedDate", ref _estimatedDate, value); }
        }

        [ModelDefault("DisplayFormat", "{0:dd/MM/yyyy}")]
        [ModelDefault("EditMask", "dd/MM/yyyy")]
        [Appearance("InventoryTransferOutLineActualDateEnabled", Enabled = false)]
        public DateTime ActualDate
        {
            get { return _actualDate; }
            set { SetPropertyValue("ActualDate", ref _actualDate, value); }
        }

        [Browsable(false)]
        [ImmediatePostData()]
        [ModelDefault("DisplayFormat", "{0:dd/MM/yyyy}")]
        [ModelDefault("EditMask", "dd/MM/yyyy")]
        [Appearance("InventoryTransferOutLineDocDateClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public DateTime DocDate
        {
            get { return _docDate; }
            set
            {
                SetPropertyValue("DocDate", ref _docDate, value);
                if (!IsLoading)
                {
                    this.JournalMonth = this._docDate.Month;
                    this.JournalYear = this._docDate.Year;
                }
            }
        }

        [Browsable(false)]
        public int JournalMonth
        {
            get { return _journalMonth; }
            set { SetPropertyValue("JournalMonth", ref _journalMonth, value); }
        }

        [Browsable(false)]
        public int JournalYear
        {
            get { return _journalYear; }
            set { SetPropertyValue("JournalYear", ref _journalYear, value); }
        }

        [VisibleInListView(false)]
        [Appearance("InventoryTransferOutLineStatusEnabled", Enabled = false)]
        public Status Status
        {
            get { return _status; }
            set { SetPropertyValue("Status", ref _status, value); }
        }

        [VisibleInListView(false)]
        [ModelDefault("DisplayFormat", "{0:dd/MM/yyyy}")]
        [ModelDefault("EditMask", "dd/MM/yyyy")]
        [Appearance("InventoryTransferOutLineStatusDateEnabled", Enabled = false)]
        public DateTime StatusDate
        {
            get { return _statusDate; }
            set { SetPropertyValue("StatusDate", ref _statusDate, value); }
        }

        [VisibleInListView(false)]
        [Appearance("InventoryTransferOutLineSignCodeEnabled", Enabled = false)]
        public string SignCode
        {
            get { return _signCode; }
            set { SetPropertyValue("SignCode", ref _signCode, value); }
        }

        [VisibleInListView(false)]
        [Appearance("InventoryTransferOutLineInventoryTransferEnabled", Enabled = false)]
        [RuleRequiredField(DefaultContexts.Save)]
        [ImmediatePostData()]
        [Association("InventoryTransferOut-InventoryTransferOutLines")]
        public InventoryTransferOut InventoryTransferOut
        {
            get { return _inventoryTransferOut; }
            set
            {
                SetPropertyValue("InventoryTransferOut", ref _inventoryTransferOut, value);
                if (!IsLoading)
                {
                    if (_inventoryTransferOut != null)
                    {
                        if (_inventoryTransferOut.Location != null)
                        {
                            this.Location = _inventoryTransferOut.Location;
                        }
                        if (this._inventoryTransferOut.LocationType != LocationType.None)
                        {
                            this.LocationType = this._inventoryTransferOut.LocationType;
                        }
                        if (this._inventoryTransferOut.StockType != StockType.None)
                        {
                            this.StockType = this._inventoryTransferOut.StockType;
                        }
                        if (_inventoryTransferOut.BegInv != null)
                        {
                            this.BegInv = _inventoryTransferOut.BegInv;
                        }
                        if (this._inventoryTransferOut.Company != null)
                        {
                            this.Company = this._inventoryTransferOut.Company;
                        }
                        if (this._inventoryTransferOut.Workplace != null)
                        {
                            this.Workplace = this._inventoryTransferOut.Workplace;
                        }
                        if (this._inventoryTransferOut.Division != null)
                        {
                            this.Division = this._inventoryTransferOut.Division;
                        }
                        if (this._inventoryTransferOut.Department != null)
                        {
                            this.Department = this._inventoryTransferOut.Department;
                        }
                        if (this._inventoryTransferOut.Section != null)
                        {
                            this.Section = this._inventoryTransferOut.Section;
                        }
                        if (this._inventoryTransferOut.Employee != null)
                        {
                            this.Employee = this._inventoryTransferOut.Employee;
                        }
                        this.EstimatedDate = _inventoryTransferOut.EstimatedDate;
                        this.DocDate = _inventoryTransferOut.DocDate;
                        this.JournalMonth = _inventoryTransferOut.JournalMonth;
                        this.JournalYear = _inventoryTransferOut.JournalYear;
                    }
                }
            }
        }

        [Browsable(false)]
        public CustomerInvoiceLine CustomerInvoiceLine
        {
            get { return _customerInvoiceLine; }
            set { SetPropertyValue("CustomerInvoiceLine", ref _customerInvoiceLine, value); }
        }

        [Association("InventoryTransferOutLine-InventoryTransferOutLots")]
        public XPCollection<InventoryTransferOutLot> InventoryTransferOutLots
        {
            get { return GetCollection<InventoryTransferOutLot>("InventoryTransferOutLots"); }
        }

        //AuditTrail
        [CollectionOperationSet(AllowAdd = false, AllowRemove = true)]
        public XPCollection<AuditDataItemPersistent> ChangeHistory
        {
            get
            {
                if (changeHistory == null)
                {
                    changeHistory = AuditedObjectWeakReference.GetAuditTrail(Session, this);
                }
                return changeHistory;
            }
        }

        #endregion Field

        //========================================== Code In Here ==========================================

        #region Get Description

        public string GetDescriptions()
        {
            string _result = "";
            try
            {
                if (this._item != null)
                {
                    string _result1 = null;
                    string _result2 = null;
                    if (this._item.Name != null)
                    {
                        _result1 = this._item.Name;
                    }
                    if (this._item.Description != null)
                    {
                        _result2 = this._item.Description;
                    }
                    _result = _result1 + " " + _result2;
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = InventoryTransferOutLine " + ex.ToString());
            }

            return _result;
        }

        #endregion Get Description

        #region No

        public void UpdateNo()
        {
            try
            {
                if (!IsLoading && Session.IsNewObject(this))
                {
                    if (this.InventoryTransferOut != null)
                    {
                        object _makRecord = Session.Evaluate<InventoryTransferOutLine>(CriteriaOperator.Parse("Max(No)"), CriteriaOperator.Parse("InventoryTransferOut=?", this.InventoryTransferOut));
                        this.No = Convert.ToInt32(_makRecord) + 1;
                        this.Save();
                        RecoveryUpdateNo();
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = InventoryTransferOutLine " + ex.ToString());
            }
        }

        public void RecoveryUpdateNo()
        {
            try
            {
                if (this.InventoryTransferOut != null)
                {
                    InventoryTransferOut _numHeader = Session.FindObject<InventoryTransferOut>
                                                (new BinaryOperator("Code", this.InventoryTransferOut.Code));

                    XPCollection<InventoryTransferOutLine> _numLines = new XPCollection<InventoryTransferOutLine>
                                                (Session, new BinaryOperator("InventoryTransferOut", _numHeader),
                                                new SortProperty("No", DevExpress.Xpo.DB.SortingDirection.Ascending));
                    if (_numLines != null)
                    {
                        int i1 = 0;
                        foreach (InventoryTransferOutLine _numLine in _numLines)
                        {
                            i1 += 1;
                            _numLine.No = i1;
                            _numLine.Save();
                        }
                        i1 = 1;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = InventoryTransferOutLine " + ex.ToString());
            }
        }

        public void RecoveryDeleteNo()
        {
            try
            {
                if (this.InventoryTransferOut != null)
                {
                    InventoryTransferOut _numHeader = Session.FindObject<InventoryTransferOut>
                                                (new BinaryOperator("Code", this.InventoryTransferOut.Code));

                    XPCollection<InventoryTransferOutLine> _numLines = new XPCollection<InventoryTransferOutLine>
                                                (Session, new GroupOperator(GroupOperatorType.And,
                                                 new BinaryOperator("This", this, BinaryOperatorType.NotEqual),
                                                 new BinaryOperator("InventoryTransferOut", _numHeader)),
                                                 new SortProperty("No", DevExpress.Xpo.DB.SortingDirection.Ascending));
                    if (_numLines != null)
                    {
                        int i = 0;
                        foreach (InventoryTransferOutLine _numLine in _numLines)
                        {
                            i += 1;
                            _numLine.No = i;
                            _numLine.Save();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = InventoryTransferOutLine " + ex.ToString());
            }
        }

        #endregion No

        #region Set

        private void SetTotalQty()
        {
            try
            {
                double _locInvLineTotal = 0;
                DateTime now = DateTime.Now;
                if (this.InventoryTransferOut != null)
                {
                    ItemUnitOfMeasure _locItemUOM = Session.FindObject<ItemUnitOfMeasure>
                                                        (new GroupOperator(GroupOperatorType.And,
                                                         new BinaryOperator("Item", this.Item),
                                                         new BinaryOperator("UOM", this.UOM),
                                                         new BinaryOperator("DefaultUOM", this.DUOM),
                                                         new BinaryOperator("Active", true)));
                    if (_locItemUOM != null)
                    {
                        if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                        {
                            _locInvLineTotal = this.Qty * _locItemUOM.DefaultConversion + this.DQty;
                        }
                        else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                        {
                            _locInvLineTotal = this.Qty / _locItemUOM.Conversion + this.DQty;
                        }
                        else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                        {
                            _locInvLineTotal = this.Qty + this.DQty;
                        }

                        this.TQty = _locInvLineTotal;
                    }
                    else
                    {
                        _locInvLineTotal = this.Qty + this.DQty;
                        this.TQty = _locInvLineTotal;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = InventoryTransferOutLine " + ex.ToString());
            }
        }

        private void SetBegInv(Location _locLocation)
        {
            try
            {
                DateTime now = DateTime.Now;
                if (this.Company != null && this.Workplace != null)
                {
                    BeginningInventory _locBegInvFrom = Session.FindObject<BeginningInventory>
                                                        (new GroupOperator(GroupOperatorType.And,
                                                        new BinaryOperator("Company", this.Company),
                                                        new BinaryOperator("Workplace", this.Workplace),
                                                        new BinaryOperator("Location", _locLocation),
                                                        new BinaryOperator("LocationType", this.LocationType),
                                                        new BinaryOperator("StockType", this.StockType),
                                                        new BinaryOperator("Active", true)));
                    if (_locBegInvFrom != null)
                    {
                        this.BegInv = _locBegInvFrom;
                    }
                    else
                    {
                        this.BegInv = null;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Business Object = InventoryTransferOut ", ex.ToString());
            }
        }

        private void SetBegInvLine(Item _locItem)
        {
            try
            {
                DateTime now = DateTime.Now;
                BeginningInventoryLine _locBegInvLine = null;
                if (this.Company != null && this.Workplace != null && this.BegInv != null)
                {
                    if (this.BinLocation != null)
                    {
                        //BinLocation
                        _locBegInvLine = Session.FindObject<BeginningInventoryLine>(
                                   new GroupOperator(GroupOperatorType.And,
                                   new BinaryOperator("Company", this.Company),
                                   new BinaryOperator("Workplace", this.Workplace),
                                   new BinaryOperator("BeginningInventory", this.BegInv),
                                   new BinaryOperator("Item", _locItem),
                                   new BinaryOperator("Location", this.Location),
                                   new BinaryOperator("BinLocation", this.BinLocation),
                                   new BinaryOperator("LocationType", this.LocationType),
                                   new BinaryOperator("StockType", this.StockType),
                                   new BinaryOperator("StockGroup", this.StockGroup),
                                   new BinaryOperator("Lock", false),
                                   new BinaryOperator("Active", true)));
                    }
                    else
                    {
                        _locBegInvLine = Session.FindObject<BeginningInventoryLine>(
                                    new GroupOperator(GroupOperatorType.And,
                                    new BinaryOperator("Company", this.Company),
                                    new BinaryOperator("Workplace", this.Workplace),
                                    new BinaryOperator("BeginningInventory", this.BegInv),
                                    new BinaryOperator("Item", _locItem),
                                    new BinaryOperator("Location", this.Location),
                                    new BinaryOperator("LocationType", this.LocationType),
                                    new BinaryOperator("StockType", this.StockType),
                                    new BinaryOperator("StockGroup", this.StockGroup),
                                    new BinaryOperator("Lock", false),
                                    new BinaryOperator("Active", true)));
                    }

                    if (_locBegInvLine != null)
                    {
                        this.BegInvLine = _locBegInvLine;
                    }
                    else
                    {
                        this.BegInvLine = null;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Business Object = InventoryTransferOutLine ", ex.ToString());
            }
        }

        #endregion Set

    }
}