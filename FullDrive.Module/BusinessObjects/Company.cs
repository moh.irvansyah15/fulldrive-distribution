﻿using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using FullDrive.Module.CustomProcess;

namespace FullDrive.Module.BusinessObjects
{
    [DefaultClassOptions]
    [DefaultProperty("Name")]
    [NavigationItem("Organization")]
    [RuleCombinationOfPropertiesIsUnique("CompanyRuleUnique", DefaultContexts.Save, "Code")]
    //[ImageName("BO_Contact")]
    //[DefaultProperty("DisplayMemberNameForLookupEditorsOfThisType")]
    //[DefaultListViewOptions(MasterDetailMode.ListViewOnly, false, NewItemRowPosition.None)]
    //[Persistent("DatabaseTableName")]
    // Specify more UI options using a declarative approach (https://documentation.devexpress.com/#eXpressAppFramework/CustomDocument112701).
    public class Company : BaseObject
    { // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).
        private string _code;
        private string _name;
        private string _address;
        private Country _country;
        private City _city;
        private string _taxNo;
        private string _taxName;
        private bool _active;
        private bool _default;
        private GlobalFunction _globFunc;
        //AuditTrail
        private XPCollection<AuditDataItemPersistent> changeHistory;

        public Company(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if (!IsLoading)
            {
                _globFunc = new GlobalFunction();
                this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.Company);
            }
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).
        }

        protected override void OnSaving()
        {
            base.OnSaving();
            if (this.Default == true)
            {
                CheckDefaultSystem();
            }
        }

        [RuleRequiredField(DefaultContexts.Save)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue("Code", ref _code, value); }
        }

        [ImmediatePostData()]
        [RuleRequiredField(DefaultContexts.Save)]
        public string Name
        {
            get { return _name; }
            set {
                SetPropertyValue("Name", ref _name, value);
                if(!IsLoading)
                {
                    if(this._name != null)
                    {
                        this.TaxName = this._name;
                    }
                    else
                    {
                        this.TaxName = null;
                    }
                }
            }
        }

        [Size(512)]
        public string Address
        {
            get { return _address; }
            set { SetPropertyValue("Address", ref _address, value); }
        }

        [DataSourceCriteria("Active = true")]
        public Country Country
        {
            get { return _country; }
            set { SetPropertyValue("Country", ref _country, value); }
        }

        [DataSourceCriteria("Active = true")]
        public City City
        {
            get { return _city; }
            set { SetPropertyValue("City", ref _city, value); }
        }

        public string TaxNo
        {
            get { return _taxNo; }
            set { SetPropertyValue("TaxNo", ref _taxNo, value); }
        }

        public string TaxName
        {
            get { return _taxName; }
            set { SetPropertyValue("TaxName", ref _taxName, value); }
        }

        public bool Active
        {
            get { return _active; }
            set { SetPropertyValue("Active", ref _active, value); }
        }

        public bool Default
        {
            get { return _default; }
            set { SetPropertyValue("Default", ref _default, value); }
        }

        [Association("Company-Divisions")]
        public XPCollection<Division> Divisions
        {
            get { return GetCollection<Division>("Divisions"); }
        }

        [Association("Company-BankAccounts")]
        public XPCollection<BankAccount> BankAccounts
        {
            get { return GetCollection<BankAccount>("BankAccounts"); }
        }

        [Association("Company-Employees")]
        public XPCollection<Employee> Employees
        {
            get { return GetCollection<Employee>("Employees"); }
        }

        [Association("Company-Workplaces")]
        public XPCollection<Workplace> Workplaces
        {
            get { return GetCollection<Workplace>("Workplaces"); }
        }

        [Association("Company-CompanyAccounts")]
        public XPCollection<CompanyAccount> CompanyAccounts
        {
            get { return GetCollection<CompanyAccount>("CompanyAccounts"); }
        }

        //AuditTrail
        [CollectionOperationSet(AllowAdd = false, AllowRemove = true)]
        public XPCollection<AuditDataItemPersistent> ChangeHistory
        {
            get
            {
                if (changeHistory == null)
                {
                    changeHistory = AuditedObjectWeakReference.GetAuditTrail(Session, this);
                }
                return changeHistory;
            }
        }

        

        //=============================================== Code In Here ===============================================

        private void CheckDefaultSystem()
        {
            try
            {
                XPCollection<Company> _companyDatas = new XPCollection<Company>(Session, new BinaryOperator("This", this, BinaryOperatorType.NotEqual));
                if (_companyDatas == null)
                {
                    return;
                }
                else
                {
                    foreach (Company _companyData in _companyDatas)
                    {
                        _companyData.Default = false;
                        _companyData.Save();
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Business Object = Company", ex.ToString());
            }
        }

    }
}