﻿using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;

using DevExpress.ExpressApp.ConditionalAppearance;
using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.Editors;

namespace FullDrive.Module.BusinessObjects
{
    [DefaultClassOptions]
    [DefaultProperty("Code")]
    [NavigationItem("Inventory")]
    [RuleCombinationOfPropertiesIsUnique("InventoryTransferInMonitoringRuleUnique", DefaultContexts.Save, "Code")]
    //[ImageName("BO_Contact")]
    //[DefaultProperty("DisplayMemberNameForLookupEditorsOfThisType")]
    //[DefaultListViewOptions(MasterDetailMode.ListViewOnly, false, NewItemRowPosition.None)]
    //[Persistent("DatabaseTableName")]
    // Specify more UI options using a declarative approach (https://documentation.devexpress.com/#eXpressAppFramework/CustomDocument112701).
    public class InventoryTransferInMonitoring : BaseObject
    { // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).
        private bool _activationPosting;
        private string _code;
        private bool _select;
        private InventoryTransferIn _inventoryTransferIn;
        private InventoryTransferInLine _inventoryTransferInLine;
        private PurchaseInvoice _purchaseInvoice;
        private OrderType _purchaseType;
        private XPCollection<Item> _availableItem;
        private Item _item;
        private Brand _brand;
        private string _description;        
        private Location _location;
        private BinLocation _binLocation;
        private StockType _stockType;
        private StockGroup _stockGroup;
        private XPCollection<UnitOfMeasure> _availableUnitOfMeasure;
        #region InisialisasiMaxQty
        private double _mxDQTY;
        private UnitOfMeasure _mxDUom;
        private double _mxQty;
        private UnitOfMeasure _mxUom;
        private double _mxTQty;
        #endregion InisialisasiMaxQty
        #region InisialisasiDefaultQty
        private double _dQty;
        private UnitOfMeasure _dUom;
        private double _qty;
        private UnitOfMeasure _uom;
        private double _tQty;
        #endregion InisialisasiDefaultQty
        #region InisialisasiRemainQty
        private double _rmDQty;
        private double _rmQty;
        private double _rmTQty;
        #endregion InisialisasiRemainQty
        #region InitialPostingQuantityTo
        private double _pDQty;
        private UnitOfMeasure _pDUom;
        private double _pQty;
        private UnitOfMeasure _pUom;
        private double _pTQty;
        #endregion InitialPostingQuantityTo
        #region InitialOrganization
        private Company _company;
        private Workplace _workplace;
        private Division _division;
        private Department _department;
        private Section _section;
        private Employee _employee;
        #endregion InitialOrganization
        private DateTime _docDate;
        private int _journalMonth;
        private int _journalYear;
        private Status _status;
        private DateTime _statusDate;        
        private PurchaseOrderMonitoring _purchaseOrderMonitoring;
        private int _postedCount;
        private string _localUserAccess;
        private GlobalFunction _globFunc;
        //AuditTrail
        private XPCollection<AuditDataItemPersistent> changeHistory;

        public InventoryTransferInMonitoring(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if (!IsLoading)
            {
                if(Session.IsNewObject(this))
                {
                    _globFunc = new GlobalFunction();
                    DateTime now = DateTime.Now;
                    #region UserAccess
                    _localUserAccess = SecuritySystem.CurrentUserName;
                    UserAccess _locUserAccess = Session.FindObject<UserAccess>(new BinaryOperator("UserName", _localUserAccess));
                    if (_locUserAccess != null)
                    {
                        if (_locUserAccess.Employee != null)
                        {
                            if (_locUserAccess.Employee.Company != null && _locUserAccess.Employee.Workplace != null)
                            {
                                this.Code = _globFunc.GetNumberingUnlockOptimisticRecordBasedOrganization(this.Session.DataLayer, ObjectList.InventoryTransferInMonitoring, _locUserAccess.Employee.Company, _locUserAccess.Employee.Workplace);
                            }
                            else
                            {
                                this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.InventoryTransferInMonitoring);
                            }
                        }
                    }
                    #endregion UserAccess
                    this.Select = true;
                    this.Status = Status.Open;
                    this.StatusDate = now;
                }                
            }
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).
        }

        #region Field

        [Browsable(false)]
        public bool ActivationPosting
        {
            get { return _activationPosting; }
            set { SetPropertyValue("ActivationPosting", ref _activationPosting, value); }
        }

        [Appearance("InventoryTranferInMonitoringCodeClose", Enabled = false)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue("Code", ref _code, value); }
        }

        [Appearance("InventoryTransferInMonitoringSelectClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public bool Select
        {
            get { return _select; }
            set { SetPropertyValue("Select", ref _select, value); }
        }

        [Appearance("InventoryTransferInMonitoringInventoryTransferInClose", Enabled = false)]
        public InventoryTransferIn InventoryTransferIn
        {
            get { return _inventoryTransferIn; }
            set { SetPropertyValue("InventoryTransferIn", ref _inventoryTransferIn, value); }
        }

        [Appearance("InventoryTransferInMonitoringInventoryTransferInLineClose", Enabled = false)]
        public InventoryTransferInLine InventoryTransferInLine
        {
            get { return _inventoryTransferInLine; }
            set { SetPropertyValue("InventoryTransferInLine", ref _inventoryTransferInLine, value); }
        }

        [Appearance("InventoryTransferInMonitoringPurchaseInvoiceClose", Enabled = false)]
        public PurchaseInvoice PurchaseInvoice
        {
            get { return _purchaseInvoice; }
            set { SetPropertyValue("PurchaseInvoice", ref _purchaseInvoice, value); }
        }

        [ImmediatePostData()]
        [Appearance("InventoryTransferInMonitoringPurchaseTypeClose", Enabled = false)]
        public OrderType PurchaseType
        {
            get { return _purchaseType; }
            set { SetPropertyValue("PurchaseType", ref _purchaseType, value); }
        }

        [Browsable(false)]
        public XPCollection<Item> AvailableItem
        {
            get
            {
                if(!IsLoading)
                {
                    if (PurchaseType == OrderType.Item)
                    {
                        _availableItem = new XPCollection<Item>(Session,
                                        new GroupOperator(GroupOperatorType.And,
                                        new BinaryOperator("ItemType", OrderType.Item)));
                    }
                    else if (PurchaseType == OrderType.Service)
                    {
                        _availableItem = new XPCollection<Item>(Session,
                                        new GroupOperator(GroupOperatorType.And,
                                        new BinaryOperator("ItemType", OrderType.Service)));
                    }
                    else
                    {
                        _availableItem = new XPCollection<Item>(Session);
                    }
                }
                return _availableItem;
            }
        }

        [DataSourceProperty("AvailableItem", DataSourcePropertyIsNullMode.SelectAll)]
        [Appearance("InventoryTransferInMonitoringItemClose", Enabled = false)]
        [ImmediatePostData()]
        public Item Item
        {
            get { return _item; }
            set
            {
                SetPropertyValue("Item", ref _item, value);
                if (!IsLoading)
                {
                    if (this._item != null)
                    {
                        this.DUOM = this._item.BasedUOM;
                        this.Brand = this._item.Brand;
                        this.Description = GetDescriptions();
                    }
                }
            }
        }

        [Appearance("InventoryTransferInMonitoringBrandClose", Enabled = false)]
        public Brand Brand
        {
            get { return _brand; }
            set { SetPropertyValue("Brand", ref _brand, value); }
        }

        [Appearance("InventoryTransferInMonitoringDescriptionClose", Enabled = false)]
        [Size(512)]
        public string Description
        {
            get { return _description; }
            set { SetPropertyValue("Description", ref _description, value); }
        }

        #region Location

        [Appearance("InventoryTransferInMonitoringLocationClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [DataSourceCriteria("Active = true")]
        [ImmediatePostData()]
        public Location Location
        {
            get { return _location; }
            set { SetPropertyValue("Location", ref _location, value); }
        }

        [Appearance("InventoryTransferInMonitoringBinLocationClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [DataSourceCriteria("Active = true")]
        public BinLocation BinLocation
        {
            get { return _binLocation; }
            set { SetPropertyValue("BinLocation", ref _binLocation, value); }
        }

        [Appearance("InventoryTransferInMonitoringStockTypeClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public StockType StockType
        {
            get { return _stockType; }
            set { SetPropertyValue("StockType", ref _stockType, value); }
        }

        [Appearance("InventoryTransferInMonitoringStockGroupClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public StockGroup StockGroup
        {
            get { return _stockGroup; }
            set { SetPropertyValue("StockGroup", ref _stockGroup, value); }
        }

        #endregion Location

        [Browsable(false)]
        public XPCollection<UnitOfMeasure> AvailableUnitOfMeasure
        {
            get
            {
                if(!IsLoading)
                {
                    string _beginString = null;
                    string _endString = null;
                    string _fullString = null;

                    if (Item == null)
                    {
                        _availableUnitOfMeasure = new XPCollection<UnitOfMeasure>(Session);
                    }
                    else
                    {
                        List<string> _stringUOM = new List<string>();

                        XPCollection<ItemUnitOfMeasure> _locItemUnitOfMeasures = new XPCollection<ItemUnitOfMeasure>
                                                                                  (Session, new GroupOperator(GroupOperatorType.And,
                                                                                   new BinaryOperator("Item", this.Item),
                                                                                   new BinaryOperator("Active", true)));

                        if (_locItemUnitOfMeasures != null && _locItemUnitOfMeasures.Count() > 0)
                        {
                            foreach (ItemUnitOfMeasure _locItemUnitOfMeasure in _locItemUnitOfMeasures)
                            {
                                _stringUOM.Add(_locItemUnitOfMeasure.UOM.Code);
                            }
                        }

                        IEnumerable<string> _stringArrayUOMDistinct = _stringUOM.Distinct();
                        string[] _stringArrayUOMList = _stringArrayUOMDistinct.ToArray();
                        if (_stringArrayUOMList.Length == 1)
                        {
                            for (int i = 0; i < _stringArrayUOMList.Length; i++)
                            {
                                UnitOfMeasure _locUOM = Session.FindObject<UnitOfMeasure>(new BinaryOperator("Code", _stringArrayUOMList[i]));
                                if (_locUOM != null)
                                {
                                    if (i == 0)
                                    {
                                        _beginString = "[Code]=='" + _locUOM.Code + "'";
                                    }
                                }
                            }
                        }
                        else if (_stringArrayUOMList.Length > 1)
                        {
                            for (int i = 0; i < _stringArrayUOMList.Length; i++)
                            {
                                UnitOfMeasure _locUOM = Session.FindObject<UnitOfMeasure>(new BinaryOperator("Code", _stringArrayUOMList[i]));
                                if (_locUOM != null)
                                {
                                    if (i == 0)
                                    {
                                        _beginString = "[Code]=='" + _locUOM.Code + "'";
                                    }
                                    else
                                    {
                                        _endString = _endString + " OR [Code]=='" + _locUOM.Code + "'";
                                    }
                                }
                            }
                        }
                        _fullString = _beginString + _endString;

                        if (_fullString != null)
                        {
                            _availableUnitOfMeasure = new XPCollection<UnitOfMeasure>(Session, CriteriaOperator.Parse(_fullString));
                        }
                    }
                }                
                return _availableUnitOfMeasure;
            }
        }

        #region MaxDefaultQty

        [Appearance("InventoryTransferInMonitoringMxDQtyClose", Enabled = false)]
        [ImmediatePostData()]
        public double MxDQty
        {
            get { return _mxDQTY; }
            set
            {
                SetPropertyValue("MxDQty", ref _mxDQTY, value);
                if (!IsLoading)
                {
                    SetMaxTotalQty();
                }
            }
        }

        [ImmediatePostData()]
        [DataSourceCriteria("Active = true")]
        [Appearance("InventoryTransferInMonitoringMxDUOMClose", Enabled = false)]
        public UnitOfMeasure MxDUOM
        {
            get { return _mxDUom; }
            set
            {
                SetPropertyValue("MxDUOM", ref _mxDUom, value);
                if (!IsLoading)
                {
                    SetMaxTotalQty();
                }
            }
        }

        [ImmediatePostData()]
        [Appearance("InventoryTransferInMonitoringMxQtyClose", Enabled = false)]
        public double MxQty
        {
            get { return _mxQty; }
            set
            {
                SetPropertyValue("MxQty", ref _mxQty, value);
                if (!IsLoading)
                {
                    SetMaxTotalQty();
                }
            }
        }

        [DataSourceProperty("AvailableUnitOfMeasure", DataSourcePropertyIsNullMode.SelectAll)]
        [DataSourceCriteria("Active = true")]
        [Appearance("InventoryTransferInMonitoringMxUOMClose", Enabled = false)]
        public UnitOfMeasure MxUOM
        {
            get { return _mxUom; }
            set
            {
                SetPropertyValue("MxUOM", ref _mxUom, value);
                if (!IsLoading)
                {
                    SetMaxTotalQty();
                }
            }
        }

        [Appearance("InventoryTransferInMonitoringMxTQtyClose", Enabled = false)]
        public double MxTQty
        {
            get { return _mxTQty; }
            set { SetPropertyValue("MxTQty", ref _mxTQty, value); }
        }

        #endregion MaxDefaultQty

        #region DefaultQty

        [Appearance("InventoryTransferInMonitoringDQtyClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [ImmediatePostData()]
        public double DQty
        {
            get { return _dQty; }
            set
            {
                SetPropertyValue("DQty", ref _dQty, value);
                if (!IsLoading)
                {
                    SetMxDQty();
                    SetTotalQty();
                }
            }
        }

        [Appearance("InventoryTransferInMonitoringDUOMClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [ImmediatePostData()]
        [DataSourceCriteria("Active = true")]
        public UnitOfMeasure DUOM
        {
            get { return _dUom; }
            set
            {
                SetPropertyValue("DUOM", ref _dUom, value);
                if (!IsLoading)
                {
                    SetMxDUOM();
                    SetTotalQty();
                }
            }
        }

        [Appearance("InventoryTransferInMonitoringQtyClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [ImmediatePostData()]
        public double Qty
        {
            get { return _qty; }
            set
            {
                SetPropertyValue("Qty", ref _qty, value);
                if (!IsLoading)
                {
                    SetMxQty();
                    SetTotalQty();
                }
            }
        }

        [Appearance("InventoryTransferInMonitoringUOMClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [ImmediatePostData()]
        [DataSourceProperty("AvailableUnitOfMeasure", DataSourcePropertyIsNullMode.SelectAll)]
        [DataSourceCriteria("Active = true")]
        public UnitOfMeasure UOM
        {
            get { return _uom; }
            set
            {
                SetPropertyValue("UOM", ref _uom, value);
                if (!IsLoading)
                {
                    SetMxUOM();
                    SetTotalQty();
                }
            }
        }

        [Appearance("InventoryTransferInMonitoringTotalQtyEnabled", Enabled = false)]
        public double TQty
        {
            get { return _tQty; }
            set { SetPropertyValue("TotalQty", ref _tQty, value); }
        }

        #endregion DefaultQty

        #region RemainQty

        [Appearance("InventoryTransferInMonitoringRmDQtyClose", Enabled = false)]
        public double RmDQty
        {
            get { return _rmDQty; }
            set { SetPropertyValue("RmDQty", ref _rmDQty, value); }
        }

        [Appearance("InventoryTransferInMonitoringRmQtyClose", Enabled = false)]
        public double RmQty
        {
            get { return _rmQty; }
            set { SetPropertyValue("RmQty", ref _rmQty, value); }
        }

        [Appearance("InventoryTransferInMonitoringRmTQtyClose", Enabled = false)]
        public double RmTQty
        {
            get { return _rmTQty; }
            set { SetPropertyValue("RmTQty", ref _rmTQty, value); }
        }

        #endregion RemainQty

        #region PostingQty

        [Appearance("InventoryTransferInMonitoringPDQtyClose", Enabled = false)]
        public double PDQty
        {
            get { return _pDQty; }
            set
            {
                SetPropertyValue("PDQty", ref _pDQty, value);
                if (!IsLoading)
                {
                    SetTotalPQty();
                }
            }
        }

        [Appearance("InventoryTransferInMonitoringPDUOMClose", Enabled = false)]
        [DataSourceCriteria("Active = true")]
        public UnitOfMeasure PDUOM
        {
            get { return _pDUom; }
            set
            {
                SetPropertyValue("PDUOM", ref _pDUom, value);
                if (!IsLoading)
                {
                    SetTotalPQty();
                }
            }
        }

        [Appearance("InventoryTransferInMonitoringPQtyToClose", Enabled = false)]
        public double PQty
        {
            get { return _pQty; }
            set
            {
                SetPropertyValue("PQty", ref _pQty, value);
                if (!IsLoading)
                {
                    SetTotalPQty();
                }
            }
        }

        [Appearance("InventoryTransferInMonitoringPUOMClose", Enabled = false)]
        [DataSourceProperty("AvailableUnitOfMeasure", DataSourcePropertyIsNullMode.SelectAll)]
        [DataSourceCriteria("Active = true")]
        public UnitOfMeasure PUOM
        {
            get { return _pUom; }
            set
            {
                SetPropertyValue("PUOM", ref _pUom, value);
                if (!IsLoading)
                {
                    SetTotalPQty();
                }
            }
        }

        [Appearance("InventoryTransferInMonitoringPTQtyEnabled", Enabled = false)]
        public double PTQty
        {
            get { return _pTQty; }
            set { SetPropertyValue("PTQty", ref _pTQty, value); }
        }

        #endregion PostingQty

        #region Organization

        [Appearance("InventoryTranferInMonitoringCompanyEnabled", Enabled = false)]
        public Company Company
        {
            get { return _company; }
            set { SetPropertyValue("Company", ref _company, value); }
        }

        [Appearance("InventoryTranferInMonitoringWorkplaceEnabled", Enabled = false)]
        public Workplace Workplace
        {
            get { return _workplace; }
            set { SetPropertyValue("Workplace", ref _workplace, value); }
        }

        [Browsable(false)]
        public Division Division
        {
            get { return _division; }
            set { SetPropertyValue("Division", ref _division, value); }
        }

        [Browsable(false)]
        public Department Department
        {
            get { return _department; }
            set { SetPropertyValue("Department", ref _department, value); }
        }

        [Browsable(false)]
        public Section Section
        {
            get { return _section; }
            set { SetPropertyValue("Section", ref _section, value); }
        }

        [Browsable(false)]
        public Employee Employee
        {
            get { return _employee; }
            set { SetPropertyValue("Employee", ref _employee, value); }
        }

        #endregion Organization

        [Browsable(false)]
        [ImmediatePostData()]
        [ModelDefault("DisplayFormat", "{0:dd/MM/yyyy}")]
        [ModelDefault("EditMask", "dd/MM/yyyy")]
        [Appearance("InventoryTransferInMonitoringDocDateClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public DateTime DocDate
        {
            get { return _docDate; }
            set
            {
                SetPropertyValue("DocDate", ref _docDate, value);
                if (!IsLoading)
                {
                    this.JournalMonth = this._docDate.Month;
                    this.JournalYear = this._docDate.Year;
                }
            }
        }

        [Browsable(false)]
        public int JournalMonth
        {
            get { return _journalMonth; }
            set { SetPropertyValue("JournalMonth", ref _journalMonth, value); }
        }

        [Browsable(false)]
        public int JournalYear
        {
            get { return _journalYear; }
            set { SetPropertyValue("JournalYear", ref _journalYear, value); }
        }

        [Appearance("InventoryTransferInMonitoringStatusEnabled", Enabled = false)]
        public Status Status
        {
            get { return _status; }
            set { SetPropertyValue("Status", ref _status, value); }
        }

        [ModelDefault("DisplayFormat", "{0:dd/MM/yyyy}")]
        [ModelDefault("EditMask", "dd/MM/yyyy")]
        [Appearance("InventoryTransferInMonitoringStatusDateEnabled", Enabled = false)]
        public DateTime StatusDate
        {
            get { return _statusDate; }
            set { SetPropertyValue("StatusDate", ref _statusDate, value); }
        }        

        [Appearance("InventoryTranferInMonitoringPurchaseOrderMonitoringEnabled", Enabled = false)]
        public PurchaseOrderMonitoring PurchaseOrderMonitoring
        {
            get { return _purchaseOrderMonitoring; }
            set { SetPropertyValue("PurchaseOrderMonitoring", ref _purchaseOrderMonitoring, value); }
        }

        [Appearance("InventoryTranferInMonitoringPostedCountEnabled", Enabled = false)]
        public int PostedCount
        {
            get { return _postedCount; }
            set { SetPropertyValue("PostedCount", ref _postedCount, value); }
        }

        //AuditTrail
        [CollectionOperationSet(AllowAdd = false, AllowRemove = true)]
        public XPCollection<AuditDataItemPersistent> ChangeHistory
        {
            get
            {
                if (changeHistory == null)
                {
                    changeHistory = AuditedObjectWeakReference.GetAuditTrail(Session, this);
                }
                return changeHistory;
            }
        }

        #endregion Field

        //=========================================== Code In Here ===========================================

        public string GetDescriptions()
        {
            string _result = "";
            try
            {
                if (this._item != null)
                {
                    string _result1 = null;
                    string _result2 = null;
                    if (this._item.Name != null)
                    {
                        _result1 = this._item.Name;
                    }
                    if (this._item.Description != null)
                    {
                        _result2 = this._item.Description;
                    }
                    _result = _result1 + " " + _result2;
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = InventoryTransferInMonitoring " + ex.ToString());
            }

            return _result;
        }

        #region Set

        private void SetMaxTotalQty()
        {
            try
            {
                double _locInvLineTotal = 0;
                DateTime now = DateTime.Now;
                if (this.InventoryTransferIn != null && this.InventoryTransferInLine != null)
                {
                    if (this.Item != null && this.MxUOM != null && this.MxDUOM != null)
                    {
                        ItemUnitOfMeasure _locItemUOM = Session.FindObject<ItemUnitOfMeasure>
                                                        (new GroupOperator(GroupOperatorType.And,
                                                         new BinaryOperator("Item", this.Item),
                                                         new BinaryOperator("UOM", this.MxUOM),
                                                         new BinaryOperator("DefaultUOM", this.MxDUOM),
                                                         new BinaryOperator("Active", true)));
                        if (_locItemUOM != null)
                        {
                            if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                            {
                                _locInvLineTotal = this.MxQty * _locItemUOM.DefaultConversion + this.MxDQty;
                            }
                            else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                            {
                                _locInvLineTotal = this.MxQty / _locItemUOM.Conversion + this.MxDQty;
                            }
                            else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                            {
                                _locInvLineTotal = this.MxQty + this.MxDQty;
                            }

                            this.MxTQty = _locInvLineTotal;
                        }
                    }
                    else
                    {
                        _locInvLineTotal = this.MxQty + this.MxDQty;
                        this.MxTQty = _locInvLineTotal;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = InventoryTransferInMonitoring " + ex.ToString());
            }
        }

        private void SetTotalQty()
        {
            try
            {
                double _locInvLineTotal = 0;
                DateTime now = DateTime.Now;
                if (this.InventoryTransferIn != null && this.InventoryTransferInLine != null)
                {
                    if (this.Item != null && this.UOM != null && this.DUOM != null)
                    {
                        ItemUnitOfMeasure _locItemUOM = Session.FindObject<ItemUnitOfMeasure>
                                                        (new GroupOperator(GroupOperatorType.And,
                                                         new BinaryOperator("Item", this.Item),
                                                         new BinaryOperator("UOM", this.UOM),
                                                         new BinaryOperator("DefaultUOM", this.DUOM),
                                                         new BinaryOperator("Active", true)));
                        if (_locItemUOM != null)
                        {
                            if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                            {
                                _locInvLineTotal = this.Qty * _locItemUOM.DefaultConversion + this.DQty;
                            }
                            else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                            {
                                _locInvLineTotal = this.Qty / _locItemUOM.Conversion + this.DQty;
                            }
                            else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                            {
                                _locInvLineTotal = this.Qty + this.DQty;
                            }

                            this.TQty = _locInvLineTotal;
                        }
                    }
                    else
                    {
                        _locInvLineTotal = this.Qty + this.DQty;
                        this.TQty = _locInvLineTotal;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = InventoryTransferInMonitoring " + ex.ToString());
            }
        }

        private void SetTotalPQty()
        {
            try
            {
                double _locInvLineTotal = 0;
                DateTime now = DateTime.Now;
                if (this.InventoryTransferIn != null && this.InventoryTransferInLine != null)
                {
                    if (this.Item != null && this.UOM != null && this.DUOM != null)
                    {
                        ItemUnitOfMeasure _locItemUOM = Session.FindObject<ItemUnitOfMeasure>
                                                        (new GroupOperator(GroupOperatorType.And,
                                                         new BinaryOperator("Item", this.Item),
                                                         new BinaryOperator("UOM", this.UOM),
                                                         new BinaryOperator("DefaultUOM", this.DUOM),
                                                         new BinaryOperator("Active", true)));
                        if (_locItemUOM != null)
                        {
                            if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                            {
                                _locInvLineTotal = this.PQty * _locItemUOM.DefaultConversion + this.PDQty;
                            }
                            else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                            {
                                _locInvLineTotal = this.PQty / _locItemUOM.Conversion + this.PDQty;
                            }
                            else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                            {
                                _locInvLineTotal = this.PQty + this.PDQty;
                            }

                            this.PTQty = _locInvLineTotal;
                        }
                    }
                    else
                    {
                        _locInvLineTotal = this.PQty + this.PDQty;
                        this.PTQty = _locInvLineTotal;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = InventoryTransferInMonitoring " + ex.ToString());
            }
        }

        #region SetMaxInQty

        private void SetMxDQty()
        {
            try
            {
                if (this.InventoryTransferIn != null && this.InventoryTransferInLine != null)
                {
                    if (this.Status == Status.Posted || this.PostedCount > 0)
                    {
                        if (this.RmDQty > 0)
                        {
                            if (this._dQty > this.RmDQty)
                            {
                                this._dQty = this.RmDQty;
                            }
                        }
                    }

                    if (this.Status == Status.Close || this.PostedCount > 0)
                    {
                        this._dQty = 0;
                    }

                    if ((this.Status == Status.Open || this.Status == Status.Progress) && this.PostedCount <= 0)
                    {
                        if (this._dQty > 0)
                        {
                            this.MxDQty = this._dQty;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = InventoryTransferInMonitoring " + ex.ToString());
            }
        }

        private void SetMxDUOM()
        {
            try
            {
                if (this.InventoryTransferIn != null && this.InventoryTransferInLine != null)
                {
                    if ((this.Status == Status.Open || this.Status == Status.Progress) && this.PostedCount <= 0)
                    {
                        this.MxDUOM = this.DUOM;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = InventoryTransferInMonitoring " + ex.ToString());
            }
        }

        private void SetMxQty()
        {
            try
            {
                if (this.InventoryTransferIn != null)
                {
                    if (this.Status == Status.Posted || this.PostedCount > 0)
                    {
                        if (this.RmQty > 0)
                        {
                            if (this._qty > this.RmQty)
                            {
                                this._qty = this.RmQty;
                            }
                        }
                    }

                    if (this.Status == Status.Close || this.PostedCount > 0)
                    {
                        this._qty = 0;
                    }

                    if ((this.Status == Status.Open || this.Status == Status.Progress) && this.PostedCount <= 0)
                    {
                        if (this._qty > 0)
                        {
                            this.MxQty = this._qty;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = InventoryTransferInMonitoring " + ex.ToString());
            }
        }

        private void SetMxUOM()
        {
            try
            {
                if (this.InventoryTransferIn != null)
                {
                    if ((this.Status == Status.Open || this.Status == Status.Progress) && this.PostedCount <= 0)
                    {
                        this.MxUOM = this.UOM;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = InventoryTransferInMonitoring " + ex.ToString());
            }
        }

        #endregion SetMaxInQty

        #endregion Set

    }
}