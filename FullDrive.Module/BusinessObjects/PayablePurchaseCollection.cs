﻿using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;

using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.ConditionalAppearance;

namespace FullDrive.Module.BusinessObjects
{
    [DefaultClassOptions]
    [DefaultProperty("Code")]
    [NavigationItem("Finance & Accounting")]
    [RuleCombinationOfPropertiesIsUnique("PayablePurchaseCollectionRuleUnique", DefaultContexts.Save, "Code")]
    //[ImageName("BO_Contact")]
    //[DefaultProperty("DisplayMemberNameForLookupEditorsOfThisType")]
    //[DefaultListViewOptions(MasterDetailMode.ListViewOnly, false, NewItemRowPosition.None)]
    //[Persistent("DatabaseTableName")]
    // Specify more UI options using a declarative approach (https://documentation.devexpress.com/#eXpressAppFramework/CustomDocument112701).
    public class PayablePurchaseCollection : BaseObject
    { // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).
        private string _code;
        private XPCollection<PurchaseInvoice> _availablePurchaseInvoice;
        private PurchaseInvoice _purchaseInvoice;
        private XPCollection<PurchasePrePaymentInvoice> _availablePurchasePrePaymentInvoice;
        private PurchasePrePaymentInvoice _purchasePrePaymentInvoice;
        private PayableTransaction _payableTransaction;
        #region InitialOrganization
        private Company _company;
        private Workplace _workplace;
        private Division _division;
        private Department _department;
        private Section _section;
        private Employee _employee;
        #endregion InitialOrganization
        private Status _status;
        private DateTime _statusDate;        
        private int _postedCount;
        private string _localUserAccess;
        private GlobalFunction _globFunc;
        //AuditTrail
        private XPCollection<AuditDataItemPersistent> changeHistory;

        public PayablePurchaseCollection(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if (!IsLoading)
            {
                _globFunc = new GlobalFunction();
                DateTime now = DateTime.Now;
                #region UserAccess
                _localUserAccess = SecuritySystem.CurrentUserName;
                UserAccess _locUserAccess = Session.FindObject<UserAccess>(new BinaryOperator("UserName", _localUserAccess));
                if (_locUserAccess != null)
                {
                    if (_locUserAccess.Employee != null)
                    {
                        if (_locUserAccess.Employee.Company != null && _locUserAccess.Employee.Workplace != null)
                        {
                            this.Code = _globFunc.GetNumberingUnlockOptimisticRecordBasedOrganization(this.Session.DataLayer, ObjectList.PayablePurchaseCollection, _locUserAccess.Employee.Company, _locUserAccess.Employee.Workplace);
                        }
                        else
                        {
                            this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.PayablePurchaseCollection);
                        }
                    }
                }
                #endregion UserAccess
                this.Status = Status.Open;
                this.StatusDate = now;
            }
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).
        }

        #region Field

        [RuleRequiredField(DefaultContexts.Save)]
        [Appearance("PayablePurchaseCollectionCodeClose", Enabled = false)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue("Code", ref _code, value); }
        }

        [Browsable(false)]
        public XPCollection<PurchaseInvoice> AvailablePurchaseInvoice
        {
            get
            {
                if(!IsLoading)
                {
                    string _beginString = null;
                    string _endString = null;
                    string _fullString = null;

                    if (this.PayableTransaction != null)
                    {
                        if (this.PayableTransaction.BuyFromVendor != null && this.PayableTransaction.PayToVendor != null)
                        {
                            XPQuery<PurchaseInvoiceMonitoring> _purchaseInvoiceMonitoringsQuery = new XPQuery<PurchaseInvoiceMonitoring>(Session);

                            var _purchaseInvoiceMonitorings = from pim in _purchaseInvoiceMonitoringsQuery
                                                              where (pim.Status != Status.Close
                                                              && pim.PurchaseInvoice.BuyFromVendor == this.PayableTransaction.BuyFromVendor
                                                              && pim.PurchaseInvoice.PayToVendor == this.PayableTransaction.PayToVendor
                                                              && pim.PayableTransaction == null
                                                              )
                                                              group pim by pim.PurchaseInvoice into g
                                                              select new { PurchaseInvoice = g.Key };

                            if (_purchaseInvoiceMonitorings != null && _purchaseInvoiceMonitorings.Count() > 0)
                            {
                                List<string> _stringPIM = new List<string>();

                                foreach (var _purchaseInvoiceMonitoring in _purchaseInvoiceMonitorings)
                                {
                                    if (_purchaseInvoiceMonitoring != null)
                                    {
                                        _stringPIM.Add(_purchaseInvoiceMonitoring.PurchaseInvoice.Code);
                                    }
                                }

                                IEnumerable<string> _stringArrayPIMDistinct = _stringPIM.Distinct();
                                string[] _stringArrayPIMList = _stringArrayPIMDistinct.ToArray();
                                if (_stringArrayPIMList.Length == 1)
                                {
                                    for (int i = 0; i < _stringArrayPIMList.Length; i++)
                                    {
                                        if (i == 0)
                                        {
                                            _beginString = "[Code]=='" + _stringArrayPIMList[i] + "'";
                                        }
                                    }
                                }
                                else if (_stringArrayPIMList.Length > 1)
                                {
                                    for (int i = 0; i < _stringArrayPIMList.Length; i++)
                                    {
                                        if (i == 0)
                                        {
                                            _beginString = "[Code]=='" + _stringArrayPIMList[i] + "'";
                                        }
                                        else
                                        {
                                            _endString = _endString + " OR [Code]=='" + _stringArrayPIMList[i] + "'";
                                        }
                                    }
                                }
                                _fullString = _beginString + _endString;

                                if (_fullString != null)
                                {
                                    _availablePurchaseInvoice = new XPCollection<PurchaseInvoice>(Session, CriteriaOperator.Parse(_fullString));
                                }
                            }
                        }
                    }
                }               
                return _availablePurchaseInvoice;
            }
        }

        [DataSourceProperty("AvailablePurchaseInvoice", DataSourcePropertyIsNullMode.SelectNothing)]
        public PurchaseInvoice PurchaseInvoice
        {
            get { return _purchaseInvoice; }
            set { SetPropertyValue("PurchaseInvoice", ref _purchaseInvoice, value); }
        }

        [Browsable(false)]
        public XPCollection<PurchasePrePaymentInvoice> AvailablePurchasePrePaymentInvoice
        {
            get
            {
                if (!IsLoading)
                {
                    string _beginString = null;
                    string _endString = null;
                    string _fullString = null;

                    if (this.PayableTransaction != null)
                    {
                        if (this.PayableTransaction.BuyFromVendor != null && this.PayableTransaction.PayToVendor != null)
                        {
                            XPQuery<PurchasePrePaymentInvoiceMonitoring> _purchasePrePaymentInvoiceMonitoringsQuery = new XPQuery<PurchasePrePaymentInvoiceMonitoring>(Session);

                            var _purchasePrePaymentInvoiceMonitorings = from ppim in _purchasePrePaymentInvoiceMonitoringsQuery
                                                              where (ppim.Status != Status.Close
                                                              && ppim.PurchasePrePaymentInvoice.BuyFromVendor == this.PayableTransaction.BuyFromVendor
                                                              && ppim.PurchasePrePaymentInvoice.PayToVendor == this.PayableTransaction.PayToVendor
                                                              && ppim.PayableTransaction == null
                                                              )
                                                              group ppim by ppim.PurchasePrePaymentInvoice into g
                                                              select new { PurchasePrePaymentInvoice = g.Key };

                            if (_purchasePrePaymentInvoiceMonitorings != null && _purchasePrePaymentInvoiceMonitorings.Count() > 0)
                            {
                                List<string> _stringPPIM = new List<string>();

                                foreach (var _purchasePrePaymentInvoiceMonitoring in _purchasePrePaymentInvoiceMonitorings)
                                {
                                    if (_purchasePrePaymentInvoiceMonitoring != null)
                                    {
                                        _stringPPIM.Add(_purchasePrePaymentInvoiceMonitoring.PurchasePrePaymentInvoice.Code);
                                    }
                                }

                                IEnumerable<string> _stringArrayPPIMDistinct = _stringPPIM.Distinct();
                                string[] _stringArrayPPIMList = _stringArrayPPIMDistinct.ToArray();
                                if (_stringArrayPPIMList.Length == 1)
                                {
                                    for (int i = 0; i < _stringArrayPPIMList.Length; i++)
                                    {
                                        if (i == 0)
                                        {
                                            _beginString = "[Code]=='" + _stringArrayPPIMList[i] + "'";
                                        }
                                    }
                                }
                                else if (_stringArrayPPIMList.Length > 1)
                                {
                                    for (int i = 0; i < _stringArrayPPIMList.Length; i++)
                                    {
                                        if (i == 0)
                                        {
                                            _beginString = "[Code]=='" + _stringArrayPPIMList[i] + "'";
                                        }
                                        else
                                        {
                                            _endString = _endString + " OR [Code]=='" + _stringArrayPPIMList[i] + "'";
                                        }
                                    }
                                }
                                _fullString = _beginString + _endString;

                                if (_fullString != null)
                                {
                                    _availablePurchasePrePaymentInvoice = new XPCollection<PurchasePrePaymentInvoice>(Session, CriteriaOperator.Parse(_fullString));
                                }
                            }
                        }
                    }
                }
                return _availablePurchasePrePaymentInvoice;
            }
        }

        [DataSourceProperty("AvailablePurchasePrePaymentInvoice", DataSourcePropertyIsNullMode.SelectNothing)]
        public PurchasePrePaymentInvoice PurchasePrePaymentInvoice
        {
            get { return _purchasePrePaymentInvoice; }
            set { SetPropertyValue("PurchasePrePaymentInvoice", ref _purchasePrePaymentInvoice, value); }
        }

        [VisibleInListView(false)]
        [ImmediatePostData()]
        [Appearance("PayablePurchaseCollectionPayableTransactionClose", Enabled = false)]
        [Association("PayableTransaction-PayablePurchaseCollections")]
        public PayableTransaction PayableTransaction
        {
            get { return _payableTransaction; }
            set
            {
                SetPropertyValue("PayableTransaction", ref _payableTransaction, value);
                if(this._payableTransaction != null)
                {
                    if(this._payableTransaction.CompanyDefault != null)
                    {
                        this.Company = this._payableTransaction.CompanyDefault;
                    }
                    if (this._payableTransaction.Workplace != null)
                    {
                        this.Workplace = this._payableTransaction.Workplace;
                    }
                    if (this._payableTransaction.Division != null)
                    {
                        this.Division = this._payableTransaction.Division;
                    }
                    if (this._payableTransaction.Department != null)
                    {
                        this.Department = this._payableTransaction.Department;
                    }
                    if (this._payableTransaction.Section != null)
                    {
                        this.Section = this._payableTransaction.Section;
                    }
                    if (this._payableTransaction.Employee != null)
                    {
                        this.Employee = this._payableTransaction.Employee;
                    }
                }
            }
        }

        #region Organization
        
        [Appearance("PayablePurchaseCollectionCompanyClose", Enabled = false)]
        public Company Company
        {
            get { return _company; }
            set { SetPropertyValue("Company", ref _company, value); }
        }
        
        [Appearance("PayablePurchaseCollectionWorkplaceClose", Enabled = false)]
        public Workplace Workplace
        {
            get { return _workplace; }
            set { SetPropertyValue("Workplace", ref _workplace, value); }
        }

        [Browsable(false)]
        public Division Division
        {
            get { return _division; }
            set { SetPropertyValue("Division", ref _division, value); }
        }

        [Browsable(false)]
        public Department Department
        {
            get { return _department; }
            set { SetPropertyValue("Department", ref _department, value); }
        }

        [Browsable(false)]
        public Section Section
        {
            get { return _section; }
            set { SetPropertyValue("Section", ref _section, value); }
        }

        [Browsable(false)]
        public Employee Employee
        {
            get { return _employee; }
            set { SetPropertyValue("Employee", ref _employee, value); }
        }

        #endregion Organization

        [VisibleInListView(false)]
        [Appearance("PayablePurchaseCollectionStatusClose", Enabled = false)]
        public Status Status
        {
            get { return _status; }
            set { SetPropertyValue("Status", ref _status, value); }
        }

        [VisibleInListView(false)]
        [ModelDefault("DisplayFormat", "{0:dd/MM/yyyy}")]
        [ModelDefault("EditMask", "dd/MM/yyyy")]
        [Appearance("PayablePurchaseCollectionStatusDateClose", Enabled = false)]
        public DateTime StatusDate
        {
            get { return _statusDate; }
            set { SetPropertyValue("StatusDate", ref _statusDate, value); }
        }

        [VisibleInListView(false)]
        [Appearance("PayablePurchaseCollectionPostedCountEnabled", Enabled = false)]
        public int PostedCount
        {
            get { return _postedCount; }
            set { SetPropertyValue("PostedCount", ref _postedCount, value); }
        }

        //AuditTrail
        [CollectionOperationSet(AllowAdd = false, AllowRemove = true)]
        public XPCollection<AuditDataItemPersistent> ChangeHistory
        {
            get
            {
                if (changeHistory == null)
                {
                    changeHistory = AuditedObjectWeakReference.GetAuditTrail(Session, this);
                }
                return changeHistory;
            }
        }

        #endregion Field

    }
}