﻿using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using FullDrive.Module.CustomProcess;
using FullDrive.Module.BusinessObjects;
using DevExpress.ExpressApp.ConditionalAppearance;
using DevExpress.XtraReports.UI;

using System.Web;
using System.Configuration;
using System.Web.Configuration;
using System.IO;

namespace FullDrive.Module.BusinessObjects
{
    [DefaultClassOptions]
    [NavigationItem("Setup")]
    //[ImageName("BO_Contact")]
    //[DefaultProperty("DisplayMemberNameForLookupEditorsOfThisType")]
    //[DefaultListViewOptions(MasterDetailMode.ListViewOnly, false, NewItemRowPosition.None)]
    //[Persistent("DatabaseTableName")]
    // Specify more UI options using a declarative approach (https://documentation.devexpress.com/#eXpressAppFramework/CustomDocument112701).
    public class ApplicationImport : BaseObject
    { // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).
        private string _code;
        private ListImport _no;
        private FileData _dataImport;
        private ObjectList _objectsList;
        private string _message;
        private GlobalFunction _globFunc;
        //AuditTrail
        private XPCollection<AuditDataItemPersistent> changeHistory;

        public ApplicationImport(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if (!IsLoading)
            {
                _globFunc = new GlobalFunction();
                this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.ApplicationImport);
            }
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).
        }

        #region Field 

        public string Code
        {
            get { return _code; }
            set { SetPropertyValue("Code", ref _code, value); }
        }

        [RuleRequiredField(DefaultContexts.Save)]
        [ImmediatePostData()]
        public ListImport No
        {
            get { return _no; }
            set { SetPropertyValue("No", ref _no, value); }
        }

        public ObjectList ObjectList
        {
            get
            {
                if (No != null)
                {
                    this._objectsList = this.No.ObjectList;
                }
                else
                {
                    this._objectsList = CustomProcess.ObjectList.None;
                }
                return _objectsList;
            }
        }

        [ImmediatePostData()]
        public FileData DataImport
        {
            get { return _dataImport; }
            set {
                SetPropertyValue("DataImport", ref _dataImport, value);
                if(!IsLoading)
                {
                    if(this._dataImport != null)
                    {
                        string fileExist = null;
                        fileExist = HttpContext.Current.Server.MapPath("~/UploadFile/" + DataImport.FileName);
                        if (File.Exists(fileExist))
                        {
                            this.Message = "File is already available in server";
                        }
                        else
                        {
                            this.Message = null;
                        }
                    }else
                    {
                        this.Message = null;
                    }
                }
            }
        }

        [Appearance("ApplicationImportMessageClose", Enabled = false)]
        public string Message
        {
            get { return _message; }
            set { SetPropertyValue("Message", ref _message, value); }
        }

        //AuditTrail
        [CollectionOperationSet(AllowAdd = false, AllowRemove = true)]
        public XPCollection<AuditDataItemPersistent> ChangeHistory
        {
            get
            {
                if (changeHistory == null)
                {
                    changeHistory = AuditedObjectWeakReference.GetAuditTrail(Session, this);
                }
                return changeHistory;
            }
        }

        #endregion Field

        //=============================================== Code In Here ===============================================

        //[Action(Caption = "Import", ConfirmationMessage = "Are you sure?", AutoCommit = false)]
        //public void Import()
        //{
        //    try
        //    {
        //        if (this.DataImport != null && this.No.ObjectList != CustomProcess.ObjectList.None)
        //        {
        //            string targetpath = null;
        //            if (Message != "File is already available in server")
        //            {
        //                targetpath = HttpContext.Current.Server.MapPath("~/UploadFile/" + this.DataImport.FileName);
        //                string ext = System.IO.Path.GetExtension(this.DataImport.FileName);
        //                FileStream fileStream = new FileStream(targetpath, FileMode.OpenOrCreate);
        //                this.DataImport.SaveToStream(fileStream);
        //                fileStream.Close();
        //                if (File.Exists(targetpath))
        //                {
        //                    _globFunc = new GlobalFunction();
        //                    _globFunc.ImportDataExcel(Session, targetpath, ext, this.No.ObjectList);
        //                }
        //                this.DataImport.Clear();
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        Tracing.Tracer.LogError(" Business Object = ApplicationImport", ex.ToString());
        //    }
        //}

    }
}