﻿using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;

using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.ConditionalAppearance;

namespace FullDrive.Module.BusinessObjects
{
    [DefaultClassOptions]
    [DefaultProperty("Code")]
    [NavigationItem("Setup")]
    [RuleCombinationOfPropertiesIsUnique("OrganizationSetupDetailRuleUnique", DefaultContexts.Save, "Code")]
    //[ImageName("BO_Contact")]
    //[DefaultProperty("DisplayMemberNameForLookupEditorsOfThisType")]
    //[DefaultListViewOptions(MasterDetailMode.ListViewOnly, false, NewItemRowPosition.None)]
    //[Persistent("DatabaseTableName")]
    // Specify more UI options using a declarative approach (https://documentation.devexpress.com/#eXpressAppFramework/CustomDocument112701).
    public class OrganizationSetupDetail : BaseObject
    { // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).
        private string _code;
        private string _name;
        private Country _country;
        private Company _company;
        private XPCollection<Workplace> _availableWorkplace;
        private bool _openWorkplace;
        private Workplace _workplace;
        private UserAccess _userAccess;
        private Division _division;
        private Department _department;
        private Section _section;
        private Employee _employee;
        private Process _process;
        private ObjectList _objectList;
        private PageType _pageType;
        private bool _active;
        private bool _default;
        private bool _allWorkplace;
        private ApplicationSetup _applicationSetup;
        private string _userAccessIn;
        private GlobalFunction _globFunc;
        //AuditTrail
        private XPCollection<AuditDataItemPersistent> changeHistory;

        public OrganizationSetupDetail(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if (!IsLoading)
            {
                _globFunc = new GlobalFunction();
                _userAccessIn = SecuritySystem.CurrentUserName;
                UserAccess _locUserAccess = Session.FindObject<UserAccess>(new BinaryOperator("UserName", _userAccessIn));

                if (_locUserAccess != null)
                {
                    if (_locUserAccess.Employee != null)
                    {
                        if (_locUserAccess.Employee.Company != null && _locUserAccess.Employee.Workplace != null)
                        {
                            this.Code = _globFunc.GetNumberingUnlockOptimisticRecordBasedOrganization(this.Session.DataLayer, ObjectList.OrganizationSetupDetail, _locUserAccess.Employee.Company, _locUserAccess.Employee.Workplace);
                        }
                        else
                        {
                            this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.OrganizationSetupDetail);
                        }
                        if (_locUserAccess.Employee.Company != null)
                        {
                            this.Company = _locUserAccess.Employee.Company;
                        }
                        if (_locUserAccess.Employee.Workplace != null)
                        {
                            this.Workplace = _locUserAccess.Employee.Workplace;
                        }else
                        {
                            this.Workplace = _globFunc.GetDefaultWorkplace(this.Session, _locUserAccess);
                        }
                    }
                }
            }
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).
        }

        #region Field

        [RuleRequiredField(DefaultContexts.Save)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue("Code", ref _code, value); }
        }

        public string Name
        {
            get { return _name; }
            set { SetPropertyValue("Name", ref _name, value); }
        }

        [DataSourceCriteria("Active = true")]
        public Country Country
        {
            get { return _country; }
            set { SetPropertyValue("Country", ref _country, value); }
        }

        [ImmediatePostData()]
        [DataSourceCriteria("Active = true")]
        public Company Company
        {
            get { return _company; }
            set { SetPropertyValue("Company", ref _company, value); }
        }

        [Browsable(false)]
        public XPCollection<Workplace> AvailableWorkplace
        {
            get
            {
                if (this.Company != null)
                {
                    _availableWorkplace = new XPCollection<Workplace>(Session,
                                        new GroupOperator(GroupOperatorType.And,
                                        new BinaryOperator("Company", this.Company),
                                        new BinaryOperator("Active", true)));
                }
                else
                {
                    _availableWorkplace = new XPCollection<Workplace>(Session,
                                        new GroupOperator(GroupOperatorType.And,
                                        new BinaryOperator("Active", true)));
                }

                return _availableWorkplace;

            }
        }

        public bool OpenWorkplace
        {
            get { return _openWorkplace; }
            set { SetPropertyValue("OpenWorkplace", ref _openWorkplace, value); }
        }

        [DataSourceProperty("AvailableWorkplace")]
        public Workplace Workplace
        {
            get { return _workplace; }
            set { SetPropertyValue("Workplace", ref _workplace, value); }
        }

        public UserAccess UserAccess
        {
            get { return _userAccess; }
            set { SetPropertyValue("UserAccess", ref _userAccess, value); }
        }

        //harus ditambahkan available based on company biar gak error di mobile
        [DataSourceCriteria("Active = true")]
        public Division Division
        {
            get { return _division; }
            set { SetPropertyValue("Division", ref _division, value); }
        }

        [DataSourceCriteria("Active = true")]
        public Department Department
        {
            get { return _department; }
            set { SetPropertyValue("Department", ref _department, value); }
        }

        [DataSourceCriteria("Active = true")]
        public Section Section
        {
            get { return _section; }
            set { SetPropertyValue("Section", ref _section, value); }
        }

        [DataSourceCriteria("Active = true")]
        public Employee Employee
        {
            get { return _employee; }
            set { SetPropertyValue("Employee", ref _employee, value); }
        }

        public Process Process
        {
            get { return _process; }
            set { SetPropertyValue("Process", ref _process, value); }
        }

        public ObjectList ObjectList
        {
            get { return _objectList; }
            set { SetPropertyValue("ObjectList", ref _objectList, value); }
        }

        public PageType PageType
        {
            get { return _pageType; }
            set { SetPropertyValue("PageType", ref _pageType, value); }
        }

        public bool Active
        {
            get { return _active; }
            set { SetPropertyValue("Active", ref _active, value); }
        }

        public bool Default
        {
            get { return _default; }
            set { SetPropertyValue("Default", ref _default, value); }
        }

        public bool AllWorkplace
        {
            get { return _allWorkplace; }
            set { SetPropertyValue("AllWorkplace", ref _allWorkplace, value); }
        }

        [Association("ApplicationSetup-OrganizationSetupDetails")]
        public ApplicationSetup ApplicationSetup
        {
            get { return _applicationSetup; }
            set { SetPropertyValue("ApplicationSetup", ref _applicationSetup, value); }
        }

        //AuditTrail
        [CollectionOperationSet(AllowAdd = false, AllowRemove = true)]
        public XPCollection<AuditDataItemPersistent> ChangeHistory
        {
            get
            {
                if (changeHistory == null)
                {
                    changeHistory = AuditedObjectWeakReference.GetAuditTrail(Session, this);
                }
                return changeHistory;
            }
        }

        #endregion Field
    }
}