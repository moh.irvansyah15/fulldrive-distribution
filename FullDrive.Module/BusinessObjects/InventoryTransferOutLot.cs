﻿using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;

using DevExpress.ExpressApp.ConditionalAppearance;
using DevExpress.ExpressApp.SystemModule;
using FullDrive.Module.CustomProcess;

namespace FullDrive.Module.BusinessObjects
{
    [DefaultClassOptions]
    [DefaultProperty("Code")]
    [NavigationItem("Inventory")]
    [RuleCombinationOfPropertiesIsUnique("InventoryTransferOutLotRuleUnique", DefaultContexts.Save, "Code")]
    //[ImageName("BO_Contact")]
    //[DefaultProperty("DisplayMemberNameForLookupEditorsOfThisType")]
    //[DefaultListViewOptions(MasterDetailMode.ListViewOnly, false, NewItemRowPosition.None)]
    //[Persistent("DatabaseTableName")]
    // Specify more UI options using a declarative approach (https://documentation.devexpress.com/#eXpressAppFramework/CustomDocument112701).
    public class InventoryTransferOutLot : BaseObject
    { // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).
        private bool _activationPosting;
        private int _no;
        private string _code;
        private bool _select;
        #region InitialOrganization
        private Company _company;
        private Workplace _workplace;
        private Division _division;
        private Department _department;
        private Section _section;
        private Employee _employee;
        #endregion InitialOrganization
        private Item _item;
        private Brand _brand;
        private string _description;
        #region InisialisasiLocation
        private XPCollection<Location> _availableLocation;
        private Location _location;
        private XPCollection<BinLocation> _availableBinLocation;
        private BinLocation _binLocation;
        private StockType _stockType;
        #endregion InisialisasiLocation
        private XPCollection<BeginningInventoryLine> _availableBeginningInventoryLine;
        private BeginningInventoryLine _begInvLine;
        private string _lotNumber;
        private XPCollection<UnitOfMeasure> _availableUnitOfMeasure;
        #region InisialisasiNewDefaultQty
        private double _qtyLot;
        private UnitOfMeasure _unitPack;
        private double _dQty;
        private UnitOfMeasure _dUom;
        private double _qty;
        private UnitOfMeasure _uom;
        private double _tQty;
        #endregion InisialisasiNewDefaultQty
        private Status _status;
        private DateTime _statusDate;
        private InventoryTransferOutLine _inventoryTransferOutLine;
        private InventoryTransferOut _inventoryTransferOut;        
        private string _userAccess;
        private GlobalFunction _globFunc;

        public InventoryTransferOutLot(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if (!IsLoading)
            {
                if (Session.IsNewObject(this))
                {
                    _globFunc = new GlobalFunction();
                    DateTime now = DateTime.Now;
                    this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.InventoryTransferOutLot);
                    this.Status = Status.Open;
                    this.StatusDate = now;
                    this.StockType = StockType.Good;
                    this.Select = true;
                }
            }
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).
        }

        protected override void OnSaving()
        {
            base.OnSaving();
            if (!IsLoading && IsSaving)
            {
                UpdateNo();
            }
        }

        protected override void OnDeleting()
        {
            base.OnDeleting();
            if (!IsLoading)
            {
                RecoveryDeleteNo();
            }
        }

        #region Field

        [Browsable(false)]
        public bool ActivationPosting
        {
            get { return _activationPosting; }
            set { SetPropertyValue("ActivationPosting", ref _activationPosting, value); }
        }

        [Appearance("InventoryTransferOutLotNoEnabled", Enabled = false)]
        public int No
        {
            get { return _no; }
            set { SetPropertyValue("No", ref _no, value); }
        }

        [RuleRequiredField(DefaultContexts.Save)]
        [Appearance("InventoryTransferOutLotCodeEnabled", Enabled = false)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue("Code", ref _code, value); }
        }

        [Appearance("InventoryTransferOutLotSelectClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public bool Select
        {
            get { return _select; }
            set { SetPropertyValue("Select", ref _select, value); }
        }

        #region Organization

        [Appearance("InventoryTransferOutLotCompanyClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Company Company
        {
            get { return _company; }
            set { SetPropertyValue("Company", ref _company, value); }
        }

        [Appearance("InventoryTransferOutLotWorkplaceClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Workplace Workplace
        {
            get { return _workplace; }
            set { SetPropertyValue("Workplace", ref _workplace, value); }
        }

        [Browsable(false)]
        public Division Division
        {
            get { return _division; }
            set { SetPropertyValue("Division", ref _division, value); }
        }

        [Browsable(false)]
        public Department Department
        {
            get { return _department; }
            set { SetPropertyValue("Department", ref _department, value); }
        }

        [Browsable(false)]
        public Section Section
        {
            get { return _section; }
            set { SetPropertyValue("Section", ref _section, value); }
        }

        [Browsable(false)]
        public Employee Employee
        {
            get { return _employee; }
            set { SetPropertyValue("Employee", ref _employee, value); }
        }

        #endregion Organization

        [ImmediatePostData()]
        [Appearance("InventoryTransferOutLotItemClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Item Item
        {
            get { return _item; }
            set
            {
                SetPropertyValue("Item", ref _item, value);
                if (!IsLoading)
                {
                    if (this._item != null)
                    {
                        this.DUOM = this._item.BasedUOM;
                        this.Brand = this._item.Brand;
                        this.Description = GetDescriptions();
                    }
                }
            }
        }

        [Appearance("InventoryTransferOutLotBrandClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Brand Brand
        {
            get { return _brand; }
            set { SetPropertyValue("Brand", ref _brand, value); }
        }

        [Appearance("InventoryTransferOutLotDescriptionClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [Size(512)]
        public string Description
        {
            get { return _description; }
            set { SetPropertyValue("Description", ref _description, value); }
        }

        #region Location

        [Browsable(false)]
        public XPCollection<Location> AvailableLocation
        {
            get
            {
                if(!IsLoading)
                {
                    _userAccess = SecuritySystem.CurrentUserName;
                    string _beginString = null;
                    string _endString = null;
                    string _fullString = null;
                    string _locLocationCode = null;
                    DirectionType _locTransferType = DirectionType.None;
                    InventoryMovingType _locInventoryMovingType = InventoryMovingType.None;
                    UserAccess _locUserAccess = Session.FindObject<UserAccess>(new BinaryOperator("UserName", _userAccess));

                    if (_locUserAccess != null)
                    {
                        if (this.InventoryTransferOutLine != null)
                        {
                            if (InventoryTransferOutLine.InventoryTransferOut != null)
                            {
                                if (InventoryTransferOutLine.InventoryTransferOut.TransferType != DirectionType.None)
                                {
                                    _locTransferType = InventoryTransferOutLine.InventoryTransferOut.TransferType;
                                }

                                if (InventoryTransferOutLine.InventoryTransferOut.InventoryMovingType != InventoryMovingType.None)
                                {
                                    _locInventoryMovingType = InventoryTransferOutLine.InventoryTransferOut.InventoryMovingType;
                                }
                            }
                            List<string> _stringLocation = new List<string>();

                            XPCollection<WarehouseSetupDetail> _locWhsSetupDetails = new XPCollection<WarehouseSetupDetail>
                                                                                      (Session, new GroupOperator(GroupOperatorType.And,
                                                                                       new BinaryOperator("UserAccess", _locUserAccess),
                                                                                       new BinaryOperator("ObjectList", ObjectList.InventoryTransferOut),
                                                                                       new BinaryOperator("Owner", true),
                                                                                       new BinaryOperator("TransferType", _locTransferType),
                                                                                       new BinaryOperator("InventoryMovingType", _locInventoryMovingType),
                                                                                       new BinaryOperator("LocationType", LocationType.Main),
                                                                                       new BinaryOperator("Active", true)));

                            if (_locWhsSetupDetails != null && _locWhsSetupDetails.Count() > 0)
                            {
                                foreach (WarehouseSetupDetail _locWhsSetupDetail in _locWhsSetupDetails)
                                {
                                    if (_locWhsSetupDetail.Location != null)
                                    {
                                        if (_locWhsSetupDetail.Location.Code != null)
                                        {
                                            _locLocationCode = _locWhsSetupDetail.Location.Code;
                                            _stringLocation.Add(_locLocationCode);
                                        }
                                    }
                                }
                            }

                            IEnumerable<string> _stringArrayLocationFromDistinct = _stringLocation.Distinct();
                            string[] _stringArrayLocationFromList = _stringArrayLocationFromDistinct.ToArray();
                            if (_stringArrayLocationFromList.Length == 1)
                            {
                                for (int i = 0; i < _stringArrayLocationFromList.Length; i++)
                                {
                                    Location _locLocationFrom = Session.FindObject<Location>(new BinaryOperator("Code", _stringArrayLocationFromList[i]));
                                    if (_locLocationFrom != null)
                                    {
                                        if (i == 0)
                                        {
                                            _beginString = "[Code]=='" + _locLocationFrom.Code + "'";
                                        }
                                    }
                                }
                            }
                            else if (_stringArrayLocationFromList.Length > 1)
                            {
                                for (int i = 0; i < _stringArrayLocationFromList.Length; i++)
                                {
                                    Location _locLocationFrom = Session.FindObject<Location>(new BinaryOperator("Code", _stringArrayLocationFromList[i]));
                                    if (_locLocationFrom != null)
                                    {
                                        if (i == 0)
                                        {
                                            _beginString = "[Code]=='" + _locLocationFrom.Code + "'";
                                        }
                                        else
                                        {
                                            _endString = " OR [Code]=='" + _locLocationFrom.Code + "'";
                                        }
                                    }
                                }
                            }
                            _fullString = _beginString + _endString;

                            if (_fullString != null)
                            {
                                _availableLocation = new XPCollection<Location>(Session, CriteriaOperator.Parse(_fullString));
                            }
                        }
                    }
                    else
                    {
                        _availableLocation = new XPCollection<Location>(Session);
                    }
                }
                return _availableLocation;
            }
        }

        [Appearance("InventoryTransferOutLotLocationClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [DataSourceProperty("AvailableLocation", DataSourcePropertyIsNullMode.SelectAll)]
        [ImmediatePostData()]
        [DataSourceCriteria("Active = true")]
        public Location Location
        {
            get { return _location; }
            set { SetPropertyValue("Location", ref _location, value); }
        }

        [Browsable(false)]
        public XPCollection<BinLocation> AvailableBinLocation
        {
            get
            {
                if(!IsLoading)
                {
                    _userAccess = SecuritySystem.CurrentUserName;
                    string _beginString = null;
                    string _endString = null;
                    string _fullString = null;
                    string _locBinLocationCode = null;
                    UserAccess _locUserAccess = Session.FindObject<UserAccess>(new BinaryOperator("UserName", _userAccess));
                    if (_locUserAccess != null)
                    {
                        if (this.Location != null)
                        {
                            if (this.InventoryTransferOutLine != null)
                            {
                                List<string> _stringBinLocation = new List<string>();

                                XPCollection<WarehouseSetupDetail> _locWhsSetupDetails = new XPCollection<WarehouseSetupDetail>
                                                                                          (Session, new GroupOperator(GroupOperatorType.And,
                                                                                           new BinaryOperator("UserAccess", _locUserAccess),
                                                                                           new BinaryOperator("Location", this.Location),
                                                                                           new BinaryOperator("Owner", true),
                                                                                           new BinaryOperator("Active", true)));

                                if (_locWhsSetupDetails != null && _locWhsSetupDetails.Count() > 0)
                                {
                                    foreach (WarehouseSetupDetail _locWhsSetupDetail in _locWhsSetupDetails)
                                    {
                                        if (_locWhsSetupDetail.BinLocation != null)
                                        {
                                            if (_locWhsSetupDetail.BinLocation.Code != null)
                                            {
                                                _locBinLocationCode = _locWhsSetupDetail.BinLocation.Code;
                                                _stringBinLocation.Add(_locBinLocationCode);
                                            }
                                        }
                                    }
                                }

                                IEnumerable<string> _stringArrayBinLocationFromDistinct = _stringBinLocation.Distinct();
                                string[] _stringArrayBinLocationFromList = _stringArrayBinLocationFromDistinct.ToArray();
                                if (_stringArrayBinLocationFromList.Length == 1)
                                {
                                    for (int i = 0; i < _stringArrayBinLocationFromList.Length; i++)
                                    {
                                        BinLocation _locBinLocationFrom = Session.FindObject<BinLocation>(new BinaryOperator("Code", _stringArrayBinLocationFromList[i]));
                                        if (_locBinLocationFrom != null)
                                        {
                                            if (i == 0)
                                            {
                                                _beginString = "[Code]=='" + _locBinLocationFrom.Code + "'";
                                            }
                                        }
                                    }
                                }
                                else if (_stringArrayBinLocationFromList.Length > 1)
                                {
                                    for (int i = 0; i < _stringArrayBinLocationFromList.Length; i++)
                                    {
                                        BinLocation _locBinLocationFrom = Session.FindObject<BinLocation>(new BinaryOperator("Code", _stringArrayBinLocationFromList[i]));
                                        if (_locBinLocationFrom != null)
                                        {
                                            if (i == 0)
                                            {
                                                _beginString = "[Code]=='" + _locBinLocationFrom.Code + "'";
                                            }
                                            else
                                            {
                                                _endString = " OR [Code]=='" + _locBinLocationFrom.Code + "'";
                                            }
                                        }
                                    }
                                }
                                _fullString = _beginString + _endString;

                                if (_fullString != null)
                                {
                                    _availableBinLocation = new XPCollection<BinLocation>(Session, CriteriaOperator.Parse(_fullString));
                                }
                            }
                        }
                    }
                    else
                    {
                        _availableBinLocation = new XPCollection<BinLocation>(Session);
                    }
                }
                return _availableBinLocation;
            }
        }

        [Appearance("InventoryTransferOutLotBinLocationClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [DataSourceProperty("AvailableBinLocation", DataSourcePropertyIsNullMode.SelectAll)]
        [DataSourceCriteria("Active = true")]
        public BinLocation BinLocation
        {
            get { return _binLocation; }
            set { SetPropertyValue("BinLocation", ref _binLocation, value); }
        }

        [Appearance("InventoryTransferOutLotStockTypeClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public StockType StockType
        {
            get { return _stockType; }
            set { SetPropertyValue("StockType", ref _stockType, value); }
        }

        #endregion Location

        [Browsable(false)]
        public XPCollection<BeginningInventoryLine> AvailableBeginningInventoryLine
        {
            get
            {
                if(!IsLoading)
                {
                    string _locItem = null;
                    string _locLocation = null;
                    string _locBinLocation = null;
                    string _fullString = null;
                    if (this.InventoryTransferOutLine != null)
                    {
                        if (Item == null)
                        {
                            _availableBeginningInventoryLine = new XPCollection<BeginningInventoryLine>(Session);
                        }
                        else
                        {
                            //If For Item
                            if (this.Item != null) { _locItem = "[Item.Code]=='" + this.Item.Code + "'"; }
                            else { _locItem = ""; }

                            //If For Location
                            if (this.Location != null && this.Item != null)
                            { _locLocation = " AND [Location.Code]=='" + this.Location.Code + "'"; }
                            else if (this.Location != null && this.Item == null)
                            { _locLocation = " [Location.Code]=='" + this.Location.Code + "'"; }
                            else { _locLocation = ""; }

                            //If for BinLocation
                            if (this.BinLocation != null && (this.Item != null || this.Location != null))
                            { _locBinLocation = " AND [BinLocation.Code]=='" + this.BinLocation.Code + "'"; }
                            else if (this.BinLocation != null && this.Item != null && this.Location != null)
                            { _locBinLocation = "[BinLocation.Code] == '" + this.BinLocation.Code + "'"; }
                            else { _locBinLocation = ""; }

                            if (_locItem != null || _locLocation != null || _locBinLocation != null)
                            {
                                _fullString = _locItem + _locLocation + _locBinLocation;
                            }

                            if (_fullString != null)
                            {
                                _availableBeginningInventoryLine = new XPCollection<BeginningInventoryLine>(Session, CriteriaOperator.Parse(_fullString));
                            }
                        }
                    }
                }
                return _availableBeginningInventoryLine;
            }
        }

        [ImmediatePostData()]
        [DataSourceProperty("AvailableBeginningInventoryLine", DataSourcePropertyIsNullMode.SelectAll)]
        [Appearance("InventoryTransferOutLotBegInvLineClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public BeginningInventoryLine BegInvLine
        {
            get { return _begInvLine; }
            set
            {
                SetPropertyValue("BegInvLine", ref _begInvLine, value);
                if (!IsLoading)
                {
                    if (this._begInvLine != null)
                    {
                        if (this._begInvLine.LotNumber != null)
                        {
                            this.LotNumber = this._begInvLine.LotNumber;
                        }
                        //if (this._begInvLine.UnitPack != null)
                        //{
                        //    this.UnitPack = this._begInvLine.UnitPack;
                        //}
                    }
                }
            }
        }

        [Appearance("InventoryTransferOutLotNewLotNumberClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public string LotNumber
        {
            get { return _lotNumber; }
            set { SetPropertyValue("LotNumber", ref _lotNumber, value); }
        }

        [Persistent]
        public double QtyAvailable
        {
            get
            {
                double _locQtyAvailable = 0;
                if (this.BegInvLine != null)
                {
                    if (this.BegInvLine.QtyAvailable > 0)
                    {
                        _locQtyAvailable = this.BegInvLine.QtyAvailable;
                    }
                    return _locQtyAvailable;
                }
                return 0;
            }
        }

        [Persistent]
        public string UOM_Available
        {
            get
            {
                string _locName = null;
                if (this.BegInvLine != null)
                {
                    if (this.BegInvLine.FullName != null)
                    {
                        _locName = this.BegInvLine.FullName;
                    }
                    return String.Format("{0}", _locName);
                }
                return null;
            }
        }

        [Browsable(false)]
        public XPCollection<UnitOfMeasure> AvailableUnitOfMeasure
        {
            get
            {
                if(!IsLoading)
                {
                    string _beginString = null;
                    string _endString = null;
                    string _fullString = null;
                    if (Item == null)
                    {
                        _availableUnitOfMeasure = new XPCollection<UnitOfMeasure>(Session);
                    }
                    else
                    {
                        List<string> _stringUOM = new List<string>();

                        XPCollection<ItemUnitOfMeasure> _locItemUnitOfMeasures = new XPCollection<ItemUnitOfMeasure>
                                                                                  (Session, new GroupOperator(GroupOperatorType.And,
                                                                                   new BinaryOperator("Item", this.Item),
                                                                                   new BinaryOperator("Active", true)));

                        if (_locItemUnitOfMeasures != null && _locItemUnitOfMeasures.Count() > 0)
                        {
                            foreach (ItemUnitOfMeasure _locItemUnitOfMeasure in _locItemUnitOfMeasures)
                            {
                                _stringUOM.Add(_locItemUnitOfMeasure.UOM.Code);
                            }
                        }

                        IEnumerable<string> _stringArrayUOMDistinct = _stringUOM.Distinct();
                        string[] _stringArrayUOMList = _stringArrayUOMDistinct.ToArray();
                        if (_stringArrayUOMList.Length == 1)
                        {
                            for (int i = 0; i < _stringArrayUOMList.Length; i++)
                            {
                                UnitOfMeasure _locUOM = Session.FindObject<UnitOfMeasure>(new BinaryOperator("Code", _stringArrayUOMList[i]));
                                if (_locUOM != null)
                                {
                                    if (i == 0)
                                    {
                                        _beginString = "[Code]=='" + _locUOM.Code + "'";
                                    }
                                }
                            }
                        }
                        else if (_stringArrayUOMList.Length > 1)
                        {
                            for (int i = 0; i < _stringArrayUOMList.Length; i++)
                            {
                                UnitOfMeasure _locUOM = Session.FindObject<UnitOfMeasure>(new BinaryOperator("Code", _stringArrayUOMList[i]));
                                if (_locUOM != null)
                                {
                                    if (i == 0)
                                    {
                                        _beginString = "[Code]=='" + _locUOM.Code + "'";
                                    }
                                    else
                                    {
                                        _endString = " OR [Code]=='" + _locUOM.Code + "'";
                                    }
                                }
                            }
                        }
                        _fullString = _beginString + _endString;

                        if (_fullString != null)
                        {
                            _availableUnitOfMeasure = new XPCollection<UnitOfMeasure>(Session, CriteriaOperator.Parse(_fullString));
                        }
                    }
                }               
                return _availableUnitOfMeasure;
            }
        }

        #region NewDefaultQty

        [Appearance("InventoryTransferOutLotQtyLotClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public double QtyLot
        {
            get { return _qtyLot; }
            set { SetPropertyValue("QtyLot", ref _qtyLot, value); }
        }

        [Appearance("InventoryTransferOutLotUnitPackClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [ImmediatePostData()]
        public UnitOfMeasure UnitPack
        {
            get { return _unitPack; }
            set { SetPropertyValue("UnitPack", ref _unitPack, value); }
        }

        [Appearance("InventoryTransferOutLotDQtyClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [ImmediatePostData()]
        public double DQty
        {
            get { return _dQty; }
            set
            {
                SetPropertyValue("DQty", ref _dQty, value);
                if (!IsLoading)
                {
                    SetTotalQty();
                }
            }
        }

        [Appearance("InventoryTransferOutLotDUOMClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [ImmediatePostData()]
        [DataSourceCriteria("Active = true")]
        public UnitOfMeasure DUOM
        {
            get { return _dUom; }
            set
            {
                SetPropertyValue("DUOM", ref _dUom, value);
                if (!IsLoading)
                {
                    SetTotalQty();
                }
            }
        }

        [Appearance("InventoryTransferOutLotQtyClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [ImmediatePostData()]
        public double Qty
        {
            get { return _qty; }
            set
            {
                SetPropertyValue("Qty", ref _qty, value);
                if (!IsLoading)
                {
                    SetTotalQty();
                }
            }
        }

        [Appearance("InventoryTransferOutLotUOMClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [ImmediatePostData()]
        [DataSourceProperty("AvailableUnitOfMeasure", DataSourcePropertyIsNullMode.SelectAll)]
        [DataSourceCriteria("Active = true")]
        public UnitOfMeasure UOM
        {
            get { return _uom; }
            set
            {
                SetPropertyValue("UOM", ref _uom, value);
                if (!IsLoading)
                {
                    SetTotalQty();
                }
            }
        }

        [Appearance("InventoryTransferOutLotTQtyEnabled", Enabled = false)]
        public double TQty
        {
            get { return _tQty; }
            set { SetPropertyValue("TQty", ref _tQty, value); }
        }

        #endregion NewDefaultQty

        [Appearance("InventoryTransferOutLotStatusEnabled", Enabled = false)]
        public Status Status
        {
            get { return _status; }
            set { SetPropertyValue("Status", ref _status, value); }
        }

        [ModelDefault("DisplayFormat", "{0:dd/MM/yyyy}")]
        [ModelDefault("EditMask", "dd/MM/yyyy")]
        [Appearance("InventoryTransferOutLotStatusDateEnabled", Enabled = false)]
        public DateTime StatusDate
        {
            get { return _statusDate; }
            set { SetPropertyValue("StatusDate", ref _statusDate, value); }
        }

        [ImmediatePostData]
        [Appearance("InventoryTransferOutLotInventoryTransferOutLineEnabled", Enabled = false)]
        [Association("InventoryTransferOutLine-InventoryTransferOutLots")]
        public InventoryTransferOutLine InventoryTransferOutLine
        {
            get { return _inventoryTransferOutLine; }
            set
            {
                SetPropertyValue("InventoryTransferOutLine", ref _inventoryTransferOutLine, value);
                if (!IsLoading)
                {
                    if (this._inventoryTransferOutLine != null)
                    {
                        this.DQty = this._inventoryTransferOutLine.DQty;
                        if (this._inventoryTransferOutLine.DUOM != null)
                        {
                            this.DUOM = this._inventoryTransferOutLine.DUOM;
                        }
                        this.Qty = this._inventoryTransferOutLine.Qty;
                        if (this._inventoryTransferOutLine.UOM != null)
                        {
                            this.UOM = this._inventoryTransferOutLine.UOM;
                        }
                        if (this._inventoryTransferOutLine.Location != null)
                        {
                            this.Location = this._inventoryTransferOutLine.Location;
                        }
                        if (this._inventoryTransferOutLine.BinLocation != null)
                        {
                            this.BinLocation = this._inventoryTransferOutLine.BinLocation;
                        }
                        if (this._inventoryTransferOutLine.Item != null)
                        {
                            this.Item = this._inventoryTransferOutLine.Item;
                        }
                        if (this._inventoryTransferOutLine.InventoryTransferOut != null)
                        {
                            this.InventoryTransferOut = this._inventoryTransferOutLine.InventoryTransferOut;
                        }
                        if (this._inventoryTransferOutLine.Company != null)
                        {
                            this.Company = this._inventoryTransferOutLine.Company;
                        }
                        if (this._inventoryTransferOutLine.Workplace != null)
                        {
                            this.Workplace = this._inventoryTransferOutLine.Workplace;
                        }
                        if (this._inventoryTransferOutLine.Division != null)
                        {
                            this.Division = this._inventoryTransferOutLine.Division;
                        }
                        if (this._inventoryTransferOutLine.Department != null)
                        {
                            this.Department = this._inventoryTransferOutLine.Department;
                        }
                        if (this._inventoryTransferOutLine.Section != null)
                        {
                            this.Section = this._inventoryTransferOutLine.Section;
                        }
                        if (this._inventoryTransferOutLine.Employee != null)
                        {
                            this.Employee = this._inventoryTransferOutLine.Employee;
                        }
                    }
                }
            }
        }

        public InventoryTransferOut InventoryTransferOut
        {
            get { return _inventoryTransferOut; }
            set { SetPropertyValue("InventoryTransferOut", ref _inventoryTransferOut, value); }
        }

        #endregion Field

        //============================================= Code In Here =============================================

        #region Get Description

        public string GetDescriptions()
        {
            string _result = "";
            try
            {
                if (this._item != null)
                {
                    string _result1 = null;
                    string _result2 = null;
                    if (this._item.Name != null)
                    {
                        _result1 = this._item.Name;
                    }
                    if (this._item.Description != null)
                    {
                        _result2 = this._item.Description;
                    }
                    _result = _result1 + " " + _result2;
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = InventoryTransferOutLot " + ex.ToString());
            }

            return _result;
        }

        #endregion Get Description

        #region No

        public void UpdateNo()
        {
            try
            {
                if (!IsLoading && Session.IsNewObject(this))
                {
                    if (this.InventoryTransferOutLine != null)
                    {
                        object _makRecord = Session.Evaluate<InventoryTransferOutLot>(CriteriaOperator.Parse("Max(No)"), CriteriaOperator.Parse("InventoryTransferOutLine=?", this.InventoryTransferOutLine));
                        this.No = Convert.ToInt32(_makRecord) + 1;
                        this.Save();
                        RecoveryUpdateNo();
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = InventoryTransferOutLot " + ex.ToString());
            }
        }

        public void RecoveryUpdateNo()
        {
            try
            {
                if (this.InventoryTransferOutLine != null)
                {
                    InventoryTransferOutLine _numHeader = Session.FindObject<InventoryTransferOutLine>
                                                (new BinaryOperator("Code", this.InventoryTransferOutLine.Code));

                    XPCollection<InventoryTransferOutLot> _numLines = new XPCollection<InventoryTransferOutLot>
                                                (Session, new BinaryOperator("InventoryTransferOutLine", _numHeader),
                                                new SortProperty("No", DevExpress.Xpo.DB.SortingDirection.Ascending));
                    if (_numLines != null)
                    {
                        int i1 = 0;
                        foreach (InventoryTransferOutLot _numLine in _numLines)
                        {
                            i1 += 1;
                            _numLine.No = i1;
                            _numLine.Save();
                        }
                        i1 = 1;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = InventoryTransferOutLot " + ex.ToString());
            }
        }

        public void RecoveryDeleteNo()
        {
            try
            {
                if (this.InventoryTransferOutLine != null)
                {
                    InventoryTransferOutLine _numHeader = Session.FindObject<InventoryTransferOutLine>
                                                (new BinaryOperator("Code", this.InventoryTransferOutLine.Code));

                    XPCollection<InventoryTransferOutLot> _numLines = new XPCollection<InventoryTransferOutLot>
                                                (Session, new GroupOperator(GroupOperatorType.And,
                                                 new BinaryOperator("This", this, BinaryOperatorType.NotEqual),
                                                 new BinaryOperator("InventoryTransferOutLine", _numHeader)),
                                                 new SortProperty("No", DevExpress.Xpo.DB.SortingDirection.Ascending));
                    if (_numLines != null)
                    {
                        int i = 0;
                        foreach (InventoryTransferOutLot _numLine in _numLines)
                        {
                            i += 1;
                            _numLine.No = i;
                            _numLine.Save();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = InventoryTransferOutLot " + ex.ToString());
            }
        }

        #endregion No

        #region Set

        private void SetTotalQty()
        {
            try
            {
                double _locInvLineTotal = 0;
                DateTime now = DateTime.Now;
                if (this.InventoryTransferOutLine != null)
                {
                    ItemUnitOfMeasure _locItemUOM = Session.FindObject<ItemUnitOfMeasure>
                                                        (new GroupOperator(GroupOperatorType.And,
                                                         new BinaryOperator("Item", this.Item),
                                                         new BinaryOperator("UOM", this.UOM),
                                                         new BinaryOperator("DefaultUOM", this.DUOM),
                                                         new BinaryOperator("Active", true)));
                    if (_locItemUOM != null)
                    {
                        if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                        {
                            _locInvLineTotal = this.Qty * _locItemUOM.DefaultConversion + this.DQty;
                        }
                        else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                        {
                            _locInvLineTotal = this.Qty / _locItemUOM.Conversion + this.DQty;
                        }
                        else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                        {
                            _locInvLineTotal = this.Qty + this.DQty;
                        }

                        this.TQty = _locInvLineTotal;
                    }
                    else
                    {
                        _locInvLineTotal = this.Qty + this.DQty;
                        this.TQty = _locInvLineTotal;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = InventoryTransferOutLot " + ex.ToString());
            }
        }

        #endregion Set

    }
}