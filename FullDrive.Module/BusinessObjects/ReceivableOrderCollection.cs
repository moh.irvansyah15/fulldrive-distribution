﻿using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;

using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.ConditionalAppearance;

namespace FullDrive.Module.BusinessObjects
{
    [DefaultClassOptions]
    [DefaultProperty("Code")]
    [NavigationItem("Finance & Accounting")]
    [RuleCombinationOfPropertiesIsUnique("ReceivableOrderCollectionRuleUnique", DefaultContexts.Save, "Code")]
    //[ImageName("BO_Contact")]
    //[DefaultProperty("DisplayMemberNameForLookupEditorsOfThisType")]
    //[DefaultListViewOptions(MasterDetailMode.ListViewOnly, false, NewItemRowPosition.None)]
    //[Persistent("DatabaseTableName")]
    // Specify more UI options using a declarative approach (https://documentation.devexpress.com/#eXpressAppFramework/CustomDocument112701).
    public class ReceivableOrderCollection : BaseObject
    { // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).
        private bool _activationPosting;
        private string _code;
        #region InitialOrganization
        private Company _company;
        private Workplace _workplace;
        private Division _division;
        private Department _department;
        private Section _section;
        private Employee _employee;
        #endregion InitialOrganization
        private Employee _pic;
        private SalesmanVehicleSetup _selectVehicle;
        private XPCollection<Vehicle> _availableVehicle;
        private Vehicle _vehicle;
        private bool _isRoute;
        private DateTime _pickingDate;
        private XPCollection<Picking> _availablePicking;
        private Picking _picking;
        private XPCollection<OrderCollection> _availableOrderCollection;
        private OrderCollection _orderCollection;
        private ReceivableTransaction _receivableTransaction;
        private Status _status;
        private DateTime _statusDate;
        private int _postedCount;
        private string _userAccessIn;
        private GlobalFunction _globFunc;

        public ReceivableOrderCollection(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if (!IsLoading)
            {
                if(Session.IsNewObject(this))
                {
                    _globFunc = new GlobalFunction();
                    DateTime now = DateTime.Now;
                    #region UserAccess
                    _userAccessIn = SecuritySystem.CurrentUserName;
                    UserAccess _locUserAccess = Session.FindObject<UserAccess>(new BinaryOperator("UserName", _userAccessIn));
                    if (_locUserAccess != null)
                    {
                        if (_locUserAccess.Employee != null)
                        {
                            if (_locUserAccess.Employee.Company != null && _locUserAccess.Employee.Workplace != null)
                            {
                                this.Code = _globFunc.GetNumberingUnlockOptimisticRecordBasedOrganization(this.Session.DataLayer, ObjectList.ReceivableOrderCollection, _locUserAccess.Employee.Company, _locUserAccess.Employee.Workplace);
                            }
                            else
                            {
                                this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.ReceivableOrderCollection);
                            }
                        }
                    }
                    #endregion UserAccess
                    this.PickingDate = now;
                    this.IsRoute = true;
                    this.Status = Status.Open;
                    this.StatusDate = now;
                }               
            }
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).
        }

        #region Field

        [Browsable(false)]
        public bool ActivationPosting
        {
            get { return _activationPosting; }
            set { SetPropertyValue("ActivationPosting", ref _activationPosting, value); }
        }

        [RuleRequiredField(DefaultContexts.Save)]
        [Appearance("ReceivableOrderCollectionCodeClose", Enabled = false)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue("Code", ref _code, value); }
        }

        #region Organization

        [Appearance("ReceivableOrderCollectionCompanyClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Company Company
        {
            get { return _company; }
            set { SetPropertyValue("Company", ref _company, value); }
        }

        [Appearance("ReceivableOrderCollectionWorkplaceClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Workplace Workplace
        {
            get { return _workplace; }
            set { SetPropertyValue("Workplace", ref _workplace, value); }
        }

        [Browsable(false)]
        public Division Division
        {
            get { return _division; }
            set { SetPropertyValue("Division", ref _division, value); }
        }

        [Browsable(false)]
        public Department Department
        {
            get { return _department; }
            set { SetPropertyValue("Department", ref _department, value); }
        }

        [Browsable(false)]
        public Section Section
        {
            get { return _section; }
            set { SetPropertyValue("Section", ref _section, value); }
        }

        [Browsable(false)]
        public Employee Employee
        {
            get { return _employee; }
            set { SetPropertyValue("Employee", ref _employee, value); }
        }

        #endregion Organization

        [ImmediatePostData()]
        [DataSourceCriteria("Company = '@This.Company' AND Workplace = '@This.Workplace' AND Active = true AND SalesRole = 2 OR SalesRole = 3")]
        [Appearance("ReceivableOrderCollectionPICClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Employee PIC
        {
            get { return _pic; }
            set { SetPropertyValue("PIC", ref _pic, value); }
        }

        [NonPersistent]
        [ImmediatePostData()]
        [DataSourceCriteria("Company = '@This.Company' AND Workplace = '@This.Workplace' AND Salesman = '@This.PIC' AND Active = true")]
        [Appearance("ReceivableOrderCollectionSelectVehicleClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public SalesmanVehicleSetup SelectVehicle
        {
            get { return _selectVehicle; }
            set
            {
                SetPropertyValue("SelectVehicle", ref _selectVehicle, value);
                if (!IsLoading)
                {
                    if (this._selectVehicle != null)
                    {
                        if (this._selectVehicle.Vehicle != null)
                        {
                            this.Vehicle = this._selectVehicle.Vehicle;
                        }
                    }
                    else
                    {
                        this.Vehicle = null;
                    }
                }
            }
        }

        [Browsable(false)]
        public XPCollection<Vehicle> AvailableVehicle
        {
            get
            {
                if (!IsLoading)
                {
                    if (this.Company != null && this.Workplace != null)
                    {
                        XPQuery<OrderCollectionMonitoring> _orderCollectionMonitoringsQuery = new XPQuery<OrderCollectionMonitoring>(Session);

                        var _orderCollectionMonitorings = from ocm in _orderCollectionMonitoringsQuery
                                                        where ((ocm.Status == Status.Open || ocm.Status == Status.Posted)
                                                        && ocm.Company == this.Company
                                                        && ocm.Workplace == this.Workplace
                                                        && ocm.OrderCollection != null
                                                        && ocm.Vehicle != null)
                                                        group ocm by ocm.Vehicle into g
                                                        select new { Vehicle = g.Key };

                        if (_orderCollectionMonitorings != null && _orderCollectionMonitorings.Count() > 0)
                        {
                            string _beginString = null;
                            string _endString = null;
                            string _fullString = null;

                            List<string> _stringDOM = new List<string>();

                            foreach (var _orderCollectionMonitoring in _orderCollectionMonitorings)
                            {
                                if (_orderCollectionMonitoring != null)
                                {
                                    if (_orderCollectionMonitoring.Vehicle != null)
                                    {
                                        if (_orderCollectionMonitoring.Vehicle.Code != null)
                                        {
                                            _stringDOM.Add(_orderCollectionMonitoring.Vehicle.Code);
                                        }
                                    }
                                }
                            }

                            IEnumerable<string> _stringArrayDOMDistinct = _stringDOM.Distinct();
                            string[] _stringArrayDOMList = _stringArrayDOMDistinct.ToArray();
                            if (_stringArrayDOMList.Length == 1)
                            {
                                for (int i = 0; i < _stringArrayDOMList.Length; i++)
                                {
                                    if (i == 0)
                                    {
                                        _beginString = "[Code]=='" + _stringArrayDOMList[i] + "'";
                                    }
                                }
                            }
                            else if (_stringArrayDOMList.Length > 1)
                            {
                                for (int i = 0; i < _stringArrayDOMList.Length; i++)
                                {
                                    if (i == 0)
                                    {
                                        _beginString = "[Code]=='" + _stringArrayDOMList[i] + "'";
                                    }
                                    else
                                    {
                                        _endString = _endString + " OR [Code]=='" + _stringArrayDOMList[i] + "'";
                                    }
                                }
                            }
                            _fullString = _beginString + _endString;

                            if (_fullString != null && _fullString != "")
                            {
                                _availableVehicle = new XPCollection<Vehicle>(Session, CriteriaOperator.Parse(_fullString));
                            }
                            else
                            {
                                _availableVehicle = new XPCollection<Vehicle>(Session, new GroupOperator(GroupOperatorType.And,
                                                                            new BinaryOperator("Company", this.Company),
                                                                            new BinaryOperator("Workplace", this.Workplace),
                                                                            new BinaryOperator("Active", true)));
                            }
                        }
                        else
                        {
                            _availableVehicle = new XPCollection<Vehicle>(Session, new GroupOperator(GroupOperatorType.And,
                                                                            new BinaryOperator("Company", this.Company),
                                                                            new BinaryOperator("Workplace", this.Workplace),
                                                                            new BinaryOperator("Active", true)));
                        }
                    }
                    else
                    {
                        _availableVehicle = new XPCollection<Vehicle>(Session, new GroupOperator(GroupOperatorType.And,
                                                                            new BinaryOperator("Active", true)));
                    }

                }

                return _availableVehicle;

            }
        }

        [ImmediatePostData()]
        [DataSourceProperty("AvailableVehicle")]
        [Appearance("ReceivableOrderCollectionVehicleClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Vehicle Vehicle
        {
            get { return _vehicle; }
            set { SetPropertyValue("Vehicle", ref _vehicle, value); }
        }

        [ImmediatePostData()]
        public bool IsRoute
        {
            get { return _isRoute; }
            set { SetPropertyValue("IsRoute", ref _isRoute, value); }
        }

        [ImmediatePostData()]
        [ModelDefault("DisplayFormat", "{0:dd/MM/yyyy}")]
        [ModelDefault("EditMask", "dd/MM/yyyy")]
        [Appearance("ReceivableOrderCollectionPickingDateClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [Appearance("ReceivableOrderCollectionPickingDateClose2", Criteria = "IsRoute = true", Enabled = true)]
        [Appearance("ReceivableOrderCollectionPickingDateClose3", Criteria = "IsRoute = false", Enabled = false)]
        public DateTime PickingDate
        {
            get { return _pickingDate; }
            set { SetPropertyValue("PickingDate", ref _pickingDate, value); }

        }

        [Browsable(false)]
        public XPCollection<Picking> AvailablePicking
        {
            get
            {
                if (!IsLoading)
                {
                    if (this.Company != null && this.Workplace != null)
                    {
                        XPQuery<OrderCollectionMonitoring> _orderCollectionMonitoringsQuery = new XPQuery<OrderCollectionMonitoring>(Session);

                        var _orderCollectionMonitorings = from ocm in _orderCollectionMonitoringsQuery
                                                  where (ocm.Status != Status.Close
                                                  && ocm.CollectStatus == CollectStatus.Collect 
                                                  && ocm.Company == this.Company
                                                  && ocm.Workplace == this.Workplace
                                                  && ocm.Customer == this.ReceivableTransaction.SalesToCustomer
                                                  && ocm.Picking != null
                                                  && ocm.OrderCollection != null
                                                  && ocm.ReceivableTransaction == null
                                                  && ocm.ReceivableTransaction2 == null)
                                                  group ocm by ocm.Picking into g
                                                  select new { Picking = g.Key };

                        if (_orderCollectionMonitorings != null && _orderCollectionMonitorings.Count() > 0)
                        {
                            string _beginString = null;
                            string _endString = null;
                            string _fullString = null;

                            List<string> _stringPM = new List<string>();

                            foreach (var _orderCollectionMonitoring in _orderCollectionMonitorings)
                            {
                                if (_orderCollectionMonitoring != null)
                                {
                                    if (_orderCollectionMonitoring.Picking != null)
                                    {
                                        if (_orderCollectionMonitoring.Picking.Code != null && _orderCollectionMonitoring.Picking.PickingDate.ToShortDateString() == this.PickingDate.ToShortDateString())
                                        {
                                            _stringPM.Add(_orderCollectionMonitoring.Picking.Code);
                                        }
                                    }
                                }
                            }

                            IEnumerable<string> _stringArrayPMDistinct = _stringPM.Distinct();
                            string[] _stringArrayPMList = _stringArrayPMDistinct.ToArray();
                            if (_stringArrayPMList.Length == 1)
                            {
                                for (int i = 0; i < _stringArrayPMList.Length; i++)
                                {
                                    if (i == 0)
                                    {
                                        _beginString = "[Code]=='" + _stringArrayPMList[i] + "'";
                                    }
                                }
                            }
                            else if (_stringArrayPMList.Length > 1)
                            {
                                for (int i = 0; i < _stringArrayPMList.Length; i++)
                                {
                                    if (i == 0)
                                    {
                                        _beginString = "[Code]=='" + _stringArrayPMList[i] + "'";
                                    }
                                    else
                                    {
                                        _endString = _endString + " OR [Code]=='" + _stringArrayPMList[i] + "'";
                                    }
                                }
                            }
                            _fullString = _beginString + _endString;

                            if (_fullString != null && _fullString != "")
                            {
                                _availablePicking = new XPCollection<Picking>(Session, CriteriaOperator.Parse(_fullString));
                            }
                        }
                    }
                }

                return _availablePicking;
            }
        }

        [ImmediatePostData()]
        [DataSourceProperty("AvailablePicking")]
        [Appearance("ReceivableOrderCollectionPickingClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [Appearance("ReceivableOrderCollectionPickingClose2", Criteria = "IsRoute = true", Enabled = true)]
        [Appearance("ReceivableOrderCollectionPickingClose3", Criteria = "IsRoute = false", Enabled = false)]
        public Picking Picking
        {
            get { return _picking; }
            set { SetPropertyValue("Picking", ref _picking, value); }
        }

        [Browsable(false)]
        public XPCollection<OrderCollection> AvailableOrderCollection
        {
            get
            {
                string _beginString = null;
                string _endString = null;
                string _fullString = null;

                if (!IsLoading)
                {
                    if (this.Company != null && this.Workplace != null && this.Picking != null && this.ReceivableTransaction != null && this.ReceivableTransaction.SalesToCustomer != null)
                    {
                        if(this.Picking != null)
                        {
                            XPQuery<OrderCollectionMonitoring> _orderCollectionMonitoringsQuery = new XPQuery<OrderCollectionMonitoring>(Session);

                            var _orderCollectionMonitorings = from ocm in _orderCollectionMonitoringsQuery
                                                              where (ocm.Status != Status.Close
                                                              && ocm.Company == this.Company
                                                              && ocm.Workplace == this.Workplace
                                                              && ocm.Customer == this.ReceivableTransaction.SalesToCustomer
                                                              && ocm.Picking == this.Picking
                                                              && ocm.CollectStatus == CollectStatus.Collect
                                                              && ocm.ReceivableTransaction == null
                                                              )
                                                              group ocm by ocm.OrderCollection into g
                                                              select new { OrderCollection = g.Key };

                            if (_orderCollectionMonitorings != null && _orderCollectionMonitorings.Count() > 0)
                            {
                                List<string> _stringOCM = new List<string>();

                                foreach (var _orderCollectionMonitoring in _orderCollectionMonitorings)
                                {
                                    if (_orderCollectionMonitoring != null)
                                    {
                                        _stringOCM.Add(_orderCollectionMonitoring.OrderCollection.Code);
                                    }
                                }

                                IEnumerable<string> _stringArrayOCMDistinct = _stringOCM.Distinct();
                                string[] _stringArrayOCMList = _stringArrayOCMDistinct.ToArray();
                                if (_stringArrayOCMList.Length == 1)
                                {
                                    for (int i = 0; i < _stringArrayOCMList.Length; i++)
                                    {
                                        if (i == 0)
                                        {
                                            _beginString = "[Code]=='" + _stringArrayOCMList[i] + "'";
                                        }
                                    }
                                }
                                else if (_stringArrayOCMList.Length > 1)
                                {
                                    for (int i = 0; i < _stringArrayOCMList.Length; i++)
                                    {
                                        if (i == 0)
                                        {
                                            _beginString = "[Code]=='" + _stringArrayOCMList[i] + "'";
                                        }
                                        else
                                        {
                                            _endString = _endString + " OR [Code]=='" + _stringArrayOCMList[i] + "'";
                                        }
                                    }
                                }
                                _fullString = _beginString + _endString;

                                if (_fullString != null)
                                {
                                    _availableOrderCollection = new XPCollection<OrderCollection>(Session, CriteriaOperator.Parse(_fullString));
                                }
                            }
                        }
                        else
                        {
                            XPQuery<OrderCollectionMonitoring> _orderCollectionMonitoringsQuery = new XPQuery<OrderCollectionMonitoring>(Session);

                            var _orderCollectionMonitorings = from ocm in _orderCollectionMonitoringsQuery
                                                              where (ocm.Status != Status.Close
                                                              && ocm.Company == this.Company
                                                              && ocm.Workplace == this.Workplace
                                                              && ocm.Customer == this.ReceivableTransaction.SalesToCustomer
                                                              && ocm.CollectStatus == CollectStatus.Collect
                                                              && ocm.ReceivableTransaction == null
                                                              )
                                                              group ocm by ocm.OrderCollection into g
                                                              select new { OrderCollection = g.Key };

                            if (_orderCollectionMonitorings != null && _orderCollectionMonitorings.Count() > 0)
                            {
                                List<string> _stringOCM = new List<string>();

                                foreach (var _orderCollectionMonitoring in _orderCollectionMonitorings)
                                {
                                    if (_orderCollectionMonitoring != null)
                                    {
                                        _stringOCM.Add(_orderCollectionMonitoring.OrderCollection.Code);
                                    }
                                }

                                IEnumerable<string> _stringArrayOCMDistinct = _stringOCM.Distinct();
                                string[] _stringArrayOCMList = _stringArrayOCMDistinct.ToArray();
                                if (_stringArrayOCMList.Length == 1)
                                {
                                    for (int i = 0; i < _stringArrayOCMList.Length; i++)
                                    {
                                        if (i == 0)
                                        {
                                            _beginString = "[Code]=='" + _stringArrayOCMList[i] + "'";
                                        }
                                    }
                                }
                                else if (_stringArrayOCMList.Length > 1)
                                {
                                    for (int i = 0; i < _stringArrayOCMList.Length; i++)
                                    {
                                        if (i == 0)
                                        {
                                            _beginString = "[Code]=='" + _stringArrayOCMList[i] + "'";
                                        }
                                        else
                                        {
                                            _endString = _endString + " OR [Code]=='" + _stringArrayOCMList[i] + "'";
                                        }
                                    }
                                }
                                _fullString = _beginString + _endString;

                                if (_fullString != null)
                                {
                                    _availableOrderCollection = new XPCollection<OrderCollection>(Session, CriteriaOperator.Parse(_fullString));
                                }
                            }
                        }
                        //Codingan lama
                        
                    }
                }
                return _availableOrderCollection;
            }
        }

        [DataSourceProperty("AvailableOrderCollection", DataSourcePropertyIsNullMode.SelectNothing)]
        public OrderCollection OrderCollection
        {
            get { return _orderCollection; }
            set { SetPropertyValue("OrderCollection", ref _orderCollection, value); }
        }

        [ImmediatePostData()]
        [Appearance("ReceivableOrderCollectionReceivableTransactionClose", Enabled = false)]
        [Association("ReceivableTransaction-ReceivableOrderCollections")]
        public ReceivableTransaction ReceivableTransaction
        {
            get { return _receivableTransaction; }
            set {
                SetPropertyValue("ReceivableTransaction", ref _receivableTransaction, value);
                if (!IsLoading)
                {
                    if (this._receivableTransaction != null)
                    {
                        if (this._receivableTransaction.Company != null)
                        {
                            this.Company = this._receivableTransaction.Company;
                        }
                        if (this._receivableTransaction.Workplace != null)
                        {
                            this.Workplace = this._receivableTransaction.Workplace;
                        }
                        if (this._receivableTransaction.Division != null)
                        {
                            this.Division = this._receivableTransaction.Division;
                        }
                        if (this._receivableTransaction.Department != null)
                        {
                            this.Department = this._receivableTransaction.Department;
                        }
                        if (this._receivableTransaction.Section != null)
                        {
                            this.Section = this._receivableTransaction.Section;
                        }
                        if (this._receivableTransaction.Employee != null)
                        {
                            this.Employee = this._receivableTransaction.Employee;
                        }
                    }
                }
            }
        }

        [Appearance("ReceivableOrderCollectionStatusClose", Enabled = false)]
        public Status Status
        {
            get { return _status; }
            set { SetPropertyValue("Status", ref _status, value); }
        }

        [ModelDefault("DisplayFormat", "{0:dd/MM/yyyy}")]
        [ModelDefault("EditMask", "dd/MM/yyyy")]
        [Appearance("ReceivableOrderCollectionStatusDateClose", Enabled = false)]
        public DateTime StatusDate
        {
            get { return _statusDate; }
            set { SetPropertyValue("StatusDate", ref _statusDate, value); }
        }

        [Appearance("ReceivableSalesCollectionPostedCountEnabled", Enabled = false)]
        public int PostedCount
        {
            get { return _postedCount; }
            set { SetPropertyValue("PostedCount", ref _postedCount, value); }
        }

        #endregion Field

        
    }
}