﻿using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;

using DevExpress.ExpressApp.Editors;
using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.ConditionalAppearance;

namespace FullDrive.Module.BusinessObjects
{
    [DefaultClassOptions]
    [DefaultProperty("Code")]
    [NavigationItem("Inventory")]
    [RuleCombinationOfPropertiesIsUnique("AmendDeliveryMonitoringRuleUnique", DefaultContexts.Save, "Code")]
    //[ImageName("BO_Contact")]
    //[DefaultProperty("DisplayMemberNameForLookupEditorsOfThisType")]
    //[DefaultListViewOptions(MasterDetailMode.ListViewOnly, false, NewItemRowPosition.None)]
    //[Persistent("DatabaseTableName")]
    // Specify more UI options using a declarative approach (https://documentation.devexpress.com/#eXpressAppFramework/CustomDocument112701).
    public class AmendDeliveryMonitoring : BaseObject
    { // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).
        private bool _activationPosting;
        private string _code;
        private bool _select;
        private AmendDelivery _amendDelivery;
        private AmendDeliveryLine _amendDeliveryLine;
        #region InitialOrganizatioin
        private Company _company;
        private Workplace _workplace;
        private Division _division;
        private Department _department;
        private Section _section;
        private Employee _employee;
        #endregion InitialOrganizatioin
        private Employee _salesman;
        private Vehicle _vehicle;
        private BusinessPartner _customer;
        private SalesInvoice _salesInvoice;
        private Currency _currency;
        private PriceGroup _priceGroup;
        private double _totAmount;
        private double _totTaxAmount;
        private double _totDiscAmount;
        private double _amount;
        private double _totAmountCmp;
        private double _totTaxAmountCmp;
        private double _totDiscAmountCmp;
        private double _amountCmp;
        private double _amountDN;
        private double _amountCN;
        private double _amountColl;
        private PaymentType _paymentType;
        private TermOfPayment _top;
        private DateTime _dueDate;
        private CollectStatus _collectStatus;
        private DateTime _collectStatusDate;
        private Status _status;
        private DateTime _statusDate;
        private int _postedCount;
        private DateTime _pickingDate;
        private Picking _picking;
        private PickingMonitoring _pickingMonitoring;
        private string _userAccessIn;
        private GlobalFunction _globFunc;
        //AuditTrail
        private XPCollection<AuditDataItemPersistent> changeHistory;

        public AmendDeliveryMonitoring(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if (!IsLoading)
            {
                if (Session.IsNewObject(this))
                {
                    _globFunc = new GlobalFunction();
                    DateTime now = DateTime.Now;
                    #region UserAccess
                    _userAccessIn = SecuritySystem.CurrentUserName;
                    UserAccess _locUserAccess = Session.FindObject<UserAccess>(new BinaryOperator("UserName", _userAccessIn));
                    if (_locUserAccess != null)
                    {
                        if (_locUserAccess.Employee != null)
                        {
                            if (_locUserAccess.Employee.Company != null && _locUserAccess.Employee.Workplace != null)
                            {
                                this.Code = _globFunc.GetNumberingUnlockOptimisticRecordBasedOrganization(this.Session.DataLayer, ObjectList.AmendDeliveryMonitoring, _locUserAccess.Employee.Company, _locUserAccess.Employee.Workplace);
                            }
                            else
                            {
                                this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.AmendDeliveryMonitoring);
                            }
                        }
                    }
                    #endregion UserAccess
                    this.Select = true;
                    this.Status = CustomProcess.Status.Open;
                    this.StatusDate = now;
                }
            }
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).
        }

        #region Field

        [Browsable(false)]
        public bool ActivationPosting
        {
            get { return _activationPosting; }
            set { SetPropertyValue("ActivationPosting", ref _activationPosting, value); }
        }

        [RuleRequiredField(DefaultContexts.Save)]
        [Appearance("AmendDeliveryMonitoringCodeClose", Enabled = false)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue("Code", ref _code, value); }
        }

        public bool Select
        {
            get { return _select; }
            set { SetPropertyValue("Select", ref _select, value); }
        }

        [Appearance("AmendDeliveryMonitoringAmendDeliveryClose", Enabled = false)]
        public AmendDelivery AmendDelivery
        {
            get { return _amendDelivery; }
            set { SetPropertyValue("AmendDelivery", ref _amendDelivery, value); }
        }

        [Appearance("AmendDeliveryMonitoringAmendDeliveryLineClose", Enabled = false)]
        public AmendDeliveryLine AmendDeliveryLine
        {
            get { return _amendDeliveryLine; }
            set { SetPropertyValue("AmendDeliveryLine", ref _amendDeliveryLine, value); }
        }

        #region Organization

        [Appearance("AmendDeliveryMonitoringCompanyClose", Enabled = false)]
        public Company Company
        {
            get { return _company; }
            set { SetPropertyValue("Company", ref _company, value); }
        }

        [Appearance("AmendDeliveryMonitoringWorkplaceClose", Enabled = false)]
        public Workplace Workplace
        {
            get { return _workplace; }
            set { SetPropertyValue("Workplace", ref _workplace, value); }
        }

        [Browsable(false)]
        public Division Division
        {
            get { return _division; }
            set { SetPropertyValue("Division", ref _division, value); }
        }

        [Browsable(false)]
        public Department Department
        {
            get { return _department; }
            set { SetPropertyValue("Department", ref _department, value); }
        }

        [Browsable(false)]
        public Section Section
        {
            get { return _section; }
            set { SetPropertyValue("Section", ref _section, value); }
        }

        [Browsable(false)]
        public Employee Employee
        {
            get { return _employee; }
            set { SetPropertyValue("Employee", ref _employee, value); }
        }

        #endregion Organization

        [Appearance("AmendDeliveryMonitoringSalesmanClose", Enabled = false)]
        public Employee Salesman
        {
            get { return _salesman; }
            set { SetPropertyValue("Salesman", ref _salesman, value); }
        }

        [Appearance("AmendDeliveryMonitoringVehicleClose", Enabled = false)]
        public Vehicle Vehicle
        {
            get { return _vehicle; }
            set { SetPropertyValue("Vehicle", ref _vehicle, value); }
        }

        [Appearance("AmendDeliveryMonitoringCustomerClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public BusinessPartner Customer
        {
            get { return _customer; }
            set { SetPropertyValue("Customer", ref _customer, value); }
        }

        [Appearance("AmendDeliveryMonitoringSalesInvoiceClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public SalesInvoice SalesInvoice
        {
            get { return _salesInvoice; }
            set { SetPropertyValue("SalesInvoice", ref _salesInvoice, value); }
        }

        [Appearance("AmendDeliveryMonitoringCurrencyClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Currency Currency
        {
            get { return _currency; }
            set { SetPropertyValue("Currency", ref _currency, value); }
        }

        [Appearance("AmendDeliveryMonitoringPriceGroupClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public PriceGroup PriceGroup
        {
            get { return _priceGroup; }
            set { SetPropertyValue("PriceGroup", ref _priceGroup, value); }
        }

        #region OriTotAmount

        [Appearance("AmendDeliveryMonitoringTotAmountClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public double TotAmount
        {
            get { return _totAmount; }
            set { SetPropertyValue("TotAmount", ref _totAmount, value); }
        }

        [Appearance("AmendDeliveryMonitoringTotTaxAmountClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public double TotTaxAmount
        {
            get { return _totTaxAmount; }
            set { SetPropertyValue("TotTaxAmount", ref _totTaxAmount, value); }
        }

        [Appearance("AmendDeliveryMonitoringTotDiscAmountClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public double TotDiscAmount
        {
            get { return _totDiscAmount; }
            set { SetPropertyValue("TotDiscAmount", ref _totDiscAmount, value); }
        }

        [ImmediatePostData()]
        [Appearance("AmendDeliveryMonitoringAmountClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public double Amount
        {
            get { return _amount; }
            set
            {
                SetPropertyValue("Amount", ref _amount, value);
                if (!IsLoading)
                {
                    SetTotalAmountCollection();
                }
            }
        }

        #endregion OriTotAmount


        #region CmpTotAmount

        [Appearance("AmendDeliveryMonitoringTotAmountCmpClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public double TotAmountCmp
        {
            get { return _totAmountCmp; }
            set { SetPropertyValue("TotAmountCmp", ref _totAmountCmp, value); }
        }

        [Appearance("AmendDeliveryMonitoringTotTaxAmountCmpClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public double TotTaxAmountCmp
        {
            get { return _totTaxAmountCmp; }
            set { SetPropertyValue("TotTaxAmountCmp", ref _totTaxAmountCmp, value); }
        }

        [Appearance("AmendDeliveryMonitoringTotDiscAmountCmpClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public double TotDiscAmountCmp
        {
            get { return _totDiscAmountCmp; }
            set { SetPropertyValue("TotDiscAmountCmp", ref _totDiscAmountCmp, value); }
        }

        [ImmediatePostData()]
        [Appearance("AmendDeliveryMonitoringAmountCmpClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public double AmountCmp
        {
            get { return _amountCmp; }
            set
            {
                SetPropertyValue("AmountCmp", ref _amountCmp, value);
                if (!IsLoading)
                {
                    SetTotalAmountCollection();
                }
            }
        }

        #endregion CmpTotAmount

        [ImmediatePostData()]
        [Appearance("AmendDeliveryMonitoringAmountDNClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public double AmountDN
        {
            get { return _amountDN; }
            set
            {
                SetPropertyValue("AmountDN", ref _amountDN, value);
                if (!IsLoading)
                {
                    SetTotalAmountCollection();
                }
            }
        }

        [ImmediatePostData()]
        [Appearance("AmendDeliveryMonitoringAmountCNClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public double AmountCN
        {
            get { return _amountCN; }
            set
            {
                SetPropertyValue("AmountCN", ref _amountCN, value);
                if (!IsLoading)
                {
                    SetTotalAmountCollection();
                }
            }
        }

        [Appearance("AmendDeliveryMonitoringAmountCollClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public double AmountColl
        {
            get { return _amountColl; }
            set { SetPropertyValue("AmountColl", ref _amountColl, value); }
        }

        [Appearance("AmendDeliveryMonitoringPaymentTypeClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public PaymentType PaymentType
        {
            get { return _paymentType; }
            set { SetPropertyValue("PaymentType", ref _paymentType, value); }
        }

        [Appearance("AmendDeliveryMonitoringTOPClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public TermOfPayment TOP
        {
            get { return _top; }
            set { SetPropertyValue("TOP", ref _top, value); }
        }

        [ModelDefault("DisplayFormat", "{0:dd/MM/yyyy}")]
        [ModelDefault("EditMask", "dd/MM/yyyy")]
        [Appearance("AmendDeliveryMonitoringDueDateClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public DateTime DueDate
        {
            get { return _dueDate; }
            set { SetPropertyValue("DueDate", ref _dueDate, value); }
        }

        [Appearance("AmendDeliveryMonitoringCollectStatusClose", Enabled = false)]
        public CollectStatus CollectStatus
        {
            get { return _collectStatus; }
            set { SetPropertyValue("CollectStatus", ref _collectStatus, value); }
        }

        [ModelDefault("DisplayFormat", "{0:dd/MM/yyyy}")]
        [ModelDefault("EditMask", "dd/MM/yyyy")]
        [Appearance("AmendDeliveryMonitoringCollectStatusDateClose", Enabled = false)]
        public DateTime CollectStatusDate
        {
            get { return _collectStatusDate; }
            set { SetPropertyValue("CollectStatusDate", ref _collectStatusDate, value); }
        }

        [Appearance("AmendDeliveryMonitoringStatusEnabled", Enabled = false)]
        public Status Status
        {
            get { return _status; }
            set { SetPropertyValue("Status", ref _status, value); }
        }

        [ModelDefault("DisplayFormat", "{0:dd/MM/yyyy}")]
        [ModelDefault("EditMask", "dd/MM/yyyy")]
        [Appearance("AmendDeliveryMonitoringStatusDateEnabled", Enabled = false)]
        public DateTime StatusDate
        {
            get { return _statusDate; }
            set { SetPropertyValue("StatusDate", ref _statusDate, value); }
        }

        [Appearance("AmendDeliveryMonitoringPostedCountEnabled", Enabled = false)]
        public int PostedCount
        {
            get { return _postedCount; }
            set { SetPropertyValue("PostedCount", ref _postedCount, value); }
        }

        [Browsable(false)]
        [Appearance("AmendDeliveryMonitoringPickingDateEnabled", Enabled = false)]
        public DateTime PickingDate
        {
            get { return _pickingDate; }
            set { SetPropertyValue("PickingDate", ref _pickingDate, value); }

        }

        [Browsable(false)]
        [Appearance("AmendDeliveryMonitoringPickingEnabled", Enabled = false)]
        public Picking Picking
        {
            get { return _picking; }
            set { SetPropertyValue("Picking", ref _picking, value); }
        }

        [Browsable(false)]
        [Appearance("AmendDeliveryMonitoringPickingMonitoringEnabled", Enabled = false)]
        public PickingMonitoring PickingMonitoring
        {
            get { return _pickingMonitoring; }
            set { SetPropertyValue("PickingMonitoring", ref _pickingMonitoring, value); }
        }

        //AuditTrail
        [CollectionOperationSet(AllowAdd = false, AllowRemove = true)]
        public XPCollection<AuditDataItemPersistent> ChangeHistory
        {
            get
            {
                if (changeHistory == null)
                {
                    changeHistory = AuditedObjectWeakReference.GetAuditTrail(Session, this);
                }
                return changeHistory;
            }
        }
        #endregion Field

        //===================================================== Code Only =================================================

        #region CodeOnly

        #region Set

        private void SetTotalAmountCollection()
        {
            try
            {
                _globFunc = new GlobalFunction();

                if (_amount >= 0 || _amountCN >= 0 || _amountDN >= 0)
                {
                    if (_globFunc.GetRoundingList(Session, ObjectList.AmendDeliveryMonitoring, FieldName.TAmount) == true)
                    {
                        this.AmountColl = _globFunc.GetRoundUp(Session, ((this.Amount + _amountDN) - this.AmountCN), ObjectList.AmendDeliveryMonitoring, FieldName.TAmount);
                    }
                    else
                    {
                        this.AmountColl = (this.Amount + _amountDN) - this.AmountCN;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = AmendDeliveryMonitoring " + ex.ToString());
            }
        }

        #endregion Set

        #endregion CodeOnly

    }
}