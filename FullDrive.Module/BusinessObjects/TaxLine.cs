﻿using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;

using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.ConditionalAppearance;

namespace FullDrive.Module.BusinessObjects
{
    [DefaultClassOptions]
    [DefaultProperty("Code")]
    [NavigationItem("Finance & Accounting")]
    [RuleCombinationOfPropertiesIsUnique("TaxLineRuleUnique", DefaultContexts.Save, "Code")]
    //[ImageName("BO_Contact")]
    //[DefaultProperty("DisplayMemberNameForLookupEditorsOfThisType")]
    //[DefaultListViewOptions(MasterDetailMode.ListViewOnly, false, NewItemRowPosition.None)]
    //[Persistent("DatabaseTableName")]
    // Specify more UI options using a declarative approach (https://documentation.devexpress.com/#eXpressAppFramework/CustomDocument112701).
    public class TaxLine : BaseObject
    { // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).
        private int _no;
        private string _code;
        private string _name;
        private TaxType _taxType;
        private TaxGroup _taxGroup;
        private TaxGroupType _taxGroupType;
        private TaxAccountGroup _taxAccountGroup;
        private TaxRule _taxRule;
        private double _txValue;
        private Tax _tax;
        private double _uAmount;
        private double _tUAmount;
        private double _discAmount;
        private double _txAmount;
        private TaxNature _taxNature;
        private TaxMode _taxMode;
        private string _description;
        private PurchaseOrderLine _purchaseOrderLine;
        private PurchaseInvoiceLine _purchaseInvoiceLine;
        private bool _active;
        private bool _default;
        private GlobalFunction _globFunc;
        //AuditTrail
        private XPCollection<AuditDataItemPersistent> changeHistory;

        public TaxLine(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if (!IsLoading)
            {
                if(Session.IsNewObject(this))
                {
                    _globFunc = new GlobalFunction();
                    this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.TaxLine);
                }               
            }
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).
        }

        protected override void OnSaving()
        {
            base.OnSaving();
            if (!IsLoading && IsSaving)
            {
                UpdateNo();
            }
        }

        protected override void OnDeleting()
        {
            base.OnDeleting();
            if (!IsLoading)
            {
                RecoveryDeleteNo();
            }
        }

        #region Field

        [Browsable(false)]
        [Appearance("TaxLineNoEnabled", Enabled = false)]
        public int No
        {
            get { return _no; }
            set { SetPropertyValue("No", ref _no, value); }
        }

        [RuleRequiredField(DefaultContexts.Save)]
        [Appearance("TaxLineCodeEnabled", Enabled = false)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue("Code", ref _code, value); }
        }

        [ImmediatePostData()]
        [DataSourceCriteria("Active = true")]
        [Association("Tax-TaxLines")]
        public Tax Tax
        {
            get { return _tax; }
            set
            {
                SetPropertyValue("Tax", ref _tax, value);
                if (!IsLoading)
                {
                    if (this._tax != null)
                    {
                        this.Name = GetName();
                        this.TaxType = this._tax.TaxType;
                        this.TaxGroup = this._tax.TaxGroup;
                        this.TaxGroupType = this._tax.TaxGroupType;
                        this.TaxNature = this._tax.TaxNature;
                        this.TaxMode = this._tax.TaxMode;
                        this.TxValue = this._tax.Value;
                        this.Active = this._tax.Active;
                        this.Default = this._tax.Default;
                    }
                    SetTaxAmount();
                }
            }
        }

        public string Name
        {
            get { return _name; }
            set { SetPropertyValue("Name", ref _name, value); }
        }

        [Appearance("TaxLineTaxTypeEnabled", Enabled = false)]
        [DataSourceCriteria("Active = true")]
        public TaxType TaxType
        {
            get { return _taxType; }
            set { SetPropertyValue("TaxType", ref _taxType, value); }
        }

        [Appearance("TaxLineTaxGroupEnabled", Enabled = false)]
        [DataSourceCriteria("Active = true")]
        public TaxGroup TaxGroup
        {
            get { return _taxGroup; }
            set { SetPropertyValue("TaxGroup", ref _taxGroup, value); }
        }

        public TaxGroupType TaxGroupType
        {
            get { return _taxGroupType; }
            set { SetPropertyValue("TaxGroupType", ref _taxGroupType, value); }
        }

        [Appearance("TaxLineTaxAccountGroupEnabled", Enabled = false)]
        [Association("TaxAccountGroup-TaxLines")]
        [DataSourceCriteria("Active = true")]
        public TaxAccountGroup TaxAccountGroup
        {
            get { return _taxAccountGroup; }
            set { SetPropertyValue("TaxAccountGroup", ref _taxAccountGroup, value); }
        }

        [Appearance("TaxLineTaxNatureEnabled", Enabled = false)]
        public TaxNature TaxNature
        {
            get { return _taxNature; }
            set { SetPropertyValue("TaxNature", ref _taxNature, value); }
        }

        [Appearance("TaxLineTaxModeEnabled", Enabled = false)]
        public TaxMode TaxMode
        {
            get { return _taxMode; }
            set { SetPropertyValue("TaxMode", ref _taxMode, value); }
        }

        [Browsable(false)]
        [Appearance("TaxLineActiveEnabled", Enabled = false)]
        public bool Active
        {
            get { return _active; }
            set { SetPropertyValue("Active", ref _active, value); }
        }

        [Browsable(false)]
        [Appearance("TaxLineDefaultEnabled", Enabled = false)]
        public bool Default
        {
            get { return _default; }
            set { SetPropertyValue("Default", ref _default, value); }
        }

        [ImmediatePostData()]
        public TaxRule TaxRule
        {
            get { return _taxRule; }
            set { SetPropertyValue("TaxRule", ref _taxRule, value); }
        }

        [Appearance("TaxLineValueEnabled", Enabled = false)]
        public double TxValue
        {
            get { return _txValue; }
            set { SetPropertyValue("TxValue", ref _txValue, value); }
        }

        [ImmediatePostData()]
        public double TTxValue
        {
            get
            {
                if (this._txValue > 0)
                {
                    return GetSumTotalTxValue();
                }
                else
                {
                    return 0;
                }
            }
        }

        [Size(512)]
        public string Description
        {
            get { return _description; }
            set { SetPropertyValue("Description", ref _description, value); }
        }

        [Appearance("TaxLineUPriceEnabled", Enabled = false)]
        [ImmediatePostData()]
        public double UAmount
        {
            get { return _uAmount; }
            set
            {
                SetPropertyValue("UAmount", ref _uAmount, value);
                if (!IsLoading)
                {
                    _globFunc = new GlobalFunction();
                    if (this._uAmount > 0)
                    {
                        if (_globFunc.GetRoundingList(Session, ObjectList.TaxLine, FieldName.UAmount) == true)
                        {
                            this._uAmount = _globFunc.GetRoundUp(Session, this._uAmount, ObjectList.TaxLine, FieldName.UAmount);
                        }
                    }
                }
            }
        }

        [Appearance("TaxLineLineTUAmountEnabled", Enabled = false)]
        [ImmediatePostData()]
        public double TUAmount
        {
            get { return _tUAmount; }
            set
            {
                SetPropertyValue("TUAmount", ref _tUAmount, value);
                if (!IsLoading)
                {
                    _globFunc = new GlobalFunction();
                    if (this._tUAmount > 0)
                    {
                        if (_globFunc.GetRoundingList(Session, ObjectList.TaxLine, FieldName.TUAmount) == true)
                        {
                            this._tUAmount = _globFunc.GetRoundUp(Session, this._tUAmount, ObjectList.TaxLine, FieldName.TUAmount);
                        }
                    }

                    SetTaxAmount();
                }
            }
        }

        [Browsable(false)]
        [ImmediatePostData()]
        public double DiscAmount
        {
            get { return _discAmount; }
            set
            {
                SetPropertyValue("DiscAmount", ref _discAmount, value);
                if(!IsLoading)
                {
                    SetTaxAmount();
                }
            }
        }
        
        [Appearance("TaxLineTxAmountEnabled", Enabled = false)]
        [ImmediatePostData()]
        public double TxAmount
        {
            get { return _txAmount; }
            set
            {
                SetPropertyValue("TxAmount", ref _txAmount, value);
                _globFunc = new GlobalFunction();
                if (this._txAmount > 0)
                {
                    if (_globFunc.GetRoundingList(Session, ObjectList.TaxLine, FieldName.TxAmount) == true)
                    {
                        this._txAmount = _globFunc.GetRoundUp(Session, this._txAmount, ObjectList.TaxLine, FieldName.TxAmount);
                    }
                }
            }
        }

        //[Browsable(false)]
        [Appearance("TaxLineTTxAmountEnabled", Enabled = false)]
        [ImmediatePostData()]
        public double TTxAmount
        {
            get
            {
                if (this._txValue > 0 && this._tUAmount > 0)
                {
                    return GetSumTotalTaxAmount();
                }
                else
                {
                    return 0;
                }
            }
        }

        [ImmediatePostData()]
        [Appearance("TaxLinePurchaseOrderLineEnabled", Enabled = false)]
        [Association("PurchaseOrderLine-TaxLines")]
        public PurchaseOrderLine PurchaseOrderLine
        {
            get { return _purchaseOrderLine; }
            set
            {
                SetPropertyValue("PurchaseOrderLine", ref _purchaseOrderLine, value);
                if (!IsLoading)
                {
                    if (this._purchaseOrderLine != null)
                    {
                        this.UAmount = this._purchaseOrderLine.UAmount;
                        this.TUAmount = this._purchaseOrderLine.TUAmount;
                        this.TaxRule = this._purchaseOrderLine.TaxRule;
                        if (this._purchaseOrderLine.TaxRule == TaxRule.AfterDiscount)
                        {
                            this.DiscAmount = this._purchaseOrderLine.DiscAmount;
                        }
                        if (this._purchaseOrderLine.TaxRule == TaxRule.BeforeDiscount || this._purchaseOrderLine.TaxRule == TaxRule.None)
                        {
                            this.DiscAmount = 0;
                        }
                    }
                }
            }
        }

        [ImmediatePostData()]
        [Appearance("TaxLinePurchaseInvoiceLineEnabled", Enabled = false)]
        [Association("PurchaseInvoiceLine-TaxLines")]
        public PurchaseInvoiceLine PurchaseInvoiceLine
        {
            get { return _purchaseInvoiceLine; }
            set
            {
                SetPropertyValue("PurchaseInvoiceLine", ref _purchaseInvoiceLine, value);
                if (!IsLoading)
                {
                    if (this._purchaseInvoiceLine != null)
                    {
                        this.UAmount = this._purchaseInvoiceLine.UAmount;
                        this.TUAmount = this._purchaseInvoiceLine.TUAmount;
                        this.TaxRule = this._purchaseInvoiceLine.TaxRule;
                        if (this._purchaseInvoiceLine.TaxRule == TaxRule.AfterDiscount)
                        {
                            this.DiscAmount = this._purchaseInvoiceLine.DiscAmount;
                        }
                        if (this._purchaseInvoiceLine.TaxRule == TaxRule.BeforeDiscount || this._purchaseInvoiceLine.TaxRule == TaxRule.None)
                        {
                            this.DiscAmount = 0;
                        }
                    }
                }
            }
        }

        //AuditTrail
        [CollectionOperationSet(AllowAdd = false, AllowRemove = true)]
        public XPCollection<AuditDataItemPersistent> ChangeHistory
        {
            get
            {
                if (changeHistory == null)
                {
                    changeHistory = AuditedObjectWeakReference.GetAuditTrail(Session, this);
                }
                return changeHistory;
            }
        }

        #endregion Field

        //============================================= Code In Here =============================================

        #region No

        public void UpdateNo()
        {
            try
            {
                if (!IsLoading && Session.IsNewObject(this))
                {
                    if (this.Tax != null)
                    {
                        object _makRecord = Session.Evaluate<TaxLine>(CriteriaOperator.Parse("Max(No)"), CriteriaOperator.Parse("Tax=?", this.Tax));
                        this.No = Convert.ToInt32(_makRecord) + 1;
                        this.Save();
                        RecoveryUpdateNo();
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = TaxLine " + ex.ToString());
            }
        }

        public void RecoveryUpdateNo()
        {
            try
            {
                if (this.Tax != null)
                {
                    Tax _numHeader = Session.FindObject<Tax>
                                                (new BinaryOperator("Code", this.Tax.Code));

                    XPCollection<TaxLine> _numLines = new XPCollection<TaxLine>
                                                (Session, new BinaryOperator("Tax", _numHeader),
                                                new SortProperty("No", DevExpress.Xpo.DB.SortingDirection.Ascending));
                    if (_numLines != null)
                    {
                        int i1 = 0;
                        foreach (TaxLine _numLine in _numLines)
                        {
                            i1 += 1;
                            _numLine.No = i1;
                            _numLine.Save();
                        }
                        i1 = 1;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = TaxLine " + ex.ToString());
            }
        }

        public void RecoveryDeleteNo()
        {
            try
            {
                if (this.Tax != null)
                {
                    Tax _numHeader = Session.FindObject<Tax>
                                                (new BinaryOperator("Code", this.Tax.Code));

                    XPCollection<TaxLine> _numLines = new XPCollection<TaxLine>
                                                (Session, new GroupOperator(GroupOperatorType.And,
                                                 new BinaryOperator("This", this, BinaryOperatorType.NotEqual),
                                                 new BinaryOperator("Tax", _numHeader)),
                                                 new SortProperty("No", DevExpress.Xpo.DB.SortingDirection.Ascending));
                    if (_numLines != null)
                    {
                        int i = 0;
                        foreach (TaxLine _numLine in _numLines)
                        {
                            i += 1;
                            _numLine.No = i;
                            _numLine.Save();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = TaxLine " + ex.ToString());
            }
        }

        #endregion No

        #region Get

        public string GetName()
        {
            string _result = "";
            try
            {
                if (this._tax != null)
                {
                    string _result1 = null;
                    if (this._tax.Name != null)
                    {
                        _result1 = this._tax.Name;
                    }
                    _result = _result1 + " ";
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = TaxLine " + ex.ToString());
            }
            return _result;
        }

        private double GetSumTotalTxValue()
        {
            double _result = 0;
            try
            {
                double _locTotalTxValue = 0;
                double _locTotalTxValue2 = 0;

                if (!IsLoading)
                {
                    XPCollection<TaxLine> _locTaxLines = new XPCollection<TaxLine>(Session,
                                                         new BinaryOperator("PurchaseOrderLine", this.PurchaseOrderLine));

                    XPCollection<TaxLine> _locTaxLine2s = new XPCollection<TaxLine>(Session,
                                                         new BinaryOperator("PurchaseInvoiceLine", this.PurchaseInvoiceLine));

                    if (_locTaxLines != null && _locTaxLines.Count() > 0)
                    {
                        foreach (TaxLine _locTaxLine in _locTaxLines)
                        {
                            if (_locTaxLine.TaxNature == TaxNature.Increase)
                            {
                                _locTotalTxValue = _locTotalTxValue + _locTaxLine.TxValue;
                            }
                            else if (_locTaxLine.TaxNature == FullDrive.Module.CustomProcess.TaxNature.Decrease)
                            {
                                _locTotalTxValue = _locTotalTxValue - _locTaxLine.TxValue;
                            }
                        }
                        _result = _locTotalTxValue;
                    }

                    else if (_locTaxLine2s != null && _locTaxLine2s.Count() > 0)
                    {
                        foreach (TaxLine _locTaxLine2 in _locTaxLine2s)
                        {
                            if (_locTaxLine2.TaxNature == TaxNature.Increase)
                            {
                                _locTotalTxValue2 = _locTotalTxValue2 + _locTaxLine2.TxValue;
                            }
                            else if (_locTaxLine2.TaxNature == TaxNature.Decrease)
                            {
                                _locTotalTxValue2 = _locTotalTxValue2 - _locTaxLine2.TxValue;
                            }
                        }
                        _result = _locTotalTxValue2;
                    }
                    else
                    {
                        _result = 0;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("BusinessObject = TaxLine" + ex.ToString());
            }
            return _result;
        }

        private double GetSumTotalTaxAmount()
        {
            double _result = 0;
            try
            {
                double _locTotalTxAmount = 0;
                double _locTotalTxAmount2 = 0;

                if (!IsLoading)
                {
                    XPCollection<TaxLine> _locTaxLines = new XPCollection<TaxLine>(Session,
                                                         new BinaryOperator("PurchaseOrderLine", this.PurchaseOrderLine));

                    XPCollection<TaxLine> _locTaxLines2s = new XPCollection<TaxLine>(Session,
                                                         new BinaryOperator("PurchaseInvoiceLine", this.PurchaseInvoiceLine));

                    if (_locTaxLines != null && _locTaxLines.Count() > 0)
                    {
                        foreach (TaxLine _locTaxLine in _locTaxLines)
                        {
                            if(_locTaxLine.TaxNature == TaxNature.Increase)
                            {
                                _locTotalTxAmount = _locTotalTxAmount + _locTaxLine.TxAmount;
                            }
                            else if(_locTaxLine.TaxNature == TaxNature.Decrease)
                            {
                                _locTotalTxAmount = _locTotalTxAmount - _locTaxLine.TxAmount;
                            }
                        }
                        _result = _locTotalTxAmount;
                    }
                    else if (_locTaxLines2s != null && _locTaxLines2s.Count() > 0)
                    {
                        foreach (TaxLine _locTaxLines2 in _locTaxLines2s)
                        {
                            if(_locTaxLines2.TaxNature == TaxNature.Increase)
                            {
                                _locTotalTxAmount2 = _locTotalTxAmount2 + _locTaxLines2.TxAmount;
                            }
                            else if(_locTaxLines2.TaxNature == TaxNature.Decrease)
                            {
                                _locTotalTxAmount2 = _locTotalTxAmount2 - _locTaxLines2.TxAmount;
                            }
                        }
                        _result = _locTotalTxAmount2;
                    }
                    else
                    {
                        _result = 0;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("BusinessObject = TaxLine" + ex.ToString());
            }
            return _result;
        }

        #endregion Get

        #region Set

        private void SetTaxAmount()
        {
            try
            {
                if (_uAmount >= 0 && Tax.Value >= 0)
                {
                    if (_globFunc.GetRoundingList(Session, ObjectList.TaxLine, FieldName.TxAmount) == true)
                    {
                        this.TxAmount = _globFunc.GetRoundUp(Session, ((this.TUAmount - this.DiscAmount) * (this.TxValue / 100)), ObjectList.TaxLine, FieldName.TxAmount);
                    }
                    else
                    {
                        this.TxAmount = ((this.TUAmount - this.DiscAmount) * (this.TxValue / 100));
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = TaxLine " + ex.ToString());
            }
        }

        #endregion Set

    }
}