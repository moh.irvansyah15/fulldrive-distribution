﻿using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;

using DevExpress.ExpressApp.Editors;
using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.ConditionalAppearance;

namespace FullDrive.Module.BusinessObjects
{
    [DefaultClassOptions]
    [DefaultProperty("Code")]
    [NavigationItem("Inventory")]
    [RuleCombinationOfPropertiesIsUnique("SalesInvoiceCollectionRuleUnique", DefaultContexts.Save, "Code")]
    //[ImageName("BO_Contact")]
    //[DefaultProperty("DisplayMemberNameForLookupEditorsOfThisType")]
    //[DefaultListViewOptions(MasterDetailMode.ListViewOnly, false, NewItemRowPosition.None)]
    //[Persistent("DatabaseTableName")]
    // Specify more UI options using a declarative approach (https://documentation.devexpress.com/#eXpressAppFramework/CustomDocument112701).
    public class SalesInvoiceCollection : BaseObject
    { // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).
        private bool _activationPosting;
        private string _code;
        #region InitialOrganization
        private Company _company;
        private Workplace _workplace;
        private Division _division;
        private Department _department;
        private Section _section;
        private Employee _employee;
        #endregion InitialOrganization
        #region From
        private LocationType _locationTypeFrom;
        private StockType _stockTypeFrom;
        private XPCollection<Workplace> _availableWorkplaceFrom;
        private Workplace _workplaceFrom;
        private XPCollection<Location> _availableLocationFrom;
        private Location _locationFrom;
        private BeginningInventory _begInvFrom;
        #endregion From
        #region To
        private LocationType _locationTypeTo;
        private StockType _stockTypeTo;
        private XPCollection<Workplace> _availableWorkplaceTo;
        private Workplace _workplaceTo;
        private DateTime _pickingDate;
        private XPCollection<Picking> _availablePicking;
        private Picking _picking;
        private Location _locationTo;
        private BeginningInventory _begInvTo;
        private XPCollection<Vehicle> _availableVehicle;
        private Vehicle _vehicle;
        #endregion To
        private XPCollection<SalesInvoice> _availableSalesInvoice;
        private SalesInvoice _salesInvoice;
        private Employee _pic;
        private TransferOut _transferOut;
        private DateTime _docDate;
        private int _journalMonth;
        private int _journalYear;
        private Status _status;
        private DateTime _statusDate;
        private string _localUserAccess;
        private GlobalFunction _globFunc;
        //AuditTrail
        private XPCollection<AuditDataItemPersistent> changeHistory;

        public SalesInvoiceCollection(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if (!IsLoading)
            {
                if (Session.IsNewObject(this))
                {
                    _globFunc = new GlobalFunction();
                    DateTime now = DateTime.Now;
                    #region UserAccess
                    _localUserAccess = SecuritySystem.CurrentUserName;
                    UserAccess _locUserAccess = Session.FindObject<UserAccess>(new BinaryOperator("UserName", _localUserAccess));
                    if (_locUserAccess != null)
                    {
                        if (_locUserAccess.Employee != null)
                        {
                            if (_locUserAccess.Employee.Company != null && _locUserAccess.Employee.Workplace != null)
                            {
                                this.Code = _globFunc.GetNumberingUnlockOptimisticRecordBasedOrganization(this.Session.DataLayer, ObjectList.SalesInvoiceCollection, _locUserAccess.Employee.Company, _locUserAccess.Employee.Workplace);
                            }
                            else
                            {
                                this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.SalesInvoiceCollection);
                            }
                        }
                    }
                    #endregion UserAccess
                    this.Status = Status.Open;
                    this.StatusDate = now;
                }
            }
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).
        }

        #region Field

        [Browsable(false)]
        [ImmediatePostData()]
        public bool ActivationPosting
        {
            get { return _activationPosting; }
            set { SetPropertyValue("ActivationPosting", ref _activationPosting, value); }
        }

        [RuleRequiredField(DefaultContexts.Save)]
        [Appearance("SalesInvoiceCollectionCodeClose", Enabled = false)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue("Code", ref _code, value); }
        }

        #region Organization

        [ImmediatePostData()]
        [RuleRequiredField(DefaultContexts.Save)]
        [DataSourceCriteria("Active = true")]
        [Appearance("SalesInvoiceCollectionCompanyClose", Enabled = false)]
        public Company Company
        {
            get { return _company; }
            set { SetPropertyValue("Company", ref _company, value); }
        }

        [VisibleInListView(false)]
        [Appearance("SalesInvoiceCollectionWorkplaceClose", Enabled = false)]
        public Workplace Workplace
        {
            get { return _workplace; }
            set { SetPropertyValue("Workplace", ref _workplace, value); }
        }

        [Browsable(false)]
        public Division Division
        {
            get { return _division; }
            set { SetPropertyValue("Division", ref _division, value); }
        }

        [Browsable(false)]
        public Department Department
        {
            get { return _department; }
            set { SetPropertyValue("Department", ref _department, value); }
        }

        [Browsable(false)]
        public Section Section
        {
            get { return _section; }
            set { SetPropertyValue("Section", ref _section, value); }
        }

        [Browsable(false)]
        public Employee Employee
        {
            get { return _employee; }
            set { SetPropertyValue("Employee", ref _employee, value); }
        }

        #endregion Organization

        #region From 

        [VisibleInListView(false)]
        [ImmediatePostData()]
        [Appearance("SalesInvoiceCollectionLocationTypeFromClose", Enabled = false)]
        public LocationType LocationTypeFrom
        {
            get { return _locationTypeFrom; }
            set { SetPropertyValue("LocationTypeFrom", ref _locationTypeFrom, value); }
        }

        [VisibleInListView(false)]
        [ImmediatePostData()]
        [Appearance("SalesInvoiceCollectionStockTypeFromClose", Enabled = false)]
        public StockType StockTypeFrom
        {
            get { return _stockTypeFrom; }
            set { SetPropertyValue("StockTypeFrom", ref _stockTypeFrom, value); }
        }

        [Browsable(false)]
        public XPCollection<Workplace> AvailableWorkplaceFrom
        {
            get
            {
                if (!IsLoading)
                {
                    string _beginString = null;
                    string _endString = null;
                    string _fullString = null;

                    if (this.Company != null)
                    {
                        _localUserAccess = SecuritySystem.CurrentUserName;
                        UserAccess _locUserAccess = Session.FindObject<UserAccess>(new BinaryOperator("UserName", _localUserAccess));
                        if (_locUserAccess != null)
                        {
                            List<string> _stringOSD = new List<string>();

                            XPCollection<OrganizationSetupDetail> _locOrgSetupDetails = new XPCollection<OrganizationSetupDetail>
                                                                                (Session, new GroupOperator(GroupOperatorType.And,
                                                                                new BinaryOperator("Company", this.Company),
                                                                                new BinaryOperator("UserAccess", _locUserAccess),
                                                                                new BinaryOperator("AllWorkplace", false),
                                                                                new BinaryOperator("Active", true)));

                            if (_locOrgSetupDetails != null && _locOrgSetupDetails.Count() > 0)
                            {
                                foreach (OrganizationSetupDetail _locOrgSetupDetail in _locOrgSetupDetails)
                                {
                                    if (_locOrgSetupDetail.Workplace != null)
                                    {
                                        if (_locOrgSetupDetail.Workplace.Code != null)
                                        {
                                            _stringOSD.Add(_locOrgSetupDetail.Workplace.Code);
                                        }
                                    }
                                }
                            }

                            IEnumerable<string> _stringArrayOSDDistinct = _stringOSD.Distinct();
                            string[] _stringArrayOSDList = _stringArrayOSDDistinct.ToArray();
                            if (_stringArrayOSDList.Length == 1)
                            {
                                for (int i = 0; i < _stringArrayOSDList.Length; i++)
                                {
                                    Workplace _locW = Session.FindObject<Workplace>(new BinaryOperator("Code", _stringArrayOSDList[i]));
                                    if (_locW != null)
                                    {
                                        if (i == 0)
                                        {
                                            _beginString = "[Code]=='" + _locW.Code + "'";
                                        }
                                    }
                                }
                            }
                            else if (_stringArrayOSDList.Length > 1)
                            {
                                for (int i = 0; i < _stringArrayOSDList.Length; i++)
                                {
                                    Workplace _locW = Session.FindObject<Workplace>(new BinaryOperator("Code", _stringArrayOSDList[i]));
                                    if (_locW != null)
                                    {
                                        if (i == 0)
                                        {
                                            _beginString = "[Code]=='" + _locW.Code + "'";
                                        }
                                        else
                                        {
                                            _endString = _endString + " OR [Code]=='" + _locW.Code + "'";
                                        }
                                    }
                                }
                            }
                            _fullString = _beginString + _endString;

                            if (_fullString != null)
                            {
                                _availableWorkplaceFrom = new XPCollection<Workplace>(Session, CriteriaOperator.Parse(_fullString));
                            }
                            else
                            {
                                OrganizationSetupDetail _locOrgniSetupDetails = Session.FindObject<OrganizationSetupDetail>
                                                                        (new GroupOperator(GroupOperatorType.And,
                                                                        new BinaryOperator("Company", this.Company),
                                                                        new BinaryOperator("UserAccess", _locUserAccess),
                                                                        new BinaryOperator("OpenWorkplace", false),
                                                                        new BinaryOperator("AllWorkplace", true),
                                                                        new BinaryOperator("Active", true),
                                                                        new BinaryOperator("Default", false)));
                                if (_locOrgniSetupDetails != null)
                                {
                                    _availableWorkplaceFrom = new XPCollection<Workplace>(Session,
                                                        new GroupOperator(GroupOperatorType.And,
                                                        new BinaryOperator("Company", this.Company),
                                                        new BinaryOperator("Active", true)));
                                }
                            }

                        }
                        else
                        {
                            _availableWorkplaceFrom = new XPCollection<Workplace>(Session,
                                                        new GroupOperator(GroupOperatorType.And,
                                                        new BinaryOperator("Company", this.Company),
                                                        new BinaryOperator("Active", true)));
                        }
                    }
                }

                return _availableWorkplaceFrom;

            }
        }

        [ImmediatePostData()]
        [Appearance("SalesInvoiceCollectionWorkplaceFromClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [DataSourceProperty("AvailableWorkplaceFrom", DataSourcePropertyIsNullMode.SelectAll)]
        public Workplace WorkplaceFrom
        {
            get { return _workplaceFrom; }
            set { SetPropertyValue("WorkplaceFrom", ref _workplaceFrom, value); }
        }

        [ImmediatePostData()]
        [Appearance("SalesInvoiceCollectionLocationFromClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [DataSourceCriteria("Company = '@This.Company' And Workplace = '@this.WorkplaceFrom' And Active = true")]
        public Location LocationFrom
        {
            get { return _locationFrom; }
            set
            {
                SetPropertyValue("LocationFrom", ref _locationFrom, value);
                if (!IsLoading)
                {
                    if (this._locationFrom != null)
                    {
                        SetBegInvFrom(_locationFrom);
                    }
                    else
                    {
                        BegInvFrom = null;
                    }
                }
            }
        }

        [Browsable(false)]
        [ImmediatePostData()]
        [Appearance("SalesInvoiceCollectionBegInvFromClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public BeginningInventory BegInvFrom
        {
            get { return _begInvFrom; }
            set { SetPropertyValue("BegInvFrom", ref _begInvFrom, value); }
        }

        #endregion From

        #region To

        [VisibleInListView(false)]
        [ImmediatePostData()]
        [Appearance("SalesInvoiceCollectionLocationTypeToClose", Enabled = false)]
        public LocationType LocationTypeTo
        {
            get { return _locationTypeTo; }
            set { SetPropertyValue("LocationTypeTo", ref _locationTypeTo, value); }
        }

        [VisibleInListView(false)]
        [ImmediatePostData()]
        [Appearance("SalesInvoiceCollectionStockTypeToClose", Enabled = false)]
        public StockType StockTypeTo
        {
            get { return _stockTypeTo; }
            set { SetPropertyValue("StockTypeTo", ref _stockTypeTo, value); }
        }

        [Browsable(false)]
        public XPCollection<Workplace> AvailableWorkplaceTo
        {
            get
            {
                if (!IsLoading)
                {
                    string _beginString = null;
                    string _endString = null;
                    string _fullString = null;

                    if (this.Company != null)
                    {
                        _localUserAccess = SecuritySystem.CurrentUserName;
                        UserAccess _locUserAccess = Session.FindObject<UserAccess>(new BinaryOperator("UserName", _localUserAccess));
                        if (_locUserAccess != null)
                        {
                            List<string> _stringOSD = new List<string>();

                            XPCollection<OrganizationSetupDetail> _locOrganizationSetupDetails = new XPCollection<OrganizationSetupDetail>
                                                                                (Session, new GroupOperator(GroupOperatorType.And,
                                                                                new BinaryOperator("Company", this.Company),
                                                                                new BinaryOperator("UserAccess", _locUserAccess),
                                                                                new BinaryOperator("AllWorkplace", false),
                                                                                new BinaryOperator("OpenWorkplace", true),
                                                                                new BinaryOperator("Active", true)));

                            if (_locOrganizationSetupDetails != null && _locOrganizationSetupDetails.Count() > 0)
                            {
                                foreach (OrganizationSetupDetail _locOrganizationSetupDetail in _locOrganizationSetupDetails)
                                {
                                    if (_locOrganizationSetupDetail.Workplace != null)
                                    {
                                        if (_locOrganizationSetupDetail.Workplace.Code != null)
                                        {
                                            _stringOSD.Add(_locOrganizationSetupDetail.Workplace.Code);
                                        }
                                    }
                                }
                            }

                            IEnumerable<string> _stringArrayOSDDistinct = _stringOSD.Distinct();
                            string[] _stringArrayOSDList = _stringArrayOSDDistinct.ToArray();
                            if (_stringArrayOSDList.Length == 1)
                            {
                                for (int i = 0; i < _stringArrayOSDList.Length; i++)
                                {
                                    Workplace _locW = Session.FindObject<Workplace>(new BinaryOperator("Code", _stringArrayOSDList[i]));
                                    if (_locW != null)
                                    {
                                        if (i == 0)
                                        {
                                            _beginString = "[Code]=='" + _locW.Code + "'";
                                        }
                                    }
                                }
                            }
                            else if (_stringArrayOSDList.Length > 1)
                            {
                                for (int i = 0; i < _stringArrayOSDList.Length; i++)
                                {
                                    Workplace _locW = Session.FindObject<Workplace>(new BinaryOperator("Code", _stringArrayOSDList[i]));
                                    if (_locW != null)
                                    {
                                        if (i == 0)
                                        {
                                            _beginString = "[Code]=='" + _locW.Code + "'";
                                        }
                                        else
                                        {
                                            _endString = _endString + " OR [Code]=='" + _locW.Code + "'";
                                        }
                                    }
                                }
                            }
                            _fullString = _beginString + _endString;

                            if (_fullString != null)
                            {
                                _availableWorkplaceTo = new XPCollection<Workplace>(Session, CriteriaOperator.Parse(_fullString));
                            }
                            else
                            {
                                OrganizationSetupDetail _locWhsSetupDetail = Session.FindObject<OrganizationSetupDetail>
                                                                        (new GroupOperator(GroupOperatorType.And,
                                                                        new BinaryOperator("Company", this.Company),
                                                                        new BinaryOperator("UserAccess", _locUserAccess),
                                                                        new BinaryOperator("AllWorkplace", true),
                                                                        new BinaryOperator("Active", true)));
                                if (_locWhsSetupDetail != null)
                                {
                                    _availableWorkplaceTo = new XPCollection<Workplace>(Session,
                                                        new GroupOperator(GroupOperatorType.And,
                                                        new BinaryOperator("Company", this.Company),
                                                        new BinaryOperator("Active", true)));
                                }
                            }
                        }
                    }
                }

                return _availableWorkplaceTo;

            }
        }

        [VisibleInListView(false)]
        [ImmediatePostData()]
        [DataSourceProperty("AvailableWorkplaceTo", DataSourcePropertyIsNullMode.SelectAll)]
        [Appearance("SalesInvoiceCollectionWorkplaceToClose", Enabled = false)]
        public Workplace WorkplaceTo
        {
            get { return _workplaceTo; }
            set { SetPropertyValue("WorkplaceTo", ref _workplaceTo, value); }
        }

        [ImmediatePostData()]
        [ModelDefault("DisplayFormat", "{0:dd/MM/yyyy}")]
        [ModelDefault("EditMask", "dd/MM/yyyy")]
        [Appearance("SalesInvoiceCollectionPickingDateClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public DateTime PickingDate
        {
            get { return _pickingDate; }
            set { SetPropertyValue("PickingDate", ref _pickingDate, value); }

        }

        [Browsable(false)]
        public XPCollection<Picking> AvailablePicking
        {
            get
            {

                if (!IsLoading)
                {
                    if (this.Company != null && this.WorkplaceFrom != null)
                    {
                        XPQuery<PickingMonitoring> _pickingMonitoringsQuery = new XPQuery<PickingMonitoring>(Session);

                        var _pickingMonitorings = from sim in _pickingMonitoringsQuery
                                                  where ((sim.Status == Status.Open || sim.Status == Status.Posted)
                                                  && sim.Company == this.Company
                                                  && sim.Workplace == this.WorkplaceFrom
                                                  && sim.TransferOut == null
                                                  && sim.TransferOrder == null)
                                                  group sim by sim.Picking into g
                                                  select new { Picking = g.Key };

                        if (_pickingMonitorings != null && _pickingMonitorings.Count() > 0)
                        {
                            string _beginString = null;
                            string _endString = null;
                            string _fullString = null;

                            List<string> _stringPM = new List<string>();

                            foreach (var _pickingMonitoring in _pickingMonitorings)
                            {
                                if (_pickingMonitoring != null)
                                {
                                    if (_pickingMonitoring.Picking != null)
                                    {
                                        if (_pickingMonitoring.Picking.Code != null && _pickingMonitoring.Picking.PickingDate.ToShortDateString() == this.PickingDate.ToShortDateString())
                                        {
                                            _stringPM.Add(_pickingMonitoring.Picking.Code);
                                        }
                                    }
                                }
                            }

                            IEnumerable<string> _stringArrayPMDistinct = _stringPM.Distinct();
                            string[] _stringArrayPMList = _stringArrayPMDistinct.ToArray();
                            if (_stringArrayPMList.Length == 1)
                            {
                                for (int i = 0; i < _stringArrayPMList.Length; i++)
                                {
                                    if (i == 0)
                                    {
                                        _beginString = "[Code]=='" + _stringArrayPMList[i] + "'";
                                    }
                                }
                            }
                            else if (_stringArrayPMList.Length > 1)
                            {
                                for (int i = 0; i < _stringArrayPMList.Length; i++)
                                {
                                    if (i == 0)
                                    {
                                        _beginString = "[Code]=='" + _stringArrayPMList[i] + "'";
                                    }
                                    else
                                    {
                                        _endString = _endString + " OR [Code]=='" + _stringArrayPMList[i] + "'";
                                    }
                                }
                            }
                            _fullString = _beginString + _endString;

                            if (_fullString != null)
                            {
                                _availablePicking = new XPCollection<Picking>(Session, CriteriaOperator.Parse(_fullString));
                            }
                        }
                    }
                }

                return _availablePicking;
            }
        }

        [ImmediatePostData()]
        [DataSourceProperty("AvailablePicking")]
        [Appearance("SalesInvoiceCollectionPickingClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Picking Picking
        {
            get { return _picking; }
            set
            {
                SetPropertyValue("Picking", ref _picking, value);
                if (!IsLoading)
                {
                    if (this._picking != null)
                    {
                        if (this._picking.Vehicle != null)
                        {
                            this.Vehicle = this._picking.Vehicle;
                        }
                        else
                        {
                            this.Vehicle = null;
                        }
                    }
                    else
                    {
                        this.Vehicle = null;
                    }
                }
            }
        }

        [Browsable(false)]
        public XPCollection<Vehicle> AvailableVehicle
        {
            get
            {
                if (!IsLoading)
                {
                    if (this.Company != null && this.WorkplaceTo != null)
                    {
                        _availableVehicle = new XPCollection<Vehicle>(Session, new GroupOperator(GroupOperatorType.And,
                                                                            new BinaryOperator("Company", this.Company),
                                                                            new BinaryOperator("Workplace", this.WorkplaceTo),
                                                                            new BinaryOperator("Active", true)));
                    }
                    else
                    {
                        _availableVehicle = new XPCollection<Vehicle>(Session, new GroupOperator(GroupOperatorType.And,
                                                                            new BinaryOperator("Active", true)));
                    }

                }

                return _availableVehicle;

            }
        }

        [ImmediatePostData()]
        [DataSourceProperty("AvailableVehicle")]
        [Appearance("SalesInvoiceCollectionVehicleClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Vehicle Vehicle
        {
            get { return _vehicle; }
            set {
                SetPropertyValue("Vehicle", ref _vehicle, value);
                if (!IsLoading)
                {
                    if (this._vehicle != null)
                    {
                        SetLocationTo(this._vehicle);
                    }
                    else
                    {
                        this.LocationTo = null;
                    }
                }
            }
        }

        [VisibleInListView(false)]
        [ImmediatePostData()]
        [Appearance("SalesInvoiceCollectionLocationToClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [DataSourceCriteria("Company = '@This.Company' And Workplace = '@this.WorkplaceTo' And Active = true")]
        public Location LocationTo
        {
            get { return _locationTo; }
            set
            {
                SetPropertyValue("LocationTo", ref _locationTo, value);
                if (!IsLoading)
                {
                    if (this._locationTo != null)
                    {
                        SetBegInvTo(_locationTo);
                    }
                    else
                    {
                        BegInvTo = null;
                    }
                }
            }

        }

        [Browsable(false)]
        [ImmediatePostData()]
        [Appearance("SalesInvoiceCollectionBegInvToClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public BeginningInventory BegInvTo
        {
            get { return _begInvTo; }
            set { SetPropertyValue("BegInvTo", ref _begInvTo, value); }
        }

        #endregion To

        [Browsable(false)]
        public XPCollection<SalesInvoice> AvailableSalesInvoice
        {
            get
            {
                XPCollection<SalesInvoice> _locavailableSalesInvoice = null;
                if (!IsLoading)
                {
                    if (this.Company != null && this.WorkplaceTo != null && this.TransferOut != null )
                    {
                        if(this.Picking != null && this.Vehicle != null)
                        {
                            XPQuery<SalesInvoiceMonitoring> _salesInvoiceMonitoringsQuery = new XPQuery<SalesInvoiceMonitoring>(Session);

                            var _salesInvoiceMonitorings = from sim in _salesInvoiceMonitoringsQuery
                                                           where ((sim.Status == Status.Open || sim.Status == Status.Posted)
                                                           && sim.Company == this.Company
                                                           && sim.Workplace == this.WorkplaceFrom
                                                           && sim.Location == this.LocationFrom
                                                           && sim.BegInv == this.BegInvFrom
                                                           && sim.StockType == this.StockTypeFrom
                                                           && sim.Picking == this.Picking
                                                           && sim.Vehicle == this.Vehicle
                                                           && sim.TransferOut == null
                                                           && sim.TransferOrder == null)
                                                           group sim by sim.SalesInvoice into g
                                                           select new { SalesInvoice = g.Key };

                            if (_salesInvoiceMonitorings != null && _salesInvoiceMonitorings.Count() > 0)
                            {
                                string _beginString = null;
                                string _endString = null;
                                string _fullString = null;

                                List<string> _stringSIM = new List<string>();

                                foreach (var _salesInvoiceMonitoring in _salesInvoiceMonitorings)
                                {
                                    if (_salesInvoiceMonitoring != null)
                                    {
                                        if (_salesInvoiceMonitoring.SalesInvoice.Code != null)
                                        {
                                            _stringSIM.Add(_salesInvoiceMonitoring.SalesInvoice.Code);
                                        }
                                    }
                                }

                                IEnumerable<string> _stringArraySIMDistinct = _stringSIM.Distinct();
                                string[] _stringArraySIMList = _stringArraySIMDistinct.ToArray();
                                if (_stringArraySIMList.Length == 1)
                                {
                                    for (int i = 0; i < _stringArraySIMList.Length; i++)
                                    {
                                        if (i == 0)
                                        {
                                            _beginString = "[Code]=='" + _stringArraySIMList[i] + "'";
                                        }
                                    }
                                }
                                else if (_stringArraySIMList.Length > 1)
                                {
                                    for (int i = 0; i < _stringArraySIMList.Length; i++)
                                    {
                                        if (i == 0)
                                        {
                                            _beginString = "[Code]=='" + _stringArraySIMList[i] + "'";
                                        }
                                        else
                                        {
                                            _endString = _endString + " OR [Code]=='" + _stringArraySIMList[i] + "'";
                                        }
                                    }
                                }
                                _fullString = _beginString + _endString;

                                if (_fullString != null)
                                {
                                    _locavailableSalesInvoice = new XPCollection<SalesInvoice>(Session, CriteriaOperator.Parse(_fullString));
                                }
                            }
                        }
                        else
                        {
                            string _beginString = null;
                            string _endString = null;
                            string _fullString = null;

                            XPQuery<SalesInvoiceMonitoring> _salesInvMonsQuery = new XPQuery<SalesInvoiceMonitoring>(Session);


                            var _salesInvMons = from sim in _salesInvMonsQuery
                                                where ((sim.Status == Status.Open || sim.Status == Status.Posted)
                                                && sim.Company == this.Company
                                                && sim.Workplace == this.WorkplaceFrom
                                                && sim.Location == this.LocationFrom
                                                && sim.BegInv == this.BegInvFrom
                                                && sim.StockType == this.StockTypeFrom
                                                && sim.TransferOrder == null)
                                                group sim by sim.SalesInvoice into g
                                                select new { SalesInvoice = g.Key };

                            if (_salesInvMons != null && _salesInvMons.Count() > 0)
                            {
                                List<string> _stringSIM = new List<string>();

                                foreach (var _salesInvMon in _salesInvMons)
                                {
                                    if (_salesInvMon != null)
                                    {
                                        if (_salesInvMon.SalesInvoice.Code != null)
                                        {
                                            _stringSIM.Add(_salesInvMon.SalesInvoice.Code);
                                        }
                                    }
                                }

                                IEnumerable<string> _stringArraySIMDistinct = _stringSIM.Distinct();
                                string[] _stringArraySIMList = _stringArraySIMDistinct.ToArray();
                                if (_stringArraySIMList.Length == 1)
                                {
                                    for (int i = 0; i < _stringArraySIMList.Length; i++)
                                    {
                                        if (i == 0)
                                        {
                                            _beginString = "[Code]=='" + _stringArraySIMList[i] + "'";
                                        }
                                    }
                                }
                                else if (_stringArraySIMList.Length > 1)
                                {
                                    for (int i = 0; i < _stringArraySIMList.Length; i++)
                                    {
                                        if (i == 0)
                                        {
                                            _beginString = "[Code]=='" + _stringArraySIMList[i] + "'";
                                        }
                                        else
                                        {
                                            _endString = _endString + " OR [Code]=='" + _stringArraySIMList[i] + "'";
                                        }
                                    }
                                }
                                _fullString = _beginString + _endString;

                                if (_fullString != null)
                                {
                                    _locavailableSalesInvoice = new XPCollection<SalesInvoice>(Session, CriteriaOperator.Parse(_fullString));
                                }
                            }
                        }
                    }

                    if (_locavailableSalesInvoice != null && _locavailableSalesInvoice.Count() > 0)
                    {
                        _availableSalesInvoice = _locavailableSalesInvoice;
                    }
                }

                return _availableSalesInvoice;
            }
        }

        [ImmediatePostData()]
        [DataSourceProperty("AvailableSalesInvoice")]
        [RuleRequiredField(DefaultContexts.Save)]
        public SalesInvoice SalesInvoice
        {
            get { return _salesInvoice; }
            set {
                SetPropertyValue("SalesInvoice", ref _salesInvoice, value);
                if(this._salesInvoice != null)
                {
                    this.DocDate = this._salesInvoice.DocDate;
                    this.JournalMonth = this._salesInvoice.JournalMonth;
                    this.JournalMonth = this._salesInvoice.JournalYear;
                    if (this._salesInvoice.PIC != null)
                    {
                        this.PIC = this._salesInvoice.PIC;
                    }
                    else
                    {
                        this.PIC = null;
                    }
                }
                else
                {
                    this.DocDate = DateTime.Now;
                    this.JournalMonth = 0;
                    this.JournalMonth = 0;
                    this.PIC = null;
                }
            }
        }

        [VisibleInListView(false)]
        [ImmediatePostData()]
        [Appearance("SalesInvoiceCollectionPICClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Employee PIC
        {
            get { return _pic; }
            set { SetPropertyValue("InternalPIC", ref _pic, value); }
        }

        [Browsable(false)]
        [ImmediatePostData()]
        [VisibleInListView(false)]
        [ModelDefault("DisplayFormat", "{0:dd/MM/yyyy}")]
        [ModelDefault("EditMask", "dd/MM/yyyy")]
        [Appearance("SalesInvoiceCollectionDocDateClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public DateTime DocDate
        {
            get { return _docDate; }
            set
            {
                SetPropertyValue("DocDate", ref _docDate, value);
                if (!IsLoading)
                {
                    this.JournalMonth = this._docDate.Month;
                    this.JournalYear = this._docDate.Year;
                }
            }
        }

        [Browsable(false)]
        public int JournalMonth
        {
            get { return _journalMonth; }
            set { SetPropertyValue("JournalMonth", ref _journalMonth, value); }
        }

        [Browsable(false)]
        public int JournalYear
        {
            get { return _journalYear; }
            set { SetPropertyValue("JournalYear", ref _journalYear, value); }
        }

        [Appearance("SalesInvoiceCollectionStatusClose", Enabled = false)]
        public Status Status
        {
            get { return _status; }
            set { SetPropertyValue("Status", ref _status, value); }
        }

        [ModelDefault("DisplayFormat", "{0:dd/MM/yyyy}")]
        [ModelDefault("EditMask", "dd/MM/yyyy")]
        [Appearance("SalesInvoiceCollectionStatusDateClose", Enabled = false)]
        public DateTime StatusDate
        {
            get { return _statusDate; }
            set { SetPropertyValue("StatusDate", ref _statusDate, value); }
        }

        [ImmediatePostData()]
        [RuleRequiredField(DefaultContexts.Save)]
        [Appearance("SalesInvoiceCollectionTransferOutClose", Enabled = false)]
        [Association("TransferOut-SalesInvoiceCollections")]
        public TransferOut TransferOut
        {
            get { return _transferOut; }
            set
            {
                SetPropertyValue("TransferOut", ref _transferOut, value);
                if (!IsLoading)
                {
                    if (this._transferOut != null)
                    {
                        if (Session.IsNewObject(this))
                        {
                            if (this._transferOut.Company != null) { this.Company = this._transferOut.Company; }
                            if (this._transferOut.Workplace != null) { this.Workplace = this._transferOut.Workplace; }
                            if (this._transferOut.Division != null) { this.Division = this._transferOut.Division; }
                            if (this._transferOut.Department != null) { this.Department = this._transferOut.Department; }
                            if (this._transferOut.Section != null) { this.Section = this._transferOut.Section; }
                            if (this._transferOut.Employee != null) { this.Employee = this._transferOut.Employee; }
                            #region From
                            if (this._transferOut.LocationTypeFrom != LocationType.None) { this.LocationTypeFrom = this._transferOut.LocationTypeFrom; }
                            if (this._transferOut.StockTypeFrom != StockType.None) { this.StockTypeFrom = this._transferOut.StockTypeFrom; }
                            if (this._transferOut.WorkplaceFrom != null) { this.WorkplaceFrom = this._transferOut.WorkplaceFrom; }
                            if (this._transferOut.LocationFrom != null) { this.LocationFrom = this._transferOut.LocationFrom; }
                            if (this._transferOut.BegInvFrom != null) { this.BegInvFrom = this._transferOut.BegInvFrom; }
                            #endregion From
                            #region To
                            if (this._transferOut.LocationTypeTo != LocationType.None) { this.LocationTypeTo = this._transferOut.LocationTypeTo; }
                            if (this._transferOut.StockTypeTo != StockType.None) { this.StockTypeTo = this._transferOut.StockTypeTo; }
                            if (this._transferOut.WorkplaceTo != null) { this.WorkplaceTo = this._transferOut.WorkplaceTo; }
                            this.PickingDate = this._transferOut.PickingDate;
                            if (this._transferOut.Picking != null) { this.Picking = this._transferOut.Picking; }
                            if (this._transferOut.Vehicle != null) { this.Vehicle = this._transferOut.Vehicle; }
                            if (this._transferOut.LocationTo != null) { this.LocationTo = this._transferOut.LocationTo; }
                            if (this._transferOut.BegInvTo != null) { this.BegInvTo = this._transferOut.BegInvTo; }
                            #endregion To
                        }
                    }
                    else
                    {
                        this.Company = null;
                        this.Workplace = null;
                        this.Division = null;
                        this.Department = null;
                        this.Section = null;
                        this.Employee = null;
                        this.LocationTypeFrom = LocationType.None;
                        this.StockTypeFrom = StockType.None;
                        this.WorkplaceFrom = null;
                        this.LocationFrom = null;
                        this.BegInvFrom = null;
                        this.LocationTypeTo = LocationType.None;
                        this.StockTypeTo = StockType.None;
                        this.WorkplaceTo = null;
                        this.Picking = null;
                        this.Vehicle = null;
                        this.LocationTo = null;
                        this.BegInvTo = null;
                    }
                }
            }
        }

        //AuditTrail
        [CollectionOperationSet(AllowAdd = false, AllowRemove = true)]
        public XPCollection<AuditDataItemPersistent> ChangeHistory
        {
            get
            {
                if (changeHistory == null)
                {
                    changeHistory = AuditedObjectWeakReference.GetAuditTrail(Session, this);
                }
                return changeHistory;
            }
        }

        #endregion Field

        //=================================== Code Only ===================================

        private void SetBegInvFrom(Location _locLocation)
        {
            try
            {
                if (this.Company != null && this.WorkplaceFrom != null)
                {
                    BeginningInventory _locBegInvFrom = Session.FindObject<BeginningInventory>
                                                        (new GroupOperator(GroupOperatorType.And,
                                                        new BinaryOperator("Company", this.Company),
                                                        new BinaryOperator("Workplace", this.WorkplaceFrom),
                                                        new BinaryOperator("Location", _locLocation),
                                                        new BinaryOperator("LocationType", this.LocationTypeFrom),
                                                        new BinaryOperator("StockType", this.StockTypeFrom),
                                                        new BinaryOperator("Active", true)));
                    if (_locBegInvFrom != null)
                    {
                        this.BegInvFrom = _locBegInvFrom;
                    }
                    else
                    {
                        this.BegInvFrom = null;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Business Object = SalesInvoiceCollection ", ex.ToString());
            }
        }

        private void SetBegInvTo(Location _locLocation)
        {
            try
            {
                DateTime now = DateTime.Now;
                if (this.Company != null && this.WorkplaceTo != null)
                {
                    BeginningInventory _locBegInv = Session.FindObject<BeginningInventory>
                                                        (new GroupOperator(GroupOperatorType.And,
                                                        new BinaryOperator("Company", this.Company),
                                                        new BinaryOperator("Workplace", this.WorkplaceTo),
                                                        new BinaryOperator("Location", _locLocation),
                                                        new BinaryOperator("LocationType", this.LocationTypeTo),
                                                        new BinaryOperator("StockType", this.StockTypeTo),
                                                        new BinaryOperator("Active", true)));
                    if (_locBegInv != null)
                    {
                        this.BegInvTo = _locBegInv;
                    }
                    else
                    {
                        this.BegInvTo = null;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Business Object = SalesInvoiceCollection ", ex.ToString());
            }
        }

        private void SetLocationTo(Vehicle _locVehicle)
        {
            try
            {
                DateTime now = DateTime.Now;
                if (this.Company != null && this.WorkplaceTo != null)
                {
                    Location _locLocation = Session.FindObject<Location>
                                                        (new GroupOperator(GroupOperatorType.And,
                                                        new BinaryOperator("Company", this.Company),
                                                        new BinaryOperator("Workplace", this.WorkplaceTo),
                                                        new BinaryOperator("OpenVehicle", true),
                                                        new BinaryOperator("Vehicle", _locVehicle),
                                                        new BinaryOperator("Active", true)));
                    if (_locLocation != null)
                    {
                        if (_locLocation != null)
                        {
                            this.LocationTo = _locLocation;
                        }
                        else
                        {
                            this.LocationTo = null;
                        }
                    }
                    else
                    {
                        this.LocationTo = null;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Business Object = SalesInvoiceCollection ", ex.ToString());
            }
        }
    }
}