﻿using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;

using DevExpress.ExpressApp.Editors;
using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.ConditionalAppearance;

namespace FullDrive.Module.BusinessObjects
{
    [DefaultClassOptions]
    [DefaultProperty("Code")]
    [NavigationItem("Finance & Accounting")]
    [RuleCombinationOfPropertiesIsUnique("ReceivableTransactionLine2RuleUnique", DefaultContexts.Save, "Code")]
    //[ImageName("BO_Contact")]
    //[DefaultProperty("DisplayMemberNameForLookupEditorsOfThisType")]
    //[DefaultListViewOptions(MasterDetailMode.ListViewOnly, false, NewItemRowPosition.None)]
    //[Persistent("DatabaseTableName")]
    // Specify more UI options using a declarative approach (https://documentation.devexpress.com/#eXpressAppFramework/CustomDocument112701).
    public class ReceivableTransactionLine2 : BaseObject
    { // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).

        private bool _activationPosting;
        private int _no;
        private string _code;
        private bool _select;
        #region InitialOrganization
        private Company _company;
        private Workplace _workplace;
        private Division _division;
        private Department _department;
        private Section _section;
        private Employee _employee;
        private Division _div;
        private Department _dept;
        private Section _sect;
        private XPCollection<Employee> _availablePIC;
        private Employee _pic;
        private Employee _collector;
        #endregion InitialOrganization
        private XPCollection<Vehicle> _availableVehicle;
        private Vehicle _vehicle;
        private DateTime _postingDate;
        private SalesInvoice _salesInvoice;
        private Currency _currency;
        private PriceGroup _priceGroup;
        private PostingType _salesType;
        private PostingMethod _postingMethod;
        private PaymentMethodType _paymentMethodType;
        private PaymentMethod _paymentMethod;
        private PaymentType _paymentType;
        private bool _openDueDate;
        private string _chequeNo;
        private DateTime _estimatedDate;
        private DateTime _actualDate;
        private DateTime _dueDate;
        private double _amount;
        private bool _openDN;
        private bool _openCN;
        private double _amountDN;
        private double _amountCN;
        private double _plan;
        private double _outstanding;
        private double _actual;
        private double _overPayment;
        private bool _settled;
        private string _description;
        #region Sales
        private XPCollection<BusinessPartner> _availableSalesToCustomer;
        private BusinessPartner _salesToCustomer;
        private string _salesToContact;
        private Country _salesToCountry;
        private City _salesToCity;
        private string _salesToAddress;
        #endregion Sales
        #region Bill
        private BusinessPartner _billToCustomer;
        private string _billToContact;
        private Country _billToCountry;
        private City _billToCity;
        private string _billToAddress;
        #endregion Bill
        #region InitialBank
        private bool _multiBank;
        private bool _bankBasedInvoice;
        private XPCollection<BankAccount> _availableCompanyBankAccount;
        private BankAccount _companyBankAccount;
        private string _companyAccountNo;
        private string _companyAccountName;
        private XPCollection<BankAccount> _availableBankAccount;
        private BankAccount _bankAccount;
        private string _accountNo;
        private string _accountName;
        #endregion InitialBank
        private DateTime _docDate;
        private int _journalMonth;
        private int _journalYear;
        private Status _status;
        private DateTime _statusdate;
        private int _postedCount;
        private string _signCode;
        private OrderCollectionMonitoring _orderCollectionMonitoring;
        private DateTime _pickingDate;
        private Picking _picking;
        private PickingMonitoring _pickingMonitoring;
        private SalesInvoiceMonitoring _salesInvoiceMonitoring;
        private ReceivableTransaction2 _receivableTransaction2;
        private string _userAccessIn;
        private GlobalFunction _globFunc;
        //AuditTrail
        private XPCollection<AuditDataItemPersistent> changeHistory;

        public ReceivableTransactionLine2(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if (!IsLoading)
            {
                if (Session.IsNewObject(this))
                {
                    DateTime now = DateTime.Now;
                    _globFunc = new GlobalFunction();
                    #region UserAccess
                    _userAccessIn = SecuritySystem.CurrentUserName;
                    UserAccess _locUserAccess = Session.FindObject<UserAccess>(new BinaryOperator("UserName", _userAccessIn));
                    if (_locUserAccess != null)
                    {
                        if (_locUserAccess.Employee != null)
                        {
                            if (_locUserAccess.Employee.Company != null && _locUserAccess.Employee.Workplace != null)
                            {
                                this.Code = _globFunc.GetNumberingUnlockOptimisticRecordBasedOrganization(this.Session.DataLayer, ObjectList.ReceivableTransactionLine2, _locUserAccess.Employee.Company, _locUserAccess.Employee.Workplace);
                            }
                            else
                            {
                                this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.ReceivableTransactionLine2);
                            }
                        }
                    }
                    #endregion UserAccess
                    this.Select = true;
                    this.Status = Status.Open;
                    this.StatusDate = now;
                    this.SalesType = PostingType.Sales;
                    this.PostingMethod = PostingMethod.Bill;
                }
            }
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).
        }

        protected override void OnSaving()
        {
            base.OnSaving();
            if (!IsLoading && IsSaving)
            {
                UpdateNo();
            }
        }

        protected override void OnDeleting()
        {
            base.OnDeleting();
            if (!IsLoading)
            {
                RecoveryDeleteNo();
            }
        }

        #region Field

        [Browsable(false)]
        [ImmediatePostData()]
        public bool ActivationPosting
        {
            get { return _activationPosting; }
            set { SetPropertyValue("ActivationPosting", ref _activationPosting, value); }
        }

        [Appearance("ReceivableTransactionLine2NoClose", Enabled = false)]
        public int No
        {
            get { return _no; }
            set { SetPropertyValue("No", ref _no, value); }
        }

        [VisibleInListView(false)]
        [Appearance("ReceivableTransactionLine2Close", Enabled = false)]
        [RuleRequiredField(DefaultContexts.Save)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue("Code", ref _code, value); }
        }

        [Appearance("ReceivableTransactionLine2SelectClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public bool Select
        {
            get { return _select; }
            set { SetPropertyValue("Select", ref _select, value); }
        }

        #region Organization

        [Appearance("ReceivableTransactionLine2CompanyClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Company Company
        {
            get { return _company; }
            set { SetPropertyValue("Company", ref _company, value); }
        }

        [Appearance("ReceivableTransactionLine2WorkplaceClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Workplace Workplace
        {
            get { return _workplace; }
            set { SetPropertyValue("Workplace", ref _workplace, value); }
        }

        [Browsable(false)]
        public Division Division
        {
            get { return _division; }
            set { SetPropertyValue("Division", ref _division, value); }
        }

        [Browsable(false)]
        public Department Department
        {
            get { return _department; }
            set { SetPropertyValue("Department", ref _department, value); }
        }

        [Browsable(false)]
        public Section Section
        {
            get { return _section; }
            set { SetPropertyValue("Section", ref _section, value); }
        }

        [Browsable(false)]
        public Employee Employee
        {
            get { return _employee; }
            set { SetPropertyValue("Employee", ref _employee, value); }
        }

        [VisibleInListView(false)]
        [ImmediatePostData()]
        [DataSourceCriteria("Company = '@This.Company' And Active = true")]
        [Appearance("ReceivableTransactionLine2DivClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Division Div
        {
            get { return _div; }
            set { SetPropertyValue("Div", ref _div, value); }
        }

        [VisibleInListView(false)]
        [ImmediatePostData()]
        [DataSourceCriteria("Company = '@This.Company' And Division = '@This.Div' And Active = true")]
        [Appearance("ReceivableTransactionLine2DeptClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Department Dept
        {
            get { return _dept; }
            set { SetPropertyValue("Dept", ref _dept, value); }
        }

        [VisibleInListView(false)]
        [ImmediatePostData()]
        [DataSourceCriteria("Company = '@This.Company' And Department = '@This.Dept' And Active = true")]
        [Appearance("ReceivableTransactionLine2SectClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Section Sect
        {
            get { return _sect; }
            set { SetPropertyValue("Sect", ref _sect, value); }
        }

        [Browsable(false)]
        public XPCollection<Employee> AvailablePIC
        {
            get
            {
                XPCollection<Employee> _locPICs = null;
                if (this.Company != null && this.Workplace != null)
                {
                    #region DivDeptSect
                    if (this.Div != null && this.Dept != null && this.Sect != null)
                    {
                        _locPICs = new XPCollection<Employee>
                                  (Session, new GroupOperator(GroupOperatorType.And,
                                    new BinaryOperator("Company", this.Company),
                                    new BinaryOperator("Workplace", this.Workplace),
                                    new BinaryOperator("Division", this.Div),
                                    new BinaryOperator("Department", this.Dept),
                                    new BinaryOperator("Section", this.Sect),
                                    new BinaryOperator("Active", true),
                                    new GroupOperator(GroupOperatorType.Or,
                                    new BinaryOperator("SalesRole", SalesRole.TakingOrder),
                                    new BinaryOperator("SalesRole", SalesRole.All))));
                    }
                    else if (this.Div != null && this.Dept != null && this.Sect == null)
                    {
                        _locPICs = new XPCollection<Employee>
                                  (Session, new GroupOperator(GroupOperatorType.And,
                                    new BinaryOperator("Company", this.Company),
                                    new BinaryOperator("Workplace", this.Workplace),
                                    new BinaryOperator("Division", this.Div),
                                    new BinaryOperator("Department", this.Dept),
                                    new BinaryOperator("Active", true),
                                    new GroupOperator(GroupOperatorType.Or,
                                    new BinaryOperator("SalesRole", SalesRole.TakingOrder),
                                    new BinaryOperator("SalesRole", SalesRole.All))));

                    }
                    else if (this.Div != null && this.Dept == null && this.Sect == null)
                    {
                        _locPICs = new XPCollection<Employee>
                                  (Session, new GroupOperator(GroupOperatorType.And,
                                    new BinaryOperator("Company", this.Company),
                                    new BinaryOperator("Workplace", this.Workplace),
                                    new BinaryOperator("Division", this.Div),
                                    new BinaryOperator("Active", true),
                                    new GroupOperator(GroupOperatorType.Or,
                                    new BinaryOperator("SalesRole", SalesRole.TakingOrder),
                                    new BinaryOperator("SalesRole", SalesRole.All))));

                    }
                    else if (this.Div == null && this.Dept == null && this.Sect == null)
                    {
                        _locPICs = new XPCollection<Employee>
                                  (Session, new GroupOperator(GroupOperatorType.And,
                                    new BinaryOperator("Company", this.Company),
                                    new BinaryOperator("Workplace", this.Workplace),
                                    new BinaryOperator("Active", true),
                                    new GroupOperator(GroupOperatorType.Or,
                                    new BinaryOperator("SalesRole", SalesRole.TakingOrder),
                                    new BinaryOperator("SalesRole", SalesRole.All))));

                    }
                    #endregion DivDeptSect
                    if (_locPICs != null && _locPICs.Count() > 0)
                    {
                        _availablePIC = _locPICs;
                    }
                }
                else
                {
                    _availablePIC = new XPCollection<Employee>(Session, new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("Active", true),
                                                            new GroupOperator(GroupOperatorType.Or,
                                                            new BinaryOperator("SalesRole", SalesRole.TakingOrder),
                                                            new BinaryOperator("SalesRole", SalesRole.All))));
                }

                return _availablePIC;

            }
        }

        [ImmediatePostData()]
        [DataSourceProperty("AvailablePIC")]
        [Appearance("ReceivableTransactionLine2PICClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Employee PIC
        {
            get { return _pic; }
            set
            {
                SetPropertyValue("Salesman", ref _pic, value);
                if (!IsLoading)
                {
                    if (this._pic != null)
                    {
                        SetVehicle(_pic);
                    }
                    else
                    {
                        this.Vehicle = null;
                    }
                }
            }
        }

        [ImmediatePostData()]
        [DataSourceCriteria("Company = '@This.Company' And Workplace = '@This.Workplace' And Active = true")]
        [Appearance("ReceivableTransactionLine2CollectorClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Employee Collector
        {
            get { return _collector; }
            set { SetPropertyValue("Collector", ref _collector, value); }
        }

        #endregion Organization

        [Browsable(false)]
        public XPCollection<Vehicle> AvailableVehicle
        {
            get
            {
                if (!IsLoading)
                {
                    if (this.Company != null && this.Workplace != null)
                    {
                        string _beginString = null;
                        string _endString = null;
                        string _fullString = null;

                        #region PIC
                        if (this.PIC != null)
                        {
                            List<string> _stringSVS = new List<string>();

                            XPCollection<SalesmanVehicleSetup> _locSalesmanVehicleSetups = new XPCollection<SalesmanVehicleSetup>
                                                                                      (Session, new GroupOperator(GroupOperatorType.And,
                                                                                       new BinaryOperator("Company", this.Company),
                                                                                       new BinaryOperator("Workplace", this.Workplace),
                                                                                       new BinaryOperator("Salesman", this.PIC),
                                                                                       new BinaryOperator("Active", true)));

                            if (_locSalesmanVehicleSetups != null && _locSalesmanVehicleSetups.Count() > 0)
                            {
                                foreach (SalesmanVehicleSetup _locSalesmanVehicleSetup in _locSalesmanVehicleSetups)
                                {
                                    if (_locSalesmanVehicleSetup.Vehicle != null)
                                    {
                                        if (_locSalesmanVehicleSetup.Vehicle.Code != null)
                                        {
                                            _stringSVS.Add(_locSalesmanVehicleSetup.Vehicle.Code);
                                        }
                                    }
                                }
                            }

                            IEnumerable<string> _stringArraySVSDistinct = _stringSVS.Distinct();
                            string[] _stringArraySVSList = _stringArraySVSDistinct.ToArray();
                            if (_stringArraySVSList.Length == 1)
                            {
                                for (int i = 0; i < _stringArraySVSList.Length; i++)
                                {
                                    Vehicle _locV = Session.FindObject<Vehicle>(new BinaryOperator("Code", _stringArraySVSList[i]));
                                    if (_locV != null)
                                    {
                                        if (i == 0)
                                        {
                                            _beginString = "[Code]=='" + _locV.Code + "'";
                                        }
                                    }
                                }
                            }
                            else if (_stringArraySVSList.Length > 1)
                            {
                                for (int i = 0; i < _stringArraySVSList.Length; i++)
                                {
                                    Vehicle _locV = Session.FindObject<Vehicle>(new BinaryOperator("Code", _stringArraySVSList[i]));
                                    if (_locV != null)
                                    {
                                        if (i == 0)
                                        {
                                            _beginString = "[Code]=='" + _locV.Code + "'";
                                        }
                                        else
                                        {
                                            _endString = _endString + " OR [Code]=='" + _locV.Code + "'";
                                        }
                                    }
                                }
                            }
                            _fullString = _beginString + _endString;

                            if (_fullString != null && _fullString != "")
                            {
                                _availableVehicle = new XPCollection<Vehicle>(Session, CriteriaOperator.Parse(_fullString));
                            }
                            else
                            {
                                _availableVehicle = new XPCollection<Vehicle>(Session, new GroupOperator(GroupOperatorType.And,
                                                                                    new BinaryOperator("Company", this.Company),
                                                                                    new BinaryOperator("Workplace", this.Workplace),
                                                                                    new BinaryOperator("Active", true)));
                            }
                        }
                        #endregion PIC
                        #region NonPIC
                        else
                        {
                            _availableVehicle = new XPCollection<Vehicle>(Session, new GroupOperator(GroupOperatorType.And,
                                                                            new BinaryOperator("Company", this.Company),
                                                                            new BinaryOperator("Workplace", this.Workplace),
                                                                            new BinaryOperator("Active", true)));
                        }
                        #endregion NonPIC
                    }
                    else
                    {
                        _availableVehicle = new XPCollection<Vehicle>(Session, new GroupOperator(GroupOperatorType.And,
                                                                            new BinaryOperator("Active", true)));
                    }

                }

                return _availableVehicle;

            }
        }

        [Browsable(false)]
        [ImmediatePostData()]
        [DataSourceProperty("AvailableVehicle")]
        [Appearance("ReceivableTransactionLine2VehicleClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Vehicle Vehicle
        {
            get { return _vehicle; }
            set { SetPropertyValue("Vehicle", ref _vehicle, value); }
        }

        [VisibleInListView(false)]
        [Appearance("ReceivableTransactionLine2PostingDateClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public DateTime PostingDate
        {
            get { return _postingDate; }
            set { SetPropertyValue("PostingDate", ref _postingDate, value); }
        }

        [ImmediatePostData()]
        [Appearance("ReceivableTransactionLine2SalesInvoiceClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public SalesInvoice SalesInvoice
        {
            get { return _salesInvoice; }
            set
            {
                SetPropertyValue("SalesInvoice", ref _salesInvoice, value);
                if (!IsLoading)
                {
                    if (_salesInvoice != null)
                    {
                        if (_salesInvoice.SalesToCustomer != null)
                        {
                            this.SalesToCustomer = _salesInvoice.SalesToCustomer;
                            this.SalesToContact = _salesInvoice.SalesToContact;
                            this.SalesToCountry = _salesInvoice.SalesToCountry;
                            this.SalesToCity = _salesInvoice.SalesToCity;
                            this.SalesToAddress = _salesInvoice.SalesToAddress;
                            this.BillToCustomer = _salesInvoice.SalesToCustomer;
                        }
                        else
                        {
                            this.SalesToCustomer = null;
                            this.SalesToContact = null;
                            this.SalesToCountry = null;
                            this.SalesToCity = null;
                            this.SalesToAddress = null;
                            this.BillToCustomer = null;
                        }
                        if (this._salesInvoice.CompanyBankAccount != null) { this.CompanyBankAccount = this._salesInvoice.CompanyBankAccount; }
                        if (this._salesInvoice.CompanyAccountName != null) { this.CompanyAccountName = this._salesInvoice.CompanyAccountName; }
                        if (this._salesInvoice.CompanyAccountNo != null) { this.CompanyAccountNo = this._salesInvoice.CompanyAccountNo; }
                        if (this._salesInvoice.BankAccount != null) { this.BankAccount = this._salesInvoice.BankAccount; }
                        if (this._salesInvoice.AccountName != null) { this.AccountName = this._salesInvoice.AccountName; }
                        if (this._salesInvoice.AccountNo != null) { this.CompanyAccountNo = this._salesInvoice.AccountNo; }
                        if (this._salesInvoice.PriceGroup != null) { this.PriceGroup = this._salesInvoice.PriceGroup; }
                    }
                    else
                    {
                        this.SalesToCustomer = null;
                        this.SalesToContact = null;
                        this.SalesToCountry = null;
                        this.SalesToCity = null;
                        this.SalesToAddress = null;
                        this.BillToCustomer = null;
                        this.CompanyBankAccount = null;
                        this.CompanyAccountName = null;
                        this.CompanyAccountNo = null;
                        this.BankAccount = null;
                        this.AccountName = null;
                        this.AccountNo = null;
                        this.PriceGroup = null;
                    }
                }
            }
        }

        [Appearance("ReceivableTransactionLine2CurrencyClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Currency Currency
        {
            get { return _currency; }
            set { SetPropertyValue("Currency", ref _currency, value); }
        }

        [VisibleInListView(false)]
        [Appearance("ReceivableTransactionLine2SalesTypeClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public PostingType SalesType
        {
            get { return _salesType; }
            set { SetPropertyValue("PostingType", ref _salesType, value); }
        }

        [VisibleInListView(false)]
        [Appearance("ReceivableTransactionLine2PostingMethodClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public PostingMethod PostingMethod
        {
            get { return _postingMethod; }
            set { SetPropertyValue("PostingMethod", ref _postingMethod, value); }
        }

        [VisibleInListView(false)]
        [Appearance("ReceivableTransactionLine2PriceGroupClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public PriceGroup PriceGroup
        {
            get { return _priceGroup; }
            set { SetPropertyValue("PriceGroup", ref _priceGroup, value); }
        }

        [VisibleInListView(false)]
        [Appearance("ReceivableTransactionLine2PaymentMethodTypeClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public PaymentMethodType PaymentMethodType
        {
            get { return _paymentMethodType; }
            set { SetPropertyValue("PaymentMethodType", ref _paymentMethodType, value); }
        }

        [VisibleInListView(false)]
        [Appearance("ReceivableTransactionLine2PaymentMethodClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public PaymentMethod PaymentMethod
        {
            get { return _paymentMethod; }
            set { SetPropertyValue("PaymentMethod", ref _paymentMethod, value); }
        }

        [ImmediatePostData()]
        [Appearance("ReceivableTransactionLine2PaymentTypeClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public PaymentType PaymentType
        {
            get { return _paymentType; }
            set
            {
                SetPropertyValue("PaymentType", ref _paymentType, value);
                if (!IsLoading)
                {
                    if (this._paymentType == PaymentType.Cheque)
                    {
                        this.OpenDueDate = true;
                    }
                    else
                    {
                        this.OpenDueDate = false;
                    }
                }
            }
        }

        [Browsable(false)]
        [ImmediatePostData()]
        public bool OpenDueDate
        {
            get { return _openDueDate; }
            set { SetPropertyValue("OpenDueDate", ref _openDueDate, value); }
        }

        [Appearance("ReceivableTransactionLine2ChequeNoClose1", Criteria = "OpenDueDate = true", Enabled = true)]
        [Appearance("ReceivableTransactionLine2ChequeNoClose2", Criteria = "OpenDueDate = false", Enabled = false)]
        [Appearance("ReceivableTransactionLine2ChequeNoClose3", Criteria = "ActivationPosting = true", Enabled = false)]
        public string ChequeNo
        {
            get { return _chequeNo; }
            set { SetPropertyValue("ChequeNo", ref _chequeNo, value); }
        }

        [ModelDefault("DisplayFormat", "{0:dd/MM/yyyy}")]
        [ModelDefault("EditMask", "dd/MM/yyyy")]
        [Appearance("ReceivableTransactionLine2EstimatedDateClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public DateTime EstimatedDate
        {
            get { return _estimatedDate; }
            set { SetPropertyValue("EstimatedDate", ref _estimatedDate, value); }
        }

        [ModelDefault("DisplayFormat", "{0:dd/MM/yyyy}")]
        [ModelDefault("EditMask", "dd/MM/yyyy")]
        [Appearance("ReceivableTransactionLine2ActualDateClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public DateTime ActualDate
        {
            get { return _actualDate; }
            set { SetPropertyValue("ActualDate", ref _actualDate, value); }
        }

        [ModelDefault("DisplayFormat", "{0:dd/MM/yyyy}")]
        [ModelDefault("EditMask", "dd/MM/yyyy")]
        [Appearance("ReceivableTransactionLineDueDateClose3", Criteria = "ActivationPosting = true", Enabled = false)]
        public DateTime DueDate
        {
            get { return _dueDate; }
            set { SetPropertyValue("DueDate", ref _dueDate, value); }
        }

        [ImmediatePostData()]
        [Appearance("ReceivableTransactionLine2AmountClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public double Amount
        {
            get { return _amount; }
            set { SetPropertyValue("Amount", ref _amount, value); }
        }

        [Browsable(false)]
        [ImmediatePostData()]
        public bool OpenDN
        {
            get { return _openDN; }
            set { SetPropertyValue("OpenDN", ref _openDN, value); }
        }

        [ImmediatePostData()]
        [Appearance("ReceivableTransactionLine2AmountDNClose1", Criteria = "OpenDN = true", Enabled = false)]
        [Appearance("ReceivableTransactionLine2AmountDNClose2", Criteria = "ActivationPosting = true", Enabled = false)]
        public double AmountDN
        {
            get { return _amountDN; }
            set
            {
                SetPropertyValue("AmountDN", ref _amountDN, value);
                if (!IsLoading)
                {
                    SetTotalPlan();
                }
            }
        }

        [Browsable(false)]
        [ImmediatePostData()]
        public bool OpenCN
        {
            get { return _openCN; }
            set { SetPropertyValue("OpenCN", ref _openCN, value); }
        }

        [ImmediatePostData()]
        [Appearance("ReceivableTransactionLine2AmountCNClose1", Criteria = "OpenCN = true", Enabled = false)]
        [Appearance("ReceivableTransactionLine2AmountCNClose2", Criteria = "ActivationPosting = true", Enabled = false)]
        public double AmountCN
        {
            get { return _amountCN; }
            set
            {
                SetPropertyValue("AmountCN", ref _amountCN, value);
                if (!IsLoading)
                {
                    SetTotalPlan();
                }
            }
        }

        [Appearance("ReceivableTransactionLine2PlanClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public double Plan
        {
            get { return _plan; }
            set { SetPropertyValue("Plan", ref _plan, value); }
        }

        [Appearance("ReceivableTransactionLine2OutstandingClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public double Outstanding
        {
            get { return _outstanding; }
            set { SetPropertyValue("Outstanding", ref _outstanding, value); }
        }

        [Appearance("ReceivableTransactionLine2ActualClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public double Actual
        {
            get { return _actual; }
            set { SetPropertyValue("Actual", ref _actual, value); }
        }

        [Appearance("ReceivableTransactionLine2OverPaymentClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public double OverPayment
        {
            get { return _overPayment; }
            set { SetPropertyValue("OverPayment", ref _overPayment, value); }
        }

        [Appearance("ReceivableTransactionLine2SettledClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public bool Settled
        {
            get { return _settled; }
            set { SetPropertyValue("Settled", ref _settled, value); }
        }

        [Appearance("ReceivableTransactionLine2DescriptionClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [Size(512)]
        public string Description
        {
            get { return _description; }
            set { SetPropertyValue("Description", ref _description, value); }
        }

        #region SalesToCostumer

        [Browsable(false)]
        public XPCollection<BusinessPartner> AvailableSalesToCustomer
        {
            get
            {
                if (!IsLoading)
                {
                    _userAccessIn = SecuritySystem.CurrentUserName;
                    string _beginString = null;
                    string _endString = null;
                    string _fullString = null;
                    string _locBusinessPartnerCode = null;
                    UserAccess _locUserAccess = Session.FindObject<UserAccess>(new BinaryOperator("UserName", _userAccessIn));
                    List<string> _stringLocation = new List<string>();
                    if (_locUserAccess != null)
                    {
                        if (this.Company != null)
                        {
                            XPQuery<OrderCollectionMonitoring> _orderCollectionMonitoringsQuery = new XPQuery<OrderCollectionMonitoring>(Session);

                            var _orderCollectionMonitorings = from ocm in _orderCollectionMonitoringsQuery
                                                              where (ocm.Company == this.Company
                                                              && ocm.Status != Status.Close
                                                              && ocm.CollectStatus == CollectStatus.Collect
                                                              && ocm.ReceivableTransaction == null
                                                              )
                                                              group ocm by ocm.Customer into g
                                                              select new { Customer = g.Key };

                            if (_orderCollectionMonitorings != null && _orderCollectionMonitorings.Count() > 0)
                            {
                                foreach (var _orderCollectionMonitoring in _orderCollectionMonitorings)
                                {
                                    if (_orderCollectionMonitoring.Customer != null)
                                    {
                                        if (_orderCollectionMonitoring.Customer.Code != null)
                                        {
                                            _locBusinessPartnerCode = _orderCollectionMonitoring.Customer.Code;
                                            _stringLocation.Add(_locBusinessPartnerCode);
                                        }
                                    }
                                }
                            }

                            IEnumerable<string> _stringArrayLocationFromDistinct = _stringLocation.Distinct();
                            string[] _stringArrayLocationFromList = _stringArrayLocationFromDistinct.ToArray();
                            if (_stringArrayLocationFromList.Length == 1)
                            {
                                for (int i = 0; i < _stringArrayLocationFromList.Length; i++)
                                {
                                    BusinessPartner _locBP = Session.FindObject<BusinessPartner>(new BinaryOperator("Code", _stringArrayLocationFromList[i]));
                                    if (_locBP != null)
                                    {
                                        if (i == 0)
                                        {
                                            _beginString = "[Code]=='" + _locBP.Code + "'";
                                        }
                                    }
                                }
                            }
                            else if (_stringArrayLocationFromList.Length > 1)
                            {
                                for (int i = 0; i < _stringArrayLocationFromList.Length; i++)
                                {
                                    BusinessPartner _locBP = Session.FindObject<BusinessPartner>(new BinaryOperator("Code", _stringArrayLocationFromList[i]));
                                    if (_locBP != null)
                                    {
                                        if (i == 0)
                                        {
                                            _beginString = "[Code]=='" + _locBP.Code + "'";
                                        }
                                        else
                                        {
                                            _endString = _endString + " OR [Code]=='" + _locBP.Code + "'";
                                        }
                                    }
                                }
                            }
                            _fullString = _beginString + _endString;

                            if (_fullString != null)
                            {
                                _availableSalesToCustomer = new XPCollection<BusinessPartner>(Session, CriteriaOperator.Parse(_fullString));
                            }
                        }
                    }
                }

                return _availableSalesToCustomer;

            }
        }

        [ImmediatePostData]
        [DataSourceProperty("AvailableSalesToCustomer", DataSourcePropertyIsNullMode.SelectAll)]
        [DataSourceCriteria("Active = true")]
        [Appearance("ReceivableTransactionLine2SalesToCustomerClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public BusinessPartner SalesToCustomer
        {
            get { return _salesToCustomer; }
            set
            {
                SetPropertyValue("SalesToCustomer", ref _salesToCustomer, value);
                if (!IsLoading)
                {
                    if (_salesToCustomer != null)
                    {
                        this.SalesToContact = _salesToCustomer.Contact;
                        this.SalesToCountry = _salesToCustomer.Country;
                        this.SalesToCity = _salesToCustomer.City;
                        this.SalesToAddress = this.SalesToAddress;
                        this.BillToCustomer = _salesToCustomer;
                    }
                    else
                    {
                        this.SalesToContact = null;
                        this.SalesToCountry = null;
                        this.SalesToCity = null;
                        this.SalesToAddress = null;
                        this.BillToCustomer = null;
                    }
                }
            }
        }

        [VisibleInListView(false)]
        [Appearance("ReceivableTransactionLine2SalesToContactClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public string SalesToContact
        {
            get { return _salesToContact; }
            set { SetPropertyValue("SalesToContact", ref _salesToContact, value); }
        }

        [VisibleInListView(false)]
        [DataSourceCriteria("Active = true")]
        [Appearance("ReceivableTransactionLine2SalesCountryClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Country SalesToCountry
        {
            get { return _salesToCountry; }
            set { SetPropertyValue("SalesToCountry", ref _salesToCountry, value); }
        }

        [VisibleInListView(false)]
        [DataSourceCriteria("Active = true")]
        [Appearance("ReceivableTransactionLine2SalesToCityClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public City SalesToCity
        {
            get { return _salesToCity; }
            set { SetPropertyValue("SalesToCity", ref _salesToCity, value); }
        }

        [VisibleInListView(false)]
        [Appearance("ReceivableTransactionLine2SalesToAddressClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [Size(512)]
        public string SalesToAddress
        {
            get { return _salesToAddress; }
            set { SetPropertyValue("SalesToAddress", ref _salesToAddress, value); }
        }

        #endregion SalesToCostumer

        #region BillToCostumer

        [VisibleInListView(false)]
        [ImmediatePostData()]
        [DataSourceCriteria("Active = true")]
        [Appearance("ReceivableTransactionLine2BillToCostumerClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public BusinessPartner BillToCustomer
        {
            get { return _billToCustomer; }
            set
            {
                SetPropertyValue("BillToCustomer", ref _billToCustomer, value);
                if (!IsLoading)
                {
                    if (this._billToCustomer != null)
                    {
                        this.BillToContact = this._billToCustomer.Contact;
                        this.BillToAddress = this._billToCustomer.Address;
                        this.BillToCountry = this._billToCustomer.Country;
                        this.BillToCity = this._billToCustomer.City;
                    }
                }
            }
        }

        [Appearance("ReceivableTransactionLine2BillToContactClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public string BillToContact
        {
            get { return _billToContact; }
            set { SetPropertyValue("BillToContact", ref _billToContact, value); }
        }

        [VisibleInListView(false)]
        [DataSourceCriteria("Active = true")]
        [Appearance("ReceivableTransactionLine2BillCountryClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Country BillToCountry
        {
            get { return _billToCountry; }
            set { SetPropertyValue("BillToCountry", ref _billToCountry, value); }
        }

        [VisibleInListView(false)]
        [DataSourceCriteria("Active = true")]
        [Appearance("ReceivableTransactionLine2BillToCityClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public City BillToCity
        {
            get { return _billToCity; }
            set { SetPropertyValue("BillToCity", ref _billToCity, value); }
        }

        [VisibleInListView(false)]
        [Appearance("ReceivableTransactionLine2BillToAddressClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [Size(512)]
        public string BillToAddress
        {
            get { return _billToAddress; }
            set { SetPropertyValue("BillToAddress", ref _billToAddress, value); }
        }

        #endregion BillToCostumer

        #region Bank

        [ImmediatePostData()]
        [Appearance("ReceivableTransactionLine2MultiBankClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public bool MultiBank
        {
            get { return _multiBank; }
            set
            {
                SetPropertyValue("MultiBank", ref _multiBank, value);
                if (!IsLoading)
                {
                    if (this._multiBank == true)
                    {
                        this.BankBasedInvoice = false;
                        this.CompanyBankAccount = null;
                        this.CompanyAccountNo = null;
                        this.CompanyAccountName = null;
                        this.BankAccount = null;
                        this.AccountNo = null;
                        this.AccountName = null;
                    }
                }
            }
        }

        [ImmediatePostData()]
        [Appearance("ReceivableTransactionLine2BankBasedInvoiceClose", Criteria = "MultiBank = true", Enabled = false)]
        [Appearance("ReceivableTransactionLine2BankBasedInvoiceClose2", Criteria = "ActivationPosting = true", Enabled = false)]
        public bool BankBasedInvoice
        {
            get { return _bankBasedInvoice; }
            set
            {
                SetPropertyValue("BankBasedInvoice", ref _bankBasedInvoice, value);
                if (!IsLoading)
                {
                    if (this._bankBasedInvoice == true)
                    {
                        if (this.SalesInvoice != null)
                        {
                            if (this.SalesInvoice.CompanyBankAccount != null) { this.CompanyBankAccount = this.SalesInvoice.CompanyBankAccount; }
                            if (this.SalesInvoice.CompanyAccountName != null) { this.CompanyAccountName = this.SalesInvoice.CompanyAccountName; }
                            if (this.SalesInvoice.CompanyAccountNo != null) { this.CompanyAccountNo = this.SalesInvoice.CompanyAccountNo; }
                            if (this.SalesInvoice.BankAccount != null) { this.BankAccount = this.SalesInvoice.BankAccount; }
                            if (this.SalesInvoice.AccountName != null) { this.AccountName = this.SalesInvoice.AccountName; }
                            if (this.SalesInvoice.AccountNo != null) { this.CompanyAccountNo = this.SalesInvoice.AccountNo; }
                        }
                    }
                    else
                    {
                        this.CompanyBankAccount = null;
                        this.CompanyAccountName = null;
                        this.CompanyAccountNo = null;
                        this.BankAccount = null;
                        this.AccountName = null;
                        this.CompanyAccountNo = null;
                    }
                }
            }
        }

        [Browsable(false)]
        public XPCollection<BankAccount> AvailableCompanyBankAccount
        {
            get
            {
                if (!IsLoading)
                {
                    if (this.Company != null)
                    {
                        if (this.Workplace != null)
                        {
                            _availableCompanyBankAccount = new XPCollection<BankAccount>(Session, new GroupOperator
                                                    (GroupOperatorType.And,
                                                    new BinaryOperator("Company", this.Company),
                                                    new BinaryOperator("Workplace", this.Workplace),
                                                    new BinaryOperator("Active", true)));
                        }
                        else
                        {
                            _availableCompanyBankAccount = new XPCollection<BankAccount>(Session, new GroupOperator
                                                    (GroupOperatorType.And,
                                                    new BinaryOperator("Company", this.Company),
                                                    new BinaryOperator("Active", true)));
                        }
                    }
                    else
                    {
                        _availableCompanyBankAccount = new XPCollection<BankAccount>(Session,
                            new GroupOperator(GroupOperatorType.And,
                            new BinaryOperator("Active", true)));
                    }
                }

                return _availableCompanyBankAccount;

            }
        }

        [ImmediatePostData()]
        [DataSourceProperty("AvailableCompanyBankAccount")]
        [Appearance("ReceivableTransactionLine2CompanyBankAccountClose", Criteria = "MultiBank = true", Enabled = false)]
        [Appearance("ReceivableTransactionLine2CompanyBankAccountClose2", Criteria = "ActivationPosting = true", Enabled = false)]
        public BankAccount CompanyBankAccount
        {
            get { return _companyBankAccount; }
            set
            {
                SetPropertyValue("CompanyBankAccount", ref _companyBankAccount, value);
                if (!IsLoading)
                {
                    if (this._companyBankAccount != null)
                    {
                        this.CompanyAccountNo = this._companyBankAccount.AccountNo;
                        this.CompanyAccountName = this._companyBankAccount.AccountName;
                    }
                }
            }
        }

        [Appearance("ReceivableTransactionLine2CompanyAccountNoClose", Criteria = "MultiBank = true", Enabled = false)]
        [Appearance("ReceivableTransactionLine2CompanyAccountNoClose2", Criteria = "ActivationPosting = true", Enabled = false)]
        public string CompanyAccountNo
        {
            get { return _companyAccountNo; }
            set { SetPropertyValue("CompanyAccountNo", ref _companyAccountNo, value); }
        }

        [Appearance("ReceivableTransactionLine2CompanyAccountNameClose", Criteria = "MultiBank = true", Enabled = false)]
        [Appearance("ReceivableTransactionLine2CompanyAccountNameClose2", Criteria = "ActivationPosting = true", Enabled = false)]
        public string CompanyAccountName
        {
            get { return _companyAccountName; }
            set { SetPropertyValue("CompanyAccountName", ref _companyAccountName, value); }
        }

        [Browsable(false)]
        public XPCollection<BankAccount> AvailableBankAccount
        {
            get
            {
                if (this.Company != null && this.Workplace != null)
                {
                    if (this.SalesInvoice != null)
                    {
                        if (this.SalesInvoice.BillToCustomer != null)
                        {
                            _availableBankAccount = new XPCollection<BankAccount>(Session,
                                               new GroupOperator(GroupOperatorType.And,
                                               new BinaryOperator("Company", this.Company),
                                               new BinaryOperator("Workplace", this.Workplace),
                                               new BinaryOperator("BusinessPartner", this.SalesInvoice.BillToCustomer),
                                               new BinaryOperator("Active", true)));
                        }
                    }
                }
                else
                {

                    _availableBankAccount = new XPCollection<BankAccount>(Session,
                                            new GroupOperator(GroupOperatorType.And,
                                            new BinaryOperator("Active", true)));

                }
                return _availableBankAccount;

            }
        }

        [ImmediatePostData()]
        [Appearance("ReceivableTransactionLine2BankAccountClose", Criteria = "MultiBank = true", Enabled = false)]
        [Appearance("ReceivableTransactionLine2BankAccountClose2", Criteria = "ActivationPosting = true", Enabled = false)]
        [DataSourceProperty("AvailableBankAccount")]
        public BankAccount BankAccount
        {
            get { return _bankAccount; }
            set
            {
                SetPropertyValue("BankAccount", ref _bankAccount, value);
                if (!IsLoading)
                {
                    if (this._bankAccount != null)
                    {
                        this.AccountNo = this._bankAccount.AccountNo;
                        this.AccountName = this._bankAccount.AccountName;
                    }
                }
            }
        }

        [Appearance("ReceivableTransactionLine2AccountNoClose", Criteria = "MultiBank = true", Enabled = false)]
        [Appearance("ReceivableTransactionLine2AccountNoClose2", Criteria = "ActivationPosting = true", Enabled = false)]
        public string AccountNo
        {
            get { return _accountNo; }
            set { SetPropertyValue("AccountNo", ref _accountNo, value); }
        }

        [Appearance("ReceivableTransactionLine2AccountNameClose", Criteria = "MultiBank = true", Enabled = false)]
        [Appearance("ReceivableTransactionLine2AccountNameClose2", Criteria = "ActivationPosting = true", Enabled = false)]
        public string AccountName
        {
            get { return _accountName; }
            set { SetPropertyValue("AccountName", ref _accountName, value); }
        }

        #endregion Bank

        [Browsable(false)]
        [ImmediatePostData()]
        [ModelDefault("DisplayFormat", "{0:dd/MM/yyyy}")]
        [ModelDefault("EditMask", "dd/MM/yyyy")]
        [Appearance("ReceivableTransactionLine2DocDateClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public DateTime DocDate
        {
            get { return _docDate; }
            set
            {
                SetPropertyValue("DocDate", ref _docDate, value);
                if (!IsLoading)
                {
                    this.JournalMonth = this._docDate.Month;
                    this.JournalYear = this._docDate.Year;
                }
            }
        }

        [Browsable(false)]
        public int JournalMonth
        {
            get { return _journalMonth; }
            set { SetPropertyValue("JournalMonth", ref _journalMonth, value); }
        }

        [Browsable(false)]
        public int JournalYear
        {
            get { return _journalYear; }
            set { SetPropertyValue("JournalYear", ref _journalYear, value); }
        }

        [Appearance("ReceivableTransactionLine2StatusClose", Enabled = false)]
        public Status Status
        {
            get { return _status; }
            set { SetPropertyValue("Status", ref _status, value); }
        }

        [ModelDefault("DisplayFormat", "{0:dd/MM/yyyy}")]
        [ModelDefault("EditMask", "dd/MM/yyyy")]
        [Appearance("ReceivableTransactionLine2StatusDateClose", Enabled = false)]
        public DateTime StatusDate
        {
            get { return _statusdate; }
            set { SetPropertyValue("StatusDate", ref _statusdate, value); }
        }

        [VisibleInListView(false)]
        [Appearance("ReceivableTransactionLine2PostedCountEnabled", Enabled = false)]
        public int PostedCount
        {
            get { return _postedCount; }
            set { SetPropertyValue("PostedCount", ref _postedCount, value); }
        }

        [VisibleInListView(false)]
        [Appearance("ReceivableTransactionLine2SignCodeClose", Enabled = false)]
        public string SignCode
        {
            get { return _signCode; }
            set { SetPropertyValue("SignCode", ref _signCode, value); }
        }

        [Browsable(false)]
        public OrderCollectionMonitoring OrderCollectionMonitoring
        {
            get { return _orderCollectionMonitoring; }
            set { SetPropertyValue("OrderCollectionMonitoring", ref _orderCollectionMonitoring, value); }
        }

        [VisibleInListView(false)]
        [ImmediatePostData()]
        [ModelDefault("DisplayFormat", "{0:dd/MM/yyyy}")]
        [ModelDefault("EditMask", "dd/MM/yyyy")]
        [Appearance("ReceivableTransactionLine2PickingDateClose", Enabled = false)]
        public DateTime PickingDate
        {
            get { return _pickingDate; }
            set { SetPropertyValue("PickingDate", ref _pickingDate, value); }

        }

        [VisibleInListView(false)]
        [Appearance("ReceivableTransactionLine2PickingClose", Enabled = false)]
        public Picking Picking
        {
            get { return _picking; }
            set { SetPropertyValue("Picking", ref _picking, value); }
        }

        [VisibleInListView(false)]
        [Appearance("ReceivableTransactionLine2PickingMonitoringClose", Enabled = false)]
        public PickingMonitoring PickingMonitoring
        {
            get { return _pickingMonitoring; }
            set { SetPropertyValue("PickingMonitoring", ref _pickingMonitoring, value); }
        }

        [Browsable(false)]
        [Appearance("ReceivableTransactionLine2SalesInvoiceMonitoringClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public SalesInvoiceMonitoring SalesInvoiceMonitoring
        {
            get { return _salesInvoiceMonitoring; }
            set { SetPropertyValue("SalesInvoiceMonitoring", ref _salesInvoiceMonitoring, value); }
        }

        [ImmediatePostData()]
        [RuleRequiredField(DefaultContexts.Save)]
        [Appearance("ReceivableTransactionLine2ReceivableTransaction2Close", Enabled = false)]
        [Association("ReceivableTransaction2-ReceivableTransactionLine2s")]
        public ReceivableTransaction2 ReceivableTransaction2
        {
            get { return _receivableTransaction2; }
            set
            {
                SetPropertyValue("ReceivableTransaction", ref _receivableTransaction2, value);
                if (!IsLoading)
                {
                    if (this._receivableTransaction2 != null)
                    {
                        if (this._receivableTransaction2.Company != null) { this.Company = this._receivableTransaction2.Company; }
                        if (this._receivableTransaction2.Workplace != null) { this.Workplace = this._receivableTransaction2.Workplace; }
                        if (this._receivableTransaction2.Division != null) { this.Division = this._receivableTransaction2.Division; }
                        if (this._receivableTransaction2.Department != null) { this.Department = this._receivableTransaction2.Department; }
                        if (this._receivableTransaction2.Section != null) { this.Section = this._receivableTransaction2.Section; }
                        if (this._receivableTransaction2.Employee != null) { this.Employee = this._receivableTransaction2.Employee; }
                    }
                }
            }
        }

        [Association("ReceivableTransactionLine2-ReceivableTransactionBanks")]
        public XPCollection<ReceivableTransactionBank> ReceivableTransactionBanks
        {
            get { return GetCollection<ReceivableTransactionBank>("ReceivableTransactionBanks"); }
        }

        //AuditTrail
        [CollectionOperationSet(AllowAdd = false, AllowRemove = true)]
        public XPCollection<AuditDataItemPersistent> ChangeHistory
        {
            get
            {
                if (changeHistory == null)
                {
                    changeHistory = AuditedObjectWeakReference.GetAuditTrail(Session, this);
                }
                return changeHistory;
            }
        }

        #endregion Field

        //================================================== Code Only ==================================================

        #region No

        public void UpdateNo()
        {
            try
            {
                if (!IsLoading && Session.IsNewObject(this))
                {
                    if (this.ReceivableTransaction2 != null)
                    {
                        object _makRecord = Session.Evaluate<ReceivableTransactionLine2>(CriteriaOperator.Parse("Max(No)"), CriteriaOperator.Parse("ReceivableTransaction2=?", this.ReceivableTransaction2));
                        this.No = Convert.ToInt32(_makRecord) + 1;
                        this.Save();
                        RecoveryUpdateNo();
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = ReceivableTransactionLine2 " + ex.ToString());
            }
        }

        public void RecoveryUpdateNo()
        {
            try
            {
                if (this.ReceivableTransaction2 != null)
                {
                    ReceivableTransaction2 _numHeader = Session.FindObject<ReceivableTransaction2>
                                            (new BinaryOperator("Code", this.ReceivableTransaction2.Code));

                    XPCollection<ReceivableTransactionLine2> _numLines = new XPCollection<ReceivableTransactionLine2>
                                                             (Session, new BinaryOperator("ReceivableTransaction2", _numHeader),
                                                              new SortProperty("No", DevExpress.Xpo.DB.SortingDirection.Ascending));
                    if (_numLines != null)
                    {
                        int i1 = 0;
                        foreach (ReceivableTransactionLine2 _numLine in _numLines)
                        {
                            i1 += 1;
                            _numLine.No = i1;
                            _numLine.Save();
                        }
                        i1 = 1;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = ReceivableTransactionLine2 " + ex.ToString());
            }
        }

        public void RecoveryDeleteNo()
        {
            try
            {
                if (this.ReceivableTransaction2 != null)
                {
                    ReceivableTransaction2 _numHeader = Session.FindObject<ReceivableTransaction2>
                                            (new BinaryOperator("Code", this.ReceivableTransaction2.Code));

                    XPCollection<ReceivableTransactionLine2> _numLines = new XPCollection<ReceivableTransactionLine2>
                                                             (Session, new GroupOperator(GroupOperatorType.And,
                                                              new BinaryOperator("This", this, BinaryOperatorType.NotEqual),
                                                              new BinaryOperator("ReceivableTransaction2", _numHeader)),
                                                              new SortProperty("No", DevExpress.Xpo.DB.SortingDirection.Ascending));
                    if (_numLines != null)
                    {
                        int i = 0;
                        foreach (ReceivableTransactionLine2 _numLine in _numLines)
                        {
                            i += 1;
                            _numLine.No = i;
                            _numLine.Save();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = ReceivableTransactionLine2 " + ex.ToString());
            }
        }

        #endregion No

        private void SetVehicle(Employee _locPIC)
        {
            try
            {
                DateTime now = DateTime.Now;
                if (this.Company != null && this.Workplace != null)
                {
                    SalesmanVehicleSetup _locSalesmanVehicleSetup = Session.FindObject<SalesmanVehicleSetup>
                                                        (new GroupOperator(GroupOperatorType.And,
                                                        new BinaryOperator("Company", this.Company),
                                                        new BinaryOperator("Workplace", this.Workplace),
                                                        new BinaryOperator("Salesman", _locPIC),
                                                        new BinaryOperator("Active", true)));
                    if (_locSalesmanVehicleSetup != null)
                    {
                        if (_locSalesmanVehicleSetup.Vehicle != null)
                        {
                            this.Vehicle = _locSalesmanVehicleSetup.Vehicle;
                        }
                        else
                        {
                            this.Vehicle = null;
                        }
                    }
                    else
                    {
                        this.Vehicle = null;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Business Object = ReceivableTransactionLine2 ", ex.ToString());
            }
        }

        private void SetTotalPlan()
        {
            try
            {
                _globFunc = new GlobalFunction();

                if (_amount >= 0 || _amountCN >= 0 || _amountDN >= 0)
                {
                    if (_globFunc.GetRoundingList(Session, ObjectList.ReceivableTransactionLine2, FieldName.TAmount) == true)
                    {
                        this.Plan = _globFunc.GetRoundUp(Session, ((this.Amount + _amountDN) - this.AmountCN), ObjectList.ReceivableTransactionLine2, FieldName.TAmount);
                    }
                    else
                    {
                        this.Plan = (this.Amount + _amountDN) - this.AmountCN;
                    }
                }

            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = ReceivableTransactionLine2 " + ex.ToString());
            }
        }

    }
}