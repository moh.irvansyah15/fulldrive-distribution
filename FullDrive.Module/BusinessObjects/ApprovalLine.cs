﻿using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;

using DevExpress.ExpressApp.Editors;
using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.ConditionalAppearance;

namespace FullDrive.Module.BusinessObjects
{
    [DefaultClassOptions]
    [DefaultProperty("Code")]
    [NavigationItem("Master")]
    [RuleCombinationOfPropertiesIsUnique("ApprovalLineRuleUnique", DefaultContexts.Save, "Code")]
    //[ImageName("BO_Contact")]
    //[DefaultProperty("DisplayMemberNameForLookupEditorsOfThisType")]
    //[DefaultListViewOptions(MasterDetailMode.ListViewOnly, false, NewItemRowPosition.None)]
    //[Persistent("DatabaseTableName")]
    // Specify more UI options using a declarative approach (https://documentation.devexpress.com/#eXpressAppFramework/CustomDocument112701).
    public class ApprovalLine : BaseObject
    { // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).
        private string _code;
        private Company _company;
        private XPCollection<Workplace> _availableWorkplace;
        private Workplace _workplace;
        private ApprovalLevel _approvalLevel;
        private Status _approvalStatus;
        private DateTime _approvalDate;
        private bool _endApproval;
        private Employee _approvedBy;
        private string _userAccess;
        private PurchaseRequisition _purchaseRequisition;
        private PurchaseOrder _purchaseOrder;
        //private Production _production;
        //private SalesQuotation _salesQuotation;
        private SalesOrder _salesOrder;
        //private TransferOrder _transferOrder;
        //private FinishProduction _finishProduction;
        //private ItemConsumption _itemConsumption;
        //private InventoryTransferOrder _inventoryTransferOrder;
        //private WorkRequisition _workRequisition;
        //private WorkOrder _workOrder;
        private CashAdvance _cashAdvance;
        private PaymentRealization _paymentRealization;
        //private MaterialRequisition _materialRequisition;
        private PurchaseInvoice _purchaseInvoice;
        private PurchasePrePaymentInvoice _purchasePrePaymentInvoice;
        //private PurchaseReturn _purchaseReturn;
        //private CreditMemo _creditMemo;
        private SalesInvoice _salesInvoice;
        //private SalesPrePaymentInvoice _salesPrePaymentInvoice;
        //private SalesReturn _salesReturn;
        //private DebitMemo _debitMemo;
        private GlobalFunction _globFunc;
        //AuditTrail
        private XPCollection<AuditDataItemPersistent> changeHistory;

        public ApprovalLine(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if (!IsLoading)
            {
                _globFunc = new GlobalFunction();
                #region UserAccess
                _userAccess = SecuritySystem.CurrentUserName;
                UserAccess _locUserAccess = Session.FindObject<UserAccess>(new BinaryOperator("UserName", _userAccess));

                if (Session.IsNewObject(this))
                {
                    if (_locUserAccess != null)
                    {
                        if (_locUserAccess.Employee != null)
                        {
                            if (_locUserAccess.Employee != null)
                            {
                                if (_locUserAccess.Employee.Company != null && _locUserAccess.Employee.Workplace != null)
                                {
                                    this.Code = _globFunc.GetNumberingUnlockOptimisticRecordBasedOrganization(this.Session.DataLayer, ObjectList.ApprovalLine, _locUserAccess.Employee.Company, _locUserAccess.Employee.Workplace);
                                }
                                else
                                {
                                    this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.ApprovalLine);
                                }
                                if (_locUserAccess.Employee.Company != null)
                                {
                                    Company = _locUserAccess.Employee.Company;
                                }
                                if (_locUserAccess.Employee.Workplace != null)
                                {
                                    Workplace = _locUserAccess.Employee.Workplace;
                                }
                                else
                                {
                                    this.Workplace = _globFunc.GetDefaultWorkplace(this.Session, _locUserAccess);
                                }
                                ApprovedBy = _locUserAccess.Employee;
                            }
                        }
                    }
                }
                #endregion UserAccess
            }


            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).
        }

        [RuleRequiredField(DefaultContexts.Save)]
        [Appearance("ApprovalLineCodeEnabled", Enabled = false)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue("Code", ref _code, value); }
        }

        [ImmediatePostData()]
        [Appearance("ApprovalLineCompanyEnabled", Enabled = false)]
        [RuleRequiredField(DefaultContexts.Save)]
        [DataSourceCriteria("Active = true")]
        public Company Company
        {
            get { return _company; }
            set { SetPropertyValue("Company", ref _company, value); }
        }

        [Browsable(false)]
        public XPCollection<Workplace> AvailableWorkplace
        {
            get
            {
                if (!IsLoading)
                {
                    if (this.Company != null)
                    {
                        _userAccess = SecuritySystem.CurrentUserName;
                        UserAccess _locUserAccess = Session.FindObject<UserAccess>(new BinaryOperator("UserName", _userAccess));
                        if (_locUserAccess != null)
                        {
                            string _beginString = null;
                            string _endString = null;
                            string _fullString = null;
                            List<string> _stringWorkplace = new List<string>();

                            XPCollection<OrganizationSetupDetail> _locOrganizationSetupDetails = new XPCollection<OrganizationSetupDetail>
                                                                            (Session, new GroupOperator(GroupOperatorType.And,
                                                                            new BinaryOperator("Company", this.Company),
                                                                            new BinaryOperator("UserAccess", _locUserAccess),
                                                                            new BinaryOperator("OpenWorkplace", true),
                                                                            new BinaryOperator("Active", true)));

                            if (_locOrganizationSetupDetails != null && _locOrganizationSetupDetails.Count() > 0)
                            {
                                foreach (OrganizationSetupDetail _locOrganizationSetupDetail in _locOrganizationSetupDetails)
                                {
                                    if (_locOrganizationSetupDetail.Workplace != null)
                                    {
                                        if (_locOrganizationSetupDetail.Workplace.Code != null)
                                        {
                                            _stringWorkplace.Add(_locOrganizationSetupDetail.Workplace.Code);
                                        }
                                    }
                                }

                                #region Workplace
                                IEnumerable<string> _stringArrayWorkplaceDistinct = _stringWorkplace.Distinct();
                                string[] _stringArrayWorkplaceList = _stringArrayWorkplaceDistinct.ToArray();
                                if (_stringArrayWorkplaceList.Length == 1)
                                {
                                    for (int i = 0; i < _stringArrayWorkplaceList.Length; i++)
                                    {
                                        Workplace _locWorkplace = Session.FindObject<Workplace>(new BinaryOperator("Code", _stringArrayWorkplaceList[i]));
                                        if (_locWorkplace != null)
                                        {
                                            if (i == 0)
                                            {
                                                _beginString = "[Code]=='" + _locWorkplace.Code + "'";
                                            }
                                        }
                                    }
                                }
                                else if (_stringArrayWorkplaceList.Length > 1)
                                {
                                    for (int i = 0; i < _stringArrayWorkplaceList.Length; i++)
                                    {
                                        Workplace _locWorkplace = Session.FindObject<Workplace>(new BinaryOperator("Code", _stringArrayWorkplaceList[i]));
                                        if (_locWorkplace != null)
                                        {
                                            if (i == 0)
                                            {
                                                _beginString = "[Code]=='" + _locWorkplace.Code + "'";
                                            }
                                            else
                                            {
                                                _endString = _endString + " OR [Code]=='" + _locWorkplace.Code + "'";
                                            }
                                        }

                                    }
                                }
                                _fullString = _beginString + _endString;


                                if (_fullString != null)
                                {
                                    _availableWorkplace = new XPCollection<Workplace>(Session, CriteriaOperator.Parse(_fullString));
                                }

                                #endregion Workplace
                            }
                            else
                            {
                                OrganizationSetupDetail _locOrgSetDetail = Session.FindObject<OrganizationSetupDetail>
                                                                            (new GroupOperator(GroupOperatorType.And,
                                                                            new BinaryOperator("Company", this.Company),
                                                                            new BinaryOperator("UserAccess", _locUserAccess),
                                                                            new BinaryOperator("AllWorkplace", true),
                                                                            new BinaryOperator("Active", true)));
                                if (_locOrgSetDetail != null)
                                {
                                    _availableWorkplace = new XPCollection<Workplace>(Session,
                                                        new GroupOperator(GroupOperatorType.And,
                                                        new BinaryOperator("Company", this.Company),
                                                        new BinaryOperator("Active", true)));
                                }
                            }
                        }
                    }
                }

                return _availableWorkplace;

            }
        }

        [ImmediatePostData()]
        [Appearance("ApprovalLineWorkplaceEnabled", Enabled = false)]
        [DataSourceProperty("AvailableWorkplace")]
        public Workplace Workplace
        {
            get { return _workplace; }
            set { SetPropertyValue("Workplace", ref _workplace, value); }
        }

        [Appearance("ApprovalLineApprovalLevelEnabled", Enabled = false)]
        public ApprovalLevel ApprovalLevel
        {
            get { return _approvalLevel; }
            set { SetPropertyValue("ApprovalLevel", ref _approvalLevel, value); }
        }

        [Appearance("ApprovalLineApprovalStatusEnabled", Enabled = false)]
        public Status ApprovalStatus
        {
            get { return _approvalStatus; }
            set { SetPropertyValue("ApprovalStatus", ref _approvalStatus, value); }
        }

        [ModelDefault("DisplayFormat", "{0:dd/MM/yyyy}")]
        [ModelDefault("EditMask", "dd/MM/yyyy")]
        [Appearance("ApprovalLineApprovalDateEnabled", Enabled = false)]
        public DateTime ApprovalDate
        {
            get { return _approvalDate; }
            set { SetPropertyValue("ApprovalDate", ref _approvalDate, value); }
        }

        [Appearance("ApprovalLineEndApprovalEnabled", Enabled = false)]
        public bool EndApproval
        {
            get { return _endApproval; }
            set { SetPropertyValue("EndApproval", ref _endApproval, value); }
        }

        [Appearance("ApprovalLineApprovedByEnabled", Enabled = false)]
        public Employee ApprovedBy
        {
            get { return _approvedBy; }
            set { SetPropertyValue("ApprovedBy", ref _approvedBy, value); }
        }

        [VisibleInListView(false)]
        [Appearance("ApprovalLinePurchaseRequisitionEnabled", Enabled = false)]
        [Association("PurchaseRequisition-ApprovalLines")]
        public PurchaseRequisition PurchaseRequisition
        {
            get { return _purchaseRequisition; }
            set { SetPropertyValue("PurchaseRequisition", ref _purchaseRequisition, value); }
        }

        [VisibleInListView(false)]
        [Appearance("ApprovalLinePurchaseOrderEnabled", Enabled = false)]
        [Association("PurchaseOrder-ApprovalLines")]
        public PurchaseOrder PurchaseOrder
        {
            get { return _purchaseOrder; }
            set { SetPropertyValue("PurchaseOrder", ref _purchaseOrder, value); }
        }

        //[Appearance("ApprovalLineProductionEnabled", Enabled = false)]
        //[Association("Production-ApprovalLines")]
        //public Production Production
        //{
        //    get { return _production; }
        //    set { SetPropertyValue("Production", ref _production, value); }
        //}

        //[Appearance("ApprovalLineSalesQuotationEnabled", Enabled = false)]
        //[Association("SalesQuotation-ApprovalLines")]
        //public SalesQuotation SalesQuotation
        //{
        //    get { return _salesQuotation; }
        //    set { SetPropertyValue("SalesQuotation", ref _salesQuotation, value); }
        //}

        [VisibleInListView(false)]
        [Appearance("ApprovalLineSalesOrderEnabled", Enabled = false)]
        [Association("SalesOrder-ApprovalLines")]
        public SalesOrder SalesOrder
        {
            get { return _salesOrder; }
            set { SetPropertyValue("SalesOrder", ref _salesOrder, value); }
        }

        //[Appearance("ApprovalLineTransferOrderEnabled", Enabled = false)]
        //[Association("TransferOrder-ApprovalLines")]
        //public TransferOrder TransferOrder
        //{
        //    get { return _transferOrder; }
        //    set { SetPropertyValue("TransferOrder", ref _transferOrder, value); }
        //}

        //[Appearance("ApprovalLineFinishProductionEnabled", Enabled = false)]
        //[Association("FinishProduction-ApprovalLines")]
        //public FinishProduction FinishProduction
        //{
        //    get { return _finishProduction; }
        //    set { SetPropertyValue("FinishProduction", ref _finishProduction, value); }
        //}

        //[Appearance("ApprovalLineItemConsumptionEnabled", Enabled = false)]
        //[Association("ItemConsumption-ApprovalLines")]
        //public ItemConsumption ItemConsumption
        //{
        //    get { return _itemConsumption; }
        //    set { SetPropertyValue("ItemConsumption", ref _itemConsumption, value); }
        //}

        //[Appearance("ApprovalLineInventoryTransferOrderEnabled", Enabled = false)]
        //[Association("InventoryTransferOrder-ApprovalLines")]
        //public InventoryTransferOrder InventoryTransferOrder
        //{
        //    get { return _inventoryTransferOrder; }
        //    set { SetPropertyValue("InventoryTransferOrder", ref _inventoryTransferOrder, value); }
        //}

        //[Appearance("ApprovalLineWorkRequisitionEnabled", Enabled = false)]
        //[Association("WorkRequisition-ApprovalLines")]
        //public WorkRequisition WorkRequisition
        //{
        //    get { return _workRequisition; }
        //    set { SetPropertyValue("WorkRequisition", ref _workRequisition, value); }
        //}

        //[Appearance("ApprovalLineWorkOrderEnabled", Enabled = false)]
        //[Association("WorkOrder-ApprovalLines")]
        //public WorkOrder WorkOrder
        //{
        //    get { return _workOrder; }
        //    set { SetPropertyValue("WorkOrder", ref _workOrder, value); }
        //}

        [VisibleInListView(false)]
        [Appearance("ApprovalLinesCashAdvanceEnabled", Enabled = false)]
        [Association("CashAdvance-ApprovalLines")]
        public CashAdvance CashAdvance
        {
            get { return _cashAdvance; }
            set { SetPropertyValue("CashAdvance", ref _cashAdvance, value); }
        }

        [VisibleInListView(false)]
        [Appearance("ApprovalLinePaymentRealizationEnabled", Enabled = false)]
        [Association("PaymentRealization-ApprovalLines")]
        public PaymentRealization PaymentRealization
        {
            get { return _paymentRealization; }
            set { SetPropertyValue("PaymentRealization", ref _paymentRealization, value); }
        }

        //[Appearance("ApprovalLineMaterialRequisitionEnabled", Enabled = false)]
        //[Association("MaterialRequisition-ApprovalLines")]
        //public MaterialRequisition MaterialRequisition
        //{
        //    get { return _materialRequisition; }
        //    set { SetPropertyValue("MaterialRequisition", ref _materialRequisition, value); }
        //}

        [VisibleInListView(false)]
        [Appearance("ApprovalLinesPurchaseInvoiceEnabled", Enabled = false)]
        [Association("PurchaseInvoice-ApprovalLines")]
        public PurchaseInvoice PurchaseInvoice
        {
            get { return _purchaseInvoice; }
            set { SetPropertyValue("PurchaseInvoice", ref _purchaseInvoice, value); }
        }

        [VisibleInListView(false)]
        [Appearance("ApprovalLinesPurchasePrePaymentInvoiceEnabled", Enabled = false)]
        [Association("PurchasePrePaymentInvoice-ApprovalLines")]
        public PurchasePrePaymentInvoice PurchasePrePaymentInvoice
        {
            get { return _purchasePrePaymentInvoice; }
            set { SetPropertyValue("PurchasePrePaymentInvoice", ref _purchasePrePaymentInvoice, value); }
        }

        //[Appearance("ApprovalLinesPurchaseReturnEnabled", Enabled = false)]
        //[Association("PurchaseReturn-ApprovalLines")]
        //public PurchaseReturn PurchaseReturn
        //{
        //    get { return _purchaseReturn; }
        //    set { SetPropertyValue("PurchaseReturn", ref _purchaseReturn, value); }
        //}

        //[Appearance("ApprovalLinesCreditMemoEnabled", Enabled = false)]
        //[Association("CreditMemo-ApprovalLines")]
        //public CreditMemo CreditMemo
        //{
        //    get { return _creditMemo; }
        //    set { SetPropertyValue("CreditMemo", ref _creditMemo, value); }
        //}

        [VisibleInListView(false)]
        [Appearance("ApprovalLinesSalesInvoiceEnabled", Enabled = false)]
        [Association("SalesInvoice-ApprovalLines")]
        public SalesInvoice SalesInvoice
        {
            get { return _salesInvoice; }
            set { SetPropertyValue("SalesInvoice", ref _salesInvoice, value); }
        }

        //[Appearance("ApprovalLinesSalesPrePaymentInvoiceEnabled", Enabled = false)]
        //[Association("SalesPrePaymentInvoice-ApprovalLines")]
        //public SalesPrePaymentInvoice SalesPrePaymentInvoice
        //{
        //    get { return _salesPrePaymentInvoice; }
        //    set { SetPropertyValue("SalesPrePaymentInvoice", ref _salesPrePaymentInvoice, value); }
        //}

        //[Appearance("ApprovalLinesSalesReturnEnabled", Enabled = false)]
        //[Association("SalesReturn-ApprovalLines")]
        //public SalesReturn SalesReturn
        //{
        //    get { return _salesReturn; }
        //    set { SetPropertyValue("SalesReturn", ref _salesReturn, value); }
        //}

        //[Appearance("ApprovalLinesDebitMemoEnabled", Enabled = false)]
        //[Association("DebitMemo-ApprovalLines")]
        //public DebitMemo DebitMemo
        //{
        //    get { return _debitMemo; }
        //    set { SetPropertyValue("DebitMemo", ref _debitMemo, value); }
        //}

        //AuditTrail
        [CollectionOperationSet(AllowAdd = false, AllowRemove = true)]
        public XPCollection<AuditDataItemPersistent> ChangeHistory
        {
            get
            {
                if (changeHistory == null)
                {
                    changeHistory = AuditedObjectWeakReference.GetAuditTrail(Session, this);
                }
                return changeHistory;
            }
        }

        //private string _PersistentProperty;
        //[XafDisplayName("My display name"), ToolTip("My hint message")]
        //[ModelDefault("EditMask", "(000)-00"), Index(0), VisibleInListView(false)]
        //[Persistent("DatabaseColumnName"), RuleRequiredField(DefaultContexts.Save)]
        //public string PersistentProperty {
        //    get { return _PersistentProperty; }
        //    set { SetPropertyValue("PersistentProperty", ref _PersistentProperty, value); }
        //}

        //[Action(Caption = "My UI Action", ConfirmationMessage = "Are you sure?", ImageName = "Attention", AutoCommit = true)]
        //public void ActionMethod() {
        //    // Trigger a custom business logic for the current record in the UI (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112619.aspx).
        //    this.PersistentProperty = "Paid";
        //}
    }
}