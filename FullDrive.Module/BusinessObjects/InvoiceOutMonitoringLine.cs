﻿using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;

using DevExpress.ExpressApp.ConditionalAppearance;
using DevExpress.ExpressApp.SystemModule;
using FullDrive.Module.CustomProcess;

namespace FullDrive.Module.BusinessObjects
{
    [DefaultClassOptions]
    [DefaultProperty("Code")]
    [NavigationItem("Task")]
    [RuleCombinationOfPropertiesIsUnique("InvoiceOutMonitoringLineRuleUnique", DefaultContexts.Save, "Code")]
    //[ImageName("BO_Contact")]
    //[DefaultProperty("DisplayMemberNameForLookupEditorsOfThisType")]
    //[DefaultListViewOptions(MasterDetailMode.ListViewOnly, false, NewItemRowPosition.None)]
    //[Persistent("DatabaseTableName")]
    // Specify more UI options using a declarative approach (https://documentation.devexpress.com/#eXpressAppFramework/CustomDocument112701).
    public class InvoiceOutMonitoringLine : BaseObject
    { // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).

        private string _code;
        private Company _company;
        private Workplace _workplace;
        private DateTime _startDate;
        private DateTime _endDate;
        private string _column1;
        private string _column2;
        private string _column3;
        private string _column4;
        private string _column5;
        private string _column6;
        private string _column7;
        private string _column8;
        private string _column9;
        private string _column10;
        private string _column11;
        private string _column12;
        private string _column13;
        private string _column14;
        private string _column15;
        private string _column16;
        private string _column17;
        private string _column18;
        private string _column19;
        private InvoiceOutMonitoring _invoiceOutMonitoring;
        private string _localUserAccess;
        private GlobalFunction _globFunc;
        //AuditTrail
        private XPCollection<AuditDataItemPersistent> changeHistory;


        public InvoiceOutMonitoringLine(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if (!IsLoading)
            {
                if (Session.IsNewObject(this))
                {
                    _globFunc = new GlobalFunction();
                    DateTime now = DateTime.Now;
                    #region UserAccess
                    _localUserAccess = SecuritySystem.CurrentUserName;
                    UserAccess _locUserAccess = Session.FindObject<UserAccess>(new BinaryOperator("UserName", _localUserAccess));
                    if (_locUserAccess != null)
                    {
                        if (_locUserAccess.Employee != null)
                        {
                            if (_locUserAccess.Employee.Company != null && _locUserAccess.Employee.Workplace != null)
                            {
                                this.Code = _globFunc.GetNumberingUnlockOptimisticRecordBasedOrganization(this.Session.DataLayer, ObjectList.InvoiceOutMonitoringLine, _locUserAccess.Employee.Company, _locUserAccess.Employee.Workplace);
                            }
                            else
                            {
                                this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.InvoiceOutMonitoringLine);
                            }
                        }
                    }
                    #endregion UserAccess
                }
            }
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).
        }

        #region Field
        [RuleRequiredField(DefaultContexts.Save)]
        [Appearance("InvoiceOutMonitoringLineCodeEnabled", Enabled = false)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue("Code", ref _code, value); }
        }

        [Appearance("InvoiceOutMonitoringLineCompanyClose", Enabled = false)]
        public Company Company
        {
            get { return _company; }
            set { SetPropertyValue("Company", ref _company, value); }
        }

        [Appearance("InvoiceOutMonitoringLineWorkplaceClose", Enabled = false)]
        public Workplace Workplace
        {
            get { return _workplace; }
            set { SetPropertyValue("Workplace", ref _workplace, value); }
        }

        [ModelDefault("DisplayFormat", "{0:dd/MM/yyyy}")]
        [ModelDefault("EditMask", "dd/MM/yyyy")]
        public DateTime StartDate
        {
            get { return _startDate; }
            set { SetPropertyValue("StartDate", ref _startDate, value); }
        }

        [ModelDefault("DisplayFormat", "{0:dd/MM/yyyy}")]
        [ModelDefault("EditMask", "dd/MM/yyyy")]
        public DateTime EndDate
        {
            get { return _endDate; }
            set { SetPropertyValue("EndDate", ref _endDate, value); }
        }

        public string Column1
        {
            get { return _column1; }
            set { SetPropertyValue("Column1", ref _column1, value); }
        }

        public string Column2
        {
            get { return _column2; }
            set { SetPropertyValue("Column2", ref _column2, value); }
        }

        public string Column3
        {
            get { return _column3; }
            set { SetPropertyValue("Column3", ref _column3, value); }
        }

        public string Column4
        {
            get { return _column4; }
            set { SetPropertyValue("Column4", ref _column4, value); }
        }

        public string Column5
        {
            get { return _column5; }
            set { SetPropertyValue("Column5", ref _column5, value); }
        }

        public string Column6
        {
            get { return _column6; }
            set { SetPropertyValue("Column6", ref _column6, value); }
        }

        public string Column7
        {
            get { return _column7; }
            set { SetPropertyValue("Column7", ref _column7, value); }
        }

        public string Column8
        {
            get { return _column8; }
            set { SetPropertyValue("Column8", ref _column8, value); }
        }

        public string Column9
        {
            get { return _column9; }
            set { SetPropertyValue("Column9", ref _column9, value); }
        }

        public string Column10
        {
            get { return _column10; }
            set { SetPropertyValue("Column10", ref _column10, value); }
        }

        public string Column11
        {
            get { return _column11; }
            set { SetPropertyValue("Column11", ref _column11, value); }
        }

        public string Column12
        {
            get { return _column12; }
            set { SetPropertyValue("Column12", ref _column12, value); }
        }

        public string Column13
        {
            get { return _column13; }
            set { SetPropertyValue("Column13", ref _column13, value); }
        }

        public string Column14
        {
            get { return _column14; }
            set { SetPropertyValue("Column14", ref _column14, value); }
        }

        public string Column15
        {
            get { return _column15; }
            set { SetPropertyValue("Column15", ref _column15, value); }
        }

        public string Column16
        {
            get { return _column16; }
            set { SetPropertyValue("Column16", ref _column16, value); }
        }

        public string Column17
        {
            get { return _column17; }
            set { SetPropertyValue("Column17", ref _column17, value); }
        }

        public string Column18
        {
            get { return _column18; }
            set { SetPropertyValue("Column18", ref _column18, value); }
        }

        public string Column19
        {
            get { return _column18; }
            set { SetPropertyValue("Column18", ref _column18, value); }
        }

        [RuleRequiredField(DefaultContexts.Save)]
        [Association("InvoiceOutMonitoring-InvoiceOutMonitoringLines")]
        [Appearance("InvoiceOutMonitoringLineInvoiceOutMonitoringEnabled", Enabled = false)]
        public InvoiceOutMonitoring InvoiceOutMonitoring
        {
            get { return _invoiceOutMonitoring; }
            set {
                SetPropertyValue("InvoiceOutMonitoring", ref _invoiceOutMonitoring, value);
                if (!IsLoading)
                {
                    if (this._invoiceOutMonitoring != null)
                    {
                        if (this._invoiceOutMonitoring.Company != null) { this.Company = this._invoiceOutMonitoring.Company; }
                        if (this._invoiceOutMonitoring.Workplace != null) { this.Workplace = this._invoiceOutMonitoring.Workplace; }
                        this.StartDate = this._invoiceOutMonitoring.StartDate;
                        this.EndDate = this._invoiceOutMonitoring.EndDate;
                    }
                }
            }
        }

        //AuditTrail
        [CollectionOperationSet(AllowAdd = false, AllowRemove = true)]
        public XPCollection<AuditDataItemPersistent> ChangeHistory
        {
            get
            {
                if (changeHistory == null)
                {
                    changeHistory = AuditedObjectWeakReference.GetAuditTrail(Session, this);
                }
                return changeHistory;
            }
        }

        #endregion Field
    }
}