﻿using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;

using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.ConditionalAppearance;

namespace FullDrive.Module.BusinessObjects
{
    [DefaultClassOptions]
    [DefaultProperty("Code")]
    [NavigationItem("Inventory")]
    [RuleCombinationOfPropertiesIsUnique("ItemReclassJournalLineRuleUnique", DefaultContexts.Save, "Code")]
    //[ImageName("BO_Contact")]
    //[DefaultProperty("DisplayMemberNameForLookupEditorsOfThisType")]
    //[DefaultListViewOptions(MasterDetailMode.ListViewOnly, false, NewItemRowPosition.None)]
    //[Persistent("DatabaseTableName")]
    // Specify more UI options using a declarative approach (https://documentation.devexpress.com/#eXpressAppFramework/CustomDocument112701).
    public class ItemReclassJournalLine : BaseObject
    { // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).
        private bool _activationPosting;
        private int _no;
        private bool _select;
        private string _code;
        #region InitialOrganization
        private Company _company;
        private Workplace _workplace;
        private Division _division;
        private Department _department;
        private Section _section;
        private Employee _employee;
        #endregion InitialOrganization
        #region InitialLocationAndBinLocation
        private LocationType _locationType;
        private StockType _stockType;
        private XPCollection<Location> _availableLocation;
        private XPCollection<BinLocation> _availableBinLocation;
        private Location _location;
        private BinLocation _binLocation;
        private StockGroup _stockGroup;
        #endregion InitialLocationAndBinLocation
        private BeginningInventory _begInv;
        private BeginningInventoryLine _begInvLine;
        private OrderType _itemType;
        private Item _item;
        private Brand _brand;
        private string _description;
        private XPCollection<UnitOfMeasure> _availableUnitOfMeasure;
        #region InitialNewDefaultQuantity
        private double _dQty;
        private UnitOfMeasure _dUom;
        private double _qty;
        private UnitOfMeasure _uom;
        private double _tQty;
        #endregion InitialNewDefaultQuantity
        private Status _status;
        private DateTime _statusDate;
        private ItemReclassJournal _itemReclassJournal;
        private string _userAccess;
        private string _localUserAccess;
        private GlobalFunction _globFunc;
        //AuditTrail
        private XPCollection<AuditDataItemPersistent> changeHistory;

        public ItemReclassJournalLine(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if(!IsLoading)
            {
                if(Session.IsNewObject(this))
                {
                    _globFunc = new GlobalFunction();
                    DateTime now = DateTime.Now;
                    #region UserAccess
                    _localUserAccess = SecuritySystem.CurrentUserName;
                    UserAccess _locUserAccess = Session.FindObject<UserAccess>(new BinaryOperator("UserName", _localUserAccess));
                    if (_locUserAccess != null)
                    {
                        if (_locUserAccess.Employee != null)
                        {
                            if (_locUserAccess.Employee.Company != null && _locUserAccess.Employee.Workplace != null)
                            {
                                this.Code = _globFunc.GetNumberingUnlockOptimisticRecordBasedOrganization(this.Session.DataLayer, ObjectList.ItemReclassJournalLine, _locUserAccess.Employee.Company, _locUserAccess.Employee.Workplace);
                            }
                            else
                            {
                                this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.ItemReclassJournalLine);
                            }
                        }
                    }
                    #endregion UserAccess
                    this.Status = Status.Open;
                    this.StatusDate = now;
                    this.StockType = StockType.Good;
                    this.Select = true;
                }
            }
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).
        }

        #region Field

        protected override void OnSaving()
        {
            base.OnSaving();
            if (!IsLoading && IsSaving)
            {
                UpdateNo();
            }
        }

        protected override void OnDeleting()
        {
            base.OnDeleting();
            if (!IsLoading)
            {
                RecoveryDeleteNo();
            }
        }       

        [Browsable(false)]
        public bool ActivationPosting
        {
            get { return _activationPosting; }
            set { SetPropertyValue("ActivationPosting", ref _activationPosting, value); }
        }

        [Appearance("ItemReclassJournalLineNoClose", Enabled = false)]
        public int No
        {
            get { return _no; }
            set { SetPropertyValue("No", ref _no, value); }
        }

        [Appearance("ItemReclassJournalLineSelectClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public bool Select
        {
            get { return _select; }
            set { SetPropertyValue("Select", ref _select, value); }
        }

        [RuleRequiredField(DefaultContexts.Save)]
        [Appearance("ItemReclassJournalLineCodeClose", Enabled = false)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue("Code", ref _code, value); }
        }

        #region Company

        [RuleRequiredField(DefaultContexts.Save)]
        [Appearance("ItemReclassJournalLineCompanyClose",Enabled = false)]
        public Company Company
        {
            get { return _company; }
            set { SetPropertyValue("Company", ref _company, value); }
        }

        [Appearance("ItemReclassJournalLineWorkplaceClose", Enabled = false)]
        public Workplace Workplace
        {
            get { return _workplace; }
            set { SetPropertyValue("Workplace", ref _workplace, value); }
        }

        [Browsable(false)]
        public Division Division
        {
            get { return _division; }
            set { SetPropertyValue("Division", ref _division, value); }
        }

        [Browsable(false)]
        public Department Department
        {
            get { return _department; }
            set { SetPropertyValue("Department", ref _department, value); }
        }

        [Browsable(false)]
        public Section Section
        {
            get { return _section; }
            set { SetPropertyValue("Section", ref _section, value); }
        }

        [Browsable(false)]
        public Employee Employee
        {
            get { return _employee; }
            set { SetPropertyValue("Employee", ref _employee, value); }
        }

        #endregion Company

        #region Location

        [ImmediatePostData()]
        [Appearance("ItemReclassJournalLineLocationTypeClose", Enabled = false)]
        public LocationType LocationType
        {
            get { return _locationType; }
            set { SetPropertyValue("LocationType", ref _locationType, value); }
        }

        [ImmediatePostData()]
        [Appearance("ItemReclassJournalLineStockTypeClose", Enabled = false)]
        public StockType StockType
        {
            get { return _stockType; }
            set { SetPropertyValue("StockType", ref _stockType, value); }
        }

        [Browsable(false)]
        public XPCollection<Location> AvailableLocation
        {
            get
            {
                _userAccess = SecuritySystem.CurrentUserName;
                string _beginString = null;
                string _endString = null;
                string _fullString = null;
                string _locLocationCode = null;
                UserAccess _locUserAccess = Session.FindObject<UserAccess>(new BinaryOperator("UserName", _userAccess));
                List<string> _stringLocation = new List<string>();
                if (_locUserAccess != null)
                {
                    if (this.ItemReclassJournal != null)
                    {
                        #region ItemReclassJournalLine
                        if (this.Company != null && this.Workplace != null && this.ItemReclassJournal.TransferType == DirectionType.Internal &&
                            (this.ItemReclassJournal.InventoryMovingType == InventoryMovingType.Adjust || this.ItemReclassJournal.InventoryMovingType == InventoryMovingType.Deformed))
                        {
                            XPCollection<WarehouseSetupDetail> _locWhsSetupDetails = null;

                            if (this.LocationType == LocationType.None)
                            {
                                _locWhsSetupDetails = new XPCollection<WarehouseSetupDetail>
                                                    (Session, new GroupOperator(GroupOperatorType.And,
                                                    new BinaryOperator("UserAccess", _locUserAccess),
                                                    new BinaryOperator("Owner", true),
                                                    new BinaryOperator("StockType", this.StockType),
                                                    new BinaryOperator("Active", true)));


                            }
                            else
                            {
                                _locWhsSetupDetails = new XPCollection<WarehouseSetupDetail>
                                                    (Session, new GroupOperator(GroupOperatorType.And,
                                                    new BinaryOperator("UserAccess", _locUserAccess),
                                                    new BinaryOperator("Owner", true),
                                                    new BinaryOperator("LocationType", this.LocationType),
                                                    new BinaryOperator("StockType", this.StockType),
                                                    new BinaryOperator("Active", true)));

                            }

                            if (_locWhsSetupDetails != null && _locWhsSetupDetails.Count() > 0)
                            {
                                foreach (WarehouseSetupDetail _locWhsSetupDetail in _locWhsSetupDetails)
                                {
                                    if (_locWhsSetupDetail.Location != null)
                                    {
                                        if (_locWhsSetupDetail.Location.Code != null)
                                        {
                                            _locLocationCode = _locWhsSetupDetail.Location.Code;
                                            _stringLocation.Add(_locLocationCode);
                                        }
                                    }
                                }
                            }

                            IEnumerable<string> _stringArrayLocationFromDistinct = _stringLocation.Distinct();
                            string[] _stringArrayLocationFromList = _stringArrayLocationFromDistinct.ToArray();
                            if (_stringArrayLocationFromList.Length == 1)
                            {
                                for (int i = 0; i < _stringArrayLocationFromList.Length; i++)
                                {
                                    Location _locLocationFrom = Session.FindObject<Location>(new BinaryOperator("Code", _stringArrayLocationFromList[i]));
                                    if (_locLocationFrom != null)
                                    {
                                        if (i == 0)
                                        {
                                            _beginString = "[Code]=='" + _locLocationFrom.Code + "'";
                                        }
                                    }
                                }
                            }
                            else if (_stringArrayLocationFromList.Length > 1)
                            {
                                for (int i = 0; i < _stringArrayLocationFromList.Length; i++)
                                {
                                    Location _locLocationFrom = Session.FindObject<Location>(new BinaryOperator("Code", _stringArrayLocationFromList[i]));
                                    if (_locLocationFrom != null)
                                    {
                                        if (i == 0)
                                        {
                                            _beginString = "[Code]=='" + _locLocationFrom.Code + "'";
                                        }
                                        else
                                        {
                                            _endString = _endString + " OR [Code]=='" + _locLocationFrom.Code + "'";
                                        }
                                    }
                                }
                            }
                            _fullString = _beginString + _endString;

                            if (_fullString != null)
                            {
                                _availableLocation = new XPCollection<Location>(Session, CriteriaOperator.Parse(_fullString));
                            }
                        }
                        #endregion ItemReclassJournalLine
                    }
                }

                return _availableLocation;

            }
        }

        [Appearance("ItemReclassJournalLineLocationClose", Enabled = false)]
        [DataSourceProperty("AvailableLocation")]
        [DataSourceCriteria("Active = true")]
        [ImmediatePostData()]
        public Location Location
        {
            get { return _location; }
            set { SetPropertyValue("Location", ref _location, value); }
        }

        [Browsable(false)]
        public XPCollection<BinLocation> AvailableBinLocation
        {
            get
            {
                _userAccess = SecuritySystem.CurrentUserName;
                string _beginString = null;
                string _endString = null;
                string _fullString = null;
                string _locBinLocationCode = null;
                UserAccess _locUserAccess = Session.FindObject<UserAccess>(new BinaryOperator("UserName", _userAccess));
                List<string> _stringBinLocation = new List<string>();
                if (_locUserAccess != null)
                {
                    if (this.Location != null)
                    {
                        if (this.ItemReclassJournal != null)
                        {
                            #region ItemReclassJournalLine
                            if (this.Company != null && this.Workplace != null && this.ItemReclassJournal.TransferType == DirectionType.Internal &&
                                (this.ItemReclassJournal.InventoryMovingType == InventoryMovingType.Adjust || this.ItemReclassJournal.InventoryMovingType == InventoryMovingType.Deformed))
                            {

                                XPCollection<WarehouseSetupDetail> _locWhsSetupDetails = new XPCollection<WarehouseSetupDetail>
                                                                                  (Session, new GroupOperator(GroupOperatorType.And,
                                                                                    new BinaryOperator("Company", this.Company),
                                                                                    new BinaryOperator("Workplace", this.Workplace),
                                                                                    new BinaryOperator("UserAccess", _locUserAccess),
                                                                                    new BinaryOperator("Location", this.Location),
                                                                                    new BinaryOperator("Owner", true),
                                                                                    new BinaryOperator("Active", true)));

                                if (_locWhsSetupDetails != null && _locWhsSetupDetails.Count() > 0)
                                {
                                    foreach (WarehouseSetupDetail _locWhsSetupDetail in _locWhsSetupDetails)
                                    {
                                        if (_locWhsSetupDetail.BinLocation != null)
                                        {
                                            if (_locWhsSetupDetail.BinLocation.Code != null)
                                            {
                                                _locBinLocationCode = _locWhsSetupDetail.BinLocation.Code;
                                                _stringBinLocation.Add(_locBinLocationCode);
                                            }
                                        }
                                    }
                                }

                                IEnumerable<string> _stringArrayBinLocationFromDistinct = _stringBinLocation.Distinct();
                                string[] _stringArrayBinLocationFromList = _stringArrayBinLocationFromDistinct.ToArray();
                                if (_stringArrayBinLocationFromList.Length == 1)
                                {
                                    for (int i = 0; i < _stringArrayBinLocationFromList.Length; i++)
                                    {
                                        BinLocation _locBinLocationFrom = Session.FindObject<BinLocation>(new BinaryOperator("Code", _stringArrayBinLocationFromList[i]));
                                        if (_locBinLocationFrom != null)
                                        {
                                            if (i == 0)
                                            {
                                                _beginString = "[Code]=='" + _locBinLocationFrom.Code + "'";
                                            }
                                        }
                                    }
                                }
                                else if (_stringArrayBinLocationFromList.Length > 1)
                                {
                                    for (int i = 0; i < _stringArrayBinLocationFromList.Length; i++)
                                    {
                                        BinLocation _locBinLocationFrom = Session.FindObject<BinLocation>(new BinaryOperator("Code", _stringArrayBinLocationFromList[i]));
                                        if (_locBinLocationFrom != null)
                                        {
                                            if (i == 0)
                                            {
                                                _beginString = "[Code]=='" + _locBinLocationFrom.Code + "'";
                                            }
                                            else
                                            {
                                                _endString = _endString + " OR [Code]=='" + _locBinLocationFrom.Code + "'";
                                            }
                                        }
                                    }
                                }
                                _fullString = _beginString + _endString;

                                if (_fullString != null)
                                {
                                    _availableBinLocation = new XPCollection<BinLocation>(Session, CriteriaOperator.Parse(_fullString));
                                }
                            }
                            #endregion ItemReclassJournalLine
                        }
                    }
                }

                return _availableBinLocation;

            }
        }

        [Appearance("ItemReclassJournalLineBinLocationClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [DataSourceProperty("AvailableBinLocation")]
        [DataSourceCriteria("Active = true")]
        public BinLocation BinLocation
        {
            get { return _binLocation; }
            set { SetPropertyValue("BinLocation", ref _binLocation, value); }
        }

        [Appearance("ItemReclassJournalLineStockGroupClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public StockGroup StockGroup
        {
            get { return _stockGroup; }
            set { SetPropertyValue("StockGroup", ref _stockGroup, value); }
        }

        #endregion Location      

        [ImmediatePostData()]
        [DataSourceCriteria("Company = '@This.Company' AND Workplace = '@This.Workplace' AND Location = '@This.Location' AND Active = true")]
        [Appearance("ItemReclassJournalLineBeginningInventoryClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public BeginningInventory BegInv
        {
            get { return _begInv; }
            set { SetPropertyValue("BegInv", ref _begInv, value); }
        }
        
        [ImmediatePostData()]
        [DataSourceCriteria("Company = '@This.Company' AND Workplace = '@This.Workplace' AND BeginningInventory = '@This.BegInv' AND Active = true")]
        [Appearance("ItemReclassJournalLineBegInvLineClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public BeginningInventoryLine BegInvLine
        {
            get { return _begInvLine; }
            set {
                SetPropertyValue("BegInvLine", ref _begInvLine, value);
                if(!IsLoading)
                {
                    if(this._begInvLine != null)
                    {
                        if(this._begInvLine.Item != null)
                        {
                            this.Item = this._begInvLine.Item;
                        }
                    }
                }
            }
        }

        [ImmediatePostData()]
        [Appearance("ItemReclassJournalLineItemClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Item Item
        {
            get { return _item; }
            set
            {
                SetPropertyValue("Item", ref _item, value);
                if (!IsLoading)
                {
                    if (this._item != null)
                    {
                        this.DUOM = this._item.BasedUOM;
                        this.Brand = this._item.Brand;
                        this.Description = GetDescriptions();
                        this.ItemType = this.Item.ItemType;
                        SetUOM();
                    }else
                    {
                        this.DUOM = null;
                        this.Brand = null;
                        this.Description = null;
                        this.UOM = null;
                    }
                }
            }
        }

        [Appearance("ItemReclassJournalLineItemTypeClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public OrderType ItemType
        {
            get { return _itemType; }
            set { SetPropertyValue("ItemType", ref _itemType, value); }
        }

        [Appearance("ItemReclassJournalLineBrandClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Brand Brand
        {
            get { return _brand; }
            set { SetPropertyValue("Brand", ref _brand, value); }
        }

        [Appearance("ItemReclassJournalLineDescriptionClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public string Description
        {
            get { return _description; }
            set { SetPropertyValue("Description", ref _description, value); }
        }

        [Persistent]
        public double QtyAvailable
        {
            get
            {
                double _locQtyAvailable = 0;
                if (this.BegInvLine != null)
                {
                    if (this.BegInvLine.QtyAvailable > 0)
                    {
                        _locQtyAvailable = this.BegInvLine.QtyAvailable;
                    }
                    return _locQtyAvailable;
                }
                return 0;
            }
        }

        [Persistent]
        public string UOM_Available
        {
            get
            {
                string _locName = null;
                if (this.BegInvLine != null)
                {
                    if (this.BegInvLine.FullName != null)
                    {
                        _locName = this.BegInvLine.FullName;
                    }
                    return String.Format("{0}", _locName);
                }
                return null;
            }
        }

        [Browsable(false)]
        public XPCollection<UnitOfMeasure> AvailableUnitOfMeasure
        {
            get
            {
                string _beginString = null;
                string _endString = null;
                string _fullString = null;
                if (Item == null)
                {
                    _availableUnitOfMeasure = new XPCollection<UnitOfMeasure>(Session);
                }
                else
                {
                    List<string> _stringUOM = new List<string>();

                    XPCollection<ItemUnitOfMeasure> _locItemUnitOfMeasures = new XPCollection<ItemUnitOfMeasure>
                                                                              (Session, new GroupOperator(GroupOperatorType.And,
                                                                               new BinaryOperator("Item", this.Item),
                                                                               new BinaryOperator("Active", true)));

                    if (_locItemUnitOfMeasures != null && _locItemUnitOfMeasures.Count() > 0)
                    {
                        foreach (ItemUnitOfMeasure _locItemUnitOfMeasure in _locItemUnitOfMeasures)
                        {
                            _stringUOM.Add(_locItemUnitOfMeasure.UOM.Code);
                        }
                    }

                    IEnumerable<string> _stringArrayUOMDistinct = _stringUOM.Distinct();
                    string[] _stringArrayUOMList = _stringArrayUOMDistinct.ToArray();
                    if (_stringArrayUOMList.Length == 1)
                    {
                        for (int i = 0; i < _stringArrayUOMList.Length; i++)
                        {
                            UnitOfMeasure _locUOM = Session.FindObject<UnitOfMeasure>(new BinaryOperator("Code", _stringArrayUOMList[i]));
                            if (_locUOM != null)
                            {
                                if (i == 0)
                                {
                                    _beginString = "[Code]=='" + _locUOM.Code + "'";
                                }
                            }
                        }
                    }
                    else if (_stringArrayUOMList.Length > 1)
                    {
                        for (int i = 0; i < _stringArrayUOMList.Length; i++)
                        {
                            UnitOfMeasure _locUOM = Session.FindObject<UnitOfMeasure>(new BinaryOperator("Code", _stringArrayUOMList[i]));
                            if (_locUOM != null)
                            {
                                if (i == 0)
                                {
                                    _beginString = "[Code]=='" + _locUOM.Code + "'";
                                }
                                else
                                {
                                    _endString = " OR [Code]=='" + _locUOM.Code + "'";
                                }
                            }
                        }
                    }
                    _fullString = _beginString + _endString;

                    if (_fullString != null)
                    {
                        _availableUnitOfMeasure = new XPCollection<UnitOfMeasure>(Session, CriteriaOperator.Parse(_fullString));
                    }

                }
                return _availableUnitOfMeasure;

            }
        }

        #region DefaultFromQty

        [Appearance("ItemReclassJournalLineDQtyClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [ImmediatePostData()]
        public double DQty
        {
            get { return _dQty; }
            set
            {
                SetPropertyValue("DQty", ref _dQty, value);
                if (!IsLoading)
                {
                    SetTotalQty();
                }
            }
        }

        [Appearance("ItemReclassJournalLineDUOMClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [ImmediatePostData()]
        [DataSourceCriteria("Active = true")]
        public UnitOfMeasure DUOM
        {
            get { return _dUom; }
            set
            {
                SetPropertyValue("DUOM", ref _dUom, value);
                if (!IsLoading)
                {
                    SetUOM();
                    SetTotalQty();
                }
            }
        }

        [Appearance("ItemReclassJournalLineQtyClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [ImmediatePostData()]
        public double Qty
        {
            get { return _qty; }
            set
            {
                SetPropertyValue("Qty", ref _qty, value);
                if (!IsLoading)
                {
                    SetTotalQty();
                }
            }
        }

        [Appearance("ItemReclassJournalLineUOMClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [ImmediatePostData()]
        [DataSourceProperty("AvailableUnitOfMeasure", DataSourcePropertyIsNullMode.SelectAll)]
        [DataSourceCriteria("Active = true")]
        public UnitOfMeasure UOM
        {
            get { return _uom; }
            set
            {
                SetPropertyValue("UOM", ref _uom, value);
                if (!IsLoading)
                {
                    SetTotalQty();
                }
            }
        }

        [Appearance("ItemReclassJournalLineTQtyEnabled", Enabled = false)]
        public double TQty
        {
            get { return _tQty; }
            set { SetPropertyValue("TQty", ref _tQty, value); }
        }

        #endregion DefaultFromQty

        [Appearance("ItemReclassJournalLineStatusEnabled", Enabled = false)]
        public Status Status
        {
            get { return _status; }
            set { SetPropertyValue("Status", ref _status, value); }
        }

        [ModelDefault("DisplayFormat", "{0:dd/MM/yyyy}")]
        [ModelDefault("EditMask", "dd/MM/yyyy")]
        [Appearance("ItemReclassJournalLineStatusDateEnabled", Enabled = false)]
        public DateTime StatusDate
        {
            get { return _statusDate; }
            set { SetPropertyValue("StatusDate", ref _statusDate, value); }
        }

        [ImmediatePostData()]
        [RuleRequiredField(DefaultContexts.Save)]
        [Appearance("ItemReclassJournalLineItemReclassJournalEnabled", Enabled = false)]
        [Association("ItemReclassJournal-ItemReclassJournalLines")]
        public ItemReclassJournal ItemReclassJournal
        {
            get { return _itemReclassJournal; }
            set
            {
                SetPropertyValue("ItemReclassJournal", ref _itemReclassJournal, value);
                if (!IsLoading)
                {
                    if (this._itemReclassJournal != null)
                    {
                        if (this._itemReclassJournal.LocationType != LocationType.None)
                        {
                            this.LocationType = this._itemReclassJournal.LocationType;
                        }
                        if (this._itemReclassJournal.StockType != StockType.None)
                        {
                            this.StockType = this._itemReclassJournal.StockType;
                        }
                        if (this._itemReclassJournal.Location != null)
                        {
                            this.Location = this._itemReclassJournal.Location;
                        }
                        if (this._itemReclassJournal.BeginningInventory != null)
                        {
                            this.BegInv = this._itemReclassJournal.BeginningInventory;
                        }
                        if (this._itemReclassJournal.Company != null)
                        {
                            this.Company = this._itemReclassJournal.Company;
                        }
                        if (this._itemReclassJournal.Workplace != null)
                        {
                            this.Workplace = this._itemReclassJournal.Workplace;
                        }
                        if (this._itemReclassJournal.Division != null)
                        {
                            this.Division = this._itemReclassJournal.Division;
                        }
                        if (this._itemReclassJournal.Department != null)
                        {
                            this.Department = this._itemReclassJournal.Department;
                        }
                        if (this._itemReclassJournal.Section != null)
                        {
                            this.Section = this._itemReclassJournal.Section;
                        }
                        if (this._itemReclassJournal.Employee != null)
                        {
                            this.Employee = this._itemReclassJournal.Employee;
                        }
                    }
                }
            }
        }

        //AuditTrail
        [CollectionOperationSet(AllowAdd = false, AllowRemove = true)]
        public XPCollection<AuditDataItemPersistent> ChangeHistory
        {
            get
            {
                if (changeHistory == null)
                {
                    changeHistory = AuditedObjectWeakReference.GetAuditTrail(Session, this);
                }
                return changeHistory;
            }
        }

        #endregion Field

        //================================================== Code Only ==================================================

        #region No

        public void UpdateNo()
        {
            try
            {
                if (!IsLoading && Session.IsNewObject(this))
                {
                    if (this.ItemReclassJournal != null)
                    {
                        object _makRecord = Session.Evaluate<ItemReclassJournalLine>(CriteriaOperator.Parse("Max(No)"), CriteriaOperator.Parse("ItemReclassJournal=?", this.ItemReclassJournal));
                        this.No = Convert.ToInt32(_makRecord) + 1;
                        this.Save();
                        RecoveryUpdateNo();
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = ItemReclassJournalLine " + ex.ToString());
            }
        }

        public void RecoveryUpdateNo()
        {
            try
            {
                if (this.ItemReclassJournal != null)
                {
                    ItemReclassJournal _numHeader = Session.FindObject<ItemReclassJournal>
                                                (new BinaryOperator("Code", this.ItemReclassJournal.Code));

                    XPCollection<ItemReclassJournalLine> _numLines = new XPCollection<ItemReclassJournalLine>
                                                (Session, new BinaryOperator("ItemReclassJournal", _numHeader),
                                                new SortProperty("No", DevExpress.Xpo.DB.SortingDirection.Ascending));
                    if (_numLines != null)
                    {
                        int i1 = 0;
                        foreach (ItemReclassJournalLine _numLine in _numLines)
                        {
                            i1 += 1;
                            _numLine.No = i1;
                            _numLine.Save();
                        }
                        i1 = 1;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = ItemReclassJournalLine " + ex.ToString());
            }
        }

        public void RecoveryDeleteNo()
        {
            try
            {
                if (this.ItemReclassJournal != null)
                {
                    TransferOut _numHeader = Session.FindObject<TransferOut>
                                                (new BinaryOperator("Code", this.ItemReclassJournal.Code));

                    XPCollection<ItemReclassJournalLine> _numLines = new XPCollection<ItemReclassJournalLine>
                                                (Session, new GroupOperator(GroupOperatorType.And,
                                                 new BinaryOperator("This", this, BinaryOperatorType.NotEqual),
                                                 new BinaryOperator("ItemReclassJournal", _numHeader)),
                                                 new SortProperty("No", DevExpress.Xpo.DB.SortingDirection.Ascending));
                    if (_numLines != null)
                    {
                        int i = 0;
                        foreach (ItemReclassJournalLine _numLine in _numLines)
                        {
                            i += 1;
                            _numLine.No = i;
                            _numLine.Save();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = ItemReclassJournalLine " + ex.ToString());
            }
        }

        #endregion No

        #region Set

        public string GetDescriptions()
        {
            string _result = "";
            try
            {
                if (this._item != null)
                {
                    string _result1 = null;
                    string _result2 = null;
                    if (this._item.Name != null)
                    {
                        _result1 = this._item.Name;
                    }
                    if (this._item.Description != null)
                    {
                        _result2 = this._item.Description;
                    }
                    _result = _result1 + " " + _result2;
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = ItemReclassJournalLine " + ex.ToString());
            }
            return _result;
        }

        private void SetTotalQty()
        {
            try
            {
                double _locInvLineTotal = 0;
                DateTime now = DateTime.Now;
                if (this.ItemReclassJournal != null)
                {
                    if (this.Item != null && this.UOM != null && this.DUOM != null)
                    {
                        ItemUnitOfMeasure _locItemUOM = Session.FindObject<ItemUnitOfMeasure>
                                                        (new GroupOperator(GroupOperatorType.And,
                                                         new BinaryOperator("Item", this.Item),
                                                         new BinaryOperator("UOM", this.UOM),
                                                         new BinaryOperator("DefaultUOM", this.DUOM),
                                                         new BinaryOperator("Active", true)));
                        if (_locItemUOM != null)
                        {
                            if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                            {
                                _locInvLineTotal = this.Qty * _locItemUOM.DefaultConversion + this.DQty;
                            }
                            else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                            {
                                _locInvLineTotal = this.Qty / _locItemUOM.Conversion + this.DQty;
                            }
                            else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                            {
                                _locInvLineTotal = this.Qty + this.DQty;
                            }

                            this.TQty = _locInvLineTotal;
                        }
                    }
                    else
                    {
                        _locInvLineTotal = this.Qty + this.DQty;
                        this.TQty = _locInvLineTotal;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = ItemReclassJournalLine " + ex.ToString());
            }
        }

        private void SetUOM()
        {
            try
            {
                if (this.Company != null && this.Item != null && this.DUOM != null)
                {
                    ItemUnitOfMeasure _locIUOM = Session.FindObject<ItemUnitOfMeasure>(new GroupOperator(GroupOperatorType.And,
                                                                            new BinaryOperator("Company", this.Company),
                                                                            new BinaryOperator("Item", this.Item),
                                                                            new BinaryOperator("DefaultUOM", this.DUOM),
                                                                            new BinaryOperator("Default", true),
                                                                            new BinaryOperator("Active", true)));
                    if (_locIUOM != null)
                    {
                        if (_locIUOM.UOM != null)
                        {
                            this.UOM = _locIUOM.UOM;
                        }
                        else
                        {
                            this.UOM = null;
                        }

                    }
                    else
                    {
                        this.UOM = null;
                    }
                }
                else
                {
                    this.UOM = null;
                }

            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = SalesInvoiceLine " + ex.ToString());
            }
        }

        #endregion Set

    }
}