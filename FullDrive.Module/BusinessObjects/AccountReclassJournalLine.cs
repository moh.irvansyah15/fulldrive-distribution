﻿using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;

using DevExpress.ExpressApp.Editors;
using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.ConditionalAppearance;

namespace FullDrive.Module.BusinessObjects
{
    [DefaultClassOptions]
    [DefaultProperty("Code")]
    [NavigationItem("Finance & Accounting")]
    [RuleCombinationOfPropertiesIsUnique("AccountReclassJournalLineRuleUnique", DefaultContexts.Save, "Code")]
    //[ImageName("BO_Contact")]
    //[DefaultProperty("DisplayMemberNameForLookupEditorsOfThisType")]
    //[DefaultListViewOptions(MasterDetailMode.ListViewOnly, false, NewItemRowPosition.None)]
    //[Persistent("DatabaseTableName")]
    // Specify more UI options using a declarative approach (https://documentation.devexpress.com/#eXpressAppFramework/CustomDocument112701).
    public class AccountReclassJournalLine : BaseObject
    { // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).
        private bool _activationPosting;
        private int _no;
        private string _code;
        #region InitialOrganization
        private Company _company;
        private XPCollection<Workplace> _availableWorkplace;
        private Workplace _workplace;
        private Division _division;
        private Department _department;
        private Section _section;
        private Employee _employee;
        #endregion InitialOrganization
        private string _name;
        private PostingType _postingType;
        private PostingMethod _postingMethod;
        private PostingMethodType _postingMethodType;
        private string _description;
        private AccountGroup _accountGroup;
        private XPCollection<ChartOfAccount> _availableChartOfAccount;
        private ChartOfAccount _account;
        private bool _openDebit;
        private double _debit;
        private bool _openCredit;
        private double _credit;
        private DateTime _docDate;
        private int _journalMonth;
        private int _journalYear;
        private Status _status;
        private DateTime _statusDate;
        private AccountReclassJournal _accountReclassJournal;
        private string _localUserAccess;
        private GlobalFunction _globFunc;
        //AuditTrail
        private XPCollection<AuditDataItemPersistent> changeHistory;

        public AccountReclassJournalLine(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if (!IsLoading)
            {
                if(Session.IsNewObject(this))
                {
                    _globFunc = new GlobalFunction();
                    DateTime now = DateTime.Now;
                    #region UserAccess
                    _localUserAccess = SecuritySystem.CurrentUserName;
                    UserAccess _locUserAccess = Session.FindObject<UserAccess>(new BinaryOperator("UserName", _localUserAccess));
                    if (_locUserAccess != null)
                    {
                        if (_locUserAccess.Employee != null)
                        {
                            if (_locUserAccess.Employee.Company != null && _locUserAccess.Employee.Workplace != null)
                            {
                                this.Code = _globFunc.GetNumberingUnlockOptimisticRecordBasedOrganization(this.Session.DataLayer, ObjectList.AccountReclassJournalLine, _locUserAccess.Employee.Company, _locUserAccess.Employee.Workplace);
                            }
                            else
                            {
                                this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.AccountReclassJournalLine);
                            }
                        }
                    }
                    #endregion UserAccess
                    this.Status = Status.Open;
                    this.StatusDate = now;
                }               
            }
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).
        }

        #region Field

        protected override void OnSaving()
        {
            base.OnSaving();
            if (!IsLoading && IsSaving)
            {
                UpdateNo();
            }
        }

        protected override void OnDeleting()
        {
            base.OnDeleting();
            if (!IsLoading)
            {
                RecoveryDeleteNo();
            }
        }

        [Browsable(false)]
        public bool ActivationPosting
        {
            get { return _activationPosting; }
            set { SetPropertyValue("ActivationPosting", ref _activationPosting, value); }
        }

        [Appearance("AccountReclassJournalLineNoEnabled", Enabled = false)]
        public int No
        {
            get { return _no; }
            set { SetPropertyValue("No", ref _no, value); }
        }

        [RuleRequiredField(DefaultContexts.Save)]
        [Appearance("AccountReclassJournalLineCodeEnabled", Enabled = false)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue("Code", ref _code, value); }
        }

        #region Organization

        [ImmediatePostData()]
        [RuleRequiredField(DefaultContexts.Save)]
        [DataSourceCriteria("Active = true")]
        [Appearance("AccountReclassJournalLineCompanyEnabled", Enabled = false)]
        public Company Company
        {
            get { return _company; }
            set { SetPropertyValue("Company", ref _company, value); }
        }

        [Browsable(false)]
        public XPCollection<Workplace> AvailableWorkplace
        {
            get
            {
                if (!IsLoading)
                {
                    if (this.Company != null)
                    {
                        _localUserAccess = SecuritySystem.CurrentUserName;
                        UserAccess _locUserAccess = Session.FindObject<UserAccess>(new BinaryOperator("UserName", _localUserAccess));
                        if (_locUserAccess != null)
                        {
                            string _beginString = null;
                            string _endString = null;
                            string _fullString = null;
                            List<string> _stringWorkplace = new List<string>();

                            XPCollection<OrganizationSetupDetail> _locOrganizationSetupDetails = new XPCollection<OrganizationSetupDetail>
                                                                            (Session, new GroupOperator(GroupOperatorType.And,
                                                                            new BinaryOperator("Company", this.Company),
                                                                            new BinaryOperator("UserAccess", _locUserAccess),
                                                                            new BinaryOperator("OpenWorkplace", true),
                                                                            new BinaryOperator("Active", true)));

                            if (_locOrganizationSetupDetails != null && _locOrganizationSetupDetails.Count() > 0)
                            {
                                foreach (OrganizationSetupDetail _locOrganizationSetupDetail in _locOrganizationSetupDetails)
                                {
                                    if (_locOrganizationSetupDetail.Workplace != null)
                                    {
                                        if (_locOrganizationSetupDetail.Workplace.Code != null)
                                        {
                                            _stringWorkplace.Add(_locOrganizationSetupDetail.Workplace.Code);
                                        }
                                    }
                                }

                                #region Workplace
                                IEnumerable<string> _stringArrayWorkplaceDistinct = _stringWorkplace.Distinct();
                                string[] _stringArrayWorkplaceList = _stringArrayWorkplaceDistinct.ToArray();
                                if (_stringArrayWorkplaceList.Length == 1)
                                {
                                    for (int i = 0; i < _stringArrayWorkplaceList.Length; i++)
                                    {
                                        Workplace _locWorkplace = Session.FindObject<Workplace>(new BinaryOperator("Code", _stringArrayWorkplaceList[i]));
                                        if (_locWorkplace != null)
                                        {
                                            if (i == 0)
                                            {
                                                _beginString = "[Code]=='" + _locWorkplace.Code + "'";
                                            }
                                        }
                                    }
                                }
                                else if (_stringArrayWorkplaceList.Length > 1)
                                {
                                    for (int i = 0; i < _stringArrayWorkplaceList.Length; i++)
                                    {
                                        Workplace _locWorkplace = Session.FindObject<Workplace>(new BinaryOperator("Code", _stringArrayWorkplaceList[i]));
                                        if (_locWorkplace != null)
                                        {
                                            if (i == 0)
                                            {
                                                _beginString = "[Code]=='" + _locWorkplace.Code + "'";
                                            }
                                            else
                                            {
                                                _endString = _endString + " OR [Code]=='" + _locWorkplace.Code + "'";
                                            }
                                        }

                                    }
                                }
                                _fullString = _beginString + _endString;


                                if (_fullString != null)
                                {
                                    _availableWorkplace = new XPCollection<Workplace>(Session, CriteriaOperator.Parse(_fullString));
                                }

                                #endregion Workplace
                            }
                            else
                            {
                                OrganizationSetupDetail _locOrgSetDetail = Session.FindObject<OrganizationSetupDetail>
                                                                            (new GroupOperator(GroupOperatorType.And,
                                                                            new BinaryOperator("Company", this.Company),
                                                                            new BinaryOperator("UserAccess", _locUserAccess),
                                                                            new BinaryOperator("AllWorkplace", true),
                                                                            new BinaryOperator("Active", true)));
                                if (_locOrgSetDetail != null)
                                {
                                    _availableWorkplace = new XPCollection<Workplace>(Session,
                                                        new GroupOperator(GroupOperatorType.And,
                                                        new BinaryOperator("Company", this.Company),
                                                        new BinaryOperator("Active", true)));
                                }
                            }
                        }
                    }
                }

                return _availableWorkplace;
            }
        }

        [ImmediatePostData()]
        [DataSourceProperty("AvailableWorkplace")]
        [Appearance("AccountReclassJournalLineWorkplaceEnabled", Enabled = false)]
        public Workplace Workplace
        {
            get { return _workplace; }
            set { SetPropertyValue("Workplace", ref _workplace, value); }
        }

        [Browsable(false)]
        public Division Division
        {
            get { return _division; }
            set { SetPropertyValue("Division", ref _division, value); }
        }

        [Browsable(false)]
        public Department Department
        {
            get { return _department; }
            set { SetPropertyValue("Department", ref _department, value); }
        }

        [Browsable(false)]
        public Section Section
        {
            get { return _section; }
            set { SetPropertyValue("Section", ref _section, value); }
        }

        [Browsable(false)]
        public Employee Employee
        {
            get { return _employee; }
            set { SetPropertyValue("Employee", ref _employee, value); }
        }

        #endregion Organization

        [Appearance("AccountReclassJournalLineNameClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public string Name
        {
            get { return _name; }
            set { SetPropertyValue("Name", ref _name, value); }
        }

        [Appearance("AccountReclassJournalLinePostingTypeClose", Enabled = false)]
        public PostingType PostingType
        {
            get { return _postingType; }
            set { SetPropertyValue("PostingType", ref _postingType, value); }
        }

        [Appearance("AccountReclassJournalLinePostingMethodClose", Enabled = false)]
        public PostingMethod PostingMethod
        {
            get { return _postingMethod; }
            set { SetPropertyValue("PostingMethod", ref _postingMethod, value); }
        }

        [Appearance("AccountReclassJournalLinePostingMethodTypeClose", Enabled = false)]
        public PostingMethodType PostingMethodType
        {
            get { return _postingMethodType; }
            set { SetPropertyValue("PostingMethodType", ref _postingMethodType, value); }
        }

        [Size(512)]
        [Appearance("AccountReclassJournalLineDescriptionClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public string Description
        {
            get { return _description; }
            set { SetPropertyValue("Description", ref _description, value); }
        }

        [ImmediatePostData]
        [DataSourceCriteria("Company = '@This.Company' And Active = true")]
        [Appearance("AccountReclassJournalLineAccountGroupClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public AccountGroup AccountGroup
        {
            get { return _accountGroup; }
            set { SetPropertyValue("AccountGroup", ref _accountGroup, value); }
        }

        [Browsable(false)]
        public XPCollection<ChartOfAccount> AvailableChartOfAccount
        {
            get
            {
                if (this.Company != null && this.Workplace != null && AccountGroup != null)
                {
                    XPCollection<ChartOfAccount> _availableChtOfAcct = new XPCollection<ChartOfAccount>
                                                               (Session, new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("Company", this.Company),
                                                                new BinaryOperator("Workplace", this.Workplace),
                                                                new BinaryOperator("AccountGroup", this.AccountGroup),
                                                                new BinaryOperator("Active", true)));
                    if(_availableChtOfAcct != null && _availableChtOfAcct.Count() > 0)
                    {
                        _availableChartOfAccount = _availableChtOfAcct;
                    }
                }
                else
                {
                    if(this.Company != null && this.Workplace != null)
                    {
                        XPCollection<ChartOfAccount> _availableChtOfAcct = new XPCollection<ChartOfAccount>
                                                               (Session, new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("Company", this.Company),
                                                                new BinaryOperator("Workplace", this.Workplace),
                                                                new BinaryOperator("Active", true)));
                        if(_availableChtOfAcct != null && _availableChtOfAcct.Count() > 0)
                        {
                            _availableChartOfAccount = _availableChtOfAcct;
                        }
                    }else
                    {
                        _availableChartOfAccount = new XPCollection<ChartOfAccount>(Session);
                    }
                }
                return _availableChartOfAccount;

            }
        }

        [ImmediatePostData()]
        [DataSourceProperty("AvailableChartOfAccount", DataSourcePropertyIsNullMode.SelectAll)]
        [Appearance("AccountReclassJournalLineAccountClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public ChartOfAccount Account
        {
            get { return _account; }
            set { SetPropertyValue("Account", ref _account, value); }
        }

        [Appearance("AccountReclassJournalLineOpenDebitClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [ImmediatePostData()]
        public bool OpenDebit
        {
            get { return _openDebit; }
            set
            {
                SetPropertyValue("CloseDebit", ref _openDebit, value);
                if (!IsLoading)
                {
                    if (this._openDebit == true)
                    {
                        this.OpenCredit = false;
                        this.Credit = 0;
                    }
                    else
                    {
                        this.Debit = 0;
                    }
                }
            }
        }

        [Appearance("AccountReclassJournalLineDebitClose1", Criteria = "OpenDebit = false", Enabled = false)]
        [Appearance("AccountReclassJournalLineDebitClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [ImmediatePostData()]
        public double Debit
        {
            get { return _debit; }
            set { SetPropertyValue("Debit", ref _debit, value); }
        }

        [Appearance("AccountReclassJournalLineOpenCreditClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [ImmediatePostData()]
        public bool OpenCredit
        {
            get { return _openCredit; }
            set
            {
                SetPropertyValue("OpenCredit", ref _openCredit, value);
                if (!IsLoading)
                {
                    if (this._openCredit == true)
                    {
                        this.OpenDebit = false;
                        this.Debit = 0;
                    }
                    else
                    {
                        this.Credit = 0;
                    }
                }
            }
        }

        [Appearance("AccountReclassJournalLineCreditClose1", Criteria = "OpenCredit = false", Enabled = false)]
        [Appearance("AccountReclassJournalLineCreditClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [ImmediatePostData()]
        public double Credit
        {
            get { return _credit; }
            set { SetPropertyValue("Credit", ref _credit, value); }
        }

        [ImmediatePostData()]
        [ModelDefault("DisplayFormat", "{0:dd/MM/yyyy}")]
        [ModelDefault("EditMask", "dd/MM/yyyy")]
        [Appearance("AccountReclassJournalLineDocDateClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public DateTime DocDate
        {
            get { return _docDate; }
            set
            {
                SetPropertyValue("DocDate", ref _docDate, value);
                if (!IsLoading)
                {
                    SetAccountingPeriodic();
                }
            }
        }

        [ImmediatePostData()]
        [Appearance("AccountReclassJournalLineJournalMonthClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public int JournalMonth
        {
            get { return _journalMonth; }
            set
            {
                SetPropertyValue("JournalMonth", ref _journalMonth, value);
                if (!IsLoading)
                {
                    if (this._journalMonth < 0)
                    {
                        this._journalMonth = 0;
                    }
                    if (this._journalMonth > 12)
                    {
                        this._journalMonth = 12;
                    }
                }
            }
        }

        [ImmediatePostData()]
        [Appearance("AccountReclassJournalLineJournalYearClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public int JournalYear
        {
            get { return _journalYear; }
            set
            {
                SetPropertyValue("JournalYear", ref _journalYear, value);
                if (!IsLoading)
                {
                    if (this._journalYear < 0)
                    {
                        this._journalYear = 0;
                    }
                    if (this._journalYear > 9999)
                    {
                        this._journalYear = 9999;
                    }
                }
            }
        }

        [Appearance("AccountReclassJournalLineStatusEnabled", Enabled = false)]
        public Status Status
        {
            get { return _status; }
            set { SetPropertyValue("Status", ref _status, value); }
        }

        [ModelDefault("DisplayFormat", "{0:dd/MM/yyyy}")]
        [ModelDefault("EditMask", "dd/MM/yyyy")]
        [Appearance("AccountReclassJournalLineStatusDateEnabled", Enabled = false)]
        public DateTime StatusDate
        {
            get { return _statusDate; }
            set { SetPropertyValue("StatusDate", ref _statusDate, value); }
        }

        [ImmediatePostData()]
        [RuleRequiredField(DefaultContexts.Save)]
        [Association("AccountReclassJournal-AccountReclassJournalLines")]
        [Appearance("AccountReclassJournalLineAccountReclassJournalEnabled", Enabled = false)]
        public AccountReclassJournal AccountReclassJournal
        {
            get { return _accountReclassJournal; }
            set
            {
                SetPropertyValue("AccountReclassJournal", ref _accountReclassJournal, value);
                if (!IsLoading)
                {
                    if (this._accountReclassJournal != null)
                    {
                        this.PostingType = this._accountReclassJournal.PostingType;
                        this.PostingMethod = this._accountReclassJournal.PostingMethod;
                        this.PostingMethodType = this._accountReclassJournal.PostingMethodType;
                        if (this._accountReclassJournal.Company != null) { this.Company = this._accountReclassJournal.Company; }
                        if (this._accountReclassJournal.Workplace != null) { this.Workplace = this._accountReclassJournal.Workplace; }
                        if (this._accountReclassJournal.Division != null) { this.Division = this._accountReclassJournal.Division; }
                        if (this._accountReclassJournal.Department != null) { this.Department = this._accountReclassJournal.Department; }
                        if (this._accountReclassJournal.Section != null) { this.Section = this._accountReclassJournal.Section; }
                        if (this._accountReclassJournal.Employee != null) { this.Employee = this._accountReclassJournal.Employee; }
                        if (this._accountReclassJournal.Description != null) { this.Description = this._accountReclassJournal.Description; }
                        this.DocDate = _accountReclassJournal.DocDate;
                        this.JournalMonth = _accountReclassJournal.JournalMonth;
                        this.JournalYear = _accountReclassJournal.JournalYear;
                    }
                }
            }
        }

        //AuditTrail
        [CollectionOperationSet(AllowAdd = false, AllowRemove = true)]
        public XPCollection<AuditDataItemPersistent> ChangeHistory
        {
            get
            {
                if (changeHistory == null)
                {
                    changeHistory = AuditedObjectWeakReference.GetAuditTrail(Session, this);
                }
                return changeHistory;
            }
        }

        #endregion Field

        //=========================================== Code In Here ===========================================

        #region No

        public void UpdateNo()
        {
            try
            {
                if (!IsLoading && Session.IsNewObject(this))
                {
                    if (this.AccountReclassJournal != null)
                    {
                        object _makRecord = Session.Evaluate<AccountReclassJournalLine>(CriteriaOperator.Parse("Max(No)"), CriteriaOperator.Parse("AccountReclassJournal=?", this.AccountReclassJournal));
                        this.No = Convert.ToInt32(_makRecord) + 1;
                        this.Save();
                        RecoveryUpdateNo();
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = AccountReclassJournalLine " + ex.ToString());
            }
        }

        public void RecoveryUpdateNo()
        {
            try
            {
                if (this.AccountReclassJournal != null)
                {
                    AccountReclassJournal _numHeader = Session.FindObject<AccountReclassJournal>
                                                (new BinaryOperator("Code", this.AccountReclassJournal.Code));

                    XPCollection<AccountReclassJournalLine> _numLines = new XPCollection<AccountReclassJournalLine>
                                                (Session, new BinaryOperator("AccountReclassJournal", _numHeader),
                                                new SortProperty("No", DevExpress.Xpo.DB.SortingDirection.Ascending));
                    if (_numLines != null)
                    {
                        int i1 = 0;
                        foreach (AccountReclassJournalLine _numLine in _numLines)
                        {
                            i1 += 1;
                            _numLine.No = i1;
                            _numLine.Save();
                        }
                        i1 = 1;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = AccountReclassJournalLine " + ex.ToString());
            }
        }

        public void RecoveryDeleteNo()
        {
            try
            {
                if (this.AccountReclassJournal != null)
                {
                    AccountReclassJournal _numHeader = Session.FindObject<AccountReclassJournal>
                                                (new BinaryOperator("Code", this.AccountReclassJournal.Code));

                    XPCollection<AccountReclassJournalLine> _numLines = new XPCollection<AccountReclassJournalLine>
                                                (Session, new GroupOperator(GroupOperatorType.And,
                                                 new BinaryOperator("This", this, BinaryOperatorType.NotEqual),
                                                 new BinaryOperator("AccountReclassJournal", _numHeader)),
                                                 new SortProperty("No", DevExpress.Xpo.DB.SortingDirection.Ascending));
                    if (_numLines != null)
                    {
                        int i = 0;
                        foreach (AccountReclassJournalLine _numLine in _numLines)
                        {
                            i += 1;
                            _numLine.No = i;
                            _numLine.Save();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = AccountReclassJournalLine " + ex.ToString());
            }
        }

        #endregion No

        private void SetAccountingPeriodic()
        {
            try
            {
                DateTime now = DateTime.Now;
                if (this.Company != null && this.Workplace != null)
                {
                    AccountingPeriodic _locAcctPeriodic = Session.FindObject<AccountingPeriodic>
                                                        (new GroupOperator(GroupOperatorType.And,
                                                        new BinaryOperator("Company", this.Company),
                                                        new BinaryOperator("Workplace", this.Workplace),
                                                        new BinaryOperator("Active", true)));
                    if (_locAcctPeriodic != null)
                    {
                        this.JournalMonth = _locAcctPeriodic.Month;
                        this.JournalYear = _locAcctPeriodic.Year;
                    }
                    else
                    {
                        this.JournalMonth = 0;
                        this.JournalYear = 0;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Business Object = AccountReclassJournal ", ex.ToString());
            }
        }

    }
}