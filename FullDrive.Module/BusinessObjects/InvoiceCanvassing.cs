﻿using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;

using DevExpress.ExpressApp.Editors;
using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.ConditionalAppearance;

namespace FullDrive.Module.BusinessObjects
{
    [DefaultClassOptions]
    [DefaultProperty("Code")]
    [NavigationItem("Sales")]
    [RuleCombinationOfPropertiesIsUnique("InvoiceCanvassingRuleUnique", DefaultContexts.Save, "Code")]
    //[ImageName("BO_Contact")]
    //[DefaultProperty("DisplayMemberNameForLookupEditorsOfThisType")]
    //[DefaultListViewOptions(MasterDetailMode.ListViewOnly, false, NewItemRowPosition.None)]
    //[Persistent("DatabaseTableName")]
    // Specify more UI options using a declarative approach (https://documentation.devexpress.com/#eXpressAppFramework/CustomDocument112701).
    public class InvoiceCanvassing : BaseObject
    { // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).

        private bool _activationPosting;
        private string _code;
        #region InitialOrganization
        private Company _company;
        private Workplace _workplace;
        private Division _division;
        private Department _department;
        private Section _section;
        private Employee _employee;
        #endregion InitialOrganization
        private Employee _salesman;
        private Vehicle _vehicle;
        private BeginningInventory _beginningInventory;
        private Currency _currency;
        private bool _mobile;
        private bool _isRoute;
        private SalesmanCustomerSetup _selectCustomer;
        private Route _selectRoute;
        private XPCollection<BusinessPartner> _availableCustomer;
        private BusinessPartner _customer;
        private string _contact;
        private string _address;
        private PriceGroup _priceGroup;
        private double _totAmount;
        private double _totTaxAmount;
        private double _totDiscAmount;
        private double _amount;
        private PaymentType _paymentType;
        private Status _status;
        private DateTime _statusDate;
        private string _userAccessIn;
        private GlobalFunction _globFunc;
        //AuditTrail
        private XPCollection<AuditDataItemPersistent> changeHistory;

        public InvoiceCanvassing(Session session)
            : base(session)
        {
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if (!IsLoading && !IsSaving && !IsInvalidated)
            {
                if (Session.IsNewObject(this))
                {
                    _globFunc = new GlobalFunction();
                    DateTime now = DateTime.Now;
                    #region UserAccess
                    _userAccessIn = SecuritySystem.CurrentUserName;
                    UserAccess _locUserAccess = Session.FindObject<UserAccess>(new BinaryOperator("UserName", _userAccessIn));
                    if (_locUserAccess != null)
                    {
                        if (_locUserAccess.Employee != null)
                        {
                            if(_locUserAccess.Employee.SalesRole == SalesRole.Canvassing || _locUserAccess.Employee.SalesRole == SalesRole.All)
                            {
                                if (_locUserAccess.Mobile == true)
                                {
                                    this.Mobile = _locUserAccess.Mobile;
                                }
                                else
                                {
                                    if (_locUserAccess.Employee.Company != null && _locUserAccess.Employee.Workplace != null)
                                    {
                                        this.Code = _globFunc.GetNumberingUnlockOptimisticRecordBasedOrganization(this.Session.DataLayer, ObjectList.InvoiceCanvassing, _locUserAccess.Employee.Company, _locUserAccess.Employee.Workplace);
                                    }
                                    else
                                    {
                                        this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.InvoiceCanvassing);
                                    }
                                }

                                if (_locUserAccess.Employee.Company != null)
                                {
                                    this.Company = _locUserAccess.Employee.Company;
                                }
                                if (_locUserAccess.Employee.Workplace != null)
                                {
                                    this.Workplace = _locUserAccess.Employee.Workplace;
                                }
                                else
                                {
                                    this.Workplace = _globFunc.GetDefaultWorkplace(this.Session, _locUserAccess);
                                }
                                if (_locUserAccess.Employee.Division != null)
                                {
                                    Division = _locUserAccess.Employee.Division;
                                }
                                if (_locUserAccess.Employee.Department != null)
                                {
                                    Department = _locUserAccess.Employee.Department;
                                }
                                if (_locUserAccess.Employee.Section != null)
                                {
                                    Section = _locUserAccess.Employee.Section;
                                }

                                this.Employee = _locUserAccess.Employee;

                                #region SalesmanSetup

                                SalesmanVehicleSetup _locSalesmanVehicleSetup = Session.FindObject<SalesmanVehicleSetup>(
                                                                    new GroupOperator(GroupOperatorType.And,
                                                                    new BinaryOperator("UserAccess", _locUserAccess),
                                                                    new BinaryOperator("Salesman", _locUserAccess.Employee),
                                                                    new BinaryOperator("Active", true)));
                                if (_locSalesmanVehicleSetup != null)
                                {
                                    
                                    if (_locSalesmanVehicleSetup.Salesman != null)
                                    {
                                        this.Salesman = _locSalesmanVehicleSetup.Salesman;
                                    }
                                    if (_locSalesmanVehicleSetup.Vehicle != null)
                                    {
                                        this.Vehicle = _locSalesmanVehicleSetup.Vehicle;
                                        BeginningInventory _locBegInv = Session.FindObject<BeginningInventory>
                                                                        (new GroupOperator(GroupOperatorType.And,
                                                                        new BinaryOperator("Company", this.Company),
                                                                        new BinaryOperator("Workplace", this.Workplace),
                                                                        new BinaryOperator("Vehicle", _locSalesmanVehicleSetup.Vehicle),
                                                                        new BinaryOperator("StockType", StockType.Good),
                                                                        new BinaryOperator("Active", true)));
                                        if (_locBegInv != null)
                                        {
                                            this.BeginningInventory = _locBegInv;
                                        }
                                    }
                                }

                                #endregion SalesmanSetup
                            }
                        }
                        
                    }
                    #endregion UserAccess
                    this.Status = CustomProcess.Status.Open;
                    this.StatusDate = now;
                    this.Currency = _globFunc.GetDefaultCurrency(this.Session);
                    this.PaymentType = PaymentType.Cash;
                    this.IsRoute = true;
                }
            }
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).
        }

        protected override void OnSaving()
        {
            base.OnSaving();
            if (!IsLoading)
            {
                if (this.Mobile == true)
                {
                    if (Session.IsNewObject(this))
                    {
                        if (!(Session is NestedUnitOfWork))
                        {
                            if(this.Code == null)
                            {
                                this.Code = "ICNM - " + DistributedIdGeneratorHelper.Generate(this.Session.DataLayer, this.GetType().FullName, string.Empty);
                            } 
                        }
                    }  
                }
            }
        }

        #region Field

        [Browsable(false)]
        [ImmediatePostData()]
        public bool ActivationPosting
        {
            get { return _activationPosting; }
            set { SetPropertyValue("ActivationPosting", ref _activationPosting, value); }
        }

        [Appearance("InvoiceCanvassingCodeClose", Enabled = false)]
        //[RuleRequiredField(DefaultContexts.Save)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue("Code", ref _code, value); }
        }

        #region Organization

        [Browsable(false)]
        [ImmediatePostData()]
        [Appearance("InvoiceCanvassingCompanyClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Company Company
        {
            get { return _company; }
            set { SetPropertyValue("Company", ref _company, value); }
        }

        [Browsable(false)]
        [ImmediatePostData()]
        [Appearance("InvoiceCanvassingWorkplaceClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Workplace Workplace
        {
            get { return _workplace; }
            set { SetPropertyValue("Workplace", ref _workplace, value); }
        }

        [Browsable(false)]
        public Division Division
        {
            get { return _division; }
            set { SetPropertyValue("Division", ref _division, value); }
        }

        [Browsable(false)]
        public Department Department
        {
            get { return _department; }
            set { SetPropertyValue("Department", ref _department, value); }
        }

        [Browsable(false)]
        public Section Section
        {
            get { return _section; }
            set { SetPropertyValue("Section", ref _section, value); }
        }

        [Browsable(false)]
        public Employee Employee
        {
            get { return _employee; }
            set { SetPropertyValue("Employee", ref _employee, value); }
        }

        #endregion Organization

        [Browsable(false)]
        [ImmediatePostData()]
        [Appearance("InvoiceCanvassingSalesmanClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Employee Salesman
        {
            get { return _salesman; }
            set { SetPropertyValue("Salesman", ref _salesman, value); }
        }

        [Browsable(false)]
        [ImmediatePostData()]
        [Appearance("InvoiceCanvassingVehicleClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Vehicle Vehicle
        {
            get { return _vehicle; }
            set { SetPropertyValue("Vehicle", ref _vehicle, value); }
        }

        [Browsable(false)]
        [ImmediatePostData()]
        [Appearance("InvoiceCanvassingBeginningInventoryClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public BeginningInventory BeginningInventory
        {
            get { return _beginningInventory; }
            set { SetPropertyValue("BeginningInventory", ref _beginningInventory, value); }
        }

        [Browsable(false)]
        [Appearance("InvoiceCanvassingCurrencyClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Currency Currency
        {
            get { return _currency; }
            set { SetPropertyValue("Currency", ref _currency, value); }
        }

        [Browsable(false)]
        [ImmediatePostData()]
        public bool Mobile
        {
            get { return _mobile; }
            set { SetPropertyValue("Mobile", ref _mobile, value); }
        }

        [ImmediatePostData()]
        public bool IsRoute
        {
            get { return _isRoute; }
            set { SetPropertyValue("IsRoute", ref _isRoute, value); }
        }
        
        [NonPersistent]
        [ImmediatePostData()]
        [DataSourceCriteria("Company = '@This.Company' AND Workplace = '@This.Workplace' AND Salesman = '@This.Salesman'")]
        [Appearance("InvoiceCanvassingSelectCustomerClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [Appearance("InvoiceCanvassingSelectCustomerClose2", Criteria = "Mobile = false", Enabled = false)]
        [Appearance("InvoiceCanvassingSelectCustomerClose3", Criteria = "IsRoute = true", Enabled = false)]
        public SalesmanCustomerSetup SelectCustomer
        {
            get { return _selectCustomer; }
            set {
                SetPropertyValue("CustomerChoose", ref _selectCustomer, value);
                if(!IsLoading )
                {
                    if(this._selectCustomer != null)
                    {
                        if(this.SelectCustomer.Customer != null)
                        {
                            this.Customer = this._selectCustomer.Customer;
                            if (this._selectCustomer.Customer.Contact != null)
                            {
                                this.Contact = this._selectCustomer.Customer.Contact;
                            }
                            if (this._selectCustomer.Customer.Address != null)
                            {
                                this.Address = this._selectCustomer.Customer.Address;
                            }
                            if (this._selectCustomer.Customer.PriceGroup != null)
                            {
                                this.PriceGroup = this._selectCustomer.Customer.PriceGroup;
                            }
                        }
                        else
                        {
                            this.Customer = this._selectCustomer.Customer;
                            if (this._selectCustomer.Customer.Contact != null)
                            {
                                this.Contact = null;
                            }
                            if (this._selectCustomer.Customer.Address != null)
                            {
                                this.Address = null;
                            }
                            if (this._selectCustomer.Customer.PriceGroup != null)
                            {
                                this.PriceGroup = null;
                            }
                        }
                          
                    }
                    else
                    {
                        this.Customer = null;
                        this.Contact = null;
                        this.Address = null;
                        this.PriceGroup = null;
                    }
                }
            }
        }

        [ImmediatePostData()]
        [NonPersistent]
        [ModelDefault("LookupProperty", "Customer")]
        [DataSourceCriteria("Company = '@This.Company' AND Workplace = '@This.Workplace' AND Salesman = '@This.Salesman' AND Active = true")]
        [Appearance("InvoiceCanvassingSelectRouteClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [Appearance("InvoiceCanvassingSelectRouteClose2", Criteria = "IsRoute = true", Enabled = true)]
        [Appearance("InvoiceCanvassingSelectRouteClose3", Criteria = "IsRoute = false", Enabled = false)]
        public Route SelectRoute
        {
            get { return _selectRoute; }
            set {
                SetPropertyValue("SelectRoute", ref _selectRoute, value);
                if (!IsLoading)
                {
                    if (this._selectRoute.StartOn  != null)
                    {
                        if (this._selectRoute.Customer != null)
                        {
                            this.Customer = this._selectRoute.Customer;
                            if (this._selectRoute.Customer.Contact != null)
                            {
                                this.Contact = this._selectRoute.Customer.Contact;
                            }
                            if (this._selectRoute.Customer.Address != null)
                            {
                                this.Address = this._selectRoute.Customer.Address;
                            }
                            if (this._selectRoute.Customer.PriceGroup != null)
                            {
                                this.PriceGroup = this._selectRoute.Customer.PriceGroup;
                            }
                        }
                        else
                        {
                            this.Customer = this._selectRoute.Customer;
                            if (this._selectRoute.Customer.Contact != null)
                            {
                                this.Contact = null;
                            }
                            if (this._selectRoute.Customer.Address != null)
                            {
                                this.Address = null;
                            }
                            if (this._selectRoute.Customer.PriceGroup != null)
                            {
                                this.PriceGroup = null;
                            }
                        }

                    }
                    else
                    {
                        this.Customer = null;
                        this.Contact = null;
                        this.Address = null;
                        this.PriceGroup = null;
                    }
                }
            }
        }

        [Browsable(false)]
        public XPCollection<BusinessPartner> AvailableCustomer
        {
            get
            {
                string _beginString = null;
                string _endString = null;
                string _fullString = null;

                if(this.Company != null && this.Workplace != null && this.Salesman != null)
                {
                    List<string> _stringSCS = new List<string>();

                    XPCollection<SalesmanCustomerSetup> _locSalesmanCustomerSetups = new XPCollection<SalesmanCustomerSetup>
                                                                              (Session, new GroupOperator(GroupOperatorType.And,
                                                                               new BinaryOperator("Company", this.Company),
                                                                               new BinaryOperator("Workplace", this.Workplace),
                                                                               new BinaryOperator("Salesman", this.Salesman),
                                                                               new BinaryOperator("Active", true)));

                    if (_locSalesmanCustomerSetups != null && _locSalesmanCustomerSetups.Count() > 0)
                    {
                        foreach (SalesmanCustomerSetup _locSalesmanCustomerSetup in _locSalesmanCustomerSetups)
                        {
                            if(_locSalesmanCustomerSetup.Customer != null)
                            {
                                if(_locSalesmanCustomerSetup.Customer.Code != null)
                                {
                                    _stringSCS.Add(_locSalesmanCustomerSetup.Customer.Code);
                                }
                            }   
                        }
                    }

                    IEnumerable<string> _stringArraySCSDistinct = _stringSCS.Distinct();
                    string[] _stringArraySCSList = _stringArraySCSDistinct.ToArray();
                    if (_stringArraySCSList.Length == 1)
                    {
                        for (int i = 0; i < _stringArraySCSList.Length; i++)
                        {
                            BusinessPartner _locBP = Session.FindObject<BusinessPartner>(new BinaryOperator("Code", _stringArraySCSList[i]));
                            if (_locBP != null)
                            {
                                if (i == 0)
                                {
                                    _beginString = "[Code]=='" + _locBP.Code + "'";
                                }
                            }
                        }
                    }
                    else if (_stringArraySCSList.Length > 1)
                    {
                        for (int i = 0; i < _stringArraySCSList.Length; i++)
                        {
                            BusinessPartner _locBP = Session.FindObject<BusinessPartner>(new BinaryOperator("Code", _stringArraySCSList[i]));
                            if (_locBP != null)
                            {
                                if (i == 0)
                                {
                                    _beginString = "[Code]=='" + _locBP.Code + "'";
                                }
                                else
                                {
                                    _endString = _endString + " OR [Code]=='" + _locBP.Code + "'";
                                }
                            }
                        }
                    }
                    _fullString = _beginString + _endString;

                    if (_fullString != null)
                    {
                        _availableCustomer = new XPCollection<BusinessPartner>(Session, CriteriaOperator.Parse(_fullString));
                    }
                }
                else
                {
                    _availableCustomer = new XPCollection<BusinessPartner>(Session, new GroupOperator(GroupOperatorType.And,
                                                                            new BinaryOperator("Active", true)));
                }

                return _availableCustomer;

            }
        }

        [ImmediatePostData()]
        [DataSourceProperty("AvailableCustomer", DataSourcePropertyIsNullMode.SelectAll)]
        [Appearance("InvoiceCanvassingCustomerClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [Appearance("InvoiceCanvassingCustomerClose2", Criteria = "Mobile = true", Enabled = false)]
        [Appearance("InvoiceCanvassingCustomerClose3", Criteria = "IsRoute = true", Enabled = false)]
        public BusinessPartner Customer
        {
            get { return _customer; }
            set {
                SetPropertyValue("Customer", ref _customer, value);
                if(!IsLoading)
                {
                    if(this._customer != null)
                    {
                        if(this._customer.Contact != null)
                        {
                            this.Contact = this._customer.Contact;
                        }
                        if(this._customer.Address != null)
                        {
                            this.Address = this._customer.Address;
                        }
                        if (this._customer.PriceGroup != null)
                        {
                            this.PriceGroup = this._customer.PriceGroup;
                        }
                    }
                    else
                    {
                        this.Contact = null;
                        this.Address = null;
                        this.PriceGroup = null;
                    }
                }
            }
        }

        [Appearance("InvoiceCanvassingContactClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public string Contact
        {
            get { return _contact; }
            set { SetPropertyValue("Contact", ref _contact, value); }
        }

        [Appearance("InvoiceCanvassingAddressClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public string Address
        {
            get { return _address; }
            set { SetPropertyValue("Address", ref _address, value); }
        }

        [Appearance("InvoiceCanvassingPriceGroupClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public PriceGroup PriceGroup
        {
            get { return _priceGroup; }
            set { SetPropertyValue("PriceGroup", ref _priceGroup, value); }
        }

        [Appearance("InvoiceCanvassingTotAmountClose", Enabled = false)]
        public double TotAmount
        {
            get { return _totAmount; }
            set { SetPropertyValue("TotAmount", ref _totAmount, value); }
        }

        [Appearance("InvoiceCanvassingTotTaxAmountClose", Enabled = false)]
        public double TotTaxAmount
        {
            get { return _totTaxAmount; }
            set { SetPropertyValue("TotTaxAmount", ref _totTaxAmount, value); }
        }

        [Appearance("InvoiceCanvassingTotDiscAmountClose", Enabled = false)]
        public double TotDiscAmount
        {
            get { return _totDiscAmount; }
            set { SetPropertyValue("TotDiscAmount", ref _totDiscAmount, value); }
        }

        [Appearance("InvoiceCanvassingAmountClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public double Amount
        {
            get { return _amount; }
            set { SetPropertyValue("Amount", ref _amount, value); }
        }

        [Appearance("InvoiceCanvassingPaymentTypeClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public PaymentType PaymentType
        {
            get { return _paymentType; }
            set { SetPropertyValue("PaymentType", ref _paymentType, value); }
        }

        [Appearance("InvoiceCanvassingStatusClose", Enabled = false)]
        public Status Status
        {
            get { return _status; }
            set { SetPropertyValue("Status", ref _status, value); }
        }

        [Appearance("InvoiceCanvassingStatusDateClose", Enabled = false)]
        public DateTime StatusDate
        {
            get { return _statusDate; }
            set { SetPropertyValue("StatusDate", ref _statusDate, value); }
        }

        [Association("InvoiceCanvassing-InvoiceCanvassingLines")]
        public XPCollection<InvoiceCanvassingLine> InvoiceCanvassingLines
        {
            get { return GetCollection<InvoiceCanvassingLine>("InvoiceCanvassingLines"); }
        }

        [Association("InvoiceCanvassing-InvoiceCanvassingFreeItems")]
        public XPCollection<InvoiceCanvassingFreeItem> InvoiceCanvassingFreeItems
        {
            get { return GetCollection<InvoiceCanvassingFreeItem>("InvoiceCanvassingFreeItems"); }
        }

        //AuditTrail
        [CollectionOperationSet(AllowAdd = false, AllowRemove = true)]
        public XPCollection<AuditDataItemPersistent> ChangeHistory
        {
            get
            {
                if (changeHistory == null)
                {
                    changeHistory = AuditedObjectWeakReference.GetAuditTrail(Session, this);
                }
                return changeHistory;
            }
        }

        #endregion Field
    }
}