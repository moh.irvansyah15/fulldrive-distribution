﻿using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;

using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.ConditionalAppearance;
using DevExpress.ExpressApp.Editors;

namespace FullDrive.Module.BusinessObjects
{
    [DefaultClassOptions]
    [DefaultProperty("Code")]
    [NavigationItem("Finance & Accounting")]
    [RuleCombinationOfPropertiesIsUnique("PayableTransactionLineRuleUnique", DefaultContexts.Save, "Code")]
    //[ImageName("BO_Contact")]
    //[DefaultProperty("DisplayMemberNameForLookupEditorsOfThisType")]
    //[DefaultListViewOptions(MasterDetailMode.ListViewOnly, false, NewItemRowPosition.None)]
    //[Persistent("DatabaseTableName")]
    // Specify more UI options using a declarative approach (https://documentation.devexpress.com/#eXpressAppFramework/CustomDocument112701).
    public class PayableTransactionLine : BaseObject
    { // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).
        private bool _activationPosting;
        private int _no;
        private string _code;
        private bool _select;
        private Currency _currency;
        private DateTime _postingDate;
        private PostingType _purchaseType;
        private PostingMethod _postingMethod;
        private XPCollection<BankAccount> _availableBankAccount;
        private bool _openCompany;
        private Company _company;
        private bool _openVendor;
        private BusinessPartner _vendor;
        private bool _multiBank;
        private BankAccount _bankAccount;
        private string _accountNo;
        private string _accountName;
        private ChartOfAccount _account;
        private string _description;
        private bool _closeDebit;
        private double _debit;
        private bool _closeCredit;
        private double _credit;
        private DateTime _docDate;
        private int _journalMonth;
        private int _journalYear;
        private Status _status;
        private DateTime _statusDate;
        #region InitialOrganization
        private Company _companyDefault;
        private Workplace _workplace;
        private Division _division;
        private Department _department;
        private Section _section;
        private Employee _employee;
        #endregion InitialOrganization
        private PurchaseInvoice _purchaseInvoice;
        private PurchasePrePaymentInvoice _purchasePrePaymentInvoice;
        private PurchaseOrder _purchaseOrder;
        private PayableTransaction _payableTransaction;
        private ExchangeRate _exchangeRate;
        private PurchaseInvoiceMonitoring _purchaseInvoiceMonitoring;
        private PurchasePrePaymentInvoiceMonitoring _purchasePrePaymentInvoiceMonitoring;
        private string _localUserAccess;
        private GlobalFunction _globFunc;
        //AuditTrail
        private XPCollection<AuditDataItemPersistent> changeHistory;

        public PayableTransactionLine(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if (!IsLoading)
            {
                if(Session.IsNewObject(this))
                {
                    _globFunc = new GlobalFunction();
                    DateTime now = DateTime.Now;
                    #region UserAccess
                    _localUserAccess = SecuritySystem.CurrentUserName;
                    UserAccess _locUserAccess = Session.FindObject<UserAccess>(new BinaryOperator("UserName", _localUserAccess));
                    if (_locUserAccess != null)
                    {
                        if (_locUserAccess.Employee != null)
                        {
                            if (_locUserAccess.Employee.Company != null && _locUserAccess.Employee.Workplace != null)
                            {
                                this.Code = _globFunc.GetNumberingUnlockOptimisticRecordBasedOrganization(this.Session.DataLayer, ObjectList.PayableTransactionLine, _locUserAccess.Employee.Company, _locUserAccess.Employee.Workplace);
                            }
                            else
                            {
                                this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.PayableTransactionLine);
                            }
                        }
                    }
                    #endregion UserAccess
                    this.Status = Status.Open;
                    this.StatusDate = now;
                    this.Select = true;
                    this.PurchaseType = PostingType.Purchase;
                    this.PostingMethod = PostingMethod.Payment;
                }               
            }
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).
        }

        protected override void OnSaving()
        {
            base.OnSaving();
            if (!IsLoading && IsSaving)
            {
                UpdateNo();
            }
        }

        protected override void OnDeleting()
        {
            base.OnDeleting();
            if (!IsLoading)
            {
                RecoveryDeleteNo();
            }
        }

        #region Field

        [Browsable(false)]
        public bool ActivationPosting
        {
            get { return _activationPosting; }
            set { SetPropertyValue("ActivationPosting", ref _activationPosting, value); }
        }

        [Appearance("PayableTransactionLineNoClose", Enabled = false)]
        public int No
        {
            get { return _no; }
            set { SetPropertyValue("No", ref _no, value); }
        }

        [VisibleInListView(false)]
        [RuleRequiredField(DefaultContexts.Save)]
        [Appearance("PayableTransactionLineCodeClose", Enabled = false)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue("Code", ref _code, value); }
        }

        [Appearance("PayableTransactionLineSelectClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public bool Select
        {
            get { return _select; }
            set { SetPropertyValue("Select", ref _select, value); }
        }

        [VisibleInListView(false)]
        [Appearance("PayableTransactionLineCurrencyClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Currency Currency
        {
            get { return _currency; }
            set { SetPropertyValue("Currency", ref _currency, value); }
        }

        [Appearance("PayableTransactionLinePostingDateClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [ModelDefault("DisplayFormat", "{0:dd/MM/yyyy}")]
        [ModelDefault("EditMask", "dd/MM/yyyy")]
        public DateTime PostingDate
        {
            get { return _postingDate; }
            set { SetPropertyValue("PostingDate", ref _postingDate, value); }
        }

        [Appearance("PayableTransactionLinePostingTypeClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public PostingType PurchaseType
        {
            get { return _purchaseType; }
            set { SetPropertyValue("PurchaseType", ref _purchaseType, value); }
        }

        [Appearance("PayableTransactionLinePostingMethodClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public PostingMethod PostingMethod
        {
            get { return _postingMethod; }
            set { SetPropertyValue("PostingMethod", ref _postingMethod, value); }
        }

        [Browsable(false)]
        [Appearance("PayableTransactionLineOpenCompanyClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [ImmediatePostData]
        public bool OpenCompany
        {
            get { return _openCompany; }
            set
            {
                SetPropertyValue("OpenCompany", ref _openCompany, value);
                if (!IsLoading)
                {
                    if (this._openCompany == true)
                    {
                        this.OpenVendor = false;
                        this.Vendor = null;
                        if (this.PayableTransaction != null)
                        {
                            if (this.PayableTransaction.CompanyDefault != null)
                            {
                                this.Company = this.PayableTransaction.CompanyDefault;
                            }
                        }
                    }
                }
            }
        }

        [Appearance("PayableTransactionLineCompanyClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [Appearance("PayableTransactionLineCompanyClose1", Criteria = "OpenCompany = false", Enabled = false)]
        [ImmediatePostData()]
        public Company Company
        {
            get { return _company; }
            set { SetPropertyValue("Company", ref _company, value); }
        }

        [Browsable(false)]
        [Appearance("PayableTransactionLineOpenVendorClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [ImmediatePostData]
        public bool OpenVendor
        {
            get { return _openVendor; }
            set
            {
                SetPropertyValue("OpenVendor", ref _openVendor, value);
                if (!IsLoading)
                {
                    if (this._openVendor == true)
                    {
                        this.OpenCompany = false;
                        this.Company = null;
                        if (this.PayableTransaction != null)
                        {
                            if (this.PayableTransaction.PayToVendor != null)
                            {
                                this.Vendor = this.PayableTransaction.PayToVendor;
                            }
                        }
                    }
                }
            }
        }

        [Appearance("PayableTransactionLineVendorClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [Appearance("PayableTransactionLineVendorClose1", Criteria = "OpenVendor = false", Enabled = false)]
        [ImmediatePostData()]
        public BusinessPartner Vendor
        {
            get { return _vendor; }
            set { SetPropertyValue("Vendor", ref _vendor, value); }
        }

        [Browsable(false)]
        public XPCollection<BankAccount> AvailableBankAccount
        {
            get
            {
                if(!IsLoading)
                {
                    if (this.Company != null && this.Vendor != null)
                    {
                        _availableBankAccount = new XPCollection<BankAccount>(Session);
                    }
                    else
                    {
                        if (this.Company != null)
                        {
                            _availableBankAccount = new XPCollection<BankAccount>(Session,
                                                    new GroupOperator(GroupOperatorType.And,
                                                    new BinaryOperator("Company", this.Company)));
                        }
                        if (this.Vendor != null)
                        {
                            _availableBankAccount = new XPCollection<BankAccount>(Session,
                                                    new GroupOperator(GroupOperatorType.And,
                                                    new BinaryOperator("BusinessPartner", this.Vendor)));
                        }
                    }
                }               
                return _availableBankAccount;
            }
        }

        [ImmediatePostData()]
        [Appearance("PayableTransactionLineMultiBankClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public bool MultiBank
        {
            get { return _multiBank; }
            set
            {
                SetPropertyValue("MultiBank", ref _multiBank, value);
                if (!IsLoading)
                {
                    if (this._multiBank == true)
                    {
                        this.BankAccount = null;
                        this.AccountNo = null;
                        this.AccountName = null;
                    }
                }
            }
        }

        [Appearance("payableTransactionLineBankAccountClose", Criteria = "MultiBank = true", Enabled = false)]
        [Appearance("PayableTransactionLineBankAccountClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [DataSourceProperty("AvailableBankAccount", DataSourcePropertyIsNullMode.SelectAll)]
        [DataSourceCriteria("Active = true")]
        public BankAccount BankAccount
        {
            get { return _bankAccount; }
            set
            {
                SetPropertyValue("BankAccount", ref _bankAccount, value);
                if (!IsLoading)
                {
                    if (this._bankAccount != null)
                    {
                        if(this._bankAccount.AccountNo != null)
                        {
                            this.AccountNo = this._bankAccount.AccountNo;
                        }
                        if(this._bankAccount.AccountName != null)
                        {
                            this.AccountName = this._bankAccount.AccountName;
                        }                        
                    }
                    else
                    {
                        this.AccountNo = null;
                        this.AccountName = null;
                    }
                }
            }
        }

        [Appearance("payableTransactionLineAccountNoClose", Criteria = "MultiBank = true", Enabled = false)]
        [Appearance("PayableTransactionLineAccountNoClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public string AccountNo
        {
            get { return _accountNo; }
            set { SetPropertyValue("AccountNo", ref _accountNo, value); }
        }

        [Appearance("payableTransactionLineAccountNameClose", Criteria = "MultiBank = true", Enabled = false)]
        [Appearance("PayableTransactionLineAccountNameClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public string AccountName
        {
            get { return _accountName; }
            set { SetPropertyValue("AccountName", ref _accountName, value); }
        }

        [Appearance("PayableTransactionLineAccountClose", Criteria = "ActivationPosting = true", Enabled = false)]
        //Membuat Based On Vendor atau Company
        public ChartOfAccount Account
        {
            get { return _account; }
            set { SetPropertyValue("Account", ref _account, value); }
        }

        [VisibleInListView(false)]
        [Appearance("PayableTransactionLineDescriptionClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public string Description
        {
            get { return _description; }
            set { SetPropertyValue("Description", ref _description, value); }
        }

        [Browsable(false)]
        [ImmediatePostData()]
        public bool CloseDebit
        {
            get { return _closeDebit; }
            set { SetPropertyValue("CloseDebit", ref _closeDebit, value); }
        }

        [Appearance("PayableTransactionLineDebitClose1", Criteria = "CloseDebit = true", Enabled = false)]
        [Appearance("PayableTransactionLineDebitClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [ImmediatePostData()]
        public double Debit
        {
            get { return _debit; }
            set
            {
                SetPropertyValue("Debit", ref _debit, value);
                if (!IsLoading)
                {
                    if (this._debit > 0)
                    {
                        this.CloseCredit = true;
                        this.CloseDebit = false;
                    }
                    else
                    {
                        this.CloseCredit = false;
                    }
                }
            }
        }

        [Browsable(false)]
        [ImmediatePostData()]
        public bool CloseCredit
        {
            get { return _closeCredit; }
            set { SetPropertyValue("CloseCredit", ref _closeCredit, value); }
        }

        [Appearance("PayableTransactionLineCreditClose1", Criteria = "CloseCredit = true", Enabled = false)]
        [Appearance("PayableTransactionLineCreditClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [ImmediatePostData()]
        public double Credit
        {
            get { return _credit; }
            set
            {
                SetPropertyValue("Credit", ref _credit, value);
                if (!IsLoading)
                {
                    if (this._credit > 0)
                    {
                        this.CloseCredit = false;
                        this.CloseDebit = true;
                    }
                    else
                    {
                        this.CloseDebit = false;
                    }
                }
            }
        }

        [Browsable(false)]
        [ImmediatePostData()]
        [ModelDefault("DisplayFormat", "{0:dd/MM/yyyy}")]
        [ModelDefault("EditMask", "dd/MM/yyyy")]
        [Appearance("PayableTransactionLineDocDateClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public DateTime DocDate
        {
            get { return _docDate; }
            set
            {
                SetPropertyValue("DocDate", ref _docDate, value);
                if (!IsLoading)
                {
                    this.JournalMonth = this._docDate.Month;
                    this.JournalYear = this._docDate.Year;
                }
            }
        }

        [Browsable(false)]
        public int JournalMonth
        {
            get { return _journalMonth; }
            set { SetPropertyValue("JournalMonth", ref _journalMonth, value); }
        }

        [Browsable(false)]
        public int JournalYear
        {
            get { return _journalYear; }
            set { SetPropertyValue("JournalYear", ref _journalYear, value); }
        }

        [VisibleInListView(false)]
        [Appearance("PayableTransactionLineStatusClose", Enabled = false)]
        public Status Status
        {
            get { return _status; }
            set { SetPropertyValue("Status", ref _status, value); }
        }

        [VisibleInListView(false)]
        [ModelDefault("DisplayFormat", "{0:dd/MM/yyyy}")]
        [ModelDefault("EditMask", "dd/MM/yyyy")]
        [Appearance("PayableTransactionLineStatusDateClose", Enabled = false)]
        public DateTime StatusDate
        {
            get { return _statusDate; }
            set { SetPropertyValue("StatusDate", ref _statusDate, value); }
        }

        #region Organization

        [VisibleInListView(false)]
        [Appearance("PayableTransactionLineCompanyDefaultClose", Enabled = false)]
        public Company CompanyDefault
        {
            get { return _companyDefault; }
            set { SetPropertyValue("CompanyDefault", ref _companyDefault, value); }
        }

        [VisibleInListView(false)]
        [Appearance("PayableTransactionLineWorkplaceClose", Enabled = false)]
        public Workplace Workplace
        {
            get { return _workplace; }
            set { SetPropertyValue("Workplace", ref _workplace, value); }
        }

        [Browsable(false)]
        public Division Division
        {
            get { return _division; }
            set { SetPropertyValue("Division", ref _division, value); }
        }

        [Browsable(false)]
        public Department Department
        {
            get { return _department; }
            set { SetPropertyValue("Department", ref _department, value); }
        }

        [Browsable(false)]
        public Section Section
        {
            get { return _section; }
            set { SetPropertyValue("Section", ref _section, value); }
        }

        [Browsable(false)]
        public Employee Employee
        {
            get { return _employee; }
            set { SetPropertyValue("Employee", ref _employee, value); }
        }

        #endregion Organization

        [VisibleInListView(false)]
        [Appearance("PayableTransactionLinePurchaseInvoiceClose", Enabled = false)]
        public PurchaseInvoice PurchaseInvoice
        {
            get { return _purchaseInvoice; }
            set { SetPropertyValue("PurchaseInvoice", ref _purchaseInvoice, value); }
        }

        [VisibleInListView(false)]
        [Appearance("PayableTransactionLinePurchaseOrderClose", Enabled = false)]
        public PurchaseOrder PurchaseOrder
        {
            get { return _purchaseOrder; }
            set { SetPropertyValue("PurchaseOrder", ref _purchaseOrder, value); }
        }

        [VisibleInListView(false)]
        [Appearance("PayableTransactionLinePurchasePrePaymentInvoiceClose", Enabled = false)]
        public PurchasePrePaymentInvoice PurchasePrePaymentInvoice
        {
            get { return _purchasePrePaymentInvoice; }
            set { SetPropertyValue("PurchasePrePaymentInvoice", ref _purchasePrePaymentInvoice, value); }
        }

        [VisibleInListView(false)]
        [Appearance("PayableTransactionLinePurchaseInvoiceMonitoringClose", Enabled = false)]
        public PurchaseInvoiceMonitoring PurchaseInvoiceMonitoring
        {
            get { return _purchaseInvoiceMonitoring; }
            set { SetPropertyValue("PurchaseInvoiceMonitoring", ref _purchaseInvoiceMonitoring, value); }
        }

        [VisibleInListView(false)]
        [Appearance("PayableTransactionLinePurchasePrePaymentInvoiceMonitoringClose", Enabled = false)]
        public PurchasePrePaymentInvoiceMonitoring PurchasePrePaymentInvoiceMonitoring
        {
            get { return _purchasePrePaymentInvoiceMonitoring; }
            set { SetPropertyValue("PurchasePrePaymentInvoiceMonitoring", ref _purchasePrePaymentInvoiceMonitoring, value); }
        }

        [VisibleInListView(false)]
        [Appearance("PayableTransactionLinePayableTransactionClose", Enabled = false)]
        [Association("PayableTransaction-PayableTransactionLines")]
        public PayableTransaction PayableTransaction
        {
            get { return _payableTransaction; }
            set
            {
                SetPropertyValue("PayableTransaction", ref _payableTransaction, value);
                if (!IsLoading)
                {
                    if (this._payableTransaction != null)
                    {
                        if (this._payableTransaction.Currency != null) { this.Currency = this._payableTransaction.Currency; }
                        if (this._payableTransaction.CompanyDefault != null) { this.CompanyDefault = this._payableTransaction.CompanyDefault; }
                        if (this._payableTransaction.Workplace != null) { this.Workplace = this._payableTransaction.Workplace; }
                        if (this._payableTransaction.Division != null) { this.Division = this._payableTransaction.Division; }
                        if (this._payableTransaction.Department != null) { this.Department = this._payableTransaction.Department; }
                        if (this._payableTransaction.Section != null) { this.Section = this._payableTransaction.Section; }
                        if (this._payableTransaction.Employee != null) { this.Employee = this._payableTransaction.Employee; }
                    }
                }
            }
        }

        [Appearance("PayableTransactionLinePayableTransactionBankClose", Criteria = "MultiBank = false", Visibility = ViewItemVisibility.Hide, Context = "DetailView")]
        [Appearance("PayableTransactionLinePayableTransactionBankEnabled", Criteria = "MultiBank = true", Visibility = ViewItemVisibility.Show, Context = "DetailView")]
        [Association("PayableTransactionLine-PayableTransactionBanks")]
        public XPCollection<PayableTransactionBank> PayableTransactionBanks
        {
            get { return GetCollection<PayableTransactionBank>("PayableTransactionBanks"); }
        }

        [Association("PayableTransactionLine-ExchangeRates")]
        public XPCollection<ExchangeRate> ExchangeRates
        {
            get { return GetCollection<ExchangeRate>("ExchangeRates"); }
        }

        //AuditTrail
        [CollectionOperationSet(AllowAdd = false, AllowRemove = true)]
        public XPCollection<AuditDataItemPersistent> ChangeHistory
        {
            get
            {
                if (changeHistory == null)
                {
                    changeHistory = AuditedObjectWeakReference.GetAuditTrail(Session, this);
                }
                return changeHistory;
            }
        }

        #endregion Field

        //========================================== Code Only ==========================================

        #region No

        public void UpdateNo()
        {
            try
            {
                if (!IsLoading && Session.IsNewObject(this))
                {
                    if (this.PayableTransaction != null)
                    {
                        object _makRecord = Session.Evaluate<PayableTransactionLine>(CriteriaOperator.Parse("Max(No)"), CriteriaOperator.Parse("PayableTransaction=?", this.PayableTransaction));
                        this.No = Convert.ToInt32(_makRecord) + 1;
                        this.Save();
                        RecoveryUpdateNo();
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PayableTransactionLine " + ex.ToString());
            }
        }

        public void RecoveryUpdateNo()
        {
            try
            {
                if (this.PayableTransaction != null)
                {
                    PayableTransaction _numHeader = Session.FindObject<PayableTransaction>
                                                (new BinaryOperator("Code", this.PayableTransaction.Code));

                    XPCollection<PayableTransactionLine> _numLines = new XPCollection<PayableTransactionLine>
                                                (Session, new BinaryOperator("PayableTransaction", _numHeader),
                                                new SortProperty("No", DevExpress.Xpo.DB.SortingDirection.Ascending));
                    if (_numLines != null)
                    {
                        int i1 = 0;
                        foreach (PayableTransactionLine _numLine in _numLines)
                        {
                            i1 += 1;
                            _numLine.No = i1;
                            _numLine.Save();
                        }
                        i1 = 1;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PayableTransactionLine " + ex.ToString());
            }
        }

        public void RecoveryDeleteNo()
        {
            try
            {
                if (this.PayableTransaction != null)
                {
                    PayableTransaction _numHeader = Session.FindObject<PayableTransaction>
                                                (new BinaryOperator("Code", this.PayableTransaction.Code));

                    XPCollection<PayableTransactionLine> _numLines = new XPCollection<PayableTransactionLine>
                                                (Session, new GroupOperator(GroupOperatorType.And,
                                                 new BinaryOperator("This", this, BinaryOperatorType.NotEqual),
                                                 new BinaryOperator("PayableTransaction", _numHeader)),
                                                 new SortProperty("No", DevExpress.Xpo.DB.SortingDirection.Ascending));
                    if (_numLines != null)
                    {
                        int i = 0;
                        foreach (PayableTransactionLine _numLine in _numLines)
                        {
                            i += 1;
                            _numLine.No = i;
                            _numLine.Save();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PayableTransactionLine " + ex.ToString());
            }
        }

        #endregion No

    }
}