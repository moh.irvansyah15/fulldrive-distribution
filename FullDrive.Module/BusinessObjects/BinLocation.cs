﻿using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;

using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.ConditionalAppearance;

namespace FullDrive.Module.BusinessObjects
{
    [DefaultClassOptions]
    [DefaultProperty("Name")]
    [NavigationItem("Territory")]
    [RuleCombinationOfPropertiesIsUnique("BinLocationRuleUnique", DefaultContexts.Save, "Code")]
    //[ImageName("BO_Contact")]
    //[DefaultProperty("DisplayMemberNameForLookupEditorsOfThisType")]
    //[DefaultListViewOptions(MasterDetailMode.ListViewOnly, false, NewItemRowPosition.None)]
    //[Persistent("DatabaseTableName")]
    // Specify more UI options using a declarative approach (https://documentation.devexpress.com/#eXpressAppFramework/CustomDocument112701).
    public class BinLocation : BaseObject
    { // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).
        private string _code;
        private Company _company;
        private Workplace _workplace;
        private string _name;
        private string _axisX;
        private string _axisZ;
        private string _axisY;
        private bool _lock;
        private string _description;
        private bool _active;
        private Location _location;
        private GlobalFunction _globFunc;
        //AuditTrail
        private XPCollection<AuditDataItemPersistent> changeHistory;

        public BinLocation(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if (!IsLoading)
            {
                _globFunc = new GlobalFunction();
                this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.BinLocation);
            }
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).
        }

        [Appearance("BinLocationCodeEnabled", Enabled = false)]
        [RuleRequiredField(DefaultContexts.Save)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue("Code", ref _code, value); }
        }

        [Appearance("BinLocationCompanyEnabled", Enabled = false)]
        [RuleRequiredField(DefaultContexts.Save)]
        public Company Company
        {
            get { return _company; }
            set { SetPropertyValue("Company", ref _company, value); }
        }

        [Appearance("BinLocationWorkplaceEnabled", Enabled = false)]
        [RuleRequiredField(DefaultContexts.Save)]
        public Workplace Workplace
        {
            get { return _workplace; }
            set { SetPropertyValue("Workplace", ref _workplace, value); }
        }

        [RuleRequiredField(DefaultContexts.Save)]
        public string Name
        {
            get { return _name; }
            set { SetPropertyValue("Name", ref _name, value); }
        }

        public string AxisX
        {
            get { return _axisX; }
            set { SetPropertyValue("AxisX", ref _axisX, value); }
        }

        public string AxisZ
        {
            get { return _axisZ; }
            set { SetPropertyValue("AxisZ", ref _axisZ, value); }
        }

        public string AxisY
        {
            get { return _axisY; }
            set { SetPropertyValue("AxisY", ref _axisY, value); }
        }

        public bool Lock
        {
            get { return _lock; }
            set { SetPropertyValue("Lock", ref _lock, value); }
        }

        [ImmediatePostData()]
        [RuleRequiredField(DefaultContexts.Save)]
        [Association("Location-BinLocations")]
        [DataSourceCriteria("Active = true")]
        public Location Location
        {
            get { return _location; }
            set
            {
                SetPropertyValue("Location", ref _location, value);
                if (!IsLoading)
                {
                    if (this._location != null)
                    {
                        if (this._location.Company != null)
                        {
                            this.Company = this._location.Company;
                        }
                        if(this._location.Workplace != null)
                        {
                            this.Workplace = this._location.Workplace;
                        }
                    }
                }
            }
        }

        

        [Size(512)]
        public string Description
        {
            get { return _description; }
            set { SetPropertyValue("Description", ref _description, value); }
        }

        public bool Active
        {
            get { return _active; }
            set { SetPropertyValue("Active", ref _active, value); }
        }

        //AuditTrail
        [CollectionOperationSet(AllowAdd = false, AllowRemove = true)]
        public XPCollection<AuditDataItemPersistent> ChangeHistory
        {
            get
            {
                if (changeHistory == null)
                {
                    changeHistory = AuditedObjectWeakReference.GetAuditTrail(Session, this);
                }
                return changeHistory;
            }
        }

        
    }
}