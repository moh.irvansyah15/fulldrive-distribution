﻿using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;

using DevExpress.ExpressApp.Editors;
using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.ConditionalAppearance;

namespace FullDrive.Module.BusinessObjects
{
    [DefaultClassOptions]
    [NavigationItem("Sales")]
    [DefaultProperty("Code")]
    [RuleCombinationOfPropertiesIsUnique("DiscountRuleRuleUnique", DefaultContexts.Save, "Code")]
    //[ImageName("BO_Contact")]
    //[DefaultProperty("DisplayMemberNameForLookupEditorsOfThisType")]
    //[DefaultListViewOptions(MasterDetailMode.ListViewOnly, false, NewItemRowPosition.None)]
    //[Persistent("DatabaseTableName")]
    // Specify more UI options using a declarative approach (https://documentation.devexpress.com/#eXpressAppFramework/CustomDocument112701).
    public class DiscountRule : BaseObject
    { // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).

        private bool _activationPosting;
        private int _no;
        private string _code;
        private string _name;
        private XPCollection<Workplace> _availableWorkplace;
        private Company _company;
        private Workplace _workplace;
        private PriceGroup _priceGroup;
        private DiscountRuleType _discountRuleType;
        private RuleType _ruleType;
        private bool _allItem;
        #region InisialisasiMPQty
        private Item _mpItem;
        private XPCollection<UnitOfMeasure> _availableUnitOfMeasureMP;
        private double _mpDQty;
        private UnitOfMeasure _mpDUom;
        private double _mpQty;
        private UnitOfMeasure _mpUom;
        private double _mpTQty;
        #endregion InisialisasiMPQty
        private Currency _currency;
        private PriceLine _priceLine;
        private double _uAmount;
        private bool _grossAmountRule;
        private double _grossTotalUAmount;
        private bool _netAmountRule;
        private double _netTotalUAmount;
        private DiscountType _discountType;
        private double _value;
        private DateTime _startDate;
        private DateTime _endDate;
        private bool _valBasedAccount;
        private DiscountAccountGroup _discountAccountGroup;
        private XPCollection<ChartOfAccount> _availableAccountNo;
        private ChartOfAccount _accountNo;
        private bool _active;
        private Status _status;
        private DateTime _statusDate;
        private SalesPromotionRule _salesPromotionRule;
        private string _localUserAccess;
        private GlobalFunction _globFunc;
        //AuditTrail
        private XPCollection<AuditDataItemPersistent> changeHistory;

        public DiscountRule(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if (!IsLoading)
            {
                _globFunc = new GlobalFunction();
                #region UserAccess
                _localUserAccess = SecuritySystem.CurrentUserName;
                UserAccess _locUserAccess = Session.FindObject<UserAccess>(new BinaryOperator("UserName", _localUserAccess));
                if (_locUserAccess != null)
                {
                    if (_locUserAccess.Employee != null)
                    {
                        if (_locUserAccess.Employee.Company != null && _locUserAccess.Employee.Workplace != null)
                        {
                            this.Code = _globFunc.GetNumberingUnlockOptimisticRecordBasedOrganization(this.Session.DataLayer, ObjectList.DiscountRule, _locUserAccess.Employee.Company, _locUserAccess.Employee.Workplace);
                        }
                        else
                        {
                            this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.DiscountRule);
                        }
                    }
                }
                #endregion UserAccess
                DateTime now = DateTime.Now;
                this.Status = CustomProcess.Status.Open;
                this.StatusDate = now;
                this.Currency = _globFunc.GetDefaultCurrency(this.Session);
            }
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).
        }

        protected override void OnSaving()
        {
            base.OnSaving();
            if (!IsLoading && IsSaving)
            {
                UpdateNo();
            }
        }
        protected override void OnDeleting()
        {
            base.OnDeleting();
            if (!IsLoading)
            {
                RecoveryDeleteNo();
            }
        }

        #region Field

        [Browsable(false)]
        public bool ActivationPosting
        {
            get { return _activationPosting; }
            set { SetPropertyValue("ActivationPosting", ref _activationPosting, value); }
        }

        [Appearance("DiscountRuleNoEnabled", Enabled = false)]
        public int No
        {
            get { return _no; }
            set { SetPropertyValue("No", ref _no, value); }
        }

        [RuleRequiredField(DefaultContexts.Save)]
        [Appearance("DiscountRuleCodeClose", Enabled = false)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue("Code", ref _code, value); }
        }

        [Appearance("DiscountRuleDiscountNameClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public string Name
        {
            get { return _name; }
            set { SetPropertyValue("Name", ref _name, value); }
        }

        [RuleRequiredField(DefaultContexts.Save)]
        [Appearance("DiscountRuleCompanyClose", Enabled = false)]
        public Company Company
        {
            get { return _company; }
            set { SetPropertyValue("Company", ref _company, value); }
        }

        [Appearance("DiscountRuleWorkplaceClose", Enabled = false)]
        public Workplace Workplace
        {
            get { return _workplace; }
            set {
                SetPropertyValue("Workplace", ref _workplace, value);
                if(!IsLoading)
                {
                    if(this._workplace != null)
                    {
                        SetPriceLine();
                    }
                }
            }
        }

        [ImmediatePostData()]
        [Appearance("DiscountRulePriceGroupClose", Enabled = false)]
        public PriceGroup PriceGroup
        {
            get { return _priceGroup; }
            set {
                SetPropertyValue("PriceGroup", ref _priceGroup, value);
                if (!IsLoading)
                {
                    if (this._priceGroup != null)
                    {
                        SetPriceLine();
                    }
                }
            }
        }

        [ImmediatePostData()]
        [Appearance("DiscountRuleDiscountRuleTypeClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public DiscountRuleType DiscountRuleType
        {
            get { return _discountRuleType; }
            set {
                SetPropertyValue("DiscountRuleType", ref _discountRuleType, value);
                if (!IsLoading)
                {
                    if (this._discountRuleType != null)
                    {
                        this.RuleType = this._discountRuleType.RuleType;
                    }
                }
            }
        }

        //[Browsable(false)]
        [Appearance("DiscountRuleRuleTypeClose", Enabled = false)]
        public RuleType RuleType
        {
            get { return _ruleType; }
            set { SetPropertyValue("RuleType", ref _ruleType, value); }
        }

        [ImmediatePostData()]
        public bool AllItem
        {
            get { return _allItem; }
            set { SetPropertyValue("AllItem", ref _allItem, value); }
        }

        #region MpQty

        [ImmediatePostData()]
        [Appearance("DiscountRuleMpItemClose1", Criteria = "AllItem = true", Enabled = false)]
        [Appearance("DiscountRuleMpItemClose2", Criteria = "ActivationPosting = true", Enabled = false)]
        public Item MpItem
        {
            get { return _mpItem; }
            set
            {
                SetPropertyValue("MpItem", ref _mpItem, value);
                if (!IsLoading)
                {
                    if (this._mpItem != null)
                    {
                        if (this._mpItem.BasedUOM != null)
                        {
                            this.MpDUOM = this._mpItem.BasedUOM;
                        }
                        SetPriceLine();
                    }
                }
            }
        }

        [Browsable(false)]
        public XPCollection<UnitOfMeasure> AvailableUnitOfMeasureMP
        {
            get
            {
                if(!IsLoading)
                {
                    if (MpItem == null)
                    {
                        _availableUnitOfMeasureMP = new XPCollection<UnitOfMeasure>(Session);
                    }
                    else
                    {
                        if (GetUnitOfMeasureCollection(this.MpItem) != null)
                        {
                            _availableUnitOfMeasureMP = new XPCollection<UnitOfMeasure>(Session, CriteriaOperator.Parse(GetUnitOfMeasureCollection(this.MpItem)));
                        }

                    }
                }
                
                return _availableUnitOfMeasureMP;

            }
        }

        [Appearance("DiscountRuleMpDQtyClose1", Criteria = "AllItem = true", Enabled = false)]
        [Appearance("DiscountRuleMpDQtyClose2", Criteria = "ActivationPosting = true", Enabled = false)]
        [ImmediatePostData()]
        public double MpDQty
        {
            get { return _mpDQty; }
            set
            {
                SetPropertyValue("MpDQty", ref _mpDQty, value);
                if (!IsLoading)
                {
                    SetMpTotalQty();
                    SetTotalUnitAmount();
                }
            }
        }

        [Appearance("DiscountRuleMpDUOMClose1", Criteria = "AllItem = true", Enabled = false)]
        [Appearance("DiscountRuleMpDUOMClose2", Criteria = "ActivationPosting = true", Enabled = false)]
        [ImmediatePostData()]
        [DataSourceCriteria("Active = true")]
        public UnitOfMeasure MpDUOM
        {
            get { return _mpDUom; }
            set
            {
                SetPropertyValue("MpDUOM", ref _mpDUom, value);
                if (!IsLoading)
                {
                    SetMpTotalQty();
                    SetTotalUnitAmount();
                }
            }
        }

        [Appearance("DiscountRuleMpQtyClose1", Criteria = "AllItem = true", Enabled = false)]
        [Appearance("DiscountRuleMpQtyClose2", Criteria = "ActivationPosting = true", Enabled = false)]
        [ImmediatePostData()]
        public double MpQty
        {
            get { return _mpQty; }
            set
            {
                SetPropertyValue("MpQty", ref _mpQty, value);
                if (!IsLoading)
                {
                    SetMpTotalQty();
                    SetTotalUnitAmount();
                }
            }
        }

        [ImmediatePostData()]
        [Appearance("DiscountRuleMpUOMClose1", Criteria = "AllItem = true", Enabled = false)]
        [Appearance("DiscountRuleMpUOMClose2", Criteria = "ActivationPosting = true", Enabled = false)]
        [DataSourceProperty("AvailableUnitOfMeasureMP", DataSourcePropertyIsNullMode.SelectAll)]
        [DataSourceCriteria("Active = true")]
        public UnitOfMeasure MpUOM
        {
            get { return _mpUom; }
            set
            {
                SetPropertyValue("MpUOM", ref _mpUom, value);
                if (!IsLoading)
                {
                    SetMpTotalQty();
                    SetTotalUnitAmount();
                }
            }
        }

        [Appearance("DiscountRuleMpTQtyEnabled", Enabled = false)]
        public double MpTQty
        {
            get { return _mpTQty; }
            set { SetPropertyValue("MpTQty", ref _mpTQty, value); }
        }

        #endregion MpQty

        [ImmediatePostData()]
        [Appearance("DiscountRuleCurrencyClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Currency Currency
        {
            get { return _currency; }
            set {
                SetPropertyValue("Currency", ref _currency, value);
                if (!IsLoading)
                {
                    if (this._currency != null)
                    {
                        SetPriceLine();
                    }
                }
            }
        }

        [Browsable(false)]
        [DataSourceCriteria("Active = true"), ImmediatePostData()]
        [Appearance("DiscountRulePriceLineClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public PriceLine PriceLine
        {
            get { return _priceLine; }
            set
            {
                SetPropertyValue("PriceLine", ref _priceLine, value);
                if (!IsLoading)
                {
                    if (this._priceLine != null)
                    {
                        this.UAmount = this._priceLine.Amount2a;
                    }
                }
            }
        }

        [Appearance("DiscountRuleUAmountClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [ImmediatePostData()]
        public double UAmount
        {
            get { return _uAmount; }
            set
            {
                SetPropertyValue("UAmount", ref _uAmount, value);
                if (!IsLoading)
                {
                    _globFunc = new GlobalFunction();
                    if (this._uAmount > 0)
                    {
                        if (_globFunc.GetRoundingList(Session, ObjectList.SalesOrderLine, FieldName.UAmount) == true)
                        {
                            this._uAmount = _globFunc.GetRoundUp(Session, this._uAmount, ObjectList.SalesOrderLine, FieldName.UAmount);
                        }
                    }
                    SetTotalUnitAmount();
                }
            }
        }

        [ImmediatePostData()]
        [Appearance("DiscountRuleGrossAmountRuleClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public bool GrossAmountRule
        {
            get { return _grossAmountRule; }
            set {
                SetPropertyValue("GrossAmountRule", ref _grossAmountRule, value);
                if(!IsLoading)
                {
                    if(this._grossAmountRule == true)
                    {
                        SetTotalUnitAmount();
                        this.NetAmountRule = false;
                    }
                    else
                    {
                        this.GrossTotalUAmount = 0;
                    }
                }
            }
        }

        [ImmediatePostData()]
        [Appearance("DiscountRuleGrossTotalUAmountEnabled1", Criteria = "ActivationPosting = true", Enabled = false)]
        [Appearance("DiscountRuleGrossTotalUAmountEnabled2", Criteria = "GrossAmountRule = false", Enabled = false)]
        public double GrossTotalUAmount
        {
            get { return _grossTotalUAmount; }
            set { SetPropertyValue("GrossTotalUAmount", ref _grossTotalUAmount, value); }
        }

        [ImmediatePostData()]
        [Appearance("DiscountRuleNetAmountRuleClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public bool NetAmountRule
        {
            get { return _netAmountRule; }
            set {
                SetPropertyValue("NetAmountRule", ref _netAmountRule, value);
                if (!IsLoading)
                {
                    if (this._netAmountRule == true)
                    {
                        SetTotalUnitAmount();
                        this.GrossAmountRule = false;
                    }
                    else
                    {
                        this.NetTotalUAmount = 0;
                    }
                }
            }
        }

        [Appearance("DiscountRuleNetTotalUAmountEnabled1", Criteria = "ActivationPosting = true", Enabled = false)]
        [Appearance("DiscountRuleNetTotalUAmountEnabled2", Criteria = "NetAmountRule = false", Enabled = false)]
        [ImmediatePostData()]
        public double NetTotalUAmount
        {
            get { return _netTotalUAmount; }
            set { SetPropertyValue("NetTotalUAmount", ref _netTotalUAmount, value); }
        }

        [ImmediatePostData()]
        [Appearance("DiscountRuleNetTotalUAmountEnabled", Criteria = "ActivationPosting = true", Enabled = false)]
        public DiscountType DiscountType
        {
            get { return _discountType; }
            set { SetPropertyValue("DiscountType", ref _discountType, value); }
        }

        [Appearance("DiscountRuleValuetEnabled", Criteria = "ActivationPosting = true", Enabled = false)]
        public double Value
        {
            get { return _value; }
            set { SetPropertyValue("Value", ref _value, value); }
        }

        [ModelDefault("DisplayFormat", "{0:dd/MM/yyyy}")]
        [ModelDefault("EditMask", "dd/MM/yyyy")]
        [Appearance("DiscountRuleStartDateClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public DateTime StartDate
        {
            get { return _startDate; }
            set { SetPropertyValue("StartDate", ref _startDate, value); }
        }

        [ModelDefault("DisplayFormat", "{0:dd/MM/yyyy}")]
        [ModelDefault("EditMask", "dd/MM/yyyy")]
        [Appearance("DiscountRuleEndDateClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public DateTime EndDate
        {
            get { return _endDate; }
            set { SetPropertyValue("EndDate", ref _endDate, value); }
        }

        [ImmediatePostData()]
        [Appearance("DiscountRuleValBasedAccountClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public bool ValBasedAccount
        {
            get { return _valBasedAccount; }
            set { SetPropertyValue("ValBasedAccount", ref _valBasedAccount, value); }
        }

        [ImmediatePostData()]
        [DataSourceCriteria("Company = '@This.Company' AND Workplace = '@This.Workplace' AND Active = true")]
        [Appearance("DiscountRuleDiscountAccountGroupClose1", Criteria = "ValBasedAccount = false", Enabled = false)]
        [Appearance("DiscountRuleDiscountAccountGroupClose2", Criteria = "ActivationPosting = true", Enabled = false)]
        public DiscountAccountGroup DiscountAccountGroup
        {
            get { return _discountAccountGroup; }
            set { SetPropertyValue("DiscountAccountGroup", ref _discountAccountGroup, value); }
        }

        [Browsable(false)]
        public XPCollection<ChartOfAccount> AvailableAccountNo
        {
            get
            {
                if(!IsLoading)
                {
                    if (this.Company != null && this.Workplace != null && this.DiscountAccountGroup != null)
                    {
                        string _beginString = null;
                        string _endString = null;
                        string _fullString = null;

                        XPCollection<JournalMap> _locJournalMapItems = new XPCollection<JournalMap>(Session,
                                                                        new GroupOperator(GroupOperatorType.And,
                                                                        new BinaryOperator("Company", this.Company),
                                                                        new BinaryOperator("Workplace", this.Workplace),
                                                                        new BinaryOperator("DiscountAccountGroup", this.DiscountAccountGroup),
                                                                        new BinaryOperator("Active", true)));

                        if (_locJournalMapItems != null && _locJournalMapItems.Count() > 0)
                        {
                            foreach (JournalMap _locJournalMapItem in _locJournalMapItems)
                            {
                                XPCollection<JournalMapLine> _locJournalMapLines = new XPCollection<JournalMapLine>
                                                                                   (Session, new GroupOperator(GroupOperatorType.And,
                                                                                    new BinaryOperator("Company", this.Company),
                                                                                    new BinaryOperator("Workplace", this.Workplace),
                                                                                    new BinaryOperator("JournalMap", _locJournalMapItem),
                                                                                    new BinaryOperator("Active", true)));

                                if (_locJournalMapLines != null && _locJournalMapLines.Count() > 0)
                                {
                                    foreach (JournalMapLine _locJournalMapLine in _locJournalMapLines)
                                    {
                                        if (_locJournalMapLine.AccountMap != null)
                                        {
                                            if (_locJournalMapLine.AccountMap.PostingType == PostingType.Sales && _locJournalMapLine.AccountMap.PostingMethod == PostingMethod.InvoiceAR
                                                && _locJournalMapLine.AccountMap.PostingMethodType == PostingMethodType.Discount)
                                            {
                                                List<string> _stringAccount = new List<string>();

                                                XPCollection<AccountMapLine> _locAccountMapLines = new XPCollection<AccountMapLine>
                                                                                               (Session, new GroupOperator(GroupOperatorType.And,
                                                                                                new BinaryOperator("Company", this.Company),
                                                                                                new BinaryOperator("Workplace", this.Workplace),
                                                                                                new BinaryOperator("AccountMap", _locJournalMapLine.AccountMap),
                                                                                                new BinaryOperator("Active", true)));

                                                if (_locAccountMapLines != null && _locAccountMapLines.Count() > 0)
                                                {
                                                    foreach (AccountMapLine _locAccountMapLine in _locAccountMapLines)
                                                    {
                                                        if (_locAccountMapLine.Account != null)
                                                        {
                                                            if (_locAccountMapLine.Account.Code != null)
                                                            {
                                                                _stringAccount.Add(_locAccountMapLine.Account.Code);
                                                            }
                                                        }
                                                    }

                                                    IEnumerable<string> _stringArrayRDistinct = _stringAccount.Distinct();
                                                    string[] _stringArrayRList = _stringArrayRDistinct.ToArray();
                                                    if (_stringArrayRList.Length == 1)
                                                    {
                                                        for (int i = 0; i < _stringArrayRList.Length; i++)
                                                        {
                                                            ChartOfAccount _locCOA = Session.FindObject<ChartOfAccount>(new BinaryOperator("Code", _stringArrayRList[i]));
                                                            if (_locCOA != null)
                                                            {
                                                                if (i == 0)
                                                                {
                                                                    _beginString = "[Code]=='" + _locCOA.Code + "'";
                                                                }
                                                            }
                                                        }
                                                    }
                                                    else if (_stringArrayRList.Length > 1)
                                                    {
                                                        for (int i = 0; i < _stringArrayRList.Length; i++)
                                                        {
                                                            ChartOfAccount _locCOA = Session.FindObject<ChartOfAccount>(new BinaryOperator("Code", _stringArrayRList[i]));
                                                            if (_locCOA != null)
                                                            {
                                                                if (i == 0)
                                                                {
                                                                    _beginString = "[Code]=='" + _locCOA.Code + "'";
                                                                }
                                                                else
                                                                {
                                                                    _endString = _endString + " OR [Code]=='" + _locCOA.Code + "'";
                                                                }
                                                            }
                                                        }
                                                    }
                                                    _fullString = _beginString + _endString;

                                                    if (_fullString != null)
                                                    {
                                                        _availableAccountNo = new XPCollection<ChartOfAccount>(Session, CriteriaOperator.Parse(_fullString));
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        else
                        {
                            _availableAccountNo = new XPCollection<ChartOfAccount>(Session, new GroupOperator(GroupOperatorType.And,
                                                                                new BinaryOperator("Company", this.Company),
                                                                                new BinaryOperator("Workplace", this.Workplace),
                                                                                new BinaryOperator("Active", true)));
                        }
                    }
                    else
                    {
                        _availableAccountNo = new XPCollection<ChartOfAccount>(Session, new GroupOperator(GroupOperatorType.And,
                                                                                new BinaryOperator("Active", true)));

                    }
                }
                
                return _availableAccountNo;

            }
        }

        [Appearance("DiscountRuleAccountNoClose1", Criteria = "ValBasedAccount = false", Enabled = false)]
        [Appearance("DiscountRuleAccountNoClose2", Criteria = "ActivationPosting = true", Enabled = false)]
        [DataSourceProperty("AvailableAccountNo")]
        public ChartOfAccount AccountNo
        {
            get { return _accountNo; }
            set { SetPropertyValue("AccountNo", ref _accountNo, value); }
        }

        [Appearance("DiscountRuleActiveClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public bool Active
        {
            get { return _active; }
            set { SetPropertyValue("Active", ref _active, value); }
        }

        [Appearance("DiscountRuleStatusClose", Enabled = false)]
        public Status Status
        {
            get { return _status; }
            set { SetPropertyValue("Status", ref _status, value); }
        }

        [ModelDefault("DisplayFormat", "{0:dd/MM/yyyy}")]
        [ModelDefault("EditMask", "dd/MM/yyyy")]
        [Appearance("DiscountRuleStatusDateClose", Enabled = false)]
        public DateTime StatusDate
        {
            get { return _statusDate; }
            set { SetPropertyValue("StatusDate", ref _statusDate, value); }
        }

        [ImmediatePostData()]
        [RuleRequiredField(DefaultContexts.Save)]
        [Association("SalesPromotionRule-DiscountRules")]
        public SalesPromotionRule SalesPromotionRule
        {
            get { return _salesPromotionRule; }
            set
            {
                SetPropertyValue("SalesPromotionRule", ref _salesPromotionRule, value);
                if (!IsLoading)
                {
                    if (this._salesPromotionRule != null)
                    {
                        if (this._salesPromotionRule.Company != null)
                        {
                            this.Company = this._salesPromotionRule.Company;
                        }
                        if (this._salesPromotionRule.Workplace != null)
                        {
                            this.Workplace = this._salesPromotionRule.Workplace;
                        }
                        if (this._salesPromotionRule.PriceGroup != null)
                        {
                            this.PriceGroup = this._salesPromotionRule.PriceGroup;
                        }
                    }
                }
            }
        }

        //AuditTrail
        [CollectionOperationSet(AllowAdd = false, AllowRemove = true)]
        public XPCollection<AuditDataItemPersistent> ChangeHistory
        {
            get
            {
                if (changeHistory == null)
                {
                    changeHistory = AuditedObjectWeakReference.GetAuditTrail(Session, this);
                }
                return changeHistory;
            }
        }

        #endregion Field

        //================================ Code Only ================================

        #region CodeOnly

        #region No

        public void UpdateNo()
        {
            try
            {
                if (!IsLoading && Session.IsNewObject(this))
                {
                    if (this.SalesPromotionRule != null)
                    {
                        object _makRecord = Session.Evaluate<DiscountRule>(CriteriaOperator.Parse("Max(No)"), CriteriaOperator.Parse("SalesPromotionRule=?", this.SalesPromotionRule));
                        this.No = Convert.ToInt32(_makRecord) + 1;
                        this.Save();
                        RecoveryUpdateNo();
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = DiscountRule " + ex.ToString());
            }
        }

        public void RecoveryUpdateNo()
        {
            try
            {
                if (this.SalesPromotionRule != null)
                {
                    SalesPromotionRule _numHeader = Session.FindObject<SalesPromotionRule>
                                            (new BinaryOperator("Code", this.SalesPromotionRule.Code));

                    XPCollection<DiscountRule> _numLines = new XPCollection<DiscountRule>
                                                             (Session, new BinaryOperator("SalesPromotionRule", _numHeader),
                                                              new SortProperty("No", DevExpress.Xpo.DB.SortingDirection.Ascending));
                    if (_numLines != null)
                    {
                        int i1 = 0;
                        foreach (DiscountRule _numLine in _numLines)
                        {
                            i1 += 1;
                            _numLine.No = i1;
                            _numLine.Save();
                        }
                        i1 = 1;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = DiscountRule " + ex.ToString());
            }
        }

        public void RecoveryDeleteNo()
        {
            try
            {
                if (this.SalesPromotionRule != null)
                {
                    SalesPromotionRule _numHeader = Session.FindObject<SalesPromotionRule>
                                            (new BinaryOperator("Code", this.SalesPromotionRule.Code));

                    XPCollection<DiscountRule> _numLines = new XPCollection<DiscountRule>
                                                             (Session, new GroupOperator(GroupOperatorType.And,
                                                              new BinaryOperator("This", this, BinaryOperatorType.NotEqual),
                                                              new BinaryOperator("SalesPromotionRule", _numHeader)),
                                                              new SortProperty("No", DevExpress.Xpo.DB.SortingDirection.Ascending));
                    if (_numLines != null)
                    {
                        int i = 0;
                        foreach (DiscountRule _numLine in _numLines)
                        {
                            i += 1;
                            _numLine.No = i;
                            _numLine.Save();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = DiscountRule " + ex.ToString());
            }
        }

        #endregion No

        private string GetUnitOfMeasureCollection(Item _locItem)
        {
            string _result = null;
            try
            {
                string _beginString = null;
                string _endString = null;

                List<string> _stringUOM = new List<string>();

                XPCollection<ItemUnitOfMeasure> _locItemUnitOfMeasures = new XPCollection<ItemUnitOfMeasure>
                                                                          (Session, new GroupOperator(GroupOperatorType.And,
                                                                           new BinaryOperator("Item", _locItem),
                                                                           new BinaryOperator("Active", true)));

                if (_locItemUnitOfMeasures != null && _locItemUnitOfMeasures.Count() > 0)
                {
                    foreach (ItemUnitOfMeasure _locItemUnitOfMeasure in _locItemUnitOfMeasures)
                    {
                        _stringUOM.Add(_locItemUnitOfMeasure.UOM.Code);
                    }
                }

                IEnumerable<string> _stringArrayUOMDistinct = _stringUOM.Distinct();
                string[] _stringArrayUOMList = _stringArrayUOMDistinct.ToArray();
                if (_stringArrayUOMList.Length == 1)
                {
                    for (int i = 0; i < _stringArrayUOMList.Length; i++)
                    {
                        UnitOfMeasure _locUOM = Session.FindObject<UnitOfMeasure>(new BinaryOperator("Code", _stringArrayUOMList[i]));
                        if (_locUOM != null)
                        {
                            if (i == 0)
                            {
                                _beginString = "[Code]=='" + _locUOM.Code + "'";
                            }
                        }
                    }
                }
                else if (_stringArrayUOMList.Length > 1)
                {
                    for (int i = 0; i < _stringArrayUOMList.Length; i++)
                    {
                        UnitOfMeasure _locUOM = Session.FindObject<UnitOfMeasure>(new BinaryOperator("Code", _stringArrayUOMList[i]));
                        if (_locUOM != null)
                        {
                            if (i == 0)
                            {
                                _beginString = "[Code]=='" + _locUOM.Code + "'";
                            }
                            else
                            {
                                _endString = _endString + " OR [Code]=='" + _locUOM.Code + "'";
                            }
                        }
                    }
                }

                _result = _beginString + _endString;

            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = DiscountRule " + ex.ToString());
            }

            return _result;
        }

        private void SetMpTotalQty()
        {
            try
            {
                double _locInvLineTotal = 0;
                DateTime now = DateTime.Now;
                if (this.SalesPromotionRule != null)
                {
                    if (this.MpItem != null && this.MpUOM != null && this.MpDUOM != null)
                    {
                        ItemUnitOfMeasure _locItemUOM = Session.FindObject<ItemUnitOfMeasure>
                                                        (new GroupOperator(GroupOperatorType.And,
                                                         new BinaryOperator("Item", this.MpItem),
                                                         new BinaryOperator("UOM", this.MpUOM),
                                                         new BinaryOperator("DefaultUOM", this.MpDUOM),
                                                         new BinaryOperator("Active", true)));
                        if (_locItemUOM != null)
                        {
                            if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                            {
                                _locInvLineTotal = this.MpQty * _locItemUOM.DefaultConversion + this.MpDQty;
                            }
                            else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                            {
                                _locInvLineTotal = this.MpQty / _locItemUOM.Conversion + this.MpDQty;
                            }
                            else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                            {
                                _locInvLineTotal = this.MpQty + this.MpDQty;
                            }

                            this.MpTQty = _locInvLineTotal;
                        }
                    }
                    else
                    {
                        _locInvLineTotal = this.MpQty + this.MpDQty;
                        this.MpTQty = _locInvLineTotal;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = DiscountRule " + ex.ToString());
            }
        }

        private void SetPriceLine()
        {
            try
            {
                if (this.Company != null && this.Workplace != null && this.MpItem != null && this.PriceGroup != null && this.Currency != null)
                {
                    PriceLine _locPriceLine = Session.FindObject<PriceLine>(new GroupOperator(GroupOperatorType.And,
                                                                            new BinaryOperator("Company", this.Company),
                                                                            new BinaryOperator("Workplace", this.Workplace),
                                                                            new BinaryOperator("Item", this.MpItem),
                                                                            new BinaryOperator("PriceGroup", this.PriceGroup),
                                                                            new BinaryOperator("Currency", this.Currency),
                                                                            new BinaryOperator("Active", true)));
                    if (_locPriceLine != null)
                    {
                        this.PriceLine = _locPriceLine;
                        this.UAmount = _locPriceLine.Amount2a;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = DiscountRule " + ex.ToString());
            }
        }

        private void SetTotalUnitAmount()
        {
            try
            {
                _globFunc = new GlobalFunction();

                if (_mpTQty >= 0 & _uAmount >= 0)
                {
                    if (_globFunc.GetRoundingList(Session, ObjectList.DiscountRule, FieldName.UAmount) == true)
                    {
                        if (this.GrossAmountRule == true)
                        {
                            this.GrossTotalUAmount = _globFunc.GetRoundUp(Session, (this.MpTQty * this.UAmount), ObjectList.InvoiceCanvassingLine, FieldName.TUAmount);
                        }
                        if (this.NetAmountRule == true)
                        {
                            this.NetTotalUAmount = _globFunc.GetRoundUp(Session, (this.MpTQty * this.UAmount), ObjectList.InvoiceCanvassingLine, FieldName.TUAmount);
                        }
                    }
                    else
                    {
                        if(this.GrossAmountRule == true)
                        {
                            this.GrossTotalUAmount = this.MpTQty * this.UAmount;
                        }
                        if(this.NetAmountRule == true)
                        {
                            this.NetTotalUAmount = this.MpTQty * this.UAmount;
                        }  
                    }

                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = DiscountRule " + ex.ToString());
            }
        }

        [Action(Caption = "GetPreviousData", ConfirmationMessage = "Are you sure?", AutoCommit = false)]
        public void GetPreviousData()
        {
            try
            {
                DiscountRule _dataLine = null;
                int _locNo = 0;
                if (this.SalesPromotionRule != null)
                {
                    if (this.Company != null && this.Workplace != null && this.PriceGroup != null)
                    {
                        object _makRecord = Session.Evaluate<DiscountRule>(CriteriaOperator.Parse("Max(No)"), 
                            CriteriaOperator.Parse("Company=? and Workplace=? and PriceGroup=? and SalesPromotionRule=?", this.Company, this.Workplace, this.PriceGroup, this.SalesPromotionRule));
                        _locNo = Convert.ToInt32(_makRecord);

                        if (_locNo > 0)
                        {
                            _dataLine = Session.FindObject<DiscountRule>
                                (new GroupOperator(GroupOperatorType.And,
                                new BinaryOperator("Company", this.Company),
                                new BinaryOperator("Workplace", this.Workplace),
                                new BinaryOperator("PriceGroup", this.PriceGroup),
                                new BinaryOperator("SalesPromotionRule", this.SalesPromotionRule),
                                new BinaryOperator("No", _locNo)));
                        }

                    }
                    else
                    {
                        object _makRecord = Session.Evaluate<DiscountRule>(CriteriaOperator.Parse("Max(No)"), CriteriaOperator.Parse("SalesPromotionRule=? ", this.SalesPromotionRule));
                        _locNo = Convert.ToInt32(_makRecord);

                        if (_locNo > 0)
                        {
                            _dataLine = Session.FindObject<DiscountRule>
                                (new GroupOperator(GroupOperatorType.And,
                                new BinaryOperator("SalesPromotionRule", this.SalesPromotionRule),
                                new BinaryOperator("No", _locNo)));
                        }

                    }

                    if (_dataLine != null)
                    {
                        this.DiscountRuleType = _dataLine.DiscountRuleType;
                        this.RuleType = _dataLine.RuleType;
                        this.AllItem = _dataLine.AllItem;
                        this.MpItem = _dataLine.MpItem;
                        this.MpDQty = _dataLine.MpDQty;
                        this.MpDUOM = _dataLine.MpDUOM;
                        this.MpQty = _dataLine.MpQty;
                        this.MpUOM = _dataLine.MpUOM;
                        this.MpTQty = _dataLine.MpTQty;
                        this.Currency = _dataLine.Currency;
                        this.PriceGroup = _dataLine.PriceGroup;
                        this.PriceLine = _dataLine.PriceLine;
                        this.UAmount = _dataLine.UAmount;
                        this.GrossAmountRule = _dataLine.GrossAmountRule;
                        this.GrossTotalUAmount = _dataLine.GrossTotalUAmount;
                        this.NetAmountRule = _dataLine.NetAmountRule;
                        this.NetTotalUAmount = _dataLine.NetTotalUAmount;
                        this.DiscountType = _dataLine.DiscountType;
                        this.Value = _dataLine.Value;
                        this.StartDate = _dataLine.StartDate;
                        this.EndDate = _dataLine.EndDate;
                        this.Active = _dataLine.Active;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = RoutingLine " + ex.ToString());
            }

        }

        #endregion CodeOnly

    }
}