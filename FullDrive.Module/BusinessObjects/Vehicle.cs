﻿using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.ConditionalAppearance;

namespace FullDrive.Module.BusinessObjects
{
    [DefaultClassOptions]
    [NavigationItem("Transportation")]
    [DefaultProperty("Name")]
    [RuleCombinationOfPropertiesIsUnique("VehicleRuleUnique", DefaultContexts.Save, "Code, VehicleNo")]
    //[ImageName("BO_Contact")]
    //[DefaultProperty("DisplayMemberNameForLookupEditorsOfThisType")]
    //[DefaultListViewOptions(MasterDetailMode.ListViewOnly, false, NewItemRowPosition.None)]
    //[Persistent("DatabaseTableName")]
    // Specify more UI options using a declarative approach (https://documentation.devexpress.com/#eXpressAppFramework/CustomDocument112701).
    public class Vehicle : BaseObject
    { // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).

        private string _code;
        private Company _company;
        private XPCollection<Workplace> _availableWorkplace;
        private Workplace _workplace;
        private VehicleType _vehicleType;
        private VehicleMode _vehicleMode;
        private string _name;
        private string _vehicleNo;
        private bool _openVendor;
        private BusinessPartner _vendor;
        private Double _capacity;
        private UnitOfMeasure _uom;
        private DateTime _vehicleYear;
        private DateTime _keur;
        private DateTime _licence;
        private DateTime _emissionTest;
        private DateTime _sipa_kiu;
        private DateTime _ibm;
        private XPCollection<Employee> _availableDriver;
        private Employee _driver;
        private SalesRole _salesRole;
        private string _remark;
        private bool _active;
        private string _userAccess;
        private GlobalFunction _globFunc;
        //AuditTrail
        private XPCollection<AuditDataItemPersistent> changeHistory;

        public Vehicle(Session session)
            : base(session)
        {
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if (!IsLoading)
            {
                _globFunc = new GlobalFunction();
                #region UserAccess
                _userAccess = SecuritySystem.CurrentUserName;
                UserAccess _locUserAccess = Session.FindObject<UserAccess>(new BinaryOperator("UserName", _userAccess));
                if (Session.IsNewObject(this))
                {
                    if (_locUserAccess != null)
                    {
                        if (_locUserAccess.Employee != null)
                        {
                            if (_locUserAccess.Employee.Company != null && _locUserAccess.Employee.Workplace != null)
                            {
                                this.Code = _globFunc.GetNumberingUnlockOptimisticRecordBasedOrganization(this.Session.DataLayer, ObjectList.Vehicle, _locUserAccess.Employee.Company, _locUserAccess.Employee.Workplace);
                            }
                            else
                            {
                                this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.Vehicle);
                            }
                            if (_locUserAccess.Employee.Company != null)
                            {
                                Company = _locUserAccess.Employee.Company;
                            }
                            if (_locUserAccess.Employee.Workplace != null)
                            {
                                Workplace = _locUserAccess.Employee.Workplace;
                            }
                            else
                            {
                                this.Workplace = _globFunc.GetDefaultWorkplace(this.Session, _locUserAccess);
                            }
                        }
                    }
                }
                #endregion UserAccess
            }
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).
        }

        #region Field

        [RuleRequiredField(DefaultContexts.Save)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue("Code", ref _code, value); }
        }

        [ImmediatePostData()]
        [DataSourceCriteria("Active = true")]
        public Company Company
        {
            get { return _company; }
            set { SetPropertyValue("Company", ref _company, value); }
        }

        [Browsable(false)]
        public XPCollection<Workplace> AvailableWorkplace
        {
            get
            {
                if (!IsLoading)
                {
                    if (this.Company != null)
                    {
                        _userAccess = SecuritySystem.CurrentUserName;
                        UserAccess _locUserAccess = Session.FindObject<UserAccess>(new BinaryOperator("UserName", _userAccess));
                        if (_locUserAccess != null)
                        {
                            string _beginString = null;
                            string _endString = null;
                            string _fullString = null;
                            List<string> _stringWorkplace = new List<string>();

                            XPCollection<OrganizationSetupDetail> _locOrganizationSetupDetails = new XPCollection<OrganizationSetupDetail>
                                                                            (Session, new GroupOperator(GroupOperatorType.And,
                                                                            new BinaryOperator("Company", this.Company),
                                                                            new BinaryOperator("UserAccess", _locUserAccess),
                                                                            new BinaryOperator("OpenWorkplace", true),
                                                                            new BinaryOperator("Active", true)));

                            if (_locOrganizationSetupDetails != null && _locOrganizationSetupDetails.Count() > 0)
                            {
                                foreach (OrganizationSetupDetail _locOrganizationSetupDetail in _locOrganizationSetupDetails)
                                {
                                    if (_locOrganizationSetupDetail.Workplace != null)
                                    {
                                        if (_locOrganizationSetupDetail.Workplace.Code != null)
                                        {
                                            _stringWorkplace.Add(_locOrganizationSetupDetail.Workplace.Code);
                                        }
                                    }
                                }

                                #region Workplace
                                IEnumerable<string> _stringArrayWorkplaceDistinct = _stringWorkplace.Distinct();
                                string[] _stringArrayWorkplaceList = _stringArrayWorkplaceDistinct.ToArray();
                                if (_stringArrayWorkplaceList.Length == 1)
                                {
                                    for (int i = 0; i < _stringArrayWorkplaceList.Length; i++)
                                    {
                                        Workplace _locWorkplace = Session.FindObject<Workplace>(new BinaryOperator("Code", _stringArrayWorkplaceList[i]));
                                        if (_locWorkplace != null)
                                        {
                                            if (i == 0)
                                            {
                                                _beginString = "[Code]=='" + _locWorkplace.Code + "'";
                                            }
                                        }
                                    }
                                }
                                else if (_stringArrayWorkplaceList.Length > 1)
                                {
                                    for (int i = 0; i < _stringArrayWorkplaceList.Length; i++)
                                    {
                                        Workplace _locWorkplace = Session.FindObject<Workplace>(new BinaryOperator("Code", _stringArrayWorkplaceList[i]));
                                        if (_locWorkplace != null)
                                        {
                                            if (i == 0)
                                            {
                                                _beginString = "[Code]=='" + _locWorkplace.Code + "'";
                                            }
                                            else
                                            {
                                                _endString = _endString + " OR [Code]=='" + _locWorkplace.Code + "'";
                                            }
                                        }

                                    }
                                }
                                _fullString = _beginString + _endString;


                                if (_fullString != null)
                                {
                                    _availableWorkplace = new XPCollection<Workplace>(Session, CriteriaOperator.Parse(_fullString));
                                }

                                #endregion Workplace
                            }
                            else
                            {
                                OrganizationSetupDetail _locOrgSetDetail = Session.FindObject<OrganizationSetupDetail>
                                                                            (new GroupOperator(GroupOperatorType.And,
                                                                            new BinaryOperator("Company", this.Company),
                                                                            new BinaryOperator("UserAccess", _locUserAccess),
                                                                            new BinaryOperator("AllWorkplace", true),
                                                                            new BinaryOperator("Active", true)));
                                if (_locOrgSetDetail != null)
                                {
                                    _availableWorkplace = new XPCollection<Workplace>(Session,
                                                        new GroupOperator(GroupOperatorType.And,
                                                        new BinaryOperator("Company", this.Company),
                                                        new BinaryOperator("Active", true)));
                                }
                            }
                        }
                    }
                }

                return _availableWorkplace;

            }
        }

        [ImmediatePostData()]
        [DataSourceProperty("AvailableWorkplace")]
        public Workplace Workplace
        {
            get { return _workplace; }
            set { SetPropertyValue("Workplace", ref _workplace, value); }
        }

        [ImmediatePostData()]
        public VehicleType VehicleType
        {
            get { return _vehicleType; }
            set {
                SetPropertyValue("VehicleType", ref _vehicleType, value);
                if(!IsLoading)
                {
                    SetName();
                }
            }
        }

        public VehicleMode VehicleMode
        {
            get { return _vehicleMode; }
            set { SetPropertyValue("VehicleMode", ref _vehicleMode, value); }
        }

        [RuleRequiredField(DefaultContexts.Save)]
        public string Name
        {
            get { return _name; }
            set { SetPropertyValue("Name", ref _name, value); }
        }

        [ImmediatePostData()]
        public string VehicleNo
        {
            get { return _vehicleNo; }
            set {
                SetPropertyValue("VehicleNo", ref _vehicleNo, value);
                if(!IsLoading)
                {
                    SetName();
                }
            }
        }

        [ImmediatePostData()]
        public bool OpenVendor
        {
            get { return _openVendor; }
            set { SetPropertyValue("OpenVendor", ref _openVendor, value); }
        }

        [ImmediatePostData()]
        [Appearance("VehicleVendorClose", Criteria = "OpenVendor = false", Enabled = false)]
        public BusinessPartner Vendor
        {
            get { return _vendor; }
            set { SetPropertyValue("Vendor", ref _vendor, value); }
        }

        public Double Capacity
        {
            get { return _capacity; }
            set { SetPropertyValue("Capacity", ref _capacity, value); }
        }

        [DataSourceCriteria("Active = true"), ImmediatePostData()]
        public UnitOfMeasure UOM
        {
            get { return _uom; }
            set { SetPropertyValue("UOM", ref _uom, value); }
        }

        public string VehicleCapacity
        {
            get
            {
                if (this.Capacity > 0 && this.UOM != null)
                {
                    return String.Format("{0} ({1})", this.Capacity.ToString(), this.UOM.Code);
                }
                return null;
            }
        }

        public DateTime VehicleYear
        {
            get { return _vehicleYear; }
            set { SetPropertyValue("VehicleYear", ref _vehicleYear, value); }
        }

        public DateTime Keur
        {
            get { return _keur; }
            set { SetPropertyValue("Keur", ref _keur, value); }
        }

        public DateTime Licence
        {
            get { return _licence; }
            set { SetPropertyValue("Licence", ref _licence, value); }
        }

        public DateTime EmissionTest
        {
            get { return _emissionTest; }
            set { SetPropertyValue("EmissionTest", ref _emissionTest, value); }
        }

        public DateTime SIPA_KIU
        {
            get { return _sipa_kiu; }
            set { SetPropertyValue("SIPA_KIU", ref _sipa_kiu, value); }
        }

        public DateTime IBM
        {
            get { return _ibm; }
            set { SetPropertyValue("IBM", ref _ibm, value); }
        }

        [Browsable(false)]
        public XPCollection<Employee> AvailableDriver
        {
            get
            {
                if (Vendor == null)
                {
                    _availableDriver = new XPCollection<Employee>(Session, 
                                        new GroupOperator(GroupOperatorType.And,
                                        new BinaryOperator("Active", true)));
                }
                else
                {
                   XPCollection<Employee> _locDrivers = new XPCollection<Employee>(Session,
                                                new GroupOperator(GroupOperatorType.And,
                                                    new BinaryOperator("Vendor", this.Vendor),
                                                    new BinaryOperator("Active", true)));
                    if(_locDrivers != null && _locDrivers.Count() > 0)
                    {
                        _availableDriver = _locDrivers;
                    }
                    else
                    {
                        _availableDriver = new XPCollection<Employee>(Session,
                                        new GroupOperator(GroupOperatorType.And,
                                        new BinaryOperator("Active", true)));
                    }
                }
                return _availableDriver;
            }
        }

        [DataSourceProperty("AvailableDriver", DataSourcePropertyIsNullMode.SelectAll)]
        public Employee Driver
        {
            get { return _driver; }
            set { SetPropertyValue("Driver", ref _driver, value); }
        }

        public SalesRole SalesRole
        {
            get { return _salesRole; }
            set { SetPropertyValue("SalesRole", ref _salesRole, value); }
        }

        [Size(512)]
        public string Remark
        {
            get { return _remark; }
            set { SetPropertyValue("Remark", ref _remark, value); }
        }

        public bool Active
        {
            get { return _active; }
            set { SetPropertyValue("Active", ref _active, value); }
        }

        [Association("Vehicle-VehicleLists")]
        public XPCollection<VehicleList> VehicleLists
        {
            get { return GetCollection<VehicleList>("VehicleLists"); }
        }

        [Association("Vehicle-Routes")]
        public XPCollection<Route> Routes
        {
            get { return GetCollection<Route>("Routes"); }
        }

        //AuditTrail
        [CollectionOperationSet(AllowAdd = false, AllowRemove = true)]
        public XPCollection<AuditDataItemPersistent> ChangeHistory
        {
            get
            {
                if (changeHistory == null)
                {
                    changeHistory = AuditedObjectWeakReference.GetAuditTrail(Session, this);
                }
                return changeHistory;
            }
        }

        #endregion Field

        //================================================= Code Only ===================================================

        #region CodeOnly

        private void SetName()
        {
            try
            {
                if(this.VehicleType != VehicleType.None || this.VehicleNo != null)
                {
                    this.Name = string.Format("({0})-{1}", this.VehicleType.ToString(), this.VehicleNo);
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Business Object = SalesmanVehicleSetup", ex.ToString());
            }
        }
        #endregion CodeOnly
    }
}