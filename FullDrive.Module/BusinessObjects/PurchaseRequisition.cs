﻿using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;

using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.ConditionalAppearance;
using DevExpress.ExpressApp.Editors;

namespace FullDrive.Module.BusinessObjects
{
    [DefaultClassOptions]
    [DefaultProperty("Code")]
    [NavigationItem("Purchase")]
    [RuleCombinationOfPropertiesIsUnique("PurchaseRequisitionRuleUnique", DefaultContexts.Save, "Code")]
    //[ImageName("BO_Contact")]
    //[DefaultProperty("DisplayMemberNameForLookupEditorsOfThisType")]
    //[DefaultListViewOptions(MasterDetailMode.ListViewOnly, false, NewItemRowPosition.None)]
    //[Persistent("DatabaseTableName")]
    // Specify more UI options using a declarative approach (https://documentation.devexpress.com/#eXpressAppFramework/CustomDocument112701).
    public class PurchaseRequisition : BaseObject
    { // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).
        private bool _activationPosting;
        private string _code;
        private string _description;
        private DateTime _requestDate;
        private DateTime _requiredDate;
        private FileData _attachment;
        private DirectionType _transferType;
        private Status _status;
        private DateTime _statusDate;
        #region InitialOrganization       
        private Company _company;
        private Workplace _workplace;
        private Division _division;
        private Department _department;
        private Section _section;       
        private Employee _employee;
        private Division _div;
        private Department _dept;
        private Section _sect;
        #endregion InitialOrganization
        private string _localUserAccess;
        private GlobalFunction _globFunc;
        //AuditTrail
        private XPCollection<AuditDataItemPersistent> changeHistory;
        //ActiveApproved
        private bool _activeApproved1;
        private bool _activeApproved2;
        private bool _activeApproved3;

        public PurchaseRequisition(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if (!IsLoading)
            {
                if(Session.IsNewObject(this))
                {
                    _globFunc = new GlobalFunction();
                    DateTime now = DateTime.Now;

                    #region UserAccess
                    _localUserAccess = SecuritySystem.CurrentUserName;
                    UserAccess _locUserAccess = Session.FindObject<UserAccess>(new BinaryOperator("UserName", _localUserAccess));
                    if (_locUserAccess != null)
                    {
                        if (_locUserAccess.Employee != null)
                        {
                            if (_locUserAccess.Employee.Company != null && _locUserAccess.Employee.Workplace != null)
                            {
                                this.Code = _globFunc.GetNumberingUnlockOptimisticRecordBasedOrganization(this.Session.DataLayer, ObjectList.PurchaseRequisition, _locUserAccess.Employee.Company, _locUserAccess.Employee.Workplace);
                            }
                            else
                            {
                                this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.PurchaseRequisition);
                            }
                            if (_locUserAccess.Employee.Company != null)
                            {
                                this.Company = _locUserAccess.Employee.Company;
                            }
                            if (_locUserAccess.Employee.Workplace != null)
                            {
                                this.Workplace = _locUserAccess.Employee.Workplace;
                            }
                            if (_locUserAccess.Employee.Division != null)
                            {
                                this.Division = _locUserAccess.Employee.Division;
                                this.Div = _locUserAccess.Employee.Division;
                            }
                            if (_locUserAccess.Employee.Department != null)
                            {
                                this.Department = _locUserAccess.Employee.Department;
                                this.Dept = _locUserAccess.Employee.Department;
                            }
                            if (_locUserAccess.Employee.Section != null)
                            {
                                this.Section = _locUserAccess.Employee.Section;
                                this.Sect = _locUserAccess.Employee.Section;
                            }
                            this.Employee = _locUserAccess.Employee;
                        }
                    }
                    #endregion UserAccess

                    this.RequestDate = DateTime.Now;
                    this.TransferType = CustomProcess.DirectionType.External;
                    this.Status = CustomProcess.Status.Open;
                    this.StatusDate = now;                    
                }
            }
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).
        }

        #region Field

        [Browsable(false)]
        public bool ActivationPosting
        {
            get { return _activationPosting; }
            set { SetPropertyValue("ActivationPosting", ref _activationPosting, value); }
        }

        [ImmediatePostData()]
        [Appearance("PurchaseRequisitionCodeClose", Enabled = false)]
        [Appearance("PurchaseRequisitionRedColor", Criteria = "ActiveApproved1 = true", BackColor = "#fd79a8")]
        [Appearance("PurchaseRequisitionYellowColor", Criteria = "ActiveApproved2 = true", BackColor = "#ffeaa7")]
        [Appearance("PurchaseRequisitionGreenOColor", Criteria = "ActiveApproved3 = true", BackColor = "#81ecec")]
        [RuleRequiredField(DefaultContexts.Save)]
        [Appearance("ProjectRequisitionCodeClose", Enabled = false)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue("Code", ref _code, value); }
        }

        [Appearance("PurchaseRequisitionDescriptionClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [Size(512)]
        public string Description
        {
            get { return _description; }
            set { SetPropertyValue("Description", ref _description, value); }
        }

        [ModelDefault("DisplayFormat", "{0:dd/MM/yyyy}")]
        [ModelDefault("EditMask", "dd/MM/yyyy")]
        [Appearance("PurchaseRequisitionRequestDateClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public DateTime RequestDate
        {
            get { return _requestDate; }
            set { SetPropertyValue("RequestDate", ref _requestDate, value); }
        }

        [ModelDefault("DisplayFormat", "{0:dd/MM/yyyy}")]
        [ModelDefault("EditMask", "dd/MM/yyyy")]
        [RuleRequiredField(DefaultContexts.Save)]
        [Appearance("PurchaseRequisitionRequiredDateClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public DateTime RequiredDate
        {
            get { return _requiredDate; }
            set { SetPropertyValue("RequiredDate", ref _requiredDate, value); }
        }

        [Appearance("PurchaseRequisitionAttachmentClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public FileData Attachment
        {
            get { return _attachment; }
            set { SetPropertyValue("Attachment", ref _attachment, value); }
        }

        [Appearance("PurchaseRequisitionTransferTypeClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public DirectionType TransferType
        {
            get { return _transferType; }
            set { SetPropertyValue("TransferType", ref _transferType, value); }
        }

        [Appearance("ProjectRequisitionStatusClose", Enabled = false)]
        public Status Status
        {
            get { return _status; }
            set { SetPropertyValue("Status", ref _status, value); }
        }

        [ModelDefault("DisplayFormat", "{0:dd/MM/yyyy}")]
        [ModelDefault("EditMask", "dd/MM/yyyy")]
        [Appearance("ProjectRequisitionStatusDateClose", Enabled = false)]
        public DateTime StatusDate
        {
            get { return _statusDate; }
            set { SetPropertyValue("StatusDate", ref _statusDate, value); }
        }

        #region Organization

        [Appearance("PurchaseRequisitionEmployeeClose", Enabled = false)]
        public Employee Employee
        {
            get { return _employee; }
            set { SetPropertyValue("Employee", ref _employee, value); }
        }

        [Appearance("PurchaseRequisitionCompanyClose", Enabled = false)]
        public Company Company
        {
            get { return _company; }
            set { SetPropertyValue("Company", ref _company, value); }
        }

        [Appearance("PurchaseRequisitionWorkplaceClose", Enabled = false)]
        public Workplace Workplace
        {
            get { return _workplace; }
            set { SetPropertyValue("Workplace", ref _workplace, value); }
        }

        [Browsable(false)]
        public Division Division
        {
            get { return _division; }
            set { SetPropertyValue("Division", ref _division, value); }
        }

        [Browsable(false)]
        public Department Department
        {
            get { return _department; }
            set { SetPropertyValue("Department", ref _department, value); }
        }

        [Browsable(false)]
        public Section Section
        {
            get { return _section; }
            set { SetPropertyValue("Section", ref _section, value); }
        }

        [ImmediatePostData()]
        [DataSourceCriteria("Company = '@This.Company' And Active = true")]
        [Appearance("PurchaseRequisitionDivClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Division Div
        {
            get { return _div; }
            set { SetPropertyValue("Div", ref _div, value); }
        }

        [ImmediatePostData()]
        [DataSourceCriteria("Company = '@This.Company' And Division = '@This.Div' And Active = true")]
        [Appearance("PurchaseRequisitionDeptClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Department Dept
        {
            get { return _dept; }
            set { SetPropertyValue("Dept", ref _dept, value); }
        }

        [ImmediatePostData()]
        [DataSourceCriteria("Company = '@This.Company' And Department = '@This.Dept' And Active = true")]
        [Appearance("PurchaseRequisitionSectClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Section Sect
        {
            get { return _sect; }
            set { SetPropertyValue("Sect", ref _sect, value); }
        }

        #endregion Organization

        #region Approved

        [Browsable(false)]
        [Appearance("PurchaseRequisitionActiveApproved1Close", Enabled = false)]
        public bool ActiveApproved1
        {
            get { return _activeApproved1; }
            set { SetPropertyValue("ActiveApproved1", ref _activeApproved1, value); }
        }

        [Browsable(false)]
        [Appearance("PurchaseRequisitionActiveApproved2Close", Enabled = false)]
        public bool ActiveApproved2
        {
            get { return _activeApproved2; }
            set { SetPropertyValue("ActiveApproved2", ref _activeApproved2, value); }
        }

        [Browsable(false)]
        [Appearance("PurchaseRequisitionActiveApproved3Close", Enabled = false)]
        public bool ActiveApproved3
        {
            get { return _activeApproved3; }
            set { SetPropertyValue("ActiveApproved3", ref _activeApproved3, value); }
        }

        #endregion Approved

        [Association("PurchaseRequisition-PurchaseRequisitionLines")]
        public XPCollection<PurchaseRequisitionLine> PurchaseRequisitionLines
        {
            get { return GetCollection<PurchaseRequisitionLine>("PurchaseRequisitionLines"); }
        }

        [Association("PurchaseRequisition-ApprovalLines")]
        public XPCollection<ApprovalLine> ApprovalLines
        {
            get { return GetCollection<ApprovalLine>("ApprovalLines"); }
        }

        //AuditTrail
        [CollectionOperationSet(AllowAdd = false, AllowRemove = true)]
        public XPCollection<AuditDataItemPersistent> ChangeHistory
        {
            get
            {
                if (changeHistory == null)
                {
                    changeHistory = AuditedObjectWeakReference.GetAuditTrail(Session, this);
                }
                return changeHistory;
            }
        }

        #endregion Field      

    }
}