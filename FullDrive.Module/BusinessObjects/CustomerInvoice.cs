﻿using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;

using DevExpress.ExpressApp.Editors;
using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.ConditionalAppearance;

namespace FullDrive.Module.BusinessObjects
{
    [DefaultClassOptions]
    [DefaultProperty("Code")]
    [NavigationItem("Sales")]
    [RuleCombinationOfPropertiesIsUnique("CustomerInvoiceRuleUnique", DefaultContexts.Save, "Code")]
    //[ImageName("BO_Contact")]
    //[DefaultProperty("DisplayMemberNameForLookupEditorsOfThisType")]
    //[DefaultListViewOptions(MasterDetailMode.ListViewOnly, false, NewItemRowPosition.None)]
    //[Persistent("DatabaseTableName")]
    // Specify more UI options using a declarative approach (https://documentation.devexpress.com/#eXpressAppFramework/CustomDocument112701).
    public class CustomerInvoice : BaseObject
    { // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).
        private bool _activationPosting;
        private string _code;
        #region InitialOrganization
        private Company _company;
        private Workplace _workplace;
        private Division _division;
        private Department _department;
        private Section _section;
        private Employee _employee;
        private XPCollection<Location> _availableLocation;
        private Location _location;
        private BeginningInventory _begInv;
        #endregion InitialOrganization
        #region InitialSales
        private BusinessPartner _salesToCustomer;
        private string _salesToContact;
        private Country _salesToCountry;
        private City _salesToCity;
        private string _salesToAddress;
        #endregion InitialSales
        #region InitialBill
        private BusinessPartner _billToCustomer;
        private string _billToContact;
        private Country _billToCountry;
        private City _billToCity;
        private string _billToAddress;
        #endregion InitialBill
        #region InitialAmount
        private Currency _currency;
        private PriceGroup _priceGroup;
        private PaymentMethod _paymentMethod;
        private PaymentMethodType _paymentMethodType;
        private PaymentType _paymentType;
        private TermOfPayment _top;
        private DateTime _dueDate;
        private string _taxNo;
        private double _totAmount;
        private double _totUnitAmount;
        private double _totTaxAmount;
        private double _totDiscAmount;
        private double _amount;
        private double _totFIAmount;
        private double _amountDisc;
        private DiscountRule _discountRule;
        private AccountingPeriodicLine _accountingPeriodicLine;
        #endregion InitialAmount
        #region InitialBank
        private XPCollection<BankAccount> _availableCompanyBankAccount;
        private BankAccount _companyBankAccount;
        private string _companyAccountNo;
        private string _companyAccountName;
        private XPCollection<BankAccount> _availableBankAccounts;
        private BankAccount _bankAccount;
        private string _accountNo;
        private string _accountName;
        #endregion InitialBank
        private string _description;
        private string _djp_No;
        private DateTime _docDate;
        private int _journalMonth;
        private int _journalYear;
        private int _postedCount;
        private string _signCode;
        private Status _status;
        private DateTime _statusDate;
        private CustomerOrder _customerOrder;
        private string _userAccess;
        private GlobalFunction _globFunc;
        //AuditTrail
        private XPCollection<AuditDataItemPersistent> changeHistory;

        public CustomerInvoice(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if (!IsLoading)
            {
                if (Session.IsNewObject(this))
                {
                    _globFunc = new GlobalFunction();
                    DateTime now = DateTime.Now;
                    #region UserAccess
                    _userAccess = SecuritySystem.CurrentUserName;
                    UserAccess _locUserAccess = Session.FindObject<UserAccess>(new BinaryOperator("UserName", _userAccess));

                    if (_locUserAccess != null)
                    {
                        if (_locUserAccess.Employee != null)
                        {
                            if (_locUserAccess.Employee.Company != null && _locUserAccess.Employee.Workplace != null)
                            {
                                this.Code = _globFunc.GetNumberingUnlockOptimisticRecordBasedOrganization(this.Session.DataLayer, ObjectList.CustomerInvoice, _locUserAccess.Employee.Company, _locUserAccess.Employee.Workplace);
                            }
                            else
                            {
                                this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.CustomerInvoice);
                            }
                            if (_locUserAccess.Employee.Company != null)
                            {
                                Company = _locUserAccess.Employee.Company;
                            }
                            if (_locUserAccess.Employee.Workplace != null)
                            {
                                Workplace = _locUserAccess.Employee.Workplace;
                            }
                            else
                            {
                                this.Workplace = _globFunc.GetDefaultWorkplace(this.Session, _locUserAccess);
                            }
                            if (_locUserAccess.Employee.Division != null)
                            {
                                Division = _locUserAccess.Employee.Division;
                            }
                            if (_locUserAccess.Employee.Department != null)
                            {
                                Department = _locUserAccess.Employee.Department;
                            }
                            if (_locUserAccess.Employee.Section != null)
                            {
                                Section = _locUserAccess.Employee.Section;
                            }
                            this.Employee = _locUserAccess.Employee;
                        }
                    }
                    #endregion UserAccess
                    this.Status = CustomProcess.Status.Open;
                    this.StatusDate = now;
                    this.Currency = _globFunc.GetDefaultCurrency(this.Session);
                    this.TOP = _globFunc.GetDefaultTermOfPayment(this.Session);
                    this.PriceGroup = _globFunc.GetPriceGroupPOS(this.Session);
                    this.DocDate = now;
                    this.JournalMonth = this.DocDate.Month;
                    this.JournalYear = this.DocDate.Year;
                }
            }
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).
        }

        #region Field

        [Browsable(false)]
        [ImmediatePostData()]
        public bool ActivationPosting
        {
            get { return _activationPosting; }
            set { SetPropertyValue("ActivationPosting", ref _activationPosting, value); }
        }

        [Appearance("CustomerInvoiceCodeClose", Enabled = false)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue("Code", ref _code, value); }
        }

        #region Organization

        [ImmediatePostData()]
        [RuleRequiredField(DefaultContexts.Save)]
        [DataSourceCriteria("Active = true")]
        [Appearance("CustomerInvoiceCompanyClose", Enabled = false)]
        public Company Company
        {
            get { return _company; }
            set { SetPropertyValue("Company", ref _company, value); }
        }

        [ImmediatePostData()]
        [RuleRequiredField(DefaultContexts.Save)]
        [DataSourceCriteria("Active = true")]
        [Appearance("CustomerInvoiceWorkplaceClose", Enabled = false)]
        public Workplace Workplace
        {
            get { return _workplace; }
            set { SetPropertyValue("Workplace", ref _workplace, value); }
        }

        [Browsable(false)]
        public Division Division
        {
            get { return _division; }
            set { SetPropertyValue("Division", ref _division, value); }
        }

        [Browsable(false)]
        public Department Department
        {
            get { return _department; }
            set { SetPropertyValue("Department", ref _department, value); }
        }

        [Browsable(false)]
        public Section Section
        {
            get { return _section; }
            set { SetPropertyValue("Section", ref _section, value); }
        }

        [ImmediatePostData()]
        [Appearance("CustomerInvoiceEmployeeClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Employee Employee
        {
            get { return _employee; }
            set
            {
                SetPropertyValue("Employee", ref _employee, value);
                if (!IsLoading)
                {
                    if (this._employee != null)
                    {
                        SetLocation(_employee);
                    }
                    else
                    {
                        this.Location = null;
                    }
                }
            }
        }

        [Browsable(false)]
        public XPCollection<Location> AvailableLocation
        {
            get
            {
                if(!IsLoading)
                {
                    string _beginString = null;
                    string _endString = null;
                    string _fullString = null;

                    if (this.Company != null && this.Workplace != null && this.Employee != null)
                    {
                        List<string> _stringSLS = new List<string>();

                        XPCollection<SalesmanLocationSetup> _locSalesmanLocationSetups = new XPCollection<SalesmanLocationSetup>
                                                                                  (Session, new GroupOperator(GroupOperatorType.And,
                                                                                   new BinaryOperator("Company", this.Company),
                                                                                   new BinaryOperator("Workplace", this.Workplace),
                                                                                   new BinaryOperator("Salesman", this.Employee),
                                                                                   new BinaryOperator("Active", true)));

                        if (_locSalesmanLocationSetups != null && _locSalesmanLocationSetups.Count() > 0)
                        {
                            foreach (SalesmanLocationSetup _locSalesmanLocationSetup in _locSalesmanLocationSetups)
                            {
                                if (_locSalesmanLocationSetup.Location != null)
                                {
                                    if (_locSalesmanLocationSetup.Location.Code != null)
                                    {
                                        _stringSLS.Add(_locSalesmanLocationSetup.Location.Code);
                                    }
                                }
                            }
                        }

                        IEnumerable<string> _stringArraySLSDistinct = _stringSLS.Distinct();
                        string[] _stringArraySLSList = _stringArraySLSDistinct.ToArray();
                        if (_stringArraySLSList.Length == 1)
                        {
                            for (int i = 0; i < _stringArraySLSList.Length; i++)
                            {
                                Location _locLoc = Session.FindObject<Location>(new BinaryOperator("Code", _stringArraySLSList[i]));
                                if (_locLoc != null)
                                {
                                    if (i == 0)
                                    {
                                        _beginString = "[Code]=='" + _locLoc.Code + "'";
                                    }
                                }
                            }
                        }
                        else if (_stringArraySLSList.Length > 1)
                        {
                            for (int i = 0; i < _stringArraySLSList.Length; i++)
                            {
                                Location _locLoc = Session.FindObject<Location>(new BinaryOperator("Code", _stringArraySLSList[i]));
                                if (_locLoc != null)
                                {
                                    if (i == 0)
                                    {
                                        _beginString = "[Code]=='" + _locLoc.Code + "'";
                                    }
                                    else
                                    {
                                        _endString = _endString + " OR [Code]=='" + _locLoc.Code + "'";
                                    }
                                }
                            }
                        }
                        _fullString = _beginString + _endString;

                        if (_fullString != null)
                        {
                            _availableLocation = new XPCollection<Location>(Session, CriteriaOperator.Parse(_fullString));
                        }
                    }
                    else
                    {
                        if (this.Company != null && this.Workplace != null)
                        {
                            _availableLocation = new XPCollection<Location>(Session, new GroupOperator(GroupOperatorType.And,
                                                                                new BinaryOperator("Company", this.Company),
                                                                                new BinaryOperator("Workplace", this.Workplace),
                                                                                new BinaryOperator("Active", true)));
                        }
                    }
                }
                return _availableLocation;
            }
        }

        [ImmediatePostData()]
        [DataSourceProperty("AvailableLocation")]
        [Appearance("CustomerInvoiceLocationClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Location Location
        {
            get { return _location; }
            set
            {
                SetPropertyValue("Location", ref _location, value);
                if (!IsLoading)
                {
                    if (this._location != null)
                    {
                        SetBegInv(_location);
                    }
                    else
                    {
                        BegInv = null;
                    }
                }
            }
        }

        //[Browsable(false)]
        [ImmediatePostData()]
        [Appearance("CustomerInvoiceBegInvClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public BeginningInventory BegInv
        {
            get { return _begInv; }
            set { SetPropertyValue("BegInv", ref _begInv, value); }
        }

        #endregion Organization

        #region SalesToCustomer

        [ImmediatePostData]
        [Appearance("CustomerInvoiceSalesToCostumerClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public BusinessPartner SalesToCustomer
        {
            get { return _salesToCustomer; }
            set
            {
                SetPropertyValue("SalesToCustomer", ref _salesToCustomer, value);
                if (!IsLoading)
                {
                    if (this._salesToCustomer != null)
                    {
                        if (this._salesToCustomer.Contact != null)
                        {
                            this.SalesToContact = this._salesToCustomer.Contact;
                        }
                        if (this._salesToCustomer.Country != null)
                        {
                            this.SalesToCountry = this._salesToCustomer.Country;
                        }
                        if (this._salesToCustomer.City != null)
                        {
                            this.SalesToCity = this._salesToCustomer.City;
                        }
                        if (this._salesToCustomer.Address != null)
                        {
                            this.SalesToAddress = this._salesToCustomer.Address;
                        }
                        if (this._salesToCustomer.TaxNo != null)
                        {
                            this.TaxNo = this._salesToCustomer.TaxNo;
                        }
                        this.BillToCustomer = this._salesToCustomer;
                    }
                    else
                    {
                        this.SalesToContact = null;
                        this.SalesToCountry = null;
                        this.SalesToCity = null;
                        this.SalesToAddress = null;
                        this.TaxNo = null;
                        this.BillToCustomer = null;
                    }
                }
            }
        }

        [Browsable(false)]
        [Appearance("CustomerInvoiceSalesToContactClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public string SalesToContact
        {
            get { return _salesToContact; }
            set { SetPropertyValue("SalesToContact", ref _salesToContact, value); }
        }

        [Browsable(false)]
        [Appearance("CustomerInvoiceSalesToCountryClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Country SalesToCountry
        {
            get { return _salesToCountry; }
            set { SetPropertyValue("SalesToCountry", ref _salesToCountry, value); }
        }

        [Browsable(false)]
        [Appearance("CustomerInvoiceSalesToCityClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public City SalesToCity
        {
            get { return _salesToCity; }
            set { SetPropertyValue("SalesToCity", ref _salesToCity, value); }
        }

        [Browsable(false)]
        [Size(512)]
        [Appearance("CustomerInvoiceSalesToAddressClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public string SalesToAddress
        {
            get { return _salesToAddress; }
            set { SetPropertyValue("SalesToAddress", ref _salesToAddress, value); }
        }

        #endregion SalesToCustomer

        #region BillToCustomer

        [ImmediatePostData()]
        [DataSourceCriteria("Active = true")]
        [Appearance("CustomerInvoiceBillToCostumerClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public BusinessPartner BillToCustomer
        {
            get { return _billToCustomer; }
            set
            {
                SetPropertyValue("BillToCostumer", ref _billToCustomer, value);
                if(!IsLoading)
                {
                    if(this._billToCustomer != null)
                    {
                        if(this._billToCustomer.Contact != null)
                        {
                            this.BillToContact = this._billToCustomer.Contact;
                        }
                        if (this._billToCustomer.Country != null)
                        {
                            this.BillToCountry = this._billToCustomer.Country;
                        }
                        if (this._billToCustomer.City != null)
                        {
                            this.BillToCity = this._billToCustomer.City;
                        }
                        if (this._billToCustomer.Address != null)
                        {
                            this.BillToAddress = this._billToCustomer.Address;
                        }
                    }
                    else
                    {
                        this.BillToContact = null;
                        this.BillToCountry = null;
                        this.BillToCity = null;
                        this.BillToAddress = null;
                    }
                }
            }
        }

        [Browsable(false)]
        [Appearance("CustomerInvoiceBillToContactClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public string BillToContact
        {
            get { return _billToContact; }
            set { SetPropertyValue("BillToContact", ref _billToContact, value); }
        }

        [Browsable(false)]
        [Appearance("CustomerInvoiceBillToCountryClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Country BillToCountry
        {
            get { return _billToCountry; }
            set { SetPropertyValue("BillToCountry", ref _billToCountry, value); }
        }

        [Browsable(false)]
        [Appearance("CustomerInvoiceBillToCityClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public City BillToCity
        {
            get { return _billToCity; }
            set { SetPropertyValue("BillToCity", ref _billToCity, value); }
        }

        [Browsable(false)]
        [Size(512)]
        [Appearance("CustomerInvoiceBillToAddressClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public string BillToAddress
        {
            get { return _billToAddress; }
            set { SetPropertyValue("BillToAddress", ref _billToAddress, value); }
        }

        #endregion BillToCostumer

        #region Amount

        [Appearance("CustomerInvoiceCurrencyClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Currency Currency
        {
            get { return _currency; }
            set { SetPropertyValue("Currency", ref _currency, value); }
        }

        [ImmediatePostData()]
        [Appearance("CustomerInvoicePriceGroupClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public PriceGroup PriceGroup
        {
            get { return _priceGroup; }
            set { SetPropertyValue("PriceGroup", ref _priceGroup, value); }
        }

        [Appearance("CustomerInvoicePaymentMethodClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public PaymentMethod PaymentMethod
        {
            get { return _paymentMethod; }
            set { SetPropertyValue("PaymentMethod", ref _paymentMethod, value); }
        }

        [Appearance("CustomerInvoicePaymentMethodTypeClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public PaymentMethodType PaymentMethodType
        {
            get { return _paymentMethodType; }
            set { SetPropertyValue("PaymentMethodType", ref _paymentMethodType, value); }
        }

        [Appearance("CustomerInvoicePaymentTypeClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public PaymentType PaymentType
        {
            get { return _paymentType; }
            set { SetPropertyValue("PaymentType", ref _paymentType, value); }
        }

        [ImmediatePostData()]
        [Appearance("CustomerInvoiceTermOfPaymentClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public TermOfPayment TOP
        {
            get { return _top; }
            set
            {
                SetPropertyValue("TOP", ref _top, value);
                if (!IsLoading)
                {
                    if (this.TOP != null)
                    {
                        this.DueDate = this.DocDate.AddDays(this.TOP.Due);
                    }
                }
            }
        }

        [ModelDefault("DisplayFormat", "{0:dd/MM/yyyy}")]
        [ModelDefault("EditMask", "dd/MM/yyyy")]
        [Appearance("CustomerInvoiceDueDateClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public DateTime DueDate
        {
            get { return _dueDate; }
            set { SetPropertyValue("DueDate", ref _dueDate, value); }
        }

        [Appearance("CustomerInvoiceTaxNoClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public string TaxNo
        {
            get { return _taxNo; }
            set { SetPropertyValue("TaxNo", ref _taxNo, value); }
        }

        [Appearance("CustomerInvoiceTotAmountClose", Enabled = false)]
        public double TotAmount
        {
            get { return _totAmount; }
            set { SetPropertyValue("TotAmount", ref _totAmount, value); }
        }

        [Appearance("CustomerInvoiceTotUnitAmountClose", Enabled = false)]
        public double TotUnitAmount
        {
            get { return _totUnitAmount; }
            set { SetPropertyValue("TotUnitAmount", ref _totUnitAmount, value); }
        }

        [Appearance("CustomerInvoiceTotTaxAmountClose", Enabled = false)]
        public double TotTaxAmount
        {
            get { return _totTaxAmount; }
            set { SetPropertyValue("TotTaxAmount", ref _totTaxAmount, value); }
        }

        [Appearance("CustomerInvoiceTotDiscAmountClose", Enabled = false)]
        public double TotDiscAmount
        {
            get { return _totDiscAmount; }
            set { SetPropertyValue("TotDiscAmount", ref _totDiscAmount, value); }
        }

        [Appearance("CustomerInvoiceAmountClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public double Amount
        {
            get { return _amount; }
            set { SetPropertyValue("Amount", ref _amount, value); }
        }

        [Appearance("CustomerInvoiceTotFIAmountClose", Enabled = false)]
        public double TotFIAmount
        {
            get { return _totFIAmount; }
            set { SetPropertyValue("TotFIAmount", ref _totFIAmount, value); }
        }

        [Browsable(false)]
        [Appearance("CustomerInvoiceAmountDiscClose", Enabled = false)]
        public double AmountDisc
        {
            get { return _amountDisc; }
            set { SetPropertyValue("AmountDisc", ref _amountDisc, value); }
        }

        [Appearance("CustomerInvoiceDiscountRuleClose", Enabled = false)]
        public DiscountRule DiscountRule
        {
            get { return _discountRule; }
            set { SetPropertyValue("DiscountRule", ref _discountRule, value); }
        }

        [Browsable(false)]
        public AccountingPeriodicLine AccountingPeriodicLine
        {
            get { return _accountingPeriodicLine; }
            set { SetPropertyValue("AccountingPeriodicLine", ref _accountingPeriodicLine, value); }
        }

        #endregion Amount

        #region Bank

        [Browsable(false)]
        public XPCollection<BankAccount> AvailableCompanyBankAccount
        {
            get
            {
                if (!IsLoading)
                {
                    if (this.Company != null)
                    {
                        if (this.Workplace != null)
                        {
                            _availableCompanyBankAccount = new XPCollection<BankAccount>(Session, new GroupOperator
                                                    (GroupOperatorType.And,
                                                    new BinaryOperator("Company", this.Company),
                                                    new BinaryOperator("Workplace", this.Workplace),
                                                    new BinaryOperator("Active", true)));
                        }
                        else
                        {
                            _availableCompanyBankAccount = new XPCollection<BankAccount>(Session, new GroupOperator
                                                    (GroupOperatorType.And,
                                                    new BinaryOperator("Company", this.Company),
                                                    new BinaryOperator("Active", true)));
                        }
                    }
                    else
                    {
                        _availableCompanyBankAccount = new XPCollection<BankAccount>(Session,
                            new GroupOperator(GroupOperatorType.And,
                            new BinaryOperator("Active", true)));
                    }
                }

                return _availableCompanyBankAccount;

            }
        }

        [ImmediatePostData()]
        [DataSourceProperty("AvailableCompanyBankAccount", DataSourcePropertyIsNullMode.SelectAll)]
        [Appearance("CustomerInvoiceCompanyBankAccountClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public BankAccount CompanyBankAccount
        {
            get { return _companyBankAccount; }
            set
            {
                SetPropertyValue("CompanyBankAccount", ref _companyBankAccount, value);
                if (!IsLoading)
                {
                    if (this._companyBankAccount != null)
                    {
                        this.CompanyAccountNo = this._companyBankAccount.AccountNo;
                        this.CompanyAccountName = this._companyBankAccount.AccountName;
                    }
                }
            }
        }

        [Appearance("CustomerInvoiceCompanyAccountNoClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public string CompanyAccountNo
        {
            get { return _companyAccountNo; }
            set { SetPropertyValue("CompanyAccountNo", ref _companyAccountNo, value); }
        }

        [Appearance("CustomerInvoiceCompanyAccountNameClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public string CompanyAccountName
        {
            get { return _companyAccountName; }
            set { SetPropertyValue("CompanyAccountName", ref _companyAccountName, value); }
        }

        [Browsable(false)]
        public XPCollection<BankAccount> AvailableBankAccounts
        {
            get
            {
                if(!IsLoading)
                {
                    if (this.BillToCustomer == null)
                    {
                        _availableBankAccounts = new XPCollection<BankAccount>(Session);
                    }
                    else
                    {
                        _availableBankAccounts = new XPCollection<BankAccount>(Session,
                                                 new GroupOperator(GroupOperatorType.And,
                                                 new BinaryOperator("BusinessPartner", this.BillToCustomer)));
                    }
                }
                return _availableBankAccounts;
            }
        }

        [ImmediatePostData()]
        [DataSourceProperty("AvailableBankAccounts", DataSourcePropertyIsNullMode.SelectAll)]
        [Appearance("CustomerInvoiceBankAccountClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public BankAccount BankAccount
        {
            get { return _bankAccount; }
            set
            {
                SetPropertyValue("BankAccount", ref _bankAccount, value);
                if (!IsLoading)
                {
                    if (this._bankAccount != null)
                    {
                        this.AccountNo = this._bankAccount.AccountNo;
                        this.AccountName = this._bankAccount.AccountName;
                    }
                }
            }
        }

        [Appearance("CustomerInvoiceAccountNoClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public string AccountNo
        {
            get { return _accountNo; }
            set { SetPropertyValue("AccountNo", ref _accountNo, value); }
        }

        [Appearance("CustomerInvoiceAccountNameClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public string AccountName
        {
            get { return _accountName; }
            set { SetPropertyValue("AccountName", ref _accountName, value); }
        }

        #endregion Bank

        [Appearance("CustomerInvoiceDescriptionClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [Size(512)]
        public string Description
        {
            get { return _description; }
            set { SetPropertyValue("Description", ref _description, value); }
        }

        public string DJP_No
        {
            get { return _djp_No; }
            set { SetPropertyValue("DJP_No", ref _djp_No, value); }
        }

        [ImmediatePostData()]
        [ModelDefault("DisplayFormat", "{0:dd/MM/yyyy}")]
        [ModelDefault("EditMask", "dd/MM/yyyy")]
        [Appearance("CustomerInvoiceDocDateClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public DateTime DocDate
        {
            get { return _docDate; }
            set
            {
                SetPropertyValue("DocDate", ref _docDate, value);
                if (!IsLoading)
                {
                    SetAccountingPeriodic();
                }
            }
        }

        [Browsable(false)]
        public int JournalMonth
        {
            get { return _journalMonth; }
            set { SetPropertyValue("JournalMonth", ref _journalMonth, value); }
        }

        [Browsable(false)]
        public int JournalYear
        {
            get { return _journalYear; }
            set { SetPropertyValue("JournalYear", ref _journalYear, value); }
        }

        [Appearance("CustomerInvoicePostedCountClose", Enabled = false)]
        public int PostedCount
        {
            get { return _postedCount; }
            set { SetPropertyValue("PostedCount", ref _postedCount, value); }
        }

        [Appearance("CustomerInvoiceSignCodeClose", Enabled = false)]
        public string SignCode
        {
            get { return _signCode; }
            set { SetPropertyValue("SignCode", ref _signCode, value); }
        }

        [Appearance("CustomerInvoiceStatusClose", Enabled = false)]
        public Status Status
        {
            get { return _status; }
            set { SetPropertyValue("Status", ref _status, value); }
        }

        [ModelDefault("DisplayFormat", "{0:dd/MM/yyyy}")]
        [ModelDefault("EditMask", "dd/MM/yyyy")]
        [Appearance("CustomerInvoiceStatusDateClose", Enabled = false)]
        public DateTime StatusDate
        {
            get { return _statusDate; }
            set { SetPropertyValue("StatusDate", ref _statusDate, value); }
        }

        [Appearance("CustomerInvoiceCustomerOrderClose", Enabled = false)]
        public CustomerOrder CustomerOrder
        {
            get { return _customerOrder; }
            set { SetPropertyValue("CustomerOrder", ref _customerOrder, value); }
        }

        [Association("CustomerInvoice-CustomerInvoiceLines")]
        public XPCollection<CustomerInvoiceLine> CustomerInvoiceLines
        {
            get { return GetCollection<CustomerInvoiceLine>("CustomerInvoiceLines"); }
        }

        [Association("CustomerInvoice-CustomerInvoiceFreeItems")]
        public XPCollection<CustomerInvoiceFreeItem> CustomerInvoiceFreeItems
        {
            get { return GetCollection<CustomerInvoiceFreeItem>("CustomerInvoiceFreeItems"); }
        }

        //AuditTrail
        [CollectionOperationSet(AllowAdd = false, AllowRemove = true)]
        public XPCollection<AuditDataItemPersistent> ChangeHistory
        {
            get
            {
                if (changeHistory == null)
                {
                    changeHistory = AuditedObjectWeakReference.GetAuditTrail(Session, this);
                }
                return changeHistory;
            }
        }

        #endregion Field

        //============================================== Code Only ==============================================

        private void SetLocation(Employee _locEmployee)
        {
            try
            {
                DateTime now = DateTime.Now;
                if (this.Company != null && this.Workplace != null)
                {
                    SalesmanLocationSetup _locSalesmanLocationSetup = Session.FindObject<SalesmanLocationSetup>
                                                        (new GroupOperator(GroupOperatorType.And,
                                                        new BinaryOperator("Company", this.Company),
                                                        new BinaryOperator("Workplace", this.Workplace),
                                                        new BinaryOperator("Salesman", _locEmployee),
                                                        new BinaryOperator("Active", true),
                                                        new BinaryOperator("Default", true)));
                    if (_locSalesmanLocationSetup != null)
                    {
                        if (_locSalesmanLocationSetup.Location != null)
                        {
                            this.Location = _locSalesmanLocationSetup.Location;
                        }
                        else
                        {
                            this.Location = null;
                        }
                    }
                    else
                    {
                        this.Location = null;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Business Object = CustomerInvoice ", ex.ToString());
            }
        }

        private void SetBegInv(Location _locLocation)
        {
            try
            {
                DateTime now = DateTime.Now;
                if (this.Company != null && this.Workplace != null)
                {
                    BeginningInventory _locBegInv = Session.FindObject<BeginningInventory>
                                                        (new GroupOperator(GroupOperatorType.And,
                                                        new BinaryOperator("Company", this.Company),
                                                        new BinaryOperator("Workplace", this.Workplace),
                                                        new BinaryOperator("Location", _locLocation),
                                                        new BinaryOperator("LocationType", LocationType.Main),
                                                        new BinaryOperator("StockType", StockType.Good),
                                                        new BinaryOperator("Active", true)));
                    if (_locBegInv != null)
                    {
                        this.BegInv = _locBegInv;
                    }
                    else
                    {
                        this.BegInv = null;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Business Object = CustomerInvoice ", ex.ToString());
            }
        }

        private void SetAccountingPeriodic()
        {
            try
            {
                DateTime now = DateTime.Now;
                if (this.Company != null && this.Workplace != null)
                {
                    AccountingPeriodic _locAcctPeriodic = Session.FindObject<AccountingPeriodic>
                                                        (new GroupOperator(GroupOperatorType.And,
                                                        new BinaryOperator("Company", this.Company),
                                                        new BinaryOperator("Workplace", this.Workplace),
                                                        new BinaryOperator("Active", true)));
                    if (_locAcctPeriodic != null)
                    {
                        this.JournalMonth = _locAcctPeriodic.Month;
                        this.JournalYear = _locAcctPeriodic.Year;
                    }
                    else
                    {
                        this.JournalMonth = 0;
                        this.JournalYear = 0;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Business Object = CustomerInvoice ", ex.ToString());
            }
        }

    }
}