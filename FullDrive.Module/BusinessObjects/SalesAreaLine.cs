﻿using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;

using DevExpress.ExpressApp.Editors;
using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.ConditionalAppearance;

namespace FullDrive.Module.BusinessObjects
{
    [DefaultClassOptions]
    [DefaultProperty("Name")]
    [NavigationItem("Territory")]
    [RuleCombinationOfPropertiesIsUnique("SalesAreaLineUnique", DefaultContexts.Save, "Code")]
    //[ImageName("BO_Contact")]
    //[DefaultProperty("DisplayMemberNameForLookupEditorsOfThisType")]
    //[DefaultListViewOptions(MasterDetailMode.ListViewOnly, false, NewItemRowPosition.None)]
    //[Persistent("DatabaseTableName")]
    // Specify more UI options using a declarative approach (https://documentation.devexpress.com/#eXpressAppFramework/CustomDocument112701).
    public class SalesAreaLine : BaseObject
    { // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).

        private string _code;
        private Company _company;
        private Workplace _workplace;
        private string _name;
        private SalesArea1 _salesArea1;
        private SalesArea2 _salesArea2;
        private SalesArea3 _salesArea3;
        private SalesArea4 _salesArea4;
        private TerritoryLevel1 _territoryLevel1;
        private TerritoryLevel2 _territoryLevel2;
        private TerritoryLevel3 _territoryLevel3;
        private TerritoryLevel4 _territoryLevel4;
        private bool _active;
        private SalesArea _salesArea;
        private GlobalFunction _globFunc;
        //AuditTrail
        private XPCollection<AuditDataItemPersistent> changeHistory;

        public SalesAreaLine(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if (!IsLoading)
            {
                if (Session.IsNewObject(this))
                {
                    _globFunc = new GlobalFunction();
                    this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.SalesAreaLine);
                }
            }
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).
        }

        #region Field

        [RuleRequiredField(DefaultContexts.Save)]
        [Appearance("SalesAreaLineCodeClose", Enabled = false)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue("Code", ref _code, value); }
        }

        [ImmediatePostData()]
        [RuleRequiredField(DefaultContexts.Save)]
        [DataSourceCriteria("Active = true")]
        [Appearance("SalesAreaLineCompanyClose", Enabled = false)]
        public Company Company
        {
            get { return _company; }
            set { SetPropertyValue("Company", ref _company, value); }
        }

        [ImmediatePostData()]
        [RuleRequiredField(DefaultContexts.Save)]
        [DataSourceCriteria("Active = true")]
        [Appearance("SalesAreaWorkplaceClose", Enabled = false)]
        public Workplace Workplace
        {
            get { return _workplace; }
            set { SetPropertyValue("Workplace", ref _workplace, value); }
        }

        [RuleRequiredField(DefaultContexts.Save)]
        public string Name
        {
            get { return _name; }
            set { SetPropertyValue("Name", ref _name, value); }
        }

        [ImmediatePostData()]
        public SalesArea1 SalesArea1
        {
            get { return _salesArea1; }
            set {
                SetPropertyValue("SalesArea1", ref _salesArea1, value);
                if(!IsLoading)
                {
                    if(this._salesArea1 != null)
                    {
                        SetName();
                    }
                }
            }
        }

        [DataSourceCriteria("Active = true And SalesArea1 = '@This.SalesArea1'"), ImmediatePostData()]
        public SalesArea2 SalesArea2
        {
            get { return _salesArea2; }
            set {
                SetPropertyValue("SalesArea2", ref _salesArea2, value);
                if (!IsLoading)
                {
                    if (this._salesArea1 != null)
                    {
                        SetName();
                    }
                }
            }
        }

        [DataSourceCriteria("Active = true And SalesArea2 = '@This.SalesArea2'"), ImmediatePostData()]
        public SalesArea3 SalesArea3
        {
            get { return _salesArea3; }
            set {
                SetPropertyValue("SalesArea3", ref _salesArea3, value);
                if (!IsLoading)
                {
                    if (this._salesArea1 != null)
                    {
                        SetName();
                    }
                }
            }
        }

        [DataSourceCriteria("Active = true And SalesArea3 = '@This.SalesArea3'"), ImmediatePostData()]
        public SalesArea4 SalesArea4
        {
            get { return _salesArea4; }
            set {
                SetPropertyValue("SalesArea4", ref _salesArea4, value);
                if (!IsLoading)
                {
                    if (this._salesArea1 != null)
                    {
                        SetName();
                    }
                }
            }
        }

        [ImmediatePostData()]
        public TerritoryLevel1 TerritoryLevel1
        {
            get { return _territoryLevel1; }
            set {
                SetPropertyValue("TerritoryLevel1", ref _territoryLevel1, value);
                if (!IsLoading)
                {
                    if (this._salesArea1 != null)
                    {
                        SetName();
                    }
                }
            }
        }

        
        [DataSourceCriteria("Active = true And TerritoryLevel1 = '@This.TerritoryLevel1'"), ImmediatePostData()]
        public TerritoryLevel2 TerritoryLevel2
        {
            get { return _territoryLevel2; }
            set {
                SetPropertyValue("TerritoryLevel2", ref _territoryLevel2, value);
                if (!IsLoading)
                {
                    if (this._salesArea1 != null)
                    {
                        SetName();
                    }
                }
            }
        }

        [DataSourceCriteria("Active = true And TerritoryLevel2 = '@This.TerritoryLevel2'"), ImmediatePostData()]
        public TerritoryLevel3 TerritoryLevel3
        {
            get { return _territoryLevel3; }
            set {
                SetPropertyValue("TerritoryLevel3", ref _territoryLevel3, value);
                if (!IsLoading)
                {
                    if (this._salesArea1 != null)
                    {
                        SetName();
                    }
                }
            }
        }

        [DataSourceCriteria("Active = true And TerritoryLevel3 = '@This.TerritoryLevel3'"), ImmediatePostData()]
        public TerritoryLevel4 TerritoryLevel4
        {
            get { return _territoryLevel4; }
            set {
                SetPropertyValue("TerritoryLevel4", ref _territoryLevel4, value);
                if (!IsLoading)
                {
                    if (this._salesArea1 != null)
                    {
                        SetName();
                    }
                }
            }
        }

        public bool Active
        {
            get { return _active; }
            set { SetPropertyValue("Active", ref _active, value); }
        }

        [ImmediatePostData()]
        [RuleRequiredField(DefaultContexts.Save)]
        [Association("SalesArea-SalesAreaLines")]
        public SalesArea SalesArea
        {
            get { return _salesArea; }
            set {
                SetPropertyValue("SalesArea", ref _salesArea, value);
                if(!IsLoading)
                {
                    if(this._salesArea != null)
                    {
                        if(this._salesArea.Company != null)
                        {
                            this.Company = this._salesArea.Company;
                        }
                        if(this._salesArea.Workplace != null)
                        {
                            this.Workplace = this._salesArea.Workplace;
                        }
                        SetName();
                    }
                }
            }
        }

        [Association("SalesAreaLine-CustomerMaps")]
        public XPCollection<CustomerMap> CustomerMaps
        {
            get { return GetCollection<CustomerMap>("CustomerMaps"); }
        }

        //AuditTrail
        [CollectionOperationSet(AllowAdd = false, AllowRemove = true)]
        public XPCollection<AuditDataItemPersistent> ChangeHistory
        {
            get
            {
                if (changeHistory == null)
                {
                    changeHistory = AuditedObjectWeakReference.GetAuditTrail(Session, this);
                }
                return changeHistory;
            }
        }

        #endregion Field

        //=========================================== Code Only ======================================

        #region CodeOnly

        private void SetName()
        {
            string _finalResult = null;
            string _locSalesArea = null;
            string _locSalesArea1 = null;
            string _locSalesArea2 = null;
            string _locSalesArea3 = null;
            string _locSalesArea4 = null;
            string _locTerritoryLevel1 = null;
            string _locTerritoryLevel2 = null;
            string _locTerritoryLevel3 = null;
            string _locTerritoryLevel4 = null;
            try
            {
                if(this._salesArea != null) { if(this._salesArea.Name != null) { _locSalesArea = "<" + this._salesArea.Name + ">"; } }
                if (this._salesArea1 != null) { if (this._salesArea1.Name != null) { _locSalesArea1 = "<" + this._salesArea1.Name + ">"; } }
                if (this._salesArea2 != null) { if (this._salesArea2.Name != null) { _locSalesArea2 = "<" + this._salesArea2.Name + ">"; } }
                if (this._salesArea3 != null) { if (this._salesArea3.Name != null) { _locSalesArea3 = "<" + this._salesArea3.Name + ">"; } }
                if (this._salesArea4 != null) { if (this._salesArea4.Name != null) { _locSalesArea4 = "<" + this._salesArea4.Name + ">"; } }
                if (this._territoryLevel1 != null) { if (this._territoryLevel1.Name != null) { _locTerritoryLevel1 = "<" + this._territoryLevel1.Name + ">"; } }
                if (this._territoryLevel2 != null) { if (this._territoryLevel2.Name != null) { _locTerritoryLevel2 = "<" + this._territoryLevel2.Name + ">"; } }
                if (this._territoryLevel3 != null) { if (this._territoryLevel3.Name != null) { _locTerritoryLevel3 = "<" + this._territoryLevel3.Name + ">"; } }
                if (this._territoryLevel4 != null) { if (this._territoryLevel4.Name != null) { _locTerritoryLevel4 = "<" + this._territoryLevel4.Name + ">"; } }


                _finalResult = String.Format("{0}{1}{2}{3}{4}{5}{6}{7}{8}", _locSalesArea, _locSalesArea1, _locSalesArea2, _locSalesArea3, _locSalesArea4, _locTerritoryLevel1, _locTerritoryLevel2, _locTerritoryLevel3, _locTerritoryLevel4).Trim().Replace(" ", "").ToLower();
                if(_finalResult != null)
                {
                    this.Name = _finalResult;
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = SalesAreaLine ", ex.ToString());
            }
        }

        #endregion CodeOnly
    }
}