﻿using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.ConditionalAppearance;
using DevExpress.ExpressApp.Editors;
using System.Drawing;
using ZXing;

namespace FullDrive.Module.BusinessObjects
{
    [DefaultClassOptions]
    [DefaultProperty("Name")]
    [NavigationItem("Organization")]
    [RuleCombinationOfPropertiesIsUnique("EmployeeRuleUnique", DefaultContexts.Save, "Code")]
    //[ImageName("BO_Contact")]
    //[DefaultProperty("DisplayMemberNameForLookupEditorsOfThisType")]
    //[DefaultListViewOptions(MasterDetailMode.ListViewOnly, false, NewItemRowPosition.None)]
    //[Persistent("DatabaseTableName")]
    // Specify more UI options using a declarative approach (https://documentation.devexpress.com/#eXpressAppFramework/CustomDocument112701).
    public class Employee : BaseObject
    { // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).

        private string _code;
        private string _name;
        private string _address;
        private string _email;
        private string _contact;
        private DateTime _birthday;
        private Image _photo;
        private Country _country;
        private City _city;
        private Company _company;
        private XPCollection<Workplace> _availableWorkplace;
        private Workplace _workplace;
        private Division _division;
        private Department _department;
        private Section _section;
        private Position _position;
        private string _remark;
        private SalesRole _salesRole;
        private bool _openSalesOrderCode;
        private bool _salesOrderCode;
        private bool _openSalesmanAreaSetup;
        private bool _openSalesmanLocationSetup;
        private bool _openSalesmanVehicleSetup;
        private bool _openSalesmanCustomerSetup;
        private bool _thirdParty;
        private BusinessPartner _vendor;
        private bool _active;
        private bool _openEmployeeDocumentTypeSetup;
        private string _userAccess; 
        private GlobalFunction _globFunc;
        //AuditTrail
        private XPCollection<AuditDataItemPersistent> changeHistory;

        public Employee(Session session)
            : base(session)
        {
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if (!IsLoading)
            {
                _globFunc = new GlobalFunction();
                this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.Employee);
                _userAccess = SecuritySystem.CurrentUserName;
                UserAccess _locUserAccess = Session.FindObject<UserAccess>(new BinaryOperator("UserName", _userAccess));
                if (Session.IsNewObject(this))
                {
                    if (_locUserAccess != null)
                    {
                        if (_locUserAccess.Employee != null)
                        {
                            if (_locUserAccess.Employee.Company != null)
                            {
                                Company = _locUserAccess.Employee.Company;
                            }
                            if (_locUserAccess.Employee.Workplace != null)
                            {
                                Workplace = _locUserAccess.Employee.Workplace;
                            }
                            else
                            {
                                this.Workplace = _globFunc.GetDefaultWorkplace(this.Session, _locUserAccess);
                            }
                        }
                    }
                }
            }
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).
        }

        #region Field

        [RuleRequiredField(DefaultContexts.Save)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue("Code", ref _code, value); }
        }

        [RuleRequiredField(DefaultContexts.Save)]
        public string Name
        {
            get { return _name; }
            set { SetPropertyValue("Name", ref _name, value); }
        }

        [Size(512)]
        public string Address
        {
            get { return _address; }
            set { SetPropertyValue("Address", ref _address, value); }
        }

        public string Email
        {
            get { return _email; }
            set { SetPropertyValue("Email", ref _email, value); }
        }

        public string Contact
        {
            get { return _contact; }
            set { SetPropertyValue("Contact", ref _contact, value); }
        }

        [ModelDefault("DisplayFormat", "{0:dd/MM/yyyy}")]
        [ModelDefault("EditMask", "dd/MM/yyyy")]
        public DateTime Birthday
        {
            get { return _birthday; }
            set { SetPropertyValue("Birthday", ref _birthday, value); }
        }

        [ImmediatePostData()]
        [DevExpress.Xpo.Size(SizeAttribute.Unlimited)]
        [VisibleInListViewAttribute(true)]
        [ImageEditor(ListViewImageEditorMode = ImageEditorMode.PictureEdit, DetailViewImageEditorMode = ImageEditorMode.PictureEdit, ListViewImageEditorCustomHeight = 40)]
        public Image Photo
        {
            get { return _photo; }
            set {
                SetPropertyValue("Photo", ref _photo, value);
                if(!IsLoading)
                {
                    string _locString = null;
                    if(this._photo != null)
                    {
                        IBarcodeReader reader = new BarcodeReader();
                        // load a bitmap
                        var barcodeBitmap = (Bitmap)_photo;
                        //var barcodeBitmap = (Bitmap)Bitmap.LoadFrom("C:\\sample-barcode-image.png");
                        // detect and decode the barcode inside the bitmap
                        var result = reader.Decode(barcodeBitmap);
                        // do something with the result
                        if (result != null)
                        {
                            _locString = result.ToString();
                            if(_locString != null)
                            {
                                this.Contact = _locString;
                            }
                        }
                    }
                }
            }
        }

        [Association("Country-Employees")]
        [DataSourceCriteria("Active = true")]
        public Country Country
        {
            get { return _country; }
            set { SetPropertyValue("Country", ref _country, value); }
        }

        [Association("City-Employees")]
        [DataSourceCriteria("Active = true")]
        public City City
        {
            get { return _city; }
            set { SetPropertyValue("City", ref _city, value); }
        }

        [Association("Company-Employees")]
        [DataSourceCriteria("Active = true")]
        [ImmediatePostData()]
        public Company Company
        {
            get { return _company; }
            set { SetPropertyValue("Company", ref _company, value); }
        }

        [Browsable(false)]
        public XPCollection<Workplace> AvailableWorkplace
        {

            get
            {
                if (this.Company != null)
                {
                    _userAccess = SecuritySystem.CurrentUserName;
                    UserAccess _locUserAccess = Session.FindObject<UserAccess>(new BinaryOperator("UserName", _userAccess));
                    if (_locUserAccess != null)
                    {
                        OrganizationSetupDetail _locOrganizationSetupDetail = Session.FindObject<OrganizationSetupDetail>
                                                                        (new GroupOperator(GroupOperatorType.And,
                                                                        new BinaryOperator("UserAccess", _locUserAccess),
                                                                        new BinaryOperator("OpenWorkplace", true),
                                                                        new BinaryOperator("Active", true),
                                                                        new BinaryOperator("Default", true)));

                        if (_locOrganizationSetupDetail != null)
                        {
                            if (_locOrganizationSetupDetail.Workplace != null)
                            {
                                if (_locOrganizationSetupDetail.Workplace.Code != null)
                                {
                                    _availableWorkplace = new XPCollection<Workplace>
                                                (Session, new GroupOperator(GroupOperatorType.And,
                                                new BinaryOperator("Code", _locOrganizationSetupDetail.Workplace.Code),
                                                new BinaryOperator("Active", true)));
                                }
                            }
                        }
                        else
                        {
                            OrganizationSetupDetail _locOrgSetDetail = Session.FindObject<OrganizationSetupDetail>
                                                                        (new GroupOperator(GroupOperatorType.And,
                                                                        new BinaryOperator("UserAccess", _locUserAccess),
                                                                        new BinaryOperator("AllWorkplace", true),
                                                                        new BinaryOperator("Active", true)));
                            if (_locOrgSetDetail != null)
                            {
                                _availableWorkplace = new XPCollection<Workplace>(Session,
                                                    new GroupOperator(GroupOperatorType.And,
                                                    new BinaryOperator("Company", this.Company),
                                                    new BinaryOperator("Active", true)));
                            }
                        }
                    }
                }

                return _availableWorkplace;

            }
        }

        [ImmediatePostData()]
        [Association("Workplace-Employees")]
        [DataSourceProperty("AvailableWorkplace", DataSourcePropertyIsNullMode.SelectAll)]
        public Workplace Workplace
        {
            get { return _workplace; }
            set { SetPropertyValue("Workplace", ref _workplace, value); }
        }

        [Association("Division-Employees")]
        [DataSourceCriteria("Active = true And Company = '@This.Company'"), ImmediatePostData()]
        public Division Division
        {
            get { return _division; }
            set { SetPropertyValue("Division", ref _division, value); }
        }

        [Association("Department-Employees")]
        [DataSourceCriteria("Active = true And Division = '@This.Division'"), ImmediatePostData()]
        public Department Department
        {
            get { return _department; }
            set { SetPropertyValue("Department", ref _department, value); }
        }

        [Association("Section-Employees")]
        [DataSourceCriteria("Active = true And Department = '@This.Department'"), ImmediatePostData()]
        public Section Section
        {
            get { return _section; }
            set { SetPropertyValue("Section", ref _section, value); }
        }

        [Association("Position-Employees")]
        [DataSourceCriteria("Active = true")]
        public Position Position
        {
            get { return _position; }
            set { SetPropertyValue("Position", ref _position, value); }
        }

        [Size(512)]
        public string Remark
        {
            get { return _remark; }
            set { SetPropertyValue("Remark", ref _remark, value); }
        }

        [ImmediatePostData()]
        public SalesRole SalesRole
        {
            get { return _salesRole; }
            set {
                SetPropertyValue("SalesRole", ref _salesRole, value);
                if (this._salesRole == SalesRole.All)
                {
                    this.OpenSalesmanAreaSetup = true;
                    this.OpenSalesmanLocationSetup = true;
                    this.OpenSalesmanVehicleSetup = true;
                    this.OpenSalesmanCustomerSetup = true;
                    this.OpenSalesOrderCode = true;
                }
                if (this._salesRole == SalesRole.Canvassing)
                {
                    this.OpenSalesmanAreaSetup = true;
                    this.OpenSalesmanLocationSetup = true;
                    this.OpenSalesmanVehicleSetup = true;
                    this.OpenSalesmanCustomerSetup = true;
                    this.OpenSalesOrderCode = true;
                }
                if(this._salesRole == SalesRole.TakingOrder)
                {
                    this.OpenSalesmanAreaSetup = true;
                    this.OpenSalesmanLocationSetup = true;
                    this.OpenSalesmanVehicleSetup = true;
                    this.OpenSalesmanCustomerSetup = true;
                    this.OpenSalesOrderCode = true;
                }
                if (this._salesRole == SalesRole.Direct)
                {
                    this.OpenSalesmanLocationSetup = true;
                    this.OpenSalesOrderCode = true;
                }
                if (this._salesRole == SalesRole.None)
                {
                    this.OpenSalesmanAreaSetup = false;
                    this.OpenSalesmanLocationSetup = false;
                    this.OpenSalesmanVehicleSetup = false;
                    this.OpenSalesmanCustomerSetup = false;
                    this.OpenSalesOrderCode = false;
                }
                
            }
        }

        [Browsable(false)]
        [ImmediatePostData()]
        public bool OpenSalesOrderCode
        {
            get { return _openSalesOrderCode; }
            set { SetPropertyValue("OpenSalesOrderCode", ref _openSalesOrderCode, value); }
        }

        [Appearance("EmployeeSalesOrderCodeHide", Criteria = "OpenSalesOrderCode = false", Visibility = ViewItemVisibility.Hide, Context = "DetailView")]
        [Appearance("EmployeeSalesOrderCodeShow", Criteria = "OpenSalesOrderCode = true", Visibility = ViewItemVisibility.Show, Context = "DetailView")]
        public bool SalesOrderCode
        {
            get { return _salesOrderCode; }
            set { SetPropertyValue("SalesOrderCode", ref _salesOrderCode, value); }
        }

        [ImmediatePostData()]
        public bool ThirdParty
        {
            get { return _thirdParty; }
            set { SetPropertyValue("ThirdParty", ref _thirdParty, value); }
        }

        [Appearance("EmployeeVendorClose", Criteria = "ThirdParty = false", Enabled = false)]
        public BusinessPartner Vendor
        {
            get { return _vendor; }
            set { SetPropertyValue("Vendor", ref _vendor, value); }
        }

        public bool Active
        {
            get { return _active; }
            set { SetPropertyValue("Active", ref _active, value); }
        }

        [ImmediatePostData()]
        public bool OpenEmployeeDocumentTypeSetup
        {
            get { return _openEmployeeDocumentTypeSetup; }
            set { SetPropertyValue("OpenEmployeeDocumentTypeSetup", ref _openEmployeeDocumentTypeSetup, value); }
        }

        #region ManyAsociation

        [Association("Employee-UserAcesses")]
        public XPCollection<UserAccess> UserAccesses
        {
            get { return GetCollection<UserAccess>("UserAccesses"); }
        }

        [Association("Employee-Addresses")]
        public XPCollection<Address> Addresses
        {
            get { return GetCollection<Address>("Addresses"); }
        }

        [Association("Employee-PhoneNumbers")]
        public XPCollection<PhoneNumber> PhoneNumbers
        {
            get { return GetCollection<PhoneNumber>("PhoneNumbers"); }
        }

        [Appearance("EmployeeEmployeeDocumentTypeSetupsHide", Criteria = "OpenEmployeeDocumentTypeSetup = false", Visibility = ViewItemVisibility.Hide, Context = "DetailView")]
        [Appearance("EmployeeEmployeeDocumentTypeSetupsShow", Criteria = "OpenEmployeeDocumentTypeSetup = true", Visibility = ViewItemVisibility.Show, Context = "DetailView")]
        [Association("Employee-EmployeeDocumentTypeSetups")]
        public XPCollection<EmployeeDocumentTypeSetup> EmployeeDocumentTypeSetups
        {
            get { return GetCollection<EmployeeDocumentTypeSetup>("EmployeeDocumentTypeSetups"); }
        }

        [Browsable(false)]
        [ImmediatePostData()]
        public bool OpenSalesmanAreaSetup
        {
            get { return _openSalesmanAreaSetup; }
            set { SetPropertyValue("OpenSalesmanAreaSetup", ref _openSalesmanAreaSetup, value); }
        }

        [Appearance("EmployeeSalesmanAreaSetupsHide", Criteria = "OpenSalesmanAreaSetup = false", Visibility = ViewItemVisibility.Hide, Context = "DetailView")]
        [Appearance("EmployeeSalesmanAreaSetupsShow", Criteria = "OpenSalesmanAreaSetup = true", Visibility = ViewItemVisibility.Show, Context = "DetailView")]
        [Association("Employee-SalesmanAreaSetups")]
        public XPCollection<SalesmanAreaSetup> SalesmanAreaSetups
        {
            get { return GetCollection<SalesmanAreaSetup>("SalesmanAreaSetups"); }
        }

        [Browsable(false)]
        [ImmediatePostData()]
        public bool OpenSalesmanLocationSetup
        {
            get { return _openSalesmanLocationSetup; }
            set { SetPropertyValue("OpenSalesmanLocationSetup", ref _openSalesmanLocationSetup, value); }
        }

        [Appearance("EmployeeSalesmanLocationSetupsHide", Criteria = "OpenSalesmanLocationSetup = false", Visibility = ViewItemVisibility.Hide, Context = "DetailView")]
        [Appearance("EmployeeSalesmanLocationSetupsShow", Criteria = "OpenSalesmanLocationSetup = true", Visibility = ViewItemVisibility.Show, Context = "DetailView")]
        [Association("Employee-SalesmanLocationSetups")]
        public XPCollection<SalesmanLocationSetup> SalesmanLocationSetups
        {
            get { return GetCollection<SalesmanLocationSetup>("SalesmanLocationSetups"); }
        }

        [Browsable(false)]
        [ImmediatePostData()]
        public bool OpenSalesmanVehicleSetup
        {
            get { return _openSalesmanVehicleSetup; }
            set { SetPropertyValue("OpenSalesmanVehicleSetup", ref _openSalesmanVehicleSetup, value); }
        }

        [Appearance("EmployeeSalesmanVehicleSetupsHide", Criteria = "OpenSalesmanVehicleSetup = false", Visibility = ViewItemVisibility.Hide, Context = "DetailView")]
        [Appearance("EmployeeSalesmanVehicleSetupsShow", Criteria = "OpenSalesmanVehicleSetup = true", Visibility = ViewItemVisibility.Show, Context = "DetailView")]
        [Association("Employee-SalesmanVehicleSetups")]
        public XPCollection<SalesmanVehicleSetup> SalesmanVehicleSetups
        {
            get { return GetCollection<SalesmanVehicleSetup>("SalesmanVehicleSetups"); }
        }

        [Browsable(false)]
        [ImmediatePostData()]
        public bool OpenSalesmanCustomerSetup
        {
            get { return _openSalesmanCustomerSetup; }
            set { SetPropertyValue("OpenSalesmanCustomerSetup", ref _openSalesmanCustomerSetup, value); }
        }

        [Appearance("EmployeeSalesmanCustomerSetupsHide", Criteria = "OpenSalesmanCustomerSetup = false", Visibility = ViewItemVisibility.Hide, Context = "DetailView")]
        [Appearance("EmployeeSalesmanCustomerSetupsShow", Criteria = "OpenSalesmanCustomerSetup = true", Visibility = ViewItemVisibility.Show, Context = "DetailView")]
        [Association("Employee-SalesmanCustomerSetups")]
        public XPCollection<SalesmanCustomerSetup> SalesmanCustomerSetups
        {
            get { return GetCollection<SalesmanCustomerSetup>("SalesmanCustomerSetups"); }
        }


        [Appearance("EmployeeRoutePlansHide", Criteria = "OpenSalesmanCustomerSetup = false", Visibility = ViewItemVisibility.Hide, Context = "DetailView")]
        [Appearance("EmployeeRoutePlansShow", Criteria = "OpenSalesmanCustomerSetup = true", Visibility = ViewItemVisibility.Show, Context = "DetailView")]
        [Association("Employee-RoutePlans")]
        public XPCollection<RoutePlan> RoutePlans
        {
            get { return GetCollection<RoutePlan>("RoutePlans"); }
        }

        [Appearance("EmployeeRoutesHide", Criteria = "OpenSalesmanCustomerSetup = false", Visibility = ViewItemVisibility.Hide, Context = "DetailView")]
        [Appearance("EmployeeRoutesShow", Criteria = "OpenSalesmanCustomerSetup = true", Visibility = ViewItemVisibility.Show, Context = "DetailView")]
        [Association("Employee-Routes")]
        public XPCollection<Route> Routes
        {
            get { return GetCollection<Route>("Routes"); }
        }

        //AuditTrail
        [CollectionOperationSet(AllowAdd = false, AllowRemove = true)]
        public XPCollection<AuditDataItemPersistent> ChangeHistory
        {
            get
            {
                if (changeHistory == null)
                {
                    changeHistory = AuditedObjectWeakReference.GetAuditTrail(Session, this);
                }
                return changeHistory;
            }
        }

        #endregion ManyAsociation
        #endregion Field

    }
}