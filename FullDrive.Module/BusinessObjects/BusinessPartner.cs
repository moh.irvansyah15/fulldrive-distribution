﻿using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.ConditionalAppearance;
using DevExpress.ExpressApp.Editors;

namespace FullDrive.Module.BusinessObjects
{
    [DefaultClassOptions]
    [DefaultProperty("Name")]
    [NavigationItem("Business Partner")]
    [RuleCombinationOfPropertiesIsUnique("BusinessPartnerRuleUnique", DefaultContexts.Save, "Code")]
    //[ImageName("BO_Contact")]
    //[DefaultProperty("DisplayMemberNameForLookupEditorsOfThisType")]
    //[DefaultListViewOptions(MasterDetailMode.ListViewOnly, false, NewItemRowPosition.None)]
    //[Persistent("DatabaseTableName")]
    // Specify more UI options using a declarative approach (https://documentation.devexpress.com/#eXpressAppFramework/CustomDocument112701).
    public class BusinessPartner : BaseObject
    { // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).

        private string _code;
        private string _name;
        private Country _country;
        private City _city;
        private Company _company;
        private XPCollection<Workplace> _availableWorkplace;
        private Workplace _workplace;
        private string _address;
        private string _contact;
        private string _email;
        private string _taxNo;
        private string _taxName;
        private TermOfPayment _top;
        private PriceGroup _priceGroup;
        private IndustrialType _industrialType;
        private BusinessPartnerGroup _businessPartnerGroup;
        private BusinessPartnerAccountGroup _businessPartnerAccountGroup;
        private BusinessPartnerType _businessPartnerType;
        private double _maxCredit;
        private TerritoryLevel1 _territoryLevel1;
        private XPCollection<TerritoryLevel2> _availableTerritoryLevel2;
        private TerritoryLevel2 _territoryLevel2;
        private XPCollection<TerritoryLevel3> _availableTerritoryLevel3;
        private TerritoryLevel3 _territoryLevel3;
        private XPCollection<TerritoryLevel4> _availableTerritoryLevel4;
        private TerritoryLevel4 _territoryLevel4;
        private string _remark;
        private bool _active;
        private string _userAccess;
        private string _userAccessIn;
        private GlobalFunction _globFunc;
        //AuditTrail
        private XPCollection<AuditDataItemPersistent> changeHistory;

        public BusinessPartner(Session session)
            : base(session)
        {
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if (!IsLoading)
            {
                _globFunc = new GlobalFunction();
                
                _userAccess = SecuritySystem.CurrentUserName;
                UserAccess _locUserAccess = Session.FindObject<UserAccess>(new BinaryOperator("UserName", _userAccess));
                if (Session.IsNewObject(this))
                {
                    if (_locUserAccess != null)
                    {
                        if (_locUserAccess.Employee != null)
                        {
                            if (_locUserAccess.Employee.Company != null && _locUserAccess.Employee.Workplace != null)
                            {
                                this.Code = _globFunc.GetNumberingUnlockOptimisticRecordBasedOrganization(this.Session.DataLayer, ObjectList.BusinessPartner, _locUserAccess.Employee.Company, _locUserAccess.Employee.Workplace);
                            }
                            else
                            {
                                this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.BusinessPartner);
                            }
                            if (_locUserAccess.Employee.Company != null )
                            {
                                this.Company = _locUserAccess.Employee.Company;
                            }
                            if ( _locUserAccess.Employee.Workplace != null)
                            {
                                this.Workplace = _locUserAccess.Employee.Workplace;
                            }else
                            {
                                this.Workplace = _globFunc.GetDefaultWorkplace(this.Session, _locUserAccess);
                            }
                        }  
                    }
                }
            }

            
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).
        }

        #region Field

        [RuleRequiredField(DefaultContexts.Save)]
        [Appearance("BusinessPartnerCodeEnabled", Enabled = false)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue("Code", ref _code, value); }
        }

        [ImmediatePostData()]
        [RuleRequiredField(DefaultContexts.Save)]
        public string Name
        {
            get { return _name; }
            set {
                SetPropertyValue("Name", ref _name, value);
                if(!IsLoading)
                {
                    if(this._name != null)
                    {
                        this.TaxName = this._name;
                    }
                }
            }
        }

        [DataSourceCriteria("Active = true")]
        public Country Country
        {
            get { return _country; }
            set { SetPropertyValue("Country", ref _country, value); }
        }

        [DataSourceCriteria("Active = true")]
        public City City
        {
            get { return _city; }
            set { SetPropertyValue("City", ref _city, value); }
        }

        [ImmediatePostData()]
        [DataSourceCriteria("Active = true")]
        public Company Company
        {
            get { return _company; }
            set { SetPropertyValue("Company", ref _company, value); }
        }

        [Browsable(false)]
        public XPCollection<Workplace> AvailableWorkplace
        {
            get
            {
                if (!IsLoading)
                {
                    if (this.Company != null)
                    {
                        _userAccessIn = SecuritySystem.CurrentUserName;
                        UserAccess _locUserAccess = Session.FindObject<UserAccess>(new BinaryOperator("UserName", _userAccessIn));
                        if (_locUserAccess != null)
                        {
                            string _beginString = null;
                            string _endString = null;
                            string _fullString = null;
                            List<string> _stringWorkplace = new List<string>();

                            XPCollection<OrganizationSetupDetail> _locOrganizationSetupDetails = new XPCollection<OrganizationSetupDetail>
                                                                            (Session, new GroupOperator(GroupOperatorType.And,
                                                                            new BinaryOperator("Company", this.Company),
                                                                            new BinaryOperator("UserAccess", _locUserAccess),
                                                                            new BinaryOperator("OpenWorkplace", true),
                                                                            new BinaryOperator("Active", true)));

                            if (_locOrganizationSetupDetails != null && _locOrganizationSetupDetails.Count() > 0)
                            {
                                foreach (OrganizationSetupDetail _locOrganizationSetupDetail in _locOrganizationSetupDetails)
                                {
                                    if (_locOrganizationSetupDetail.Workplace != null)
                                    {
                                        if (_locOrganizationSetupDetail.Workplace.Code != null)
                                        {
                                            _stringWorkplace.Add(_locOrganizationSetupDetail.Workplace.Code);
                                        }
                                    }
                                }

                                #region Workplace
                                IEnumerable<string> _stringArrayWorkplaceDistinct = _stringWorkplace.Distinct();
                                string[] _stringArrayWorkplaceList = _stringArrayWorkplaceDistinct.ToArray();
                                if (_stringArrayWorkplaceList.Length == 1)
                                {
                                    for (int i = 0; i < _stringArrayWorkplaceList.Length; i++)
                                    {
                                        Workplace _locWorkplace = Session.FindObject<Workplace>(new BinaryOperator("Code", _stringArrayWorkplaceList[i]));
                                        if (_locWorkplace != null)
                                        {
                                            if (i == 0)
                                            {
                                                _beginString = "[Code]=='" + _locWorkplace.Code + "'";
                                            }
                                        }
                                    }
                                }
                                else if (_stringArrayWorkplaceList.Length > 1)
                                {
                                    for (int i = 0; i < _stringArrayWorkplaceList.Length; i++)
                                    {
                                        Workplace _locWorkplace = Session.FindObject<Workplace>(new BinaryOperator("Code", _stringArrayWorkplaceList[i]));
                                        if (_locWorkplace != null)
                                        {
                                            if (i == 0)
                                            {
                                                _beginString = "[Code]=='" + _locWorkplace.Code + "'";
                                            }
                                            else
                                            {
                                                _endString = _endString + " OR [Code]=='" + _locWorkplace.Code + "'";
                                            }
                                        }

                                    }
                                }
                                _fullString = _beginString + _endString;


                                if (_fullString != null)
                                {
                                    _availableWorkplace = new XPCollection<Workplace>(Session, CriteriaOperator.Parse(_fullString));
                                }

                                #endregion Workplace
                            }
                            else
                            {
                                OrganizationSetupDetail _locOrgSetDetail = Session.FindObject<OrganizationSetupDetail>
                                                                            (new GroupOperator(GroupOperatorType.And,
                                                                            new BinaryOperator("Company", this.Company),
                                                                            new BinaryOperator("UserAccess", _locUserAccess),
                                                                            new BinaryOperator("AllWorkplace", true),
                                                                            new BinaryOperator("Active", true)));
                                if (_locOrgSetDetail != null)
                                {
                                    _availableWorkplace = new XPCollection<Workplace>(Session,
                                                        new GroupOperator(GroupOperatorType.And,
                                                        new BinaryOperator("Company", this.Company),
                                                        new BinaryOperator("Active", true)));
                                }
                            }
                        }
                    }
                }

                return _availableWorkplace;

            }
        }

        [ImmediatePostData()]
        [DataSourceProperty("AvailableWorkplace")]
        public Workplace Workplace
        {
            get { return _workplace; }
            set { SetPropertyValue("Workplace", ref _workplace, value); }
        }

        [Size(512)]
        public string Address
        {
            get { return _address; }
            set { SetPropertyValue("Address", ref _address, value); }
        }

        public string Contact
        {
            get { return _contact; }
            set { SetPropertyValue("Contact", ref _contact, value); }
        }

        public string Email
        {
            get { return _email; }
            set { SetPropertyValue("Email", ref _email, value); }
        }

        public string TaxNo
        {
            get { return _taxNo; }
            set { SetPropertyValue("TaxNo", ref _taxNo, value); }
        }

        public string TaxName
        {
            get { return _taxName; }
            set { SetPropertyValue("TaxName", ref _taxName, value); }
        }

        [Association("IndustrialType-BusinessPartners")]
        [DataSourceCriteria("Active = true")]
        public IndustrialType IndustrialType
        {
            get { return _industrialType; }
            set { SetPropertyValue("IndustrialType", ref _industrialType, value); }
        }

        [Association("TOP-BusinesPartners")]
        [DataSourceCriteria("Active = true")]
        public TermOfPayment TOP
        {
            get { return _top; }
            set { SetPropertyValue("TOP", ref _top, value); }
        }

        [Association("PriceGroup-BusinessPartners")]
        public PriceGroup PriceGroup
        {
            get { return _priceGroup; }
            set { SetPropertyValue("PriceGroup", ref _priceGroup, value); }
        }

        [Association("BusinessPartnerGroup-BusinessPartners")]
        public BusinessPartnerGroup BusinessPartnerGroup
        {
            get { return _businessPartnerGroup; }
            set { SetPropertyValue("BusinessPartnerGroup", ref _businessPartnerGroup, value); }
        }

        [DataSourceCriteria("Active = true And Company = '@This.Company' And Workplace = '@This.Workplace'"), ImmediatePostData()]
        [Association("BusinessPartnerAccountGroup-BusinessPartners")]
        public BusinessPartnerAccountGroup BusinessPartnerAccountGroup
        {
            get { return _businessPartnerAccountGroup; }
            set { SetPropertyValue("BusinessPartnerAccountGroup", ref _businessPartnerAccountGroup, value); }
        }

        public BusinessPartnerType BusinessPartnerType
        {
            get { return _businessPartnerType; }
            set { SetPropertyValue("BusinessPartnerType", ref _businessPartnerType, value); }
        }

        public double MaxCredit
        {
            get { return _maxCredit; }
            set { SetPropertyValue("MaxCredit", ref _maxCredit, value); }
        }

        #region Territory

        [ImmediatePostData()]
        public TerritoryLevel1 TerritoryLevel1
        {
            get { return _territoryLevel1; }
            set { SetPropertyValue("TerritoryLevel1", ref _territoryLevel1, value); }
        }

        [Browsable(false)]
        public XPCollection<TerritoryLevel2> AvailableTerritoryLevel2
        {

            get
            {
                if (this.Company != null && this.Workplace != null && TerritoryLevel1 != null)
                {
                    _availableTerritoryLevel2 = new XPCollection<TerritoryLevel2>
                                        (Session, new GroupOperator(GroupOperatorType.And,
                                        new BinaryOperator("Company", this.Company),
                                        new BinaryOperator("Workplace", this.Workplace),
                                        new BinaryOperator("TerritoryLevel1", TerritoryLevel1),
                                        new BinaryOperator("Active", true)));


                }
                else
                {
                    _availableTerritoryLevel2 = new XPCollection<TerritoryLevel2>
                                        (Session, new GroupOperator(GroupOperatorType.And,
                                        new BinaryOperator("Active", true)));
                }

                return _availableTerritoryLevel2;

            }
        }

        [ImmediatePostData()]
        [DataSourceProperty("AvailableTerritoryLevel2", DataSourcePropertyIsNullMode.SelectAll)]
        public TerritoryLevel2 TerritoryLevel2
        {
            get { return _territoryLevel2; }
            set { SetPropertyValue("TerritoryLevel2", ref _territoryLevel2, value); }
        }

        [Browsable(false)]
        public XPCollection<TerritoryLevel3> AvailableTerritoryLevel3
        {

            get
            {
                if (this.Company != null && this.Workplace != null && TerritoryLevel2 != null)
                {
                    _availableTerritoryLevel3 = new XPCollection<TerritoryLevel3>
                                        (Session, new GroupOperator(GroupOperatorType.And,
                                        new BinaryOperator("Company", this.Company),
                                        new BinaryOperator("Workplace", this.Workplace),
                                        new BinaryOperator("TerritoryLevel2", this.TerritoryLevel2),
                                        new BinaryOperator("Active", true)));


                }
                else
                {
                    _availableTerritoryLevel3 = new XPCollection<TerritoryLevel3>
                                        (Session, new GroupOperator(GroupOperatorType.And,
                                        new BinaryOperator("Active", true)));
                }

                return _availableTerritoryLevel3;

            }
        }

        [ImmediatePostData()]
        [DataSourceProperty("AvailableTerritoryLevel3", DataSourcePropertyIsNullMode.SelectAll)]
        public TerritoryLevel3 TerritoryLevel3
        {
            get { return _territoryLevel3; }
            set { SetPropertyValue("TerritoryLevel3", ref _territoryLevel3, value); }
        }

        [Browsable(false)]
        public XPCollection<TerritoryLevel4> AvailableTerritoryLevel4
        {

            get
            {
                if (this.Company != null && this.Workplace != null && TerritoryLevel3 != null)
                {
                    _availableTerritoryLevel4 = new XPCollection<TerritoryLevel4>
                                        (Session, new GroupOperator(GroupOperatorType.And,
                                        new BinaryOperator("Company", this.Company),
                                        new BinaryOperator("Workplace", this.Workplace),
                                        new BinaryOperator("TerritoryLevel3", this.TerritoryLevel3),
                                        new BinaryOperator("Active", true)));


                }
                else
                {
                    _availableTerritoryLevel4 = new XPCollection<TerritoryLevel4>
                                        (Session, new GroupOperator(GroupOperatorType.And,
                                        new BinaryOperator("Active", true)));
                }

                return _availableTerritoryLevel4;

            }
        }

        [ImmediatePostData()]
        [DataSourceProperty("AvailableTerritoryLevel4", DataSourcePropertyIsNullMode.SelectAll)]
        public TerritoryLevel4 TerritoryLevel4
        {
            get { return _territoryLevel4; }
            set { SetPropertyValue("TerritoryLevel4", ref _territoryLevel4, value); }
        }

        #endregion Territory

        [Size(512)]
        public string Remark
        {
            get { return _remark; }
            set { SetPropertyValue("Remark", ref _remark, value); }
        }

        public bool Active
        {
            get { return _active; }
            set { SetPropertyValue("Active", ref _active, value); }
        }

        [Association("BusinessPartner-BankAccounts")]
        public XPCollection<BankAccount> BankAccounts
        {
            get { return GetCollection<BankAccount>("BankAccounts"); }
        }

        [Association("BusinessPartner-Addresses")]
        public XPCollection<Address> Addresses
        {
            get { return GetCollection<Address>("Addresses"); }
        }

        [Association("BusinessPartner-CreditLimitLists")]
        public XPCollection<CreditLimitList> CreditLimitLists
        {
            get { return GetCollection<CreditLimitList>("CreditLimitLists"); }
        }

        [Association("BusinessPartner-BusinessPartnerPriceGroupSetups")]
        public XPCollection<BusinessPartnerPriceGroupSetup> BusinessPartnerPriceGroupSetups
        {
            get { return GetCollection<BusinessPartnerPriceGroupSetup>("BusinessPartnerPriceGroupSetups"); }
        }

        [Association("BusinessPartner-BusinessPartnerAccounts")]
        public XPCollection<BusinessPartnerAccount> BusinessPartnerAccounts
        {
            get { return GetCollection<BusinessPartnerAccount>("BusinessPartnerAccounts"); }
        }

        //AuditTrail
        [CollectionOperationSet(AllowAdd = false, AllowRemove = true)]
        public XPCollection<AuditDataItemPersistent> ChangeHistory
        {
            get
            {
                if (changeHistory == null)
                {
                    changeHistory = AuditedObjectWeakReference.GetAuditTrail(Session, this);
                }
                return changeHistory;
            }
        }

        #endregion Field
    }
}