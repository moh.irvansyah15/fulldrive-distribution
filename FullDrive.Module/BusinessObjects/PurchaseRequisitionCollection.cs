﻿using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;

using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.ConditionalAppearance;

namespace FullDrive.Module.BusinessObjects
{
    [DefaultClassOptions]
    [DefaultProperty("Code")]
    [NavigationItem("Purchase")]
    [RuleCombinationOfPropertiesIsUnique("PurchaseRequisitionCollectionRuleUnique", DefaultContexts.Save, "Code")]
    //[ImageName("BO_Contact")]
    //[DefaultProperty("DisplayMemberNameForLookupEditorsOfThisType")]
    //[DefaultListViewOptions(MasterDetailMode.ListViewOnly, false, NewItemRowPosition.None)]
    //[Persistent("DatabaseTableName")]
    // Specify more UI options using a declarative approach (https://documentation.devexpress.com/#eXpressAppFramework/CustomDocument112701).
    public class PurchaseRequisitionCollection : BaseObject
    { // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).
        private bool _activationPosting;
        private string _code;
        private XPCollection<PurchaseRequisition> _availablePurchaseRequisition;
        private PurchaseRequisition _purchaseRequisition;
        private PurchaseOrder _purchaseOrder;
        #region InitialOrganization
        private Company _company;
        private Workplace _workplace;
        private Division _division;
        private Department _department;
        private Section _section;
        private Employee _employee;
        private Division _div;
        private Department _dept;
        private Section _sect;
        #endregion InitialOrganization
        private Status _status;
        private DateTime _statusDate;      
        private string _localUserAccess;
        private GlobalFunction _globFunc;
        //AuditTrail
        private XPCollection<AuditDataItemPersistent> changeHistory;

        public PurchaseRequisitionCollection(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if (!IsLoading)
            {
                if(Session.IsNewObject(this))
                {
                    _globFunc = new GlobalFunction();
                    #region UserAccess
                    _localUserAccess = SecuritySystem.CurrentUserName;
                    UserAccess _locUserAccess = Session.FindObject<UserAccess>(new BinaryOperator("UserName", _localUserAccess));
                    if (_locUserAccess != null)
                    {
                        if (_locUserAccess.Employee != null)
                        {
                            if (_locUserAccess.Employee.Company != null && _locUserAccess.Employee.Workplace != null)
                            {
                                this.Code = _globFunc.GetNumberingUnlockOptimisticRecordBasedOrganization(this.Session.DataLayer, ObjectList.PurchaseRequisitionCollection, _locUserAccess.Employee.Company, _locUserAccess.Employee.Workplace);
                            }
                            else
                            {
                                this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.PurchaseRequisitionCollection);
                            }
                        }
                    }
                    #endregion UserAccess
                    DateTime now = DateTime.Now;
                    this.Status = Status.Open;
                    this.StatusDate = now;
                }               
            }
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).
        }

        #region Field

        [Browsable(false)]
        public bool ActivationPosting
        {
            get { return _activationPosting; }
            set { SetPropertyValue("ActivationPosting", ref _activationPosting, value); }
        }

        [RuleRequiredField(DefaultContexts.Save)]
        [Appearance("PurchaseRequisitionCollectionCodeClose", Enabled = false)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue("Code", ref _code, value); }
        }

        [Browsable(false)]
        public XPCollection<PurchaseRequisition> AvailablePurchaseRequisition
        {
            get
            {
                if(!IsLoading)
                {
                    string _beginString = null;
                    string _endString = null;
                    string _fullString = null;

                    if (this.PurchaseOrder != null)
                    {
                        XPQuery<PurchaseRequisitionMonitoring> _purchaseRequisitionMonitoringsQuery = new XPQuery<PurchaseRequisitionMonitoring>(Session);

                        var _purchaseRequisitionMonitorings = from prm in _purchaseRequisitionMonitoringsQuery
                                                              where ((prm.Status == Status.Open || prm.Status == Status.Posted)
                                                              && prm.PurchaseOrder == null)
                                                              group prm by prm.PurchaseRequisition into g
                                                              select new { PurchaseRequisition = g.Key };

                        if (_purchaseRequisitionMonitorings != null && _purchaseRequisitionMonitorings.Count() > 0)
                        {
                            List<string> _stringPRM = new List<string>();

                            foreach (var _purchaseRequisitionMonitoring in _purchaseRequisitionMonitorings)
                            {
                                if (_purchaseRequisitionMonitoring != null)
                                {
                                    if (_purchaseRequisitionMonitoring.PurchaseRequisition.Code != null)
                                    {
                                        _stringPRM.Add(_purchaseRequisitionMonitoring.PurchaseRequisition.Code);
                                    }
                                }
                            }

                            IEnumerable<string> _stringArrayPRMDistinct = _stringPRM.Distinct();
                            string[] _stringArrayPRMList = _stringArrayPRMDistinct.ToArray();
                            if (_stringArrayPRMList.Length == 1)
                            {
                                for (int i = 0; i < _stringArrayPRMList.Length; i++)
                                {
                                    if (i == 0)
                                    {
                                        _beginString = "[Code]=='" + _stringArrayPRMList[i] + "'";
                                    }
                                }
                            }
                            else if (_stringArrayPRMList.Length > 1)
                            {
                                for (int i = 0; i < _stringArrayPRMList.Length; i++)
                                {
                                    if (i == 0)
                                    {
                                        _beginString = "[Code]=='" + _stringArrayPRMList[i] + "'";
                                    }
                                    else
                                    {
                                        _endString = _endString + " OR [Code]=='" + _stringArrayPRMList[i] + "'";
                                    }
                                }
                            }
                            _fullString = _beginString + _endString;

                            if (_fullString != null)
                            {
                                _availablePurchaseRequisition = new XPCollection<PurchaseRequisition>(Session, CriteriaOperator.Parse(_fullString));
                            }
                        }
                    }
                }
                return _availablePurchaseRequisition;
            }
        }

        [RuleRequiredField(DefaultContexts.Save)]
        [ImmediatePostData()]
        [DataSourceProperty("AvailablePurchaseRequisition", DataSourcePropertyIsNullMode.SelectNothing)]
        public PurchaseRequisition PurchaseRequisition
        {
            get { return _purchaseRequisition; }
            set { SetPropertyValue("PurchaseRequisition", ref _purchaseRequisition, value); }
        }

        #region Organization

        [Appearance("PurchaseRequisitionCollectionCompanyClose", Enabled = false)]
        public Company Company
        {
            get { return _company; }
            set { SetPropertyValue("Company", ref _company, value); }
        }

        [Appearance("PurchaseRequisitionCollectionWorkplaceClose", Enabled = false)]
        public Workplace Workplace
        {
            get { return _workplace; }
            set { SetPropertyValue("Workplace", ref _workplace, value); }
        }

        [Browsable(false)]
        public Division Division
        {
            get { return _division; }
            set { SetPropertyValue("Division", ref _division, value); }
        }

        [Browsable(false)]
        public Department Department
        {
            get { return _department; }
            set { SetPropertyValue("Department", ref _department, value); }
        }

        [Browsable(false)]
        public Section Section
        {
            get { return _section; }
            set { SetPropertyValue("Section", ref _section, value); }
        }

        [Browsable(false)]
        public Employee Employee
        {
            get { return _employee; }
            set { SetPropertyValue("Employee", ref _employee, value); }
        }

        [ImmediatePostData()]
        [VisibleInListView(false)]
        [DataSourceCriteria("Company = '@This.Company' And Active = true")]
        [Appearance("PurchaseRequisitionCollectionDivClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Division Div
        {
            get { return _div; }
            set { SetPropertyValue("Div", ref _div, value); }
        }

        [ImmediatePostData()]
        [VisibleInListView(false)]
        [DataSourceCriteria("Company = '@This.Company' And Division = '@This.Div' And Active = true")]
        [Appearance("PurchaseRequisitionCollectionDeptClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Department Dept
        {
            get { return _dept; }
            set { SetPropertyValue("Dept", ref _dept, value); }
        }

        [ImmediatePostData()]
        [VisibleInListView(false)]
        [DataSourceCriteria("Company = '@This.Company' And Department = '@This.Dept' And Active = true")]
        [Appearance("PurchaseRequisitionCollectionSectClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Section Sect
        {
            get { return _sect; }
            set { SetPropertyValue("Sect", ref _sect, value); }
        }

        #endregion Organization

        [VisibleInListView(false)]
        [Appearance("PurchaseRequisitionCollectionStatusClose", Enabled = false)]
        public Status Status
        {
            get { return _status; }
            set { SetPropertyValue("Status", ref _status, value); }
        }

        [VisibleInListView(false)]
        [ModelDefault("DisplayFormat", "{0:dd/MM/yyyy}")]
        [ModelDefault("EditMask", "dd/MM/yyyy")]
        [Appearance("PurchaseRequisitionCollectionStatusDateClose", Enabled = false)]
        public DateTime StatusDate
        {
            get { return _statusDate; }
            set { SetPropertyValue("StatusDate", ref _statusDate, value); }
        }

        [VisibleInListView(false)]
        [ImmediatePostData()]
        [Appearance("PurchaseRequisitionCollectionPurchaseOrderClose", Enabled = false)]
        [Association("PurchaseOrder-PurchaseRequisitionCollections")]
        public PurchaseOrder PurchaseOrder
        {
            get { return _purchaseOrder; }
            set
            {
                SetPropertyValue("PurchaseOrder", ref _purchaseOrder, value);
                if (!IsLoading)
                {
                    if (this._purchaseOrder != null)
                    {
                        if (this._purchaseOrder.Company != null) { this.Company = this._purchaseOrder.Company; }
                        if (this._purchaseOrder.Workplace != null) { this.Workplace = this._purchaseOrder.Workplace; }
                        if (this._purchaseOrder.Division != null) { this.Division = this._purchaseOrder.Division; }
                        if (this._purchaseOrder.Department != null) { this.Department = this._purchaseOrder.Department; }
                        if (this._purchaseOrder.Section != null) { this.Section = this._purchaseOrder.Section; }
                        if (this._purchaseOrder.Employee != null) { this.Employee = this._purchaseOrder.Employee; }
                        if (this._purchaseOrder.Div != null) { this.Div = this._purchaseOrder.Div; }
                        if (this._purchaseOrder.Dept != null) { this.Dept = this._purchaseOrder.Dept; }
                        if (this._purchaseOrder.Sect != null) { this.Sect = this._purchaseOrder.Sect; }
                    }
                }
            }
        }

        //AuditTrail
        [CollectionOperationSet(AllowAdd = false, AllowRemove = true)]
        public XPCollection<AuditDataItemPersistent> ChangeHistory
        {
            get
            {
                if (changeHistory == null)
                {
                    changeHistory = AuditedObjectWeakReference.GetAuditTrail(Session, this);
                }
                return changeHistory;
            }
        }

        #endregion Field
        
    }
}