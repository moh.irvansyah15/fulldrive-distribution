﻿using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;

using DevExpress.ExpressApp.Editors;
using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.ConditionalAppearance;

namespace FullDrive.Module.BusinessObjects
{
    [DefaultClassOptions]
    [DefaultProperty("Code")]
    [NavigationItem("Sales")]
    [RuleCombinationOfPropertiesIsUnique("InvoiceCanvassingFreeItemRuleUnique", DefaultContexts.Save, "Code")]
    //[ImageName("BO_Contact")]
    //[DefaultProperty("DisplayMemberNameForLookupEditorsOfThisType")]
    //[DefaultListViewOptions(MasterDetailMode.ListViewOnly, false, NewItemRowPosition.None)]
    //[Persistent("DatabaseTableName")]
    // Specify more UI options using a declarative approach (https://documentation.devexpress.com/#eXpressAppFramework/CustomDocument112701).
    public class InvoiceCanvassingFreeItem : BaseObject
    { // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).

        private bool _activationPosting;
        private int _no;
        private string _code;
        #region InitialOrganization
        private Company _company;
        private Workplace _workplace;
        private Division _division;
        private Department _department;
        private Section _section;
        private Employee _employee;
        #endregion InitialOrganization
        private InvoiceCanvassingLine _invoiceCanvassingLine;
        private bool _mobile;
        #region InisialisasiFPQty
        private Item _fpItem;
        private XPCollection<UnitOfMeasure> _availableUnitOfMeasureFP;
        private double _fpDQty;
        private UnitOfMeasure _fpDUom;
        private double _fpQty;
        private UnitOfMeasure _fpUom;
        private double _fpTQty;
        private StockType _fpStockType;
        #endregion InisialisasiCPQty
        private Status _status;
        private DateTime _statusDate;
        private InvoiceCanvassing _invoiceCanvassing;
        private string _userAccessIn;
        private GlobalFunction _globFunc;
        //AuditTrail
        private XPCollection<AuditDataItemPersistent> changeHistory;

        public InvoiceCanvassingFreeItem(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if (!IsLoading)
            {
                if (Session.IsNewObject(this))
                {
                    _globFunc = new GlobalFunction();
                    DateTime now = DateTime.Now;
                    #region UserAccess
                    _userAccessIn = SecuritySystem.CurrentUserName;
                    UserAccess _locUserAccess = Session.FindObject<UserAccess>(new BinaryOperator("UserName", _userAccessIn));
                    if (_locUserAccess != null)
                    {
                        if (_locUserAccess.Employee != null)
                        {
                            if (_locUserAccess.Mobile == true)
                            {
                                this.Mobile = _locUserAccess.Mobile;
                            }
                            else
                            {
                                if (_locUserAccess.Employee.Company != null && _locUserAccess.Employee.Workplace != null)
                                {
                                    this.Code = _globFunc.GetNumberingUnlockOptimisticRecordBasedOrganization(this.Session.DataLayer, ObjectList.InvoiceCanvassingFreeItem, _locUserAccess.Employee.Company, _locUserAccess.Employee.Workplace);
                                }
                                else
                                {
                                    this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.InvoiceCanvassingFreeItem);
                                }
                            }
                        }
                    }
                    #endregion UserAccess
                    this.Status = CustomProcess.Status.Open;
                    this.StatusDate = now;
                    this.ActivationPosting = true;
                    this.FpStockType = StockType.Good;
                }       
            }
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).
        }

        protected override void OnSaving()
        {
            base.OnSaving();
            if (!IsLoading)
            {
                if(!IsSaving)
                {
                    UpdateNo();
                }
                if (this.Mobile == true)
                {
                    if (Session.IsNewObject(this))
                    {
                        if (!(Session is NestedUnitOfWork))
                        {
                            if (this.Code == null)
                            {
                                this.Code = "ICFIM - " + DistributedIdGeneratorHelper.Generate(this.Session.DataLayer, this.GetType().FullName, string.Empty);
                            }
                        }
                    }
                }
            }
        }
        protected override void OnDeleting()
        {
            base.OnDeleting();
            if (!IsLoading)
            {
                RecoveryDeleteNo();
            }
        }

        #region Field

        [Browsable(false)]
        public bool ActivationPosting
        {
            get { return _activationPosting; }
            set { SetPropertyValue("ActivationPosting", ref _activationPosting, value); }
        }

        [Appearance("InvoiceCanvassingFreeItemNoEnabled", Enabled = false)]
        public int No
        {
            get { return _no; }
            set { SetPropertyValue("No", ref _no, value); }
        }

        [RuleRequiredField(DefaultContexts.Save)]
        [Appearance("InvoiceCanvassingFreeItemCodeClose", Enabled = false)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue("Code", ref _code, value); }
        }

        #region Organization

        [Appearance("InvoiceCanvassingFreeItemCompanyClose", Enabled = false)]
        public Company Company
        {
            get { return _company; }
            set { SetPropertyValue("Company", ref _company, value); }
        }

        [Appearance("InvoiceCanvassingFreeItemWorkplaceClose", Enabled = false)]
        public Workplace Workplace
        {
            get { return _workplace; }
            set { SetPropertyValue("Workplace", ref _workplace, value); }
        }

        [Browsable(false)]
        public Division Division
        {
            get { return _division; }
            set { SetPropertyValue("Division", ref _division, value); }
        }

        [Browsable(false)]
        public Department Department
        {
            get { return _department; }
            set { SetPropertyValue("Department", ref _department, value); }
        }

        [Browsable(false)]
        public Section Section
        {
            get { return _section; }
            set { SetPropertyValue("Section", ref _section, value); }
        }

        [Browsable(false)]
        public Employee Employee
        {
            get { return _employee; }
            set { SetPropertyValue("Employee", ref _employee, value); }
        }

        #endregion Organization

        [Appearance("InvoiceCanvassingFreeItemInvoiceCanvassingLineClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public InvoiceCanvassingLine InvoiceCanvassingLine
        {
            get { return _invoiceCanvassingLine; }
            set { SetPropertyValue("InvoiceCanvassingLine", ref _invoiceCanvassingLine, value); }
        }

        [Browsable(false)]
        [ImmediatePostData()]
        public bool Mobile
        {
            get { return _mobile; }
            set { SetPropertyValue("Mobile", ref _mobile, value); }
        }

        #region FpQty

        [ImmediatePostData()]
        [Appearance("InvoiceCanvassingFreeItemFpItemClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Item FpItem
        {
            get { return _fpItem; }
            set
            {
                SetPropertyValue("FpItem", ref _fpItem, value);
                if (!IsLoading)
                {
                    if (this._fpItem != null)
                    {
                        if (this._fpItem.BasedUOM != null)
                        {
                            this.FpDUOM = this._fpItem.BasedUOM;
                        }
                    }
                }
            }
        }

        [Browsable(false)]
        public XPCollection<UnitOfMeasure> AvailableUnitOfMeasureFP
        {
            get
            {
                if (FpItem == null)
                {
                    _availableUnitOfMeasureFP = new XPCollection<UnitOfMeasure>(Session);
                }
                else
                {
                    if (GetUnitOfMeasureCollection(this.FpItem) != null)
                    {
                        _availableUnitOfMeasureFP = new XPCollection<UnitOfMeasure>(Session, CriteriaOperator.Parse(GetUnitOfMeasureCollection(this.FpItem)));
                    }

                }
                return _availableUnitOfMeasureFP;

            }
        }

        [Appearance("InvoiceCanvassingFreeItemFpDQtyClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [ImmediatePostData()]
        public double FpDQty
        {
            get { return _fpDQty; }
            set
            {
                SetPropertyValue("FpDQty", ref _fpDQty, value);
                if (!IsLoading)
                {
                    SetFpTotalQty();
                }
            }
        }

        [Appearance("InvoiceCanvassingFreeItemFpDUOMClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [ImmediatePostData()]
        [DataSourceCriteria("Active = true")]
        public UnitOfMeasure FpDUOM
        {
            get { return _fpDUom; }
            set
            {
                SetPropertyValue("FpDUOM", ref _fpDUom, value);
                if (!IsLoading)
                {
                    SetFpTotalQty();
                }
            }
        }

        [Appearance("InvoiceCanvassingFreeItemFpQtyClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [ImmediatePostData()]
        public double FpQty
        {
            get { return _fpQty; }
            set
            {
                SetPropertyValue("FpQty", ref _fpQty, value);
                if (!IsLoading)
                {
                    SetFpTotalQty();
                }
            }
        }

        [ImmediatePostData()]
        [Appearance("InvoiceCavanssingFreeItemFpUOMClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [DataSourceProperty("AvailableUnitOfMeasureFP", DataSourcePropertyIsNullMode.SelectAll)]
        [DataSourceCriteria("Active = true")]
        public UnitOfMeasure FpUOM
        {
            get { return _fpUom; }
            set
            {
                SetPropertyValue("FpUOM", ref _fpUom, value);
                if (!IsLoading)
                {
                    SetFpTotalQty();
                }
            }
        }

        [Appearance("InvoiceCanvassingFreeItemFpTQtyEnabled", Enabled = false)]
        public double FpTQty
        {
            get { return _fpTQty; }
            set { SetPropertyValue("FpTQty", ref _fpTQty, value); }
        }

        [Appearance("InvoiceCavanssingFreeItemFpStockTypeClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public StockType FpStockType
        {
            get { return _fpStockType; }
            set { SetPropertyValue("FpStockType", ref _fpStockType, value); }
        }

        #endregion FpQty

        [Appearance("InvoiceCanvassingFreeItemStatusClose", Enabled = false)]
        public Status Status
        {
            get { return _status; }
            set { SetPropertyValue("Status", ref _status, value); }
        }

        [Appearance("InvoiceCanvassingFreeItemStatusDateClose", Enabled = false)]
        public DateTime StatusDate
        {
            get { return _statusDate; }
            set { SetPropertyValue("StatusDate", ref _statusDate, value); }
        }

        [ImmediatePostData()]
        [RuleRequiredField(DefaultContexts.Save)]
        [Appearance("InvoiceCanvassingFreeItemInvoiceCanvassingClose", Enabled = false)]
        [Association("InvoiceCanvassing-InvoiceCanvassingFreeItems")]
        public InvoiceCanvassing InvoiceCanvassing
        {
            get { return _invoiceCanvassing; }
            set
            {
                SetPropertyValue("InvoiceCanvassing", ref _invoiceCanvassing, value);
                if (!IsLoading)
                {

                    if (this._invoiceCanvassing != null)
                    {
                        if (Session.IsNewObject(this))
                        {
                            if (this._invoiceCanvassing.Company != null)
                            {
                                this.Company = this._invoiceCanvassing.Company;
                            }
                            if (this._invoiceCanvassing.Workplace != null)
                            {
                                this.Workplace = this._invoiceCanvassing.Workplace;
                            }
                            if (this._invoiceCanvassing.Division != null)
                            {
                                this.Division = this._invoiceCanvassing.Division;
                            }
                            if (this._invoiceCanvassing.Department != null)
                            {
                                this.Department = this._invoiceCanvassing.Department;
                            }
                            if (this._invoiceCanvassing.Section != null)
                            {
                                this.Section = this._invoiceCanvassing.Section;
                            }
                            if (this._invoiceCanvassing.Employee != null)
                            {
                                this.Employee = this._invoiceCanvassing.Employee;
                            }
                        }
                    }
                }
            }
        }

        //AuditTrail
        [CollectionOperationSet(AllowAdd = false, AllowRemove = true)]
        public XPCollection<AuditDataItemPersistent> ChangeHistory
        {
            get
            {
                if (changeHistory == null)
                {
                    changeHistory = AuditedObjectWeakReference.GetAuditTrail(Session, this);
                }
                return changeHistory;
            }
        }

        #endregion Field

        //================================ Code Only ================================

        #region CodeOnly

        #region No

        public void UpdateNo()
        {
            try
            {
                if (!IsLoading && Session.IsNewObject(this))
                {
                    if (this.InvoiceCanvassing != null)
                    {
                        object _makRecord = Session.Evaluate<InvoiceCanvassingFreeItem>(CriteriaOperator.Parse("Max(No)"), CriteriaOperator.Parse("InvoiceCanvassing=?", this.InvoiceCanvassing));
                        this.No = Convert.ToInt32(_makRecord) + 1;
                        this.Save();
                        RecoveryUpdateNo();
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = InvoiceCanvassingFreeItem " + ex.ToString());
            }
        }

        public void RecoveryUpdateNo()
        {
            try
            {
                if (this.InvoiceCanvassing != null)
                {
                    InvoiceCanvassing _numHeader = Session.FindObject<InvoiceCanvassing>
                                            (new BinaryOperator("Code", this.InvoiceCanvassing.Code));

                    XPCollection<InvoiceCanvassingFreeItem> _numLines = new XPCollection<InvoiceCanvassingFreeItem>
                                                             (Session, new BinaryOperator("InvoiceCanvassing", _numHeader),
                                                              new SortProperty("No", DevExpress.Xpo.DB.SortingDirection.Ascending));
                    if (_numLines != null)
                    {
                        int i1 = 0;
                        foreach (InvoiceCanvassingFreeItem _numLine in _numLines)
                        {
                            i1 += 1;
                            _numLine.No = i1;
                            _numLine.Save();
                        }
                        i1 = 1;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = InvoiceCanvassingFreeItem " + ex.ToString());
            }
        }

        public void RecoveryDeleteNo()
        {
            try
            {
                if (this.InvoiceCanvassing != null)
                {
                    InvoiceCanvassing _numHeader = Session.FindObject<InvoiceCanvassing>
                                            (new BinaryOperator("Code", this.InvoiceCanvassing.Code));

                    XPCollection<InvoiceCanvassingFreeItem> _numLines = new XPCollection<InvoiceCanvassingFreeItem>
                                                             (Session, new GroupOperator(GroupOperatorType.And,
                                                              new BinaryOperator("This", this, BinaryOperatorType.NotEqual),
                                                              new BinaryOperator("InvoiceCanvassing", _numHeader)),
                                                              new SortProperty("No", DevExpress.Xpo.DB.SortingDirection.Ascending));
                    if (_numLines != null)
                    {
                        int i = 0;
                        foreach (InvoiceCanvassingFreeItem _numLine in _numLines)
                        {
                            i += 1;
                            _numLine.No = i;
                            _numLine.Save();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = InvoiceCanvassingFreeItem " + ex.ToString());
            }
        }

        #endregion No

        private string GetUnitOfMeasureCollection(Item _locItem)
        {
            string _result = null;
            try
            {
                string _beginString = null;
                string _endString = null;

                List<string> _stringUOM = new List<string>();

                XPCollection<ItemUnitOfMeasure> _locItemUnitOfMeasures = new XPCollection<ItemUnitOfMeasure>
                                                                          (Session, new GroupOperator(GroupOperatorType.And,
                                                                           new BinaryOperator("Item", _locItem),
                                                                           new BinaryOperator("Active", true)));

                if (_locItemUnitOfMeasures != null && _locItemUnitOfMeasures.Count() > 0)
                {
                    foreach (ItemUnitOfMeasure _locItemUnitOfMeasure in _locItemUnitOfMeasures)
                    {
                        _stringUOM.Add(_locItemUnitOfMeasure.UOM.Code);
                    }
                }

                IEnumerable<string> _stringArrayUOMDistinct = _stringUOM.Distinct();
                string[] _stringArrayUOMList = _stringArrayUOMDistinct.ToArray();
                if (_stringArrayUOMList.Length == 1)
                {
                    for (int i = 0; i < _stringArrayUOMList.Length; i++)
                    {
                        UnitOfMeasure _locUOM = Session.FindObject<UnitOfMeasure>(new BinaryOperator("Code", _stringArrayUOMList[i]));
                        if (_locUOM != null)
                        {
                            if (i == 0)
                            {
                                _beginString = "[Code]=='" + _locUOM.Code + "'";
                            }
                        }
                    }
                }
                else if (_stringArrayUOMList.Length > 1)
                {
                    for (int i = 0; i < _stringArrayUOMList.Length; i++)
                    {
                        UnitOfMeasure _locUOM = Session.FindObject<UnitOfMeasure>(new BinaryOperator("Code", _stringArrayUOMList[i]));
                        if (_locUOM != null)
                        {
                            if (i == 0)
                            {
                                _beginString = "[Code]=='" + _locUOM.Code + "'";
                            }
                            else
                            {
                                _endString = _endString + " OR [Code]=='" + _locUOM.Code + "'";
                            }
                        }
                    }
                }

                _result = _beginString + _endString;

            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = InvoiceCanvassingLine " + ex.ToString());
            }

            return _result;
        }

        private void SetFpTotalQty()
        {
            try
            {
                double _locInvLineTotal = 0;
                DateTime now = DateTime.Now;
                if (this.InvoiceCanvassing != null)
                {
                    if (this.FpItem != null && this.FpUOM != null && this.FpDUOM != null)
                    {
                        ItemUnitOfMeasure _locItemUOM = Session.FindObject<ItemUnitOfMeasure>
                                                        (new GroupOperator(GroupOperatorType.And,
                                                         new BinaryOperator("Item", this.FpItem),
                                                         new BinaryOperator("UOM", this.FpUOM),
                                                         new BinaryOperator("DefaultUOM", this.FpDUOM),
                                                         new BinaryOperator("Active", true)));
                        if (_locItemUOM != null)
                        {
                            if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                            {
                                _locInvLineTotal = this.FpQty * _locItemUOM.DefaultConversion + this.FpDQty;
                            }
                            else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                            {
                                _locInvLineTotal = this.FpQty / _locItemUOM.Conversion + this.FpDQty;
                            }
                            else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                            {
                                _locInvLineTotal = this.FpQty + this.FpDQty;
                            }

                            this.FpTQty = _locInvLineTotal;
                        }
                    }
                    else
                    {
                        _locInvLineTotal = this.FpQty + this.FpDQty;
                        this.FpTQty = _locInvLineTotal;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = InvoiceCanvassingFreeItem " + ex.ToString());
            }
        }

        #endregion CodeOnly
    }
}