﻿using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;

using DevExpress.ExpressApp.Editors;
using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.ConditionalAppearance;

namespace FullDrive.Module.BusinessObjects
{
    [DefaultClassOptions]
    [DefaultProperty("Code")]
    [NavigationItem("Finance & Accounting")]
    [RuleCombinationOfPropertiesIsUnique("PurchaseInvoiceRuleUnique", DefaultContexts.Save, "Code")]
    //[ImageName("BO_Contact")]
    //[DefaultProperty("DisplayMemberNameForLookupEditorsOfThisType")]
    //[DefaultListViewOptions(MasterDetailMode.ListViewOnly, false, NewItemRowPosition.None)]
    //[Persistent("DatabaseTableName")]
    // Specify more UI options using a declarative approach (https://documentation.devexpress.com/#eXpressAppFramework/CustomDocument112701).
    public class PurchaseInvoice : BaseObject
    { // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).
        private bool _activationPosting;
        private string _code;
        private ObjectList _objectList;
        XPCollection<NumberingLine> _availableNumberingLine;
        private NumberingLine _numbering;
        #region Purchase
        private BusinessPartner _buyFromVendor;
        private string _buyFromContact;
        private Country _buyFromCountry;
        private City _buyFromCity;
        private string _buyFromAddress;
        #endregion Purchase
        #region Pay
        private BusinessPartner _payToVendor;
        private string _payToContact;
        private Country _payToCountry;
        private City _payToCity;
        private string _payToAddress;
        #endregion Pay
        private PaymentMethod _paymentMethod;
        private PaymentMethodType _paymentMethodType;
        private PaymentType _paymentType;
        private TermOfPayment _top;
        private Currency _currency;
        #region Bank
        private XPCollection<BankAccount> _availableBankAccountCompany;
        private BankAccount _bankAccountCompany;
        private string _companyAccountNo;
        private string _companyAccountName;
        private XPCollection<BankAccount> _availableBankAccounts;
        private BankAccount _bankAccount;
        private string _accountNo;
        private string _accountName;
        #endregion Bank
        private string _taxNo;
        #region InisialisasiAmount
        private double _tAmount;
        private double _amountDN;
        private double _amountCN;
        private double _amount;
        #endregion InisialisasiAmount
        private string _description;
        private DateTime _estimatedDate;
        private DateTime _docDate;
        private int _journalMonth;
        private int _journalYear;
        private int _postedCount;
        private Status _status;
        private DateTime _statusDate;
        #region InitialOrganization
        private Company _company;
        private Workplace _workplace;
        private Division _division;
        private Department _department;
        private Section _section;
        private Employee _employee;
        #endregion InitialOrganization
        private string _localUserAccess;
        private GlobalFunction _globFunc;
        //Approved
        private bool _activeApproved1;
        private bool _activeApproved2;
        private bool _activeApproved3;
        //AuditTrail
        private XPCollection<AuditDataItemPersistent> changeHistory;

        public PurchaseInvoice(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if (!IsLoading)
            {
                if(Session.IsNewObject(this))
                {
                    _globFunc = new GlobalFunction();
                    DateTime now = DateTime.Now;
                    #region UserAccess
                    _localUserAccess = SecuritySystem.CurrentUserName;
                    UserAccess _locUserAccess = Session.FindObject<UserAccess>(new BinaryOperator("UserName", _localUserAccess));
                    if (_locUserAccess != null)
                    {
                        if (_locUserAccess.Employee != null)
                        {
                            if (_locUserAccess.Employee.Company != null && _locUserAccess.Employee.Workplace != null)
                            {
                                this.Code = _globFunc.GetNumberingUnlockOptimisticRecordBasedOrganization(this.Session.DataLayer, ObjectList.PurchaseInvoice, _locUserAccess.Employee.Company, _locUserAccess.Employee.Workplace);
                            }
                            else
                            {
                                this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.PurchaseInvoice);
                            }
                            if (_locUserAccess.Employee.Company != null)
                            {
                                this.Company = _locUserAccess.Employee.Company;
                            }
                            if (_locUserAccess.Employee.Workplace != null)
                            {
                                this.Workplace = _locUserAccess.Employee.Workplace;
                            }
                            if (_locUserAccess.Employee.Division != null)
                            {
                                Division = _locUserAccess.Employee.Division;
                            }
                            if (_locUserAccess.Employee.Department != null)
                            {
                                Department = _locUserAccess.Employee.Department;
                            }
                            if (_locUserAccess.Employee.Section != null)
                            {
                                Section = _locUserAccess.Employee.Section;
                            }
                            this.Employee = _locUserAccess.Employee;
                        }
                    }
                    #endregion UserAccess
                    this.Status = CustomProcess.Status.Open;
                    this.StatusDate = now;
                    this.ObjectList = CustomProcess.ObjectList.PurchaseInvoice;
                    this.Currency = _globFunc.GetDefaultCurrency(this.Session);
                }                
            }
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).
        }

        #region Field

        [ImmediatePostData()]
        [Browsable(false)]
        public bool ActivationPosting
        {
            get { return _activationPosting; }
            set { SetPropertyValue("ActivationPosting", ref _activationPosting, value); }
        }

        [Appearance("PurchaseInvoiceRedColor", Criteria = "ActiveApproved1 = true", BackColor = "#fd79a8")]
        [Appearance("PurchaseInvoiceYellowColor", Criteria = "ActiveApproved2 = true", BackColor = "#ffeaa7")]
        [Appearance("PurchaseInvoiceGreenOColor", Criteria = "ActiveApproved3 = true", BackColor = "#81ecec")]
        [RuleRequiredField(DefaultContexts.Save)]
        [Appearance("PurchaseInvoiceCodeClose", Enabled = false)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue("Code", ref _code, value); }
        }

        [Browsable(false)]
        public ObjectList ObjectList
        {
            get { return _objectList; }
            set { SetPropertyValue("ObjectList", ref _objectList, value); }
        }

        [Browsable(false)]
        public XPCollection<NumberingLine> AvailableNumberingLine
        {
            get
            {
                if(!IsLoading)
                {
                    if (ObjectList == ObjectList.None)
                    {
                        _availableNumberingLine = new XPCollection<NumberingLine>(Session);
                    }
                    else
                    {
                        _availableNumberingLine = new XPCollection<NumberingLine>(Session,
                                                  new GroupOperator(GroupOperatorType.And,
                                                  new BinaryOperator("ObjectList", this.ObjectList),
                                                  new BinaryOperator("Selection", true),
                                                  new BinaryOperator("Active", true)));
                    }
                }                
                return _availableNumberingLine;
            }
        }

        [NonPersistent()]
        [ImmediatePostData()]
        [DataSourceProperty("AvailableNumberingLine", DataSourcePropertyIsNullMode.SelectAll)]
        public NumberingLine Numbering
        {
            get { return _numbering; }
            set
            {
                NumberingLine _oldNumbering = _numbering;
                SetPropertyValue("Numbering", ref _numbering, value);
                if (!IsLoading)
                {
                    if (_numbering != null)
                    {
                        if (this.Session != null)
                        {
                            if (this.Session.DataLayer != null)
                            {
                                this.Code = LocGetNumberingSelectionUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.PurchaseInvoice, _numbering);
                            }
                        }
                    }
                }
            }
        }

        #region BuyFromVendor

        [ImmediatePostData()]
        [DataSourceCriteria("Active = true")]
        [Appearance("PurchaseInvoiceBuyFromVendorClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public BusinessPartner BuyFromVendor
        {
            get { return _buyFromVendor; }
            set
            {
                SetPropertyValue("BuyFromVendor", ref _buyFromVendor, value);
                if (!IsLoading)
                {
                    if (_buyFromVendor != null)
                    {
                        if (this._buyFromVendor.Contact != null)
                        {
                            this.BuyFromContact = _buyFromVendor.Contact;
                        }
                        if (this._buyFromVendor.Country != null)
                        {
                            this.BuyFromCountry = _buyFromVendor.Country;
                        }
                        if (this._buyFromVendor.City != null)
                        {
                            this.BuyFromCity = _buyFromVendor.City;
                        }
                        if (this._buyFromVendor.Address != null)
                        {
                            this.BuyFromAddress = _buyFromVendor.Address;
                        }
                        if (this._buyFromVendor.TOP != null)
                        {
                            this.TOP = _buyFromVendor.TOP;
                        }

                        this.PayToVendor = _buyFromVendor;
                    }
                }
            }
        }

        [Appearance("PurchaseInvoiceBuyFromContactClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public string BuyFromContact
        {
            get { return _buyFromContact; }
            set { SetPropertyValue("BuyFromContact", ref _buyFromContact, value); }
        }

        [DataSourceCriteria("Active = true")]
        [Appearance("PurchaseInvoiceBuyFromCountryClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Country BuyFromCountry
        {
            get { return _buyFromCountry; }
            set { SetPropertyValue("BuyFromCountry", ref _buyFromCountry, value); }
        }

        [DataSourceCriteria("Active = true")]
        [Appearance("PurchaseInvoiceBuyFromCityClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public City BuyFromCity
        {
            get { return _buyFromCity; }
            set { SetPropertyValue("BuyFromCity", ref _buyFromCity, value); }
        }

        [Appearance("PurchaseInvoiceBuyFromAddressClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [Size(512)]
        public string BuyFromAddress
        {
            get { return _buyFromAddress; }
            set { SetPropertyValue("BuyFromAddress", ref _buyFromAddress, value); }
        }

        #endregion BuyFromVendor

        #region PayToVendor

        [ImmediatePostData()]
        [DataSourceCriteria("Active = true")]
        [Appearance("PurchaseInvoicePayToVendorClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public BusinessPartner PayToVendor
        {
            get { return _payToVendor; }
            set
            {
                SetPropertyValue("PayToVendor", ref _payToVendor, value);
                if (!IsLoading)
                {
                    if (this._payToVendor != null)
                    {
                        if (this._payToVendor.Contact != null)
                        {
                            this.PayToContact = this._payToVendor.Contact;
                        }
                        if (this._payToVendor.Address != null)
                        {
                            this.PayToAddress = this._payToVendor.Address;
                        }
                        if (this._payToVendor.Country != null)
                        {
                            this.PayToCountry = this._payToVendor.Country;
                        }
                        if (this._payToVendor.City != null)
                        {
                            this.PayToCity = this._payToVendor.City;
                        }
                    }
                }
            }
        }

        [Appearance("PurchaseInvoicePayToContactClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public string PayToContact
        {
            get { return _payToContact; }
            set { SetPropertyValue("PayToContact", ref _payToContact, value); }
        }

        [DataSourceCriteria("Active = true")]
        [Appearance("PurchaseInvoicePayToCountryClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Country PayToCountry
        {
            get { return _payToCountry; }
            set { SetPropertyValue("PayToCountry", ref _payToCountry, value); }
        }

        [DataSourceCriteria("Active = true")]
        [Appearance("PurchaseInvoicePayToCityClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public City PayToCity
        {
            get { return _payToCity; }
            set { SetPropertyValue("PayToCity", ref _payToCity, value); }
        }

        [Appearance("PurchaseInvoicePayToAddressClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [Size(512)]
        public string PayToAddress
        {
            get { return _payToAddress; }
            set { SetPropertyValue("PayToAddress", ref _payToAddress, value); }
        }

        #endregion PayToVendor

        [Appearance("PurchaseInvoicePaymentMethodClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public PaymentMethod PaymentMethod
        {
            get { return _paymentMethod; }
            set { SetPropertyValue("PaymentMethod", ref _paymentMethod, value); }
        }

        [Appearance("PurchaseInvoicePaymentMethodTypeClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public PaymentMethodType PaymentMethodType
        {
            get { return _paymentMethodType; }
            set { SetPropertyValue("PaymentMethodType", ref _paymentMethodType, value); }
        }

        [Appearance("PurchaseInvoicePaymentTypeClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public PaymentType PaymentType
        {
            get { return _paymentType; }
            set { SetPropertyValue("PaymentType", ref _paymentType, value); }
        }

        [Appearance("PurchaseInvoiceTermOfPaymentClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public TermOfPayment TOP
        {
            get { return _top; }
            set { SetPropertyValue("TOP", ref _top, value); }
        }

        [Appearance("PurchaseInvoiceCurrencyClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Currency Currency
        {
            get { return _currency; }
            set { SetPropertyValue("Currency", ref _currency, value); }
        }

        #region Bank

        [Browsable(false)]
        public XPCollection<BankAccount> AvailableBankAccountCompany
        {
            get
            {
                if(!IsLoading)
                {
                    if (this.Company == null)
                    {
                        _availableBankAccountCompany = new XPCollection<BankAccount>(Session);
                    }
                    else
                    {
                        _availableBankAccountCompany = new XPCollection<BankAccount>(Session, new GroupOperator
                                                    (GroupOperatorType.And,
                                                    new BinaryOperator("Company", this.Company)));
                    }
                }               
                return _availableBankAccountCompany;
            }
        }

        [ImmediatePostData()]
        [DataSourceProperty("AvailableBankAccountCompany", DataSourcePropertyIsNullMode.SelectAll)]
        [Appearance("PurchaseInvoiceBankAccountCompanyClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public BankAccount BankAccountCompany
        {
            get { return _bankAccountCompany; }
            set
            {
                SetPropertyValue("BankAccountCompany", ref _bankAccountCompany, value);
                if (!IsLoading)
                {
                    if (this._bankAccountCompany != null)
                    {
                        if(this._bankAccountCompany.AccountNo != null)
                        {
                            this.CompanyAccountNo = this._bankAccountCompany.AccountNo;
                        }
                        if(this._bankAccountCompany.AccountName != null)
                        {
                            this.CompanyAccountName = this._bankAccountCompany.AccountName;
                        }                        
                    }
                    else
                    {
                        this.CompanyAccountNo = null;
                        this.CompanyAccountName = null;
                    }
                }
            }
        }

        [Appearance("PurchaseInvoiceCompanyAccountNoClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public string CompanyAccountNo
        {
            get { return _companyAccountNo; }
            set { SetPropertyValue("CompanyAccountNo", ref _companyAccountNo, value); }
        }

        [Appearance("PurchaseInvoiceCompanyAccountNameClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public string CompanyAccountName
        {
            get { return _companyAccountName; }
            set { SetPropertyValue("CompanyAccountName", ref _companyAccountName, value); }
        }

        [Browsable(false)]
        public XPCollection<BankAccount> AvailableBankAccounts
        {
            get
            {
                if(!IsLoading)
                {
                    if (this.PayToVendor == null)
                    {
                        _availableBankAccounts = new XPCollection<BankAccount>(Session);
                    }
                    else
                    {
                        _availableBankAccounts = new XPCollection<BankAccount>(Session, new GroupOperator
                                                    (GroupOperatorType.And,
                                                    new BinaryOperator("BusinessPartner", this.PayToVendor)));
                    }
                }                
                return _availableBankAccounts;
            }
        }

        [ImmediatePostData()]
        [DataSourceProperty("AvailableBankAccounts", DataSourcePropertyIsNullMode.SelectAll)]
        [Appearance("PurchaseInvoiceBankAccountClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public BankAccount BankAccount
        {
            get { return _bankAccount; }
            set
            {
                SetPropertyValue("BankAccount", ref _bankAccount, value);
                if (!IsLoading)
                {
                    if (this._bankAccount != null)
                    {
                        if(this._bankAccount.AccountNo != null)
                        {
                            this.AccountNo = this._bankAccount.AccountNo;
                        }
                        if(this._bankAccount.AccountName != null)
                        {
                            this.AccountName = this._bankAccount.AccountName;
                        }                        
                    }
                    else
                    {
                        this.AccountNo = null;
                        this.AccountName = null;
                    }
                }
            }
        }

        [Appearance("PurchaseInvoiceAccountNoClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public string AccountNo
        {
            get { return _accountNo; }
            set { SetPropertyValue("AccountNo", ref _accountNo, value); }
        }

        [Appearance("PurchaseInvoiceAccountNameClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public string AccountName
        {
            get { return _accountName; }
            set { SetPropertyValue("AccountName", ref _accountName, value); }
        }

        #endregion Bank

        [Appearance("PurchaseInvoiceTaxNoClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public string TaxNo
        {
            get { return _taxNo; }
            set { SetPropertyValue("TaxNo", ref _taxNo, value); }
        }

        #region Amount

        [Appearance("PurchaseInvoiceTAmountClose", Enabled = false)]
        public double TAmount
        {
            get { return _tAmount; }
            set { SetPropertyValue("TAmount", ref _tAmount, value); }
        }

        [ImmediatePostData()]
        [Appearance("PurchaseInvoiceAmountDNClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public double AmountDN
        {
            get { return _amountDN; }
            set
            {
                SetPropertyValue("AmountDN", ref _amountDN, value);
                if (!IsLoading)
                {
                    SetTotalAmount();
                }
            }
        }

        [ImmediatePostData()]
        [Appearance("PurchaseInvoiceAmountCNClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public double AmountCN
        {
            get { return _amountCN; }
            set
            {
                SetPropertyValue("AmountCN", ref _amountCN, value);
                if (!IsLoading)
                {
                    SetTotalAmount();
                }
            }
        }

        //[Appearance("PurchaseInvoiceAmountClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [ImmediatePostData()]
        public double Amount
        {
            get { return _amount; }
            set
            {
                SetPropertyValue("Amount", ref _amount, value);
                if (!IsLoading)
                {
                    SetTotalAmount();
                }
            }
        }

        #endregion Amount

        [ModelDefault("DisplayFormat", "{0:dd/MM/yyyy}")]
        [ModelDefault("EditMask", "dd/MM/yyyy")]
        [Appearance("PurchaseInvoiceEstimatedDateClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public DateTime EstimatedDate
        {
            get { return _estimatedDate; }
            set { SetPropertyValue("EstimatedDate", ref _estimatedDate, value); }
        }

        [Appearance("PurchaseInvoiceDescriptionClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [Size(512)]
        public string Description
        {
            get { return _description; }
            set { SetPropertyValue("Description", ref _description, value); }
        }

        [Browsable(false)]
        [ImmediatePostData()]
        [ModelDefault("DisplayFormat", "{0:dd/MM/yyyy}")]
        [ModelDefault("EditMask", "dd/MM/yyyy")]
        [Appearance("PurchaseInvoiceDocDateClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public DateTime DocDate
        {
            get { return _docDate; }
            set
            {
                SetPropertyValue("DocDate", ref _docDate, value);
                if (!IsLoading)
                {
                    this.JournalMonth = this._docDate.Month;
                    this.JournalYear = this._docDate.Year;
                }
            }
        }

        [Browsable(false)]
        public int JournalMonth
        {
            get { return _journalMonth; }
            set { SetPropertyValue("JournalMonth", ref _journalMonth, value); }
        }

        [Browsable(false)]
        public int JournalYear
        {
            get { return _journalYear; }
            set { SetPropertyValue("JournalYear", ref _journalYear, value); }
        }

        [Appearance("PurchaseInvoicePostedCountClose", Enabled = false)]
        public int PostedCount
        {
            get { return _postedCount; }
            set { SetPropertyValue("PostedCount", ref _postedCount, value); }
        }

        [Appearance("PurchaseInvoiceStatusClose", Enabled = false)]
        public Status Status
        {
            get { return _status; }
            set { SetPropertyValue("Status", ref _status, value); }
        }

        [ModelDefault("DisplayFormat", "{0:dd/MM/yyyy}")]
        [ModelDefault("EditMask", "dd/MM/yyyy")]
        [Appearance("PurchaseInvoiceStatusDateClose", Enabled = false)]
        public DateTime StatusDate
        {
            get { return _statusDate; }
            set { SetPropertyValue("StatusDate", ref _statusDate, value); }
        }

        #region Organization

        [Appearance("PurchaseInvoiceCompanyClose", Enabled = false)]
        public Company Company
        {
            get { return _company; }
            set { SetPropertyValue("Company", ref _company, value); }
        }

        [Appearance("PurchaseInvoiceWorkplaceClose", Enabled = false)]
        public Workplace Workplace
        {
            get { return _workplace; }
            set { SetPropertyValue("Workplace", ref _workplace, value); }
        }

        [Browsable(false)]
        public Division Division
        {
            get { return _division; }
            set { SetPropertyValue("Division", ref _division, value); }
        }

        [Browsable(false)]
        public Department Department
        {
            get { return _department; }
            set { SetPropertyValue("Department", ref _department, value); }
        }

        [Browsable(false)]
        public Section Section
        {
            get { return _section; }
            set { SetPropertyValue("Section", ref _section, value); }
        }

        [Browsable(false)]
        public Employee Employee
        {
            get { return _employee; }
            set { SetPropertyValue("Employee", ref _employee, value); }
        }

        #endregion Organization

        #region Active Approved

        [Browsable(false)]
        [Appearance("PurchaseInvoiceActiveApproved1Close", Enabled = false)]
        public bool ActiveApproved1
        {
            get { return _activeApproved1; }
            set { SetPropertyValue("ActiveApproved1", ref _activeApproved1, value); }
        }

        [Browsable(false)]
        [Appearance("PurchaseInvoiceActiveApproved2Close", Enabled = false)]
        public bool ActiveApproved2
        {
            get { return _activeApproved2; }
            set { SetPropertyValue("ActiveApproved2", ref _activeApproved2, value); }
        }

        [Browsable(false)]
        [Appearance("PurchaseInvoiceActiveApproved3Close", Enabled = false)]
        public bool ActiveApproved3
        {
            get { return _activeApproved3; }
            set { SetPropertyValue("ActiveApproved3", ref _activeApproved3, value); }
        }

        #endregion Active Approved

        [Association("PurchaseInvoice-PurchaseInvoiceLines")]
        public XPCollection<PurchaseInvoiceLine> PurchaseInvoiceLines
        {
            get { return GetCollection<PurchaseInvoiceLine>("PurchaseInvoiceLines"); }
        }

        [Association("PurchaseInvoice-ApprovalLines")]
        public XPCollection<ApprovalLine> ApprovalLines
        {
            get { return GetCollection<ApprovalLine>("ApprovalLines"); }
        }

        [Association("PurchaseInvoice-PaymentOutPlans")]
        public XPCollection<PaymentOutPlan> PaymentOutPlans
        {
            get { return GetCollection<PaymentOutPlan>("PaymentOutPlans"); }
        }

        [Association("PurchaseInvoice-InvoicePurchaseCollections")]
        public XPCollection<InvoicePurchaseCollection> InvoicePurchaseCollections
        {
            get { return GetCollection<InvoicePurchaseCollection>("InvoicePurchaseCollections"); }
        }    

        //AuditTrail
        [CollectionOperationSet(AllowAdd = false, AllowRemove = true)]
        public XPCollection<AuditDataItemPersistent> ChangeHistory
        {
            get
            {
                if (changeHistory == null)
                {
                    changeHistory = AuditedObjectWeakReference.GetAuditTrail(Session, this);
                }
                return changeHistory;
            }
        }

        #endregion Field

        //============================================ Code Only ============================================

        #region LocalNumbering

        public string LocGetNumberingSelectionUnlockOptimisticRecord(IDataLayer _currIDataLayer, ObjectList _currObject, NumberingLine _currNumberingLine)
        {
            string _result = null;
            Session _generatorSession = null;
            try
            {
                if (_currIDataLayer != null)
                {
                    _generatorSession = new Session(_currIDataLayer);
                }

                if (_generatorSession != null)
                {
                    ApplicationSetup _appSetup = _generatorSession.FindObject<ApplicationSetup>(new GroupOperator(GroupOperatorType.And,
                                            new BinaryOperator("Active", true), new BinaryOperator("DefaultSystem", true)));
                    if (_appSetup != null)
                    {
                        NumberingHeader _numberingHeader = _generatorSession.FindObject<NumberingHeader>
                                                            (new GroupOperator(GroupOperatorType.And,
                                                             new BinaryOperator("NumberingType", NumberingType.Objects),
                                                             new BinaryOperator("ApplicationSetup", _appSetup, BinaryOperatorType.Equal),
                                                             new BinaryOperator("Active", true)));
                        if (_numberingHeader != null)
                        {
                            NumberingLine _numberingLine = _generatorSession.FindObject<NumberingLine>
                                                        (new GroupOperator(GroupOperatorType.And,
                                                         new BinaryOperator("Oid", _currNumberingLine.Oid),
                                                         new BinaryOperator("ObjectList", _currObject),
                                                         new BinaryOperator("Selection", true),
                                                         new BinaryOperator("Active", true),
                                                         new BinaryOperator("NumberingHeader", _numberingHeader)));
                            if (_numberingLine != null)
                            {
                                _numberingLine.LastValue = _numberingLine.LastValue + _numberingLine.IncrementValue;
                                _numberingLine.Save();
                                _result = _numberingLine.FormatedValue;
                                _numberingLine.Session.CommitTransaction();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Module = GlobalFunction " + ex.ToString());
            }
            return _result;
        }

        #endregion LocalNumbering

        private void SetTotalAmount()
        {
            try
            {
                _globFunc = new GlobalFunction();

                if (_amount >= 0 || _amountCN >= 0 || _amountDN >= 0)
                {
                    if (_globFunc.GetRoundingList(Session, ObjectList.PurchaseInvoice, FieldName.TAmount) == true)
                    {
                        this.TAmount = _globFunc.GetRoundUp(Session, ((this.Amount + _amountDN) - this.AmountCN), ObjectList.PurchaseInvoice, FieldName.TAmount);
                    }
                    else
                    {
                        this.TAmount = (this.Amount + _amountDN) - this.AmountCN;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PurchaseInvoice " + ex.ToString());
            }
        }

    }
}