﻿using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;

using DevExpress.ExpressApp.Editors;
using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.ConditionalAppearance;

namespace FullDrive.Module.BusinessObjects
{
    [DefaultClassOptions]
    [DefaultProperty("Code")]
    [NavigationItem("Sales")]
    [RuleCombinationOfPropertiesIsUnique("RoutePlanRuleUnique", DefaultContexts.Save, "Code")]
    //[ImageName("BO_Contact")]
    //[DefaultProperty("DisplayMemberNameForLookupEditorsOfThisType")]
    //[DefaultListViewOptions(MasterDetailMode.ListViewOnly, false, NewItemRowPosition.None)]
    //[Persistent("DatabaseTableName")]
    // Specify more UI options using a declarative approach (https://documentation.devexpress.com/#eXpressAppFramework/CustomDocument112701).
    public class RoutePlan : BaseObject
    { // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).

        private int _no;
        private bool _select;
        private string _code;
        private string _name;
        private Company _company;
        private XPCollection<Workplace> _availableWorkplace;
        private Workplace _workplace;
        private XPCollection<Employee> _availableSalesman;
        private Employee _salesman;        
        private PriceGroup _priceGroup;
        private XPCollection<SalesArea> _availableSalesArea;
        private SalesArea _salesArea;
        private XPCollection<SalesAreaLine> _availableSalesAreaLine;
        private SalesAreaLine _salesAreaLine;
        private XPCollection<BusinessPartner> _availableCustomer;
        private BusinessPartner _customer;
        private ScheduleType _scheduleType;
        private bool _isRangeDate;
        private DateTime _startDate;
        private DateTime _endDate;
        private bool _active;
        private string _userAccessIn;
        private GlobalFunction _globFunc;
        //AuditTrail
        private XPCollection<AuditDataItemPersistent> changeHistory;

        public RoutePlan(Session session)
            : base(session)
        {
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if (!IsLoading)
            {
                if (Session.IsNewObject(this))
                {
                    _globFunc = new GlobalFunction();
                    #region UserAccess
                    _userAccessIn = SecuritySystem.CurrentUserName;
                    UserAccess _locUserAccess = Session.FindObject<UserAccess>(new BinaryOperator("UserName", _userAccessIn));
                
                    if (_locUserAccess != null)
                    {
                        if (_locUserAccess.Employee != null)
                        {
                            if (_locUserAccess.Employee.Company != null && _locUserAccess.Employee.Workplace != null)
                            {
                                this.Code = _globFunc.GetNumberingUnlockOptimisticRecordBasedOrganization(this.Session.DataLayer, ObjectList.RoutePlan, _locUserAccess.Employee.Company, _locUserAccess.Employee.Workplace);
                            }
                            else
                            {
                                this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.RoutePlan);
                            }
                            if (_locUserAccess.Employee.Company != null)
                            {
                                Company = _locUserAccess.Employee.Company;
                            }
                            if (_locUserAccess.Employee.Workplace != null)
                            {
                                Workplace = _locUserAccess.Employee.Workplace;
                            }else
                            {
                                this.Workplace = _globFunc.GetDefaultWorkplace(this.Session, _locUserAccess);
                            }
                        }
                    }
                    #endregion UserAccess
                }
            }
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).
        }

        protected override void OnSaving()
        {
            base.OnSaving();
            if (!IsLoading)
            {
                UpdateNo();
            }
        }

        protected override void OnDeleting()
        {
            base.OnDeleting();
            if (!IsLoading)
            {
                RecoveryDeleteNo();
            }
        }

        #region Field

        [Appearance("RoutePlanNoEnabled", Enabled = false)]
        public int No
        {
            get { return _no; }
            set { SetPropertyValue("No", ref _no, value); }
        }

        public bool Select
        {
            get { return _select; }
            set { SetPropertyValue("Select", ref _select, value); }
        }

        [Appearance("RoutePlanCodeEnabled", Enabled = false)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue("Code", ref _code, value); }
        }

        [RuleRequiredField(DefaultContexts.Save)]
        public string Name
        {
            get { return _name; }
            set { SetPropertyValue("Name", ref _name, value); }
        }

        [ImmediatePostData()]
        [RuleRequiredField(DefaultContexts.Save)]
        [DataSourceCriteria("Active = true")]
        public Company Company
        {
            get { return _company; }
            set { SetPropertyValue("Company", ref _company, value); }
        }

        [Browsable(false)]
        public XPCollection<Workplace> AvailableWorkplace
        {

            get
            {
                if (this.Company != null)
                {
                    _userAccessIn = SecuritySystem.CurrentUserName;
                    UserAccess _locUserAccess = Session.FindObject<UserAccess>(new BinaryOperator("UserName", _userAccessIn));
                    if (_locUserAccess != null)
                    {
                        OrganizationSetupDetail _locOrganizationSetupDetail = Session.FindObject<OrganizationSetupDetail>
                                                                        (new GroupOperator(GroupOperatorType.And,
                                                                        new BinaryOperator("UserAccess", _locUserAccess),
                                                                        new BinaryOperator("OpenWorkplace", true),
                                                                        new BinaryOperator("Active", true),
                                                                        new BinaryOperator("Default", true)));

                        if (_locOrganizationSetupDetail != null)
                        {
                            if (_locOrganizationSetupDetail.Workplace != null)
                            {
                                if (_locOrganizationSetupDetail.Workplace.Code != null)
                                {
                                    _availableWorkplace = new XPCollection<Workplace>
                                                (Session, new GroupOperator(GroupOperatorType.And,
                                                new BinaryOperator("Code", _locOrganizationSetupDetail.Workplace.Code),
                                                new BinaryOperator("Active", true)));
                                }
                            }
                        }
                        else
                        {
                            OrganizationSetupDetail _locOrgSetDetail = Session.FindObject<OrganizationSetupDetail>
                                                                        (new GroupOperator(GroupOperatorType.And,
                                                                        new BinaryOperator("UserAccess", _locUserAccess),
                                                                        new BinaryOperator("AllWorkplace", true),
                                                                        new BinaryOperator("Active", true)));
                            if (_locOrgSetDetail != null)
                            {
                                _availableWorkplace = new XPCollection<Workplace>(Session,
                                                    new GroupOperator(GroupOperatorType.And,
                                                    new BinaryOperator("Company", this.Company),
                                                    new BinaryOperator("Active", true)));
                            }
                        }
                    }
                }

                return _availableWorkplace;

            }
        }

        [ImmediatePostData()]
        [DataSourceProperty("AvailableWorkplace", DataSourcePropertyIsNullMode.SelectAll)]
        public Workplace Workplace
        {
            get { return _workplace; }
            set { SetPropertyValue("Workplace", ref _workplace, value); }
        }

        [Browsable(false)]
        public XPCollection<Employee> AvailableSalesman
        {

            get
            {
                if (this.Company != null && this.Workplace != null)
                {
                    XPCollection<Employee> _locEmployees = new XPCollection<Employee>(Session,
                                                        new GroupOperator(GroupOperatorType.And,
                                                        new BinaryOperator("Company", this.Company),
                                                        new BinaryOperator("Workplace", this.Workplace),
                                                        new BinaryOperator("Active", true),
                                                        new GroupOperator(GroupOperatorType.Or,
                                                        new BinaryOperator("SalesRole", SalesRole.Canvassing),
                                                        new BinaryOperator("SalesRole", SalesRole.TakingOrder))));

                    if (_locEmployees != null && _locEmployees.Count() > 0)
                    {
                        _availableSalesman = _locEmployees;
                    }
                }
                else
                {
                    XPCollection<Employee> _locEmployees = new XPCollection<Employee>(Session,
                                                        new GroupOperator(GroupOperatorType.And,
                                                        new BinaryOperator("Active", true),
                                                        new GroupOperator(GroupOperatorType.Or,
                                                        new BinaryOperator("SalesRole", SalesRole.Canvassing),
                                                        new BinaryOperator("SalesRole", SalesRole.TakingOrder))));

                    if (_locEmployees != null && _locEmployees.Count() > 0)
                    {
                        _availableSalesman = _locEmployees;
                    }
                }

                return _availableSalesman;

            }
        }

        [ImmediatePostData()]
        [Association("Employee-RoutePlans")]
        [RuleRequiredField(DefaultContexts.Save)]
        [DataSourceProperty("AvailableSalesman", DataSourcePropertyIsNullMode.SelectAll)]
        public Employee Salesman
        {
            get { return _salesman; }
            set
            {
                SetPropertyValue("Salesman", ref _salesman, value);
                if (_salesman != null)
                {
                    if (this._salesman.Company != null)
                    {
                        this.Company = this._salesman.Company;
                    }
                    if (this._salesman.Workplace != null)
                    {
                        this.Workplace = this._salesman.Workplace;
                    }
                }
            }
        }

        [ImmediatePostData()]
        public PriceGroup PriceGroup
        {
            get { return _priceGroup; }
            set { SetPropertyValue("PriceGroup", ref _priceGroup, value); }
        }

        [Browsable(false)]
        public XPCollection<SalesArea> AvailableSalesArea
        {

            get
            {

                string _beginString = null;
                string _endString = null;
                string _fullString = null;

                if (this.Company != null && this.Workplace != null && this.Salesman != null)
                {
                    List<string> _stringSAS = new List<string>();

                    XPCollection<SalesmanAreaSetup> _locSalesmanAreaSetups = new XPCollection<SalesmanAreaSetup>
                                                                              (Session, new GroupOperator(GroupOperatorType.And,
                                                                                new BinaryOperator("Company", this.Company),
                                                                                new BinaryOperator("Workplace", this.Workplace),
                                                                                new BinaryOperator("Salesman", this.Salesman),
                                                                                new BinaryOperator("Active", true)));

                    if (_locSalesmanAreaSetups != null && _locSalesmanAreaSetups.Count() > 0)
                    {
                        foreach (SalesmanAreaSetup _locSalesmanAreaSetup in _locSalesmanAreaSetups)
                        {
                            _stringSAS.Add(_locSalesmanAreaSetup.SalesArea.Code);
                        }
                    }

                    IEnumerable<string> _stringArraySASDistinct = _stringSAS.Distinct();
                    string[] _stringArraySASList = _stringArraySASDistinct.ToArray();
                    if (_stringArraySASList.Length == 1)
                    {
                        for (int i = 0; i < _stringArraySASList.Length; i++)
                        {
                            SalesArea _locSAS = Session.FindObject<SalesArea>(new BinaryOperator("Code", _stringArraySASList[i]));
                            if (_locSAS != null)
                            {
                                if (i == 0)
                                {
                                    _beginString = "[Code]=='" + _locSAS.Code + "'";
                                }
                            }
                        }
                    }
                    else if (_stringArraySASList.Length > 1)
                    {
                        for (int i = 0; i < _stringArraySASList.Length; i++)
                        {
                            SalesArea _locSAS = Session.FindObject<SalesArea>(new BinaryOperator("Code", _stringArraySASList[i]));
                            if (_locSAS != null)
                            {
                                if (i == 0)
                                {
                                    _beginString = "[Code]=='" + _locSAS.Code + "'";
                                }
                                else
                                {
                                    _endString = _endString + " OR [Code]=='" + _locSAS.Code + "'";
                                }
                            }
                        }
                    }
                    _fullString = _beginString + _endString;

                    if (_fullString != null)
                    {
                        _availableSalesArea = new XPCollection<SalesArea>(Session, CriteriaOperator.Parse(_fullString));
                    }else
                    {
                        XPCollection<SalesArea> _locSalesAreas = new XPCollection<SalesArea>(Session,
                                                        new GroupOperator(GroupOperatorType.And,
                                                        new BinaryOperator("Company", this.Company),
                                                        new BinaryOperator("Workplace", this.Workplace),
                                                        new BinaryOperator("Active", true)));

                        if (_locSalesAreas != null && _locSalesAreas.Count() > 0)
                        {
                            _availableSalesArea = _locSalesAreas;
                        }
                    }

                }
                else
                {
                    XPCollection<SalesArea> _locSalesAreas = new XPCollection<SalesArea>(Session,
                                                        new GroupOperator(GroupOperatorType.And,
                                                        new BinaryOperator("Active", true)));

                    if (_locSalesAreas != null && _locSalesAreas.Count() > 0)
                    {
                        _availableSalesArea = _locSalesAreas;
                    }
                }

                return _availableSalesArea;

            }
        }

        [ImmediatePostData()]
        [DataSourceProperty("AvailableSalesArea", DataSourcePropertyIsNullMode.SelectAll)]
        public SalesArea SalesArea
        {
            get { return _salesArea; }
            set { SetPropertyValue("SalesArea", ref _salesArea, value); }
        }

        [Browsable(false)]
        public XPCollection<SalesAreaLine> AvailableSalesAreaLine
        {
            get
            {

                string _beginString = null;
                string _endString = null;
                string _fullString = null;

                if (this.Company != null && this.Workplace != null && this.Salesman != null && this.SalesArea != null)
                {
                    List<string> _stringSAS = new List<string>();

                    XPCollection<SalesmanAreaSetup> _locSalesmanAreaSetups = new XPCollection<SalesmanAreaSetup>
                                                                              (Session, new GroupOperator(GroupOperatorType.And,
                                                                                new BinaryOperator("Company", this.Company),
                                                                                new BinaryOperator("Workplace", this.Workplace),
                                                                                new BinaryOperator("Salesman", this.Salesman),
                                                                                new BinaryOperator("SalesArea", this.SalesArea),
                                                                                new BinaryOperator("Active", true)));

                    if (_locSalesmanAreaSetups != null && _locSalesmanAreaSetups.Count() > 0)
                    {
                        foreach (SalesmanAreaSetup _locSalesmanAreaSetup in _locSalesmanAreaSetups)
                        {
                            if (_locSalesmanAreaSetup.SalesAreaLine != null)
                            {
                                if (_locSalesmanAreaSetup.SalesAreaLine.Code != null)
                                {
                                    _stringSAS.Add(_locSalesmanAreaSetup.SalesAreaLine.Code);
                                }
                            }

                        }
                    }

                    IEnumerable<string> _stringArraySASDistinct = _stringSAS.Distinct();
                    string[] _stringArraySASList = _stringArraySASDistinct.ToArray();
                    if (_stringArraySASList.Length == 1)
                    {
                        for (int i = 0; i < _stringArraySASList.Length; i++)
                        {
                            SalesAreaLine _locSAS = Session.FindObject<SalesAreaLine>(new BinaryOperator("Code", _stringArraySASList[i]));
                            if (_locSAS != null)
                            {
                                if (i == 0)
                                {
                                    _beginString = "[Code]=='" + _locSAS.Code + "'";
                                }
                            }
                        }
                    }
                    else if (_stringArraySASList.Length > 1)
                    {
                        for (int i = 0; i < _stringArraySASList.Length; i++)
                        {
                            SalesAreaLine _locSAS = Session.FindObject<SalesAreaLine>(new BinaryOperator("Code", _stringArraySASList[i]));
                            if (_locSAS != null)
                            {
                                if (i == 0)
                                {
                                    _beginString = "[Code]=='" + _locSAS.Code + "'";
                                }
                                else
                                {
                                    _endString = _endString + " OR [Code]=='" + _locSAS.Code + "'";
                                }
                            }
                        }
                    }
                    _fullString = _beginString + _endString;

                    if (_fullString != null)
                    {
                        _availableSalesAreaLine = new XPCollection<SalesAreaLine>(Session, CriteriaOperator.Parse(_fullString));
                    }
                    else
                    {
                        XPCollection<SalesAreaLine> _locSalesAreaLines = new XPCollection<SalesAreaLine>(Session,
                                                        new GroupOperator(GroupOperatorType.And,
                                                        new BinaryOperator("Company", this.Company),
                                                        new BinaryOperator("Workplace", this.Workplace),
                                                        new BinaryOperator("SalesArea", this.SalesArea),
                                                        new BinaryOperator("Active", true)));

                        if (_locSalesAreaLines != null && _locSalesAreaLines.Count() > 0)
                        {
                            _availableSalesAreaLine = _locSalesAreaLines;
                        }
                    }

                }
                
                else
                {
                    XPCollection<SalesAreaLine> _locSalesAreaLines = new XPCollection<SalesAreaLine>(Session,
                                                        new GroupOperator(GroupOperatorType.And,
                                                        new BinaryOperator("Active", true)));

                    if (_locSalesAreaLines != null && _locSalesAreaLines.Count() > 0)
                    {
                        _availableSalesAreaLine = _locSalesAreaLines;
                    }
                }

                return _availableSalesAreaLine;

            }
        }

        [ImmediatePostData()]
        [DataSourceProperty("AvailableSalesAreaLine", DataSourcePropertyIsNullMode.SelectAll)]
        public SalesAreaLine SalesAreaLine
        {
            get { return _salesAreaLine; }
            set { SetPropertyValue("SalesAreaLine", ref _salesAreaLine, value); }
        }

        //Ambil ke SalesmanCustomerSetup
        [Browsable(false)]
        public XPCollection<BusinessPartner> AvailableCustomer
        {
            get
            {
                string _beginString = null;
                string _endString = null;
                string _fullString = null;

                #region SalesmanCustomerSetup
                if (this.Company != null && this.Workplace != null && this.PriceGroup != null && this.SalesArea != null && this.SalesAreaLine != null)
                {
                    List<string> _stringSCS = new List<string>();

                    XPCollection<SalesmanCustomerSetup> _locSalesmanCustomerSetups = new XPCollection<SalesmanCustomerSetup>
                                                                              (Session, new GroupOperator(GroupOperatorType.And,
                                                                                new BinaryOperator("Company", this.Company),
                                                                                new BinaryOperator("Workplace", this.Workplace),
                                                                                new BinaryOperator("PriceGroup", this.PriceGroup),
                                                                                new BinaryOperator("SalesArea", this.SalesArea),
                                                                                new BinaryOperator("SalesAreaLine", this.SalesAreaLine),
                                                                                new BinaryOperator("Active", true)));

                    if (_locSalesmanCustomerSetups != null && _locSalesmanCustomerSetups.Count() > 0)
                    {
                        foreach (SalesmanCustomerSetup _locSalesmanCustomerSetup in _locSalesmanCustomerSetups)
                        {
                            _stringSCS.Add(_locSalesmanCustomerSetup.Customer.Code);
                        }
                    }

                    IEnumerable<string> _stringArraySCSDistinct = _stringSCS.Distinct();
                    string[] _stringArraySCSList = _stringArraySCSDistinct.ToArray();
                    if (_stringArraySCSList.Length == 1)
                    {
                        for (int i = 0; i < _stringArraySCSList.Length; i++)
                        {
                            BusinessPartner _locBP = Session.FindObject<BusinessPartner>(new BinaryOperator("Code", _stringArraySCSList[i]));
                            if (_locBP != null)
                            {
                                if (i == 0)
                                {
                                    _beginString = "[Code]=='" + _locBP.Code + "'";
                                }
                            }
                        }
                    }
                    else if (_stringArraySCSList.Length > 1)
                    {
                        for (int i = 0; i < _stringArraySCSList.Length; i++)
                        {
                            BusinessPartner _locBP = Session.FindObject<BusinessPartner>(new BinaryOperator("Code", _stringArraySCSList[i]));
                            if (_locBP != null)
                            {
                                if (i == 0)
                                {
                                    _beginString = "[Code]=='" + _locBP.Code + "'";
                                }
                                else
                                {
                                    _endString = _endString + " OR [Code]=='" + _locBP.Code + "'";
                                }
                            }
                        }
                    }
                    _fullString = _beginString + _endString;

                    if (_fullString != null)
                    {
                        _availableCustomer = new XPCollection<BusinessPartner>(Session, CriteriaOperator.Parse(_fullString));
                    }
                    else
                    {
                        if (this.Company != null && this.Workplace != null && this.PriceGroup != null && this.SalesArea == null && this.SalesAreaLine == null)
                        {
                            XPCollection<BusinessPartner> _locCustomers = new XPCollection<BusinessPartner>(Session,
                                                                new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("Company", this.Company),
                                                                new BinaryOperator("Workplace", this.Workplace),
                                                                new BinaryOperator("PriceGroup", this.PriceGroup),
                                                                new BinaryOperator("Active", true)));

                            if (_locCustomers != null && _locCustomers.Count() > 0)
                            {
                                _availableCustomer = _locCustomers;
                            }
                        }
                    }
                }
                #endregion SalesmanCustomerSetup
                #region BusinessPartner
                else
                {
                    XPCollection<BusinessPartner> _locCustomers = new XPCollection<BusinessPartner>(Session,
                                                        new GroupOperator(GroupOperatorType.And,
                                                        new BinaryOperator("Active", true)));

                    if (_locCustomers != null && _locCustomers.Count() > 0)
                    {
                        _availableCustomer = _locCustomers;
                    }
                }
                #endregion BusinessPartner
                return _availableCustomer;

            }
        }

        [ImmediatePostData()]
        [DataSourceProperty("AvailableCustomer", DataSourcePropertyIsNullMode.SelectAll)]
        public BusinessPartner Customer
        {
            get { return _customer; }
            set
            {
                SetPropertyValue("Customer", ref _customer, value);
                if (!IsLoading)
                {
                    if (this._customer != null)
                    {
                        if (this._customer.Name != null)
                        {
                            this.Name = this._customer.Name;
                        }
                    }
                    else
                    {
                        this.Name = null;
                    }
                }
            }
        }

        public ScheduleType ScheduleType
        {
            get { return _scheduleType; }
            set { SetPropertyValue("ScheduleType", ref _scheduleType, value); }
        }

        [ImmediatePostData()]
        public bool IsRangeDate
        {
            get { return _isRangeDate; }
            set { SetPropertyValue("IsRangeDate", ref _isRangeDate, value); }
        }

        [Appearance("RoutePlanStartDateClose1", Criteria = "IsRangeDate = true", Enabled = true)]
        [Appearance("RoutePlanStartDateClose2", Criteria = "IsRangeDate = false", Enabled = false)]
        public DateTime StartDate
        {
            get { return _startDate; }
            set { SetPropertyValue("StartDate", ref _startDate, value); }
        }

        [Appearance("RoutePlanEndDateClose1", Criteria = "IsRangeDate = true", Enabled = true)]
        [Appearance("RoutePlanEndDateClose2", Criteria = "IsRangeDate = false", Enabled = false)]
        public DateTime EndDate
        {
            get { return _endDate; }
            set { SetPropertyValue("EndDate", ref _endDate, value); }
        }

        public bool Active
        {
            get { return _active; }
            set { SetPropertyValue("Active", ref _active, value); }
        }

        //AuditTrail
        [CollectionOperationSet(AllowAdd = false, AllowRemove = true)]
        public XPCollection<AuditDataItemPersistent> ChangeHistory
        {
            get
            {
                if (changeHistory == null)
                {
                    changeHistory = AuditedObjectWeakReference.GetAuditTrail(Session, this);
                }
                return changeHistory;
            }
        }

        #endregion Field

        //========================================================================= Code Only =================================================================

        #region CodeOnly

        #region Numbering

        public void UpdateNo()
        {
            try
            {
                if (!IsLoading && Session.IsNewObject(this))
                {
                    if (this.Salesman != null)
                    {
                        object _makRecord = Session.Evaluate<RoutePlan>(CriteriaOperator.Parse("Max(No)"), CriteriaOperator.Parse("Salesman=?", this.Salesman));
                        this.No = Convert.ToInt32(_makRecord) + 1;
                        this.Save();
                        RecoveryUpdateNo();
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = RoutePlan " + ex.ToString());
            }
        }

        public void RecoveryUpdateNo()
        {
            try
            {
                if (this.Salesman != null)
                {
                    Employee _numHeader = Session.FindObject<Employee>
                                                (new BinaryOperator("Code", this.Salesman.Code));

                    XPCollection<RoutePlan> _numLines = new XPCollection<RoutePlan>
                                                (Session, new BinaryOperator("Salesman", _numHeader),
                                                new SortProperty("No", DevExpress.Xpo.DB.SortingDirection.Ascending));
                    if (_numLines != null)
                    {
                        int i1 = 0;
                        foreach (RoutePlan _numLine in _numLines)
                        {
                            i1 += 1;
                            _numLine.No = i1;
                            _numLine.Save();
                        }
                        i1 = 1;
                        Session.CommitTransaction();
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = RoutePlan " + ex.ToString());
            }
        }

        public void RecoveryDeleteNo()
        {
            try
            {
                if (this.Salesman != null)
                {
                    Employee _numHeader = Session.FindObject<Employee>
                                                (new BinaryOperator("Code", this.Salesman.Code));

                    XPCollection<RoutePlan> _numLines = new XPCollection<RoutePlan>
                                                (Session, new GroupOperator(GroupOperatorType.And,
                                                 new BinaryOperator("This", this, BinaryOperatorType.NotEqual),
                                                 new BinaryOperator("Salesman", _numHeader)),
                                                 new SortProperty("No", DevExpress.Xpo.DB.SortingDirection.Ascending));
                    if (_numLines != null)
                    {
                        int i = 0;
                        foreach (RoutePlan _numLine in _numLines)
                        {
                            i += 1;
                            _numLine.No = i;
                            _numLine.Save();
                        }
                        Session.CommitTransaction();
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = RoutePlan " + ex.ToString());
            }
        }

        #endregion Numbering

        #endregion CodeOnly
    }
}