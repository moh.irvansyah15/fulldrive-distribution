﻿using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;

using DevExpress.ExpressApp.Editors;
using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.ConditionalAppearance;

namespace FullDrive.Module.BusinessObjects
{
    [DefaultClassOptions]
    [DefaultProperty("Code")]
    [NavigationItem("Sales")]
    [RuleCombinationOfPropertiesIsUnique("SalesInvoiceRuleUnique", DefaultContexts.Save, "Code")]
    //[ImageName("BO_Contact")]
    //[DefaultProperty("DisplayMemberNameForLookupEditorsOfThisType")]
    //[DefaultListViewOptions(MasterDetailMode.ListViewOnly, false, NewItemRowPosition.None)]
    //[Persistent("DatabaseTableName")]
    // Specify more UI options using a declarative approach (https://documentation.devexpress.com/#eXpressAppFramework/CustomDocument112701).
    public class SalesInvoice : BaseObject
    { // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).

        #region InitialSystem
        private bool _activationPosting;
        private bool _activeApproved1;
        private bool _activeApproved2;
        private bool _activeApproved3;
        private string _code;
        XPCollection<NumberingLine> _availableNumberingLine;
        private NumberingLine _numbering;
        #endregion InitialSystem
        #region InitialOrganization
        private Company _company;
        private Workplace _workplace;
        private Division _division;
        private Department _department;
        private Section _section;
        private Employee _employee;
        private Division _div;
        private Department _dept;
        private Section _sect;
        private XPCollection<Employee> _availablePIC;
        private Employee _pic;
        private XPCollection<Location> _availableLocation;
        private Location _location;
        private BeginningInventory _begInv;
        #endregion InitialOrganization
        #region InitialSales
        private DateTime _etd;
        private DateTime _eta;
        private BusinessPartner _salesToCustomer;
        private string _salesToContact;
        private Country _salesToCountry;
        private City _salesToCity;
        private string _salesToAddress;
        #endregion InitialSales
        #region InitialBill
        XPCollection<BusinessPartner> _availableSalesToCustomer;
        private BusinessPartner _billToCustomer;
        private string _billToContact;
        private Country _billToCountry;
        private City _billToCity;
        private string _billToAddress;
        #endregion InitialBill
        #region InitialAmount
        private Currency _currency;
        private PriceGroup _priceGroup;
        private PaymentMethod _paymentMethod;
        private PaymentMethodType _paymentMethodType;
        private PaymentType _paymentType;
        private TermOfPayment _top;
        private DateTime _dueDate;
        private string _taxNo;
        private double _totAmount;
        private double _totUnitAmount;
        private double _totTaxAmount;
        private double _totDiscAmount;
        private double _amount;
        private double _totFIAmount;
        private double _amountDisc;
        private DiscountRule _discountRule;
        private AccountingPeriodicLine _accountingPeriodicLine;
        private CreditLimitList _creditLimitList;
        private bool _overCredit;
        private string _message;
        private CreditLimitStatus _creditLimitStatus;
        private DateTime _creditLimitStatusDate;
        #endregion InitialAmount
        #region InitialBank
        private XPCollection<BankAccount> _availableCompanyBankAccount;
        private BankAccount _companyBankAccount;
        private string _companyAccountNo;
        private string _companyAccountName;
        private XPCollection<BankAccount> _availableBankAccounts;
        private BankAccount _bankAccount;
        private string _accountNo;
        private string _accountName;
        #endregion InitialBank
        private string _description;
        private string _djp_No;
        private DateTime _docDate;
        private int _journalMonth;
        private int _journalYear;
        private int _postedCount;
        private Status _status;
        private DateTime _statusDate;
        private string _userAccess;
        private GlobalFunction _globFunc;
        //AuditTrail
        private XPCollection<AuditDataItemPersistent> changeHistory;


        public SalesInvoice(Session session)
            : base(session)
        {
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if (!IsLoading)
            {
                if (Session.IsNewObject(this))
                {
                    _globFunc = new GlobalFunction();
                    DateTime now = DateTime.Now;
                    #region UserAccess
                    _userAccess = SecuritySystem.CurrentUserName;
                    UserAccess _locUserAccess = Session.FindObject<UserAccess>(new BinaryOperator("UserName", _userAccess));

                    if (_locUserAccess != null)
                    {
                        if (_locUserAccess.Employee != null)
                        {
                            if (_locUserAccess.Employee.Company != null && _locUserAccess.Employee.Workplace != null)
                            {
                                this.Code = _globFunc.GetNumberingUnlockOptimisticRecordBasedOrganization(this.Session.DataLayer, ObjectList.SalesInvoice, _locUserAccess.Employee.Company, _locUserAccess.Employee.Workplace);
                            }
                            else
                            {
                                this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.SalesInvoice);
                            }
                            if (_locUserAccess.Employee.SalesRole == SalesRole.All || _locUserAccess.Employee.SalesRole == SalesRole.TakingOrder)
                            {
                                this.PIC = _locUserAccess.Employee;
                            }
                            if (_locUserAccess.Employee.Company != null)
                            {
                                Company = _locUserAccess.Employee.Company;
                            }
                            if (_locUserAccess.Employee.Workplace != null)
                            {
                                Workplace = _locUserAccess.Employee.Workplace;
                            }else
                            {
                                this.Workplace = _globFunc.GetDefaultWorkplace(this.Session, _locUserAccess);
                            }
                            if (_locUserAccess.Employee.Division != null)
                            {
                                this.Division = _locUserAccess.Employee.Division;
                                this.Div = _locUserAccess.Employee.Division;
                            }
                            if (_locUserAccess.Employee.Department != null)
                            {
                                this.Department = _locUserAccess.Employee.Department;
                                this.Dept = _locUserAccess.Employee.Department;
                            }
                            if (_locUserAccess.Employee.Section != null)
                            {
                                this.Section = _locUserAccess.Employee.Section;
                                this.Sect = _locUserAccess.Employee.Section;
                            }
                            this.Employee = _locUserAccess.Employee;
                        } 
                    }
                    #endregion UserAccess
                    this.Status = CustomProcess.Status.Open;
                    this.StatusDate = now;
                    this.Currency = _globFunc.GetDefaultCurrency(this.Session);
                    this.ETA = now;
                    this.ETD = now;
                    this.DocDate = now;
                    this.JournalMonth = this.DocDate.Month;
                    this.JournalYear = this.DocDate.Year;
                }
            }
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).
        }

        protected override void OnDeleting()
        {
            base.OnDeleting();
            if(!IsLoading)
            {
                if(this.CreditLimitList != null)
                {
                    double _locReductCal = 0;
                    double _locCurrCredit = 0;
                    if(this.Status == Status.Posted || this.Status == Status.Close)
                    {
                        _locReductCal = this.CreditLimitList.CurrCredit - this.Amount;
                        if (_locReductCal < 0)
                        {
                            _locCurrCredit = 0;
                        }else
                        {
                            _locCurrCredit = _locReductCal;
                        }

                        this.CreditLimitList.CurrCredit = _locCurrCredit;
                        this.CreditLimitList.Save();
                        this.CreditLimitList.Session.CommitTransaction();
                    }
                }
            }
        }

        #region Field

        #region System

        [Browsable(false)]
        [ImmediatePostData()]
        public bool ActivationPosting
        {
            get { return _activationPosting; }
            set { SetPropertyValue("ActivationPosting", ref _activationPosting, value); }
        }

        [Browsable(false)]
        [Appearance("SalesInvoiceActiveApproved1Close", Enabled = false)]
        public bool ActiveApproved1
        {
            get { return _activeApproved1; }
            set { SetPropertyValue("ActiveApproved1", ref _activeApproved1, value); }
        }

        [Browsable(false)]
        [Appearance("SalesInvoiceActiveApproved2Close", Enabled = false)]
        public bool ActiveApproved2
        {
            get { return _activeApproved2; }
            set { SetPropertyValue("ActiveApproved2", ref _activeApproved2, value); }
        }

        [Browsable(false)]
        [Appearance("SalesInvoiceActiveApproved3Close", Enabled = false)]
        public bool ActiveApproved3
        {
            get { return _activeApproved3; }
            set { SetPropertyValue("ActiveApproved3", ref _activeApproved3, value); }
        }

        [Appearance("SalesInvoiceRedColor", Criteria = "ActiveApproved1 = true", BackColor = "#fd79a8")]
        [Appearance("SalesInvoiceYellowColor", Criteria = "ActiveApproved2 = true", BackColor = "#ffeaa7")]
        [Appearance("SalesInvoiceGreenOColor", Criteria = "ActiveApproved3 = true", BackColor = "#81ecec")]
        [RuleRequiredField(DefaultContexts.Save)]
        [Appearance("SalesInvoiceCodeClose", Enabled = false)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue("Code", ref _code, value); }
        }

        [Browsable(false)]
        public XPCollection<NumberingLine> AvailableNumberingLine
        {
            get
            {
                XPCollection<NumberingLine> _locNumberingLines = new XPCollection<NumberingLine>(Session,
                                              new GroupOperator(GroupOperatorType.And,
                                              new BinaryOperator("ObjectList", ObjectList.SalesInvoice),
                                              new BinaryOperator("Selection", true),
                                              new BinaryOperator("Active", true)));
                if (_locNumberingLines != null && _locNumberingLines.Count() > 0)
                {
                    _availableNumberingLine = _locNumberingLines;
                }
                else
                {
                    _availableNumberingLine = new XPCollection<NumberingLine>(Session,
                                              new GroupOperator(GroupOperatorType.And,
                                              new BinaryOperator("Active", true)));
                }
                return _availableNumberingLine;
            }
        }

        [NonPersistent()]
        [ImmediatePostData()]
        [DataSourceProperty("AvailableNumberingLine", DataSourcePropertyIsNullMode.SelectAll)]
        public NumberingLine Numbering
        {
            get { return _numbering; }
            set
            {
                SetPropertyValue("Numbering", ref _numbering, value);
                if (!IsLoading)
                {
                    _globFunc = new GlobalFunction();
                    if (_numbering != null)
                    {
                        this.Code = _globFunc.GetNumberingSelectionUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.SalesInvoice, _numbering);
                    }
                }
            }
        }
        #endregion System

        #region Organization

        [ImmediatePostData()]
        [RuleRequiredField(DefaultContexts.Save)]
        [DataSourceCriteria("Active = true")]
        [Appearance("SalesInvoiceCompanyClose", Enabled = false)]
        public Company Company
        {
            get { return _company; }
            set { SetPropertyValue("Company", ref _company, value); }
        }

        [ImmediatePostData()]
        [RuleRequiredField(DefaultContexts.Save)]
        [DataSourceCriteria("Active = true")]
        [Appearance("SalesInvoiceWorkplaceClose", Enabled = false)]
        public Workplace Workplace
        {
            get { return _workplace; }
            set { SetPropertyValue("Workplace", ref _workplace, value); }
        }

        [Browsable(false)]
        public Division Division
        {
            get { return _division; }
            set { SetPropertyValue("Division", ref _division, value); }
        }

        [Browsable(false)]
        public Department Department
        {
            get { return _department; }
            set { SetPropertyValue("Department", ref _department, value); }
        }

        [Browsable(false)]
        public Section Section
        {
            get { return _section; }
            set { SetPropertyValue("Section", ref _section, value); }
        }

        [Browsable(false)]
        public Employee Employee
        {
            get { return _employee; }
            set { SetPropertyValue("Employee", ref _employee, value); }
        }

        [ImmediatePostData()]
        [DataSourceCriteria("Company = '@This.Company' And Active = true")]
        [Appearance("SalesInvoiceDivClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Division Div
        {
            get { return _div; }
            set { SetPropertyValue("Div", ref _div, value); }
        }

        [ImmediatePostData()]
        [DataSourceCriteria("Company = '@This.Company' And Division = '@This.Div' And Active = true")]
        [Appearance("SalesInvoiceDeptClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Department Dept
        {
            get { return _dept; }
            set { SetPropertyValue("Dept", ref _dept, value); }
        }

        [ImmediatePostData()]
        [DataSourceCriteria("Company = '@This.Company' And Department = '@This.Dept' And Active = true")]
        [Appearance("SalesInvoiceSectClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Section Sect
        {
            get { return _sect; }
            set { SetPropertyValue("Sect", ref _sect, value); }
        }

        [Browsable(false)]
        public XPCollection<Employee> AvailablePIC
        {
            get
            {
                XPCollection<Employee> _locPICs = null;
                if (this.Company != null && this.Workplace != null)
                {
                    #region DivDeptSect
                    if (this.Div != null && this.Dept != null && this.Sect != null)
                    {
                        _locPICs = new XPCollection<Employee>
                                  (Session, new GroupOperator(GroupOperatorType.And,
                                    new BinaryOperator("Company", this.Company),
                                    new BinaryOperator("Workplace", this.Workplace),
                                    new BinaryOperator("Division", this.Div),
                                    new BinaryOperator("Department", this.Dept),
                                    new BinaryOperator("Section", this.Sect),
                                    new BinaryOperator("Active", true),
                                    new GroupOperator(GroupOperatorType.Or,
                                    new BinaryOperator("SalesRole", SalesRole.TakingOrder),
                                    new BinaryOperator("SalesRole", SalesRole.All))));
                    }
                    else if (this.Div != null && this.Dept != null && this.Sect == null)
                    {
                        _locPICs = new XPCollection<Employee>
                                  (Session, new GroupOperator(GroupOperatorType.And,
                                    new BinaryOperator("Company", this.Company),
                                    new BinaryOperator("Workplace", this.Workplace),
                                    new BinaryOperator("Division", this.Div),
                                    new BinaryOperator("Department", this.Dept),
                                    new BinaryOperator("Active", true),
                                    new GroupOperator(GroupOperatorType.Or,
                                    new BinaryOperator("SalesRole", SalesRole.TakingOrder),
                                    new BinaryOperator("SalesRole", SalesRole.All))));

                    }
                    else if (this.Div != null && this.Dept == null && this.Sect == null)
                    {
                        _locPICs = new XPCollection<Employee>
                                  (Session, new GroupOperator(GroupOperatorType.And,
                                    new BinaryOperator("Company", this.Company),
                                    new BinaryOperator("Workplace", this.Workplace),
                                    new BinaryOperator("Division", this.Div),
                                    new BinaryOperator("Active", true),
                                    new GroupOperator(GroupOperatorType.Or,
                                    new BinaryOperator("SalesRole", SalesRole.TakingOrder),
                                    new BinaryOperator("SalesRole", SalesRole.All))));

                    }
                    else if (this.Div == null && this.Dept == null && this.Sect == null)
                    {
                        _locPICs = new XPCollection<Employee>
                                  (Session, new GroupOperator(GroupOperatorType.And,
                                    new BinaryOperator("Company", this.Company),
                                    new BinaryOperator("Workplace", this.Workplace),
                                    new BinaryOperator("Active", true),
                                    new GroupOperator(GroupOperatorType.Or,
                                    new BinaryOperator("SalesRole", SalesRole.TakingOrder),
                                    new BinaryOperator("SalesRole", SalesRole.All))));

                    }
                    #endregion DivDeptSect
                    if (_locPICs != null && _locPICs.Count() > 0)
                    {
                        _availablePIC = _locPICs;
                    }
                }
                else
                {
                    _availablePIC = new XPCollection<Employee>(Session, new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("Active", true),
                                                            new GroupOperator(GroupOperatorType.Or,
                                                            new BinaryOperator("SalesRole", SalesRole.TakingOrder),
                                                            new BinaryOperator("SalesRole", SalesRole.All))));
                }

                return _availablePIC;

            }
        }

        [ImmediatePostData()]
        [DataSourceProperty("AvailablePIC")]
        [Appearance("SalesInvoicePICClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Employee PIC
        {
            get { return _pic; }
            set {
                SetPropertyValue("InternalPIC", ref _pic, value);
                if (!IsLoading)
                {
                    if (this._pic != null)
                    {
                        SetLocation(_pic);
                    }
                    else
                    {
                        this.Location = null;
                    }
                }
            }
        }

        [Browsable(false)]
        public XPCollection<Location> AvailableLocation
        {
            get
            {
                if(!IsLoading)
                {
                    string _beginString = null;
                    string _endString = null;
                    string _fullString = null;

                    if (this.Company != null && this.Workplace != null)
                    {
                        #region PIC
                        if(this.PIC != null)
                        {
                            List<string> _stringSLS = new List<string>();

                            XPCollection<SalesmanLocationSetup> _locSalesmanLocationSetups = new XPCollection<SalesmanLocationSetup>
                                                                                      (Session, new GroupOperator(GroupOperatorType.And,
                                                                                       new BinaryOperator("Company", this.Company),
                                                                                       new BinaryOperator("Workplace", this.Workplace),
                                                                                       new BinaryOperator("Salesman", this.PIC),
                                                                                       new BinaryOperator("Active", true)));

                            if (_locSalesmanLocationSetups != null && _locSalesmanLocationSetups.Count() > 0)
                            {
                                foreach (SalesmanLocationSetup _locSalesmanLocationSetup in _locSalesmanLocationSetups)
                                {
                                    if (_locSalesmanLocationSetup.Location != null)
                                    {
                                        if (_locSalesmanLocationSetup.Location.Code != null)
                                        {
                                            _stringSLS.Add(_locSalesmanLocationSetup.Location.Code);
                                        }
                                    }
                                }
                            }

                            IEnumerable<string> _stringArraySLSDistinct = _stringSLS.Distinct();
                            string[] _stringArraySLSList = _stringArraySLSDistinct.ToArray();
                            if (_stringArraySLSList.Length == 1)
                            {
                                for (int i = 0; i < _stringArraySLSList.Length; i++)
                                {
                                    Location _locLoc = Session.FindObject<Location>(new BinaryOperator("Code", _stringArraySLSList[i]));
                                    if (_locLoc != null)
                                    {
                                        if (i == 0)
                                        {
                                            _beginString = "[Code]=='" + _locLoc.Code + "'";
                                        }
                                    }
                                }
                            }
                            else if (_stringArraySLSList.Length > 1)
                            {
                                for (int i = 0; i < _stringArraySLSList.Length; i++)
                                {
                                    Location _locLoc = Session.FindObject<Location>(new BinaryOperator("Code", _stringArraySLSList[i]));
                                    if (_locLoc != null)
                                    {
                                        if (i == 0)
                                        {
                                            _beginString = "[Code]=='" + _locLoc.Code + "'";
                                        }
                                        else
                                        {
                                            _endString = _endString + " OR [Code]=='" + _locLoc.Code + "'";
                                        }
                                    }
                                }
                            }
                            _fullString = _beginString + _endString;

                            if (_fullString != null)
                            {
                                _availableLocation = new XPCollection<Location>(Session, CriteriaOperator.Parse(_fullString));
                            }
                        }
                        #endregion PIC
                        #region NonPIC
                        else
                        {
                            _availableLocation = new XPCollection<Location>(Session, new GroupOperator(GroupOperatorType.And,
                                                                                new BinaryOperator("Company", this.Company),
                                                                                new BinaryOperator("Workplace", this.Workplace),
                                                                                new BinaryOperator("OpenVehicle", false),
                                                                                new BinaryOperator("Active", true)));
                        }
                        #endregion NonPIC
                    }
                    
                }
                
                return _availableLocation;

            }
        }

        [ImmediatePostData()]
        [DataSourceProperty("AvailableLocation")]
        [Appearance("SalesInvoiceLocationClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Location Location
        {
            get { return _location; }
            set
            {
                SetPropertyValue("Location", ref _location, value);
                if (!IsLoading)
                {
                    if (this._location != null)
                    {
                        SetBegInv(_location);
                    }
                    else
                    {
                        BegInv = null;
                    }
                }
            }

        }

        [Browsable(false)]
        [ImmediatePostData()]
        [Appearance("SalesInvoiceBegInvClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public BeginningInventory BegInv
        {
            get { return _begInv; }
            set { SetPropertyValue("BegInv", ref _begInv, value); }
        }

        #endregion Organization

        #region SalesToCustomer

        [ModelDefault("DisplayFormat", "{0:dd/MM/yyyy}")]
        [ModelDefault("EditMask", "dd/MM/yyyy")]
        [Appearance("SalesOrderETDClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public DateTime ETD
        {
            get { return _etd; }
            set { SetPropertyValue("ETD", ref _etd, value); }
        }

        [ModelDefault("DisplayFormat", "{0:dd/MM/yyyy}")]
        [ModelDefault("EditMask", "dd/MM/yyyy")]
        [Appearance("SalesOrderETAClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public DateTime ETA
        {
            get { return _eta; }
            set { SetPropertyValue("ETA", ref _eta, value); }
        }

        [Browsable(false)]
        public XPCollection<BusinessPartner> AvailableSalesToCustomer
        {
            get
            {
                if(!IsLoading)
                {
                    string _beginString = null;
                    string _endString = null;
                    string _fullString = null;

                    if (this.Company != null)
                    {
                        if(this.Workplace != null)
                        {
                            #region PIC
                            if (this.PIC != null)
                            {
                                List<string> _stringSCS = new List<string>();

                                XPCollection<SalesmanCustomerSetup> _locSalesmanCustomerSetups = new XPCollection<SalesmanCustomerSetup>
                                                                                          (Session, new GroupOperator(GroupOperatorType.And,
                                                                                           new BinaryOperator("Company", this.Company),
                                                                                           new BinaryOperator("Workplace", this.Workplace),
                                                                                           new BinaryOperator("Salesman", this.PIC),
                                                                                           new BinaryOperator("Active", true)));

                                if (_locSalesmanCustomerSetups != null && _locSalesmanCustomerSetups.Count() > 0)
                                {
                                    foreach (SalesmanCustomerSetup _locSalesmanCustomerSetup in _locSalesmanCustomerSetups)
                                    {
                                        if (_locSalesmanCustomerSetup.Customer != null)
                                        {
                                            if (_locSalesmanCustomerSetup.Customer.Code != null)
                                            {
                                                _stringSCS.Add(_locSalesmanCustomerSetup.Customer.Code);
                                            }
                                        }
                                    }

                                    IEnumerable<string> _stringArraySCSDistinct = _stringSCS.Distinct();
                                    string[] _stringArraySCSList = _stringArraySCSDistinct.ToArray();
                                    if (_stringArraySCSList.Length == 1)
                                    {
                                        for (int i = 0; i < _stringArraySCSList.Length; i++)
                                        {
                                            BusinessPartner _locBP = Session.FindObject<BusinessPartner>(new BinaryOperator("Code", _stringArraySCSList[i]));
                                            if (_locBP != null)
                                            {
                                                if (i == 0)
                                                {
                                                    _beginString = "[Code]=='" + _locBP.Code + "'";
                                                }
                                            }
                                        }
                                    }
                                    else if (_stringArraySCSList.Length > 1)
                                    {
                                        for (int i = 0; i < _stringArraySCSList.Length; i++)
                                        {
                                            BusinessPartner _locBP = Session.FindObject<BusinessPartner>(new BinaryOperator("Code", _stringArraySCSList[i]));
                                            if (_locBP != null)
                                            {
                                                if (i == 0)
                                                {
                                                    _beginString = "[Code]=='" + _locBP.Code + "'";
                                                }
                                                else
                                                {
                                                    _endString = _endString + " OR [Code]=='" + _locBP.Code + "'";
                                                }
                                            }
                                        }
                                    }
                                    _fullString = _beginString + _endString;
                                }
                                if (_fullString != null)
                                {
                                    _availableSalesToCustomer = new XPCollection<BusinessPartner>(Session, CriteriaOperator.Parse(_fullString));
                                }
                            }
                            #endregion PIC
                            #region NonPIC
                            else
                            {
                                _availableSalesToCustomer = new XPCollection<BusinessPartner>(Session, new GroupOperator(GroupOperatorType.And,
                                                                                    new BinaryOperator("Company", this.Company),
                                                                                    new BinaryOperator("Workplace", this.Workplace),
                                                                                    new BinaryOperator("Active", true)));
                            }
                            #endregion NonPIC 
                        }
                        else
                        {
                            _availableSalesToCustomer = new XPCollection<BusinessPartner>(Session, new GroupOperator(GroupOperatorType.And,
                                                                                    new BinaryOperator("Company", this.Company),
                                                                                    new BinaryOperator("Active", true)));
                        }
                        
                    }
                }
                
                return _availableSalesToCustomer;

            }
        }

        [ImmediatePostData]
        [DataSourceProperty("AvailableSalesToCustomer")]
        [Appearance("SalesInvoiceSalesToCostumerClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public BusinessPartner SalesToCustomer
        {
            get { return _salesToCustomer; }
            set
            {
                SetPropertyValue("SalesToCustomer", ref _salesToCustomer, value);
                if (!IsLoading)
                {
                    if (this._salesToCustomer != null)
                    {
                        if (this._salesToCustomer.Contact != null)
                        {
                            this.SalesToContact = this._salesToCustomer.Contact;
                        }
                        if (this._salesToCustomer.Country != null)
                        {
                            this.SalesToCountry = this._salesToCustomer.Country;
                        }
                        if (this._salesToCustomer.City != null)
                        {
                            this.SalesToCity = this._salesToCustomer.City;
                        }
                        if (this._salesToCustomer.Address != null)
                        {
                            this.SalesToAddress = this._salesToCustomer.Address;
                        }
                        if (this._salesToCustomer.TaxNo != null)
                        {
                            this.TaxNo = this._salesToCustomer.TaxNo;
                        }
                        if (this._salesToCustomer.TOP != null)
                        {
                            this.TOP = this._salesToCustomer.TOP;
                        }
                        if (_salesToCustomer.PriceGroup != null)
                        {
                            this.PriceGroup = _salesToCustomer.PriceGroup;
                        }
                        this.BillToCustomer = this._salesToCustomer;

                        SetCreditLimitList(this._salesToCustomer);
                    }
                    else
                    {
                        this.SalesToContact = null;
                        this.SalesToCountry = null;
                        this.SalesToCity = null;
                        this.SalesToAddress = null;
                        this.TaxNo = null;
                        this.TOP = null;
                        this.PriceGroup = null;
                        this.BillToCustomer = null;
                        this.CreditLimitList = null;
                        this.OverCredit = false;
                        this.Message = null;
                        this.CreditLimitStatus = CreditLimitStatus.None;
                    }
                }
            }
        }

        [Appearance("SalesInvoiceSalesToContactClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public string SalesToContact
        {
            get { return _salesToContact; }
            set { SetPropertyValue("SalesToContact", ref _salesToContact, value); }
        }

        [DataSourceCriteria("Active = true")]
        [Appearance("SalesInvoiceSalesToCountryClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Country SalesToCountry
        {
            get { return _salesToCountry; }
            set { SetPropertyValue("SalesToCountry", ref _salesToCountry, value); }
        }

        [DataSourceCriteria("Active = true")]
        [Appearance("SalesInvoiceSalesToCityClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public City SalesToCity
        {
            get { return _salesToCity; }
            set { SetPropertyValue("SalesToCity", ref _salesToCity, value); }
        }

        [Appearance("SalesInvoiceSalesToAddressClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [Size(512)]
        public string SalesToAddress
        {
            get { return _salesToAddress; }
            set { SetPropertyValue("SalesToAddress", ref _salesToAddress, value); }
        }

        #endregion SalesToCustomer

        #region BillToCustomer

        [ImmediatePostData()]
        [DataSourceCriteria("Active = true")]
        [Appearance("SalesInvoiceBillToCostumerClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public BusinessPartner BillToCustomer
        {
            get { return _billToCustomer; }
            set
            {
                SetPropertyValue("BillToCostumer", ref _billToCustomer, value);
                if (!IsLoading)
                {
                    if (this._billToCustomer != null)
                    {
                        if (this._billToCustomer.Contact != null)
                        {
                            this.BillToContact = this._billToCustomer.Contact;
                        }
                        if (this._billToCustomer.Country != null)
                        {
                            this.BillToCountry = this._billToCustomer.Country;
                        }
                        if (this._billToCustomer.City != null)
                        {
                            this.BillToCity = this._billToCustomer.City;
                        }
                        if (this._billToCustomer.Address != null)
                        {
                            this.BillToAddress = this._billToCustomer.Address;
                        }

                    }
                }
            }
        }

        [Appearance("SalesInvoiceBillToContactClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public string BillToContact
        {
            get { return _billToContact; }
            set { SetPropertyValue("BillToContact", ref _billToContact, value); }
        }

        [DataSourceCriteria("Active = true")]
        [Appearance("SalesInvoiceBillCountryClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Country BillToCountry
        {
            get { return _billToCountry; }
            set { SetPropertyValue("BillToCountry", ref _billToCountry, value); }
        }

        [DataSourceCriteria("Active = true")]
        [Appearance("SalesInvoiceBillToCityClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public City BillToCity
        {
            get { return _billToCity; }
            set { SetPropertyValue("BillToCity", ref _billToCity, value); }
        }

        [Appearance("SalesInvoiceBillToAddressClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [Size(512)]
        public string BillToAddress
        {
            get { return _billToAddress; }
            set { SetPropertyValue("BillToAddress", ref _billToAddress, value); }
        }

        #endregion BillToCostumer

        #region Amount

        [Appearance("SalesInvoiceCurrencyClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Currency Currency
        {
            get { return _currency; }
            set { SetPropertyValue("Currency", ref _currency, value); }
        }

        [ImmediatePostData()]
        [Appearance("SalesInvoicePriceGroupClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public PriceGroup PriceGroup
        {
            get { return _priceGroup; }
            set { SetPropertyValue("PriceGroup", ref _priceGroup, value); }
        }

        [Appearance("SalesInvoicePaymentMethodClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public PaymentMethod PaymentMethod
        {
            get { return _paymentMethod; }
            set { SetPropertyValue("PaymentMethod", ref _paymentMethod, value); }
        }

        [Appearance("SalesInvoicePaymentMethodTypeClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public PaymentMethodType PaymentMethodType
        {
            get { return _paymentMethodType; }
            set { SetPropertyValue("PaymentMethodType", ref _paymentMethodType, value); }
        }

        [Appearance("SalesInvoicePaymentTypeClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public PaymentType PaymentType
        {
            get { return _paymentType; }
            set { SetPropertyValue("PaymentType", ref _paymentType, value); }
        }

        [ImmediatePostData()]
        [Appearance("SalesInvoiceTermOfPaymentClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public TermOfPayment TOP
        {
            get { return _top; }
            set {
                SetPropertyValue("TOP", ref _top, value);
                if(!IsLoading)
                {
                    if(this.TOP != null)
                    {
                        this.DueDate = this.DocDate.AddDays(this.TOP.Due);
                    }
                }
            }
        }

        [ModelDefault("DisplayFormat", "{0:dd/MM/yyyy}")]
        [ModelDefault("EditMask", "dd/MM/yyyy")]
        [Appearance("SalesInvoiceDueDateClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public DateTime DueDate
        {
            get { return _dueDate; }
            set { SetPropertyValue("DueDate", ref _dueDate, value); }
        }

        [Appearance("SalesInvoiceTaxNoClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public string TaxNo
        {
            get { return _taxNo; }
            set { SetPropertyValue("TaxNo", ref _taxNo, value); }
        }

        [Appearance("SalesInvoiceTotAmountClose", Enabled = false)]
        public double TotAmount
        {
            get { return _totAmount; }
            set { SetPropertyValue("TotAmount", ref _totAmount, value); }
        }

        [Appearance("SalesInvoiceTotUnitAmountClose", Enabled = false)]
        public double TotUnitAmount
        {
            get { return _totUnitAmount; }
            set { SetPropertyValue("TotUnitAmount", ref _totUnitAmount, value); }
        }

        [Appearance("SalesInvoiceTotTaxAmountClose", Enabled = false)]
        public double TotTaxAmount
        {
            get { return _totTaxAmount; }
            set { SetPropertyValue("TotTaxAmount", ref _totTaxAmount, value); }
        }

        [Appearance("SalesInvoiceTotDiscAmountClose", Enabled = false)]
        public double TotDiscAmount
        {
            get { return _totDiscAmount; }
            set { SetPropertyValue("TotDiscAmount", ref _totDiscAmount, value); }
        }

        [Appearance("SalesInvoiceAmountClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public double Amount
        {
            get { return _amount; }
            set { SetPropertyValue("Amount", ref _amount, value); }
        }

        [Appearance("SalesInvoiceTotFIAmountClose", Enabled = false)]
        public double TotFIAmount
        {
            get { return _totFIAmount; }
            set { SetPropertyValue("TotFIAmount", ref _totFIAmount, value); }
        }

        [Browsable(false)]
        [Appearance("SalesInvoiceAmountDiscClose", Enabled = false)]
        public double AmountDisc
        {
            get { return _amountDisc; }
            set { SetPropertyValue("AmountDisc", ref _amountDisc, value); }
        }

        [Appearance("SalesInvoiceDiscountRuleClose", Enabled = false)]
        public DiscountRule DiscountRule
        {
            get { return _discountRule; }
            set { SetPropertyValue("DiscountRule", ref _discountRule, value); }
        }

        [Browsable(false)]
        public AccountingPeriodicLine AccountingPeriodicLine
        {
            get { return _accountingPeriodicLine; }
            set { SetPropertyValue("AccountingPeriodicLine", ref _accountingPeriodicLine, value); }
        }

        #region CreditLimit

        [Browsable(false)]
        [Appearance("SalesInvoiceCreditLimitListClose", Enabled = false)]
        public CreditLimitList CreditLimitList
        {
            get { return _creditLimitList; }
            set { SetPropertyValue("CreditLimitList", ref _creditLimitList, value); }
        }

        [Browsable(false)]
        [Appearance("SalesInvoiceOverCreditClose", Enabled = false)]
        public bool OverCredit
        {
            get { return _overCredit; }
            set { SetPropertyValue("OverCredit", ref _overCredit, value); }
        }

        [Appearance("SalesInvoiceMessageHide", Criteria = "OverCredit = false", Visibility = ViewItemVisibility.Hide, Context = "DetailView")]
        [Appearance("SalesInvoiceMessageShow", Criteria = "OverCredit = true", Visibility = ViewItemVisibility.Show, Context = "DetailView")]
        [Appearance("SalesInvoiceMessageRedColor", Criteria = "OverCredit = true", BackColor = "#fd79a8")]
        [Appearance("SalesInvoiceMessageClose", Enabled = false)]
        public string Message
        {
            get { return _message; }
            set { SetPropertyValue("Message", ref _message, value); }
        }

        [Browsable(false)]
        [Appearance("SalesInvoiceCreditLimitStatusClose", Enabled = false)]
        public CreditLimitStatus CreditLimitStatus
        {
            get { return _creditLimitStatus; }
            set { SetPropertyValue("CreditLimitStatus", ref _creditLimitStatus, value); }
        }

        [Browsable(false)]
        [ModelDefault("DisplayFormat", "{0:dd/MM/yyyy}")]
        [ModelDefault("EditMask", "dd/MM/yyyy")]
        [Appearance("SalesInvoiceCreditLimitStatusDateClose", Enabled = false)]
        public DateTime CreditLimitStatusDate
        {
            get { return _creditLimitStatusDate; }
            set { SetPropertyValue("CreditLimitStatusDate", ref _creditLimitStatusDate, value); }
        }

        #endregion CreditLimit

        #endregion Amount

        #region Bank

        [Browsable(false)]
        public XPCollection<BankAccount> AvailableCompanyBankAccount
        {
            get
            {
                if(!IsLoading)
                {
                    if (this.Company != null )
                    {
                        if(this.Workplace != null)
                        {
                            _availableCompanyBankAccount = new XPCollection<BankAccount>(Session, new GroupOperator
                                                    (GroupOperatorType.And,
                                                    new BinaryOperator("Company", this.Company),
                                                    new BinaryOperator("Workplace", this.Workplace),
                                                    new BinaryOperator("Active", true)));
                        }else
                        {
                            _availableCompanyBankAccount = new XPCollection<BankAccount>(Session, new GroupOperator
                                                    (GroupOperatorType.And,
                                                    new BinaryOperator("Company", this.Company),
                                                    new BinaryOperator("Active", true)));
                        }
                    }
                    else
                    {
                        _availableCompanyBankAccount = new XPCollection<BankAccount>(Session,
                            new GroupOperator(GroupOperatorType.And,
                            new BinaryOperator("Active", true)));
                    }
                }
                
                return _availableCompanyBankAccount;

            }
        }

        [ImmediatePostData()]
        [DataSourceProperty("AvailableCompanyBankAccount", DataSourcePropertyIsNullMode.SelectAll)]
        [Appearance("SalesInvoiceCompanyBankAccountClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public BankAccount CompanyBankAccount
        {
            get { return _companyBankAccount; }
            set
            {
                SetPropertyValue("CompanyBankAccount", ref _companyBankAccount, value);
                if (!IsLoading)
                {
                    if (this._companyBankAccount != null)
                    {
                        this.CompanyAccountNo = this._companyBankAccount.AccountNo;
                        this.CompanyAccountName = this._companyBankAccount.AccountName;
                    }
                }
            }
        }

        [Appearance("SalesInvoiceCompanyAccountNoClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public string CompanyAccountNo
        {
            get { return _companyAccountNo; }
            set { SetPropertyValue("CompanyAccountNo", ref _companyAccountNo, value); }
        }

        [Appearance("SalesInvoiceCompanyAccountNameClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public string CompanyAccountName
        {
            get { return _companyAccountName; }
            set { SetPropertyValue("CompanyAccountName", ref _companyAccountName, value); }
        }

        [Browsable(false)]
        public XPCollection<BankAccount> AvailableBankAccounts
        {
            get
            {
                if(!IsLoading)
                {
                    if (this.BillToCustomer == null)
                    {
                        _availableBankAccounts = new XPCollection<BankAccount>(Session);
                    }
                    else
                    {
                        _availableBankAccounts = new XPCollection<BankAccount>(Session,
                                                 new GroupOperator(GroupOperatorType.And,
                                                 new BinaryOperator("BusinessPartner", this.BillToCustomer)));
                    }
                }
                
                return _availableBankAccounts;
            }
        }

        [ImmediatePostData()]
        [DataSourceProperty("AvailableBankAccounts", DataSourcePropertyIsNullMode.SelectAll)]
        [Appearance("SalesInvoiceBankAccountClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public BankAccount BankAccount
        {
            get { return _bankAccount; }
            set
            {
                SetPropertyValue("BankAccount", ref _bankAccount, value);
                if (!IsLoading)
                {
                    if (this._bankAccount != null)
                    {
                        this.AccountNo = this._bankAccount.AccountNo;
                        this.AccountName = this._bankAccount.AccountName;
                    }
                }
            }
        }

        [Appearance("SalesInvoiceAccountNoClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public string AccountNo
        {
            get { return _accountNo; }
            set { SetPropertyValue("AccountNo", ref _accountNo, value); }
        }

        [Appearance("SalesInvoiceAccountNameClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public string AccountName
        {
            get { return _accountName; }
            set { SetPropertyValue("AccountName", ref _accountName, value); }
        }

        #endregion Bank

        [Appearance("SalesInvoiceDescriptionClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [Size(512)]
        public string Description
        {
            get { return _description; }
            set { SetPropertyValue("Description", ref _description, value); }
        }

        public string DJP_No
        {
            get { return _djp_No; }
            set { SetPropertyValue("DJP_No", ref _djp_No, value); }
        }

        [ImmediatePostData()]
        [ModelDefault("DisplayFormat", "{0:dd/MM/yyyy}")]
        [ModelDefault("EditMask", "dd/MM/yyyy")]
        [Appearance("SalesInvoiceDocDateClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public DateTime DocDate
        {
            get { return _docDate; }
            set {
                SetPropertyValue("DocDate", ref _docDate, value);
                if(!IsLoading)
                {
                    SetAccountingPeriodic();
                }
            }
        }

        [Browsable(false)]
        public int JournalMonth
        {
            get { return _journalMonth; }
            set { SetPropertyValue("JournalMonth", ref _journalMonth, value); }
        }

        [Browsable(false)]
        public int JournalYear
        {
            get { return _journalYear; }
            set { SetPropertyValue("JournalYear", ref _journalYear, value); }
        }

        [Browsable(false)]
        [Appearance("SalesInvoicePostedCountClose", Enabled = false)]
        public int PostedCount
        {
            get { return _postedCount; }
            set { SetPropertyValue("PostedCount", ref _postedCount, value); }
        }

        [Appearance("SalesInvoiceStatusClose", Enabled = false)]
        public Status Status
        {
            get { return _status; }
            set { SetPropertyValue("Status", ref _status, value); }
        }

        [ModelDefault("DisplayFormat", "{0:dd/MM/yyyy}")]
        [ModelDefault("EditMask", "dd/MM/yyyy")]
        [Appearance("SalesInvoiceStatusDateClose", Enabled = false)]
        public DateTime StatusDate
        {
            get { return _statusDate; }
            set { SetPropertyValue("StatusDate", ref _statusDate, value); }
        }

        [Association("SalesInvoice-SalesInvoiceLines")]
        public XPCollection<SalesInvoiceLine> SalesInvoiceLines
        {
            get { return GetCollection<SalesInvoiceLine>("SalesInvoiceLines"); }
        }

        [Association("SalesInvoice-ApprovalLines")]
        public XPCollection<ApprovalLine> ApprovalLines
        {
            get { return GetCollection<ApprovalLine>("ApprovalLines"); }
        }

        [Association("SalesInvoice-SalesInvoiceFreeItems")]
        public XPCollection<SalesInvoiceFreeItem> SalesInvoiceFreeItems
        {
            get { return GetCollection<SalesInvoiceFreeItem>("SalesInvoiceFreeItems"); }
        }

        [Association("SalesInvoice-SalesOrderCollections")]
        public XPCollection<SalesOrderCollection> SalesOrderCollections
        {
            get { return GetCollection<SalesOrderCollection>("SalesOrderCollections"); }
        }

        //AuditTrail
        [CollectionOperationSet(AllowAdd = false, AllowRemove = true)]
        public XPCollection<AuditDataItemPersistent> ChangeHistory
        {
            get
            {
                if (changeHistory == null)
                {
                    changeHistory = AuditedObjectWeakReference.GetAuditTrail(Session, this);
                }
                return changeHistory;
            }
        }

        #endregion Field

        //=================================== Code Only ===================================

        private void SetCreditLimitList(BusinessPartner _locBusinessPartner)
        {
            try
            {
                DateTime now = DateTime.Now;
                if (this.Company != null && this.Workplace != null)
                {
                    CreditLimitList _locCreditLimitList = Session.FindObject<CreditLimitList>
                                                        (new GroupOperator(GroupOperatorType.And,
                                                        new BinaryOperator("Company", this.Company),
                                                        new BinaryOperator("Workplace", this.Workplace),
                                                        new BinaryOperator("BusinessPartner", _locBusinessPartner),
                                                        new BinaryOperator("Active", true)));
                    if (_locCreditLimitList != null)
                    {
                        this.CreditLimitList = _locCreditLimitList;
                        if (_locCreditLimitList.OverCredit == true)
                        {
                            this.OverCredit = true;
                            this.Message = "Over Credit";
                            this.CreditLimitStatus = CreditLimitStatus.Hold;
                            this.CreditLimitStatusDate = now;
                        }else
                        {
                            this.OverCredit = false;
                            this.Message = null;
                            this.CreditLimitStatus = CreditLimitStatus.None;
                        }
                    }
                    else
                    {
                        this.CreditLimitList = null;
                        this.OverCredit = false;
                        this.Message = null;
                        this.CreditLimitStatus = CreditLimitStatus.None;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Business Object = SalesInvoice ", ex.ToString());
            }

        }

        private void SetLocation(Employee _locPIC)
        {
            try
            {
                DateTime now = DateTime.Now;
                if (this.Company != null && this.Workplace != null)
                {
                    SalesmanLocationSetup _locSalesmanLocationSetup = Session.FindObject<SalesmanLocationSetup>
                                                        (new GroupOperator(GroupOperatorType.And,
                                                        new BinaryOperator("Company", this.Company),
                                                        new BinaryOperator("Workplace", this.Workplace),
                                                        new BinaryOperator("Salesman", _locPIC),
                                                        new BinaryOperator("Active", true),
                                                        new BinaryOperator("Default", true)));
                    if (_locSalesmanLocationSetup != null)
                    {
                        if (_locSalesmanLocationSetup.Location != null)
                        {

                            this.Location = _locSalesmanLocationSetup.Location;
                        }
                        else
                        {
                            this.Location = null;
                        }
                    }else
                    {
                        this.Location = null;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Business Object = SalesInvoice ", ex.ToString());
            }

        }

        private void SetBegInv(Location _locLocation)
        {
            try
            {
                DateTime now = DateTime.Now;
                if (this.Company != null && this.Workplace != null)
                {
                    BeginningInventory _locBegInv = Session.FindObject<BeginningInventory>
                                                        (new GroupOperator(GroupOperatorType.And,
                                                        new BinaryOperator("Company", this.Company),
                                                        new BinaryOperator("Workplace", this.Workplace),
                                                        new BinaryOperator("Location", _locLocation),
                                                        new BinaryOperator("LocationType", LocationType.Main),
                                                        new BinaryOperator("StockType", StockType.Good),
                                                        new BinaryOperator("Active", true)));
                    if (_locBegInv != null)
                    {
                        this.BegInv = _locBegInv;
                    }else
                    {
                        this.BegInv = null;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Business Object = SalesInvoice ", ex.ToString());
            }
        }

        private void SetAccountingPeriodic()
        {
            try
            {
                DateTime now = DateTime.Now;
                if (this.Company != null && this.Workplace != null)
                {
                    AccountingPeriodic _locAcctPeriodic = Session.FindObject<AccountingPeriodic>
                                                        (new GroupOperator(GroupOperatorType.And,
                                                        new BinaryOperator("Company", this.Company),
                                                        new BinaryOperator("Workplace", this.Workplace),
                                                        new BinaryOperator("Active", true)));
                    if (_locAcctPeriodic != null)
                    {
                        this.JournalMonth = _locAcctPeriodic.Month;
                        this.JournalYear = _locAcctPeriodic.Year;
                    }
                    else
                    {
                        this.JournalMonth = 0;
                        this.JournalYear = 0;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Business Object = SalesInvoice ", ex.ToString());
            }
        }

    }
}