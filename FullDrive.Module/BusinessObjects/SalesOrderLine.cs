﻿using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;

using DevExpress.ExpressApp.ConditionalAppearance;
using DevExpress.ExpressApp.SystemModule;
using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.Editors;

namespace FullDrive.Module.BusinessObjects
{
    [DefaultClassOptions]
    [DefaultProperty("Code")]
    [NavigationItem("Sales")]
    [RuleCombinationOfPropertiesIsUnique("SalesOrderLineRuleUnique", DefaultContexts.Save, "Code")]
    //[ImageName("BO_Contact")]
    //[DefaultProperty("DisplayMemberNameForLookupEditorsOfThisType")]
    //[DefaultListViewOptions(MasterDetailMode.ListViewOnly, false, NewItemRowPosition.None)]
    //[Persistent("DatabaseTableName")]
    // Specify more UI options using a declarative approach (https://documentation.devexpress.com/#eXpressAppFramework/CustomDocument112701).
    public class SalesOrderLine : BaseObject
    { // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).

        #region InitialSystem
        private bool _activationPosting;
        private bool _freeItemChecked;
        private bool _discountChecked;
        private bool _selectEnabled;
        private bool _redColor;
        private int _no;
        private string _message;
        private bool _select;
        private string _code;
        #endregion InitialSystem
        #region InitialOrganization
        private Company _company;
        private Workplace _workplace;
        private Division _division;
        private Department _department;
        private Section _section;
        private Employee _employee;
        private Division _div;
        private Department _dept;
        private Section _sect;
        private XPCollection<Employee> _availablePIC;
        private Employee _pic;
        private XPCollection<Location> _availableLocation;
        private Location _location;
        private BeginningInventory _begInv;
        #endregion InitialOrganization 
        private OrderType _salesType;
        private XPCollection<Item> _availableItem;
        private Item _item;
        private BeginningInventoryLine _begInvLine;
        private string _description;
        #region InisialisasiDefaultQty
        private double _dQty;
        private UnitOfMeasure _dUom;
        private double _qty;
        private XPCollection<UnitOfMeasure> _availableUnitOfMeasure;
        private UnitOfMeasure _uom;
        private double _tQty;
        private StockType _stockType;
        #endregion InisialisasiDefaultQty
        #region InisialisasiAmount
        private Currency _currency;
        private PriceGroup _priceGroup;
        private PriceLine _priceLine;
        private double _uAmount;
        private double _tUAmount;
        private DiscountRule _discountRule;
        private AccountingPeriodicLine _accountingPeriodicLine;
        private double _discAmount;
        private Tax _tax;
        private double _txValue;
        private double _txAmount;
        private double _tAmount;
        #endregion InisialisasiAmount
        private DateTime _etd;
        private DateTime _eta;
        private Status _status;
        private DateTime _statusDate;
        private int _postedCount;
        private SalesOrder _salesOrder;
        private string _localUserAccess;
        private GlobalFunction _globFunc;
        //AuditTrail
        private XPCollection<AuditDataItemPersistent> changeHistory;

        public SalesOrderLine(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).
            if (!IsLoading)
            {
                _globFunc = new GlobalFunction();
                DateTime now = DateTime.Now;
                #region UserAccess
                _localUserAccess = SecuritySystem.CurrentUserName;
                UserAccess _locUserAccess = Session.FindObject<UserAccess>(new BinaryOperator("UserName", _localUserAccess));
                if (_locUserAccess != null)
                {
                    if (_locUserAccess.Employee != null)
                    {
                        if (_locUserAccess.Employee.Company != null && _locUserAccess.Employee.Workplace != null)
                        {
                            this.Code = _globFunc.GetNumberingUnlockOptimisticRecordBasedOrganization(this.Session.DataLayer, ObjectList.SalesOrderLine, _locUserAccess.Employee.Company, _locUserAccess.Employee.Workplace);
                        }
                        else
                        {
                            this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.SalesOrderLine);
                        }
                    }
                }
                #endregion UserAccess
                this.SalesType = CustomProcess.OrderType.Item;
                this.Status = CustomProcess.Status.Open;
                this.StatusDate = now;
                this.Select = true;
                this.DiscountChecked = false;
                this.StockType = StockType.Good;
            }
        }

        protected override void OnSaving()
        {
            base.OnSaving();
            if (!IsLoading && IsSaving)
            {
                UpdateNo();
            }
        }
        protected override void OnDeleting()
        {
            base.OnDeleting();
            if (!IsLoading)
            {
                RecoveryDeleteNo();
                RollBackQtyOnHandAndBalHold();
            }
        }

        #region Field

        #region System

        [Browsable(false)]
        [ImmediatePostData()]
        public bool ActivationPosting
        {
            get { return _activationPosting; }
            set { SetPropertyValue("ActivationPosting", ref _activationPosting, value); }
        }

        [Browsable(false)]
        public bool FreeItemChecked
        {
            get { return _freeItemChecked; }
            set { SetPropertyValue("FreeItemChecked", ref _freeItemChecked, value); }
        }

        [Browsable(false)]
        public bool DiscountChecked
        {
            get { return _discountChecked; }
            set { SetPropertyValue("DiscountChecked", ref _discountChecked, value); }
        }

        [Browsable(false)]
        [ImmediatePostData()]
        public bool SelectEnabled
        {
            get { return _selectEnabled; }
            set { SetPropertyValue("SelectEnabled", ref _selectEnabled, value); }
        }

        [Browsable(false)]
        [ImmediatePostData()]
        public bool RecColor
        {
            get { return _redColor; }
            set { SetPropertyValue("RecColor", ref _redColor, value); }
        }

        [Appearance("SalesOrderLineNoRedColor", Criteria = "RecColor = true", BackColor = "#fd79a8")]
        [Appearance("SalesOrderLineNoEnabled", Enabled = false)]
        public int No
        {
            get { return _no; }
            set { SetPropertyValue("No", ref _no, value); }
        }

        [VisibleInListView(false)]
        [Appearance("SalesOrderLineMessageHide", Criteria = "RecColor = false", Visibility = ViewItemVisibility.Hide, Context = "DetailView")]
        [Appearance("SalesOrderLineMessageShow", Criteria = "RecColor = true", Visibility = ViewItemVisibility.Show, Context = "DetailView")]
        [Appearance("SalesOrderLineMessageRedColor", Criteria = "RecColor = true", BackColor = "#fd79a8")]
        [Appearance("SalesOrderLineMessageEnabled", Enabled = false)]
        public string Message
        {
            get { return _message; }
            set { SetPropertyValue("Message", ref _message, value); }
        }

        [Appearance("SalesOrderLineSelectClose", Criteria = "SelectEnabled = true", Enabled = false)]
        public bool Select
        {
            get { return _select; }
            set { SetPropertyValue("Select", ref _select, value); }
        }

        [VisibleInListView(false)]
        [RuleRequiredField(DefaultContexts.Save)]
        [Appearance("SalesOrderLineCodeEnabled", Enabled = false)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue("Code", ref _code, value); }
        }

        #endregion System

        #region Organization
        
        [ImmediatePostData()]
        [VisibleInListView(false)]
        [RuleRequiredField(DefaultContexts.Save)]
        [DataSourceCriteria("Active = true")]
        [Appearance("SalesOrderLineCompanyClose", Enabled = false)]
        public Company Company
        {
            get { return _company; }
            set { SetPropertyValue("Company", ref _company, value); }
        }

        [ImmediatePostData()]
        [VisibleInListView(false)]
        [RuleRequiredField(DefaultContexts.Save)]
        [DataSourceCriteria("Active = true")]
        [Appearance("SalesOrderLineWorkplaceClose", Enabled = false)]
        public Workplace Workplace
        {
            get { return _workplace; }
            set { SetPropertyValue("Workplace", ref _workplace, value); }
        }

        [Browsable(false)]
        public Division Division
        {
            get { return _division; }
            set { SetPropertyValue("Division", ref _division, value); }
        }

        [Browsable(false)]
        public Department Department
        {
            get { return _department; }
            set { SetPropertyValue("Department", ref _department, value); }
        }

        [Browsable(false)]
        public Section Section
        {
            get { return _section; }
            set { SetPropertyValue("Section", ref _section, value); }
        }

        [Browsable(false)]
        public Employee Employee
        {
            get { return _employee; }
            set { SetPropertyValue("Employee", ref _employee, value); }
        }

        [ImmediatePostData()]
        [VisibleInListView(false)]
        [DataSourceCriteria("Company = '@This.Company' And Active = true")]
        [Appearance("SalesOrderLineDivClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Division Div
        {
            get { return _div; }
            set { SetPropertyValue("Div", ref _div, value); }
        }

        [ImmediatePostData()]
        [VisibleInListView(false)]
        [DataSourceCriteria("Company = '@This.Company' And Division = '@This.Div' And Active = true")]
        [Appearance("SalesOrderLineDeptClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Department Dept
        {
            get { return _dept; }
            set { SetPropertyValue("Dept", ref _dept, value); }
        }

        [ImmediatePostData()]
        [VisibleInListView(false)]
        [DataSourceCriteria("Company = '@This.Company' And Department = '@This.Dept' And Active = true")]
        [Appearance("SalesOrderLineSectClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Section Sect
        {
            get { return _sect; }
            set { SetPropertyValue("Sect", ref _sect, value); }
        }

        [Browsable(false)]
        public XPCollection<Employee> AvailablePIC
        {
            get
            {
                XPCollection<Employee> _locPICs = null;
                if (this.Company != null && this.Workplace != null)
                {
                    #region DivDeptSect
                    if (this.Div != null && this.Dept != null && this.Sect != null)
                    {
                        _locPICs = new XPCollection<Employee>
                                  (Session, new GroupOperator(GroupOperatorType.And,
                                    new BinaryOperator("Company", this.Company),
                                    new BinaryOperator("Workplace", this.Workplace),
                                    new BinaryOperator("Division", this.Div),
                                    new BinaryOperator("Department", this.Dept),
                                    new BinaryOperator("Section", this.Sect),
                                    new BinaryOperator("Active", true),
                                    new GroupOperator(GroupOperatorType.Or,
                                    new BinaryOperator("SalesRole", SalesRole.TakingOrder),
                                    new BinaryOperator("SalesRole", SalesRole.All))));
                    }
                    else if (this.Div != null && this.Dept != null && this.Sect == null)
                    {
                        _locPICs = new XPCollection<Employee>
                                  (Session, new GroupOperator(GroupOperatorType.And,
                                    new BinaryOperator("Company", this.Company),
                                    new BinaryOperator("Workplace", this.Workplace),
                                    new BinaryOperator("Division", this.Div),
                                    new BinaryOperator("Department", this.Dept),
                                    new BinaryOperator("Active", true),
                                    new GroupOperator(GroupOperatorType.Or,
                                    new BinaryOperator("SalesRole", SalesRole.TakingOrder),
                                    new BinaryOperator("SalesRole", SalesRole.All))));

                    }
                    else if (this.Div != null && this.Dept == null && this.Sect == null)
                    {
                        _locPICs = new XPCollection<Employee>
                                  (Session, new GroupOperator(GroupOperatorType.And,
                                    new BinaryOperator("Company", this.Company),
                                    new BinaryOperator("Workplace", this.Workplace),
                                    new BinaryOperator("Division", this.Div),
                                    new BinaryOperator("Active", true),
                                    new GroupOperator(GroupOperatorType.Or,
                                    new BinaryOperator("SalesRole", SalesRole.TakingOrder),
                                    new BinaryOperator("SalesRole", SalesRole.All))));

                    }
                    else if (this.Div == null && this.Dept == null && this.Sect == null)
                    {
                        _locPICs = new XPCollection<Employee>
                                  (Session, new GroupOperator(GroupOperatorType.And,
                                    new BinaryOperator("Company", this.Company),
                                    new BinaryOperator("Workplace", this.Workplace),
                                    new BinaryOperator("Active", true),
                                    new GroupOperator(GroupOperatorType.Or,
                                    new BinaryOperator("SalesRole", SalesRole.TakingOrder),
                                    new BinaryOperator("SalesRole", SalesRole.All))));

                    }
                    #endregion DivDeptSect
                    if (_locPICs != null && _locPICs.Count() > 0)
                    {
                        _availablePIC = _locPICs;
                    }
                }
                else
                {
                    _availablePIC = new XPCollection<Employee>(Session, new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("Active", true),
                                                            new GroupOperator(GroupOperatorType.Or,
                                                            new BinaryOperator("SalesRole", SalesRole.TakingOrder),
                                                            new BinaryOperator("SalesRole", SalesRole.All))));
                }

                return _availablePIC;

            }
        }

        [Browsable(false)]
        [ImmediatePostData()]
        [DataSourceProperty("AvailablePIC")]
        [Appearance("SalesOrderLinePICClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Employee PIC
        {
            get { return _pic; }
            set
            {
                SetPropertyValue("PIC", ref _pic, value);
                if (!IsLoading)
                {
                    if (this._pic != null)
                    {
                        SetLocation(_pic); 
                    }
                    else
                    {
                        this.Location = null;
                    }
                }
            }
        }

        [Browsable(false)]
        public XPCollection<Location> AvailableLocation
        {
            get
            {
                string _beginString = null;
                string _endString = null;
                string _fullString = null;

                if (this.Company != null && this.Workplace != null)
                {
                    if(this.PIC != null)
                    {
                        List<string> _stringSLS = new List<string>();

                        XPCollection<SalesmanLocationSetup> _locSalesmanLocationSetups = new XPCollection<SalesmanLocationSetup>
                                                                                  (Session, new GroupOperator(GroupOperatorType.And,
                                                                                   new BinaryOperator("Company", this.Company),
                                                                                   new BinaryOperator("Workplace", this.Workplace),
                                                                                   new BinaryOperator("Salesman", this.PIC),
                                                                                   new BinaryOperator("Active", true)));

                        if (_locSalesmanLocationSetups != null && _locSalesmanLocationSetups.Count() > 0)
                        {
                            foreach (SalesmanLocationSetup _locSalesmanLocationSetup in _locSalesmanLocationSetups)
                            {
                                if (_locSalesmanLocationSetup.Location != null)
                                {
                                    if (_locSalesmanLocationSetup.Location.Code != null)
                                    {
                                        _stringSLS.Add(_locSalesmanLocationSetup.Location.Code);
                                    }
                                }
                            }
                        }

                        IEnumerable<string> _stringArraySLSDistinct = _stringSLS.Distinct();
                        string[] _stringArraySLSList = _stringArraySLSDistinct.ToArray();
                        if (_stringArraySLSList.Length == 1)
                        {
                            for (int i = 0; i < _stringArraySLSList.Length; i++)
                            {
                                Location _locLoc = Session.FindObject<Location>(new BinaryOperator("Code", _stringArraySLSList[i]));
                                if (_locLoc != null)
                                {
                                    if (i == 0)
                                    {
                                        _beginString = "[Code]=='" + _locLoc.Code + "'";
                                    }
                                }
                            }
                        }
                        else if (_stringArraySLSList.Length > 1)
                        {
                            for (int i = 0; i < _stringArraySLSList.Length; i++)
                            {
                                Location _locLoc = Session.FindObject<Location>(new BinaryOperator("Code", _stringArraySLSList[i]));
                                if (_locLoc != null)
                                {
                                    if (i == 0)
                                    {
                                        _beginString = "[Code]=='" + _locLoc.Code + "'";
                                    }
                                    else
                                    {
                                        _endString = _endString + " OR [Code]=='" + _locLoc.Code + "'";
                                    }
                                }
                            }
                        }
                        _fullString = _beginString + _endString;

                        if (_fullString != null)
                        {
                            _availableLocation = new XPCollection<Location>(Session, CriteriaOperator.Parse(_fullString));
                        }
                    }else
                    {
                        if (this.Company != null && this.Workplace != null)
                        {
                            _availableLocation = new XPCollection<Location>(Session, new GroupOperator(GroupOperatorType.And,
                                                                                new BinaryOperator("Company", this.Company),
                                                                                new BinaryOperator("Workplace", this.Workplace),
                                                                                new BinaryOperator("OpenVehicle", false),
                                                                                new BinaryOperator("Active", true)));
                        }
                    }
                }
                

                return _availableLocation;

            }
        }

        [VisibleInListView(false)]
        [ImmediatePostData()]
        [DataSourceProperty("AvailableLocation")]
        [Appearance("SalesOrderLineLocationClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Location Location
        {
            get { return _location; }
            set
            {
                SetPropertyValue("Location", ref _location, value);
                if (!IsLoading)
                {
                    if (this._location != null)
                    {
                        SetBegInv(_location);
                    }
                    else
                    {
                        BegInv = null;
                    }
                }
            }

        }

        [Browsable(false)]
        [ImmediatePostData()]
        [Appearance("SalesOrderLineBegInvClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public BeginningInventory BegInv
        {
            get { return _begInv; }
            set { SetPropertyValue("BegInv", ref _begInv, value); }
        }

        #endregion Organization

        [VisibleInListView(false)]
        [ImmediatePostData()]
        [Appearance("SalesOrderLineSalesTypeClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public OrderType SalesType
        {
            get { return _salesType; }
            set { SetPropertyValue("SalesType", ref _salesType, value); }
        }

        [Browsable(false)]
        public XPCollection<Item> AvailableItem
        {
            get
            {
                if(!IsLoading)
                {
                    string _beginString = null;
                    string _endString = null;
                    string _fullString = null;
                    //&& this.BegInv != null
                    if (this.Company != null && this.Workplace != null)
                    {
                        List<string> _stringITM = new List<string>();

                        XPQuery<BeginningInventoryLine> _beginningInventoryLinesQuery = new XPQuery<BeginningInventoryLine>(Session);

                        var _beginningInventoryLines = from bil in _beginningInventoryLinesQuery
                                                       where (bil.Company == this.Company
                                                       && bil.Workplace == this.Workplace
                                                       && bil.Item.ItemType == this.SalesType
                                                       && bil.BeginningInventory == this.BegInv
                                                       && bil.QtyAvailable > 0
                                                       && (bil.QtyAvailable - bil.QtyOnHand) > 0
                                                       && (bil.StockGroup == StockGroup.Normal || bil.StockGroup == StockGroup.None)
                                                       && bil.Lock == false
                                                       && bil.Vehicle == null
                                                       && bil.Active == true
                                                       )
                                                       group bil by bil.Item into g
                                                       select new { Item = g.Key };
                        if (_beginningInventoryLines != null && _beginningInventoryLines.Count() > 0)
                        {
                            foreach (var _beginningInventoryLine in _beginningInventoryLines)
                            {
                                _stringITM.Add(_beginningInventoryLine.Item.Code);
                            }
                        }



                        IEnumerable<string> _stringArrayITMDistinct = _stringITM.Distinct();
                        string[] _stringArrayITMList = _stringArrayITMDistinct.ToArray();
                        if (_stringArrayITMList.Length == 1)
                        {
                            for (int i = 0; i < _stringArrayITMList.Length; i++)
                            {
                                Item _locITM = Session.FindObject<Item>(new BinaryOperator("Code", _stringArrayITMList[i]));
                                if (_locITM != null)
                                {
                                    if (i == 0)
                                    {
                                        _beginString = "[Code]=='" + _locITM.Code + "'";
                                    }
                                }
                            }
                        }
                        else if (_stringArrayITMList.Length > 1)
                        {
                            for (int i = 0; i < _stringArrayITMList.Length; i++)
                            {
                                Item _locITM = Session.FindObject<Item>(new BinaryOperator("Code", _stringArrayITMList[i]));
                                if (_locITM != null)
                                {
                                    if (i == 0)
                                    {
                                        _beginString = "[Code]=='" + _locITM.Code + "'";
                                    }
                                    else
                                    {
                                        _endString = _endString + " OR [Code]=='" + _locITM.Code + "'";
                                    }
                                }
                            }
                        }
                        _fullString = _beginString + _endString;

                        if (_fullString != null)
                        {
                            _availableItem = new XPCollection<Item>(Session, CriteriaOperator.Parse(_fullString));
                        }
                    }
                    else
                    {
                        _availableItem = new XPCollection<Item>(Session, new GroupOperator(GroupOperatorType.And,
                                                        new BinaryOperator("Company", this.Company),
                                                        new BinaryOperator("Workplace", this.Workplace),
                                                        new BinaryOperator("Active", true)));
                    }
                } 
                return _availableItem;
            }
        }

        [Appearance("SalesOrderLineItemRedColor", Criteria = "RecColor = true", BackColor = "#fd79a8")]
        [DataSourceCriteria("Active = true"), ImmediatePostData()]
        [DataSourceProperty("AvailableItem")]
        [Appearance("SalesOrderLineItemClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Item Item
        {
            get { return _item; }
            set
            {
                SetPropertyValue("Item", ref _item, value);

                if (!IsLoading)
                {
                    if (this._item != null)
                    {
                        if (this._item.Description != null)
                        {
                            this.Description = this._item.Description;
                        }
                        if (this._item.BasedUOM != null)
                        {
                            this.DUOM = this._item.BasedUOM; 
                        }
                        SetUOM();
                        SetPriceLine();
                        SetDiscountRule();
                        SetBegInvLine(_item);
                    }
                    else
                    {
                        this.UAmount = 0;
                        this.DiscountRule = null;
                        this.Description = null;
                        this.DUOM = null;
                        this.BegInvLine = null;
                    }
                }
            }
        }

        [Browsable(false)]
        [ImmediatePostData()]
        [Appearance("SalesOrderLineBegInvLineClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public BeginningInventoryLine BegInvLine
        {
            get { return _begInvLine; }
            set { SetPropertyValue("BegInvLine", ref _begInvLine, value); }
        }

        [VisibleInListView(false)]
        [Appearance("SalesOrderLineDescriptionClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [Size(512)]
        public string Description
        {
            get { return _description; }
            set { SetPropertyValue("Description", ref _description, value); }
        }

        public string Stock
        {
            get
            {
                double _locTotAvailableQty = 0;
                string _locBasedUOM = null;
                
                if (this.BegInvLine != null)
                {
                    _locTotAvailableQty = (BegInvLine.QtyAvailable - BegInvLine.QtyOnHand);
                    
                    if(this.Item.BasedUOM != null)
                    {
                        if(this.Item.BasedUOM != null)
                        {
                            if(this.Item.BasedUOM.Name != null)
                            {
                                _locBasedUOM = this.Item.BasedUOM.Name;
                            }
                        } 
                    }
                    return String.Format("{0} ({1})", _locTotAvailableQty.ToString(), _locBasedUOM); 
                }
                return null;
            }
        }

        #region DefaultQty

        [Appearance("SalesOrderLineDQtyClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [ImmediatePostData()]
        public double DQty
        {
            get { return _dQty; }
            set
            {
                SetPropertyValue("DQty", ref _dQty, value);
                if (!IsLoading)
                {
                    SetTotalQty();
                    SetTotalUnitAmount();
                    SetTotalAmount();
                }
            }
        }

        [Appearance("SalesOrderLineDUOMClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [ImmediatePostData()]
        [DataSourceCriteria("Active = true")]
        public UnitOfMeasure DUOM
        {
            get { return _dUom; }
            set
            {
                SetPropertyValue("DUOM", ref _dUom, value);
                if (!IsLoading)
                {
                    SetUOM();
                    SetTotalQty();
                    SetTotalUnitAmount();
                    SetTotalAmount();
                }
            }
        }

        [Appearance("SalesOrderLineQtyClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [ImmediatePostData()]
        public double Qty
        {
            get { return _qty; }
            set
            {
                SetPropertyValue("Qty", ref _qty, value);
                if (!IsLoading)
                {
                    SetTotalQty();
                    SetTotalUnitAmount();
                    SetTotalAmount();
                }
            }
        }

        [Browsable(false)]
        public XPCollection<UnitOfMeasure> AvailableUnitOfMeasure
        {
            get
            {
                if(!IsLoading)
                {
                    string _beginString = null;
                    string _endString = null;
                    string _fullString = null;
                    if (Item == null)
                    {
                        _availableUnitOfMeasure = new XPCollection<UnitOfMeasure>(Session, new GroupOperator(GroupOperatorType.And,
                                                                                new BinaryOperator("Active", true)));
                    }
                    else
                    {
                        List<string> _stringUOM = new List<string>();

                        XPCollection<ItemUnitOfMeasure> _locItemUnitOfMeasures = new XPCollection<ItemUnitOfMeasure>
                                                                                  (Session, new GroupOperator(GroupOperatorType.And,
                                                                                   new BinaryOperator("Item", this.Item),
                                                                                   new BinaryOperator("Active", true)));

                        if (_locItemUnitOfMeasures != null && _locItemUnitOfMeasures.Count() > 0)
                        {
                            foreach (ItemUnitOfMeasure _locItemUnitOfMeasure in _locItemUnitOfMeasures)
                            {
                                _stringUOM.Add(_locItemUnitOfMeasure.UOM.Code);
                            }
                        }

                        IEnumerable<string> _stringArrayUOMDistinct = _stringUOM.Distinct();
                        string[] _stringArrayUOMList = _stringArrayUOMDistinct.ToArray();
                        if (_stringArrayUOMList.Length == 1)
                        {
                            for (int i = 0; i < _stringArrayUOMList.Length; i++)
                            {
                                UnitOfMeasure _locUOM = Session.FindObject<UnitOfMeasure>(new BinaryOperator("Code", _stringArrayUOMList[i]));
                                if (_locUOM != null)
                                {
                                    if (i == 0)
                                    {
                                        _beginString = "[Code]=='" + _locUOM.Code + "'";
                                    }
                                }
                            }
                        }
                        else if (_stringArrayUOMList.Length > 1)
                        {
                            for (int i = 0; i < _stringArrayUOMList.Length; i++)
                            {
                                UnitOfMeasure _locUOM = Session.FindObject<UnitOfMeasure>(new BinaryOperator("Code", _stringArrayUOMList[i]));
                                if (_locUOM != null)
                                {
                                    if (i == 0)
                                    {
                                        _beginString = "[Code]=='" + _locUOM.Code + "'";
                                    }
                                    else
                                    {
                                        _endString = _endString + " OR [Code]=='" + _locUOM.Code + "'";
                                    }
                                }
                            }
                        }
                        _fullString = _beginString + _endString;

                        if (_fullString != null)
                        {
                            _availableUnitOfMeasure = new XPCollection<UnitOfMeasure>(Session, CriteriaOperator.Parse(_fullString));
                        }

                    }
                }

                return _availableUnitOfMeasure;

            }
        }

        [ImmediatePostData()]
        [DataSourceProperty("AvailableUnitOfMeasure", DataSourcePropertyIsNullMode.SelectAll)]
        [Appearance("SalesOrderLineUOMClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public UnitOfMeasure UOM
        {
            get { return _uom; }
            set
            {
                SetPropertyValue("UOM", ref _uom, value);
                if (!IsLoading)
                {
                    SetTotalQty();
                    SetTotalUnitAmount();
                    SetTotalAmount();
                }
            }
        }

        [Appearance("SalesOrderLineTotalQtyEnabled", Enabled = false)]
        public double TQty
        {
            get { return _tQty; }
            set { SetPropertyValue("TotalQty", ref _tQty, value); }
        }

        [VisibleInListView(false)]
        [Persistent]
        public string FullName
        {
            get
            {
                if (this.DQty > 0 && this.Qty > 0)
                {
                    return String.Format("{0} {1} {2} {3}", this.Qty, this.UOM.Code, this.DQty, this.DUOM.Code);
                }
                else if(this.DQty > 0 && this.Qty == 0)
                {
                    return String.Format("{0} {1}", this.DQty, this.DUOM.Code);
                }
                else if(this.DQty == 0 && this.Qty > 0)
                {
                    return String.Format("{0} {1}", this.Qty, this.UOM.Code);
                }
                return null;
            }
        }

        [VisibleInListView(false)]
        [Appearance("SalesOrderLineStockTypeClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public StockType StockType
        {
            get { return _stockType; }
            set { SetPropertyValue("StockType", ref _stockType, value); }
        }

        #endregion DefaultQty

        #region Amount

        [VisibleInListView(false)]
        [Appearance("SalesOrderLineCurrencyClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Currency Currency
        {
            get { return _currency; }
            set { SetPropertyValue("Currency", ref _currency, value); }
        }

        [VisibleInListView(false)]
        [ImmediatePostData()]
        [Appearance("SalesOrderLinePriceGroupClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public PriceGroup PriceGroup
        {
            get { return _priceGroup; }
            set {
                SetPropertyValue("PriceGroup", ref _priceGroup, value);
                if(!IsLoading)
                {
                    if(this._priceGroup != null)
                    {
                        SetPriceLine();
                        SetDiscountRule();
                    }
                }
            }
        }

        [Browsable(false)]
        [DataSourceCriteria("Active = true"), ImmediatePostData()]
        [Appearance("SalesOrderLinePriceLineClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public PriceLine PriceLine
        {
            get { return _priceLine; }
            set
            {
                SetPropertyValue("PriceLine", ref _priceLine, value);
                if (!IsLoading)
                {
                    if (this._priceLine != null)
                    {
                        this.UAmount = this._priceLine.Amount2a;
                    }
                }
            }
        }

        [Appearance("SalesOrderLineUAmountClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [ImmediatePostData()]
        public double UAmount
        {
            get { return _uAmount; }
            set
            {
                SetPropertyValue("UAmount", ref _uAmount, value);
                if (!IsLoading)
                {
                    _globFunc = new GlobalFunction();
                    if (this._uAmount > 0)
                    {
                        if (_globFunc.GetRoundingList(Session, ObjectList.SalesOrderLine, FieldName.UAmount) == true)
                        {
                            this._uAmount = _globFunc.GetRoundUp(Session, this._uAmount, ObjectList.SalesOrderLine, FieldName.UAmount);
                        }
                    }
                    SetTotalUnitAmount();
                    SetTotalAmount();
                }
            }
        }

        [Appearance("SalesOrderLineTUAmountEnabled", Enabled = false)]
        [ImmediatePostData()]
        public double TUAmount
        {
            get { return _tUAmount; }
            set { SetPropertyValue("TUAmount", ref _tUAmount, value); }
        }

        [Appearance("SalesOrderLineTaxClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [ImmediatePostData]
        [DataSourceCriteria("Active = true")]
        public Tax Tax
        {
            get { return _tax; }
            set
            {
                SetPropertyValue("Tax", ref _tax, value);
                if (!IsLoading)
                {
                    if (this._tax != null)
                    {
                        this.TxValue = this._tax.Value;
                    }
                    else
                    {
                        SetNormalTax();
                    }
                }
            }
        }

        [Browsable(false)]
        [ImmediatePostData()]
        [Appearance("SalesOrderLineTxValueClose", Enabled = false)]
        public double TxValue
        {
            get { return _txValue; }
            set { SetPropertyValue("TxValue", ref _txValue, value); }
        }

        [Appearance("SalesOrderLineTxAmountEnabled", Enabled = false)]
        [ImmediatePostData()]
        public double TxAmount
        {
            get { return _txAmount; }
            set
            {
                SetPropertyValue("TxAmount", ref _txAmount, value);
                if (!IsLoading)
                {
                    _globFunc = new GlobalFunction();
                    if (this._txAmount > 0)
                    {
                        if (_globFunc.GetRoundingList(Session, ObjectList.SalesOrderLine, FieldName.TxAmount) == true)
                        {
                            this._txAmount = _globFunc.GetRoundUp(Session, this._txAmount, ObjectList.SalesOrderLine, FieldName.TxAmount);
                        }
                    }

                    SetTotalAmount();
                }
            }
        }

        [ImmediatePostData()]
        [DataSourceCriteria("Company = '@This.Company' AND Workplace = '@This.Workplace' AND MpItem = '@This.Item' AND PriceGroup = '@This.PriceGroup' AND Active = true")]
        [Appearance("SalesInvoiceLineDiscountRuleClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public DiscountRule DiscountRule
        {
            get { return _discountRule; }
            set { SetPropertyValue("DiscountRule", ref _discountRule, value); }
        }

        [Browsable(false)]
        public AccountingPeriodicLine AccountingPeriodicLine
        {
            get { return _accountingPeriodicLine; }
            set { SetPropertyValue("AccountingPeriodicLine", ref _accountingPeriodicLine, value); }
        }

        [Appearance("SalesOrderLineDiscAmountEnabled", Enabled = false)]
        [ImmediatePostData()]
        public double DiscAmount
        {
            get { return _discAmount; }
            set
            {
                SetPropertyValue("DiscAmount", ref _discAmount, value);
                if (!IsLoading)
                {
                    _globFunc = new GlobalFunction();
                    if (this._discAmount > 0)
                    {
                        if (_globFunc.GetRoundingList(Session, ObjectList.SalesOrderLine, FieldName.DiscAmount) == true)
                        {
                            this._discAmount = _globFunc.GetRoundUp(Session, this._discAmount, ObjectList.SalesOrderLine, FieldName.DiscAmount);
                        }
                    }
                }
            }
        }

        [Appearance("SalesOrderLineTPriceEnabled", Enabled = false)]
        [ImmediatePostData()]
        public double TAmount
        {
            get { return _tAmount; }
            set
            {
                SetPropertyValue("TAmount", ref _tAmount, value);
                if (!IsLoading)
                {
                    _globFunc = new GlobalFunction();
                    if (this._tAmount > 0)
                    {
                        if (_globFunc.GetRoundingList(Session, ObjectList.SalesOrderLine, FieldName.TAmount) == true)
                        {
                            this._tAmount = _globFunc.GetRoundUp(Session, this._tAmount, ObjectList.SalesOrderLine, FieldName.TAmount);
                        }
                    }
                }
            }
        }

        #endregion Amount

        [VisibleInListView(false)]
        [ModelDefault("DisplayFormat", "{0:dd/MM/yyyy}")]
        [ModelDefault("EditMask", "dd/MM/yyyy")]
        [Appearance("SalesOrderLineETDClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public DateTime ETD
        {
            get { return _etd; }
            set { SetPropertyValue("ETD", ref _etd, value); }
        }

        [VisibleInListView(false)]
        [ModelDefault("DisplayFormat", "{0:dd/MM/yyyy}")]
        [ModelDefault("EditMask", "dd/MM/yyyy")]
        [Appearance("SalesOrderLineETAClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public DateTime ETA
        {
            get { return _eta; }
            set { SetPropertyValue("ETA", ref _eta, value); }
        }

        [VisibleInListView(false)]
        [Appearance("SalesOrderLineStatusEnabled", Enabled = false)]
        public Status Status
        {
            get { return _status; }
            set { SetPropertyValue("Status", ref _status, value); }
        }

        [VisibleInListView(false)]
        [ModelDefault("DisplayFormat", "{0:dd/MM/yyyy}")]
        [ModelDefault("EditMask", "dd/MM/yyyy")]
        [Appearance("SalesOrderLineStatusDateEnabled", Enabled = false)]
        public DateTime StatusDate
        {
            get { return _statusDate; }
            set { SetPropertyValue("StatusDate", ref _statusDate, value); }
        }

        [VisibleInListView(false)]
        [Appearance("SalesOrderLinePostedCountEnabled", Enabled = false)]
        public int PostedCount
        {
            get { return _postedCount; }
            set { SetPropertyValue("PostedCount", ref _postedCount, value); }
        }
       
        [ImmediatePostData()]
        [VisibleInListView(false)]
        [Association("SalesOrder-SalesOrderLines")]
        [Appearance("SalesOrderLineSalesOrderEnabled", Enabled = false)]
        [RuleRequiredField(DefaultContexts.Save)]
        public SalesOrder SalesOrder
        {
            get { return _salesOrder; }
            set
            {
                SetPropertyValue("SalesOrder", ref _salesOrder, value);
                if (!IsLoading)
                {
                    if (this._salesOrder != null)
                    {
                        if (this._salesOrder.Company != null) { this.Company = this._salesOrder.Company; }
                        if (this._salesOrder.Workplace != null) { this.Workplace = this._salesOrder.Workplace; }
                        if (this._salesOrder.Division != null) { this.Division = this._salesOrder.Division; }
                        if (this._salesOrder.Department != null) { this.Department = this._salesOrder.Department; }
                        if (this._salesOrder.Section != null) { this.Section = this._salesOrder.Section; }
                        if (this._salesOrder.Employee != null) { this.Employee = this._salesOrder.Employee; }
                        if (this._salesOrder.Div != null) { this.Div = this._salesOrder.Div; }
                        if (this._salesOrder.Dept != null) { this.Dept = this._salesOrder.Dept; }
                        if (this._salesOrder.Sect != null) { this.Sect = this._salesOrder.Sect; }
                        if (this._salesOrder.PIC != null) { this.PIC = this._salesOrder.PIC; }
                        if (this._salesOrder.Location != null) { this.Location = this._salesOrder.Location; }
                        if (this._salesOrder.BegInv != null) { this.BegInv = this._salesOrder.BegInv; }
                        if (this._salesOrder.PriceGroup != null) { this.PriceGroup = this._salesOrder.PriceGroup; }
                        if (this._salesOrder.Currency != null) { this.Currency = this._salesOrder.Currency; }
                        this.ETD = this._salesOrder.ETD;
                        this.ETA = this._salesOrder.ETA;
                    }
                }
            }
        }

        //AuditTrail
        [CollectionOperationSet(AllowAdd = false, AllowRemove = true)]
        public XPCollection<AuditDataItemPersistent> ChangeHistory
        {
            get
            {
                if (changeHistory == null)
                {
                    changeHistory = AuditedObjectWeakReference.GetAuditTrail(Session, this);
                }
                return changeHistory;
            }
        }

        #endregion Field

        //================================================== Code Only ===============================================

        #region CodeOnly
        public string GetDescriptions()
        {
            string _result = "";
            try
            {
                if (this._item != null)
                {
                    string _result1 = null;
                    string _result2 = null;
                    if (this._item.Name != null)
                    {
                        _result1 = this._item.Name;
                    }
                    if (this._item.Description != null)
                    {
                        _result2 = this._item.Description;
                    }
                    _result = _result1 + " " + _result2;
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = SalesOrderLine " + ex.ToString());
            }

            return _result;
        }

        #region No

        public void UpdateNo()
        {
            try
            {
                if (!IsLoading && Session.IsNewObject(this))
                {
                    if (this.SalesOrder != null)
                    {
                        object _makRecord = Session.Evaluate<SalesOrderLine>(CriteriaOperator.Parse("Max(No)"), CriteriaOperator.Parse("SalesOrder=?", this.SalesOrder));
                        this.No = Convert.ToInt32(_makRecord) + 1;
                        this.Save();
                        RecoveryUpdateNo();
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = SalesOrderLine " + ex.ToString());
            }
        }

        public void RecoveryUpdateNo()
        {
            try
            {
                if (this.SalesOrder != null)
                {
                    SalesOrder _numHeader = Session.FindObject<SalesOrder>
                                            (new BinaryOperator("Code", this.SalesOrder.Code));

                    XPCollection<SalesOrderLine> _numLines = new XPCollection<SalesOrderLine>
                                                             (Session, new BinaryOperator("SalesOrder", _numHeader),
                                                              new SortProperty("No", DevExpress.Xpo.DB.SortingDirection.Ascending));
                    if (_numLines != null)
                    {
                        int i1 = 0;
                        foreach (SalesOrderLine _numLine in _numLines)
                        {
                            i1 += 1;
                            _numLine.No = i1;
                            _numLine.Save();
                        }
                        i1 = 1;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = SalesOrderLine " + ex.ToString());
            }
        }

        public void RecoveryDeleteNo()
        {
            try
            {
                if (this.SalesOrder != null)
                {
                    SalesOrder _numHeader = Session.FindObject<SalesOrder>
                                            (new BinaryOperator("Code", this.SalesOrder.Code));

                    XPCollection<SalesOrderLine> _numLines = new XPCollection<SalesOrderLine>
                                                             (Session, new GroupOperator(GroupOperatorType.And,
                                                              new BinaryOperator("This", this, BinaryOperatorType.NotEqual),
                                                              new BinaryOperator("SalesOrder", _numHeader)),
                                                              new SortProperty("No", DevExpress.Xpo.DB.SortingDirection.Ascending));
                    if (_numLines != null)
                    {
                        int i = 0;
                        foreach (SalesOrderLine _numLine in _numLines)
                        {
                            i += 1;
                            _numLine.No = i;
                            _numLine.Save();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = SalesOrderLine " + ex.ToString());
            }
        }

        #endregion No

        #region Set

        private void SetTotalQty()
        {
            try
            {
                double _locInvLineTotal = 0;
                DateTime now = DateTime.Now;
                if (this.SalesOrder != null)
                {
                    if (this.Item != null && this.UOM != null && this.DUOM != null)
                    {
                        ItemUnitOfMeasure _locItemUOM = Session.FindObject<ItemUnitOfMeasure>
                                                        (new GroupOperator(GroupOperatorType.And,
                                                         new BinaryOperator("Item", this.Item),
                                                         new BinaryOperator("UOM", this.UOM),
                                                         new BinaryOperator("DefaultUOM", this.DUOM),
                                                         new BinaryOperator("Active", true)));
                        if (_locItemUOM != null)
                        {
                            if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                            {
                                _locInvLineTotal = this.Qty * _locItemUOM.DefaultConversion + this.DQty;
                            }
                            else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                            {
                                _locInvLineTotal = this.Qty / _locItemUOM.Conversion + this.DQty;
                            }
                            else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                            {
                                _locInvLineTotal = this.Qty + this.DQty;
                            }

                            this.TQty = _locInvLineTotal;
                        }
                    }
                    else
                    {
                        _locInvLineTotal = this.Qty + this.DQty;
                        this.TQty = _locInvLineTotal;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = SalesOrderLine " + ex.ToString());
            }
        }

        private void SetTotalUnitAmount()
        {
            try
            {
                _globFunc = new GlobalFunction();

                if (_tQty >= 0 & _uAmount >= 0)
                {
                    if (_globFunc.GetRoundingList(Session, ObjectList.SalesOrderLine, FieldName.TUAmount) == true)
                    {
                        this.TUAmount = _globFunc.GetRoundUp(Session, (this.TQty * this.UAmount), ObjectList.SalesOrderLine, FieldName.TUAmount);
                    }
                    else
                    {
                        this.TUAmount = this.TQty * this.UAmount;
                    }
                }
                else
                {
                    this.TUAmount = 0;
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = SalesOrderLine " + ex.ToString());
            }
        }

        private void SetTotalAmount()
        {
            try
            {
                _globFunc = new GlobalFunction();
                if (!IsLoading)
                {
                    if (_uAmount >= 0 && _txAmount >= 0 && _discAmount >= 0)
                    {
                        if (this.Tax != null)
                        {
                            if (this.Tax.TaxNature == TaxNature.Increase)
                            {
                                if (_globFunc.GetRoundingList(Session, ObjectList.SalesOrderLine, FieldName.TAmount) == true)
                                {
                                    this.TAmount = _globFunc.GetRoundUp(Session, (this.TUAmount + this.TxAmount - this.DiscAmount), ObjectList.SalesOrderLine, FieldName.TAmount);
                                }
                                else
                                {
                                    this.TAmount = this.TUAmount + this.TxAmount - this.DiscAmount;
                                }
                            }
                            else if (this.Tax.TaxNature == TaxNature.Decrease)
                            {
                                if (_globFunc.GetRoundingList(Session, ObjectList.SalesOrderLine, FieldName.TAmount) == true)
                                {
                                    this.TAmount = _globFunc.GetRoundUp(Session, (this.TUAmount - this.TxAmount - this.DiscAmount), ObjectList.SalesOrderLine, FieldName.TAmount);
                                }
                                else
                                {
                                    this.TAmount = this.TUAmount - this.TxAmount - this.DiscAmount;
                                }
                            }
                            else if (this.Tax.TaxNature == TaxNature.None)
                            {
                                if (_globFunc.GetRoundingList(Session, ObjectList.SalesOrderLine, FieldName.TAmount) == true)
                                {
                                    this.TAmount = _globFunc.GetRoundUp(Session, (this.TUAmount - this.DiscAmount), ObjectList.SalesOrderLine, FieldName.TAmount);
                                }
                                else
                                {
                                    this.TAmount = this.TUAmount - this.DiscAmount;
                                }
                            }
                            else
                            {
                                if (_globFunc.GetRoundingList(Session, ObjectList.SalesOrderLine, FieldName.TAmount) == true)
                                {
                                    this.TAmount = _globFunc.GetRoundUp(Session, (this.TUAmount + this.TxAmount - this.DiscAmount), ObjectList.SalesOrderLine, FieldName.TAmount);
                                }
                                else
                                {
                                    this.TAmount = this.TUAmount + this.TxAmount - this.DiscAmount;
                                }
                            }
                        }
                        else
                        {
                            this.TAmount = this.TUAmount + this.TxAmount - this.DiscAmount;
                        }
                    }
                    else
                    {
                        this.TAmount = this.TUAmount + this.TxAmount - this.DiscAmount;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = SalesOrderLine " + ex.ToString());
            }
        }

        private void SetNormalTax()
        {
            try
            {
                if (_tax != null && _txValue > 0 && _txAmount > 0)
                {
                    this.Tax = this.Tax;
                    this.TxValue = this.TxValue;
                    this.TxAmount = this.TxAmount;
                }
                else
                {
                    this.TxValue = 0;
                    this.TxAmount = 0;
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = SalesOrderLine " + ex.ToString());
            }
        }

        private void SetPriceLine()
        {
            try
            {
                if (this.Company != null && this.Workplace != null && this.Item != null && this.PriceGroup != null)
                {
                    PriceLine _locPriceLine = Session.FindObject<PriceLine>(new GroupOperator(GroupOperatorType.And,
                                                                            new BinaryOperator("Company", this.Company),
                                                                            new BinaryOperator("Workplace", this.Workplace),
                                                                            new BinaryOperator("Item", this.Item),
                                                                            new BinaryOperator("PriceGroup", this.PriceGroup),
                                                                            new BinaryOperator("Active", true)));
                    if(_locPriceLine != null)
                    {
                        this.PriceLine = _locPriceLine;
                        this.UAmount = _locPriceLine.Amount2a;
                    }
                    else
                    {
                        this.PriceLine = null;
                        this.UAmount = 0;
                    }
                }
                else
                {
                    this.PriceLine = null;
                    this.UAmount = 0;
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = SalesOrderLine " + ex.ToString());
            }
        }

        private void SetUOM()
        {
            try
            {
                if (this.Company != null && this.Item != null && this.DUOM != null)
                {
                    ItemUnitOfMeasure _locIUOM = Session.FindObject<ItemUnitOfMeasure>(new GroupOperator(GroupOperatorType.And,
                                                                            new BinaryOperator("Company", this.Company),
                                                                            new BinaryOperator("Item", this.Item),
                                                                            new BinaryOperator("DefaultUOM", this.DUOM),
                                                                            new BinaryOperator("Default", true),
                                                                            new BinaryOperator("Active", true)));
                    if (_locIUOM != null)
                    {
                        if (_locIUOM.UOM != null)
                        {
                            this.UOM = _locIUOM.UOM;
                        }
                        else
                        {
                            this.UOM = null;
                        }

                    }
                    else
                    {
                        this.UOM = null;
                    }
                }
                else
                {
                    this.UOM = null;
                }

            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = SalesOrderLine " + ex.ToString());
            }
        }

        private void SetDiscountRule()
        {
            try
            {
                if (this.Company != null && this.Workplace != null && this.Item != null && this.PriceGroup != null)
                {
                    DiscountRule _locDiscountRule2a = Session.FindObject<DiscountRule>
                                                            (new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("Company", this.Company),
                                                            new BinaryOperator("Workplace", this.Workplace),
                                                            new BinaryOperator("PriceGroup", this.PriceGroup),
                                                            new BinaryOperator("AllItem", true),
                                                            new BinaryOperator("RuleType", RuleType.Rule2),
                                                            new BinaryOperator("Active", true)));
                    if (_locDiscountRule2a != null)
                    {
                        this.DiscountRule = _locDiscountRule2a;
                    }
                    else
                    {
                        DiscountRule _locDiscountRule2b = Session.FindObject<DiscountRule>
                                                            (new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("Company", this.Company),
                                                            new BinaryOperator("Workplace", this.Workplace),
                                                            new BinaryOperator("PriceGroup", this.PriceGroup),
                                                            new BinaryOperator("MpItem", this.Item),
                                                            new BinaryOperator("RuleType", RuleType.Rule2),
                                                            new BinaryOperator("Active", true)));
                        if (_locDiscountRule2b != null)
                        {
                            this.DiscountRule = _locDiscountRule2b;
                        }
                        else
                        {
                            this.DiscountRule = null;
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = SalesOrderLine " + ex.ToString());
            }
        }

        private void SetLocation(Employee _locPIC)
        {
            try
            {
                DateTime now = DateTime.Now;
                if (this.Company != null && this.Workplace != null)
                {
                    SalesmanLocationSetup _locSalesmanLocationSetup = Session.FindObject<SalesmanLocationSetup>
                                                        (new GroupOperator(GroupOperatorType.And,
                                                        new BinaryOperator("Company", this.Company),
                                                        new BinaryOperator("Workplace", this.Workplace),
                                                        new BinaryOperator("Salesman", _locPIC),
                                                        new BinaryOperator("Active", true),
                                                        new BinaryOperator("Default", true)));
                    if (_locSalesmanLocationSetup != null)
                    {
                        if (_locSalesmanLocationSetup.Location != null)
                        {
                            this.Location = _locSalesmanLocationSetup.Location;
                        }
                        else
                        {
                            this.Location = null;
                        }
                    }
                    else
                    {
                        this.Location = null;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Business Object = SalesOrderLine ", ex.ToString());
            }

        }

        private void SetBegInv(Location _locLocation)
        {
            try
            {
                DateTime now = DateTime.Now;
                if (this.Company != null && this.Workplace != null)
                {
                    BeginningInventory _locBegInv = Session.FindObject<BeginningInventory>
                                                        (new GroupOperator(GroupOperatorType.And,
                                                        new BinaryOperator("Company", this.Company),
                                                        new BinaryOperator("Workplace", this.Workplace),
                                                        new BinaryOperator("Location", _locLocation),
                                                        new BinaryOperator("LocationType", LocationType.Main),
                                                        new BinaryOperator("StockType", StockType.Good),
                                                        new BinaryOperator("Active", true)));
                    if (_locBegInv != null)
                    {
                        this.BegInv = _locBegInv;
                    }
                    else
                    {
                        this.BegInv = null;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Business Object = SalesOrderLine ", ex.ToString());
            }
        }

        private void SetBegInvLine(Item _locItem)
        {
            try
            {
                DateTime now = DateTime.Now;
                if (this.Company != null && this.Workplace != null && this.BegInv != null)
                {
                    BeginningInventoryLine _locBegInvLine = Session.FindObject<BeginningInventoryLine>(
                                                                            new GroupOperator(GroupOperatorType.And,
                                                                            new BinaryOperator("Company", this.Company),
                                                                            new BinaryOperator("Workplace", this.Workplace),
                                                                            new BinaryOperator("BeginningInventory", this.BegInv),
                                                                            new BinaryOperator("Item", _locItem),
                                                                            new BinaryOperator("StockType", StockType.Good),
                                                                            new BinaryOperator("StockGroup", StockGroup.Normal),
                                                                            new BinaryOperator("LocationType", LocationType.Main),
                                                                            new BinaryOperator("Lock", false),
                                                                            new BinaryOperator("Active", true)));
                    if (_locBegInvLine != null)
                    {
                        this.BegInvLine = _locBegInvLine;
                    }
                    else
                    {
                        this.BegInvLine = null;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Business Object = SalesOrderLine ", ex.ToString());
            }
        }

        #endregion Set

        #region RollBack

        public void RollBackQtyOnHandAndBalHold()
        {
            try
            {
                double _locQtyOnHand = 0;
                double _locBalHold = 0;
                if (this.SalesOrder != null)
                {
                    if(this.Status == Status.Progress || this.Status == Status.Posted || this.Status == Status.Close)
                    {
                        if(this.BegInvLine != null)
                        {
                            _locQtyOnHand = this.BegInvLine.QtyOnHand - this.TQty;
                            if(_locQtyOnHand < 0 )
                            {
                                _locQtyOnHand = 0;
                            }
                            this.BegInvLine.QtyOnHand = _locQtyOnHand;
                            this.BegInvLine.Save();
                            this.BegInvLine.Session.CommitTransaction();
                        }
                        if(this.AccountingPeriodicLine != null)
                        {
                            _locBalHold = this.AccountingPeriodicLine.BalHold - this.DiscAmount;
                            if (_locBalHold < 0)
                            {
                                _locBalHold = 0;
                            }
                            this.AccountingPeriodicLine.BalHold = _locBalHold;
                            this.AccountingPeriodicLine.Save();
                            this.AccountingPeriodicLine.Session.CommitTransaction();
                        }

                        SalesOrderMonitoring _locSalesOrderMonitoring = Session.FindObject<SalesOrderMonitoring>(new GroupOperator(GroupOperatorType.And,
                                                                        new BinaryOperator("SalesOrder", this.SalesOrder),
                                                                        new BinaryOperator("SalesOrderLine", this)));
                        if(_locSalesOrderMonitoring != null)
                        {
                            _locSalesOrderMonitoring.Delete();
                            _locSalesOrderMonitoring.Session.CommitTransaction();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = SalesOrderLine " + ex.ToString());
            }
        }

        #endregion RollBack

        #endregion CodeOnly
    }
}