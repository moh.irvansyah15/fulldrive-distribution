﻿using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;

using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.ConditionalAppearance;

namespace FullDrive.Module.BusinessObjects
{
    [DefaultClassOptions]
    [DefaultProperty("Code")]
    [NavigationItem("Cash Advance")]
    [RuleCombinationOfPropertiesIsUnique("CashAdvanceCollectionRuleUnique", DefaultContexts.Save, "Code")]
    //[ImageName("BO_Contact")]
    //[DefaultProperty("DisplayMemberNameForLookupEditorsOfThisType")]
    //[DefaultListViewOptions(MasterDetailMode.ListViewOnly, false, NewItemRowPosition.None)]
    //[Persistent("DatabaseTableName")]
    // Specify more UI options using a declarative approach (https://documentation.devexpress.com/#eXpressAppFramework/CustomDocument112701).
    public class CashAdvanceCollection : BaseObject
    { // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).
        private bool _activationPosting;
        private string _code;
        #region InitialOrganization
        private Company _company;
        private Workplace _workplace;
        private Division _division;
        private Department _department;
        private Section _section;
        private Employee _employee;
        private Division _divisionCA;
        private Department _departmentCA;
        private Section _sectionCA;
        private Employee _employeeCA;
        private Position _position;
        #endregion InitialOrganization
        private XPCollection<CashAdvance> _availableCashAdvance;
        private CashAdvance _cashAdvance;
        private PaymentRealization _paymentRealization;
        private Status _status;
        private DateTime _statusDate;
        
        private int _postedCount;
        private string _localUserAccess;
        private GlobalFunction _globFunc;
        //AuditTrail
        private XPCollection<AuditDataItemPersistent> changeHistory;

        public CashAdvanceCollection(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if (!IsLoading)
            {
                if(Session.IsNewObject(this))
                {
                    _globFunc = new GlobalFunction();
                    DateTime now = DateTime.Now;
                    #region UserAccess
                    _localUserAccess = SecuritySystem.CurrentUserName;
                    UserAccess _locUserAccess = Session.FindObject<UserAccess>(new BinaryOperator("UserName", _localUserAccess));
                    if (_locUserAccess != null)
                    {
                        if (_locUserAccess.Employee != null)
                        {
                            if (_locUserAccess.Employee.Company != null && _locUserAccess.Employee.Workplace != null)
                            {
                                this.Code = _globFunc.GetNumberingUnlockOptimisticRecordBasedOrganization(this.Session.DataLayer, ObjectList.CashAdvanceCollection, _locUserAccess.Employee.Company, _locUserAccess.Employee.Workplace);
                            }
                            else
                            {
                                this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.CashAdvanceCollection);
                            }
                        }
                    }
                    #endregion UserAccess
                    this.Status = Status.Open;
                    this.StatusDate = now;
                }                
            }
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).
        }

        #region Field

        [Browsable(false)]
        public bool ActivationPosting
        {
            get { return _activationPosting; }
            set { SetPropertyValue("ActivationPosting", ref _activationPosting, value); }
        }

        [RuleRequiredField(DefaultContexts.Save)]
        [Appearance("CashAdvanceCollectionCodeClose", Enabled = false)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue("Code", ref _code, value); }
        }

        #region Organization

        [VisibleInListView(false)]
        [RuleRequiredField(DefaultContexts.Save)]
        [Appearance("CashAdvanceCollectionCompanyClose", Enabled = false)]
        public Company Company
        {
            get { return _company; }
            set { SetPropertyValue("Company", ref _company, value); }
        }

        [VisibleInListView(false)]
        [Appearance("CashAdvanceCollectionWorkplaceClose", Enabled = false)]
        public Workplace Workplace
        {
            get { return _workplace; }
            set { SetPropertyValue("Workplace", ref _workplace, value); }
        }

        [Browsable(false)]
        public Division Division
        {
            get { return _division; }
            set { SetPropertyValue("Division", ref _division, value); }
        }

        [Browsable(false)]
        public Department Department
        {
            get { return _department; }
            set { SetPropertyValue("Department", ref _department, value); }
        }

        [Browsable(false)]
        public Section Section
        {
            get { return _section; }
            set { SetPropertyValue("Section", ref _section, value); }
        }

        [Browsable(false)]
        public Employee Employee
        {
            get { return _employee; }
            set { SetPropertyValue("Employee", ref _employee, value); }
        }

        [Browsable(false)]
        [DataSourceCriteria("Company = '@This.Company' And Active = true "), ImmediatePostData()]
        [Appearance("CashAdvanceCollectionDivisionCAClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Division DivisionCA
        {
            get { return _divisionCA; }
            set { SetPropertyValue("DivisionCA", ref _divisionCA, value); }
        }

        [Browsable(false)]
        [DataSourceCriteria("Company = '@This.Company' And Division = '@This.DivisionCA' And Active = true"), ImmediatePostData()]
        [Appearance("CashAdvanceCollectionDepartmentCAClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Department DepartmentCA
        {
            get { return _departmentCA; }
            set { SetPropertyValue("DepartmentCA", ref _departmentCA, value); }
        }

        [Browsable(false)]
        [DataSourceCriteria("Company = '@This.Company' And Department = '@This.DepartmentCA' And Active = true "), ImmediatePostData()]
        [Appearance("CashAdvanceCollectionSectionCAClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Section SectionCA
        {
            get { return _sectionCA; }
            set { SetPropertyValue("SectionCA", ref _sectionCA, value); }
        }

        [Browsable(false)]
        [ImmediatePostData()]
        [DataSourceCriteria("Company = '@This.Company' And Workplace = '@This.Workplace' And Division = '@This.DivisionCA' And Department = '@This.DepartmentCA' And Section = '@This.SectionCA' And Active = true ")]
        [Appearance("CashAdvanceCollectionEmployeeCAClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Employee EmployeeCA
        {
            get { return _employeeCA; }
            set
            {
                SetPropertyValue("EmployeeCA", ref _employeeCA, value);
                if (!IsLoading)
                {
                    if (this._employeeCA != null)
                    {
                        if (this._employeeCA.Position != null)
                        {
                            this.Position = this._employeeCA.Position;
                        }
                    }
                }
            }
        }

        [Browsable(false)]
        [Appearance("CashAdvanceCollectionPositionClose", Enabled = false)]
        public Position Position
        {
            get { return _position; }
            set { SetPropertyValue("Position", ref _position, value); }
        }

        #endregion Organization

        [Browsable(false)]
        public XPCollection<CashAdvance> AvailableCashAdvance
        {
            get
            {
                string _beginString = null;
                string _endString = null;
                string _fullString = null;

                if (!IsLoading)
                {
                    if (this.PaymentRealization != null)
                    {
                        if(this.Company != null && this.Workplace != null)
                        {
                            #region Organization 1
                            if(this.DivisionCA != null && this.DepartmentCA != null && this.SectionCA != null && this.EmployeeCA != null)
                            {

                                XPQuery<CashAdvanceMonitoring> _cashAdvanceMonitoringsQuery = new XPQuery<CashAdvanceMonitoring>(Session);

                                var _cashAdvanceMonitorings = from cam in _cashAdvanceMonitoringsQuery
                                                              where ((cam.Status == Status.Open || cam.Status == Status.Posted)
                                                              && cam.Company == this.Company
                                                              && cam.Workplace == this.Workplace
                                                              && cam.Division == this.DivisionCA
                                                              && cam.Department == this.DepartmentCA
                                                              && cam.Section == this.SectionCA
                                                              && cam.Employee == this.EmployeeCA
                                                              && cam.PaymentRealization == null)
                                                              group cam by cam.CashAdvance into g
                                                              select new { CashAdvance = g.Key };

                                if (_cashAdvanceMonitorings != null && _cashAdvanceMonitorings.Count() > 0)
                                {
                                    List<string> _stringCAM = new List<string>();

                                    foreach (var _cashAdvanceMonitoring in _cashAdvanceMonitorings)
                                    {
                                        if (_cashAdvanceMonitoring != null)
                                        {
                                            if (_cashAdvanceMonitoring.CashAdvance.Code != null)
                                            {
                                                _stringCAM.Add(_cashAdvanceMonitoring.CashAdvance.Code);
                                            }
                                        }
                                    }

                                    IEnumerable<string> _stringArrayCAMDistinct = _stringCAM.Distinct();
                                    string[] _stringArrayCAMList = _stringArrayCAMDistinct.ToArray();
                                    if (_stringArrayCAMList.Length == 1)
                                    {
                                        for (int i = 0; i < _stringArrayCAMList.Length; i++)
                                        {
                                            if (i == 0)
                                            {
                                                _beginString = "[Code]=='" + _stringArrayCAMList[i] + "'";
                                            }
                                        }
                                    }
                                    else if (_stringArrayCAMList.Length > 1)
                                    {
                                        for (int i = 0; i < _stringArrayCAMList.Length; i++)
                                        {
                                            if (i == 0)
                                            {
                                                _beginString = "[Code]=='" + _stringArrayCAMList[i] + "'";
                                            }
                                            else
                                            {
                                                _endString = _endString + " OR [Code]=='" + _stringArrayCAMList[i] + "'";
                                            }
                                        }
                                    }
                                    _fullString = _beginString + _endString; 
                                }
                            }
                            #endregion Organization 1
                            #region Organization 2
                            else if (this.DivisionCA != null && this.DepartmentCA != null && this.SectionCA != null && this.EmployeeCA == null)
                            {
                                XPQuery<CashAdvanceMonitoring> _cashAdvanceMonitoringsQuery = new XPQuery<CashAdvanceMonitoring>(Session);

                                var _cashAdvanceMonitorings = from cam in _cashAdvanceMonitoringsQuery
                                                              where ((cam.Status == Status.Open || cam.Status == Status.Posted)
                                                              && cam.Company == this.Company
                                                              && cam.Workplace == this.Workplace
                                                              && cam.Division == this.DivisionCA
                                                              && cam.Department == this.DepartmentCA
                                                              && cam.Section == this.SectionCA
                                                              && cam.PaymentRealization == null)
                                                              group cam by cam.CashAdvance into g
                                                              select new { CashAdvance = g.Key };

                                if (_cashAdvanceMonitorings != null && _cashAdvanceMonitorings.Count() > 0)
                                {
                                    List<string> _stringCAM = new List<string>();

                                    foreach (var _cashAdvanceMonitoring in _cashAdvanceMonitorings)
                                    {
                                        if (_cashAdvanceMonitoring != null)
                                        {
                                            if (_cashAdvanceMonitoring.CashAdvance.Code != null)
                                            {
                                                _stringCAM.Add(_cashAdvanceMonitoring.CashAdvance.Code);
                                            }
                                        }
                                    }

                                    IEnumerable<string> _stringArrayCAMDistinct = _stringCAM.Distinct();
                                    string[] _stringArrayCAMList = _stringArrayCAMDistinct.ToArray();
                                    if (_stringArrayCAMList.Length == 1)
                                    {
                                        for (int i = 0; i < _stringArrayCAMList.Length; i++)
                                        {
                                            if (i == 0)
                                            {
                                                _beginString = "[Code]=='" + _stringArrayCAMList[i] + "'";
                                            }
                                        }
                                    }
                                    else if (_stringArrayCAMList.Length > 1)
                                    {
                                        for (int i = 0; i < _stringArrayCAMList.Length; i++)
                                        {
                                            if (i == 0)
                                            {
                                                _beginString = "[Code]=='" + _stringArrayCAMList[i] + "'";
                                            }
                                            else
                                            {
                                                _endString = _endString + " OR [Code]=='" + _stringArrayCAMList[i] + "'";
                                            }
                                        }
                                    }
                                    _fullString = _beginString + _endString;
                                }
                            }
                            #endregion Organization 2
                            #region Organization 3
                            else if (this.DivisionCA != null && this.DepartmentCA != null && this.SectionCA == null && this.EmployeeCA == null)
                            {
                                XPQuery<CashAdvanceMonitoring> _cashAdvanceMonitoringsQuery = new XPQuery<CashAdvanceMonitoring>(Session);

                                var _cashAdvanceMonitorings = from cam in _cashAdvanceMonitoringsQuery
                                                              where ((cam.Status == Status.Open || cam.Status == Status.Posted)
                                                              && cam.Company == this.Company
                                                              && cam.Workplace == this.Workplace
                                                              && cam.Division == this.DivisionCA
                                                              && cam.Department == this.DepartmentCA
                                                              && cam.PaymentRealization == null)
                                                              group cam by cam.CashAdvance into g
                                                              select new { CashAdvance = g.Key };

                                if (_cashAdvanceMonitorings != null && _cashAdvanceMonitorings.Count() > 0)
                                {
                                    List<string> _stringCAM = new List<string>();

                                    foreach (var _cashAdvanceMonitoring in _cashAdvanceMonitorings)
                                    {
                                        if (_cashAdvanceMonitoring != null)
                                        {
                                            if (_cashAdvanceMonitoring.CashAdvance.Code != null)
                                            {
                                                _stringCAM.Add(_cashAdvanceMonitoring.CashAdvance.Code);
                                            }
                                        }
                                    }

                                    IEnumerable<string> _stringArrayCAMDistinct = _stringCAM.Distinct();
                                    string[] _stringArrayCAMList = _stringArrayCAMDistinct.ToArray();
                                    if (_stringArrayCAMList.Length == 1)
                                    {
                                        for (int i = 0; i < _stringArrayCAMList.Length; i++)
                                        {
                                            if (i == 0)
                                            {
                                                _beginString = "[Code]=='" + _stringArrayCAMList[i] + "'";
                                            }
                                        }
                                    }
                                    else if (_stringArrayCAMList.Length > 1)
                                    {
                                        for (int i = 0; i < _stringArrayCAMList.Length; i++)
                                        {
                                            if (i == 0)
                                            {
                                                _beginString = "[Code]=='" + _stringArrayCAMList[i] + "'";
                                            }
                                            else
                                            {
                                                _endString = _endString + " OR [Code]=='" + _stringArrayCAMList[i] + "'";
                                            }
                                        }
                                    }
                                    _fullString = _beginString + _endString;
                                }
                            }
                            #endregion Organization 3
                            #region Organization 4
                            else if (this.DivisionCA != null && this.DepartmentCA == null && this.SectionCA == null && this.EmployeeCA == null)
                            {
                                XPQuery<CashAdvanceMonitoring> _cashAdvanceMonitoringsQuery = new XPQuery<CashAdvanceMonitoring>(Session);

                                var _cashAdvanceMonitorings = from cam in _cashAdvanceMonitoringsQuery
                                                              where ((cam.Status == Status.Open || cam.Status == Status.Posted)
                                                              && cam.Company == this.Company
                                                              && cam.Workplace == this.Workplace
                                                              && cam.Division == this.DivisionCA
                                                              && cam.PaymentRealization == null)
                                                              group cam by cam.CashAdvance into g
                                                              select new { CashAdvance = g.Key };

                                if (_cashAdvanceMonitorings != null && _cashAdvanceMonitorings.Count() > 0)
                                {
                                    List<string> _stringCAM = new List<string>();

                                    foreach (var _cashAdvanceMonitoring in _cashAdvanceMonitorings)
                                    {
                                        if (_cashAdvanceMonitoring != null)
                                        {
                                            if (_cashAdvanceMonitoring.CashAdvance.Code != null)
                                            {
                                                _stringCAM.Add(_cashAdvanceMonitoring.CashAdvance.Code);
                                            }
                                        }
                                    }

                                    IEnumerable<string> _stringArrayCAMDistinct = _stringCAM.Distinct();
                                    string[] _stringArrayCAMList = _stringArrayCAMDistinct.ToArray();
                                    if (_stringArrayCAMList.Length == 1)
                                    {
                                        for (int i = 0; i < _stringArrayCAMList.Length; i++)
                                        {
                                            if (i == 0)
                                            {
                                                _beginString = "[Code]=='" + _stringArrayCAMList[i] + "'";
                                            }
                                        }
                                    }
                                    else if (_stringArrayCAMList.Length > 1)
                                    {
                                        for (int i = 0; i < _stringArrayCAMList.Length; i++)
                                        {
                                            if (i == 0)
                                            {
                                                _beginString = "[Code]=='" + _stringArrayCAMList[i] + "'";
                                            }
                                            else
                                            {
                                                _endString = _endString + " OR [Code]=='" + _stringArrayCAMList[i] + "'";
                                            }
                                        }
                                    }
                                    _fullString = _beginString + _endString;
                                }
                            }
                            #endregion Organization 4
                            #region Organization 5
                            else
                            {
                                XPQuery<CashAdvanceMonitoring> _cashAdvanceMonitoringsQuery = new XPQuery<CashAdvanceMonitoring>(Session);

                                var _cashAdvanceMonitorings = from cam in _cashAdvanceMonitoringsQuery
                                                              where ((cam.Status == Status.Open || cam.Status == Status.Posted)
                                                              && cam.Company == this.Company
                                                              && cam.Workplace == this.Workplace
                                                              && cam.PaymentRealization == null)
                                                              group cam by cam.CashAdvance into g
                                                              select new { CashAdvance = g.Key };

                                if (_cashAdvanceMonitorings != null && _cashAdvanceMonitorings.Count() > 0)
                                {
                                    List<string> _stringCAM = new List<string>();

                                    foreach (var _cashAdvanceMonitoring in _cashAdvanceMonitorings)
                                    {
                                        if (_cashAdvanceMonitoring != null)
                                        {
                                            if (_cashAdvanceMonitoring.CashAdvance.Code != null)
                                            {
                                                _stringCAM.Add(_cashAdvanceMonitoring.CashAdvance.Code);
                                            }
                                        }
                                    }

                                    IEnumerable<string> _stringArrayCAMDistinct = _stringCAM.Distinct();
                                    string[] _stringArrayCAMList = _stringArrayCAMDistinct.ToArray();
                                    if (_stringArrayCAMList.Length == 1)
                                    {
                                        for (int i = 0; i < _stringArrayCAMList.Length; i++)
                                        {
                                            if (i == 0)
                                            {
                                                _beginString = "[Code]=='" + _stringArrayCAMList[i] + "'";
                                            }
                                        }
                                    }
                                    else if (_stringArrayCAMList.Length > 1)
                                    {
                                        for (int i = 0; i < _stringArrayCAMList.Length; i++)
                                        {
                                            if (i == 0)
                                            {
                                                _beginString = "[Code]=='" + _stringArrayCAMList[i] + "'";
                                            }
                                            else
                                            {
                                                _endString = _endString + " OR [Code]=='" + _stringArrayCAMList[i] + "'";
                                            }
                                        }
                                    }
                                    _fullString = _beginString + _endString;
                                }
                            }
                            #endregion Organization 5
                            
                            if (_fullString != null)
                            {
                                _availableCashAdvance = new XPCollection<CashAdvance>(Session, CriteriaOperator.Parse(_fullString));
                            }
                        }
                    }
                }

                return _availableCashAdvance;
            }
        }

        [ImmediatePostData()]
        [DataSourceProperty("AvailableCashAdvance", DataSourcePropertyIsNullMode.SelectNothing)]
        public CashAdvance CashAdvance
        {
            get { return _cashAdvance; }
            set { SetPropertyValue("CashAdvance", ref _cashAdvance, value); }
        }

        [ImmediatePostData()]
        [Appearance("CashAdvanceCollectionPaymentRealizationClose", Enabled = false)]
        [Association("PaymentRealization-CashAdvanceCollections")]
        public PaymentRealization PaymentRealization
        {
            get { return _paymentRealization; }
            set
            {
                SetPropertyValue("PaymentRealization", ref _paymentRealization, value);
                if (!IsLoading)
                {
                    if (this._paymentRealization != null)
                    {
                        if (this._paymentRealization.Company != null)
                        {
                            this.Company = this._paymentRealization.Company;
                        }
                        if (this._paymentRealization.Workplace != null)
                        {
                            this.Workplace = this._paymentRealization.Workplace;
                        }
                        if (this._paymentRealization.Division != null)
                        {
                            this.Division = this._paymentRealization.Division;
                        }
                        if (this._paymentRealization.Department != null)
                        {
                            this.Department = this._paymentRealization.Department;
                        }
                        if (this._paymentRealization.Section != null)
                        {
                            this.Section = this._paymentRealization.Section;
                        }
                        if (this._paymentRealization.Employee != null)
                        {
                            this.Employee = this._paymentRealization.Employee;
                        }
                        if (this._paymentRealization.DivisionCA != null)
                        {
                            this.DivisionCA = this._paymentRealization.DivisionCA;
                        }
                        if (this._paymentRealization.DepartmentCA != null)
                        {
                            this.DepartmentCA = this._paymentRealization.DepartmentCA;
                        }
                        if (this._paymentRealization.SectionCA != null)
                        {
                            this.SectionCA = this._paymentRealization.SectionCA;
                        }
                        if (this._paymentRealization.EmployeeCA != null)
                        {
                            this.EmployeeCA = this._paymentRealization.EmployeeCA;
                        }
                    }
                }
            }
        }

        [Appearance("CashAdvanceCollectionStatusClose", Enabled = false)]
        public Status Status
        {
            get { return _status; }
            set { SetPropertyValue("Status", ref _status, value); }
        }

        [ModelDefault("DisplayFormat", "{0:dd/MM/yyyy}")]
        [ModelDefault("EditMask", "dd/MM/yyyy")]
        [Appearance("CashAdvanceCollectionStatusDateClose", Enabled = false)]
        public DateTime StatusDate
        {
            get { return _statusDate; }
            set { SetPropertyValue("StatusDate", ref _statusDate, value); }
        }

        [Browsable(false)]
        [Appearance("CashAdvanceCollectionPostedCountEnabled", Enabled = false)]
        public int PostedCount
        {
            get { return _postedCount; }
            set { SetPropertyValue("PostedCount", ref _postedCount, value); }
        }

        //AuditTrail
        [CollectionOperationSet(AllowAdd = false, AllowRemove = true)]
        public XPCollection<AuditDataItemPersistent> ChangeHistory
        {
            get
            {
                if (changeHistory == null)
                {
                    changeHistory = AuditedObjectWeakReference.GetAuditTrail(Session, this);
                }
                return changeHistory;
            }
        }

        #endregion Field

    }
}