﻿using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;

using DevExpress.ExpressApp.Editors;
using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.ConditionalAppearance;

namespace FullDrive.Module.BusinessObjects
{
    [DefaultClassOptions]
    [DefaultProperty("Code")]
    [NavigationItem("Sales")]
    [RuleCombinationOfPropertiesIsUnique("CustomerOrderFreeItemRuleUnique", DefaultContexts.Save, "Code")]
    //[ImageName("BO_Contact")]
    //[DefaultProperty("DisplayMemberNameForLookupEditorsOfThisType")]
    //[DefaultListViewOptions(MasterDetailMode.ListViewOnly, false, NewItemRowPosition.None)]
    //[Persistent("DatabaseTableName")]
    // Specify more UI options using a declarative approach (https://documentation.devexpress.com/#eXpressAppFramework/CustomDocument112701).
    public class CustomerOrderFreeItem : BaseObject
    { // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).
        private bool _activationPosting;
        private int _no;
        private string _code;
        #region InitialOrganization
        private Company _company;
        private Workplace _workplace;
        private Division _division;
        private Department _department;
        private Section _section;
        private Employee _employee;
        #endregion InitialOrganization
        private XPCollection<Location> _availableLocation;
        private Location _location;
        private BeginningInventory _begInv;
        private CustomerOrderLine _customerOrderLine;
        private FreeItemRule _freeItemRule;
        #region InisialisasiFPQty
        private LocationType _fpLocationType;
        private StockType _fpStockType;
        private StockGroup _fpStockGroup;
        private Item _fpItem;
        private XPCollection<UnitOfMeasure> _availableUnitOfMeasureFP;
        private double _fpDQty;
        private UnitOfMeasure _fpDUom;
        private double _fpQty;
        private UnitOfMeasure _fpUom;
        private double _fpTQty;
        private BeginningInventoryLine _begInvLine;
        #endregion InisialisasiCPQty
        #region InitialAmount
        private Currency _currency;
        private PriceGroup _priceGroup;
        private PriceLine _priceLine;
        private double _uAmount;
        private double _tUAmount;
        #endregion InitialAmount
        private Status _status;
        private DateTime _statusDate;
        private CustomerOrder _customerOrder;
        private string _localUserAccess;
        private GlobalFunction _globFunc;
        //AuditTrail
        private XPCollection<AuditDataItemPersistent> changeHistory;

        public CustomerOrderFreeItem(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if (!IsLoading)
            {
                if (Session.IsNewObject(this))
                {
                    _globFunc = new GlobalFunction();
                    DateTime now = DateTime.Now;
                    #region UserAccess
                    _localUserAccess = SecuritySystem.CurrentUserName;
                    UserAccess _locUserAccess = Session.FindObject<UserAccess>(new BinaryOperator("UserName", _localUserAccess));
                    if (_locUserAccess != null)
                    {
                        if (_locUserAccess.Employee != null)
                        {
                            if (_locUserAccess.Employee.Company != null && _locUserAccess.Employee.Workplace != null)
                            {
                                this.Code = _globFunc.GetNumberingUnlockOptimisticRecordBasedOrganization(this.Session.DataLayer, ObjectList.CustomerOrderFreeItem, _locUserAccess.Employee.Company, _locUserAccess.Employee.Workplace);
                            }
                            else
                            {
                                this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.CustomerOrderFreeItem);
                            }
                        }
                    }
                    #endregion UserAccess
                    this.Status = CustomProcess.Status.Open;
                    this.StatusDate = now;
                    this.ActivationPosting = true;
                    this.FpLocationType = LocationType.Main;
                    this.FpStockType = StockType.Good;
                    this.FpStockGroup = StockGroup.FreeItem;
                }
            }
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).
        }

        protected override void OnSaving()
        {
            base.OnSaving();
            if (!IsLoading && IsSaving)
            {
                UpdateNo();
            }
        }

        protected override void OnDeleting()
        {
            base.OnDeleting();
            if (!IsLoading)
            {
                RecoveryDeleteNo();
                RollBackQtyOnHand();
            }
        }

        #region Field

        [Browsable(false)]
        public bool ActivationPosting
        {
            get { return _activationPosting; }
            set { SetPropertyValue("ActivationPosting", ref _activationPosting, value); }
        }

        [Appearance("CustomerOrderFreeItemNoEnabled", Enabled = false)]
        public int No
        {
            get { return _no; }
            set { SetPropertyValue("No", ref _no, value); }
        }

        [VisibleInListView(false)]
        [RuleRequiredField(DefaultContexts.Save)]
        [Appearance("CustomerOrderFreeItemCodeClose", Enabled = false)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue("Code", ref _code, value); }
        }

        #region Organization

        [VisibleInListView(false)]
        [Appearance("CustomerOrderFreeItemCompanyClose", Enabled = false)]
        public Company Company
        {
            get { return _company; }
            set { SetPropertyValue("Company", ref _company, value); }
        }

        [VisibleInListView(false)]
        [Appearance("CustomerOrderFreeItemWorkplaceClose", Enabled = false)]
        public Workplace Workplace
        {
            get { return _workplace; }
            set { SetPropertyValue("Workplace", ref _workplace, value); }
        }

        [Browsable(false)]
        public Division Division
        {
            get { return _division; }
            set { SetPropertyValue("Division", ref _division, value); }
        }

        [Browsable(false)]
        public Department Department
        {
            get { return _department; }
            set { SetPropertyValue("Department", ref _department, value); }
        }

        [Browsable(false)]
        public Section Section
        {
            get { return _section; }
            set { SetPropertyValue("Section", ref _section, value); }
        }

        [Browsable(false)]
        [ImmediatePostData()]
        public Employee Employee
        {
            get { return _employee; }
            set
            {
                SetPropertyValue("Employee", ref _employee, value);
                if (!IsLoading)
                {
                    if (this._employee != null)
                    {
                        SetLocation(_employee);
                    }
                    else
                    {
                        this.Location = null;
                    }
                }
            }
        }

        #endregion Organization

        [Browsable(false)]
        public XPCollection<Location> AvailableLocation
        {
            get
            {
                if(!IsLoading)
                {
                    string _beginString = null;
                    string _endString = null;
                    string _fullString = null;

                    if (this.Company != null && this.Workplace != null && this.Employee != null)
                    {
                        List<string> _stringSLS = new List<string>();

                        XPCollection<SalesmanLocationSetup> _locSalesmanLocationSetups = new XPCollection<SalesmanLocationSetup>
                                                                                  (Session, new GroupOperator(GroupOperatorType.And,
                                                                                   new BinaryOperator("Company", this.Company),
                                                                                   new BinaryOperator("Workplace", this.Workplace),
                                                                                   new BinaryOperator("Salesman", this.Employee),
                                                                                   new BinaryOperator("Active", true)));

                        if (_locSalesmanLocationSetups != null && _locSalesmanLocationSetups.Count() > 0)
                        {
                            foreach (SalesmanLocationSetup _locSalesmanLocationSetup in _locSalesmanLocationSetups)
                            {
                                if (_locSalesmanLocationSetup.Location != null)
                                {
                                    if (_locSalesmanLocationSetup.Location.Code != null)
                                    {
                                        _stringSLS.Add(_locSalesmanLocationSetup.Location.Code);
                                    }
                                }
                            }
                        }

                        IEnumerable<string> _stringArraySLSDistinct = _stringSLS.Distinct();
                        string[] _stringArraySLSList = _stringArraySLSDistinct.ToArray();
                        if (_stringArraySLSList.Length == 1)
                        {
                            for (int i = 0; i < _stringArraySLSList.Length; i++)
                            {
                                Location _locLoc = Session.FindObject<Location>(new BinaryOperator("Code", _stringArraySLSList[i]));
                                if (_locLoc != null)
                                {
                                    if (i == 0)
                                    {
                                        _beginString = "[Code]=='" + _locLoc.Code + "'";
                                    }
                                }
                            }
                        }
                        else if (_stringArraySLSList.Length > 1)
                        {
                            for (int i = 0; i < _stringArraySLSList.Length; i++)
                            {
                                Location _locLoc = Session.FindObject<Location>(new BinaryOperator("Code", _stringArraySLSList[i]));
                                if (_locLoc != null)
                                {
                                    if (i == 0)
                                    {
                                        _beginString = "[Code]=='" + _locLoc.Code + "'";
                                    }
                                    else
                                    {
                                        _endString = _endString + " OR [Code]=='" + _locLoc.Code + "'";
                                    }
                                }
                            }
                        }
                        _fullString = _beginString + _endString;

                        if (_fullString != null)
                        {
                            _availableLocation = new XPCollection<Location>(Session, CriteriaOperator.Parse(_fullString));
                        }

                    }
                    else
                    {
                        if (this.Company != null && this.Workplace != null)
                        {
                            _availableLocation = new XPCollection<Location>(Session, new GroupOperator(GroupOperatorType.And,
                                                                                new BinaryOperator("Company", this.Company),
                                                                                new BinaryOperator("Workplace", this.Workplace),
                                                                                new BinaryOperator("Active", true)));
                        }
                    }
                }
                return _availableLocation;
            }
        }

        [Browsable(false)]
        [ImmediatePostData()]
        [DataSourceProperty("AvailableLocation")]
        [Appearance("CustomerOrderFreeItemLocationClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Location Location
        {
            get { return _location; }
            set
            {
                SetPropertyValue("Location", ref _location, value);
                if (!IsLoading)
                {
                    if (this._location != null)
                    {
                        SetBegInv(_location);
                    }
                    else
                    {
                        BegInv = null;
                    }
                }
            }
        }

        [Browsable(false)]
        [ImmediatePostData()]
        [Appearance("CustomerOrderFreeItemBegInvClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public BeginningInventory BegInv
        {
            get { return _begInv; }
            set { SetPropertyValue("BegInv", ref _begInv, value); }
        }

        [Appearance("CustomerOrderFreeItemCustomerOrderLineClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public CustomerOrderLine CustomerOrderLine
        {
            get { return _customerOrderLine; }
            set { SetPropertyValue("CustomerOrderLine", ref _customerOrderLine, value); }
        }

        [Appearance("CustomerOrderFreeItemFreeItemRuleClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public FreeItemRule FreeItemRule
        {
            get { return _freeItemRule; }
            set { SetPropertyValue("FreeItemRule", ref _freeItemRule, value); }
        }

        #region FpQty

        [VisibleInListView(false)]
        [Appearance("CustomerOrderFreeItemFpLocationTypeClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public LocationType FpLocationType
        {
            get { return _fpLocationType; }
            set { SetPropertyValue("FpLocationType", ref _fpLocationType, value); }
        }

        [VisibleInListView(false)]
        [Appearance("CustomerOrderFreeItemFpStockTypeClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public StockType FpStockType
        {
            get { return _fpStockType; }
            set { SetPropertyValue("FpStockType", ref _fpStockType, value); }
        }

        [VisibleInListView(false)]
        [Appearance("CustomerOrderFreeItemFpStockGroupClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public StockGroup FpStockGroup
        {
            get { return _fpStockGroup; }
            set { SetPropertyValue("FpStockGroup", ref _fpStockGroup, value); }
        }

        [ImmediatePostData()]
        [Appearance("CustomerOrderFreeItemFpItemClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Item FpItem
        {
            get { return _fpItem; }
            set
            {
                SetPropertyValue("FpItem", ref _fpItem, value);
                if (!IsLoading)
                {
                    if (this._fpItem != null)
                    {
                        if (this._fpItem.BasedUOM != null)
                        {
                            this.FpDUOM = this._fpItem.BasedUOM;
                        }
                        SetPriceLine();
                        SetBegInvLine(this._fpItem);
                    }
                    else
                    {
                        this.FpDUOM = null;
                        this.BegInvLine = null;
                    }
                }
            }
        }

        [Browsable(false)]
        [ImmediatePostData()]
        [Appearance("CustomerOrderFreeItemBegInvLineClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public BeginningInventoryLine BegInvLine
        {
            get { return _begInvLine; }
            set { SetPropertyValue("BegInvLine", ref _begInvLine, value); }
        }

        [Browsable(false)]
        public XPCollection<UnitOfMeasure> AvailableUnitOfMeasureFP
        {
            get
            {
                if(!IsLoading)
                {
                    if (FpItem == null)
                    {
                        _availableUnitOfMeasureFP = new XPCollection<UnitOfMeasure>(Session);
                    }
                    else
                    {
                        if (GetUnitOfMeasureCollection(this.FpItem) != null)
                        {
                            _availableUnitOfMeasureFP = new XPCollection<UnitOfMeasure>(Session, CriteriaOperator.Parse(GetUnitOfMeasureCollection(this.FpItem)));
                        }

                    }
                }               
                return _availableUnitOfMeasureFP;
            }
        }

        [Appearance("CustomerOrderFreeItemFpDQtyClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [ImmediatePostData()]
        public double FpDQty
        {
            get { return _fpDQty; }
            set
            {
                SetPropertyValue("FpDQty", ref _fpDQty, value);
                if (!IsLoading)
                {
                    SetFpTotalQty();
                    SetTotalUnitAmount();
                }
            }
        }

        [Appearance("CustomerOrderFreeItemFpDUOMClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [ImmediatePostData()]
        [DataSourceCriteria("Active = true")]
        public UnitOfMeasure FpDUOM
        {
            get { return _fpDUom; }
            set
            {
                SetPropertyValue("FpDUOM", ref _fpDUom, value);
                if (!IsLoading)
                {
                    SetFpTotalQty();
                    SetTotalUnitAmount();
                }
            }
        }

        [Appearance("CustomerOrderFreeItemFpQtyClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [ImmediatePostData()]
        public double FpQty
        {
            get { return _fpQty; }
            set
            {
                SetPropertyValue("FpQty", ref _fpQty, value);
                if (!IsLoading)
                {
                    SetFpTotalQty();
                    SetTotalUnitAmount();
                }
            }
        }

        [ImmediatePostData()]
        [Appearance("CustomerOrderFreeItemFpUOMClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [DataSourceProperty("AvailableUnitOfMeasureFP", DataSourcePropertyIsNullMode.SelectAll)]
        [DataSourceCriteria("Active = true")]
        public UnitOfMeasure FpUOM
        {
            get { return _fpUom; }
            set
            {
                SetPropertyValue("FpUOM", ref _fpUom, value);
                if (!IsLoading)
                {
                    SetFpTotalQty();
                    SetTotalUnitAmount();
                }
            }
        }

        [Appearance("CustomerOrderFreeItemFpTQtyEnabled", Enabled = false)]
        public double FpTQty
        {
            get { return _fpTQty; }
            set { SetPropertyValue("FpTQty", ref _fpTQty, value); }
        }

        [VisibleInListView(false)]
        [Persistent]
        public string FullName
        {
            get
            {
                if (this.FpDQty > 0 && this.FpQty > 0)
                {
                    return String.Format("{0} {1} {2} {3}", this.FpQty, this.FpUOM.Code, this.FpDQty, this.FpDUOM.Code);
                }
                else if (this.FpDQty > 0 && this.FpQty == 0)
                {
                    return String.Format("{0} {1}", this.FpDQty, this.FpDUOM.Code);
                }
                else if (this.FpDQty == 0 && this.FpQty > 0)
                {
                    return String.Format("{0} {1}", this.FpQty, this.FpUOM.Code);
                }
                return null;
            }
        }

        #endregion FpQty

        #region Amount

        [VisibleInListView(false)]
        [Appearance("CustomerOrderFreeItemCurrencyClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Currency Currency
        {
            get { return _currency; }
            set { SetPropertyValue("Currency", ref _currency, value); }
        }

        [VisibleInListView(false)]
        [ImmediatePostData()]
        [Appearance("CustomerOrderFreeItemPriceGroupClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public PriceGroup PriceGroup
        {
            get { return _priceGroup; }
            set
            {
                SetPropertyValue("PriceGroup", ref _priceGroup, value);
                if (!IsLoading)
                {
                    if (this._priceGroup != null)
                    {
                        SetPriceLine();
                    }
                }
            }
        }

        [Browsable(false)]
        [DataSourceCriteria("Active = true"), ImmediatePostData()]
        [Appearance("CustomerOrderFreeItemPriceLineClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public PriceLine PriceLine
        {
            get { return _priceLine; }
            set
            {
                SetPropertyValue("PriceLine", ref _priceLine, value);
                if (!IsLoading)
                {
                    if (this._priceLine != null)
                    {
                        this.UAmount = this._priceLine.Amount2a;
                    }
                }
            }
        }

        [Appearance("CustomerOrderFreeItemUAmountClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [ImmediatePostData()]
        public double UAmount
        {
            get { return _uAmount; }
            set
            {
                SetPropertyValue("UAmount", ref _uAmount, value);
                if (!IsLoading)
                {
                    _globFunc = new GlobalFunction();
                    if (this._uAmount > 0)
                    {
                        if (_globFunc.GetRoundingList(Session, ObjectList.SalesOrderFreeItem, FieldName.UAmount) == true)
                        {
                            this._uAmount = _globFunc.GetRoundUp(Session, this._uAmount, ObjectList.SalesOrderFreeItem, FieldName.UAmount);
                        }
                    }
                    SetTotalUnitAmount();
                }
            }
        }

        [Appearance("CustomerOrderFreeItemTUAmountEnabled", Enabled = false)]
        [ImmediatePostData()]
        public double TUAmount
        {
            get { return _tUAmount; }
            set { SetPropertyValue("TUAmount", ref _tUAmount, value); }
        }

        #endregion Amount

        [VisibleInListView(false)]
        [Appearance("CustomerOrderFreeItemStatusClose", Enabled = false)]
        public Status Status
        {
            get { return _status; }
            set { SetPropertyValue("Status", ref _status, value); }
        }

        [VisibleInListView(false)]
        [ModelDefault("DisplayFormat", "{0:dd/MM/yyyy}")]
        [ModelDefault("EditMask", "dd/MM/yyyy")]
        [Appearance("CustomerOrderFreeItemStatusDateClose", Enabled = false)]
        public DateTime StatusDate
        {
            get { return _statusDate; }
            set { SetPropertyValue("StatusDate", ref _statusDate, value); }
        }

        [VisibleInListView(false)]
        [ImmediatePostData()]
        [RuleRequiredField(DefaultContexts.Save)]
        [Appearance("CustomerOrderFreeItemCustomerOrderClose", Enabled = false)]
        [Association("CustomerOrder-CustomerOrderFreeItems")]
        public CustomerOrder CustomerOrder
        {
            get { return _customerOrder; }
            set
            {
                SetPropertyValue("CustomerOrder", ref _customerOrder, value);
                if (!IsLoading)
                {

                    if (this._customerOrder != null)
                    {
                        if (Session.IsNewObject(this))
                        {
                            if (this._customerOrder.Company != null)
                            {
                                this.Company = this._customerOrder.Company;
                            }
                            if (this._customerOrder.Workplace != null)
                            {
                                this.Workplace = this._customerOrder.Workplace;
                            }
                            if (this._customerOrder.Division != null)
                            {
                                this.Division = this._customerOrder.Division;
                            }
                            if (this._customerOrder.Department != null)
                            {
                                this.Department = this._customerOrder.Department;
                            }
                            if (this._customerOrder.Section != null)
                            {
                                this.Section = this._customerOrder.Section;
                            }
                            if (this._customerOrder.Employee != null)
                            {
                                this.Employee = this._customerOrder.Employee;
                            }
                            if (this._customerOrder.Location != null)
                            {
                                this.Location = this._customerOrder.Location;
                            }
                            if (this._customerOrder.BegInv != null)
                            {
                                this.BegInv = this._customerOrder.BegInv;
                            }
                            if (this._customerOrder.PriceGroup != null)
                            {
                                this.PriceGroup = this._customerOrder.PriceGroup;
                            }
                            if (this._customerOrder.Currency != null)
                            {
                                this.Currency = this._customerOrder.Currency;
                            }
                        }
                    }
                }
            }
        }

        //AuditTrail
        [CollectionOperationSet(AllowAdd = false, AllowRemove = true)]
        public XPCollection<AuditDataItemPersistent> ChangeHistory
        {
            get
            {
                if (changeHistory == null)
                {
                    changeHistory = AuditedObjectWeakReference.GetAuditTrail(Session, this);
                }
                return changeHistory;
            }
        }

        #endregion Field

        //================================ Code Only ================================

        #region CodeOnly

        #region No

        public void UpdateNo()
        {
            try
            {
                if (!IsLoading && Session.IsNewObject(this))
                {
                    if (this.CustomerOrder != null)
                    {
                        object _makRecord = Session.Evaluate<CustomerOrderFreeItem>(CriteriaOperator.Parse("Max(No)"), CriteriaOperator.Parse("CustomerOrder=?", this.CustomerOrder));
                        this.No = Convert.ToInt32(_makRecord) + 1;
                        this.Save();
                        RecoveryUpdateNo();
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = CustomerOrderFreeItem " + ex.ToString());
            }
        }

        public void RecoveryUpdateNo()
        {
            try
            {
                if (this.CustomerOrder != null)
                {
                    CustomerOrder _numHeader = Session.FindObject<CustomerOrder>
                                            (new BinaryOperator("Code", this.CustomerOrder.Code));

                    XPCollection<CustomerOrderFreeItem> _numLines = new XPCollection<CustomerOrderFreeItem>
                                                             (Session, new BinaryOperator("CustomerOrder", _numHeader),
                                                              new SortProperty("No", DevExpress.Xpo.DB.SortingDirection.Ascending));
                    if (_numLines != null)
                    {
                        int i1 = 0;
                        foreach (CustomerOrderFreeItem _numLine in _numLines)
                        {
                            i1 += 1;
                            _numLine.No = i1;
                            _numLine.Save();
                        }
                        i1 = 1;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = CustomerOrderFreeItem " + ex.ToString());
            }
        }

        public void RecoveryDeleteNo()
        {
            try
            {
                if (this.CustomerOrder != null)
                {
                    CustomerOrder _numHeader = Session.FindObject<CustomerOrder>
                                            (new BinaryOperator("Code", this.CustomerOrder.Code));

                    XPCollection<CustomerOrderFreeItem> _numLines = new XPCollection<CustomerOrderFreeItem>
                                                             (Session, new GroupOperator(GroupOperatorType.And,
                                                              new BinaryOperator("This", this, BinaryOperatorType.NotEqual),
                                                              new BinaryOperator("CustomerOrder", _numHeader)),
                                                              new SortProperty("No", DevExpress.Xpo.DB.SortingDirection.Ascending));
                    if (_numLines != null)
                    {
                        int i = 0;
                        foreach (CustomerOrderFreeItem _numLine in _numLines)
                        {
                            i += 1;
                            _numLine.No = i;
                            _numLine.Save();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = CustomerOrderFreeItem " + ex.ToString());
            }
        }

        #endregion No

        private string GetUnitOfMeasureCollection(Item _locItem)
        {
            string _result = null;
            try
            {
                string _beginString = null;
                string _endString = null;

                List<string> _stringUOM = new List<string>();

                XPCollection<ItemUnitOfMeasure> _locItemUnitOfMeasures = new XPCollection<ItemUnitOfMeasure>
                                                                          (Session, new GroupOperator(GroupOperatorType.And,
                                                                           new BinaryOperator("Item", _locItem),
                                                                           new BinaryOperator("Active", true)));

                if (_locItemUnitOfMeasures != null && _locItemUnitOfMeasures.Count() > 0)
                {
                    foreach (ItemUnitOfMeasure _locItemUnitOfMeasure in _locItemUnitOfMeasures)
                    {
                        _stringUOM.Add(_locItemUnitOfMeasure.UOM.Code);
                    }
                }

                IEnumerable<string> _stringArrayUOMDistinct = _stringUOM.Distinct();
                string[] _stringArrayUOMList = _stringArrayUOMDistinct.ToArray();
                if (_stringArrayUOMList.Length == 1)
                {
                    for (int i = 0; i < _stringArrayUOMList.Length; i++)
                    {
                        UnitOfMeasure _locUOM = Session.FindObject<UnitOfMeasure>(new BinaryOperator("Code", _stringArrayUOMList[i]));
                        if (_locUOM != null)
                        {
                            if (i == 0)
                            {
                                _beginString = "[Code]=='" + _locUOM.Code + "'";
                            }
                        }
                    }
                }
                else if (_stringArrayUOMList.Length > 1)
                {
                    for (int i = 0; i < _stringArrayUOMList.Length; i++)
                    {
                        UnitOfMeasure _locUOM = Session.FindObject<UnitOfMeasure>(new BinaryOperator("Code", _stringArrayUOMList[i]));
                        if (_locUOM != null)
                        {
                            if (i == 0)
                            {
                                _beginString = "[Code]=='" + _locUOM.Code + "'";
                            }
                            else
                            {
                                _endString = _endString + " OR [Code]=='" + _locUOM.Code + "'";
                            }
                        }
                    }
                }

                _result = _beginString + _endString;

            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = CustomerOrderFreeItem " + ex.ToString());
            }

            return _result;
        }

        private void SetFpTotalQty()
        {
            try
            {
                double _locInvLineTotal = 0;
                DateTime now = DateTime.Now;
                if (this.CustomerOrder != null)
                {
                    if (this.FpItem != null && this.FpUOM != null && this.FpDUOM != null)
                    {
                        ItemUnitOfMeasure _locItemUOM = Session.FindObject<ItemUnitOfMeasure>
                                                        (new GroupOperator(GroupOperatorType.And,
                                                         new BinaryOperator("Item", this.FpItem),
                                                         new BinaryOperator("UOM", this.FpUOM),
                                                         new BinaryOperator("DefaultUOM", this.FpDUOM),
                                                         new BinaryOperator("Active", true)));
                        if (_locItemUOM != null)
                        {
                            if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                            {
                                _locInvLineTotal = this.FpQty * _locItemUOM.DefaultConversion + this.FpDQty;
                            }
                            else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                            {
                                _locInvLineTotal = this.FpQty / _locItemUOM.Conversion + this.FpDQty;
                            }
                            else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                            {
                                _locInvLineTotal = this.FpQty + this.FpDQty;
                            }

                            this.FpTQty = _locInvLineTotal;
                        }
                    }
                    else
                    {
                        _locInvLineTotal = this.FpQty + this.FpDQty;
                        this.FpTQty = _locInvLineTotal;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = CustomerOrderFreeItem " + ex.ToString());
            }
        }

        private void SetTotalUnitAmount()
        {
            try
            {
                _globFunc = new GlobalFunction();

                if (_fpTQty >= 0 & _uAmount >= 0)
                {
                    if (_globFunc.GetRoundingList(Session, ObjectList.CustomerOrderFreeItem, FieldName.TUAmount) == true)
                    {
                        this.TUAmount = _globFunc.GetRoundUp(Session, (this.FpTQty * this.UAmount), ObjectList.CustomerOrderFreeItem, FieldName.TUAmount);
                    }
                    else
                    {
                        this.TUAmount = this.FpTQty * this.UAmount;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = CustomerOrderFreeItem " + ex.ToString());
            }
        }

        private void SetPriceLine()
        {
            try
            {
                if (this.Company != null && this.Workplace != null && this.FpItem != null && this.PriceGroup != null)
                {
                    PriceLine _locPriceLine = Session.FindObject<PriceLine>(new GroupOperator(GroupOperatorType.And,
                                                                            new BinaryOperator("Company", this.Company),
                                                                            new BinaryOperator("Workplace", this.Workplace),
                                                                            new BinaryOperator("Item", this.FpItem),
                                                                            new BinaryOperator("PriceGroup", this.PriceGroup),
                                                                            new BinaryOperator("Active", true)));
                    if (_locPriceLine != null)
                    {
                        this.PriceLine = _locPriceLine;
                        this.UAmount = _locPriceLine.Amount2a;
                    }
                    else
                    {
                        this.PriceLine = null;
                        this.UAmount = 0;
                    }
                }
                else
                {
                    this.PriceLine = null;
                    this.UAmount = 0;
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = CustomerOrderFreeItem " + ex.ToString());
            }
        }

        private void SetLocation(Employee _locEmployee)
        {
            try
            {
                DateTime now = DateTime.Now;
                if (this.Company != null && this.Workplace != null)
                {
                    SalesmanLocationSetup _locSalesmanLocationSetup = Session.FindObject<SalesmanLocationSetup>
                                                        (new GroupOperator(GroupOperatorType.And,
                                                        new BinaryOperator("Company", this.Company),
                                                        new BinaryOperator("Workplace", this.Workplace),
                                                        new BinaryOperator("Salesman", _locEmployee),
                                                        new BinaryOperator("Active", true),
                                                        new BinaryOperator("Default", true)));
                    if (_locSalesmanLocationSetup != null)
                    {
                        if (_locSalesmanLocationSetup.Location != null)
                        {
                            this.Location = _locSalesmanLocationSetup.Location;
                        }
                        else
                        {
                            this.Location = null;
                        }
                    }
                    else
                    {
                        this.Location = null;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Business Object = CustomerOrderFreeItem ", ex.ToString());
            }
        }

        private void SetBegInv(Location _locLocation)
        {
            try
            {
                DateTime now = DateTime.Now;
                if (this.Company != null && this.Workplace != null)
                {
                    BeginningInventory _locBegInv = Session.FindObject<BeginningInventory>
                                                        (new GroupOperator(GroupOperatorType.And,
                                                        new BinaryOperator("Company", this.Company),
                                                        new BinaryOperator("Workplace", this.Workplace),
                                                        new BinaryOperator("Location", _locLocation),
                                                        new BinaryOperator("LocationType", this.FpLocationType),
                                                        new BinaryOperator("StockType", this.FpStockType),
                                                        new BinaryOperator("Active", true)));
                    if (_locBegInv != null)
                    {
                        this.BegInv = _locBegInv;
                    }
                    else
                    {
                        this.BegInv = null;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Business Object = CustomerOrderFreeItem ", ex.ToString());
            }
        }

        private void SetBegInvLine(Item _locItem)
        {
            try
            {
                DateTime now = DateTime.Now;
                if (this.Company != null && this.Workplace != null && this.BegInv != null)
                {
                    BeginningInventoryLine _locBegInvLine = Session.FindObject<BeginningInventoryLine>(
                                                                            new GroupOperator(GroupOperatorType.And,
                                                                            new BinaryOperator("Company", this.Company),
                                                                            new BinaryOperator("Workplace", this.Workplace),
                                                                            new BinaryOperator("BeginningInventory", this.BegInv),
                                                                            new BinaryOperator("Item", _locItem),
                                                                            new BinaryOperator("LocationType", this.FpLocationType),
                                                                            new BinaryOperator("StockType", this.FpStockType),
                                                                            new BinaryOperator("StockGroup", this.FpStockGroup),
                                                                            new BinaryOperator("Lock", false),
                                                                            new BinaryOperator("Active", true)));
                    if (_locBegInvLine != null)
                    {
                        this.BegInvLine = _locBegInvLine;
                    }
                    else
                    {
                        this.BegInvLine = null;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Business Object = CustomerOrderFreeItem ", ex.ToString());
            }
        }

        #region RollBack

        public void RollBackQtyOnHand()
        {
            try
            {
                double _locQtyOnHand = 0;
                if (this.CustomerOrder != null)
                {
                    if (this.Status == Status.Progress || this.Status == Status.Posted || this.Status == Status.Close)
                    {
                        if (this.BegInvLine != null)
                        {
                            _locQtyOnHand = this.BegInvLine.QtyOnHand - this.FpTQty;
                            if (_locQtyOnHand < 0)
                            {
                                _locQtyOnHand = 0;
                            }
                            this.BegInvLine.QtyOnHand = _locQtyOnHand;
                            this.BegInvLine.Save();
                            this.BegInvLine.Session.CommitTransaction();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = CustomerOrderFreeItem " + ex.ToString());
            }
        }

        #endregion RollBack

        #endregion CodeOnly

    }
}