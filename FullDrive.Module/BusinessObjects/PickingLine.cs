﻿using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;

using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.ConditionalAppearance;

namespace FullDrive.Module.BusinessObjects
{
    [DefaultClassOptions]
    [DefaultProperty("Code")]
    [NavigationItem("Sales")]
    [RuleCombinationOfPropertiesIsUnique("PickingLineRuleUnique", DefaultContexts.Save, "Code")]
    //[ImageName("BO_Contact")]
    //[DefaultProperty("DisplayMemberNameForLookupEditorsOfThisType")]
    //[DefaultListViewOptions(MasterDetailMode.ListViewOnly, false, NewItemRowPosition.None)]
    //[Persistent("DatabaseTableName")]
    // Specify more UI options using a declarative approach (https://documentation.devexpress.com/#eXpressAppFramework/CustomDocument112701).
    public class PickingLine : BaseObject
    { // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).

        private bool _activationPosting;
        private int _no;
        private string _code;
        private bool _select;
        #region InitialOrganization
        private Company _company;
        private Workplace _workplace;
        private Division _division;
        private Department _department;
        private Section _section;
        private Employee _employee;
        private Division _div;
        private Department _dept;
        private Section _sect;
        private XPCollection<Employee> _availablePIC;
        private Employee _pic;
        #endregion InitialOrganization
        private XPCollection<SalesInvoice> _availableSalesInvoice;
        private SalesInvoice _salesInvoice;
        private BusinessPartner _customer;
        private string _address;
        private DateTime _pickingDate;
        private XPCollection<Vehicle> _availableVehicle;
        private Vehicle _vehicle;
        private BeginningInventory _begInv;
        private Status _status;
        private DateTime _statusDate;
        private Picking _picking;
        private string _localUserAccess;
        private GlobalFunction _globFunc;
        //AuditTrail
        private XPCollection<AuditDataItemPersistent> changeHistory;

        public PickingLine(Session session)
            : base(session)
        {
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if (!IsLoading)
            {
                if (Session.IsNewObject(this))
                {
                    _globFunc = new GlobalFunction();
                    DateTime now = DateTime.Now;
                    #region UserAccess
                    _localUserAccess = SecuritySystem.CurrentUserName;
                    UserAccess _locUserAccess = Session.FindObject<UserAccess>(new BinaryOperator("UserName", _localUserAccess));
                    if (_locUserAccess != null)
                    {
                        if (_locUserAccess.Employee != null)
                        {
                            if (_locUserAccess.Employee.Company != null && _locUserAccess.Employee.Workplace != null)
                            {
                                this.Code = _globFunc.GetNumberingUnlockOptimisticRecordBasedOrganization(this.Session.DataLayer, ObjectList.PickingLine, _locUserAccess.Employee.Company, _locUserAccess.Employee.Workplace);
                            }
                            else
                            {
                                this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.PickingLine);
                            }
                        }
                    }
                    #endregion UserAccess
                    this.Status = CustomProcess.Status.Open;
                    this.StatusDate = now;
                    this.Select = true;
                }
            }
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).
        }

        protected override void OnSaving()
        {
            base.OnSaving();
            if (!IsLoading && IsSaving)
            {
                UpdateNo();
            }
        }

        protected override void OnDeleting()
        {
            base.OnDeleting();
            if (!IsLoading)
            {
                RecoveryDeleteNo();
            }
        }

        #region Field

        [Browsable(false)]
        [ImmediatePostData()]
        public bool ActivationPosting
        {
            get { return _activationPosting; }
            set { SetPropertyValue("ActivationPosting", ref _activationPosting, value); }
        }

        [Appearance("PickingLineNoClose", Enabled = false)]
        public int No
        {
            get { return _no; }
            set { SetPropertyValue("No", ref _no, value); }
        }

        [VisibleInListView(false)]
        [RuleRequiredField(DefaultContexts.Save)]
        [Appearance("PickingLineCodeClose", Enabled = false)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue("Code", ref _code, value); }
        }

        [Appearance("PickingLineSelectClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public bool Select
        {
            get { return _select; }
            set { SetPropertyValue("Select", ref _select, value); }
        }

        #region Organization

        [ImmediatePostData()]
        [Appearance("PickingLineCompanyClose", Enabled = false)]
        public Company Company
        {
            get { return _company; }
            set { SetPropertyValue("Company", ref _company, value); }
        }

        [ImmediatePostData()]
        [Appearance("PickingLineWorkplaceClose", Enabled = false)]
        public Workplace Workplace
        {
            get { return _workplace; }
            set { SetPropertyValue("Workplace", ref _workplace, value); }
        }

        [Browsable(false)]
        public Division Division
        {
            get { return _division; }
            set { SetPropertyValue("Division", ref _division, value); }
        }

        [Browsable(false)]
        public Department Department
        {
            get { return _department; }
            set { SetPropertyValue("Department", ref _department, value); }
        }

        [Browsable(false)]
        public Section Section
        {
            get { return _section; }
            set { SetPropertyValue("Section", ref _section, value); }
        }

        [Browsable(false)]
        public Employee Employee
        {
            get { return _employee; }
            set { SetPropertyValue("Employee", ref _employee, value); }
        }

        [ImmediatePostData()]
        [DataSourceCriteria("Company = '@This.Company' And Active = true")]
        [Appearance("PickingLineDivClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Division Div
        {
            get { return _div; }
            set { SetPropertyValue("Div", ref _div, value); }
        }

        [ImmediatePostData()]
        [DataSourceCriteria("Company = '@This.Company' And Division = '@This.Div' And Active = true")]
        [Appearance("PickingLineDeptClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Department Dept
        {
            get { return _dept; }
            set { SetPropertyValue("Dept", ref _dept, value); }
        }

        [ImmediatePostData()]
        [DataSourceCriteria("Company = '@This.Company' And Department = '@This.Dept' And Active = true")]
        [Appearance("PickingLineSectClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Section Sect
        {
            get { return _sect; }
            set { SetPropertyValue("Sect", ref _sect, value); }
        }

        [Browsable(false)]
        public XPCollection<Employee> AvailablePIC
        {
            get
            {
                XPCollection<Employee> _locPICs = null;
                if (this.Company != null && this.Workplace != null)
                {
                    #region DivDeptSect
                    if (this.Div != null && this.Dept != null && this.Sect != null)
                    {
                        _locPICs = new XPCollection<Employee>
                                  (Session, new GroupOperator(GroupOperatorType.And,
                                    new BinaryOperator("Company", this.Company),
                                    new BinaryOperator("Workplace", this.Workplace),
                                    new BinaryOperator("Division", this.Div),
                                    new BinaryOperator("Department", this.Dept),
                                    new BinaryOperator("Section", this.Sect),
                                    new BinaryOperator("Active", true),
                                    new GroupOperator(GroupOperatorType.Or,
                                    new BinaryOperator("SalesRole", SalesRole.TakingOrder),
                                    new BinaryOperator("SalesRole", SalesRole.All))));
                    }
                    else if (this.Div != null && this.Dept != null && this.Sect == null)
                    {
                        _locPICs = new XPCollection<Employee>
                                  (Session, new GroupOperator(GroupOperatorType.And,
                                    new BinaryOperator("Company", this.Company),
                                    new BinaryOperator("Workplace", this.Workplace),
                                    new BinaryOperator("Division", this.Div),
                                    new BinaryOperator("Department", this.Dept),
                                    new BinaryOperator("Active", true),
                                    new GroupOperator(GroupOperatorType.Or,
                                    new BinaryOperator("SalesRole", SalesRole.TakingOrder),
                                    new BinaryOperator("SalesRole", SalesRole.All))));

                    }
                    else if (this.Div != null && this.Dept == null && this.Sect == null)
                    {
                        _locPICs = new XPCollection<Employee>
                                  (Session, new GroupOperator(GroupOperatorType.And,
                                    new BinaryOperator("Company", this.Company),
                                    new BinaryOperator("Workplace", this.Workplace),
                                    new BinaryOperator("Division", this.Div),
                                    new BinaryOperator("Active", true),
                                    new GroupOperator(GroupOperatorType.Or,
                                    new BinaryOperator("SalesRole", SalesRole.TakingOrder),
                                    new BinaryOperator("SalesRole", SalesRole.All))));

                    }
                    else if (this.Div == null && this.Dept == null && this.Sect == null)
                    {
                        _locPICs = new XPCollection<Employee>
                                  (Session, new GroupOperator(GroupOperatorType.And,
                                    new BinaryOperator("Company", this.Company),
                                    new BinaryOperator("Workplace", this.Workplace),
                                    new BinaryOperator("Active", true),
                                    new GroupOperator(GroupOperatorType.Or,
                                    new BinaryOperator("SalesRole", SalesRole.TakingOrder),
                                    new BinaryOperator("SalesRole", SalesRole.All))));

                    }
                    #endregion DivDeptSect
                    if (_locPICs != null && _locPICs.Count() > 0)
                    {
                        _availablePIC = _locPICs;
                    }
                }
                else
                {
                    _availablePIC = new XPCollection<Employee>(Session, new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("Active", true),
                                                            new GroupOperator(GroupOperatorType.Or,
                                                            new BinaryOperator("SalesRole", SalesRole.TakingOrder),
                                                            new BinaryOperator("SalesRole", SalesRole.All))));
                }

                return _availablePIC;

            }
        }

        [ImmediatePostData()]
        [DataSourceProperty("AvailablePIC")]
        [Appearance("PickingLinePICClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Employee PIC
        {
            get { return _pic; }
            set
            {
                SetPropertyValue("InternalPIC", ref _pic, value);
                if (!IsLoading)
                {
                    if (this._pic != null)
                    {
                        SetVehicle(_pic);
                    }
                    else
                    {
                        this.Vehicle = null;
                    }
                }
            }
        }

        #endregion Organization

        [Browsable(false)]
        public XPCollection<SalesInvoice> AvailableSalesInvoice
        {
            get
            {
                if (!IsLoading)
                {
                    if (this.Company != null && this.Workplace != null && this.Picking != null )
                    {
                        #region PIC
                        if(this.PIC != null)
                        {
                            XPQuery<SalesInvoiceMonitoring> _salesInvoiceMonitoringsQuery = new XPQuery<SalesInvoiceMonitoring>(Session);

                            var _salesInvoiceMonitorings = from sim in _salesInvoiceMonitoringsQuery
                                                           where ((sim.Status == Status.Open || sim.Status == Status.Posted)
                                                           && sim.Company == this.Company
                                                           && sim.Workplace == this.Workplace
                                                           && sim.PIC == this.PIC
                                                           && sim.Picking == null)
                                                           group sim by sim.SalesInvoice into g
                                                           select new { SalesInvoice = g.Key };

                            if (_salesInvoiceMonitorings != null && _salesInvoiceMonitorings.Count() > 0)
                            {
                                string _beginString = null;
                                string _endString = null;
                                string _fullString = null;

                                List<string> _stringSIM = new List<string>();

                                foreach (var _salesInvoiceMonitoring in _salesInvoiceMonitorings)
                                {
                                    if (_salesInvoiceMonitoring != null)
                                    {
                                        if (_salesInvoiceMonitoring.SalesInvoice.Code != null)
                                        {
                                            _stringSIM.Add(_salesInvoiceMonitoring.SalesInvoice.Code);
                                        }
                                    }
                                }

                                IEnumerable<string> _stringArraySIMDistinct = _stringSIM.Distinct();
                                string[] _stringArraySIMList = _stringArraySIMDistinct.ToArray();
                                if (_stringArraySIMList.Length == 1)
                                {
                                    for (int i = 0; i < _stringArraySIMList.Length; i++)
                                    {
                                        if (i == 0)
                                        {
                                            _beginString = "[Code]=='" + _stringArraySIMList[i] + "'";
                                        }
                                    }
                                }
                                else if (_stringArraySIMList.Length > 1)
                                {
                                    for (int i = 0; i < _stringArraySIMList.Length; i++)
                                    {
                                        if (i == 0)
                                        {
                                            _beginString = "[Code]=='" + _stringArraySIMList[i] + "'";
                                        }
                                        else
                                        {
                                            _endString = _endString + " OR [Code]=='" + _stringArraySIMList[i] + "'";
                                        }
                                    }
                                }
                                _fullString = _beginString + _endString;

                                if (_fullString != null)
                                {
                                    _availableSalesInvoice = new XPCollection<SalesInvoice>(Session, CriteriaOperator.Parse(_fullString));
                                }
                            }
                        }
                        #endregion PIC
                        #region NonPIC
                        else
                        {
                            XPQuery<SalesInvoiceMonitoring> _salesInvoiceMonitoringsQuery = new XPQuery<SalesInvoiceMonitoring>(Session);

                            var _salesInvoiceMonitorings = from sim in _salesInvoiceMonitoringsQuery
                                                           where ((sim.Status == Status.Open || sim.Status == Status.Posted)
                                                           && sim.Company == this.Company
                                                           && sim.Workplace == this.Workplace
                                                           && sim.Picking == null)
                                                           group sim by sim.SalesInvoice into g
                                                           select new { SalesInvoice = g.Key };

                            if (_salesInvoiceMonitorings != null && _salesInvoiceMonitorings.Count() > 0)
                            {
                                string _beginString = null;
                                string _endString = null;
                                string _fullString = null;

                                List<string> _stringSIM = new List<string>();

                                foreach (var _salesInvoiceMonitoring in _salesInvoiceMonitorings)
                                {
                                    if (_salesInvoiceMonitoring != null)
                                    {
                                        if (_salesInvoiceMonitoring.SalesInvoice.Code != null)
                                        {
                                            _stringSIM.Add(_salesInvoiceMonitoring.SalesInvoice.Code);
                                        }
                                    }
                                }

                                IEnumerable<string> _stringArraySIMDistinct = _stringSIM.Distinct();
                                string[] _stringArraySIMList = _stringArraySIMDistinct.ToArray();
                                if (_stringArraySIMList.Length == 1)
                                {
                                    for (int i = 0; i < _stringArraySIMList.Length; i++)
                                    {
                                        if (i == 0)
                                        {
                                            _beginString = "[Code]=='" + _stringArraySIMList[i] + "'";
                                        }
                                    }
                                }
                                else if (_stringArraySIMList.Length > 1)
                                {
                                    for (int i = 0; i < _stringArraySIMList.Length; i++)
                                    {
                                        if (i == 0)
                                        {
                                            _beginString = "[Code]=='" + _stringArraySIMList[i] + "'";
                                        }
                                        else
                                        {
                                            _endString = _endString + " OR [Code]=='" + _stringArraySIMList[i] + "'";
                                        }
                                    }
                                }
                                _fullString = _beginString + _endString;

                                if (_fullString != null)
                                {
                                    _availableSalesInvoice = new XPCollection<SalesInvoice>(Session, CriteriaOperator.Parse(_fullString));
                                }
                            }
                        }
                        #endregion NonPIC
                    }
                }

                return _availableSalesInvoice;
            }
        }

        [ImmediatePostData()]
        [DataSourceProperty("AvailableSalesInvoice")]
        [Appearance("PickingLineSalesInvoiceClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public SalesInvoice SalesInvoice
        {
            get { return _salesInvoice; }
            set {
                SetPropertyValue("SalesInvoice", ref _salesInvoice, value);
                if(!IsLoading)
                {
                    if (this._salesInvoice != null)
                    {
                        if (this._salesInvoice.SalesToCustomer != null)
                        {
                            this.Customer = this._salesInvoice.SalesToCustomer;
                        }
                        else
                        {
                            this.Customer = null;
                        }
                        if (this._salesInvoice.BillToAddress != null)
                        {
                            this.Address = this._salesInvoice.BillToAddress;
                        }
                        else
                        {
                            this.Address = null;
                        }
                    }
                    else
                    {
                        this.Customer = null;
                        this.Address = null;
                    }
                }
            }
        }

        //[Appearance("PickingLineCustomerClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public BusinessPartner Customer
        {
            get { return _customer; }
            set { SetPropertyValue("Customer", ref _customer, value); }
        }

        [Appearance("PickingLineAddressClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public string Address
        {
            get { return _address; }
            set { SetPropertyValue("Address", ref _address, value); }
        }

        [ModelDefault("DisplayFormat", "{0:dd/MM/yyyy}")]
        [ModelDefault("EditMask", "dd/MM/yyyy")]
        [Appearance("PickingLinePickingDateClose", Enabled = false)]
        public DateTime PickingDate
        {
            get { return _pickingDate; }
            set { SetPropertyValue("PickingDate", ref _pickingDate, value); }

        }

        [Browsable(false)]
        public XPCollection<Vehicle> AvailableVehicle
        {
            get
            {
                if (!IsLoading)
                {
                    if (this.Company != null && this.Workplace != null)
                    {
                        string _beginString = null;
                        string _endString = null;
                        string _fullString = null;

                        #region PIC
                        if (this.PIC != null)
                        {
                            List<string> _stringSVS = new List<string>();

                            XPCollection<SalesmanVehicleSetup> _locSalesmanVehicleSetups = new XPCollection<SalesmanVehicleSetup>
                                                                                      (Session, new GroupOperator(GroupOperatorType.And,
                                                                                       new BinaryOperator("Company", this.Company),
                                                                                       new BinaryOperator("Workplace", this.Workplace),
                                                                                       new BinaryOperator("Salesman", this.PIC),
                                                                                       new BinaryOperator("Active", true)));

                            if (_locSalesmanVehicleSetups != null && _locSalesmanVehicleSetups.Count() > 0)
                            {
                                foreach (SalesmanVehicleSetup _locSalesmanVehicleSetup in _locSalesmanVehicleSetups)
                                {
                                    if (_locSalesmanVehicleSetup.Vehicle != null)
                                    {
                                        if (_locSalesmanVehicleSetup.Vehicle.Code != null)
                                        {
                                            _stringSVS.Add(_locSalesmanVehicleSetup.Vehicle.Code);
                                        }
                                    }
                                }
                            }

                            IEnumerable<string> _stringArraySVSDistinct = _stringSVS.Distinct();
                            string[] _stringArraySVSList = _stringArraySVSDistinct.ToArray();
                            if (_stringArraySVSList.Length == 1)
                            {
                                for (int i = 0; i < _stringArraySVSList.Length; i++)
                                {
                                    Vehicle _locV = Session.FindObject<Vehicle>(new BinaryOperator("Code", _stringArraySVSList[i]));
                                    if (_locV != null)
                                    {
                                        if (i == 0)
                                        {
                                            _beginString = "[Code]=='" + _locV.Code + "'";
                                        }
                                    }
                                }
                            }
                            else if (_stringArraySVSList.Length > 1)
                            {
                                for (int i = 0; i < _stringArraySVSList.Length; i++)
                                {
                                    Vehicle _locV = Session.FindObject<Vehicle>(new BinaryOperator("Code", _stringArraySVSList[i]));
                                    if (_locV != null)
                                    {
                                        if (i == 0)
                                        {
                                            _beginString = "[Code]=='" + _locV.Code + "'";
                                        }
                                        else
                                        {
                                            _endString = _endString + " OR [Code]=='" + _locV.Code + "'";
                                        }
                                    }
                                }
                            }
                            _fullString = _beginString + _endString;

                            if (_fullString != null && _fullString != "")
                            {
                                _availableVehicle = new XPCollection<Vehicle>(Session, CriteriaOperator.Parse(_fullString));
                            }
                            else
                            {
                                _availableVehicle = new XPCollection<Vehicle>(Session, new GroupOperator(GroupOperatorType.And,
                                                                                    new BinaryOperator("Company", this.Company),
                                                                                    new BinaryOperator("Workplace", this.Workplace),
                                                                                    new BinaryOperator("Active", true)));
                            }
                        }
                        #endregion PIC
                        #region NonPIC
                        else
                        {
                            _availableVehicle = new XPCollection<Vehicle>(Session, new GroupOperator(GroupOperatorType.And,
                                                                            new BinaryOperator("Company", this.Company),
                                                                            new BinaryOperator("Workplace", this.Workplace),
                                                                            new BinaryOperator("Active", true)));
                        }
                        #endregion NonPIC
                    }
                    else
                    {
                        _availableVehicle = new XPCollection<Vehicle>(Session, new GroupOperator(GroupOperatorType.And,
                                                                            new BinaryOperator("Active", true)));
                    }

                }

                return _availableVehicle;

            }
        }

        [ImmediatePostData()]
        [DataSourceProperty("AvailableVehicle")]
        [Appearance("PickingLineVehicleClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Vehicle Vehicle
        {
            get { return _vehicle; }
            set {
                SetPropertyValue("Vehicle", ref _vehicle, value);
                if (!IsLoading)
                {
                    if (this._vehicle != null)
                    {
                        SetBegInv(this._vehicle);
                    }
                    else
                    {
                        BegInv = null;
                    }
                }
            }
        }

        [Browsable(false)]
        [Appearance("PickingBegInvClose", Enabled = false)]
        public BeginningInventory BegInv
        {
            get { return _begInv; }
            set { SetPropertyValue("BegInv", ref _begInv, value); }
        }

        [VisibleInListView(false)]
        [Appearance("PickingLineStatusClose", Enabled = false)]
        public Status Status
        {
            get { return _status; }
            set { SetPropertyValue("Status", ref _status, value); }
        }

        [VisibleInListView(false)]
        [ModelDefault("DisplayFormat", "{0:dd/MM/yyyy}")]
        [ModelDefault("EditMask", "dd/MM/yyyy")]
        [Appearance("PickingLineStatusDateClose", Enabled = false)]
        public DateTime StatusDate
        {
            get { return _statusDate; }
            set { SetPropertyValue("StatusDate", ref _statusDate, value); }
        }

        [ImmediatePostData()]
        [RuleRequiredField(DefaultContexts.Save)]
        [Appearance("PickingLinePickingEnabled", Enabled = false)]
        [Association("Picking-PickingLines")]
        public Picking Picking
        {
            get { return _picking; }
            set
            {
                SetPropertyValue("Picking", ref _picking, value);
                if (!IsLoading)
                {
                    if (this._picking != null)
                    {
                        if (this._picking.Company != null) { this.Company = this._picking.Company; }
                        if (this._picking.Workplace != null) { this.Workplace = this._picking.Workplace; }
                        if (this._picking.Division != null) { this.Division = this._picking.Division; }
                        if (this._picking.Department != null) { this.Department = this._picking.Department; }
                        if (this._picking.Section != null) { this.Section = this._picking.Section; }
                        if (this._picking.Employee != null) { this.Employee = this._picking.Employee; }
                        if (this._picking.Div != null) { this.Div = this._picking.Div; }
                        if (this._picking.Dept != null) { this.Dept = this._picking.Dept; }
                        if (this._picking.Sect != null) { this.Sect = this._picking.Sect; }
                        if (this._picking.PIC != null) { this.PIC = this._picking.PIC; }
                        if (this._picking.Vehicle != null) { this.Vehicle = this._picking.Vehicle; }
                        this.PickingDate = this._picking.PickingDate;
                        if (this._picking.BegInv != null) { this.BegInv = this._picking.BegInv; }
                    }
                }
            }
        }

        //AuditTrail
        [CollectionOperationSet(AllowAdd = false, AllowRemove = true)]
        public XPCollection<AuditDataItemPersistent> ChangeHistory
        {
            get
            {
                if (changeHistory == null)
                {
                    changeHistory = AuditedObjectWeakReference.GetAuditTrail(Session, this);
                }
                return changeHistory;
            }
        }

        #endregion Field

        //========================================== Code In Here ==========================================

        #region No

        public void UpdateNo()
        {
            try
            {
                if (!IsLoading && Session.IsNewObject(this))
                {
                    if (this.Picking != null)
                    {
                        object _makRecord = Session.Evaluate<PickingLine>(CriteriaOperator.Parse("Max(No)"), CriteriaOperator.Parse("Picking=?", this.Picking));
                        this.No = Convert.ToInt32(_makRecord) + 1;
                        this.Save();
                        RecoveryUpdateNo();
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PickingLine " + ex.ToString());
            }
        }

        public void RecoveryUpdateNo()
        {
            try
            {
                if (this.Picking != null)
                {
                    Picking _numHeader = Session.FindObject<Picking>
                                                (new BinaryOperator("Code", this.Picking.Code));

                    XPCollection<PickingLine> _numLines = new XPCollection<PickingLine>
                                                (Session, new BinaryOperator("Picking", _numHeader),
                                                new SortProperty("No", DevExpress.Xpo.DB.SortingDirection.Ascending));
                    if (_numLines != null)
                    {
                        int i1 = 0;
                        foreach (PickingLine _numLine in _numLines)
                        {
                            i1 += 1;
                            _numLine.No = i1;
                            _numLine.Save();
                        }
                        i1 = 1;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PickingLine " + ex.ToString());
            }
        }

        public void RecoveryDeleteNo()
        {
            try
            {
                if (this.Picking != null)
                {
                    Picking _numHeader = Session.FindObject<Picking>
                                                (new BinaryOperator("Code", this.Picking.Code));

                    XPCollection<PickingLine> _numLines = new XPCollection<PickingLine>
                                                (Session, new GroupOperator(GroupOperatorType.And,
                                                 new BinaryOperator("This", this, BinaryOperatorType.NotEqual),
                                                 new BinaryOperator("Picking", _numHeader)),
                                                 new SortProperty("No", DevExpress.Xpo.DB.SortingDirection.Ascending));
                    if (_numLines != null)
                    {
                        int i = 0;
                        foreach (PickingLine _numLine in _numLines)
                        {
                            i += 1;
                            _numLine.No = i;
                            _numLine.Save();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PickingLine " + ex.ToString());
            }
        }

        #endregion No

        private void SetVehicle(Employee _locPIC)
        {
            try
            {
                DateTime now = DateTime.Now;
                if (this.Company != null && this.Workplace != null)
                {
                    SalesmanVehicleSetup _locSalesmanVehicleSetup = Session.FindObject<SalesmanVehicleSetup>
                                                        (new GroupOperator(GroupOperatorType.And,
                                                        new BinaryOperator("Company", this.Company),
                                                        new BinaryOperator("Workplace", this.Workplace),
                                                        new BinaryOperator("Salesman", _locPIC),
                                                        new BinaryOperator("Active", true)));
                    if (_locSalesmanVehicleSetup != null)
                    {
                        if (_locSalesmanVehicleSetup.Vehicle != null)
                        {
                            this.Vehicle = _locSalesmanVehicleSetup.Vehicle;
                        }
                        else
                        {
                            this.Vehicle = null;
                        }
                    }
                    else
                    {
                        this.Vehicle = null;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Business Object = PickingLine ", ex.ToString());
            }
        }

        private void SetBegInv(Vehicle _locVehicle)
        {
            try
            {
                DateTime now = DateTime.Now;
                if (this.Company != null && this.Workplace != null)
                {
                    BeginningInventory _locBegInv = Session.FindObject<BeginningInventory>
                                                        (new GroupOperator(GroupOperatorType.And,
                                                        new BinaryOperator("Company", this.Company),
                                                        new BinaryOperator("Workplace", this.Workplace),
                                                        new BinaryOperator("OpenVehicle", true),
                                                        new BinaryOperator("Vehicle", _locVehicle),
                                                        new BinaryOperator("LocationType", LocationType.Vehicle),
                                                        new BinaryOperator("StockType", StockType.Good),
                                                        new BinaryOperator("Active", true)));
                    if (_locBegInv != null)
                    {
                        this.BegInv = _locBegInv;
                    }
                    else
                    {
                        this.BegInv = null;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Business Object = PickingLine ", ex.ToString());
            }
        }
    }
}