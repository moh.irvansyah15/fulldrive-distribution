﻿using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;

using DevExpress.ExpressApp.Editors;
using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.ConditionalAppearance;

namespace FullDrive.Module.BusinessObjects
{
    [DefaultClassOptions]
    [DefaultProperty("Code")]
    [NavigationItem("Inventory")]
    [RuleCombinationOfPropertiesIsUnique("AmendDeliveryRuleUnique", DefaultContexts.Save, "Code")]
    //[ImageName("BO_Contact")]
    //[DefaultProperty("DisplayMemberNameForLookupEditorsOfThisType")]
    //[DefaultListViewOptions(MasterDetailMode.ListViewOnly, false, NewItemRowPosition.None)]
    //[Persistent("DatabaseTableName")]
    // Specify more UI options using a declarative approach (https://documentation.devexpress.com/#eXpressAppFramework/CustomDocument112701).
    public class AmendDelivery : BaseObject
    { // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).
        private bool _activationPosting;
        private string _code;
        #region InitialOrganization
        private Company _company;
        private XPCollection<Workplace> _availableWorkplace;
        private Workplace _workplace;
        private Division _division;
        private Department _department;
        private Section _section;
        private Employee _employee;
        #endregion InitialOrganization
        private Employee _pic;
        private SalesmanVehicleSetup _selectVehicle;
        private XPCollection<Vehicle> _availableVehicle;
        private Vehicle _vehicle;
        private bool _isRoute;
        private DateTime _pickingDate;
        private XPCollection<Picking> _availablePicking;
        private Picking _picking;
        private DateTime _routeDate;
        private Route _selectRoute;
        private double _grandTotalAmount;
        private double _grandTotalAmountDN;
        private double _grandTotalAmountCN;
        private double _grandTotalAmountColl;
        private Status _status;
        private DateTime _statusDate;
        private string _userAccessIn;
        private GlobalFunction _globFunc;
        //AuditTrail
        private XPCollection<AuditDataItemPersistent> changeHistory;

        public AmendDelivery(Session session)
            : base(session)
        {
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if (!IsLoading && !IsSaving && !IsInvalidated)
            {
                if (Session.IsNewObject(this))
                {
                    _globFunc = new GlobalFunction();
                    DateTime now = DateTime.Now;
                    #region UserAccess
                    _userAccessIn = SecuritySystem.CurrentUserName;
                    UserAccess _locUserAccess = Session.FindObject<UserAccess>(new BinaryOperator("UserName", _userAccessIn));

                    if (_locUserAccess != null)
                    {
                        if (_locUserAccess.Employee != null)
                        {
                            
                            if (_locUserAccess.Employee.Company != null && _locUserAccess.Employee.Workplace != null)
                            {
                                this.Code = _globFunc.GetNumberingUnlockOptimisticRecordBasedOrganization(this.Session.DataLayer, ObjectList.AmendDelivery, _locUserAccess.Employee.Company, _locUserAccess.Employee.Workplace);
                            }
                            else
                            {
                                this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.AmendDelivery);
                            }

                            if (_locUserAccess.Employee.Company != null)
                            {
                                Company = _locUserAccess.Employee.Company;
                            }
                            if (_locUserAccess.Employee.Workplace != null)
                            {
                                this.Workplace = _locUserAccess.Employee.Workplace;
                            }
                            else
                            {
                                this.Workplace = _globFunc.GetDefaultWorkplace(this.Session, _locUserAccess);
                            }
                            if (_locUserAccess.Employee.Division != null)
                            {
                                Division = _locUserAccess.Employee.Division;
                            }
                            if (_locUserAccess.Employee.Department != null)
                            {
                                Department = _locUserAccess.Employee.Department;
                            }
                            if (_locUserAccess.Employee.Section != null)
                            {
                                Section = _locUserAccess.Employee.Section;
                            }

                            this.Employee = _locUserAccess.Employee;

                            
                        }
                    }
                    #endregion UserAccess
                    this.Status = CustomProcess.Status.Open;
                    this.StatusDate = now;
                    this.IsRoute = true;
                }
            }
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).
        }


        #region Field

        [Browsable(false)]
        [ImmediatePostData()]
        public bool ActivationPosting
        {
            get { return _activationPosting; }
            set { SetPropertyValue("ActivationPosting", ref _activationPosting, value); }
        }

        [Appearance("AmendDeliveryCodeClose", Enabled = false)]
        [RuleRequiredField(DefaultContexts.Save)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue("Code", ref _code, value); }
        }

        #region Organization

        [ImmediatePostData()]
        [RuleRequiredField(DefaultContexts.Save)]
        [DataSourceCriteria("Active = true")]
        [Appearance("AmendDeliveryCompanyClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Company Company
        {
            get { return _company; }
            set { SetPropertyValue("Company", ref _company, value); }
        }

        [Browsable(false)]
        public XPCollection<Workplace> AvailableWorkplace
        {
            get
            {
                if (!IsLoading)
                {
                    if (this.Company != null)
                    {
                        _userAccessIn = SecuritySystem.CurrentUserName;
                        UserAccess _locUserAccess = Session.FindObject<UserAccess>(new BinaryOperator("UserName", _userAccessIn));
                        if (_locUserAccess != null)
                        {
                            string _beginString = null;
                            string _endString = null;
                            string _fullString = null;
                            List<string> _stringWorkplace = new List<string>();

                            XPCollection<OrganizationSetupDetail> _locOrganizationSetupDetails = new XPCollection<OrganizationSetupDetail>
                                                                            (Session, new GroupOperator(GroupOperatorType.And,
                                                                            new BinaryOperator("Company", this.Company),
                                                                            new BinaryOperator("UserAccess", _locUserAccess),
                                                                            new BinaryOperator("OpenWorkplace", true),
                                                                            new BinaryOperator("Active", true)));

                            if (_locOrganizationSetupDetails != null && _locOrganizationSetupDetails.Count() > 0)
                            {
                                foreach (OrganizationSetupDetail _locOrganizationSetupDetail in _locOrganizationSetupDetails)
                                {
                                    if (_locOrganizationSetupDetail.Workplace != null)
                                    {
                                        if (_locOrganizationSetupDetail.Workplace.Code != null)
                                        {
                                            _stringWorkplace.Add(_locOrganizationSetupDetail.Workplace.Code);
                                        }
                                    }
                                }

                                #region Workplace
                                IEnumerable<string> _stringArrayWorkplaceDistinct = _stringWorkplace.Distinct();
                                string[] _stringArrayWorkplaceList = _stringArrayWorkplaceDistinct.ToArray();
                                if (_stringArrayWorkplaceList.Length == 1)
                                {
                                    for (int i = 0; i < _stringArrayWorkplaceList.Length; i++)
                                    {
                                        Workplace _locWorkplace = Session.FindObject<Workplace>(new BinaryOperator("Code", _stringArrayWorkplaceList[i]));
                                        if (_locWorkplace != null)
                                        {
                                            if (i == 0)
                                            {
                                                _beginString = "[Code]=='" + _locWorkplace.Code + "'";
                                            }
                                        }
                                    }
                                }
                                else if (_stringArrayWorkplaceList.Length > 1)
                                {
                                    for (int i = 0; i < _stringArrayWorkplaceList.Length; i++)
                                    {
                                        Workplace _locWorkplace = Session.FindObject<Workplace>(new BinaryOperator("Code", _stringArrayWorkplaceList[i]));
                                        if (_locWorkplace != null)
                                        {
                                            if (i == 0)
                                            {
                                                _beginString = "[Code]=='" + _locWorkplace.Code + "'";
                                            }
                                            else
                                            {
                                                _endString = _endString + " OR [Code]=='" + _locWorkplace.Code + "'";
                                            }
                                        }

                                    }
                                }
                                _fullString = _beginString + _endString;


                                if (_fullString != null)
                                {
                                    _availableWorkplace = new XPCollection<Workplace>(Session, CriteriaOperator.Parse(_fullString));
                                }

                                #endregion Workplace
                            }
                            else
                            {
                                OrganizationSetupDetail _locOrgSetDetail = Session.FindObject<OrganizationSetupDetail>
                                                                            (new GroupOperator(GroupOperatorType.And,
                                                                            new BinaryOperator("Company", this.Company),
                                                                            new BinaryOperator("UserAccess", _locUserAccess),
                                                                            new BinaryOperator("AllWorkplace", true),
                                                                            new BinaryOperator("Active", true)));
                                if (_locOrgSetDetail != null)
                                {
                                    _availableWorkplace = new XPCollection<Workplace>(Session,
                                                        new GroupOperator(GroupOperatorType.And,
                                                        new BinaryOperator("Company", this.Company),
                                                        new BinaryOperator("Active", true)));
                                }
                            }
                        }
                    }
                }

                return _availableWorkplace;
            }
        }

        [Browsable(false)]
        [ImmediatePostData()]
        [Appearance("AmendDeliveryWorkplaceClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Workplace Workplace
        {
            get { return _workplace; }
            set { SetPropertyValue("Workplace", ref _workplace, value); }
        }

        [Browsable(false)]
        public Division Division
        {
            get { return _division; }
            set { SetPropertyValue("Division", ref _division, value); }
        }

        [Browsable(false)]
        public Department Department
        {
            get { return _department; }
            set { SetPropertyValue("Department", ref _department, value); }
        }

        [Browsable(false)]
        public Section Section
        {
            get { return _section; }
            set { SetPropertyValue("Section", ref _section, value); }
        }

        [Browsable(false)]
        public Employee Employee
        {
            get { return _employee; }
            set { SetPropertyValue("Employee", ref _employee, value); }
        }

        #endregion Organization

        [ImmediatePostData()]
        [DataSourceCriteria("Company = '@This.Company' AND Workplace = '@This.Workplace' AND Active = true AND SalesRole = 2 OR SalesRole = 3")]
        [Appearance("AmendDeliveryPICClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Employee PIC
        {
            get { return _pic; }
            set { SetPropertyValue("Salesman", ref _pic, value); }
        }

        [NonPersistent]
        [ImmediatePostData()]
        [DataSourceCriteria("Company = '@This.Company' AND Workplace = '@This.Workplace' AND Salesman = '@This.PIC' AND Active = true ")]
        [Appearance("AmendDeliverySelectVehicleClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public SalesmanVehicleSetup SelectVehicle
        {
            get { return _selectVehicle; }
            set
            {
                SetPropertyValue("SelectVehicle", ref _selectVehicle, value);
                if (!IsLoading)
                {
                    if (this._selectVehicle != null)
                    {
                        if (this._selectVehicle.Vehicle != null)
                        {
                            this.Vehicle = this._selectVehicle.Vehicle;
                        }
                    }
                    else
                    {
                        this.Vehicle = null;
                    }
                }
            }
        }

        [Browsable(false)]
        public XPCollection<Vehicle> AvailableVehicle
        {
            get
            {
                if (!IsLoading)
                {
                    if (this.Company != null && this.Workplace != null)
                    {
                        XPQuery<DeliveryOrderMonitoring> _deliveryOrderMonitoringsQuery = new XPQuery<DeliveryOrderMonitoring>(Session);

                        var _deliveryOrderMonitorings = from tim in _deliveryOrderMonitoringsQuery
                                                        where ((tim.Status == Status.Open || tim.Status == Status.Posted)
                                                        && tim.Company == this.Company
                                                        && tim.Workplace == this.Workplace
                                                        && tim.DeliveryOrder != null
                                                        && tim.Vehicle != null)
                                                        group tim by tim.Vehicle into g
                                                        select new { Vehicle = g.Key };

                        if (_deliveryOrderMonitorings != null && _deliveryOrderMonitorings.Count() > 0)
                        {
                            string _beginString = null;
                            string _endString = null;
                            string _fullString = null;

                            List<string> _stringDOM = new List<string>();

                            foreach (var _deliveryOrderMonitoring in _deliveryOrderMonitorings)
                            {
                                if (_deliveryOrderMonitoring != null)
                                {
                                    if (_deliveryOrderMonitoring.Vehicle != null)
                                    {
                                        if (_deliveryOrderMonitoring.Vehicle.Code != null)
                                        {
                                            _stringDOM.Add(_deliveryOrderMonitoring.Vehicle.Code);
                                        }
                                    }
                                }
                            }

                            IEnumerable<string> _stringArrayDOMDistinct = _stringDOM.Distinct();
                            string[] _stringArrayDOMList = _stringArrayDOMDistinct.ToArray();
                            if (_stringArrayDOMList.Length == 1)
                            {
                                for (int i = 0; i < _stringArrayDOMList.Length; i++)
                                {
                                    if (i == 0)
                                    {
                                        _beginString = "[Code]=='" + _stringArrayDOMList[i] + "'";
                                    }
                                }
                            }
                            else if (_stringArrayDOMList.Length > 1)
                            {
                                for (int i = 0; i < _stringArrayDOMList.Length; i++)
                                {
                                    if (i == 0)
                                    {
                                        _beginString = "[Code]=='" + _stringArrayDOMList[i] + "'";
                                    }
                                    else
                                    {
                                        _endString = _endString + " OR [Code]=='" + _stringArrayDOMList[i] + "'";
                                    }
                                }
                            }
                            _fullString = _beginString + _endString;

                            if (_fullString != null && _fullString != "")
                            {
                                _availableVehicle = new XPCollection<Vehicle>(Session, CriteriaOperator.Parse(_fullString));
                            }
                            else
                            {
                                _availableVehicle = new XPCollection<Vehicle>(Session, new GroupOperator(GroupOperatorType.And,
                                                                            new BinaryOperator("Company", this.Company),
                                                                            new BinaryOperator("Workplace", this.Workplace),
                                                                            new BinaryOperator("Active", true)));
                            }
                        }
                        else
                        {
                            _availableVehicle = new XPCollection<Vehicle>(Session, new GroupOperator(GroupOperatorType.And,
                                                                            new BinaryOperator("Company", this.Company),
                                                                            new BinaryOperator("Workplace", this.Workplace),
                                                                            new BinaryOperator("Active", true)));
                        }
                    }
                    else
                    {
                        _availableVehicle = new XPCollection<Vehicle>(Session, new GroupOperator(GroupOperatorType.And,
                                                                            new BinaryOperator("Active", true)));
                    }

                }

                return _availableVehicle;

            }
        }

        [ImmediatePostData()]
        [Appearance("AmendDeliveryVehicleClose", Enabled = false)]
        public Vehicle Vehicle
        {
            get { return _vehicle; }
            set { SetPropertyValue("Vehicle", ref _vehicle, value); }
        }

        [ImmediatePostData()]
        public bool IsRoute
        {
            get { return _isRoute; }
            set { SetPropertyValue("IsRoute", ref _isRoute, value); }
        }

        [ImmediatePostData()]
        [ModelDefault("DisplayFormat", "{0:dd/MM/yyyy}")]
        [ModelDefault("EditMask", "dd/MM/yyyy")]
        [Appearance("AmendDeliveryPickingDateClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [Appearance("AmendDeliveryPickingDateClose2", Criteria = "IsRoute = true", Enabled = true)]
        [Appearance("AmendDeliveryPickingDateClose3", Criteria = "IsRoute = false", Enabled = false)]
        public DateTime PickingDate
        {
            get { return _pickingDate; }
            set { SetPropertyValue("PickingDate", ref _pickingDate, value); }

        }

        [Browsable(false)]
        public XPCollection<Picking> AvailablePicking
        {
            get
            {
                if (!IsLoading)
                {
                    if (this.Company != null && this.Workplace != null)
                    {
                        XPQuery<PickingMonitoring> _pickingMonitoringsQuery = new XPQuery<PickingMonitoring>(Session);

                        var _pickingMonitorings = from pm in _pickingMonitoringsQuery
                                                  where ((pm.Status == Status.Open || pm.Status == Status.Posted)
                                                  && pm.CollectStatus == CollectStatus.NotReady
                                                  && pm.Company == this.Company
                                                  && pm.Workplace == this.Workplace
                                                  && pm.Picking != null
                                                  && pm.DeliveryOrder != null)
                                                  group pm by pm.Picking into g
                                                  select new { Picking = g.Key };

                        if (_pickingMonitorings != null && _pickingMonitorings.Count() > 0)
                        {
                            string _beginString = null;
                            string _endString = null;
                            string _fullString = null;

                            List<string> _stringPM = new List<string>();

                            foreach (var _pickingMonitoring in _pickingMonitorings)
                            {
                                if (_pickingMonitoring != null)
                                {
                                    if (_pickingMonitoring.Picking != null)
                                    {
                                        if (_pickingMonitoring.Picking.Code != null && _pickingMonitoring.Picking.PickingDate.ToShortDateString() == this.PickingDate.ToShortDateString())
                                        {
                                            _stringPM.Add(_pickingMonitoring.Picking.Code);
                                        }
                                    }
                                }
                            }

                            IEnumerable<string> _stringArrayPMDistinct = _stringPM.Distinct();
                            string[] _stringArrayPMList = _stringArrayPMDistinct.ToArray();
                            if (_stringArrayPMList.Length == 1)
                            {
                                for (int i = 0; i < _stringArrayPMList.Length; i++)
                                {
                                    if (i == 0)
                                    {
                                        _beginString = "[Code]=='" + _stringArrayPMList[i] + "'";
                                    }
                                }
                            }
                            else if (_stringArrayPMList.Length > 1)
                            {
                                for (int i = 0; i < _stringArrayPMList.Length; i++)
                                {
                                    if (i == 0)
                                    {
                                        _beginString = "[Code]=='" + _stringArrayPMList[i] + "'";
                                    }
                                    else
                                    {
                                        _endString = _endString + " OR [Code]=='" + _stringArrayPMList[i] + "'";
                                    }
                                }
                            }
                            _fullString = _beginString + _endString;

                            if (_fullString != null && _fullString != "")
                            {
                                _availablePicking = new XPCollection<Picking>(Session, CriteriaOperator.Parse(_fullString));
                            }
                        }
                    }
                }

                return _availablePicking;
            }
        }

        [ImmediatePostData()]
        [DataSourceProperty("AvailablePicking")]
        [Appearance("AmendDeliveryPickingClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [Appearance("AmendDeliveryPickingClose2", Criteria = "IsRoute = true", Enabled = true)]
        [Appearance("AmendDeliveryPickingClose3", Criteria = "IsRoute = false", Enabled = false)]
        public Picking Picking
        {
            get { return _picking; }
            set
            {
                SetPropertyValue("Picking", ref _picking, value);
                if (!IsLoading)
                {
                    if (this._picking != null)
                    {
                        if (this._picking.Vehicle != null)
                        {
                            this.Vehicle = this._picking.Vehicle;
                        }
                        else
                        {
                            this.Vehicle = null;
                        }
                    }
                    else
                    {
                        this.Vehicle = null;
                    }
                }
            }
        }

        [Appearance("AmendDeliveryGrandTotalAmountClose", Enabled = false)]
        public double GrandTotalAmount
        {
            get { return _grandTotalAmount; }
            set { SetPropertyValue("GrandTotalAmount", ref _grandTotalAmount, value); }
        }

        [Appearance("AmendDeliveryGrandTotalAmountDNClose", Enabled = false)]
        public double GrandTotalAmountDN
        {
            get { return _grandTotalAmountDN; }
            set { SetPropertyValue("GrandTotalAmountDN", ref _grandTotalAmountDN, value); }
        }

        [Appearance("AmendDeliveryGrandTotalAmountCNClose", Enabled = false)]
        public double GrandTotalAmountCN
        {
            get { return _grandTotalAmountCN; }
            set { SetPropertyValue("GrandTotalAmountCN", ref _grandTotalAmountCN, value); }
        }

        [Appearance("AmendDeliveryGrandTotalAmountColllose", Enabled = false)]
        public double GrandTotalAmountColl
        {
            get { return _grandTotalAmountColl; }
            set { SetPropertyValue("GrandTotalAmountColl", ref _grandTotalAmountColl, value); }
        }

        [Appearance("AmendDeliveryStatusClose", Enabled = false)]
        public Status Status
        {
            get { return _status; }
            set { SetPropertyValue("Status", ref _status, value); }
        }

        [ModelDefault("DisplayFormat", "{0:dd/MM/yyyy}")]
        [ModelDefault("EditMask", "dd/MM/yyyy")]
        [Appearance("AmendDeliveryStatusDateClose", Enabled = false)]
        public DateTime StatusDate
        {
            get { return _statusDate; }
            set { SetPropertyValue("StatusDate", ref _statusDate, value); }
        }

        [Association("AmendDelivery-AmendDeliveryLines")]
        public XPCollection<AmendDeliveryLine> AmendDeliveryLines
        {
            get { return GetCollection<AmendDeliveryLine>("AmendDeliveryLines"); }
        }

        //AuditTrail
        [CollectionOperationSet(AllowAdd = false, AllowRemove = true)]
        public XPCollection<AuditDataItemPersistent> ChangeHistory
        {
            get
            {
                if (changeHistory == null)
                {
                    changeHistory = AuditedObjectWeakReference.GetAuditTrail(Session, this);
                }
                return changeHistory;
            }
        }

        #endregion Field


    }
}