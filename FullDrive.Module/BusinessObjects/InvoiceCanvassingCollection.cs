﻿using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;

using DevExpress.ExpressApp.Editors;
using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.ConditionalAppearance;

namespace FullDrive.Module.BusinessObjects
{
    [DefaultClassOptions]
    [DefaultProperty("Code")]
    [NavigationItem("Finance & Accounting")]
    [RuleCombinationOfPropertiesIsUnique("InvoiceCanvassingCollectionRuleUnique", DefaultContexts.Save, "Code")]
    //[ImageName("BO_Contact")]
    //[DefaultProperty("DisplayMemberNameForLookupEditorsOfThisType")]
    //[DefaultListViewOptions(MasterDetailMode.ListViewOnly, false, NewItemRowPosition.None)]
    //[Persistent("DatabaseTableName")]
    // Specify more UI options using a declarative approach (https://documentation.devexpress.com/#eXpressAppFramework/CustomDocument112701).
    public class InvoiceCanvassingCollection : BaseObject
    { // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).

        private bool _activationPosting;
        private string _code;
        #region InitialOrganization
        private Company _company;
        private Workplace _workplace;
        private Division _division;
        private Department _department;
        private Section _section;
        private Employee _employee;
        #endregion InitialOrganization
        private XPCollection<Employee> _availableSalesman;
        private Employee _salesman;
        private XPCollection<Vehicle> _availableVehicle;
        private Vehicle _vehicle;
        private PriceGroup _priceGroup;
        private XPCollection<InvoiceCanvassing> _availableInvoiceCanvassing;
        private InvoiceCanvassing _invoiceCanvassing;
        private XPCollection<BusinessPartner> _availableCustomer;
        private BusinessPartner _customer;
        private string _contact;
        private string _address;
        private Status _status;
        private DateTime _statusDate;
        private CanvassingCollection _canvassingCollection;
        private string _userAccessIn;
        private GlobalFunction _globFunc;
        //AuditTrail
        private XPCollection<AuditDataItemPersistent> changeHistory;

        public InvoiceCanvassingCollection(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if (!IsLoading)
            {
                if (Session.IsNewObject(this))
                {
                    _globFunc = new GlobalFunction();
                    DateTime now = DateTime.Now;
                    #region UserAccess
                    _userAccessIn = SecuritySystem.CurrentUserName;
                    UserAccess _locUserAccess = Session.FindObject<UserAccess>(new BinaryOperator("UserName", _userAccessIn));
                    if (_locUserAccess != null)
                    {
                        if (_locUserAccess.Employee != null)
                        {
                            if (_locUserAccess.Employee.Company != null && _locUserAccess.Employee.Workplace != null)
                            {
                                this.Code = _globFunc.GetNumberingUnlockOptimisticRecordBasedOrganization(this.Session.DataLayer, ObjectList.InvoiceCanvassingCollection, _locUserAccess.Employee.Company, _locUserAccess.Employee.Workplace);
                            }
                            else
                            {
                                this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.InvoiceCanvassingCollection);
                            }
                        }
                    }
                    #endregion UserAccess
                    this.Status = Status.Open;
                    this.StatusDate = now;
                }    
            }
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).
        }

        #region Field

        [Browsable(false)]
        public bool ActivationPosting
        {
            get { return _activationPosting; }
            set { SetPropertyValue("ActivationPosting", ref _activationPosting, value); }
        }

        [RuleRequiredField(DefaultContexts.Save)]
        [Appearance("InvoiceCanvassingCollectionCodeClose", Enabled = false)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue("Code", ref _code, value); }
        }

        #region Organization

        [ImmediatePostData()]
        [RuleRequiredField(DefaultContexts.Save)]
        [Appearance("InvoiceCanvassingCollectionCompanyClose", Enabled = false)]
        public Company Company
        {
            get { return _company; }
            set { SetPropertyValue("Company", ref _company, value); }
        }

        [ImmediatePostData()]
        [Appearance("InvoiceCanvassingCollectionWorkplaceClose", Enabled = false)]
        public Workplace Workplace
        {
            get { return _workplace; }
            set { SetPropertyValue("Workplace", ref _workplace, value); }
        }

        [Browsable(false)]
        public Division Division
        {
            get { return _division; }
            set { SetPropertyValue("Division", ref _division, value); }
        }

        [Browsable(false)]
        public Department Department
        {
            get { return _department; }
            set { SetPropertyValue("Department", ref _department, value); }
        }

        [Browsable(false)]
        public Section Section
        {
            get { return _section; }
            set { SetPropertyValue("Section", ref _section, value); }
        }

        [Browsable(false)]
        public Employee Employee
        {
            get { return _employee; }
            set { SetPropertyValue("Employee", ref _employee, value); }
        }

        #endregion Organization

        [ImmediatePostData()]
        [Appearance("InvoiceCanvassingCollectionSalesmanClose", Enabled = false)]
        public Employee Salesman
        {
            get { return _salesman; }
            set { SetPropertyValue("Salesman", ref _salesman, value); }
        }

        [Browsable(false)]
        public XPCollection<Vehicle> AvailableVehicle
        {
            get
            {
                string _beginString = null;
                string _endString = null;
                string _fullString = null;


                if (this.Company != null && this.Workplace != null && this.Salesman != null)
                {

                    List<string> _stringSVS = new List<string>();

                    XPCollection<SalesmanVehicleSetup> _locSalesmanVehicleSetups = new XPCollection<SalesmanVehicleSetup>
                                                                              (Session, new GroupOperator(GroupOperatorType.And,
                                                                               new BinaryOperator("Company", this.Company),
                                                                               new BinaryOperator("Workplace", this.Workplace),
                                                                               new BinaryOperator("Salesman", this.Salesman),
                                                                               new BinaryOperator("Active", true)));

                    if (_locSalesmanVehicleSetups != null && _locSalesmanVehicleSetups.Count() > 0)
                    {
                        foreach (SalesmanVehicleSetup _locSalesmanVehicleSetup in _locSalesmanVehicleSetups)
                        {
                            if (_locSalesmanVehicleSetup.Vehicle != null)
                            {
                                if (_locSalesmanVehicleSetup.Vehicle.Code != null)
                                {
                                    _stringSVS.Add(_locSalesmanVehicleSetup.Vehicle.Code);
                                }
                            }
                        }
                    }

                    IEnumerable<string> _stringArraySVSDistinct = _stringSVS.Distinct();
                    string[] _stringArraySVSList = _stringArraySVSDistinct.ToArray();
                    if (_stringArraySVSList.Length == 1)
                    {
                        for (int i = 0; i < _stringArraySVSList.Length; i++)
                        {
                            Vehicle _locVHC = Session.FindObject<Vehicle>(new BinaryOperator("Code", _stringArraySVSList[i]));
                            if (_locVHC != null)
                            {
                                if (i == 0)
                                {
                                    _beginString = "[Code]=='" + _locVHC.Code + "'";
                                }
                            }
                        }
                    }
                    else if (_stringArraySVSList.Length > 1)
                    {
                        for (int i = 0; i < _stringArraySVSList.Length; i++)
                        {
                            Vehicle _locVHC = Session.FindObject<Vehicle>(new BinaryOperator("Code", _stringArraySVSList[i]));
                            if (_locVHC != null)
                            {
                                if (i == 0)
                                {
                                    _beginString = "[Code]=='" + _locVHC.Code + "'";
                                }
                                else
                                {
                                    _endString = _endString + " OR [Code]=='" + _locVHC.Code + "'";
                                }
                            }
                        }
                    }
                    _fullString = _beginString + _endString;

                    if (_fullString != null)
                    {
                        _availableVehicle = new XPCollection<Vehicle>(Session, CriteriaOperator.Parse(_fullString));
                    }

                }
                else
                {
                    _availableVehicle = new XPCollection<Vehicle>(Session, new GroupOperator(GroupOperatorType.And,
                                                                            new BinaryOperator("Active", true)));
                }

                return _availableVehicle;

            }
        }

        [ImmediatePostData()]
        [DataSourceProperty("AvailableVehicle", DataSourcePropertyIsNullMode.SelectAll)]
        [Appearance("InvoiceCanvassingCollectionVehicleClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Vehicle Vehicle
        {
            get { return _vehicle; }
            set { SetPropertyValue("Vehicle", ref _vehicle, value); }
        }

        [Appearance("CanvassingCollectionPriceGroupClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public PriceGroup PriceGroup
        {
            get { return _priceGroup; }
            set { SetPropertyValue("PriceGroup", ref _priceGroup, value); }
        }

        [Browsable(false)]
        public XPCollection<InvoiceCanvassing> AvailableInvoiceCanvassing
        {
            get
            {
                string _beginString = null;
                string _endString = null;
                string _fullString = null;

                if (!IsLoading)
                {
                    if (this.CanvassingCollection != null)
                    {
                        if(this.Company != null && this.Workplace != null && this.Salesman != null && this.Vehicle !=null && this.PriceGroup != null)
                        {
                            XPQuery<InvoiceCanvassingMonitoring> _invoiceCanvassingMonitoringsQuery = new XPQuery<InvoiceCanvassingMonitoring>(Session);

                            var _invoiceCanvassingMonitorings = from icm in _invoiceCanvassingMonitoringsQuery
                                                                where ((icm.Status == Status.Open || icm.Status == Status.Posted)
                                                                && icm.Company == this.Company
                                                                && icm.Workplace == this.Workplace
                                                                && icm.Salesman == this.Salesman
                                                                && icm.Vehicle == this.Vehicle
                                                                && icm.PriceGroup == this.PriceGroup
                                                                && icm.CanvassingCollection == null)
                                                                group icm by icm.InvoiceCanvassing into g
                                                                select new { InvoiceCanvassing = g.Key };

                            if (_invoiceCanvassingMonitorings != null && _invoiceCanvassingMonitorings.Count() > 0)
                            {
                                List<string> _stringICM = new List<string>();

                                foreach (var _invoiceCanvassingMonitoring in _invoiceCanvassingMonitorings)
                                {
                                    if (_invoiceCanvassingMonitoring != null)
                                    {
                                        if (_invoiceCanvassingMonitoring.InvoiceCanvassing.Code != null)
                                        {
                                            _stringICM.Add(_invoiceCanvassingMonitoring.InvoiceCanvassing.Code);
                                        }
                                    }
                                }

                                IEnumerable<string> _stringArrayICMDistinct = _stringICM.Distinct();
                                string[] _stringArrayICMList = _stringArrayICMDistinct.ToArray();
                                if (_stringArrayICMList.Length == 1)
                                {
                                    for (int i = 0; i < _stringArrayICMList.Length; i++)
                                    {
                                        if (i == 0)
                                        {
                                            _beginString = "[Code]=='" + _stringArrayICMList[i] + "'";
                                        }
                                    }
                                }
                                else if (_stringArrayICMList.Length > 1)
                                {
                                    for (int i = 0; i < _stringArrayICMList.Length; i++)
                                    {
                                        if (i == 0)
                                        {
                                            _beginString = "[Code]=='" + _stringArrayICMList[i] + "'";
                                        }
                                        else
                                        {
                                            _endString = _endString + " OR [Code]=='" + _stringArrayICMList[i] + "'";
                                        }
                                    }
                                }
                                _fullString = _beginString + _endString;

                                if (_fullString != null)
                                {
                                    _availableInvoiceCanvassing = new XPCollection<InvoiceCanvassing>(Session, CriteriaOperator.Parse(_fullString));
                                }
                            }
                        }
                        else
                        {
                            if(this.Company != null && this.Workplace != null)
                            {
                                _availableInvoiceCanvassing = new XPCollection<InvoiceCanvassing>(Session,
                                                            new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("Company", this.Company),
                                                            new BinaryOperator("Workplace", this.Workplace),
                                                            new BinaryOperator("Status", Status.Posted)));
                            }
                            else
                            {
                                _availableInvoiceCanvassing = new XPCollection<InvoiceCanvassing>(Session, 
                                                            new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("Status", Status.Posted)));
                            }
                            
                        }
                        
                    }
                }

                return _availableInvoiceCanvassing;
            }
        }

        [ImmediatePostData()]
        [RuleRequiredField(DefaultContexts.Save)]
        [DataSourceProperty("AvailableInvoiceCanvassing", DataSourcePropertyIsNullMode.SelectNothing)]
        public InvoiceCanvassing InvoiceCanvassing
        {
            get { return _invoiceCanvassing; }
            set {
                SetPropertyValue("InvoiceCanvassing", ref _invoiceCanvassing, value);
                if(!IsLoading)
                {
                    if(this._invoiceCanvassing != null)
                    {
                        if(this._invoiceCanvassing.Customer != null)
                        {
                            this.Customer = this._invoiceCanvassing.Customer;
                        }
                        else
                        {
                            this.Customer = null;
                            this.Contact = null;
                            this.Address = null;
                        } 
                    }
                    else
                    {
                        this.Customer = null;
                        this.Contact = null;
                        this.Address = null;
                    }
                }
            }
        }

        [ImmediatePostData()]
        [Appearance("InvoiceCanvassingCollectionCustomerClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public BusinessPartner Customer
        {
            get { return _customer; }
            set
            {
                SetPropertyValue("Customer", ref _customer, value);
                if (!IsLoading)
                {
                    if (this._customer != null)
                    {
                        if (this._customer.Contact != null)
                        {
                            this.Contact = this._customer.Contact;
                        }
                        if (this._customer.Address != null)
                        {
                            this.Address = this._customer.Address;
                        }
                    }
                    else
                    {
                        this.Contact = null;
                        this.Address = null;
                    }
                }
            }
        }

        [Appearance("InvoiceCanvassingCollectionContactClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public string Contact
        {
            get { return _contact; }
            set { SetPropertyValue("Contact", ref _contact, value); }
        }

        [Appearance("InvoiceCanvassingCollectionAddressClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public string Address
        {
            get { return _address; }
            set { SetPropertyValue("Address", ref _address, value); }
        }

        [Appearance("InvoiceCanvassingCollectionStatusClose", Enabled = false)]
        public Status Status
        {
            get { return _status; }
            set { SetPropertyValue("Status", ref _status, value); }
        }

        [ModelDefault("DisplayFormat", "{0:dd/MM/yyyy}")]
        [ModelDefault("EditMask", "dd/MM/yyyy")]
        [Appearance("InvoiceCanvassingCollectionStatusDateClose", Enabled = false)]
        public DateTime StatusDate
        {
            get { return _statusDate; }
            set { SetPropertyValue("StatusDate", ref _statusDate, value); }
        }

        [ImmediatePostData()]
        [RuleRequiredField(DefaultContexts.Save)]
        [Appearance("InvoiceCanvassingCollectionCanvassingCollectionClose", Enabled = false)]
        [Association("CanvassingCollection-InvoiceCanvassingCollections")]
        public CanvassingCollection CanvassingCollection
        {
            get { return _canvassingCollection; }
            set {
                SetPropertyValue("CanvassingCollection", ref _canvassingCollection, value);
                if (!IsLoading)
                {

                    if (this._canvassingCollection != null)
                    {
                        if (Session.IsNewObject(this))
                        {
                            if (this._canvassingCollection.Company != null)
                            {
                                this.Company = this._canvassingCollection.Company;
                            }
                            if (this._canvassingCollection.Workplace != null)
                            {
                                this.Workplace = this._canvassingCollection.Workplace;
                            }
                            if (this._canvassingCollection.Division != null)
                            {
                                this.Division = this._canvassingCollection.Division;
                            }
                            if (this._canvassingCollection.Department != null)
                            {
                                this.Department = this._canvassingCollection.Department;
                            }
                            if (this._canvassingCollection.Section != null)
                            {
                                this.Section = this._canvassingCollection.Section;
                            }
                            if (this._canvassingCollection.Employee != null)
                            {
                                this.Employee = this._canvassingCollection.Employee;
                            }
                            if (this._canvassingCollection.Salesman != null)
                            {
                                this.Salesman = this._canvassingCollection.Salesman;
                            }
                            if (this._canvassingCollection.Vehicle != null)
                            {
                                this.Vehicle = this._canvassingCollection.Vehicle;
                            }
                            if(this._canvassingCollection.PriceGroup != null)
                            {
                                this.PriceGroup = this._canvassingCollection.PriceGroup;
                            }
                        }
                    }
                }
            }
        }

        //AuditTrail
        [CollectionOperationSet(AllowAdd = false, AllowRemove = true)]
        public XPCollection<AuditDataItemPersistent> ChangeHistory
        {
            get
            {
                if (changeHistory == null)
                {
                    changeHistory = AuditedObjectWeakReference.GetAuditTrail(Session, this);
                }
                return changeHistory;
            }
        }

        #endregion Field
    }
}