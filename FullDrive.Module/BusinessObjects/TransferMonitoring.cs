﻿using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;

using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.ConditionalAppearance;

namespace FullDrive.Module.BusinessObjects
{
    [DefaultClassOptions]
    [DefaultProperty("Code")]
    [NavigationItem("Inventory")]
    [RuleCombinationOfPropertiesIsUnique("TransferMonitoringRuleUnique", DefaultContexts.Save, "Code")]
    //[ImageName("BO_Contact")]
    //[DefaultProperty("DisplayMemberNameForLookupEditorsOfThisType")]
    //[DefaultListViewOptions(MasterDetailMode.ListViewOnly, false, NewItemRowPosition.None)]
    //[Persistent("DatabaseTableName")]
    // Specify more UI options using a declarative approach (https://documentation.devexpress.com/#eXpressAppFramework/CustomDocument112701).
    public class TransferMonitoring : BaseObject
    { // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).

        private bool _activationPosting;
        private int _no;
        private string _code;
        private bool _select;
        #region InitialOrganization
        private Company _company;
        private Workplace _workplace;
        private Division _division;
        private Department _department;
        private Section _section;
        private Employee _employee;
        #endregion InitialOrganization
        private Transfer _transfer;
        private TransferLine _transferLine;
        #region From
        private LocationType _locationTypeFrom;
        private StockType _stockTypeFrom;
        private Workplace _workplaceFrom;
        private Location _locationFrom;
        private BinLocation _binLocationFrom;
        private StockGroup _stockGroupFrom;
        private BeginningInventory _begInvFrom;
        #endregion From
        #region To
        private LocationType _locationTypeTo;
        private StockType _stockTypeTo;
        private Workplace _workplaceTo;
        private Location _locationTo;
        private BinLocation _binLocationTo;
        private StockGroup _stockGroupTo;
        private BeginningInventory _begInvTo;
        #endregion To
        private XPCollection<Item> _availableItem;
        private Item _item;
        private Brand _brand;
        private string _description;
        private BeginningInventoryLine _begInvLineFrom;
        private XPCollection<UnitOfMeasure> _availableUnitOfMeasure;
        private BeginningInventoryLine _begInvLineTo;
        #region InitialDefaultQuantityFrom
        private double _dQty;
        private UnitOfMeasure _dUom;
        private double _qty;
        private UnitOfMeasure _uom;
        private double _tQty;
        #endregion InitialDefaultQuantityFrom
        private Status _status;
        private DateTime _statusDate;
        private string _signCode;
        private string _userAccess;
        private string _localUserAccess;
        private GlobalFunction _globFunc;
        //AuditTrail
        private XPCollection<AuditDataItemPersistent> changeHistory;

        public TransferMonitoring(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).
            if (!IsLoading)
            {
                if (Session.IsNewObject(this))
                {
                    _globFunc = new GlobalFunction();
                    DateTime now = DateTime.Now;
                    #region UserAccess
                    _localUserAccess = SecuritySystem.CurrentUserName;
                    UserAccess _locUserAccess = Session.FindObject<UserAccess>(new BinaryOperator("UserName", _localUserAccess));
                    if (_locUserAccess != null)
                    {
                        if (_locUserAccess.Employee != null)
                        {
                            if (_locUserAccess.Employee.Company != null && _locUserAccess.Employee.Workplace != null)
                            {
                                this.Code = _globFunc.GetNumberingUnlockOptimisticRecordBasedOrganization(this.Session.DataLayer, ObjectList.TransferLine, _locUserAccess.Employee.Company, _locUserAccess.Employee.Workplace);
                            }
                            else
                            {
                                this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.TransferLine);
                            }
                        }
                    }
                    #endregion UserAccess
                    this.Status = CustomProcess.Status.Open;
                    this.StatusDate = now;
                    this.Select = true;
                }
            }
        }


        #region Field

        [Browsable(false)]
        [ImmediatePostData()]
        public bool ActivationPosting
        {
            get { return _activationPosting; }
            set { SetPropertyValue("ActivationPosting", ref _activationPosting, value); }
        }

        [Appearance("TransferMonitoringNoClose", Enabled = false)]
        public int No
        {
            get { return _no; }
            set { SetPropertyValue("No", ref _no, value); }
        }

        [VisibleInListView(false)]
        [RuleRequiredField(DefaultContexts.Save)]
        [Appearance("TransferMonitoringCodeClose", Enabled = false)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue("Code", ref _code, value); }
        }

        [Appearance("TransferMonitoringSelectClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public bool Select
        {
            get { return _select; }
            set { SetPropertyValue("Select", ref _select, value); }
        }

        #region Organization

        [VisibleInListView(false)]
        [Appearance("TransferMonitoringCompanyClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Company Company
        {
            get { return _company; }
            set { SetPropertyValue("Company", ref _company, value); }
        }

        [VisibleInListView(false)]
        [Appearance("TransferMonitoringWorkplaceClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Workplace Workplace
        {
            get { return _workplace; }
            set { SetPropertyValue("Workplace", ref _workplace, value); }
        }

        [Browsable(false)]
        public Division Division
        {
            get { return _division; }
            set { SetPropertyValue("Division", ref _division, value); }
        }

        [Browsable(false)]
        public Department Department
        {
            get { return _department; }
            set { SetPropertyValue("Department", ref _department, value); }
        }

        [Browsable(false)]
        public Section Section
        {
            get { return _section; }
            set { SetPropertyValue("Section", ref _section, value); }
        }

        [Browsable(false)]
        public Employee Employee
        {
            get { return _employee; }
            set { SetPropertyValue("Employee", ref _employee, value); }
        }

        #endregion Organization

        [Appearance("TransferMonitoringTransferClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Transfer Transfer
        {
            get { return _transfer; }
            set { SetPropertyValue("Transfer", ref _transfer, value); }
        }

        [Appearance("TransferMonitoringTransferLineClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public TransferLine TransferLine
        {
            get { return _transferLine; }
            set { SetPropertyValue("TransferLine", ref _transferLine, value); }
        }

        #region From

        [ImmediatePostData()]
        [Appearance("TransferMonitoringLocationTypeFromClose", Enabled = false)]
        public LocationType LocationTypeFrom
        {
            get { return _locationTypeFrom; }
            set { SetPropertyValue("LocationTypeFrom", ref _locationTypeFrom, value); }
        }

        [Appearance("TransferMonitoringStockTypeFromClose", Enabled = false)]
        public StockType StockTypeFrom
        {
            get { return _stockTypeFrom; }
            set { SetPropertyValue("StockTypeFrom", ref _stockTypeFrom, value); }
        }

        [ImmediatePostData()]
        [Appearance("TransferMonitoringWorkplaceFromClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Workplace WorkplaceFrom
        {
            get { return _workplaceFrom; }
            set { SetPropertyValue("WorkplaceFrom", ref _workplaceFrom, value); }
        }

        [ImmediatePostData()]
        [Appearance("TransferMonitoringLocationFromClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [DataSourceCriteria("Active = true")]
        public Location LocationFrom
        {
            get { return _locationFrom; }
            set
            {
                SetPropertyValue("LocationFrom", ref _locationFrom, value);
                if (!IsLoading)
                {
                    if (this._locationFrom != null)
                    {
                        SetBegInvFrom(_locationFrom);
                        SetBegInvLineFrom();
                    }
                    else
                    {
                        BegInvFrom = null;
                        BegInvLineFrom = null;
                    }
                }
            }
        }

        [VisibleInListView(false)]
        [ImmediatePostData()]
        [Appearance("TransferMonitoringBinLocationFromClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [DataSourceCriteria("Active = true")]
        public BinLocation BinLocationFrom
        {
            get { return _binLocationFrom; }
            set
            {
                SetPropertyValue("BinLocationFrom", ref _binLocationFrom, value);
                if (!IsLoading)
                {
                    if (this._binLocationFrom != null)
                    {
                        SetBegInvLineFrom();
                    }
                    else
                    {
                        this.BegInvLineFrom = null;
                    }
                }
            }
        }

        [Appearance("TransferMonitoringStockGroupFromClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public StockGroup StockGroupFrom
        {
            get { return _stockGroupFrom; }
            set { SetPropertyValue("StockGroupFrom", ref _stockGroupFrom, value); }
        }

        [Browsable(false)]
        [ImmediatePostData()]
        [Appearance("TransferMonitoringBegInvFromClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public BeginningInventory BegInvFrom
        {
            get { return _begInvFrom; }
            set { SetPropertyValue("BegInvFrom", ref _begInvFrom, value); }
        }

        #endregion From

        #region To

        [ImmediatePostData()]
        [Appearance("TransferMonitoringLocationTypeToClose", Enabled = false)]
        public LocationType LocationTypeTo
        {
            get { return _locationTypeTo; }
            set { SetPropertyValue("LocationTypeTo", ref _locationTypeTo, value); }
        }

        [Appearance("TransferMonitoringStockTypeToClose", Enabled = false)]
        public StockType StockTypeTo
        {
            get { return _stockTypeTo; }
            set { SetPropertyValue("StockTypeTo", ref _stockTypeTo, value); }
        }

        [ImmediatePostData()]
        [Appearance("TransferMonitoringWorkplaceToClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Workplace WorkplaceTo
        {
            get { return _workplaceTo; }
            set { SetPropertyValue("WorkplaceTo", ref _workplaceTo, value); }
        }

        [ImmediatePostData]
        [Appearance("TransferMonitoringLocationToClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [DataSourceCriteria("Active = true")]
        public Location LocationTo
        {
            get { return _locationTo; }
            set
            {
                SetPropertyValue("LocationTo", ref _locationTo, value);
                if (!IsLoading)
                {
                    if (this._locationTo != null)
                    {
                        SetBegInvTo(_locationTo);
                    }
                    else
                    {
                        BegInvTo = null;
                    }
                }
            }
        }

        [VisibleInListView(false)]
        [Appearance("TransferMonitoringBinLocationToClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [DataSourceCriteria("Active = true")]
        public BinLocation BinLocationTo
        {
            get { return _binLocationTo; }
            set { SetPropertyValue("BinLocationTo", ref _binLocationTo, value); }
        }

        [Appearance("TransferMonitoringStockGroupToClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public StockGroup StockGroupTo
        {
            get { return _stockGroupTo; }
            set { SetPropertyValue("StockGroupTo", ref _stockGroupTo, value); }
        }

        [Browsable(false)]
        [ImmediatePostData()]
        [Appearance("TransferMonitoringBegInvToClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public BeginningInventory BegInvTo
        {
            get { return _begInvTo; }
            set { SetPropertyValue("BegInvTo", ref _begInvTo, value); }
        }

        #endregion To

        [Browsable(false)]
        public XPCollection<Item> AvailableItem
        {
            get
            {
                string _beginString = null;
                string _endString = null;
                string _fullString = null;
                //&& this.BegInv != null
                if (this.Company != null && this.Workplace != null)
                {
                    List<string> _stringITM = new List<string>();

                    XPQuery<BeginningInventoryLine> _beginningInventoryLinesQuery = new XPQuery<BeginningInventoryLine>(Session);

                    var _beginningInventoryLines = from bil in _beginningInventoryLinesQuery
                                                   where (bil.Company == this.Company
                                                   && bil.Workplace == this.Workplace
                                                   && bil.BeginningInventory == this.BegInvFrom
                                                   && bil.QtyAvailable > 0
                                                   && bil.LocationType == this.LocationTypeFrom
                                                   && bil.StockType == this.StockTypeFrom
                                                   && bil.Lock == false
                                                   && bil.Active == true
                                                   )
                                                   group bil by bil.Item into g
                                                   select new { Item = g.Key };
                    if (_beginningInventoryLines != null && _beginningInventoryLines.Count() > 0)
                    {
                        foreach (var _beginningInventoryLine in _beginningInventoryLines)
                        {
                            _stringITM.Add(_beginningInventoryLine.Item.Code);
                        }
                    }

                    IEnumerable<string> _stringArrayITMDistinct = _stringITM.Distinct();
                    string[] _stringArrayITMList = _stringArrayITMDistinct.ToArray();
                    if (_stringArrayITMList.Length == 1)
                    {
                        for (int i = 0; i < _stringArrayITMList.Length; i++)
                        {
                            Item _locITM = Session.FindObject<Item>(new BinaryOperator("Code", _stringArrayITMList[i]));
                            if (_locITM != null)
                            {
                                if (i == 0)
                                {
                                    _beginString = "[Code]=='" + _locITM.Code + "'";
                                }
                            }
                        }
                    }
                    else if (_stringArrayITMList.Length > 1)
                    {
                        for (int i = 0; i < _stringArrayITMList.Length; i++)
                        {
                            Item _locITM = Session.FindObject<Item>(new BinaryOperator("Code", _stringArrayITMList[i]));
                            if (_locITM != null)
                            {
                                if (i == 0)
                                {
                                    _beginString = "[Code]=='" + _locITM.Code + "'";
                                }
                                else
                                {
                                    _endString = _endString + " OR [Code]=='" + _locITM.Code + "'";
                                }
                            }
                        }
                    }
                    _fullString = _beginString + _endString;

                    if (_fullString != null)
                    {
                        _availableItem = new XPCollection<Item>(Session, CriteriaOperator.Parse(_fullString));
                    }
                }
                else
                {
                    _availableItem = new XPCollection<Item>(Session, new GroupOperator(GroupOperatorType.And,
                                                    new BinaryOperator("Company", this.Company),                 
                                                    new BinaryOperator("Active", true)));
                }

                return _availableItem;

            }
        }

        [ImmediatePostData()]
        [DataSourceProperty("AvailableItem")]
        [Appearance("TransferMonitoringItemClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Item Item
        {
            get { return _item; }
            set
            {
                SetPropertyValue("Item", ref _item, value);
                if (!IsLoading)
                {
                    if (this._item != null)
                    {
                        this.DUOM = this._item.BasedUOM;
                        this.Brand = this._item.Brand;
                        this.Description = GetDescriptions();
                        SetBegInvLineFrom();
                    }
                    else
                    {
                        this.DUOM = null;
                        this.Brand = null;
                        this.Description = null;
                        this.BegInvLineFrom = null;
                    }
                }
            }
        }

        [VisibleInListView(false)]
        [Appearance("TransferMonitoringBrandClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Brand Brand
        {
            get { return _brand; }
            set { SetPropertyValue("Brand", ref _brand, value); }
        }

        [VisibleInListView(false)]
        [Appearance("TransferMonitoringDescriptionClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [Size(512)]
        public string Description
        {
            get { return _description; }
            set { SetPropertyValue("Description", ref _description, value); }
        }

        //[Browsable(false)]
        [ImmediatePostData()]
        [Appearance("TransferMonitoringBegInvLineFromClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public BeginningInventoryLine BegInvLineFrom
        {
            get { return _begInvLineFrom; }
            set { SetPropertyValue("BegInvLineFrom", ref _begInvLineFrom, value); }
        }

        //[Browsable(false)]
        [ImmediatePostData()]
        [Appearance("TransferMonitoringBegInvLineToClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public BeginningInventoryLine BegInvLineTo
        {
            get { return _begInvLineTo; }
            set { SetPropertyValue("BegInvLineTo", ref _begInvLineTo, value); }
        }

        [Browsable(false)]
        public XPCollection<UnitOfMeasure> AvailableUnitOfMeasure
        {
            get
            {
                string _beginString = null;
                string _endString = null;
                string _fullString = null;
                if (Item == null)
                {
                    _availableUnitOfMeasure = new XPCollection<UnitOfMeasure>(Session);
                }
                else
                {
                    List<string> _stringUOM = new List<string>();

                    XPCollection<ItemUnitOfMeasure> _locItemUnitOfMeasures = new XPCollection<ItemUnitOfMeasure>
                                                                              (Session, new GroupOperator(GroupOperatorType.And,
                                                                               new BinaryOperator("Item", this.Item),
                                                                               new BinaryOperator("Active", true)));

                    if (_locItemUnitOfMeasures != null && _locItemUnitOfMeasures.Count() > 0)
                    {
                        foreach (ItemUnitOfMeasure _locItemUnitOfMeasure in _locItemUnitOfMeasures)
                        {
                            _stringUOM.Add(_locItemUnitOfMeasure.UOM.Code);
                        }
                    }

                    IEnumerable<string> _stringArrayUOMDistinct = _stringUOM.Distinct();
                    string[] _stringArrayUOMList = _stringArrayUOMDistinct.ToArray();
                    if (_stringArrayUOMList.Length == 1)
                    {
                        for (int i = 0; i < _stringArrayUOMList.Length; i++)
                        {
                            UnitOfMeasure _locUOM = Session.FindObject<UnitOfMeasure>(new BinaryOperator("Code", _stringArrayUOMList[i]));
                            if (_locUOM != null)
                            {
                                if (i == 0)
                                {
                                    _beginString = "[Code]=='" + _locUOM.Code + "'";
                                }
                            }
                        }
                    }
                    else if (_stringArrayUOMList.Length > 1)
                    {
                        for (int i = 0; i < _stringArrayUOMList.Length; i++)
                        {
                            UnitOfMeasure _locUOM = Session.FindObject<UnitOfMeasure>(new BinaryOperator("Code", _stringArrayUOMList[i]));
                            if (_locUOM != null)
                            {
                                if (i == 0)
                                {
                                    _beginString = "[Code]=='" + _locUOM.Code + "'";
                                }
                                else
                                {
                                    _endString = " OR [Code]=='" + _locUOM.Code + "'";
                                }
                            }
                        }
                    }
                    _fullString = _beginString + _endString;

                    if (_fullString != null)
                    {
                        _availableUnitOfMeasure = new XPCollection<UnitOfMeasure>(Session, CriteriaOperator.Parse(_fullString));
                    }

                }
                return _availableUnitOfMeasure;

            }
        }

        #region DefaultQty

        [Appearance("TransferMonitoringDQtyClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [ImmediatePostData()]
        public double DQty
        {
            get { return _dQty; }
            set
            {
                SetPropertyValue("DQty", ref _dQty, value);
                if (!IsLoading)
                {
                    SetTotalQty();
                }
            }
        }

        [Appearance("TransferMonitoringDUOMClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [ImmediatePostData()]
        [DataSourceCriteria("Active = true")]
        public UnitOfMeasure DUOM
        {
            get { return _dUom; }
            set
            {
                SetPropertyValue("DUOM", ref _dUom, value);
                if (!IsLoading)
                {
                    SetTotalQty();
                }
            }
        }

        [Appearance("TransferMonitoringQtyClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [ImmediatePostData()]
        public double Qty
        {
            get { return _qty; }
            set
            {
                SetPropertyValue("Qty", ref _qty, value);
                if (!IsLoading)
                {
                    SetTotalQty();
                }
            }
        }

        [ImmediatePostData()]
        [Appearance("TransferMonitoringUOMClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [DataSourceCriteria("Active = true")]
        public UnitOfMeasure UOM
        {
            get { return _uom; }
            set
            {
                SetPropertyValue("UOM", ref _uom, value);
                if (!IsLoading)
                {
                    SetTotalQty();
                }
            }
        }

        [Appearance("TransferLineTQtyEnabled", Enabled = false)]
        public double TQty
        {
            get { return _tQty; }
            set { SetPropertyValue("TQtyFrom", ref _tQty, value); }
        }

        #endregion DefaultQty

        [VisibleInListView(false)]
        [Appearance("TransferLineStatusClose", Enabled = false)]
        public Status Status
        {
            get { return _status; }
            set { SetPropertyValue("Status", ref _status, value); }
        }

        [VisibleInListView(false)]
        [ModelDefault("DisplayFormat", "{0:dd/MM/yyyy}")]
        [ModelDefault("EditMask", "dd/MM/yyyy")]
        [Appearance("TransferLineStatusDateClose", Enabled = false)]
        public DateTime StatusDate
        {
            get { return _statusDate; }
            set { SetPropertyValue("StatusDate", ref _statusDate, value); }
        }

        [Browsable(false)]
        [Appearance("TransferLineSignCodeClose", Enabled = false)]
        public string SignCode
        {
            get { return _signCode; }
            set { SetPropertyValue("SignCode", ref _signCode, value); }
        }

        //AuditTrail
        [CollectionOperationSet(AllowAdd = false, AllowRemove = true)]
        public XPCollection<AuditDataItemPersistent> ChangeHistory
        {
            get
            {
                if (changeHistory == null)
                {
                    changeHistory = AuditedObjectWeakReference.GetAuditTrail(Session, this);
                }
                return changeHistory;
            }
        }

        #endregion Field

        //========================================== Code In Here ==========================================

        #region Get Description

        public string GetDescriptions()
        {
            string _result = "";
            try
            {
                if (this._item != null)
                {
                    string _result1 = null;
                    string _result2 = null;
                    if (this._item.Name != null)
                    {
                        _result1 = this._item.Name;
                    }
                    if (this._item.Description != null)
                    {
                        _result2 = this._item.Description;
                    }
                    _result = _result1 + " " + _result2;
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = TransferLine " + ex.ToString());
            }

            return _result;
        }

        #endregion Get Description

        #region Set

        private void SetTotalQty()
        {
            try
            {
                double _locInvLineTotal = 0;
                DateTime now = DateTime.Now;
                if (this.Transfer != null)
                {
                    if (this.Item != null && this.UOM != null && this.DUOM != null)
                    {
                        ItemUnitOfMeasure _locItemUOM = Session.FindObject<ItemUnitOfMeasure>
                                                        (new GroupOperator(GroupOperatorType.And,
                                                         new BinaryOperator("Item", this.Item),
                                                         new BinaryOperator("UOM", this.UOM),
                                                         new BinaryOperator("DefaultUOM", this.DUOM),
                                                         new BinaryOperator("Active", true)));
                        if (_locItemUOM != null)
                        {
                            if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                            {
                                _locInvLineTotal = this.Qty * _locItemUOM.DefaultConversion + this.DQty;
                            }
                            else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                            {
                                _locInvLineTotal = this.Qty / _locItemUOM.Conversion + this.DQty;
                            }
                            else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                            {
                                _locInvLineTotal = this.Qty + this.DQty;
                            }

                            this.TQty = _locInvLineTotal;
                        }
                    }
                    else
                    {
                        _locInvLineTotal = this.Qty + this.DQty;
                        this.TQty = _locInvLineTotal;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = TransferLine " + ex.ToString());
            }
        }

        private void SetBegInvFrom(Location _locLocation)
        {
            try
            {
                DateTime now = DateTime.Now;
                if (this.Company != null && this.WorkplaceFrom != null)
                {
                    BeginningInventory _locBegInvFrom = Session.FindObject<BeginningInventory>
                                                        (new GroupOperator(GroupOperatorType.And,
                                                        new BinaryOperator("Company", this.Company),
                                                        new BinaryOperator("Workplace", this.WorkplaceFrom),
                                                        new BinaryOperator("Location", _locLocation),
                                                        new BinaryOperator("LocationType", this.LocationTypeFrom),
                                                        new BinaryOperator("StockType", this.StockTypeFrom),
                                                        new BinaryOperator("Active", true)));
                    if (_locBegInvFrom != null)
                    {
                        this.BegInvFrom = _locBegInvFrom;
                    }
                    else
                    {
                        this.BegInvFrom = null;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Business Object = TransferLine ", ex.ToString());
            }
        }

        private void SetBegInvTo(Location _locLocation)
        {
            try
            {
                DateTime now = DateTime.Now;
                if (this.Company != null && this.WorkplaceFrom != null)
                {
                    BeginningInventory _locBegInvTo = Session.FindObject<BeginningInventory>
                                                        (new GroupOperator(GroupOperatorType.And,
                                                        new BinaryOperator("Company", this.Company),
                                                        new BinaryOperator("Workplace", this.WorkplaceFrom),
                                                        new BinaryOperator("Location", _locLocation),
                                                        new BinaryOperator("LocationType", this.LocationTypeTo),
                                                        new BinaryOperator("StockType", this.StockTypeTo),
                                                        new BinaryOperator("Active", true)));
                    if (_locBegInvTo != null)
                    {
                        this.BegInvTo = _locBegInvTo;
                    }
                    else
                    {
                        this.BegInvTo = null;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Business Object = TransferLine ", ex.ToString());
            }
        }

        private void SetBegInvLineFrom()
        {
            try
            {
                DateTime now = DateTime.Now;
                BeginningInventoryLine _locBegInvLine = null;
                if (this.Company != null && this.WorkplaceFrom != null && this.Item != null && this.BegInvFrom != null && this.LocationFrom != null)
                {
                    if (this.BinLocationFrom != null)
                    {
                        //BinLocation
                        _locBegInvLine = Session.FindObject<BeginningInventoryLine>(
                                   new GroupOperator(GroupOperatorType.And,
                                   new BinaryOperator("Company", this.Company),
                                   new BinaryOperator("Workplace", this.WorkplaceFrom),
                                   new BinaryOperator("BeginningInventory", this.BegInvFrom),
                                   new BinaryOperator("Location", this.LocationFrom),
                                   new BinaryOperator("Item", this.Item),
                                   new BinaryOperator("BinLocation", this.BinLocationFrom),
                                   new BinaryOperator("LocationType", this.LocationTypeFrom),
                                   new BinaryOperator("StockType", this.StockTypeFrom),
                                   new BinaryOperator("StockGroup", this.StockGroupFrom),
                                   new BinaryOperator("Lock", false),
                                   new BinaryOperator("Active", true)));
                    }
                    else
                    {
                        _locBegInvLine = Session.FindObject<BeginningInventoryLine>(
                                    new GroupOperator(GroupOperatorType.And,
                                    new BinaryOperator("Company", this.Company),
                                    new BinaryOperator("Workplace", this.WorkplaceFrom),
                                    new BinaryOperator("BeginningInventory", this.BegInvFrom),
                                    new BinaryOperator("Location", this.LocationFrom),
                                    new BinaryOperator("Item", this.Item),
                                    new BinaryOperator("LocationType", this.LocationTypeFrom),
                                    new BinaryOperator("StockType", this.StockTypeFrom),
                                    new BinaryOperator("StockGroup", this.StockGroupFrom),
                                    new BinaryOperator("Lock", false),
                                    new BinaryOperator("Active", true)));
                    }

                    if (_locBegInvLine != null)
                    {
                        this.BegInvLineFrom = _locBegInvLine;
                    }
                    else
                    {
                        this.BegInvLineFrom = null;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Business Object = TransferLine ", ex.ToString());
            }
        }

        #endregion Set

    }
}