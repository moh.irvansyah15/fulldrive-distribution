﻿using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;

using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.ConditionalAppearance;

namespace FullDrive.Module.BusinessObjects
{
    [DefaultClassOptions]
    [DefaultProperty("FullName")]
    [NavigationItem("Inventory")]
    [RuleCombinationOfPropertiesIsUnique("BeginingInventoryLineRuleUnique", DefaultContexts.Save, "Code")]
    //[ImageName("BO_Contact")]
    //[DefaultProperty("DisplayMemberNameForLookupEditorsOfThisType")]
    //[DefaultListViewOptions(MasterDetailMode.ListViewOnly, false, NewItemRowPosition.None)]
    //[Persistent("DatabaseTableName")]
    // Specify more UI options using a declarative approach (https://documentation.devexpress.com/#eXpressAppFramework/CustomDocument112701).
    public class BeginningInventoryLine : BaseObject
    { // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).

        private string _code;
        private string _name;
        private Company _company;
        private Workplace _workplace;
        private Location _location;
        private XPCollection<BinLocation> _availableBinLocation;
        private BinLocation _binLocation;
        private Vehicle _vehicle;
        private Item _item;
        private OrderType _itemType;
        private BusinessPartner _principal;
        private Brand _brand;
        private string _description;
        private double _qtyBegining;
        private double _qtyMinimal;
        private double _qtyAvailable;
        private double _qtyOnHand;
        private double _qtySale;
        private UnitOfMeasure _defaultUom;
        private LocationType _locationType;
        private StockType _stockType;
        private StockGroup _stockGroup;
        private string _lotNumber;
        private bool _lock;
        private bool _active;
        private string _signCode;
        private BeginningInventory _beginningInventory;
        private string _userAccess;
        private GlobalFunction _globFunc;
        //AuditTrail
        private XPCollection<AuditDataItemPersistent> changeHistory;

        public BeginningInventoryLine(Session session)
            : base(session)
        {
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if (!IsLoading)
            {
                _globFunc = new GlobalFunction();
                #region UserAccess
                _userAccess = SecuritySystem.CurrentUserName;
                UserAccess _locUserAccess = Session.FindObject<UserAccess>(new BinaryOperator("UserName", _userAccess));
                if (Session.IsNewObject(this))
                {
                    if (_locUserAccess != null)
                    {
                        if (_locUserAccess.Employee != null)
                        {
                            if (_locUserAccess.Employee.Company != null && _locUserAccess.Employee.Workplace != null)
                            {
                                this.Code = _globFunc.GetNumberingUnlockOptimisticRecordBasedOrganization(this.Session.DataLayer, ObjectList.BeginningInventoryLine, _locUserAccess.Employee.Company, _locUserAccess.Employee.Workplace);
                            }
                            else
                            {
                                this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.BeginningInventoryLine);
                            }
                        }
                    }
                }
                #endregion UserAccess
                this.Active = true;
                this.Lock = false;
            }
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).
        }

        #region Field

        [RuleRequiredField(DefaultContexts.Save)]
        [Appearance("BeginningInventoryLineCodeEnabled", Enabled = false)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue("Code", ref _code, value); }
        }

        [Appearance("BeginningInventoryLineNameEnabled", Enabled = false)]
        public string Name
        {
            get { return _name; }
            set { SetPropertyValue("Name", ref _name, value); }
        }

        [RuleRequiredField(DefaultContexts.Save)]
        [Appearance("BeginningInventoryLineCompanyEnabled", Enabled = false)]
        [DataSourceCriteria("Active = true")]
        public Company Company
        {
            get { return _company; }
            set { SetPropertyValue("Company", ref _company, value); }
        }

        [Appearance("BeginningInventoryLineWorkplaceEnabled", Enabled = false)]
        [DataSourceCriteria("Active = true")]
        public Workplace Workplace
        {
            get { return _workplace; }
            set { SetPropertyValue("Workplace", ref _workplace, value); }
        }

        [ImmediatePostData()]
        [DataSourceCriteria("Active = true")]
        public Location Location
        {
            get { return _location; }
            set { SetPropertyValue("Location", ref _location, value); }
        }

        [Browsable(false)]
        public XPCollection<BinLocation> AvailableBinLocation
        {

            get
            {
                if (this.Location != null)
                {
                    _availableBinLocation = new XPCollection<BinLocation>
                                        (Session, new GroupOperator(GroupOperatorType.And,
                                        new BinaryOperator("Location", this.Location),
                                        new BinaryOperator("Active", true)));


                }
                else
                {
                    _availableBinLocation = new XPCollection<BinLocation>
                                        (Session, new GroupOperator(GroupOperatorType.And,
                                        new BinaryOperator("Active", true)));
                }

                return _availableBinLocation;

            }
        }

        [DataSourceProperty("AvailableBinLocation", DataSourcePropertyIsNullMode.SelectAll)]
        public BinLocation BinLocation
        {
            get { return _binLocation; }
            set { SetPropertyValue("BinLocation", ref _binLocation, value); }
        }

        [Browsable(false)]
        [ImmediatePostData()]
        public Vehicle Vehicle
        {
            get { return _vehicle; }
            set { SetPropertyValue("Vehicle", ref _vehicle, value); }
        }

        [Persistent]
        public string FullName
        {
            get
            {
                string _locName = null;
                if (this.Item != null || this.LotNumber != null)
                {
                    if (this.Item.Name != null)
                    {
                        _locName = this.Item.Name;
                    }
                    return String.Format("({0})-({1})-({2})", _locName, this.LotNumber, this.QtyAvailable);
                }
                return null;
            }
        }

        [ImmediatePostData()]
        public Item Item
        {
            get { return _item; }
            set
            {
                SetPropertyValue("Item", ref _item, value);
                if (!IsLoading)
                {
                    if (this._item != null)
                    {
                        if (this._item.BasedUOM != null)
                        {
                            this.DefaultUOM = this._item.BasedUOM;
                        }
                        if (this._item.Principal != null)
                        {
                            this.Principal = this._item.Principal;
                        }
                        if (this._item.Brand != null)
                        {
                            this.Brand = this._item.Brand;
                        }
                        if(this._item.ItemType != OrderType.None)
                        {
                            this.ItemType = this._item.ItemType;
                        }
                        if(this._item.Name != null)
                        {
                            this.Name = this._item.Name;
                        }
                    }
                    else
                    {
                        this.DefaultUOM = null;
                        this.Principal = null;
                        this.Brand = null;
                        this.ItemType = OrderType.None;
                        this.Name = null;
                    }
                }
            }
        }

        public OrderType ItemType
        {
            get { return _itemType; }
            set { SetPropertyValue("ItemType", ref _itemType, value); }
        }

        public BusinessPartner Principal
        {
            get { return _principal; }
            set { SetPropertyValue("Principal", ref _principal, value); }
        }

        public Brand Brand
        {
            get { return _brand; }
            set { SetPropertyValue("Brand", ref _brand, value); }
        }

        public string Description
        {
            get { return _description; }
            set { SetPropertyValue("Description", ref _description, value); }
        }

        public double QtyBegining
        {
            get { return _qtyBegining; }
            set { SetPropertyValue("QtyBegining", ref _qtyBegining, value); }
        }

        public double QtyMinimal
        {
            get { return _qtyMinimal; }
            set { SetPropertyValue("QtyMinimal", ref _qtyMinimal, value); }
        }

        [ImmediatePostData()]
        public double QtyAvailable
        {
            get { return _qtyAvailable; }
            set { SetPropertyValue("QtyAvailable", ref _qtyAvailable, value); }
        }

        public double QtyOnHand
        {
            get { return _qtyOnHand; }
            set { SetPropertyValue("QtyOnHand", ref _qtyOnHand, value); }
        }

        public double QtySale
        {
            get { return _qtySale; }
            set { SetPropertyValue("QtySale", ref _qtySale, value); }
        }

        [Appearance("BeginningInventoryLineDefaultUOMClose",Enabled = false)]
        [DataSourceCriteria("Active = true")]
        public UnitOfMeasure DefaultUOM
        {
            get { return _defaultUom; }
            set { SetPropertyValue("DefaultUOM", ref _defaultUom, value); }
        }

        public LocationType LocationType
        {
            get { return _locationType; }
            set { SetPropertyValue("LocationType", ref _locationType, value); }
        }

        public StockType StockType
        {
            get { return _stockType; }
            set { SetPropertyValue("StockType", ref _stockType, value); }
        }

        public StockGroup StockGroup
        {
            get { return _stockGroup; }
            set { SetPropertyValue("StockGroup", ref _stockGroup, value); }
        }

        [ImmediatePostData()]
        public string LotNumber
        {
            get { return _lotNumber; }
            set { SetPropertyValue("LotNumber", ref _lotNumber, value); }
        }

        public bool Lock
        {
            get { return _lock; }
            set { SetPropertyValue("Lock", ref _lock, value); }
        }

        public bool Active
        {
            get { return _active; }
            set { SetPropertyValue("Active", ref _active, value); }
        }

        [Browsable(false)]
        public string SignCode
        {
            get { return _signCode; }
            set { SetPropertyValue("SignCode", ref _signCode, value); }
        }

        [ImmediatePostData()]
        [RuleRequiredField(DefaultContexts.Save)]
        [Association("BeginningInventory-BeginningInventoryLines")]
        public BeginningInventory BeginningInventory
        {
            get { return _beginningInventory; }
            set
            {
                SetPropertyValue("BeginningInventory", ref _beginningInventory, value);
                if (!IsLoading)
                {
                    if (this._beginningInventory != null)
                    {
                        if (this._beginningInventory.Company != null) { this.Company = this._beginningInventory.Company; }
                        if (this._beginningInventory.Workplace != null) { this.Workplace = this._beginningInventory.Workplace; }
                        if (this._beginningInventory.Location != null) { this.Location = this._beginningInventory.Location; }
                        if (this._beginningInventory.Vehicle != null) { this.Vehicle = this._beginningInventory.Vehicle; }
                        this.LocationType = this._beginningInventory.LocationType;
                        this.StockType = this._beginningInventory.StockType;
                    }else
                    {
                        this.Company = null;
                        this.Workplace = null;
                        this.Location = null;
                        this.Vehicle = null;
                        this.LocationType = LocationType.None;
                        this.StockType = StockType.None;
                    }
                }
            }
        }

        //AuditTrail
        [CollectionOperationSet(AllowAdd = false, AllowRemove = true)]
        public XPCollection<AuditDataItemPersistent> ChangeHistory
        {
            get
            {
                if (changeHistory == null)
                {
                    changeHistory = AuditedObjectWeakReference.GetAuditTrail(Session, this);
                }
                return changeHistory;
            }
        }

        #endregion Field
    }
}