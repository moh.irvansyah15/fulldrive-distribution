﻿using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;

using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.ConditionalAppearance;

namespace FullDrive.Module.BusinessObjects
{
    [DefaultClassOptions]
    [DefaultProperty("Code")]
    [NavigationItem("Inventory")]
    [RuleCombinationOfPropertiesIsUnique("TransferLineRuleUnique", DefaultContexts.Save, "Code")]
    //[ImageName("BO_Contact")]
    //[DefaultProperty("DisplayMemberNameForLookupEditorsOfThisType")]
    //[DefaultListViewOptions(MasterDetailMode.ListViewOnly, false, NewItemRowPosition.None)]
    //[Persistent("DatabaseTableName")]
    // Specify more UI options using a declarative approach (https://documentation.devexpress.com/#eXpressAppFramework/CustomDocument112701).
    public class TransferLine : BaseObject
    { // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).
        private bool _activationPosting;
        private int _no;
        private string _code;
        private bool _select;
        #region InitialOrganization
        private Company _company;
        private Workplace _workplace;
        private Division _division;
        private Department _department;
        private Section _section;
        private Employee _employee;
        #endregion InitialOrganization
        #region From
        private LocationType _locationTypeFrom;
        private StockType _stockTypeFrom;
        private XPCollection<Workplace> _availableWorkplaceFrom;
        private Workplace _workplaceFrom;
        private XPCollection<Location> _availableLocationFrom;
        private Location _locationFrom;
        private XPCollection<BinLocation> _availableBinLocationFrom;
        private BinLocation _binLocationFrom;
        private StockGroup _stockGroupFrom;
        private BeginningInventory _begInvFrom;
        #endregion From
        #region To
        private LocationType _locationTypeTo;
        private StockType _stockTypeTo;
        private XPCollection<Workplace> _availableWorkplaceTo;
        private Workplace _workplaceTo;
        private XPCollection<Location> _availableLocationTo;
        private Location _locationTo;
        private XPCollection<BinLocation> _availableBinLocationTo;
        private BinLocation _binLocationTo;
        private StockGroup _stockGroupTo;
        private BeginningInventory _begInvTo;
        #endregion To
        private XPCollection<Item> _availableItem;
        private Item _item;
        private Brand _brand;
        private string _description;
        private BeginningInventoryLine _begInvLineFrom;
        private XPCollection<UnitOfMeasure> _availableUnitOfMeasure;
        private BeginningInventoryLine _begInvLineTo;
        #region InitialDefaultQuantityFrom
        private double _dQty;
        private UnitOfMeasure _dUom;
        private double _qty;
        private UnitOfMeasure _uom;
        private double _tQty;
        #endregion InitialDefaultQuantityFrom
        private Status _status;
        private DateTime _statusDate;
        private Transfer _transfer;
        private string _signCode;
        private string _userAccess;
        private string _localUserAccess;
        private GlobalFunction _globFunc;
        //AuditTrail
        private XPCollection<AuditDataItemPersistent> changeHistory;

        public TransferLine(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).
            if (!IsLoading)
            {
                if (Session.IsNewObject(this))
                {
                    _globFunc = new GlobalFunction();
                    DateTime now = DateTime.Now;
                    #region UserAccess
                    _localUserAccess = SecuritySystem.CurrentUserName;
                    UserAccess _locUserAccess = Session.FindObject<UserAccess>(new BinaryOperator("UserName", _localUserAccess));
                    if (_locUserAccess != null)
                    {
                        if (_locUserAccess.Employee != null)
                        {
                            if (_locUserAccess.Employee.Company != null && _locUserAccess.Employee.Workplace != null)
                            {
                                this.Code = _globFunc.GetNumberingUnlockOptimisticRecordBasedOrganization(this.Session.DataLayer, ObjectList.TransferLine, _locUserAccess.Employee.Company, _locUserAccess.Employee.Workplace);
                            }
                            else
                            {
                                this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.TransferLine);
                            }
                        }
                    }
                    #endregion UserAccess
                    this.Status = CustomProcess.Status.Open;
                    this.StatusDate = now;
                    this.Select = true;
                    this.StockGroupFrom = StockGroup.Normal;
                    this.StockGroupTo = StockGroup.Normal;
                }
            }
        }

        #region Field

        protected override void OnSaving()
        {
            base.OnSaving();
            if (!IsLoading && IsSaving)
            {
                UpdateNo();
            }
        }

        protected override void OnDeleting()
        {
            base.OnDeleting();
            if (!IsLoading)
            {
                RecoveryDeleteNo();
            }
        }

        [Browsable(false)]
        [ImmediatePostData()]
        public bool ActivationPosting
        {
            get { return _activationPosting; }
            set { SetPropertyValue("ActivationPosting", ref _activationPosting, value); }
        }

        [Appearance("TransferLineNoClose", Enabled = false)]
        public int No
        {
            get { return _no; }
            set { SetPropertyValue("No", ref _no, value); }
        }

        [VisibleInListView(false)]
        [RuleRequiredField(DefaultContexts.Save)]
        [Appearance("TransferLineCodeClose", Enabled = false)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue("Code", ref _code, value); }
        }

        [Appearance("TransferLineSelectClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public bool Select
        {
            get { return _select; }
            set { SetPropertyValue("Select", ref _select, value); }
        }

        #region Organization

        [VisibleInListView(false)]
        [Appearance("TransferLineCompanyClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Company Company
        {
            get { return _company; }
            set { SetPropertyValue("Company", ref _company, value); }
        }

        [VisibleInListView(false)]
        [Appearance("TransferLineWorkplaceClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Workplace Workplace
        {
            get { return _workplace; }
            set { SetPropertyValue("Workplace", ref _workplace, value); }
        }

        [Browsable(false)]
        public Division Division
        {
            get { return _division; }
            set { SetPropertyValue("Division", ref _division, value); }
        }

        [Browsable(false)]
        public Department Department
        {
            get { return _department; }
            set { SetPropertyValue("Department", ref _department, value); }
        }

        [Browsable(false)]
        public Section Section
        {
            get { return _section; }
            set { SetPropertyValue("Section", ref _section, value); }
        }

        [Browsable(false)]
        public Employee Employee
        {
            get { return _employee; }
            set { SetPropertyValue("Employee", ref _employee, value); }
        }

        #endregion Organization

        #region From

        [ImmediatePostData()]
        [Appearance("TransferLineLocationTypeFromClose", Enabled = false)]
        public LocationType LocationTypeFrom
        {
            get { return _locationTypeFrom; }
            set { SetPropertyValue("LocationTypeFrom", ref _locationTypeFrom, value); }
        }

        [Appearance("TransferLineStockTypeFromClose", Enabled = false)]
        public StockType StockTypeFrom
        {
            get { return _stockTypeFrom; }
            set { SetPropertyValue("StockTypeFrom", ref _stockTypeFrom, value); }
        }

        [Browsable(false)]
        public XPCollection<Workplace> AvailableWorkplaceFrom
        {
            get
            {
                string _beginString = null;
                string _endString = null;
                string _fullString = null;

                if (this.Company != null)
                {
                    _userAccess = SecuritySystem.CurrentUserName;
                    UserAccess _locUserAccess = Session.FindObject<UserAccess>(new BinaryOperator("UserName", _userAccess));
                    if (_locUserAccess != null)
                    {
                        OrganizationSetupDetail _locOrganizationSetupDetail = Session.FindObject<OrganizationSetupDetail>
                                                                        (new GroupOperator(GroupOperatorType.And,
                                                                        new BinaryOperator("Company", this.Company),
                                                                        new BinaryOperator("UserAccess", _locUserAccess),
                                                                        new BinaryOperator("AllWorkplace", false),
                                                                        new BinaryOperator("OpenWorkplace", true),
                                                                        new BinaryOperator("Active", true),
                                                                        new BinaryOperator("Default", true)));

                        if (_locOrganizationSetupDetail != null)
                        {
                            if (_locOrganizationSetupDetail.Workplace != null)
                            {
                                if (_locOrganizationSetupDetail.Workplace.Code != null)
                                {
                                    _availableWorkplaceFrom = new XPCollection<Workplace>
                                                (Session, new GroupOperator(GroupOperatorType.And,
                                                new BinaryOperator("Code", _locOrganizationSetupDetail.Workplace.Code),
                                                new BinaryOperator("Active", true)));
                                }
                            }
                        }
                        else
                        {

                            List<string> _stringOSD = new List<string>();

                            XPCollection<OrganizationSetupDetail> _locOrgSetupDetails = new XPCollection<OrganizationSetupDetail>
                                                                                (Session, new GroupOperator(GroupOperatorType.And,
                                                                                new BinaryOperator("Company", this.Company),
                                                                                new BinaryOperator("UserAccess", _locUserAccess),
                                                                                new BinaryOperator("AllWorkplace", false),
                                                                                new BinaryOperator("OpenWorkplace", true),
                                                                                new BinaryOperator("Active", true),
                                                                                new BinaryOperator("Default", false)));

                            if (_locOrgSetupDetails != null && _locOrgSetupDetails.Count() > 0)
                            {
                                foreach (OrganizationSetupDetail _locOrgSetupDetail in _locOrgSetupDetails)
                                {
                                    if (_locOrgSetupDetail.Workplace != null)
                                    {
                                        if (_locOrgSetupDetail.Workplace.Code != null)
                                        {
                                            _stringOSD.Add(_locOrgSetupDetail.Workplace.Code);
                                        }
                                    }
                                }
                            }

                            IEnumerable<string> _stringArrayOSDDistinct = _stringOSD.Distinct();
                            string[] _stringArrayOSDList = _stringArrayOSDDistinct.ToArray();
                            if (_stringArrayOSDList.Length == 1)
                            {
                                for (int i = 0; i < _stringArrayOSDList.Length; i++)
                                {
                                    Workplace _locW = Session.FindObject<Workplace>(new BinaryOperator("Code", _stringArrayOSDList[i]));
                                    if (_locW != null)
                                    {
                                        if (i == 0)
                                        {
                                            _beginString = "[Code]=='" + _locW.Code + "'";
                                        }
                                    }
                                }
                            }
                            else if (_stringArrayOSDList.Length > 1)
                            {
                                for (int i = 0; i < _stringArrayOSDList.Length; i++)
                                {
                                    Workplace _locW = Session.FindObject<Workplace>(new BinaryOperator("Code", _stringArrayOSDList[i]));
                                    if (_locW != null)
                                    {
                                        if (i == 0)
                                        {
                                            _beginString = "[Code]=='" + _locW.Code + "'";
                                        }
                                        else
                                        {
                                            _endString = _endString + " OR [Code]=='" + _locW.Code + "'";
                                        }
                                    }
                                }
                            }
                            _fullString = _beginString + _endString;

                            if (_fullString != null)
                            {
                                _availableWorkplaceFrom = new XPCollection<Workplace>(Session, CriteriaOperator.Parse(_fullString));
                            }
                            else
                            {
                                OrganizationSetupDetail _locOrgniSetupDetails = Session.FindObject<OrganizationSetupDetail>
                                                                        (new GroupOperator(GroupOperatorType.And,
                                                                        new BinaryOperator("Company", this.Company),
                                                                        new BinaryOperator("UserAccess", _locUserAccess),
                                                                        new BinaryOperator("OpenWorkplace", false),
                                                                        new BinaryOperator("AllWorkplace", true),
                                                                        new BinaryOperator("Active", true),
                                                                        new BinaryOperator("Default", false)));
                                if (_locOrgniSetupDetails != null)
                                {
                                    _availableWorkplaceFrom = new XPCollection<Workplace>(Session,
                                                        new GroupOperator(GroupOperatorType.And,
                                                        new BinaryOperator("Company", this.Company),
                                                        new BinaryOperator("Active", true)));
                                }
                            }
                        }
                    }
                    else
                    {
                        _availableWorkplaceFrom = new XPCollection<Workplace>(Session,
                                                    new GroupOperator(GroupOperatorType.And,
                                                    new BinaryOperator("Company", this.Company),
                                                    new BinaryOperator("Active", true)));
                    }
                }

                return _availableWorkplaceFrom;

            }
        }

        [ImmediatePostData()]
        [Appearance("TransferLineWorkplaceFromClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [DataSourceProperty("AvailableWorkplaceFrom", DataSourcePropertyIsNullMode.SelectAll)]
        public Workplace WorkplaceFrom
        {
            get { return _workplaceFrom; }
            set { SetPropertyValue("WorkplaceFrom", ref _workplaceFrom, value); }
        }

        [Browsable(false)]
        public XPCollection<Location> AvailableLocationFrom
        {
            get
            {
                _userAccess = SecuritySystem.CurrentUserName;
                string _beginString = null;
                string _endString = null;
                string _fullString = null;
                string _locLocationCode = null;
                UserAccess _locUserAccess = Session.FindObject<UserAccess>(new BinaryOperator("UserName", _userAccess));
                List<string> _stringLocation = new List<string>();
                if (_locUserAccess != null)
                {
                    if (this.Transfer != null)
                    {
                        #region Transfer
                        if (this.Transfer.InventoryMovingType == InventoryMovingType.Transfer)
                        {
                            XPCollection<WarehouseSetupDetail> _locWhsSetupDetails = null;

                            if (this.LocationTypeFrom == LocationType.None)
                            {
                                _locWhsSetupDetails = new XPCollection<WarehouseSetupDetail>
                                                    (Session, new GroupOperator(GroupOperatorType.And,
                                                    new BinaryOperator("UserAccess", _locUserAccess),
                                                    new BinaryOperator("Owner", true),
                                                    new BinaryOperator("Active", true)));


                            }
                            else
                            {
                                _locWhsSetupDetails = new XPCollection<WarehouseSetupDetail>
                                                    (Session, new GroupOperator(GroupOperatorType.And,
                                                    new BinaryOperator("UserAccess", _locUserAccess),
                                                    new BinaryOperator("Owner", true),
                                                    new BinaryOperator("LocationType", this.LocationTypeFrom),
                                                    new BinaryOperator("Active", true)));

                            }

                            if (_locWhsSetupDetails != null && _locWhsSetupDetails.Count() > 0)
                            {
                                foreach (WarehouseSetupDetail _locWhsSetupDetail in _locWhsSetupDetails)
                                {
                                    if (_locWhsSetupDetail.Location != null)
                                    {
                                        if (_locWhsSetupDetail.Location.Code != null)
                                        {
                                            _locLocationCode = _locWhsSetupDetail.Location.Code;
                                            _stringLocation.Add(_locLocationCode);
                                        }
                                    }
                                }
                            }

                            IEnumerable<string> _stringArrayLocationFromDistinct = _stringLocation.Distinct();
                            string[] _stringArrayLocationFromList = _stringArrayLocationFromDistinct.ToArray();
                            if (_stringArrayLocationFromList.Length == 1)
                            {
                                for (int i = 0; i < _stringArrayLocationFromList.Length; i++)
                                {
                                    Location _locLocationFrom = Session.FindObject<Location>(new BinaryOperator("Code", _stringArrayLocationFromList[i]));
                                    if (_locLocationFrom != null)
                                    {
                                        if (i == 0)
                                        {
                                            _beginString = "[Code]=='" + _locLocationFrom.Code + "'";
                                        }
                                    }
                                }
                            }
                            else if (_stringArrayLocationFromList.Length > 1)
                            {
                                for (int i = 0; i < _stringArrayLocationFromList.Length; i++)
                                {
                                    Location _locLocationFrom = Session.FindObject<Location>(new BinaryOperator("Code", _stringArrayLocationFromList[i]));
                                    if (_locLocationFrom != null)
                                    {
                                        if (i == 0)
                                        {
                                            _beginString = "[Code]=='" + _locLocationFrom.Code + "'";
                                        }
                                        else
                                        {
                                            _endString = _endString + " OR [Code]=='" + _locLocationFrom.Code + "'";
                                        }
                                    }
                                }
                            }
                            _fullString = _beginString + _endString;

                            if (_fullString != null)
                            {
                                _availableLocationFrom = new XPCollection<Location>(Session, CriteriaOperator.Parse(_fullString));
                            }

                        }
                        #endregion Transfer
                    }
                }

                return _availableLocationFrom;

            }
        }

        [ImmediatePostData()]
        [Appearance("TransferLineLocationFromClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [DataSourceProperty("AvailableLocationFrom")]
        [DataSourceCriteria("Active = true")]
        public Location LocationFrom
        {
            get { return _locationFrom; }
            set
            {
                SetPropertyValue("LocationFrom", ref _locationFrom, value);
                if (!IsLoading)
                {
                    if (this._locationFrom != null)
                    {
                        SetBegInvFrom(_locationFrom);
                        SetBegInvLineFrom();
                    }
                    else
                    {
                        BegInvFrom = null;
                        BegInvLineFrom = null;
                    }
                }
            }
        }

        [Browsable(false)]
        public XPCollection<BinLocation> AvailableBinLocationFrom
        {
            get
            {
                _userAccess = SecuritySystem.CurrentUserName;
                string _beginString = null;
                string _endString = null;
                string _fullString = null;
                string _locBinLocationCode = null;
                UserAccess _locUserAccess = Session.FindObject<UserAccess>(new BinaryOperator("UserName", _userAccess));
                List<string> _stringBinLocation = new List<string>();
                if (_locUserAccess != null)
                {
                    if (this.LocationFrom != null)
                    {
                        if (this.Transfer != null)
                        {
                            #region Transfer
                            if (this.Transfer.InventoryMovingType == InventoryMovingType.Transfer)
                            {

                                XPCollection<WarehouseSetupDetail> _locWhsSetupDetails = new XPCollection<WarehouseSetupDetail>
                                                                                  (Session, new GroupOperator(GroupOperatorType.And,
                                                                                   new BinaryOperator("UserAccess", _locUserAccess),
                                                                                   new BinaryOperator("Location", this.LocationFrom),
                                                                                   new BinaryOperator("Owner", true),
                                                                                   new BinaryOperator("Active", true)));

                                if (_locWhsSetupDetails != null && _locWhsSetupDetails.Count() > 0)
                                {
                                    foreach (WarehouseSetupDetail _locWhsSetupDetail in _locWhsSetupDetails)
                                    {
                                        if (_locWhsSetupDetail.BinLocation != null)
                                        {
                                            if (_locWhsSetupDetail.BinLocation.Code != null)
                                            {
                                                _locBinLocationCode = _locWhsSetupDetail.BinLocation.Code;
                                                _stringBinLocation.Add(_locBinLocationCode);
                                            }
                                        }
                                    }
                                }

                                IEnumerable<string> _stringArrayBinLocationFromDistinct = _stringBinLocation.Distinct();
                                string[] _stringArrayBinLocationFromList = _stringArrayBinLocationFromDistinct.ToArray();
                                if (_stringArrayBinLocationFromList.Length == 1)
                                {
                                    for (int i = 0; i < _stringArrayBinLocationFromList.Length; i++)
                                    {
                                        BinLocation _locBinLocationFrom = Session.FindObject<BinLocation>(new BinaryOperator("Code", _stringArrayBinLocationFromList[i]));
                                        if (_locBinLocationFrom != null)
                                        {
                                            if (i == 0)
                                            {
                                                _beginString = "[Code]=='" + _locBinLocationFrom.Code + "'";
                                            }
                                        }
                                    }
                                }
                                else if (_stringArrayBinLocationFromList.Length > 1)
                                {
                                    for (int i = 0; i < _stringArrayBinLocationFromList.Length; i++)
                                    {
                                        BinLocation _locBinLocationFrom = Session.FindObject<BinLocation>(new BinaryOperator("Code", _stringArrayBinLocationFromList[i]));
                                        if (_locBinLocationFrom != null)
                                        {
                                            if (i == 0)
                                            {
                                                _beginString = "[Code]=='" + _locBinLocationFrom.Code + "'";
                                            }
                                            else
                                            {
                                                _endString = _endString + " OR [Code]=='" + _locBinLocationFrom.Code + "'";
                                            }
                                        }
                                    }
                                }
                                _fullString = _beginString + _endString;

                                if (_fullString != null)
                                {
                                    _availableBinLocationFrom = new XPCollection<BinLocation>(Session, CriteriaOperator.Parse(_fullString));
                                }
                            }
                            #endregion Transfer
                        }
                    }
                }

                return _availableBinLocationFrom;

            }
        }

        [VisibleInListView(false)]
        [ImmediatePostData()]
        [Appearance("TransferLineBinLocationFromClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [DataSourceProperty("AvailableBinLocationFrom")]
        [DataSourceCriteria("Active = true")]
        public BinLocation BinLocationFrom
        {
            get { return _binLocationFrom; }
            set
            {
                SetPropertyValue("BinLocationFrom", ref _binLocationFrom, value);
                if (!IsLoading)
                {
                    if (this._binLocationFrom != null)
                    {
                        SetBegInvLineFrom();
                    }
                    else
                    {
                        this.BegInvLineFrom = null;
                    }
                }
            }
        }

        [Appearance("TransferLineStockGroupFromClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public StockGroup StockGroupFrom
        {
            get { return _stockGroupFrom; }
            set { SetPropertyValue("StockGroupFrom", ref _stockGroupFrom, value); }
        }

        [Browsable(false)]
        [ImmediatePostData()]
        [Appearance("TransferLineBegInvFromClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public BeginningInventory BegInvFrom
        {
            get { return _begInvFrom; }
            set { SetPropertyValue("BegInvFrom", ref _begInvFrom, value); }
        }

        #endregion From

        #region To

        [ImmediatePostData()]
        [Appearance("TransferLineLocationTypeToClose", Enabled = false)]
        public LocationType LocationTypeTo
        {
            get { return _locationTypeTo; }
            set { SetPropertyValue("LocationTypeTo", ref _locationTypeTo, value); }
        }

        [Appearance("TransferLineStockTypeToClose", Enabled = false)]
        public StockType StockTypeTo
        {
            get { return _stockTypeTo; }
            set { SetPropertyValue("StockTypeTo", ref _stockTypeTo, value); }
        }

        [Browsable(false)]
        public XPCollection<Workplace> AvailableWorkplaceTo
        {
            get
            {
                string _beginString = null;
                string _endString = null;
                string _fullString = null;

                if (this.Company != null)
                {
                    _userAccess = SecuritySystem.CurrentUserName;
                    UserAccess _locUserAccess = Session.FindObject<UserAccess>(new BinaryOperator("UserName", _userAccess));
                    if (_locUserAccess != null)
                    {
                        List<string> _stringOSD = new List<string>();

                        XPCollection<OrganizationSetupDetail> _locOrganizationSetupDetails = new XPCollection<OrganizationSetupDetail>
                                                                            (Session, new GroupOperator(GroupOperatorType.And,
                                                                            new BinaryOperator("Company", this.Company),
                                                                            new BinaryOperator("UserAccess", _locUserAccess),
                                                                            new BinaryOperator("AllWorkplace", false),
                                                                            new BinaryOperator("OpenWorkplace", true),
                                                                            new BinaryOperator("Active", true)));

                        if (_locOrganizationSetupDetails != null && _locOrganizationSetupDetails.Count() > 0)
                        {
                            foreach (OrganizationSetupDetail _locOrganizationSetupDetail in _locOrganizationSetupDetails)
                            {
                                if (_locOrganizationSetupDetail.Workplace != null)
                                {
                                    if (_locOrganizationSetupDetail.Workplace.Code != null)
                                    {
                                        _stringOSD.Add(_locOrganizationSetupDetail.Workplace.Code);
                                    }
                                }
                            }
                        }

                        IEnumerable<string> _stringArrayOSDDistinct = _stringOSD.Distinct();
                        string[] _stringArrayOSDList = _stringArrayOSDDistinct.ToArray();
                        if (_stringArrayOSDList.Length == 1)
                        {
                            for (int i = 0; i < _stringArrayOSDList.Length; i++)
                            {
                                Workplace _locW = Session.FindObject<Workplace>(new BinaryOperator("Code", _stringArrayOSDList[i]));
                                if (_locW != null)
                                {
                                    if (i == 0)
                                    {
                                        _beginString = "[Code]=='" + _locW.Code + "'";
                                    }
                                }
                            }
                        }
                        else if (_stringArrayOSDList.Length > 1)
                        {
                            for (int i = 0; i < _stringArrayOSDList.Length; i++)
                            {
                                Workplace _locW = Session.FindObject<Workplace>(new BinaryOperator("Code", _stringArrayOSDList[i]));
                                if (_locW != null)
                                {
                                    if (i == 0)
                                    {
                                        _beginString = "[Code]=='" + _locW.Code + "'";
                                    }
                                    else
                                    {
                                        _endString = _endString + " OR [Code]=='" + _locW.Code + "'";
                                    }
                                }
                            }
                        }
                        _fullString = _beginString + _endString;

                        if (_fullString != null)
                        {
                            _availableWorkplaceTo = new XPCollection<Workplace>(Session, CriteriaOperator.Parse(_fullString));
                        }
                        else
                        {
                            OrganizationSetupDetail _locWhsSetupDetail = Session.FindObject<OrganizationSetupDetail>
                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                    new BinaryOperator("Company", this.Company),
                                                                    new BinaryOperator("UserAccess", _locUserAccess),
                                                                    new BinaryOperator("AllWorkplace", true),
                                                                    new BinaryOperator("Active", true)));
                            if (_locWhsSetupDetail != null)
                            {
                                _availableWorkplaceTo = new XPCollection<Workplace>(Session,
                                                    new GroupOperator(GroupOperatorType.And,
                                                    new BinaryOperator("Company", this.Company),
                                                    new BinaryOperator("Active", true)));
                            }
                        }
                    }
                }

                return _availableWorkplaceTo;

            }
        }

        [ImmediatePostData()]
        [Appearance("TransferLineWorkplaceToClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [DataSourceProperty("AvailableWorkplaceTo", DataSourcePropertyIsNullMode.SelectAll)]
        public Workplace WorkplaceTo
        {
            get { return _workplaceTo; }
            set { SetPropertyValue("WorkplaceTo", ref _workplaceTo, value); }
        }

        [Browsable(false)]
        public XPCollection<Location> AvailableLocationTo
        {
            get
            {
                _userAccess = SecuritySystem.CurrentUserName;
                string _beginString = null;
                string _endString = null;
                string _fullString = null;
                string _locLocationCode = null;
                UserAccess _locUserAccess = Session.FindObject<UserAccess>(new BinaryOperator("UserName", _userAccess));
                List<string> _stringLocation = new List<string>();
                if (_locUserAccess != null)
                {
                    if (this.Transfer != null)
                    {
                        #region Transfer
                        if (this.Transfer.InventoryMovingType == InventoryMovingType.Transfer)
                        {
                            XPCollection<WarehouseSetupDetail> _locWhsSetupDetails = null;

                            if (this.LocationTypeTo == LocationType.None)
                            {
                                _locWhsSetupDetails = new XPCollection<WarehouseSetupDetail>
                                                     (Session, new GroupOperator(GroupOperatorType.And,
                                                     new BinaryOperator("UserAccess", _locUserAccess),
                                                     new BinaryOperator("Active", true)));
                            }
                            else
                            {
                                _locWhsSetupDetails = new XPCollection<WarehouseSetupDetail>
                                                    (Session, new GroupOperator(GroupOperatorType.And,
                                                    new BinaryOperator("UserAccess", _locUserAccess),
                                                    new BinaryOperator("LocationType", this.LocationTypeTo),
                                                    new BinaryOperator("Active", true)));
                            }

                            if (_locWhsSetupDetails != null && _locWhsSetupDetails.Count() > 0)
                            {
                                foreach (WarehouseSetupDetail _locWhsSetupDetail in _locWhsSetupDetails)
                                {
                                    if (_locWhsSetupDetail.Location != null)
                                    {
                                        if (_locWhsSetupDetail.Location.Code != null)
                                        {
                                            _locLocationCode = _locWhsSetupDetail.Location.Code;
                                        }
                                    }
                                    _stringLocation.Add(_locLocationCode);
                                }
                            }

                            IEnumerable<string> _stringArrayLocationToDistinct = _stringLocation.Distinct();
                            string[] _stringArrayLocationToList = _stringArrayLocationToDistinct.ToArray();
                            if (_stringArrayLocationToList.Length == 1)
                            {
                                for (int i = 0; i < _stringArrayLocationToList.Length; i++)
                                {
                                    Location _locLocationTo = Session.FindObject<Location>(new BinaryOperator("Code", _stringArrayLocationToList[i]));
                                    if (_locLocationTo != null)
                                    {
                                        if (i == 0)
                                        {
                                            _beginString = "[Code]=='" + _locLocationTo.Code + "'";
                                        }
                                    }
                                }
                            }
                            else if (_stringArrayLocationToList.Length > 1)
                            {
                                for (int i = 0; i < _stringArrayLocationToList.Length; i++)
                                {
                                    Location _locLocationTo = Session.FindObject<Location>(new BinaryOperator("Code", _stringArrayLocationToList[i]));
                                    if (_locLocationTo != null)
                                    {
                                        if (i == 0)
                                        {
                                            _beginString = "[Code]=='" + _locLocationTo.Code + "'";
                                        }
                                        else
                                        {
                                            _endString = _endString + " OR [Code]=='" + _locLocationTo.Code + "'";
                                        }
                                    }
                                }
                            }
                            _fullString = _beginString + _endString;

                            if (_fullString != null)
                            {
                                _availableLocationTo = new XPCollection<Location>(Session, CriteriaOperator.Parse(_fullString));
                            }
                        }
                        #endregion Transfer
                    }
                }

                return _availableLocationTo;

            }
        }

        [ImmediatePostData]
        [Appearance("TransferLineLocationToClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [DataSourceProperty("AvailableLocationTo")]
        [DataSourceCriteria("Active = true")]
        public Location LocationTo
        {
            get { return _locationTo; }
            set
            {
                SetPropertyValue("LocationTo", ref _locationTo, value);
                if (!IsLoading)
                {
                    if (this._locationTo != null)
                    {
                        SetBegInvTo(_locationTo);
                    }
                    else
                    {
                        BegInvTo = null;
                    }
                }
            }
        }

        [Browsable(false)]
        public XPCollection<BinLocation> AvailableBinLocationTo
        {
            get
            {
                _userAccess = SecuritySystem.CurrentUserName;
                string _beginString = null;
                string _endString = null;
                string _fullString = null;
                string _locBinLocationCode = null;
                UserAccess _locUserAccess = Session.FindObject<UserAccess>(new BinaryOperator("UserName", _userAccess));
                List<string> _stringBinLocation = new List<string>();
                if (_locUserAccess != null)
                {
                    if (this.LocationTo != null)
                    {
                        if (this.Transfer != null)
                        {
                            #region Transfer
                            if (this.Transfer.InventoryMovingType == InventoryMovingType.Transfer)
                            {
                                XPCollection<WarehouseSetupDetail> _locWhsSetupDetails = new XPCollection<WarehouseSetupDetail>
                                                                                  (Session, new GroupOperator(GroupOperatorType.And,
                                                                                   new BinaryOperator("UserAccess", _locUserAccess),
                                                                                   new BinaryOperator("Location", this.LocationTo),
                                                                                   new BinaryOperator("Active", true)));

                                if (_locWhsSetupDetails != null && _locWhsSetupDetails.Count() > 0)
                                {
                                    foreach (WarehouseSetupDetail _locWhsSetupDetail in _locWhsSetupDetails)
                                    {
                                        if (_locWhsSetupDetail.BinLocation != null)
                                        {
                                            if (_locWhsSetupDetail.BinLocation.Code != null)
                                            {
                                                _locBinLocationCode = _locWhsSetupDetail.BinLocation.Code;
                                            }
                                        }
                                        _stringBinLocation.Add(_locBinLocationCode);
                                    }
                                }

                                IEnumerable<string> _stringArrayBinLocationToDistinct = _stringBinLocation.Distinct();
                                string[] _stringArrayBinLocationToList = _stringArrayBinLocationToDistinct.ToArray();
                                if (_stringArrayBinLocationToList.Length == 1)
                                {
                                    for (int i = 0; i < _stringArrayBinLocationToList.Length; i++)
                                    {
                                        BinLocation _locBinLocationTo = Session.FindObject<BinLocation>(new BinaryOperator("Code", _stringArrayBinLocationToList[i]));
                                        if (_locBinLocationTo != null)
                                        {
                                            if (i == 0)
                                            {
                                                _beginString = "[Code]=='" + _locBinLocationTo.Code + "'";
                                            }
                                        }
                                    }
                                }
                                else if (_stringArrayBinLocationToList.Length > 1)
                                {
                                    for (int i = 0; i < _stringArrayBinLocationToList.Length; i++)
                                    {
                                        BinLocation _locBinLocationTo = Session.FindObject<BinLocation>(new BinaryOperator("Code", _stringArrayBinLocationToList[i]));
                                        if (_locBinLocationTo != null)
                                        {
                                            if (i == 0)
                                            {
                                                _beginString = "[Code]=='" + _locBinLocationTo.Code + "'";
                                            }
                                            else
                                            {
                                                _endString = _endString + " OR [Code]=='" + _locBinLocationTo.Code + "'";
                                            }
                                        }
                                    }
                                }
                                _fullString = _beginString + _endString;

                                if (_fullString != null)
                                {
                                    _availableBinLocationTo = new XPCollection<BinLocation>(Session, CriteriaOperator.Parse(_fullString));
                                }
                            }
                            #endregion Transfer
                        }
                    }
                }
                else
                {
                    _availableBinLocationTo = new XPCollection<BinLocation>(Session);
                }

                return _availableBinLocationTo;

            }
        }

        [VisibleInListView(false)]
        [Appearance("TransferLineBinLocationToClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [DataSourceProperty("AvailableBinLocationTo")]
        [DataSourceCriteria("Active = true")]
        public BinLocation BinLocationTo
        {
            get { return _binLocationTo; }
            set { SetPropertyValue("BinLocationTo", ref _binLocationTo, value); }
        }

        [Appearance("TransferLineStockGroupToClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public StockGroup StockGroupTo
        {
            get { return _stockGroupTo; }
            set { SetPropertyValue("StockGroupTo", ref _stockGroupTo, value); }
        }

        [Browsable(false)]
        [ImmediatePostData()]
        [Appearance("TransferLineBegInvToClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public BeginningInventory BegInvTo
        {
            get { return _begInvTo; }
            set { SetPropertyValue("BegInvTo", ref _begInvTo, value); }
        }

        #endregion To

        [Browsable(false)]
        public XPCollection<Item> AvailableItem
        {
            get
            {
                string _beginString = null;
                string _endString = null;
                string _fullString = null;
                //&& this.BegInv != null
                if (this.Company != null && this.Workplace != null)
                {
                    List<string> _stringITM = new List<string>();

                    XPQuery<BeginningInventoryLine> _beginningInventoryLinesQuery = new XPQuery<BeginningInventoryLine>(Session);

                    var _beginningInventoryLines = from bil in _beginningInventoryLinesQuery
                                                   where (bil.Company == this.Company
                                                   && bil.Workplace == this.Workplace
                                                   && bil.BeginningInventory == this.BegInvFrom
                                                   && bil.QtyAvailable > 0
                                                   && bil.LocationType == this.LocationTypeFrom
                                                   && bil.StockType == this.StockTypeFrom
                                                   && bil.Lock == false
                                                   && bil.Active == true
                                                   )
                                                   group bil by bil.Item into g
                                                   select new { Item = g.Key };
                    if (_beginningInventoryLines != null && _beginningInventoryLines.Count() > 0)
                    {
                        foreach (var _beginningInventoryLine in _beginningInventoryLines)
                        {
                            _stringITM.Add(_beginningInventoryLine.Item.Code);
                        }
                    }

                    IEnumerable<string> _stringArrayITMDistinct = _stringITM.Distinct();
                    string[] _stringArrayITMList = _stringArrayITMDistinct.ToArray();
                    if (_stringArrayITMList.Length == 1)
                    {
                        for (int i = 0; i < _stringArrayITMList.Length; i++)
                        {
                            Item _locITM = Session.FindObject<Item>(new BinaryOperator("Code", _stringArrayITMList[i]));
                            if (_locITM != null)
                            {
                                if (i == 0)
                                {
                                    _beginString = "[Code]=='" + _locITM.Code + "'";
                                }
                            }
                        }
                    }
                    else if (_stringArrayITMList.Length > 1)
                    {
                        for (int i = 0; i < _stringArrayITMList.Length; i++)
                        {
                            Item _locITM = Session.FindObject<Item>(new BinaryOperator("Code", _stringArrayITMList[i]));
                            if (_locITM != null)
                            {
                                if (i == 0)
                                {
                                    _beginString = "[Code]=='" + _locITM.Code + "'";
                                }
                                else
                                {
                                    _endString = _endString + " OR [Code]=='" + _locITM.Code + "'";
                                }
                            }
                        }
                    }
                    _fullString = _beginString + _endString;

                    if (_fullString != null)
                    {
                        _availableItem = new XPCollection<Item>(Session, CriteriaOperator.Parse(_fullString));
                    }
                }
                else
                {
                    _availableItem = new XPCollection<Item>(Session, new GroupOperator(GroupOperatorType.And,
                                                    new BinaryOperator("Company", this.Company),
                                                    new BinaryOperator("Active", true)));
                }

                return _availableItem;

            }
        }

        [ImmediatePostData()]
        [DataSourceProperty("AvailableItem")]
        [Appearance("TransferLineItemClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Item Item
        {
            get { return _item; }
            set
            {
                SetPropertyValue("Item", ref _item, value);
                if (!IsLoading)
                {
                    if (this._item != null)
                    {
                        
                        this.DUOM = this._item.BasedUOM;
                        this.Brand = this._item.Brand;
                        this.Description = GetDescriptions();
                        SetUOM();
                        SetBegInvLineFrom();
                    }
                    else
                    {
                        this.DUOM = null;
                        this.Brand = null;
                        this.Description = null;
                        this.BegInvLineFrom = null;
                    }
                }
            }
        }

        [VisibleInListView(false)]
        [Appearance("TransferLineBrandClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Brand Brand
        {
            get { return _brand; }
            set { SetPropertyValue("Brand", ref _brand, value); }
        }

        [VisibleInListView(false)]
        [Appearance("TransferLineDescriptionClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [Size(512)]
        public string Description
        {
            get { return _description; }
            set { SetPropertyValue("Description", ref _description, value); }
        }

        //[Browsable(false)]
        [ImmediatePostData()]
        [Appearance("TransferLineBegInvLineFromClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public BeginningInventoryLine BegInvLineFrom
        {
            get { return _begInvLineFrom; }
            set { SetPropertyValue("BegInvLineFrom", ref _begInvLineFrom, value); }
        }

        //[Browsable(false)]
        [ImmediatePostData()]
        [Appearance("TransferLineBegInvLineToClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public BeginningInventoryLine BegInvLineTo
        {
            get { return _begInvLineTo; }
            set { SetPropertyValue("BegInvLineTo", ref _begInvLineTo, value); }
        }

        [Browsable(false)]
        public XPCollection<UnitOfMeasure> AvailableUnitOfMeasure
        {
            get
            {
                string _beginString = null;
                string _endString = null;
                string _fullString = null;
                if (Item == null)
                {
                    _availableUnitOfMeasure = new XPCollection<UnitOfMeasure>(Session);
                }
                else
                {
                    List<string> _stringUOM = new List<string>();

                    XPCollection<ItemUnitOfMeasure> _locItemUnitOfMeasures = new XPCollection<ItemUnitOfMeasure>
                                                                              (Session, new GroupOperator(GroupOperatorType.And,
                                                                               new BinaryOperator("Item", this.Item),
                                                                               new BinaryOperator("Active", true)));

                    if (_locItemUnitOfMeasures != null && _locItemUnitOfMeasures.Count() > 0)
                    {
                        foreach (ItemUnitOfMeasure _locItemUnitOfMeasure in _locItemUnitOfMeasures)
                        {
                            _stringUOM.Add(_locItemUnitOfMeasure.UOM.Code);
                        }
                    }

                    IEnumerable<string> _stringArrayUOMDistinct = _stringUOM.Distinct();
                    string[] _stringArrayUOMList = _stringArrayUOMDistinct.ToArray();
                    if (_stringArrayUOMList.Length == 1)
                    {
                        for (int i = 0; i < _stringArrayUOMList.Length; i++)
                        {
                            UnitOfMeasure _locUOM = Session.FindObject<UnitOfMeasure>(new BinaryOperator("Code", _stringArrayUOMList[i]));
                            if (_locUOM != null)
                            {
                                if (i == 0)
                                {
                                    _beginString = "[Code]=='" + _locUOM.Code + "'";
                                }
                            }
                        }
                    }
                    else if (_stringArrayUOMList.Length > 1)
                    {
                        for (int i = 0; i < _stringArrayUOMList.Length; i++)
                        {
                            UnitOfMeasure _locUOM = Session.FindObject<UnitOfMeasure>(new BinaryOperator("Code", _stringArrayUOMList[i]));
                            if (_locUOM != null)
                            {
                                if (i == 0)
                                {
                                    _beginString = "[Code]=='" + _locUOM.Code + "'";
                                }
                                else
                                {
                                    _endString = " OR [Code]=='" + _locUOM.Code + "'";
                                }
                            }
                        }
                    }
                    _fullString = _beginString + _endString;

                    if (_fullString != null)
                    {
                        _availableUnitOfMeasure = new XPCollection<UnitOfMeasure>(Session, CriteriaOperator.Parse(_fullString));
                    }

                }
                return _availableUnitOfMeasure;

            }
        }

        #region DefaultQty

        [Appearance("TransferLineDQtyClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [ImmediatePostData()]
        public double DQty
        {
            get { return _dQty; }
            set
            {
                SetPropertyValue("DQty", ref _dQty, value);
                if (!IsLoading)
                {
                    SetTotalQty();
                }
            }
        }

        [Appearance("TransferLineDUOMClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [ImmediatePostData()]
        [DataSourceCriteria("Active = true")]
        public UnitOfMeasure DUOM
        {
            get { return _dUom; }
            set
            {
                SetPropertyValue("DUOM", ref _dUom, value);
                if (!IsLoading)
                {
                    SetTotalQty();
                }
            }
        }

        [Appearance("TransferLineQtyClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [ImmediatePostData()]
        public double Qty
        {
            get { return _qty; }
            set
            {
                SetPropertyValue("Qty", ref _qty, value);
                if (!IsLoading)
                {
                    SetTotalQty();
                }
            }
        }

        [Appearance("TransferLineUOMClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [ImmediatePostData()]
        [DataSourceProperty("AvailableUnitOfMeasure", DataSourcePropertyIsNullMode.SelectAll)]
        [DataSourceCriteria("Active = true")]
        public UnitOfMeasure UOM
        {
            get { return _uom; }
            set
            {
                SetPropertyValue("UOM", ref _uom, value);
                if (!IsLoading)
                {
                    SetTotalQty();
                }
            }
        }

        [Appearance("TransferLineTQtyEnabled", Enabled = false)]
        public double TQty
        {
            get { return _tQty; }
            set { SetPropertyValue("TQtyFrom", ref _tQty, value); }
        }

        #endregion DefaultQty

        [VisibleInListView(false)]
        [Appearance("TransferLineStatusClose", Enabled = false)]
        public Status Status
        {
            get { return _status; }
            set { SetPropertyValue("Status", ref _status, value); }
        }

        [VisibleInListView(false)]
        [ModelDefault("DisplayFormat", "{0:dd/MM/yyyy}")]
        [ModelDefault("EditMask", "dd/MM/yyyy")]
        [Appearance("TransferLineStatusDateClose", Enabled = false)]
        public DateTime StatusDate
        {
            get { return _statusDate; }
            set { SetPropertyValue("StatusDate", ref _statusDate, value); }
        }

        [Browsable(false)]
        [Appearance("TransferLineSignCodeClose", Enabled = false)]
        public string SignCode
        {
            get { return _signCode; }
            set { SetPropertyValue("SignCode", ref _signCode, value); }
        }

        [ImmediatePostData()]
        [RuleRequiredField(DefaultContexts.Save)]
        [Appearance("TransferLineTransferEnabled", Enabled = false)]
        [Association("Transfer-TransferLines")]
        public Transfer Transfer
        {
            get { return _transfer; }
            set
            {
                SetPropertyValue("Transfer", ref _transfer, value);
                if (!IsLoading)
                {
                    if (this._transfer != null)
                    {
                        if (this._transfer.Company != null) { this.Company = this._transfer.Company; }
                        if (this._transfer.Workplace != null) { this.Workplace = this._transfer.Workplace; }
                        if (this._transfer.Division != null) { this.Division = this._transfer.Division; }
                        if (this._transfer.Department != null) { this.Department = this._transfer.Department; }
                        if (this._transfer.Section != null) { this.Section = this._transfer.Section; }
                        if (this._transfer.Employee != null) { this.Employee = this._transfer.Employee; }
                        if (this._transfer.LocationTypeFrom != LocationType.None) { this.LocationTypeFrom = this._transfer.LocationTypeFrom; }
                        if (this._transfer.StockTypeFrom != StockType.None) { this.StockTypeFrom = this._transfer.StockTypeFrom; }
                        if (this._transfer.WorkplaceFrom != null) { this.WorkplaceFrom = this._transfer.WorkplaceFrom; }
                        if (this._transfer.LocationFrom != null) { this.LocationFrom = this._transfer.LocationFrom; }
                        if (this._transfer.BegInvFrom != null) { this.BegInvFrom = this._transfer.BegInvFrom; }
                        if (this._transfer.LocationTypeTo != LocationType.None) { this.LocationTypeTo = this._transfer.LocationTypeTo; }
                        if (this._transfer.StockTypeTo != StockType.None) { this.StockTypeTo = this._transfer.StockTypeTo; }
                        if (this._transfer.WorkplaceTo != null) { this.WorkplaceTo = this._transfer.WorkplaceTo; }
                        if (this._transfer.LocationTo != null) { this.LocationTo = this._transfer.LocationTo; }
                        if (this._transfer.BegInvTo != null) { this.BegInvTo = this._transfer.BegInvTo; }
                    }
                }
            }
        }

        //AuditTrail
        [CollectionOperationSet(AllowAdd = false, AllowRemove = true)]
        public XPCollection<AuditDataItemPersistent> ChangeHistory
        {
            get
            {
                if (changeHistory == null)
                {
                    changeHistory = AuditedObjectWeakReference.GetAuditTrail(Session, this);
                }
                return changeHistory;
            }
        }

        #endregion Field

        //========================================== Code In Here ==========================================

        #region Get Description

        public string GetDescriptions()
        {
            string _result = "";
            try
            {
                if (this._item != null)
                {
                    string _result1 = null;
                    string _result2 = null;
                    if (this._item.Name != null)
                    {
                        _result1 = this._item.Name;
                    }
                    if (this._item.Description != null)
                    {
                        _result2 = this._item.Description;
                    }
                    _result = _result1 + " " + _result2;
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = TransferLine " + ex.ToString());
            }

            return _result;
        }

        #endregion Get Description

        #region No

        public void UpdateNo()
        {
            try
            {
                if (!IsLoading && Session.IsNewObject(this))
                {
                    if (this.Transfer != null)
                    {
                        object _makRecord = Session.Evaluate<TransferLine>(CriteriaOperator.Parse("Max(No)"), CriteriaOperator.Parse("Transfer=?", this.Transfer));
                        this.No = Convert.ToInt32(_makRecord) + 1;
                        this.Save();
                        RecoveryUpdateNo();
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = TransferLine " + ex.ToString());
            }
        }

        public void RecoveryUpdateNo()
        {
            try
            {
                if (this.Transfer != null)
                {
                    Transfer _numHeader = Session.FindObject<Transfer>
                                                (new BinaryOperator("Code", this.Transfer.Code));

                    XPCollection<TransferLine> _numLines = new XPCollection<TransferLine>
                                                (Session, new BinaryOperator("Transfer", _numHeader),
                                                new SortProperty("No", DevExpress.Xpo.DB.SortingDirection.Ascending));
                    if (_numLines != null)
                    {
                        int i1 = 0;
                        foreach (TransferLine _numLine in _numLines)
                        {
                            i1 += 1;
                            _numLine.No = i1;
                            _numLine.Save();
                        }
                        i1 = 1;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = TransferLine " + ex.ToString());
            }
        }

        public void RecoveryDeleteNo()
        {
            try
            {
                if (this.Transfer != null)
                {
                    Transfer _numHeader = Session.FindObject<Transfer>
                                                (new BinaryOperator("Code", this.Transfer.Code));

                    XPCollection<TransferLine> _numLines = new XPCollection<TransferLine>
                                                (Session, new GroupOperator(GroupOperatorType.And,
                                                 new BinaryOperator("This", this, BinaryOperatorType.NotEqual),
                                                 new BinaryOperator("Transfer", _numHeader)),
                                                 new SortProperty("No", DevExpress.Xpo.DB.SortingDirection.Ascending));
                    if (_numLines != null)
                    {
                        int i = 0;
                        foreach (TransferLine _numLine in _numLines)
                        {
                            i += 1;
                            _numLine.No = i;
                            _numLine.Save();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = TransferLine " + ex.ToString());
            }
        }

        #endregion No

        #region Set

        private void SetUOM()
        {
            try
            {
                if (this.Company != null && this.Item != null && this.DUOM != null)
                {
                    ItemUnitOfMeasure _locIUOM = Session.FindObject<ItemUnitOfMeasure>(new GroupOperator(GroupOperatorType.And,
                                                                            new BinaryOperator("Company", this.Company),
                                                                            new BinaryOperator("Item", this.Item),
                                                                            new BinaryOperator("DefaultUOM", this.DUOM),
                                                                            new BinaryOperator("Default", true),
                                                                            new BinaryOperator("Active", true)));
                    if (_locIUOM != null)
                    {
                        if (_locIUOM.UOM != null)
                        {
                            this.UOM = _locIUOM.UOM;
                        }
                        else
                        {
                            this.UOM = null;
                        }

                    }
                    else
                    {
                        this.UOM = null;
                    }
                }
                else
                {
                    this.UOM = null;
                }

            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = TransferLine " + ex.ToString());
            }
        }

        private void SetTotalQty()
        {
            try
            {
                double _locInvLineTotal = 0;
                DateTime now = DateTime.Now;
                if (this.Transfer != null)
                {
                    if (this.Item != null && this.UOM != null && this.DUOM != null)
                    {
                        ItemUnitOfMeasure _locItemUOM = Session.FindObject<ItemUnitOfMeasure>
                                                        (new GroupOperator(GroupOperatorType.And,
                                                         new BinaryOperator("Item", this.Item),
                                                         new BinaryOperator("UOM", this.UOM),
                                                         new BinaryOperator("DefaultUOM", this.DUOM),
                                                         new BinaryOperator("Active", true)));
                        if (_locItemUOM != null)
                        {
                            if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                            {
                                _locInvLineTotal = this.Qty * _locItemUOM.DefaultConversion + this.DQty;
                            }
                            else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                            {
                                _locInvLineTotal = this.Qty / _locItemUOM.Conversion + this.DQty;
                            }
                            else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                            {
                                _locInvLineTotal = this.Qty + this.DQty;
                            }

                            this.TQty = _locInvLineTotal;
                        }
                    }
                    else
                    {
                        _locInvLineTotal = this.Qty + this.DQty;
                        this.TQty = _locInvLineTotal;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = TransferLine " + ex.ToString());
            }
        }

        private void SetBegInvFrom(Location _locLocation)
        {
            try
            {
                DateTime now = DateTime.Now;
                if (this.Company != null && this.WorkplaceFrom != null)
                {
                    BeginningInventory _locBegInvFrom = Session.FindObject<BeginningInventory>
                                                        (new GroupOperator(GroupOperatorType.And,
                                                        new BinaryOperator("Company", this.Company),
                                                        new BinaryOperator("Workplace", this.WorkplaceFrom),
                                                        new BinaryOperator("Location", _locLocation),
                                                        new BinaryOperator("LocationType", this.LocationTypeFrom),
                                                        new BinaryOperator("StockType", this.StockTypeFrom),
                                                        new BinaryOperator("Active", true)));
                    if (_locBegInvFrom != null)
                    {
                        this.BegInvFrom = _locBegInvFrom;
                    }
                    else
                    {
                        this.BegInvFrom = null;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Business Object = TransferLine ", ex.ToString());
            }
        }

        private void SetBegInvTo(Location _locLocation)
        {
            try
            {
                DateTime now = DateTime.Now;
                if (this.Company != null && this.WorkplaceFrom != null)
                {
                    BeginningInventory _locBegInvTo = Session.FindObject<BeginningInventory>
                                                        (new GroupOperator(GroupOperatorType.And,
                                                        new BinaryOperator("Company", this.Company),
                                                        new BinaryOperator("Workplace", this.WorkplaceFrom),
                                                        new BinaryOperator("Location", _locLocation),
                                                        new BinaryOperator("LocationType", this.LocationTypeTo),
                                                        new BinaryOperator("StockType", this.StockTypeTo),
                                                        new BinaryOperator("Active", true)));
                    if (_locBegInvTo != null)
                    {
                        this.BegInvTo = _locBegInvTo;
                    }
                    else
                    {
                        this.BegInvTo = null;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Business Object = TransferLine ", ex.ToString());
            }
        }

        private void SetBegInvLineFrom()
        {
            try
            {
                DateTime now = DateTime.Now;
                BeginningInventoryLine _locBegInvLine = null;
                if (this.Company != null && this.WorkplaceFrom != null && this.Item != null && this.BegInvFrom != null && this.LocationFrom != null)
                {
                    if (this.BinLocationFrom != null)
                    {
                        //BinLocation
                        _locBegInvLine = Session.FindObject<BeginningInventoryLine>(
                                   new GroupOperator(GroupOperatorType.And,
                                   new BinaryOperator("Company", this.Company),
                                   new BinaryOperator("Workplace", this.WorkplaceFrom),
                                   new BinaryOperator("BeginningInventory", this.BegInvFrom),
                                   new BinaryOperator("Location", this.LocationFrom),
                                   new BinaryOperator("Item", this.Item),
                                   new BinaryOperator("BinLocation", this.BinLocationFrom),
                                   new BinaryOperator("LocationType", this.LocationTypeFrom),
                                   new BinaryOperator("StockType", this.StockTypeFrom),
                                   new BinaryOperator("StockGroup", this.StockGroupFrom),
                                   new BinaryOperator("Lock", false),
                                   new BinaryOperator("Active", true)));
                    }
                    else
                    {
                        _locBegInvLine = Session.FindObject<BeginningInventoryLine>(
                                    new GroupOperator(GroupOperatorType.And,
                                    new BinaryOperator("Company", this.Company),
                                    new BinaryOperator("Workplace", this.WorkplaceFrom),
                                    new BinaryOperator("BeginningInventory", this.BegInvFrom),
                                    new BinaryOperator("Location", this.LocationFrom),
                                    new BinaryOperator("Item", this.Item),
                                    new BinaryOperator("LocationType", this.LocationTypeFrom),
                                    new BinaryOperator("StockType", this.StockTypeFrom),
                                    new BinaryOperator("StockGroup", this.StockGroupFrom),
                                    new BinaryOperator("Lock", false),
                                    new BinaryOperator("Active", true)));
                    }

                    if (_locBegInvLine != null)
                    {
                        this.BegInvLineFrom = _locBegInvLine;
                    }
                    else
                    {
                        this.BegInvLineFrom = null;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Business Object = TransferLine ", ex.ToString());
            }
        }

        #endregion Set

    }
}