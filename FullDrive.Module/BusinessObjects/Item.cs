﻿using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.ConditionalAppearance;

namespace FullDrive.Module.BusinessObjects
{
    [DefaultClassOptions]
    [DefaultProperty("Name")]
    [NavigationItem("Product")]
    [RuleCombinationOfPropertiesIsUnique("ItemRuleUnique", DefaultContexts.Save, "Code")]
    //[ImageName("BO_Contact")]
    //[DefaultProperty("DisplayMemberNameForLookupEditorsOfThisType")]
    //[DefaultListViewOptions(MasterDetailMode.ListViewOnly, false, NewItemRowPosition.None)]
    //[Persistent("DatabaseTableName")]
    // Specify more UI options using a declarative approach (https://documentation.devexpress.com/#eXpressAppFramework/CustomDocument112701).
    public class Item : BaseObject
    { // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).
        private string _code;
        private Company _company;
        private ObjectList _objectList;
        XPCollection<NumberingLine> _availableNumberingLine;
        private NumberingLine _numbering;
        private string _name;
        private string _shortName;
        private UnitOfMeasure _basedUOM;
        private DateTime _startDate;
        private DateTime _endDate;
        private BusinessPartner _principal;
        private ItemGroup _itemGroup;
        private Brand _brand;
        private OrderType _itemType;
        private bool _pis;
        private Item _partItemService;
        private string _description;
        private bool _active;
        private GlobalFunction _globFunc;
        //AuditTrail
        private XPCollection<AuditDataItemPersistent> changeHistory;

        public Item(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if (!IsLoading)
            {
                _globFunc = new GlobalFunction();
                this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.Item);
                this.ObjectList = CustomProcess.ObjectList.Item;
                this.ItemType = OrderType.Item;
            }
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).
        }

        #region Field

        [RuleRequiredField(DefaultContexts.Save)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue("Code", ref _code, value); }
        }

        [RuleRequiredField(DefaultContexts.Save)]
        public Company Company
        {
            get { return _company; }
            set { SetPropertyValue("Company", ref _company, value); }
        }

        [Browsable(false)]
        public ObjectList ObjectList
        {
            get { return _objectList; }
            set { SetPropertyValue("ObjectList", ref _objectList, value); }
        }

        [Browsable(false)]
        public XPCollection<NumberingLine> AvailableNumberingLine
        {
            get
            {
                if (ObjectList == ObjectList.None)
                {
                    _availableNumberingLine = new XPCollection<NumberingLine>(Session);
                }
                else
                {
                    _availableNumberingLine = new XPCollection<NumberingLine>(Session,
                                              new GroupOperator(GroupOperatorType.And,
                                              new BinaryOperator("ObjectList", this.ObjectList),
                                              new BinaryOperator("Selection", true),
                                              new BinaryOperator("Active", true)));
                }
                return _availableNumberingLine;

            }
        }

        [NonPersistent()]
        [ImmediatePostData()]
        [DataSourceProperty("AvailableNumberingLine", DataSourcePropertyIsNullMode.SelectAll)]
        public NumberingLine Numbering
        {
            get { return _numbering; }
            set
            {
                NumberingLine _oldNumbering = _numbering;
                SetPropertyValue("Numbering", ref _numbering, value);
                if (!IsLoading && _numbering != null && _numbering != _oldNumbering)
                {
                    this.Code = _globFunc.GetNumberingSelectionUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.Item, _numbering);
                }
            }
        }

        [RuleRequiredField(DefaultContexts.Save)]
        public string Name
        {
            get { return _name; }
            set { SetPropertyValue("Name", ref _name, value); }
        }

        public string ShortName
        {
            get { return _shortName; }
            set { SetPropertyValue("ShortName", ref _shortName, value); }
        }

        [DataSourceCriteria("Active = true")]
        public UnitOfMeasure BasedUOM
        {
            get { return _basedUOM; }
            set { SetPropertyValue("BasedUOM", ref _basedUOM, value); }
        }

        [ModelDefault("DisplayFormat", "{0:dd/MM/yyyy}")]
        [ModelDefault("EditMask", "dd/MM/yyyy")]
        public DateTime StartDate
        {
            get { return _startDate; }
            set { SetPropertyValue("StartDate", ref _startDate, value); }
        }

        [ModelDefault("DisplayFormat", "{0:dd/MM/yyyy}")]
        [ModelDefault("EditMask", "dd/MM/yyyy")]
        public DateTime EndDate
        {
            get { return _endDate; }
            set { SetPropertyValue("EndDate", ref _endDate, value); }
        }

        public BusinessPartner Principal
        {
            get { return _principal; }
            set { SetPropertyValue("Principal", ref _principal, value); }
        }

        [Association("ItemGroup-Items")]
        [DataSourceCriteria("Active = true")]
        public ItemGroup ItemGroup
        {
            get { return _itemGroup; }
            set { SetPropertyValue("ItemGroup", ref _itemGroup, value); }
        }

        public Brand Brand
        {
            get { return _brand; }
            set { SetPropertyValue("Brand", ref _brand, value); }
        }

        [ImmediatePostData()]
        public OrderType ItemType
        {
            get { return _itemType; }
            set
            {
                SetPropertyValue("ItemType", ref _itemType, value);
                if (!IsLoading)
                {
                    if (this._itemType == OrderType.Service)
                    {
                        this.PIS = true;
                    }
                    else
                    {
                        this.PIS = false;
                        this.PartItemService = null;
                    }
                }
            }
        }

        [Browsable(false)]
        public bool PIS
        {
            get { return _pis; }
            set { SetPropertyValue("PIS", ref _pis, value); }
        }

        [Appearance("PurchaseRequisitionLineSelectClose", Criteria = "PIS = false", Enabled = false)]
        public Item PartItemService
        {
            get { return _partItemService; }
            set { SetPropertyValue("PartItemService", ref _partItemService, value); }
        }

        [Size(512)]
        public string Description
        {
            get { return _description; }
            set { SetPropertyValue("Description", ref _description, value); }
        }

        public bool Active
        {
            get { return _active; }
            set { SetPropertyValue("Active", ref _active, value); }
        }

        [Association("Item-ItemUnitOfMeasures")]
        public XPCollection<ItemUnitOfMeasure> ItemUnitOfMeasures
        {
            get { return GetCollection<ItemUnitOfMeasure>("ItemUnitOfMeasures"); }
        }

        [Association("Item-ItemComponents")]
        public XPCollection<ItemComponent> ItemComponents
        {
            get { return GetCollection<ItemComponent>("ItemComponents"); }
        }

        [Association("Item-ItemAccounts")]
        public XPCollection<ItemAccount> ItemAccounts
        {
            get { return GetCollection<ItemAccount>("ItemAccounts"); }
        }

        //AuditTrail
        [CollectionOperationSet(AllowAdd = false, AllowRemove = true)]
        public XPCollection<AuditDataItemPersistent> ChangeHistory
        {
            get
            {
                if (changeHistory == null)
                {
                    changeHistory = AuditedObjectWeakReference.GetAuditTrail(Session, this);
                }
                return changeHistory;
            }
        }

        #endregion Field

    }
}