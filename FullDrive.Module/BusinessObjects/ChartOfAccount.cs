﻿using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.ConditionalAppearance;

namespace FullDrive.Module.BusinessObjects
{
    [DefaultClassOptions]
    [DefaultProperty("Name")]
    [NavigationItem("Finance & Accounting")]
    [RuleCombinationOfPropertiesIsUnique("ChartOfAccountRuleUnique", DefaultContexts.Save, "Code")]
    //[ImageName("BO_Contact")]
    //[DefaultProperty("DisplayMemberNameForLookupEditorsOfThisType")]
    //[DefaultListViewOptions(MasterDetailMode.ListViewOnly, false, NewItemRowPosition.None)]
    //[Persistent("DatabaseTableName")]
    // Specify more UI options using a declarative approach (https://documentation.devexpress.com/#eXpressAppFramework/CustomDocument112701).
    public class ChartOfAccount : BaseObject
    { // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).
        private string _code;
        private Company _company;
        private string _name;
        private FinancialStatment _financialStatment;
        private AccountGroup _accountGroup;
        private AccountType _accountType;
        private ChartOfAccount _totalingStart;
        private ChartOfAccount _totalingEnd;
        private BalanceType _balanceType;
        private double _balance;
        private AccountCharge _accountCharge;
        private bool _debitEnabled;
        private double _debit;
        private bool _creditEnabled;
        private double _credit;
        private bool _active;
        private string _localUserAccess;
        private GlobalFunction _globFunc;
        //AuditTrail
        private XPCollection<AuditDataItemPersistent> changeHistory;

        public ChartOfAccount(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if (!IsLoading)
            {
                _globFunc = new GlobalFunction();
                this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.ChartOfAccount);
                #region UserAccess
                _localUserAccess = SecuritySystem.CurrentUserName;
                UserAccess _locUserAccess = Session.FindObject<UserAccess>(new BinaryOperator("UserName", _localUserAccess));
                if (_locUserAccess != null)
                {
                    if (_locUserAccess.Employee != null)
                    {
                        if (_locUserAccess.Employee.Company != null)
                        {
                            Company = _locUserAccess.Employee.Company;
                        } 
                    }
                }
                #endregion UserAccess
            }
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).
        }

        #region Field

        [RuleRequiredField(DefaultContexts.Save)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue("Code", ref _code, value); }
        }

        [ImmediatePostData()]
        [RuleRequiredField(DefaultContexts.Save)]
        [DataSourceCriteria("Active = true")]
        public Company Company
        {
            get { return _company; }
            set { SetPropertyValue("Company", ref _company, value); }
        }

        public string Name
        {
            get { return _name; }
            set { SetPropertyValue("Name", ref _name, value); }
        }

        public FinancialStatment FinancialStatment
        {
            get { return _financialStatment; }
            set { SetPropertyValue("FinancialStatment", ref _financialStatment, value); }
        }

        public AccountGroup AccountGroup
        {
            get { return _accountGroup; }
            set { SetPropertyValue("AccountGroup", ref _accountGroup, value); }
        }

        public AccountType AccountType
        {
            get { return _accountType; }
            set { SetPropertyValue("AccountType", ref _accountType, value); }
        }

        [DataSourceCriteria("Active = true")]
        public ChartOfAccount TotallingStart
        {
            get { return _totalingStart; }
            set { SetPropertyValue("TotallingStart", ref _totalingStart, value); }
        }

        [DataSourceCriteria("Active = true")]
        public ChartOfAccount TotallingEnd
        {
            get { return _totalingEnd; }
            set { SetPropertyValue("TotallingEnd", ref _totalingEnd, value); }
        }

        public BalanceType BalanceType
        {
            get { return _balanceType; }
            set { SetPropertyValue("BalanceType", ref _balanceType, value); }
        }

        public double Balance
        {
            get { return _balance; }
            set { SetPropertyValue("Balance", ref _balance, value); }
        }

        [ImmediatePostData()]
        public AccountCharge AccountCharge
        {
            get { return _accountCharge; }
            set
            {
                SetPropertyValue("AccountCharge", ref _accountCharge, value);
                if (!IsLoading)
                {
                    if (this._accountCharge == AccountCharge.Debit)
                    {
                        this.DebitEnabled = true;
                        this.CreditEnabled = false;
                    }
                    else if (this._accountCharge == AccountCharge.Credit)
                    {
                        this.DebitEnabled = false;
                        this.CreditEnabled = true;
                    }
                    else if (this._accountCharge == AccountCharge.Both)
                    {
                        this.DebitEnabled = true;
                        this.CreditEnabled = true;
                    }
                    else
                    {
                        this.DebitEnabled = false;
                        this.CreditEnabled = false;
                    }
                }
            }
        }

        [Browsable(false)]
        public bool DebitEnabled
        {
            get { return _debitEnabled; }
            set { SetPropertyValue("DebitEnabled", ref _debitEnabled, value); }
        }

        [Appearance("ChartOfAccountDebitEnabled", Criteria = "DebitEnabled = false", Enabled = false)]
        public double Debit
        {
            get { return _debit; }
            set { SetPropertyValue("Debit", ref _debit, value); }
        }

        [Browsable(false)]
        public bool CreditEnabled
        {
            get { return _creditEnabled; }
            set { SetPropertyValue("CreditEnabled", ref _creditEnabled, value); }
        }

        [Appearance("ChartOfAccountCreditEnabled", Criteria = "CreditEnabled = false", Enabled = false)]
        public double Credit
        {
            get { return _credit; }
            set { SetPropertyValue("Credit", ref _credit, value); }
        }

        public bool Active
        {
            get { return _active; }
            set { SetPropertyValue("Active", ref _active, value); }
        }

        //AuditTrail
        [CollectionOperationSet(AllowAdd = false, AllowRemove = true)]
        public XPCollection<AuditDataItemPersistent> ChangeHistory
        {
            get
            {
                if (changeHistory == null)
                {
                    changeHistory = AuditedObjectWeakReference.GetAuditTrail(Session, this);
                }
                return changeHistory;
            }
        }

        #endregion Field
    }
}