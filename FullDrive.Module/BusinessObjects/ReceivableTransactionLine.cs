﻿using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;

using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.ConditionalAppearance;
using DevExpress.ExpressApp.SystemModule;

namespace FullDrive.Module.BusinessObjects
{
    [DefaultClassOptions]
    [DefaultProperty("Code")]
    [NavigationItem("Finance & Accounting")]
    [RuleCombinationOfPropertiesIsUnique("ReceivableTransactionLineRuleUnique", DefaultContexts.Save, "Code")]
    //[ImageName("BO_Contact")]
    //[DefaultProperty("DisplayMemberNameForLookupEditorsOfThisType")]
    //[DefaultListViewOptions(MasterDetailMode.ListViewOnly, false, NewItemRowPosition.None)]
    //[Persistent("DatabaseTableName")]
    // Specify more UI options using a declarative approach (https://documentation.devexpress.com/#eXpressAppFramework/CustomDocument112701).
    public class ReceivableTransactionLine : BaseObject
    { // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).
        private bool _activationPosting;
        private int _no;
        private string _code;
        private bool _select;
        #region InitialOrganization
        private Company _company;
        private Workplace _workplace;
        private Division _division;
        private Department _department;
        private Section _section;
        private Employee _employee;
        private Division _div;
        private Department _dept;
        private Section _sect;
        private XPCollection<Employee> _availablePIC;
        private Employee _pic;
        private Employee _collector;
        #endregion InitialOrganization
        private XPCollection<Vehicle> _availableVehicle;
        private Vehicle _vehicle;
        private Currency _currency;
        private DateTime _postingDate;
        private SalesInvoice _salesInvoice;
        private PriceGroup _priceGroup;
        private PostingType _salesType;
        private PostingMethod _postingMethod;
        private PaymentMethodType _paymentMethodType;
        private PaymentMethod _paymentMethod;
        private PaymentType _paymentType;
        private bool _openDueDate;
        private DateTime _estimatedDate;
        private DateTime _actualDate;
        private DateTime _dueDate;
        private string _chequeNo;
        private double _amount;
        private bool _openDN;
        private bool _openCN;
        private double _amountDN;
        private double _amountCN;
        private double _plan;
        private double _outstanding;
        private double _actual;
        private double _overPayment;
        private bool _settled; 
        private string _description;
        #region InitialBank
        private bool _multiBank;
        private bool _bankBasedInvoice;
        private XPCollection<BankAccount> _availableCompanyBankAccount;
        private BankAccount _companyBankAccount;
        private string _companyAccountNo;
        private string _companyAccountName;
        private XPCollection<BankAccount> _availableBankAccount;
        private BankAccount _bankAccount;
        private string _accountNo;
        private string _accountName;
        #endregion InitialBank
        private DateTime _docDate;
        private int _journalMonth;
        private int _journalYear;
        private Status _status;
        private DateTime _statusdate;
        private int _postedCount;
        private string _signCode;
        private OrderCollectionMonitoring _orderCollectionMonitoring;
        private DateTime _pickingDate;
        private Picking _picking;
        private PickingMonitoring _pickingMonitoring;
        private SalesInvoiceMonitoring _salesInvoiceMonitoring;
        private ReceivableTransaction _receivableTransaction; 
        private string _userAccessIn;
        private GlobalFunction _globFunc;
        //AuditTrail
        private XPCollection<AuditDataItemPersistent> changeHistory;

        public ReceivableTransactionLine(Session session)
            : base(session)
        {
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if (!IsLoading)
            {
                if(Session.IsNewObject(this))
                {
                    DateTime now = DateTime.Now;
                    _globFunc = new GlobalFunction();
                    #region UserAccess
                    _userAccessIn = SecuritySystem.CurrentUserName;
                    UserAccess _locUserAccess = Session.FindObject<UserAccess>(new BinaryOperator("UserName", _userAccessIn));
                    if (_locUserAccess != null)
                    {
                        if (_locUserAccess.Employee != null)
                        {
                            if (_locUserAccess.Employee.Company != null && _locUserAccess.Employee.Workplace != null)
                            {
                                this.Code = _globFunc.GetNumberingUnlockOptimisticRecordBasedOrganization(this.Session.DataLayer, ObjectList.ReceivableTransactionLine, _locUserAccess.Employee.Company, _locUserAccess.Employee.Workplace);
                            }
                            else
                            {
                                this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.ReceivableTransactionLine);
                            }
                        }
                    }
                    #endregion UserAccess
                    this.Select = true;
                    this.Status = Status.Open;
                    this.StatusDate = now;
                    this.SalesType = PostingType.Sales;
                    this.PostingMethod = PostingMethod.Bill;
                }                
            }
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).
        }

        #region Field

        protected override void OnSaving()
        {
            base.OnSaving();
            if (!IsLoading && IsSaving)
            {
                UpdateNo();
            }
        }

        protected override void OnDeleting()
        {
            base.OnDeleting();
            if (!IsLoading)
            {
                RecoveryDeleteNo();
            }
        }       

        [Browsable(false)]
        [ImmediatePostData()]
        public bool ActivationPosting
        {
            get { return _activationPosting; }
            set { SetPropertyValue("ActivationPosting", ref _activationPosting, value); }
        }

        [Appearance("ReceivableTransactionLineNoClose", Enabled = false)]
        public int No
        {
            get { return _no; }
            set { SetPropertyValue("No", ref _no, value); }
        }

        [Appearance("ReceivableTransactionLineClose", Enabled = false)]
        [RuleRequiredField(DefaultContexts.Save)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue("Code", ref _code, value); }
        }

        [Appearance("ReceivableTransactionLineSelectClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public bool Select
        {
            get { return _select; }
            set { SetPropertyValue("Select", ref _select, value); }
        }

        #region Organization

        [Appearance("ReceivableTransactionLineCompanyClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Company Company
        {
            get { return _company; }
            set { SetPropertyValue("Company", ref _company, value); }
        }

        [Appearance("ReceivableTransactionLineWorkplaceClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Workplace Workplace
        {
            get { return _workplace; }
            set { SetPropertyValue("Workplace", ref _workplace, value); }
        }

        [Browsable(false)]
        public Division Division
        {
            get { return _division; }
            set { SetPropertyValue("Division", ref _division, value); }
        }

        [Browsable(false)]
        public Department Department
        {
            get { return _department; }
            set { SetPropertyValue("Department", ref _department, value); }
        }

        [Browsable(false)]
        public Section Section
        {
            get { return _section; }
            set { SetPropertyValue("Section", ref _section, value); }
        }

        [Browsable(false)]
        public Employee Employee
        {
            get { return _employee; }
            set { SetPropertyValue("Employee", ref _employee, value); }
        }

        [VisibleInListView(false)]
        [ImmediatePostData()]
        [DataSourceCriteria("Company = '@This.Company' And Active = true")]
        [Appearance("ReceivableTransactionLineDivClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Division Div
        {
            get { return _div; }
            set { SetPropertyValue("Div", ref _div, value); }
        }

        [VisibleInListView(false)]
        [ImmediatePostData()]
        [DataSourceCriteria("Company = '@This.Company' And Division = '@This.Div' And Active = true")]
        [Appearance("ReceivableTransactionLineDeptClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Department Dept
        {
            get { return _dept; }
            set { SetPropertyValue("Dept", ref _dept, value); }
        }

        [VisibleInListView(false)]
        [ImmediatePostData()]
        [DataSourceCriteria("Company = '@This.Company' And Department = '@This.Dept' And Active = true")]
        [Appearance("ReceivableTransactionLineSectClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Section Sect
        {
            get { return _sect; }
            set { SetPropertyValue("Sect", ref _sect, value); }
        }

        [Browsable(false)]
        public XPCollection<Employee> AvailablePIC
        {
            get
            {
                XPCollection<Employee> _locPICs = null;
                if (this.Company != null && this.Workplace != null)
                {
                    #region DivDeptSect
                    if (this.Div != null && this.Dept != null && this.Sect != null)
                    {
                        _locPICs = new XPCollection<Employee>
                                  (Session, new GroupOperator(GroupOperatorType.And,
                                    new BinaryOperator("Company", this.Company),
                                    new BinaryOperator("Workplace", this.Workplace),
                                    new BinaryOperator("Division", this.Div),
                                    new BinaryOperator("Department", this.Dept),
                                    new BinaryOperator("Section", this.Sect),
                                    new BinaryOperator("Active", true),
                                    new GroupOperator(GroupOperatorType.Or,
                                    new BinaryOperator("SalesRole", SalesRole.TakingOrder),
                                    new BinaryOperator("SalesRole", SalesRole.All))));
                    }
                    else if (this.Div != null && this.Dept != null && this.Sect == null)
                    {
                        _locPICs = new XPCollection<Employee>
                                  (Session, new GroupOperator(GroupOperatorType.And,
                                    new BinaryOperator("Company", this.Company),
                                    new BinaryOperator("Workplace", this.Workplace),
                                    new BinaryOperator("Division", this.Div),
                                    new BinaryOperator("Department", this.Dept),
                                    new BinaryOperator("Active", true),
                                    new GroupOperator(GroupOperatorType.Or,
                                    new BinaryOperator("SalesRole", SalesRole.TakingOrder),
                                    new BinaryOperator("SalesRole", SalesRole.All))));

                    }
                    else if (this.Div != null && this.Dept == null && this.Sect == null)
                    {
                        _locPICs = new XPCollection<Employee>
                                  (Session, new GroupOperator(GroupOperatorType.And,
                                    new BinaryOperator("Company", this.Company),
                                    new BinaryOperator("Workplace", this.Workplace),
                                    new BinaryOperator("Division", this.Div),
                                    new BinaryOperator("Active", true),
                                    new GroupOperator(GroupOperatorType.Or,
                                    new BinaryOperator("SalesRole", SalesRole.TakingOrder),
                                    new BinaryOperator("SalesRole", SalesRole.All))));

                    }
                    else if (this.Div == null && this.Dept == null && this.Sect == null)
                    {
                        _locPICs = new XPCollection<Employee>
                                  (Session, new GroupOperator(GroupOperatorType.And,
                                    new BinaryOperator("Company", this.Company),
                                    new BinaryOperator("Workplace", this.Workplace),
                                    new BinaryOperator("Active", true),
                                    new GroupOperator(GroupOperatorType.Or,
                                    new BinaryOperator("SalesRole", SalesRole.TakingOrder),
                                    new BinaryOperator("SalesRole", SalesRole.All))));

                    }
                    #endregion DivDeptSect
                    if (_locPICs != null && _locPICs.Count() > 0)
                    {
                        _availablePIC = _locPICs;
                    }
                }
                else
                {
                    _availablePIC = new XPCollection<Employee>(Session, new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("Active", true),
                                                            new GroupOperator(GroupOperatorType.Or,
                                                            new BinaryOperator("SalesRole", SalesRole.TakingOrder),
                                                            new BinaryOperator("SalesRole", SalesRole.All))));
                }

                return _availablePIC;

            }
        }

        [ImmediatePostData()]
        [DataSourceProperty("AvailablePIC")]
        [Appearance("ReceivableTransactionLinePICClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Employee PIC
        {
            get { return _pic; }
            set
            {
                SetPropertyValue("Salesman", ref _pic, value);
                if (!IsLoading)
                {
                    if (this._pic != null)
                    {
                        SetVehicle(_pic);
                    }
                    else
                    {
                        this.Vehicle = null;
                    }
                }
            }
        }

        [ImmediatePostData()]
        [DataSourceCriteria("Company = '@This.Company' And Workplace = '@This.Workplace' And Active = true")]
        [Appearance("ReceivableTransactionLineCollectorClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Employee Collector
        {
            get { return _collector; }
            set { SetPropertyValue("Collector", ref _collector, value); }
        }

        #endregion Organization

        [Browsable(false)]
        public XPCollection<Vehicle> AvailableVehicle
        {
            get
            {
                if (!IsLoading)
                {
                    if (this.Company != null && this.Workplace != null)
                    {
                        string _beginString = null;
                        string _endString = null;
                        string _fullString = null;

                        #region PIC
                        if (this.PIC != null)
                        {
                            List<string> _stringSVS = new List<string>();

                            XPCollection<SalesmanVehicleSetup> _locSalesmanVehicleSetups = new XPCollection<SalesmanVehicleSetup>
                                                                                      (Session, new GroupOperator(GroupOperatorType.And,
                                                                                       new BinaryOperator("Company", this.Company),
                                                                                       new BinaryOperator("Workplace", this.Workplace),
                                                                                       new BinaryOperator("Salesman", this.PIC),
                                                                                       new BinaryOperator("Active", true)));

                            if (_locSalesmanVehicleSetups != null && _locSalesmanVehicleSetups.Count() > 0)
                            {
                                foreach (SalesmanVehicleSetup _locSalesmanVehicleSetup in _locSalesmanVehicleSetups)
                                {
                                    if (_locSalesmanVehicleSetup.Vehicle != null)
                                    {
                                        if (_locSalesmanVehicleSetup.Vehicle.Code != null)
                                        {
                                            _stringSVS.Add(_locSalesmanVehicleSetup.Vehicle.Code);
                                        }
                                    }
                                }
                            }

                            IEnumerable<string> _stringArraySVSDistinct = _stringSVS.Distinct();
                            string[] _stringArraySVSList = _stringArraySVSDistinct.ToArray();
                            if (_stringArraySVSList.Length == 1)
                            {
                                for (int i = 0; i < _stringArraySVSList.Length; i++)
                                {
                                    Vehicle _locV = Session.FindObject<Vehicle>(new BinaryOperator("Code", _stringArraySVSList[i]));
                                    if (_locV != null)
                                    {
                                        if (i == 0)
                                        {
                                            _beginString = "[Code]=='" + _locV.Code + "'";
                                        }
                                    }
                                }
                            }
                            else if (_stringArraySVSList.Length > 1)
                            {
                                for (int i = 0; i < _stringArraySVSList.Length; i++)
                                {
                                    Vehicle _locV = Session.FindObject<Vehicle>(new BinaryOperator("Code", _stringArraySVSList[i]));
                                    if (_locV != null)
                                    {
                                        if (i == 0)
                                        {
                                            _beginString = "[Code]=='" + _locV.Code + "'";
                                        }
                                        else
                                        {
                                            _endString = _endString + " OR [Code]=='" + _locV.Code + "'";
                                        }
                                    }
                                }
                            }
                            _fullString = _beginString + _endString;

                            if (_fullString != null && _fullString != "")
                            {
                                _availableVehicle = new XPCollection<Vehicle>(Session, CriteriaOperator.Parse(_fullString));
                            }
                            else
                            {
                                _availableVehicle = new XPCollection<Vehicle>(Session, new GroupOperator(GroupOperatorType.And,
                                                                                    new BinaryOperator("Company", this.Company),
                                                                                    new BinaryOperator("Workplace", this.Workplace),
                                                                                    new BinaryOperator("Active", true)));
                            }
                        }
                        #endregion PIC
                        #region NonPIC
                        else
                        {
                            _availableVehicle = new XPCollection<Vehicle>(Session, new GroupOperator(GroupOperatorType.And,
                                                                            new BinaryOperator("Company", this.Company),
                                                                            new BinaryOperator("Workplace", this.Workplace),
                                                                            new BinaryOperator("Active", true)));
                        }
                        #endregion NonPIC
                    }
                    else
                    {
                        _availableVehicle = new XPCollection<Vehicle>(Session, new GroupOperator(GroupOperatorType.And,
                                                                            new BinaryOperator("Active", true)));
                    }

                }

                return _availableVehicle;

            }
        }

        [Browsable(false)]
        [ImmediatePostData()]
        [DataSourceProperty("AvailableVehicle")]
        [Appearance("ReceivableTransactionLineVehicleClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Vehicle Vehicle
        {
            get { return _vehicle; }
            set { SetPropertyValue("Vehicle", ref _vehicle, value); }
        }

        [Appearance("ReceivableTransactionLinePostingDateClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public DateTime PostingDate
        {
            get { return _postingDate; }
            set { SetPropertyValue("PostingDate", ref _postingDate, value); }
        }

        [Appearance("ReceivableTransactionLineSalesInvoiceClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public SalesInvoice SalesInvoice
        {
            get { return _salesInvoice; }
            set { SetPropertyValue("SalesInvoice", ref _salesInvoice, value); }
        }

        [Appearance("ReceivableTransactionLineCurrencyClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Currency Currency
        {
            get { return _currency; }
            set { SetPropertyValue("Currency", ref _currency, value); }
        }

        [Appearance("ReceivableTransactionLineSalesTypeClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public PostingType SalesType
        {
            get { return _salesType; }
            set { SetPropertyValue("PostingType", ref _salesType, value); }
        }

        [Appearance("ReceivableTransactionLinePostingMethodClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public PostingMethod PostingMethod
        {
            get { return _postingMethod; }
            set { SetPropertyValue("PostingMethod", ref _postingMethod, value); }
        }

        [VisibleInListView(false)]
        [Appearance("ReceivableTransactionLine2PriceGroupClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public PriceGroup PriceGroup
        {
            get { return _priceGroup; }
            set { SetPropertyValue("PriceGroup", ref _priceGroup, value); }
        }

        [VisibleInListView(false)]
        [Appearance("ReceivableTransactionLinePaymentMethodTypeClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public PaymentMethodType PaymentMethodType
        {
            get { return _paymentMethodType; }
            set { SetPropertyValue("PaymentMethodType", ref _paymentMethodType, value); }
        }

        [VisibleInListView(false)]
        [Appearance("ReceivableTransactionLinePaymentMethodClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public PaymentMethod PaymentMethod
        {
            get { return _paymentMethod; }
            set { SetPropertyValue("PaymentMethod", ref _paymentMethod, value); }
        }

        [ImmediatePostData()]
        [Appearance("ReceivableTransactionLinePaymentTypeClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public PaymentType PaymentType
        {
            get { return _paymentType; }
            set
            {
                SetPropertyValue("PaymentType", ref _paymentType, value);
                if (!IsLoading)
                {
                    if (this._paymentType == PaymentType.Cheque)
                    {
                        this.OpenDueDate = true;
                    }
                    else
                    {
                        this.OpenDueDate = false;
                    }
                }
            }
        }

        [Browsable(false)]
        [ImmediatePostData()]
        public bool OpenDueDate
        {
            get { return _openDueDate; }
            set { SetPropertyValue("OpenDueDate", ref _openDueDate, value); }
        }

        [Appearance("ReceivableTransactionLineChequeNoClose1", Criteria = "OpenDueDate = true", Enabled = true)]
        [Appearance("ReceivableTransactionLineChequeNoClose2", Criteria = "OpenDueDate = false", Enabled = false)]
        [Appearance("ReceivableTransactionLineChequeNoClose3", Criteria = "ActivationPosting = true", Enabled = false)]
        public string ChequeNo
        {
            get { return _chequeNo; }
            set { SetPropertyValue("ChequeNo", ref _chequeNo, value); }
        }

        [ModelDefault("DisplayFormat", "{0:dd/MM/yyyy}")]
        [ModelDefault("EditMask", "dd/MM/yyyy")]
        [Appearance("ReceivableTransactionLineEstimatedDateClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public DateTime EstimatedDate
        {
            get { return _estimatedDate; }
            set { SetPropertyValue("EstimatedDate", ref _estimatedDate, value); }
        }

        [ModelDefault("DisplayFormat", "{0:dd/MM/yyyy}")]
        [ModelDefault("EditMask", "dd/MM/yyyy")]
        [Appearance("ReceivableTransactionLineActualDateClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public DateTime ActualDate
        {
            get { return _actualDate; }
            set { SetPropertyValue("ActualDate", ref _actualDate, value); }
        }

        [ModelDefault("DisplayFormat", "{0:dd/MM/yyyy}")]
        [ModelDefault("EditMask", "dd/MM/yyyy")]
        [Appearance("ReceivableTransactionLineDueDateClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public DateTime DueDate
        {
            get { return _dueDate; }
            set { SetPropertyValue("DueDate", ref _dueDate, value); }
        }

        [ImmediatePostData()]
        [Appearance("ReceivableTransactionLineAmountClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public double Amount
        {
            get { return _amount; }
            set { SetPropertyValue("Amount", ref _amount, value); }
        }

        [Browsable(false)]
        [ImmediatePostData()]
        public bool OpenDN
        {
            get { return _openDN; }
            set { SetPropertyValue("OpenDN", ref _openDN, value); }
        }

        [ImmediatePostData()]
        [Appearance("ReceivableTransactionLineAmountDNClose1", Criteria = "OpenDN = true", Enabled = false)]
        [Appearance("ReceivableTransactionLineAmountDNClose2", Criteria = "ActivationPosting = true", Enabled = false)]
        public double AmountDN
        {
            get { return _amountDN; }
            set
            {
                SetPropertyValue("AmountDN", ref _amountDN, value);
                if (!IsLoading)
                {
                    SetTotalPlan();
                }
            }
        }

        [Browsable(false)]
        [ImmediatePostData()]
        public bool OpenCN
        {
            get { return _openCN; }
            set { SetPropertyValue("OpenCN", ref _openCN, value); }
        }

        [ImmediatePostData()]
        [Appearance("ReceivableTransactionLineAmountCNClose1", Criteria = "OpenCN = true", Enabled = false)]
        [Appearance("ReceivableTransactionLineAmountCNClose2", Criteria = "ActivationPosting = true", Enabled = false)]
        public double AmountCN
        {
            get { return _amountCN; }
            set
            {
                SetPropertyValue("AmountCN", ref _amountCN, value);
                if (!IsLoading)
                {
                    SetTotalPlan();
                }
            }
        }

        [Appearance("ReceivableTransactionLinePlanClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public double Plan
        {
            get { return _plan; }
            set { SetPropertyValue("Plan", ref _plan, value); }
        }

        [Appearance("ReceivableTransactionLineOutstandingClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public double Outstanding
        {
            get { return _outstanding; }
            set { SetPropertyValue("Outstanding", ref _outstanding, value); }
        }

        [Appearance("ReceivableTransactionLineActualClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public double Actual
        {
            get { return _actual; }
            set { SetPropertyValue("Actual", ref _actual, value); }
        }

        [Appearance("ReceivableTransactionLineOverPaymentClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public double OverPayment
        {
            get { return _overPayment; }
            set { SetPropertyValue("OverPayment", ref _overPayment, value); }
        }

        [Appearance("ReceivableTransactionLineSettledClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public bool Settled
        {
            get { return _settled; }
            set { SetPropertyValue("Settled", ref _settled, value); }
        }

        [Appearance("ReceivableTransactionLineDescriptionClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [Size(512)]
        public string Description
        {
            get { return _description; }
            set { SetPropertyValue("Description", ref _description, value); }
        }

        #region Bank

        [ImmediatePostData()]
        [Appearance("ReceivableTransactionLineMultiBankClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public bool MultiBank
        {
            get { return _multiBank; }
            set {
                SetPropertyValue("MultiBank", ref _multiBank, value);
                if(!IsLoading)
                {
                    if(this._multiBank == true)
                    {
                        this.BankBasedInvoice = false;
                        this.CompanyBankAccount = null;
                        this.CompanyAccountNo = null;
                        this.CompanyAccountName = null;
                        this.BankAccount = null;
                        this.AccountNo = null;
                        this.AccountName = null;
                    }
                }
            }
        }

        [ImmediatePostData()]
        [Appearance("ReceivableTransactionLineBankBasedInvoiceClose", Criteria = "MultiBank = true", Enabled = false)]
        [Appearance("ReceivableTransactionLineBankBasedInvoiceClose2", Criteria = "ActivationPosting = true", Enabled = false)]
        public bool BankBasedInvoice
        {
            get { return _bankBasedInvoice; }
            set {
                SetPropertyValue("BankBasedInvoice", ref _bankBasedInvoice, value);
                if(!IsLoading)
                {
                    if(this._bankBasedInvoice == true)
                    {
                        if(this.SalesInvoice != null)
                        {
                            if (this.SalesInvoice.CompanyBankAccount != null) { this.CompanyBankAccount = this.SalesInvoice.CompanyBankAccount; }
                            if (this.SalesInvoice.CompanyAccountName != null) { this.CompanyAccountName = this.SalesInvoice.CompanyAccountName; }
                            if (this.SalesInvoice.CompanyAccountNo != null) { this.CompanyAccountNo = this.SalesInvoice.CompanyAccountNo; }
                            if (this.SalesInvoice.BankAccount != null) { this.BankAccount = this.SalesInvoice.BankAccount; }
                            if (this.SalesInvoice.AccountName != null) { this.AccountName = this.SalesInvoice.AccountName; }
                            if (this.SalesInvoice.AccountNo != null) { this.CompanyAccountNo = this.SalesInvoice.AccountNo; }
                        }
                    }
                    else
                    {
                        this.CompanyBankAccount = null;
                        this.CompanyAccountName = null;
                        this.CompanyAccountNo = null;
                        this.BankAccount = null;
                        this.AccountName = null; 
                        this.AccountNo = null; 
                    }
                }
            }
        }

        [Browsable(false)]
        public XPCollection<BankAccount> AvailableCompanyBankAccount
        {
            get
            {
                if (!IsLoading)
                {
                    if (this.Company != null)
                    {
                        if (this.Workplace != null)
                        {
                            _availableCompanyBankAccount = new XPCollection<BankAccount>(Session, new GroupOperator
                                                    (GroupOperatorType.And,
                                                    new BinaryOperator("Company", this.Company),
                                                    new BinaryOperator("Workplace", this.Workplace),
                                                    new BinaryOperator("Active", true)));
                        }
                        else
                        {
                            _availableCompanyBankAccount = new XPCollection<BankAccount>(Session, new GroupOperator
                                                    (GroupOperatorType.And,
                                                    new BinaryOperator("Company", this.Company),
                                                    new BinaryOperator("Active", true)));
                        }
                    }
                    else
                    {
                        _availableCompanyBankAccount = new XPCollection<BankAccount>(Session,
                            new GroupOperator(GroupOperatorType.And,
                            new BinaryOperator("Active", true)));
                    }
                }

                return _availableCompanyBankAccount;

            }
        }

        [ImmediatePostData()]
        [DataSourceProperty("AvailableCompanyBankAccount")]
        [Appearance("ReceivableTransactionLineCompanyBankAccountClose", Criteria = "MultiBank = true", Enabled = false)]
        [Appearance("ReceivableTransactionLineCompanyBankAccountClose2", Criteria = "ActivationPosting = true", Enabled = false)]
        public BankAccount CompanyBankAccount
        {
            get { return _companyBankAccount; }
            set
            {
                SetPropertyValue("CompanyBankAccount", ref _companyBankAccount, value);
                if (!IsLoading)
                {
                    if (this._companyBankAccount != null)
                    {
                        this.CompanyAccountNo = this._companyBankAccount.AccountNo;
                        this.CompanyAccountName = this._companyBankAccount.AccountName;
                    }
                }
            }
        }

        [Appearance("ReceivableTransactionLineCompanyAccountNoClose", Criteria = "MultiBank = true", Enabled = false)]
        [Appearance("ReceivableTransactionLineCompanyAccountNoClose2", Criteria = "ActivationPosting = true", Enabled = false)]
        public string CompanyAccountNo
        {
            get { return _companyAccountNo; }
            set { SetPropertyValue("CompanyAccountNo", ref _companyAccountNo, value); }
        }

        [Appearance("ReceivableTransactionLineCompanyAccountNameClose", Criteria = "MultiBank = true", Enabled = false)]
        [Appearance("ReceivableTransactionLineCompanyAccountNameClose2", Criteria = "ActivationPosting = true", Enabled = false)]
        public string CompanyAccountName
        {
            get { return _companyAccountName; }
            set { SetPropertyValue("CompanyAccountName", ref _companyAccountName, value); }
        }

        [Browsable(false)]
        public XPCollection<BankAccount> AvailableBankAccount
        {
            get
            {
                if (this.Company != null && this.Workplace != null)
                {
                    if(this.SalesInvoice != null)
                    {
                        if(this.SalesInvoice.BillToCustomer != null)
                        {
                            _availableBankAccount = new XPCollection<BankAccount>(Session,
                                               new GroupOperator(GroupOperatorType.And,
                                               new BinaryOperator("Company", this.Company),
                                               new BinaryOperator("Workplace", this.Workplace),
                                               new BinaryOperator("BusinessPartner", this.SalesInvoice.BillToCustomer),
                                               new BinaryOperator("Active", true)));
                        }
                    }
                }
                else
                {

                    _availableBankAccount = new XPCollection<BankAccount>(Session,
                                            new GroupOperator(GroupOperatorType.And,
                                            new BinaryOperator("Active", true)));

                }
                return _availableBankAccount;

            }
        }

        [ImmediatePostData()]
        [Appearance("ReceivableTransactionLineBankAccountClose", Criteria = "MultiBank = true", Enabled = false)]
        [Appearance("ReceivableTransactionLineBankAccountClose2", Criteria = "ActivationPosting = true", Enabled = false)]
        [DataSourceProperty("AvailableBankAccount")]
        [DataSourceCriteria("Active = true")]
        public BankAccount BankAccount
        {
            get { return _bankAccount; }
            set
            {
                SetPropertyValue("BankAccount", ref _bankAccount, value);
                if (!IsLoading)
                {
                    if (this._bankAccount != null)
                    {
                        this.AccountNo = this._bankAccount.AccountNo;
                        this.AccountName = this._bankAccount.AccountName;
                    }
                }
            }
        }

        [Appearance("ReceivableTransactionLineAccountNoClose", Criteria = "MultiBank = true", Enabled = false)]
        [Appearance("ReceivableTransactionLineAccountNoClose2", Criteria = "ActivationPosting = true", Enabled = false)]
        public string AccountNo
        {
            get { return _accountNo; }
            set { SetPropertyValue("AccountNo", ref _accountNo, value); }
        }

        [Appearance("ReceivableTransactionLineAccountNameClose", Criteria = "MultiBank = true", Enabled = false)]
        [Appearance("ReceivableTransactionLineAccountNameClose2", Criteria = "ActivationPosting = true", Enabled = false)]
        public string AccountName
        {
            get { return _accountName; }
            set { SetPropertyValue("AccountName", ref _accountName, value); }
        }

        #endregion Bank

        [Browsable(false)]
        [ImmediatePostData()]
        [ModelDefault("DisplayFormat", "{0:dd/MM/yyyy}")]
        [ModelDefault("EditMask", "dd/MM/yyyy")]
        [Appearance("ReceivableTransactionLineDocDateClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public DateTime DocDate
        {
            get { return _docDate; }
            set
            {
                SetPropertyValue("DocDate", ref _docDate, value);
                if (!IsLoading)
                {
                    this.JournalMonth = this._docDate.Month;
                    this.JournalYear = this._docDate.Year;
                }
            }
        }

        [Browsable(false)]
        public int JournalMonth
        {
            get { return _journalMonth; }
            set { SetPropertyValue("JournalMonth", ref _journalMonth, value); }
        }

        [Browsable(false)]
        public int JournalYear
        {
            get { return _journalYear; }
            set { SetPropertyValue("JournalYear", ref _journalYear, value); }
        }

        [Appearance("ReceivableTransactionLineStatusClose", Enabled = false)]
        public Status Status
        {
            get { return _status; }
            set { SetPropertyValue("Status", ref _status, value); }
        }

        [ModelDefault("DisplayFormat", "{0:dd/MM/yyyy}")]
        [ModelDefault("EditMask", "dd/MM/yyyy")]
        [Appearance("ReceivableTransactionLineStatusDateClose", Enabled = false)]
        public DateTime StatusDate
        {
            get { return _statusdate; }
            set { SetPropertyValue("StatusDate", ref _statusdate, value); }
        } 

        [Appearance("ReceivableTransactionLinePostedCountEnabled", Enabled = false)]
        public int PostedCount
        {
            get { return _postedCount; }
            set { SetPropertyValue("PostedCount", ref _postedCount, value); }
        }

        [Appearance("ReceivableTransactionLineSignCodeClose", Enabled = false)]
        public string SignCode
        {
            get { return _signCode; }
            set { SetPropertyValue("SignCode", ref _signCode, value); }
        }

        [Browsable(false)]
        public OrderCollectionMonitoring OrderCollectionMonitoring
        {
            get { return _orderCollectionMonitoring; }
            set { SetPropertyValue("OrderCollectionMonitoring", ref _orderCollectionMonitoring, value); }
        }

        [VisibleInListView(false)]
        [ImmediatePostData()]
        [ModelDefault("DisplayFormat", "{0:dd/MM/yyyy}")]
        [ModelDefault("EditMask", "dd/MM/yyyy")]
        [Appearance("ReceivableTransactionLinePickingDateClose", Enabled = false)]
        public DateTime PickingDate
        {
            get { return _pickingDate; }
            set { SetPropertyValue("PickingDate", ref _pickingDate, value); }

        }

        [VisibleInListView(false)]
        [Appearance("ReceivableTransactionLinePickingClose", Enabled = false)]
        public Picking Picking
        {
            get { return _picking; }
            set { SetPropertyValue("Picking", ref _picking, value); }
        }

        [VisibleInListView(false)]
        [Appearance("ReceivableTransactionLinePickingMonitoringClose", Enabled = false)]
        public PickingMonitoring PickingMonitoring
        {
            get { return _pickingMonitoring; }
            set { SetPropertyValue("PickingMonitoring", ref _pickingMonitoring, value); }
        }

        [Browsable(false)]
        [Appearance("ReceivableTransactionLineSalesInvoiceMonitoringClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public SalesInvoiceMonitoring SalesInvoiceMonitoring
        {
            get { return _salesInvoiceMonitoring; }
            set { SetPropertyValue("SalesInvoiceMonitoring", ref _salesInvoiceMonitoring, value); }
        }

        [ImmediatePostData()]
        [Association("ReceivableTransaction-ReceivableTransactionLines")]
        public ReceivableTransaction ReceivableTransaction
        {
            get { return _receivableTransaction; }
            set
            {
                SetPropertyValue("ReceivableTransaction", ref _receivableTransaction, value);
                if (!IsLoading)
                {
                    if (this._receivableTransaction != null)
                    {
                        if (this._receivableTransaction.Company != null) { this.Company = this._receivableTransaction.Company; }
                        if (this._receivableTransaction.Workplace != null) { this.Workplace = this._receivableTransaction.Workplace; }
                        if (this._receivableTransaction.Division != null) { this.Division = this._receivableTransaction.Division; }
                        if (this._receivableTransaction.Department != null) { this.Department = this._receivableTransaction.Department; }
                        if (this._receivableTransaction.Section != null) { this.Section = this._receivableTransaction.Section; }
                        if (this._receivableTransaction.Employee != null) { this.Employee = this._receivableTransaction.Employee; }
                        if (this._receivableTransaction.Currency != null) { this.Currency = this._receivableTransaction.Currency; }
                        if(BankBasedInvoice == false)
                        {
                            if (this._receivableTransaction.CompanyBankAccount != null) { this.CompanyBankAccount = this._receivableTransaction.CompanyBankAccount; }
                            if (this._receivableTransaction.CompanyAccountName != null) { this.CompanyAccountName = this._receivableTransaction.CompanyAccountName; }
                            if (this._receivableTransaction.CompanyAccountNo != null) { this.CompanyAccountNo = this._receivableTransaction.CompanyAccountNo; }
                            if (this._receivableTransaction.BankAccount != null) { this.BankAccount = this._receivableTransaction.BankAccount; }
                            if (this._receivableTransaction.AccountName != null) { this.AccountName = this._receivableTransaction.AccountName; }
                            if (this._receivableTransaction.AccountNo != null) { this.CompanyAccountNo = this._receivableTransaction.AccountNo; }
                        }
                        
                    }
                }
            }
        }

        [Association("ReceivableTransactionLine-ReceivableTransactionBanks")]
        public XPCollection<ReceivableTransactionBank> ReceivableTransactionBanks
        {
            get { return GetCollection<ReceivableTransactionBank>("ReceivableTransactionBanks"); }
        }

        //AuditTrail
        [CollectionOperationSet(AllowAdd = false, AllowRemove = true)]
        public XPCollection<AuditDataItemPersistent> ChangeHistory
        {
            get
            {
                if (changeHistory == null)
                {
                    changeHistory = AuditedObjectWeakReference.GetAuditTrail(Session, this);
                }
                return changeHistory;
            }
        }

        #endregion Field

        //================================================== Code Only ==================================================

        #region No

        public void UpdateNo()
        {
            try
            {
                if (!IsLoading && Session.IsNewObject(this))
                {
                    if (this.ReceivableTransaction != null)
                    {
                        object _makRecord = Session.Evaluate<ReceivableTransactionLine>(CriteriaOperator.Parse("Max(No)"), CriteriaOperator.Parse("ReceivableTransaction=?", this.ReceivableTransaction));
                        this.No = Convert.ToInt32(_makRecord) + 1;
                        this.Save();
                        RecoveryUpdateNo();
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = ReceivableTransactionLine " + ex.ToString());
            }
        }

        public void RecoveryUpdateNo()
        {
            try
            {
                if (this.ReceivableTransaction != null)
                {
                    ReceivableTransaction _numHeader = Session.FindObject<ReceivableTransaction>
                                            (new BinaryOperator("Code", this.ReceivableTransaction.Code));

                    XPCollection<ReceivableTransactionLine> _numLines = new XPCollection<ReceivableTransactionLine>
                                                             (Session, new BinaryOperator("ReceivableTransaction", _numHeader),
                                                              new SortProperty("No", DevExpress.Xpo.DB.SortingDirection.Ascending));
                    if (_numLines != null)
                    {
                        int i1 = 0;
                        foreach (ReceivableTransactionLine _numLine in _numLines)
                        {
                            i1 += 1;
                            _numLine.No = i1;
                            _numLine.Save();
                        }
                        i1 = 1;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = ReceivableTransactionLine " + ex.ToString());
            }
        }

        public void RecoveryDeleteNo()
        {
            try
            {
                if (this.ReceivableTransaction != null)
                {
                    ReceivableTransaction _numHeader = Session.FindObject<ReceivableTransaction>
                                            (new BinaryOperator("Code", this.ReceivableTransaction.Code));

                    XPCollection<ReceivableTransactionLine> _numLines = new XPCollection<ReceivableTransactionLine>
                                                             (Session, new GroupOperator(GroupOperatorType.And,
                                                              new BinaryOperator("This", this, BinaryOperatorType.NotEqual),
                                                              new BinaryOperator("ReceivableTransaction", _numHeader)),
                                                              new SortProperty("No", DevExpress.Xpo.DB.SortingDirection.Ascending));
                    if (_numLines != null)
                    {
                        int i = 0;
                        foreach (ReceivableTransactionLine _numLine in _numLines)
                        {
                            i += 1;
                            _numLine.No = i;
                            _numLine.Save();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = ReceivableTransactionLine " + ex.ToString());
            }
        }

        #endregion No

        private void SetVehicle(Employee _locPIC)
        {
            try
            {
                DateTime now = DateTime.Now;
                if (this.Company != null && this.Workplace != null)
                {
                    SalesmanVehicleSetup _locSalesmanVehicleSetup = Session.FindObject<SalesmanVehicleSetup>
                                                        (new GroupOperator(GroupOperatorType.And,
                                                        new BinaryOperator("Company", this.Company),
                                                        new BinaryOperator("Workplace", this.Workplace),
                                                        new BinaryOperator("Salesman", _locPIC),
                                                        new BinaryOperator("Active", true)));
                    if (_locSalesmanVehicleSetup != null)
                    {
                        if (_locSalesmanVehicleSetup.Vehicle != null)
                        {
                            this.Vehicle = _locSalesmanVehicleSetup.Vehicle;
                        }
                        else
                        {
                            this.Vehicle = null;
                        }
                    }
                    else
                    {
                        this.Vehicle = null;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Business Object = ReceivableTransactionLine ", ex.ToString());
            }
        }

        private void SetTotalPlan()
        {
            try
            {
                _globFunc = new GlobalFunction();

                if (_amount >= 0 || _amountCN >= 0 || _amountDN >= 0)
                {
                    if (_globFunc.GetRoundingList(Session, ObjectList.ReceivableTransactionLine, FieldName.TAmount) == true)
                    {
                        this.Plan = _globFunc.GetRoundUp(Session, ((this.Amount + _amountDN) - this.AmountCN), ObjectList.ReceivableTransactionLine, FieldName.TAmount);
                    }
                    else
                    {
                        this.Plan = (this.Amount + _amountDN) - this.AmountCN;
                    }
                }
                
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = ReceivableTransactionLine " + ex.ToString());
            }
        }

    }
}