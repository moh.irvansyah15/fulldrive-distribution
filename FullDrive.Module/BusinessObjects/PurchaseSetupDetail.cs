﻿using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;

using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.ConditionalAppearance;

namespace FullDrive.Module.BusinessObjects
{
    [DefaultClassOptions]
    [DefaultProperty("Code")]
    [NavigationItem("Setup")]
    [RuleCombinationOfPropertiesIsUnique("PurchaseSetupDetailRuleUnique", DefaultContexts.Save, "Code")]
    //[ImageName("BO_Contact")]
    //[DefaultProperty("DisplayMemberNameForLookupEditorsOfThisType")]
    //[DefaultListViewOptions(MasterDetailMode.ListViewOnly, false, NewItemRowPosition.None)]
    //[Persistent("DatabaseTableName")]
    // Specify more UI options using a declarative approach (https://documentation.devexpress.com/#eXpressAppFramework/CustomDocument112701).
    public class PurchaseSetupDetail : BaseObject
    { // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).
        private string _code;
        private Company _company;
        private XPCollection<Workplace> _availableWorkplace;
        private Workplace _workplace;
        private string _name;
        private UserAccess _userAccess;
        private ObjectList _objectList;
        private bool _businessPartnerActive;
        private BusinessPartner _businessPartner;
        private bool _splitInvoice;
        private bool _allSplitInvoice;
        private bool _activationQuantity;
        private bool _active;
        private ApplicationSetup _applicationSetup;
        private GlobalFunction _globFunc;
        //AuditTrail
        private XPCollection<AuditDataItemPersistent> changeHistory;

        public PurchaseSetupDetail(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if (!IsLoading)
            {
                _globFunc = new GlobalFunction();
                this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.PurchaseSetupDetail);
            }
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).
        }

        protected override void OnSaving()
        {
            base.OnSaving();
            if (this.AllSplitInvoice == true)
            {
                CheckAllSplitInvoiceSystem();
            }
            if (this.SplitInvoice == true)
            {
                CheckSplitInvoiceSystem();
            }
        }

        #region Field

        [RuleRequiredField(DefaultContexts.Save)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue("Code", ref _code, value); }
        }

        [ImmediatePostData()]
        [DataSourceCriteria("Active = true")]
        public Company Company
        {
            get { return _company; }
            set { SetPropertyValue("Company", ref _company, value); }
        }

        [Browsable(false)]
        public XPCollection<Workplace> AvailableWorkplace
        {
            get
            {
                if (this.Company != null)
                {
                    _availableWorkplace = new XPCollection<Workplace>(Session,
                                        new GroupOperator(GroupOperatorType.And,
                                        new BinaryOperator("Company", this.Company),
                                        new BinaryOperator("Active", true)));
                }
                else
                {
                    _availableWorkplace = new XPCollection<Workplace>(Session,
                                        new GroupOperator(GroupOperatorType.And,
                                        new BinaryOperator("Active", true)));
                }

                return _availableWorkplace;

            }
        }

        [DataSourceProperty("AvailableWorkplace")]
        public Workplace Workplace
        {
            get { return _workplace; }
            set { SetPropertyValue("Workplace", ref _workplace, value); }
        }

        public string Name
        {
            get { return _name; }
            set { SetPropertyValue("Name", ref _name, value); }
        }

        public UserAccess UserAccess
        {
            get { return _userAccess; }
            set { SetPropertyValue("UserAccess", ref _userAccess, value); }
        }

        public ObjectList ObjectList
        {
            get { return _objectList; }
            set { SetPropertyValue("ObjectList", ref _objectList, value); }
        }

        [ImmediatePostData()]
        [Appearance("PurchaseSetupDetailBusinessPartnerActiveClose", Criteria = "AllSplitInvoice = true", Enabled = false)]
        public bool BusinessPartnerActive
        {
            get { return _businessPartnerActive; }
            set { SetPropertyValue("BusinessPartnerActive", ref _businessPartnerActive, value); }
        }

        [Appearance("PurchaseSetupDetailBusinessPartnerClose", Criteria = "BusinessPartnerActive = false", Enabled = false)]
        public BusinessPartner BusinessPartner
        {
            get { return _businessPartner; }
            set { SetPropertyValue("BusinessPartner", ref _businessPartner, value); }
        }

        [Appearance("PurchaseSetupDetailSplitInvoiceClose", Criteria = "AllSplitInvoice = true", Enabled = false)]
        public bool SplitInvoice
        {
            get { return _splitInvoice; }
            set { SetPropertyValue("SplitInvoice", ref _splitInvoice, value); }
        }

        [ImmediatePostData()]
        public bool AllSplitInvoice
        {
            get { return _allSplitInvoice; }
            set
            {
                SetPropertyValue("AllSplitInvoice", ref _allSplitInvoice, value);
                if (!IsLoading)
                {
                    if (this._allSplitInvoice == true)
                    {
                        this.SplitInvoice = false;
                        this.BusinessPartnerActive = false;
                        this.BusinessPartner = null;
                    }
                }
            }
        }


        public bool ActivationQuantity
        {
            get { return _activationQuantity; }
            set { SetPropertyValue("ActivationQuantity", ref _activationQuantity, value); }
        }

        public bool Active
        {
            get { return _active; }
            set { SetPropertyValue("Active", ref _active, value); }
        }

        [Appearance("ApplicationSetupApplicationSetupDetailEnabled", Enabled = false)]
        [RuleRequiredField(DefaultContexts.Save)]
        [Association("ApplicationSetup-PurchaseSetupDetails")]
        public ApplicationSetup ApplicationSetup
        {
            get { return _applicationSetup; }
            set { SetPropertyValue("ApplicationSetup", ref _applicationSetup, value); }
        }

        //AuditTrail
        [CollectionOperationSet(AllowAdd = false, AllowRemove = true)]
        public XPCollection<AuditDataItemPersistent> ChangeHistory
        {
            get
            {
                if (changeHistory == null)
                {
                    changeHistory = AuditedObjectWeakReference.GetAuditTrail(Session, this);
                }
                return changeHistory;
            }
        }

        #endregion Field

        //private string _PersistentProperty;
        //[XafDisplayName("My display name"), ToolTip("My hint message")]
        //[ModelDefault("EditMask", "(000)-00"), Index(0), VisibleInListView(false)]
        //[Persistent("DatabaseColumnName"), RuleRequiredField(DefaultContexts.Save)]
        //public string PersistentProperty {
        //    get { return _PersistentProperty; }
        //    set { SetPropertyValue("PersistentProperty", ref _PersistentProperty, value); }
        //}

        //[Action(Caption = "My UI Action", ConfirmationMessage = "Are you sure?", ImageName = "Attention", AutoCommit = true)]
        //public void ActionMethod() {
        //    // Trigger a custom business logic for the current record in the UI (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112619.aspx).
        //    this.PersistentProperty = "Paid";
        //}

        //=============================================== Code In Here ===============================================

        private void CheckAllSplitInvoiceSystem()
        {
            try
            {
                XPCollection<PurchaseSetupDetail> _appSetups = new XPCollection<PurchaseSetupDetail>(Session, new GroupOperator(GroupOperatorType.And,
                                                               new BinaryOperator("ApplicationSetup", this.ApplicationSetup),
                                                               new BinaryOperator("This", this, BinaryOperatorType.NotEqual)));
                if (_appSetups == null)
                {
                    return;
                }
                else
                {
                    foreach (PurchaseSetupDetail _appSetup in _appSetups)
                    {
                        _appSetup.AllSplitInvoice = false;
                        _appSetup.Active = false;
                        _appSetup.BusinessPartnerActive = false;
                        _appSetup.BusinessPartner = null;
                        _appSetup.SplitInvoice = false;
                        _appSetup.Save();
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Business Object = PurchaseSetupDetail", ex.ToString());
            }
        }

        private void CheckSplitInvoiceSystem()
        {
            XPCollection<PurchaseSetupDetail> _appSetups = new XPCollection<PurchaseSetupDetail>(Session, new GroupOperator(GroupOperatorType.And,
                                                               new BinaryOperator("ApplicationSetup", this.ApplicationSetup),
                                                               new BinaryOperator("AllSplitGoodsReceive", true)));
            if (_appSetups == null)
            {
                return;
            }
            else
            {
                this.Active = false;
                this.BusinessPartnerActive = false;
                this.BusinessPartner = null;
                this.SplitInvoice = false;
                this.Save();
            }
        }

    }
}