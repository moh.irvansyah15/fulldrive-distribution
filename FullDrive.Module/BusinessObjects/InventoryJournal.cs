﻿using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;

using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.ConditionalAppearance;

namespace FullDrive.Module.BusinessObjects
{
    [DefaultClassOptions]
    [DefaultProperty("Code")]
    [NavigationItem("Inventory")]
    [RuleCombinationOfPropertiesIsUnique("InventoryJournalRuleUnique", DefaultContexts.Save, "Code")]
    //[ImageName("BO_Contact")]
    //[DefaultProperty("DisplayMemberNameForLookupEditorsOfThisType")]
    //[DefaultListViewOptions(MasterDetailMode.ListViewOnly, false, NewItemRowPosition.None)]
    //[Persistent("DatabaseTableName")]
    // Specify more UI options using a declarative approach (https://documentation.devexpress.com/#eXpressAppFramework/CustomDocument112701).
    public class InventoryJournal : BaseObject
    { // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).
        private string _code;
        private Company _company;
        private Workplace _workplace;
        private DocumentType _documentType;
        private string _docNo;
        private Employee _salesman;
        private Location _location;
        private BinLocation _binLocation;
        private LocationType _locationType;
        private StockType _stockType;
        private StockGroup _stockGroup;
        private Item _item;
        private double _qtyPos;
        private double _qtyNeg;
        private UnitOfMeasure _dUom;
        private DateTime _journalDate;
        private string _lotNumber;
        private InventoryTransferOut _inventoryTransferOut;
        private InventoryTransferIn _inventoryTransferIn;
        private TransferOut _transferOut;
        private TransferIn _transferIn;
        private ItemReclassJournal _itemReclassJournal;
        //private CuttingProcessLot _cuttingProcessLot;
        private InvoiceCanvassing _invoiceCanvassing;
        private DeliveryOrder _deliveryOrder;
        private Transfer _transfer;
        private TransferOrder _transferOrder;
        private string _userAccess;
        private GlobalFunction _globFunc;
        //AuditTrail
        private XPCollection<AuditDataItemPersistent> changeHistory;

        public InventoryJournal(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if (!IsLoading)
            {
                _globFunc = new GlobalFunction();
                #region UserAccess
                _userAccess = SecuritySystem.CurrentUserName;
                UserAccess _locUserAccess = Session.FindObject<UserAccess>(new BinaryOperator("UserName", _userAccess));
                if (_locUserAccess != null)
                {
                    if (_locUserAccess.Employee != null)
                    {
                        if (_locUserAccess.Employee.Company != null && _locUserAccess.Employee.Workplace != null)
                        {
                            this.Code = _globFunc.GetNumberingUnlockOptimisticRecordBasedOrganization(this.Session.DataLayer, ObjectList.InventoryJournal, _locUserAccess.Employee.Company, _locUserAccess.Employee.Workplace);
                        }
                        else
                        {
                            this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.InventoryJournal);
                        }
                        if (_locUserAccess.Employee.Company != null)
                        {
                            Company = _locUserAccess.Employee.Company;
                        }
                        if (_locUserAccess.Employee.Workplace != null)
                        {
                            this.Workplace = _locUserAccess.Employee.Workplace;
                        }
                        else
                        {
                            this.Workplace = _globFunc.GetDefaultWorkplace(this.Session, _locUserAccess);
                        }
                    }
                }
                #endregion UserAccess 
            }
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).
        }

        #region Field

        [Appearance("InventoryJournalCodeEnabled", Enabled = false)]
        [RuleRequiredField(DefaultContexts.Save)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue("Code", ref _code, value); }
        }

        [DataSourceCriteria("Active = true")]
        public Company Company
        {
            get { return _company; }
            set { SetPropertyValue("Company", ref _company, value); }
        }

        public Workplace Workplace
        {
            get { return _workplace; }
            set { SetPropertyValue("Workplace", ref _workplace, value); }
        }

        public DocumentType DocumentType
        {
            get { return _documentType; }
            set { SetPropertyValue("DocumentType", ref _documentType, value); }
        }

        public string DocNo
        {
            get { return _docNo; }
            set { SetPropertyValue("DocNo", ref _docNo, value); }
        }

        public Employee Salesman
        {
            get { return _salesman; }
            set { SetPropertyValue("Salesman", ref _salesman, value); }
        }

        [DataSourceCriteria("Active = true")]
        public Location Location
        {
            get { return _location; }
            set { SetPropertyValue("Location", ref _location, value); }
        }

        [DataSourceCriteria("Active = true")]
        public BinLocation BinLocation
        {
            get { return _binLocation; }
            set { SetPropertyValue("binLocation", ref _binLocation, value); }
        }

        public LocationType LocationType
        {
            get { return _locationType; }
            set { SetPropertyValue("LocationType", ref _locationType, value); }
        }

        public StockType StockType
        {
            get { return _stockType; }
            set { SetPropertyValue("StockType", ref _stockType, value); }
        }

        public StockGroup StockGroup
        {
            get { return _stockGroup; }
            set { SetPropertyValue("StockGroup", ref _stockGroup, value); }
        }

        [DataSourceCriteria("Active = true")]
        public Item Item
        {
            get { return _item; }
            set { SetPropertyValue("Item", ref _item, value); }
        }

        public double QtyPos
        {
            get { return _qtyPos; }
            set { SetPropertyValue("QtyPos", ref _qtyPos, value); }
        }

        public double QtyNeg
        {
            get { return _qtyNeg; }
            set { SetPropertyValue("QtyNeg", ref _qtyNeg, value); }
        }

        [DataSourceCriteria("Active = true")]
        public UnitOfMeasure DUOM
        {
            get { return _dUom; }
            set { SetPropertyValue("DUOM", ref _dUom, value); }
        }

        [ModelDefault("DisplayFormat", "{0:dd/MM/yyyy}")]
        [ModelDefault("EditMask", "dd/MM/yyyy")]
        public DateTime JournalDate
        {
            get { return _journalDate; }
            set { SetPropertyValue("JournalDate", ref _journalDate, value); }
        }

        public string LotNumber
        {
            get { return _lotNumber; }
            set { SetPropertyValue("LotNumber", ref _lotNumber, value); }
        }

        public InventoryTransferOut InventoryTransferOut
        {
            get { return _inventoryTransferOut; }
            set { SetPropertyValue("InventoryTransferOut", ref _inventoryTransferOut, value); }
        }

        public InventoryTransferIn InventoryTransferIn
        {
            get { return _inventoryTransferIn; }
            set { SetPropertyValue("InventoryTransferIn", ref _inventoryTransferIn, value); }
        }

        public TransferOut TransferOut
        {
            get { return _transferOut; }
            set { SetPropertyValue("TransferOut", ref _transferOut, value); }
        }

        public TransferIn TransferIn
        {
            get { return _transferIn; }
            set { SetPropertyValue("TransferIn", ref _transferIn, value); }
        }

        public ItemReclassJournal ItemReclassJournal
        {
            get { return _itemReclassJournal; }
            set { SetPropertyValue("ItemReclassJournal", ref _itemReclassJournal, value); }
        }

        public InvoiceCanvassing InvoiceCanvassing
        {
            get { return _invoiceCanvassing; }
            set { SetPropertyValue("InvoiceCanvassing", ref _invoiceCanvassing, value); }
        }

        public DeliveryOrder DeliveryOrder
        {
            get { return _deliveryOrder; }
            set { SetPropertyValue("DeliveryOrder", ref _deliveryOrder, value); }
        }

        public Transfer Transfer
        {
            get { return _transfer; }
            set { SetPropertyValue("Transfer", ref _transfer, value); }
        }

        public TransferOrder TransferOrder
        {
            get { return _transferOrder; }
            set { SetPropertyValue("TransferOrder", ref _transferOrder, value); }
        }

        //AuditTrail
        [CollectionOperationSet(AllowAdd = false, AllowRemove = true)]
        public XPCollection<AuditDataItemPersistent> ChangeHistory
        {
            get
            {
                if (changeHistory == null)
                {
                    changeHistory = AuditedObjectWeakReference.GetAuditTrail(Session, this);
                }
                return changeHistory;
            }
        }

        #endregion Field

    }
}