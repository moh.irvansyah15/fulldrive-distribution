﻿using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;

using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.ConditionalAppearance;

namespace FullDrive.Module.BusinessObjects
{
    [DefaultClassOptions]
    [DefaultProperty("Code")]
    [NavigationItem("Finance & Accounting")]
    [RuleCombinationOfPropertiesIsUnique("GeneralJournalRuleUnique", DefaultContexts.Save, "Code")]
    //[ImageName("BO_Contact")]
    //[DefaultProperty("DisplayMemberNameForLookupEditorsOfThisType")]
    //[DefaultListViewOptions(MasterDetailMode.ListViewOnly, false, NewItemRowPosition.None)]
    //[Persistent("DatabaseTableName")]
    // Specify more UI options using a declarative approach (https://documentation.devexpress.com/#eXpressAppFramework/CustomDocument112701).
    public class GeneralJournal : BaseObject
    { // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).
        private string _code;
        private Company _company;
        private Workplace _workplace;
        private DateTime _postingDate;
        private PostingType _postingType;
        private OrderType _orderType;
        private PostingMethod _postingMethod;
        private PostingMethodType _postingMethodType;
        private ChartOfAccount _account;
        private string _description;
        private double _debit;
        private double _credit;
        private int _journalMonth;
        private int _journalYear;
        private PurchaseOrder _purchaseOrder;
        private PurchaseOrderLine _purchaseOrderLine;
        private InventoryTransferIn _inventoryTransferIn;
        private InventoryTransferInLine _inventoryTransferInLine;
        private PurchaseInvoice _purchaseInvoice;
        private PurchasePrePaymentInvoice _purchasePrePaymentInvoice;
        private PayableTransaction _payableTransaction;
        private DeliveryOrder _deliveryOrder;
        private AmendDelivery _amendDelivery;
        private OrderCollection _orderCollection;
        private ReceivableTransaction _receivableTransaction;
        private ReceivableTransaction2 _receivableTransaction2;
        private CustomerInvoice _customerInvoice;
        private InventoryTransferOut _inventoryTransferOut;
        private AccountReclassJournal _accountReclassJournal;
        private CashAdvance _cashAdvance;
        private PaymentRealization _paymentRealization;
        private string _userAccess;     
        private GlobalFunction _globFunc;
        //AuditTrail
        private XPCollection<AuditDataItemPersistent> changeHistory;


        public GeneralJournal(Session session)
            : base(session)
        {
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if (!IsLoading)
            {
                _globFunc = new GlobalFunction();
                #region UserAccess
                _userAccess = SecuritySystem.CurrentUserName;
                UserAccess _locUserAccess = Session.FindObject<UserAccess>(new BinaryOperator("UserName", _userAccess));
                if (Session.IsNewObject(this))
                {
                    if (_locUserAccess != null)
                    {
                        if (_locUserAccess.Employee != null)
                        {
                            if (_locUserAccess.Employee.Company != null && _locUserAccess.Employee.Workplace != null)
                            {
                                this.Code = _globFunc.GetNumberingUnlockOptimisticRecordBasedOrganization(this.Session.DataLayer, ObjectList.GeneralJournal, _locUserAccess.Employee.Company, _locUserAccess.Employee.Workplace);
                            }
                            else
                            {
                                this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.GeneralJournal);
                            }
                            if (_locUserAccess.Employee.Company != null)
                            {
                                Company = _locUserAccess.Employee.Company;
                            }
                            if (_locUserAccess.Employee.Workplace != null)
                            {
                                this.Workplace = _locUserAccess.Employee.Workplace;
                            }
                            else
                            {
                                this.Workplace = _globFunc.GetDefaultWorkplace(this.Session, _locUserAccess);
                            }
                        }
                    }
                }
                #endregion UserAccess
            }
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).
        }

        #region Field

        [RuleRequiredField(DefaultContexts.Save)]
        [Appearance("GeneralJournalCodeClose", Enabled = false)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue("Code", ref _code, value); }
        }

        [DataSourceCriteria("Active = true")]
        [Appearance("GeneralJournalCompanyClose", Enabled = false)]
        public Company Company
        {
            get { return _company; }
            set { SetPropertyValue("Company", ref _company, value); }
        }

        [Appearance("GeneralJournalWorkplaceClose", Enabled = false)]
        public Workplace Workplace
        {
            get { return _workplace; }
            set { SetPropertyValue("Workplace", ref _workplace, value); }
        }

        [Appearance("GeneralJournalPostingDateClose", Enabled = false)]
        [ModelDefault("DisplayFormat", "{0:dd/MM/yyyy}")]
        [ModelDefault("EditMask", "dd/MM/yyyy")]
        public DateTime PostingDate
        {
            get { return _postingDate; }
            set { SetPropertyValue("PostingDate", ref _postingDate, value); }
        }

        [Appearance("GeneralJournalPostingTypeClose", Enabled = false)]
        public PostingType PostingType
        {
            get { return _postingType; }
            set { SetPropertyValue("PostingType", ref _postingType, value); }
        }

        [Appearance("GeneralJournalOrderTypeClose", Enabled = false)]
        public OrderType OrderType
        {
            get { return _orderType; }
            set { SetPropertyValue("OrderType", ref _orderType, value); }
        }

        [Appearance("GeneralJournalPostingMethodClose", Enabled = false)]
        public PostingMethod PostingMethod
        {
            get { return _postingMethod; }
            set { SetPropertyValue("PostingMethod", ref _postingMethod, value); }
        }

        [Appearance("GeneralJournalPostingMethodTypeClose", Enabled = false)]
        public PostingMethodType PostingMethodType
        {
            get { return _postingMethodType; }
            set { SetPropertyValue("PostingMethodType", ref _postingMethodType, value); }
        }

        [Appearance("GeneralJournalAccountClose", Enabled = false)]
        public ChartOfAccount Account
        {
            get { return _account; }
            set { SetPropertyValue("Account", ref _account, value); }
        }

        [Appearance("GeneralJournalDisriptionClose", Enabled = false)]
        [Size(512)]
        public string Description
        {
            get { return _description; }
            set { SetPropertyValue("Description", ref _description, value); }
        }

        [Appearance("GeneralJournalDebitClose", Enabled = false)]
        public double Debit
        {
            get { return _debit; }
            set { SetPropertyValue("Debit", ref _debit, value); }
        }

        [Appearance("GeneralJournalCreditClose", Enabled = false)]
        public double Credit
        {
            get { return _credit; }
            set { SetPropertyValue("Credit", ref _credit, value); }
        }

        [Appearance("GeneralJournalJournalMonthClose", Enabled = false)]
        public int JournalMonth
        {
            get { return _journalMonth; }
            set { SetPropertyValue("JournalMonth", ref _journalMonth, value); }
        }

        [Appearance("GeneralJournalJournalYearClose", Enabled = false)]
        public int JournalYear
        {
            get { return _journalYear; }
            set { SetPropertyValue("JournalYear", ref _journalYear, value); }
        }

        [Appearance("GeneralJournalPurchaseOrderClose", Enabled = false)]
        public PurchaseOrder PurchaseOrder
        {
            get { return _purchaseOrder; }
            set { SetPropertyValue("PurchaseOrder", ref _purchaseOrder, value); }
        }

        [Appearance("GeneralJournalPurchaseOrderLineClose", Enabled = false)]
        public PurchaseOrderLine PurchaseOrderLine
        {
            get { return _purchaseOrderLine; }
            set { SetPropertyValue("PurchaseOrderLine", ref _purchaseOrderLine, value); }
        }

        [Appearance("GeneralJournalInventoryTransferInClose", Enabled = false)]
        public InventoryTransferIn InventoryTransferIn
        {
            get { return _inventoryTransferIn; }
            set { SetPropertyValue("InventoryTransferIn", ref _inventoryTransferIn, value); }
        }

        [Appearance("GeneralJournalInventoryTransferInLineClose", Enabled = false)]
        public InventoryTransferInLine InventoryTransferInLine
        {
            get { return _inventoryTransferInLine; }
            set { SetPropertyValue("InventoryTransferInLine", ref _inventoryTransferInLine, value); }
        }

        [Appearance("GeneralJournalPurchaseInvoiceClose", Enabled = false)]
        public PurchaseInvoice PurchaseInvoice
        {
            get { return _purchaseInvoice; }
            set { SetPropertyValue("PurchaseInvoice", ref _purchaseInvoice, value); }
        }

        [Appearance("GeneralJournalPurchasePrePaymentInvoiceClose", Enabled = false)]
        public PurchasePrePaymentInvoice PurchasePrePaymentInvoice
        {
            get { return _purchasePrePaymentInvoice; }
            set { SetPropertyValue("PurchasePrePaymentInvoice", ref _purchasePrePaymentInvoice, value); }
        }

        [Appearance("GeneralJournalPayableTransactionClose", Enabled = false)]
        public PayableTransaction PayableTransaction
        {
            get { return _payableTransaction; }
            set { SetPropertyValue("PayableTransaction", ref _payableTransaction, value); }
        }

        [Appearance("GeneralJournalDeliveryOrderClose", Enabled = false)]
        public DeliveryOrder DeliveryOrder
        {
            get { return _deliveryOrder; }
            set { SetPropertyValue("DeliveryOrder", ref _deliveryOrder, value); }
        }

        [Appearance("GeneralJournalAmendDeliveryClose", Enabled = false)]
        public AmendDelivery AmendDelivery
        {
            get { return _amendDelivery; }
            set { SetPropertyValue("AmendDelivery", ref _amendDelivery, value); }
        }

        [Appearance("GeneralJournalOrderCollectionClose", Enabled = false)]
        public OrderCollection OrderCollection
        {
            get { return _orderCollection; }
            set { SetPropertyValue("OrderCollection", ref _orderCollection, value); }
        }

        [Appearance("GeneralJournalReceivableTransactionClose", Enabled = false)]
        public ReceivableTransaction ReceivableTransaction
        {
            get { return _receivableTransaction; }
            set { SetPropertyValue("ReceivableTransaction", ref _receivableTransaction, value); }
        }

        [Appearance("GeneralJournalReceivableTransaction2Close", Enabled = false)]
        public ReceivableTransaction2 ReceivableTransaction2
        {
            get { return _receivableTransaction2; }
            set { SetPropertyValue("ReceivableTransaction2", ref _receivableTransaction2, value); }
        }

        [Appearance("GeneralJournalCustomerInvoiceClose", Enabled = false)]
        public CustomerInvoice CustomerInvoice
        {
            get { return _customerInvoice; }
            set { SetPropertyValue("CustomerInvoice", ref _customerInvoice, value); }
        }

        [Appearance("GeneralJournalInventoryTransferOutClose", Enabled = false)]
        public InventoryTransferOut InventoryTransferOut
        {
            get { return _inventoryTransferOut; }
            set { SetPropertyValue("InventoryTransferOut", ref _inventoryTransferOut, value); }
        }

        [Appearance("GeneralJournalAccountReclassJournalClose", Enabled = false)]
        public AccountReclassJournal AccountReclassJournal
        {
            get { return _accountReclassJournal; }
            set { SetPropertyValue("AccountReclassJournal", ref _accountReclassJournal, value); }
        }

        [Appearance("GeneralJournalCashAdvanceClose", Enabled = false)]
        public CashAdvance CashAdvance
        {
            get { return _cashAdvance; }
            set { SetPropertyValue("CashAdvance", ref _cashAdvance, value); }
        }

        [Appearance("GeneralJournalPaymentRealizationClose", Enabled = false)]
        public PaymentRealization PaymentRealization
        {
            get { return _paymentRealization; }
            set { SetPropertyValue("PaymentRealization", ref _paymentRealization, value); }
        }

        //AuditTrail
        [CollectionOperationSet(AllowAdd = false, AllowRemove = true)]
        public XPCollection<AuditDataItemPersistent> ChangeHistory
        {
            get
            {
                if (changeHistory == null)
                {
                    changeHistory = AuditedObjectWeakReference.GetAuditTrail(Session, this);
                }
                return changeHistory;
            }
        }

        #endregion Field

    }
}