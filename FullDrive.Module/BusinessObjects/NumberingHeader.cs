﻿using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using FullDrive.Module.CustomProcess;

namespace FullDrive.Module.BusinessObjects
{
    [DefaultClassOptions]
    [DefaultProperty("Name")]
    [NavigationItem("Setup")]
    //[ImageName("BO_Contact")]
    //[DefaultProperty("DisplayMemberNameForLookupEditorsOfThisType")]
    //[DefaultListViewOptions(MasterDetailMode.ListViewOnly, false, NewItemRowPosition.None)]
    //[Persistent("DatabaseTableName")]
    // Specify more UI options using a declarative approach (https://documentation.devexpress.com/#eXpressAppFramework/CustomDocument112701).
    public class NumberingHeader : BaseObject
    { // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).
        private string _code;
        private Company _company;
        private XPCollection<Workplace> _availableWorkplace;
        private Workplace _workplace;
        private string _name;
        private NumberingType _numberingType;
        private Nullable<int> _totalLine = null;
        private ApplicationSetup _applicationSetup;
        private bool _active;
        private string _localUserAccess;
        private GlobalFunction _globFunc;
        //AuditTrail
        private XPCollection<AuditDataItemPersistent> changeHistory;

        public NumberingHeader(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).
            if (!IsLoading)
            {
                if (Session.IsNewObject(this))
                {

                    _globFunc = new GlobalFunction();
                    this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.NumberingHeader);
                    #region UserAccess
                    _localUserAccess = SecuritySystem.CurrentUserName;
                    UserAccess _locUserAccess = Session.FindObject<UserAccess>(new BinaryOperator("UserName", _localUserAccess));
                    if (_locUserAccess != null)
                    {
                        if (_locUserAccess.Employee != null)
                        {
                            if (_locUserAccess.Employee.Company != null)
                            {
                                this.Company = _locUserAccess.Employee.Company;
                            }
                            if (_locUserAccess.Employee.Workplace != null)
                            {
                                this.Workplace = _locUserAccess.Employee.Workplace;
                            }
                        }
                    }
                    #endregion UserAccess
                    
                }
            }
        }

        protected override void OnSaving()
        {
            base.OnSaving();
            if (this.Active == true)
            {
                CheckActived();
            }
        }

        #region Field

        public string Code
        {
            get { return _code; }
            set { SetPropertyValue("Code", ref _code, value); }
        }

        [ImmediatePostData()]
        [DataSourceCriteria("Active = true ")]
        public Company Company
        {
            get { return _company; }
            set { SetPropertyValue("Company", ref _company, value); }
        }

        [Browsable(false)]
        public XPCollection<Workplace> AvailableWorkplace
        {
            get
            {
                if (this.Company != null)
                {
                    _availableWorkplace = new XPCollection<Workplace>(Session,
                                        new GroupOperator(GroupOperatorType.And,
                                        new BinaryOperator("Company", this.Company),
                                        new BinaryOperator("Active", true)));
                }
                else
                {
                    _availableWorkplace = new XPCollection<Workplace>(Session,
                                        new GroupOperator(GroupOperatorType.And,
                                        new BinaryOperator("Active", true)));
                }

                return _availableWorkplace;

            }
        }

        [DataSourceProperty("AvailableWorkplace")]
        public Workplace Workplace
        {
            get { return _workplace; }
            set { SetPropertyValue("Workplace", ref _workplace, value); }
        }

        public string Name
        {
            get { return _name; }
            set { SetPropertyValue("Name", ref _name, value); }
        }

        public NumberingType NumberingType
        {
            get { return _numberingType; }
            set { SetPropertyValue("NumberingType", ref _numberingType, value); }
        }

        [Persistent("TotalLine")]
        public Nullable<int> TotalLine
        {
            get
            {
                return _totalLine;
            }
        }

        [Association("ApplicationSetup-NumberingHeaders")]
        public ApplicationSetup ApplicationSetup
        {
            get { return _applicationSetup; }
            set { SetPropertyValue("ApplicationSetup", ref _applicationSetup, value); }
        }

        public bool Active
        {
            get { return _active; }
            set { SetPropertyValue("Active", ref _active, value); }
        }

        [Association("NumberingHeader-NumberingLines")]
        public XPCollection<NumberingLine> NumberingLines
        {
            get { return GetCollection<NumberingLine>("NumberingLines"); }
        }

        //AuditTrail
        [CollectionOperationSet(AllowAdd = false, AllowRemove = true)]
        public XPCollection<AuditDataItemPersistent> ChangeHistory
        {
            get
            {
                if (changeHistory == null)
                {
                    changeHistory = AuditedObjectWeakReference.GetAuditTrail(Session, this);
                }
                return changeHistory;
            }
        }

        #endregion Field

        //=============================================== Code In Here ===============================================

        public void UpdateTotalLine(bool forceChangeEvents)
        {
            try
            {
                Nullable<int> oldTotalLine = _totalLine;
                _totalLine = Convert.ToInt32(Session.Evaluate<NumberingHeader>(CriteriaOperator.Parse("NumberingLines.Count"), CriteriaOperator.Parse("Oid=?", Oid)));
                if (forceChangeEvents)
                {
                    OnChanged("TotalLine", oldTotalLine, _totalLine);
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = NumberingHeader ", ex.ToString());
            }
        }

        private void Reset()
        {
            _totalLine = null;
        }

        private void CheckActived()
        {
            try
            {
                XPCollection<NumberingHeader> _numHeaders = new XPCollection<NumberingHeader>(Session,
                                                            new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("Active", true, BinaryOperatorType.Equal),
                                                            new BinaryOperator("This", this, BinaryOperatorType.NotEqual),
                                                            new BinaryOperator("Type", this.NumberingType, BinaryOperatorType.Equal)
                                                            ));
                if (_numHeaders == null)
                {
                    return;
                }
                else
                {
                    foreach (NumberingHeader _numHeader in _numHeaders)
                    {
                        _numHeader.Active = false;
                        _numHeader.Save();
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = NumberingHeader ", ex.ToString());
            }
        }

    }
}