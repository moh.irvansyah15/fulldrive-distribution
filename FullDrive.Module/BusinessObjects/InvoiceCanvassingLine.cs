﻿using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;

using DevExpress.ExpressApp.Editors;
using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.ConditionalAppearance;

namespace FullDrive.Module.BusinessObjects
{
    [DefaultClassOptions]
    [DefaultProperty("Code")]
    [NavigationItem("Sales")]
    [RuleCombinationOfPropertiesIsUnique("InvoiceCanvassingLineRuleUnique", DefaultContexts.Save, "Code")]
    //[ImageName("BO_Contact")]
    //[DefaultProperty("DisplayMemberNameForLookupEditorsOfThisType")]
    //[DefaultListViewOptions(MasterDetailMode.ListViewOnly, false, NewItemRowPosition.None)]
    //[Persistent("DatabaseTableName")]
    // Specify more UI options using a declarative approach (https://documentation.devexpress.com/#eXpressAppFramework/CustomDocument112701).
    public class InvoiceCanvassingLine : BaseObject
    { // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).

        private bool _activationPosting;
        private bool _freeItemChecked;
        private bool _discountChecked;
        private int _no;
        private string _code;
        #region InitialOrganization
        private Company _company;
        private Workplace _workplace;
        private Division _division;
        private Department _department;
        private Section _section;
        private Employee _employee;
        #endregion InitialOrganization
        private Employee _salesman;
        private Vehicle _vehicle;
        private BeginningInventory _beginningInventory;
        private bool _mobile;
        private BeginningInventoryLine _selectItem;
        private XPCollection<Item> _availableItem;
        private Item _item;
        private string _description;
        #region InisialisasiDefaultQty
        private double _dQty;
        private UnitOfMeasure _dUom;
        private double _qty;
        private XPCollection<UnitOfMeasure> _availableUnitOfMeasure;
        private UnitOfMeasure _uom;
        private double _tQty;
        private StockType _stockType;
        #endregion InisialisasiDefaultQty
        #region InisialisasiAmount
        private Currency _currency;
        private PriceGroup _priceGroup;
        private PriceLine _priceLine;
        private double _uAmount;
        private double _tUAmount;
        private DiscountRule _discountRule;
        private double _discAmount;
        private Tax _tax;
        private double _txValue;
        private double _txAmount;
        private double _tAmount;
        #endregion InisialisasiAmount
        #region InisialisasiRemainQty
        private double _rmDQty;
        private double _rmQty;
        private double _rmTQty;
        #endregion InisialisasiRemainQty
        #region InitialPostingQuantityTo
        private double _pDQty;
        private UnitOfMeasure _pDUom;
        private double _pQty;
        private UnitOfMeasure _pUom;
        private double _pTQty;
        #endregion InitialPostingQuantityTo
        private Status _status;
        private DateTime _statusDate;
        private int _postedCount;
        private InvoiceCanvassing _invoiceCanvassing;
        private string _userAccessIn;
        private GlobalFunction _globFunc;
        //AuditTrail
        private XPCollection<AuditDataItemPersistent> changeHistory;

        public InvoiceCanvassingLine(Session session)
            : base(session)
        {
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if (!IsLoading)
            {
                if (Session.IsNewObject(this))
                {
                    _globFunc = new GlobalFunction();
                    DateTime now = DateTime.Now;
                    #region UserAccess
                    _userAccessIn = SecuritySystem.CurrentUserName;
                    UserAccess _locUserAccess = Session.FindObject<UserAccess>(new BinaryOperator("UserName", _userAccessIn));
                    if (_locUserAccess != null)
                    {
                        if (_locUserAccess.Employee != null)
                        {
                            if (_locUserAccess.Mobile == true)
                            {
                                this.Mobile = _locUserAccess.Mobile;
                            }
                            else
                            {
                                if (_locUserAccess.Employee.Company != null && _locUserAccess.Employee.Workplace != null)
                                {
                                    this.Code = _globFunc.GetNumberingUnlockOptimisticRecordBasedOrganization(this.Session.DataLayer, ObjectList.InvoiceCanvassingLine, _locUserAccess.Employee.Company, _locUserAccess.Employee.Workplace);
                                }
                                else
                                {
                                    this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.InvoiceCanvassingLine);
                                }
                            }
                        }
                    }
                    #endregion UserAccess
                    this.Status = CustomProcess.Status.Open;
                    this.StatusDate = now;
                    this.DiscountChecked = false;
                    this.StockType = StockType.Good;
                }
                    
            }
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).
        }

        protected override void OnSaving()
        {
            base.OnSaving();
            if (!IsLoading )
            {
                if(IsSaving)
                {
                    UpdateNo();
                }
                if (this.Mobile == true)
                {
                    if (Session.IsNewObject(this))
                    {
                        if (!(Session is NestedUnitOfWork))
                        {
                            if (this.Code == null)
                            {
                                this.Code = "ICLNM - " + DistributedIdGeneratorHelper.Generate(this.Session.DataLayer, this.GetType().FullName, string.Empty);
                            }
                        }
                    }
                }
            }
        }
        protected override void OnDeleting()
        {
            base.OnDeleting();
            if (!IsLoading)
            {
                RecoveryDeleteNo();
            }
        }

        #region Field

        [Browsable(false)]
        public bool ActivationPosting
        {
            get { return _activationPosting; }
            set { SetPropertyValue("ActivationPosting", ref _activationPosting, value); }
        }

        [Browsable(false)]
        public bool FreeItemChecked
        {
            get { return _freeItemChecked; }
            set { SetPropertyValue("FreeItemChecked", ref _freeItemChecked, value); }
        }

        [Browsable(false)]
        public bool DiscountChecked
        {
            get { return _discountChecked; }
            set { SetPropertyValue("DiscountChecked", ref _discountChecked, value); }
        }

        [Appearance("InvoiceCanvassingLineNoEnabled", Enabled = false)]
        public int No
        {
            get { return _no; }
            set { SetPropertyValue("No", ref _no, value); }
        }

        //[RuleRequiredField(DefaultContexts.Save)]
        [Appearance("InvoiceCanvassingLineCodeClose", Enabled = false)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue("Code", ref _code, value); }
        }

        #region Organization

        [Browsable(false)]
        [ImmediatePostData()]
        [Appearance("InvoiceCanvassingLineCompanyClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Company Company
        {
            get { return _company; }
            set { SetPropertyValue("Company", ref _company, value); }
        }

        [Browsable(false)]
        [ImmediatePostData()]
        [Appearance("InvoiceCanvassingLineWorkplaceClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Workplace Workplace
        {
            get { return _workplace; }
            set { SetPropertyValue("Workplace", ref _workplace, value); }
        }

        [Browsable(false)]
        public Division Division
        {
            get { return _division; }
            set { SetPropertyValue("Division", ref _division, value); }
        }

        [Browsable(false)]
        public Department Department
        {
            get { return _department; }
            set { SetPropertyValue("Department", ref _department, value); }
        }

        [Browsable(false)]
        public Section Section
        {
            get { return _section; }
            set { SetPropertyValue("Section", ref _section, value); }
        }

        [Browsable(false)]
        public Employee Employee
        {
            get { return _employee; }
            set { SetPropertyValue("Employee", ref _employee, value); }
        }

        #endregion Organization
        [Browsable(false)]
        [ImmediatePostData()]
        [Appearance("InvoiceCanvassingLineSalesmanClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Employee Salesman
        {
            get { return _salesman; }
            set { SetPropertyValue("Salesman", ref _salesman, value); }
        }

        [Browsable(false)]
        [ImmediatePostData()]
        [Appearance("InvoiceCanvassingLineVehicleClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Vehicle Vehicle
        {
            get { return _vehicle; }
            set { SetPropertyValue("Vehicle", ref _vehicle, value); }
        }

        [Browsable(false)]
        [ImmediatePostData()]
        [Appearance("InvoiceCanvassingLineBeginningInventoryClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public BeginningInventory BeginningInventory
        {
            get { return _beginningInventory; }
            set { SetPropertyValue("BeginningInventory", ref _beginningInventory, value); }
        }

        [Browsable(false)]
        [ImmediatePostData()]
        public bool Mobile
        {
            get { return _mobile; }
            set { SetPropertyValue("Mobile", ref _mobile, value); }
        }

        [ImmediatePostData()]
        [DataSourceCriteria("Company = '@This.Company' AND Workplace = '@This.Workplace' AND BeginningInventory = '@This.BeginningInventory' And Active = true")]
        [Appearance("InvoiceCanvassingLineSelectItemHide", Criteria = "Mobile = false", Visibility = ViewItemVisibility.Hide, Context = "DetailView")]
        [Appearance("InvoiceCanvassingLineSelectItemShow", Criteria = "Mobile = true", Visibility = ViewItemVisibility.Show, Context = "DetailView")]
        [Appearance("InvoiceCanvassingLineSelectItemClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public BeginningInventoryLine SelectItem
        {
            get { return _selectItem; }
            set {
                SetPropertyValue("SelectItem", ref _selectItem, value);
                if(!IsLoading)
                {
                    if(this._selectItem.Item != null)
                    {
                        this.Item = this._selectItem.Item;
                        if (this._selectItem.Item.Description != null)
                        {

                            this.Description = this._selectItem.Item.Description;
                        }
                        if (this._selectItem.Item.BasedUOM != null)
                        {
                            this.DUOM = this._selectItem.Item.BasedUOM;
                        }
                        if(this._selectItem.StockType != StockType.None)
                        {
                            this.StockType = this._selectItem.StockType;
                        }
                        SetUOM();
                        SetPriceLine();
                        SetDiscountRule();
                    }
                    else
                    {
                        this.Item = null;
                        this.Description = null;
                        this.DUOM = null;
                        this.StockType = StockType.None;
                    }
                }
            }
        }

        [Browsable(false)]
        public XPCollection<Item> AvailableItem
        {
            get
            {
                string _beginString = null;
                string _endString = null;
                string _fullString = null;

                if (this.BeginningInventory != null)
                {
                    List<string> _stringBIL = new List<string>();

                    XPCollection<BeginningInventoryLine> _locBeginningInventoryLines = new XPCollection<BeginningInventoryLine>
                                                                        (Session, new GroupOperator(GroupOperatorType.And,
                                                                        new BinaryOperator("BeginningInventory", this.BeginningInventory),
                                                                        new BinaryOperator("Active", true)));

                    if (_locBeginningInventoryLines != null && _locBeginningInventoryLines.Count() > 0)
                    {
                        foreach (BeginningInventoryLine _locBeginningInventoryLine in _locBeginningInventoryLines)
                        {
                            if (_locBeginningInventoryLine.Item != null)
                            {
                                if (_locBeginningInventoryLine.Item.Code != null)
                                {
                                    _stringBIL.Add(_locBeginningInventoryLine.Item.Code);
                                }
                            }
                        }
                    }

                    IEnumerable<string> _stringArrayBILDistinct = _stringBIL.Distinct();
                    string[] _stringArrayBILList = _stringArrayBILDistinct.ToArray();
                    if (_stringArrayBILList.Length == 1)
                    {
                        for (int i = 0; i < _stringArrayBILList.Length; i++)
                        {
                            Item _locItm = Session.FindObject<Item>(new BinaryOperator("Code", _stringArrayBILList[i]));
                            if (_locItm != null)
                            {
                                if (i == 0)
                                {
                                    _beginString = "[Code]=='" + _locItm.Code + "'";
                                }
                            }
                        }
                    }
                    else if (_stringArrayBILList.Length > 1)
                    {
                        for (int i = 0; i < _stringArrayBILList.Length; i++)
                        {
                            Item _locItm = Session.FindObject<Item>(new BinaryOperator("Code", _stringArrayBILList[i]));
                            if (_locItm != null)
                            {
                                if (i == 0)
                                {
                                    _beginString = "[Code]=='" + _locItm.Code + "'";
                                }
                                else
                                {
                                    _endString = _endString + " OR [Code]=='" + _locItm.Code + "'";
                                }
                            }
                        }
                    }
                    _fullString = _beginString + _endString;

                    if (_fullString != null)
                    {
                        _availableItem = new XPCollection<Item>(Session, CriteriaOperator.Parse(_fullString));
                    }
                }

                return _availableItem;

            }
        }

        [ImmediatePostData()]
        [DataSourceProperty("AvailableItem")]
        [Appearance("InvoiceCanvassingLineItemShow", Criteria = "Mobile = false", Visibility = ViewItemVisibility.Show, Context = "DetailView")]
        [Appearance("InvoiceCanvassingLineItemHide", Criteria = "Mobile = true", Visibility = ViewItemVisibility.Hide, Context = "DetailView")]
        [Appearance("InvoiceCanvassingLineItemClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Item Item
        {
            get { return _item; }
            set
            {
                SetPropertyValue("Item", ref _item, value);
                if (!IsLoading)
                {
                    if (this._item != null)
                    {
                        if (this._item.Description != null)
                        {
                            this.Description = this._item.Description;
                        }
                        if(this._item.BasedUOM != null)
                        {
                            this.DUOM = this._item.BasedUOM;
                        }
                        SetUOM();
                        SetPriceLine();
                        SetDiscountRule();
                    } 
                    else
                    {
                        this.Description = null;
                        this.DUOM = null;
                    }
                    
                }
            }
        }

        [Appearance("InvoiceCanvassingLineDescriptionClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public string Description
        {
            get { return _description; }
            set { SetPropertyValue("Description", ref _description, value); }
        }

        #region DefaultQty

        [Appearance("InvoiceCanvassingLineDQtyClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [ImmediatePostData()]
        public double DQty
        {
            get { return _dQty; }
            set
            {
                SetPropertyValue("DQty", ref _dQty, value);
                if (!IsLoading)
                {
                    SetTotalQty();
                    SetTotalUnitAmount();
                    SetTotalAmount();
                }
            }
        }

        [Appearance("InvoiceCanvassingLineDUOMClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [ImmediatePostData()]
        [DataSourceCriteria("Active = true")]
        public UnitOfMeasure DUOM
        {
            get { return _dUom; }
            set
            {
                SetPropertyValue("DUOM", ref _dUom, value);
                if (!IsLoading)
                {
                    SetUOM();
                    SetTotalQty();
                    SetTotalUnitAmount();
                    SetTotalAmount();
                    
                }
            }
        }

        [Appearance("InvoiceCanvassingLineQtyClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [ImmediatePostData()]
        public double Qty
        {
            get { return _qty; }
            set
            {
                SetPropertyValue("Qty", ref _qty, value);
                if (!IsLoading)
                {
                    SetTotalQty();
                    SetTotalUnitAmount();
                    SetTotalAmount();
                }
            }
        }

        [Browsable(false)]
        public XPCollection<UnitOfMeasure> AvailableUnitOfMeasure
        {
            get
            {
                string _beginString = null;
                string _endString = null;
                string _fullString = null;
                if (Item == null)
                {
                    _availableUnitOfMeasure = new XPCollection<UnitOfMeasure>(Session, new GroupOperator(GroupOperatorType.And,
                                                                            new BinaryOperator("Active", true)));
                }
                else
                {
                    List<string> _stringUOM = new List<string>();

                    XPCollection<ItemUnitOfMeasure> _locItemUnitOfMeasures = new XPCollection<ItemUnitOfMeasure>
                                                                              (Session, new GroupOperator(GroupOperatorType.And,
                                                                               new BinaryOperator("Item", this.Item),
                                                                               new BinaryOperator("Active", true)));

                    if (_locItemUnitOfMeasures != null && _locItemUnitOfMeasures.Count() > 0)
                    {
                        foreach (ItemUnitOfMeasure _locItemUnitOfMeasure in _locItemUnitOfMeasures)
                        {
                            _stringUOM.Add(_locItemUnitOfMeasure.UOM.Code);
                        }
                    }

                    IEnumerable<string> _stringArrayUOMDistinct = _stringUOM.Distinct();
                    string[] _stringArrayUOMList = _stringArrayUOMDistinct.ToArray();
                    if (_stringArrayUOMList.Length == 1)
                    {
                        for (int i = 0; i < _stringArrayUOMList.Length; i++)
                        {
                            UnitOfMeasure _locUOM = Session.FindObject<UnitOfMeasure>(new BinaryOperator("Code", _stringArrayUOMList[i]));
                            if (_locUOM != null)
                            {
                                if (i == 0)
                                {
                                    _beginString = "[Code]=='" + _locUOM.Code + "'";
                                }
                            }
                        }
                    }
                    else if (_stringArrayUOMList.Length > 1)
                    {
                        for (int i = 0; i < _stringArrayUOMList.Length; i++)
                        {
                            UnitOfMeasure _locUOM = Session.FindObject<UnitOfMeasure>(new BinaryOperator("Code", _stringArrayUOMList[i]));
                            if (_locUOM != null)
                            {
                                if (i == 0)
                                {
                                    _beginString = "[Code]=='" + _locUOM.Code + "'";
                                }
                                else
                                {
                                    _endString = _endString + " OR [Code]=='" + _locUOM.Code + "'";
                                }
                            }
                        }
                    }
                    _fullString = _beginString + _endString;

                    if (_fullString != null)
                    {
                        _availableUnitOfMeasure = new XPCollection<UnitOfMeasure>(Session, CriteriaOperator.Parse(_fullString));
                    }

                }
                return _availableUnitOfMeasure;

            }
        }

        [ImmediatePostData()]
        [DataSourceProperty("AvailableUnitOfMeasure", DataSourcePropertyIsNullMode.SelectAll)]
        [DataSourceCriteria("Active = true")]
        public UnitOfMeasure UOM
        {
            get { return _uom; }
            set
            {
                SetPropertyValue("UOM", ref _uom, value);
                if (!IsLoading)
                {
                    SetTotalQty();
                    SetTotalUnitAmount();
                    SetTotalAmount();
                }
            }
        }

        [Appearance("InvoiceCanvassingLineTotalQtyEnabled", Enabled = false)]
        public double TQty
        {
            get { return _tQty; }
            set { SetPropertyValue("TotalQty", ref _tQty, value); }
        }

        [Appearance("InvoiceCanvassingLineStockTypeClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public StockType StockType
        {
            get { return _stockType; }
            set { SetPropertyValue("StockType", ref _stockType, value); }
        }

        #endregion DefaultQty

        #region Amount

        [Browsable(false)]
        [Appearance("InvoiceCanvassingLineCurrencyClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Currency Currency
        {
            get { return _currency; }
            set { SetPropertyValue("Currency", ref _currency, value); }
        }

        [ImmediatePostData()]
        [Appearance("InvoiceCanvassingLinePriceGroupClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public PriceGroup PriceGroup
        {
            get { return _priceGroup; }
            set
            {
                SetPropertyValue("PriceGroup", ref _priceGroup, value);
                if (!IsLoading)
                {
                    if (this._priceGroup != null)
                    {
                        SetPriceLine();
                        SetDiscountRule();
                    }
                }
            }
        }

        [Browsable(false)]
        [ImmediatePostData()]
        [Appearance("InvoiceCanvassingLinePriceLineClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public PriceLine PriceLine
        {
            get { return _priceLine; }
            set
            {
                SetPropertyValue("PriceLine", ref _priceLine, value);
                if (!IsLoading)
                {
                    if (this._priceLine != null)
                    {
                        this.UAmount = this._priceLine.Amount2a;
                    }
                }
            }
        }

        [Appearance("InvoiceCanvassingLineUAmountClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [ImmediatePostData()]
        public double UAmount
        {
            get { return _uAmount; }
            set
            {
                SetPropertyValue("UAmount", ref _uAmount, value);
                if (!IsLoading)
                {
                    _globFunc = new GlobalFunction();
                    if (this._uAmount > 0)
                    {
                        if (_globFunc.GetRoundingList(Session, ObjectList.InvoiceCanvassingLine, FieldName.UAmount) == true)
                        {
                            this._uAmount = _globFunc.GetRoundUp(Session, this._uAmount, ObjectList.InvoiceCanvassingLine, FieldName.UAmount);
                        }
                    }
                    SetTotalUnitAmount();
                    SetTotalAmount();
                }
            }
        }

        [Appearance("InvoiceCanvassingLineTUAmountEnabled", Enabled = false)]
        [ImmediatePostData()]
        public double TUAmount
        {
            get { return _tUAmount; }
            set { SetPropertyValue("TUAmount", ref _tUAmount, value); }
        }

        [ImmediatePostData]
        [Appearance("InvoiceCanvassingLineTaxClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [DataSourceCriteria("Active = true")]
        public Tax Tax
        {
            get { return _tax; }
            set
            {
                SetPropertyValue("Tax", ref _tax, value);
                if (!IsLoading)
                {
                    if (this._tax != null)
                    {
                        this.TxValue = this._tax.Value; 
                    }
                    else
                    {
                        SetNormalTax();
                    }
                }
            }
        }

        [Browsable(false)]
        [ImmediatePostData()]
        [Appearance("InvoiceCanvassingLineTxValueClose", Enabled = false)]
        public double TxValue
        {
            get { return _txValue; }
            set { SetPropertyValue("TxValue", ref _txValue, value); }
        }

        [Appearance("InvoiceCanvassingLineTxAmountEnabled", Enabled = false)]
        [ImmediatePostData()]
        public double TxAmount
        {
            get { return _txAmount; }
            set
            {
                SetPropertyValue("TxAmount", ref _txAmount, value);
                if (!IsLoading)
                {
                    _globFunc = new GlobalFunction();
                    if (this._txAmount > 0)
                    {
                        if (_globFunc.GetRoundingList(Session, ObjectList.InvoiceCanvassingLine, FieldName.TxAmount) == true)
                        {
                            this._txAmount = _globFunc.GetRoundUp(Session, this._txAmount, ObjectList.InvoiceCanvassingLine, FieldName.TxAmount);
                        }
                    }
                }
            }
        }

        [ImmediatePostData()]
        [DataSourceCriteria("Company = '@This.Company' AND Workplace = '@This.Workplace' AND MpItem = '@This.Item' AND PriceGroup = '@This.PriceGroup' AND Active = true")]
        [Appearance("InvoiceCanvassingLineDiscountRuleClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public DiscountRule DiscountRule
        {
            get { return _discountRule; }
            set { SetPropertyValue("DiscountRule", ref _discountRule, value); }
        }

        [Appearance("InvoiceCanvassingLineDiscAmountEnabled", Enabled = false)]
        [ImmediatePostData()]
        public double DiscAmount
        {
            get { return _discAmount; }
            set
            {
                SetPropertyValue("DiscAmount", ref _discAmount, value);
                if (!IsLoading)
                {
                    _globFunc = new GlobalFunction();
                    if (this._discAmount > 0)
                    {
                        if (_globFunc.GetRoundingList(Session, ObjectList.InvoiceCanvassingLine, FieldName.DiscAmount) == true)
                        {
                            this._discAmount = _globFunc.GetRoundUp(Session, this._discAmount, ObjectList.InvoiceCanvassingLine, FieldName.DiscAmount);
                        }
                    }
                }
            }
        }

        [Appearance("InvoiceCanvassingLineTAmountEnabled", Enabled = false)]
        [ImmediatePostData()]
        public double TAmount
        {
            get { return _tAmount; }
            set
            {
                SetPropertyValue("TAmount", ref _tAmount, value);
                if (!IsLoading)
                {
                    _globFunc = new GlobalFunction();
                    if (this._tAmount > 0)
                    {
                        if (_globFunc.GetRoundingList(Session, ObjectList.InvoiceCanvassingLine, FieldName.TAmount) == true)
                        {
                            this._tAmount = _globFunc.GetRoundUp(Session, this._tAmount, ObjectList.InvoiceCanvassingLine, FieldName.TAmount);
                        }
                    }
                }
            }
        }

        #endregion Amount

        [Appearance("InvoiceCanvassingLineStatusClose", Enabled = false)]
        public Status Status
        {
            get { return _status; }
            set { SetPropertyValue("Status", ref _status, value); }
        }

        [ModelDefault("DisplayFormat", "{0:dd/MM/yyyy}")]
        [ModelDefault("EditMask", "dd/MM/yyyy")]
        [Appearance("InvoiceCanvassingLineStatusDateClose", Enabled = false)]
        public DateTime StatusDate
        {
            get { return _statusDate; }
            set { SetPropertyValue("StatusDate", ref _statusDate, value); }
        }

        [Appearance("InvoiceCanvassingLinePostedCountEnabled", Enabled = false)]
        public int PostedCount
        {
            get { return _postedCount; }
            set { SetPropertyValue("PostedCount", ref _postedCount, value); }
        }

        [ImmediatePostData()]
        [RuleRequiredField(DefaultContexts.Save)]
        [Appearance("InvoiceCanvassingLineInvoiceCanvassingClose", Enabled = false)]
        [Association("InvoiceCanvassing-InvoiceCanvassingLines")]
        public InvoiceCanvassing InvoiceCanvassing
        {
            get { return _invoiceCanvassing; }
            set {
                SetPropertyValue("InvoiceCanvassing", ref _invoiceCanvassing, value);
                if(!IsLoading)
                {
                    if(this._invoiceCanvassing != null)
                    {
                        if(Session.IsNewObject(this))
                        {
                            if (this._invoiceCanvassing.Company != null)
                            {
                                this.Company = this._invoiceCanvassing.Company;
                            }
                            if (this._invoiceCanvassing.Workplace != null)
                            {
                                this.Workplace = this._invoiceCanvassing.Workplace;
                            }
                            if (this._invoiceCanvassing.Division != null)
                            {
                                this.Division = this._invoiceCanvassing.Division;
                            }
                            if (this._invoiceCanvassing.Department != null)
                            {
                                this.Department = this._invoiceCanvassing.Department;
                            }
                            if (this._invoiceCanvassing.Section != null)
                            {
                                this.Section = this._invoiceCanvassing.Section;
                            }
                            if (this._invoiceCanvassing.Employee != null)
                            {
                                this.Employee = this._invoiceCanvassing.Employee;
                            }
                            if (this._invoiceCanvassing.Salesman != null)
                            {
                                this.Salesman = this._invoiceCanvassing.Salesman;
                            }
                            if (this._invoiceCanvassing.Vehicle != null)
                            {
                                this.Vehicle = this._invoiceCanvassing.Vehicle;
                            }
                            if (this._invoiceCanvassing.BeginningInventory != null)
                            {
                                this.BeginningInventory = this._invoiceCanvassing.BeginningInventory;
                            }
                            if(this._invoiceCanvassing.Currency != null)
                            {
                                this.Currency = this._invoiceCanvassing.Currency;
                            }
                            if (this._invoiceCanvassing.PriceGroup != null)
                            {
                                this.PriceGroup = this._invoiceCanvassing.PriceGroup;
                            }
                            
                        }
                    }
                }
            }
        }

        //AuditTrail
        [CollectionOperationSet(AllowAdd = false, AllowRemove = true)]
        public XPCollection<AuditDataItemPersistent> ChangeHistory
        {
            get
            {
                if (changeHistory == null)
                {
                    changeHistory = AuditedObjectWeakReference.GetAuditTrail(Session, this);
                }
                return changeHistory;
            }
        }

        #endregion Field

        //=============================================== Code Only =========================================

        #region CodeOnly

        #region No

        public void UpdateNo()
        {
            try
            {
                if (!IsLoading && Session.IsNewObject(this))
                {
                    if (this.InvoiceCanvassing != null)
                    {
                        object _makRecord = Session.Evaluate<InvoiceCanvassingLine>(CriteriaOperator.Parse("Max(No)"), CriteriaOperator.Parse("InvoiceCanvassing=?", this.InvoiceCanvassing));
                        this.No = Convert.ToInt32(_makRecord) + 1;
                        this.Save();
                        RecoveryUpdateNo();
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = InvoiceCanvassingLine " + ex.ToString());
            }
        }

        public void RecoveryUpdateNo()
        {
            try
            {
                if (this.InvoiceCanvassing != null)
                {
                    InvoiceCanvassing _numHeader = Session.FindObject<InvoiceCanvassing>
                                            (new BinaryOperator("Code", this.InvoiceCanvassing.Code));

                    XPCollection<InvoiceCanvassingLine> _numLines = new XPCollection<InvoiceCanvassingLine>
                                                             (Session, new BinaryOperator("InvoiceCanvassing", _numHeader),
                                                              new SortProperty("No", DevExpress.Xpo.DB.SortingDirection.Ascending));
                    if (_numLines != null)
                    {
                        int i1 = 0;
                        foreach (InvoiceCanvassingLine _numLine in _numLines)
                        {
                            i1 += 1;
                            _numLine.No = i1;
                            _numLine.Save();
                        }
                        i1 = 1;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = InvoiceCanvassingLine " + ex.ToString());
            }
        }

        public void RecoveryDeleteNo()
        {
            try
            {
                if (this.InvoiceCanvassing != null)
                {
                    InvoiceCanvassing _numHeader = Session.FindObject<InvoiceCanvassing>
                                            (new BinaryOperator("Code", this.InvoiceCanvassing.Code));

                    XPCollection<InvoiceCanvassingLine> _numLines = new XPCollection<InvoiceCanvassingLine>
                                                             (Session, new GroupOperator(GroupOperatorType.And,
                                                              new BinaryOperator("This", this, BinaryOperatorType.NotEqual),
                                                              new BinaryOperator("InvoiceCanvassing", _numHeader)),
                                                              new SortProperty("No", DevExpress.Xpo.DB.SortingDirection.Ascending));
                    if (_numLines != null)
                    {
                        int i = 0;
                        foreach (InvoiceCanvassingLine _numLine in _numLines)
                        {
                            i += 1;
                            _numLine.No = i;
                            _numLine.Save();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = InvoiceCanvassingLine " + ex.ToString());
            }
        }

        #endregion No

        #region Set

        private void SetTotalQty()
        {
            try
            {
                double _locInvLineTotal = 0;
                DateTime now = DateTime.Now;
                if (this.InvoiceCanvassing != null)
                {
                    if (this.Item != null && this.UOM != null && this.DUOM != null)
                    {

                        ItemUnitOfMeasure _locItemUOM  = this.Item.Session.FindObject<ItemUnitOfMeasure>
                                                            (new GroupOperator(GroupOperatorType.And,
                                                             new BinaryOperator("Item", this.Item),
                                                             new BinaryOperator("UOM", this.UOM),
                                                             new BinaryOperator("DefaultUOM", this.DUOM),
                                                             new BinaryOperator("Active", true)));

                        if (_locItemUOM != null)
                        {
                            if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                            {
                                _locInvLineTotal = this.Qty * _locItemUOM.DefaultConversion + this.DQty;
                            }
                            else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                            {
                                _locInvLineTotal = this.Qty / _locItemUOM.Conversion + this.DQty;
                            }
                            else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                            {
                                _locInvLineTotal = this.Qty + this.DQty;
                            }

                            this.TQty = _locInvLineTotal;
                        }
                    }
                    else
                    {
                        _locInvLineTotal = this.Qty + this.DQty;
                        this.TQty = _locInvLineTotal;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = InvoiceCanvassingLine " + ex.ToString());
            }
        }

        private void SetTotalUnitAmount()
        {
            try
            {
                _globFunc = new GlobalFunction();

                if (_tQty >= 0 & _uAmount >= 0)
                {
                    if (_globFunc.GetRoundingList(Session, ObjectList.InvoiceCanvassingLine, FieldName.TUAmount) == true)
                    {
                        this.TUAmount = _globFunc.GetRoundUp(Session, (this.TQty * this.UAmount), ObjectList.InvoiceCanvassingLine, FieldName.TUAmount);
                    }
                    else
                    {
                        this.TUAmount = this.TQty * this.UAmount;
                    }

                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = InvoiceCanvassingLine " + ex.ToString());
            }
        }

        private void SetTotalAmount()
        {
            try
            {
                _globFunc = new GlobalFunction();
                if (!IsLoading)
                {
                    if (_uAmount >= 0 && _tQty > 0)
                    {
                        if (_globFunc.GetRoundingList(Session, ObjectList.InvoiceCanvassingLine, FieldName.TAmount) == true)
                        {
                            this.TAmount = _globFunc.GetRoundUp(Session, (this.TUAmount), ObjectList.InvoiceCanvassingLine, FieldName.TAmount);
                        }
                        else
                        {
                            this.TAmount = this.TUAmount ;
                        }
                    }
                       
                }
                    
                
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = InvoiceCanvassingLine " + ex.ToString());
            }
        }

        private void SetNormalTax()
        {
            try
            {
                if (_tax != null && _txValue > 0 && _txAmount > 0)
                {
                    this.Tax = this.Tax;
                    this.TxValue = this.TxValue;
                    this.TxAmount = this.TxAmount;
                }
                else
                {
                    this.TxValue = 0;
                    this.TxAmount = 0;
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = InvoiceCanvassingLine " + ex.ToString());
            }
        }

        private void SetPriceLine()
        {
            try
            {
                if (this.Company != null && this.Workplace != null && this.Item != null && this.PriceGroup != null && this.Currency != null)
                {
                    PriceLine _locPriceLine = Session.FindObject<PriceLine>(new GroupOperator(GroupOperatorType.And,
                                                                            new BinaryOperator("Company", this.Company),
                                                                            new BinaryOperator("Workplace", this.Workplace),
                                                                            new BinaryOperator("Item", this.Item),
                                                                            new BinaryOperator("PriceGroup", this.PriceGroup),
                                                                            new BinaryOperator("Currency", this.Currency),
                                                                            new BinaryOperator("Active", true)));
                    if (_locPriceLine != null)
                    {
                        this.PriceLine = _locPriceLine;
                        this.UAmount = _locPriceLine.Amount2a;
                    }
                    else
                    {
                        this.PriceLine = null;
                        this.UAmount = 0;
                    }
                }else
                {
                    this.PriceLine = null;
                    this.UAmount = 0;
                }
                
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = InvoiceCanvassingLine " + ex.ToString());
            }
        }

        private void SetUOM()
        {
            try
            {
                if (this.Company != null && this.Item != null && this.DUOM != null)
                {
                    ItemUnitOfMeasure _locIUOM = Session.FindObject<ItemUnitOfMeasure>(new GroupOperator(GroupOperatorType.And,
                                                                            new BinaryOperator("Company", this.Company),
                                                                            new BinaryOperator("Item", this.Item),
                                                                            new BinaryOperator("Default", true),
                                                                            new BinaryOperator("Active", true)));
                    if (_locIUOM != null)
                    {
                        if(_locIUOM.UOM != null)
                        {
                            this.UOM = _locIUOM.UOM;
                        }
                        else
                        {
                            this.UOM = null;
                        }
                        
                    }
                    else
                    {
                        this.UOM = null;
                    }
                }
                else
                {
                    this.UOM = null;
                }

            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = InvoiceCanvassingLine " + ex.ToString());
            }
        }

        private void SetDiscountRule()
        {
            try
            {
                if (this.Company != null && this.Workplace != null && this.Item != null && this.PriceGroup != null )
                {
                    DiscountRule _locDiscountRule2a = Session.FindObject<DiscountRule>
                                                            (new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("Company", this.Company),
                                                            new BinaryOperator("Workplace", this.Workplace),
                                                            new BinaryOperator("PriceGroup", this.PriceGroup),
                                                            new BinaryOperator("AllItem", true),
                                                            new BinaryOperator("RuleType", RuleType.Rule2),
                                                            new BinaryOperator("Active", true)));
                    if(_locDiscountRule2a != null)
                    {
                        this.DiscountRule = _locDiscountRule2a;
                    }else
                    {
                        DiscountRule _locDiscountRule2b = Session.FindObject<DiscountRule>
                                                            (new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("Company", this.Company),
                                                            new BinaryOperator("Workplace", this.Workplace),
                                                            new BinaryOperator("PriceGroup", this.PriceGroup),
                                                            new BinaryOperator("MpItem", this.Item),
                                                            new BinaryOperator("RuleType", RuleType.Rule2),
                                                            new BinaryOperator("Active", true)));
                        if (_locDiscountRule2b != null)
                        {
                            this.DiscountRule = _locDiscountRule2b;
                        }
                    }
                    
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = InvoiceCanvassingLine " + ex.ToString());
            }
        }

        #endregion Set

        #endregion CodeOnly
    }
}