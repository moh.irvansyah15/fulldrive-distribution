﻿using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;

using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.ConditionalAppearance;

using System.Web;
using System.Configuration;
using System.Web.Configuration;
using System.IO;
using DevExpress.ExpressApp.Editors;

namespace FullDrive.Module.BusinessObjects
{
    [DefaultClassOptions]
    [NavigationItem("Finance & Accounting")]
    [DefaultProperty("Code")]
    [RuleCombinationOfPropertiesIsUnique("AccountingPeriodicRuleUnique", DefaultContexts.Save, "Code")]
    //[ImageName("BO_Contact")]
    //[DefaultProperty("DisplayMemberNameForLookupEditorsOfThisType")]
    //[DefaultListViewOptions(MasterDetailMode.ListViewOnly, false, NewItemRowPosition.None)]
    //[Persistent("DatabaseTableName")]
    // Specify more UI options using a declarative approach (https://documentation.devexpress.com/#eXpressAppFramework/CustomDocument112701).
    public class AccountingPeriodic : BaseObject
    { // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).
        private string _code;
        private string _name;
        #region InitialOrganization
        private Company _company;
        private XPCollection<Workplace> _availableWorkplace;
        private Workplace _workplace;
        private Division _division;
        private Department _department;
        private Section _section;
        private Employee _employee;
        #endregion InitialOrganization
        private DateTime _startDate;
        private DateTime _endDate;
        private int _month;
        private int _year;
        private DateTime _closingDate;
        private bool _importData;
        private ObjectList _objectList;
        private FileData _dataImport;
        private string _message;
        private bool _active;
        private string _localUserAccess;
        private GlobalFunction _globFunc;
        //AuditTrail
        private XPCollection<AuditDataItemPersistent> changeHistory;

        public AccountingPeriodic(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if (!IsLoading)
            {
                if(Session.IsNewObject(this))
                {
                    _globFunc = new GlobalFunction();
                    #region UserAccess
                    _localUserAccess = SecuritySystem.CurrentUserName;
                    UserAccess _locUserAccess = Session.FindObject<UserAccess>(new BinaryOperator("UserName", _localUserAccess));
                    if (_locUserAccess != null)
                    {
                        if (_locUserAccess.Employee != null)
                        {
                            if (_locUserAccess.Employee.Company != null && _locUserAccess.Employee.Workplace != null)
                            {
                                this.Code = _globFunc.GetNumberingUnlockOptimisticRecordBasedOrganization(this.Session.DataLayer, ObjectList.AccountingPeriodic, _locUserAccess.Employee.Company, _locUserAccess.Employee.Workplace);
                            }
                            else
                            {
                                this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.AccountingPeriodic);
                            }
                            if (_locUserAccess.Employee.Company != null)
                            {
                                Company = _locUserAccess.Employee.Company;
                            }
                            if (_locUserAccess.Employee.Workplace != null)
                            {
                                this.Workplace = _locUserAccess.Employee.Workplace;
                            }
                            if (_locUserAccess.Employee.Division != null)
                            {
                                this.Division = _locUserAccess.Employee.Division;
                            }
                            if (_locUserAccess.Employee.Department != null)
                            {
                                this.Department = _locUserAccess.Employee.Department;
                            }
                            if (_locUserAccess.Employee.Section != null)
                            {
                                this.Section = _locUserAccess.Employee.Section;
                            }
                            this.Employee = _locUserAccess.Employee;
                        }
                    }
                    #endregion UserAccess 
                    this.ObjectList = ObjectList.AccountingPeriodicLine;
                }
            }
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).
        }

        protected override void OnSaving()
        {
            base.OnSaving();
            if (!IsLoading && IsSaving)
            {
                if (this.Active == true)
                {
                    CheckActiveSystem();
                }
            }
        }

        #region Field

        [RuleRequiredField(DefaultContexts.Save)]
        [Appearance("AccountingPeriodicCodeClose", Enabled = false)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue("Code", ref _code, value); }
        }

        public string Name
        {
            get { return _name; }
            set { SetPropertyValue("Name", ref _name, value); }
        }

        #region Organization

        [ImmediatePostData()]
        [RuleRequiredField(DefaultContexts.Save)]
        [DataSourceCriteria("Active = true")]
        public Company Company
        {
            get { return _company; }
            set { SetPropertyValue("Company", ref _company, value); }
        }

        [Browsable(false)]
        public XPCollection<Workplace> AvailableWorkplace
        {
            get
            {
                if (!IsLoading)
                {
                    if (this.Company != null)
                    {
                        _localUserAccess = SecuritySystem.CurrentUserName;
                        UserAccess _locUserAccess = Session.FindObject<UserAccess>(new BinaryOperator("UserName", _localUserAccess));
                        if (_locUserAccess != null)
                        {
                            string _beginString = null;
                            string _endString = null;
                            string _fullString = null;
                            List<string> _stringWorkplace = new List<string>();

                            XPCollection<OrganizationSetupDetail> _locOrganizationSetupDetails = new XPCollection<OrganizationSetupDetail>
                                                                            (Session, new GroupOperator(GroupOperatorType.And,
                                                                            new BinaryOperator("Company", this.Company),
                                                                            new BinaryOperator("UserAccess", _locUserAccess),
                                                                            new BinaryOperator("OpenWorkplace", true),
                                                                            new BinaryOperator("Active", true)));

                            if (_locOrganizationSetupDetails != null && _locOrganizationSetupDetails.Count() > 0)
                            {
                                foreach (OrganizationSetupDetail _locOrganizationSetupDetail in _locOrganizationSetupDetails)
                                {
                                    if (_locOrganizationSetupDetail.Workplace != null)
                                    {
                                        if (_locOrganizationSetupDetail.Workplace.Code != null)
                                        {
                                            _stringWorkplace.Add(_locOrganizationSetupDetail.Workplace.Code);
                                        }
                                    }
                                }

                                #region Workplace
                                IEnumerable<string> _stringArrayWorkplaceDistinct = _stringWorkplace.Distinct();
                                string[] _stringArrayWorkplaceList = _stringArrayWorkplaceDistinct.ToArray();
                                if (_stringArrayWorkplaceList.Length == 1)
                                {
                                    for (int i = 0; i < _stringArrayWorkplaceList.Length; i++)
                                    {
                                        Workplace _locWorkplace = Session.FindObject<Workplace>(new BinaryOperator("Code", _stringArrayWorkplaceList[i]));
                                        if (_locWorkplace != null)
                                        {
                                            if (i == 0)
                                            {
                                                _beginString = "[Code]=='" + _locWorkplace.Code + "'";
                                            }
                                        }
                                    }
                                }
                                else if (_stringArrayWorkplaceList.Length > 1)
                                {
                                    for (int i = 0; i < _stringArrayWorkplaceList.Length; i++)
                                    {
                                        Workplace _locWorkplace = Session.FindObject<Workplace>(new BinaryOperator("Code", _stringArrayWorkplaceList[i]));
                                        if (_locWorkplace != null)
                                        {
                                            if (i == 0)
                                            {
                                                _beginString = "[Code]=='" + _locWorkplace.Code + "'";
                                            }
                                            else
                                            {
                                                _endString = _endString + " OR [Code]=='" + _locWorkplace.Code + "'";
                                            }
                                        }

                                    }
                                }
                                _fullString = _beginString + _endString;


                                if (_fullString != null)
                                {
                                    _availableWorkplace = new XPCollection<Workplace>(Session, CriteriaOperator.Parse(_fullString));
                                }

                                #endregion Workplace
                            }
                            else
                            {
                                OrganizationSetupDetail _locOrgSetDetail = Session.FindObject<OrganizationSetupDetail>
                                                                            (new GroupOperator(GroupOperatorType.And,
                                                                            new BinaryOperator("Company", this.Company),
                                                                            new BinaryOperator("UserAccess", _locUserAccess),
                                                                            new BinaryOperator("AllWorkplace", true),
                                                                            new BinaryOperator("Active", true)));
                                if (_locOrgSetDetail != null)
                                {
                                    _availableWorkplace = new XPCollection<Workplace>(Session,
                                                        new GroupOperator(GroupOperatorType.And,
                                                        new BinaryOperator("Company", this.Company),
                                                        new BinaryOperator("Active", true)));
                                }
                            }
                        }
                    }
                }

                return _availableWorkplace;
            }
        }

        [ImmediatePostData()]
        [DataSourceProperty("AvailableWorkplace")]
        public Workplace Workplace
        {
            get { return _workplace; }
            set { SetPropertyValue("Workplace", ref _workplace, value); }
        }

        [Browsable(false)]
        public Division Division
        {
            get { return _division; }
            set { SetPropertyValue("Division", ref _division, value); }
        }

        [Browsable(false)]
        public Department Department
        {
            get { return _department; }
            set { SetPropertyValue("Department", ref _department, value); }
        }

        [Browsable(false)]
        public Section Section
        {
            get { return _section; }
            set { SetPropertyValue("Section", ref _section, value); }
        }

        [Browsable(false)]
        public Employee Employee
        {
            get { return _employee; }
            set { SetPropertyValue("Employee", ref _employee, value); }
        }

        #endregion Organization

        [ImmediatePostData()]
        [ModelDefault("DisplayFormat", "{0:dd/MM/yyyy}")]
        [ModelDefault("EditMask", "dd/MM/yyyy")]
        public DateTime StartDate
        {
            get { return _startDate; }
            set {
                SetPropertyValue("StartDate", ref _startDate, value);
                if(!IsLoading)
                {
                    this.Month = this._startDate.Month;
                    this.Year = this._startDate.Year;
                }
            }
        }

        [ModelDefault("DisplayFormat", "{0:dd/MM/yyyy}")]
        [ModelDefault("EditMask", "dd/MM/yyyy")]
        public DateTime EndDate
        {
            get { return _endDate; }
            set { SetPropertyValue("EndDate", ref _endDate, value); }
        }

        //[Appearance("AccountingPeriodicMonthClose", Enabled = false)]
        [ImmediatePostData()]
        public int Month
        {
            get { return _month; }
            set {
                SetPropertyValue("Month", ref _month, value);
                if(!IsLoading)
                {
                    if(this._month < 0)
                    {
                        this._month = 0;
                    }
                    if(this._month > 12)
                    {
                        this._month = 12;
                    }
                }
            }
        }

        //[Appearance("AccountingPeriodicYearClose", Enabled = false)]
        [ImmediatePostData()]
        public int Year
        {
            get { return _year; }
            set {
                SetPropertyValue("Year", ref _year, value);
                if(!IsLoading)
                {
                    if(this._year < 0)
                    {
                        this.Year = 0;
                    }
                    if(this._year > 9999 )
                    {
                        this._year = 9999;
                    }
                }
            }
        }

        [ModelDefault("DisplayFormat", "{0:dd/MM/yyyy}")]
        [ModelDefault("EditMask", "dd/MM/yyyy")]
        public DateTime ClosingDate
        {
            get { return _closingDate; }
            set { SetPropertyValue("ClosingDate", ref _closingDate, value); }
        }

        [ImmediatePostData()]
        public bool ImportData
        {
            get { return _importData; }
            set {
                SetPropertyValue("ImportData", ref _importData, value);
                if(!IsLoading)
                {
                    if(this._importData == false)
                    {
                        this.DataImport.Clear();
                        this.Message = null;
                    }
                }
            }
        }

        [Browsable(false)]
        public ObjectList ObjectList
        {
            get { return _objectList; }
            set { SetPropertyValue("ObjectList", ref _objectList, value); }
        }

        [ImmediatePostData()]
        [Appearance("AccountingPeriodicDataImportClose1", Criteria = "ImportData = false", Enabled = false)]
        [Appearance("AccountingPeriodicDataImportClose2", Criteria = "ImportData = true", Enabled = true)]
        public FileData DataImport
        {
            get { return _dataImport; }
            set {
                SetPropertyValue("DataImport", ref _dataImport, value);
                if (!IsLoading)
                {
                    if (this._dataImport != null)
                    {
                        string fileExist = null;
                        fileExist = HttpContext.Current.Server.MapPath("~/UploadFile/" + DataImport.FileName);
                        if (File.Exists(fileExist))
                        {
                            this.Message = "File is already available in server";
                        }
                        else
                        {
                            this.Message = null;
                        }
                    }
                    else
                    {
                        this.Message = null;
                    }
                }
            }
        }

        [Appearance("AccountingPeriodicMessageClose1", Criteria = "ImportData = false", Enabled = false)]
        [Appearance("AccountingPeriodicMessageClose2", Criteria = "ImportData = true", Enabled = true)]
        public string Message
        {
            get { return _message; }
            set { SetPropertyValue("Message", ref _message, value); }
        }

        public bool Active
        {
            get { return _active; }
            set { SetPropertyValue("Active", ref _active, value); }
        }

        [Association("AccountingPeriodic-AccountingPeriodicLines")]
        public XPCollection<AccountingPeriodicLine> AccountingPeriodicLines
        {
            get { return GetCollection<AccountingPeriodicLine>("AccountingPeriodicLines"); }
        }

        //AuditTrail
        [CollectionOperationSet(AllowAdd = false, AllowRemove = true)]
        public XPCollection<AuditDataItemPersistent> ChangeHistory
        {
            get
            {
                if (changeHistory == null)
                {
                    changeHistory = AuditedObjectWeakReference.GetAuditTrail(Session, this);
                }
                return changeHistory;
            }
        }

        #endregion Field

        //=========================================== Code Only ===========================================

        private void CheckActiveSystem()
        {
            try
            {
                if(this.Active == true)
                {
                    XPCollection<AccountingPeriodic> _locAccountPeriodics = new XPCollection<AccountingPeriodic>(Session, new GroupOperator(GroupOperatorType.And,
                                                                            new BinaryOperator("This", this, BinaryOperatorType.NotEqual)));
                    if (_locAccountPeriodics != null && _locAccountPeriodics.Count() > 0)
                    {
                        foreach (AccountingPeriodic _locAccountPeriodic in _locAccountPeriodics)
                        {
                            if (_locAccountPeriodic.Active == true && _locAccountPeriodic.Company == this.Company && _locAccountPeriodic.Workplace == this.Workplace)
                            {
                                _locAccountPeriodic.Active = false;
                                _locAccountPeriodic.Save();
                            }
                        }
                    }
                }
                  
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Business Object = AccountingPeriodic", ex.ToString());
            }
        }
    }
}