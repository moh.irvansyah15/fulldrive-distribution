﻿using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;

using DevExpress.ExpressApp.Editors;
using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.ConditionalAppearance;

namespace FullDrive.Module.BusinessObjects
{
    [DefaultClassOptions]
    [DefaultProperty("Name")]
    [NavigationItem("Sales")]
    [RuleCombinationOfPropertiesIsUnique("RouteInvoiceRuleUnique", DefaultContexts.Save, "Code")]
    //[ImageName("BO_Contact")]
    //[DefaultProperty("DisplayMemberNameForLookupEditorsOfThisType")]
    //[DefaultListViewOptions(MasterDetailMode.ListViewOnly, false, NewItemRowPosition.None)]
    //[Persistent("DatabaseTableName")]
    // Specify more UI options using a declarative approach (https://documentation.devexpress.com/#eXpressAppFramework/CustomDocument112701).
    public class RouteInvoice : BaseObject
    { // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).


        private string _code;
        private Company _company;
        private Workplace _workplace;
        private String _name;
        private Employee _salesman;
        private Vehicle _vehicle;
        private BusinessPartner _customer;
        private DeliveryOrder _deliveryOrder;
        private SalesInvoice _salesInvoice;
        private SalesInvoiceCmp _salesInvoiceCmp;
        private double _totAmount;
        private double _totTaxAmount;
        private double _totDiscAmount;
        private double _amount;
        private double _amountDN;
        private double _amountCN;
        private double _amountColl;
        private CollectStatus _collectStatus;
        private DateTime _collectStatusDate;
        private OrderCollection _orderCollection;
        private AmendDelivery _amendDelivery;
        private bool _multiInvoice;
        private bool _active;
        private bool _ad;
        private bool _oc;
        private Route _route;
        private string _userAccessIn;
        private GlobalFunction _globFunc;
        //AuditTrail
        private XPCollection<AuditDataItemPersistent> changeHistory;

        public RouteInvoice(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if (!IsLoading)
            {
                if (Session.IsNewObject(this))
                {
                    _globFunc = new GlobalFunction();
                    #region UserAccess
                    _userAccessIn = SecuritySystem.CurrentUserName;
                    UserAccess _locUserAccess = Session.FindObject<UserAccess>(new BinaryOperator("UserName", _userAccessIn));

                    if (_locUserAccess != null)
                    {
                        if (_locUserAccess.Employee != null)
                        {
                            if (_locUserAccess.Employee.Company != null && _locUserAccess.Employee.Workplace != null)
                            {
                                this.Code = _globFunc.GetNumberingUnlockOptimisticRecordBasedOrganization(this.Session.DataLayer, ObjectList.RouteInvoice, _locUserAccess.Employee.Company, _locUserAccess.Employee.Workplace);
                            }
                            else
                            {
                                this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.RouteInvoice);
                            }
                        }
                    }
                    #endregion UserAccess
                    this.Active = true;
                }
            }
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).
        }

        #region Field
        //[Appearance("RouteInvoiceCodeEnabled", Enabled = false)]
        [RuleRequiredField(DefaultContexts.Save)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue("Code", ref _code, value); }
        }

        [ImmediatePostData()]
        [RuleRequiredField(DefaultContexts.Save)]
        [DataSourceCriteria("Active = true")]
        [Appearance("RouteInvoiceCompanyEnabled", Enabled = false)]
        public Company Company
        {
            get { return _company; }
            set { SetPropertyValue("Company", ref _company, value); }
        }

        [ImmediatePostData()]
        [RuleRequiredField(DefaultContexts.Save)]
        [DataSourceCriteria("Active = true")]
        [Appearance("RouteInvoiceWorkplaceEnabled", Enabled = false)]
        public Workplace Workplace
        {
            get { return _workplace; }
            set { SetPropertyValue("Workplace", ref _workplace, value); }
        }

        [ImmediatePostData()]
        [RuleRequiredField(DefaultContexts.Save)]
        public string Name
        {
            get { return _name; }
            set { SetPropertyValue("Name", ref _name, value); }
        }

        public Employee Salesman
        {
            get { return _salesman; }
            set { SetPropertyValue("Salesman", ref _salesman, value); }
        }

        public Vehicle Vehicle
        {
            get { return _vehicle; }
            set { SetPropertyValue("Vehicle", ref _vehicle, value); }
        }

        [ImmediatePostData()]
        public BusinessPartner Customer
        {
            get { return _customer; }
            set {
                SetPropertyValue("Customer", ref _customer, value);
                if(!IsLoading)
                {
                    SetName();
                }
            }
        }

        public DeliveryOrder DeliveryOrder
        {
            get { return _deliveryOrder; }
            set { SetPropertyValue("DeliveryOrder", ref _deliveryOrder, value); }
        }

        public OrderCollection OrderCollection
        {
            get { return _orderCollection; }
            set { SetPropertyValue("OrderCollection", ref _orderCollection, value); }
        }

        public AmendDelivery AmendDelivery
        {
            get { return _amendDelivery; }
            set { SetPropertyValue("AmendDelivery", ref _amendDelivery, value); }
        }

        [ImmediatePostData()]
        public SalesInvoice SalesInvoice
        {
            get { return _salesInvoice; }
            set {
                SetPropertyValue("SalesInvoice", ref _salesInvoice, value);
                if(!IsLoading)
                {
                    SetName();
                }
            }
        }

        [ImmediatePostData()]
        public SalesInvoiceCmp SalesInvoiceCmp
        {
            get { return _salesInvoiceCmp; }
            set { SetPropertyValue("SalesInvoiceCmp", ref _salesInvoiceCmp, value); }
        }

        #region OriTotAmount

        [Appearance("RouteInvoiceTotAmountClose", Enabled = false)]
        public double TotAmount
        {
            get { return _totAmount; }
            set { SetPropertyValue("TotAmount", ref _totAmount, value); }
        }

        [Appearance("RouteInvoiceTotTaxAmountClose", Enabled = false)]
        public double TotTaxAmount
        {
            get { return _totTaxAmount; }
            set { SetPropertyValue("TotTaxAmount", ref _totTaxAmount, value); }
        }

        [Appearance("RouteInvoiceTotDiscAmountClose", Enabled = false)]
        public double TotDiscAmount
        {
            get { return _totDiscAmount; }
            set { SetPropertyValue("TotDiscAmount", ref _totDiscAmount, value); }
        }

        [ImmediatePostData()]
        [Appearance("RouteInvoiceAmountClose", Enabled = false)]
        public double Amount
        {
            get { return _amount; }
            set
            {
                SetPropertyValue("Amount", ref _amount, value);
                if (!IsLoading)
                {
                    SetTotalAmountCollection();
                }
            }
        }

        #endregion OriTotAmount

        [ImmediatePostData()]
        [Appearance("RouteInvoiceAmountDNClose", Enabled = false)]
        public double AmountDN
        {
            get { return _amountDN; }
            set
            {
                SetPropertyValue("AmountDN", ref _amountDN, value);
                if (!IsLoading)
                {
                    SetTotalAmountCollection();
                }
            }
        }

        [ImmediatePostData()]
        [Appearance("RouteInvoiceAmountCNClose", Enabled = false)]
        public double AmountCN
        {
            get { return _amountCN; }
            set
            {
                SetPropertyValue("AmountCN", ref _amountCN, value);
                if (!IsLoading)
                {
                    SetTotalAmountCollection();
                }
            }
        }

        [Appearance("RouteInvoiceAmountCollClose", Enabled = false)]
        public double AmountColl
        {
            get { return _amountColl; }
            set { SetPropertyValue("AmountColl", ref _amountColl, value); }
        }

        public CollectStatus CollectStatus
        {
            get { return _collectStatus; }
            set { SetPropertyValue("CollectStatus", ref _collectStatus, value); }
        }

        [ModelDefault("DisplayFormat", "{0:dd/MM/yyyy}")]
        [ModelDefault("EditMask", "dd/MM/yyyy")]
        [Appearance("RouteInvoiceCollectStatusDateEnabled", Enabled = false)]
        public DateTime CollectStatusDate
        {
            get { return _collectStatusDate; }
            set { SetPropertyValue("CollectStatusDate", ref _collectStatusDate, value); }
        }

        public bool MultiInvoice
        {
            get { return _multiInvoice; }
            set { SetPropertyValue("MultiInvoice", ref _multiInvoice, value); }
        }

        public bool Active
        {
            get { return _active; }
            set { SetPropertyValue("Active", ref _active, value); }
        }

        public bool AD
        {
            get { return _ad; }
            set { SetPropertyValue("AD", ref _ad, value); }
        }

        public bool OC
        {
            get { return _oc; }
            set { SetPropertyValue("OC", ref _oc, value); }
        }

        [ImmediatePostData()]
        [RuleRequiredField(DefaultContexts.Save)]
        [Association("Route-RouteInvoices")]
        [Appearance("RouteInvoiceRouteClose", Enabled = false)]
        public Route Route
        {
            get { return _route; }
            set {
                SetPropertyValue("Route", ref _route, value);
                if(!IsLoading)
                {
                    if(this._route != null)
                    {
                        if (this._route.Company != null)
                        {
                            this.Company = this._route.Company;
                        }
                        if (this._route.Workplace != null)
                        {
                            this.Workplace = this._route.Workplace;
                        }
                        if (this._route.Salesman != null)
                        {
                            this.Salesman = this._route.Salesman;
                        }
                        if (this._route.Customer != null)
                        {
                            this.Customer = this._route.Customer;
                        }
                        
                    }
                }
            }
        }

        //AuditTrail
        [CollectionOperationSet(AllowAdd = false, AllowRemove = true)]
        public XPCollection<AuditDataItemPersistent> ChangeHistory
        {
            get
            {
                if (changeHistory == null)
                {
                    changeHistory = AuditedObjectWeakReference.GetAuditTrail(Session, this);
                }
                return changeHistory;
            }
        }
        #endregion Field

        //============================= Code Only ===================================

        private void SetName()
        {
            try
            {
                string _locCustomerName = null;
                string _locSalesInvoiceCode = null;
                if (this.Customer != null && this.Vehicle != null)
                {
                    if (this.Customer.Name != null)
                    {
                        _locCustomerName = this.Customer.Name;
                    }
                    if (this.SalesInvoice.Code != null)
                    {
                        _locSalesInvoiceCode = this.SalesInvoice.Code;
                    }
                    this.Name = String.Format("{0} ({1})", _locCustomerName, _locSalesInvoiceCode).Trim();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = Location ", ex.ToString());
            }
        }

        private void SetTotalAmountCollection()
        {
            try
            {
                _globFunc = new GlobalFunction();

                if (_amount >= 0 || _amountCN >= 0 || _amountDN >= 0)
                {
                    if (_globFunc.GetRoundingList(Session, ObjectList.RouteInvoice, FieldName.TUAmount) == true)
                    {
                        this.AmountColl = _globFunc.GetRoundUp(Session, ((this.Amount + AmountDN) - this.AmountCN), ObjectList.RouteInvoice, FieldName.TUAmount);
                    }
                    else
                    {
                        this.AmountColl = (this.Amount + AmountDN) - this.AmountCN;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = AmendDeliveryLine " + ex.ToString());
            }
        }
    }
}