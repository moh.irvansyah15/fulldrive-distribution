﻿using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;

using DevExpress.ExpressApp.Editors;
using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.ConditionalAppearance;

namespace FullDrive.Module.BusinessObjects
{
    [DefaultClassOptions]
    [DefaultProperty("Name")]
    [NavigationItem("Territory")]
    [RuleCombinationOfPropertiesIsUnique("CustomerMapUnique", DefaultContexts.Save, "Code")]
    //[ImageName("BO_Contact")]
    //[DefaultProperty("DisplayMemberNameForLookupEditorsOfThisType")]
    //[DefaultListViewOptions(MasterDetailMode.ListViewOnly, false, NewItemRowPosition.None)]
    //[Persistent("DatabaseTableName")]
    // Specify more UI options using a declarative approach (https://documentation.devexpress.com/#eXpressAppFramework/CustomDocument112701).
    public class CustomerMap : BaseObject
    { // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).

        private string _code;
        private Company _company;
        private Workplace _workplace;
        private string _name;
        private PriceGroup _priceGroup;
        private SalesArea _salesArea;
        private SalesAreaLine _salesAreaLine;
        private TerritoryLevel1 _territoryLevel1;
        private TerritoryLevel2 _territoryLevel2;
        private TerritoryLevel3 _territoryLevel3;
        private TerritoryLevel4 _territoryLevel4;
        private XPCollection<BusinessPartner> _availableCustomer;
        private BusinessPartner _customer;
        private bool _active;
        private GlobalFunction _globFunc;
        //AuditTrail
        private XPCollection<AuditDataItemPersistent> changeHistory;

        public CustomerMap(Session session)
            : base(session)
        {
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if (!IsLoading)
            {
                if (Session.IsNewObject(this))
                {
                    _globFunc = new GlobalFunction();
                    this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.CustomerMap);
                }
            }
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).
        }

        #region Field

        [RuleRequiredField(DefaultContexts.Save)]
        [Appearance("CustomerMapCodeClose", Enabled = false)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue("Code", ref _code, value); }
        }

        [ImmediatePostData()]
        [RuleRequiredField(DefaultContexts.Save)]
        [DataSourceCriteria("Active = true")]
        [Appearance("CustomerMapCompanyClose", Enabled = false)]
        public Company Company
        {
            get { return _company; }
            set { SetPropertyValue("Company", ref _company, value); }
        }

        [ImmediatePostData()]
        [RuleRequiredField(DefaultContexts.Save)]
        [DataSourceCriteria("Active = true")]
        [Appearance("CustomerMapWorkplaceClose", Enabled = false)]
        public Workplace Workplace
        {
            get { return _workplace; }
            set { SetPropertyValue("Workplace", ref _workplace, value); }
        }

        [RuleRequiredField(DefaultContexts.Save)]
        public string Name
        {
            get { return _name; }
            set { SetPropertyValue("Name", ref _name, value); }
        }

        [ImmediatePostData()]
        public PriceGroup PriceGroup
        {
            get { return _priceGroup; }
            set { SetPropertyValue("PriceGroup", ref _priceGroup, value); }
        }

        [ImmediatePostData()]
        public SalesArea SalesArea
        {
            get { return _salesArea; }
            set { SetPropertyValue("SalesArea", ref _salesArea, value); }
        }

        [ImmediatePostData()]
        [RuleRequiredField(DefaultContexts.Save)]
        [Association("SalesAreaLine-CustomerMaps")]
        public SalesAreaLine SalesAreaLine
        {
            get { return _salesAreaLine; }
            set
            {
                SetPropertyValue("SalesAreaLine", ref _salesAreaLine, value);
                if (!IsLoading)
                {
                    if (this._salesAreaLine != null)
                    {
                        if (this._salesAreaLine.Company != null)
                        {
                            this.Company = this._salesAreaLine.Company;
                        }
                        if (this._salesAreaLine.Workplace != null)
                        {
                            this.Workplace = this._salesAreaLine.Workplace;
                        }
                        if(this._salesAreaLine.SalesArea != null)
                        {
                            this.SalesArea = this._salesAreaLine.SalesArea;
                        }
                        if (this._salesAreaLine.TerritoryLevel1 != null)
                        {
                            this.TerritoryLevel1 = this._salesAreaLine.TerritoryLevel1;
                        }
                        if (this._salesAreaLine.TerritoryLevel2 != null)
                        {
                            this.TerritoryLevel2 = this._salesAreaLine.TerritoryLevel2;
                        }
                        if (this._salesAreaLine.TerritoryLevel3 != null)
                        {
                            this.TerritoryLevel3 = this._salesAreaLine.TerritoryLevel3;
                        }
                        if (this._salesAreaLine.TerritoryLevel4 != null)
                        {
                            this.TerritoryLevel4 = this._salesAreaLine.TerritoryLevel4;
                        }
                    }
                }
            }
        }

        [ImmediatePostData()]
        public TerritoryLevel1 TerritoryLevel1
        {
            get { return _territoryLevel1; }
            set { SetPropertyValue("TerritoryLevel1", ref _territoryLevel1, value); }
        }

        [DataSourceCriteria("Active = true And TerritoryLevel1 = '@This.TerritoryLevel1'"), ImmediatePostData()]
        public TerritoryLevel2 TerritoryLevel2
        {
            get { return _territoryLevel2; }
            set { SetPropertyValue("TerritoryLevel2", ref _territoryLevel2, value); }
        }

        [DataSourceCriteria("Active = true And TerritoryLevel2 = '@This.TerritoryLevel2'"), ImmediatePostData()]
        public TerritoryLevel3 TerritoryLevel3
        {
            get { return _territoryLevel3; }
            set { SetPropertyValue("TerritoryLevel3", ref _territoryLevel3, value); }
        }

        [DataSourceCriteria("Active = true And TerritoryLevel3 = '@This.TerritoryLevel3'"), ImmediatePostData()]
        public TerritoryLevel4 TerritoryLevel4
        {
            get { return _territoryLevel4; }
            set { SetPropertyValue("TerritoryLevel4", ref _territoryLevel4, value); }
        }

        [Browsable(false)]
        public XPCollection<BusinessPartner> AvailableCustomer
        {

            get
            {
                if (this.Company != null && this.Workplace != null && this.PriceGroup != null && 
                    this.TerritoryLevel1 != null && this.TerritoryLevel2 != null && this.TerritoryLevel3 != null && this.TerritoryLevel4 != null)
                {
                    XPCollection<BusinessPartner> _locCustomers = new XPCollection<BusinessPartner>(Session,
                                                        new GroupOperator(GroupOperatorType.And,
                                                        new BinaryOperator("Company", this.Company),
                                                        new BinaryOperator("Workplace", this.Workplace),
                                                        new BinaryOperator("PriceGroup", this.PriceGroup),
                                                        new BinaryOperator("TerritoryLevel1", this.TerritoryLevel1),
                                                        new BinaryOperator("TerritoryLevel2", this.TerritoryLevel2),
                                                        new BinaryOperator("TerritoryLevel3", this.TerritoryLevel3),
                                                        new BinaryOperator("TerritoryLevel4", this.TerritoryLevel4),
                                                        new BinaryOperator("Active", true)));

                    if (_locCustomers != null && _locCustomers.Count() > 0)
                    {
                        _availableCustomer = _locCustomers;
                    }
                }
                else if (this.Company != null && this.Workplace != null && this.PriceGroup != null &&
                    this.TerritoryLevel1 != null && this.TerritoryLevel2 != null && this.TerritoryLevel3 != null && this.TerritoryLevel4 == null)
                {
                    XPCollection<BusinessPartner> _locCustomers = new XPCollection<BusinessPartner>(Session,
                                                        new GroupOperator(GroupOperatorType.And,
                                                        new BinaryOperator("Company", this.Company),
                                                        new BinaryOperator("Workplace", this.Workplace),
                                                        new BinaryOperator("PriceGroup", this.PriceGroup),
                                                        new BinaryOperator("TerritoryLevel1", this.TerritoryLevel1),
                                                        new BinaryOperator("TerritoryLevel2", this.TerritoryLevel2),
                                                        new BinaryOperator("TerritoryLevel3", this.TerritoryLevel3),
                                                        new BinaryOperator("Active", true)));

                    if (_locCustomers != null && _locCustomers.Count() > 0)
                    {
                        _availableCustomer = _locCustomers;
                    }
                }
                else if (this.Company != null && this.Workplace != null && this.PriceGroup != null &&
                    this.TerritoryLevel1 != null && this.TerritoryLevel2 != null && this.TerritoryLevel3 == null && this.TerritoryLevel4 == null)
                {
                    XPCollection<BusinessPartner> _locCustomers = new XPCollection<BusinessPartner>(Session,
                                                        new GroupOperator(GroupOperatorType.And,
                                                        new BinaryOperator("Company", this.Company),
                                                        new BinaryOperator("Workplace", this.Workplace),
                                                        new BinaryOperator("PriceGroup", this.PriceGroup),
                                                        new BinaryOperator("TerritoryLevel1", this.TerritoryLevel1),
                                                        new BinaryOperator("TerritoryLevel2", this.TerritoryLevel2),
                                                        new BinaryOperator("Active", true)));

                    if (_locCustomers != null && _locCustomers.Count() > 0)
                    {
                        _availableCustomer = _locCustomers;
                    }
                }else if (this.Company != null && this.Workplace != null && this.PriceGroup != null &&
                    this.TerritoryLevel1 != null && this.TerritoryLevel2 == null && this.TerritoryLevel3 == null && this.TerritoryLevel4 == null)
                {
                    XPCollection<BusinessPartner> _locCustomers = new XPCollection<BusinessPartner>(Session,
                                                        new GroupOperator(GroupOperatorType.And,
                                                        new BinaryOperator("Company", this.Company),
                                                        new BinaryOperator("Workplace", this.Workplace),
                                                        new BinaryOperator("PriceGroup", this.PriceGroup),
                                                        new BinaryOperator("TerritoryLevel1", this.TerritoryLevel1),
                                                        new BinaryOperator("Active", true)));

                    if (_locCustomers != null && _locCustomers.Count() > 0)
                    {
                        _availableCustomer = _locCustomers;
                    }
                }else if (this.Company != null && this.Workplace != null && this.PriceGroup != null &&
                    this.TerritoryLevel1 == null && this.TerritoryLevel2 == null && this.TerritoryLevel3 == null && this.TerritoryLevel4 == null)
                {
                    XPCollection<BusinessPartner> _locCustomers = new XPCollection<BusinessPartner>(Session,
                                                        new GroupOperator(GroupOperatorType.And,
                                                        new BinaryOperator("Company", this.Company),
                                                        new BinaryOperator("Workplace", this.Workplace),
                                                        new BinaryOperator("PriceGroup", this.PriceGroup),
                                                        new BinaryOperator("Active", true)));

                    if (_locCustomers != null && _locCustomers.Count() > 0)
                    {
                        _availableCustomer = _locCustomers;
                    }
                }else
                {
                    XPCollection<BusinessPartner> _locCustomers = new XPCollection<BusinessPartner>(Session,
                                                        new GroupOperator(GroupOperatorType.And,
                                                        new BinaryOperator("Company", this.Company),
                                                        new BinaryOperator("Workplace", this.Workplace),
                                                        new BinaryOperator("Active", true)));

                    if (_locCustomers != null && _locCustomers.Count() > 0)
                    {
                        _availableCustomer = _locCustomers;
                    }
                }

                return _availableCustomer;

            }
        }

        [ImmediatePostData()]
        [RuleRequiredField(DefaultContexts.Save)]
        [DataSourceProperty("AvailableCustomer", DataSourcePropertyIsNullMode.SelectAll)]
        public BusinessPartner Customer
        {
            get { return _customer; }
            set {
                SetPropertyValue("Customer", ref _customer, value);
                if(!IsLoading)
                {
                    if(this._customer != null)
                    {
                        if(this._customer.Name != null)
                        {
                            this.Name = this._customer.Name;
                        }
                    }
                }
            }
        }

        public bool Active
        {
            get { return _active; }
            set { SetPropertyValue("Active", ref _active, value); }
        }

        //AuditTrail
        [CollectionOperationSet(AllowAdd = false, AllowRemove = true)]
        public XPCollection<AuditDataItemPersistent> ChangeHistory
        {
            get
            {
                if (changeHistory == null)
                {
                    changeHistory = AuditedObjectWeakReference.GetAuditTrail(Session, this);
                }
                return changeHistory;
            }
        }

        #endregion Field

    }
}