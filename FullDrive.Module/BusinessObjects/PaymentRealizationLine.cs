﻿using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;

using DevExpress.ExpressApp.ConditionalAppearance;
using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.Xpo;
using System.Collections;
using System.IO;

namespace FullDrive.Module.BusinessObjects
{
    [DefaultClassOptions]
    [DefaultProperty("Code")]
    [NavigationItem("Cash Advance")]
    [RuleCombinationOfPropertiesIsUnique("PaymentRealizationLineRuleUnique", DefaultContexts.Save, "Code")]
    //[ImageName("BO_Contact")]
    //[DefaultProperty("DisplayMemberNameForLookupEditorsOfThisType")]
    //[DefaultListViewOptions(MasterDetailMode.ListViewOnly, false, NewItemRowPosition.None)]
    //[Persistent("DatabaseTableName")]
    // Specify more UI options using a declarative approach (https://documentation.devexpress.com/#eXpressAppFramework/CustomDocument112701).
    public class PaymentRealizationLine : BaseObject
    { // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).
        private bool _activationPosting;
        private string _code;
        private bool _select;
        #region InitialOrganization
        private Company _company;
        private Workplace _workplace;
        private Division _division;
        private Department _department;
        private Section _section;
        private Employee _employee;
        private Division _divisionCA;
        private Department _departmentCA;
        private Section _sectionCA;
        private Employee _employeeCA;
        #endregion InitialOrganization
        private Position _position;
        private string _name;
        private CashAdvanceType _cashAdvanceType;
        private FileData _attachment;
        private DateTime _uploadDate;
        private double _amount;
        private double _amountRealized;
        private double _amountRefund;
        private double _amountOutstanding;
        private string _description;
        private DateTime _docDate;
        private int _journalMonth;
        private int _journalYear;
        private PaymentStatus _paymentStatus;
        private DateTime _paymentStatusDate;
        private Status _status;
        private DateTime _statusDate;
        private string _signCode;
        private CashAdvance _cashAdvance;
        private CashAdvanceMonitoring _cashAdvanceMonitoring;
        private PaymentRealization _paymentRealization;
        private string _localUserAccess;
        private GlobalFunction _globFunc;
        //AuditTrail
        private XPCollection<AuditDataItemPersistent> changeHistory;

        public PaymentRealizationLine(Session session)
            : base(session)
        {
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if(!IsLoading)
            {
                if(Session.IsNewObject(this))
                {
                    _globFunc = new GlobalFunction();
                    DateTime now = DateTime.Now;
                    #region UserAccess
                    _localUserAccess = SecuritySystem.CurrentUserName;
                    UserAccess _locUserAccess = Session.FindObject<UserAccess>(new BinaryOperator("UserName", _localUserAccess));
                    if (_locUserAccess != null)
                    {
                        if (_locUserAccess.Employee != null)
                        {
                            if (_locUserAccess.Employee.Company != null && _locUserAccess.Employee.Workplace != null)
                            {
                                this.Code = _globFunc.GetNumberingUnlockOptimisticRecordBasedOrganization(this.Session.DataLayer, ObjectList.PaymentRealizationLine, _locUserAccess.Employee.Company, _locUserAccess.Employee.Workplace);
                            }
                            else
                            {
                                this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.PaymentRealizationLine);
                            }
                        }
                    }
                    #endregion UserAccess
                    this.Select = true;
                    this.Status = Status.Open;
                    this.StatusDate = now;
                    this.PaymentStatus = PaymentStatus.Settled;
                    this.PaymentStatusDate = now;
                }               
            }
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).
        }

        #region Field

        [Browsable(false)]
        [ImmediatePostData()]
        public bool ActivationPosting
        {
            get { return _activationPosting; }
            set { SetPropertyValue("ActivationPosting", ref _activationPosting, value); }
        }

        [RuleRequiredField(DefaultContexts.Save)]
        [Appearance("PaymentRealizationLineCodeClose", Enabled = false)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue("Code", ref _code, value); }
        }

        [Appearance("PaymentRealizationLineSelectClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public bool Select
        {
            get { return _select; }
            set { SetPropertyValue("Select", ref _select, value); }
        }

        #region Organization

        [DataSourceCriteria("Active = true"), ImmediatePostData()]
        [Appearance("PaymentRealizationLineCompanyEnabled", Enabled = false)]
        public Company Company
        {
            get { return _company; }
            set { SetPropertyValue("Company", ref _company, value); }
        }

        [DataSourceCriteria("Company = '@This.Company' And Active = true"), ImmediatePostData()]
        [Appearance("PaymentRealizationLineWorkplaceEnabled", Enabled = false)]
        public Workplace Workplace
        {
            get { return _workplace; }
            set { SetPropertyValue("Workplace", ref _workplace, value); }
        }

        [Browsable(false)]
        [Appearance("PaymentRealizationLineDivisionEnabled", Enabled = false)]
        public Division Division
        {
            get { return _division; }
            set { SetPropertyValue("Division", ref _division, value); }
        }

        [Browsable(false)]
        [Appearance("PaymentRealizationLineDepartmentEnabled", Enabled = false)]
        public Department Department
        {
            get { return _department; }
            set { SetPropertyValue("Department", ref _department, value); }
        }

        [Browsable(false)]
        [Appearance("PaymentRealizationLineSectionEnabled", Enabled = false)]
        public Section Section
        {
            get { return _section; }
            set { SetPropertyValue("Section", ref _section, value); }
        }

        [Browsable(false)]
        [Appearance("PaymentRealizationLineEmployeeClose", Enabled = false)]
        public Employee Employee
        {
            get { return _employee; }
            set {
                SetPropertyValue("Employee", ref _employee, value);
                if (!IsLoading)
                {
                    UpdateName();
                }
            }
        }

        [DataSourceCriteria("Company = '@This.Company' And Active = true "), ImmediatePostData()]
        [Appearance("PaymentRealizationLineDivisionCAClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Division DivisionCA
        {
            get { return _divisionCA; }
            set { SetPropertyValue("DivisionCA", ref _divisionCA, value); }
        }

        [DataSourceCriteria("Company = '@This.Company' And Division = '@This.DivisionCA' And Active = true"), ImmediatePostData()]
        [Appearance("PaymentRealizationLineDepartmentCAClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Department DepartmentCA
        {
            get { return _departmentCA; }
            set { SetPropertyValue("DepartmentCA", ref _departmentCA, value); }
        }

        [DataSourceCriteria("Company = '@This.Company' And Department = '@This.DepartmentCA' And Active = true "), ImmediatePostData()]
        [Appearance("PaymentRealizationLineSectionCAClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Section SectionCA
        {
            get { return _sectionCA; }
            set { SetPropertyValue("SectionCA", ref _sectionCA, value); }
        }

        [ImmediatePostData()]
        [DataSourceCriteria("Company = '@This.Company' And Workplace = '@This.Workplace' And Division = '@This.DivisionCA' And Department = '@This.DepartmentCA' And Section = '@This.SectionCA' And Active = true ")]
        [Appearance("PaymentRealizationLineEmployeeCAClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Employee EmployeeCA
        {
            get { return _employeeCA; }
            set
            {
                SetPropertyValue("EmployeeCA", ref _employeeCA, value);
                if (!IsLoading)
                {
                    if (this._employeeCA != null)
                    {
                        if (this._employeeCA.Position != null)
                        {
                            this.Position = this._employeeCA.Position;
                        }
                    }
                }
            }
        }

        #endregion Organization

        [Appearance("PaymentRealizationLinePositionClose", Enabled = false)]
        public Position Position
        {
            get { return _position; }
            set { SetPropertyValue("Position", ref _position, value); }
        }

        [Appearance("PaymentRealizationLineNameClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public string Name
        {
            get { return _name; }
            set { SetPropertyValue("Name", ref _name, value); }
        }

        [Appearance("PaymentRealizationLineCashAdvanceTypeClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public CashAdvanceType CashAdvanceType
        {
            get { return _cashAdvanceType; }
            set { SetPropertyValue("CashAdvanceType", ref _cashAdvanceType, value); }
        }

        [ImmediatePostData()]
        [Appearance("PaymentRealizationLineAttachmentRealizationClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public FileData Attachment
        {
            get { return _attachment; }
            set
            {
                SetPropertyValue("Attachment", ref _attachment, value);
                if (!IsLoading)
                {
                    if (this._attachment != null)
                    {
                        this.UploadDate = DateTime.Now;
                    }
                }
            }
        }

        [ModelDefault("DisplayFormat", "{0:dd/MM/yyyy}")]
        [ModelDefault("EditMask", "dd/MM/yyyy")]
        [Appearance("PaymentRealizationLineUploadDateClose", Enabled = false)]
        public DateTime UploadDate
        {
            get { return _uploadDate; }
            set { SetPropertyValue("UploadDate", ref _uploadDate, value); }
        }

        [Appearance("PaymentRealizationLineAmountClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public double Amount
        {
            get { return _amount; }
            set { SetPropertyValue("Amount", ref _amount, value); }
        }

        [ImmediatePostData()]
        [Appearance("PaymentRealizationLineAmountRealizedClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public double AmountRealized
        {
            get { return _amountRealized; }
            set {
                SetPropertyValue("AmountRealized", ref _amountRealized, value);
                if(!IsLoading)
                {
                    if(this._amountRealized > 0)
                    {
                        this.AmountRefund = this.Amount - this._amountRealized;
                    }
                }
            }
        }

        [ImmediatePostData()]
        [Appearance("PaymentRealizationLineAmountRefundClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public double AmountRefund
        {
            get { return _amountRefund; }
            set {
                SetPropertyValue("AmountRefund", ref _amountRefund, value);
                if(!IsLoading)
                {
                    DateTime now = DateTime.Now;
                    if (this._amountRefund > 0)
                    {
                        if((this.Amount - this.AmountRealized) - this._amountRefund > 0)
                        {
                            this.AmountOutstanding = (this.Amount - this.AmountRealized) - this._amountRefund;
                            this.PaymentStatus = PaymentStatus.Debt;
                            this.PaymentStatusDate = now;
                        }
                    }
                }
            }
        }

        [ImmediatePostData()]
        [Appearance("PaymentRealizationLineAmountOutstandingClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public double AmountOutstanding
        {
            get { return _amountOutstanding; }
            set { SetPropertyValue("AmountOutstanding", ref _amountOutstanding, value); }
        }

        [Size(512)]
        [Appearance("PaymentRealizationLineNotesClose", Criteria = "ActivationPosting = True", Enabled = false)]
        public string Description
        {
            get { return _description; }
            set { SetPropertyValue("Description", ref _description, value); }
        }

        [Browsable(false)]
        [ImmediatePostData()]
        [ModelDefault("DisplayFormat", "{0:dd/MM/yyyy}")]
        [ModelDefault("EditMask", "dd/MM/yyyy")]
        [Appearance("PaymentRealizationLineDocDateClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public DateTime DocDate
        {
            get { return _docDate; }
            set
            {
                SetPropertyValue("DocDate", ref _docDate, value);
                if (!IsLoading)
                {
                    SetAccountingPeriodic();
                }
            }
        }

        [Browsable(false)]
        public int JournalMonth
        {
            get { return _journalMonth; }
            set { SetPropertyValue("JournalMonth", ref _journalMonth, value); }
        }

        [Browsable(false)]
        public int JournalYear
        {
            get { return _journalYear; }
            set { SetPropertyValue("JournalYear", ref _journalYear, value); }
        }

        [Appearance("PaymentRealizationLinePaymentStatusClose", Enabled = false)]
        public PaymentStatus PaymentStatus
        {
            get { return _paymentStatus; }
            set { SetPropertyValue("PaymentStatus", ref _paymentStatus, value); }
        }

        [ModelDefault("DisplayFormat", "{0:dd/MM/yyyy}")]
        [ModelDefault("EditMask", "dd/MM/yyyy")]
        [Appearance("PaymentRealizationLinePaymentStatusDateClose", Enabled = false)]
        public DateTime PaymentStatusDate
        {
            get { return _paymentStatusDate; }
            set { SetPropertyValue("PaymentStatusDate", ref _paymentStatusDate, value); }
        }

        [Appearance("PaymentRealizationLineStatusClose", Enabled = false)]
        public Status Status
        {
            get { return _status; }
            set { SetPropertyValue("Status", ref _status, value); }
        }

        [ModelDefault("DisplayFormat", "{0:dd/MM/yyyy}")]
        [ModelDefault("EditMask", "dd/MM/yyyy")]
        [Appearance("PaymentRealizationLineStatusDateClose", Enabled = false)]
        public DateTime StatusDate
        {
            get { return _statusDate; }
            set { SetPropertyValue("StatusDate", ref _statusDate, value); }
        }

        [Appearance("PaymentRealizationLineSignCodeEnabled", Enabled = false)]
        public string SignCode
        {
            get { return _signCode; }
            set { SetPropertyValue("SignCode", ref _signCode, value); }
        }

        [Appearance("PaymentRealizationLineCashAdvanceClose", Enabled = false)]
        public CashAdvance CashAdvance
        {
            get { return _cashAdvance; }
            set { SetPropertyValue("CashAdvance", ref _cashAdvance, value); }
        }

        [Appearance("PaymentRealizationLineCashAdvanceMonitoringEnabled", Enabled = false)]
        public CashAdvanceMonitoring CashAdvanceMonitoring
        {
            get { return _cashAdvanceMonitoring; }
            set { SetPropertyValue("CashAdvanceMonitoring", ref _cashAdvanceMonitoring, value); }
        }

        [Association("PaymentRealization-PaymentRealizationLines")]
        [Appearance("PaymentRealizationLinePaymentRealizationClose", Enabled = false)]
        public PaymentRealization PaymentRealization
        {
            get { return _paymentRealization; }
            set
            {
                SetPropertyValue("PaymentRealization", ref _paymentRealization, value);
                if (this._paymentRealization != null)
                {
                    if (this._paymentRealization.Company != null)
                    {
                        this.Company = this._paymentRealization.Company;
                    }
                    if (this._paymentRealization.Workplace != null)
                    {
                        this.Workplace = this._paymentRealization.Workplace;
                    }
                    if (this._paymentRealization.Division != null)
                    {
                        this.Division = this._paymentRealization.Division;
                    }
                    if (this._paymentRealization.Department != null)
                    {
                        this.Department = this._paymentRealization.Department;
                    }
                    if (this._paymentRealization.Section != null)
                    {
                        this.Section = this._paymentRealization.Section;
                    }
                    if (this._paymentRealization.Employee != null)
                    {
                        this.Employee = this._paymentRealization.Employee;
                    }
                    if (this._paymentRealization.DivisionCA != null)
                    {
                        this.DivisionCA = this._paymentRealization.DivisionCA;
                    }
                    if (this._paymentRealization.DepartmentCA != null)
                    {
                        this.DepartmentCA = this._paymentRealization.DepartmentCA;
                    }
                    if (this._paymentRealization.SectionCA != null)
                    {
                        this.SectionCA = this._paymentRealization.SectionCA;
                    }
                    if (this._paymentRealization.EmployeeCA != null)
                    {
                        this.EmployeeCA = this._paymentRealization.EmployeeCA;
                    }
                    if (this._paymentRealization.Position != null)
                    {
                        this.Position = this._paymentRealization.Position;
                    }
                }
            }
        }

        //AuditTrail
        [CollectionOperationSet(AllowAdd = false, AllowRemove = true)]
        public XPCollection<AuditDataItemPersistent> ChangeHistory
        {
            get
            {
                if (changeHistory == null)
                {
                    changeHistory = AuditedObjectWeakReference.GetAuditTrail(Session, this);
                }
                return changeHistory;
            }
        }

        #endregion Field

        //========================================== Code Only ==========================================

        #region UpdateName

        private void UpdateName()
        {
            try
            {
                this._name = string.Format("PaymentRealizationLine [ {0} ]", Employee.Name);
            }

            catch (Exception ex)
            {
                Tracing.Tracer.LogError("BusinessObject = PaymentRealizationLine" + ex.ToString());
            }
        }

        #endregion UpdateName

        private void SetAccountingPeriodic()
        {
            try
            {
                DateTime now = DateTime.Now;
                if (this.Company != null && this.Workplace != null)
                {
                    AccountingPeriodic _locAcctPeriodic = Session.FindObject<AccountingPeriodic>
                                                        (new GroupOperator(GroupOperatorType.And,
                                                        new BinaryOperator("Company", this.Company),
                                                        new BinaryOperator("Workplace", this.Workplace),
                                                        new BinaryOperator("Active", true)));
                    if (_locAcctPeriodic != null)
                    {
                        this.JournalMonth = _locAcctPeriodic.Month;
                        this.JournalYear = _locAcctPeriodic.Year;
                    }
                    else
                    {
                        this.JournalMonth = 0;
                        this.JournalYear = 0;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Business Object = PaymentRealization ", ex.ToString());
            }
        }

    }
}