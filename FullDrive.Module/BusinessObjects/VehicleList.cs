﻿using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;

using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.ConditionalAppearance;


namespace FullDrive.Module.BusinessObjects
{
    [DefaultClassOptions]
    [NavigationItem("Transportation")]
    [DefaultProperty("Name")]
    [RuleCombinationOfPropertiesIsUnique("VehicleListRuleUnique", DefaultContexts.Save, "Code")]
    //[ImageName("BO_Contact")]
    //[DefaultProperty("DisplayMemberNameForLookupEditorsOfThisType")]
    //[DefaultListViewOptions(MasterDetailMode.ListViewOnly, false, NewItemRowPosition.None)]
    //[Persistent("DatabaseTableName")]
    // Specify more UI options using a declarative approach (https://documentation.devexpress.com/#eXpressAppFramework/CustomDocument112701).
    public class VehicleList : BaseObject
    { // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).

        private string _code;
        private Company _company;
        private Workplace _workplace;
        private string _name;
        private XPCollection<Vehicle> _availableRegVehicle;
        private Vehicle _regVehicle;
        private Vehicle _vehicle;
        private GlobalFunction _globFunc;

        public VehicleList(Session session)
            : base(session)
        {
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if (!IsLoading)
            {
                _globFunc = new GlobalFunction();
                this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.VehicleList);
            }
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).
        }

        #region Field

        [RuleRequiredField(DefaultContexts.Save)]
        [Appearance("VehicleListCodeEnabled", Enabled = false)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue("Code", ref _code, value); }
        }

        [RuleRequiredField(DefaultContexts.Save)]
        [Appearance("VehicleListCompanyEnabled", Enabled = false)]
        public Company Company
        {
            get { return _company; }
            set { SetPropertyValue("Company", ref _company, value); }
        }

        [RuleRequiredField(DefaultContexts.Save)]
        [Appearance("VehicleListWorkplaceEnabled", Enabled = false)]
        public Workplace Workplace
        {
            get { return _workplace; }
            set { SetPropertyValue("Workplace", ref _workplace, value); }
        }

        public string Name
        {
            get { return _name; }
            set { SetPropertyValue("Name", ref _name, value); }
        }

        [Browsable(false)]
        public XPCollection<Vehicle> AvailableRegVehicle
        {
            get
            {

                if (this.Company != null && this.Workplace != null)
                {
                    XPCollection<Vehicle> _locRegVehicles = new XPCollection<Vehicle>
                                                            (Session, new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("Company", this.Company),
                                                            new BinaryOperator("Workplace", this.Workplace),
                                                            new BinaryOperator("VehicleType", VehicleType.Real),
                                                            new BinaryOperator("Active", true)));
                    if (_locRegVehicles != null && _locRegVehicles.Count() > 0)
                    {
                        _availableRegVehicle = _locRegVehicles;
                    }
                }
                else
                {
                    XPCollection<Vehicle> _locRegVehicles = new XPCollection<Vehicle>(Session, new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("VehicleType", VehicleType.Real),
                                                            new BinaryOperator("Active", true)));
                    if(_locRegVehicles != null && _locRegVehicles.Count() > 0)
                    {
                        _availableRegVehicle = _locRegVehicles;
                    }
                }

                return _availableRegVehicle;

            }
        }

        [ImmediatePostData()]
        [DataSourceProperty("AvailableRegVehicle", DataSourcePropertyIsNullMode.SelectAll)]
        public Vehicle RegVehicle
        {
            get { return _regVehicle; }
            set {
                SetPropertyValue("RegVehicle", ref _regVehicle, value);
                if(!IsLoading)
                {
                    if(this._regVehicle != null)
                    {
                        if(this._regVehicle.Name != null)
                        {
                            this.Name = this._regVehicle.Name;
                        }
                    }
                }
            }
        }

        [ImmediatePostData()]
        [RuleRequiredField(DefaultContexts.Save)]
        [Appearance("VehicleListVehicleEnabled", Enabled = false)]
        [Association("Vehicle-VehicleLists")]
        public Vehicle Vehicle
        {
            get { return _vehicle; }
            set {
                SetPropertyValue("Vehicle", ref _vehicle, value);
                if(!IsLoading)
                {
                    if(this._vehicle != null)
                    {
                        if(this._vehicle.Company != null)
                        {
                            this.Company = this._vehicle.Company;
                        }
                        if (this._vehicle.Workplace != null)
                        {
                            this.Workplace = this._vehicle.Workplace;
                        }
                    }
                }
            }
        }

        #endregion Field

    }
}