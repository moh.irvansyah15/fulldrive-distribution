﻿using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;

using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.ConditionalAppearance;

namespace FullDrive.Module.BusinessObjects
{
    [DefaultClassOptions]
    [DefaultProperty("Code")]
    [NavigationItem("Setup")]
    [RuleCombinationOfPropertiesIsUnique("MailSetupDetailRuleUnique", DefaultContexts.Save, "Code")]
    //[ImageName("BO_Contact")]
    //[DefaultProperty("DisplayMemberNameForLookupEditorsOfThisType")]
    //[DefaultListViewOptions(MasterDetailMode.ListViewOnly, false, NewItemRowPosition.None)]
    //[Persistent("DatabaseTableName")]
    // Specify more UI options using a declarative approach (https://documentation.devexpress.com/#eXpressAppFramework/CustomDocument112701).
    public class MailSetupDetail : BaseObject
    { // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).
        private string _code;
        private string _name;
        private UserAccess _userAccess;
        private Company _company;
        private Division _division;
        private Department _department;
        private Section _section;
        private ObjectList _objectList;
        private string _smtpHost;
        private int _smtpPort;
        private string _mailFrom;
        private string _mailFromPassword;
        private string _mailTo;
        private string _mailSubject;
        private string _messageBody;
        private bool _active;
        private ApplicationSetup _applicationSetup;
        private string _userAccessIn;
        private GlobalFunction _globFunc;
        //AuditTrail
        private XPCollection<AuditDataItemPersistent> changeHistory;

        public MailSetupDetail(Session session)
            : base(session)
        {
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if (!IsLoading)
            {
                _globFunc = new GlobalFunction();
                this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.MailSetupDetail);
                _userAccessIn = SecuritySystem.CurrentUserName;
                UserAccess _locUserAccess = Session.FindObject<UserAccess>(new BinaryOperator("UserName", _userAccessIn));

                if (_locUserAccess != null)
                {
                    if (_locUserAccess.Employee != null)
                    {
                        if (_locUserAccess.Employee.Company != null)
                        {
                            this.Company = _locUserAccess.Employee.Company;
                        }
                    }
                }
            }
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).
        }

        [RuleRequiredField(DefaultContexts.Save)]
        [Appearance("MailSetupDetailCodeEnabled", Enabled = false)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue("Code", ref _code, value); }
        }

        public string Name
        {
            get { return _name; }
            set { SetPropertyValue("Name", ref _name, value); }
        }

        public UserAccess UserAccess
        {
            get { return _userAccess; }
            set { SetPropertyValue("UserAccess", ref _userAccess, value); }
        }

        public Company Company
        {
            get { return _company; }
            set { SetPropertyValue("Company", ref _company, value); }
        }

        public Division Division
        {
            get { return _division; }
            set { SetPropertyValue("Division", ref _division, value); }
        }

        public Department Department
        {
            get { return _department; }
            set { SetPropertyValue("Department", ref _department, value); }
        }

        public Section Section
        {
            get { return _section; }
            set { SetPropertyValue("Section", ref _section, value); }
        }

        public ObjectList ObjectList
        {
            get { return _objectList; }
            set { SetPropertyValue("ObjectList", ref _objectList, value); }
        }

        public string SmtpHost
        {
            get { return _smtpHost; }
            set { SetPropertyValue("SmtpHost", ref _smtpHost, value); }
        }

        public int SmtpPort
        {
            get { return _smtpPort; }
            set { SetPropertyValue("SmtpPort", ref _smtpPort, value); }
        }

        public string MailFrom
        {
            get { return _mailFrom; }
            set { SetPropertyValue("MailFrom", ref _mailFrom, value); }
        }

        public string MailFromPassword
        {
            get { return _mailFromPassword; }
            set { SetPropertyValue("MailFromPassword", ref _mailFromPassword, value); }
        }

        public string MailTo
        {
            get { return _mailTo; }
            set { SetPropertyValue("MailTo", ref _mailTo, value); }
        }

        public string MailSubject
        {
            get { return _mailSubject; }
            set { SetPropertyValue("MailSubject", ref _mailSubject, value); }
        }

        public string MessageBody
        {
            get { return _messageBody; }
            set { SetPropertyValue("MessageBody", ref _messageBody, value); }
        }

        public bool Active
        {
            get { return _active; }
            set { SetPropertyValue("Active", ref _active, value); }
        }

        [Appearance("MailSetupDetailApplicationSetupDetailEnabled", Enabled = false)]
        [RuleRequiredField(DefaultContexts.Save)]
        [Association("ApplicationSetup-MailSetupDetails")]
        public ApplicationSetup ApplicationSetup
        {
            get { return _applicationSetup; }
            set { SetPropertyValue("ApplicationSetup", ref _applicationSetup, value); }
        }

        //AuditTrail
        [CollectionOperationSet(AllowAdd = false, AllowRemove = true)]
        public XPCollection<AuditDataItemPersistent> ChangeHistory
        {
            get
            {
                if (changeHistory == null)
                {
                    changeHistory = AuditedObjectWeakReference.GetAuditTrail(Session, this);
                }
                return changeHistory;
            }
        }

        //private string _PersistentProperty;
        //[XafDisplayName("My display name"), ToolTip("My hint message")]
        //[ModelDefault("EditMask", "(000)-00"), Index(0), VisibleInListView(false)]
        //[Persistent("DatabaseColumnName"), RuleRequiredField(DefaultContexts.Save)]
        //public string PersistentProperty {
        //    get { return _PersistentProperty; }
        //    set { SetPropertyValue("PersistentProperty", ref _PersistentProperty, value); }
        //}

        //[Action(Caption = "My UI Action", ConfirmationMessage = "Are you sure?", ImageName = "Attention", AutoCommit = true)]
        //public void ActionMethod() {
        //    // Trigger a custom business logic for the current record in the UI (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112619.aspx).
        //    this.PersistentProperty = "Paid";
        //}
    }
}