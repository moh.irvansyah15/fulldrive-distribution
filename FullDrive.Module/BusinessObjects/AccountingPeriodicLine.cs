﻿using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;

using DevExpress.ExpressApp.ConditionalAppearance;
using DevExpress.ExpressApp.SystemModule;
using FullDrive.Module.CustomProcess;

namespace FullDrive.Module.BusinessObjects
{
    [DefaultClassOptions]
    [NavigationItem("Finance & Accounting")]
    [DefaultProperty("Code")]
    [RuleCombinationOfPropertiesIsUnique("AccountingPeriodicLineRuleUnique", DefaultContexts.Save, "Code")]
    //[ImageName("BO_Contact")]
    //[DefaultProperty("DisplayMemberNameForLookupEditorsOfThisType")]
    //[DefaultListViewOptions(MasterDetailMode.ListViewOnly, false, NewItemRowPosition.None)]
    //[Persistent("DatabaseTableName")]
    // Specify more UI options using a declarative approach (https://documentation.devexpress.com/#eXpressAppFramework/CustomDocument112701).
    public class AccountingPeriodicLine : BaseObject
    { // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).
        private int _no;
        private string _code;
        #region InitialOrganization
        private Company _company;
        private Workplace _workplace;
        private Division _division;
        private Department _department;
        private Section _section;
        private Employee _employee;
        #endregion InitialOrganization
        private ChartOfAccount _accountNo;
        private double _balance;
        private double _balHold;
        private double _debit;
        private double _credit;
        private DateTime _startDate;
        private DateTime _endDate;
        private int _month;
        private int _year;
        private DateTime _closingDate;
        private bool _active;
        private AccountingPeriodic _accountingPeriodic;
        private string _localUserAccess;
        private GlobalFunction _globFunc;
        //AuditTrail
        private XPCollection<AuditDataItemPersistent> changeHistory;

        public AccountingPeriodicLine(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if (!IsLoading)
            {
                if (Session.IsNewObject(this))
                {
                    _globFunc = new GlobalFunction();
                    DateTime now = DateTime.Now;
                    #region UserAccess
                    _localUserAccess = SecuritySystem.CurrentUserName;
                    UserAccess _locUserAccess = Session.FindObject<UserAccess>(new BinaryOperator("UserName", _localUserAccess));
                    if (_locUserAccess != null)
                    {
                        if (_locUserAccess.Employee != null)
                        {
                            if (_locUserAccess.Employee.Company != null && _locUserAccess.Employee.Workplace != null)
                            {
                                this.Code = _globFunc.GetNumberingUnlockOptimisticRecordBasedOrganization(this.Session.DataLayer, ObjectList.AccountingPeriodicLine, _locUserAccess.Employee.Company, _locUserAccess.Employee.Workplace);
                            }
                            else
                            {
                                this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.AccountingPeriodicLine);
                            }
                        }
                    }
                    #endregion UserAccess
                }
            }
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).
        }

        protected override void OnSaving()
        {
            base.OnSaving();
            if (!IsLoading && IsSaving)
            {
                UpdateNo();
            }
        }

        protected override void OnDeleting()
        {
            base.OnDeleting();
            if (!IsLoading)
            {
                RecoveryDeleteNo();
            }
        }

        #region Field

        [Appearance("AccountingPeriodicLineNoEnabled", Enabled = false)]
        public int No
        {
            get { return _no; }
            set { SetPropertyValue("No", ref _no, value); }
        }
        
        [RuleRequiredField(DefaultContexts.Save)]
        [Appearance("AccountingPeriodicLineCodeEnabled", Enabled = false)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue("Code", ref _code, value); }
        }

        #region Organization
        
        [Appearance("AccountingPeriodicLineCompanyClose", Enabled = false)]
        public Company Company
        {
            get { return _company; }
            set { SetPropertyValue("Company", ref _company, value); }
        }
        
        [Appearance("AccountingPeriodicLineWorkplaceClose", Enabled = false)]
        public Workplace Workplace
        {
            get { return _workplace; }
            set { SetPropertyValue("Workplace", ref _workplace, value); }
        }

        [Browsable(false)]
        public Division Division
        {
            get { return _division; }
            set { SetPropertyValue("Division", ref _division, value); }
        }

        [Browsable(false)]
        public Department Department
        {
            get { return _department; }
            set { SetPropertyValue("Department", ref _department, value); }
        }

        [Browsable(false)]
        public Section Section
        {
            get { return _section; }
            set { SetPropertyValue("Section", ref _section, value); }
        }

        [Browsable(false)]
        public Employee Employee
        {
            get { return _employee; }
            set { SetPropertyValue("Employee", ref _employee, value); }
        }

        #endregion Organization

        public ChartOfAccount AccountNo
        {
            get { return _accountNo; }
            set { SetPropertyValue("AccountNo", ref _accountNo, value); }
        }

        public double Balance
        {
            get { return _balance; }
            set { SetPropertyValue("Balance", ref _balance, value); }
        }

        public double BalHold
        {
            get { return _balHold; }
            set { SetPropertyValue("BalHold", ref _balHold, value); }
        }

        public double Debit
        {
            get { return _debit; }
            set { SetPropertyValue("Debit", ref _debit, value); }
        }

        public double Credit
        {
            get { return _credit; }
            set { SetPropertyValue("Credit", ref _credit, value); }
        }

        [ImmediatePostData()]
        [ModelDefault("DisplayFormat", "{0:dd/MM/yyyy}")]
        [ModelDefault("EditMask", "dd/MM/yyyy")]
        public DateTime StartDate
        {
            get { return _startDate; }
            set
            {
                SetPropertyValue("StartDate", ref _startDate, value);
                if (!IsLoading)
                {
                    this.Month = this._startDate.Month;
                    this.Year = this._startDate.Year;
                }
            }
        }

        [ModelDefault("DisplayFormat", "{0:dd/MM/yyyy}")]
        [ModelDefault("EditMask", "dd/MM/yyyy")]
        public DateTime EndDate
        {
            get { return _endDate; }
            set { SetPropertyValue("EndDate", ref _endDate, value); }
        }

        [Appearance("AccountingPeriodicMonthClose", Enabled = false)]
        public int Month
        {
            get { return _month; }
            set { SetPropertyValue("Month", ref _month, value); }
        }

        [Appearance("AccountingPeriodicLineYearClose", Enabled = false)]
        public int Year
        {
            get { return _year; }
            set { SetPropertyValue("Year", ref _year, value); }
        }

        [ModelDefault("DisplayFormat", "{0:dd/MM/yyyy}")]
        [ModelDefault("EditMask", "dd/MM/yyyy")]
        public DateTime ClosingDate
        {
            get { return _closingDate; }
            set { SetPropertyValue("ClosingDate", ref _closingDate, value); }
        }

        public bool Active
        {
            get { return _active; }
            set { SetPropertyValue("Active", ref _active, value); }
        }

        [ImmediatePostData()]
        [RuleRequiredField(DefaultContexts.Save)]
        [Association("AccountingPeriodic-AccountingPeriodicLines")]
        [Appearance("AccountingPeriodicLineAccountingPeriodicEnabled", Enabled = false)]
        public AccountingPeriodic AccountingPeriodic
        {
            get { return _accountingPeriodic; }
            set
            {
                SetPropertyValue("AccountingPeriodic", ref _accountingPeriodic, value);
                if(!IsLoading)
                {
                    if(this._accountingPeriodic != null)
                    {
                        if(this._accountingPeriodic.Company != null) { this.Company = this._accountingPeriodic.Company; }
                        if (this._accountingPeriodic.Workplace != null) { this.Workplace = this._accountingPeriodic.Workplace; }
                        if (this._accountingPeriodic.Division != null) { this.Division = this._accountingPeriodic.Division; }
                        if (this._accountingPeriodic.Department != null) { this.Department = this._accountingPeriodic.Department; }
                        if (this._accountingPeriodic.Section != null) { this.Section = this._accountingPeriodic.Section; }
                        if (this._accountingPeriodic.Employee != null) { this.Employee = this._accountingPeriodic.Employee; }
                        this.StartDate = this._accountingPeriodic.StartDate;
                        this.EndDate = this._accountingPeriodic.EndDate;
                        this.Month = this._accountingPeriodic.Month;
                        this.ClosingDate = this._accountingPeriodic.ClosingDate;
                    }
                }
            }
        }

        //AuditTrail
        [CollectionOperationSet(AllowAdd = false, AllowRemove = true)]
        public XPCollection<AuditDataItemPersistent> ChangeHistory
        {
            get
            {
                if (changeHistory == null)
                {
                    changeHistory = AuditedObjectWeakReference.GetAuditTrail(Session, this);
                }
                return changeHistory;
            }
        }

        #endregion Field

        //=========================================== Code Only ===========================================

        #region No

        public void UpdateNo()
        {
            try
            {
                if (!IsLoading && Session.IsNewObject(this))
                {
                    if (this.AccountingPeriodic != null)
                    {
                        object _makRecord = Session.Evaluate<AccountingPeriodicLine>(CriteriaOperator.Parse("Max(No)"), CriteriaOperator.Parse("AccountingPeriodic=?", this.AccountingPeriodic));
                        this.No = Convert.ToInt32(_makRecord) + 1;
                        this.Save();
                        RecoveryUpdateNo();
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = AccountingPeriodicLine " + ex.ToString());
            }
        }

        public void RecoveryUpdateNo()
        {
            try
            {
                if (this.AccountingPeriodic != null)
                {
                    AccountingPeriodic _numHeader = Session.FindObject<AccountingPeriodic>
                                                (new BinaryOperator("Code", this.AccountingPeriodic.Code));

                    XPCollection<AccountingPeriodicLine> _numLines = new XPCollection<AccountingPeriodicLine>
                                                (Session, new BinaryOperator("AccountingPeriodic", _numHeader),
                                                new SortProperty("No", DevExpress.Xpo.DB.SortingDirection.Ascending));
                    if (_numLines != null)
                    {
                        int i1 = 0;
                        foreach (AccountingPeriodicLine _numLine in _numLines)
                        {
                            i1 += 1;
                            _numLine.No = i1;
                            _numLine.Save();
                        }
                        i1 = 1;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = AccountingPeriodicLine " + ex.ToString());
            }
        }

        public void RecoveryDeleteNo()
        {
            try
            {
                if (this.AccountingPeriodic != null)
                {
                    AccountingPeriodic _numHeader = Session.FindObject<AccountingPeriodic>
                                                (new BinaryOperator("Code", this.AccountingPeriodic.Code));

                    XPCollection<AccountingPeriodicLine> _numLines = new XPCollection<AccountingPeriodicLine>
                                                (Session, new GroupOperator(GroupOperatorType.And,
                                                 new BinaryOperator("This", this, BinaryOperatorType.NotEqual),
                                                 new BinaryOperator("AccountingPeriodic", _numHeader)),
                                                 new SortProperty("No", DevExpress.Xpo.DB.SortingDirection.Ascending));
                    if (_numLines != null)
                    {
                        int i = 0;
                        foreach (AccountingPeriodicLine _numLine in _numLines)
                        {
                            i += 1;
                            _numLine.No = i;
                            _numLine.Save();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = AccountingPeriodicLine " + ex.ToString());
            }
        }

        #endregion No

        
    }
}