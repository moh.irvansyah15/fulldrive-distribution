﻿using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;

using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.ConditionalAppearance;

namespace FullDrive.Module.BusinessObjects
{
    [DefaultClassOptions]
    [DefaultProperty("Code")]
    [NavigationItem("Finance & Accounting")]
    [RuleCombinationOfPropertiesIsUnique("ReceivableTransactionRuleUnique", DefaultContexts.Save, "Code")]
    //[ImageName("BO_Contact")]
    //[DefaultProperty("DisplayMemberNameForLookupEditorsOfThisType")]
    //[DefaultListViewOptions(MasterDetailMode.ListViewOnly, false, NewItemRowPosition.None)]
    //[Persistent("DatabaseTableName")]
    // Specify more UI options using a declarative approach (https://documentation.devexpress.com/#eXpressAppFramework/CustomDocument112701).
    public class ReceivableTransaction : BaseObject
    { // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).
        private bool _activationPosting;
        private string _code;
        #region InitialOrganization
        private Company _company;
        private Workplace _workplace;
        private Division _division;
        private Department _department;
        private Section _section;
        private Employee _employee;
        #endregion InitialOrganization
        #region Sales
        private XPCollection<BusinessPartner> _availableSalesToCustomer;
        private BusinessPartner _salesToCustomer;
        private string _salesToContact;
        private Country _salesToCountry;
        private City _salesToCity;
        private string _salesToAddress;
        #endregion Sales
        #region Bill
        private BusinessPartner _billToCustomer;
        private string _billToContact;
        private Country _billToCountry;
        private City _billToCity;
        private string _billToAddress;
        #endregion Bill
        private string _description;
        private Currency _currency;
        private PostingType _salesType;
        private PostingMethod _postingMethod;
        private PriceGroup _priceGroup;
        #region InitialBank
        private XPCollection<BankAccount> _availableCompanyBankAccount;
        private BankAccount _companyBankAccount;
        private string _companyAccountNo;
        private string _companyAccountName;
        private XPCollection<BankAccount> _availableBankAccount;
        private BankAccount _bankAccount;
        private string _accountNo;
        private string _accountName;
        #endregion InitialBank
        private double _grandTotalAmount;
        private double _grandTotalAmountDN;
        private double _grandTotalAmountCN;
        private double _grandTotalPlan;
        private Status _status;
        private DateTime _statusDate;    
        private string _signCode;
        private string _userAccess;        
        private GlobalFunction _globFunc;
        //AuditTrail
        private XPCollection<AuditDataItemPersistent> changeHistory;

        public ReceivableTransaction(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if (!IsLoading)
            {
                if(Session.IsNewObject(this))
                {
                    DateTime now = DateTime.Now;
                    _globFunc = new GlobalFunction();
                    #region UserAccess
                    _userAccess = SecuritySystem.CurrentUserName;
                    UserAccess _locUserAccess = Session.FindObject<UserAccess>(new BinaryOperator("UserName", _userAccess));
                    if (_locUserAccess != null)
                    {
                        if (_locUserAccess.Employee != null)
                        {
                            if (_locUserAccess.Employee.Company != null && _locUserAccess.Employee.Workplace != null)
                            {
                                this.Code = _globFunc.GetNumberingUnlockOptimisticRecordBasedOrganization(this.Session.DataLayer, ObjectList.ReceivableTransaction, _locUserAccess.Employee.Company, _locUserAccess.Employee.Workplace);
                            }
                            else
                            {
                                this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.ReceivableTransaction);
                            }

                            if (_locUserAccess.Employee.Company != null)
                            {
                                Company = _locUserAccess.Employee.Company;
                            }
                            if (_locUserAccess.Employee.Workplace != null)
                            {
                                this.Workplace = _locUserAccess.Employee.Workplace;
                            }
                            else
                            {
                                this.Workplace = _globFunc.GetDefaultWorkplace(this.Session, _locUserAccess);
                            }
                            if (_locUserAccess.Employee.Division != null)
                            {
                                Division = _locUserAccess.Employee.Division;
                            }
                            if (_locUserAccess.Employee.Department != null)
                            {
                                Department = _locUserAccess.Employee.Department;
                            }
                            if (_locUserAccess.Employee.Section != null)
                            {
                                Section = _locUserAccess.Employee.Section;
                            }

                            this.Employee = _locUserAccess.Employee;
 
                        }
                
                    }
                    #endregion UserAccess
                    this.Status = Status.Open;
                    this.StatusDate = now;
                    this.SalesType = PostingType.Sales;
                    this.PostingMethod = PostingMethod.Bill;
                    this.Currency = _globFunc.GetDefaultCurrency(this.Session);
                    
                }                
            }
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).
        }

        #region Field

        [Browsable(false)]
        public bool ActivationPosting
        {
            get { return _activationPosting; }
            set { SetPropertyValue("ActivationPosting", ref _activationPosting, value); }
        }

        [RuleRequiredField(DefaultContexts.Save)]
        [Appearance("ReceivableTransactionCodeClose", Enabled = false)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue("Code", ref _code, value); }
        }

        #region Organization

        [ImmediatePostData()]
        [RuleRequiredField(DefaultContexts.Save)]
        [DataSourceCriteria("Active = true")]
        [Appearance("ReceivableTransactionCompanyClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Company Company
        {
            get { return _company; }
            set { SetPropertyValue("Company", ref _company, value); }
        }

        [ImmediatePostData()]
        [RuleRequiredField(DefaultContexts.Save)]
        [DataSourceCriteria("Active = true")]
        [Appearance("ReceivableTransactionWorkplaceClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Workplace Workplace
        {
            get { return _workplace; }
            set { SetPropertyValue("Workplace", ref _workplace, value); }
        }

        [Browsable(false)]
        public Division Division
        {
            get { return _division; }
            set { SetPropertyValue("Division", ref _division, value); }
        }

        [Browsable(false)]
        public Department Department
        {
            get { return _department; }
            set { SetPropertyValue("Department", ref _department, value); }
        }

        [Browsable(false)]
        public Section Section
        {
            get { return _section; }
            set { SetPropertyValue("Section", ref _section, value); }
        }

        [Browsable(false)]
        public Employee Employee
        {
            get { return _employee; }
            set { SetPropertyValue("Employee", ref _employee, value); }
        }

        #endregion Organization

        #region SalesToCostumer

        [Browsable(false)]
        public XPCollection<BusinessPartner> AvailableSalesToCustomer
        {
            get
            {
                if(!IsLoading)
                {
                    _userAccess = SecuritySystem.CurrentUserName;
                    string _beginString = null;
                    string _endString = null;
                    string _fullString = null;
                    string _locBusinessPartnerCode = null;
                    UserAccess _locUserAccess = Session.FindObject<UserAccess>(new BinaryOperator("UserName", _userAccess));
                    List<string> _stringLocation = new List<string>();
                    if (_locUserAccess != null)
                    {
                        if (this.Company != null)
                        {
                            XPQuery<OrderCollectionMonitoring> _orderCollectionMonitoringsQuery = new XPQuery<OrderCollectionMonitoring>(Session);

                            var _orderCollectionMonitorings = from ocm in _orderCollectionMonitoringsQuery
                                                              where (ocm.Company == this.Company
                                                              && ocm.Status != Status.Close
                                                              && ocm.CollectStatus == CollectStatus.Collect
                                                              && ocm.ReceivableTransaction == null
                                                              )
                                                              group ocm by ocm.Customer into g
                                                              select new { Customer = g.Key };

                            if (_orderCollectionMonitorings != null && _orderCollectionMonitorings.Count() > 0)
                            {
                                foreach (var _orderCollectionMonitoring in _orderCollectionMonitorings)
                                {
                                    if (_orderCollectionMonitoring.Customer != null)
                                    {
                                        if (_orderCollectionMonitoring.Customer.Code != null)
                                        {
                                            _locBusinessPartnerCode = _orderCollectionMonitoring.Customer.Code;
                                            _stringLocation.Add(_locBusinessPartnerCode);
                                        }
                                    }
                                }
                            }

                            IEnumerable<string> _stringArrayLocationFromDistinct = _stringLocation.Distinct();
                            string[] _stringArrayLocationFromList = _stringArrayLocationFromDistinct.ToArray();
                            if (_stringArrayLocationFromList.Length == 1)
                            {
                                for (int i = 0; i < _stringArrayLocationFromList.Length; i++)
                                {
                                    BusinessPartner _locBP = Session.FindObject<BusinessPartner>(new BinaryOperator("Code", _stringArrayLocationFromList[i]));
                                    if (_locBP != null)
                                    {
                                        if (i == 0)
                                        {
                                            _beginString = "[Code]=='" + _locBP.Code + "'";
                                        }
                                    }
                                }
                            }
                            else if (_stringArrayLocationFromList.Length > 1)
                            {
                                for (int i = 0; i < _stringArrayLocationFromList.Length; i++)
                                {
                                    BusinessPartner _locBP = Session.FindObject<BusinessPartner>(new BinaryOperator("Code", _stringArrayLocationFromList[i]));
                                    if (_locBP != null)
                                    {
                                        if (i == 0)
                                        {
                                            _beginString = "[Code]=='" + _locBP.Code + "'";
                                        }
                                        else
                                        {
                                            _endString = _endString + " OR [Code]=='" + _locBP.Code + "'";
                                        }
                                    }
                                }
                            }
                            _fullString = _beginString + _endString;

                            if (_fullString != null)
                            {
                                _availableSalesToCustomer = new XPCollection<BusinessPartner>(Session, CriteriaOperator.Parse(_fullString));
                            }  
                        }
                    }
                }

                return _availableSalesToCustomer;

            }
        }

        [ImmediatePostData]
        [DataSourceProperty("AvailableSalesToCustomer", DataSourcePropertyIsNullMode.SelectAll)]
        [DataSourceCriteria("Active = true")]
        [Appearance("ReceivableTransactionSalesToCustomerClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public BusinessPartner SalesToCustomer
        {
            get { return _salesToCustomer; }
            set
            {
                SetPropertyValue("SalesToCustomer", ref _salesToCustomer, value);
                if (!IsLoading)
                {
                    if (_salesToCustomer != null)
                    {
                        this.SalesToContact = _salesToCustomer.Contact;
                        this.SalesToCountry = _salesToCustomer.Country;
                        this.SalesToCity = _salesToCustomer.City;
                        this.SalesToAddress = this.SalesToAddress;
                        this.PriceGroup = this._salesToCustomer.PriceGroup;
                        this.BillToCustomer = _salesToCustomer;
                    }
                    else
                    {
                        this.SalesToContact = null;
                        this.SalesToCountry = null;
                        this.SalesToCity = null;
                        this.SalesToAddress = null;
                        this.PriceGroup = null;
                        this.BillToCustomer = null;
                    }
                }
            }
        }

        [Appearance("ReceivableTransactionSalesToContactClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public string SalesToContact
        {
            get { return _salesToContact; }
            set { SetPropertyValue("SalesToContact", ref _salesToContact, value); }
        }

        [DataSourceCriteria("Active = true")]
        [Appearance("ReceivableTransactionSalesCountryClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Country SalesToCountry
        {
            get { return _salesToCountry; }
            set { SetPropertyValue("SalesToCountry", ref _salesToCountry, value); }
        }

        [DataSourceCriteria("Active = true")]
        [Appearance("ReceivableTransactionSalesToCityClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public City SalesToCity
        {
            get { return _salesToCity; }
            set { SetPropertyValue("SalesToCity", ref _salesToCity, value); }
        }

        [Appearance("ReceivableTransactionSalesToAddressClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [Size(512)]
        public string SalesToAddress
        {
            get { return _salesToAddress; }
            set { SetPropertyValue("SalesToAddress", ref _salesToAddress, value); }
        }

        #endregion SalesToCostumer

        #region BillToCostumer

        [ImmediatePostData()]
        [DataSourceCriteria("Active = true")]
        [Appearance("ReceivableTransactionBillToCostumerClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public BusinessPartner BillToCustomer
        {
            get { return _billToCustomer; }
            set
            {
                SetPropertyValue("BillToCustomer", ref _billToCustomer, value);
                if (!IsLoading)
                {
                    if (this._billToCustomer != null)
                    {
                        this.BillToContact = this._billToCustomer.Contact;
                        this.BillToAddress = this._billToCustomer.Address;
                        this.BillToCountry = this._billToCustomer.Country;
                        this.BillToCity = this._billToCustomer.City;
                    }
                }
            }
        }

        [Appearance("ReceivableTransactionBillToContactClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public string BillToContact
        {
            get { return _billToContact; }
            set { SetPropertyValue("BillToContact", ref _billToContact, value); }
        }

        [DataSourceCriteria("Active = true")]
        [Appearance("ReceivableTransactionBillCountryClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Country BillToCountry
        {
            get { return _billToCountry; }
            set { SetPropertyValue("BillToCountry", ref _billToCountry, value); }
        }

        [DataSourceCriteria("Active = true")]
        [Appearance("ReceivableTransactionBillToCityClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public City BillToCity
        {
            get { return _billToCity; }
            set { SetPropertyValue("BillToCity", ref _billToCity, value); }
        }

        [Appearance("ReceivableTransactionBillToAddressClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [Size(512)]
        public string BillToAddress
        {
            get { return _billToAddress; }
            set { SetPropertyValue("BillToAddress", ref _billToAddress, value); }
        }

        #endregion BillToCostumer

        [Size(512)]
        [Appearance("ReceivableTransactionDescriptionClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public string Description
        {
            get { return _description; }
            set { SetPropertyValue("Description", ref _description, value); }
        }

        [Appearance("ReceivableTransactionCurrencyClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Currency Currency
        {
            get { return _currency; }
            set { SetPropertyValue("Currency", ref _currency, value); }
        }

        [Appearance("ReceivableTransactionPostingTypeClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public PostingType SalesType
        {
            get { return _salesType; }
            set { SetPropertyValue("PostingType", ref _salesType, value); }
        }

        [Appearance("ReceivableTransactionPostingMethodClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public PostingMethod PostingMethod
        {
            get { return _postingMethod; }
            set { SetPropertyValue("PostingMethod", ref _postingMethod, value); }
        }

        [ImmediatePostData()]
        [Appearance("ReceivableTransactionPriceGroupClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public PriceGroup PriceGroup
        {
            get { return _priceGroup; }
            set { SetPropertyValue("PriceGroup", ref _priceGroup, value); }
        }

        #region Bank

        [Browsable(false)]
        public XPCollection<BankAccount> AvailableCompanyBankAccount
        {
            get
            {
                if (!IsLoading)
                {
                    if (this.Company != null)
                    {
                        if (this.Workplace != null)
                        {
                            _availableCompanyBankAccount = new XPCollection<BankAccount>(Session, new GroupOperator
                                                    (GroupOperatorType.And,
                                                    new BinaryOperator("Company", this.Company),
                                                    new BinaryOperator("Workplace", this.Workplace),
                                                    new BinaryOperator("Active", true)));
                        }
                        else
                        {
                            _availableCompanyBankAccount = new XPCollection<BankAccount>(Session, new GroupOperator
                                                    (GroupOperatorType.And,
                                                    new BinaryOperator("Company", this.Company),
                                                    new BinaryOperator("Active", true)));
                        }
                    }
                    else
                    {
                        _availableCompanyBankAccount = new XPCollection<BankAccount>(Session,
                            new GroupOperator(GroupOperatorType.And,
                            new BinaryOperator("Active", true)));
                    }
                }

                return _availableCompanyBankAccount;

            }
        }

        [ImmediatePostData()]
        [DataSourceProperty("AvailableCompanyBankAccount")]
        [Appearance("ReceivableTransactionCompanyBankAccountClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public BankAccount CompanyBankAccount
        {
            get { return _companyBankAccount; }
            set
            {
                SetPropertyValue("CompanyBankAccount", ref _companyBankAccount, value);
                if (!IsLoading)
                {
                    if (this._companyBankAccount != null)
                    {
                        this.CompanyAccountNo = this._companyBankAccount.AccountNo;
                        this.CompanyAccountName = this._companyBankAccount.AccountName;
                    }
                }
            }
        }

        [Appearance("ReceivableTransactionCompanyAccountNoClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public string CompanyAccountNo
        {
            get { return _companyAccountNo; }
            set { SetPropertyValue("CompanyAccountNo", ref _companyAccountNo, value); }
        }

        [Appearance("ReceivableTransactionCompanyAccountNameClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public string CompanyAccountName
        {
            get { return _companyAccountName; }
            set { SetPropertyValue("CompanyAccountName", ref _companyAccountName, value); }
        }

        [Browsable(false)]
        public XPCollection<BankAccount> AvailableBankAccount
        {
            get
            {
                if (this.Company != null && this.Workplace != null && this.BillToCustomer != null)
                {
                    _availableBankAccount = new XPCollection<BankAccount>(Session,
                                               new GroupOperator(GroupOperatorType.And,
                                               new BinaryOperator("Company", this.Company),
                                               new BinaryOperator("Workplace", this.Workplace),
                                               new BinaryOperator("BusinessPartner", this.BillToCustomer),
                                               new BinaryOperator("Active", true)));
                    
                    
                }
                else
                {

                    _availableBankAccount = new XPCollection<BankAccount>(Session,
                                            new GroupOperator(GroupOperatorType.And,
                                            new BinaryOperator("Active", true)));

                }
                return _availableBankAccount;

            }
        }

        [Appearance("ReceivableTransactionBankAccountClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [DataSourceProperty("AvailableBankAccount")]
        [DataSourceCriteria("Active = true")]
        public BankAccount BankAccount
        {
            get { return _bankAccount; }
            set
            {
                SetPropertyValue("BankAccount", ref _bankAccount, value);
                if (!IsLoading)
                {
                    if (this._bankAccount != null)
                    {
                        this.AccountNo = this._bankAccount.AccountNo;
                        this.AccountName = this._bankAccount.AccountName;
                    }
                }
            }
        }

        [Appearance("ReceivableTransactionAccountNoClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public string AccountNo
        {
            get { return _accountNo; }
            set { SetPropertyValue("AccountNo", ref _accountNo, value); }
        }

        [Appearance("ReceivableTransactionAccountNameClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public string AccountName
        {
            get { return _accountName; }
            set { SetPropertyValue("AccountName", ref _accountName, value); }
        }

        #endregion Bank

        [Appearance("ReceivableTransactionGrandTotalAmountClose", Enabled = false)]
        public double GrandTotalAmount
        {
            get { return _grandTotalAmount; }
            set { SetPropertyValue("GrandTotalAmount", ref _grandTotalAmount, value); }
        }

        [Appearance("ReceivableTransactionGrandTotalAmountDNClose", Enabled = false)]
        public double GrandTotalAmountDN
        {
            get { return _grandTotalAmountDN; }
            set { SetPropertyValue("GrandTotalAmountDN", ref _grandTotalAmountDN, value); }
        }

        [Appearance("ReceivableTransactionGrandTotalAmountCNClose", Enabled = false)]
        public double GrandTotalAmountCN
        {
            get { return _grandTotalAmountCN; }
            set { SetPropertyValue("GrandTotalAmountCN", ref _grandTotalAmountCN, value); }
        }

        [Appearance("ReceivableTransactionGrandTotalPlanClose", Enabled = false)]
        public double GrandTotalPlan
        {
            get { return _grandTotalPlan; }
            set { SetPropertyValue("GrandTotalPlan", ref _grandTotalPlan, value); }
        }

        [Appearance("ReceivableTransactionStatusClose", Enabled = false)]
        public Status Status
        {
            get { return _status; }
            set { SetPropertyValue("Status", ref _status, value); }
        }

        [ModelDefault("DisplayFormat", "{0:dd/MM/yyyy}")]
        [ModelDefault("EditMask", "dd/MM/yyyy")]
        [Appearance("ReceivableTransactionStatusDateClose", Enabled = false)]
        public DateTime StatusDate
        {
            get { return _statusDate; }
            set { SetPropertyValue("StatusDate", ref _statusDate, value); }
        }

        [Appearance("ReceivableTransactionSignCodeClose", Enabled = false)]
        public string SignCode
        {
            get { return _signCode; }
            set { SetPropertyValue("SignCode", ref _signCode, value); }
        }        

        [Association("ReceivableTransaction-ReceivableTransactionLines")]
        public XPCollection<ReceivableTransactionLine> ReceivableTransactionLines
        {
            get { return GetCollection<ReceivableTransactionLine>("ReceivableTransactionLines"); }
        }

        [Association("ReceivableTransaction-ReceivableOrderCollections")]
        public XPCollection<ReceivableOrderCollection> ReceivableOrderCollections
        {
            get { return GetCollection<ReceivableOrderCollection>("ReceivableOrderCollections"); }
        }

        //AuditTrail
        [CollectionOperationSet(AllowAdd = false, AllowRemove = true)]
        public XPCollection<AuditDataItemPersistent> ChangeHistory
        {
            get
            {
                if (changeHistory == null)
                {
                    changeHistory = AuditedObjectWeakReference.GetAuditTrail(Session, this);
                }
                return changeHistory;
            }
        }

        #endregion Field

    }
}