﻿using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;

using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.ConditionalAppearance;
using DevExpress.ExpressApp.SystemModule;

namespace FullDrive.Module.BusinessObjects
{
    [DefaultClassOptions]
    [DefaultProperty("Code")]
    [NavigationItem("Finance & Accounting")]
    [RuleCombinationOfPropertiesIsUnique("payableTransactionBankRuleUnique", DefaultContexts.Save, "Code")]
    //[ImageName("BO_Contact")]
    //[DefaultProperty("DisplayMemberNameForLookupEditorsOfThisType")]
    //[DefaultListViewOptions(MasterDetailMode.ListViewOnly, false, NewItemRowPosition.None)]
    //[Persistent("DatabaseTableName")]
    // Specify more UI options using a declarative approach (https://documentation.devexpress.com/#eXpressAppFramework/CustomDocument112701).
    public class PayableTransactionBank : BaseObject
    { // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).
        private bool _activationPosting;
        private string _code;
        #region InitialOrganization
        private Company _companyDefault;
        private Workplace _workplace;
        private Division _division;
        private Department _department;
        private Section _section;
        private Employee _employee;
        #endregion InitialOrganization
        private PurchaseInvoice _purchaseInvoice;
        private double _amount;
        #region InitialBank
        private XPCollection<BankAccount> _availableBankAccount;
        private BankAccount _bankAccount;
        private string _accountNo;
        private string _accountName;
        #endregion InitialBank
        private Status _status;
        private DateTime _statusdate;
        private PayableTransactionLine _payableTransactionLine;
        private string _userAccessIn;
        private GlobalFunction _globFunc;
        //AuditTrail
        private XPCollection<AuditDataItemPersistent> changeHistory;

        public PayableTransactionBank(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if (!IsLoading)
            {
                if (Session.IsNewObject(this))
                {
                    DateTime now = DateTime.Now;
                    _globFunc = new GlobalFunction();
                    #region UserAccess
                    _userAccessIn = SecuritySystem.CurrentUserName;
                    UserAccess _locUserAccess = Session.FindObject<UserAccess>(new BinaryOperator("UserName", _userAccessIn));
                    if (_locUserAccess != null)
                    {
                        if (_locUserAccess.Employee != null)
                        {
                            if (_locUserAccess.Employee.Company != null && _locUserAccess.Employee.Workplace != null)
                            {
                                this.Code = _globFunc.GetNumberingUnlockOptimisticRecordBasedOrganization(this.Session.DataLayer, ObjectList.PayableTransactionBank, _locUserAccess.Employee.Company, _locUserAccess.Employee.Workplace);
                            }
                            else
                            {
                                this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.PayableTransactionBank);
                            }
                        }
                    }
                    #endregion UserAccess
                    this.Status = Status.Open;
                    this.StatusDate = now;
                }
            }
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).
        }

        #region Field

        [Browsable(false)]
        [ImmediatePostData()]
        public bool ActivationPosting
        {
            get { return _activationPosting; }
            set { SetPropertyValue("ActivationPosting", ref _activationPosting, value); }
        }

        [Appearance("PayableTransactionBankClose", Enabled = false)]
        [RuleRequiredField(DefaultContexts.Save)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue("Code", ref _code, value); }
        }

        #region Organization

        [Appearance("PayableTransactionBankCompanyDefaultClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Company CompanyDefault
        {
            get { return _companyDefault; }
            set { SetPropertyValue("CompanyDefault", ref _companyDefault, value); }
        }

        [Appearance("PayableTransactionBankWorkplaceClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Workplace Workplace
        {
            get { return _workplace; }
            set { SetPropertyValue("Workplace", ref _workplace, value); }
        }

        [Browsable(false)]
        public Division Division
        {
            get { return _division; }
            set { SetPropertyValue("Division", ref _division, value); }
        }

        [Browsable(false)]
        public Department Department
        {
            get { return _department; }
            set { SetPropertyValue("Department", ref _department, value); }
        }

        [Browsable(false)]
        public Section Section
        {
            get { return _section; }
            set { SetPropertyValue("Section", ref _section, value); }
        }

        [Browsable(false)]
        public Employee Employee
        {
            get { return _employee; }
            set { SetPropertyValue("Employee", ref _employee, value); }
        }

        #endregion Organization

        [Appearance("PayableTransactionBankPurchaseInvoiceClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public PurchaseInvoice PurchaseInvoice
        {
            get { return _purchaseInvoice; }
            set { SetPropertyValue("PurchaseInvoice", ref _purchaseInvoice, value); }
        }

        [ImmediatePostData()]
        [Appearance("PayableTransactionBankAmountClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public double Amount
        {
            get { return _amount; }
            set { SetPropertyValue("Amount", ref _amount, value); }
        }

        #region Bank

        [Browsable(false)]
        public XPCollection<BankAccount> AvailableBankAccount
        {
            get
            {
                if (this.CompanyDefault != null && this.Workplace != null)
                {
                    if (this.PurchaseInvoice != null)
                    {
                        if (this.PurchaseInvoice.PayToVendor != null)
                        {
                            _availableBankAccount = new XPCollection<BankAccount>(Session,
                                               new GroupOperator(GroupOperatorType.And,
                                               new BinaryOperator("Company", this.CompanyDefault),
                                               new BinaryOperator("Workplace", this.Workplace),
                                               new BinaryOperator("BusinessPartner", this.PurchaseInvoice.PayToVendor),
                                               new BinaryOperator("Active", true)));
                        }
                    }
                }
                else
                {

                    _availableBankAccount = new XPCollection<BankAccount>(Session,
                                            new GroupOperator(GroupOperatorType.And,
                                            new BinaryOperator("Active", true)));

                }
                return _availableBankAccount;

            }
        }

        [Appearance("PayableTransactionLineBankAccountClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [DataSourceProperty("AvailableBankAccount")]
        [DataSourceCriteria("Active = true")]
        public BankAccount BankAccount
        {
            get { return _bankAccount; }
            set
            {
                SetPropertyValue("BankAccount", ref _bankAccount, value);
                if (!IsLoading)
                {
                    if (this._bankAccount != null)
                    {
                        this.AccountNo = this._bankAccount.AccountNo;
                        this.AccountName = this._bankAccount.AccountName;
                    }
                }
            }
        }

        [Appearance("PayableTransactionLineAccountNoClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public string AccountNo
        {
            get { return _accountNo; }
            set { SetPropertyValue("AccountNo", ref _accountNo, value); }
        }

        [Appearance("PayableTransactionLineAccountNameClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public string AccountName
        {
            get { return _accountName; }
            set { SetPropertyValue("AccountName", ref _accountName, value); }
        }

        #endregion Bank

        [Appearance("PayableTransactionLineStatusClose", Enabled = false)]
        public Status Status
        {
            get { return _status; }
            set { SetPropertyValue("Status", ref _status, value); }
        }

        [ModelDefault("DisplayFormat", "{0:dd/MM/yyyy}")]
        [ModelDefault("EditMask", "dd/MM/yyyy")]
        [Appearance("PayableTransactionLineStatusDateClose", Enabled = false)]
        public DateTime StatusDate
        {
            get { return _statusdate; }
            set { SetPropertyValue("StatusDate", ref _statusdate, value); }
        }

        [Association("PayableTransactionLine-PayableTransactionBanks")]
        [Appearance("PayableTransactionBankPayableTransactionLineClose", Enabled = false)]
        public PayableTransactionLine PayableTransactionLine
        {
            get { return _payableTransactionLine; }
            set
            {
                SetPropertyValue("PayableTransactionLine", ref _payableTransactionLine, value);
                if (!IsLoading)
                {
                    if (this._payableTransactionLine != null)
                    {
                        if (this._payableTransactionLine.CompanyDefault != null) { this.CompanyDefault = this._payableTransactionLine.CompanyDefault; }
                        if (this._payableTransactionLine.Workplace != null) { this.Workplace = this._payableTransactionLine.Workplace; }
                        if (this._payableTransactionLine.Division != null) { this.Division = this._payableTransactionLine.Division; }
                        if (this._payableTransactionLine.Department != null) { this.Department = this._payableTransactionLine.Department; }
                        if (this._payableTransactionLine.Section != null) { this.Section = this._payableTransactionLine.Section; }
                        if (this._payableTransactionLine.Employee != null) { this.Employee = this._payableTransactionLine.Employee; }
                        if (this._payableTransactionLine.PurchaseInvoice != null) { this.PurchaseInvoice = this._payableTransactionLine.PurchaseInvoice; }
                        //if (this._payableTransactionLine.Amount > 0) { this.Amount = this._payableTransactionLine.Amount; }
                    }
                }
            }
        }

        //AuditTrail
        [CollectionOperationSet(AllowAdd = false, AllowRemove = true)]
        public XPCollection<AuditDataItemPersistent> ChangeHistory
        {
            get
            {
                if (changeHistory == null)
                {
                    changeHistory = AuditedObjectWeakReference.GetAuditTrail(Session, this);
                }
                return changeHistory;
            }
        }

        #endregion Field
      
    }
}