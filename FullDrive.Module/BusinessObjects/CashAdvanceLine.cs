﻿using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;

using DevExpress.ExpressApp.ConditionalAppearance;
using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.Xpo;
using System.Collections;
using System.IO;

namespace FullDrive.Module.BusinessObjects
{
    [DefaultClassOptions]
    [DefaultProperty("Code")]
    [NavigationItem("Cash Advance")]
    [RuleCombinationOfPropertiesIsUnique("CashAdvanceLineRuleUnique", DefaultContexts.Save, "Code")]
    //[ImageName("BO_Contact")]
    //[DefaultProperty("DisplayMemberNameForLookupEditorsOfThisType")]
    //[DefaultListViewOptions(MasterDetailMode.ListViewOnly, false, NewItemRowPosition.None)]
    //[Persistent("DatabaseTableName")]
    // Specify more UI options using a declarative approach (https://documentation.devexpress.com/#eXpressAppFramework/CustomDocument112701).
    public class CashAdvanceLine : BaseObject
    { // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).
        private bool _activationPosting;
        private string _code;
        private bool _select;
        #region InitialOrganization
        private Company _company;
        private Workplace _workplace;
        private Division _division;
        private Department _department;
        private Section _section;
        private Employee _employee;
        #endregion InitialOrganization
        private string _name;
        private DateTime _requestDate;
        private DateTime _requiredDate;
        private CashAdvanceType _cashAdvanceType;
        private double _amount;
        private double _amountApproved;
        private string _description;
        private DateTime _docDate;
        private int _journalMonth;
        private int _journalYear;
        private Status _status;
        private DateTime _statusDate;
        private string _currentUser;
        private CashAdvance _cashAdvance;
        private string _localUserAccess;
        private GlobalFunction _globFunc;
        //AuditTrail
        private XPCollection<AuditDataItemPersistent> changeHistory;

        public CashAdvanceLine(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if(!IsLoading)
            {
                if(Session.IsNewObject(this))
                {
                    _globFunc = new GlobalFunction();
                    DateTime now = DateTime.Now;
                    #region UserAccess
                    _localUserAccess = SecuritySystem.CurrentUserName;
                    UserAccess _locUserAccess = Session.FindObject<UserAccess>(new BinaryOperator("UserName", _localUserAccess));
                    if (_locUserAccess != null)
                    {
                        if (_locUserAccess.Employee != null)
                        {
                            if (_locUserAccess.Employee.Company != null && _locUserAccess.Employee.Workplace != null)
                            {
                                this.Code = _globFunc.GetNumberingUnlockOptimisticRecordBasedOrganization(this.Session.DataLayer, ObjectList.CashAdvanceLine, _locUserAccess.Employee.Company, _locUserAccess.Employee.Workplace);
                            }
                            else
                            {
                                this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.CashAdvanceLine);
                            }
                        }
                    }
                    #endregion UserAccess
                    this.Status = Status.Open;
                    this.Select = true;
                    this.RequestDate = now;
                    this.StatusDate = now;
                    this.RequiredDate = now;
                }               
            }
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).
        }

        #region Field

        [Browsable(false)]
        public bool ActivationPosting
        {
            get { return _activationPosting; }
            set { SetPropertyValue("ActivationPosting", ref _activationPosting, value); }
        }

        [RuleRequiredField(DefaultContexts.Save)]
        [ImmediatePostData()]
        [Appearance("CashAdvanceCodeClose", Enabled = false)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue("Code", ref _code, value); }
        }

        [Appearance("CashAdvanceLineSelectClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public bool Select
        {
            get { return _select; }
            set { SetPropertyValue("Select", ref _select, value); }
        }

        [ImmediatePostData()]
        [Appearance("CashAdvanceLineNameClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public string Name
        {
            get
            {
                if (Code != null)
                {
                    if (Employee != null)
                    {
                        UpdateName();
                    }
                }
                return _name;
            }
            set
            {
                SetPropertyValue("Name", ref _name, value);
                UpdateName();
            }
        }

        #region Organization

        [VisibleInListView(false)]
        [RuleRequiredField(DefaultContexts.Save)]
        [Appearance("CashAdvanceLineCompanyEnabled", Enabled = false)]
        public Company Company
        {
            get { return _company; }
            set { SetPropertyValue("Company", ref _company, value); }
        }

        [VisibleInListView(false)]
        [Appearance("CashAdvanceLineWorkplaceEnabled", Enabled = false)]
        public Workplace Workplace
        {
            get { return _workplace; }
            set { SetPropertyValue("Workplace", ref _workplace, value); }
        }

        [Browsable(false)]
        [Appearance("CashAdvanceLineDivisionEnabled", Enabled = false)]
        public Division Division
        {
            get { return _division; }
            set { SetPropertyValue("Division", ref _division, value); }
        }

        [Browsable(false)]
        [Appearance("CashAdvanceLineDepartmentEnabled", Enabled = false)]
        public Department Department
        {
            get { return _department; }
            set { SetPropertyValue("Department", ref _department, value); }
        }

        [Browsable(false)]
        [Appearance("CashAdvanceLineSectionEnabled", Enabled = false)]
        public Section Section
        {
            get { return _section; }
            set { SetPropertyValue("Section", ref _section, value); }
        }

        [ImmediatePostData()]
        [Appearance("CashAdvanceLineEmployeeClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Employee Employee
        {
            get
            {
                if (CashAdvance != null)
                {
                    this._employee = this._cashAdvance.Employee;
                }
                return _employee;
            }
            set
            {
                SetPropertyValue("Employee", ref _employee, value);
                UpdateName();
            }
        }

        #endregion Organization

        [ModelDefault("DisplayFormat", "{0:dd/MM/yyyy}")]
        [ModelDefault("EditMask", "dd/MM/yyyy")]
        [Appearance("CashAdvanceLineRequestDateClose", Enabled = false)]
        public DateTime RequestDate
        {
            get { return _requestDate; }
            set { SetPropertyValue("RequestDate", ref _requestDate, value); }
        }

        [ModelDefault("DisplayFormat", "{0:dd/MM/yyyy}")]
        [ModelDefault("EditMask", "dd/MM/yyyy")]
        [Appearance("CashAdvanceLineRequiredDateClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public DateTime RequiredDate
        {
            get { return _requiredDate; }
            set { SetPropertyValue("RequiredDate", ref _requiredDate, value); }
        }

        [DataSourceCriteria("Active = true And Company = '@This.Company' And Workplace = '@This.Workplace'"), ImmediatePostData()]
        [Appearance("CashAdvanceLineCashAdvanceTypeClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public CashAdvanceType CashAdvanceType
        {
            get { return _cashAdvanceType; }
            set { SetPropertyValue("CashAdvaceType", ref _cashAdvanceType, value); }
        }

        [ImmediatePostData()]
        [Appearance("CashAdvanceLineAmountClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public double Amount
        {
            get { return _amount; }
            set
            {
                SetPropertyValue("Amount", ref _amount, value);
                if (!IsLoading)
                {
                    if (this._amount > 0)
                    {
                        this.AmountApproved = this._amount;
                    }
                }
            }
        }

        [ImmediatePostData]
        [Appearance("CashAdvanceLineAmountApprovedClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public double AmountApproved
        {
            get { return _amountApproved; }
            set
            {
                SetPropertyValue("AmountApproved", ref _amountApproved, value);
                if (!IsLoading)
                {
                    if (this._amountApproved > 0)
                    {
                        if (this._amountApproved > this.Amount)
                        {
                            this._amountApproved = this.Amount;
                        }
                    }
                }
            }
        }

        [Size(512)]
        [Appearance("CashAdvanceLineDescriptionClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public string Description
        {
            get { return _description; }
            set { SetPropertyValue("Description", ref _description, value); }
        }

        [ImmediatePostData()]
        [ModelDefault("DisplayFormat", "{0:dd/MM/yyyy}")]
        [ModelDefault("EditMask", "dd/MM/yyyy")]
        [Appearance("CashAdvanceLineDocDateClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public DateTime DocDate
        {
            get { return _docDate; }
            set
            {
                SetPropertyValue("DocDate", ref _docDate, value);
                if (!IsLoading)
                {
                    SetAccountingPeriodic();
                }
            }
        }

        [Browsable(false)]
        public int JournalMonth
        {
            get { return _journalMonth; }
            set { SetPropertyValue("JournalMonth", ref _journalMonth, value); }
        }

        [Browsable(false)]
        public int JournalYear
        {
            get { return _journalYear; }
            set { SetPropertyValue("JournalYear", ref _journalYear, value); }
        }

        [Appearance("CashAdvanceLineStatusClose", Enabled = false)]
        public Status Status
        {
            get { return _status; }
            set { SetPropertyValue("Status", ref _status, value); }
        }

        [ModelDefault("DisplayFormat", "{0:dd/MM/yyyy}")]
        [ModelDefault("EditMask", "dd/MM/yyyy")]
        [Appearance("CashAdvanceLineStatusDateClose", Enabled = false)]
        public DateTime StatusDate
        {
            get { return _statusDate; }
            set { SetPropertyValue("StatusDate", ref _statusDate, value); }
        }

        [Association("CashAdvance-CashAdvanceLines")]
        [Appearance("CashAdvanceLineCashAdvanceClose", Enabled = false)]
        public CashAdvance CashAdvance
        {
            get { return _cashAdvance; }
            set
            {
                SetPropertyValue("CashAdvance", ref _cashAdvance, value);
                if (!IsLoading)
                {
                    if (this._cashAdvance != null)
                    {
                        if (this._cashAdvance.Company != null) { this.Company = this._cashAdvance.Company; }
                        if (this._cashAdvance.Workplace != null) { this.Workplace = this._cashAdvance.Workplace; }
                        if (this._cashAdvance.Division != null) { this.Division = this._cashAdvance.Division; }
                        if (this._cashAdvance.Department != null) { this.Department = this._cashAdvance.Department; }
                        if (this._cashAdvance.Section != null) { this.Section = this._cashAdvance.Section; }
                        if (this._cashAdvance.Employee != null) { this.Employee = this._cashAdvance.Employee; }
                        this.DocDate = this._cashAdvance.DocDate;
                        this.JournalMonth = this._cashAdvance.JournalMonth;
                        this.JournalYear = this._cashAdvance.JournalYear;
                        this.Amount = this._cashAdvance.TotAmountApproved;
                    }
                }
            }
        }

        //AuditTrail
        [CollectionOperationSet(AllowAdd = false, AllowRemove = true)]
        public XPCollection<AuditDataItemPersistent> ChangeHistory
        {
            get
            {
                if (changeHistory == null)
                {
                    changeHistory = AuditedObjectWeakReference.GetAuditTrail(Session, this);
                }
                return changeHistory;
            }
        }

        #endregion Field

        //=========================================== Code Only ===========================================

        private void UpdateName()
        {
            try
            {
                this._name = string.Format("Cash Advance Line {0} [ {1} ]", Code, Employee.Name);
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("BusinessObject = CashAdvanceLine" + ex.ToString());
            }
        }

        private void SetAccountingPeriodic()
        {
            try
            {
                DateTime now = DateTime.Now;
                if (this.Company != null && this.Workplace != null)
                {
                    AccountingPeriodic _locAcctPeriodic = Session.FindObject<AccountingPeriodic>
                                                        (new GroupOperator(GroupOperatorType.And,
                                                        new BinaryOperator("Company", this.Company),
                                                        new BinaryOperator("Workplace", this.Workplace),
                                                        new BinaryOperator("Active", true)));
                    if (_locAcctPeriodic != null)
                    {
                        this.JournalMonth = _locAcctPeriodic.Month;
                        this.JournalYear = _locAcctPeriodic.Year;
                    }
                    else
                    {
                        this.JournalMonth = 0;
                        this.JournalYear = 0;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Business Object = CashAdvance ", ex.ToString());
            }
        }

    }
}