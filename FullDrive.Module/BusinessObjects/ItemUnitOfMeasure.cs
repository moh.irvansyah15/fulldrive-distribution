﻿using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.ConditionalAppearance;

namespace FullDrive.Module.BusinessObjects
{
    [DefaultClassOptions]
    [DefaultProperty("Name")]
    [NavigationItem("Product")]
    [RuleCombinationOfPropertiesIsUnique("ItemUnitOfMeasureRuleUnique", DefaultContexts.Save, "Code")]
    //[ImageName("BO_Contact")]
    //[DefaultProperty("DisplayMemberNameForLookupEditorsOfThisType")]
    //[DefaultListViewOptions(MasterDetailMode.ListViewOnly, false, NewItemRowPosition.None)]
    //[Persistent("DatabaseTableName")]
    // Specify more UI options using a declarative approach (https://documentation.devexpress.com/#eXpressAppFramework/CustomDocument112701).
    public class ItemUnitOfMeasure : BaseObject
    { // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).
        private int _no;
        private string _code;
        private Company _company;
        private string _name;
        private Item _item;
        private UnitOfMeasure _uom;
        private Double _conversion;
        private UnitOfMeasure _defaultUOM;
        private Double _defaultConversion;
        private bool _active;
        private bool _default;
        private GlobalFunction _globFunc;
        //AuditTrail
        private XPCollection<AuditDataItemPersistent> changeHistory;

        public ItemUnitOfMeasure(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if (!IsLoading)
            {
                _globFunc = new GlobalFunction();
                this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.ItemUnitOfMeasure);
            }
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).
        }

        protected override void OnSaving()
        {
            base.OnSaving();
            if (!IsLoading && IsSaving)
            {
                UpdateNo();
            }
            if (this.Active == true)
            {
                CheckDefaultSystem();
            }
        }

        protected override void OnDeleting()
        {
            base.OnDeleting();
            RecoveryDeleteNo();
        }

        #region Field



        [Appearance("ItemUnitOfMeasureNoEnabled", Enabled = false)]
        public int No
        {
            get { return _no; }
            set { SetPropertyValue("No", ref _no, value); }
        }

        [RuleRequiredField(DefaultContexts.Save)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue("Code", ref _code, value); }
        }

        [RuleRequiredField(DefaultContexts.Save)]
        public Company Company
        {
            get { return _company; }
            set { SetPropertyValue("Company", ref _company, value); }
        }

        [RuleRequiredField(DefaultContexts.Save)]
        public string Name
        {
            get { return _name; }
            set { SetPropertyValue("Name", ref _name, value); }
        }

        [ImmediatePostData()]
        [RuleRequiredField(DefaultContexts.Save)]
        [Association("Item-ItemUnitOfMeasures")]
        public Item Item
        {
            get { return _item; }
            set
            {
                SetPropertyValue("Item", ref _item, value);
                if (!IsLoading)
                {
                    if (this._item != null)
                    {
                        if (this._item.BasedUOM != null)
                        {
                            this.DefaultUOM = _item.BasedUOM;
                        }
                        if(this._item.Company != null)
                        {
                            this.Company = this._item.Company;
                        }
                    }
                }
            }
        }

        [ImmediatePostData()]
        [DataSourceCriteria("Active = true")]
        public UnitOfMeasure UOM
        {
            get { return _uom; }
            set {
                SetPropertyValue("UOM", ref _uom, value);
                if(!IsLoading)
                {
                    if(this._uom != null)
                    {
                        if(this._uom.FullName != null)
                        {
                            this.Name = this._uom.FullName;
                        }
                    }
                }
            }
        }

        public Double Conversion
        {
            get { return _conversion; }
            set { SetPropertyValue("Conversion", ref _conversion, value); }
        }

        [DataSourceCriteria("Active = true")]
        public UnitOfMeasure DefaultUOM
        {
            get { return _defaultUOM; }
            set { SetPropertyValue("DefaultUOM", ref _defaultUOM, value); }
        }

        public Double DefaultConversion
        {
            get { return _defaultConversion; }
            set { SetPropertyValue("DefaultConversion", ref _defaultConversion, value); }
        }

        public bool Active
        {
            get { return _active; }
            set { SetPropertyValue("Active", ref _active, value); }
        }

        public bool Default
        {
            get { return _default; }
            set { SetPropertyValue("Default", ref _default, value); }
        }

        //AuditTrail
        [CollectionOperationSet(AllowAdd = false, AllowRemove = true)]
        public XPCollection<AuditDataItemPersistent> ChangeHistory
        {
            get
            {
                if (changeHistory == null)
                {
                    changeHistory = AuditedObjectWeakReference.GetAuditTrail(Session, this);
                }
                return changeHistory;
            }
        }

        #endregion Field

        //=============================================== Code In Here ===============================================

        #region Numbering

        public void UpdateNo()
        {
            try
            {
                if (!IsLoading && Session.IsNewObject(this))
                {
                    if (this.Item != null)
                    {
                        object _makRecord = Session.Evaluate<ItemUnitOfMeasure>(CriteriaOperator.Parse("Max(No)"), CriteriaOperator.Parse("Item=?", this.Item));
                        this.No = Convert.ToInt32(_makRecord) + 1;
                        this.Save();
                        RecoveryUpdateNo();
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = ItemUnitOfMeasure " + ex.ToString());
            }
        }

        public void RecoveryUpdateNo()
        {
            try
            {
                if (this.Item != null)
                {
                    Item _numHeader = Session.FindObject<Item>
                                                (new BinaryOperator("OID", this.Item.Oid));

                    XPCollection<ItemUnitOfMeasure> _numLines = new XPCollection<ItemUnitOfMeasure>
                                                (Session, new BinaryOperator("Item", _numHeader),
                                                new SortProperty("No", DevExpress.Xpo.DB.SortingDirection.Ascending));
                    if (_numLines != null)
                    {
                        int i1 = 0;
                        foreach (ItemUnitOfMeasure _numLine in _numLines)
                        {
                            i1 += 1;
                            _numLine.No = i1;
                            _numLine.Save();
                        }
                        i1 = 1;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = ItemUnitOfMeasure " + ex.ToString());
            }
        }

        public void RecoveryDeleteNo()
        {
            try
            {
                if (this.Item != null)
                {
                    Item _numHeader = Session.FindObject<Item>
                                                (new BinaryOperator("OID", this.Item.Oid));

                    XPCollection<ItemUnitOfMeasure> _numLines = new XPCollection<ItemUnitOfMeasure>
                                                (Session, new GroupOperator(GroupOperatorType.And,
                                                 new BinaryOperator("This", this, BinaryOperatorType.NotEqual),
                                                 new BinaryOperator("Item", _numHeader)),
                                                 new SortProperty("No", DevExpress.Xpo.DB.SortingDirection.Ascending));
                    if (_numLines != null)
                    {
                        int i = 0;
                        foreach (ItemUnitOfMeasure _numLine in _numLines)
                        {
                            i += 1;
                            _numLine.No = i;
                            _numLine.Save();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = ItemUnitOfMeasure " + ex.ToString());
            }
        }

        #endregion Numbering

        private void CheckDefaultSystem()
        {
            try
            {
                if (this.Company != null && this.Item != null)
                {
                    XPCollection<ItemUnitOfMeasure> _locIUOMs = new XPCollection<ItemUnitOfMeasure>(Session, new GroupOperator(GroupOperatorType.And,
                                                                            new BinaryOperator("Company", this.Company, BinaryOperatorType.Equal),
                                                                            new BinaryOperator("Item", this.Item, BinaryOperatorType.Equal),
                                                                            new BinaryOperator("This", this, BinaryOperatorType.NotEqual)));
                    if (_locIUOMs == null)
                    {
                        return;
                    }
                    else
                    {
                        foreach (ItemUnitOfMeasure _locIUOM in _locIUOMs)
                        {
                            _locIUOM.Active = false;
                            _locIUOM.Save();
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Business Object = PriceLine", ex.ToString());
            }
        }

    }
}