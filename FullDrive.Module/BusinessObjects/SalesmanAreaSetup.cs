﻿using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;

using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.ConditionalAppearance;

namespace FullDrive.Module.BusinessObjects
{
    [DefaultClassOptions]
    [DefaultProperty("Name")]
    [NavigationItem("Sales")]
    [RuleCombinationOfPropertiesIsUnique("SalesmanAreaSetupRuleUnique", DefaultContexts.Save, "Code")]
    //[ImageName("BO_Contact")]
    //[DefaultProperty("DisplayMemberNameForLookupEditorsOfThisType")]
    //[DefaultListViewOptions(MasterDetailMode.ListViewOnly, false, NewItemRowPosition.None)]
    //[Persistent("DatabaseTableName")]
    // Specify more UI options using a declarative approach (https://documentation.devexpress.com/#eXpressAppFramework/CustomDocument112701).
    public class SalesmanAreaSetup : BaseObject
    { // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).

        private int _no;
        private string _code;
        private string _name;
        private Company _company;
        private XPCollection<Workplace> _availableWorkplace;
        private Workplace _workplace;
        private XPCollection<Employee> _availableSalesman;
        private Employee _salesman;
        private SalesArea _salesArea;
        private SalesAreaLine _salesAreaLine;
        private bool _active;
        private string _userAccessIn;
        private GlobalFunction _globFunc;
        //AuditTrail
        private XPCollection<AuditDataItemPersistent> changeHistory;

        public SalesmanAreaSetup(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if (!IsLoading)
            {
                _globFunc = new GlobalFunction();
                #region UserAccess
                _userAccessIn = SecuritySystem.CurrentUserName;
                UserAccess _locUserAccess = Session.FindObject<UserAccess>(new BinaryOperator("UserName", _userAccessIn));
                if (Session.IsNewObject(this))
                {
                    if (_locUserAccess != null)
                    {
                        if (_locUserAccess.Employee != null)
                        {
                            if (_locUserAccess.Employee.Company != null && _locUserAccess.Employee.Workplace != null)
                            {
                                this.Code = _globFunc.GetNumberingUnlockOptimisticRecordBasedOrganization(this.Session.DataLayer, ObjectList.SalesmanAreaSetup, _locUserAccess.Employee.Company, _locUserAccess.Employee.Workplace);
                            }
                            else
                            {
                                this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.SalesmanAreaSetup);
                            }
                            if (_locUserAccess.Employee.Company != null)
                            {
                                Company = _locUserAccess.Employee.Company;
                            }
                            if (_locUserAccess.Employee.Workplace != null)
                            {
                                Workplace = _locUserAccess.Employee.Workplace;
                            }
                            else
                            {
                                this.Workplace = _globFunc.GetDefaultWorkplace(this.Session, _locUserAccess);
                            }
                        }  
                    }
                }
                #endregion UserAccess
            }
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).
        }

        protected override void OnSaving()
        {
            base.OnSaving();
            if (!IsLoading)
            {
                UpdateNo();
            }
        }

        protected override void OnDeleting()
        {
            base.OnDeleting();
            if (!IsLoading)
            {
                RecoveryDeleteNo();
            }
        }

        #region Field

        [Appearance("SalesmanAreaSetupNoEnabled", Enabled = false)]
        public int No
        {
            get { return _no; }
            set { SetPropertyValue("No", ref _no, value); }
        }

        [RuleRequiredField(DefaultContexts.Save)]
        [Appearance("SalesmanAreaSetupCodeClose", Enabled = false)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue("Code", ref _code, value); }
        }

        [RuleRequiredField(DefaultContexts.Save)]
        public string Name
        {
            get { return _name; }
            set { SetPropertyValue("Name", ref _name, value); }
        }

        [ImmediatePostData()]
        [RuleRequiredField(DefaultContexts.Save)]
        [DataSourceCriteria("Active = true")]
        public Company Company
        {
            get { return _company; }
            set { SetPropertyValue("Company", ref _company, value); }
        }

        [Browsable(false)]
        public XPCollection<Workplace> AvailableWorkplace
        {
            get
            {
                if(!IsLoading)
                {
                    if (this.Company != null)
                    {
                        _userAccessIn = SecuritySystem.CurrentUserName;
                        UserAccess _locUserAccess = Session.FindObject<UserAccess>(new BinaryOperator("UserName", _userAccessIn));
                        if (_locUserAccess != null)
                        {
                            OrganizationSetupDetail _locOrganizationSetupDetail = Session.FindObject<OrganizationSetupDetail>
                                                                            (new GroupOperator(GroupOperatorType.And,
                                                                            new BinaryOperator("Company", this.Company),
                                                                            new BinaryOperator("UserAccess", _locUserAccess),
                                                                            new BinaryOperator("OpenWorkplace", true),
                                                                            new BinaryOperator("Active", true),
                                                                            new BinaryOperator("Default", true)));

                            if (_locOrganizationSetupDetail != null)
                            {
                                if (_locOrganizationSetupDetail.Workplace != null)
                                {
                                    if (_locOrganizationSetupDetail.Workplace.Code != null)
                                    {
                                        _availableWorkplace = new XPCollection<Workplace>
                                                    (Session, new GroupOperator(GroupOperatorType.And,
                                                    new BinaryOperator("Code", _locOrganizationSetupDetail.Workplace.Code),
                                                    new BinaryOperator("Active", true)));
                                    }
                                }
                            }
                            else
                            {
                                OrganizationSetupDetail _locOrgSetDetail = Session.FindObject<OrganizationSetupDetail>
                                                                            (new GroupOperator(GroupOperatorType.And,
                                                                            new BinaryOperator("Company", this.Company),
                                                                            new BinaryOperator("UserAccess", _locUserAccess),
                                                                            new BinaryOperator("AllWorkplace", true),
                                                                            new BinaryOperator("Active", true)));
                                if (_locOrgSetDetail != null)
                                {
                                    _availableWorkplace = new XPCollection<Workplace>(Session,
                                                        new GroupOperator(GroupOperatorType.And,
                                                        new BinaryOperator("Company", this.Company),
                                                        new BinaryOperator("Active", true)));
                                }
                            }
                        }
                    }
                }
                
                return _availableWorkplace;

            }
        }

        [ImmediatePostData()]
        [DataSourceProperty("AvailableWorkplace", DataSourcePropertyIsNullMode.SelectAll)]
        public Workplace Workplace
        {
            get { return _workplace; }
            set { SetPropertyValue("Workplace", ref _workplace, value); }
        }

        [Browsable(false)]
        public XPCollection<Employee> AvailableSalesman
        {
            get
            {
                if(!IsLoading)
                {
                    if (this.Company != null && this.Workplace != null)
                    {
                        XPCollection<Employee> _locEmployees = new XPCollection<Employee>(Session,
                                                            new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("Company", this.Company),
                                                            new BinaryOperator("Workplace", this.Workplace),
                                                            new BinaryOperator("Active", true),
                                                            new GroupOperator(GroupOperatorType.Or,
                                                            new BinaryOperator("SalesRole", SalesRole.Canvassing),
                                                            new BinaryOperator("SalesRole", SalesRole.TakingOrder))));

                        if (_locEmployees != null && _locEmployees.Count() > 0)
                        {
                            _availableSalesman = _locEmployees;
                        }
                    }
                    else
                    {
                        XPCollection<Employee> _locEmployees = new XPCollection<Employee>(Session,
                                                            new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("Active", true),
                                                            new GroupOperator(GroupOperatorType.Or,
                                                            new BinaryOperator("SalesRole", SalesRole.Canvassing),
                                                            new BinaryOperator("SalesRole", SalesRole.TakingOrder))));

                        if (_locEmployees != null && _locEmployees.Count() > 0)
                        {
                            _availableSalesman = _locEmployees;
                        }
                    }
                }
                
                return _availableSalesman;
            }
        }

        [ImmediatePostData()]
        [Association("Employee-SalesmanAreaSetups")]
        [DataSourceProperty("AvailableSalesman", DataSourcePropertyIsNullMode.SelectAll)]
        public Employee Salesman
        {
            get { return _salesman; }
            set
            {
                SetPropertyValue("Salesman", ref _salesman, value);
                if (_salesman != null)
                {
                    if (this._salesman.Company != null)
                    {
                        this.Company = this._salesman.Company;
                    }
                    if (this._salesman.Workplace != null)
                    {
                        this.Workplace = this._salesman.Workplace;
                    }
                }
            }
        }


        [RuleRequiredField(DefaultContexts.Save)]
        [DataSourceCriteria("Active = true And Company = '@This.Company' And Workplace = '@This.Workplace'"), ImmediatePostData()]
        public SalesArea SalesArea
        {
            get { return _salesArea; }
            set { SetPropertyValue("SalesArea", ref _salesArea, value); }
        }

        [RuleRequiredField(DefaultContexts.Save)]
        [DataSourceCriteria("Active = true And SalesArea = '@This.SalesArea'"), ImmediatePostData()]
        public SalesAreaLine SalesAreaLine
        {
            get { return _salesAreaLine; }
            set {
                SetPropertyValue("SalesAreaLine", ref _salesAreaLine, value);
                if(!IsLoading)
                {
                    if(this._salesAreaLine != null)
                    {
                        if(this._salesAreaLine.Name != null)
                        {
                            this.Name = this._salesAreaLine.Name;
                        }
                    }
                }
            }
        }
        
        public bool Active
        {
            get { return _active; }
            set { SetPropertyValue("Active", ref _active, value); }
        }

        //AuditTrail
        [CollectionOperationSet(AllowAdd = false, AllowRemove = true)]
        public XPCollection<AuditDataItemPersistent> ChangeHistory
        {
            get
            {
                if (changeHistory == null)
                {
                    changeHistory = AuditedObjectWeakReference.GetAuditTrail(Session, this);
                }
                return changeHistory;
            }
        }

        #endregion Field

        //=============================================== Code In Here ===============================================
        #region CodeOnly

        #region Numbering

        public void UpdateNo()
        {
            try
            {
                if (!IsLoading && Session.IsNewObject(this))
                {
                    if (this.Salesman != null)
                    {
                        object _makRecord = Session.Evaluate<SalesmanAreaSetup>(CriteriaOperator.Parse("Max(No)"), CriteriaOperator.Parse("Salesman=?", this.Salesman));
                        this.No = Convert.ToInt32(_makRecord) + 1;
                        this.Save();
                        RecoveryUpdateNo();
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = SalesmanAreaSetup " + ex.ToString());
            }
        }

        public void RecoveryUpdateNo()
        {
            try
            {
                if (this.Salesman != null)
                {
                    Employee _numHeader = Session.FindObject<Employee>
                                                (new BinaryOperator("Code", this.Salesman.Code));

                    XPCollection<SalesmanAreaSetup> _numLines = new XPCollection<SalesmanAreaSetup>
                                                (Session, new BinaryOperator("Salesman", _numHeader),
                                                new SortProperty("No", DevExpress.Xpo.DB.SortingDirection.Ascending));
                    if (_numLines != null)
                    {
                        int i1 = 0;
                        foreach (SalesmanAreaSetup _numLine in _numLines)
                        {
                            i1 += 1;
                            _numLine.No = i1;
                            _numLine.Save();
                        }
                        i1 = 1;
                        Session.CommitTransaction();
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = SalesmanAreaSetup " + ex.ToString());
            }
        }

        public void RecoveryDeleteNo()
        {
            try
            {
                if (this.Salesman != null)
                {
                    Employee _numHeader = Session.FindObject<Employee>
                                                (new BinaryOperator("Code", this.Salesman.Code));

                    XPCollection<SalesmanAreaSetup> _numLines = new XPCollection<SalesmanAreaSetup>
                                                (Session, new GroupOperator(GroupOperatorType.And,
                                                 new BinaryOperator("This", this, BinaryOperatorType.NotEqual),
                                                 new BinaryOperator("Salesman", _numHeader)),
                                                 new SortProperty("No", DevExpress.Xpo.DB.SortingDirection.Ascending));
                    if (_numLines != null)
                    {
                        int i = 0;
                        foreach (SalesmanAreaSetup _numLine in _numLines)
                        {
                            i += 1;
                            _numLine.No = i;
                            _numLine.Save();
                        }
                        Session.CommitTransaction();
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = SalesmanAreaSetup " + ex.ToString());
            }
        }

        #endregion Numbering

        #endregion CodeOnly

    }
}