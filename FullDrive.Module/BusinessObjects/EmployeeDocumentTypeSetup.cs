﻿using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;

using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.ConditionalAppearance;

namespace FullDrive.Module.BusinessObjects
{
    [DefaultClassOptions]
    [DefaultProperty("Name")]
    [NavigationItem("Setup")]
    [RuleCombinationOfPropertiesIsUnique("EmployeeDocumentTypeSetupRuleUnique", DefaultContexts.Save, "Code")]
    //[ImageName("BO_Contact")]
    //[DefaultProperty("DisplayMemberNameForLookupEditorsOfThisType")]
    //[DefaultListViewOptions(MasterDetailMode.ListViewOnly, false, NewItemRowPosition.None)]
    //[Persistent("DatabaseTableName")]
    // Specify more UI options using a declarative approach (https://documentation.devexpress.com/#eXpressAppFramework/CustomDocument112701).
    public class EmployeeDocumentTypeSetup : BaseObject
    { // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).

        private int _no;
        private string _code;
        private string _name;
        private Company _company;
        private XPCollection<Workplace> _availableWorkplace;
        private Workplace _workplace;
        private Employee _employee;
        private UserAccess _userAccess;
        private DirectionType _transferType;
        private InventoryMovingType _inventoryMovingType;
        private ObjectList _objectList;
        private DocumentType _documentType;
        private bool _active;
        private bool _default;
        private string _userAccessIn;
        private GlobalFunction _globFunc;

        //AuditTrail
        private XPCollection<AuditDataItemPersistent> changeHistory;

        public EmployeeDocumentTypeSetup(Session session)
            : base(session)
        {
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).
            _globFunc = new GlobalFunction();
            #region UserAccess
            _userAccessIn = SecuritySystem.CurrentUserName;
            UserAccess _locUserAccess = Session.FindObject<UserAccess>(new BinaryOperator("UserName", _userAccessIn));
            if (Session.IsNewObject(this))
            {
                if (_locUserAccess != null)
                {
                    if (_locUserAccess.Employee != null)
                    {
                        if (_locUserAccess.Employee.Company != null && _locUserAccess.Employee.Workplace != null)
                        {
                            this.Code = _globFunc.GetNumberingUnlockOptimisticRecordBasedOrganization(this.Session.DataLayer, ObjectList.EmployeeDocumentTypeSetup, _locUserAccess.Employee.Company, _locUserAccess.Employee.Workplace);
                        }
                        else
                        {
                            this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.EmployeeDocumentTypeSetup);
                        }
                        if (_locUserAccess.Employee.Company != null)
                        {
                            Company = _locUserAccess.Employee.Company;
                        }
                        if (_locUserAccess.Employee.Workplace != null)
                        {
                            Workplace = _locUserAccess.Employee.Workplace;
                        }
                        else
                        {
                            this.Workplace = _globFunc.GetDefaultWorkplace(this.Session, _locUserAccess);
                        }
                    }
                }
            }
            #endregion UserAccess
        }
        protected override void OnSaving()
        {
            base.OnSaving();
            if (!IsLoading)
            {
                if (this.Default == true)
                {
                    CheckDefaultSystem();
                }
                UpdateNo();
            }
        }

        protected override void OnDeleting()
        {
            base.OnDeleting();
            if (!IsLoading)
            {
                RecoveryDeleteNo();
            }
        }

        #region Field

        [Appearance("EmployeeDocumentTypeSetupNoEnabled", Enabled = false)]
        public int No
        {
            get { return _no; }
            set { SetPropertyValue("No", ref _no, value); }
        }

        [RuleRequiredField(DefaultContexts.Save)]
        [Appearance("EmployeeDocumentTypeSetupCodeClose", Enabled = false)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue("Code", ref _code, value); }
        }

        public string Name
        {
            get { return _name; }
            set { SetPropertyValue("Name", ref _name, value); }
        }

        [ImmediatePostData()]
        [DataSourceCriteria("Active = true")]
        public Company Company
        {
            get { return _company; }
            set { SetPropertyValue("Company", ref _company, value); }
        }

        [Browsable(false)]
        public XPCollection<Workplace> AvailableWorkplace
        {

            get
            {
                if (this.Company != null)
                {
                    _userAccessIn = SecuritySystem.CurrentUserName;
                    UserAccess _locUserAccess = Session.FindObject<UserAccess>(new BinaryOperator("UserName", _userAccessIn));
                    if (_locUserAccess != null)
                    {
                        OrganizationSetupDetail _locOrganizationSetupDetail = Session.FindObject<OrganizationSetupDetail>
                                                                        (new GroupOperator(GroupOperatorType.And,
                                                                        new BinaryOperator("UserAccess", _locUserAccess),
                                                                        new BinaryOperator("OpenWorkplace", true),
                                                                        new BinaryOperator("Active", true),
                                                                        new BinaryOperator("Default", true)));

                        if (_locOrganizationSetupDetail != null)
                        {
                            if (_locOrganizationSetupDetail.Workplace != null)
                            {
                                if (_locOrganizationSetupDetail.Workplace.Code != null)
                                {
                                    _availableWorkplace = new XPCollection<Workplace>
                                                (Session, new GroupOperator(GroupOperatorType.And,
                                                new BinaryOperator("Code", _locOrganizationSetupDetail.Workplace.Code),
                                                new BinaryOperator("Active", true)));
                                }
                            }
                        }
                        else
                        {
                            OrganizationSetupDetail _locOrgSetDetail = Session.FindObject<OrganizationSetupDetail>
                                                                        (new GroupOperator(GroupOperatorType.And,
                                                                        new BinaryOperator("UserAccess", _locUserAccess),
                                                                        new BinaryOperator("AllWorkplace", true),
                                                                        new BinaryOperator("Active", true)));
                            if (_locOrgSetDetail != null)
                            {
                                _availableWorkplace = new XPCollection<Workplace>(Session,
                                                    new GroupOperator(GroupOperatorType.And,
                                                    new BinaryOperator("Company", this.Company),
                                                    new BinaryOperator("Active", true)));
                            }
                        }
                    }
                }

                return _availableWorkplace;

            }
        }

        [ImmediatePostData()]
        [DataSourceProperty("AvailableWorkplace", DataSourcePropertyIsNullMode.SelectAll)]
        public Workplace Workplace
        {
            get { return _workplace; }
            set { SetPropertyValue("Workplace", ref _workplace, value); }
        }

        [ImmediatePostData()]
        [RuleRequiredField(DefaultContexts.Save)]
        [Association("Employee-EmployeeDocumentTypeSetups")]
        public Employee Employee
        {
            get { return _employee; }
            set
            {
                SetPropertyValue("Employee", ref _employee, value);
                if (_employee != null)
                {
                    if (this._employee.Company != null)
                    {
                        this.Company = this._employee.Company;
                    }
                    if (this._employee.Workplace != null)
                    {
                        this.Workplace = this._employee.Workplace;
                        if (Session.IsNewObject(this))
                        {
                            UserAccess _locUserAccess = Session.FindObject<UserAccess>(
                                                    new GroupOperator(GroupOperatorType.And,
                                                    new BinaryOperator("Employee", this._employee),
                                                    new BinaryOperator("Company", this._employee.Company),
                                                    new BinaryOperator("Workplace", this._employee.Workplace)));
                            if (_locUserAccess != null)
                            {
                                this.UserAccess = _locUserAccess;
                            }
                        }
                    }
                }
            }
        }

        public UserAccess UserAccess
        {
            get { return _userAccess; }
            set { SetPropertyValue("UserAccess", ref _userAccess, value); }
        }

        [ImmediatePostData()]
        public DirectionType TransferType
        {
            get { return _transferType; }
            set { SetPropertyValue("TransferType", ref _transferType, value); }
        }

        [ImmediatePostData()]
        public InventoryMovingType InventoryMovingType
        {
            get { return _inventoryMovingType; }
            set { SetPropertyValue("InventoryMovingType", ref _inventoryMovingType, value); }
        }

        [ImmediatePostData()]
        public ObjectList ObjectList
        {
            get { return _objectList; }
            set { SetPropertyValue("ObjectList", ref _objectList, value); }
        }

        [ImmediatePostData()]
        [DataSourceCriteria("Company = '@This.Company' AND TransferType = '@This.TransferType' AND InventoryMovingType = '@This.InventoryMovingType' AND ObjectList = '@This.ObjectList' AND Active = true")]
        public DocumentType DocumentType
        {
            get { return _documentType; }
            set {
                SetPropertyValue("DocumentType", ref _documentType, value);
                if(!IsLoading)
                {
                    if(this._documentType != null)
                    {
                        if(this._documentType.Name != null)
                        {
                            this.Name = this._documentType.Name;
                        }
                        else
                        {
                            this.Name = null;
                        }
                    }
                    else
                    {
                        this.Name = null;
                    } 
                }
            }
        }

        public bool Active
        {
            get { return _active; }
            set { SetPropertyValue("Active", ref _active, value); }
        }

        public bool Default
        {
            get { return _default; }
            set { SetPropertyValue("Default", ref _default, value); }
        }

        //AuditTrail
        [CollectionOperationSet(AllowAdd = false, AllowRemove = true)]
        public XPCollection<AuditDataItemPersistent> ChangeHistory
        {
            get
            {
                if (changeHistory == null)
                {
                    changeHistory = AuditedObjectWeakReference.GetAuditTrail(Session, this);
                }
                return changeHistory;
            }
        }

        #endregion Field

        //========================================================================= Code Only =================================================================

        #region CodeOnly

        #region Numbering

        public void UpdateNo()
        {
            try
            {
                if (!IsLoading && Session.IsNewObject(this))
                {
                    if (this.Employee != null)
                    {
                        object _makRecord = Session.Evaluate<EmployeeDocumentTypeSetup>(CriteriaOperator.Parse("Max(No)"), CriteriaOperator.Parse("Employee=?", this.Employee));
                        this.No = Convert.ToInt32(_makRecord) + 1;
                        this.Save();
                        RecoveryUpdateNo();
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = EmployeeDocumentTypeSetup " + ex.ToString());
            }
        }

        public void RecoveryUpdateNo()
        {
            try
            {
                if (this.Employee != null)
                {
                    Employee _numHeader = Session.FindObject<Employee>
                                                (new BinaryOperator("Code", this.Employee.Code));

                    XPCollection<EmployeeDocumentTypeSetup> _numLines = new XPCollection<EmployeeDocumentTypeSetup>
                                                (Session, new BinaryOperator("Employee", _numHeader),
                                                new SortProperty("No", DevExpress.Xpo.DB.SortingDirection.Ascending));
                    if (_numLines != null)
                    {
                        int i1 = 0;
                        foreach (EmployeeDocumentTypeSetup _numLine in _numLines)
                        {
                            i1 += 1;
                            _numLine.No = i1;
                            _numLine.Save();
                        }
                        i1 = 1;
                        Session.CommitTransaction();
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = EmployeeDocumentTypeSetup " + ex.ToString());
            }
        }

        public void RecoveryDeleteNo()
        {
            try
            {
                if (this.Employee != null)
                {
                    Employee _numHeader = Session.FindObject<Employee>
                                                (new BinaryOperator("Code", this.Employee.Code));

                    XPCollection<EmployeeDocumentTypeSetup> _numLines = new XPCollection<EmployeeDocumentTypeSetup>
                                                (Session, new GroupOperator(GroupOperatorType.And,
                                                 new BinaryOperator("This", this, BinaryOperatorType.NotEqual),
                                                 new BinaryOperator("Employee", _numHeader)),
                                                 new SortProperty("No", DevExpress.Xpo.DB.SortingDirection.Ascending));
                    if (_numLines != null)
                    {
                        int i = 0;
                        foreach (EmployeeDocumentTypeSetup _numLine in _numLines)
                        {
                            i += 1;
                            _numLine.No = i;
                            _numLine.Save();
                        }
                        Session.CommitTransaction();
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = EmployeeDocumentTypeSetup " + ex.ToString());
            }
        }

        #endregion Numbering

        private void CheckDefaultSystem()
        {
            try
            {
                XPCollection<EmployeeDocumentTypeSetup> _locEmployeeDocumentTypeSetups = new XPCollection<EmployeeDocumentTypeSetup>(Session, new GroupOperator(GroupOperatorType.And,
                                                                                        new BinaryOperator("This", this, BinaryOperatorType.NotEqual)));

                if(_locEmployeeDocumentTypeSetups != null && _locEmployeeDocumentTypeSetups.Count() > 0)
                {
                    foreach (EmployeeDocumentTypeSetup _locEmployeeDocumentTypeSetup in _locEmployeeDocumentTypeSetups)
                    {
                        if(_locEmployeeDocumentTypeSetup.DocumentType == this.DocumentType && _locEmployeeDocumentTypeSetup.Employee == this.Employee && _locEmployeeDocumentTypeSetup.Default == true)
                        {
                            _locEmployeeDocumentTypeSetup.Default = false;
                            _locEmployeeDocumentTypeSetup.Save();
                        }   
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Business Object = EmployeeDocumentTypeSetup ", ex.ToString());
            }
        }

        private void SetName()
        {
            string _finalResult = null;
            string _locDocTypeName = null;
            string _locEmployeeName = null;
            
            try
            {
                if (this._documentType != null) { if (this._documentType.Name != null) { _locDocTypeName = "<" + this._documentType.Name + ">"; } }
                if (this._employee != null) { if (this._employee.Name != null) { _locEmployeeName = "<" + this._employee.Name + ">"; } }

                _finalResult = String.Format("{0}{1}", _locDocTypeName, _locEmployeeName).Trim();
                if (_finalResult != null)
                {
                    this._name = _finalResult;
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = EmployeeDocumenTypeSetup ", ex.ToString());
            }
        }

        #endregion CodeOnly
    }
}