﻿using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;

using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.ConditionalAppearance;

namespace FullDrive.Module.BusinessObjects
{
    [DefaultClassOptions]
    [DefaultProperty("Code")]
    [NavigationItem("Finance & Accounting")]
    [RuleCombinationOfPropertiesIsUnique("ExchangeRateRuleUnique", DefaultContexts.Save, "Code")]
    //[ImageName("BO_Contact")]
    //[DefaultProperty("DisplayMemberNameForLookupEditorsOfThisType")]
    //[DefaultListViewOptions(MasterDetailMode.ListViewOnly, false, NewItemRowPosition.None)]
    //[Persistent("DatabaseTableName")]
    // Specify more UI options using a declarative approach (https://documentation.devexpress.com/#eXpressAppFramework/CustomDocument112701).
    public class ExchangeRate : BaseObject
    { // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).
        private string _code;
        private CurrencyRate _currencyFrom;
        private Double _rate1;
        private CurrencyRate _currencyTo;
        private Double _rate2;
        private Double _billDebit;
        private Double _billCredit;
        private Double _rate;
        private Double _foreignAmountDebit;
        private Double _foreignAmountCredit;
        private Currency _currencyDefault;
        private PayableTransactionLine _payableTransactionLine;
        //private ReceivableTransactionLine _receivableTransactionLine;
        private GlobalFunction _globFunc;
        //AuditTrail
        private XPCollection<AuditDataItemPersistent> changeHistory;

        public ExchangeRate(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if (!IsLoading)
            {
                if(Session.IsNewObject(this))
                {
                    _globFunc = new GlobalFunction();
                    this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.ExchangeRate);
                    DateTime now = DateTime.Now;
                    //this.CurrencyDefault = _globFunc.GetDefaultCurrency(Session);
                }
            }
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).
        }

        #region Field

        [RuleRequiredField(DefaultContexts.Save)]
        [Appearance("CurrencyRateCodeEnabled", Enabled = false)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue("Code", ref _code, value); }
        }

        [ImmediatePostData()]
        public CurrencyRate currencyFrom
        {
            get { return _currencyFrom; }
            set
            {
                SetPropertyValue("_currencyFrom", ref _currencyFrom, value);
                if (this._currencyFrom != null)
                {
                    this.Rate1 = this._currencyFrom.Rate2;
                }
                else
                {
                    this.Rate1 = 0;
                }
            }
        }

        [Appearance("ExchangeRateRate1Enabled", Enabled = false)]
        [ImmediatePostData()]
        public Double Rate1
        {
            get { return _rate1; }
            set { SetPropertyValue("Rate1", ref _rate1, value); }
        }

        [ImmediatePostData()]
        public CurrencyRate CurrencyTo
        {
            get { return _currencyTo; }
            set
            {
                SetPropertyValue("currencyTo", ref _currencyTo, value);
                if (this._currencyTo != null)
                {
                    this.Rate2 = this._currencyTo.Rate2;
                }
                else
                {
                    this.Rate2 = 0;
                }
            }
        }

        [Appearance("ExchangeRateRate2Enabled", Enabled = false)]
        [ImmediatePostData()]
        public Double Rate2
        {
            get { return _rate2; }
            set
            {
                SetPropertyValue("Rate2", ref _rate2, value);
                if (!IsLoading)
                {
                    SetRate();
                }
            }
        }

        [Appearance("ExchangeRateBillDebitEnabled", Enabled = false)]
        [ImmediatePostData()]
        public Double BillDebit
        {
            get { return _billDebit; }
            set { SetPropertyValue("BillDebit", ref _billDebit, value); }
        }

        [Appearance("ExchangeRateBillCreditEnabled", Enabled = false)]
        [ImmediatePostData()]
        public Double BillCredit
        {
            get { return _billCredit; }
            set { SetPropertyValue("BillCredit", ref _billCredit, value); }
        }

        [Appearance("ExchangeRateRateEnabled", Enabled = false)]
        [ImmediatePostData()]
        public Double Rate
        {
            get { return _rate; }
            set
            {
                SetPropertyValue("Rate", ref _rate, value);
                if (!IsLoading)
                {
                    SetForeignAmount();
                }
            }
        }

        [Appearance("ExchangeRateForeignAmountDebitEnabled", Enabled = false)]
        [ImmediatePostData()]
        public Double ForeignAmountDebit
        {
            get { return _foreignAmountDebit; }
            set { SetPropertyValue("ForeignAmountDebit", ref _foreignAmountDebit, value); }
        }

        [Appearance("ExchangeRateForeignAmountCreditEnabled", Enabled = false)]
        [ImmediatePostData()]
        public Double ForeignAmountCredit
        {
            get { return _foreignAmountCredit; }
            set { SetPropertyValue("ForeignAmountCredit", ref _foreignAmountCredit, value); }
        }

        [Browsable(false)]
        [Appearance("ReceivableLineCurrencyDefaultEnabled", Enabled = false)]
        public Currency CurrencyDefault
        {
            get { return _currencyDefault; }
            set { SetPropertyValue("CurrencyDefault", ref _currencyDefault, value); }
        }

        [Appearance("ExchangeRatePayableTransactionLineEnabled", Enabled = false)]
        [Association("PayableTransactionLine-ExchangeRates")]
        public PayableTransactionLine PayableTransactionLine
        {
            get { return _payableTransactionLine; }
            set
            {
                SetPropertyValue("PayableTransactionLine", ref _payableTransactionLine, value);
                if (!IsLoading)
                {
                    if (this._payableTransactionLine != null)
                    {
                        this.BillDebit = this._payableTransactionLine.Debit;
                        this.BillCredit = this._payableTransactionLine.Credit;
                    }
                }
            }
        }

        //[Appearance("ExchangeRateReceivableTransactionLineEnabled", Enabled = false)]
        //[Association("ReceivableTransactionLine-ExchangeRates")]
        //public ReceivableTransactionLine ReceivableTransactionLine
        //{
        //    get { return _receivableTransactionLine; }
        //    set
        //    {
        //        SetPropertyValue("ReceivableTransactionLine", ref _receivableTransactionLine, value);
        //        if (!IsLoading)
        //        {
        //            if (this._receivableTransactionLine != null)
        //            {
        //                this.BillDebit = this._receivableTransactionLine.Debit;
        //                this.BillCredit = this._receivableTransactionLine.Credit;
        //            }
        //        }
        //    }
        //}

        //AuditTrail
        [CollectionOperationSet(AllowAdd = false, AllowRemove = true)]
        public XPCollection<AuditDataItemPersistent> ChangeHistory
        {
            get
            {
                if (changeHistory == null)
                {
                    changeHistory = AuditedObjectWeakReference.GetAuditTrail(Session, this);
                }
                return changeHistory;
            }
        }

        #endregion Field

        //================================================== Code Only ==================================================

        public void SetRate()
        {
            try
            {
                if (!IsLoading)
                {
                    if (_currencyFrom != null && _currencyTo != null && _billDebit >= 0 && _billCredit >= 0)
                    {
                        if (_rate2 == 1)
                        {
                            this.Rate = this.Rate1;
                        }
                        else if (_rate2 >= 1)
                        {
                            this.Rate = this.Rate2;
                        }
                        else
                        {
                            this.Rate = 0;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = ExchangeRate " + ex.ToString());
            }
        }

        public void SetForeignAmount()
        {
            try
            {
                if (!IsLoading)
                {
                    if (_currencyFrom != null && _currencyTo != null && _billDebit >= 0 && _billCredit >= 0 && _rate1 >= 0 && _rate2 >= 0)
                    {
                        if (_rate1 == 1)
                        {
                            this.ForeignAmountDebit = this.BillDebit / this.Rate2;
                            this.ForeignAmountCredit = this.BillCredit / this.Rate2;
                        }
                        else if (_rate1 > 1)
                        {
                            this.ForeignAmountDebit = this.BillDebit * this.Rate1;
                            this.ForeignAmountCredit = this.BillCredit * this.Rate1;
                        }
                        else
                        {
                            this.ForeignAmountDebit = 0;
                            this.ForeignAmountCredit = 0;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = ExchangeRate " + ex.ToString());
            }
        }

    }
}