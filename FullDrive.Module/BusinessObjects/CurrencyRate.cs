﻿using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;

using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.ConditionalAppearance;

namespace FullDrive.Module.BusinessObjects
{
    [DefaultClassOptions]
    [DefaultProperty("FullName")]
    [NavigationItem("Rate")]
    [RuleCombinationOfPropertiesIsUnique("CurrencyRateRuleUnique", DefaultContexts.Save, "Code")]
    //[ImageName("BO_Contact")]
    //[DefaultProperty("DisplayMemberNameForLookupEditorsOfThisType")]
    //[DefaultListViewOptions(MasterDetailMode.ListViewOnly, false, NewItemRowPosition.None)]
    //[Persistent("DatabaseTableName")]
    // Specify more UI options using a declarative approach (https://documentation.devexpress.com/#eXpressAppFramework/CustomDocument112701).
    public class CurrencyRate : BaseObject
    { // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).
        private string _code;
        //private DateTime _effectiveDate;
        private DateTime _startDate;
        private DateTime _endDate;
        private Currency _currency1;
        private Double _rate1;
        private Currency _currency2;
        private Double _rate2;
        private string _fullName;
        private bool _active;
        private GlobalFunction _globFunc;
        //AuditTrail
        private XPCollection<AuditDataItemPersistent> changeHistory;

        public CurrencyRate(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if (!IsLoading)
            {
                _globFunc = new GlobalFunction();
                this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.CurrencyRate);
            }
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).
        }

        [RuleRequiredField(DefaultContexts.Save)]
        [ImmediatePostData()]
        public string Code
        {
            get { return _code; }
            set
            {
                SetPropertyValue("Code", ref _code, value);
                if (!IsLoading)
                {
                    if (this._code != null)
                    {
                        SetFullName();
                    }
                }
            }
        }

        //[ModelDefault("DisplayFormat", "{0:dd/MM/yy H:mm:ss}")]
        //[ModelDefault("EditMask", "dd/MM/yy H:mm:ss")]
        //[EditorAlias(FullDriveCenterEditorAliases.CustomDateTimeEditor)]
        //public DateTime EffectiveDate
        //{
        //    get { return _effectiveDate; }
        //    set { SetPropertyValue("EffectiveDate", ref _effectiveDate, value); }
        //}

        [ModelDefault("DisplayFormat", "{0:dd/MM/yy H:mm:ss}")]
        [ModelDefault("EditMask", "dd/MM/yy H:mm:ss")]
        [EditorAlias(FullDriveCenterEditorAliases.CustomDateTimeEditor)]
        public DateTime StartDate
        {
            get { return _startDate; }
            set { SetPropertyValue("StartDate", ref _startDate, value); }
        }

        [ModelDefault("DisplayFormat", "{0:dd/MM/yy H:mm:ss}")]
        [ModelDefault("EditMask", "dd/MM/yy H:mm:ss")]
        [EditorAlias(FullDriveCenterEditorAliases.CustomDateTimeEditor)]
        public DateTime EndDate
        {
            get { return _endDate; }
            set { SetPropertyValue("EndDate", ref _endDate, value); }
        }

        [Association("Currency-CurrencyRates")]
        [DataSourceCriteria("Active = true"), ImmediatePostData()]
        public Currency Currency1
        {
            get { return _currency1; }
            set
            {
                SetPropertyValue("Currency1", ref _currency1, value);
                if (!IsLoading)
                {
                    if (this._currency1 != null)
                    {
                        SetFullName();
                    }
                }
            }
        }

        [ImmediatePostData()]
        public Double Rate1
        {
            get { return _rate1; }
            set
            {
                SetPropertyValue("Rate1", ref _rate1, value);
                if (!IsLoading)
                {
                    if (this._currency1 != null)
                    {
                        //this._rate1 = this._currency1.CurrentRate;
                        SetFullName();
                    }

                }
            }
        }

        [DataSourceCriteria("Active = true"), ImmediatePostData()]
        public Currency Currency2
        {
            get { return _currency2; }
            set
            {
                SetPropertyValue("Currency2", ref _currency2, value);
                if (!IsLoading)
                {
                    if (this._currency2 != null)
                    {
                        SetFullName();
                    }
                }
            }
        }

        [ImmediatePostData()]
        public Double Rate2
        {
            get { return _rate2; }
            set
            {
                SetPropertyValue("Rate2", ref _rate2, value);
                if (!IsLoading)
                {
                    SetFullName();
                }
            }
        }

        [Appearance("CurrencyRateFullNameEnabled", Enabled = false)]
        public string FullName
        {
            get { return _fullName; }
            set { SetPropertyValue("FullName", ref _fullName, value); }
        }

        public bool Active
        {
            get { return _active; }
            set { SetPropertyValue("Active", ref _active, value); }
        }

        //AuditTrail
        [CollectionOperationSet(AllowAdd = false, AllowRemove = true)]
        public XPCollection<AuditDataItemPersistent> ChangeHistory
        {
            get
            {
                if (changeHistory == null)
                {
                    changeHistory = AuditedObjectWeakReference.GetAuditTrail(Session, this);
                }
                return changeHistory;
            }
        }

        //private string _PersistentProperty;
        //[XafDisplayName("My display name"), ToolTip("My hint message")]
        //[ModelDefault("EditMask", "(000)-00"), Index(0), VisibleInListView(false)]
        //[Persistent("DatabaseColumnName"), RuleRequiredField(DefaultContexts.Save)]
        //public string PersistentProperty {
        //    get { return _PersistentProperty; }
        //    set { SetPropertyValue("PersistentProperty", ref _PersistentProperty, value); }
        //}

        //[Action(Caption = "My UI Action", ConfirmationMessage = "Are you sure?", ImageName = "Attention", AutoCommit = true)]
        //public void ActionMethod() {
        //    // Trigger a custom business logic for the current record in the UI (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112619.aspx).
        //    this.PersistentProperty = "Paid";
        //}

        //=============================================== Code In Here ===============================================

        #region SetFullName
        private void SetFullName()
        {
            try
            {
                if (this._code != null)
                {
                    this.FullName = String.Format("({0}) Conversion ({1} {2}) to ({3} {4})", this.Code, this.Rate1, this.Currency1, this.Rate2, this.Currency2);
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = CurrencyRate ", ex.ToString());
            }
        }
        #endregion

    }
}