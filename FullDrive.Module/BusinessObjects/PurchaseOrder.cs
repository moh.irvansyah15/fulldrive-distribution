﻿using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;

using DevExpress.ExpressApp.Editors;
using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.ConditionalAppearance;

namespace FullDrive.Module.BusinessObjects
{
    [DefaultClassOptions]
    [DefaultProperty("Code")]
    [NavigationItem("Purchase")]
    [RuleCombinationOfPropertiesIsUnique("PurchaseOrderRuleUnique", DefaultContexts.Save, "Code")]
    //[ImageName("BO_Contact")]
    //[DefaultProperty("DisplayMemberNameForLookupEditorsOfThisType")]
    //[DefaultListViewOptions(MasterDetailMode.ListViewOnly, false, NewItemRowPosition.None)]
    //[Persistent("DatabaseTableName")]
    // Specify more UI options using a declarative approach (https://documentation.devexpress.com/#eXpressAppFramework/CustomDocument112701).
    public class PurchaseOrder : BaseObject
    { // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).
        private bool _activationPosting;
        private string _code;
        private ObjectList _objectList;
        XPCollection<NumberingLine> _availableNumberingLine;
        private NumberingLine _numbering;
        #region InitialBusinessPartner
        private BusinessPartner _buyFromVendor;
        private string _buyFromContact;
        private Country _buyFromCountry;
        private City _buyFromCity;
        private string _buyFromAddress;
        private DateTime _orderDate;
        private string _vendorOrderNo;
        private string _vendorShipmentNo;
        private string _vendorInvoiceNo;        
        private string _taxNo;
        #endregion InitialBusinessPartner
        private DirectionType _transferType;
        private TermOfPayment _top;
        private string _remarkTOP;
        private Currency _currency;
        private PaymentMethod _paymentMethod; 
        private double _dp_percentage;
        private DateTime _estimatedDate;
        private DateTime _docDate;
        private string _djp_No;
        private int _journalMonth;
        private int _journalYear;
        private Status _status;
        private DateTime _statusDate;              
        private string _remarks;
        #region InitialOrganization       
        private Company _company;
        private Workplace _workplace;
        private Division _division;
        private Department _department;
        private Section _section;
        private Employee _employee;
        private Division _div;
        private Department _dept;
        private Section _sect;
        #endregion InitialOrganization
        #region InisialisasiAmount 
        private double _totAmount;
        private double _totUnitAmount;
        private double _totTaxAmount;
        private double _totDiscAmount;
        private double _amount;    
        #endregion InisialisasiAmount
        private string _localUserAccess;
        private GlobalFunction _globFunc;
        //Approved
        private bool _activeApproved1;
        private bool _activeApproved2;
        private bool _activeApproved3;
        //AuditTrail
        private XPCollection<AuditDataItemPersistent> changeHistory;

        public PurchaseOrder(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if (!IsLoading)
            {
                if(Session.IsNewObject(this))
                {
                    _globFunc = new GlobalFunction();
                    DateTime now = DateTime.Now;
                    #region UserAccess
                    _localUserAccess = SecuritySystem.CurrentUserName;
                    UserAccess _locUserAccess = Session.FindObject<UserAccess>(new BinaryOperator("UserName", _localUserAccess));
                    if (_locUserAccess != null)
                    {
                        if (_locUserAccess.Employee != null)
                        {
                            if (_locUserAccess.Employee.Company != null && _locUserAccess.Employee.Workplace != null)
                            {
                                this.Code = _globFunc.GetNumberingUnlockOptimisticRecordBasedOrganization(this.Session.DataLayer, ObjectList.PurchaseOrder, _locUserAccess.Employee.Company, _locUserAccess.Employee.Workplace);
                            }
                            else
                            {
                                this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.PurchaseOrder);
                            }
                            if (_locUserAccess.Employee.Company != null)
                            {
                                this.Company = _locUserAccess.Employee.Company;
                            }
                            if (_locUserAccess.Employee.Workplace != null)
                            {
                                this.Workplace = _locUserAccess.Employee.Workplace;
                            }
                            if (_locUserAccess.Employee.Division != null)
                            {
                                this.Division = _locUserAccess.Employee.Division;
                                this.Div = _locUserAccess.Employee.Division;
                            }
                            if (_locUserAccess.Employee.Department != null)
                            {
                                this.Department = _locUserAccess.Employee.Department;
                                this.Dept = _locUserAccess.Employee.Department;
                            }
                            if (_locUserAccess.Employee.Section != null)
                            {
                                Section = _locUserAccess.Employee.Section;
                                this.Sect = _locUserAccess.Employee.Section;
                            }
                            this.Employee = _locUserAccess.Employee;
                        }
                    }
                    #endregion UserAccess
                    this.Status = CustomProcess.Status.Open;
                    this.StatusDate = now;
                    this.OrderDate = now;
                    this.TransferType = CustomProcess.DirectionType.External;
                    this.ObjectList = CustomProcess.ObjectList.PurchaseOrder;
                    this.Currency = _globFunc.GetDefaultCurrency(this.Session);
                    this.DocDate = now;
                    this.JournalMonth = this.DocDate.Month;
                    this.JournalYear = this.DocDate.Year;
                }
            }
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).
        }

        #region Field

        [Browsable(false)]
        [ImmediatePostData()]
        public bool ActivationPosting
        {
            get { return _activationPosting; }
            set { SetPropertyValue("ActivationPosting", ref _activationPosting, value); }
        }

        [ImmediatePostData()]
        [Appearance("PurchaseOrderCodeClose", Enabled = false)]
        [Appearance("PurchaseOrderRedColor", Criteria = "ActiveApproved1 = true", BackColor = "#fd79a8")]
        [Appearance("PurchaseOrderYellowColor", Criteria = "ActiveApproved2 = true", BackColor = "#ffeaa7")]
        [Appearance("PurchaseOrderGreenOColor", Criteria = "ActiveApproved3 = true", BackColor = "#81ecec")]
        [RuleRequiredField(DefaultContexts.Save)]
        [Appearance("PurchaseOrderCodeClose", Enabled = false)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue("Code", ref _code, value); }
        }

        [Browsable(false)]
        public ObjectList ObjectList
        {
            get { return _objectList; }
            set { SetPropertyValue("ObjectList", ref _objectList, value); }
        }

        [Browsable(false)]
        public XPCollection<NumberingLine> AvailableNumberingLine
        {
            get
            {
                if(!IsLoading)
                {
                    if (ObjectList == ObjectList.None)
                    {
                        _availableNumberingLine = new XPCollection<NumberingLine>(Session);
                    }
                    else
                    {
                        _availableNumberingLine = new XPCollection<NumberingLine>(Session,
                                                  new GroupOperator(GroupOperatorType.And,
                                                  new BinaryOperator("ObjectList", this.ObjectList),
                                                  new BinaryOperator("Selection", true),
                                                  new BinaryOperator("Active", true)));
                    }
                }                
                return _availableNumberingLine;
            }
        }

        [NonPersistent()]
        [ImmediatePostData()]
        [DataSourceProperty("AvailableNumberingLine", DataSourcePropertyIsNullMode.SelectAll)]
        public NumberingLine Numbering
        {
            get { return _numbering; }
            set
            {
                NumberingLine _oldNumbering = _numbering;
                SetPropertyValue("Numbering", ref _numbering, value);
                if (!IsLoading)
                {
                    if (_numbering != null)
                    {
                        if (this.Session != null)
                        {
                            if (this.Session.DataLayer != null)
                            {
                                this.Code = LocGetNumberingSelectionUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.PurchaseOrder, _numbering);
                            }
                        }
                    }
                }
            }
        }

        #region BusinessPartner

        [ImmediatePostData()]
        [DataSourceCriteria("Active = true")]
        [Appearance("PurchaseOrderBuyFromVendorClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public BusinessPartner BuyFromVendor
        {
            get { return _buyFromVendor; }
            set
            {
                SetPropertyValue("BuyFromVendor", ref _buyFromVendor, value);
                if (!IsLoading)
                {
                    if (this._buyFromVendor != null)
                    {
                        if(this._buyFromVendor.Contact != null)
                        {
                            this.BuyFromContact = this._buyFromVendor.Contact;
                        }
                        if(this._buyFromVendor.Country != null)
                        {
                            this.BuyFromCountry = this._buyFromVendor.Country;
                        }
                        if(this._buyFromVendor.City != null)
                        {
                            this.BuyFromCity = this._buyFromVendor.City;
                        }
                        if (this._buyFromVendor.Address != null)
                        {
                            this.BuyFromAddress = this._buyFromVendor.Address;
                        }
                        if (this._buyFromVendor.TaxNo != null)
                        {
                            this.TaxNo = this._buyFromVendor.TaxNo;
                        }
                        if(this._buyFromVendor.TOP != null)
                        {
                            this.TOP = this._buyFromVendor.TOP;
                        }                       
                    }
                    else
                    {
                        this.BuyFromContact = null;
                        this.BuyFromCountry = null;
                        this.BuyFromCity = null;
                        this.BuyFromAddress = null;
                        this.TaxNo = null;
                        this.TOP = null;
                    }
                }
            }
        }

        [Appearance("PurchaseOrderBuyFromContactClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public string BuyFromContact
        {
            get { return _buyFromContact; }
            set { SetPropertyValue("BuyFromContact", ref _buyFromContact, value); }
        }

        [DataSourceCriteria("Active = true")]
        [Appearance("PurchaseOrderBuyFromCountryClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Country BuyFromCountry
        {
            get { return _buyFromCountry; }
            set { SetPropertyValue("BuyFromCountry", ref _buyFromCountry, value); }
        }

        [DataSourceCriteria("Active = true")]
        [Appearance("PurchaseOrderBuyFromCityClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public City BuyFromCity
        {
            get { return _buyFromCity; }
            set { SetPropertyValue("BuyFromCity", ref _buyFromCity, value); }
        }

        [Size(512)]
        [Appearance("PurchaseOrderBuyFromAddressClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public string BuyFromAddress
        {
            get { return _buyFromAddress; }
            set { SetPropertyValue("BuyFromAddress", ref _buyFromAddress, value); }
        }

        [ModelDefault("DisplayFormat", "{0:dd/MM/yyyy}")]
        [ModelDefault("EditMask", "dd/MM/yyyy")]
        [Appearance("PurchaseOrderOrderDateClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public DateTime OrderDate
        {
            get { return _orderDate; }
            set { SetPropertyValue("OrderDate", ref _orderDate, value); }
        }

        [Appearance("PurchaseOrderVendorOrderNoClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public string VendorOrderNo
        {
            get { return _vendorOrderNo; }
            set { SetPropertyValue("VendorOrderNo", ref _vendorOrderNo, value); }
        }

        [Appearance("PurchaseOrderVendorShipmentNoClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public string VendorShipmentNo
        {
            get { return _vendorShipmentNo; }
            set { SetPropertyValue("VendorShipmentNo", ref _vendorShipmentNo, value); }
        }

        [Appearance("PurchaseOrderVendorInvoiceNoClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public string VendorInvoiceNo
        {
            get { return _vendorInvoiceNo; }
            set { SetPropertyValue("VendorInvoiceNo", ref _vendorInvoiceNo, value); }
        }

        [Appearance("PurchaseOrderTaxNoClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public string TaxNo
        {
            get { return _taxNo; }
            set { SetPropertyValue("TaxNo", ref _taxNo, value); }
        }

        #endregion BusinessPartner

        [Appearance("PurchaseOrderTransferTypeClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public DirectionType TransferType
        {
            get { return _transferType; }
            set { SetPropertyValue("TransferType", ref _transferType, value); }
        }

        [DataSourceCriteria("Active = true")]
        [Appearance("PurchaseOrderTOPClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public TermOfPayment TOP
        {
            get { return _top; }
            set { SetPropertyValue("TOP", ref _top, value); }
        }

        [Appearance("PurchaseOrderRemarkTOPClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public string RemarkTOP
        {
            get { return _remarkTOP; }
            set { SetPropertyValue("RemarkTOP", ref _remarkTOP, value); }
        }

        [Appearance("PurchaseOrderCurrencyClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Currency Currency
        {
            get { return _currency; }
            set { SetPropertyValue("Currency", ref _currency, value); }
        }

        [DataSourceCriteria("Active = true")]
        [ImmediatePostData()]
        [Appearance("PurchaseOrderPaymentMethodClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public PaymentMethod PaymentMethod
        {
            get { return _paymentMethod; }
            set { SetPropertyValue("PaymentMethod", ref _paymentMethod, value); }
        }

        //menghitung total di line terus di kalikan 
        [ImmediatePostData()]
        [Appearance("PurchaseOrderDP_PercentageClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public double DP_Percentage
        {
            get { return _dp_percentage; }
            set { SetPropertyValue("DP_Percentage", ref _dp_percentage, value); }
        }

        [ModelDefault("DisplayFormat", "{0:dd/MM/yyyy}")]
        [ModelDefault("EditMask", "dd/MM/yyyy")]
        [Appearance("PurchaseOrderEstimatedDateClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public DateTime EstimatedDate
        {
            get { return _estimatedDate; }
            set { SetPropertyValue("EstimatedDate", ref _estimatedDate, value); }
        }

        public string DJP_No
        {
            get { return _djp_No; }
            set { SetPropertyValue("DJP_No", ref _djp_No, value); }
        }

        [ImmediatePostData()]
        [ModelDefault("DisplayFormat", "{0:dd/MM/yyyy}")]
        [ModelDefault("EditMask", "dd/MM/yyyy")]
        [Appearance("PurchaseOrderDocDateClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public DateTime DocDate
        {
            get { return _docDate; }
            set
            {
                SetPropertyValue("DocDate", ref _docDate, value);
                if (!IsLoading)
                {
                    this.JournalMonth = this._docDate.Month;
                    this.JournalYear = this._docDate.Year;
                }
            }
        }

        [Browsable(false)]
        public int JournalMonth
        {
            get { return _journalMonth; }
            set { SetPropertyValue("JournalMonth", ref _journalMonth, value); }
        }

        [Browsable(false)]
        public int JournalYear
        {
            get { return _journalYear; }
            set { SetPropertyValue("JournalYear", ref _journalYear, value); }
        }

        [Appearance("PurchaseOrderStatusClose", Enabled = false)]
        public Status Status
        {
            get { return _status; }
            set { SetPropertyValue("Status", ref _status, value); }
        }

        [ModelDefault("DisplayFormat", "{0:dd/MM/yyyy}")]
        [ModelDefault("EditMask", "dd/MM/yyyy")]
        [Appearance("PurchaseOrderStatusDateClose", Enabled = false)]
        public DateTime StatusDate
        {
            get { return _statusDate; }
            set { SetPropertyValue("StatusDate", ref _statusDate, value); }
        }      

        [Appearance("PurchaseOrderRemarksClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [Size(512)]
        public string Remarks
        {
            get { return _remarks; }
            set { SetPropertyValue("Remarks", ref _remarks, value); }
        }

        #region Organization

        [Appearance("PurchaseOrderCompanyClose", Enabled = false)]
        public Company Company
        {
            get { return _company; }
            set { SetPropertyValue("Company", ref _company, value); }
        }

        [Appearance("PurchaseOrderWorkplaceClose", Enabled = false)]
        public Workplace Workplace
        {
            get { return _workplace; }
            set { SetPropertyValue("Workplace", ref _workplace, value); }
        }

        [Browsable(false)]
        public Division Division
        {
            get { return _division; }
            set { SetPropertyValue("Division", ref _division, value); }
        }

        [Browsable(false)]
        public Department Department
        {
            get { return _department; }
            set { SetPropertyValue("Department", ref _department, value); }
        }

        [Browsable(false)]
        public Section Section
        {
            get { return _section; }
            set { SetPropertyValue("Section", ref _section, value); }
        }

        [Browsable(false)]
        public Employee Employee
        {
            get { return _employee; }
            set { SetPropertyValue("Employee", ref _employee, value); }
        }

        [ImmediatePostData()]
        [DataSourceCriteria("Company = '@This.Company' And Active = true")]
        [Appearance("PurchaseOrderDivClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Division Div
        {
            get { return _div; }
            set { SetPropertyValue("Div", ref _div, value); }
        }

        [ImmediatePostData()]
        [DataSourceCriteria("Company = '@This.Company' And Division = '@This.Div' And Active = true")]
        [Appearance("PurchaseOrderDeptClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Department Dept
        {
            get { return _dept; }
            set { SetPropertyValue("Dept", ref _dept, value); }
        }

        [ImmediatePostData()]
        [DataSourceCriteria("Company = '@This.Company' And Department = '@This.Dept' And Active = true")]
        [Appearance("PurchaseOrderSectClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Section Sect
        {
            get { return _sect; }
            set { SetPropertyValue("Sect", ref _sect, value); }
        }

        #endregion Organization

        #region Amount

        [Appearance("PurchaseOrderTotAmountClose", Enabled = false)]
        public double TotAmount
        {
            get { return _totAmount; }
            set { SetPropertyValue("TotAmount", ref _totAmount, value); }
        }

        [Appearance("PurchaseOrderTotUnitAmountClose", Enabled = false)]
        public double TotUnitAmount
        {
            get { return _totUnitAmount; }
            set { SetPropertyValue("TotUnitAmount", ref _totUnitAmount, value); }
        }

        [Appearance("PurchaseOrderTotTaxAmountClose", Enabled = false)]
        public double TotTaxAmount
        {
            get { return _totTaxAmount; }
            set { SetPropertyValue("TotTaxAmount", ref _totTaxAmount, value); }
        }

        [Appearance("PurchaseOrderTotDiscAmountClose", Enabled = false)]
        public double TotDiscAmount
        {
            get { return _totDiscAmount; }
            set { SetPropertyValue("TotDiscAmount", ref _totDiscAmount, value); }
        }

        //[Appearance("PurchaseOrderAmountClose", Enabled = false)]
        public double Amount
        {
            get { return _amount; }
            set { SetPropertyValue("Amount", ref _amount, value); }
        }

        #endregion Amount

        #region Approved

        [Browsable(false)]
        [Appearance("PurchaseOrderActiveApproved1Close", Enabled = false)]
        public bool ActiveApproved1
        {
            get { return _activeApproved1; }
            set { SetPropertyValue("ActiveApproved1", ref _activeApproved1, value); }
        }

        [Browsable(false)]
        [Appearance("PurchaseOrderActiveApproved2Close", Enabled = false)]
        public bool ActiveApproved2
        {
            get { return _activeApproved2; }
            set { SetPropertyValue("ActiveApproved2", ref _activeApproved2, value); }
        }

        [Browsable(false)]
        [Appearance("PurchaseOrderActiveApproved3Close", Enabled = false)]
        public bool ActiveApproved3
        {
            get { return _activeApproved3; }
            set { SetPropertyValue("ActiveApproved3", ref _activeApproved3, value); }
        }

        #endregion Approved

        [Association("PurchaseOrder-PurchaseOrderLines")]
        public XPCollection<PurchaseOrderLine> PurchaseOrderLines
        {
            get { return GetCollection<PurchaseOrderLine>("PurchaseOrderLines"); }
        }

        [Association("PurchaseOrder-ApprovalLines")]
        public XPCollection<ApprovalLine> ApprovalLines
        {
            get { return GetCollection<ApprovalLine>("ApprovalLines"); }
        }

        [Association("PurchaseOrder-PurchaseRequisitionCollections")]
        public XPCollection<PurchaseRequisitionCollection> PurchaseRequisitionCollections
        {
            get { return GetCollection<PurchaseRequisitionCollection>("PurchaseRequisitionCollections"); }
        }

        //AuditTrail
        [CollectionOperationSet(AllowAdd = false, AllowRemove = true)]
        public XPCollection<AuditDataItemPersistent> ChangeHistory
        {
            get
            {
                if (changeHistory == null)
                {
                    changeHistory = AuditedObjectWeakReference.GetAuditTrail(Session, this);
                }
                return changeHistory;
            }
        }

        #endregion Field

        //=============================================== Code Only ===============================================

        #region LocalNumbering

        public string LocGetNumberingSelectionUnlockOptimisticRecord(IDataLayer _currIDataLayer, ObjectList _currObject, NumberingLine _currNumberingLine)
        {
            string _result = null;
            Session _generatorSession = null;
            try
            {
                if (_currIDataLayer != null)
                {
                    _generatorSession = new Session(_currIDataLayer);
                }

                if (_generatorSession != null)
                {
                    ApplicationSetup _appSetup = _generatorSession.FindObject<ApplicationSetup>(new GroupOperator(GroupOperatorType.And,
                                            new BinaryOperator("Active", true), new BinaryOperator("DefaultSystem", true)));
                    if (_appSetup != null)
                    {
                        NumberingHeader _numberingHeader = _generatorSession.FindObject<NumberingHeader>
                                                            (new GroupOperator(GroupOperatorType.And,
                                                             new BinaryOperator("NumberingType", NumberingType.Objects),
                                                             new BinaryOperator("ApplicationSetup", _appSetup, BinaryOperatorType.Equal),
                                                             new BinaryOperator("Active", true)));
                        if (_numberingHeader != null)
                        {
                            NumberingLine _numberingLine = _generatorSession.FindObject<NumberingLine>
                                                        (new GroupOperator(GroupOperatorType.And,
                                                         new BinaryOperator("Oid", _currNumberingLine.Oid),
                                                         new BinaryOperator("ObjectList", _currObject),
                                                         new BinaryOperator("Selection", true),
                                                         new BinaryOperator("Active", true),
                                                         new BinaryOperator("NumberingHeader", _numberingHeader)));
                            if (_numberingLine != null)
                            {
                                _numberingLine.LastValue = _numberingLine.LastValue + _numberingLine.IncrementValue;
                                _numberingLine.Save();
                                _result = _numberingLine.FormatedValue;
                                _numberingLine.Session.CommitTransaction();
                            }

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Module = GlobalFunction " + ex.ToString());
            }
            return _result;
        }

        #endregion LocalNumbering

    }
}