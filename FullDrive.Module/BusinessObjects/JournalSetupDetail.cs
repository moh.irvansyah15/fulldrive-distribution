﻿using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;

using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.ConditionalAppearance;

namespace FullDrive.Module.BusinessObjects
{
    [DefaultClassOptions]
    [DefaultProperty("Code")]
    [NavigationItem("Setup")]
    [RuleCombinationOfPropertiesIsUnique("JournalSetupDetailRuleUnique", DefaultContexts.Save, "Code")]
    //[ImageName("BO_Contact")]
    //[DefaultProperty("DisplayMemberNameForLookupEditorsOfThisType")]
    //[DefaultListViewOptions(MasterDetailMode.ListViewOnly, false, NewItemRowPosition.None)]
    //[Persistent("DatabaseTableName")]
    // Specify more UI options using a declarative approach (https://documentation.devexpress.com/#eXpressAppFramework/CustomDocument112701).
    public class JournalSetupDetail : BaseObject
    { // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).
        private string _code;
        private Company _company;
        private XPCollection<Workplace> _availableWorkplace;
        private Workplace _workplace;
        private JournalSetupType _journalSetupType;
        private ObjectList _objectList;
        private bool _openJournal;
        private JournalProcess _journalProcess;
        private bool _openCosting;
        private CostingMethod _costingMethod;
        private bool _active;
        private ApplicationSetup _applicationSetup;
        private string _userAccessIn;
        private GlobalFunction _globFunc;
        //AuditTrail
        private XPCollection<AuditDataItemPersistent> changeHistory;

        public JournalSetupDetail(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if (!IsLoading)
            {
                _globFunc = new GlobalFunction();
                #region UserAccess
                _userAccessIn = SecuritySystem.CurrentUserName;
                UserAccess _locUserAccess = Session.FindObject<UserAccess>(new BinaryOperator("UserName", _userAccessIn));

                if (_locUserAccess != null)
                {
                    if (_locUserAccess.Employee != null)
                    {
                        if (_locUserAccess.Employee.Company != null && _locUserAccess.Employee.Workplace != null)
                        {
                            this.Code = _globFunc.GetNumberingUnlockOptimisticRecordBasedOrganization(this.Session.DataLayer, ObjectList.JournalSetupDetail, _locUserAccess.Employee.Company, _locUserAccess.Employee.Workplace);
                        }
                        else
                        {
                            this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.JournalSetupDetail);
                        }
                        if (_locUserAccess.Employee.Company != null)
                        {
                            this.Company = _locUserAccess.Employee.Company;
                        }
                        if (_locUserAccess.Employee.Workplace != null)
                        {
                            this.Workplace = _locUserAccess.Employee.Workplace;
                        }
                        else
                        {
                            this.Workplace = _globFunc.GetDefaultWorkplace(this.Session, _locUserAccess);
                        }
                    }
                }
                #endregion UserAccess
            }
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).
        }

        #region Field

        [RuleRequiredField(DefaultContexts.Save)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue("Code", ref _code, value); }
        }

        [ImmediatePostData()]
        [DataSourceCriteria("Active = true ")]
        public Company Company
        {
            get { return _company; }
            set { SetPropertyValue("Company", ref _company, value); }
        }

        [Browsable(false)]
        public XPCollection<Workplace> AvailableWorkplace
        {
            get
            {
                if (this.Company != null)
                {
                    _availableWorkplace = new XPCollection<Workplace>(Session,
                                        new GroupOperator(GroupOperatorType.And,
                                        new BinaryOperator("Company", this.Company),
                                        new BinaryOperator("Active", true)));
                }
                else
                {
                    _availableWorkplace = new XPCollection<Workplace>(Session,
                                        new GroupOperator(GroupOperatorType.And,
                                        new BinaryOperator("Active", true)));
                }

                return _availableWorkplace;

            }
        }

        [DataSourceProperty("AvailableWorkplace", DataSourcePropertyIsNullMode.SelectAll)]
        public Workplace Workplace
        {
            get { return _workplace; }
            set { SetPropertyValue("Workplace", ref _workplace, value); }
        }

        [ImmediatePostData()]
        public JournalSetupType JournalSetupType
        {
            get { return _journalSetupType; }
            set {
                SetPropertyValue("JournalSetupType", ref _journalSetupType, value);
                if(!IsLoading)
                {
                    if(this._journalSetupType == JournalSetupType.None)
                    {
                        this.OpenJournal = false;
                        this.OpenCosting = false;
                    }
                    if(this._journalSetupType == JournalSetupType.Costing)
                    {
                        this.OpenJournal = false;
                        this.OpenCosting = true;
                    }
                    if (this._journalSetupType == JournalSetupType.Journal)
                    {
                        this.OpenJournal = true;
                        this.OpenCosting = false;
                    }
                }
            }
        }

        public ObjectList ObjectList
        {
            get { return _objectList; }
            set { SetPropertyValue("ObjectList", ref _objectList, value); }
        }

        [Browsable(false)]
        [ImmediatePostData()]
        public bool OpenJournal
        {
            get { return _openJournal; }
            set { SetPropertyValue("OpenJournal", ref _openJournal, value); }
        }

        [Appearance("JournalSetupDetailJournalProcessClose", Criteria = "OpenJournal = false", Enabled = false)]
        public JournalProcess JournalProcess
        {
            get { return _journalProcess; }
            set { SetPropertyValue("JournalProcess", ref _journalProcess, value); }
        }

        [Browsable(false)]
        [ImmediatePostData()]
        public bool OpenCosting
        {
            get { return _openCosting; }
            set { SetPropertyValue("OpenCosting", ref _openCosting, value); }
        }

        [Appearance("JournalSetupDetailCostingMethodClose", Criteria = "OpenCosting = false", Enabled = false)]
        public CostingMethod CostingMethod
        {
            get { return _costingMethod; }
            set { SetPropertyValue("CostingMethod", ref _costingMethod, value); }
        }

        public bool Active
        {
            get { return _active; }
            set { SetPropertyValue("Active", ref _active, value); }
        }

        [Appearance("JournalSetupDetailApplicationSetupEnabled", Enabled = false)]
        [RuleRequiredField(DefaultContexts.Save)]
        [Association("ApplicationSetup-JournalSetupDetails")]
        public ApplicationSetup ApplicationSetup
        {
            get { return _applicationSetup; }
            set { SetPropertyValue("ApplicationSetup", ref _applicationSetup, value); }
        }

        //AuditTrail
        [CollectionOperationSet(AllowAdd = false, AllowRemove = true)]
        public XPCollection<AuditDataItemPersistent> ChangeHistory
        {
            get
            {
                if (changeHistory == null)
                {
                    changeHistory = AuditedObjectWeakReference.GetAuditTrail(Session, this);
                }
                return changeHistory;
            }
        }

        #endregion Field

    }
}