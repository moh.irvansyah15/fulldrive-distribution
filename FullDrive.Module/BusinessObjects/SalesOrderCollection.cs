﻿using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;

using DevExpress.ExpressApp.Editors;
using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.ConditionalAppearance;

namespace FullDrive.Module.BusinessObjects
{
    [DefaultClassOptions]
    [DefaultProperty("Code")]
    [NavigationItem("Sales")]
    [RuleCombinationOfPropertiesIsUnique("SalesOrderCollectionRuleUnique", DefaultContexts.Save, "Code")]
    //[ImageName("BO_Contact")]
    //[DefaultProperty("DisplayMemberNameForLookupEditorsOfThisType")]
    //[DefaultListViewOptions(MasterDetailMode.ListViewOnly, false, NewItemRowPosition.None)]
    //[Persistent("DatabaseTableName")]
    // Specify more UI options using a declarative approach (https://documentation.devexpress.com/#eXpressAppFramework/CustomDocument112701).
    public class SalesOrderCollection : BaseObject
    { // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).

        private bool _activationPosting;
        private string _code;
        #region InitialOrganization
        private Company _company;
        private Workplace _workplace;
        private Division _division;
        private Department _department;
        private Section _section;
        private Employee _employee;
        private Division _div;
        private Department _dept;
        private Section _sect;
        private XPCollection<Employee> _availablePIC;
        private Employee _pic;
        private XPCollection<Location> _availableLocation;
        private Location _location;
        private BeginningInventory _begInv;
        #endregion InitialOrganization
        private XPCollection<PriceGroup> _availablePriceGroup;
        private PriceGroup _priceGroup;
        private BusinessPartner _salesToCustomer;
        private XPCollection<SalesOrder> _availableSalesOrder;
        private SalesOrder _salesOrder;
        private DiscountRule _discountRule;
        private Status _status;
        private DateTime _statusDate;
        private SalesInvoice _salesInvoice;
        private string _localUserAccess;
        private GlobalFunction _globFunc;
        //AuditTrail
        private XPCollection<AuditDataItemPersistent> changeHistory;

        public SalesOrderCollection(Session session)
            : base(session)
        {
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if (!IsLoading)
            {
                if (Session.IsNewObject(this))
                {
                    _globFunc = new GlobalFunction();
                    DateTime now = DateTime.Now;
                    #region UserAccess
                    _localUserAccess = SecuritySystem.CurrentUserName;
                    UserAccess _locUserAccess = Session.FindObject<UserAccess>(new BinaryOperator("UserName", _localUserAccess));
                    if (_locUserAccess != null)
                    {
                        if (_locUserAccess.Employee != null)
                        {
                            if (_locUserAccess.Employee.Company != null && _locUserAccess.Employee.Workplace != null)
                            {
                                this.Code = _globFunc.GetNumberingUnlockOptimisticRecordBasedOrganization(this.Session.DataLayer, ObjectList.SalesOrderCollection, _locUserAccess.Employee.Company, _locUserAccess.Employee.Workplace);
                            }
                            else
                            {
                                this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.SalesOrderCollection);
                            }
                        }
                    }
                    #endregion UserAccess
                    this.Status = Status.Open;
                    this.StatusDate = now;
                }
            }
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).
        }

        #region Field

        [Browsable(false)]
        [ImmediatePostData()]
        public bool ActivationPosting
        {
            get { return _activationPosting; }
            set { SetPropertyValue("ActivationPosting", ref _activationPosting, value); }
        }

        [RuleRequiredField(DefaultContexts.Save)]
        [Appearance("SalesOrderCollectionCodeClose", Enabled = false)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue("Code", ref _code, value); }
        }

        #region Organization

        [ImmediatePostData()]
        [RuleRequiredField(DefaultContexts.Save)]
        [DataSourceCriteria("Active = true")]
        [Appearance("SalesOrderCollectionCompanyClose", Enabled = false)]
        public Company Company
        {
            get { return _company; }
            set { SetPropertyValue("Company", ref _company, value); }
        }

        [ImmediatePostData()]
        [RuleRequiredField(DefaultContexts.Save)]
        [DataSourceCriteria("Active = true")]
        [Appearance("SalesOrderCollectionWorkplaceClose", Enabled = false)]
        public Workplace Workplace
        {
            get { return _workplace; }
            set { SetPropertyValue("Workplace", ref _workplace, value); }
        }

        [Browsable(false)]
        public Division Division
        {
            get { return _division; }
            set { SetPropertyValue("Division", ref _division, value); }
        }

        [Browsable(false)]
        public Department Department
        {
            get { return _department; }
            set { SetPropertyValue("Department", ref _department, value); }
        }

        [Browsable(false)]
        public Section Section
        {
            get { return _section; }
            set { SetPropertyValue("Section", ref _section, value); }
        }

        [Browsable(false)]
        public Employee Employee
        {
            get { return _employee; }
            set { SetPropertyValue("Employee", ref _employee, value); }
        }

        [ImmediatePostData()]
        [VisibleInListView(false)]
        [DataSourceCriteria("Company = '@This.Company' And Active = true")]
        [Appearance("SalesOrderCollectionDivClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Division Div
        {
            get { return _div; }
            set { SetPropertyValue("Div", ref _div, value); }
        }

        [ImmediatePostData()]
        [VisibleInListView(false)]
        [DataSourceCriteria("Company = '@This.Company' And Division = '@This.Div' And Active = true")]
        [Appearance("SalesOrderCollectionDeptClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Department Dept
        {
            get { return _dept; }
            set { SetPropertyValue("Dept", ref _dept, value); }
        }

        [ImmediatePostData()]
        [VisibleInListView(false)]
        [DataSourceCriteria("Company = '@This.Company' And Department = '@This.Dept' And Active = true")]
        [Appearance("SalesOrderCollectionSectClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Section Sect
        {
            get { return _sect; }
            set { SetPropertyValue("Sect", ref _sect, value); }
        }

        [Browsable(false)]
        public XPCollection<Employee> AvailablePIC
        {
            get
            {
                XPCollection<Employee> _locPICs = null;
                if (this.Company != null && this.Workplace != null)
                {
                    #region DivDeptSect
                    if (this.Div != null && this.Dept != null && this.Sect != null)
                    {
                        _locPICs = new XPCollection<Employee>
                                  (Session, new GroupOperator(GroupOperatorType.And,
                                    new BinaryOperator("Company", this.Company),
                                    new BinaryOperator("Workplace", this.Workplace),
                                    new BinaryOperator("Division", this.Div),
                                    new BinaryOperator("Department", this.Dept),
                                    new BinaryOperator("Section", this.Sect),
                                    new BinaryOperator("Active", true),
                                    new GroupOperator(GroupOperatorType.Or,
                                    new BinaryOperator("SalesRole", SalesRole.TakingOrder),
                                    new BinaryOperator("SalesRole", SalesRole.All))));
                    }
                    else if (this.Div != null && this.Dept != null && this.Sect == null)
                    {
                        _locPICs = new XPCollection<Employee>
                                  (Session, new GroupOperator(GroupOperatorType.And,
                                    new BinaryOperator("Company", this.Company),
                                    new BinaryOperator("Workplace", this.Workplace),
                                    new BinaryOperator("Division", this.Div),
                                    new BinaryOperator("Department", this.Dept),
                                    new BinaryOperator("Active", true),
                                    new GroupOperator(GroupOperatorType.Or,
                                    new BinaryOperator("SalesRole", SalesRole.TakingOrder),
                                    new BinaryOperator("SalesRole", SalesRole.All))));

                    }
                    else if (this.Div != null && this.Dept == null && this.Sect == null)
                    {
                        _locPICs = new XPCollection<Employee>
                                  (Session, new GroupOperator(GroupOperatorType.And,
                                    new BinaryOperator("Company", this.Company),
                                    new BinaryOperator("Workplace", this.Workplace),
                                    new BinaryOperator("Division", this.Div),
                                    new BinaryOperator("Active", true),
                                    new GroupOperator(GroupOperatorType.Or,
                                    new BinaryOperator("SalesRole", SalesRole.TakingOrder),
                                    new BinaryOperator("SalesRole", SalesRole.All))));

                    }
                    else if (this.Div == null && this.Dept == null && this.Sect == null)
                    {
                        _locPICs = new XPCollection<Employee>
                                  (Session, new GroupOperator(GroupOperatorType.And,
                                    new BinaryOperator("Company", this.Company),
                                    new BinaryOperator("Workplace", this.Workplace),
                                    new BinaryOperator("Active", true),
                                    new GroupOperator(GroupOperatorType.Or,
                                    new BinaryOperator("SalesRole", SalesRole.TakingOrder),
                                    new BinaryOperator("SalesRole", SalesRole.All))));

                    }
                    #endregion DivDeptSect
                    if (_locPICs != null && _locPICs.Count() > 0)
                    {
                        _availablePIC = _locPICs;
                    }
                }
                else
                {
                    _availablePIC = new XPCollection<Employee>(Session, new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("Active", true),
                                                            new GroupOperator(GroupOperatorType.Or,
                                                            new BinaryOperator("SalesRole", SalesRole.TakingOrder),
                                                            new BinaryOperator("SalesRole", SalesRole.All))));
                }

                return _availablePIC;

            }
        }

        [ImmediatePostData()]
        [DataSourceProperty("AvailablePIC")]
        [Appearance("SalesOrderCollectionPICClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Employee PIC
        {
            get { return _pic; }
            set {
                SetPropertyValue("InternalPIC", ref _pic, value);
                if (!IsLoading)
                {
                    if (this._pic != null)
                    {
                        SetLocation(_pic);
                    }
                    else
                    {
                        this.Location = null;
                    }
                }
            }
        }

        [Browsable(false)]
        public XPCollection<Location> AvailableLocation
        {
            get
            {
                string _beginString = null;
                string _endString = null;
                string _fullString = null;

                if (this.Company != null && this.Workplace != null && this.PIC != null)
                {
                    List<string> _stringSLS = new List<string>();

                    XPCollection<SalesmanLocationSetup> _locSalesmanLocationSetups = new XPCollection<SalesmanLocationSetup>
                                                                              (Session, new GroupOperator(GroupOperatorType.And,
                                                                               new BinaryOperator("Company", this.Company),
                                                                               new BinaryOperator("Workplace", this.Workplace),
                                                                               new BinaryOperator("Salesman", this.PIC),
                                                                               new BinaryOperator("Active", true)));

                    if (_locSalesmanLocationSetups != null && _locSalesmanLocationSetups.Count() > 0)
                    {
                        foreach (SalesmanLocationSetup _locSalesmanLocationSetup in _locSalesmanLocationSetups)
                        {
                            if (_locSalesmanLocationSetup.Location != null)
                            {
                                if (_locSalesmanLocationSetup.Location.Code != null)
                                {
                                    _stringSLS.Add(_locSalesmanLocationSetup.Location.Code);
                                }
                            }
                        }
                    }

                    IEnumerable<string> _stringArraySLSDistinct = _stringSLS.Distinct();
                    string[] _stringArraySLSList = _stringArraySLSDistinct.ToArray();
                    if (_stringArraySLSList.Length == 1)
                    {
                        for (int i = 0; i < _stringArraySLSList.Length; i++)
                        {
                            Location _locLoc = Session.FindObject<Location>(new BinaryOperator("Code", _stringArraySLSList[i]));
                            if (_locLoc != null)
                            {
                                if (i == 0)
                                {
                                    _beginString = "[Code]=='" + _locLoc.Code + "'";
                                }
                            }
                        }
                    }
                    else if (_stringArraySLSList.Length > 1)
                    {
                        for (int i = 0; i < _stringArraySLSList.Length; i++)
                        {
                            Location _locLoc = Session.FindObject<Location>(new BinaryOperator("Code", _stringArraySLSList[i]));
                            if (_locLoc != null)
                            {
                                if (i == 0)
                                {
                                    _beginString = "[Code]=='" + _locLoc.Code + "'";
                                }
                                else
                                {
                                    _endString = _endString + " OR [Code]=='" + _locLoc.Code + "'";
                                }
                            }
                        }
                    }
                    _fullString = _beginString + _endString;

                    if (_fullString != null)
                    {
                        _availableLocation = new XPCollection<Location>(Session, CriteriaOperator.Parse(_fullString));
                    }

                }
                else
                {
                    if (this.Company != null && this.Workplace != null)
                    {
                        _availableLocation = new XPCollection<Location>(Session, new GroupOperator(GroupOperatorType.And,
                                                                            new BinaryOperator("Company", this.Company),
                                                                            new BinaryOperator("Workplace", this.Workplace),
                                                                            new BinaryOperator("LocationType", LocationType.Main),
                                                                            new BinaryOperator("Active", true)));
                    }
                }

                return _availableLocation;

            }
        }

        [ImmediatePostData()]
        [DataSourceProperty("AvailableLocation")]
        [Appearance("SalesInvoiceLocationClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Location Location
        {
            get { return _location; }
            set
            {
                SetPropertyValue("Location", ref _location, value);
                if (!IsLoading)
                {
                    if (this._location != null)
                    {
                        SetBegInv(_location);
                    }
                    else
                    {
                        BegInv = null;
                    }
                }
            }

        }

        [Browsable(false)]
        [ImmediatePostData()]
        [Appearance("SalesInvoiceBegInvClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public BeginningInventory BegInv
        {
            get { return _begInv; }
            set { SetPropertyValue("BegInv", ref _begInv, value); }
        }

        #endregion Organization

        [Browsable(false)]
        public XPCollection<PriceGroup> AvailablePriceGroup
        {
            get
            {
                string _beginString = null;
                string _endString = null;
                string _fullString = null;


                if (this.Company != null && this.Workplace != null && this.PIC != null)
                {
                    List<string> _stringSCS = new List<string>();

                    XPQuery<SalesmanCustomerSetup> _salesmenCustomerSetupssQuery = new XPQuery<SalesmanCustomerSetup>(Session);

                    var _salesmanCustomerSetups = from scs in _salesmenCustomerSetupssQuery
                                                  where (scs.Company == this.Company
                                                  && scs.Workplace == this.Workplace
                                                  && scs.Salesman == this.PIC
                                                  )
                                                  group scs by scs.PriceGroup into g
                                                  select new { PriceGroup = g.Key };


                    if (_salesmanCustomerSetups != null && _salesmanCustomerSetups.Count() > 0)
                    {
                        foreach (var _salesmanCustomerSetup in _salesmanCustomerSetups)
                        {
                            if (_salesmanCustomerSetup.PriceGroup != null)
                            {
                                if (_salesmanCustomerSetup.PriceGroup.Code != null)
                                {
                                    _stringSCS.Add(_salesmanCustomerSetup.PriceGroup.Code);
                                }
                            }
                        }
                    }

                    IEnumerable<string> _stringArraySCSDistinct = _stringSCS.Distinct();
                    string[] _stringArraySCSList = _stringArraySCSDistinct.ToArray();
                    if (_stringArraySCSList.Length == 1)
                    {
                        for (int i = 0; i < _stringArraySCSList.Length; i++)
                        {
                            PriceGroup _locPG = Session.FindObject<PriceGroup>(new BinaryOperator("Code", _stringArraySCSList[i]));
                            if (_locPG != null)
                            {
                                if (i == 0)
                                {
                                    _beginString = "[Code]=='" + _locPG.Code + "'";
                                }
                            }
                        }
                    }
                    else if (_stringArraySCSList.Length > 1)
                    {
                        for (int i = 0; i < _stringArraySCSList.Length; i++)
                        {
                            PriceGroup _locPG = Session.FindObject<PriceGroup>(new BinaryOperator("Code", _stringArraySCSList[i]));
                            if (_locPG != null)
                            {
                                if (i == 0)
                                {
                                    _beginString = "[Code]=='" + _locPG.Code + "'";
                                }
                                else
                                {
                                    _endString = _endString + " OR [Code]=='" + _locPG.Code + "'";
                                }
                            }
                        }
                    }
                    _fullString = _beginString + _endString;

                    if (_fullString != null)
                    {
                        _availablePriceGroup = new XPCollection<PriceGroup>(Session, CriteriaOperator.Parse(_fullString));
                    }

                }
                else
                {
                    _availablePriceGroup = new XPCollection<PriceGroup>(Session, new GroupOperator(GroupOperatorType.And,
                                                                            new BinaryOperator("Active", true)));
                }

                return _availablePriceGroup;

            }
        }

        [ImmediatePostData()]
        [DataSourceProperty("AvailablePriceGroup", DataSourcePropertyIsNullMode.SelectAll)]
        [Appearance("SalesOrderCollectionPriceGroupClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public PriceGroup PriceGroup
        {
            get { return _priceGroup; }
            set { SetPropertyValue("PriceGroup", ref _priceGroup, value); }
        }

        //[Browsable(false)]
        [ImmediatePostData()]
        [Appearance("SalesOrderCollectionSalesToCustomerClose", Enabled = false)]
        public BusinessPartner SalesToCustomer
        {
            get { return _salesToCustomer; }
            set { SetPropertyValue("SalesToCustomer", ref _salesToCustomer, value); }
        }

        [Browsable(false)]
        public XPCollection<SalesOrder> AvailableSalesOrder
        {
            get
            {
                string _beginString = null;
                string _endString = null;
                string _fullString = null;

                if (!IsLoading)
                {
                    if (this.SalesInvoice != null)
                    {
                        if(this.Company != null && this.Workplace != null && this.PIC != null && this.Location != null && this.BegInv != null && this.PriceGroup != null && this.SalesToCustomer != null)
                        {
                            XPQuery<SalesOrderMonitoring> _salesOrderMonitoringsQuery = new XPQuery<SalesOrderMonitoring>(Session);

                            var _salesOrderMonitorings = from som in _salesOrderMonitoringsQuery
                                                         where ((som.Status == Status.Open || som.Status == Status.Posted)
                                                         && som.Company == this.Company
                                                         && som.Workplace == this.Workplace
                                                         && som.Location == this.Location
                                                         && som.BegInv == this.BegInv 
                                                         && som.PriceGroup == this.PriceGroup
                                                         && som.SalesOrder.SalesToCustomer == this.SalesToCustomer 
                                                         && som.SalesInvoice == null)
                                                         group som by som.SalesOrder into g
                                                         select new { SalesOrder = g.Key };

                            if (_salesOrderMonitorings != null && _salesOrderMonitorings.Count() > 0)
                            {
                                List<string> _stringSOM = new List<string>();

                                foreach (var _salesOrderMonitoring in _salesOrderMonitorings)
                                {
                                    if (_salesOrderMonitoring != null)
                                    {
                                        if (_salesOrderMonitoring.SalesOrder.Code != null)
                                        {
                                            _stringSOM.Add(_salesOrderMonitoring.SalesOrder.Code);
                                        }
                                    }
                                }

                                IEnumerable<string> _stringArraySOMDistinct = _stringSOM.Distinct();
                                string[] _stringArraySOMList = _stringArraySOMDistinct.ToArray();
                                if (_stringArraySOMList.Length == 1)
                                {
                                    for (int i = 0; i < _stringArraySOMList.Length; i++)
                                    {
                                        if (i == 0)
                                        {
                                            _beginString = "[Code]=='" + _stringArraySOMList[i] + "'";
                                        }
                                    }
                                }
                                else if (_stringArraySOMList.Length > 1)
                                {
                                    for (int i = 0; i < _stringArraySOMList.Length; i++)
                                    {
                                        if (i == 0)
                                        {
                                            _beginString = "[Code]=='" + _stringArraySOMList[i] + "'";
                                        }
                                        else
                                        {
                                            _endString = _endString + " OR [Code]=='" + _stringArraySOMList[i] + "'";
                                        }
                                    }
                                }
                                _fullString = _beginString + _endString;

                                if (_fullString != null)
                                {
                                    _availableSalesOrder = new XPCollection<SalesOrder>(Session, CriteriaOperator.Parse(_fullString));
                                }
                            }
                        }
                        else
                        {
                            if(this.Company != null && this.Workplace != null && this.SalesToCustomer != null)
                            {
                                _availableSalesOrder = new XPCollection<SalesOrder>(Session,
                                                            new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("Company", this.Company),
                                                            new BinaryOperator("Workplace", this.Workplace),
                                                            new BinaryOperator("SalesToCustomer", this.SalesToCustomer),
                                                            new BinaryOperator("Status", Status.Posted)));
                            }
                            else
                            {
                                _availableSalesOrder = new XPCollection<SalesOrder>(Session,
                                                            new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("Status", Status.Posted)));
                            }
                        }
                        
                    }
                }

                return _availableSalesOrder;
            }
        }

        [ImmediatePostData()]
        [DataSourceProperty("AvailableSalesOrder", DataSourcePropertyIsNullMode.SelectNothing)]
        [RuleRequiredField(DefaultContexts.Save)]
        public SalesOrder SalesOrder
        {
            get { return _salesOrder; }
            set {
                SetPropertyValue("SalesOrder", ref _salesOrder, value);
                if(!IsLoading)
                {
                    if(this._salesOrder != null)
                    {
                        this.DiscountRule = this._salesOrder.DiscountRule;
                    }
                }
            }
        }

        [Appearance("SalesOrderCollectionDiscountRuleClose", Enabled = false)]
        public DiscountRule DiscountRule
        {
            get { return _discountRule; }
            set { SetPropertyValue("DiscountRule", ref _discountRule, value); }
        }

        [Appearance("SalesOrderCollectionStatusClose", Enabled = false)]
        public Status Status
        {
            get { return _status; }
            set { SetPropertyValue("Status", ref _status, value); }
        }

        [ModelDefault("DisplayFormat", "{0:dd/MM/yyyy}")]
        [ModelDefault("EditMask", "dd/MM/yyyy")]
        [Appearance("SalesOrderCollectionStatusDateClose", Enabled = false)]
        public DateTime StatusDate
        {
            get { return _statusDate; }
            set { SetPropertyValue("StatusDate", ref _statusDate, value); }
        }

        [ImmediatePostData()]
        [RuleRequiredField(DefaultContexts.Save)]
        [Appearance("InvoiceCanvassingCollectionCanvassingCollectionClose", Enabled = false)]
        [Association("SalesInvoice-SalesOrderCollections")]
        public SalesInvoice SalesInvoice
        {
            get { return _salesInvoice; }
            set
            {
                SetPropertyValue("SalesInvoice", ref _salesInvoice, value);
                if (!IsLoading)
                {

                    if (this._salesInvoice != null)
                    {
                        if (Session.IsNewObject(this))
                        {
                            if (this._salesInvoice.Company != null) { this.Company = this._salesInvoice.Company; }
                            if (this._salesInvoice.Workplace != null) { this.Workplace = this._salesInvoice.Workplace; }
                            if (this._salesInvoice.Division != null) { this.Division = this._salesInvoice.Division; }
                            if (this._salesInvoice.Department != null) { this.Department = this._salesInvoice.Department; }
                            if (this._salesInvoice.Section != null) { this.Section = this._salesInvoice.Section; }
                            if (this._salesInvoice.Employee != null) { this.Employee = this._salesInvoice.Employee; }
                            if (this._salesInvoice.Div != null) { this.Div = this._salesInvoice.Div; }
                            if (this._salesInvoice.Dept != null) { this.Dept = this._salesInvoice.Dept; }
                            if (this._salesInvoice.Sect != null) { this.Sect = this._salesInvoice.Sect; }
                            if (this._salesInvoice.PIC != null) { this.PIC = this._salesInvoice.PIC; }
                            if (this._salesInvoice.Location != null) { this.Location = this._salesInvoice.Location; }
                            if (this._salesInvoice.BegInv != null) { this.BegInv = this._salesInvoice.BegInv; }
                            if (this._salesInvoice.PriceGroup != null) { this.PriceGroup = this._salesInvoice.PriceGroup; }
                            if (this._salesInvoice.SalesToCustomer != null) { this.SalesToCustomer = this._salesInvoice.SalesToCustomer; }
                        }
                    }
                }
            }
        }

        #endregion Field

        private void SetLocation(Employee _locPIC)
        {
            try
            {
                DateTime now = DateTime.Now;
                if (this.Company != null && this.Workplace != null)
                {
                    SalesmanLocationSetup _locSalesmanLocationSetup = Session.FindObject<SalesmanLocationSetup>
                                                        (new GroupOperator(GroupOperatorType.And,
                                                        new BinaryOperator("Company", this.Company),
                                                        new BinaryOperator("Workplace", this.Workplace),
                                                        new BinaryOperator("Salesman", _locPIC),
                                                        new BinaryOperator("Active", true),
                                                        new BinaryOperator("Default", true)));
                    if (_locSalesmanLocationSetup != null)
                    {
                        if (_locSalesmanLocationSetup.Location != null)
                        {

                            this.Location = _locSalesmanLocationSetup.Location;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Business Object = SalesInvoice ", ex.ToString());
            }

        }

        private void SetBegInv(Location _locLocation)
        {
            try
            {
                DateTime now = DateTime.Now;
                if (this.Company != null && this.Workplace != null)
                {
                    BeginningInventory _locBegInv = Session.FindObject<BeginningInventory>
                                                        (new GroupOperator(GroupOperatorType.And,
                                                        new BinaryOperator("Company", this.Company),
                                                        new BinaryOperator("Workplace", this.Workplace),
                                                        new BinaryOperator("Location", _locLocation),
                                                        new BinaryOperator("Active", true)));
                    if (_locBegInv != null)
                    {
                        this.BegInv = _locBegInv;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Business Object = SalesInvoice ", ex.ToString());
            }
        }
    }
}