﻿using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;

using DevExpress.ExpressApp.Editors;
using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.ConditionalAppearance;

namespace FullDrive.Module.BusinessObjects
{
    [DefaultClassOptions]
    [DefaultProperty("Code")]
    [NavigationItem("Sales")]
    [RuleCombinationOfPropertiesIsUnique("SalesInvoiceFreeItemCmpRuleUnique", DefaultContexts.Save, "Code")]
    //[ImageName("BO_Contact")]
    //[DefaultProperty("DisplayMemberNameForLookupEditorsOfThisType")]
    //[DefaultListViewOptions(MasterDetailMode.ListViewOnly, false, NewItemRowPosition.None)]
    //[Persistent("DatabaseTableName")]
    // Specify more UI options using a declarative approach (https://documentation.devexpress.com/#eXpressAppFramework/CustomDocument112701).
    public class SalesInvoiceFreeItemCmp : BaseObject
    { // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).
        private bool _activationPosting;
        private int _no;
        private string _code;
        #region InitialOrganization
        private Company _company;
        private Workplace _workplace;
        private Division _division;
        private Department _department;
        private Section _section;
        private Employee _employee;
        #endregion InitialOrganization
        private SalesInvoiceLineCmp _salesInvoiceLineCmp;
        private FreeItemRule _freeItemRule;
        private SalesInvoiceFreeItem _salesInvoiceFreeItem;
        #region InisialisasiFPQty
        private Item _fpItem;
        private XPCollection<UnitOfMeasure> _availableUnitOfMeasureFP;
        private double _fpDQty;
        private UnitOfMeasure _fpDUom;
        private double _fpQty;
        private UnitOfMeasure _fpUom;
        private double _fpTQty;
        private StockType _fpStockType;
        #endregion InisialisasiFPQty
        #region InitialAmount
        private Currency _currency;
        private PriceGroup _priceGroup;
        private PriceLine _priceLine;
        private double _uAmount;
        private double _tUAmount;
        #endregion InitialAmount
        #region InisialisasiNFPQty
        private XPCollection<UnitOfMeasure> _availableUnitOfMeasureNFP;
        private double _nFpDQty;
        private UnitOfMeasure _nFpDUom;
        private double _nFpQty;
        private UnitOfMeasure _nFpUom;
        private double _nFpTQty;
        #endregion InisialisasiNFPQty
        #region InitialAmount
        private double _nTUAmount;
        #endregion InitialAmount
        private DateTime _etd;
        private DateTime _eta;
        private DateTime _docDate;
        private int _journalMonth;
        private int _journalYear;
        private Status _status;
        private DateTime _statusDate;
        private string _signCode;
        private SalesInvoiceCmp _salesInvoiceCmp;
        private string _localUserAccess;
        private GlobalFunction _globFunc;
        //AuditTrail
        private XPCollection<AuditDataItemPersistent> changeHistory;

        public SalesInvoiceFreeItemCmp(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if (!IsLoading)
            {

                if (Session.IsNewObject(this))
                {
                    _globFunc = new GlobalFunction();
                    DateTime now = DateTime.Now;
                    #region UserAccess
                    _localUserAccess = SecuritySystem.CurrentUserName;
                    UserAccess _locUserAccess = Session.FindObject<UserAccess>(new BinaryOperator("UserName", _localUserAccess));
                    if (_locUserAccess != null)
                    {
                        if (_locUserAccess.Employee != null)
                        {
                            if (_locUserAccess.Employee.Company != null && _locUserAccess.Employee.Workplace != null)
                            {
                                this.Code = _globFunc.GetNumberingUnlockOptimisticRecordBasedOrganization(this.Session.DataLayer, ObjectList.SalesInvoiceFreeItemCmp, _locUserAccess.Employee.Company, _locUserAccess.Employee.Workplace);
                            }
                            else
                            {
                                this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.SalesInvoiceFreeItemCmp);
                            }
                        }
                    }
                    #endregion UserAccess
                    this.Status = CustomProcess.Status.Open;
                    this.StatusDate = now;
                    this.ActivationPosting = true;
                    this.FpStockType = StockType.Good;
                }
            }
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).
        }

        protected override void OnSaving()
        {
            base.OnSaving();
            if (!IsLoading)
            {
                if (!IsSaving)
                {
                    UpdateNo();
                }
            }
        }
        protected override void OnDeleting()
        {
            base.OnDeleting();
            if (!IsLoading)
            {
                RecoveryDeleteNo();
            }
        }

        #region Field

        [Browsable(false)]
        public bool ActivationPosting
        {
            get { return _activationPosting; }
            set { SetPropertyValue("ActivationPosting", ref _activationPosting, value); }
        }

        [Appearance("SalesInvoiceFreeItemCmpNoEnabled", Enabled = false)]
        public int No
        {
            get { return _no; }
            set { SetPropertyValue("No", ref _no, value); }
        }

        [RuleRequiredField(DefaultContexts.Save)]
        [Appearance("SalesInvoiceFreeItemCmpCodeClose", Enabled = false)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue("Code", ref _code, value); }
        }

        #region Organization

        [Appearance("SalesInvoiceFreeItemCmpCompanyClose", Enabled = false)]
        public Company Company
        {
            get { return _company; }
            set { SetPropertyValue("Company", ref _company, value); }
        }

        [Appearance("SalesInvoiceFreeItemCmpWorkplaceClose", Enabled = false)]
        public Workplace Workplace
        {
            get { return _workplace; }
            set { SetPropertyValue("Workplace", ref _workplace, value); }
        }

        [Browsable(false)]
        public Division Division
        {
            get { return _division; }
            set { SetPropertyValue("Division", ref _division, value); }
        }

        [Browsable(false)]
        public Department Department
        {
            get { return _department; }
            set { SetPropertyValue("Department", ref _department, value); }
        }

        [Browsable(false)]
        public Section Section
        {
            get { return _section; }
            set { SetPropertyValue("Section", ref _section, value); }
        }

        [Browsable(false)]
        public Employee Employee
        {
            get { return _employee; }
            set { SetPropertyValue("Employee", ref _employee, value); }
        }

        #endregion Organization

        [Appearance("SalesInvoiceFreeItemCmpSalesInvoiceLineCmpClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public SalesInvoiceLineCmp SalesInvoiceLineCmp
        {
            get { return _salesInvoiceLineCmp; }
            set { SetPropertyValue("SalesInvoiceLineCmp", ref _salesInvoiceLineCmp, value); }
        }

        [Appearance("SalesInvoiceFreeItemCmpFreeItemRuleClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public FreeItemRule FreeItemRule
        {
            get { return _freeItemRule; }
            set { SetPropertyValue("FreeItemRule", ref _freeItemRule, value); }
        }

        [Appearance("SalesInvoiceFreeItemCmpSalesInvoiceFreeItemClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public SalesInvoiceFreeItem SalesInvoiceFreeItem
        {
            get { return _salesInvoiceFreeItem; }
            set { SetPropertyValue("SalesInvoiceFreeItem", ref _salesInvoiceFreeItem, value); }
        }

        #region FpQty

        [ImmediatePostData()]
        [Appearance("SalesInvoiceFreeItemCmpFpItemClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Item FpItem
        {
            get { return _fpItem; }
            set
            {
                SetPropertyValue("FpItem", ref _fpItem, value);
                if (!IsLoading)
                {
                    if (this._fpItem != null)
                    {
                        if (this._fpItem.BasedUOM != null)
                        {
                            this.FpDUOM = this._fpItem.BasedUOM;
                            this.NFpDUOM = this._fpItem.BasedUOM;
                        }
                        SetPriceLine();
                    }
                }
            }
        }

        [Browsable(false)]
        public XPCollection<UnitOfMeasure> AvailableUnitOfMeasureFP
        {
            get
            {
                if (FpItem == null)
                {
                    _availableUnitOfMeasureFP = new XPCollection<UnitOfMeasure>(Session);
                }
                else
                {
                    if (GetUnitOfMeasureCollection(this.FpItem) != null)
                    {
                        _availableUnitOfMeasureFP = new XPCollection<UnitOfMeasure>(Session, CriteriaOperator.Parse(GetUnitOfMeasureCollection(this.FpItem)));
                    }

                }
                return _availableUnitOfMeasureFP;

            }
        }

        [Appearance("SalesInvoiceFreeItemCmpFpDQtyClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [ImmediatePostData()]
        public double FpDQty
        {
            get { return _fpDQty; }
            set
            {
                SetPropertyValue("FpDQty", ref _fpDQty, value);
                if (!IsLoading)
                {
                    SetFpTotalQty();
                    SetTotalUnitAmount();
                }
            }
        }

        [Appearance("SalesInvoiceFreeItemCmpFpDUOMClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [ImmediatePostData()]
        [DataSourceCriteria("Active = true")]
        public UnitOfMeasure FpDUOM
        {
            get { return _fpDUom; }
            set
            {
                SetPropertyValue("FpDUOM", ref _fpDUom, value);
                if (!IsLoading)
                {
                    SetFpTotalQty();
                    SetTotalUnitAmount();
                }
            }
        }

        [Appearance("SalesInvoiceFreeItemCmpFpQtyClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [ImmediatePostData()]
        public double FpQty
        {
            get { return _fpQty; }
            set
            {
                SetPropertyValue("FpQty", ref _fpQty, value);
                if (!IsLoading)
                {
                    SetFpTotalQty();
                    SetTotalUnitAmount();
                }
            }
        }

        [ImmediatePostData()]
        [Appearance("SalesInvoiceFreeItemCmpFpUOMClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [DataSourceProperty("AvailableUnitOfMeasureFP", DataSourcePropertyIsNullMode.SelectAll)]
        [DataSourceCriteria("Active = true")]
        public UnitOfMeasure FpUOM
        {
            get { return _fpUom; }
            set
            {
                SetPropertyValue("FpUOM", ref _fpUom, value);
                if (!IsLoading)
                {
                    SetFpTotalQty();
                    SetTotalUnitAmount();
                }
            }
        }

        [Appearance("SalesInvoiceFreeItemCmpFpTQtyEnabled", Enabled = false)]
        public double FpTQty
        {
            get { return _fpTQty; }
            set { SetPropertyValue("FpTQty", ref _fpTQty, value); }
        }

        [Appearance("SalesInvoiceFreeItemCmpFpStockTypeClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public StockType FpStockType
        {
            get { return _fpStockType; }
            set { SetPropertyValue("FpStockType", ref _fpStockType, value); }
        }

        #endregion FpQty

        #region Amount

        [VisibleInListView(false)]
        [Appearance("SalesInvoiceFreeItemCmpCurrencyClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Currency Currency
        {
            get { return _currency; }
            set { SetPropertyValue("Currency", ref _currency, value); }
        }

        [VisibleInListView(false)]
        [ImmediatePostData()]
        [Appearance("SalesInvoiceFreeItemCmpPriceGroupClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public PriceGroup PriceGroup
        {
            get { return _priceGroup; }
            set
            {
                SetPropertyValue("PriceGroup", ref _priceGroup, value);
                if (!IsLoading)
                {
                    if (this._priceGroup != null)
                    {
                        SetPriceLine();
                    }
                }
            }
        }

        [Browsable(false)]
        [DataSourceCriteria("Active = true"), ImmediatePostData()]
        [Appearance("SalesInvoiceFreeItemCmpPriceLineClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public PriceLine PriceLine
        {
            get { return _priceLine; }
            set
            {
                SetPropertyValue("PriceLine", ref _priceLine, value);
                if (!IsLoading)
                {
                    if (this._priceLine != null)
                    {
                        this.UAmount = this._priceLine.Amount2a;
                    }
                }
            }
        }

        [Appearance("SalesInvoiceFreeItemCmpUAmountClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [ImmediatePostData()]
        public double UAmount
        {
            get { return _uAmount; }
            set
            {
                SetPropertyValue("UAmount", ref _uAmount, value);
                if (!IsLoading)
                {
                    _globFunc = new GlobalFunction();
                    if (this._uAmount > 0)
                    {
                        if (_globFunc.GetRoundingList(Session, ObjectList.SalesInvoiceFreeItemCmp, FieldName.UAmount) == true)
                        {
                            this._uAmount = _globFunc.GetRoundUp(Session, this._uAmount, ObjectList.SalesInvoiceFreeItemCmp, FieldName.UAmount);
                        }
                    }
                    SetTotalUnitAmount();
                }
            }
        }

        [Appearance("SalesInvoiceFreeItemCmpTUAmountEnabled", Enabled = false)]
        [ImmediatePostData()]
        public double TUAmount
        {
            get { return _tUAmount; }
            set { SetPropertyValue("TUAmount", ref _tUAmount, value); }
        }

        #endregion Amount

        #region NFpQty

        [Browsable(false)]
        public XPCollection<UnitOfMeasure> AvailableUnitOfMeasureNFP
        {
            get
            {
                if (FpItem == null)
                {
                    _availableUnitOfMeasureNFP = new XPCollection<UnitOfMeasure>(Session);
                }
                else
                {
                    if (GetUnitOfMeasureCollection(this.FpItem) != null)
                    {
                        _availableUnitOfMeasureNFP = new XPCollection<UnitOfMeasure>(Session, CriteriaOperator.Parse(GetUnitOfMeasureCollection(this.FpItem)));
                    }

                }
                return _availableUnitOfMeasureNFP;

            }
        }

        [Appearance("SalesInvoiceFreeItemCmpNFpDQtyClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [ImmediatePostData()]
        public double NFpDQty
        {
            get { return _nFpDQty; }
            set
            {
                SetPropertyValue("NFpDQty", ref _nFpDQty, value);
                if (!IsLoading)
                {
                    SetNFpTotalQty();
                    SetNTotalUnitAmount();
                }
            }
        }

        [Appearance("SalesInvoiceFreeItemCmpNFpDUOMClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [ImmediatePostData()]
        [DataSourceCriteria("Active = true")]
        public UnitOfMeasure NFpDUOM
        {
            get { return _nFpDUom; }
            set
            {
                SetPropertyValue("NFpDUOM", ref _nFpDUom, value);
                if (!IsLoading)
                {
                    SetNFpTotalQty();
                    SetNTotalUnitAmount();
                }
            }
        }

        [Appearance("SalesInvoiceFreeItemCmpNFpQtyClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [ImmediatePostData()]
        public double NFpQty
        {
            get { return _nFpQty; }
            set
            {
                SetPropertyValue("NFpQty", ref _nFpQty, value);
                if (!IsLoading)
                {
                    SetNFpTotalQty();
                    SetNTotalUnitAmount();
                }
            }
        }

        [ImmediatePostData()]
        [Appearance("SalesInvoiceFreeItemCmpNFpUOMClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [DataSourceProperty("AvailableUnitOfMeasureNFP", DataSourcePropertyIsNullMode.SelectAll)]
        [DataSourceCriteria("Active = true")]
        public UnitOfMeasure NFpUOM
        {
            get { return _nFpUom; }
            set
            {
                SetPropertyValue("NFpUOM", ref _nFpUom, value);
                if (!IsLoading)
                {
                    SetNFpTotalQty();
                    SetNTotalUnitAmount();
                }
            }
        }

        [Appearance("SalesInvoiceFreeItemCmpNFpTQtyEnabled", Enabled = false)]
        public double NFpTQty
        {
            get { return _nFpTQty; }
            set { SetPropertyValue("NFpTQty", ref _nFpTQty, value); }
        }

        #endregion NFpQty

        #region Amount

        [Appearance("SalesInvoiceFreeItemCmpNTUAmountEnabled", Enabled = false)]
        [ImmediatePostData()]
        public double NTUAmount
        {
            get { return _nTUAmount; }
            set { SetPropertyValue("NTUAmount", ref _nTUAmount, value); }
        }

        #endregion Amount

        [ModelDefault("DisplayFormat", "{0:dd/MM/yyyy}")]
        [ModelDefault("EditMask", "dd/MM/yyyy")]
        [Appearance("SalesInvoiceFreeItemCmpETDClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public DateTime ETD
        {
            get { return _etd; }
            set { SetPropertyValue("ETD", ref _etd, value); }
        }

        [ModelDefault("DisplayFormat", "{0:dd/MM/yyyy}")]
        [ModelDefault("EditMask", "dd/MM/yyyy")]
        [Appearance("SalesInvoiceFreeItemCmpETAClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public DateTime ETA
        {
            get { return _eta; }
            set { SetPropertyValue("ETA", ref _eta, value); }
        }

        [VisibleInListView(false)]
        [ImmediatePostData()]
        [ModelDefault("DisplayFormat", "{0:dd/MM/yyyy}")]
        [ModelDefault("EditMask", "dd/MM/yyyy")]
        [Appearance("SalesInvoiceFreeItemCmpDocDateClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public DateTime DocDate
        {
            get { return _docDate; }
            set
            {
                SetPropertyValue("DocDate", ref _docDate, value);
                if (!IsLoading)
                {
                    this.JournalMonth = this._docDate.Month;
                    this.JournalYear = this._docDate.Year;
                }
            }
        }

        [Browsable(false)]
        public int JournalMonth
        {
            get { return _journalMonth; }
            set { SetPropertyValue("JournalMonth", ref _journalMonth, value); }
        }

        [Browsable(false)]
        public int JournalYear
        {
            get { return _journalYear; }
            set { SetPropertyValue("JournalYear", ref _journalYear, value); }
        }

        [Appearance("SalesInvoiceFreeItemCmpStatusClose", Enabled = false)]
        public Status Status
        {
            get { return _status; }
            set { SetPropertyValue("Status", ref _status, value); }
        }

        [ModelDefault("DisplayFormat", "{0:dd/MM/yyyy}")]
        [ModelDefault("EditMask", "dd/MM/yyyy")]
        [Appearance("SalesInvoiceFreeItemCmpStatusDateClose", Enabled = false)]
        public DateTime StatusDate
        {
            get { return _statusDate; }
            set { SetPropertyValue("StatusDate", ref _statusDate, value); }
        }

        [Appearance("SalesInvoiceFreeItemCmpSignCodesssClose", Enabled = false)]
        public string SignCode
        {
            get { return _signCode; }
            set { SetPropertyValue("SignCode", ref _signCode, value); }
        }

        [ImmediatePostData()]
        [RuleRequiredField(DefaultContexts.Save)]
        [Appearance("SalesInvoiceFreeItemCmpSalesInvoiceCmpClose", Enabled = false)]
        [Association("SalesInvoiceCmp-SalesInvoiceFreeItemCmps")]
        public SalesInvoiceCmp SalesInvoiceCmp
        {
            get { return _salesInvoiceCmp; }
            set
            {
                SetPropertyValue("SalesInvoiceCmp", ref _salesInvoiceCmp, value);
                if (!IsLoading)
                {

                    if (this._salesInvoiceCmp != null)
                    {
                        if (Session.IsNewObject(this))
                        {
                            if (this._salesInvoiceCmp.Company != null) { this.Company = this._salesInvoiceCmp.Company; }
                            if (this._salesInvoiceCmp.Workplace != null) { this.Workplace = this._salesInvoiceCmp.Workplace; }
                            if (this._salesInvoiceCmp.Division != null) { this.Division = this._salesInvoiceCmp.Division; }
                            if (this._salesInvoiceCmp.Department != null) { this.Department = this._salesInvoiceCmp.Department; }
                            if (this._salesInvoiceCmp.Section != null) { this.Section = this._salesInvoiceCmp.Section; }
                            if (this._salesInvoiceCmp.Employee != null) { this.Employee = this._salesInvoiceCmp.Employee; }
                            if (this._salesInvoiceCmp.PriceGroup != null) { this.PriceGroup = this._salesInvoiceCmp.PriceGroup; }
                            if (this._salesInvoiceCmp.Currency != null) { this.Currency = this._salesInvoiceCmp.Currency; }
                            this.ETD = this._salesInvoiceCmp.ETD;
                            this.ETA = this._salesInvoiceCmp.ETA;
                        }
                    }
                }
            }
        }

        //AuditTrail
        [CollectionOperationSet(AllowAdd = false, AllowRemove = true)]
        public XPCollection<AuditDataItemPersistent> ChangeHistory
        {
            get
            {
                if (changeHistory == null)
                {
                    changeHistory = AuditedObjectWeakReference.GetAuditTrail(Session, this);
                }
                return changeHistory;
            }
        }

        #endregion Field

        //================================ Code Only ================================

        #region CodeOnly

        #region No

        public void UpdateNo()
        {
            try
            {
                if (!IsLoading && Session.IsNewObject(this))
                {
                    if (this.SalesInvoiceCmp != null)
                    {
                        object _makRecord = Session.Evaluate<SalesInvoiceFreeItemCmp>(CriteriaOperator.Parse("Max(No)"), CriteriaOperator.Parse("SalesInvoiceCmp=?", this.SalesInvoiceCmp));
                        this.No = Convert.ToInt32(_makRecord) + 1;
                        this.Save();
                        RecoveryUpdateNo();
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = SalesInvoiceFreeItemCmp " + ex.ToString());
            }
        }

        public void RecoveryUpdateNo()
        {
            try
            {
                if (this.SalesInvoiceCmp != null)
                {
                    SalesInvoiceCmp _numHeader = Session.FindObject<SalesInvoiceCmp>
                                            (new BinaryOperator("Code", this.SalesInvoiceCmp.Code));

                    XPCollection<SalesInvoiceFreeItemCmp> _numLines = new XPCollection<SalesInvoiceFreeItemCmp>
                                                             (Session, new BinaryOperator("SalesInvoiceCmp", _numHeader),
                                                              new SortProperty("No", DevExpress.Xpo.DB.SortingDirection.Ascending));
                    if (_numLines != null)
                    {
                        int i1 = 0;
                        foreach (SalesInvoiceFreeItemCmp _numLine in _numLines)
                        {
                            i1 += 1;
                            _numLine.No = i1;
                            _numLine.Save();
                        }
                        i1 = 1;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = SalesInvoiceFreeItemCmp " + ex.ToString());
            }
        }

        public void RecoveryDeleteNo()
        {
            try
            {
                if (this.SalesInvoiceCmp != null)
                {
                    SalesInvoiceCmp _numHeader = Session.FindObject<SalesInvoiceCmp>
                                            (new BinaryOperator("Code", this.SalesInvoiceCmp.Code));

                    XPCollection<SalesInvoiceFreeItemCmp> _numLines = new XPCollection<SalesInvoiceFreeItemCmp>
                                                             (Session, new GroupOperator(GroupOperatorType.And,
                                                              new BinaryOperator("This", this, BinaryOperatorType.NotEqual),
                                                              new BinaryOperator("SalesInvoiceCmp", _numHeader)),
                                                              new SortProperty("No", DevExpress.Xpo.DB.SortingDirection.Ascending));
                    if (_numLines != null)
                    {
                        int i = 0;
                        foreach (SalesInvoiceFreeItemCmp _numLine in _numLines)
                        {
                            i += 1;
                            _numLine.No = i;
                            _numLine.Save();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = SalesInvoiceFreeItemCmp " + ex.ToString());
            }
        }

        #endregion No

        private string GetUnitOfMeasureCollection(Item _locItem)
        {
            string _result = null;
            try
            {
                string _beginString = null;
                string _endString = null;

                List<string> _stringUOM = new List<string>();

                XPCollection<ItemUnitOfMeasure> _locItemUnitOfMeasures = new XPCollection<ItemUnitOfMeasure>
                                                                          (Session, new GroupOperator(GroupOperatorType.And,
                                                                           new BinaryOperator("Item", _locItem),
                                                                           new BinaryOperator("Active", true)));

                if (_locItemUnitOfMeasures != null && _locItemUnitOfMeasures.Count() > 0)
                {
                    foreach (ItemUnitOfMeasure _locItemUnitOfMeasure in _locItemUnitOfMeasures)
                    {
                        _stringUOM.Add(_locItemUnitOfMeasure.UOM.Code);
                    }
                }

                IEnumerable<string> _stringArrayUOMDistinct = _stringUOM.Distinct();
                string[] _stringArrayUOMList = _stringArrayUOMDistinct.ToArray();
                if (_stringArrayUOMList.Length == 1)
                {
                    for (int i = 0; i < _stringArrayUOMList.Length; i++)
                    {
                        UnitOfMeasure _locUOM = Session.FindObject<UnitOfMeasure>(new BinaryOperator("Code", _stringArrayUOMList[i]));
                        if (_locUOM != null)
                        {
                            if (i == 0)
                            {
                                _beginString = "[Code]=='" + _locUOM.Code + "'";
                            }
                        }
                    }
                }
                else if (_stringArrayUOMList.Length > 1)
                {
                    for (int i = 0; i < _stringArrayUOMList.Length; i++)
                    {
                        UnitOfMeasure _locUOM = Session.FindObject<UnitOfMeasure>(new BinaryOperator("Code", _stringArrayUOMList[i]));
                        if (_locUOM != null)
                        {
                            if (i == 0)
                            {
                                _beginString = "[Code]=='" + _locUOM.Code + "'";
                            }
                            else
                            {
                                _endString = _endString + " OR [Code]=='" + _locUOM.Code + "'";
                            }
                        }
                    }
                }

                _result = _beginString + _endString;

            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = SalesInvoiceFreeItemCmp " + ex.ToString());
            }

            return _result;
        }

        private void SetFpTotalQty()
        {
            try
            {
                double _locInvLineTotal = 0;
                DateTime now = DateTime.Now;
                if (this.SalesInvoiceCmp != null)
                {
                    if (this.FpItem != null && this.FpUOM != null && this.FpDUOM != null)
                    {
                        ItemUnitOfMeasure _locItemUOM = Session.FindObject<ItemUnitOfMeasure>
                                                        (new GroupOperator(GroupOperatorType.And,
                                                         new BinaryOperator("Item", this.FpItem),
                                                         new BinaryOperator("UOM", this.FpUOM),
                                                         new BinaryOperator("DefaultUOM", this.FpDUOM),
                                                         new BinaryOperator("Active", true)));
                        if (_locItemUOM != null)
                        {
                            if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                            {
                                _locInvLineTotal = this.FpQty * _locItemUOM.DefaultConversion + this.FpDQty;
                            }
                            else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                            {
                                _locInvLineTotal = this.FpQty / _locItemUOM.Conversion + this.FpDQty;
                            }
                            else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                            {
                                _locInvLineTotal = this.FpQty + this.FpDQty;
                            }

                            this.FpTQty = _locInvLineTotal;
                        }
                    }
                    else
                    {
                        _locInvLineTotal = this.FpQty + this.FpDQty;
                        this.FpTQty = _locInvLineTotal;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = SalesInvoiceFreeItemCmp " + ex.ToString());
            }
        }

        private void SetTotalUnitAmount()
        {
            try
            {
                _globFunc = new GlobalFunction();

                if (_fpTQty >= 0 & _uAmount >= 0)
                {
                    if (_globFunc.GetRoundingList(Session, ObjectList.SalesInvoiceFreeItemCmp, FieldName.TUAmount) == true)
                    {
                        this.TUAmount = _globFunc.GetRoundUp(Session, (this.FpTQty * this.UAmount), ObjectList.SalesInvoiceFreeItemCmp, FieldName.TUAmount);
                    }
                    else
                    {
                        this.TUAmount = this.FpTQty * this.UAmount;
                    }

                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = SalesInvoiceFreeItemCmp " + ex.ToString());
            }
        }

        private void SetPriceLine()
        {
            try
            {
                if (this.Company != null && this.Workplace != null && this.FpItem != null && this.PriceGroup != null)
                {
                    PriceLine _locPriceLine = Session.FindObject<PriceLine>(new GroupOperator(GroupOperatorType.And,
                                                                            new BinaryOperator("Company", this.Company),
                                                                            new BinaryOperator("Workplace", this.Workplace),
                                                                            new BinaryOperator("Item", this.FpItem),
                                                                            new BinaryOperator("PriceGroup", this.PriceGroup),
                                                                            new BinaryOperator("Active", true)));
                    if (_locPriceLine != null)
                    {
                        this.PriceLine = _locPriceLine;
                        this.UAmount = _locPriceLine.Amount2a;
                    }
                    else
                    {
                        this.PriceLine = null;
                        this.UAmount = 0;
                    }
                }
                else
                {
                    this.PriceLine = null;
                    this.UAmount = 0;
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = SalesInvoiceFreeItemCmp " + ex.ToString());
            }
        }

        private void SetNFpTotalQty()
        {
            try
            {
                double _locInvLineTotal = 0;
                DateTime now = DateTime.Now;
                if (this.SalesInvoiceCmp != null)
                {
                    if (this.FpItem != null && this.NFpUOM != null && this.NFpDUOM != null)
                    {
                        ItemUnitOfMeasure _locItemUOM = Session.FindObject<ItemUnitOfMeasure>
                                                        (new GroupOperator(GroupOperatorType.And,
                                                         new BinaryOperator("Item", this.FpItem),
                                                         new BinaryOperator("UOM", this.NFpUOM),
                                                         new BinaryOperator("DefaultUOM", this.NFpDUOM),
                                                         new BinaryOperator("Active", true)));
                        if (_locItemUOM != null)
                        {
                            if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                            {
                                _locInvLineTotal = this.NFpQty * _locItemUOM.DefaultConversion + this.NFpDQty;
                            }
                            else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                            {
                                _locInvLineTotal = this.NFpQty / _locItemUOM.Conversion + this.NFpDQty;
                            }
                            else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                            {
                                _locInvLineTotal = this.NFpQty + this.NFpDQty;
                            }

                            this.NFpTQty = _locInvLineTotal;
                        }
                    }
                    else
                    {
                        _locInvLineTotal = this.NFpQty + this.NFpDQty;
                        this.NFpTQty = _locInvLineTotal;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = SalesInvoiceFreeItemCmp " + ex.ToString());
            }
        }

        private void SetNTotalUnitAmount()
        {
            try
            {
                _globFunc = new GlobalFunction();

                if (_fpTQty >= 0 & _uAmount >= 0)
                {
                    if (_globFunc.GetRoundingList(Session, ObjectList.SalesInvoiceFreeItemCmp, FieldName.TUAmount) == true)
                    {
                        this.NTUAmount = _globFunc.GetRoundUp(Session, (this.NFpTQty * this.UAmount), ObjectList.SalesInvoiceFreeItemCmp, FieldName.TUAmount);
                    }
                    else
                    {
                        this.NTUAmount = this.NFpTQty * this.UAmount;
                    }

                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = SalesInvoiceFreeItemCmp " + ex.ToString());
            }
        }

        #endregion CodeOnly

    }
}