﻿using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;

using DevExpress.ExpressApp.Editors;
using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.ConditionalAppearance;

namespace FullDrive.Module.BusinessObjects
{
    [DefaultClassOptions]
    [DefaultProperty("Subject")]
    [NavigationItem("Sales")]
    [RuleCombinationOfPropertiesIsUnique("RouteRuleUnique", DefaultContexts.Save, "Code")]
    //[ImageName("BO_Contact")]
    //[DefaultProperty("DisplayMemberNameForLookupEditorsOfThisType")]
    //[DefaultListViewOptions(MasterDetailMode.ListViewOnly, false, NewItemRowPosition.None)]
    //[Persistent("DatabaseTableName")]
    // Specify more UI options using a declarative approach (https://documentation.devexpress.com/#eXpressAppFramework/CustomDocument112701).
    public class Route : Event
    { // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).

        private int _no;
        private string _code;
        private Company _company;
        private XPCollection<Workplace> _availableWorkplace;
        private Workplace _workplace;
        private XPCollection<Employee> _availableSalesman;
        private Employee _salesman;
        private XPCollection<Vehicle> _availableVehicle;
        private Vehicle _vehicle;
        private PriceGroup _priceGroup;
        private XPCollection<SalesArea> _availableSalesArea;
        private SalesArea _salesArea;
        private SalesAreaLine _salesAreaLine;
        private XPCollection<BusinessPartner> _availableCustomer;
        private BusinessPartner _customer;
        private SalesInvoice _salesInvoice;
        private SalesArea1 _salesArea1;
        private SalesArea2 _salesArea2;
        private SalesArea3 _salesArea3;
        private SalesArea4 _salesArea4;
        private TerritoryLevel1 _territoryLevel1;
        private TerritoryLevel2 _territoryLevel2;
        private TerritoryLevel3 _territoryLevel3;
        private TerritoryLevel4 _territoryLevel4;
        private DateTime _routeDate;
        private string _signCode;
        private bool _active;
        private bool _ad;
        private bool _oc;
        private string _userAccessIn;
        private GlobalFunction _globFunc;
        //AuditTrail
        private XPCollection<AuditDataItemPersistent> changeHistory;


        public Route(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if (!IsLoading)
            {
                if (Session.IsNewObject(this))
                {
                    _globFunc = new GlobalFunction();
                    #region UserAccess
                    _userAccessIn = SecuritySystem.CurrentUserName;
                    UserAccess _locUserAccess = Session.FindObject<UserAccess>(new BinaryOperator("UserName", _userAccessIn));
                
                    if (_locUserAccess != null)
                    {
                        if (_locUserAccess.Employee != null)
                        {
                            if (_locUserAccess.Employee.Company != null && _locUserAccess.Employee.Workplace != null)
                            {
                                this.Code = _globFunc.GetNumberingUnlockOptimisticRecordBasedOrganization(this.Session.DataLayer, ObjectList.Route, _locUserAccess.Employee.Company, _locUserAccess.Employee.Workplace);
                            }
                            else
                            {
                                this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.Route);
                            }
                            if (_locUserAccess.Employee.Company != null)
                            {
                                Company = _locUserAccess.Employee.Company;
                            }
                            if (_locUserAccess.Employee.Workplace != null)
                            {
                                Workplace = _locUserAccess.Employee.Workplace;
                            }else
                            {
                                this.Workplace = _globFunc.GetDefaultWorkplace(this.Session, _locUserAccess);
                            }
                        }
                    }
                    #endregion UserAccess
                    this.Active = true; 
                }
            }
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).
        }

        #region Field
        [Appearance("RouteNoEnabled", Enabled = false)]
        public int No
        {
            get { return _no; }
            set { SetPropertyValue("No", ref _no, value); }
        }

        [Appearance("RouteCodeEnabled", Enabled = false)]
        [RuleRequiredField(DefaultContexts.Save)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue("Code", ref _code, value); }
        }

        [ImmediatePostData()]
        [RuleRequiredField(DefaultContexts.Save)]
        [DataSourceCriteria("Active = true")]
        [Appearance("RoutePlanCompanyEnabled", Enabled = false)]
        public Company Company
        {
            get { return _company; }
            set { SetPropertyValue("Company", ref _company, value); }
        }

        [Browsable(false)]
        public XPCollection<Workplace> AvailableWorkplace
        {

            get
            {
                if (this.Company != null)
                {
                    _userAccessIn = SecuritySystem.CurrentUserName;
                    UserAccess _locUserAccess = Session.FindObject<UserAccess>(new BinaryOperator("UserName", _userAccessIn));
                    if (_locUserAccess != null)
                    {
                        OrganizationSetupDetail _locOrganizationSetupDetail = Session.FindObject<OrganizationSetupDetail>
                                                                        (new GroupOperator(GroupOperatorType.And,
                                                                        new BinaryOperator("UserAccess", _locUserAccess),
                                                                        new BinaryOperator("OpenWorkplace", true),
                                                                        new BinaryOperator("Active", true),
                                                                        new BinaryOperator("Default", true)));

                        if (_locOrganizationSetupDetail != null)
                        {
                            if (_locOrganizationSetupDetail.Workplace != null)
                            {
                                if (_locOrganizationSetupDetail.Workplace.Code != null)
                                {
                                    _availableWorkplace = new XPCollection<Workplace>
                                                (Session, new GroupOperator(GroupOperatorType.And,
                                                new BinaryOperator("Code", _locOrganizationSetupDetail.Workplace.Code),
                                                new BinaryOperator("Active", true)));
                                }
                            }
                        }
                        else
                        {
                            OrganizationSetupDetail _locOrgSetDetail = Session.FindObject<OrganizationSetupDetail>
                                                                        (new GroupOperator(GroupOperatorType.And,
                                                                        new BinaryOperator("UserAccess", _locUserAccess),
                                                                        new BinaryOperator("AllWorkplace", true),
                                                                        new BinaryOperator("Active", true)));
                            if (_locOrgSetDetail != null)
                            {
                                _availableWorkplace = new XPCollection<Workplace>(Session,
                                                    new GroupOperator(GroupOperatorType.And,
                                                    new BinaryOperator("Company", this.Company),
                                                    new BinaryOperator("Active", true)));
                            }
                        }
                    }
                }

                return _availableWorkplace;

            }
        }

        [ImmediatePostData()]
        [RuleRequiredField(DefaultContexts.Save)]
        [Appearance("RoutePlanWorkplaceEnabled", Enabled = false)]
        [DataSourceProperty("AvailableWorkplace", DataSourcePropertyIsNullMode.SelectAll)]
        public Workplace Workplace
        {
            get { return _workplace; }
            set { SetPropertyValue("Workplace", ref _workplace, value); }
        }

        [Browsable(false)]
        public XPCollection<Employee> AvailableSalesman
        {

            get
            {
                if (this.Company != null && this.Workplace != null)
                {
                    XPCollection<Employee> _locEmployees = new XPCollection<Employee>(Session,
                                                        new GroupOperator(GroupOperatorType.And,
                                                        new BinaryOperator("Company", this.Company),
                                                        new BinaryOperator("Workplace", this.Workplace),
                                                        new BinaryOperator("Active", true),
                                                        new GroupOperator(GroupOperatorType.Or,
                                                        new BinaryOperator("SalesRole", SalesRole.Canvassing),
                                                        new BinaryOperator("SalesRole", SalesRole.TakingOrder))));

                    if (_locEmployees != null && _locEmployees.Count() > 0)
                    {
                        _availableSalesman = _locEmployees;
                    }
                }
                else
                {
                    XPCollection<Employee> _locEmployees = new XPCollection<Employee>(Session,
                                                        new GroupOperator(GroupOperatorType.And,
                                                        new BinaryOperator("Active", true),
                                                        new GroupOperator(GroupOperatorType.Or,
                                                        new BinaryOperator("SalesRole", SalesRole.Canvassing),
                                                        new BinaryOperator("SalesRole", SalesRole.TakingOrder))));

                    if (_locEmployees != null && _locEmployees.Count() > 0)
                    {
                        _availableSalesman = _locEmployees;
                    }
                }

                return _availableSalesman;

            }
        }

        [ImmediatePostData()]
        [Association("Employee-Routes")]
        [DataSourceProperty("AvailableSalesman", DataSourcePropertyIsNullMode.SelectAll)]
        public Employee Salesman
        {
            get { return _salesman; }
            set
            {
                SetPropertyValue("Salesman", ref _salesman, value);
                if (_salesman != null)
                {
                    if (this._salesman.Company != null)
                    {
                        this.Company = this._salesman.Company;
                    }
                    if (this._salesman.Workplace != null)
                    {
                        this.Workplace = this._salesman.Workplace;
                    }
                }
                else
                {
                    this.Company = null;
                    this.Workplace = null;
                }

            }
        }

        [Browsable(false)]
        public XPCollection<Vehicle> AvailableVehicle
        {

            get
            {

                string _beginString = null;
                string _endString = null;
                string _fullString = null;

                if (this.Company != null && this.Workplace != null && this.Salesman != null)
                {
                    List<string> _stringSVS = new List<string>();

                    XPCollection<SalesmanVehicleSetup> _locSalesmanVehicleSetups = new XPCollection<SalesmanVehicleSetup>
                                                                              (Session, new GroupOperator(GroupOperatorType.And,
                                                                                new BinaryOperator("Company", this.Company),
                                                                                new BinaryOperator("Workplace", this.Workplace),
                                                                                new BinaryOperator("Salesman", this.Salesman),
                                                                                new BinaryOperator("Active", true)));

                    if (_locSalesmanVehicleSetups != null && _locSalesmanVehicleSetups.Count() > 0)
                    {
                        foreach (SalesmanVehicleSetup _locSalesmanVehicleSetup in _locSalesmanVehicleSetups)
                        {
                            if(_locSalesmanVehicleSetup.Vehicle.Code != null)
                            {
                                _stringSVS.Add(_locSalesmanVehicleSetup.Vehicle.Code);
                            }
                        }
                    }

                    IEnumerable<string> _stringArraySVSDistinct = _stringSVS.Distinct();
                    string[] _stringArraySVSList = _stringArraySVSDistinct.ToArray();
                    if (_stringArraySVSList.Length == 1)
                    {
                        for (int i = 0; i < _stringArraySVSList.Length; i++)
                        {
                            Vehicle _locV = Session.FindObject<Vehicle>(new BinaryOperator("Code", _stringArraySVSList[i]));
                            if (_locV != null)
                            {
                                if (i == 0)
                                {
                                    _beginString = "[Code]=='" + _locV.Code + "'";
                                }
                            }
                        }
                    }
                    else if (_stringArraySVSList.Length > 1)
                    {
                        for (int i = 0; i < _stringArraySVSList.Length; i++)
                        {
                            Vehicle _locV = Session.FindObject<Vehicle>(new BinaryOperator("Code", _stringArraySVSList[i]));
                            if (_locV != null)
                            {
                                if (i == 0)
                                {
                                    _beginString = "[Code]=='" + _locV.Code + "'";
                                }
                                else
                                {
                                    _endString = _endString + " OR [Code]=='" + _locV.Code + "'";
                                }
                            }
                        }
                    }
                    _fullString = _beginString + _endString;

                    if (_fullString != null)
                    {
                        _availableVehicle = new XPCollection<Vehicle>(Session, CriteriaOperator.Parse(_fullString));
                    }

                }
                else if (this.Company != null && this.Workplace != null && this.Salesman != null)
                {
                    XPCollection<Vehicle> _locVehicles = new XPCollection<Vehicle>(Session,
                                                        new GroupOperator(GroupOperatorType.And,
                                                        new BinaryOperator("Company", this.Company),
                                                        new BinaryOperator("Workplace", this.Workplace),
                                                        new BinaryOperator("Active", true)));

                    if (_locVehicles != null && _locVehicles.Count() > 0)
                    {
                        _availableVehicle = _locVehicles;
                    }
                }
                else
                {
                    XPCollection<Vehicle> _locVehicles = new XPCollection<Vehicle>(Session,
                                                        new GroupOperator(GroupOperatorType.And,
                                                        new BinaryOperator("Active", true)));

                    if (_locVehicles != null && _locVehicles.Count() > 0)
                    {
                        _availableVehicle = _locVehicles;
                    }
                }

                return _availableVehicle;

            }
        }

        [ImmediatePostData()]
        [Association("Vehicle-Routes")]
        [DataSourceProperty("AvailableVehicle", DataSourcePropertyIsNullMode.SelectAll)]
        public Vehicle Vehicle
        {
            get { return _vehicle; }
            set {
                SetPropertyValue("Vehicle", ref _vehicle, value);
                if(!IsLoading)
                {
                    if(this._vehicle != null)
                    {
                        SetSubject();
                    }  
                }
            }
        }

        [ImmediatePostData()]
        public PriceGroup PriceGroup
        {
            get { return _priceGroup; }
            set { SetPropertyValue("PriceGroup", ref _priceGroup, value); }
        }

        [Browsable(false)]
        public XPCollection<SalesArea> AvailableSalesArea
        {

            get
            {

                string _beginString = null;
                string _endString = null;
                string _fullString = null;

                if (this.Company != null && this.Workplace != null && this.Salesman != null)
                {
                    List<string> _stringSAS = new List<string>();

                    XPCollection<SalesmanAreaSetup> _locSalesmanAreaSetups = new XPCollection<SalesmanAreaSetup>
                                                                              (Session, new GroupOperator(GroupOperatorType.And,
                                                                                new BinaryOperator("Company", this.Company),
                                                                                new BinaryOperator("Workplace", this.Workplace),
                                                                                new BinaryOperator("Salesman", this.Salesman),
                                                                                new BinaryOperator("Active", true)));

                    if (_locSalesmanAreaSetups != null && _locSalesmanAreaSetups.Count() > 0)
                    {
                        foreach (SalesmanAreaSetup _locSalesmanAreaSetup in _locSalesmanAreaSetups)
                        {
                            _stringSAS.Add(_locSalesmanAreaSetup.SalesArea.Code);
                        }
                    }

                    IEnumerable<string> _stringArraySASDistinct = _stringSAS.Distinct();
                    string[] _stringArraySASList = _stringArraySASDistinct.ToArray();
                    if (_stringArraySASList.Length == 1)
                    {
                        for (int i = 0; i < _stringArraySASList.Length; i++)
                        {
                            SalesArea _locSAS = Session.FindObject<SalesArea>(new BinaryOperator("Code", _stringArraySASList[i]));
                            if (_locSAS != null)
                            {
                                if (i == 0)
                                {
                                    _beginString = "[Code]=='" + _locSAS.Code + "'";
                                }
                            }
                        }
                    }
                    else if (_stringArraySASList.Length > 1)
                    {
                        for (int i = 0; i < _stringArraySASList.Length; i++)
                        {
                            SalesArea _locSAS = Session.FindObject<SalesArea>(new BinaryOperator("Code", _stringArraySASList[i]));
                            if (_locSAS != null)
                            {
                                if (i == 0)
                                {
                                    _beginString = "[Code]=='" + _locSAS.Code + "'";
                                }
                                else
                                {
                                    _endString = _endString + " OR [Code]=='" + _locSAS.Code + "'";
                                }
                            }
                        }
                    }
                    _fullString = _beginString + _endString;

                    if (_fullString != null)
                    {
                        _availableSalesArea = new XPCollection<SalesArea>(Session, CriteriaOperator.Parse(_fullString));
                    }

                }
                else if (this.Company != null && this.Workplace != null && this.Salesman != null)
                {
                    XPCollection<SalesArea> _locSalesAreas = new XPCollection<SalesArea>(Session,
                                                        new GroupOperator(GroupOperatorType.And,
                                                        new BinaryOperator("Company", this.Company),
                                                        new BinaryOperator("Workplace", this.Workplace),
                                                        new BinaryOperator("Active", true)));

                    if (_locSalesAreas != null && _locSalesAreas.Count() > 0)
                    {
                        _availableSalesArea = _locSalesAreas;
                    }
                }
                else
                {
                    XPCollection<SalesArea> _locSalesAreas = new XPCollection<SalesArea>(Session,
                                                        new GroupOperator(GroupOperatorType.And,
                                                        new BinaryOperator("Active", true)));

                    if (_locSalesAreas != null && _locSalesAreas.Count() > 0)
                    {
                        _availableSalesArea = _locSalesAreas;
                    }
                }

                return _availableSalesArea;

            }
        }

        [ImmediatePostData()]
        [DataSourceProperty("AvailableSalesArea", DataSourcePropertyIsNullMode.SelectAll)]
        public SalesArea SalesArea
        {
            get { return _salesArea; }
            set { SetPropertyValue("SalesArea", ref _salesArea, value); }
        }

        [DataSourceCriteria("Active = true And SalesArea = '@This.SalesArea'"), ImmediatePostData()]
        public SalesAreaLine SalesAreaLine
        {
            get { return _salesAreaLine; }
            set {
                SetPropertyValue("SalesAreaLine", ref _salesAreaLine, value);
                if(!IsLoading)
                {
                    if(this._salesAreaLine != null)
                    {
                        if(this._salesAreaLine.SalesArea1 != null)
                        {
                            this.SalesArea1 = this._salesAreaLine.SalesArea1;
                        }
                        if (this._salesAreaLine.SalesArea2 != null)
                        {
                            this.SalesArea2 = this._salesAreaLine.SalesArea2;
                        }
                        if (this._salesAreaLine.SalesArea3 != null)
                        {
                            this.SalesArea3 = this._salesAreaLine.SalesArea3;
                        }
                        if (this._salesAreaLine.SalesArea4 != null)
                        {
                            this.SalesArea4 = this._salesAreaLine.SalesArea4;
                        }
                        if (this._salesAreaLine.TerritoryLevel1 != null)
                        {
                            this.TerritoryLevel1 = this._salesAreaLine.TerritoryLevel1;
                        }
                        if (this._salesAreaLine.TerritoryLevel2 != null)
                        {
                            this.TerritoryLevel2 = this._salesAreaLine.TerritoryLevel2;
                        }
                        if (this._salesAreaLine.TerritoryLevel3 != null)
                        {
                            this.TerritoryLevel3 = this._salesAreaLine.TerritoryLevel3;
                        }
                        if (this._salesAreaLine.TerritoryLevel4 != null)
                        {
                            this.TerritoryLevel4 = this._salesAreaLine.TerritoryLevel4;
                        }
                    }
                    else
                    {
                        this.SalesArea1 = null;
                        this.SalesArea2 = null;
                        this.SalesArea3 = null;
                        this.SalesArea4 = null;
                        this.TerritoryLevel1 = null;
                        this.TerritoryLevel2 = null;
                        this.TerritoryLevel3 = null;
                        this.TerritoryLevel4 = null;
                    }
                }
            }
        }

        [Browsable(false)]
        public XPCollection<BusinessPartner> AvailableCustomer
        {
            get
            {
                string _beginString = null;
                string _endString = null;
                string _fullString = null;

                #region RoutePlans
                if (this.Company != null && this.Workplace != null && this.PriceGroup != null && this.SalesArea != null && this.SalesAreaLine != null)
                {
                    List<string> _stringRP = new List<string>();

                    XPCollection<RoutePlan> _locRoutePlans = new XPCollection<RoutePlan>
                                                                              (Session, new GroupOperator(GroupOperatorType.And,
                                                                                new BinaryOperator("Company", this.Company),
                                                                                new BinaryOperator("Workplace", this.Workplace),
                                                                                new BinaryOperator("SalesArea", this.SalesArea),
                                                                                new BinaryOperator("SalesAreaLine", this.SalesAreaLine),
                                                                                new BinaryOperator("Active", true)));

                    if (_locRoutePlans != null && _locRoutePlans.Count() > 0)
                    {
                        foreach (RoutePlan _locRoutePlan in _locRoutePlans)
                        {
                            if(_locRoutePlan.Customer != null)
                            {
                                if(_locRoutePlan.Customer.Code != null)
                                {
                                    _stringRP.Add(_locRoutePlan.Customer.Code);
                                }
                            }
                        }
                    }

                    IEnumerable<string> _stringArrayRPDistinct = _stringRP.Distinct();
                    string[] _stringArrayRPList = _stringArrayRPDistinct.ToArray();
                    if (_stringArrayRPList.Length == 1)
                    {
                        for (int i = 0; i < _stringArrayRPList.Length; i++)
                        {
                            BusinessPartner _locBP = Session.FindObject<BusinessPartner>(new BinaryOperator("Code", _stringArrayRPList[i]));
                            if (_locBP != null)
                            {
                                if (i == 0)
                                {
                                    _beginString = "[Code]=='" + _locBP.Code + "'";
                                }
                            }
                        }
                    }
                    else if (_stringArrayRPList.Length > 1)
                    {
                        for (int i = 0; i < _stringArrayRPList.Length; i++)
                        {
                            BusinessPartner _locBP = Session.FindObject<BusinessPartner>(new BinaryOperator("Code", _stringArrayRPList[i]));
                            if (_locBP != null)
                            {
                                if (i == 0)
                                {
                                    _beginString = "[Code]=='" + _locBP.Code + "'";
                                }
                                else
                                {
                                    _endString = _endString + " OR [Code]=='" + _locBP.Code + "'";
                                }
                            }
                        }
                    }
                    _fullString = _beginString + _endString;

                    if (_fullString != null)
                    {
                        _availableCustomer = new XPCollection<BusinessPartner>(Session, CriteriaOperator.Parse(_fullString));
                    }
                }
                #endregion RoutePlans
                #region BusinessPartner
                else if (this.Company != null && this.Workplace != null && this.PriceGroup != null && this.SalesArea == null && this.SalesAreaLine == null)
                {
                    XPCollection<BusinessPartner> _locCustomers = new XPCollection<BusinessPartner>(Session,
                                                        new GroupOperator(GroupOperatorType.And,
                                                        new BinaryOperator("Company", this.Company),
                                                        new BinaryOperator("Workplace", this.Workplace),
                                                        new BinaryOperator("PriceGroup", this.PriceGroup),
                                                        new BinaryOperator("Active", true)));

                    if (_locCustomers != null && _locCustomers.Count() > 0)
                    {
                        _availableCustomer = _locCustomers;
                    }
                }
                else
                {
                    XPCollection<BusinessPartner> _locCustomers = new XPCollection<BusinessPartner>(Session,
                                                        new GroupOperator(GroupOperatorType.And,
                                                        new BinaryOperator("Active", true)));

                    if (_locCustomers != null && _locCustomers.Count() > 0)
                    {
                        _availableCustomer = _locCustomers;
                    }
                }
                #endregion BusinessPartner
                return _availableCustomer;

            }
        }

        [ImmediatePostData()]
        [DataSourceProperty("AvailableCustomer", DataSourcePropertyIsNullMode.SelectAll)]
        public BusinessPartner Customer
        {
            get { return _customer; }
            set
            {
                SetPropertyValue("Customer", ref _customer, value);
                if (!IsLoading)
                {
                    if (this._customer != null)
                    {
                        SetSubject();
                    }
                }
            }
        }

        public SalesInvoice SalesInvoice
        {
            get { return _salesInvoice; }
            set { SetPropertyValue("SalesInvoice", ref _salesInvoice, value); }
        }

        #region SalesArea

        [ImmediatePostData()]
        public SalesArea1 SalesArea1
        {
            get { return _salesArea1; }
            set
            {
                SetPropertyValue("SalesArea1", ref _salesArea1, value);
            }
        }

        [DataSourceCriteria("Active = true And SalesArea1 = '@This.SalesArea1'"), ImmediatePostData()]
        public SalesArea2 SalesArea2
        {
            get { return _salesArea2; }
            set
            {
                SetPropertyValue("SalesArea2", ref _salesArea2, value);
            }
        }

        [DataSourceCriteria("Active = true And SalesArea2 = '@This.SalesArea2'"), ImmediatePostData()]
        public SalesArea3 SalesArea3
        {
            get { return _salesArea3; }
            set
            {
                SetPropertyValue("SalesArea3", ref _salesArea3, value);
            }
        }

        [DataSourceCriteria("Active = true And SalesArea3 = '@This.SalesArea3'"), ImmediatePostData()]
        public SalesArea4 SalesArea4
        {
            get { return _salesArea4; }
            set
            {
                SetPropertyValue("SalesArea4", ref _salesArea4, value);
            }
        }
        #endregion SalesArea
        #region Territory

        [ImmediatePostData()]
        public TerritoryLevel1 TerritoryLevel1
        {
            get { return _territoryLevel1; }
            set
            {
                SetPropertyValue("TerritoryLevel1", ref _territoryLevel1, value);
            }
        }

        [DataSourceCriteria("Active = true And TerritoryLevel1 = '@This.TerritoryLevel1'"), ImmediatePostData()]
        public TerritoryLevel2 TerritoryLevel2
        {
            get { return _territoryLevel2; }
            set
            {
                SetPropertyValue("TerritoryLevel2", ref _territoryLevel2, value);
            }
        }

        [DataSourceCriteria("Active = true And TerritoryLevel2 = '@This.TerritoryLevel2'"), ImmediatePostData()]
        public TerritoryLevel3 TerritoryLevel3
        {
            get { return _territoryLevel3; }
            set
            {
                SetPropertyValue("TerritoryLevel3", ref _territoryLevel3, value);
            }
        }

        [DataSourceCriteria("Active = true And TerritoryLevel3 = '@This.TerritoryLevel3'"), ImmediatePostData()]
        public TerritoryLevel4 TerritoryLevel4
        {
            get { return _territoryLevel4; }
            set
            {
                SetPropertyValue("TerritoryLevel4", ref _territoryLevel4, value);
            }
        }

        #endregion Territory

        public DateTime RouteDate
        {
            get { return _routeDate; }
            set { SetPropertyValue("RouteDate", ref _routeDate, value); }
        }

        [Appearance("RouteSignCodeEnabled", Enabled = false)]
        public string SignCode
        {
            get { return _signCode; }
            set { SetPropertyValue("SignCode", ref _signCode, value); }
        }

        public bool Active
        {
            get { return _active; }
            set { SetPropertyValue("Active", ref _active, value); }
        }

        public bool AD
        {
            get { return _ad; }
            set { SetPropertyValue("AD", ref _ad, value); }
        }

        public bool OC
        {
            get { return _oc; }
            set { SetPropertyValue("OC", ref _oc, value); }
        }

        [Association("Route-RouteInvoices")]
        public XPCollection<RouteInvoice> RouteInvoices
        {
            get { return GetCollection<RouteInvoice>("RouteInvoices"); }
        }

        //AuditTrail
        [CollectionOperationSet(AllowAdd = false, AllowRemove = true)]
        public XPCollection<AuditDataItemPersistent> ChangeHistory
        {
            get
            {
                if (changeHistory == null)
                {
                    changeHistory = AuditedObjectWeakReference.GetAuditTrail(Session, this);
                }
                return changeHistory;
            }
        }
        #endregion Field

        //============================ Code Only ===============================================

        private void SetSubject()
        {
            try
            {
                string _locCustomerName = null;
                string _locVehicleName = null;
                if (this.Customer != null && this.Vehicle != null)
                {
                    if(this.Customer.Name != null)
                    {
                        _locCustomerName = this.Customer.Name;
                    }
                    if (this.Vehicle.Name != null)
                    {
                        _locVehicleName = this.Vehicle.Name;
                    }
                    this.Subject = String.Format("{0} ({1})", _locCustomerName, _locVehicleName).Trim();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = Location ", ex.ToString());
            }
        }

    }
}