﻿using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;

using DevExpress.ExpressApp.ConditionalAppearance;
using DevExpress.ExpressApp.SystemModule;
using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.Editors;

namespace FullDrive.Module.BusinessObjects
{
    [DefaultClassOptions]
    [DefaultProperty("Code")]
    [NavigationItem("Cash Advance")]
    [RuleCombinationOfPropertiesIsUnique("PaymentRealizationMonitoringRuleUnique", DefaultContexts.Save, "Code")]
    //[ImageName("BO_Contact")]
    //[DefaultProperty("DisplayMemberNameForLookupEditorsOfThisType")]
    //[DefaultListViewOptions(MasterDetailMode.ListViewOnly, false, NewItemRowPosition.None)]
    //[Persistent("DatabaseTableName")]
    // Specify more UI options using a declarative approach (https://documentation.devexpress.com/#eXpressAppFramework/CustomDocument112701).
    public class PaymentRealizationMonitoring : BaseObject
    { // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).
        private string _code;
        private PaymentRealization _paymentRealization;
        private PaymentRealizationLine _paymentRealizationLine;
        #region InitialOrganization
        private Company _company;
        private Workplace _workplace;
        private Division _division;
        private Department _department;
        private Section _section;
        private Employee _employee;
        private Division _divisionCA;
        private Department _departmentCA;
        private Section _sectionCA;
        private Employee _employeeCA;
        #endregion InitialOrganization
        private Position _position;
        private CashAdvanceType _cashAdvanceType;
        private double _amount;
        private double _amountRealized;
        private double _amountRefund;
        private double _amountOutstanding;
        private string _description;
        private DateTime _docDate;
        private int _journalMonth;
        private int _journalYear;
        private PaymentStatus _paymentStatus;
        private DateTime _paymentStatusDate;
        private Status _status;
        private DateTime _statusDate;
        private string _signCode;
        private CashAdvance _cashAdvance;
        private CashAdvanceMonitoring _cashAdvanceMonitoring;
        private string _localUserAccess;
        private GlobalFunction _globFunc;
        //AuditTrail
        private XPCollection<AuditDataItemPersistent> changeHistory;

        public PaymentRealizationMonitoring(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if(!IsLoading)
            {
                if(Session.IsNewObject(this))
                {
                    _globFunc = new GlobalFunction();
                    DateTime now = DateTime.Now;
                    #region UserAccess
                    _localUserAccess = SecuritySystem.CurrentUserName;
                    UserAccess _locUserAccess = Session.FindObject<UserAccess>(new BinaryOperator("UserName", _localUserAccess));
                    if (_locUserAccess != null)
                    {
                        if (_locUserAccess.Employee != null)
                        {
                            if (_locUserAccess.Employee.Company != null && _locUserAccess.Employee.Workplace != null)
                            {
                                this.Code = _globFunc.GetNumberingUnlockOptimisticRecordBasedOrganization(this.Session.DataLayer, ObjectList.PaymentRealizationMonitoring, _locUserAccess.Employee.Company, _locUserAccess.Employee.Workplace);
                            }
                            else
                            {
                                this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.PaymentRealizationMonitoring);
                            }
                        }
                    }
                    #endregion UserAccess
                    this.Status = Status.Open;
                    this.StatusDate = now;
                }               
            }
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).
        }

        #region Field

        [RuleRequiredField(DefaultContexts.Save)]
        [Appearance("PaymentRealizationMonitoringCodeClose", Enabled = false)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue("Code", ref _code, value); }
        }

        [Appearance("PaymentRealizationMonitoringPaymentRealizationClose", Enabled = false)]
        public PaymentRealization PaymentRealization
        {
            get { return _paymentRealization; }
            set { SetPropertyValue("PaymentRealization", ref _paymentRealization, value); }
        }

        [Appearance("PaymentRealizationMonitoringPaymentRealizationLineClose", Enabled = false)]
        public PaymentRealizationLine PaymentRealizationLine
        {
            get { return _paymentRealizationLine; }
            set { SetPropertyValue("PaymentRealizationLine", ref _paymentRealizationLine, value); }
        }

        [Appearance("PaymentRealizationMonitoringCashAdvanceTypeClose", Enabled = false)]
        public CashAdvanceType CashAdvanceType
        {
            get { return _cashAdvanceType; }
            set { SetPropertyValue("CashAdvanceType", ref _cashAdvanceType, value); }
        }

        #region Organization

        [Appearance("PaymentRealizationMonitoringCompanyClose", Enabled = false)]
        public Company Company
        {
            get { return _company; }
            set { SetPropertyValue("Company", ref _company, value); }
        }

        [Appearance("PaymentRealizationMonitoringWorkplaceClose", Enabled = false)]
        public Workplace Workplace
        {
            get { return _workplace; }
            set { SetPropertyValue("Workplace", ref _workplace, value); }
        }

        [Browsable(false)]
        [Appearance("PaymentRealizationMonitoringDivisionClose", Enabled = false)]
        public Division Division
        {
            get { return _division; }
            set { SetPropertyValue("Division", ref _division, value); }
        }

        [Browsable(false)]
        [Appearance("PaymentRealizationMonitoringDepartmentClose", Enabled = false)]
        public Department Department
        {
            get { return _department; }
            set { SetPropertyValue("Department", ref _department, value); }
        }

        [Browsable(false)]
        [Appearance("PaymentRealizationMonitoringSectionClose", Enabled = false)]
        public Section Section
        {
            get { return _section; }
            set { SetPropertyValue("Section", ref _section, value); }
        }

        [Browsable(false)]
        [Appearance("PaymentRealizationMonitoringEmployeeClose", Enabled = false)]
        public Employee Employee
        {
            get { return _employee; }
            set { SetPropertyValue("Employee", ref _employee, value); }
        }

        [DataSourceCriteria("Company = '@This.Company' And Active = true "), ImmediatePostData()]
        [Appearance("PaymentRealizationMonitoringDivisionCAClose", Enabled = false)]
        public Division DivisionCA
        {
            get { return _divisionCA; }
            set { SetPropertyValue("DivisionCA", ref _divisionCA, value); }
        }

        [DataSourceCriteria("Company = '@This.Company' And Division = '@This.DivisionCA' And Active = true"), ImmediatePostData()]
        [Appearance("PaymentRealizationMonitoringDepartmentCAClose", Enabled = false)]
        public Department DepartmentCA
        {
            get { return _departmentCA; }
            set { SetPropertyValue("DepartmentCA", ref _departmentCA, value); }
        }

        [DataSourceCriteria("Company = '@This.Company' And Department = '@This.DepartmentCA' And Active = true "), ImmediatePostData()]
        [Appearance("PaymentRealizationMonitoringSectionCAClose", Enabled = false)]
        public Section SectionCA
        {
            get { return _sectionCA; }
            set { SetPropertyValue("SectionCA", ref _sectionCA, value); }
        }

        [ImmediatePostData()]
        [DataSourceCriteria("Company = '@This.Company' And Workplace = '@This.Workplace' And Division = '@This.DivisionCA' And Department = '@This.DepartmentCA' And Section = '@This.SectionCA' And Active = true ")]
        [Appearance("PaymentRealizationMonitoringEmployeeCAClose", Enabled = false)]
        public Employee EmployeeCA
        {
            get { return _employeeCA; }
            set
            {
                SetPropertyValue("EmployeeCA", ref _employeeCA, value);
                if (!IsLoading)
                {
                    if (this._employeeCA != null)
                    {
                        if (this._employeeCA.Position != null)
                        {
                            this.Position = this._employeeCA.Position;
                        }
                    }
                }
            }
        }

        #endregion Organization

        [Appearance("PaymentRealizationMonitoringPositionClose", Enabled = false)]
        public Position Position
        {
            get { return _position; }
            set { SetPropertyValue("Position", ref _position, value); }
        }

        [Appearance("PaymentRealizationMonitoringAmountClose", Enabled = false)]
        public double Amount
        {
            get { return _amount; }
            set { SetPropertyValue("Amount", ref _amount, value); }
        }

        [ImmediatePostData()]
        [Appearance("PaymentRealizationMonitoringAmountRealizedClose", Enabled = false)]
        public double AmountRealized
        {
            get { return _amountRealized; }
            set {
                SetPropertyValue("AmountRealized", ref _amountRealized, value);
                if (!IsLoading)
                {
                    if (this._amountRealized > 0)
                    {
                        this.AmountRefund = this.Amount - this._amountRealized;
                    }
                }
            }
        }

        [ImmediatePostData()]
        [Appearance("PaymentRealizationMonitoringAmountRefundClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public double AmountRefund
        {
            get { return _amountRefund; }
            set {
                SetPropertyValue("AmountRefund", ref _amountRefund, value);
                if(!IsLoading)
                {
                    DateTime now = DateTime.Now;
                    if (this._amountRefund > 0)
                    {
                        if((this.Amount - this.AmountRealized) - this._amountRefund > 0)
                        {
                            this.AmountOutstanding = (this.Amount - this.AmountRealized) - this._amountRefund;
                            this.PaymentStatus = PaymentStatus.Debt;
                            this.PaymentStatusDate = now;
                        }
                    }
                }
            }
        }

        [ImmediatePostData()]
        [Appearance("PaymentRealizationMonitoringAmountOutstandingClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public double AmountOutstanding
        {
            get { return _amountOutstanding; }
            set { SetPropertyValue("AmountOutstanding", ref _amountOutstanding, value); }
        }

        [Size(512)]
        [Appearance("PaymentRealizationMonitoringDescriptionClose", Enabled = false)]
        public string Description
        {
            get { return _description; }
            set { SetPropertyValue("Description", ref _description, value); }
        }

        [Browsable(false)]
        [ImmediatePostData()]
        [ModelDefault("DisplayFormat", "{0:dd/MM/yyyy}")]
        [ModelDefault("EditMask", "dd/MM/yyyy")]
        [Appearance("PaymentRealizationLineDocDateClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public DateTime DocDate
        {
            get { return _docDate; }
            set
            {
                SetPropertyValue("DocDate", ref _docDate, value);
                if (!IsLoading)
                {
                    SetAccountingPeriodic();
                }
            }
        }

        [Browsable(false)]
        public int JournalMonth
        {
            get { return _journalMonth; }
            set { SetPropertyValue("JournalMonth", ref _journalMonth, value); }
        }

        [Browsable(false)]
        public int JournalYear
        {
            get { return _journalYear; }
            set { SetPropertyValue("JournalYear", ref _journalYear, value); }
        }

        [Appearance("PaymentRealizationMonitoringPaymentStatusClose", Enabled = false)]
        public PaymentStatus PaymentStatus
        {
            get { return _paymentStatus; }
            set { SetPropertyValue("PaymentStatus", ref _paymentStatus, value); }
        }

        [ModelDefault("DisplayFormat", "{0:dd/MM/yyyy}")]
        [ModelDefault("EditMask", "dd/MM/yyyy")]
        [Appearance("PaymentRealizationMonitoringPaymentStatusDateClose", Enabled = false)]
        public DateTime PaymentStatusDate
        {
            get { return _paymentStatusDate; }
            set { SetPropertyValue("PaymentStatusDate", ref _paymentStatusDate, value); }
        }

        [Appearance("PaymentRealizationMonitoringStatusClose", Enabled = false)]
        public Status Status
        {
            get { return _status; }
            set { SetPropertyValue("Status", ref _status, value); }
        }

        [ModelDefault("DisplayFormat", "{0:dd/MM/yyyy}")]
        [ModelDefault("EditMask", "dd/MM/yyyy")]
        [Appearance("PaymentRealizationMonitoringStatusDateClose", Enabled = false)]
        public DateTime StatusDate
        {
            get { return _statusDate; }
            set { SetPropertyValue("StatusDate", ref _statusDate, value); }
        }

        [Appearance("PaymentRealizationMonitoringCashAdvanceClose", Enabled = false)]
        public CashAdvance CashAdvance
        {
            get { return _cashAdvance; }
            set { SetPropertyValue("CashAdvance", ref _cashAdvance, value); }
        }

        [Appearance("PaymentRealizationMonitoringCashAdvanceMonitoringClose", Enabled = false)]
        public CashAdvanceMonitoring CashAdvanceMonitoring
        {
            get { return _cashAdvanceMonitoring; }
            set { SetPropertyValue("CashAdvanceMonitoring", ref _cashAdvanceMonitoring, value); }
        }

        [Appearance("PaymentRealizationMonitoringSignCodeClose", Enabled = false)]
        public string SignCode
        {
            get { return _signCode; }
            set { SetPropertyValue("SignCode", ref _signCode, value); }
        }

        //AuditTrail
        [CollectionOperationSet(AllowAdd = false, AllowRemove = true)]
        public XPCollection<AuditDataItemPersistent> ChangeHistory
        {
            get
            {
                if (changeHistory == null)
                {
                    changeHistory = AuditedObjectWeakReference.GetAuditTrail(Session, this);
                }
                return changeHistory;
            }
        }

        #endregion Field

        private void SetAccountingPeriodic()
        {
            try
            {
                DateTime now = DateTime.Now;
                if (this.Company != null && this.Workplace != null)
                {
                    AccountingPeriodic _locAcctPeriodic = Session.FindObject<AccountingPeriodic>
                                                        (new GroupOperator(GroupOperatorType.And,
                                                        new BinaryOperator("Company", this.Company),
                                                        new BinaryOperator("Workplace", this.Workplace),
                                                        new BinaryOperator("Active", true)));
                    if (_locAcctPeriodic != null)
                    {
                        this.JournalMonth = _locAcctPeriodic.Month;
                        this.JournalYear = _locAcctPeriodic.Year;
                    }
                    else
                    {
                        this.JournalMonth = 0;
                        this.JournalYear = 0;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Business Object = PaymentRealization ", ex.ToString());
            }
        }

    }
}