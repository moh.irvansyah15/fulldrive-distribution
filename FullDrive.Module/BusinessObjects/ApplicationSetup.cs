﻿using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using FullDrive.Module.CustomProcess;

namespace FullDrive.Module.BusinessObjects
{
    [DefaultClassOptions]
    [DefaultProperty("SetupName")]
    [NavigationItem("Setup")]
    //[ImageName("BO_Contact")]
    //[DefaultProperty("DisplayMemberNameForLookupEditorsOfThisType")]
    //[DefaultListViewOptions(MasterDetailMode.ListViewOnly, false, NewItemRowPosition.None)]
    //[Persistent("DatabaseTableName")]
    // Specify more UI options using a declarative approach (https://documentation.devexpress.com/#eXpressAppFramework/CustomDocument112701).
    public class ApplicationSetup : BaseObject
    { // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).
        private string _setupCode;
        private Company _company;
        private string _setupName;
        private bool _active;
        private bool _defaultSystem;
        private string _localUserAccess;
        //AuditTrail
        private XPCollection<AuditDataItemPersistent> changeHistory;

        public ApplicationSetup(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).
            _localUserAccess = SecuritySystem.CurrentUserName;
            UserAccess _locUserAccess = Session.FindObject<UserAccess>(new BinaryOperator("UserName", _localUserAccess));
            if (_locUserAccess != null)
            {
                if (_locUserAccess.Employee != null)
                {
                    if (_locUserAccess.Employee.Company != null)
                    {
                        this.Company = _locUserAccess.Employee.Company;
                    } 
                }
            }
        }

        protected override void OnSaving()
        {
            base.OnSaving();
            if(!IsLoading)
            {
                if (this.DefaultSystem == true)
                {
                    CheckDefaultSystem();
                }
                if (this.Active == true)
                {
                    CheckActiveSystem();
                }
            }
        }

        #region Field

        public string SetupCode
        {
            get { return _setupCode; }
            set { SetPropertyValue("SetupCode", ref _setupCode, value); }
        }

        public Company Company
        {
            get { return _company; }
            set { SetPropertyValue("Company", ref _company, value); }
        }

        public string SetupName
        {
            get { return _setupName; }
            set { SetPropertyValue("SetupName", ref _setupName, value); }
        }

        public bool Active
        {
            get { return _active; }
            set { SetPropertyValue("Active", ref _active, value); }
        }

        public bool DefaultSystem
        {
            get { return _defaultSystem; }
            set { SetPropertyValue("DefaultSystem", ref _defaultSystem, value); }
        }

        [Association("ApplicationSetup-NumberingHeaders")]
        public XPCollection<NumberingHeader> NumberingHeaders
        {
            get { return GetCollection<NumberingHeader>("NumberingHeaders"); }
        }

        [Association("ApplicationSetup-ApplicationSetupDetails")]
        public XPCollection<ApplicationSetupDetail> ApplicationSetupDetails
        {
            get { return GetCollection<ApplicationSetupDetail>("ApplicationSetupDetails"); }
        }

        [Association("ApplicationSetup-RoundingSetupDetails")]
        public XPCollection<RoundingSetupDetail> RoundingSetupDetails
        {
            get { return GetCollection<RoundingSetupDetail>("RoundingSetupDetails"); }
        }

        [Association("ApplicationSetup-WarehouseSetupDetails")]
        public XPCollection<WarehouseSetupDetail> WarehouseSetupDetails
        {
            get { return GetCollection<WarehouseSetupDetail>("WarehouseSetupDetails"); }
        }

        [Association("ApplicationSetup-OrganizationSetupDetails")]
        public XPCollection<OrganizationSetupDetail> OrganizationSetupDetails
        {
            get { return GetCollection<OrganizationSetupDetail>("OrganizationSetupDetails"); }
        }

        [Association("ApplicationSetup-MailSetupDetails")]
        public XPCollection<MailSetupDetail> MailSetupDetails
        {
            get { return GetCollection<MailSetupDetail>("MailSetupDetails"); }
        }

        [Association("ApplicationSetup-PurchaseSetupDetails")]
        public XPCollection<PurchaseSetupDetail> PurchaseSetupDetails
        {
            get { return GetCollection<PurchaseSetupDetail>("PurchaseSetupDetails"); }
        }

        [Association("ApplicationSetup-JournalSetupDetails")]
        public XPCollection<JournalSetupDetail> JournalSetupDetails
        {
            get { return GetCollection<JournalSetupDetail>("JournalSetupDetails"); }
        }

        //AuditTrail
        [CollectionOperationSet(AllowAdd = false, AllowRemove = true)]
        public XPCollection<AuditDataItemPersistent> ChangeHistory
        {
            get
            {
                if (changeHistory == null)
                {
                    changeHistory = AuditedObjectWeakReference.GetAuditTrail(Session, this);
                }
                return changeHistory;
            }
        }

        #endregion Field

        //=============================================== Code In Here ===============================================

        private void CheckActiveSystem()
        {
            try
            {
                XPCollection<ApplicationSetup> _appSetups = new XPCollection<ApplicationSetup>(Session, 
                                                            new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("This", this, BinaryOperatorType.NotEqual)));
                if (_appSetups != null && _appSetups.Count() > 0)
                {
                    foreach (ApplicationSetup _appSetup in _appSetups)
                    {
                        if(_appSetup.Company == this.Company && this.Active == true)
                        {
                            _appSetup.Active = false;
                            _appSetup.Save();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Business Object = ApplicationSetup", ex.ToString());
            }
        }

        private void CheckDefaultSystem()
        {
            try
            {
                XPCollection<ApplicationSetup> _appSetups = new XPCollection<ApplicationSetup>(Session,
                                                            new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("This", this, BinaryOperatorType.NotEqual)));
                if (_appSetups != null && _appSetups.Count() > 0)
                {
                    foreach (ApplicationSetup _appSetup in _appSetups)
                    {
                        if (this.DefaultSystem == true)
                        {
                            _appSetup.DefaultSystem = false;
                            _appSetup.Save();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Business Object = ApplicationSetup", ex.ToString());
            }
        }

    }
}