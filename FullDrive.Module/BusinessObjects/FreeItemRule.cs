﻿using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;

using DevExpress.ExpressApp.Editors;
using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.ConditionalAppearance;

namespace FullDrive.Module.BusinessObjects
{
    [DefaultClassOptions]
    [NavigationItem("Sales")]
    [DefaultProperty("Code")]
    [RuleCombinationOfPropertiesIsUnique("FreeItemRuleRuleUnique", DefaultContexts.Save, "Code")]
    //[ImageName("BO_Contact")]
    //[DefaultProperty("DisplayMemberNameForLookupEditorsOfThisType")]
    //[DefaultListViewOptions(MasterDetailMode.ListViewOnly, false, NewItemRowPosition.None)]
    //[Persistent("DatabaseTableName")]
    // Specify more UI options using a declarative approach (https://documentation.devexpress.com/#eXpressAppFramework/CustomDocument112701).
    public class FreeItemRule : BaseObject
    { // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).

        private bool _activationPosting;
        private int _no;
        private string _code;
        private Company _company;
        private Workplace _workplace;
        private Employee _employee;
        private LocationType _locationType;
        private StockType _stockType;
        private XPCollection<Location> _availableLocation;
        private Location _location;
        private PriceGroup _priceGroup;
        private FreeItemType1 _freeItemType1;
        private bool _openAmount;
        private bool _grossAmountRule;
        private double _grossTotalUAmount;
        private bool _netAmountRule;
        private double _netTotalUAmount;
        private bool _multiItem;
        //MainPromoItem
        #region InisialisasiMPQty
        private Item _mpItem;
        private XPCollection<UnitOfMeasure> _availableUnitOfMeasureMP;
        private double _mpDQty;
        private UnitOfMeasure _mpDUom;
        private double _mpQty;
        private UnitOfMeasure _mpUom;
        private double _mpTQty;
        #endregion InisialisasiMPQty
        //ComparationItem
        #region InisialisasiCPQty
        private Item _cpItem;
        private XPCollection<UnitOfMeasure> _availableUnitOfMeasureCP;
        private double _cpDQty;
        private UnitOfMeasure _cpDUom;
        private double _cpQty;
        private UnitOfMeasure _cpUom;
        private double _cpTQty;
        #endregion InisialisasiCPQty
        //FreeItem
        #region InisialisasiFPQty
        private BeginningInventory _fpBegInv;
        private StockGroup _fpStockGroup;
        private XPCollection<Item> _availableFpItem;
        private Item _fpItem;
        private XPCollection<UnitOfMeasure> _availableUnitOfMeasureFP;
        private double _fpDQty;
        private UnitOfMeasure _fpDUom;
        private double _fpQty;
        private UnitOfMeasure _fpUom;
        private double _fpTQty;
        private BeginningInventoryLine _fpBegInvLine;
        #endregion InisialisasiCPQty
        private FreeItemRuleType _freeItemRuleType;
        private RuleType _ruleType;
        private int _maxLoop;
        private DateTime _startDate;
        private DateTime _endDate;
        private DiscountAccountGroup _discountAccountGroup;
        private bool _active;
        private Status _status;
        private DateTime _statusDate;
        private SalesPromotionRule _salesPromotionRule;
        private string _localUserAccess;
        private GlobalFunction _globFunc;
        //AuditTrail
        private XPCollection<AuditDataItemPersistent> changeHistory;

        public FreeItemRule(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if (!IsLoading)
            {
                _globFunc = new GlobalFunction();
                DateTime now = DateTime.Now;
                #region UserAccess
                _localUserAccess = SecuritySystem.CurrentUserName;
                UserAccess _locUserAccess = Session.FindObject<UserAccess>(new BinaryOperator("UserName", _localUserAccess));
                if (_locUserAccess != null)
                {
                    if (_locUserAccess.Employee != null)
                    {
                        if (_locUserAccess.Employee.Company != null && _locUserAccess.Employee.Workplace != null)
                        {
                            this.Code = _globFunc.GetNumberingUnlockOptimisticRecordBasedOrganization(this.Session.DataLayer, ObjectList.FreeItemRule, _locUserAccess.Employee.Company, _locUserAccess.Employee.Workplace);
                        }
                        else
                        {
                            this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.FreeItemRule);
                        }
                        
                        this.Employee = _locUserAccess.Employee;
                    }
                }
                #endregion UserAccess
                this.Status = CustomProcess.Status.Open;
                this.StatusDate = now;
                this.FreeItemRuleType = _globFunc.GetDefaultFreeItemRuleType(Session);
                this.LocationType = LocationType.Main;
                this.StockType = StockType.Good;
                this.FpStockGroup = StockGroup.FreeItem;
                this.RuleType = RuleType.None;
            }
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).
        }

        protected override void OnSaving()
        {
            base.OnSaving();
            if (!IsLoading && IsSaving)
            {
                UpdateNo();
            }
        }
        protected override void OnDeleting()
        {
            base.OnDeleting();
            if (!IsLoading)
            {
                RecoveryDeleteNo();
            }
        }

        #region Field

        [Browsable(false)]
        public bool ActivationPosting
        {
            get { return _activationPosting; }
            set { SetPropertyValue("ActivationPosting", ref _activationPosting, value); }
        }

        [Appearance("FreeItemRuleNoEnabled", Enabled = false)]
        public int No
        {
            get { return _no; }
            set { SetPropertyValue("No", ref _no, value); }
        }

        [RuleRequiredField(DefaultContexts.Save)]
        [Appearance("FreeItemRuleCodeClose", Enabled = false)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue("Code", ref _code, value); }
        }

        [RuleRequiredField(DefaultContexts.Save)]
        [Appearance("FreeItemRuleCompanyClose", Enabled = false)]
        public Company Company
        {
            get { return _company; }
            set { SetPropertyValue("Company", ref _company, value); }
        }

        [ImmediatePostData()]
        [Appearance("FreeItemRuleWorkplaceClose", Enabled = false)]
        public Workplace Workplace
        {
            get { return _workplace; }
            set { SetPropertyValue("Workplace", ref _workplace, value); }
        }

        //[Browsable(false)]
        public Employee Employee
        {
            get { return _employee; }
            set { SetPropertyValue("Employee", ref _employee, value); }
        }

        [ImmediatePostData()]
        [Appearance("FreeItemRuleLocationTypeClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public LocationType LocationType
        {
            get { return _locationType; }
            set { SetPropertyValue("LocationType", ref _locationType, value); }
        }

        [ImmediatePostData()]
        [Appearance("FreeItemRuleStockTypeClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public StockType StockType
        {
            get { return _stockType; }
            set { SetPropertyValue("StockType", ref _stockType, value); }
        }

        [Browsable(false)]
        public XPCollection<Location> AvailableLocation
        {
            get
            {
                string _beginString = null;
                string _endString = null;
                string _fullString = null;

                if (this.Company != null && this.Workplace != null && this.Employee != null)
                {
                    
                    #region SalesmanLocationSetup
                    List<string> _stringSLS = new List<string>();

                    XPCollection<SalesmanLocationSetup> _locSalesmanLocationSetups = new XPCollection<SalesmanLocationSetup>
                                                                              (Session, new GroupOperator(GroupOperatorType.And,
                                                                               new BinaryOperator("Company", this.Company),
                                                                               new BinaryOperator("Workplace", this.Workplace),
                                                                               new BinaryOperator("Salesman", this.Employee),
                                                                               new BinaryOperator("Active", true)));

                    if (_locSalesmanLocationSetups != null && _locSalesmanLocationSetups.Count() > 0)
                    {
                        foreach (SalesmanLocationSetup _locSalesmanLocationSetup in _locSalesmanLocationSetups)
                        {
                            if (_locSalesmanLocationSetup.Location != null)
                            {
                                if (_locSalesmanLocationSetup.Location.Code != null)
                                {
                                    _stringSLS.Add(_locSalesmanLocationSetup.Location.Code);
                                }
                            }
                        }
                    }

                    IEnumerable<string> _stringArraySLSDistinct = _stringSLS.Distinct();
                    string[] _stringArraySLSList = _stringArraySLSDistinct.ToArray();
                    if (_stringArraySLSList.Length == 1)
                    {
                        for (int i = 0; i < _stringArraySLSList.Length; i++)
                        {
                            Location _locLoc = Session.FindObject<Location>(new BinaryOperator("Code", _stringArraySLSList[i]));
                            if (_locLoc != null)
                            {
                                if (i == 0)
                                {
                                    _beginString = "[Code]=='" + _locLoc.Code + "'";
                                }
                            }
                        }
                    }
                    else if (_stringArraySLSList.Length > 1)
                    {
                        for (int i = 0; i < _stringArraySLSList.Length; i++)
                        {
                            Location _locLoc = Session.FindObject<Location>(new BinaryOperator("Code", _stringArraySLSList[i]));
                            if (_locLoc != null)
                            {
                                if (i == 0)
                                {
                                    _beginString = "[Code]=='" + _locLoc.Code + "'";
                                }
                                else
                                {
                                    _endString = _endString + " OR [Code]=='" + _locLoc.Code + "'";
                                }
                            }
                        }
                    }
                    _fullString = _beginString + _endString;

                    if (_fullString != null)
                    {
                        _availableLocation = new XPCollection<Location>(Session, CriteriaOperator.Parse(_fullString));
                    }

                }
                else
                {
                    if (this.Company != null && this.Workplace != null)
                    {
                        _availableLocation = new XPCollection<Location>(Session, new GroupOperator(GroupOperatorType.And,
                                                                            new BinaryOperator("Company", this.Company),
                                                                            new BinaryOperator("Workplace", this.Workplace),
                                                                            new BinaryOperator("Active", true)));
                    }
                }

                #endregion SalesmanLocationSetup

                return _availableLocation;

            }
        }

        [ImmediatePostData()]
        [DataSourceProperty("AvailableLocation")]
        [Appearance("FreeItemRuleLocationClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Location Location
        {
            get { return _location; }
            set {
                SetPropertyValue("Location", ref _location, value);
                if(!IsLoading)
                {
                    if(this._location != null)
                    {
                        SetBegInv(this._location);
                    }
                    else
                    {
                        this.FpBegInv = null;
                    }
                }
            }
        }

        [Appearance("FreeItemRulePriceGroupClose", Enabled = false)]
        public PriceGroup PriceGroup
        {
            get { return _priceGroup; }
            set { SetPropertyValue("PriceGroup", ref _priceGroup, value); }
        }

        [ImmediatePostData()]
        [Appearance("FreeItemRuleFreeItemType1Close", Criteria = "ActivationPosting = true", Enabled = false)]
        public FreeItemType1 FreeItemType1
        {
            get { return _freeItemType1; }
            set { SetPropertyValue("FreeItemType1", ref _freeItemType1, value); }
        }

        [ImmediatePostData()]
        public bool OpenAmount
        {
            get { return _openAmount; }
            set { SetPropertyValue("OpenAmount", ref _openAmount, value); }
        }

        #region Amount

        [ImmediatePostData()]
        [Appearance("FreeItemRuleGrossAmountRuleOpenAmount_1", Criteria = "OpenAmount = true", Enabled = true)]
        [Appearance("FreeItemRuleGrossAmountRuleOpenAmount_2", Criteria = "OpenAmount = false", Enabled = false)]
        [Appearance("FreeItemRuleGrossAmountRuleClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public bool GrossAmountRule
        {
            get { return _grossAmountRule; }
            set
            {
                SetPropertyValue("GrossAmountRule", ref _grossAmountRule, value);
                if (!IsLoading)
                {
                    if (this._grossAmountRule == true)
                    {
                        this.NetTotalUAmount = 0;
                        this.NetAmountRule = false;
                    }
                    else
                    {
                        this.GrossTotalUAmount = 0;
                    }
                }
            }
        }

        [ImmediatePostData()]
        [Appearance("FreeItemRuleGrossTotalUAmountOpenAmount_1", Criteria = "OpenAmount = true", Enabled = true)]
        [Appearance("FreeItemRuleGrossTotalUAmountOpenAmount_2", Criteria = "OpenAmount = false", Enabled = false)]
        [Appearance("FreeItemRuleGrossTotalUAmountEnabled1", Criteria = "ActivationPosting = true", Enabled = false)]
        [Appearance("FreeItemRuleGrossTotalUAmountEnabled2", Criteria = "GrossAmountRule = false", Enabled = false)]
        public double GrossTotalUAmount
        {
            get { return _grossTotalUAmount; }
            set { SetPropertyValue("GrossTotalUAmount", ref _grossTotalUAmount, value); }
        }

        [ImmediatePostData()]
        [Appearance("FreeItemRuleNetAmountRuleOpenAmount_1", Criteria = "OpenAmount = true", Enabled = true)]
        [Appearance("FreeItemRuleNetAmountRuleOpenAmount_2", Criteria = "OpenAmount = false", Enabled = false)]
        [Appearance("FreeItemRuleNetAmountRuleClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public bool NetAmountRule
        {
            get { return _netAmountRule; }
            set
            {
                SetPropertyValue("NetAmountRule", ref _netAmountRule, value);
                if (!IsLoading)
                {
                    if (this._netAmountRule == true)
                    {
                        this.GrossTotalUAmount = 0;
                        this.GrossAmountRule = false;
                    }
                    else
                    {
                        this.NetTotalUAmount = 0;
                    }
                }
            }
        }

        [Appearance("FreeItemRuleNetTotalUAmountOpenAmount_1", Criteria = "OpenAmount = true", Enabled = true)]
        [Appearance("FreeItemRuleNetTotalUAmountOpenAmount_2", Criteria = "OpenAmount = false", Enabled = false)]
        [Appearance("FreeItemRuleNetTotalUAmountEnabled1", Criteria = "ActivationPosting = true", Enabled = false)]
        [Appearance("FreeItemRuleNetTotalUAmountEnabled2", Criteria = "NetAmountRule = false", Enabled = false)]
        [ImmediatePostData()]
        public double NetTotalUAmount
        {
            get { return _netTotalUAmount; }
            set { SetPropertyValue("NetTotalUAmount", ref _netTotalUAmount, value); }
        }

        #endregion Amount

        [ImmediatePostData()]
        public bool MultiItem
        {
            get { return _multiItem; }
            set { SetPropertyValue("MultiItem", ref _multiItem, value); }
        }

        #region MpQty

        [ImmediatePostData()]
        [Appearance("FreeItemRuleMpItemClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Item MpItem
        {
            get { return _mpItem; }
            set
            {
                SetPropertyValue("MpItem", ref _mpItem, value);
                if (!IsLoading)
                {
                    if (this._mpItem != null)
                    {
                        if (this._mpItem.BasedUOM != null)
                        {
                            this.MpDUOM = this._mpItem.BasedUOM;
                        }
                    }
                }
            }
        }

        [Browsable(false)]
        public XPCollection<UnitOfMeasure> AvailableUnitOfMeasureMP
        {
            get
            {
                if (MpItem == null)
                {
                    _availableUnitOfMeasureMP = new XPCollection<UnitOfMeasure>(Session);
                }
                else
                {
                    if (GetUnitOfMeasureCollection(this.MpItem) != null)
                    {
                        _availableUnitOfMeasureMP = new XPCollection<UnitOfMeasure>(Session, CriteriaOperator.Parse(GetUnitOfMeasureCollection(this.MpItem)));
                    }

                }
                return _availableUnitOfMeasureMP;

            }
        }

        [Appearance("FreeItemRuleMpDQtyClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [ImmediatePostData()]
        public double MpDQty
        {
            get { return _mpDQty; }
            set
            {
                SetPropertyValue("MpDQty", ref _mpDQty, value);
                if (!IsLoading)
                {
                    SetMpTotalQty();
                }
            }
        }

        [ImmediatePostData()]
        [Appearance("FreeItemRuleMpDUOMClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [DataSourceCriteria("Active = true")]
        public UnitOfMeasure MpDUOM
        {
            get { return _mpDUom; }
            set
            {
                SetPropertyValue("MpDUOM", ref _mpDUom, value);
                if (!IsLoading)
                {
                    SetMpTotalQty();  
                }
            }
        }

        [ImmediatePostData()]
        [Appearance("FreeItemRuleMpQtyClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public double MpQty
        {
            get { return _mpQty; }
            set
            {
                SetPropertyValue("MpQty", ref _mpQty, value);
                if (!IsLoading)
                {
                    SetMpTotalQty();
                }
            }
        }

        [ImmediatePostData()]
        [Appearance("FreeItemRuleMpUOMClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [DataSourceProperty("AvailableUnitOfMeasureMP", DataSourcePropertyIsNullMode.SelectAll)]
        [DataSourceCriteria("Active = true")]
        public UnitOfMeasure MpUOM
        {
            get { return _mpUom; }
            set
            {
                SetPropertyValue("MpUOM", ref _mpUom, value);
                if (!IsLoading)
                {
                    SetMpTotalQty();
                }
            }
        }

        [Appearance("FreeItemRuleMpTQtyEnabled", Enabled = false)]
        public double MpTQty
        {
            get { return _mpTQty; }
            set { SetPropertyValue("MpTQty", ref _mpTQty, value); }
        }

        #endregion MpQty

        #region CpQty

        [ImmediatePostData()]
        [Appearance("FreeItemRuleCpItemClose1", Criteria = "MultiItem = true", Enabled = false)]
        [Appearance("FreeItemRuleCpItemClose2", Criteria = "ActivationPosting = true", Enabled = false)]
        public Item CpItem
        {
            get { return _cpItem; }
            set
            {
                SetPropertyValue("CpItem", ref _cpItem, value);
                if (!IsLoading)
                {
                    if (this._cpItem != null)
                    {
                        if (this._cpItem.BasedUOM != null)
                        {
                            this.CpDUOM = this._cpItem.BasedUOM;
                        }
                    }
                }
            }
        }

        [Browsable(false)]
        public XPCollection<UnitOfMeasure> AvailableUnitOfMeasureCP
        {
            get
            {
                if (CpItem == null)
                {
                    _availableUnitOfMeasureCP = new XPCollection<UnitOfMeasure>(Session);
                }
                else
                {
                    if (GetUnitOfMeasureCollection(this.CpItem) != null)
                    {
                        _availableUnitOfMeasureCP = new XPCollection<UnitOfMeasure>(Session, CriteriaOperator.Parse(GetUnitOfMeasureCollection(this.CpItem)));
                    }

                }
                return _availableUnitOfMeasureCP;

            }
        }

        [ImmediatePostData()]
        [Appearance("FreeItemRuleCpDQtyClose1", Criteria = "MultiItem = true", Enabled = false)]
        [Appearance("FreeItemRuleCpDQtyClose2", Criteria = "ActivationPosting = true", Enabled = false)]
        public double CpDQty
        {
            get { return _cpDQty; }
            set
            {
                SetPropertyValue("CpDQty", ref _cpDQty, value);
                if (!IsLoading)
                {
                    SetCpTotalQty();
                }
            }
        }

        [ImmediatePostData()]
        [Appearance("FreeItemRuleCpDUOMClose1", Criteria = "MultiItem = true", Enabled = false)]
        [Appearance("FreeItemRuleCpDUOMClose2", Criteria = "ActivationPosting = true", Enabled = false)]
        [DataSourceCriteria("Active = true")]
        public UnitOfMeasure CpDUOM
        {
            get { return _cpDUom; }
            set
            {
                SetPropertyValue("CpDUOM", ref _cpDUom, value);
                if (!IsLoading)
                {
                    SetCpTotalQty();
                }
            }
        }

        [ImmediatePostData()]
        [Appearance("FreeItemRuleCpQtyClose1", Criteria = "MultiItem = true", Enabled = false)]
        [Appearance("FreeItemRuleCpQtyClose2", Criteria = "ActivationPosting = true", Enabled = false)]
        public double CpQty
        {
            get { return _cpQty; }
            set
            {
                SetPropertyValue("CpQty", ref _cpQty, value);
                if (!IsLoading)
                {
                    SetCpTotalQty();
                }
            }
        }

        [ImmediatePostData()]
        [Appearance("FreeItemRuleCpUOMClose1", Criteria = "MultiItem = true", Enabled = false)]
        [Appearance("FreeItemRuleCpUOMClose2", Criteria = "ActivationPosting = true", Enabled = false)]
        [DataSourceProperty("AvailableUnitOfMeasureCP", DataSourcePropertyIsNullMode.SelectAll)]
        [DataSourceCriteria("Active = true")]
        public UnitOfMeasure CpUOM
        {
            get { return _cpUom; }
            set
            {
                SetPropertyValue("CpUOM", ref _cpUom, value);
                if (!IsLoading)
                {
                    SetCpTotalQty();
                }
            }
        }

        [Appearance("FreeItemRuleCpTQtyEnabled", Enabled = false)]
        public double CpTQty
        {
            get { return _cpTQty; }
            set { SetPropertyValue("CpTQty", ref _cpTQty, value); }
        }

        #endregion CpQty

        #region FpQty

        //[Browsable(false)]
        [ImmediatePostData()]
        [Appearance("FreeItemRuleFpBegInvClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public BeginningInventory FpBegInv
        {
            get { return _fpBegInv; }
            set { SetPropertyValue("FpBegInv", ref _fpBegInv, value); }
        }

        [ImmediatePostData()]
        [Appearance("FreeItemRuleFpStockGroupClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public StockGroup FpStockGroup
        {
            get { return _fpStockGroup; }
            set { SetPropertyValue("FpStockGroup", ref _fpStockGroup, value); }
        }

        [Browsable(false)]
        public XPCollection<Item> AvailableFpItem
        {
            get
            {
                string _beginString = null;
                string _endString = null;
                string _fullString = null;
                if (this.Company != null && this.Workplace != null)
                {
                    List<string> _stringITM = new List<string>();

                    XPQuery<BeginningInventoryLine> _beginningInventoryLinesQuery = new XPQuery<BeginningInventoryLine>(Session);

                    var _beginningInventoryLines = from bil in _beginningInventoryLinesQuery
                                                   where (bil.Company == this.Company
                                                   && bil.Workplace == this.Workplace
                                                   && bil.BeginningInventory == this.FpBegInv
                                                   && bil.Location == this.Location
                                                   && bil.StockType == this.StockType
                                                   && bil.StockGroup == this.FpStockGroup
                                                   && bil.QtyAvailable > 0
                                                   && bil.Lock == false
                                                   && bil.Active == true
                                                   )
                                                   group bil by bil.Item into g
                                                   select new { Item = g.Key };
                    if (_beginningInventoryLines != null && _beginningInventoryLines.Count() > 0)
                    {
                        foreach (var _beginningInventoryLine in _beginningInventoryLines)
                        {
                            _stringITM.Add(_beginningInventoryLine.Item.Code);
                        }
                    }

                    IEnumerable<string> _stringArrayITMDistinct = _stringITM.Distinct();
                    string[] _stringArrayITMList = _stringArrayITMDistinct.ToArray();
                    if (_stringArrayITMList.Length == 1)
                    {
                        for (int i = 0; i < _stringArrayITMList.Length; i++)
                        {
                            Item _locITM = Session.FindObject<Item>(new BinaryOperator("Code", _stringArrayITMList[i]));
                            if (_locITM != null)
                            {
                                if (i == 0)
                                {
                                    _beginString = "[Code]=='" + _locITM.Code + "'";
                                }
                            }
                        }
                    }
                    else if (_stringArrayITMList.Length > 1)
                    {
                        for (int i = 0; i < _stringArrayITMList.Length; i++)
                        {
                            Item _locITM = Session.FindObject<Item>(new BinaryOperator("Code", _stringArrayITMList[i]));
                            if (_locITM != null)
                            {
                                if (i == 0)
                                {
                                    _beginString = "[Code]=='" + _locITM.Code + "'";
                                }
                                else
                                {
                                    _endString = _endString + " OR [Code]=='" + _locITM.Code + "'";
                                }
                            }
                        }
                    }
                    _fullString = _beginString + _endString;

                    if (_fullString != null)
                    {
                        _availableFpItem = new XPCollection<Item>(Session, CriteriaOperator.Parse(_fullString));
                    }
                }
                else
                {
                    if (this.Company != null && this.Workplace != null)
                    {
                        _availableFpItem = new XPCollection<Item>(Session, new GroupOperator(GroupOperatorType.And,
                                                    new BinaryOperator("Company", this.Company),
                                                    new BinaryOperator("Workplace", this.Workplace),
                                                    new BinaryOperator("Active", true)));
                    }

                }

                return _availableFpItem;

            }
        }

        [ImmediatePostData()]
        [DataSourceProperty("AvailableFpItem")]
        [Appearance("FreeItemRuleFpItemClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Item FpItem
        {
            get { return _fpItem; }
            set
            {
                SetPropertyValue("FpItem", ref _fpItem, value);
                if (!IsLoading)
                {
                    if (this._fpItem != null)
                    {
                        if (this._fpItem.BasedUOM != null)
                        {
                            this.FpDUOM = this._fpItem.BasedUOM;
                        }
                        SetUOMBasedIUOM();
                        SetBegInvLine();
                    }else
                    {
                        this.FpBegInvLine = null;
                        this.FpDUOM = null;
                    }
                }
            }
        }

        [Browsable(false)]
        public XPCollection<UnitOfMeasure> AvailableUnitOfMeasureFP
        {
            get
            {
                if (FpItem == null)
                {
                    _availableUnitOfMeasureFP = new XPCollection<UnitOfMeasure>(Session);
                }
                else
                {
                    if (GetUnitOfMeasureCollection(this.FpItem) != null)
                    {
                        _availableUnitOfMeasureFP = new XPCollection<UnitOfMeasure>(Session, CriteriaOperator.Parse(GetUnitOfMeasureCollection(this.FpItem)));
                    }

                }
                return _availableUnitOfMeasureFP;

            }
        }

        [Appearance("FreeItemRuleFpDQtyClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [ImmediatePostData()]
        public double FpDQty
        {
            get { return _fpDQty; }
            set
            {
                SetPropertyValue("FpDQty", ref _fpDQty, value);
                if (!IsLoading)
                {
                    SetFpTotalQty();
                }
            }
        }

        [Appearance("FreeItemRuleFpDUOMClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [ImmediatePostData()]
        [DataSourceCriteria("Active = true")]
        public UnitOfMeasure FpDUOM
        {
            get { return _fpDUom; }
            set
            {
                SetPropertyValue("FpDUOM", ref _fpDUom, value);
                if (!IsLoading)
                {
                    SetFpTotalQty();
                }
            }
        }

        [Appearance("FreeItemRuleFpQtyClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [ImmediatePostData()]
        public double FpQty
        {
            get { return _fpQty; }
            set
            {
                SetPropertyValue("FpQty", ref _fpQty, value);
                if (!IsLoading)
                {
                    SetFpTotalQty();
                }
            }
        }

        [ImmediatePostData()]
        [Appearance("FreeItemRuleFpUOMClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [DataSourceProperty("AvailableUnitOfMeasureFP", DataSourcePropertyIsNullMode.SelectAll)]
        [DataSourceCriteria("Active = true")]
        public UnitOfMeasure FpUOM
        {
            get { return _fpUom; }
            set
            {
                SetPropertyValue("FpUOM", ref _fpUom, value);
                if (!IsLoading)
                {
                    SetFpTotalQty();
                }
            }
        }

        [Appearance("FreeItemRuleFpTQtyEnabled", Enabled = false)]
        public double FpTQty
        {
            get { return _fpTQty; }
            set { SetPropertyValue("FpTQty", ref _fpTQty, value); }
        }
        
        //[Browsable(false)]
        [Appearance("FreeItemRuleFpBegInvLineEnabled", Enabled = false)]
        public BeginningInventoryLine FpBegInvLine
        {
            get { return _fpBegInvLine; }
            set { SetPropertyValue("FpBegInvLine", ref _fpBegInvLine, value); }
        }

        #endregion FpQty

        [ImmediatePostData()]
        [Appearance("FreeItemRuleFreeItemRuleTypeClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public FreeItemRuleType FreeItemRuleType
        {
            get { return _freeItemRuleType; }
            set {
                SetPropertyValue("FreeItemRuleType", ref _freeItemRuleType, value);
                if(!IsLoading)
                {
                    if(this._freeItemRuleType != null)
                    {
                        this.RuleType = this._freeItemRuleType.RuleType;
                    }
                    else
                    {
                        this.RuleType = RuleType.None;
                    }
                }
            }
        }

        [Appearance("FreeItemRuleRuleTypeClose", Enabled = false)]
        public RuleType RuleType
        {
            get { return _ruleType; }
            set { SetPropertyValue("RuleType", ref _ruleType, value); }
        }

        [Appearance("FreeItemRuleFreeItemMaxLoopClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public int MaxLoop
        {
            get { return _maxLoop; }
            set { SetPropertyValue("MaxLoop", ref _maxLoop, value); }
        }

        [ModelDefault("DisplayFormat", "{0:dd/MM/yyyy}")]
        [ModelDefault("EditMask", "dd/MM/yyyy")]
        [Appearance("FreeItemRuleStartDateClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public DateTime StartDate
        {
            get { return _startDate; }
            set { SetPropertyValue("StartDate", ref _startDate, value); }
        }

        [ModelDefault("DisplayFormat", "{0:dd/MM/yyyy}")]
        [ModelDefault("EditMask", "dd/MM/yyyy")]
        [Appearance("FreeItemRuleEndDateClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public DateTime EndDate
        {
            get { return _endDate; }
            set { SetPropertyValue("EndDate", ref _endDate, value); }
        }

        [DataSourceCriteria("Company = '@This.Company' AND Workplace = '@This.Workplace' AND Active = true")]
        [Appearance("FreeItemRuleDiscountAccountGroupClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public DiscountAccountGroup DiscountAccountGroup
        {
            get { return _discountAccountGroup; }
            set { SetPropertyValue("DiscountAccountGroup", ref _discountAccountGroup, value); }
        }

        [Appearance("FreeItemRuleActiveClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public bool Active
        {
            get { return _active; }
            set { SetPropertyValue("Active", ref _active, value); }
        }

        [Appearance("FreeItemRuleStatusClose", Enabled = false)]
        public Status Status
        {
            get { return _status; }
            set { SetPropertyValue("Status", ref _status, value); }
        }

        [ModelDefault("DisplayFormat", "{0:dd/MM/yyyy}")]
        [ModelDefault("EditMask", "dd/MM/yyyy")]
        [Appearance("FreeItemRuleStatusDateClose", Enabled = false)]
        public DateTime StatusDate
        {
            get { return _statusDate; }
            set { SetPropertyValue("StatusDate", ref _statusDate, value); }
        }

        [ImmediatePostData]
        [RuleRequiredField(DefaultContexts.Save)]
        [Association("SalesPromotionRule-FreeItemRules")]
        public SalesPromotionRule SalesPromotionRule
        {
            get { return _salesPromotionRule; }
            set {
                SetPropertyValue("SalesPromotionRule", ref _salesPromotionRule, value);
                if(!IsLoading)
                {
                    if(this._salesPromotionRule != null)
                    {
                        if(this._salesPromotionRule.Company != null)
                        {
                            this.Company = this._salesPromotionRule.Company;
                        }
                        if (this._salesPromotionRule.Workplace != null)
                        {
                            this.Workplace = this._salesPromotionRule.Workplace;
                        }
                        if (this._salesPromotionRule.PriceGroup != null)
                        {
                            this.PriceGroup = this._salesPromotionRule.PriceGroup;
                        }
                    }
                }
            }
        }

        //AuditTrail
        [CollectionOperationSet(AllowAdd = false, AllowRemove = true)]
        public XPCollection<AuditDataItemPersistent> ChangeHistory
        {
            get
            {
                if (changeHistory == null)
                {
                    changeHistory = AuditedObjectWeakReference.GetAuditTrail(Session, this);
                }
                return changeHistory;
            }
        }

        #endregion Field

        //================================ Code Only ================================

        #region CodeOnly

        #region No

        public void UpdateNo()
        {
            try
            {
                if (!IsLoading && Session.IsNewObject(this))
                {
                    if (this.SalesPromotionRule != null)
                    {
                        object _makRecord = Session.Evaluate<FreeItemRule>(CriteriaOperator.Parse("Max(No)"), CriteriaOperator.Parse("SalesPromotionRule=?", this.SalesPromotionRule));
                        this.No = Convert.ToInt32(_makRecord) + 1;
                        this.Save();
                        RecoveryUpdateNo();
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = FreeItemRule " + ex.ToString());
            }
        }

        public void RecoveryUpdateNo()
        {
            try
            {
                if (this.SalesPromotionRule != null)
                {
                    SalesPromotionRule _numHeader = Session.FindObject<SalesPromotionRule>
                                            (new BinaryOperator("Code", this.SalesPromotionRule.Code));

                    XPCollection<FreeItemRule> _numLines = new XPCollection<FreeItemRule>
                                                             (Session, new BinaryOperator("SalesPromotionRule", _numHeader),
                                                              new SortProperty("No", DevExpress.Xpo.DB.SortingDirection.Ascending));
                    if (_numLines != null)
                    {
                        int i1 = 0;
                        foreach (FreeItemRule _numLine in _numLines)
                        {
                            i1 += 1;
                            _numLine.No = i1;
                            _numLine.Save();
                        }
                        i1 = 1;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = FreeItemRule " + ex.ToString());
            }
        }

        public void RecoveryDeleteNo()
        {
            try
            {
                if (this.SalesPromotionRule != null)
                {
                    SalesPromotionRule _numHeader = Session.FindObject<SalesPromotionRule>
                                            (new BinaryOperator("Code", this.SalesPromotionRule.Code));

                    XPCollection<FreeItemRule> _numLines = new XPCollection<FreeItemRule>
                                                             (Session, new GroupOperator(GroupOperatorType.And,
                                                              new BinaryOperator("This", this, BinaryOperatorType.NotEqual),
                                                              new BinaryOperator("SalesPromotionRule", _numHeader)),
                                                              new SortProperty("No", DevExpress.Xpo.DB.SortingDirection.Ascending));
                    if (_numLines != null)
                    {
                        int i = 0;
                        foreach (FreeItemRule _numLine in _numLines)
                        {
                            i += 1;
                            _numLine.No = i;
                            _numLine.Save();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = FreeItemRule " + ex.ToString());
            }
        }

        #endregion No

        private string GetUnitOfMeasureCollection(Item _locItem)
        {
            string _result = null;
            try
            {
                string _beginString = null;
                string _endString = null;
                
                List<string> _stringUOM = new List<string>();

                XPCollection<ItemUnitOfMeasure> _locItemUnitOfMeasures = new XPCollection<ItemUnitOfMeasure>
                                                                          (Session, new GroupOperator(GroupOperatorType.And,
                                                                           new BinaryOperator("Item", _locItem),
                                                                           new BinaryOperator("Active", true)));

                if (_locItemUnitOfMeasures != null && _locItemUnitOfMeasures.Count() > 0)
                {
                    foreach (ItemUnitOfMeasure _locItemUnitOfMeasure in _locItemUnitOfMeasures)
                    {
                        _stringUOM.Add(_locItemUnitOfMeasure.UOM.Code);
                    }
                }

                IEnumerable<string> _stringArrayUOMDistinct = _stringUOM.Distinct();
                string[] _stringArrayUOMList = _stringArrayUOMDistinct.ToArray();
                if (_stringArrayUOMList.Length == 1)
                {
                    for (int i = 0; i < _stringArrayUOMList.Length; i++)
                    {
                        UnitOfMeasure _locUOM = Session.FindObject<UnitOfMeasure>(new BinaryOperator("Code", _stringArrayUOMList[i]));
                        if (_locUOM != null)
                        {
                            if (i == 0)
                            {
                                _beginString = "[Code]=='" + _locUOM.Code + "'";
                            }
                        }
                    }
                }
                else if (_stringArrayUOMList.Length > 1)
                {
                    for (int i = 0; i < _stringArrayUOMList.Length; i++)
                    {
                        UnitOfMeasure _locUOM = Session.FindObject<UnitOfMeasure>(new BinaryOperator("Code", _stringArrayUOMList[i]));
                        if (_locUOM != null)
                        {
                            if (i == 0)
                            {
                                _beginString = "[Code]=='" + _locUOM.Code + "'";
                            }
                            else
                            {
                                _endString = _endString + " OR [Code]=='" + _locUOM.Code + "'";
                            }
                        }
                    }
                }

                _result = _beginString + _endString;
            
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = InvoiceCanvassingLine " + ex.ToString());
            }

            return _result;
        }

        private void SetMpTotalQty()
        {
            try
            {
                double _locInvLineTotal = 0;
                DateTime now = DateTime.Now;
                if (this.SalesPromotionRule != null)
                {
                    if (this.MpItem != null && this.MpUOM != null && this.MpDUOM != null)
                    {
                        ItemUnitOfMeasure _locItemUOM = Session.FindObject<ItemUnitOfMeasure>
                                                        (new GroupOperator(GroupOperatorType.And,
                                                         new BinaryOperator("Item", this.MpItem),
                                                         new BinaryOperator("UOM", this.MpUOM),
                                                         new BinaryOperator("DefaultUOM", this.MpDUOM),
                                                         new BinaryOperator("Active", true)));
                        if (_locItemUOM != null)
                        {
                            if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                            {
                                _locInvLineTotal = this.MpQty * _locItemUOM.DefaultConversion + this.MpDQty;
                            }
                            else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                            {
                                _locInvLineTotal = this.MpQty / _locItemUOM.Conversion + this.MpDQty;
                            }
                            else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                            {
                                _locInvLineTotal = this.MpQty + this.MpDQty;
                            }

                            this.MpTQty = _locInvLineTotal;
                        }
                    }
                    else
                    {
                        _locInvLineTotal = this.MpQty + this.MpDQty;
                        this.MpTQty = _locInvLineTotal;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = FreeItemRule " + ex.ToString());
            }
        }

        private void SetCpTotalQty()
        {
            try
            {
                double _locInvLineTotal = 0;
                DateTime now = DateTime.Now;
                if (this.SalesPromotionRule != null)
                {
                    if (this.CpItem != null && this.CpUOM != null && this.CpDUOM != null)
                    {
                        ItemUnitOfMeasure _locItemUOM = Session.FindObject<ItemUnitOfMeasure>
                                                        (new GroupOperator(GroupOperatorType.And,
                                                         new BinaryOperator("Item", this.CpItem),
                                                         new BinaryOperator("UOM", this.CpUOM),
                                                         new BinaryOperator("DefaultUOM", this.CpDUOM),
                                                         new BinaryOperator("Active", true)));
                        if (_locItemUOM != null)
                        {
                            if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                            {
                                _locInvLineTotal = this.CpQty * _locItemUOM.DefaultConversion + this.CpDQty;
                            }
                            else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                            {
                                _locInvLineTotal = this.CpQty / _locItemUOM.Conversion + this.CpDQty;
                            }
                            else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                            {
                                _locInvLineTotal = this.CpQty + this.CpDQty;
                            }

                            this.CpTQty = _locInvLineTotal;
                        }
                    }
                    else
                    {
                        _locInvLineTotal = this.CpQty + this.CpDQty;
                        this.CpTQty = _locInvLineTotal;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = FreeItemRule " + ex.ToString());
            }
        }

        private void SetFpTotalQty()
        {
            try
            {
                double _locInvLineTotal = 0;
                DateTime now = DateTime.Now;
                if (this.SalesPromotionRule != null)
                {
                    if (this.FpItem != null && this.FpUOM != null && this.FpDUOM != null)
                    {
                        ItemUnitOfMeasure _locItemUOM = Session.FindObject<ItemUnitOfMeasure>
                                                        (new GroupOperator(GroupOperatorType.And,
                                                         new BinaryOperator("Item", this.FpItem),
                                                         new BinaryOperator("UOM", this.FpUOM),
                                                         new BinaryOperator("DefaultUOM", this.FpDUOM),
                                                         new BinaryOperator("Active", true)));
                        if (_locItemUOM != null)
                        {
                            if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                            {
                                _locInvLineTotal = this.FpQty * _locItemUOM.DefaultConversion + this.FpDQty;
                            }
                            else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                            {
                                _locInvLineTotal = this.FpQty / _locItemUOM.Conversion + this.FpDQty;
                            }
                            else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                            {
                                _locInvLineTotal = this.FpQty + this.FpDQty;
                            }

                            this.FpTQty = _locInvLineTotal;
                        }
                    }
                    else
                    {
                        _locInvLineTotal = this.FpQty + this.FpDQty;
                        this.FpTQty = _locInvLineTotal;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = FreeItemRule " + ex.ToString());
            }
        }

        private void SetUOMBasedIUOM()
        {
            try
            {
                ItemUnitOfMeasure _locItemUOM = Session.FindObject<ItemUnitOfMeasure>
                                        (new GroupOperator(GroupOperatorType.And,
                                         new BinaryOperator("Item", this.FpItem),
                                         new BinaryOperator("DefaultUOM", this.FpDUOM),
                                         new BinaryOperator("Default", true),
                                         new BinaryOperator("Active", true)));
                if(_locItemUOM != null)
                {
                    if(_locItemUOM.UOM != null)
                    {
                        this.FpUOM = _locItemUOM.UOM;
                    } 
                    else
                    {
                        this.FpUOM = null;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = FreeItemRule " + ex.ToString());
            }
        }

        [Action(Caption = "GetPreviousData", ConfirmationMessage = "Are you sure?", AutoCommit = false)]
        public void GetPreviousData()
        {
            try
            {
                FreeItemRule _dataLine = null;
                int _locNo = 0;
                if (this.SalesPromotionRule != null)
                {
                    if (this.Company != null && this.Workplace != null && this.PriceGroup != null)
                    {
                        object _makRecord = Session.Evaluate<FreeItemRule>(CriteriaOperator.Parse("Max(No)"),
                            CriteriaOperator.Parse("Company=? and Workplace=? and PriceGroup=? and SalesPromotionRule=?", this.Company, this.Workplace, this.PriceGroup, this.SalesPromotionRule));
                        _locNo = Convert.ToInt32(_makRecord);

                        if (_locNo > 0)
                        {
                            _dataLine = Session.FindObject<FreeItemRule>
                                (new GroupOperator(GroupOperatorType.And,
                                new BinaryOperator("Company", this.Company),
                                new BinaryOperator("Workplace", this.Workplace),
                                new BinaryOperator("PriceGroup", this.PriceGroup),
                                new BinaryOperator("SalesPromotionRule", this.SalesPromotionRule),
                                new BinaryOperator("No", _locNo)));
                        }

                    }
                    else
                    {
                        object _makRecord = Session.Evaluate<FreeItemRule>(CriteriaOperator.Parse("Max(No)"), CriteriaOperator.Parse("SalesPromotionRule=? ", this.SalesPromotionRule));
                        _locNo = Convert.ToInt32(_makRecord);

                        if (_locNo > 0)
                        {
                            _dataLine = Session.FindObject<FreeItemRule>
                                (new GroupOperator(GroupOperatorType.And,
                                new BinaryOperator("SalesPromotionRule", this.SalesPromotionRule),
                                new BinaryOperator("No", _locNo)));
                        }

                    }

                    if (_dataLine != null)
                    {
                        this.FreeItemRuleType = _dataLine.FreeItemRuleType;
                        this.RuleType = _dataLine.RuleType;
                        this.MultiItem = _dataLine.MultiItem;
                        this.MpItem = _dataLine.MpItem;
                        this.MpDQty = _dataLine.MpDQty;
                        this.MpDUOM = _dataLine.MpDUOM;
                        this.MpQty = _dataLine.MpQty;
                        this.MpUOM = _dataLine.MpUOM;
                        this.MpTQty = _dataLine.MpTQty;
                        this.CpItem = _dataLine.CpItem;
                        this.CpDQty = _dataLine.CpDQty;
                        this.CpDUOM = _dataLine.CpDUOM;
                        this.CpQty = _dataLine.CpQty;
                        this.CpUOM = _dataLine.CpUOM;
                        this.CpTQty = _dataLine.CpTQty;
                        this.FpItem = _dataLine.FpItem;
                        this.FpDQty = _dataLine.FpDQty;
                        this.FpDUOM = _dataLine.FpDUOM;
                        this.FpQty = _dataLine.FpQty;
                        this.FpUOM = _dataLine.FpUOM;
                        this.FpTQty = _dataLine.FpTQty;
                        this.PriceGroup = _dataLine.PriceGroup;
                        this.MaxLoop = _dataLine.MaxLoop;
                        this.StartDate = _dataLine.StartDate;
                        this.EndDate = _dataLine.EndDate;
                        this.Active = _dataLine.Active;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = RoutingLine " + ex.ToString());
            }

        }

        private void SetBegInv(Location _locLocation)
        {
            try
            {
                DateTime now = DateTime.Now;
                if (this.Company != null && this.Workplace != null)
                {
                    BeginningInventory _locBegInv = Session.FindObject<BeginningInventory>
                                                        (new GroupOperator(GroupOperatorType.And,
                                                        new BinaryOperator("Company", this.Company),
                                                        new BinaryOperator("Workplace", this.Workplace),
                                                        new BinaryOperator("Location", _locLocation),
                                                        new BinaryOperator("LocationType", this.LocationType),
                                                        new BinaryOperator("StockType", this.StockType),
                                                        new BinaryOperator("Active", true)));
                    if (_locBegInv != null)
                    {
                        this.FpBegInv = _locBegInv;
                    }
                    else
                    {
                        this.FpBegInv = null;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Business Object = FreeItemRule ", ex.ToString());
            }
        }

        private void SetBegInvLine()
        {
            try
            {
                DateTime now = DateTime.Now;
                
                if (this.Company != null && this.Workplace != null && this.FpItem != null && this.FpBegInv != null && this.Location != null)
                {
                    BeginningInventoryLine _locBegInvLine = Session.FindObject<BeginningInventoryLine>(
                                                           new GroupOperator(GroupOperatorType.And,
                                                           new BinaryOperator("Company", this.Company),
                                                           new BinaryOperator("Workplace", this.Workplace),
                                                           new BinaryOperator("BeginningInventory", this.FpBegInv),
                                                           new BinaryOperator("Location", this.Location),
                                                           new BinaryOperator("Item", this.FpItem),
                                                           new BinaryOperator("LocationType", this.LocationType),
                                                           new BinaryOperator("StockType", this.StockType),
                                                           new BinaryOperator("StockGroup", this.FpStockGroup),
                                                           new BinaryOperator("Lock", false),
                                                           new BinaryOperator("Active", true)));

                    if (_locBegInvLine != null)
                    {
                        this.FpBegInvLine = _locBegInvLine;
                    }
                    else
                    {
                        this.FpBegInvLine = null;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Business Object = FreeItemRule ", ex.ToString());
            }
        }
        #endregion CodeOnly
    }
}