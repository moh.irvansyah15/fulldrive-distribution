﻿using System;
using System.Linq;
using DevExpress.ExpressApp;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using DevExpress.ExpressApp.Updating;
using DevExpress.ExpressApp.Security;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Security.Strategy;
using DevExpress.Xpo;
using DevExpress.ExpressApp.Xpo;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.BaseImpl.PermissionPolicy;

using FullDrive.Module.BusinessObjects;
using FullDrive.Module.CustomProcess;

namespace FullDrive.Module.DatabaseUpdate {
    // For more typical usage scenarios, be sure to check out https://docs.devexpress.com/eXpressAppFramework/DevExpress.ExpressApp.Updating.ModuleUpdater
    public class Updater : ModuleUpdater {
        public Updater(IObjectSpace objectSpace, Version currentDBVersion) :
            base(objectSpace, currentDBVersion) {
        }
        public override void UpdateDatabaseAfterUpdateSchema() {
            base.UpdateDatabaseAfterUpdateSchema();
            //string name = "MyName";
            //DomainObject1 theObject = ObjectSpace.FindObject<DomainObject1>(CriteriaOperator.Parse("Name=?", name));
            //if(theObject == null) {
            //    theObject = ObjectSpace.CreateObject<DomainObject1>();
            //    theObject.Name = name;
            //}
            DefSecurity1();
            CreateDefaultRole();
            DefAppSetup();
            //This line persists created object(s).
        }

        public override void UpdateDatabaseBeforeUpdateSchema() {
            base.UpdateDatabaseBeforeUpdateSchema();
            //if(CurrentDBVersion < new Version("1.1.0.0") && CurrentDBVersion > new Version("0.0.0.0")) {
            //    RenameColumn("DomainObject1Table", "OldColumnName", "NewColumnName");
            //}
        }

        private void CreateDefaultRole()
        {
            UserAccessRole defaultRole = ObjectSpace.FindObject<UserAccessRole>(new BinaryOperator("Name", "Default"));
            if (defaultRole == null)
            {
                defaultRole = ObjectSpace.CreateObject<UserAccessRole>();
                defaultRole.Name = "Default";

                defaultRole.AddObjectPermission<UserAccessRole>(SecurityOperations.Read, "[Oid] = CurrentUserId()", SecurityPermissionState.Allow);
                defaultRole.AddNavigationPermission(@"Application/NavigationItems/Items/Default/Items/MyDetails", SecurityPermissionState.Allow);
                defaultRole.AddMemberPermission<UserAccessRole>(SecurityOperations.Write, "ChangePasswordOnFirstLogon", "[Oid] = CurrentUserId()", SecurityPermissionState.Allow);
                defaultRole.AddMemberPermission<UserAccessRole>(SecurityOperations.Write, "StoredPassword", "[Oid] = CurrentUserId()", SecurityPermissionState.Allow);
                defaultRole.AddTypePermissionsRecursively<UserAccessRole>(SecurityOperations.Read, SecurityPermissionState.Deny);
                defaultRole.AddTypePermissionsRecursively<ModelDifference>(SecurityOperations.ReadWriteAccess, SecurityPermissionState.Allow);
                defaultRole.AddTypePermissionsRecursively<ModelDifferenceAspect>(SecurityOperations.ReadWriteAccess, SecurityPermissionState.Allow);
                defaultRole.AddTypePermissionsRecursively<ModelDifference>(SecurityOperations.Create, SecurityPermissionState.Allow);
                defaultRole.AddTypePermissionsRecursively<ModelDifferenceAspect>(SecurityOperations.Create, SecurityPermissionState.Allow);
            }
            ObjectSpace.CommitChanges();
        }

        private void DefSecurity1()
        {
            //--- Menjalankan program ini harus terlebih dahulu menjalankan program pada point 1 -----
            UserAccessRole adminEmployeeRole = ObjectSpace.FindObject<UserAccessRole>(new BinaryOperator("Name", SecurityStrategy.AdministratorRoleName));
            if (adminEmployeeRole == null)
            {
                adminEmployeeRole = ObjectSpace.CreateObject<UserAccessRole>();
                adminEmployeeRole.Name = SecurityStrategy.AdministratorRoleName;
                adminEmployeeRole.IsAdministrative = true;
                adminEmployeeRole.Save();
            }

            UserAccess adminEmployee = ObjectSpace.FindObject<UserAccess>(
                new BinaryOperator("UserName", "adminfd"));
            if (adminEmployee == null)
            {
                adminEmployee = ObjectSpace.CreateObject<UserAccess>();
                adminEmployee.Code = "UA-000001";
                adminEmployee.UserName = "adminfd";
                adminEmployee.SetPassword("fdadmin@#2020");
                adminEmployee.UserAccessRoles.Add(adminEmployeeRole);
            }
            UserAccess adminEmployee1 = ObjectSpace.FindObject<UserAccess>(
                new BinaryOperator("UserName", "adminfd1"));
            if (adminEmployee1 == null)
            {
                adminEmployee1 = ObjectSpace.CreateObject<UserAccess>();
                adminEmployee1.Code = "UA-000002";
                adminEmployee1.UserName = "adminfd1";
                adminEmployee1.SetPassword("fdadmin@#2020");
                adminEmployee1.UserAccessRoles.Add(adminEmployeeRole);
            }
            UserAccess adminEmployee2 = ObjectSpace.FindObject<UserAccess>(
                new BinaryOperator("UserName", "adminfd2"));
            if (adminEmployee2 == null)
            {
                adminEmployee2 = ObjectSpace.CreateObject<UserAccess>();
                adminEmployee2.Code = "UA-000003";
                adminEmployee2.UserName = "adminfd2";
                adminEmployee2.SetPassword("fdadmin@#2020");
                adminEmployee2.UserAccessRoles.Add(adminEmployeeRole);
            }
            #region UserTakingOrder
            UserAccess adminEmployeeto1 = ObjectSpace.FindObject<UserAccess>(
                new BinaryOperator("UserName", "adminto1"));
            if (adminEmployeeto1 == null)
            {
                adminEmployeeto1 = ObjectSpace.CreateObject<UserAccess>();
                adminEmployeeto1.Code = "UA-000004";
                adminEmployeeto1.UserName = "adminto1";
                adminEmployeeto1.SetPassword("fdadmin@#2020");
                adminEmployeeto1.UserAccessRoles.Add(adminEmployeeRole);
            }
            UserAccess adminEmployeeto2 = ObjectSpace.FindObject<UserAccess>(
                new BinaryOperator("UserName", "adminto2"));
            if (adminEmployeeto2 == null)
            {
                adminEmployeeto2 = ObjectSpace.CreateObject<UserAccess>();
                adminEmployeeto2.Code = "UA-000005";
                adminEmployeeto2.UserName = "adminto2";
                adminEmployeeto2.SetPassword("fdadmin@#2020");
                adminEmployeeto2.UserAccessRoles.Add(adminEmployeeRole);
            }
            UserAccess adminEmployeeto3 = ObjectSpace.FindObject<UserAccess>(
                new BinaryOperator("UserName", "adminto3"));
            if (adminEmployeeto3 == null)
            {
                adminEmployeeto3 = ObjectSpace.CreateObject<UserAccess>();
                adminEmployeeto3.Code = "UA-000006";
                adminEmployeeto3.UserName = "adminto3";
                adminEmployeeto3.SetPassword("fdadmin@#2020");
                adminEmployeeto3.UserAccessRoles.Add(adminEmployeeRole);
            }
            UserAccess adminEmployeeto4 = ObjectSpace.FindObject<UserAccess>(
                new BinaryOperator("UserName", "adminto4"));
            if (adminEmployeeto4 == null)
            {
                adminEmployeeto4 = ObjectSpace.CreateObject<UserAccess>();
                adminEmployeeto4.Code = "UA-000007";
                adminEmployeeto4.UserName = "adminto4";
                adminEmployeeto4.SetPassword("fdadmin@#2020");
                adminEmployeeto4.UserAccessRoles.Add(adminEmployeeRole);
            }
            #endregion UserTakingOrder
            #region UserWarehouse
            UserAccess adminEmployeewhs1 = ObjectSpace.FindObject<UserAccess>(
                new BinaryOperator("UserName", "adminwhs1"));
            if (adminEmployeewhs1 == null)
            {
                adminEmployeewhs1 = ObjectSpace.CreateObject<UserAccess>();
                adminEmployeewhs1.Code = "UA-000008";
                adminEmployeewhs1.UserName = "adminwhs1";
                adminEmployeewhs1.SetPassword("fdadmin@#2020");
                adminEmployeewhs1.UserAccessRoles.Add(adminEmployeeRole);
            }
            UserAccess adminEmployeewhs2 = ObjectSpace.FindObject<UserAccess>(
                new BinaryOperator("UserName", "adminwhs2"));
            if (adminEmployeewhs2 == null)
            {
                adminEmployeewhs2 = ObjectSpace.CreateObject<UserAccess>();
                adminEmployeewhs2.Code = "UA-000009";
                adminEmployeewhs2.UserName = "adminwhs2";
                adminEmployeewhs2.SetPassword("fdadmin@#2020");
                adminEmployeewhs2.UserAccessRoles.Add(adminEmployeeRole);
            }
            UserAccess adminEmployeewhs3 = ObjectSpace.FindObject<UserAccess>(
                new BinaryOperator("UserName", "adminwhs3"));
            if (adminEmployeewhs3 == null)
            {
                adminEmployeewhs3 = ObjectSpace.CreateObject<UserAccess>();
                adminEmployeewhs3.Code = "UA-000010";
                adminEmployeewhs3.UserName = "adminwhs3";
                adminEmployeewhs3.SetPassword("fdadmin@#2020");
                adminEmployeewhs3.UserAccessRoles.Add(adminEmployeeRole);
            }
            UserAccess adminEmployeewhs4 = ObjectSpace.FindObject<UserAccess>(
                new BinaryOperator("UserName", "adminwhs4"));
            if (adminEmployeewhs4 == null)
            {
                adminEmployeewhs4 = ObjectSpace.CreateObject<UserAccess>();
                adminEmployeewhs4.Code = "UA-000011";
                adminEmployeewhs4.UserName = "adminwhs4";
                adminEmployeewhs4.SetPassword("fdadmin@#2020");
                adminEmployeewhs4.UserAccessRoles.Add(adminEmployeeRole);
            }
            #endregion UserWarehouse
            #region DeliveryOrder
            UserAccess adminEmployeedo1 = ObjectSpace.FindObject<UserAccess>(
                new BinaryOperator("UserName", "admindo1"));
            if (adminEmployeedo1 == null)
            {
                adminEmployeedo1 = ObjectSpace.CreateObject<UserAccess>();
                adminEmployeedo1.Code = "UA-000012";
                adminEmployeedo1.UserName = "admindo1";
                adminEmployeedo1.SetPassword("fdadmin@#2020");
                adminEmployeedo1.UserAccessRoles.Add(adminEmployeeRole);
            }
            UserAccess adminEmployeedo2 = ObjectSpace.FindObject<UserAccess>(
                new BinaryOperator("UserName", "admindo2"));
            if (adminEmployeedo2 == null)
            {
                adminEmployeedo2 = ObjectSpace.CreateObject<UserAccess>();
                adminEmployeedo2.Code = "UA-000013";
                adminEmployeedo2.UserName = "admindo2";
                adminEmployeedo2.SetPassword("fdadmin@#2020");
                adminEmployeedo2.UserAccessRoles.Add(adminEmployeeRole);
            }
            #endregion DeliveryOrder
            ObjectSpace.CommitChanges();
        }

        private void DefAppSetup()
        {
            try
            {
                ApplicationSetup _appSetup = ObjectSpace.FindObject<ApplicationSetup>
                                         (CriteriaOperator.Parse("SetupName = 'Setup App 1'"));
                if (_appSetup == null)
                {
                    _appSetup = ObjectSpace.CreateObject<ApplicationSetup>();
                    _appSetup.SetupName = "Setup App 1";
                    _appSetup.SetupCode = "APS0001";
                    _appSetup.Active = true;
                    _appSetup.DefaultSystem = true;

                    //Numbering Header
                    NumberingHeader _objNumberingHeader = new NumberingHeader(_appSetup.Session)
                    {
                        Code = "GN-0001",
                        Name = "Group Numbering Object",
                        NumberingType = NumberingType.Objects,
                        Active = true,
                        ApplicationSetup = _appSetup
                    };

                    _objNumberingHeader = new NumberingHeader(_appSetup.Session)
                    {
                        Code = "GN-0002",
                        Name = "Group Numbering Document",
                        NumberingType = NumberingType.Documents,
                        Active = true,
                        ApplicationSetup = _appSetup
                    };

                    _objNumberingHeader = new NumberingHeader(_appSetup.Session)
                    {
                        Code = "GN-0003",
                        Name = "Group Numbering Employee",
                        NumberingType = NumberingType.Employee,
                        Active = true,
                        ApplicationSetup = _appSetup
                    };

                    //List Import
                    ListImport _objListImport = new ListImport(_appSetup.Session)
                    {
                        No = 1,
                        ObjectList = ObjectList.ListImport
                    };

                    _objListImport = new ListImport(_appSetup.Session)
                    {
                        No = 2,
                        ObjectList = ObjectList.NumberingHeader
                    };

                    _objListImport = new ListImport(_appSetup.Session)
                    {
                        No = 3,
                        ObjectList = ObjectList.NumberingLine
                    };
                    _objListImport = new ListImport(_appSetup.Session)
                    {
                        No = 4,
                        ObjectList = ObjectList.Country
                    };
                    _objListImport = new ListImport(_appSetup.Session)
                    {
                        No = 5,
                        ObjectList = ObjectList.City
                    };
                    _objListImport = new ListImport(_appSetup.Session)
                    {
                        No = 6,
                        ObjectList = ObjectList.Company
                    };
                    _objListImport = new ListImport(_appSetup.Session)
                    {
                        No = 7,
                        ObjectList = ObjectList.Workplace
                    };
                    ObjectSpace.CommitChanges();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(ex);
            }
        }

    }
}
