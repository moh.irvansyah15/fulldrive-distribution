﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using FullDrive.Module.BusinessObjects;
using DevExpress.Xpo;

namespace FullDrive.Module.Web.Interface
{
    public interface IOrderCollectionScannerView
    {
        string Code { get; set; }

        string ScanCode { get; set; }

        OrderCollection OrderCollection { get; set; }

        event EventHandler ScanCodeChanged;
    }
}
