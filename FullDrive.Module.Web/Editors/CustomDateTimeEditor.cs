﻿using System;
using System.Web.UI.WebControls;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Model;
using DevExpress.ExpressApp.Web.Editors.ASPx;
using DevExpress.Web;
using FullDrive.Module.BusinessObjects;

namespace FullDrive.Module.Web.Editors
{
    [PropertyEditor(typeof(DateTime), FullDriveCenterEditorAliases.CustomDateTimeEditor, false)]
    public class CustomDateTimeEditor : ASPxDateTimePropertyEditor
    {
        public CustomDateTimeEditor(Type objectType, IModelMemberViewItem info) : base(objectType, info) { }

        protected override void SetupControl(WebControl control)
        {
            base.SetupControl(control);
            if (ViewEditMode == ViewEditMode.Edit)
            {
                ASPxDateEdit dateEdit = (ASPxDateEdit)control;
                dateEdit.AllowUserInput = true;
                dateEdit.AllowNull = true;
                dateEdit.TimeSectionProperties.Visible = true;
                dateEdit.UseMaskBehavior = true;
            }
        }
    }
}
