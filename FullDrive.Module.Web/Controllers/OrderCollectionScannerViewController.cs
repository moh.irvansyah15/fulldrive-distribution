﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Web.Editors;
using FullDrive.Module.BusinessObjects;
using FullDrive.Module.Web.Interface;

namespace FullDrive.Module.Web.Controllers
{
    public class OrderCollectionScannerViewController : ObjectViewController<DetailView, OrderCollectionScanner>
    {
        protected override void OnActivated()
        {
            base.OnActivated();
            foreach (WebCustomUserControlViewItem item in View.GetItems<WebCustomUserControlViewItem>())
            {
                item.ControlCreated += ViewItem_ControlCreated;
            }
            View.CurrentObjectChanged += View_CurrentObjectChanged;
        }

        protected override void OnDeactivated()
        {
            foreach (WebCustomUserControlViewItem item in View.GetItems<WebCustomUserControlViewItem>())
            {
                item.ControlCreated -= ViewItem_ControlCreated;
            }
            View.CurrentObjectChanged -= View_CurrentObjectChanged;
            base.OnDeactivated();
        }

        private void ViewItem_ControlCreated(object sender, EventArgs e)
        {
            IOrderCollectionScannerView orderCollectionScannerView = ((WebCustomUserControlViewItem)sender).Control as IOrderCollectionScannerView;
            if (orderCollectionScannerView != null)
            {
                //orderCollectionScannerView.ScanCodeChanged += OrderCollectionScannerView_ScanCodeChanged;
                InitializeOrderCollectionScannerView(orderCollectionScannerView);
            }
        }

        private void View_CurrentObjectChanged(object sender, EventArgs e)
        {
            foreach (WebCustomUserControlViewItem item in View.GetItems<WebCustomUserControlViewItem>())
            {
                IOrderCollectionScannerView orderCollectionScannerView = item.Control as IOrderCollectionScannerView;
                if (orderCollectionScannerView != null)
                {
                    InitializeOrderCollectionScannerView(orderCollectionScannerView);
                }
            }
        }

        private void OrderCollectionScannerView_ScanCodeChanged(object sender, EventArgs e)
        {
            if (ViewCurrentObject != null)
            {
                ViewCurrentObject.ScanCode = ((IOrderCollectionScannerView)sender).ScanCode;
            }
        }

        private void InitializeOrderCollectionScannerView(IOrderCollectionScannerView orderCollectionScannerView)
        {
            if (ViewCurrentObject != null)
            {
                orderCollectionScannerView.ScanCode = ViewCurrentObject.ScanCode;
                orderCollectionScannerView.Code = ViewCurrentObject.Code;
                orderCollectionScannerView.OrderCollection = ViewCurrentObject.OrderCollection;
            }
            else
            {
                orderCollectionScannerView.ScanCode = null;
                orderCollectionScannerView.Code = null;
                orderCollectionScannerView.OrderCollection = null;
            }
        }

    }
}
