﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Web.Editors;
using FullDrive.Module.BusinessObjects;
using FullDrive.Module.Web.Interface;

namespace FullDrive.Module.Web.Controllers
{
    public class PickingScannerViewController : ObjectViewController<DetailView, PickingScanner>
    {
        protected override void OnActivated()
        {
            base.OnActivated();
            foreach (WebCustomUserControlViewItem item in View.GetItems<WebCustomUserControlViewItem>())
            {
                item.ControlCreated += ViewItem_ControlCreated;
            }
            View.CurrentObjectChanged += View_CurrentObjectChanged;
        }

        protected override void OnDeactivated()
        {
            foreach (WebCustomUserControlViewItem item in View.GetItems<WebCustomUserControlViewItem>())
            {
                item.ControlCreated -= ViewItem_ControlCreated;
            }
            View.CurrentObjectChanged -= View_CurrentObjectChanged;
            base.OnDeactivated();
        }

        private void ViewItem_ControlCreated(object sender, EventArgs e)
        {
            IPickingScannerView pickingScannerView = ((WebCustomUserControlViewItem)sender).Control as IPickingScannerView;
            if (pickingScannerView != null)
            {
                //orderCollectionScannerView.ScanCodeChanged += OrderCollectionScannerView_ScanCodeChanged;
                InitializePickingScannerView(pickingScannerView);
            }
        }

        private void View_CurrentObjectChanged(object sender, EventArgs e)
        {
            foreach (WebCustomUserControlViewItem item in View.GetItems<WebCustomUserControlViewItem>())
            {
                IPickingScannerView pickingScannerView = item.Control as IPickingScannerView;
                if (pickingScannerView != null)
                {
                    InitializePickingScannerView(pickingScannerView);
                }
            }
        }

        private void PickingScannerView_ScanCodeChanged(object sender, EventArgs e)
        {
            if (ViewCurrentObject != null)
            {
                ViewCurrentObject.ScanCode = ((IPickingScannerView)sender).ScanCode;
            }
        }

        private void InitializePickingScannerView(IPickingScannerView pickingScannerView)
        {
            if (ViewCurrentObject != null)
            {
                pickingScannerView.ScanCode = ViewCurrentObject.ScanCode;
                pickingScannerView.Code = ViewCurrentObject.Code;
                pickingScannerView.Picking = ViewCurrentObject.Picking;
            }
            else
            {
                pickingScannerView.ScanCode = null;
                pickingScannerView.Code = null;
                pickingScannerView.Picking = null;
            }
        }
    }
}
