﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.IO;
using System.Web.Services;
using FullDrive.Module.BusinessObjects;
using FullDrive.Module.Web;
using FullDrive.Module.Web.Interface;
using FullDrive.Module.Web.Controllers;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Xpo;
using DevExpress.ExpressApp.Web;
using DevExpress.XtraCharts.Native;
using System.Xml;
using DevExpress.Data.Filtering;

namespace FullDrive.Web
{
    public partial class PickingScannerViewControl : System.Web.UI.UserControl, IPickingScannerView
    {
        private bool isInitialized;
        private string _scanCode;
        private string _code;
        private Picking _picking;

        public string value;

        protected void Page_Load(object sender, EventArgs e)
        {
            value = ScanCodeControl.Text;
        }

        protected override void OnInit(EventArgs e)
        {

            isInitialized = true;
            UpdateScanCodeControl();
            UpdateCodeControl();
            UpdatePickingControl();
            ScanCodeControl.TextChanged += ScanCodeControl_TextChanged;
            base.OnInit(e);
        }

        private void UpdateScanCodeControl()
        {
            if (!isInitialized) return;
            ScanCodeControl.Text = ScanCode;
        }

        private void UpdateCodeControl()
        {
            if (!isInitialized) return;
            CodeControl.Text = Code;
        }

        private void UpdatePickingControl()
        {
            if (!isInitialized) return;
            PickingControl.Text = Picking.ToString();
        }

        private void ScanCodeControl_TextChanged(object sender, EventArgs e)
        {
            _scanCode = ScanCodeControl.Text;
            if (ScanCodeChanged != null)
            {
                ScanCodeChanged(this, EventArgs.Empty);
            }
        }

        public string ScanCode
        {
            get { return _scanCode; }
            set
            {
                _scanCode = value;
                UpdateScanCodeControl();
            }
        }

        public string Code
        {
            get { return _code; }
            set
            {
                _code = value;
                UpdateCodeControl();
            }
        }

        public Picking Picking
        {
            get { return _picking; }
            set
            {
                _picking = value;
                UpdatePickingControl();
            }
        }

        public event EventHandler ScanCodeChanged;

        protected void Button1_Click(object sender, EventArgs e)
        {
            string _locHiddenValue = Request.Form["ScanCodeControlHide"];
            if (_locHiddenValue != null && _locHiddenValue != "" && this.PickingControl.Text != null)
            {
                string _locConString = null;
                string _locOPConString = null;
                Session _locSession = null;
                Company _locCompany = null;
                Workplace _locWorkplace = null;
                Division _locDivision = null;
                Department _locDepartment = null;
                Section _locSection = null;
                Employee _locEmployee = null;
                Division _locDiv = null;
                Department _locDept = null;
                Section _locSect = null;
                Employee _locPIC = null;
                Vehicle _locVehicle = null;
                BeginningInventory _locBegInv = null;

               XmlDocument doc = new XmlDocument();
                doc.Load(Server.MapPath("~/ExtConfig/ConnConfig.xml"));
                //Display all the Activities alone in the XML  
                XmlNodeList elemList = doc.GetElementsByTagName("connectionStrings");
                for (int i = 0; i < elemList.Count; i++)
                {
                    _locConString = elemList[i].InnerText.Trim();

                }

                if (_locConString != null)
                {
                    _locOPConString = _locConString;
                    XPObjectSpaceProvider osProvider = new XPObjectSpaceProvider(@_locOPConString, null);
                    if (osProvider != null)
                    {
                        IObjectSpace _locObjectSpace = osProvider.CreateObjectSpace();
                        if (_locObjectSpace != null)
                        {
                            SalesInvoice _locSalesInvoice = _locObjectSpace.FindObject<SalesInvoice>(new BinaryOperator("Code", _locHiddenValue));

                            if(_locSalesInvoice.Session != null)
                            {
                                _locSession = _locSalesInvoice.Session;
                            }
                            
                            if (_locSalesInvoice != null && _locSession != null)
                            {
                                SalesInvoice _locSalesInvoiceXPO = _locSession.FindObject<SalesInvoice>(new BinaryOperator("Code", _locHiddenValue));

                                Picking _locPicking = _locSession.FindObject<Picking>(new BinaryOperator("Code", PickingControl.Text));

                                if(_locSalesInvoiceXPO != null && _locPicking != null)
                                {
                                    PickingLine _locPickingLine = _locSession.FindObject<PickingLine>
                                                                            (new GroupOperator(GroupOperatorType.And,
                                                                            new BinaryOperator("SalesInvoice", _locSalesInvoice),
                                                                            new BinaryOperator("Picking", _locPicking)));
                                    if (_locPickingLine == null)
                                    {
                                        if (_locPicking.Company != null) { _locCompany = _locPicking.Company; }
                                        if (_locPicking.Workplace != null) { _locWorkplace = _locPicking.Workplace; }
                                        if (_locPicking.Division != null) { _locDivision = _locPicking.Division; }
                                        if (_locPicking.Department != null) { _locDepartment = _locPicking.Department; }
                                        if (_locPicking.Section != null) { _locSection = _locPicking.Section; }
                                        if (_locPicking.Employee != null) { _locEmployee = _locPicking.Employee; }
                                        if (_locPicking.Div != null) { _locDiv = _locPicking.Div; }
                                        if (_locPicking.Dept != null) { _locDept = _locPicking.Dept; }
                                        if (_locPicking.Sect != null) { _locSect = _locPicking.Sect; }
                                        if (_locPicking.PIC != null) { _locPIC = _locPicking.PIC; }
                                        if (_locPicking.Vehicle != null) { _locVehicle = _locPicking.Vehicle; }
                                        if (_locPicking.BegInv != null) { _locBegInv = _locPicking.BegInv; }

                                        PickingLine _saveDataPickingLine = new PickingLine(_locSession)
                                        {
                                            Picking = _locPicking,
                                            Company = _locCompany,
                                            Workplace = _locWorkplace,
                                            Division = _locDivision,
                                            Department = _locDepartment,
                                            Section = _locSection,
                                            Employee = _locEmployee,
                                            Div = _locDiv,
                                            Dept = _locDept,
                                            Sect = _locSect,
                                            PIC = _locPIC,
                                            SalesInvoice = _locSalesInvoiceXPO,
                                            Customer = _locSalesInvoiceXPO.SalesToCustomer,
                                            Vehicle = _locVehicle,
                                            BegInv = _locBegInv,
                                            PickingDate = _locPicking.PickingDate,
                                            Address = _locSalesInvoiceXPO.SalesToAddress,
                                        };
                                        _saveDataPickingLine.Save();
                                        _saveDataPickingLine.Session.CommitTransaction();
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}