﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="OrderCollectionScannerViewControl.ascx.cs" Inherits="FullDrive.Web.OrderCollectionScannerViewControl" %>
<%@ Register assembly="DevExpress.Web.v19.2, Version=19.2.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web" tagprefix="dx" %>

<dx:ASPxTextBox ID="CodeControl" runat="server" Caption="Code" Width="100%" Visible="true" ></dx:ASPxTextBox>
<div style="height:5px"></div>
<dx:ASPxTextBox ID="ScanCodeControl" runat="server" Caption="ScanCode" Width="100%" Visible="true" EnableClientSideAPI="True"></dx:ASPxTextBox>
<div style="height:5px"></div>
<dx:ASPxTextBox ID="OrderCollectionControl" runat="server" Caption="OrderCollection" Width="100%" Visible="true" Enabled="false" ></dx:ASPxTextBox>
<div style="height:5px"></div>
<asp:HiddenField ID="ScanCodeControlHide2" runat="server" />
<input type="hidden" id="ScanCodeControlHide" name ="ScanCodeControlHide" />
<div style="height:5px"></div>



<head>

<script type="text/javascript" src='<%=ResolveUrl("~/scr/grid.js") %>'></script>
<script type="text/javascript" src='<%=ResolveUrl("~/scr/version.js") %>'></script>
<script type="text/javascript" src='<%=ResolveUrl("~/scr/detector.js") %>'></script>
<script type="text/javascript" src='<%=ResolveUrl("~/scr/formatinf.js") %>'></script>
<script type="text/javascript" src='<%=ResolveUrl("~/scr/errorlevel.js") %>'></script>
<script type="text/javascript" src='<%=ResolveUrl("~/scr/bitmat.js") %>'></script>
<script type="text/javascript" src='<%=ResolveUrl("~/scr/datablock.js") %>'></script>
<script type="text/javascript" src='<%=ResolveUrl("~/scr/bmparser.js") %>'></script>
<script type="text/javascript" src='<%=ResolveUrl("~/scr/datamask.js") %>'></script>
<script type="text/javascript" src='<%=ResolveUrl("~/scr/rsdecoder.js") %>'></script>
<script type="text/javascript" src='<%=ResolveUrl("~/scr/gf256poly.js") %>'></script>
<script type="text/javascript" src='<%=ResolveUrl("~/scr/gf256.js") %>'></script>
<script type="text/javascript" src='<%=ResolveUrl("~/scr/decoder.js") %>'></script>
<script type="text/javascript" src='<%=ResolveUrl("~/scr/qrcode.js") %>'></script>
<script type="text/javascript" src='<%=ResolveUrl("~/scr/findpat.js") %>'></script>
<script type="text/javascript" src='<%=ResolveUrl("~/scr/alignpat.js") %>'></script>
<script type="text/javascript" src='<%=ResolveUrl("~/scr/databr.js") %>'></script>

<script type="text/javascript">
    var gCtx = null;
	var gCanvas = null;

	var imageData = null;
	var ii=0;
	var jj=0;
	var c=0;
	
	
    function dragenter(e) {
      e.stopPropagation();
      e.preventDefault();
    }

    function dragover(e) {
      e.stopPropagation();
      e.preventDefault();
    }

    function drop(e) {
      e.stopPropagation();
      e.preventDefault();

      var dt = e.dataTransfer;
      var files = dt.files;

      handleFiles(files);
    }

    function handleFiles(f)
    {
	    var o=[];
	    for(var i =0;i<f.length;i++)
	    {
	      var reader = new FileReader();

          reader.onload = (function(theFile) {
            return function(e) {
              qrcode.decode(e.target.result);
            };
          })(f[i]);

          // Read in the image file as a data URL.
          reader.readAsDataURL(f[i]);	}
    }
	
    function read(a)
    {
        if (a != null && a != "" && a != "Failed to load the image")
        {
            //alert(a);
            var h = document.getElementById("ScanCodeControlHide");
            h.value = a;
            document.getElementById('<%=ScanCodeControl.ClientID%>').innerText = a;
        }
    }	
	
    function load()
    {
        initCanvas(640, 480);
        qrcode.setWebcam("video-webcam");
	    qrcode.callback = read;
	    qrcode.decode("meqrthumb.png");
    }

    function initCanvas(ww,hh)
	{
		gCanvas = document.getElementById("qr-canvas");
		gCanvas.addEventListener("dragenter", dragenter, false);  
		gCanvas.addEventListener("dragover", dragover, false);  
		gCanvas.addEventListener("drop", drop, false);
		var w = ww;
		var h = hh;
		gCanvas.style.width = w + "px";
		gCanvas.style.height = h + "px";
		gCanvas.width = w;
		gCanvas.height = h;
		gCtx = gCanvas.getContext("2d");
		gCtx.clearRect(0, 0, w, h);
		imageData = gCtx.getImageData( 0,0,320,240);
	}

	function passLine(stringPixels) { 
		//a = (intVal >> 24) & 0xff;

		var coll = stringPixels.split("-");
	
		for(var i=0;i<320;i++) { 
			var intVal = parseInt(coll[i]);
			r = (intVal >> 16) & 0xff;
			g = (intVal >> 8) & 0xff;
			b = (intVal ) & 0xff;
			imageData.data[c+0]=r;
			imageData.data[c+1]=g;
			imageData.data[c+2]=b;
			imageData.data[c+3]=255;
			c+=4;
		} 

		if(c>=320*240*4) { 
			c=0;
      			gCtx.putImageData(imageData, 0,0);
		} 
 	} 
       
</script>

</head>

<body onload="load()">
    <div class="container">
        <label for="videoSource">Video source: </label><select id="videoSource"></select>
        <br />
	    <video autoplay="true" id="video-webcam" width="360" height="360" >
        Browsermu tidak mendukung bro, upgrade donk!
        </video>
    </div>
    <script type="text/javascript">
        const videoElement = document.querySelector("video");
        const videoSelect = document.querySelector("select#videoSource");

        navigator.mediaDevices
          .enumerateDevices()
          .then(gotDevices)
          .then(getStream)
          .catch(handleError);

        videoSelect.onchange = getStream;

        function gotDevices(deviceInfos) {
            for (let i = 0; i !== deviceInfos.length; ++i) {
                const deviceInfo = deviceInfos[i];
                const option = document.createElement("option");
                option.value = deviceInfo.deviceId;
                if (deviceInfo.kind === "videoinput") {
                    option.text = deviceInfo.label || "camera " + (videoSelect.length + 1);
                    videoSelect.appendChild(option);
                } else {
                    console.log("Found another kind of device: ", deviceInfo);
                }
            }
        }

        function getStream() {
            if (window.stream) {
                window.stream.getTracks().forEach(function (track) {
                    track.stop();
                });
            }

            const constraints = {
                video: {
                    deviceId: { exact: videoSelect.value },
                },
            };

            navigator.mediaDevices
              .getUserMedia(constraints)
              .then(gotStream)
              .catch(handleError);
        }

        function gotStream(stream) {
            window.stream = stream; // make stream available to console
            videoElement.srcObject = stream;
        }

        function handleError(error) {
            console.error("Error: ", error);
        }
</script>
    

<asp:Button ID="Button1" runat="server" Text="SaveScan" OnClick="Button1_Click" />
    <br>
<canvas id="qr-canvas" width="640" height="480"></canvas>
    
</body>