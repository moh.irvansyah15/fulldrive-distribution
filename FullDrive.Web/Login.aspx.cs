﻿using System;

using DevExpress.ExpressApp.Web;
using DevExpress.ExpressApp.Web.Templates;
using System.Web.UI.WebControls;

public partial class LoginPage : BaseXafPage {
    public override System.Web.UI.Control InnerContentPlaceHolder {
        get {
            return Content;
        }
    }

    protected void Page_Init()
    {
        CustomizeTemplateContent += (s, e) => {
            IHeaderImageControlContainer content = TemplateContent as IHeaderImageControlContainer;
            if (content == null) return;
            content.HeaderImageControl.DefaultThemeImageLocation = "Images";
            content.HeaderImageControl.ImageName = "LogoFullDrive.png";
            content.HeaderImageControl.Width = Unit.Pixel(160);
            content.HeaderImageControl.Height = Unit.Pixel(120);
        };

    }

}
