﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.IO;
using System.Web.Services;
using FullDrive.Module.BusinessObjects;
using FullDrive.Module.Web;
using FullDrive.Module.Web.Interface;
using FullDrive.Module.Web.Controllers;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Xpo;
using DevExpress.ExpressApp.Web;
using DevExpress.XtraCharts.Native;
using System.Xml;
using DevExpress.Data.Filtering;

namespace FullDrive.Web
{

    public partial class OrderCollectionScannerViewControl : System.Web.UI.UserControl, IOrderCollectionScannerView
    {
        
        private bool isInitialized;
        private string _scanCode;
        private string _code;
        private OrderCollection _orderCollection;

        public string value;

        protected void Page_Load(object sender, EventArgs e)
        {
            value = ScanCodeControl.Text;
        }

        protected override void OnInit(EventArgs e)
        {
            
            isInitialized = true;
            UpdateScanCodeControl();
            UpdateCodeControl();
            UpdateOrderCollectionControl();
            ScanCodeControl.TextChanged += ScanCodeControl_TextChanged;
            base.OnInit(e);
        }

        private void UpdateScanCodeControl()
        {
            if (!isInitialized) return;
            ScanCodeControl.Text = ScanCode;
        }

        private void UpdateCodeControl()
        {
            if (!isInitialized) return;
            CodeControl.Text = Code;
        }

        private void UpdateOrderCollectionControl()
        {
            if (!isInitialized) return;
            OrderCollectionControl.Text = OrderCollection.ToString();
        }
        
        private void ScanCodeControl_TextChanged(object sender, EventArgs e)
        {
            _scanCode = ScanCodeControl.Text;
            if (ScanCodeChanged != null)
            {
                ScanCodeChanged(this, EventArgs.Empty);
            }
        }

        public string ScanCode
        {
            get { return _scanCode; }
            set
            {
                _scanCode = value;
                UpdateScanCodeControl();
            }
        }

        public string Code
        {
            get { return _code; }
            set
            {
                _code = value;
                UpdateCodeControl();
            }
        }

        public OrderCollection OrderCollection
        {
            get { return _orderCollection; }
            set
            {
                _orderCollection = value;
                UpdateOrderCollectionControl();
            }
        }

        public event EventHandler ScanCodeChanged;

        protected void Button1_Click(object sender, EventArgs e)
        {
            string _locHiddenValue = Request.Form["ScanCodeControlHide"];
            if (_locHiddenValue != null && _locHiddenValue != "" && this.OrderCollectionControl.Text != null)
            {
                string _locConString = null;
                string _locOPConString = null;

                XmlDocument doc = new XmlDocument();
                doc.Load(Server.MapPath("~/ExtConfig/ConnConfig.xml"));
                //Display all the Activities alone in the XML  
                XmlNodeList elemList = doc.GetElementsByTagName("connectionStrings");
                for (int i = 0; i < elemList.Count; i++)
                {
                    _locConString = elemList[i].InnerText.Trim();
                    
                }

                if(_locConString != null)
                {
                    _locOPConString = _locConString;
                    XPObjectSpaceProvider osProvider = new XPObjectSpaceProvider(@_locOPConString, null);
                    if (osProvider != null)
                    {
                        IObjectSpace _locObjectSpace = osProvider.CreateObjectSpace();
                        if(_locObjectSpace != null)
                        {
                            SalesInvoice _locSalesInvoice = _locObjectSpace.FindObject<SalesInvoice>(new BinaryOperator("Code", _locHiddenValue));
                            OrderCollection _locOrderCollection = _locObjectSpace.FindObject<OrderCollection>(new BinaryOperator("Code", OrderCollectionControl.Text));
                            if (_locSalesInvoice != null && _locOrderCollection != null)
                            {
                                OrderCollectionLine _locOrderCollectionLine = _locObjectSpace.FindObject<OrderCollectionLine>
                                                                            (new GroupOperator(GroupOperatorType.And,
                                                                            new BinaryOperator("SalesInvoice", _locSalesInvoice),
                                                                            new BinaryOperator("OrderCollection", _locOrderCollection)));
                                if (_locOrderCollectionLine != null)
                                {
                                    _locOrderCollectionLine.Select = true;
                                    _locOrderCollectionLine.Save();
                                    _locOrderCollectionLine.Session.CommitTransaction();
                                }  
                            }
                        }  
                    }
                }
            }
        }
    }


}