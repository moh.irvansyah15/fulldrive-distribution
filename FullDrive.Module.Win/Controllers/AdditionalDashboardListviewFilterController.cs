﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;

using DevExpress.ExpressApp.Dashboards.Win;
using FullDrive.Module.BusinessObjects;

namespace FullDrive.Module.Win.Controllers
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class AdditionalDashboardListviewFilterController : ViewController
    {
        private NewObjectViewController newObjectController;

        public AdditionalDashboardListviewFilterController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.
        }
        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
            Frame.GetController<WinNewDashboardController>().Active.SetItemValue("custom", false);
            newObjectController = Frame.GetController<NewObjectViewController>();
            newObjectController.ObjectCreating += NewObjectController_ObjectCreating;
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }
        protected override void OnDeactivated()
        {
            if (newObjectController != null)
            {
                newObjectController.ObjectCreating -= NewObjectController_ObjectCreating;
                newObjectController = null;
            }
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        private void NewObjectController_ObjectCreating(object sender, ObjectCreatingEventArgs e)
        {
            if (typeof(IDashboardData).IsAssignableFrom(e.ObjectType))
            {
                CreateNewDashboard(e);
            }
        }

        private void CreateNewDashboard(ObjectCreatingEventArgs e)
        {
            IObjectSpace os = Application.CreateObjectSpace(typeof(ExtraData));
            ExtraData extraData = os.CreateObject<ExtraData>();
            DetailView detailView = Application.CreateDetailView(os, extraData);
            detailView.ViewEditMode = ViewEditMode.Edit;
            DialogController controller = Application.CreateController<DialogController>();
            controller.Accepting += Controller_Accepting;
            ShowViewParameters showViewParameters = new ShowViewParameters();
            showViewParameters.Controllers.Add(controller);
            showViewParameters.CreatedView = detailView;
            showViewParameters.TargetWindow = TargetWindow.NewWindow;
            showViewParameters.Context = TemplateContext.PopupWindow; // B255491
            Application.ShowViewStrategy.ShowView(showViewParameters, new ShowViewSource(Frame, null));
            e.Cancel = true;
        }

        private void Controller_Accepting(object sender, DialogControllerAcceptingEventArgs e)
        {
            IObjectSpace objectSpace = Application.CreateObjectSpace();
            ExtraData extraData = (ExtraData)e.AcceptActionArgs.CurrentObject;
            WinShowDashboardDesignerController showDashboardDesignerController = Frame.GetController<WinShowDashboardDesignerController>();
            showDashboardDesignerController.ShowDesigner(objectSpace, typeof(AdditionalDashboard));
            AdditionalDashboard dashboardData = showDashboardDesignerController.GetDashboardData() as AdditionalDashboard;
            if (dashboardData != null)
            {
                dashboardData.Company = extraData.Company;
                dashboardData.Division = extraData.Division;
                dashboardData.Department = extraData.Department;
                dashboardData.Section = extraData.Section;
                objectSpace.CommitChanges();
                View.ObjectSpace.Refresh();
            }
        }

        private void AdditionalDashboardListViewFilterController_Closed(object sender, EventArgs e)
        {
            if (View != null)
            {
                View.ObjectSpace.Refresh();
            }
        }

        private void CustomDashboardAdditionalDashboard_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            IObjectSpace objectSpace = Application.CreateObjectSpace();
            DetailView view = Application.CreateDetailView(objectSpace, "AdditionalDashboard_DetailView", false);
            e.ShowViewParameters.CreatedView = view;
            view.CurrentObject = objectSpace.GetObject(e.CurrentObject);
            view.ViewEditMode = ViewEditMode.Edit;
            view.Closed += AdditionalDashboardListViewFilterController_Closed;
        }
    }
}
