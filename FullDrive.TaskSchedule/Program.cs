﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DevExpress.ExpressApp.Xpo;
using DevExpress.ExpressApp;
using FullDrive.Module.BusinessObjects;
using System.Timers;

namespace FullDrive.TaskSchedule
{
    class Program
    {
        public static System.Timers.Timer aTimer;

        static void Main(string[] args)
        {
            LongProcessInterval();
            Console.ReadLine();
        }

        private static void LongProcessInterval()
        {
            
            aTimer = new System.Timers.Timer(14400000);
            //aTimer = new System.Timers.Timer(10000);
            aTimer.Elapsed += new ElapsedEventHandler(RunThis);
            aTimer.AutoReset = true;
            aTimer.Enabled = true;
        }

        private static void RunThis(object source, ElapsedEventArgs e)
        {
            DateTime now = DateTime.Now;
            Console.WriteLine("System auto check every 4 Hours");
            XpoTypesInfoHelper.GetXpoTypeInfoSource();
            XafTypesInfo.Instance.RegisterEntity(typeof(TaskMode));
            XafTypesInfo.Instance.RegisterEntity(typeof(DiscountRule));
            XafTypesInfo.Instance.RegisterEntity(typeof(FreeItemRule));
            XafTypesInfo.Instance.RegisterEntity(typeof(PriceLine));
            XafTypesInfo.Instance.RegisterEntity(typeof(NumberingLine));
            XPObjectSpaceProvider osProvider = new XPObjectSpaceProvider(
            @"XpoProvider=MSSqlServer;Data Source=localhost;User ID=sa;Password=coolvank@#2020;Initial Catalog=DISTRIBUTION_DB;Persist Security Info=true", null);

            if (osProvider != null)
            {

                IObjectSpace objectSpace = osProvider.CreateObjectSpace();
                foreach (TaskMode _locTaskMode in objectSpace.GetObjects<TaskMode>())
                {
                    if (_locTaskMode.Active == true && _locTaskMode.TaskObject == Module.CustomProcess.TaskObject.DiscountRule )
                    {
                        foreach (DiscountRule _locDiscountRule in objectSpace.GetObjects<DiscountRule>())
                        {
                            if ( _locDiscountRule.Active == false && _locDiscountRule.StartDate.ToShortDateString() == now.ToShortDateString() )
                            { 
                                _locDiscountRule.Active = true;
                            }
                            if (_locDiscountRule.Active == true && _locDiscountRule.EndDate < now)
                            {
                                _locDiscountRule.Active = false;
                            }

                        }
                    }
                    if (_locTaskMode.Active == true && _locTaskMode.TaskObject == Module.CustomProcess.TaskObject.FreeItemRule)
                    {
                        foreach (FreeItemRule _locFreeItemRule in objectSpace.GetObjects<FreeItemRule>())
                        {
                            if ( _locFreeItemRule.Active == false && _locFreeItemRule.StartDate.ToShortDateString() == now.ToShortDateString() )
                            {
                                _locFreeItemRule.Active = true;
                            }
                            if (_locFreeItemRule.Active == true && _locFreeItemRule.EndDate < now)
                            {
                                _locFreeItemRule.Active = false;
                            }
                        }
                    }
                    if (_locTaskMode.Active == true && _locTaskMode.TaskObject == Module.CustomProcess.TaskObject.PriceLine)
                    {
                        foreach (PriceLine _locPriceLine in objectSpace.GetObjects<PriceLine>())
                        {
                            if ( _locPriceLine.Active == false && _locPriceLine.StartDate.ToShortDateString() == now.ToShortDateString() )
                            {
                                _locPriceLine.Active = true;
                            }
                            if (_locPriceLine.Active == true && _locPriceLine.EndDate < now)
                            {
                                _locPriceLine.Active = false;
                            }
                        }
                    }
                    if (_locTaskMode.Active == true && _locTaskMode.TaskObject == Module.CustomProcess.TaskObject.NumberingLine)
                    {
                        foreach (NumberingLine _locNumberingLine in objectSpace.GetObjects<NumberingLine>())
                        {
                            if(_locNumberingLine.Date == true) 
                            {
                                if (_locNumberingLine.Active == false && _locNumberingLine.StartDate.ToShortDateString() == now.ToShortDateString())
                                {
                                    _locNumberingLine.Active = true;
                                }
                                if (_locNumberingLine.Active == true && _locNumberingLine.EndDate < now)
                                {
                                    _locNumberingLine.Active = false;
                                }
                            }
                        }
                    }
                }
                objectSpace.CommitChanges();
                objectSpace.Refresh();
                objectSpace.Dispose();

            }
            osProvider.Dispose();
        }
    }
}
