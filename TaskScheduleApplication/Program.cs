﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DevExpress.ExpressApp.Xpo;
using DevExpress.ExpressApp;
using FullDrive.Module.BusinessObjects;
using System.Timers;
using System.Threading.Tasks;

namespace TaskScheduleApplication
{
    class Program
    {
        public static System.Timers.Timer aTimer;
       
        static async void Main(string[] args)
        {
            //await LongProcessInterval();
            Console.ReadLine();
        }

        private Task LongProcessInterval()
        {
            return Task.Run(() => { System.Threading.Thread.Sleep(5000); });
            aTimer = new System.Timers.Timer(14400000);
            aTimer.Elapsed += new ElapsedEventHandler(RunThis);
            aTimer.AutoReset = true;
            aTimer.Enabled = true;
        }

        private static void RunThis(object source, ElapsedEventArgs e)
        {
            DateTime now = DateTime.Now;
            Console.WriteLine("System auto check every 4 Hours");
            XpoTypesInfoHelper.GetXpoTypeInfoSource();
            XafTypesInfo.Instance.RegisterEntity(typeof(DiscountRule));
            XafTypesInfo.Instance.RegisterEntity(typeof(FreeItemRule));
            XPObjectSpaceProvider osProvider = new XPObjectSpaceProvider(
            @"XpoProvider=MSSqlServer;Data Source=localhost;User ID=sa;Password=coolvank@#2020;Initial Catalog=Main_DISTRIBUTION_DB;Persist Security Info=true", null);
            
            if (osProvider != null )
            {
                
                IObjectSpace objectSpace = osProvider.CreateObjectSpace();
                
                foreach (DiscountRule _locDiscountRule in objectSpace.GetObjects<DiscountRule>())
                {
                    if (_locDiscountRule.Active == false && _locDiscountRule.StartDate > now.AddDays(-1) && _locDiscountRule.StartDate < now.AddDays(1))
                    {
                        _locDiscountRule.Active = true;
                    }
                    if(_locDiscountRule.Active == true && _locDiscountRule.EndDate < now)
                    {
                        _locDiscountRule.Active = false;
                    }
                    
                }
                foreach (FreeItemRule _locFreeItemRule in objectSpace.GetObjects<FreeItemRule>())
                {
                    if (_locFreeItemRule.Active == false && _locFreeItemRule.StartDate > now.AddDays(-1) && _locFreeItemRule.StartDate < now.AddDays(1))
                    {
                        _locFreeItemRule.Active = true;
                    }
                    if (_locFreeItemRule.Active == true && _locFreeItemRule.EndDate < now)
                    {
                        _locFreeItemRule.Active = false;
                    }
                }
                objectSpace.CommitChanges();
                objectSpace.Refresh();
                objectSpace.Dispose();
                
            }
            osProvider.Dispose();
        }
    }
}
